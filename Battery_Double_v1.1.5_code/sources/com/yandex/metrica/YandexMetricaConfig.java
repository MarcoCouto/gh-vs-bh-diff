package com.yandex.metrica;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.az;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.yg;
import com.yandex.metrica.impl.ob.yk;
import com.yandex.metrica.impl.ob.yl;

public class YandexMetricaConfig {
    @NonNull
    public final String apiKey;
    @Nullable
    public final String appVersion;
    @Nullable
    public final Boolean crashReporting;
    @Nullable
    public final Boolean firstActivationAsUpdate;
    @Nullable
    public final Boolean installedAppCollecting;
    @Nullable
    public final Location location;
    @Nullable
    public final Boolean locationTracking;
    @Nullable
    public final Boolean logs;
    @Nullable
    public final Integer maxReportsInDatabaseCount;
    @Nullable
    public final Boolean nativeCrashReporting;
    @Nullable
    public final PreloadInfo preloadInfo;
    @Nullable
    public final Integer sessionTimeout;
    @Nullable
    public final Boolean statisticsSending;

    public static class Builder {

        /* renamed from: a reason: collision with root package name */
        private static final yk<String> f4606a = new yg(new yl());
        /* access modifiers changed from: private */
        @NonNull
        public final String b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;
        /* access modifiers changed from: private */
        @Nullable
        public Integer d;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean e;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean f;
        /* access modifiers changed from: private */
        @Nullable
        public Location g;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean h;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean i;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean j;
        /* access modifiers changed from: private */
        @Nullable
        public PreloadInfo k;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean l;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean m;
        /* access modifiers changed from: private */
        @Nullable
        public Integer n;

        protected Builder(@NonNull String str) {
            f4606a.a(str);
            this.b = str;
        }

        @NonNull
        public Builder withAppVersion(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Builder withSessionTimeout(int i2) {
            this.d = Integer.valueOf(i2);
            return this;
        }

        @NonNull
        public Builder withCrashReporting(boolean z) {
            this.e = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public Builder withNativeCrashReporting(boolean z) {
            this.f = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public Builder withLogs() {
            this.j = Boolean.valueOf(true);
            return this;
        }

        @NonNull
        public Builder withLocation(@Nullable Location location) {
            this.g = location;
            return this;
        }

        @NonNull
        public Builder withLocationTracking(boolean z) {
            this.h = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public Builder withInstalledAppCollecting(boolean z) {
            this.i = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public Builder withPreloadInfo(@Nullable PreloadInfo preloadInfo) {
            this.k = preloadInfo;
            return this;
        }

        @NonNull
        public Builder handleFirstActivationAsUpdate(boolean z) {
            this.l = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public Builder withStatisticsSending(boolean z) {
            this.m = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public Builder withMaxReportsInDatabaseCount(int i2) {
            this.n = Integer.valueOf(i2);
            return this;
        }

        @NonNull
        public YandexMetricaConfig build() {
            return new YandexMetricaConfig(this);
        }
    }

    @NonNull
    public static Builder newConfigBuilder(@NonNull String str) {
        return new Builder(str);
    }

    public static YandexMetricaConfig fromJson(String str) {
        return new az().a(str);
    }

    protected YandexMetricaConfig(@NonNull Builder builder) {
        this.apiKey = builder.b;
        this.appVersion = builder.c;
        this.sessionTimeout = builder.d;
        this.crashReporting = builder.e;
        this.nativeCrashReporting = builder.f;
        this.location = builder.g;
        this.locationTracking = builder.h;
        this.installedAppCollecting = builder.i;
        this.logs = builder.j;
        this.preloadInfo = builder.k;
        this.firstActivationAsUpdate = builder.l;
        this.statisticsSending = builder.m;
        this.maxReportsInDatabaseCount = builder.n;
    }

    protected YandexMetricaConfig(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        this.apiKey = yandexMetricaConfig.apiKey;
        this.appVersion = yandexMetricaConfig.appVersion;
        this.sessionTimeout = yandexMetricaConfig.sessionTimeout;
        this.crashReporting = yandexMetricaConfig.crashReporting;
        this.nativeCrashReporting = yandexMetricaConfig.nativeCrashReporting;
        this.location = yandexMetricaConfig.location;
        this.locationTracking = yandexMetricaConfig.locationTracking;
        this.installedAppCollecting = yandexMetricaConfig.installedAppCollecting;
        this.logs = yandexMetricaConfig.logs;
        this.preloadInfo = yandexMetricaConfig.preloadInfo;
        this.firstActivationAsUpdate = yandexMetricaConfig.firstActivationAsUpdate;
        this.statisticsSending = yandexMetricaConfig.statisticsSending;
        this.maxReportsInDatabaseCount = yandexMetricaConfig.maxReportsInDatabaseCount;
    }

    @NonNull
    public static Builder createBuilderFromConfig(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        Builder newConfigBuilder = newConfigBuilder(yandexMetricaConfig.apiKey);
        if (cx.a((Object) yandexMetricaConfig.appVersion)) {
            newConfigBuilder.withAppVersion(yandexMetricaConfig.appVersion);
        }
        if (cx.a((Object) yandexMetricaConfig.sessionTimeout)) {
            newConfigBuilder.withSessionTimeout(yandexMetricaConfig.sessionTimeout.intValue());
        }
        if (cx.a((Object) yandexMetricaConfig.crashReporting)) {
            newConfigBuilder.withCrashReporting(yandexMetricaConfig.crashReporting.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.nativeCrashReporting)) {
            newConfigBuilder.withNativeCrashReporting(yandexMetricaConfig.nativeCrashReporting.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.location)) {
            newConfigBuilder.withLocation(yandexMetricaConfig.location);
        }
        if (cx.a((Object) yandexMetricaConfig.locationTracking)) {
            newConfigBuilder.withLocationTracking(yandexMetricaConfig.locationTracking.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.installedAppCollecting)) {
            newConfigBuilder.withInstalledAppCollecting(yandexMetricaConfig.installedAppCollecting.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.logs) && yandexMetricaConfig.logs.booleanValue()) {
            newConfigBuilder.withLogs();
        }
        if (cx.a((Object) yandexMetricaConfig.preloadInfo)) {
            newConfigBuilder.withPreloadInfo(yandexMetricaConfig.preloadInfo);
        }
        if (cx.a((Object) yandexMetricaConfig.firstActivationAsUpdate)) {
            newConfigBuilder.handleFirstActivationAsUpdate(yandexMetricaConfig.firstActivationAsUpdate.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.statisticsSending)) {
            newConfigBuilder.withStatisticsSending(yandexMetricaConfig.statisticsSending.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.maxReportsInDatabaseCount)) {
            newConfigBuilder.withMaxReportsInDatabaseCount(yandexMetricaConfig.maxReportsInDatabaseCount.intValue());
        }
        return newConfigBuilder;
    }

    public String toJson() {
        return new az().a(this);
    }
}
