package com.yandex.metrica;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.vr;
import com.yandex.metrica.impl.ob.vz;
import com.yandex.metrica.impl.ob.yf;
import com.yandex.metrica.impl.ob.yg;
import com.yandex.metrica.impl.ob.yk;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class MetricaEventHandler extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    public static final Set<BroadcastReceiver> f4596a = new HashSet();
    private static final yk<BroadcastReceiver[]> b = new yg(new yf("Broadcast receivers"));

    public void onReceive(Context context, Intent intent) {
        if (a(intent)) {
            a(context, intent);
        }
        vz a2 = vr.a();
        for (BroadcastReceiver broadcastReceiver : f4596a) {
            String format = String.format("Sending referrer to %s", new Object[]{broadcastReceiver.getClass().getName()});
            if (a2.c()) {
                a2.a(format);
            }
            broadcastReceiver.onReceive(context, intent);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(Intent intent) {
        return "com.android.vending.INSTALL_REFERRER".equals(intent.getAction());
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra(TapjoyConstants.TJC_REFERRER);
        if (!TextUtils.isEmpty(stringExtra)) {
            db.a(context).a(stringExtra);
        }
    }

    static void a(BroadcastReceiver... broadcastReceiverArr) {
        b.a(broadcastReceiverArr);
        Collections.addAll(f4596a, broadcastReceiverArr);
    }
}
