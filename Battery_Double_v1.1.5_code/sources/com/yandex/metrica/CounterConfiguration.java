package com.yandex.metrica;

import android.content.ContentValues;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.oo;
import com.yandex.metrica.impl.ob.x;

@Deprecated
public class CounterConfiguration implements Parcelable {
    public static final Creator<CounterConfiguration> CREATOR = new Creator<CounterConfiguration>() {
        /* renamed from: a */
        public CounterConfiguration createFromParcel(Parcel parcel) {
            return new CounterConfiguration((ContentValues) parcel.readBundle(x.class.getClassLoader()).getParcelable("com.yandex.metrica.CounterConfiguration.data"));
        }

        /* renamed from: a */
        public CounterConfiguration[] newArray(int i) {
            return new CounterConfiguration[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final ContentValues f4593a;

    public enum a {
        MAIN(ParametersKeys.MAIN),
        MANUAL("manual"),
        APPMETRICA("appmetrica"),
        COMMUTATION("commutation"),
        SELF_DIAGNOSTIC_MAIN("self_diagnostic_main"),
        SELF_DIAGNOSTIC_MANUAL("self_diagnostic_manual");
        
        @NonNull
        private final String g;

        private a(String str) {
            this.g = str;
        }

        @NonNull
        public String a() {
            return this.g;
        }

        @NonNull
        public static a a(@Nullable String str) {
            a[] values;
            for (a aVar : values()) {
                if (aVar.g.equals(str)) {
                    return aVar;
                }
            }
            return MAIN;
        }
    }

    public int describeContents() {
        return 0;
    }

    public synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append("CounterConfiguration{mParamsMapping=");
        sb.append(this.f4593a);
        sb.append('}');
        return sb.toString();
    }

    public CounterConfiguration(@NonNull CounterConfiguration counterConfiguration) {
        synchronized (counterConfiguration) {
            this.f4593a = new ContentValues(counterConfiguration.f4593a);
            r();
        }
    }

    public CounterConfiguration() {
        this.f4593a = new ContentValues();
    }

    public CounterConfiguration(@NonNull String str) {
        this();
        synchronized (this) {
            b(str);
        }
    }

    public CounterConfiguration(j jVar) {
        this();
        synchronized (this) {
            f(jVar.apiKey);
            a(jVar.sessionTimeout);
            a(jVar);
            b(jVar);
            c(jVar);
            d(jVar);
            b(jVar.f);
            c(jVar.g);
            e(jVar);
            f(jVar);
            g(jVar);
            h(jVar);
            i(jVar);
            b(jVar.statisticsSending);
            d(jVar.maxReportsInDatabaseCount);
            c(jVar.nativeCrashReporting);
            a(a.MAIN);
        }
    }

    public CounterConfiguration(@NonNull f fVar) {
        this();
        synchronized (this) {
            f(fVar.apiKey);
            a(fVar.sessionTimeout);
            b(fVar.f4610a);
            c(fVar.b);
            a(fVar.logs);
            b(fVar.statisticsSending);
            d(fVar.maxReportsInDatabaseCount);
            e(fVar.apiKey);
        }
    }

    private void e(@Nullable String str) {
        if ("20799a27-fa80-4b36-b2db-0f8141f24180".equals(str)) {
            a(a.APPMETRICA);
        } else {
            a(a.MANUAL);
        }
    }

    private void f(@Nullable String str) {
        if (cx.a((Object) str)) {
            b(str);
        }
    }

    private void a(@Nullable Integer num) {
        if (cx.a((Object) num)) {
            c(num.intValue());
        }
    }

    private void a(j jVar) {
        if (cx.a((Object) jVar.location)) {
            a(jVar.location);
        }
    }

    private void b(j jVar) {
        if (cx.a((Object) jVar.locationTracking)) {
            a(jVar.locationTracking.booleanValue());
        }
    }

    private void c(j jVar) {
        if (cx.a((Object) jVar.installedAppCollecting)) {
            b(jVar.installedAppCollecting.booleanValue());
        }
    }

    private void d(j jVar) {
        if (cx.a((Object) jVar.f5324a)) {
            a(jVar.f5324a);
        }
    }

    private void b(@Nullable Integer num) {
        if (cx.a((Object) num)) {
            a(num.intValue());
        }
    }

    private void c(@Nullable Integer num) {
        if (cx.a((Object) num)) {
            b(num.intValue());
        }
    }

    private void a(@Nullable Boolean bool) {
        if (cx.a((Object) bool)) {
            c(bool.booleanValue());
        }
    }

    private void e(j jVar) {
        if (!TextUtils.isEmpty(jVar.appVersion)) {
            d(jVar.appVersion);
        }
    }

    private void f(j jVar) {
        if (cx.a((Object) jVar.e)) {
            d(jVar.e.intValue());
        }
    }

    private void g(j jVar) {
        if (cx.a((Object) jVar.j)) {
            d(jVar.j.booleanValue());
        }
    }

    private void h(j jVar) {
        if (cx.a((Object) jVar.k)) {
            e(jVar.k.booleanValue());
        }
    }

    private void i(j jVar) {
        if (cx.a((Object) jVar.firstActivationAsUpdate)) {
            f(jVar.firstActivationAsUpdate.booleanValue());
        }
    }

    private void b(@Nullable Boolean bool) {
        if (cx.a((Object) bool)) {
            g(bool.booleanValue());
        }
    }

    private void d(@Nullable Integer num) {
        if (cx.a((Object) num)) {
            this.f4593a.put("MAX_REPORTS_IN_DB_COUNT", num);
        }
    }

    private void c(@Nullable Boolean bool) {
        if (cx.a((Object) bool)) {
            this.f4593a.put("CFG_NATIVE_CRASHES_ENABLED", bool);
        }
    }

    @VisibleForTesting
    public synchronized void a(int i) {
        this.f4593a.put("CFG_DISPATCH_PERIOD", Integer.valueOf(i));
    }

    @Nullable
    public Integer a() {
        return this.f4593a.getAsInteger("CFG_DISPATCH_PERIOD");
    }

    @VisibleForTesting
    public synchronized void b(int i) {
        ContentValues contentValues = this.f4593a;
        String str = "CFG_MAX_REPORTS_COUNT";
        if (i <= 0) {
            i = Integer.MAX_VALUE;
        }
        contentValues.put(str, Integer.valueOf(i));
    }

    @Nullable
    public Integer b() {
        return this.f4593a.getAsInteger("CFG_MAX_REPORTS_COUNT");
    }

    @VisibleForTesting
    public synchronized void c(int i) {
        this.f4593a.put("CFG_SESSION_TIMEOUT", Integer.valueOf(i));
    }

    @Nullable
    public Integer c() {
        return this.f4593a.getAsInteger("CFG_SESSION_TIMEOUT");
    }

    public final synchronized void a(@Nullable String str) {
        ContentValues contentValues = this.f4593a;
        String str2 = "CFG_DEVICE_SIZE_TYPE";
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        contentValues.put(str2, str);
    }

    @Nullable
    public String d() {
        return this.f4593a.getAsString("CFG_DEVICE_SIZE_TYPE");
    }

    @VisibleForTesting
    public synchronized void b(String str) {
        this.f4593a.put("CFG_API_KEY", str);
    }

    public synchronized void c(String str) {
        this.f4593a.put("CFG_UUID", str);
    }

    public String e() {
        return this.f4593a.getAsString("CFG_API_KEY");
    }

    public synchronized void a(boolean z) {
        this.f4593a.put("CFG_LOCATION_TRACKING", Boolean.valueOf(z));
    }

    @Nullable
    public Boolean f() {
        return this.f4593a.getAsBoolean("CFG_LOCATION_TRACKING");
    }

    public final synchronized void d(String str) {
        this.f4593a.put("CFG_APP_VERSION", str);
    }

    public String g() {
        return this.f4593a.getAsString("CFG_APP_VERSION");
    }

    public synchronized void d(int i) {
        this.f4593a.put("CFG_APP_VERSION_CODE", String.valueOf(i));
    }

    public String h() {
        return this.f4593a.getAsString("CFG_APP_VERSION_CODE");
    }

    public synchronized void b(boolean z) {
        this.f4593a.put("CFG_COLLECT_INSTALLED_APPS", Boolean.valueOf(z));
    }

    @Nullable
    public Boolean i() {
        return this.f4593a.getAsBoolean("CFG_COLLECT_INSTALLED_APPS");
    }

    public final synchronized void a(Location location) {
        this.f4593a.put("CFG_MANUAL_LOCATION", oo.a(location));
    }

    public synchronized void c(boolean z) {
        this.f4593a.put("CFG_IS_LOG_ENABLED", Boolean.valueOf(z));
    }

    @Nullable
    public Boolean j() {
        return this.f4593a.getAsBoolean("CFG_IS_LOG_ENABLED");
    }

    public Location k() {
        if (this.f4593a.containsKey("CFG_MANUAL_LOCATION")) {
            return oo.a(this.f4593a.getAsByteArray("CFG_MANUAL_LOCATION"));
        }
        return null;
    }

    @Nullable
    public Boolean l() {
        return this.f4593a.getAsBoolean("CFG_AUTO_PRELOAD_INFO_DETECTION");
    }

    @Nullable
    public Boolean m() {
        return this.f4593a.getAsBoolean("CFG_NATIVE_CRASHES_ENABLED");
    }

    public synchronized void d(boolean z) {
        this.f4593a.put("CFG_AUTO_PRELOAD_INFO_DETECTION", Boolean.valueOf(z));
    }

    public synchronized void e(boolean z) {
        this.f4593a.put("CFG_PERMISSIONS_COLLECTING", Boolean.valueOf(z));
    }

    public final synchronized void f(boolean z) {
        this.f4593a.put("CFG_IS_FIRST_ACTIVATION_AS_UPDATE", Boolean.valueOf(z));
    }

    @Nullable
    public Boolean n() {
        return this.f4593a.getAsBoolean("CFG_IS_FIRST_ACTIVATION_AS_UPDATE");
    }

    @Nullable
    public Integer o() {
        return this.f4593a.getAsInteger("MAX_REPORTS_IN_DB_COUNT");
    }

    public Boolean p() {
        return this.f4593a.getAsBoolean("CFG_STATISTICS_SENDING");
    }

    public final synchronized void g(boolean z) {
        this.f4593a.put("CFG_STATISTICS_SENDING", Boolean.valueOf(z));
    }

    public synchronized void writeToParcel(Parcel parcel, int i) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.yandex.metrica.CounterConfiguration.data", this.f4593a);
        parcel.writeBundle(bundle);
    }

    public synchronized void a(@NonNull a aVar) {
        this.f4593a.put("CFG_REPORTER_TYPE", aVar.a());
    }

    @NonNull
    public a q() {
        return a.a(this.f4593a.getAsString("CFG_REPORTER_TYPE"));
    }

    public synchronized void a(Bundle bundle) {
        bundle.putParcelable("COUNTER_CFG_OBJ", this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        return;
     */
    public synchronized void b(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getInt("CFG_DISPATCH_PERIOD") != 0) {
                a(bundle.getInt("CFG_DISPATCH_PERIOD"));
            }
            if (bundle.getInt("CFG_SESSION_TIMEOUT") != 0) {
                c(bundle.getInt("CFG_SESSION_TIMEOUT"));
            }
            if (bundle.getInt("CFG_MAX_REPORTS_COUNT") != 0) {
                b(bundle.getInt("CFG_MAX_REPORTS_COUNT"));
            }
            if (bundle.getString("CFG_API_KEY") != null && !"-1".equals(bundle.getString("CFG_API_KEY"))) {
                b(bundle.getString("CFG_API_KEY"));
            }
        }
    }

    public static CounterConfiguration c(Bundle bundle) {
        CounterConfiguration counterConfiguration = null;
        if (bundle != null) {
            try {
                counterConfiguration = (CounterConfiguration) bundle.getParcelable("COUNTER_CFG_OBJ");
            } catch (Throwable unused) {
                return null;
            }
        }
        if (counterConfiguration == null) {
            counterConfiguration = new CounterConfiguration();
        }
        counterConfiguration.b(bundle);
        return counterConfiguration;
    }

    @VisibleForTesting
    CounterConfiguration(ContentValues contentValues) {
        this.f4593a = contentValues;
        r();
    }

    private void r() {
        if (!this.f4593a.containsKey("CFG_REPORTER_TYPE")) {
            if (this.f4593a.containsKey("CFG_MAIN_REPORTER")) {
                if (this.f4593a.getAsBoolean("CFG_MAIN_REPORTER").booleanValue()) {
                    a(a.MAIN);
                } else {
                    e(e());
                }
            } else if (this.f4593a.containsKey("CFG_COMMUTATION_REPORTER") && this.f4593a.getAsBoolean("CFG_COMMUTATION_REPORTER").booleanValue()) {
                a(a.COMMUTATION);
            }
        }
    }
}
