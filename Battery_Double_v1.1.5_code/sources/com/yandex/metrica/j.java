package com.yandex.metrica;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.YandexMetricaConfig.Builder;
import com.yandex.metrica.impl.ob.cx;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

public class j extends YandexMetricaConfig {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final String f5324a;
    @Nullable
    public final Map<String, String> b;
    @Nullable
    public final String c;
    @Nullable
    public final List<String> d;
    @Nullable
    public final Integer e;
    @Nullable
    public final Integer f;
    @Nullable
    public final Integer g;
    @Nullable
    public final Map<String, String> h;
    @Nullable
    public final Map<String, String> i;
    @Nullable
    public final Boolean j;
    @Nullable
    public final Boolean k;
    @Nullable
    public final Boolean l;
    @Nullable
    public final e m;
    @Nullable
    public final c n;

    public static final class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public String f5325a;
        /* access modifiers changed from: private */
        @NonNull
        public Builder b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> d;
        /* access modifiers changed from: private */
        @Nullable
        public Integer e;
        /* access modifiers changed from: private */
        @Nullable
        public Map<String, String> f;
        /* access modifiers changed from: private */
        @Nullable
        public Integer g;
        /* access modifiers changed from: private */
        @Nullable
        public Integer h;
        /* access modifiers changed from: private */
        @NonNull
        public LinkedHashMap<String, String> i = new LinkedHashMap<>();
        /* access modifiers changed from: private */
        @NonNull
        public LinkedHashMap<String, String> j = new LinkedHashMap<>();
        /* access modifiers changed from: private */
        @Nullable
        public Boolean k;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean l;
        /* access modifiers changed from: private */
        @Nullable
        public e m;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean n;
        /* access modifiers changed from: private */
        @Nullable
        public c o;

        protected a(@NonNull String str) {
            this.b = YandexMetricaConfig.newConfigBuilder(str);
        }

        @NonNull
        public a a(@NonNull String str) {
            this.b.withAppVersion(str);
            return this;
        }

        @NonNull
        public a a(int i2) {
            this.b.withSessionTimeout(i2);
            return this;
        }

        @NonNull
        public a b(@Nullable String str) {
            this.f5325a = str;
            return this;
        }

        @NonNull
        public a a(boolean z) {
            this.b.withCrashReporting(z);
            return this;
        }

        @NonNull
        public a b(boolean z) {
            this.b.withNativeCrashReporting(z);
            return this;
        }

        @NonNull
        public a c(boolean z) {
            this.n = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public a a() {
            this.b.withLogs();
            return this;
        }

        @NonNull
        public a a(@Nullable Location location) {
            this.b.withLocation(location);
            return this;
        }

        @NonNull
        public a d(boolean z) {
            this.b.withLocationTracking(z);
            return this;
        }

        @NonNull
        public a e(boolean z) {
            this.b.withInstalledAppCollecting(z);
            return this;
        }

        @NonNull
        public a f(boolean z) {
            this.b.withStatisticsSending(z);
            return this;
        }

        @NonNull
        public a a(@NonNull String str, @Nullable String str2) {
            this.i.put(str, str2);
            return this;
        }

        @NonNull
        public a c(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public a a(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public a b(int i2) {
            if (i2 >= 0) {
                this.e = Integer.valueOf(i2);
                return this;
            }
            throw new IllegalArgumentException(String.format(Locale.US, "Invalid %1$s. %1$s should be positive.", new Object[]{"App Build Number"}));
        }

        @NonNull
        public a a(@Nullable Map<String, String> map, @Nullable Boolean bool) {
            this.k = bool;
            this.f = map;
            return this;
        }

        @NonNull
        public a c(int i2) {
            this.h = Integer.valueOf(i2);
            return this;
        }

        @NonNull
        public a d(int i2) {
            this.g = Integer.valueOf(i2);
            return this;
        }

        @NonNull
        public a a(@Nullable PreloadInfo preloadInfo) {
            this.b.withPreloadInfo(preloadInfo);
            return this;
        }

        @NonNull
        public a g(boolean z) {
            this.b.handleFirstActivationAsUpdate(z);
            return this;
        }

        @NonNull
        public a b(@NonNull String str, @Nullable String str2) {
            this.j.put(str, str2);
            return this;
        }

        @NonNull
        public a h(boolean z) {
            this.l = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public a a(@NonNull e eVar) {
            this.m = eVar;
            return this;
        }

        @NonNull
        public a e(int i2) {
            this.b.withMaxReportsInDatabaseCount(i2);
            return this;
        }

        @NonNull
        public a a(@Nullable c cVar) {
            this.o = cVar;
            return this;
        }

        @NonNull
        public j b() {
            return new j(this);
        }
    }

    public j(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        super(yandexMetricaConfig);
        this.f5324a = null;
        this.b = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.c = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.d = null;
        this.h = null;
        this.m = null;
        this.l = null;
        this.n = null;
    }

    @NonNull
    public static j a(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        if (yandexMetricaConfig instanceof j) {
            return (j) yandexMetricaConfig;
        }
        return new j(yandexMetricaConfig);
    }

    @NonNull
    public static a a(@NonNull String str) {
        return new a(str);
    }

    @NonNull
    public static a a(@NonNull j jVar) {
        a a2 = b(jVar).a((List<String>) new ArrayList<String>());
        if (cx.a((Object) jVar.f5324a)) {
            a2.c(jVar.f5324a);
        }
        if (cx.a((Object) jVar.b) && cx.a((Object) jVar.j)) {
            a2.a(jVar.b, jVar.j);
        }
        if (cx.a((Object) jVar.e)) {
            a2.b(jVar.e.intValue());
        }
        if (cx.a((Object) jVar.f)) {
            a2.d(jVar.f.intValue());
        }
        if (cx.a((Object) jVar.g)) {
            a2.c(jVar.g.intValue());
        }
        if (cx.a((Object) jVar.c)) {
            a2.b(jVar.c);
        }
        if (cx.a((Object) jVar.i)) {
            for (Entry entry : jVar.i.entrySet()) {
                a2.b((String) entry.getKey(), (String) entry.getValue());
            }
        }
        if (cx.a((Object) jVar.k)) {
            a2.h(jVar.k.booleanValue());
        }
        if (cx.a((Object) jVar.d)) {
            a2.a(jVar.d);
        }
        if (cx.a((Object) jVar.h)) {
            for (Entry entry2 : jVar.h.entrySet()) {
                a2.a((String) entry2.getKey(), (String) entry2.getValue());
            }
        }
        if (cx.a((Object) jVar.m)) {
            a2.a(jVar.m);
        }
        if (cx.a((Object) jVar.l)) {
            a2.c(jVar.l.booleanValue());
        }
        return a2;
    }

    @NonNull
    public static a b(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        a a2 = a(yandexMetricaConfig.apiKey);
        if (cx.a((Object) yandexMetricaConfig.appVersion)) {
            a2.a(yandexMetricaConfig.appVersion);
        }
        if (cx.a((Object) yandexMetricaConfig.sessionTimeout)) {
            a2.a(yandexMetricaConfig.sessionTimeout.intValue());
        }
        if (cx.a((Object) yandexMetricaConfig.crashReporting)) {
            a2.a(yandexMetricaConfig.crashReporting.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.nativeCrashReporting)) {
            a2.b(yandexMetricaConfig.nativeCrashReporting.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.location)) {
            a2.a(yandexMetricaConfig.location);
        }
        if (cx.a((Object) yandexMetricaConfig.locationTracking)) {
            a2.d(yandexMetricaConfig.locationTracking.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.installedAppCollecting)) {
            a2.e(yandexMetricaConfig.installedAppCollecting.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.logs) && yandexMetricaConfig.logs.booleanValue()) {
            a2.a();
        }
        if (cx.a((Object) yandexMetricaConfig.preloadInfo)) {
            a2.a(yandexMetricaConfig.preloadInfo);
        }
        if (cx.a((Object) yandexMetricaConfig.firstActivationAsUpdate)) {
            a2.g(yandexMetricaConfig.firstActivationAsUpdate.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.statisticsSending)) {
            a2.f(yandexMetricaConfig.statisticsSending.booleanValue());
        }
        if (cx.a((Object) yandexMetricaConfig.maxReportsInDatabaseCount)) {
            a2.e(yandexMetricaConfig.maxReportsInDatabaseCount.intValue());
        }
        a(yandexMetricaConfig, a2);
        return a2;
    }

    private static void a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull a aVar) {
        if (yandexMetricaConfig instanceof j) {
            j jVar = (j) yandexMetricaConfig;
            if (cx.a((Object) jVar.d)) {
                aVar.a(jVar.d);
            }
            if (cx.a((Object) jVar.n)) {
                aVar.a(jVar.n);
            }
        }
    }

    private j(@NonNull a aVar) {
        List<String> list;
        Map<String, String> map;
        Map<String, String> map2;
        super(aVar.b);
        this.e = aVar.e;
        List c2 = aVar.d;
        Map<String, String> map3 = null;
        if (c2 == null) {
            list = null;
        } else {
            list = Collections.unmodifiableList(c2);
        }
        this.d = list;
        this.f5324a = aVar.c;
        Map e2 = aVar.f;
        if (e2 == null) {
            map = null;
        } else {
            map = Collections.unmodifiableMap(e2);
        }
        this.b = map;
        this.g = aVar.h;
        this.f = aVar.g;
        this.c = aVar.f5325a;
        if (aVar.i == null) {
            map2 = null;
        } else {
            map2 = Collections.unmodifiableMap(aVar.i);
        }
        this.h = map2;
        if (aVar.j != null) {
            map3 = Collections.unmodifiableMap(aVar.j);
        }
        this.i = map3;
        this.j = aVar.k;
        this.k = aVar.l;
        this.m = aVar.m;
        this.l = aVar.n;
        this.n = aVar.o;
    }
}
