package com.yandex.metrica.profile;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rf;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class UserProfile {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<UserProfileUpdate<? extends rf>> f5333a;

    public static class Builder {

        /* renamed from: a reason: collision with root package name */
        private final LinkedList<UserProfileUpdate<? extends rf>> f5334a = new LinkedList<>();

        Builder() {
        }

        public Builder apply(@NonNull UserProfileUpdate<? extends rf> userProfileUpdate) {
            this.f5334a.add(userProfileUpdate);
            return this;
        }

        @NonNull
        public UserProfile build() {
            return new UserProfile(this.f5334a);
        }
    }

    private UserProfile(@NonNull List<UserProfileUpdate<? extends rf>> list) {
        this.f5333a = Collections.unmodifiableList(list);
    }

    @NonNull
    public List<UserProfileUpdate<? extends rf>> getUserProfileUpdates() {
        return this.f5333a;
    }

    @NonNull
    public static Builder newBuilder() {
        return new Builder();
    }
}
