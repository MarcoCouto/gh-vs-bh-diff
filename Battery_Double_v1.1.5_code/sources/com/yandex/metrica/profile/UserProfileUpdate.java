package com.yandex.metrica.profile;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rf;

public class UserProfileUpdate<T extends rf> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    final T f5335a;

    UserProfileUpdate(@NonNull T t) {
        this.f5335a = t;
    }

    @NonNull
    public T getUserProfileUpdatePatcher() {
        return this.f5335a;
    }
}
