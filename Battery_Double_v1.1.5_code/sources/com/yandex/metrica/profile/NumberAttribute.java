package com.yandex.metrica.profile;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.qn;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qu;
import com.yandex.metrica.impl.ob.qv;
import com.yandex.metrica.impl.ob.qx;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.xs;
import com.yandex.metrica.impl.ob.yk;

public final class NumberAttribute {

    /* renamed from: a reason: collision with root package name */
    private final qt f5331a;

    NumberAttribute(@NonNull String str, @NonNull yk<String> ykVar, @NonNull qn qnVar) {
        this.f5331a = new qt(str, ykVar, qnVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValue(double d) {
        qx qxVar = new qx(this.f5331a.a(), d, new qu(), new qq(new qv(new xs(100))));
        return new UserProfileUpdate<>(qxVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueIfUndefined(double d) {
        qx qxVar = new qx(this.f5331a.a(), d, new qu(), new ra(new qv(new xs(100))));
        return new UserProfileUpdate<>(qxVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(1, this.f5331a.a(), new qu(), new qv(new xs(100))));
    }
}
