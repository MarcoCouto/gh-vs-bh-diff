package com.yandex.metrica.profile;

import com.yandex.metrica.impl.ob.rb;
import com.yandex.metrica.impl.ob.yd;

public class NotificationsEnabledAttribute extends BooleanAttribute {
    NotificationsEnabledAttribute() {
        super("appmetrica_notifications_enabled", new yd(), new rb());
    }
}
