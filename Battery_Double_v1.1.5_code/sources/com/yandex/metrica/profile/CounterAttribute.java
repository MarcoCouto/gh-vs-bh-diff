package com.yandex.metrica.profile;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.qn;
import com.yandex.metrica.impl.ob.qs;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.yk;

public final class CounterAttribute {

    /* renamed from: a reason: collision with root package name */
    private final qt f5329a;

    CounterAttribute(@NonNull String str, @NonNull yk<String> ykVar, @NonNull qn qnVar) {
        this.f5329a = new qt(str, ykVar, qnVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withDelta(double d) {
        return new UserProfileUpdate<>(new qs(this.f5329a.a(), d));
    }
}
