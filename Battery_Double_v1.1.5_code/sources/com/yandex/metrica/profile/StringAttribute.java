package com.yandex.metrica.profile;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.qn;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rc;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.yb;
import com.yandex.metrica.impl.ob.yk;

public class StringAttribute {

    /* renamed from: a reason: collision with root package name */
    private final yb<String> f5332a;
    private final qt b;

    StringAttribute(@NonNull String str, @NonNull yb<String> ybVar, @NonNull yk<String> ykVar, @NonNull qn qnVar) {
        this.b = new qt(str, ykVar, qnVar);
        this.f5332a = ybVar;
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValue(@NonNull String str) {
        rc rcVar = new rc(this.b.a(), str, this.f5332a, this.b.c(), new qq(this.b.b()));
        return new UserProfileUpdate<>(rcVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueIfUndefined(@NonNull String str) {
        rc rcVar = new rc(this.b.a(), str, this.f5332a, this.b.c(), new ra(this.b.b()));
        return new UserProfileUpdate<>(rcVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(0, this.b.a(), this.b.c(), this.b.b()));
    }
}
