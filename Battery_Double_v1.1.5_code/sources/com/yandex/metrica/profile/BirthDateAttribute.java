package com.yandex.metrica.profile;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.qo;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rb;
import com.yandex.metrica.impl.ob.rc;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.xt;
import com.yandex.metrica.impl.ob.yd;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class BirthDateAttribute {

    /* renamed from: a reason: collision with root package name */
    private final qt f5327a = new qt("appmetrica_birth_date", new yd(), new rb());

    BirthDateAttribute() {
    }

    public UserProfileUpdate<? extends rf> withBirthDate(int i) {
        return a(a(i), "yyyy", (qo) new qq(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDateIfUndefined(int i) {
        return a(a(i), "yyyy", (qo) new ra(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDate(int i, int i2) {
        return a(a(i, i2), "yyyy-MM", (qo) new qq(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDateIfUndefined(int i, int i2) {
        return a(a(i, i2), "yyyy-MM", (qo) new ra(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDate(int i, int i2, int i3) {
        return a(a(i, i2, i3), "yyyy-MM-dd", (qo) new qq(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDateIfUndefined(int i, int i2, int i3) {
        return a(a(i, i2, i3), "yyyy-MM-dd", (qo) new ra(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withAge(int i) {
        return a(a(Calendar.getInstance(Locale.US).get(1) - i), "yyyy", (qo) new qq(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withAgeIfUndefined(int i) {
        return a(a(Calendar.getInstance(Locale.US).get(1) - i), "yyyy", (qo) new ra(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDate(@NonNull Calendar calendar) {
        return a(calendar, "yyyy-MM-dd", (qo) new qq(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDateIfUndefined(@NonNull Calendar calendar) {
        return a(calendar, "yyyy-MM-dd", (qo) new ra(this.f5327a.b()));
    }

    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(0, this.f5327a.a(), new yd(), new rb()));
    }

    private Calendar a(int i) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        return gregorianCalendar;
    }

    private Calendar a(int i, int i2) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        gregorianCalendar.set(2, i2 - 1);
        gregorianCalendar.set(5, 1);
        return gregorianCalendar;
    }

    private Calendar a(int i, int i2, int i3) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        gregorianCalendar.set(2, i2 - 1);
        gregorianCalendar.set(5, i3);
        return gregorianCalendar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @SuppressLint({"SimpleDateFormat"})
    public UserProfileUpdate<? extends rf> a(@NonNull Calendar calendar, @NonNull String str, @NonNull qo qoVar) {
        rc rcVar = new rc(this.f5327a.a(), new SimpleDateFormat(str).format(calendar.getTime()), new xt(), new yd(), qoVar);
        return new UserProfileUpdate<>(rcVar);
    }
}
