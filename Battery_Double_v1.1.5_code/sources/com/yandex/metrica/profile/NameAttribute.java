package com.yandex.metrica.profile;

import com.yandex.metrica.impl.ob.rb;
import com.yandex.metrica.impl.ob.xz;
import com.yandex.metrica.impl.ob.yd;

public class NameAttribute extends StringAttribute {
    NameAttribute() {
        super("appmetrica_name", new xz(100, "Name attribute"), new yd(), new rb());
    }
}
