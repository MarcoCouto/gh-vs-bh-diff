package com.yandex.metrica.profile;

import android.support.annotation.NonNull;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.yandex.metrica.impl.ob.qu;
import com.yandex.metrica.impl.ob.qv;
import com.yandex.metrica.impl.ob.xs;
import com.yandex.metrica.impl.ob.xz;

public class Attribute {
    @NonNull
    public static StringAttribute customString(@NonNull String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("String attribute \"");
        sb.append(str);
        sb.append("\"");
        return new StringAttribute(str, new xz(Callback.DEFAULT_DRAG_ANIMATION_DURATION, sb.toString()), new qu(), new qv(new xs(100)));
    }

    @NonNull
    public static NumberAttribute customNumber(@NonNull String str) {
        return new NumberAttribute(str, new qu(), new qv(new xs(100)));
    }

    @NonNull
    public static BooleanAttribute customBoolean(@NonNull String str) {
        return new BooleanAttribute(str, new qu(), new qv(new xs(100)));
    }

    @NonNull
    public static CounterAttribute customCounter(@NonNull String str) {
        return new CounterAttribute(str, new qu(), new qv(new xs(100)));
    }

    @NonNull
    public static GenderAttribute gender() {
        return new GenderAttribute();
    }

    @NonNull
    public static BirthDateAttribute birthDate() {
        return new BirthDateAttribute();
    }

    @NonNull
    public static NotificationsEnabledAttribute notificationsEnabled() {
        return new NotificationsEnabledAttribute();
    }

    @NonNull
    public static NameAttribute name() {
        return new NameAttribute();
    }
}
