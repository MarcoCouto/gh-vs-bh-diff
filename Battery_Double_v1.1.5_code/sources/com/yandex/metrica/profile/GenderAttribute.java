package com.yandex.metrica.profile;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rb;
import com.yandex.metrica.impl.ob.rc;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.xt;
import com.yandex.metrica.impl.ob.yd;

public class GenderAttribute {

    /* renamed from: a reason: collision with root package name */
    private final qt f5330a = new qt("appmetrica_gender", new yd(), new rb());

    public enum Gender {
        MALE("M"),
        FEMALE("F"),
        OTHER("O");
        
        private final String mStringValue;

        private Gender(String str) {
            this.mStringValue = str;
        }

        public String getStringValue() {
            return this.mStringValue;
        }
    }

    GenderAttribute() {
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValue(@NonNull Gender gender) {
        rc rcVar = new rc(this.f5330a.a(), gender.getStringValue(), new xt(), this.f5330a.c(), new qq(this.f5330a.b()));
        return new UserProfileUpdate<>(rcVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueIfUndefined(@NonNull Gender gender) {
        rc rcVar = new rc(this.f5330a.a(), gender.getStringValue(), new xt(), this.f5330a.c(), new ra(this.f5330a.b()));
        return new UserProfileUpdate<>(rcVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(0, this.f5330a.a(), this.f5330a.c(), this.f5330a.b()));
    }
}
