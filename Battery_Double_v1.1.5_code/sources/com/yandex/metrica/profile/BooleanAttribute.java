package com.yandex.metrica.profile;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.qn;
import com.yandex.metrica.impl.ob.qp;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.yk;

public class BooleanAttribute {

    /* renamed from: a reason: collision with root package name */
    private final qt f5328a;

    BooleanAttribute(@NonNull String str, @NonNull yk<String> ykVar, @NonNull qn qnVar) {
        this.f5328a = new qt(str, ykVar, qnVar);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValue(boolean z) {
        return new UserProfileUpdate<>(new qp(this.f5328a.a(), z, this.f5328a.c(), new qq(this.f5328a.b())));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueIfUndefined(boolean z) {
        return new UserProfileUpdate<>(new qp(this.f5328a.a(), z, this.f5328a.c(), new ra(this.f5328a.b())));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(3, this.f5328a.a(), this.f5328a.c(), this.f5328a.b()));
    }
}
