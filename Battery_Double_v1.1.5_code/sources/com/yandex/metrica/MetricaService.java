package com.yandex.metrica;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.bc;
import com.yandex.metrica.impl.ob.bd;
import com.yandex.metrica.impl.ob.be;
import com.yandex.metrica.impl.ob.bt;
import com.yandex.metrica.impl.ob.cl;
import com.yandex.metrica.impl.ob.dr;
import com.yandex.metrica.impl.ob.dt;
import com.yandex.metrica.impl.ob.dz;
import com.yandex.metrica.impl.ob.vz;

public class MetricaService extends Service {

    /* renamed from: a reason: collision with root package name */
    private c f4597a = new c() {
        public void a(int i) {
            MetricaService.this.stopSelfResult(i);
        }
    };
    /* access modifiers changed from: private */
    public bc b;
    private final com.yandex.metrica.IMetricaService.a c = new com.yandex.metrica.IMetricaService.a() {
        @Deprecated
        public void a(String str, int i, String str2, Bundle bundle) throws RemoteException {
            MetricaService.this.b.a(str, i, str2, bundle);
        }

        public void a(Bundle bundle) throws RemoteException {
            MetricaService.this.b.a(bundle);
        }
    };

    static class a extends Binder {
        a() {
        }
    }

    static class b extends Binder {
        b() {
        }
    }

    public interface c {
        void a(int i);
    }

    public void onCreate() {
        super.onCreate();
        al.a(getApplicationContext());
        a(getResources().getConfiguration());
        vz.a(getApplicationContext());
        this.b = new bd(new be(getApplicationContext(), this.f4597a));
        this.b.a();
        al.a().a(new cl(this.b));
    }

    public void onStart(Intent intent, int i) {
        this.b.a(intent, i);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        this.b.a(intent, i, i2);
        return 2;
    }

    public IBinder onBind(Intent intent) {
        IBinder iBinder;
        String action = intent.getAction();
        if ("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER".equals(action)) {
            iBinder = new b();
        } else if ("com.yandex.metrica.ACTION_C_BG_L".equals(action)) {
            iBinder = new a();
        } else {
            iBinder = this.c;
        }
        this.b.a(intent);
        return iBinder;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        this.b.b(intent);
    }

    public void onDestroy() {
        this.b.b();
        super.onDestroy();
    }

    public boolean onUnbind(Intent intent) {
        this.b.c(intent);
        String action = intent.getAction();
        if ("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER".equals(action)) {
            return false;
        }
        if (!"com.yandex.metrica.ACTION_C_BG_L".equals(action) && a(intent)) {
            return false;
        }
        return true;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a(configuration);
    }

    private void a(Configuration configuration) {
        dr.a().b((dt) new dz(bt.a(configuration.locale)));
    }

    private boolean a(Intent intent) {
        return intent == null || intent.getData() == null;
    }
}
