package com.yandex.metrica;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.app.job.JobWorkItem;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.de;
import com.yandex.metrica.impl.ob.dg;
import com.yandex.metrica.impl.ob.jl;
import com.yandex.metrica.impl.ob.jr;
import com.yandex.metrica.impl.ob.jv;
import com.yandex.metrica.impl.ob.jw;
import com.yandex.metrica.impl.ob.jx;
import com.yandex.metrica.impl.ob.jy;
import com.yandex.metrica.impl.ob.jz;
import com.yandex.metrica.impl.ob.ka;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@TargetApi(26)
public class ConfigurationJobService extends JobService {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    SparseArray<jy> f4588a = new SparseArray<>();
    @NonNull
    Map<String, jy> b = new HashMap();
    private jr c;
    @Nullable
    private String d;

    public boolean complexJob(int i) {
        return i == 1512302347;
    }

    public void onCreate() {
        super.onCreate();
        al.a(getApplicationContext());
        Context applicationContext = getApplicationContext();
        this.d = String.format(Locale.US, "[ConfigurationJobService:%s]", new Object[]{applicationContext.getPackageName()});
        this.c = new jr();
        jv jvVar = new jv(getApplicationContext(), this.c.a(), new jl(applicationContext));
        de deVar = new de(applicationContext, new dg(applicationContext));
        this.f4588a.append(1512302345, new jz(getApplicationContext(), jvVar));
        this.f4588a.append(1512302346, new ka(getApplicationContext(), jvVar, deVar));
        this.b.put("com.yandex.metrica.configuration.service.PLC", new jx(applicationContext, this.c.a()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        jobFinished(r3, false);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0018 */
    public boolean onStartJob(@Nullable JobParameters jobParameters) {
        if (jobParameters == null) {
            return false;
        }
        if (!complexJob(jobParameters.getJobId())) {
            return b(jobParameters);
        }
        a(jobParameters);
        return true;
    }

    /* access modifiers changed from: private */
    public void a(@NonNull final JobParameters jobParameters) {
        this.c.a().a((Runnable) new Runnable() {
            public void run() {
                ConfigurationJobService.this.c(jobParameters);
            }
        });
    }

    private boolean b(@NonNull final JobParameters jobParameters) {
        jy jyVar = (jy) this.f4588a.get(jobParameters.getJobId());
        if (jyVar == null) {
            return false;
        }
        this.c.a(jyVar, jobParameters.getTransientExtras(), new jw() {
            public void a() {
                try {
                    ConfigurationJobService.this.jobFinished(jobParameters, false);
                } catch (Throwable unused) {
                }
            }
        });
        return true;
    }

    /* access modifiers changed from: private */
    public void c(@NonNull final JobParameters jobParameters) {
        while (true) {
            try {
                final JobWorkItem dequeueWork = jobParameters.dequeueWork();
                if (dequeueWork != null) {
                    Intent intent = dequeueWork.getIntent();
                    if (intent != null) {
                        jy jyVar = (jy) this.b.get(intent.getAction());
                        if (jyVar != null) {
                            this.c.a(jyVar, intent.getExtras(), new jw() {
                                public void a() {
                                    try {
                                        jobParameters.completeWork(dequeueWork);
                                        ConfigurationJobService.this.a(jobParameters);
                                    } catch (Throwable unused) {
                                    }
                                }
                            });
                        } else {
                            jobParameters.completeWork(dequeueWork);
                        }
                    } else {
                        jobParameters.completeWork(dequeueWork);
                    }
                } else {
                    return;
                }
            } catch (Throwable unused) {
                jobFinished(jobParameters, true);
                return;
            }
        }
    }

    public boolean onStopJob(@Nullable JobParameters jobParameters) {
        return jobParameters != null && complexJob(jobParameters.getJobId());
    }
}
