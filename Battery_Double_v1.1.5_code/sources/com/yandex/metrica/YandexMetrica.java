package com.yandex.metrica;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.sb;
import com.yandex.metrica.profile.UserProfile;
import java.util.Map;

public final class YandexMetrica {

    /* renamed from: a reason: collision with root package name */
    private static final sb f4605a = new sb(db.a());

    public static int getLibraryApiLevel() {
        return 85;
    }

    @NonNull
    public static String getLibraryVersion() {
        return "3.8.0";
    }

    private YandexMetrica() {
    }

    public static void activate(@NonNull Context context, @NonNull YandexMetricaConfig yandexMetricaConfig) {
        f4605a.a(context, yandexMetricaConfig);
    }

    public static void sendEventsBuffer() {
        f4605a.e();
    }

    public static void resumeSession(@Nullable Activity activity) {
        f4605a.a(activity);
    }

    public static void pauseSession(@Nullable Activity activity) {
        f4605a.b(activity);
    }

    public static void enableActivityAutoTracking(@NonNull Application application) {
        f4605a.a(application);
    }

    public static void reportEvent(@NonNull String str) {
        f4605a.a(str);
    }

    public static void reportError(@NonNull String str, @Nullable Throwable th) {
        f4605a.a(str, th);
    }

    public static void reportUnhandledException(@NonNull Throwable th) {
        f4605a.a(th);
    }

    public static void reportNativeCrash(@NonNull String str) {
        f4605a.b(str);
    }

    public static void reportEvent(@NonNull String str, @Nullable String str2) {
        f4605a.a(str, str2);
    }

    public static void reportEvent(@NonNull String str, @Nullable Map<String, Object> map) {
        f4605a.a(str, map);
    }

    public static void reportAppOpen(@NonNull Activity activity) {
        f4605a.c(activity);
    }

    public static void reportAppOpen(@NonNull String str) {
        f4605a.c(str);
    }

    @Deprecated
    public static void reportReferralUrl(@NonNull String str) {
        f4605a.d(str);
    }

    public static void setLocation(@Nullable Location location) {
        f4605a.a(location);
    }

    public static void setLocationTracking(boolean z) {
        f4605a.a(z);
    }

    public static void setLocationTracking(@NonNull Context context, boolean z) {
        f4605a.a(context, z);
    }

    public static void setStatisticsSending(@NonNull Context context, boolean z) {
        f4605a.b(context, z);
    }

    public static void activateReporter(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        f4605a.a(context, reporterConfig);
    }

    @NonNull
    public static IReporter getReporter(@NonNull Context context, @NonNull String str) {
        return f4605a.a(context, str);
    }

    public static void registerReferrerBroadcastReceivers(@NonNull BroadcastReceiver... broadcastReceiverArr) {
        MetricaEventHandler.a(broadcastReceiverArr);
    }

    public static void requestDeferredDeeplinkParameters(@NonNull DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        f4605a.a(deferredDeeplinkParametersListener);
    }

    public static void requestAppMetricaDeviceID(@NonNull AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        f4605a.a(appMetricaDeviceIDListener);
    }

    public static void setUserProfileID(@Nullable String str) {
        f4605a.e(str);
    }

    public static void reportUserProfile(@NonNull UserProfile userProfile) {
        f4605a.a(userProfile);
    }

    public static void reportRevenue(@NonNull Revenue revenue) {
        f4605a.a(revenue);
    }
}
