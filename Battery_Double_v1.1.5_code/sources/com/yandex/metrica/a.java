package com.yandex.metrica;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.xh;
import java.util.HashSet;
import java.util.Set;

public class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final xh f4607a;
    private final long b;
    private final Set<C0103a> c;
    private final Runnable d;
    private boolean e;

    /* renamed from: com.yandex.metrica.a$a reason: collision with other inner class name */
    public interface C0103a {
        void a();

        void b();
    }

    public a(long j) {
        this(j, db.k().b());
    }

    a(long j, @NonNull xh xhVar) {
        this.c = new HashSet();
        this.d = new Runnable() {
            public void run() {
                a.this.d();
            }
        };
        this.e = true;
        this.f4607a = xhVar;
        this.b = j;
    }

    public void a() {
        if (this.e) {
            this.e = false;
            this.f4607a.b(this.d);
            c();
        }
    }

    public void b() {
        if (!this.e) {
            this.e = true;
            this.f4607a.a(this.d, this.b);
        }
    }

    private void c() {
        for (C0103a a2 : new HashSet(this.c)) {
            a2.a();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        for (C0103a b2 : new HashSet(this.c)) {
            b2.b();
        }
    }
}
