package com.yandex.metrica;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.de;
import com.yandex.metrica.impl.ob.df;
import com.yandex.metrica.impl.ob.jn;
import com.yandex.metrica.impl.ob.jr;
import com.yandex.metrica.impl.ob.jv;
import com.yandex.metrica.impl.ob.jy;
import com.yandex.metrica.impl.ob.jz;
import com.yandex.metrica.impl.ob.ka;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationService extends Service {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Map<String, jy> f4592a = new HashMap();
    private jr b;
    @Nullable
    private String c;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        al.a(getApplicationContext());
        this.c = String.format("[ConfigurationService:%s]", new Object[]{getPackageName()});
        this.b = new jr();
        Context applicationContext = getApplicationContext();
        jv jvVar = new jv(applicationContext, this.b.a(), new jn(applicationContext));
        this.f4592a.put("com.yandex.metrica.configuration.ACTION_INIT", new ka(getApplicationContext(), jvVar, cx.a(21) ? new de(applicationContext, new df(applicationContext)) : null));
        this.f4592a.put("com.yandex.metrica.configuration.ACTION_SCHEDULED_START", new jz(getApplicationContext(), jvVar));
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        Bundle bundle = null;
        jy jyVar = (jy) this.f4592a.get(intent == null ? null : intent.getAction());
        if (jyVar != null) {
            jr jrVar = this.b;
            if (intent != null) {
                bundle = intent.getExtras();
            }
            jrVar.a(jyVar, bundle);
        }
        return 2;
    }
}
