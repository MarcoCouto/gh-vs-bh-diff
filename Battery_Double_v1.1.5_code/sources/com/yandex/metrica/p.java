package com.yandex.metrica;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.interact.DeviceInfo;
import com.yandex.metrica.impl.ob.da;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public final class p {

    /* renamed from: a reason: collision with root package name */
    private static final List<String> f5326a = Arrays.asList(new String[]{"yandex_mobile_metrica_uuid", "yandex_mobile_metrica_device_id", "appmetrica_device_id_hash", "yandex_mobile_metrica_get_ad_url", "yandex_mobile_metrica_report_ad_url"});

    @Deprecated
    public static void a(IIdentifierCallback iIdentifierCallback) {
        a(iIdentifierCallback, f5326a);
    }

    @Deprecated
    public static void a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        da.a(iIdentifierCallback, (List<String>) new ArrayList<String>(list));
    }

    public static void a(Context context, IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        da.a(context, iIdentifierCallback, new ArrayList(list));
    }

    public static void a(Context context, IIdentifierCallback iIdentifierCallback, @NonNull String... strArr) {
        a(context, iIdentifierCallback, Arrays.asList(strArr));
    }

    public static void a(Context context, IIdentifierCallback iIdentifierCallback) {
        a(context, iIdentifierCallback, f5326a);
    }

    @NonNull
    public static String u(@NonNull String str) {
        return da.a(str);
    }

    @Nullable
    public static Boolean plat() {
        try {
            return (Boolean) da.d().get();
        } catch (Throwable unused) {
            return null;
        }
    }

    public static boolean iifa() {
        return da.a();
    }

    @Nullable
    public static String pgai() {
        try {
            return (String) da.b().get();
        } catch (Throwable unused) {
            return null;
        }
    }

    @NonNull
    public static DeviceInfo gdi(@NonNull Context context) {
        return da.a(context);
    }

    @NonNull
    public static String gcni(@NonNull Context context) {
        return da.b(context);
    }

    @Nullable
    @Deprecated
    public static String guid() {
        return da.c();
    }

    @Nullable
    public static String guid(@NonNull Context context) {
        return da.d(context);
    }

    @Nullable
    public static String gdid(@NonNull Context context) {
        return da.e(context);
    }

    @NonNull
    public static String mpn(Context context) {
        return da.f(context);
    }

    public static void rce(int i, String str, String str2, Map<String, String> map) {
        da.a(i, str, str2, map);
    }

    @NonNull
    public static String gmsvn(int i) {
        return da.a(i);
    }

    public static void seb() {
        da.e();
    }

    @Deprecated
    @NonNull
    public static YandexMetricaConfig cpcwh(YandexMetricaConfig yandexMetricaConfig, String str) {
        return da.a(yandexMetricaConfig, str);
    }

    @Deprecated
    public static void rolu(Context context, Object obj) {
        da.a(context, obj);
    }

    @Deprecated
    public static void urolu(Context context, Object obj) {
        da.b(context, obj);
    }

    @Nullable
    @Deprecated
    public static Location glkl(Context context) {
        return da.g(context);
    }

    @Nullable
    @Deprecated
    public static Integer gbc(Context context) {
        return da.c(context.getApplicationContext());
    }
}
