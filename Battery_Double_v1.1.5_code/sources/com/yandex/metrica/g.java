package com.yandex.metrica;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.sa;
import com.yandex.metrica.profile.UserProfile;
import java.util.Map;

public class g {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final sa f4612a;

    public void a() {
    }

    public void a(@Nullable Activity activity) {
    }

    public void a(@NonNull Application application) {
    }

    public void a(@NonNull Context context, @NonNull f fVar) {
    }

    public void a(@NonNull Context context, boolean z) {
    }

    public void a(@Nullable Location location) {
    }

    public void a(@NonNull AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
    }

    public void a(@NonNull DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
    }

    public void a(@NonNull Revenue revenue) {
    }

    public void a(@NonNull UserProfile userProfile) {
    }

    public void a(@NonNull String str) {
    }

    public void a(@NonNull String str, @Nullable String str2) {
    }

    public void a(@NonNull String str, @Nullable Throwable th) {
    }

    public void a(@NonNull String str, @Nullable Map<String, Object> map) {
    }

    public void a(@NonNull Throwable th) {
    }

    public void a(boolean z) {
    }

    public void b(@NonNull Activity activity) {
    }

    public void b(@NonNull Context context, boolean z) {
    }

    public void b(@Nullable String str) {
    }

    public void c(@NonNull Activity activity) {
    }

    public void c(@NonNull String str) {
    }

    public void d(@NonNull String str) {
    }

    public g(@NonNull sa saVar) {
        this.f4612a = saVar;
    }

    public void a(@NonNull Context context, @NonNull YandexMetricaConfig yandexMetricaConfig) {
        this.f4612a.a(context, j.a(yandexMetricaConfig));
    }
}
