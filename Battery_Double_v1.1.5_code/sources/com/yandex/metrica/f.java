package com.yandex.metrica;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.ReporterConfig.Builder;
import com.yandex.metrica.impl.ob.cx;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class f extends ReporterConfig {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final Integer f4610a;
    @Nullable
    public final Integer b;
    public final Map<String, String> c;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        Builder f4611a;
        Integer b;
        Integer c;
        LinkedHashMap<String, String> d = new LinkedHashMap<>();

        public a(String str) {
            this.f4611a = ReporterConfig.newConfigBuilder(str);
        }

        @NonNull
        public a a(int i) {
            this.f4611a.withSessionTimeout(i);
            return this;
        }

        @NonNull
        public a a() {
            this.f4611a.withLogs();
            return this;
        }

        @NonNull
        public a b(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @NonNull
        public a c(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @NonNull
        public a a(String str, String str2) {
            this.d.put(str, str2);
            return this;
        }

        @NonNull
        public a a(boolean z) {
            this.f4611a.withStatisticsSending(z);
            return this;
        }

        @NonNull
        public a d(int i) {
            this.f4611a.withMaxReportsInDatabaseCount(i);
            return this;
        }

        @NonNull
        public f b() {
            return new f(this);
        }
    }

    f(@NonNull a aVar) {
        super(aVar.f4611a);
        this.b = aVar.b;
        this.f4610a = aVar.c;
        this.c = aVar.d == null ? null : Collections.unmodifiableMap(aVar.d);
    }

    private f(ReporterConfig reporterConfig) {
        super(reporterConfig);
        if (reporterConfig instanceof f) {
            f fVar = (f) reporterConfig;
            this.f4610a = fVar.f4610a;
            this.b = fVar.b;
            this.c = fVar.c;
            return;
        }
        this.f4610a = null;
        this.b = null;
        this.c = null;
    }

    public static f a(@NonNull ReporterConfig reporterConfig) {
        return new f(reporterConfig);
    }

    public static a a(@NonNull f fVar) {
        a a2 = a(fVar.apiKey);
        if (cx.a((Object) fVar.sessionTimeout)) {
            a2.a(fVar.sessionTimeout.intValue());
        }
        if (cx.a((Object) fVar.logs) && fVar.logs.booleanValue()) {
            a2.a();
        }
        if (cx.a((Object) fVar.statisticsSending)) {
            a2.a(fVar.statisticsSending.booleanValue());
        }
        if (cx.a((Object) fVar.maxReportsInDatabaseCount)) {
            a2.d(fVar.maxReportsInDatabaseCount.intValue());
        }
        if (cx.a((Object) fVar.f4610a)) {
            a2.c(fVar.f4610a.intValue());
        }
        if (cx.a((Object) fVar.b)) {
            a2.b(fVar.b.intValue());
        }
        if (cx.a((Object) fVar.c)) {
            for (Entry entry : fVar.c.entrySet()) {
                a2.a((String) entry.getKey(), (String) entry.getValue());
            }
        }
        return a2;
    }

    public static a a(@NonNull String str) {
        return new a(str);
    }
}
