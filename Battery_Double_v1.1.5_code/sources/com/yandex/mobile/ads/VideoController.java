package com.yandex.mobile.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.dx;

public final class VideoController {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final dx f5341a;

    public VideoController(@NonNull dx dxVar) {
        this.f5341a = dxVar;
    }

    public final void setVideoEventListener(@Nullable VideoEventListener videoEventListener) {
        this.f5341a.a(videoEventListener);
    }
}
