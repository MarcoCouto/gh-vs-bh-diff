package com.yandex.mobile.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ac;
import com.yandex.mobile.ads.impl.di;
import com.yandex.mobile.ads.impl.hj;
import com.yandex.mobile.ads.impl.iu;
import com.yandex.mobile.ads.impl.iv;

public final class InterstitialAd extends hj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final iv f5340a;

    public InterstitialAd(@NonNull Context context) {
        super(context);
        iu iuVar = new iu(context);
        this.f5340a = new iv(context, iuVar);
        iuVar.a(this.f5340a.s());
    }

    public final void setBlockId(String str) {
        this.f5340a.a_(str);
    }

    public final String getBlockId() {
        return this.f5340a.r();
    }

    public final void setInterstitialEventListener(InterstitialEventListener interstitialEventListener) {
        this.f5340a.a(interstitialEventListener);
    }

    public final InterstitialEventListener getInterstitialEventListener() {
        return this.f5340a.E();
    }

    public final void loadAd(AdRequest adRequest) {
        this.f5340a.a(adRequest);
    }

    public final void shouldOpenLinksInApp(boolean z) {
        this.f5340a.a_(z);
    }

    public final void show() {
        if (this.f5340a.z()) {
            this.f5340a.a();
        }
    }

    public final boolean isLoaded() {
        return this.f5340a.z();
    }

    public final void destroy() {
        if (!di.a((ac) this.f5340a)) {
            this.f5340a.e();
        }
    }
}
