package com.yandex.mobile.ads;

import com.yandex.mobile.ads.impl.ff;
import com.yandex.mobile.ads.impl.hi;

public final class MobileAds {
    public static String getLibraryVersion() {
        return "2.110";
    }

    public static void enableLogging(boolean z) {
        hi.a(z);
    }

    public static void enableDebugErrorIndicator(boolean z) {
        ff.a().a(z);
    }

    public static void setUserConsent(boolean z) {
        ff.a().b(z);
    }
}
