package com.yandex.mobile.ads;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.RelativeLayout;
import com.yandex.mobile.ads.impl.l;
import com.yandex.mobile.ads.impl.n;
import com.yandex.mobile.ads.impl.q;

public final class AdActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    public static final String f5336a = AdActivity.class.getCanonicalName();
    @Nullable
    private RelativeLayout b;
    @Nullable
    private l c;

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        l lVar;
        super.onCreate(bundle);
        this.b = new RelativeLayout(this);
        RelativeLayout relativeLayout = this.b;
        Intent intent = getIntent();
        if (intent != null) {
            Window window = getWindow();
            ResultReceiver a2 = a(intent);
            lVar = n.a().a(this, relativeLayout, a2, new q(this, a2), intent, window);
        } else {
            lVar = null;
        }
        this.c = lVar;
        if (this.c != null) {
            this.c.a();
            this.c.c_();
            setContentView(this.b);
            return;
        }
        finish();
    }

    @Nullable
    private static ResultReceiver a(@NonNull Intent intent) {
        try {
            return (ResultReceiver) intent.getParcelableExtra("extra_receiver");
        } catch (Exception unused) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        if (this.c != null) {
            this.c.d();
            this.c.g();
        }
        if (this.b != null) {
            this.b.removeAllViews();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void onPause() {
        if (this.c != null) {
            this.c.f();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public final void onResume() {
        super.onResume();
        if (this.c != null) {
            this.c.e();
        }
    }

    public final void onBackPressed() {
        if (this.c == null || this.c.c()) {
            super.onBackPressed();
        }
    }
}
