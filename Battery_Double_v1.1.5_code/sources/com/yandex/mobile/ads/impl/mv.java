package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.support.annotation.NonNull;
import android.support.v4.internal.view.SupportMenu;
import android.view.View;

public final class mv extends View {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final dk f5633a;
    @NonNull
    private Paint b;
    @NonNull
    private Paint c;
    @NonNull
    private Paint d;
    private float e = 40.0f;
    private float f;
    private float g;
    private float h;

    public mv(@NonNull Context context, @NonNull dk dkVar) {
        super(context);
        this.f5633a = dkVar;
        this.f = (float) dk.a(context, 34.0f);
        this.g = (float) dk.a(context, 3.0f);
        this.h = (float) dk.a(context, 20.0f);
        this.b = new Paint();
        this.b.setStyle(Style.FILL);
        this.c = new Paint();
        this.c.setStyle(Style.STROKE);
        this.c.setStrokeWidth(this.g);
        this.c.setAntiAlias(true);
        this.d = new Paint();
        this.d.setStyle(Style.FILL);
        this.d.setTextSize(this.h);
        this.d.setTextAlign(Align.CENTER);
        a();
    }

    private void a() {
        this.c.setColor(dv.a((int) SupportMenu.CATEGORY_MASK, this.e));
        this.b.setColor(dv.a(-1, this.e));
        this.d.setColor(dv.a((int) SupportMenu.CATEGORY_MASK, this.e));
    }

    public final void setSelected(boolean z) {
        super.setSelected(z);
        this.e = z ? 0.0f : 40.0f;
        a();
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        setMeasuredDimension((int) this.f, (int) this.f);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float f2 = this.f / 2.0f;
        canvas.drawCircle(f2, f2, f2, this.b);
        canvas.drawCircle(f2, f2, f2 - (this.g / 2.0f), this.c);
        canvas.drawText("!", this.f / 2.0f, (this.f / 2.0f) - ((this.d.descent() + this.d.ascent()) / 2.0f), this.d);
    }
}
