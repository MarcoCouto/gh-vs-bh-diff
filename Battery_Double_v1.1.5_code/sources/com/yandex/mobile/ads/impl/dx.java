package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.VideoEventListener;

public final class dx implements dy {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Object f5449a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @Nullable
    public VideoEventListener c;

    public final void a(@Nullable VideoEventListener videoEventListener) {
        synchronized (f5449a) {
            this.c = videoEventListener;
        }
    }

    public final void a() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (dx.f5449a) {
                    if (dx.this.c != null) {
                        dx.this.c.onVideoComplete();
                    }
                }
            }
        });
    }
}
