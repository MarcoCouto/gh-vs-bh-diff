package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public class kj<V extends View, T> implements ki<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ld<V, T> f5590a;

    public kj(@NonNull ld<V, T> ldVar) {
        this.f5590a = ldVar;
    }

    public final void a() {
        View a2 = this.f5590a.a();
        if (a2 != null) {
            this.f5590a.a(a2);
        }
    }

    public final void a(@NonNull T t) {
        View a2 = this.f5590a.a();
        if (a2 != null) {
            this.f5590a.b(a2, t);
            a2.setVisibility(0);
        }
    }

    public final void a(@NonNull ni niVar, @NonNull li liVar) {
        this.f5590a.a(niVar, liVar);
    }

    public final boolean b(@NonNull T t) {
        View a2 = this.f5590a.a();
        return a2 != null && this.f5590a.a(a2, t);
    }

    public final boolean b() {
        return this.f5590a.b();
    }

    public final boolean c() {
        return this.f5590a.c();
    }

    public final boolean d() {
        return this.f5590a.d();
    }
}
