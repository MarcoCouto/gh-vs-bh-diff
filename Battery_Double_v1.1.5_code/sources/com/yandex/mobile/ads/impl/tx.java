package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class tx implements Parcelable {
    public static final Creator<tx> CREATOR = new Creator<tx>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new tx[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new tx(parcel);
        }
    };
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f5799a;
    @Nullable
    private final String b;

    public static class a {
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public String f5800a;
        /* access modifiers changed from: private */
        @Nullable
        public String b;

        @NonNull
        public final a a(@NonNull String str) {
            this.f5800a = str;
            return this;
        }

        @NonNull
        public final a b(@NonNull String str) {
            this.b = str;
            return this;
        }
    }

    public int describeContents() {
        return 0;
    }

    /* synthetic */ tx(a aVar, byte b2) {
        this(aVar);
    }

    private tx(@NonNull a aVar) {
        this.f5799a = aVar.f5800a;
        this.b = aVar.b;
    }

    @Nullable
    public final String a() {
        return this.f5799a;
    }

    @Nullable
    public final String b() {
        return this.b;
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.f5799a);
        parcel.writeString(this.b);
    }

    protected tx(@NonNull Parcel parcel) {
        this.f5799a = parcel.readString();
        this.b = parcel.readString();
    }
}
