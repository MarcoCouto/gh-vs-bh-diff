package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class qs {

    /* renamed from: a reason: collision with root package name */
    private AtomicInteger f5723a;
    private final Map<String, Queue<qr<?>>> b;
    private final Set<qr<?>> c;
    private final PriorityBlockingQueue<qr<?>> d;
    private final PriorityBlockingQueue<qr<?>> e;
    private final qk f;
    private final qo g;
    private final qu h;
    private qp[] i;
    private ql j;
    private List<Object> k;

    public interface a {
        boolean a(qr<?> qrVar);
    }

    private qs(qk qkVar, qo qoVar, qu quVar) {
        this.f5723a = new AtomicInteger();
        this.b = new HashMap();
        this.c = new HashSet();
        this.d = new PriorityBlockingQueue<>();
        this.e = new PriorityBlockingQueue<>();
        this.k = new ArrayList();
        this.f = qkVar;
        this.g = qoVar;
        this.i = new qp[1];
        this.h = quVar;
    }

    public qs(qk qkVar, qo qoVar) {
        this(qkVar, qoVar, new qn(new Handler(Looper.getMainLooper())));
    }

    public final void a(a aVar) {
        synchronized (this.c) {
            for (qr qrVar : this.c) {
                if (aVar.a(qrVar)) {
                    qrVar.i();
                }
            }
        }
    }

    public final <T> qr<T> a(qr<T> qrVar) {
        qrVar.a(this);
        synchronized (this.c) {
            this.c.add(qrVar);
        }
        qrVar.c(this.f5723a.incrementAndGet());
        if (!qrVar.m()) {
            this.e.add(qrVar);
            return qrVar;
        }
        synchronized (this.b) {
            String b2 = qrVar.b();
            if (this.b.containsKey(b2)) {
                Queue queue = (Queue) this.b.get(b2);
                if (queue == null) {
                    queue = new LinkedList();
                }
                queue.add(qrVar);
                this.b.put(b2, queue);
                if (qw.b) {
                    qw.a("Request for cacheKey=%s is in flight, putting on hold.", b2);
                }
            } else {
                this.b.put(b2, null);
                this.d.add(qrVar);
            }
        }
        return qrVar;
    }

    /* access modifiers changed from: 0000 */
    public final <T> void b(qr<T> qrVar) {
        synchronized (this.c) {
            this.c.remove(qrVar);
        }
        synchronized (this.k) {
            Iterator it = this.k.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        if (qrVar.m()) {
            synchronized (this.b) {
                String b2 = qrVar.b();
                Queue queue = (Queue) this.b.remove(b2);
                if (queue != null) {
                    if (qw.b) {
                        qw.a("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(queue.size()), b2);
                    }
                    this.d.addAll(queue);
                }
            }
        }
    }

    public final void a() {
        if (this.j != null) {
            this.j.a();
        }
        for (int i2 = 0; i2 < this.i.length; i2++) {
            if (this.i[i2] != null) {
                this.i[i2].a();
            }
        }
        this.j = new ql(this.d, this.e, this.f, this.h);
        this.j.start();
        for (int i3 = 0; i3 < this.i.length; i3++) {
            qp qpVar = new qp(this.e, this.g, this.f, this.h);
            this.i[i3] = qpVar;
            qpVar.start();
        }
    }
}
