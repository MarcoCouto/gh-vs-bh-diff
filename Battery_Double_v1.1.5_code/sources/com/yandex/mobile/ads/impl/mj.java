package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeGenericAd;
import com.yandex.mobile.ads.nativeads.az;
import com.yandex.mobile.ads.nativeads.c;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.v;

final class mj implements mk {
    mj() {
    }

    public final NativeGenericAd a(@NonNull Context context, @NonNull np npVar, @NonNull v vVar, @NonNull i iVar, @NonNull c cVar) {
        az azVar = new az(context, npVar, vVar, iVar, cVar);
        return azVar;
    }
}
