package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class nn {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f5652a;
    private final float b;

    public nn(@Nullable String str, float f) {
        this.f5652a = str;
        this.b = f;
    }

    @Nullable
    public final String a() {
        return this.f5652a;
    }

    public final float b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nn nnVar = (nn) obj;
        if (Float.compare(nnVar.b, this.b) != 0) {
            return false;
        }
        return this.f5652a.equals(nnVar.f5652a);
    }

    public final int hashCode() {
        return (this.f5652a.hashCode() * 31) + (this.b != 0.0f ? Float.floatToIntBits(this.b) : 0);
    }
}
