package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.bf.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class bu implements bv<be> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bm f5402a = new bm();

    @Nullable
    public final /* synthetic */ Object b(@NonNull qq qqVar) {
        return a(qqVar);
    }

    @Nullable
    public static be a(@NonNull qq qqVar) {
        String a2 = bm.a(qqVar);
        if (!TextUtils.isEmpty(a2)) {
            return a(a2);
        }
        return null;
    }

    @Nullable
    private static be a(@NonNull String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Map a2 = a(jSONObject);
            if (a2.isEmpty()) {
                return null;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("networks");
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                bf b = b(jSONArray.getJSONObject(i));
                if (b != null) {
                    arrayList.add(b);
                }
            }
            if (!arrayList.isEmpty()) {
                return new be(arrayList, a2);
            }
            return null;
        } catch (JSONException unused) {
            return null;
        }
    }

    @NonNull
    private static Map<String, String> a(@NonNull JSONObject jSONObject) throws JSONException {
        try {
            return dm.a(jSONObject, "passback_parameters");
        } catch (JSONException e) {
            throw new JSONException(e.getMessage());
        }
    }

    @Nullable
    private static bf b(@NonNull JSONObject jSONObject) {
        try {
            String c = dm.c(jSONObject, "adapter");
            Map a2 = dm.a(jSONObject, "network_data");
            if (a2.isEmpty()) {
                return null;
            }
            List b = dm.b(jSONObject, "click_tracking_urls");
            List b2 = dm.b(jSONObject, "impression_tracking_urls");
            return new a(c, a2).a(b2).b(b).c(dm.b(jSONObject, "ad_response_tracking_urls")).a();
        } catch (JSONException unused) {
            return null;
        }
    }
}
