package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ru.a;

public final class of extends bp<nq> {

    /* renamed from: a reason: collision with root package name */
    private final cq<nq> f5671a;

    public of(@NonNull Context context, @NonNull cq<nq> cqVar, @NonNull fc fcVar, @NonNull String str, @NonNull String str2, @NonNull a<x<nq>> aVar) {
        super(context, fcVar, str, str2, aVar, new pw());
        this.f5671a = cqVar;
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i) {
        return 200 == i || b(i);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public final /* synthetic */ Object a_(@NonNull qq qqVar) {
        return (nq) this.f5671a.a(qqVar);
    }
}
