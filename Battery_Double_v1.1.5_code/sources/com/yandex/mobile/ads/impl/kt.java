package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.i;

public final class kt implements ks {
    @NonNull
    public final ky a(@NonNull MediaView mediaView, @NonNull fc fcVar, @NonNull i iVar, @NonNull ap apVar) {
        mediaView.removeAllViews();
        return new kz(mediaView);
    }
}
