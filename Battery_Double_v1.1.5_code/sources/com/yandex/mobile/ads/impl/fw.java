package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class fw {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Object f5499a = new Object();
    @NonNull
    private final List<ga> b = new CopyOnWriteArrayList();

    public final void a(@NonNull Context context, @NonNull ga gaVar) {
        synchronized (this.f5499a) {
            this.b.add(gaVar);
            gb.a(context).a(gaVar);
        }
    }

    public final void a(@NonNull Context context) {
        synchronized (this.f5499a) {
            gb a2 = gb.a(context);
            for (ga b2 : this.b) {
                a2.b(b2);
            }
            this.b.clear();
        }
    }
}
