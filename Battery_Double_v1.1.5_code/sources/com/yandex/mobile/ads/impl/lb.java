package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.nativeads.Rating;

public final class lb<V extends View & Rating> extends ld<V, String> {
    public final /* bridge */ /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        return true;
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        String str = (String) obj;
        try {
            ((Rating) view).setRating(Float.valueOf(Math.max(Float.parseFloat(str), 0.0f)));
        } catch (NumberFormatException unused) {
            String.format("Could not parse rating value. Rating value is %s", new Object[]{str});
        }
    }

    public lb(@NonNull V v) {
        super(v);
    }

    public final void a(@NonNull V v) {
        ((Rating) v).setRating(Float.valueOf(0.0f));
        super.a(v);
    }
}
