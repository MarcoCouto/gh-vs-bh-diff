package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.core.identifiers.ad.service.f;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

final class fz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gc f5501a;
    @NonNull
    private final f b;
    @NonNull
    private final Executor c = Executors.newSingleThreadExecutor(new Cdo("YandexMobileAds.AdvertisingId"));

    fz(@NonNull Context context) {
        this.f5501a = new gc(context);
        this.b = new f(context);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final fx fxVar) {
        this.c.execute(new Runnable() {
            public final void run() {
                fu a2 = fz.a(fz.this);
                if (a2 != null) {
                    fxVar.a(a2);
                } else {
                    fxVar.a();
                }
            }
        });
    }

    static /* synthetic */ fu a(fz fzVar) {
        fu a2 = fzVar.f5501a.a();
        return a2 == null ? fzVar.b.a() : a2;
    }
}
