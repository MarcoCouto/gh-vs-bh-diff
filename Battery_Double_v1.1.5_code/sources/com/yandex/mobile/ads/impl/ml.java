package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import java.util.HashMap;
import java.util.Map;

public final class ml {

    /* renamed from: a reason: collision with root package name */
    private static final Map<NativeAdType, mk> f5624a = new HashMap<NativeAdType, mk>() {
        {
            put(NativeAdType.APP_INSTALL, new mh());
            put(NativeAdType.CONTENT, new mi());
            put(NativeAdType.IMAGE, new mj());
        }
    };

    @Nullable
    public static mk a(@NonNull NativeAdType nativeAdType) {
        return (mk) f5624a.get(nativeAdType);
    }
}
