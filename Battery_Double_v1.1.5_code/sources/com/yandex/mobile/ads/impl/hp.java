package com.yandex.mobile.ads.impl;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import com.yandex.mobile.ads.impl.hq.f;

public final class hp {

    static class a {

        /* renamed from: a reason: collision with root package name */
        static final Interpolator f5531a = new LinearInterpolator();
        static final Interpolator b = new AccelerateInterpolator();
    }

    static class b extends Animation {

        /* renamed from: a reason: collision with root package name */
        private Camera f5532a;
        private float b;
        private float c;
        private float d;
        private float e;
        private float f;
        private float g;

        public b(float f2, float f3, float f4, float f5, float f6, float f7) {
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.e = f5;
            this.f = f6;
            this.g = f7;
        }

        public final void initialize(int i, int i2, int i3, int i4) {
            super.initialize(i, i2, i3, i4);
            this.f5532a = new Camera();
        }

        /* access modifiers changed from: protected */
        public final void applyTransformation(float f2, Transformation transformation) {
            float f3 = this.b + ((this.c - this.b) * f2);
            double d2 = (double) this.f;
            double d3 = (double) ((this.g + f2) / 2.0f);
            Double.isNaN(d3);
            double sin = Math.sin(d3 * 3.141592653589793d);
            Double.isNaN(d2);
            float f4 = (float) (d2 * sin);
            Matrix matrix = transformation.getMatrix();
            this.f5532a.save();
            this.f5532a.translate(0.0f, 0.0f, f4);
            this.f5532a.rotateY(f3);
            this.f5532a.getMatrix(matrix);
            this.f5532a.restore();
            matrix.preTranslate(-this.d, -this.e);
            matrix.postTranslate(this.d, this.e);
        }
    }

    public static void a(f fVar, Rect rect) {
        fVar.setPivot(((float) rect.width()) / 2.0f, ((float) rect.height()) / 2.0f);
        fVar.setDepthZ(((float) rect.width()) / 2.0f);
        fVar.setAnimationDuration(400);
        fVar.setFromInterpolator(a.f5531a);
        fVar.setToInterpolator(a.b);
        fVar.a();
    }
}
