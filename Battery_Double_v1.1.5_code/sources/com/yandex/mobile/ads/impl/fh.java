package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Locale;

public final class fh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fl f5486a = new fl();
    @NonNull
    private final fj b = new fj();

    @Nullable
    public final String a(@NonNull Context context) {
        return this.f5486a.a(context).name().toLowerCase(Locale.US);
    }
}
