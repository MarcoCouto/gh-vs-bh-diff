package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ru.a;

public final class br extends ru<fg> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cq<fg> f5401a;

    public br(@NonNull String str, @NonNull cq<fg> cqVar, @NonNull a<fg> aVar) {
        super(0, str, aVar);
        this.f5401a = cqVar;
    }

    /* access modifiers changed from: protected */
    public final qt<fg> a(@NonNull qq qqVar) {
        if (200 == qqVar.f5720a) {
            fg fgVar = (fg) this.f5401a.a(qqVar);
            if (fgVar != null) {
                return qt.a(fgVar, ri.a(qqVar));
            }
        }
        return qt.a(new s(qqVar, 9));
    }

    /* access modifiers changed from: protected */
    public final re a(re reVar) {
        return super.a((re) new s(reVar.f5728a, 9));
    }
}
