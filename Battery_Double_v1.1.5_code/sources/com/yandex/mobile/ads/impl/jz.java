package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.facebook.share.internal.MessengerShareContentUtility;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.Map;

public final class jz {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, jy> f5585a = new HashMap<String, jy>() {
        {
            put(MessengerShareContentUtility.MEDIA_IMAGE, new ka());
            put("string", new kd());
            put("media", new kb());
        }
    };

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003c  */
    @NonNull
    public final jy a(@NonNull String str) {
        char c;
        String str2;
        int hashCode = str.hashCode();
        if (hashCode != -1074675180) {
            if (hashCode != 3226745) {
                if (hashCode == 103772132 && str.equals("media")) {
                    c = 2;
                    switch (c) {
                        case 0:
                        case 1:
                            str2 = MessengerShareContentUtility.MEDIA_IMAGE;
                            break;
                        case 2:
                            str2 = "media";
                            break;
                        default:
                            str2 = "string";
                            break;
                    }
                    return (jy) this.f5585a.get(str2);
                }
            } else if (str.equals(SettingsJsonConstants.APP_ICON_KEY)) {
                c = 1;
                switch (c) {
                    case 0:
                    case 1:
                        break;
                    case 2:
                        break;
                }
                return (jy) this.f5585a.get(str2);
            }
        } else if (str.equals("favicon")) {
            c = 0;
            switch (c) {
                case 0:
                case 1:
                    break;
                case 2:
                    break;
            }
            return (jy) this.f5585a.get(str2);
        }
        c = 65535;
        switch (c) {
            case 0:
            case 1:
                break;
            case 2:
                break;
        }
        return (jy) this.f5585a.get(str2);
    }
}
