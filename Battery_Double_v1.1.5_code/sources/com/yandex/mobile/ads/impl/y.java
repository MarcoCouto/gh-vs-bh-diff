package com.yandex.mobile.ads.impl;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import java.lang.ref.WeakReference;

public final class y extends ResultReceiver {

    /* renamed from: a reason: collision with root package name */
    private WeakReference<a> f5821a = new WeakReference<>(null);

    public interface a {
        void a(int i, @Nullable Bundle bundle);
    }

    public y(Handler handler) {
        super(handler);
    }

    public final void a(a aVar) {
        this.f5821a = new WeakReference<>(aVar);
    }

    /* access modifiers changed from: protected */
    public final void onReceiveResult(int i, @Nullable Bundle bundle) {
        if (this.f5821a != null) {
            a aVar = (a) this.f5821a.get();
            if (aVar != null) {
                aVar.a(i, bundle);
                new Object[1][0] = Integer.valueOf(i);
            }
        }
    }
}
