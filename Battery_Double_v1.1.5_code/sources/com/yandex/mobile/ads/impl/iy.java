package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class iy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Object f5569a = new Object();
    private boolean b;

    public final void a(@NonNull Runnable runnable) {
        synchronized (this.f5569a) {
            if (!this.b) {
                this.b = true;
                runnable.run();
            }
        }
    }
}
