package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.List;

public final class nu extends ns {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<a> f5658a;

    public static class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final String f5659a;
        @NonNull
        private final String b;

        public a(@NonNull String str, @NonNull String str2) {
            this.f5659a = str;
            this.b = str2;
        }

        @NonNull
        public final String a() {
            return this.f5659a;
        }

        @NonNull
        public final String b() {
            return this.b;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (!this.f5659a.equals(aVar.f5659a)) {
                return false;
            }
            return this.b.equals(aVar.b);
        }

        public final int hashCode() {
            return (this.f5659a.hashCode() * 31) + this.b.hashCode();
        }
    }

    public nu(@NonNull String str, @NonNull List<a> list) {
        super(str);
        this.f5658a = list;
    }

    @NonNull
    public final List<a> b() {
        return this.f5658a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.f5658a.equals(((nu) obj).f5658a);
    }

    public final int hashCode() {
        return (super.hashCode() * 31) + this.f5658a.hashCode();
    }
}
