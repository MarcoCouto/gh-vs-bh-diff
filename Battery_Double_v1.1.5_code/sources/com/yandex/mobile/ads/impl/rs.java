package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;

public final class rs implements rj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final rj f5745a;
    @NonNull
    private final hb b = new hb();
    @Nullable
    private final Context c;

    public rs(@Nullable Context context, @NonNull rj rjVar) {
        this.f5745a = rjVar;
        this.c = context != null ? context.getApplicationContext() : null;
    }

    public final HttpResponse a(qr<?> qrVar, Map<String, String> map) throws IOException, re {
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        hashMap.put(qj.USER_AGENT.a(), hb.a(this.c));
        return this.f5745a.a(qrVar, hashMap);
    }
}
