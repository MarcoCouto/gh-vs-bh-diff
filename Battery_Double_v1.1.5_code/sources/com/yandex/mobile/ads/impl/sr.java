package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;

public final class sr {
    @NonNull
    public static sq a(@NonNull Context context, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull RequestListener<sk> requestListener) {
        return new sq(context, new tz(vastRequestConfiguration), requestListener);
    }
}
