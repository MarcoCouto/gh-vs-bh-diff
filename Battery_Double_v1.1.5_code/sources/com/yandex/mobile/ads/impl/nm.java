package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import java.util.List;

public final class nm {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final List<ns> f5651a;
    @Nullable
    private final cz b;
    @Nullable
    private final String c;
    @Nullable
    private final String d;

    public nm(@Nullable List<ns> list, @Nullable cz czVar, @Nullable String str, @Nullable String str2) {
        this.f5651a = list;
        this.b = czVar;
        this.c = str;
        this.d = str2;
    }

    @Nullable
    public final List<ns> a() {
        return this.f5651a;
    }

    @Nullable
    public final cz b() {
        return this.b;
    }

    @Nullable
    public final String c() {
        return this.c;
    }

    @Nullable
    public final String d() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nm nmVar = (nm) obj;
        if (this.f5651a == null ? nmVar.f5651a != null : !this.f5651a.equals(nmVar.f5651a)) {
            return false;
        }
        if (this.b == null ? nmVar.b != null : !this.b.equals(nmVar.b)) {
            return false;
        }
        if (this.c == null ? nmVar.c != null : !this.c.equals(nmVar.c)) {
            return false;
        }
        if (this.d != null) {
            return this.d.equals(nmVar.d);
        }
        return nmVar.d == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((this.f5651a != null ? this.f5651a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return hashCode + i;
    }
}
