package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class ic {

    /* renamed from: a reason: collision with root package name */
    private static volatile ic f5545a;
    private static final Object b = new Object();

    @NonNull
    public static ic a() {
        if (f5545a == null) {
            synchronized (b) {
                if (f5545a == null) {
                    f5545a = new ic();
                }
            }
        }
        return f5545a;
    }

    private ic() {
    }

    @NonNull
    public static ib a(boolean z) {
        if (z) {
            return new id();
        }
        return new ia();
    }
}
