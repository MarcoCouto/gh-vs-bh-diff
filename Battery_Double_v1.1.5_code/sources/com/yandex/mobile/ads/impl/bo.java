package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.io.File;

public final class bo {
    public static qs a(@NonNull Context context) {
        qw.a("Yandex Mobile Ads");
        qw.b = false;
        rf rfVar = new rf(new rq(context, new rx()).a());
        new bk();
        File a2 = bk.a(context, "mobileads-volley-cache");
        return new qs(new rh(a2, (int) dn.a(a2)), rfVar);
    }
}
