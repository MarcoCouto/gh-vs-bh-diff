package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.Map;
import java.util.WeakHashMap;

public final class aj {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5369a = new Object();
    private static volatile aj b;
    @NonNull
    private final Map<a, Object> c = new WeakHashMap();

    public interface a {
        void a(@NonNull fg fgVar);
    }

    private aj() {
    }

    public static aj a() {
        if (b == null) {
            synchronized (f5369a) {
                if (b == null) {
                    b = new aj();
                }
            }
        }
        return b;
    }

    public final void a(@NonNull a aVar) {
        synchronized (f5369a) {
            if (!this.c.containsKey(aVar)) {
                this.c.put(aVar, null);
            }
        }
    }

    public final void a(@NonNull Context context, @NonNull fg fgVar) {
        synchronized (f5369a) {
            ff.a().a(context, fgVar);
            for (a a2 : this.c.keySet()) {
                a2.a(fgVar);
            }
        }
    }
}
