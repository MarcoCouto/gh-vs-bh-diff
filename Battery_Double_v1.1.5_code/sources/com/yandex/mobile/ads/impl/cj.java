package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.gu.a;
import java.util.HashMap;
import java.util.Map;

public final class cj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gs f5418a;
    @NonNull
    private final b b;
    @NonNull
    private final fc c;
    @NonNull
    private final cn d = new cn();
    @Nullable
    private x e;
    @Nullable
    private a f;

    public cj(@NonNull Context context, @NonNull b bVar, @NonNull fc fcVar) {
        this.b = bVar;
        this.c = fcVar;
        this.f5418a = gs.a(context);
    }

    public final void a(@NonNull gu.b bVar) {
        b(bVar, a());
    }

    public final void a(@NonNull gu.b bVar, @NonNull Map<String, Object> map) {
        Map a2 = a();
        a2.putAll(map);
        b(bVar, a2);
    }

    public final void a(@NonNull am amVar) {
        b(amVar.c(), c(amVar));
    }

    public final void b(@NonNull am amVar) {
        b(amVar.e(), c(amVar));
    }

    private void b(@NonNull gu.b bVar, @NonNull Map<String, Object> map) {
        this.f5418a.a(new gu(bVar, map));
    }

    @NonNull
    private Map<String, Object> c(@NonNull am amVar) {
        Map<String, Object> a2 = a();
        a2.put(IronSourceConstants.EVENTS_ERROR_REASON, amVar.b().a());
        String a3 = amVar.a();
        if (!TextUtils.isEmpty(a3)) {
            a2.put("asset_name", a3);
        }
        return a2;
    }

    @NonNull
    private Map<String, Object> a() {
        dr drVar = new dr(new HashMap());
        drVar.a("adapter", "Yandex");
        if (this.e != null) {
            drVar.a("block_id", this.e.b());
            drVar.a("ad_type_format", this.e.c());
            drVar.a("product_type", this.e.d());
            b a2 = this.e.a();
            if (a2 != null) {
                drVar.a("ad_type", a2.a());
            } else {
                drVar.a("ad_type");
            }
        } else {
            drVar.a("block_id");
            drVar.a("ad_type_format");
            drVar.a("product_type");
        }
        drVar.a(cn.a(this.c.c()));
        if (this.f != null) {
            drVar.a(this.f.a());
        }
        return drVar.a();
    }

    public final void a(@NonNull x xVar) {
        this.e = xVar;
    }

    public final void a(@NonNull a aVar) {
        this.f = aVar;
    }
}
