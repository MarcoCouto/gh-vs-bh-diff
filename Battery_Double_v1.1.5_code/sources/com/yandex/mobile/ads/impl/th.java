package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ru.a;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class th extends rv<VideoAd, List<VideoAd>> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tl f5787a = new tl();

    public th(@NonNull Context context, @NonNull String str, @NonNull a<List<VideoAd>> aVar, @NonNull VideoAd videoAd, @NonNull gv<VideoAd, List<VideoAd>> gvVar) {
        super(context, 0, str, aVar, videoAd, gvVar);
    }

    /* access modifiers changed from: protected */
    public final qt<List<VideoAd>> a(@NonNull qq qqVar, int i) {
        sk a2 = this.f5787a.a(qqVar);
        if (a2 == null) {
            return qt.a(new sn("Can't parse VAST response."));
        }
        List b = a2.a().b();
        if (b.isEmpty()) {
            return qt.a(new sm());
        }
        return qt.a(b, null);
    }
}
