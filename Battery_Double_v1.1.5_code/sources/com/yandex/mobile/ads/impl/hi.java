package com.yandex.mobile.ads.impl;

import android.util.Log;
import java.util.Locale;

public final class hi extends hh {

    /* renamed from: a reason: collision with root package name */
    private static boolean f5529a;

    public static void a(boolean z) {
        f5529a = z;
    }

    public static void a(String str, Object... objArr) {
        if (f5529a) {
            Log.i("Yandex Mobile Ads", String.format(Locale.US, str, objArr));
        }
    }

    public static void b(String str, Object... objArr) {
        if (f5529a) {
            Log.w("Yandex Mobile Ads", String.format(Locale.US, str, objArr));
        }
    }

    public static void c(String str, Object... objArr) {
        if (f5529a) {
            Log.e("Yandex Mobile Ads", String.format(Locale.US, str, objArr));
        }
    }
}
