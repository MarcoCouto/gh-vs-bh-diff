package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.explorestack.iab.vast.tags.VastTagName;
import com.yandex.mobile.ads.video.models.common.Extension;
import com.yandex.mobile.ads.video.models.common.a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class tt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tn f5797a;
    @NonNull
    private final ts b;

    public tt(@NonNull tn tnVar) {
        this.f5797a = tnVar;
        this.b = new ts(tnVar);
    }

    @NonNull
    static List<Extension> a(@NonNull XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        tn.a(xmlPullParser, VastTagName.EXTENSIONS);
        ArrayList arrayList = new ArrayList();
        while (tn.b(xmlPullParser)) {
            if (tn.a(xmlPullParser)) {
                if (VastTagName.EXTENSION.equals(xmlPullParser.getName())) {
                    tn.a(xmlPullParser, VastTagName.EXTENSION);
                    Extension extension = null;
                    String attributeValue = xmlPullParser.getAttributeValue(null, "type");
                    String c = tn.c(xmlPullParser);
                    if (!TextUtils.isEmpty(attributeValue) && !TextUtils.isEmpty(c)) {
                        extension = a.a(attributeValue, c);
                    }
                    if (extension != null) {
                        arrayList.add(extension);
                    }
                } else {
                    tn.d(xmlPullParser);
                }
            }
        }
        return arrayList;
    }
}
