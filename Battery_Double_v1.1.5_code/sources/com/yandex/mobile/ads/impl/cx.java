package com.yandex.mobile.ads.impl;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.lang.ref.WeakReference;

final class cx {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5428a;
    @NonNull
    private final cw b;
    @NonNull
    private final ln c = new a(this);
    @NonNull
    private final lk d = ll.a();

    @VisibleForTesting
    static class a implements ln {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private WeakReference<Activity> f5429a;
        @NonNull
        private final cx b;

        a(@NonNull cx cxVar) {
            this.b = cxVar;
        }

        public final void a(@NonNull Activity activity) {
            new StringBuilder("onResume, activity = ").append(activity);
            if (this.f5429a != null && activity.equals(this.f5429a.get())) {
                this.b.d();
            }
        }

        public final void b(@NonNull Activity activity) {
            new StringBuilder("onPause, activity = ").append(activity);
            if (this.f5429a == null) {
                this.f5429a = new WeakReference<>(activity);
            }
        }
    }

    cx(@NonNull Context context, @NonNull fc fcVar, @NonNull cz czVar) {
        this.f5428a = context.getApplicationContext();
        this.b = new cw(context, fcVar, czVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.b.a(com.yandex.mobile.ads.impl.cw.a.WEBVIEW);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.b.b(com.yandex.mobile.ads.impl.cw.a.WEBVIEW);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        this.d.a(this.f5428a, this.c);
        this.b.a(com.yandex.mobile.ads.impl.cw.a.BROWSER);
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        this.b.b(com.yandex.mobile.ads.impl.cw.a.BROWSER);
        this.d.b(this.f5428a, this.c);
    }

    public final void e() {
        this.d.a(this.f5428a, this.c);
    }

    public final void f() {
        this.d.b(this.f5428a, this.c);
    }

    public final void a(@NonNull com.yandex.mobile.ads.impl.gu.a aVar) {
        this.b.a(aVar);
    }
}
