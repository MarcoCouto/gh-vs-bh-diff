package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.nativeads.y;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import org.json.JSONException;
import org.json.JSONObject;

public final class ok {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5675a;
    @NonNull
    private final oo b;

    public ok(@NonNull Context context, @NonNull oo ooVar) {
        this.f5675a = context.getApplicationContext();
        this.b = ooVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f6, code lost:
        if (r11.equals("close_button") != false) goto L_0x00fa;
     */
    @NonNull
    public final ni a(@NonNull JSONObject jSONObject) throws JSONException, y {
        char c;
        boolean z;
        oy oyVar;
        oy oyVar2;
        JSONObject jSONObject2 = jSONObject;
        char c2 = 5;
        if (oi.a(jSONObject2, "name", "type", String.CLICKABLE, VastAttributes.REQUIRED, "value")) {
            String a2 = oh.a(jSONObject2, "type");
            String a3 = oh.a(jSONObject2, "name");
            switch (a3.hashCode()) {
                case -1074675180:
                    if (a3.equals("favicon")) {
                        c = 0;
                        break;
                    }
                case -938102371:
                    if (a3.equals(CampaignEx.JSON_KEY_STAR)) {
                        c = 4;
                        break;
                    }
                case -807286424:
                    if (a3.equals("review_count")) {
                        c = 5;
                        break;
                    }
                case -191501435:
                    if (a3.equals("feedback")) {
                        c = 1;
                        break;
                    }
                case 3226745:
                    if (a3.equals(SettingsJsonConstants.APP_ICON_KEY)) {
                        c = 2;
                        break;
                    }
                case 103772132:
                    if (a3.equals("media")) {
                        c = 3;
                        break;
                    }
                default:
                    c = 65535;
                    break;
            }
            switch (c) {
                case 0:
                case 1:
                case 2:
                    z = MessengerShareContentUtility.MEDIA_IMAGE.equals(a2);
                    break;
                case 3:
                    z = "media".equals(a2);
                    break;
                case 4:
                case 5:
                    z = "number".equals(a2);
                    break;
                default:
                    z = "string".equals(a2);
                    break;
            }
            if (z) {
                JSONObject optJSONObject = jSONObject2.optJSONObject("link");
                nm a4 = optJSONObject != null ? this.b.a(optJSONObject) : null;
                Context context = this.f5675a;
                switch (a3.hashCode()) {
                    case -1678958759:
                        break;
                    case -1074675180:
                        if (a3.equals("favicon")) {
                            c2 = 0;
                            break;
                        }
                    case -938102371:
                        if (a3.equals(CampaignEx.JSON_KEY_STAR)) {
                            c2 = 3;
                            break;
                        }
                    case -807286424:
                        if (a3.equals("review_count")) {
                            c2 = 4;
                            break;
                        }
                    case -191501435:
                        if (a3.equals("feedback")) {
                            c2 = 6;
                            break;
                        }
                    case 3226745:
                        if (a3.equals(SettingsJsonConstants.APP_ICON_KEY)) {
                            c2 = 1;
                            break;
                        }
                    case 103772132:
                        if (a3.equals("media")) {
                            c2 = 2;
                            break;
                        }
                    default:
                        c2 = 65535;
                        break;
                }
                switch (c2) {
                    case 0:
                    case 1:
                        oyVar2 = new pb(context);
                        break;
                    case 2:
                        oyVar2 = new pc(context);
                        break;
                    case 3:
                    case 4:
                        oyVar = new pd();
                        break;
                    case 5:
                        oyVar = new oz();
                        break;
                    case 6:
                        oyVar = new pa(new pb(context));
                        break;
                    default:
                        oyVar = new pf();
                        break;
                }
                oyVar = oyVar2;
                ni niVar = new ni(a3, a2, oyVar.a(jSONObject2), a4, jSONObject2.getBoolean(String.CLICKABLE), jSONObject2.getBoolean(VastAttributes.REQUIRED));
                return niVar;
            }
            throw new y("Native Ad json has not required attributes");
        }
        throw new y("Native Ad json has not required attributes");
    }
}
