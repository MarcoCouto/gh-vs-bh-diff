package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ej.a;

public final class ed extends eg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ee f5454a;
    @NonNull
    private ej b = new el();

    public ed(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar, @NonNull ap apVar) {
        super(context, xVar, fcVar);
        this.f5454a = new ee(this, apVar);
    }

    public final void c(@NonNull String str) {
        this.f5454a.b(str);
    }

    public final void setAspectRatio(float f) {
        this.b = new ek(f);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f5454a.a();
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        a a2 = this.b.a(i, i2);
        super.onMeasure(a2.f5461a, a2.b);
    }

    public final void setClickListener(@NonNull lt ltVar) {
        this.f5454a.a(ltVar);
    }
}
