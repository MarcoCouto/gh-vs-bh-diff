package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public final class ge {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5508a = new Object();
    private static volatile ge b;
    @NonNull
    private final Context c;
    @NonNull
    private final gh d = new gh();
    @NonNull
    private final gg e = new gg();

    private ge(@NonNull Context context) {
        this.c = context.getApplicationContext();
    }

    @NonNull
    public static ge a(@NonNull Context context) {
        if (b == null) {
            synchronized (f5508a) {
                if (b == null) {
                    b = new ge(context);
                }
            }
        }
        return b;
    }

    @Nullable
    public final Location a() {
        Location a2;
        synchronized (f5508a) {
            Context context = this.c;
            ArrayList arrayList = new ArrayList();
            arrayList.add(new go(context));
            fg a3 = ff.a().a(context);
            if (a3 != null && !a3.h()) {
                arrayList.add(gj.a(context));
            }
            a2 = gh.a(a((List<gf>) arrayList));
        }
        return a2;
    }

    @NonNull
    private static List<Location> a(@NonNull List<gf> list) {
        ArrayList arrayList = new ArrayList();
        for (gf a2 : list) {
            Location a3 = a2.a();
            if (a3 != null) {
                arrayList.add(a3);
            }
        }
        return arrayList;
    }
}
