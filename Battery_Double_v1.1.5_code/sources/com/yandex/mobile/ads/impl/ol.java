package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.y;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ol {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final on f5676a = new on();

    @NonNull
    public static ny a(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, y {
        JSONObject jSONObject2 = jSONObject.getJSONObject(str);
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject2.optJSONArray("installedPackages");
        if (optJSONArray != null) {
            int i = 0;
            while (i < optJSONArray.length()) {
                JSONObject jSONObject3 = optJSONArray.getJSONObject(i);
                if (oi.a(jSONObject3, "name")) {
                    arrayList.add(new nz(oh.a(jSONObject3, "name"), di.a(jSONObject3, "minVersion", 0), di.a(jSONObject3, "maxVersion", Integer.MAX_VALUE)));
                    i++;
                } else {
                    throw new y("Native Ad json has not required attributes");
                }
            }
        }
        return new ny(arrayList);
    }
}
