package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.gu.c;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;

public final class og implements bv<nq> {

    /* renamed from: a reason: collision with root package name */
    private final bm f5672a = new bm();
    @NonNull
    private final oh b;

    public og(@NonNull Context context) {
        this.b = new oh(context);
    }

    @Nullable
    private nq a(@NonNull String str) {
        try {
            nq a2 = this.b.a(str);
            try {
                a2.a("status", c.SUCCESS);
                return a2;
            } catch (y | JSONException unused) {
                return a2;
            }
        } catch (y | JSONException unused2) {
            return null;
        }
    }

    @Nullable
    public final /* synthetic */ Object b(@NonNull qq qqVar) {
        String a2 = bm.a(qqVar);
        if (!TextUtils.isEmpty(a2)) {
            return a(a2);
        }
        return null;
    }
}
