package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.c;
import java.util.List;
import java.util.Map;

public final class pw extends co<nq> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final px f5702a = new px();

    /* access modifiers changed from: protected */
    @NonNull
    public final Map<String, Object> a(@NonNull fc fcVar) {
        Map<String, Object> a2 = super.a(fcVar);
        a2.put("image_loading_automatically", Boolean.valueOf(fcVar.t()));
        String[] n = fcVar.n();
        if (n != null && n.length > 0) {
            a2.put("image_sizes", fcVar.n());
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final Map<String, Object> a(@NonNull fc fcVar, @Nullable qt<x<nq>> qtVar, int i) {
        c cVar;
        Map<String, Object> a2 = super.a(fcVar, qtVar, i);
        if (204 == i) {
            cVar = c.NO_ADS;
        } else if (qtVar == null || qtVar.f5724a == null || i != 200) {
            cVar = c.ERROR;
        } else {
            x xVar = (x) qtVar.f5724a;
            nq nqVar = (nq) xVar.t();
            cVar = nqVar != null ? (c) nqVar.a().get("status") : xVar.s() == null ? c.ERROR : null;
        }
        if (cVar != null) {
            a2.put("status", cVar.a());
        }
        if (!(qtVar == null || qtVar.f5724a == null)) {
            List a3 = px.a((x) qtVar.f5724a);
            if (!a3.isEmpty()) {
                a2.put("image_sizes", a3.toArray(new String[a3.size()]));
            }
            List b = px.b((x) qtVar.f5724a);
            if (!b.isEmpty()) {
                a2.put("native_ad_types", b.toArray(new String[b.size()]));
            }
        }
        return a2;
    }
}
