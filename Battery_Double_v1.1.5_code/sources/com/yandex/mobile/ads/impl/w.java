package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public final class w {

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        public static final int f5816a = Color.parseColor("#fffeec95");
        static final int b = Color.parseColor("#fff5cf60");
        static final int c = Color.parseColor("#ffd8d8d8");
    }

    static final class b {

        /* renamed from: a reason: collision with root package name */
        static final ColorDrawable f5817a = new ColorDrawable(Color.parseColor("#80ffffff"));
        static final LayerDrawable b;

        static {
            GradientDrawable gradientDrawable = new GradientDrawable(Orientation.LEFT_RIGHT, new int[]{0, 0});
            gradientDrawable.setCornerRadius(0.0f);
            GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.LEFT_RIGHT, new int[]{a.b, a.b});
            gradientDrawable2.setCornerRadius(0.0f);
            LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{gradientDrawable, new ClipDrawable(gradientDrawable2, 3, 1)});
            layerDrawable.setId(0, 16908288);
            layerDrawable.setId(1, 16908301);
            b = layerDrawable;
        }

        static StateListDrawable a() {
            ColorDrawable colorDrawable = new ColorDrawable(a.f5816a);
            ColorDrawable colorDrawable2 = new ColorDrawable(0);
            int[] iArr = {16842919};
            int[] iArr2 = new int[0];
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(iArr, colorDrawable);
            stateListDrawable.addState(iArr2, colorDrawable2);
            return stateListDrawable;
        }
    }

    static final class c {
        @NonNull
        static LayoutParams a() {
            return new LayoutParams(-2, -1);
        }
    }

    static final class d {
        @NonNull
        static ImageView a(@NonNull Context context, @NonNull String str, int i) {
            ImageView imageView = new ImageView(context);
            imageView.setImageBitmap(di.a(str));
            imageView.setAdjustViewBounds(true);
            imageView.setPadding(i, i, i, i);
            imageView.setBackgroundDrawable(b.a());
            return imageView;
        }
    }

    private enum e {
        BROWSER_CONTROL_PANEL_HEIGHT,
        BROWSER_CONTROL_PANEL_BUTTON_PADDING,
        BROWSER_CONTROL_PANEL_TITLE_TEXT_SIZE;
        
        private static final Map<String, Object> d = null;

        static {
            HashMap hashMap = new HashMap();
            d = hashMap;
            hashMap.put(BROWSER_CONTROL_PANEL_HEIGHT.a("values_dimen_%s"), Integer.valueOf(48));
            d.put(BROWSER_CONTROL_PANEL_HEIGHT.a("values_dimen_%s_sw600dp"), Integer.valueOf(56));
            d.put(BROWSER_CONTROL_PANEL_BUTTON_PADDING.a("values_dimen_%s"), Integer.valueOf(15));
            d.put(BROWSER_CONTROL_PANEL_BUTTON_PADDING.a("values_dimen_%s_sw600dp"), Integer.valueOf(17));
            d.put(BROWSER_CONTROL_PANEL_TITLE_TEXT_SIZE.a("values_dimen_%s"), Integer.valueOf(19));
            d.put(BROWSER_CONTROL_PANEL_TITLE_TEXT_SIZE.a("values_dimen_%s_sw600dp"), Integer.valueOf(23));
        }

        private static String a(String str, String str2) {
            return String.format(Locale.US, str, new Object[]{str2});
        }

        private String a(String str) {
            return a(str, name());
        }

        /* access modifiers changed from: 0000 */
        public final int b(Context context) {
            return dv.a(context, (float) a(context));
        }

        public final String toString() {
            return name();
        }

        /* access modifiers changed from: 0000 */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x002f */
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0043 A[Catch:{ Exception -> 0x0048 }] */
        public final int a(Context context) {
            StringBuilder sb = new StringBuilder(name());
            if (dv.g(context) >= 600) {
                sb.append("_sw600dp");
            }
            Integer num = (Integer) d.get(a("values_dimen_%s", sb.toString()));
            if (num != null) {
                return num.intValue();
            }
            try {
                Integer num2 = (Integer) d.get(a("values_dimen_%s", name()));
                if (num2 != null) {
                    return num2.intValue();
                }
            } catch (Exception unused) {
            }
            return 0;
        }
    }
}
