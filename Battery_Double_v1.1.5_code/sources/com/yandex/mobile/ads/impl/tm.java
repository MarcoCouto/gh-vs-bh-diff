package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import org.xmlpull.v1.XmlPullParser;

final class tm {
    tm() {
    }

    @NonNull
    static sl a(@NonNull XmlPullParser xmlPullParser) {
        return new sl(a(xmlPullParser, VastAttributes.ALLOW_MULTIPLE_ADS), a(xmlPullParser, VastAttributes.FOLLOW_ADDITIONAL_WRAPPERS));
    }

    private static boolean a(@NonNull XmlPullParser xmlPullParser, @NonNull String str) {
        return Boolean.parseBoolean(xmlPullParser.getAttributeValue(null, str));
    }
}
