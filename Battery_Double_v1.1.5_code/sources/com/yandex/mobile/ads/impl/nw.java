package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.List;

public final class nw extends ns {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5661a;
    @NonNull
    private final List<nx> b;

    public nw(@NonNull String str, @NonNull String str2, @NonNull List<nx> list) {
        super(str);
        this.f5661a = str2;
        this.b = list;
    }

    @NonNull
    public final String b() {
        return this.f5661a;
    }

    @NonNull
    public final List<nx> c() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        nw nwVar = (nw) obj;
        if (!this.f5661a.equals(nwVar.f5661a)) {
            return false;
        }
        return this.b.equals(nwVar.b);
    }

    public final int hashCode() {
        return (((super.hashCode() * 31) + this.f5661a.hashCode()) * 31) + this.b.hashCode();
    }
}
