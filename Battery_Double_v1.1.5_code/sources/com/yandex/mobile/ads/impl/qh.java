package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public final class qh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gs f5709a;
    @NonNull
    private final cn b;
    @NonNull
    private final fc c;
    @NonNull
    private final List<ni> d;

    public qh(@NonNull Context context, @NonNull fc fcVar, @Nullable List<ni> list) {
        this.c = fcVar;
        if (list == null) {
            list = Collections.emptyList();
        }
        this.d = list;
        this.f5709a = gs.a(context);
        this.b = new cn();
    }

    public final void a(@NonNull List<String> list) {
        List<ni> list2 = this.d;
        ArrayList arrayList = new ArrayList();
        for (ni a2 : list2) {
            arrayList.add(a2.a());
        }
        ArrayList arrayList2 = new ArrayList(list);
        arrayList2.removeAll(arrayList);
        if (!arrayList2.isEmpty()) {
            HashMap hashMap = new HashMap();
            String e = this.c.e();
            if (e != null) {
                hashMap.put("block_id", e);
            }
            hashMap.put("assets", arrayList2.toArray());
            hashMap.putAll(cn.a(this.c.c()));
            this.f5709a.a(new gu(b.REQUIRED_ASSET_MISSING, hashMap));
        }
    }
}
