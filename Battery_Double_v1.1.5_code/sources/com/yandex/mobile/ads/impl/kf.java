package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class kf {
    @NonNull
    public static kg a(@NonNull x<nq> xVar) {
        be s = xVar.s();
        if (s != null) {
            return new ke(s);
        }
        return new kh();
    }
}
