package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.webkit.ConsoleMessage;
import android.webkit.ConsoleMessage.MessageLevel;
import android.webkit.WebChromeClient;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;

final class dz extends WebChromeClient {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cg f5451a;

    dz(@NonNull cg cgVar) {
        this.f5451a = cgVar;
    }

    public final boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
        String format = String.format("Console message: %s, sourceId: %s, lineNumber: %s", new Object[]{consoleMessage.message(), consoleMessage.sourceId(), Integer.valueOf(consoleMessage.lineNumber())});
        if (consoleMessage.messageLevel() == MessageLevel.ERROR) {
            HashMap hashMap = new HashMap();
            hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, format);
            this.f5451a.a(b.JS_ERROR, hashMap);
        }
        return true;
    }
}
