package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.ArrayList;
import java.util.List;

final class sv {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Context f5777a;
    /* access modifiers changed from: private */
    @NonNull
    public final up b;
    @NonNull
    private final uj c = new uj();
    /* access modifiers changed from: private */
    @NonNull
    public final List<VideoAd> d = new ArrayList();

    private class a implements RequestListener<List<VideoAd>> {
        @NonNull
        private final RequestListener<List<VideoAd>> b;

        public final /* synthetic */ void onSuccess(@NonNull Object obj) {
            ui a2 = uj.a((List) obj);
            sv.this.d.addAll(a2.a());
            List b2 = a2.b();
            if (b2.isEmpty()) {
                a();
            } else {
                sv.this.b.a(sv.this.f5777a, b2, this);
            }
        }

        a(RequestListener<List<VideoAd>> requestListener) {
            this.b = requestListener;
        }

        public final void onFailure(@NonNull VideoAdError videoAdError) {
            a();
        }

        private void a() {
            if (sv.this.d.isEmpty()) {
                this.b.onFailure(VideoAdError.createNoAdError(new sm()));
                return;
            }
            this.b.onSuccess(sv.this.d);
        }
    }

    sv(@NonNull Context context, @NonNull ub ubVar) {
        this.f5777a = context.getApplicationContext();
        this.b = new up(ubVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull List<VideoAd> list, @NonNull RequestListener<List<VideoAd>> requestListener) {
        ui a2 = uj.a(list);
        this.d.addAll(a2.a());
        this.b.a(this.f5777a, a2.b(), new a(requestListener));
    }
}
