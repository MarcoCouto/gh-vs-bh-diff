package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.MobileAds;
import com.yandex.mobile.ads.b;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class ad {
    private final String A;
    private final Map<String, String> B;
    private final Long C;
    private final String D;
    private final String E;
    private final String F;
    private final String G;
    private final String H;
    private final String I;
    private final String J;
    private final String K;
    private final String L;
    private final String M;
    private final String N;
    private final String O;
    private final String P;
    private final Integer Q;
    private final String R;
    private final String S;
    private final Boolean T;
    private final String U;
    private StringBuilder V;

    /* renamed from: a reason: collision with root package name */
    private final String f5360a;
    private final String b;
    private final String c;
    private final Integer d;
    private final Integer e;
    private final String f;
    private final Integer g;
    private final Integer h;
    private final Float i;
    private final Location j;
    private final Integer k;
    private final Integer l;
    private final String m;
    private final String n;
    private final AdRequest o;
    private final Integer p;
    private final Integer q;
    private final String r;
    private final Boolean s;
    private final String t;
    private final Integer u;
    private final String v;
    private final String w;
    private final String x;
    private final String y;
    private final String z;

    public static class a<T> {
        /* access modifiers changed from: private */
        public String A;
        /* access modifiers changed from: private */
        public String B;
        /* access modifiers changed from: private */
        public String C;
        /* access modifiers changed from: private */
        public String D;
        /* access modifiers changed from: private */
        public String E;
        /* access modifiers changed from: private */
        public Long F;
        /* access modifiers changed from: private */
        public String G;
        /* access modifiers changed from: private */
        public String H;
        /* access modifiers changed from: private */
        public String I;
        /* access modifiers changed from: private */
        public String J;
        /* access modifiers changed from: private */
        public String K;
        /* access modifiers changed from: private */
        public String L;
        /* access modifiers changed from: private */
        public String M;
        /* access modifiers changed from: private */
        public String N;
        /* access modifiers changed from: private */
        public String O;
        /* access modifiers changed from: private */
        public String P;
        /* access modifiers changed from: private */
        public String Q;
        /* access modifiers changed from: private */
        public String R;
        /* access modifiers changed from: private */
        public String S;
        /* access modifiers changed from: private */
        public int T;
        /* access modifiers changed from: private */
        public String U;
        /* access modifiers changed from: private */
        public String V;
        /* access modifiers changed from: private */
        public String W;
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final fh f5361a;
        private final boolean b;
        /* access modifiers changed from: private */
        @NonNull
        public final Map<String, String> c;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean d;
        /* access modifiers changed from: private */
        public String e;
        /* access modifiers changed from: private */
        public String f;
        /* access modifiers changed from: private */
        public String g;
        /* access modifiers changed from: private */
        public Integer h;
        /* access modifiers changed from: private */
        public Integer i;
        /* access modifiers changed from: private */
        public String j;
        /* access modifiers changed from: private */
        public Location k;
        /* access modifiers changed from: private */
        public Integer l;
        /* access modifiers changed from: private */
        public Integer m;
        /* access modifiers changed from: private */
        public Float n;
        /* access modifiers changed from: private */
        public Integer o;
        /* access modifiers changed from: private */
        public Integer p;
        /* access modifiers changed from: private */
        public String q;
        /* access modifiers changed from: private */
        public String r;
        /* access modifiers changed from: private */
        public AdRequest s;
        /* access modifiers changed from: private */
        public Integer t;
        /* access modifiers changed from: private */
        public Integer u;
        /* access modifiers changed from: private */
        public String v;
        /* access modifiers changed from: private */
        public Boolean w;
        /* access modifiers changed from: private */
        public String x;
        /* access modifiers changed from: private */
        public Integer y;
        /* access modifiers changed from: private */
        public String z;

        /* synthetic */ a(boolean z2, byte b2) {
            this(z2);
        }

        private a(boolean z2) {
            this.c = new HashMap();
            this.b = z2;
            this.f5361a = new fh();
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(String str) {
            this.e = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b(@Nullable String str) {
            this.f = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> c(String str) {
            this.H = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(b bVar) {
            if (bVar != null) {
                this.g = bVar.a();
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@NonNull Context context) {
            this.k = this.b ? null : ge.a(context).a();
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(int i2, int i3) {
            this.h = Integer.valueOf(i2);
            this.i = Integer.valueOf(i3);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b(Context context) {
            this.l = Integer.valueOf(dv.c(context));
            this.m = Integer.valueOf(dv.d(context));
            this.n = Float.valueOf(dv.e(context));
            this.T = dv.f(context);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> c(Context context) {
            if (1 == context.getResources().getConfiguration().orientation) {
                this.j = "portrait";
            } else {
                this.j = "landscape";
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(Integer num) {
            this.y = num;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable AdRequest adRequest) {
            String str;
            String str2;
            if (adRequest != null) {
                HashMap hashMap = null;
                this.s = this.b ? null : adRequest;
                new bz();
                String contextQuery = adRequest.getContextQuery();
                if (!TextUtils.isEmpty(contextQuery)) {
                    String encode = Uri.encode(contextQuery);
                    if (encode != null && encode.length() > 1024) {
                        hi.b("Exceeded the length of the parameter! The maximum size of the parameter is %s bytes. First %s bytes of the parameter will be used", Integer.valueOf(1024), Integer.valueOf(1024));
                        String encode2 = Uri.encode(" ");
                        String substring = encode.substring(0, 1024);
                        encode = encode.startsWith(encode2, 1024) ? substring : substring.substring(0, substring.lastIndexOf(encode2));
                    }
                    str = Uri.decode(encode);
                } else {
                    str = null;
                }
                this.z = str;
                List<String> contextTags = adRequest.getContextTags();
                StringBuilder sb = new StringBuilder();
                if (contextTags != null) {
                    String str3 = "";
                    for (String str4 : contextTags) {
                        if (!TextUtils.isEmpty(str4)) {
                            sb.append(str3);
                            sb.append(str4);
                            sb.append("\n");
                            str3 = "3";
                        }
                    }
                }
                String sb2 = sb.toString();
                if (!TextUtils.isEmpty(sb2)) {
                    String encode3 = Uri.encode(sb2);
                    if (encode3 != null && encode3.length() > 2048) {
                        hi.b("Exceeded the length of the parameter! The maximum size of the parameter is %s bytes. First %s bytes of the parameter will be used", Integer.valueOf(2048), Integer.valueOf(2048));
                        String encode4 = Uri.encode("\n");
                        encode3 = encode3.substring(0, 2048);
                        if (!encode3.endsWith(encode4)) {
                            encode3 = encode3.substring(0, encode3.lastIndexOf(encode4));
                        }
                    }
                    str2 = Uri.decode(encode3);
                } else {
                    str2 = null;
                }
                this.A = str2;
                this.U = bz.a(adRequest.getAge());
                this.V = bz.a(adRequest.getGender());
                Map parameters = adRequest.getParameters();
                if (parameters != null && !parameters.isEmpty()) {
                    hashMap = new HashMap();
                    StringBuilder sb3 = new StringBuilder();
                    Iterator it = parameters.entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Entry entry = (Entry) it.next();
                        sb3.append(RequestParameters.AMPERSAND);
                        sb3.append(Uri.encode((String) entry.getKey()));
                        sb3.append(RequestParameters.EQUAL);
                        sb3.append(Uri.encode((String) entry.getValue()));
                        if (sb3.length() > 61440) {
                            hi.b("Exceeded the length of the parameter! The maximum size of the parameter is %s bytes. First %s bytes of the parameter will be used", Integer.valueOf(61440), Integer.valueOf(61440));
                            break;
                        }
                        hashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                a((Map<String, String>) hashMap);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable String[] strArr) {
            this.B = c(strArr);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b(@Nullable String[] strArr) {
            this.C = c(strArr);
            return this;
        }

        @Nullable
        private static String c(@Nullable String[] strArr) {
            if (strArr == null || strArr.length <= 0) {
                return null;
            }
            String str = "";
            StringBuilder sb = new StringBuilder();
            for (String str2 : strArr) {
                if (!TextUtils.isEmpty(str2)) {
                    sb.append(str);
                    sb.append(str2);
                    str = ",";
                }
            }
            return sb.toString();
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable ae aeVar) {
            if (aeVar != null) {
                this.D = aeVar.a();
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable af afVar) {
            if (afVar != null && af.PROMO == afVar) {
                this.E = afVar.a();
            }
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable Map<String, String> map) {
            if (map != null) {
                this.c.putAll(map);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> d(Context context) {
            this.o = dp.a(context);
            this.p = dp.b(context);
            this.q = dp.d(context);
            this.r = dp.c(context);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> e(Context context) {
            this.t = dp.e(context);
            this.u = dp.f(context);
            this.v = dp.g(context);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@NonNull Context context, @Nullable String str) {
            this.J = this.f5361a.a(context);
            this.K = "android";
            this.L = VERSION.RELEASE;
            this.M = Build.MANUFACTURER;
            this.N = Build.MODEL;
            this.O = fj.a(context);
            if (this.b) {
                str = null;
            }
            this.S = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a() {
            this.W = MobileAds.getLibraryVersion();
            return this;
        }

        /* access modifiers changed from: 0000 */
        public final a<T> d(@Nullable String str) {
            this.I = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b() {
            this.d = ff.a().e();
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable fu fuVar) {
            if (fuVar != null) {
                this.w = Boolean.valueOf(fuVar.b());
                if (!this.b && this.w != null && !this.w.booleanValue()) {
                    this.x = fuVar.a();
                }
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(long j2) {
            this.F = Long.valueOf(j2);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> e(String str) {
            this.G = str;
            return this;
        }

        @NonNull
        public final String c() {
            return new ad(this, 0).toString();
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> f(Context context) {
            this.P = context.getPackageName();
            this.Q = dj.a(context);
            this.R = dj.b(context);
            return this;
        }
    }

    /* synthetic */ ad(a aVar, byte b2) {
        this(aVar);
    }

    private ad(a<?> aVar) {
        this.f5360a = aVar.e;
        this.b = aVar.f;
        this.c = aVar.g;
        this.d = aVar.h;
        this.e = aVar.i;
        this.f = aVar.j;
        this.g = aVar.l;
        this.h = aVar.m;
        this.i = aVar.n;
        this.j = aVar.k;
        this.k = aVar.o;
        this.l = aVar.p;
        this.m = aVar.q;
        this.n = aVar.r;
        this.o = aVar.s;
        this.p = aVar.t;
        this.q = aVar.u;
        this.r = aVar.v;
        this.s = aVar.w;
        this.t = aVar.x;
        this.u = aVar.y;
        this.v = aVar.z;
        this.w = aVar.A;
        this.x = aVar.B;
        this.y = aVar.C;
        this.z = aVar.D;
        this.A = aVar.E;
        this.B = aVar.c;
        this.C = aVar.F;
        this.D = aVar.G;
        this.E = aVar.H;
        this.F = aVar.I;
        this.G = aVar.J;
        this.H = aVar.K;
        this.I = aVar.L;
        this.J = aVar.M;
        this.K = aVar.N;
        this.L = aVar.O;
        this.M = aVar.P;
        this.N = aVar.Q;
        this.O = aVar.R;
        this.P = aVar.S;
        this.Q = Integer.valueOf(aVar.T);
        this.R = aVar.U;
        this.S = aVar.V;
        this.T = aVar.d;
        this.U = aVar.W;
    }

    public static a<?> a(boolean z2) {
        return new a<>(z2, 0);
    }

    public final String toString() {
        if (!TextUtils.isEmpty(this.V)) {
            return this.V.toString();
        }
        this.V = new StringBuilder();
        a("ad_unit_id", (Object) this.f5360a);
        a("uuid", (Object) this.b);
        a("width", (Object) this.d);
        a("height", (Object) this.e);
        a("orientation", (Object) this.f);
        a("screen_width", (Object) this.g);
        a("screen_height", (Object) this.h);
        a("scalefactor", (Object) this.i);
        a(RequestParameters.NETWORK_MCC, (Object) this.k);
        a(RequestParameters.NETWORK_MNC, (Object) this.l);
        a("ad_type", (Object) this.c);
        a("network_type", (Object) this.m);
        a("carrier", (Object) this.n);
        a("cellid", (Object) this.p);
        a("lac", (Object) this.q);
        a("wifi", (Object) this.r);
        a("dnt", this.s);
        a("google_aid", (Object) this.t);
        a("battery_charge", (Object) this.u);
        a("context_query", (Object) this.v);
        a("context_taglist", (Object) this.w);
        a("image_sizes", (Object) this.x);
        a("app_supported_features", (Object) this.y);
        a("response_ad_format", (Object) this.z);
        a("ad_source", (Object) this.A);
        a("debug_yandexuid", (Object) this.E);
        a("user_id", (Object) this.F);
        a("session_random", (Object) this.C);
        a(HttpRequest.PARAM_CHARSET, (Object) this.D);
        a(TapjoyConstants.TJC_DEVICE_TYPE_NAME, (Object) this.G);
        a("os_name", (Object) this.H);
        a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, (Object) this.I);
        a("manufacturer", (Object) this.J);
        a("model", (Object) this.K);
        a("locale", (Object) this.L);
        a("app_id", (Object) this.M);
        a("app_version_code", (Object) this.N);
        a("app_version_name", (Object) this.O);
        a("device-id", (Object) this.P);
        a("screen_dpi", (Object) this.Q);
        a(IronSourceSegment.AGE, (Object) this.R);
        a("gender", (Object) this.S);
        a("user_consent", this.T);
        a("sdk_version", (Object) this.U);
        a(this.B);
        AdRequest adRequest = this.o;
        if (adRequest != null) {
            a(adRequest.getLocation());
        }
        if (this.o == null || this.o.getLocation() == null) {
            a(this.j);
        }
        return this.V.toString();
    }

    private void a(Map<String, String> map) {
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                a((String) entry.getKey(), entry.getValue());
            }
        }
    }

    private void a(String str, Object obj) {
        if (obj != null) {
            a();
            StringBuilder sb = this.V;
            sb.append(Uri.encode(str));
            sb.append(RequestParameters.EQUAL);
            sb.append(Uri.encode(obj.toString()));
        }
    }

    private void a(String str, Boolean bool) {
        if (bool != null) {
            a(str, (Object) Integer.valueOf(bool.booleanValue() ? 1 : 0));
        }
    }

    private void a(Location location) {
        if (location != null) {
            a("lat", (Object) String.valueOf(location.getLatitude()));
            a("lon", (Object) String.valueOf(location.getLongitude()));
            a("precision", (Object) String.valueOf((int) location.getAccuracy()));
        }
    }

    private void a() {
        this.V.append(TextUtils.isEmpty(this.V) ? "" : RequestParameters.AMPERSAND);
    }

    @Nullable
    public static String a(@NonNull fc fcVar) {
        String f2 = fcVar.f();
        if (TextUtils.isEmpty(f2)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(f2);
        sb.append(f2.endsWith("/") ? "" : "/");
        sb.append("v4/ad");
        return sb.toString();
    }

    @NonNull
    public static a a(@NonNull Context context, @NonNull fc fcVar) {
        a a2 = a(fe.a(context)).a(fcVar.c()).a(fcVar.a()).a(fcVar.i()).f(context).a(dp.h(context)).a(fcVar.e()).e("UTF-8").c(fcVar.h()).a(context, fcVar.g()).a(fcVar.n()).e(context).a(context).c(context).d(context).a(fcVar.o()).b(context).a().a(di.f5439a);
        al b2 = fcVar.b();
        if (b2 != null) {
            a2 = a2.a(b2.b(context), b2.a(context));
        }
        return a2.a(fcVar.p()).b(fcVar.r()).d(fcVar.s()).b(fcVar.d());
    }
}
