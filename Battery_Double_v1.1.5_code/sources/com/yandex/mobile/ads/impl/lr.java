package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class lr implements lp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ed f5609a;

    public lr(@NonNull ed edVar) {
        this.f5609a = edVar;
    }

    public final void a(@NonNull nm nmVar, @NonNull lq lqVar) {
        this.f5609a.setClickListener(new lu(nmVar, lqVar));
    }
}
