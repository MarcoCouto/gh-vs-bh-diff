package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.regex.Pattern;

public final class hz {

    /* renamed from: a reason: collision with root package name */
    private static final Pattern f5543a = Pattern.compile("(<script)(.*)(src=\"mraid\\.js\")(.*)(<\\/script>)");

    public static boolean a(@NonNull String str) {
        return f5543a.matcher(str).find();
    }
}
