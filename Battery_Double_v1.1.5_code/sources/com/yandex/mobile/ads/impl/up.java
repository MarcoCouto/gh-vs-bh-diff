package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class up {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ub f5812a;
    private int b;

    public up(@NonNull ub ubVar) {
        this.f5812a = ubVar;
    }

    public final void a(@NonNull Context context, @NonNull List<VideoAd> list, @NonNull RequestListener<List<VideoAd>> requestListener) {
        this.b++;
        if (this.b <= 5) {
            new uq(this.f5812a).a(context, list, requestListener);
        } else {
            requestListener.onFailure(VideoAdError.createInternalError("Maximum count of VAST wrapper requests exceeded."));
        }
    }
}
