package com.yandex.mobile.ads.impl;

public final class oc {

    /* renamed from: a reason: collision with root package name */
    private final int f5666a;

    oc(int i) {
        this.f5666a = i;
    }

    /* access modifiers changed from: 0000 */
    public final int a(int i) {
        return i > 0 ? Math.min(i, this.f5666a) : this.f5666a;
    }
}
