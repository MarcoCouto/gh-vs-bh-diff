package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.lang.ref.WeakReference;

final class r extends ab {

    final class a extends WebChromeClient {
        private final WeakReference<Context> b;

        a(Context context) {
            this.b = new WeakReference<>(context);
        }

        public final void onProgressChanged(WebView webView, int i) {
            c a2 = r.a(this.b);
            if (a2 != null) {
                a2.a(webView, i);
            }
        }
    }

    final class b extends WebViewClient {
        private final WeakReference<Context> b;

        b(Context context) {
            this.b = new WeakReference<>(context);
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (TextUtils.isEmpty(str) || (!dt.b(str) && dt.c(str))) {
                return false;
            }
            return dt.a(webView.getContext(), str, false);
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            c a2 = r.a(this.b);
            if (a2 != null) {
                a2.h();
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            c a2 = r.a(this.b);
            if (a2 != null) {
                a2.i();
            }
        }
    }

    interface c {
        void a(WebView webView, int i);

        void h();

        void i();
    }

    public r(Context context) {
        super(context);
        a_(context);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public final void a_(Context context) {
        super.a_(context);
        setBackgroundColor(-1);
        setInitialScale(1);
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setDomStorageEnabled(true);
        b_();
        setScrollbarFadingEnabled(true);
        setDrawingCacheEnabled(true);
        setWebChromeClient(new a(context));
        setWebViewClient(new b(context));
    }

    static c a(WeakReference<Context> weakReference) {
        if (weakReference.get() instanceof c) {
            return (c) weakReference.get();
        }
        return null;
    }
}
