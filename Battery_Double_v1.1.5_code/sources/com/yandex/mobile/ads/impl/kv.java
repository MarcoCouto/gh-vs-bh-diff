package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.at.a;

public final class kv extends ld<TextView, nj> {
    public final /* synthetic */ void a(@NonNull View view) {
        TextView textView = (TextView) view;
        textView.setText("");
        super.a(textView);
    }

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        TextView textView = (TextView) view;
        nj njVar = (nj) obj;
        if (a.TEXT == njVar.b()) {
            return textView.getText().toString().equals(njVar.a());
        }
        return true;
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        TextView textView = (TextView) view;
        nj njVar = (nj) obj;
        if (a.TEXT == njVar.b()) {
            textView.setText(njVar.a());
        }
    }

    public kv(@NonNull TextView textView) {
        super(textView);
    }
}
