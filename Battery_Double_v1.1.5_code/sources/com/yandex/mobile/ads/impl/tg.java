package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ru.a;
import java.io.UnsupportedEncodingException;

abstract class tg<R, T> extends rv<R, T> {
    /* access modifiers changed from: protected */
    public abstract T a(String str) throws Exception;

    private tg(String str, a<T> aVar, Context context, R r, gv<R, T> gvVar) {
        super(context, 0, str, aVar, r, gvVar);
    }

    tg(String str, a<T> aVar, Context context, R r, gv<R, T> gvVar, byte b) {
        this(str, aVar, context, r, gvVar);
    }

    /* access modifiers changed from: protected */
    public final qt<T> a(@NonNull qq qqVar, int i) {
        String str;
        try {
            str = new String(qqVar.b, ri.a(qqVar.c));
        } catch (UnsupportedEncodingException unused) {
            str = new String(qqVar.b);
        }
        try {
            return c(a(str));
        } catch (Exception e) {
            return a(e);
        }
    }

    /* access modifiers changed from: protected */
    public qt<T> a(Exception exc) {
        return qt.a(new sn((Throwable) exc));
    }

    /* access modifiers changed from: protected */
    public qt<T> c(T t) {
        return qt.a(t, null);
    }
}
