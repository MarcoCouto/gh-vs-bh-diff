package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class as {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5378a;
    /* access modifiers changed from: private */
    @NonNull
    public final b b = new b(Looper.getMainLooper());
    @NonNull
    private final c c;
    /* access modifiers changed from: private */
    @NonNull
    public final String d;
    /* access modifiers changed from: private */
    @NonNull
    public final List<a> e;
    /* access modifiers changed from: private */
    @NonNull
    public final List<a> f;
    @NonNull
    private final cj g;
    @NonNull
    private final cv h;
    @Nullable
    private aq i;
    private boolean j;
    private int k;
    private boolean l;
    private boolean m;

    @VisibleForTesting
    static class a {

        /* renamed from: a reason: collision with root package name */
        String f5379a;
        long b;
        int c;

        a(String str, long j, int i) {
            this.f5379a = str;
            this.b = j;
            this.c = i;
        }
    }

    private static class b extends Handler {
        b(Looper looper) {
            super(looper);
        }

        public final void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Pair pair = (Pair) message.obj;
                    as asVar = (as) ((WeakReference) pair.first).get();
                    if (asVar != null) {
                        a aVar = (a) pair.second;
                        new StringBuilder("handleMessage, clazz = ").append(asVar.d);
                        asVar.e.remove(aVar);
                        am a2 = asVar.a(aVar);
                        asVar.b(aVar, a2);
                        if (as.c(a2)) {
                            asVar.f.remove(aVar);
                            if (asVar.f.isEmpty()) {
                                asVar.a(a2.c());
                                return;
                            }
                        } else {
                            asVar.a();
                        }
                    }
                    return;
                case 2:
                    as asVar2 = (as) ((WeakReference) message.obj).get();
                    if (asVar2 != null) {
                        StringBuilder sb = new StringBuilder("mNoticeTrackingChecker mNotTrackedNotices.size = ");
                        sb.append(asVar2.f.size());
                        sb.append(", clazz = ");
                        sb.append(asVar2.d);
                        int size = asVar2.f.size();
                        for (int i = 0; i < size; i++) {
                            a aVar2 = (a) asVar2.f.get(i);
                            if (!asVar2.e.contains(aVar2)) {
                                am a3 = asVar2.a(aVar2);
                                if (as.c(a3)) {
                                    asVar2.b.sendMessageDelayed(Message.obtain(asVar2.b, 1, new Pair(new WeakReference(asVar2), aVar2)), aVar2.b);
                                    asVar2.e.add(aVar2);
                                    asVar2.b(a3.d());
                                } else {
                                    asVar2.a(a3);
                                }
                            }
                        }
                        if (asVar2.d()) {
                            asVar2.b.sendMessageDelayed(Message.obtain(asVar2.b, 2, new WeakReference(asVar2)), 300);
                            break;
                        }
                    }
                    break;
            }
        }
    }

    public interface c {
        @NonNull
        am a(int i);
    }

    public as(@NonNull Context context, @NonNull fc fcVar, @NonNull cj cjVar, @NonNull c cVar, @NonNull String str) {
        this.f5378a = context;
        this.c = cVar;
        this.g = cjVar;
        this.d = str;
        this.e = new ArrayList();
        this.f = new ArrayList();
        this.h = new cv(context, fcVar);
    }

    public final synchronized void a(@NonNull x xVar, @NonNull List<bg> list) {
        new StringBuilder("updateNotices(), clazz = ").append(this.d);
        this.g.a(xVar);
        this.f.clear();
        this.k = 0;
        this.j = false;
        this.l = false;
        this.m = false;
        b();
        a(list);
    }

    @VisibleForTesting
    private synchronized void a(List<bg> list) {
        for (bg bgVar : list) {
            this.f.add(new a(bgVar.b(), bgVar.a(), bgVar.c()));
        }
    }

    public final synchronized void a() {
        new StringBuilder("startTrackingIfNeeded(), clazz = ").append(this.d);
        if (ag.a().a(this.f5378a)) {
            if (!hc.a(this.f) && d()) {
                this.b.sendMessage(Message.obtain(this.b, 2, new WeakReference(this)));
            }
        }
    }

    public final synchronized void b() {
        new StringBuilder("stopTracking(), clazz = ").append(this.d);
        this.b.removeMessages(2);
        this.b.removeMessages(1);
        this.e.clear();
    }

    /* access modifiers changed from: private */
    public synchronized boolean d() {
        return this.f.size() > this.e.size();
    }

    public final synchronized void c() {
        StringBuilder sb = new StringBuilder("forceTracking(), mNotTrackedNotices.size = ");
        sb.append(this.f.size());
        sb.append(", clazz = ");
        sb.append(this.d);
        b();
        com.yandex.mobile.ads.impl.gu.b bVar = com.yandex.mobile.ads.impl.gu.b.IMPRESSION_TRACKING_SUCCESS;
        ArrayList arrayList = new ArrayList();
        this.m = false;
        for (a aVar : this.f) {
            am a2 = a(aVar);
            a(aVar, a2);
            if (c(a2)) {
                arrayList.add(aVar);
                bVar = a2.c();
                b(a2.d());
            }
        }
        if (!arrayList.isEmpty()) {
            this.f.removeAll(arrayList);
            if (this.f.isEmpty()) {
                a(bVar);
            }
        }
        a();
    }

    private synchronized void a(@NonNull a aVar, @NonNull am amVar) {
        b(aVar, amVar);
        if (!this.m && !c(amVar)) {
            this.g.b(amVar);
            this.m = true;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(@NonNull a aVar, @NonNull am amVar) {
        if (c(amVar)) {
            this.h.a(aVar.f5379a);
        } else {
            a(amVar);
        }
    }

    /* access modifiers changed from: private */
    public static boolean c(@NonNull am amVar) {
        return amVar.b() == com.yandex.mobile.ads.impl.am.a.SUCCESS;
    }

    /* access modifiers changed from: private */
    @NonNull
    public am a(@NonNull a aVar) {
        am a2 = this.c.a(aVar.c);
        new StringBuilder("validateTrackingState(), validationResult = ").append(a2.b().a());
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0063, code lost:
        return;
     */
    public final synchronized void a(@NonNull Intent intent, boolean z) {
        StringBuilder sb = new StringBuilder("handleIntent(), intent = ");
        sb.append(intent);
        sb.append(", isAdVisible = ");
        sb.append(z);
        sb.append(", clazz = ");
        sb.append(this.d);
        String action = intent.getAction();
        char c2 = 65535;
        int hashCode = action.hashCode();
        if (hashCode != -2128145023) {
            if (hashCode != -1454123155) {
                if (hashCode == 823795052) {
                    if (action.equals("android.intent.action.USER_PRESENT")) {
                        c2 = 2;
                    }
                }
            } else if (action.equals("android.intent.action.SCREEN_ON")) {
                c2 = 1;
            }
        } else if (action.equals("android.intent.action.SCREEN_OFF")) {
            c2 = 0;
        }
        switch (c2) {
            case 0:
                b();
                return;
            case 1:
            case 2:
                if (z) {
                    a();
                    break;
                }
                break;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void a(com.yandex.mobile.ads.impl.gu.b bVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("failure_tracked", Boolean.valueOf(this.j));
        this.g.a(bVar, hashMap);
        if (this.i != null) {
            this.i.a();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void a(am amVar) {
        this.k++;
        if (this.k == 20) {
            this.g.a(amVar);
            this.j = true;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void b(@NonNull com.yandex.mobile.ads.impl.gu.b bVar) {
        if (!this.l) {
            this.g.a(bVar);
            hi.a("Ad binding successful", new Object[0]);
            this.l = true;
        }
    }

    public final void a(@NonNull aq aqVar) {
        this.i = aqVar;
    }
}
