package com.yandex.mobile.ads.impl;

import android.text.TextUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class se {
    public static void a(Object obj, String str) {
        if (obj == null) {
            c(str);
        }
    }

    public static void a(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            c(str2);
        }
    }

    private static void c(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" not set!");
        throw new IllegalArgumentException(sb.toString());
    }

    public static Long a(String str) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
            return Long.valueOf(simpleDateFormat.parse(str).getTime() - simpleDateFormat.parse("00:00:00").getTime());
        } catch (ParseException unused) {
            return null;
        }
    }

    public static Integer b(String str) {
        try {
            return Integer.valueOf(str);
        } catch (NumberFormatException unused) {
            return null;
        }
    }
}
