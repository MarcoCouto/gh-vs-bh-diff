package com.yandex.mobile.ads.impl;

import android.os.SystemClock;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class rh implements qk {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, a> f5731a = new LinkedHashMap(16, 0.75f, true);
    private long b = 0;
    private final File c;
    private final int d;

    static class a {

        /* renamed from: a reason: collision with root package name */
        public long f5732a;
        public String b;
        public String c;
        public long d;
        public long e;
        public long f;
        public long g;
        public Map<String, String> h;

        private a() {
        }

        public a(String str, com.yandex.mobile.ads.impl.qk.a aVar) {
            this.b = str;
            this.f5732a = (long) aVar.f5712a.length;
            this.c = aVar.b;
            this.d = aVar.c;
            this.e = aVar.d;
            this.f = aVar.e;
            this.g = aVar.f;
            this.h = aVar.g;
        }

        public static a a(InputStream inputStream) throws IOException {
            a aVar = new a();
            if (rh.a(inputStream) == 538247942) {
                aVar.b = rh.c(inputStream);
                aVar.c = rh.c(inputStream);
                if (aVar.c.equals("")) {
                    aVar.c = null;
                }
                aVar.d = rh.b(inputStream);
                aVar.e = rh.b(inputStream);
                aVar.f = rh.b(inputStream);
                aVar.g = rh.b(inputStream);
                aVar.h = rh.d(inputStream);
                return aVar;
            }
            throw new IOException();
        }

        public final boolean a(OutputStream outputStream) {
            try {
                rh.a(outputStream, 538247942);
                rh.a(outputStream, this.b);
                rh.a(outputStream, this.c == null ? "" : this.c);
                rh.a(outputStream, this.d);
                rh.a(outputStream, this.e);
                rh.a(outputStream, this.f);
                rh.a(outputStream, this.g);
                Map<String, String> map = this.h;
                if (map != null) {
                    rh.a(outputStream, map.size());
                    for (Entry entry : map.entrySet()) {
                        rh.a(outputStream, (String) entry.getKey());
                        rh.a(outputStream, (String) entry.getValue());
                    }
                } else {
                    rh.a(outputStream, 0);
                }
                outputStream.flush();
                return true;
            } catch (IOException e2) {
                qw.b("%s", e2.toString());
                return false;
            }
        }
    }

    private static class b extends FilterInputStream {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public int f5733a;

        /* synthetic */ b(InputStream inputStream, byte b) {
            this(inputStream);
        }

        private b(InputStream inputStream) {
            super(inputStream);
            this.f5733a = 0;
        }

        public final int read() throws IOException {
            int read = super.read();
            if (read != -1) {
                this.f5733a++;
            }
            return read;
        }

        public final int read(byte[] bArr, int i, int i2) throws IOException {
            int read = super.read(bArr, i, i2);
            if (read != -1) {
                this.f5733a += read;
            }
            return read;
        }
    }

    public rh(File file, int i) {
        this.c = file;
        this.d = i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x007f A[SYNTHETIC, Splitter:B:32:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00a1 A[SYNTHETIC, Splitter:B:45:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c3 A[SYNTHETIC, Splitter:B:58:0x00c3] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ce A[SYNTHETIC, Splitter:B:67:0x00ce] */
    public final synchronized com.yandex.mobile.ads.impl.qk.a a(String str) {
        b bVar;
        a aVar = (a) this.f5731a.get(str);
        if (aVar == null) {
            return null;
        }
        File d2 = d(str);
        try {
            bVar = new b(new BufferedInputStream(new FileInputStream(d2)), 0);
            try {
                a.a((InputStream) bVar);
                byte[] a2 = a((InputStream) bVar, (int) (d2.length() - ((long) bVar.f5733a)));
                com.yandex.mobile.ads.impl.qk.a aVar2 = new com.yandex.mobile.ads.impl.qk.a();
                aVar2.f5712a = a2;
                aVar2.b = aVar.c;
                aVar2.c = aVar.d;
                aVar2.d = aVar.e;
                aVar2.e = aVar.f;
                aVar2.f = aVar.g;
                aVar2.g = aVar.h;
                try {
                    bVar.close();
                    return aVar2;
                } catch (IOException unused) {
                    return null;
                }
            } catch (IOException e) {
                e = e;
                qw.b("%s: %s", d2.getAbsolutePath(), e.toString());
                b(str);
                if (bVar != null) {
                }
                return null;
            } catch (NegativeArraySizeException e2) {
                e = e2;
                qw.b("%s: %s", d2.getAbsolutePath(), e.toString());
                b(str);
                if (bVar != null) {
                }
                return null;
            } catch (Throwable th) {
                th = th;
                try {
                    qw.b("%s: %s", d2.getAbsolutePath(), th.toString());
                    b(str);
                    if (bVar != null) {
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    if (bVar != null) {
                        try {
                            bVar.close();
                        } catch (IOException unused2) {
                            return null;
                        }
                    }
                    throw th;
                }
            }
        } catch (IOException e3) {
            e = e3;
            bVar = null;
            qw.b("%s: %s", d2.getAbsolutePath(), e.toString());
            b(str);
            if (bVar != null) {
                try {
                    bVar.close();
                } catch (IOException unused3) {
                    return null;
                }
            }
            return null;
        } catch (NegativeArraySizeException e4) {
            e = e4;
            bVar = null;
            qw.b("%s: %s", d2.getAbsolutePath(), e.toString());
            b(str);
            if (bVar != null) {
                try {
                    bVar.close();
                } catch (IOException unused4) {
                    return null;
                }
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            bVar = null;
            if (bVar != null) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:25|26|(2:35|36)|37|38) */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0063 */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005a A[SYNTHETIC, Splitter:B:32:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0066 A[SYNTHETIC, Splitter:B:40:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0069 A[SYNTHETIC] */
    public final synchronized void a() {
        if (this.c.exists()) {
            File[] listFiles = this.c.listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    BufferedInputStream bufferedInputStream = null;
                    try {
                        BufferedInputStream bufferedInputStream2 = new BufferedInputStream(new FileInputStream(file));
                        try {
                            a a2 = a.a((InputStream) bufferedInputStream2);
                            a2.f5732a = file.length();
                            a(a2.b, a2);
                        } catch (Throwable th) {
                            th = th;
                            bufferedInputStream = bufferedInputStream2;
                            if (bufferedInputStream != null) {
                                bufferedInputStream.close();
                            }
                            throw th;
                        }
                        try {
                            bufferedInputStream2.close();
                        } catch (IOException unused) {
                        }
                    } catch (Throwable unused2) {
                        if (file != null) {
                        }
                        if (bufferedInputStream == null) {
                        }
                    }
                }
                return;
            }
            return;
        } else if (!this.c.mkdirs()) {
            qw.c("Unable to create cache dir %s", this.c.getAbsolutePath());
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:32|33|(1:35)|36|37) */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00f5, code lost:
        if (r3.delete() == false) goto L_0x00f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00f7, code lost:
        com.yandex.mobile.ads.impl.qw.b("Could not clean up file %s", r3.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0107, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x00f1 */
    public final synchronized void a(String str, com.yandex.mobile.ads.impl.qk.a aVar) {
        String str2 = str;
        com.yandex.mobile.ads.impl.qk.a aVar2 = aVar;
        synchronized (this) {
            long length = (long) aVar2.f5712a.length;
            if (this.b + length >= ((long) this.d)) {
                if (qw.b) {
                    qw.a("Pruning old cache entries.", new Object[0]);
                }
                long j = this.b;
                long elapsedRealtime = SystemClock.elapsedRealtime();
                Iterator it = this.f5731a.entrySet().iterator();
                int i = 0;
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    a aVar3 = (a) ((Entry) it.next()).getValue();
                    if (d(aVar3.b).delete()) {
                        this.b -= aVar3.f5732a;
                    } else {
                        qw.b("Could not delete cache entry for key=%s, filename=%s", aVar3.b, c(aVar3.b));
                    }
                    it.remove();
                    i++;
                    if (((float) (this.b + length)) < ((float) this.d) * 0.9f) {
                        break;
                    }
                }
                if (qw.b) {
                    qw.a("pruned %d files, %d bytes, %d ms", Integer.valueOf(i), Long.valueOf(this.b - j), Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
                }
            }
            File d2 = d(str);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(d2));
            a aVar4 = new a(str2, aVar2);
            if (aVar4.a((OutputStream) bufferedOutputStream)) {
                bufferedOutputStream.write(aVar2.f5712a);
                bufferedOutputStream.close();
                a(str2, aVar4);
                return;
            }
            bufferedOutputStream.close();
            qw.b("Failed to write header for %s", d2.getAbsolutePath());
            throw new IOException();
        }
    }

    private synchronized void b(String str) {
        boolean delete = d(str).delete();
        a aVar = (a) this.f5731a.get(str);
        if (aVar != null) {
            this.b -= aVar.f5732a;
            this.f5731a.remove(str);
        }
        if (!delete) {
            qw.b("Could not delete cache entry for key=%s, filename=%s", str, c(str));
        }
    }

    private static String c(String str) {
        int length = str.length() / 2;
        String valueOf = String.valueOf(str.substring(0, length).hashCode());
        StringBuilder sb = new StringBuilder();
        sb.append(valueOf);
        sb.append(String.valueOf(str.substring(length).hashCode()));
        return sb.toString();
    }

    private File d(String str) {
        return new File(this.c, c(str));
    }

    private void a(String str, a aVar) {
        if (!this.f5731a.containsKey(str)) {
            this.b += aVar.f5732a;
        } else {
            this.b += aVar.f5732a - ((a) this.f5731a.get(str)).f5732a;
        }
        this.f5731a.put(str, aVar);
    }

    private static byte[] a(InputStream inputStream, int i) throws IOException {
        byte[] bArr = new byte[i];
        int i2 = 0;
        while (i2 < i) {
            int read = inputStream.read(bArr, i2, i - i2);
            if (read == -1) {
                break;
            }
            i2 += read;
        }
        if (i2 == i) {
            return bArr;
        }
        StringBuilder sb = new StringBuilder("Expected ");
        sb.append(i);
        sb.append(" bytes, read ");
        sb.append(i2);
        sb.append(" bytes");
        throw new IOException(sb.toString());
    }

    private static int e(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }

    static void a(OutputStream outputStream, int i) throws IOException {
        outputStream.write((i >> 0) & 255);
        outputStream.write((i >> 8) & 255);
        outputStream.write((i >> 16) & 255);
        outputStream.write((i >> 24) & 255);
    }

    static int a(InputStream inputStream) throws IOException {
        return (e(inputStream) << 24) | (e(inputStream) << 0) | 0 | (e(inputStream) << 8) | (e(inputStream) << 16);
    }

    static void a(OutputStream outputStream, long j) throws IOException {
        outputStream.write((byte) ((int) (j >>> 0)));
        outputStream.write((byte) ((int) (j >>> 8)));
        outputStream.write((byte) ((int) (j >>> 16)));
        outputStream.write((byte) ((int) (j >>> 24)));
        outputStream.write((byte) ((int) (j >>> 32)));
        outputStream.write((byte) ((int) (j >>> 40)));
        outputStream.write((byte) ((int) (j >>> 48)));
        outputStream.write((byte) ((int) (j >>> 56)));
    }

    static long b(InputStream inputStream) throws IOException {
        return ((((long) e(inputStream)) & 255) << 0) | 0 | ((((long) e(inputStream)) & 255) << 8) | ((((long) e(inputStream)) & 255) << 16) | ((((long) e(inputStream)) & 255) << 24) | ((((long) e(inputStream)) & 255) << 32) | ((((long) e(inputStream)) & 255) << 40) | ((((long) e(inputStream)) & 255) << 48) | ((255 & ((long) e(inputStream))) << 56);
    }

    static void a(OutputStream outputStream, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        a(outputStream, (long) bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }

    static String c(InputStream inputStream) throws IOException {
        return new String(a(inputStream, (int) b(inputStream)), "UTF-8");
    }

    static Map<String, String> d(InputStream inputStream) throws IOException {
        int a2 = a(inputStream);
        Map<String, String> emptyMap = a2 == 0 ? Collections.emptyMap() : new HashMap<>(a2);
        for (int i = 0; i < a2; i++) {
            emptyMap.put(c(inputStream).intern(), c(inputStream).intern());
        }
        return emptyMap;
    }
}
