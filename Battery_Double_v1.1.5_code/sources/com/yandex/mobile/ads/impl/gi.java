package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class gi {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5510a;

    public gi(@NonNull Context context) {
        this.f5510a = context.getApplicationContext();
    }

    public final boolean a() {
        return a("android.permission.ACCESS_COARSE_LOCATION");
    }

    public final boolean b() {
        return a("android.permission.ACCESS_FINE_LOCATION");
    }

    private boolean a(@NonNull String str) {
        try {
            return this.f5510a.checkCallingOrSelfPermission(str) == 0;
        } catch (Throwable unused) {
            return false;
        }
    }
}
