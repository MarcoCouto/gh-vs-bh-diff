package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class ls implements lp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final View f5610a;

    public ls(@NonNull View view) {
        this.f5610a = view;
    }

    public final void a(@NonNull nm nmVar, @NonNull lq lqVar) {
        this.f5610a.setOnClickListener(lqVar.a(nmVar));
        this.f5610a.setOnTouchListener(lw.a(this.f5610a.getContext()));
    }
}
