package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class sk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final sj f5770a;
    @Nullable
    private final String b;

    public sk(@NonNull sj sjVar, @Nullable String str) {
        this.f5770a = sjVar;
        this.b = str;
    }

    @NonNull
    public final sj a() {
        return this.f5770a;
    }

    @Nullable
    public final String b() {
        return this.b;
    }
}
