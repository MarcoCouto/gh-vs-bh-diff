package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.b;

public final class fc {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private al f5482a;
    @NonNull
    private final b b;
    @Nullable
    private AdRequest c;
    @Nullable
    private af d;
    @Nullable
    private ae e;
    @Nullable
    private String f;
    @Nullable
    private String g;
    @Nullable
    private String h;
    @Nullable
    private String i;
    @Nullable
    private String j;
    @Nullable
    private fu k;
    @Nullable
    private String[] l;
    @Nullable
    private int m;
    @Nullable
    private String[] n;
    @Nullable
    private String o;
    private boolean p;
    private boolean q;
    private int r = 1;
    private int s = gq.b;

    public fc(@NonNull b bVar) {
        this.b = bVar;
    }

    @NonNull
    public final b a() {
        return this.b;
    }

    @Nullable
    public final al b() {
        return this.f5482a;
    }

    public final void a(@Nullable al alVar) {
        if (alVar == null) {
            throw new IllegalArgumentException("Ad size can't be null or empty.");
        } else if (this.f5482a == null) {
            this.f5482a = alVar;
        } else {
            throw new IllegalArgumentException("Ad size can't be set twice.");
        }
    }

    @Nullable
    public final AdRequest c() {
        return this.c;
    }

    public final void a(@Nullable AdRequest adRequest) {
        this.c = adRequest;
    }

    @Nullable
    public final synchronized String d() {
        return this.f;
    }

    public final synchronized void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f = str;
        }
    }

    @Nullable
    public final String e() {
        return this.g;
    }

    @Nullable
    public final synchronized String f() {
        return this.h;
    }

    public final synchronized void c(@Nullable String str) {
        this.h = str;
    }

    public final synchronized void d(@Nullable String str) {
        this.i = str;
    }

    @Nullable
    public final synchronized String g() {
        return this.i;
    }

    @Nullable
    public final String h() {
        return this.j;
    }

    @Nullable
    public final fu i() {
        return this.k;
    }

    public final synchronized void a(@Nullable fu fuVar) {
        this.k = fuVar;
    }

    public final boolean j() {
        return this.p;
    }

    public final void a(boolean z) {
        this.p = z;
    }

    public final int k() {
        return this.r;
    }

    public final int l() {
        return this.s;
    }

    public final boolean m() {
        return !TextUtils.isEmpty(this.g);
    }

    public final void a(@NonNull String[] strArr) {
        this.l = strArr;
    }

    @Nullable
    public final String[] n() {
        return this.l;
    }

    public final void a(@NonNull ae aeVar) {
        this.e = aeVar;
    }

    @Nullable
    public final ae o() {
        return this.e;
    }

    public final void a(@NonNull af afVar) {
        this.d = afVar;
    }

    @Nullable
    public final af p() {
        return this.d;
    }

    public final void a(@NonNull int i2) {
        this.m = i2;
    }

    @Nullable
    public final int q() {
        return this.m;
    }

    public final void b(@NonNull String[] strArr) {
        this.n = strArr;
    }

    @Nullable
    public final String[] r() {
        return this.n;
    }

    @Nullable
    public final String s() {
        return this.o;
    }

    public final void e(@Nullable String str) {
        this.o = str;
    }

    public final void b(boolean z) {
        this.q = z;
    }

    public final boolean t() {
        return this.q;
    }

    public final void b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Block ID can't be null or empty.");
        } else if (TextUtils.isEmpty(this.g)) {
            this.g = str;
        } else {
            throw new IllegalArgumentException("Block ID can't be set twice.");
        }
    }
}
