package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.interstitial.e;
import com.yandex.mobile.ads.rewarded.b;

final class jq implements jn {
    jq() {
    }

    @NonNull
    public final jm a(@NonNull hm hmVar) {
        return new e(hmVar);
    }

    @NonNull
    public final jm a(@NonNull b bVar) {
        return new com.yandex.mobile.ads.mediation.rewarded.e(bVar);
    }
}
