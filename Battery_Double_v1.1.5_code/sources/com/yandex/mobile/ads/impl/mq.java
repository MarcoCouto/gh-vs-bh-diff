package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.bc;
import java.util.List;

final class mq {
    mq() {
    }

    @NonNull
    static mp a(@NonNull bc bcVar, @NonNull List<bg> list) {
        int i;
        if (list.isEmpty()) {
            i = 50;
        } else {
            int c = ((bg) list.get(0)).c();
            for (bg c2 : list) {
                c = Math.max(c, c2.c());
            }
            i = c;
        }
        return new mp(bcVar, i);
    }
}
