package com.yandex.mobile.ads.impl;

import android.os.Process;
import com.yandex.mobile.ads.impl.qk.a;
import java.util.concurrent.BlockingQueue;

public final class ql extends Thread {

    /* renamed from: a reason: collision with root package name */
    private static final boolean f5713a = qw.b;
    private final BlockingQueue<qr<?>> b;
    /* access modifiers changed from: private */
    public final BlockingQueue<qr<?>> c;
    private final qk d;
    private final qu e;
    private volatile boolean f = false;

    public ql(BlockingQueue<qr<?>> blockingQueue, BlockingQueue<qr<?>> blockingQueue2, qk qkVar, qu quVar) {
        this.b = blockingQueue;
        this.c = blockingQueue2;
        this.d = qkVar;
        this.e = quVar;
    }

    public final void a() {
        this.f = true;
        interrupt();
    }

    public final void run() {
        if (f5713a) {
            qw.a("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.d.a();
        while (true) {
            try {
                final qr qrVar = (qr) this.b.take();
                if (qrVar.j()) {
                    qrVar.g();
                } else {
                    a a2 = this.d.a(qrVar.b());
                    if (a2 == null) {
                        this.c.put(qrVar);
                    } else {
                        if (a2.e < System.currentTimeMillis()) {
                            qrVar.a(a2);
                            this.c.put(qrVar);
                        } else {
                            qt a3 = qrVar.a(new qq(a2.f5712a, a2.g));
                            if (!(a2.f < System.currentTimeMillis())) {
                                this.e.a(qrVar, a3);
                            } else {
                                qrVar.a(a2);
                                a3.d = true;
                                this.e.a(qrVar, a3, new Runnable() {
                                    public final void run() {
                                        try {
                                            ql.this.c.put(qrVar);
                                        } catch (InterruptedException unused) {
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            } catch (InterruptedException unused) {
                if (this.f) {
                    return;
                }
            }
        }
    }
}
