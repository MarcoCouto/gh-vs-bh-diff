package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.banner.b;
import com.yandex.mobile.ads.mediation.banner.e;

final class jk implements jj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final be f5579a;

    jk(@NonNull be beVar) {
        this.f5579a = beVar;
    }

    @NonNull
    public final b a(@NonNull a aVar) {
        return new e(aVar, this.f5579a);
    }
}
