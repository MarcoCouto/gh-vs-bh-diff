package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IIdentifierCallback.Reason;
import com.yandex.metrica.p;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class fp implements IIdentifierCallback, fr {

    /* renamed from: a reason: collision with root package name */
    private static final long f5494a = ((long) gq.b);
    private static final Object b = new Object();
    private static volatile fr c;
    @NonNull
    private final Context d;
    @NonNull
    private final fo e = new fo();
    @NonNull
    private final WeakHashMap<fq, Object> f = new WeakHashMap<>();
    @NonNull
    private final Handler g = new Handler(Looper.getMainLooper());
    @NonNull
    private final ft h = new ft();
    @Nullable
    private Map<String, String> i;
    private boolean j;

    private fp(@NonNull Context context) {
        this.d = context.getApplicationContext();
        gs.a(context);
    }

    @NonNull
    public static fr a(@NonNull Context context) {
        if (c == null) {
            synchronized (b) {
                if (c == null) {
                    c = new fp(context.getApplicationContext());
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:19|20) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:14|15|16|17) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        onRequestError(com.yandex.metrica.IIdentifierCallback.Reason.UNKNOWN);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0046 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004e */
    public final void a(@NonNull fq fqVar) {
        synchronized (b) {
            if (this.i == null || !ft.a(this.i)) {
                this.f.put(fqVar, null);
                if (!this.j) {
                    this.j = true;
                    this.g.postDelayed(new Runnable() {
                        public final void run() {
                            fp.this.onRequestError(Reason.UNKNOWN);
                        }
                    }, f5494a);
                    Context context = this.d;
                    if (he.b(p.class, "a", context, this)) {
                        p.a(context, (IIdentifierCallback) this);
                        p.a(this);
                    } else {
                        p.a(this);
                    }
                }
            } else {
                fqVar.a(this.i);
            }
        }
    }

    public final void b(@NonNull fq fqVar) {
        synchronized (b) {
            this.f.remove(fqVar);
        }
    }

    public final void onReceive(@Nullable Map<String, String> map) {
        synchronized (b) {
            if (map != null) {
                try {
                    if (ft.a(map)) {
                        this.i = new HashMap(map);
                        a(this.i);
                    }
                } finally {
                }
            }
            onRequestError(Reason.INVALID_RESPONSE);
        }
    }

    private void a(@NonNull Map<String, String> map) {
        synchronized (b) {
            a();
            for (fq a2 : this.f.keySet()) {
                a2.a(map);
            }
            this.f.clear();
        }
    }

    public final void onRequestError(@NonNull Reason reason) {
        synchronized (b) {
            a();
            for (fq a2 : this.f.keySet()) {
                a2.a();
            }
            this.f.clear();
        }
    }

    private void a() {
        this.g.removeCallbacksAndMessages(null);
        this.j = false;
    }
}
