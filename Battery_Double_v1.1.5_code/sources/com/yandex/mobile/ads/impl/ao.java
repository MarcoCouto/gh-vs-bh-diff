package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.regex.Pattern;

public final class ao implements Comparable<ao> {

    /* renamed from: a reason: collision with root package name */
    private static final Pattern f5375a = Pattern.compile("^[\\d]*(\\.[\\d]*)*(\\-.*)?");
    private final String b;
    private int[] c;
    private String d;
    private Integer e;
    private boolean f = true;

    public ao(String str) {
        this.b = str.toLowerCase();
        a();
    }

    private int a(int i) {
        if (i >= this.c.length) {
            return 0;
        }
        return this.c[i];
    }

    /* renamed from: a */
    public final int compareTo(@NonNull ao aoVar) {
        int i = -1;
        if (this.f && aoVar.f) {
            int max = Math.max(this.c.length, aoVar.c.length);
            int i2 = 0;
            while (true) {
                if (i2 >= max) {
                    i = 0;
                    break;
                }
                int a2 = a(i2);
                int a3 = aoVar.a(i2);
                if (a2 > a3) {
                    i = 1;
                    break;
                } else if (a2 < a3) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i != 0) {
                return i;
            }
            if (!this.e.equals(aoVar.e)) {
                return this.e.compareTo(aoVar.e);
            }
            if (this.e.equals(Integer.valueOf(2))) {
                return 0;
            }
            return a(this.d, aoVar.d);
        } else if (this.f) {
            return 1;
        } else {
            if (aoVar.f) {
                return -1;
            }
            return a(this.b, aoVar.b);
        }
    }

    private static int a(String str, String str2) {
        int compareTo = str.compareTo(str2);
        if (compareTo < 0) {
            return -1;
        }
        return compareTo > 0 ? 1 : 0;
    }

    private void a() {
        String[] split = this.b.split("-");
        if (!f5375a.matcher(this.b).matches()) {
            this.f = false;
        }
        String[] split2 = split[0].split("\\.");
        if (this.f) {
            this.c = new int[split2.length];
            for (int i = 0; i < this.c.length; i++) {
                this.c[i] = Integer.parseInt(split2[i]);
            }
            int indexOf = this.b.indexOf(45);
            if (indexOf < 0 || indexOf >= this.b.length() - 1) {
                this.e = Integer.valueOf(2);
            } else {
                this.d = this.b.substring(indexOf);
                this.e = Integer.valueOf(this.d.endsWith("-snapshot") ^ true ? 1 : 0);
            }
        }
    }
}
