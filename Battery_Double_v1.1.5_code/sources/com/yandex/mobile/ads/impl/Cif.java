package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

/* renamed from: com.yandex.mobile.ads.impl.if reason: invalid class name */
public final class Cif extends ih {
    public Cif(@NonNull eg egVar, @NonNull ec ecVar, @NonNull dy dyVar, @NonNull ii iiVar) {
        super(egVar, ecVar);
        this.f5547a.a(dyVar);
        this.f5547a.a(iiVar);
    }
}
