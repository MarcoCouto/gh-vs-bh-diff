package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.lang.ref.WeakReference;

public final class da implements db {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<Context> f5433a;
    @Nullable
    private final ResultReceiver b;
    private final boolean c;

    public da(@Nullable Context context, boolean z, @Nullable ResultReceiver resultReceiver) {
        this.f5433a = new WeakReference<>(context);
        this.c = z;
        this.b = resultReceiver;
    }

    public final void a(@NonNull cg cgVar, @Nullable String str) {
        p.a((Context) this.f5433a.get(), cgVar, str, this.b, this.c);
    }
}
