package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

final class lo implements lk {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5607a = new Object();
    private static volatile lo b;

    public final void a(@NonNull Context context, @NonNull ln lnVar) {
    }

    public final void b(@NonNull Context context, @NonNull ln lnVar) {
    }

    lo() {
    }

    @NonNull
    public static lo a() {
        if (b == null) {
            synchronized (f5607a) {
                if (b == null) {
                    b = new lo();
                }
            }
        }
        return b;
    }
}
