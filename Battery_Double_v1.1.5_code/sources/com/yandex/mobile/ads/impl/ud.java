package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.video.BlocksInfoRequest;
import com.yandex.mobile.ads.video.models.blocksinfo.BlocksInfo;
import java.util.HashMap;

public final class ud implements gv<BlocksInfoRequest, BlocksInfo> {
    public final /* synthetic */ gu a(@Nullable qt qtVar, int i, @NonNull Object obj) {
        BlocksInfoRequest blocksInfoRequest = (BlocksInfoRequest) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("partner_id", blocksInfoRequest.getPartnerId());
        hashMap.put("category_id", blocksInfoRequest.getCategoryId());
        if (i != -1) {
            hashMap.put("code", Integer.valueOf(i));
        }
        return new gu(b.BLOCKS_INFO_RESPONSE, hashMap);
    }

    public final /* synthetic */ gu a(Object obj) {
        BlocksInfoRequest blocksInfoRequest = (BlocksInfoRequest) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("partner_id", blocksInfoRequest.getPartnerId());
        hashMap.put("category_id", blocksInfoRequest.getCategoryId());
        return new gu(b.BLOCKS_INFO_REQUEST, hashMap);
    }
}
