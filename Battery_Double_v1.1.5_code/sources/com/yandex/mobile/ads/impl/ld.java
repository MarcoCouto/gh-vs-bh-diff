package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import java.lang.ref.WeakReference;

public abstract class ld<V extends View, T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<V> f5599a;

    public abstract boolean a(@NonNull V v, @NonNull T t);

    public abstract void b(@NonNull V v, @NonNull T t);

    public ld(@NonNull V v) {
        this.f5599a = new WeakReference<>(v);
    }

    public void a(@NonNull V v) {
        v.setVisibility(8);
        v.setOnClickListener(null);
        v.setOnTouchListener(null);
        v.setSelected(false);
    }

    @Nullable
    public final V a() {
        return (View) this.f5599a.get();
    }

    public void a(@NonNull ni niVar, @NonNull li liVar) {
        View a2 = a();
        if (a2 != null) {
            liVar.a(niVar, a2);
            liVar.a(niVar, (lp) new ls(a2));
        }
    }

    public final boolean b() {
        return a() != null;
    }

    public final boolean c() {
        View a2 = a();
        return a2 != null && !dv.d(a2);
    }

    public final boolean d() {
        return dv.a(a(), 100);
    }
}
