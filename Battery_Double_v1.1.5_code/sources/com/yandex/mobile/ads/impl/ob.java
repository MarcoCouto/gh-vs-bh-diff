package com.yandex.mobile.ads.impl;

import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.widget.ImageView.ScaleType;
import com.yandex.mobile.ads.impl.rl.b;
import com.yandex.mobile.ads.impl.rl.c;
import com.yandex.mobile.ads.impl.rl.d;

final class ob extends rl {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final oa f5665a;
    @NonNull
    private final oc b;

    @TargetApi(13)
    ob(@NonNull qs qsVar, @NonNull b bVar, @NonNull oa oaVar, @NonNull oc ocVar) {
        super(qsVar, bVar);
        this.f5665a = oaVar;
        this.b = ocVar;
    }

    public final c a(String str, d dVar, int i, int i2) {
        return super.a(str, dVar, this.b.a(i), i2);
    }

    public final String a(@NonNull String str, int i, int i2, @NonNull ScaleType scaleType) {
        return oa.a(str, scaleType);
    }
}
