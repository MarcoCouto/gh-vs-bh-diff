package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import com.yandex.mobile.ads.video.VmapError;

public final class sh implements VmapError {

    /* renamed from: a reason: collision with root package name */
    private final int f5765a;
    @Nullable
    private final String b;

    public sh(int i, @Nullable String str) {
        this.f5765a = i;
        this.b = str;
    }

    public final int getCode() {
        return this.f5765a;
    }

    @Nullable
    public final String getDescription() {
        return this.b;
    }
}
