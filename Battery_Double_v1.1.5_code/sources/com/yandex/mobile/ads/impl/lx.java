package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;

public final class lx implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final View f5616a;

    public lx(@Nullable View view) {
        this.f5616a = view;
    }

    public final void onClick(View view) {
        if (this.f5616a != null) {
            this.f5616a.setSelected(!this.f5616a.isSelected());
        }
    }
}
