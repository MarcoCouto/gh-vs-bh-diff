package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.mediation.base.a;

public interface aw<T extends a> {
    @Nullable
    au<T> a(@NonNull Context context);
}
