package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class gn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gl f5515a;
    @NonNull
    private final gi b;
    @Nullable
    private gm c;

    gn(@NonNull Context context) {
        this.f5515a = new gl(context);
        this.b = new gi(context);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final gm a() {
        return this.c != null ? this.c : c();
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.c = c();
    }

    @Nullable
    private gm c() {
        gk a2 = this.f5515a.a();
        if (a2 != null) {
            boolean a3 = this.b.a();
            boolean b2 = this.b.b();
            if (a3 || b2) {
                return a2.a();
            }
        }
        return null;
    }
}
