package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.RelativeLayout;
import java.util.HashMap;
import java.util.Map;

public final class n {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private static final Object f5639a = new Object();
    private static volatile n b;
    @NonNull
    private final Map<String, m> c = new HashMap();

    @NonNull
    public static n a() {
        if (b == null) {
            synchronized (f5639a) {
                if (b == null) {
                    b = new n();
                }
            }
        }
        return b;
    }

    private n() {
        a("window_type_browser", new k());
    }

    public final synchronized void a(@NonNull String str, @NonNull m mVar) {
        if (!this.c.containsKey(str)) {
            this.c.put(str, mVar);
        }
    }

    @Nullable
    public final synchronized l a(@NonNull Context context, @NonNull RelativeLayout relativeLayout, @Nullable ResultReceiver resultReceiver, @NonNull o oVar, @NonNull Intent intent, @NonNull Window window) {
        String stringExtra = intent.getStringExtra("window_type");
        if (stringExtra != null) {
            m mVar = (m) this.c.get(stringExtra);
            if (mVar != null) {
                return mVar.a(context, relativeLayout, resultReceiver, oVar, intent, window);
            }
        }
        return null;
    }
}
