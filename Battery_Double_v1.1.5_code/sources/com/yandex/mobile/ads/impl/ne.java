package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.nativeads.q;

final class ne implements nb<ns> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cg f5643a;
    @NonNull
    private final q b;

    ne(@NonNull cg cgVar, @NonNull q qVar) {
        this.f5643a = cgVar;
        this.b = qVar;
    }

    public final void a(@NonNull Context context, @NonNull ns nsVar) {
        this.b.g();
        this.f5643a.a(b.CLOSE);
    }
}
