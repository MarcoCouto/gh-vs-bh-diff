package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.tapjoy.TapjoyConstants;
import com.yandex.mobile.ads.MobileAds;

final class sy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5782a;
    @NonNull
    private final fe b = new fe();
    @NonNull
    private final fh c = new fh();
    @NonNull
    private final ge d;

    sy(@NonNull Context context, @NonNull fc fcVar) {
        this.f5782a = fcVar;
        this.d = ge.a(context);
    }

    private static void a(@NonNull Builder builder, @NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(str, str2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull Builder builder) {
        a(builder, "app_id", context.getPackageName());
        a(builder, "app_version_code", dj.a(context));
        a(builder, "app_version_name", dj.b(context));
        a(builder, "sdk_version", MobileAds.getLibraryVersion());
        a(builder, TapjoyConstants.TJC_DEVICE_TYPE_NAME, this.c.a(context));
        a(builder, "locale", fj.a(context));
        a(builder, "manufacturer", Build.MANUFACTURER);
        a(builder, "model", Build.MODEL);
        a(builder, "os_name", "android");
        a(builder, TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
        if (!fe.a(context)) {
            Location a2 = this.d.a();
            if (a2 != null) {
                a(builder, "lat", String.valueOf(a2.getLatitude()));
                a(builder, "lon", String.valueOf(a2.getLongitude()));
                a(builder, "precision", String.valueOf(a2.getAccuracy()));
            }
        }
        if (!fe.a(context)) {
            a(builder, "device-id", this.f5782a.g());
            fu i = this.f5782a.i();
            if (i != null) {
                Boolean valueOf = Boolean.valueOf(i.b());
                if (valueOf != null && !valueOf.booleanValue()) {
                    a(builder, "google_aid", i.a());
                }
            }
        }
    }
}
