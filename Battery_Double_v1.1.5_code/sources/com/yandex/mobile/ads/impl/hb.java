package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class hb {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gz f5526a = new gz();

    @NonNull
    public static String a(@Nullable Context context) {
        gy gyVar;
        if (context != null) {
            fg a2 = ff.a().a(context);
            if (a2 != null && a2.g()) {
                gyVar = new gw();
                return gyVar.a();
            }
        }
        gyVar = new gx();
        return gyVar.a();
    }
}
