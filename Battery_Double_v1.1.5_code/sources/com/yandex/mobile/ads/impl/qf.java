package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

final class qf implements qb<Drawable> {

    /* renamed from: a reason: collision with root package name */
    private final qe f5707a = new qe();

    private static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final int f5708a;
        /* access modifiers changed from: private */
        public final int b;
        /* access modifiers changed from: private */
        public final int c;
        /* access modifiers changed from: private */
        public final int d;

        a(int i) {
            this.f5708a = Color.alpha(i);
            this.b = Color.red(i);
            this.c = Color.green(i);
            this.d = Color.blue(i);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.f5708a == aVar.f5708a && this.b == aVar.b && this.c == aVar.c && this.d == aVar.d;
        }

        public final int hashCode() {
            return (((((this.f5708a * 31) + this.b) * 31) + this.c) * 31) + this.d;
        }
    }

    qf() {
    }

    public final boolean a(@NonNull Drawable drawable, @NonNull Bitmap bitmap) {
        Bitmap bitmap2;
        Bitmap bitmap3;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                bitmap2 = bitmapDrawable.getBitmap();
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap2, 1, 1, true);
                Bitmap createScaledBitmap2 = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
                a aVar = new a(createScaledBitmap.getPixel(0, 0));
                a aVar2 = new a(createScaledBitmap2.getPixel(0, 0));
                return Math.abs(aVar.f5708a - aVar2.f5708a) > 20 && Math.abs(aVar.b - aVar2.b) <= 20 && Math.abs(aVar.c - aVar2.c) <= 20 && Math.abs(aVar.d - aVar2.d) <= 20;
            }
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (intrinsicWidth <= 0 || intrinsicHeight <= 0) {
            bitmap3 = Bitmap.createBitmap(1, 1, Config.ARGB_8888);
        } else {
            bitmap3 = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(bitmap3);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        bitmap2 = bitmap3;
        Bitmap createScaledBitmap3 = Bitmap.createScaledBitmap(bitmap2, 1, 1, true);
        Bitmap createScaledBitmap22 = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        a aVar3 = new a(createScaledBitmap3.getPixel(0, 0));
        a aVar22 = new a(createScaledBitmap22.getPixel(0, 0));
        if (Math.abs(aVar3.f5708a - aVar22.f5708a) > 20) {
        }
    }
}
