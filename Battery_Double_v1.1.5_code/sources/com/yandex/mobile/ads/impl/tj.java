package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.ru.a;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class tj extends rv<VmapRequestConfiguration, Vmap> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tw f5788a = new tw();
    @NonNull
    private final bm b = new bm();

    public tj(@NonNull Context context, @NonNull String str, @NonNull a<Vmap> aVar, @NonNull VmapRequestConfiguration vmapRequestConfiguration, @NonNull gv<VmapRequestConfiguration, Vmap> gvVar) {
        super(context, 0, str, aVar, vmapRequestConfiguration, gvVar);
        new Object[1][0] = str;
    }

    /* access modifiers changed from: protected */
    public final qt<Vmap> a(@NonNull qq qqVar, int i) {
        String a2 = bm.a(qqVar);
        if (!TextUtils.isEmpty(a2)) {
            try {
                Vmap a3 = this.f5788a.a(a2);
                if (a3 != null) {
                    return qt.a(a3, null);
                }
            } catch (Exception e) {
                return qt.a(new sn((Throwable) e));
            }
        }
        return qt.a(new sn("Can't parse VMAP response"));
    }
}
