package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class no {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final nn f5653a;
    @Nullable
    private final nl b;

    public no(@Nullable nn nnVar, @Nullable nl nlVar) {
        this.f5653a = nnVar;
        this.b = nlVar;
    }

    @Nullable
    public final nn a() {
        return this.f5653a;
    }

    @Nullable
    public final nl b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof no)) {
            return false;
        }
        no noVar = (no) obj;
        if (this.f5653a == null ? noVar.f5653a != null : !this.f5653a.equals(noVar.f5653a)) {
            return false;
        }
        if (this.b != null) {
            return this.b.equals(noVar.b);
        }
        return noVar.b == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.f5653a != null ? this.f5653a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }
}
