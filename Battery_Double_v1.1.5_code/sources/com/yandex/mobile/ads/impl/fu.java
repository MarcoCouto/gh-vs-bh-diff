package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class fu {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5498a;
    private final boolean b;

    public fu(@NonNull String str, boolean z) {
        this.f5498a = str;
        this.b = z;
    }

    @NonNull
    public final String a() {
        return this.f5498a;
    }

    public final boolean b() {
        return this.b;
    }
}
