package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import javax.net.ssl.SSLSocketFactory;

public final class rq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final rr f5744a;
    @NonNull
    private final rx b;
    @Nullable
    private final Context c;

    public rq(@Nullable Context context, @NonNull rx rxVar) {
        this.b = rxVar;
        this.c = context != null ? context.getApplicationContext() : null;
        this.f5744a = ff.a().c();
    }

    public final rj a() {
        SSLSocketFactory sSLSocketFactory;
        if (hc.a(21)) {
            sSLSocketFactory = rw.a();
        } else {
            sSLSocketFactory = null;
        }
        return new rs(this.c, this.f5744a.a(sSLSocketFactory));
    }
}
