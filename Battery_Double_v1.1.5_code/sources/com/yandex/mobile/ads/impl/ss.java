package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class ss implements RequestListener<List<VideoAd>> {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ua f5774a;
    @NonNull
    private final sv b;
    @NonNull
    private final VideoAdRequest c;

    class a implements RequestListener<List<VideoAd>> {
        @NonNull
        private final RequestListener<List<VideoAd>> b;

        public final /* synthetic */ void onSuccess(@NonNull Object obj) {
            List list = (List) obj;
            ss.this.f5774a.a();
            this.b.onSuccess(list);
        }

        a(RequestListener<List<VideoAd>> requestListener) {
            this.b = requestListener;
        }

        public final void onFailure(@NonNull VideoAdError videoAdError) {
            ss.this.f5774a.a(videoAdError);
            this.b.onFailure(videoAdError);
        }
    }

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        List list = (List) obj;
        RequestListener requestListener = this.c.getRequestListener();
        if (requestListener != null) {
            this.b.a(list, new a(requestListener));
        }
    }

    ss(@NonNull VideoAdRequest videoAdRequest, @NonNull ub ubVar) {
        this.c = videoAdRequest;
        Context context = videoAdRequest.getContext();
        this.f5774a = new ua(context, ubVar);
        this.b = new sv(context, ubVar);
    }

    public final void onFailure(@NonNull VideoAdError videoAdError) {
        RequestListener requestListener = this.c.getRequestListener();
        if (requestListener != null) {
            requestListener.onFailure(videoAdError);
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VideoAdRequest a() {
        return this.c;
    }
}
