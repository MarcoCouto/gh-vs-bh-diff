package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class iq {
    public static boolean a(@NonNull Context context, @Nullable al alVar) {
        return a(context, alVar, dv.c(context), dv.d(context));
    }

    public static boolean b(@NonNull Context context, @Nullable al alVar) {
        return a(context, alVar, dv.d(context), dv.c(context));
    }

    private static boolean a(@NonNull Context context, @Nullable al alVar, int i, int i2) {
        if (alVar == null) {
            return false;
        }
        int b = alVar.b(context);
        int a2 = alVar.a(context);
        if (b > i || a2 > i2) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0034, code lost:
        r8 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0035, code lost:
        if (r8 == false) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003b, code lost:
        if (a(r6, r7) == false) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0022, code lost:
        r8 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x002a, code lost:
        if (r1 == 0) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0031, code lost:
        if (r3 <= r1) goto L_0x0022;
     */
    public static boolean a(@NonNull Context context, @NonNull al alVar, @NonNull al alVar2) {
        int b = alVar2.b(context);
        int a2 = alVar2.a(context);
        int b2 = alVar.b(context);
        int a3 = alVar.a(context);
        switch (alVar2.c()) {
            case FIXED:
                if (b2 <= b) {
                    if (b > 0) {
                    }
                }
                break;
            case FLEXIBLE:
                if (b2 <= b) {
                    if (b > 0) {
                        if (a3 > a2) {
                        }
                    }
                }
                break;
            case SCREEN:
                break;
        }
    }
}
