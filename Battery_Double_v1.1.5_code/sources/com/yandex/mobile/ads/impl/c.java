package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdEventListener;
import com.yandex.mobile.ads.AdRequestError;

public final class c implements AdEventListener {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Object f5405a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    @NonNull
    private final ck c;
    /* access modifiers changed from: private */
    @Nullable
    public AdEventListener d;

    public c(@NonNull Context context) {
        this.c = new ck(context);
    }

    public final void a(@NonNull fc fcVar) {
        this.c.a(fcVar);
    }

    public final void onAdClosed() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (c.f5405a) {
                    if (c.this.d != null) {
                        c.this.d.onAdClosed();
                    }
                }
            }
        });
    }

    public final void onAdFailedToLoad(@NonNull final AdRequestError adRequestError) {
        this.c.a(adRequestError);
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (c.f5405a) {
                    if (c.this.d != null) {
                        c.this.d.onAdFailedToLoad(adRequestError);
                    }
                }
            }
        });
    }

    public final void a() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (c.f5405a) {
                    if (c.this.d != null) {
                        he.a((Object) c.this.d, "onAdImpressionTracked", new Object[0]);
                    }
                }
            }
        });
    }

    public final void onAdLeftApplication() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (c.f5405a) {
                    if (c.this.d != null) {
                        c.this.d.onAdLeftApplication();
                    }
                }
            }
        });
    }

    public final void onAdLoaded() {
        this.c.a();
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (c.f5405a) {
                    if (c.this.d != null) {
                        c.this.d.onAdLoaded();
                    }
                }
            }
        });
    }

    public final void onAdOpened() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (c.f5405a) {
                    if (c.this.d != null) {
                        c.this.d.onAdOpened();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final AdEventListener b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable AdEventListener adEventListener) {
        this.d = adEventListener;
    }
}
