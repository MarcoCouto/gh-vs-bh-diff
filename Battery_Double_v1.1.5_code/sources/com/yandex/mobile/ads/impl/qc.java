package com.yandex.mobile.ads.impl;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

public final class qc {
    public static qb a(@NonNull Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return new qd();
        }
        return new qf();
    }
}
