package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Window;
import android.widget.RelativeLayout;

public final class it implements m {
    @Nullable
    public final l a(@NonNull Context context, @NonNull RelativeLayout relativeLayout, @Nullable ResultReceiver resultReceiver, @NonNull o oVar, @NonNull Intent intent, @NonNull Window window) {
        ir a2 = ir.a();
        x b = a2.b();
        fc c = a2.c();
        String str = b != null ? (String) b.t() : null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        boolean a3 = a(intent);
        iw iwVar = new iw(b, str, c);
        Context context2 = context;
        ResultReceiver resultReceiver2 = resultReceiver;
        is isVar = new is(context, relativeLayout, new da(context, a3, resultReceiver), oVar, window, iwVar);
        return isVar;
    }

    private static boolean a(@NonNull Intent intent) {
        try {
            return intent.getBooleanExtra("extra_interstitial_isShouldOpenLinksInApp", false);
        } catch (Exception unused) {
            return false;
        }
    }
}
