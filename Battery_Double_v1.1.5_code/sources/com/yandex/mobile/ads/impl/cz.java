package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class cz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5431a;
    private final long b;

    public cz(@NonNull String str, long j) {
        this.f5431a = str;
        this.b = j;
    }

    @NonNull
    public final String a() {
        return this.f5431a;
    }

    public final long b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        cz czVar = (cz) obj;
        if (this.b != czVar.b) {
            return false;
        }
        return this.f5431a.equals(czVar.f5431a);
    }

    public final int hashCode() {
        return (this.f5431a.hashCode() * 31) + ((int) (this.b ^ (this.b >>> 32)));
    }
}
