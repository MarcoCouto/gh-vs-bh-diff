package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.net.URI;

public final class ds {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5444a;

    public ds(@NonNull Context context) {
        this.f5444a = context.getApplicationContext();
    }

    @Nullable
    public final String a(@Nullable String str) {
        fg a2 = ff.a().a(this.f5444a);
        boolean z = a2 != null && a2.l();
        if (TextUtils.isEmpty(str) || !z) {
            return str;
        }
        String decode = Uri.decode(str.trim());
        if (!TextUtils.isEmpty(decode) && decode.startsWith("//")) {
            decode = "https:".concat(String.valueOf(decode));
        }
        return dq.a(b(decode));
    }

    @Nullable
    private static String b(@Nullable String str) {
        try {
            Uri parse = Uri.parse(str);
            URI uri = new URI(parse.getScheme(), parse.getAuthority(), parse.getPath(), parse.getQuery(), parse.getFragment());
            return uri.toASCIIString();
        } catch (Exception unused) {
            new Object[1][0] = str;
            return str;
        }
    }
}
