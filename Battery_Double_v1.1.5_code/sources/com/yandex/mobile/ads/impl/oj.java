package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class oj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ds f5674a;

    public oj(@NonNull ds dsVar) {
        this.f5674a = dsVar;
    }

    @NonNull
    public final String a(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, y {
        String a2 = this.f5674a.a(oh.a(jSONObject, str));
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        throw new y("Native Ad json has not required attributes");
    }
}
