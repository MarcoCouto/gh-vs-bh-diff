package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IReporter;
import com.yandex.mobile.ads.impl.aj.a;

final class gt implements a {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private IReporter f5520a;

    public gt(@NonNull aj ajVar) {
        ajVar.a(this);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable IReporter iReporter) {
        this.f5520a = iReporter;
    }

    public final void a(@NonNull fg fgVar) {
        if (this.f5520a != null) {
            this.f5520a.setStatisticsSending(fgVar.c());
        }
    }
}
