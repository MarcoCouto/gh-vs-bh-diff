package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.am.a;
import com.yandex.mobile.ads.impl.gu.b;

public final class pv implements cf {
    @NonNull
    public final b a() {
        return b.AD_UNIT_FORCED_IMPRESSION_TRACKING_FAILURE;
    }

    @NonNull
    public final b a(@NonNull a aVar) {
        return a.SUCCESS == aVar ? b.AD_UNIT_IMPRESSION_TRACKING_SUCCESS : b.AD_UNIT_IMPRESSION_TRACKING_FAILURE;
    }

    @NonNull
    public final b b() {
        return b.AD_UNIT_IMPRESSION_TRACKING_START;
    }
}
