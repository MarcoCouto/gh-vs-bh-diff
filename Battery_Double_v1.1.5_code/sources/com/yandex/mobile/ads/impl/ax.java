package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.Map;

public interface ax<T, L> {
    void a(@NonNull Context context, @NonNull T t, @NonNull L l, @NonNull Map<String, Object> map, @NonNull Map<String, String> map2);

    void a(@NonNull T t);
}
