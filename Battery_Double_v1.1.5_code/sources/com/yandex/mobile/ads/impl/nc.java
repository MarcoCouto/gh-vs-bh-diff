package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.q;

final class nc {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5641a;
    @NonNull
    private final cg b;
    @NonNull
    private final mz c;
    @NonNull
    private final q d;
    @NonNull
    private final af e;

    nc(@NonNull fc fcVar, @NonNull cg cgVar, @NonNull mz mzVar, @NonNull af afVar, @NonNull q qVar) {
        this.f5641a = fcVar;
        this.b = cgVar;
        this.c = mzVar;
        this.e = afVar;
        this.d = qVar;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007d  */
    @Nullable
    public final nb a(@NonNull Context context, @NonNull ns nsVar) {
        char c2;
        String a2 = nsVar.a();
        int hashCode = a2.hashCode();
        if (hashCode == -342500282) {
            if (a2.equals("shortcut")) {
                c2 = 3;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == -191501435) {
            if (a2.equals("feedback")) {
                c2 = 2;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 94756344) {
            if (a2.equals("close")) {
                c2 = 0;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 629233382 && a2.equals("deeplink")) {
            c2 = 1;
            switch (c2) {
                case 0:
                    return new ne(this.b, this.d);
                case 1:
                    return new nf(new pl(context, this.b, this.c));
                case 2:
                    return new ng(new pq(this.f5641a, this.b, this.e, this.d));
                case 3:
                    return new nh(new pt(context, this.b, this.e));
                default:
                    return null;
            }
        }
        c2 = 65535;
        switch (c2) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }
}
