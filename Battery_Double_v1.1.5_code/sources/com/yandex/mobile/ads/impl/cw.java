package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;

public final class cw {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cv f5426a;
    @NonNull
    private final cn b = new cn();
    @NonNull
    private final gs c;
    @NonNull
    private final fc d;
    @NonNull
    private final cz e;
    @Nullable
    private a f;
    @Nullable
    private com.yandex.mobile.ads.impl.gu.a g;
    private long h;

    public enum a {
        BROWSER("browser"),
        WEBVIEW(ParametersKeys.WEB_VIEW);
        
        final String c;

        private a(String str) {
            this.c = str;
        }
    }

    cw(@NonNull Context context, @NonNull fc fcVar, @NonNull cz czVar) {
        this.d = fcVar;
        this.e = czVar;
        this.f5426a = new cv(context, fcVar);
        this.c = gs.a(context);
    }

    public final void a(@Nullable com.yandex.mobile.ads.impl.gu.a aVar) {
        this.g = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull a aVar) {
        new StringBuilder("startActivityInteraction, type = ").append(aVar);
        this.h = System.currentTimeMillis();
        this.f = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final void b(@Nullable a aVar) {
        new StringBuilder("finishActivityInteraction, type = ").append(aVar);
        if (this.h != 0 && this.f == aVar) {
            long currentTimeMillis = System.currentTimeMillis() - this.h;
            String str = currentTimeMillis < 1000 ? "<1" : (currentTimeMillis <= 1000 || currentTimeMillis > 2000) ? (currentTimeMillis <= 2000 || currentTimeMillis > 3000) ? (currentTimeMillis <= 3000 || currentTimeMillis > DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS) ? (currentTimeMillis <= DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS || currentTimeMillis > 10000) ? (currentTimeMillis <= 10000 || currentTimeMillis > MTGInterstitialActivity.WEB_LOAD_TIME) ? (currentTimeMillis <= MTGInterstitialActivity.WEB_LOAD_TIME || currentTimeMillis > 20000) ? ">20" : "15-20" : "10-15" : "5-10" : "3-5" : "2-3" : "1-2";
            HashMap hashMap = new HashMap();
            hashMap.put("type", aVar.c);
            hashMap.put("ad_type", this.d.a().a());
            hashMap.put("block_id", this.d.e());
            hashMap.put(String.INTERVAL, str);
            hashMap.putAll(cn.a(this.d.c()));
            if (this.g != null) {
                hashMap.putAll(this.g.a());
            }
            this.c.a(new gu(b.RETURNED_TO_APP, hashMap));
            Object[] objArr = {aVar.c, str};
            if (currentTimeMillis <= this.e.b()) {
                this.f5426a.a(this.e.a());
            }
            this.h = 0;
            this.f = null;
        }
    }
}
