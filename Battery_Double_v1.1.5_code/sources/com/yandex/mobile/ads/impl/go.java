package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public final class go implements gf {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gh f5516a = new gh();
    @NonNull
    private final gp b;
    @Nullable
    private final LocationManager c;

    public go(@NonNull Context context) {
        Context applicationContext = context.getApplicationContext();
        this.c = (LocationManager) applicationContext.getSystemService("location");
        this.b = new gp(applicationContext, this.c);
    }

    @Nullable
    public final Location a() {
        List<String> b2 = b();
        if (b2 == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String a2 : b2) {
            Location a3 = this.b.a(a2);
            if (a3 != null) {
                arrayList.add(a3);
            }
        }
        return gh.a(arrayList);
    }

    @Nullable
    private List<String> b() {
        try {
            if (this.c != null) {
                return this.c.getAllProviders();
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }
}
