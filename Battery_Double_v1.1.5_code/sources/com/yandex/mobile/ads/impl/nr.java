package com.yandex.mobile.ads.impl;

public class nr {

    /* renamed from: a reason: collision with root package name */
    private String f5656a;
    private boolean b;

    public final void a(String str) {
        this.f5656a = str;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final String a() {
        return this.f5656a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nr nrVar = (nr) obj;
        if (this.b != nrVar.b) {
            return false;
        }
        if (this.f5656a != null) {
            return this.f5656a.equals(nrVar.f5656a);
        }
        return nrVar.f5656a == null;
    }

    public int hashCode() {
        return ((this.f5656a != null ? this.f5656a.hashCode() : 0) * 31) + (this.b ? 1 : 0);
    }
}
