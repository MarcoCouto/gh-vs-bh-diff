package com.yandex.mobile.ads.impl;

public class bg {

    /* renamed from: a reason: collision with root package name */
    private long f5391a;
    private String b;
    private int c;

    public final void a(long j) {
        this.f5391a = j;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final long a() {
        return this.f5391a;
    }

    public final String b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bg bgVar = (bg) obj;
        if (this.f5391a != bgVar.f5391a || this.c != bgVar.c) {
            return false;
        }
        if (this.b != null) {
            return this.b.equals(bgVar.b);
        }
        return bgVar.b == null;
    }

    public int hashCode() {
        return (((((int) (this.f5391a ^ (this.f5391a >>> 32))) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + this.c;
    }
}
