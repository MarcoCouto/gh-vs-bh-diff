package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;
import java.util.Map;

public final class ck {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gs f5419a;
    @Nullable
    private a b;
    @NonNull
    private final cn c = new cn();
    @Nullable
    private fc d;

    public ck(@NonNull Context context) {
        this.f5419a = gs.a(context);
    }

    public final void a(@NonNull fc fcVar) {
        this.d = fcVar;
    }

    public final void a(@NonNull a aVar) {
        this.b = aVar;
    }

    private void a(@NonNull Map<String, Object> map) {
        this.f5419a.a(b(map));
    }

    private gu b(@NonNull Map<String, Object> map) {
        if (this.d != null) {
            map.put("ad_type", this.d.a().a());
            String e = this.d.e();
            if (e != null) {
                map.put("block_id", e);
            }
            map.putAll(cn.a(this.d.c()));
        }
        if (this.b != null) {
            map.putAll(this.b.a());
        }
        return new gu(b.AD_LOADING_RESULT, map);
    }

    public final void a() {
        HashMap hashMap = new HashMap();
        hashMap.put("status", "success");
        a((Map<String, Object>) hashMap);
    }

    public final void a(@NonNull AdRequestError adRequestError) {
        HashMap hashMap = new HashMap();
        hashMap.put("status", "error");
        hashMap.put("failure_reason", adRequestError.getDescription());
        a((Map<String, Object>) hashMap);
    }
}
