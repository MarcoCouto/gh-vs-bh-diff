package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;
import com.yandex.mobile.ads.impl.od.b;

final class oe implements b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final LruCache<String, Bitmap> f5670a;
    @NonNull
    private final oa b;

    oe(@NonNull LruCache<String, Bitmap> lruCache, @NonNull oa oaVar) {
        this.f5670a = lruCache;
        this.b = oaVar;
    }

    @Nullable
    public final Bitmap a(@NonNull String str) {
        if (VERSION.SDK_INT < 12) {
            return null;
        }
        return (Bitmap) this.f5670a.get(oa.a(str));
    }

    public final void a(@NonNull String str, @NonNull Bitmap bitmap) {
        if (VERSION.SDK_INT >= 12) {
            this.f5670a.put(oa.a(str), bitmap);
        }
    }
}
