package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class sb implements rz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cv f5751a;
    @NonNull
    private final bj b;

    public sb(@NonNull Context context, @NonNull fc fcVar, @NonNull bj bjVar) {
        this.f5751a = new cv(context, fcVar);
        this.b = bjVar;
    }

    public final void a() {
        this.f5751a.a(this.b.a());
    }
}
