package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.sw.b;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class tb {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ug f5785a = new ug();

    @NonNull
    public final qr<Vmap> a(@NonNull Context context, @NonNull fc fcVar, @NonNull VmapRequestConfiguration vmapRequestConfiguration, @NonNull RequestListener<Vmap> requestListener) {
        String pageId = vmapRequestConfiguration.getPageId();
        String categoryId = vmapRequestConfiguration.getCategoryId();
        String d = fcVar.d();
        String f = fcVar.f();
        if (TextUtils.isEmpty(f)) {
            f = "https://mobile.yandexadexchange.net";
        }
        Builder buildUpon = Uri.parse(f).buildUpon();
        buildUpon.appendPath("v1").appendPath("vmap").appendPath(pageId).appendQueryParameter("video-category-id", categoryId).appendQueryParameter("uuid", d).build();
        new sy(context, fcVar).a(context, buildUpon);
        tj tjVar = new tj(context, buildUpon.build().toString(), new b(requestListener), vmapRequestConfiguration, this.f5785a);
        return tjVar;
    }
}
