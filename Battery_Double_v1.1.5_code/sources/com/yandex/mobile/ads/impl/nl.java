package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class nl {

    /* renamed from: a reason: collision with root package name */
    private int f5650a;
    private int b;
    @Nullable
    private String c;
    @Nullable
    private String d;

    public final int a() {
        return this.f5650a;
    }

    public final int b() {
        return this.b;
    }

    @Nullable
    public final String c() {
        return this.c;
    }

    @Nullable
    public final String d() {
        return this.d;
    }

    public final void a(int i) {
        this.f5650a = i;
    }

    public final void b(int i) {
        this.b = i;
    }

    public final void a(@Nullable String str) {
        this.c = str;
    }

    public final void b(@Nullable String str) {
        this.d = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nl nlVar = (nl) obj;
        if (this.f5650a != nlVar.f5650a || this.b != nlVar.b) {
            return false;
        }
        if (this.c == null ? nlVar.c != null : !this.c.equals(nlVar.c)) {
            return false;
        }
        if (this.d != null) {
            return this.d.equals(nlVar.d);
        }
        return nlVar.d == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.f5650a * 31) + this.b) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return hashCode + i;
    }
}
