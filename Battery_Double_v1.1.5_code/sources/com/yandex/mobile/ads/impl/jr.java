package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public abstract class jr<T, V> {

    /* renamed from: a reason: collision with root package name */
    private static final nm f5581a = null;

    @NonNull
    public abstract ni<T> a(@NonNull String str, @NonNull V v);

    @NonNull
    protected static ni<T> a(@NonNull String str, @NonNull String str2, @NonNull T t) {
        ni niVar = new ni(str, str2, t, f5581a, false, false);
        return niVar;
    }
}
