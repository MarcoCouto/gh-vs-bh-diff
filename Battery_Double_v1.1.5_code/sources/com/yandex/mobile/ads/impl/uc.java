package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.VideoAdRequest;

public final class uc implements ub {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final VideoAdRequest f5804a;

    public uc(@NonNull VideoAdRequest videoAdRequest) {
        this.f5804a = videoAdRequest;
    }

    @NonNull
    public final String a() {
        return this.f5804a.getBlocksInfo().getPartnerId();
    }

    @NonNull
    public final String b() {
        return this.f5804a.getBlockId();
    }
}
