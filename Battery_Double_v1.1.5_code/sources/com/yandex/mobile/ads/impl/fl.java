package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.util.concurrent.Callable;

public final class fl {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final hf f5489a = new hf();
    @NonNull
    private final fm b = new fm();

    @NonNull
    public final fk a(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        fm fmVar = this.b;
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        Display display = (Display) fn.a(new Callable<Display>(windowManager) {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ WindowManager f5491a;

            {
                this.f5491a = r2;
            }

            @Nullable
            public final /* synthetic */ Object call() throws Exception {
                return this.f5491a.getDefaultDisplay();
            }
        }, windowManager, "WindowManager");
        boolean z = false;
        Object point = new Point(0, 0);
        Object a2 = fn.a(new Callable<Point>(display) {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ Display f5492a;

            {
                this.f5492a = r2;
            }

            @NonNull
            public final /* synthetic */ Object call() throws Exception {
                return fm.a(this.f5492a);
            }
        }, display, "Display");
        if (a2 == null) {
            a2 = point;
        }
        Point point2 = (Point) a2;
        int i = point2.x;
        int i2 = point2.y;
        float f = displayMetrics.density;
        float f2 = (float) i;
        float f3 = (float) i2;
        float min = Math.min(f2 / f, f3 / f);
        float f4 = f * 160.0f;
        float f5 = f2 / f4;
        float f6 = f3 / f4;
        double sqrt = Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
        if (sqrt >= 15.0d && !hf.a(context, "android.hardware.touchscreen")) {
            z = true;
        }
        if (z) {
            return fk.TV;
        }
        if (sqrt >= 7.0d || min >= 600.0f) {
            return fk.TABLET;
        }
        return fk.PHONE;
    }
}
