package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import com.yandex.mobile.ads.nativeads.MediaView;

public final class km implements ki<no>, kk<no> {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final kx f5591a;
    @Nullable
    private final ky b;

    public final /* synthetic */ void a(@NonNull Object obj) {
        no noVar = (no) obj;
        a(noVar.b());
        MediaView mediaView = (MediaView) a((ld<V, T>) this.b);
        if (mediaView != null) {
            this.b.b(mediaView, noVar);
            mediaView.setVisibility(0);
        }
    }

    public final /* synthetic */ boolean b(@NonNull Object obj) {
        no noVar = (no) obj;
        return a((ld<V, T>) this.f5591a, (T) noVar.b()) || a((ld<V, T>) this.b, (T) noVar);
    }

    public final /* synthetic */ void c(@NonNull Object obj) {
        no noVar = (no) obj;
        a(noVar.b());
        MediaView mediaView = (MediaView) a((ld<V, T>) this.b);
        if (mediaView != null) {
            this.b.a(noVar);
            mediaView.setVisibility(0);
        }
    }

    public km(@Nullable kx kxVar, @Nullable ky kyVar) {
        this.f5591a = kxVar;
        this.b = kyVar;
    }

    public final void a() {
        ImageView imageView = (ImageView) a((ld<V, T>) this.f5591a);
        if (imageView != null) {
            this.f5591a.a(imageView);
        }
        MediaView mediaView = (MediaView) a((ld<V, T>) this.b);
        if (mediaView != null) {
            this.b.a(mediaView);
        }
    }

    private void a(@Nullable nl nlVar) {
        ImageView imageView = (ImageView) a((ld<V, T>) this.f5591a);
        if (imageView != null && nlVar != null) {
            this.f5591a.b(imageView, nlVar);
            imageView.setVisibility(0);
        }
    }

    public final void a(@NonNull ni niVar, @NonNull li liVar) {
        if (this.f5591a != null) {
            this.f5591a.a(niVar, liVar);
        }
        if (this.b != null) {
            this.b.a(niVar, liVar);
        }
    }

    private static <V extends View, T> boolean a(@Nullable ld<V, T> ldVar, @Nullable T t) {
        View a2 = a(ldVar);
        return (a2 == null || t == null || !ldVar.a(a2, t)) ? false : true;
    }

    @Nullable
    private static <V extends View, T> V a(@Nullable ld<V, T> ldVar) {
        if (ldVar != null) {
            return ldVar.a();
        }
        return null;
    }

    public final boolean b() {
        return (this.b != null && this.b.b()) || (this.f5591a != null && this.f5591a.b());
    }

    public final boolean c() {
        return (this.b != null && this.b.c()) || (this.f5591a != null && this.f5591a.c());
    }

    public final boolean d() {
        return (this.b != null && this.b.d()) || (this.f5591a != null && this.f5591a.d());
    }
}
