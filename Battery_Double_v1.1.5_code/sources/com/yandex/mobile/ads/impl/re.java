package com.yandex.mobile.ads.impl;

public class re extends Exception {

    /* renamed from: a reason: collision with root package name */
    public final qq f5728a;
    private long b;

    public re() {
        this.f5728a = null;
    }

    public re(qq qqVar) {
        this.f5728a = qqVar;
    }

    public re(String str) {
        super(str);
        this.f5728a = null;
    }

    public re(String str, Throwable th) {
        super(str, th);
        this.f5728a = null;
    }

    public re(Throwable th) {
        super(th);
        this.f5728a = null;
    }

    public void a(long j) {
        this.b = j;
    }
}
