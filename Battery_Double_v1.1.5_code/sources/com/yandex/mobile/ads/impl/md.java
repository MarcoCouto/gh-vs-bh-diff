package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.n;
import com.yandex.mobile.ads.nativeads.o;
import com.yandex.mobile.ads.nativeads.s;

final class md {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5620a;
    @NonNull
    private final n b = new n();
    @NonNull
    private final ma c = new ma();

    md(@NonNull fc fcVar) {
        this.f5620a = fcVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull o oVar, @NonNull i iVar, @NonNull s sVar, @NonNull mb mbVar) {
        lz a2 = ma.a(this.f5620a.o());
        if (a2 != null) {
            a2.a(context, oVar, iVar, sVar, mbVar);
        } else {
            mbVar.a(v.f5815a);
        }
    }
}
