package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.a;
import java.util.List;

public final class cc {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cv f5414a;
    @NonNull
    private final cl b;
    @Nullable
    private final List<String> c;

    public cc(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar, @Nullable List<String> list) {
        this.c = list;
        this.f5414a = new cv(context, fcVar);
        this.b = new cl(context, xVar);
    }

    public final void a(@NonNull a aVar) {
        this.b.a(aVar);
    }

    public final void a() {
        if (this.c != null) {
            for (String a2 : this.c) {
                this.f5414a.a(a2);
            }
        }
        this.b.a();
    }
}
