package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import android.util.Xml;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.video.models.ad.Creative;
import com.yandex.mobile.ads.video.models.ad.Icon;
import com.yandex.mobile.ads.video.models.ad.Icon.IconResourceType;
import com.yandex.mobile.ads.video.models.ad.MediaFile;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import com.yandex.mobile.ads.video.models.ad.VideoAdConfigurator;
import com.yandex.mobile.ads.video.models.blocksinfo.Block;
import com.yandex.mobile.ads.video.models.blocksinfo.BlocksInfo;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class tk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tm f5789a = new tm();

    private static class a {

        /* renamed from: a reason: collision with root package name */
        private VideoAd f5790a;
        private XmlPullParser b;

        /* synthetic */ a(VideoAd videoAd, XmlPullParser xmlPullParser, byte b2) {
            this(videoAd, xmlPullParser);
        }

        private a(VideoAd videoAd, XmlPullParser xmlPullParser) {
            this.f5790a = videoAd;
            this.b = xmlPullParser;
        }

        static /* synthetic */ void a(a aVar, String str, String str2) throws IOException, XmlPullParserException {
            tk.b(aVar.b, str);
            he.a((Object) aVar.f5790a, str2, tk.l(aVar.b));
        }
    }

    private static class b {

        /* renamed from: a reason: collision with root package name */
        private BlocksInfo f5791a;
        private XmlPullParser b;

        /* synthetic */ b(BlocksInfo blocksInfo, XmlPullParser xmlPullParser, byte b2) {
            this(blocksInfo, xmlPullParser);
        }

        private b(BlocksInfo blocksInfo, XmlPullParser xmlPullParser) {
            this.f5791a = blocksInfo;
            this.b = xmlPullParser;
        }

        static /* synthetic */ void a(b bVar, String str, String str2) throws IOException, XmlPullParserException {
            tk.b(bVar.b, str);
            he.a((Object) bVar.f5791a, str2, tk.l(bVar.b));
        }
    }

    @Nullable
    public final sj a(String str) throws XmlPullParserException, IOException {
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        newPullParser.setInput(new StringReader(str));
        newPullParser.nextTag();
        b(newPullParser, VastTagName.VAST);
        return b(newPullParser);
    }

    @Nullable
    private sj b(@NonNull XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        String attributeValue = xmlPullParser.getAttributeValue(null, "version");
        ArrayList arrayList = new ArrayList();
        while (n(xmlPullParser)) {
            if (m(xmlPullParser) && xmlPullParser.getName().equals(VastTagName.AD)) {
                arrayList.add(c(xmlPullParser));
            }
        }
        if (!TextUtils.isEmpty(attributeValue)) {
            return new sj(attributeValue, arrayList);
        }
        return null;
    }

    public final List<VideoAd> b(String str) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        newPullParser.setInput(new StringReader(str));
        newPullParser.nextTag();
        b(newPullParser, VastTagName.VAST);
        while (n(newPullParser)) {
            if (m(newPullParser) && newPullParser.getName().equals(VastTagName.AD)) {
                arrayList.add(c(newPullParser));
            }
        }
        return arrayList;
    }

    private VideoAd c(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        VideoAd videoAd = (VideoAd) he.a(VideoAd.class, Boolean.FALSE);
        b(xmlPullParser, VastTagName.AD);
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                String name = xmlPullParser.getName();
                if (name.equals(VastTagName.IN_LINE)) {
                    videoAd = (VideoAd) he.a(VideoAd.class, Boolean.FALSE);
                    b(xmlPullParser, VastTagName.IN_LINE);
                    while (n(xmlPullParser)) {
                        if (m(xmlPullParser)) {
                            a(xmlPullParser, videoAd);
                        }
                    }
                } else if (name.equals(VastTagName.WRAPPER)) {
                    videoAd = (VideoAd) he.a(VideoAd.class, Boolean.TRUE);
                    b(xmlPullParser, VastTagName.WRAPPER);
                    if (videoAd != null) {
                        b(xmlPullParser, VastTagName.WRAPPER);
                        new VideoAdConfigurator(videoAd).setWrapperConfiguration(tm.a(xmlPullParser));
                    }
                    while (n(xmlPullParser)) {
                        if (m(xmlPullParser)) {
                            if (TextUtils.equals(xmlPullParser.getName(), VastTagName.VAST_AD_TAG_URI)) {
                                b(xmlPullParser, VastTagName.VAST_AD_TAG_URI);
                                he.a((Object) videoAd, "setVastAdTagUri", l(xmlPullParser));
                            } else {
                                a(xmlPullParser, videoAd);
                            }
                        }
                    }
                }
            }
        }
        return videoAd;
    }

    private static void a(XmlPullParser xmlPullParser, VideoAd videoAd) throws IOException, XmlPullParserException {
        a aVar = new a(videoAd, xmlPullParser, 0);
        String name = xmlPullParser.getName();
        if (TextUtils.equals(name, VastTagName.IMPRESSION)) {
            a.a(aVar, VastTagName.IMPRESSION, "addImpression");
        } else if (TextUtils.equals(name, "Error")) {
            a.a(aVar, "Error", "addError");
        } else if (TextUtils.equals(name, "Survey")) {
            a.a(aVar, "Survey", "setSurvey");
        } else if (TextUtils.equals(name, "Description")) {
            a.a(aVar, "Description", "setDescription");
        } else if (TextUtils.equals(name, "AdTitle")) {
            a.a(aVar, "AdTitle", "setAdTitle");
        } else if (TextUtils.equals(name, VastTagName.AD_SYSTEM)) {
            a.a(aVar, VastTagName.AD_SYSTEM, "setAdSystem");
        } else if (TextUtils.equals(name, VastTagName.CREATIVES)) {
            he.a((Object) videoAd, "addCreatives", d(xmlPullParser));
        } else {
            k(xmlPullParser);
        }
    }

    private static List<Creative> d(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        ArrayList arrayList = new ArrayList();
        b(xmlPullParser, VastTagName.CREATIVES);
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                if (TextUtils.equals(xmlPullParser.getName(), VastTagName.CREATIVE)) {
                    Creative e = e(xmlPullParser);
                    if (e != null) {
                        arrayList.add(e);
                    }
                } else {
                    k(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    private static Creative e(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        b(xmlPullParser, VastTagName.CREATIVE);
        String attributeValue = xmlPullParser.getAttributeValue(null, "id");
        Creative creative = null;
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                if (TextUtils.equals(xmlPullParser.getName(), VastTagName.LINEAR)) {
                    creative = (Creative) he.a(Creative.class, new Object[0]);
                    if (creative != null) {
                        b(xmlPullParser, VastTagName.LINEAR);
                        he.a((Object) creative, "setId", attributeValue);
                        while (n(xmlPullParser)) {
                            String name = xmlPullParser.getName();
                            if (m(xmlPullParser)) {
                                if (TextUtils.equals(name, VastTagName.DURATION)) {
                                    b(xmlPullParser, VastTagName.DURATION);
                                    he.a((Object) creative, "setDurationHHMMSS", l(xmlPullParser));
                                } else if (TextUtils.equals(name, VastTagName.TRACKING_EVENTS)) {
                                    String str = "addTrackingEvents";
                                    Object[] objArr = new Object[1];
                                    ArrayList arrayList = new ArrayList();
                                    b(xmlPullParser, VastTagName.TRACKING_EVENTS);
                                    while (n(xmlPullParser)) {
                                        if (m(xmlPullParser)) {
                                            if (TextUtils.equals(xmlPullParser.getName(), VastTagName.TRACKING)) {
                                                b(xmlPullParser, VastTagName.TRACKING);
                                                String attributeValue2 = xmlPullParser.getAttributeValue(null, "event");
                                                String l = l(xmlPullParser);
                                                if (!TextUtils.isEmpty(l)) {
                                                    arrayList.add(new Pair(attributeValue2, l));
                                                }
                                            } else {
                                                k(xmlPullParser);
                                            }
                                        }
                                    }
                                    objArr[0] = arrayList;
                                    he.a((Object) creative, str, objArr);
                                } else if (TextUtils.equals(name, VastTagName.MEDIA_FILES)) {
                                    he.a((Object) creative, "addMediaFiles", h(xmlPullParser));
                                } else if (TextUtils.equals(name, VastTagName.VIDEO_CLICKS)) {
                                    he.a((Object) creative, "addClickThroughUrls", a(xmlPullParser, creative));
                                } else if (TextUtils.equals(name, "Icons")) {
                                    he.a((Object) creative, "addIcons", f(xmlPullParser));
                                } else {
                                    k(xmlPullParser);
                                }
                            }
                        }
                    }
                } else {
                    k(xmlPullParser);
                }
            }
        }
        return creative;
    }

    private static List<Icon> f(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        ArrayList arrayList = new ArrayList();
        b(xmlPullParser, "Icons");
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                if (TextUtils.equals(xmlPullParser.getName(), "Icon")) {
                    arrayList.add(g(xmlPullParser));
                } else {
                    k(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    private static Icon g(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        Icon icon = (Icon) he.a(Icon.class, new Object[0]);
        b(xmlPullParser, "Icon");
        a(icon, xmlPullParser, "program", "setProgram");
        a(icon, xmlPullParser, "height", "setHeight");
        a(icon, xmlPullParser, "width", "setWidth");
        a(icon, xmlPullParser, "xPosition", "setHorizontalPosition");
        a(icon, xmlPullParser, "yPosition", "setVerticalPosition");
        a(icon, xmlPullParser, VastAttributes.API_FRAMEWORK, "setApiFramework");
        a(icon, xmlPullParser, "offset", "setOffset");
        a(icon, xmlPullParser, IronSourceConstants.EVENTS_DURATION, "setDuration");
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                String name = xmlPullParser.getName();
                if (IconResourceType.contains(name)) {
                    b(xmlPullParser, name);
                    String l = l(xmlPullParser);
                    he.a((Object) icon, "setResourceType", name);
                    he.a((Object) icon, "setResourceUrl", l);
                }
            }
        }
        return icon;
    }

    private static List<MediaFile> h(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        ArrayList arrayList = new ArrayList();
        b(xmlPullParser, VastTagName.MEDIA_FILES);
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                if (TextUtils.equals(xmlPullParser.getName(), VastTagName.MEDIA_FILE)) {
                    MediaFile mediaFile = (MediaFile) he.a(MediaFile.class, new Object[0]);
                    b(xmlPullParser, VastTagName.MEDIA_FILE);
                    a(mediaFile, xmlPullParser, "id", "setId");
                    a(mediaFile, xmlPullParser, VastAttributes.DELIVERY, "setDeliveryMethod");
                    a(mediaFile, xmlPullParser, "height", "setHeight");
                    a(mediaFile, xmlPullParser, "width", "setWidth");
                    a(mediaFile, xmlPullParser, VastAttributes.BITRATE, "setBitrate");
                    a(mediaFile, xmlPullParser, "type", "setMimeType");
                    he.a((Object) mediaFile, "setUri", l(xmlPullParser));
                    arrayList.add(mediaFile);
                } else {
                    k(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    private static List<String> a(XmlPullParser xmlPullParser, Creative creative) throws IOException, XmlPullParserException {
        ArrayList arrayList = new ArrayList();
        b(xmlPullParser, VastTagName.VIDEO_CLICKS);
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                String name = xmlPullParser.getName();
                if (TextUtils.equals(name, VastTagName.CLICK_THROUGH)) {
                    b(xmlPullParser, VastTagName.CLICK_THROUGH);
                    arrayList.add(l(xmlPullParser));
                } else if (TextUtils.equals(name, VastTagName.CLICK_TRACKING)) {
                    b(xmlPullParser, VastTagName.CLICK_TRACKING);
                    String l = l(xmlPullParser);
                    if (!TextUtils.isEmpty(l)) {
                        he.a((Object) creative, "addTrackingEvent", Events.CREATIVE_CLICK_TRACKING, l);
                    }
                } else {
                    k(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    public static BlocksInfo c(String str) throws XmlPullParserException, IOException {
        BlocksInfo blocksInfo = (BlocksInfo) he.a(BlocksInfo.class, new Object[0]);
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        newPullParser.setInput(new StringReader(str));
        newPullParser.nextTag();
        b(newPullParser, "Blocksinfo");
        b bVar = new b(blocksInfo, newPullParser, 0);
        while (n(newPullParser)) {
            String name = newPullParser.getName();
            if (m(newPullParser)) {
                if (TextUtils.equals(name, "Version")) {
                    b.a(bVar, "Version", "setVersion");
                } else if (TextUtils.equals(name, "PartnerID")) {
                    b.a(bVar, "PartnerID", "setPartnerId");
                } else if (TextUtils.equals(name, "SessionID")) {
                    b.a(bVar, "SessionID", "setSessionId");
                } else if (TextUtils.equals(name, "CategoryID")) {
                    b.a(bVar, "CategoryID", "setCategoryId");
                } else if (TextUtils.equals(name, "CategoryName")) {
                    b.a(bVar, "CategoryName", "setCategoryName");
                } else if (TextUtils.equals(name, "Skin")) {
                    b.a(bVar, "Skin", "setSkin");
                } else if (TextUtils.equals(name, "VPAIDEnabled")) {
                    b.a(bVar, "VPAIDEnabled", "setVPaidEnabled");
                } else if (TextUtils.equals(name, "BufferEmptyLimit")) {
                    b.a(bVar, "BufferEmptyLimit", "setBufferEmptyLimit");
                } else if (TextUtils.equals(name, "Title")) {
                    b.a(bVar, "Title", "setTitle");
                } else if (TextUtils.equals(name, "SkipDelay")) {
                    b.a(bVar, "SkipDelay", "setSkipDelay");
                } else if (TextUtils.equals(name, "SkinTimeout")) {
                    b.a(bVar, "SkinTimeout", "setSkinTimeout");
                } else if (TextUtils.equals(name, "VPAIDTimeout")) {
                    b.a(bVar, "VPAIDTimeout", "setVpaidTimeout");
                } else if (TextUtils.equals(name, "WrapperTimeout")) {
                    b.a(bVar, "WrapperTimeout", "setWrapperTimeout");
                } else if (TextUtils.equals(name, "VideoTimeout")) {
                    b.a(bVar, "VideoTimeout", "setVideoTimeout");
                } else if (TextUtils.equals(name, "VASTTimeout")) {
                    b.a(bVar, "VASTTimeout", "setVastTimeout");
                } else if (TextUtils.equals(name, "TimeLeftShow")) {
                    b.a(bVar, "TimeLeftShow", "setShowTimeLeft");
                } else if (TextUtils.equals(name, "SkipTimeLeftShow")) {
                    b.a(bVar, "SkipTimeLeftShow", "setShowSkipTimeLeft");
                } else if (TextUtils.equals(name, "BufferFullTimeout")) {
                    b.a(bVar, "BufferFullTimeout", "setBufferFullTimeout");
                } else if (TextUtils.equals(name, "WrapperMaxCount")) {
                    b.a(bVar, "WrapperMaxCount", "setWrapperMaxCount");
                } else if (TextUtils.equals(name, "FirstBuffTimeout")) {
                    b.a(bVar, "FirstBuffTimeout", "setFirstBuffTimeout");
                } else if (TextUtils.equals(name, "Blocks")) {
                    he.a((Object) blocksInfo, "addBlocks", i(newPullParser));
                } else {
                    k(newPullParser);
                }
            }
        }
        return blocksInfo;
    }

    private static List<Block> i(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        ArrayList arrayList = new ArrayList();
        b(xmlPullParser, "Blocks");
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                if (TextUtils.equals(xmlPullParser.getName(), "Block")) {
                    arrayList.add(j(xmlPullParser));
                } else {
                    k(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    private static Block j(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        Block block = (Block) he.a(Block.class, new Object[0]);
        b(xmlPullParser, "Block");
        he.a((Object) block, "setType", xmlPullParser.getAttributeValue(null, "type"));
        he.a((Object) block, "setId", xmlPullParser.getAttributeValue(null, "BlockID"));
        while (n(xmlPullParser)) {
            if (m(xmlPullParser)) {
                String name = xmlPullParser.getName();
                if (TextUtils.equals(name, "StartTime")) {
                    he.a((Object) block, "setStartTime", l(xmlPullParser));
                } else if (TextUtils.equals(name, VastTagName.DURATION)) {
                    he.a((Object) block, "setDuration", l(xmlPullParser));
                } else if (TextUtils.equals(name, "PositionsCount")) {
                    he.a((Object) block, "setPositionsCount", l(xmlPullParser));
                } else {
                    k(xmlPullParser);
                }
            }
        }
        return block;
    }

    private static void k(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        if (xmlPullParser.getEventType() == 2) {
            int i = 1;
            while (i != 0) {
                switch (xmlPullParser.next()) {
                    case 2:
                        i++;
                        break;
                    case 3:
                        i--;
                        break;
                }
            }
            return;
        }
        throw new IllegalStateException();
    }

    private static void a(Object obj, XmlPullParser xmlPullParser, String str, String str2) throws IOException, XmlPullParserException {
        he.a(obj, str2, xmlPullParser.getAttributeValue(null, str));
    }

    /* access modifiers changed from: private */
    public static String l(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        String str = "";
        if (xmlPullParser.next() == 4) {
            str = xmlPullParser.getText();
            xmlPullParser.nextTag();
        }
        return str.trim();
    }

    private static boolean m(XmlPullParser xmlPullParser) throws XmlPullParserException {
        return xmlPullParser.getEventType() == 2;
    }

    private static boolean n(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        return xmlPullParser.next() != 3;
    }

    /* access modifiers changed from: private */
    public static void b(XmlPullParser xmlPullParser, String str) throws IOException, XmlPullParserException {
        xmlPullParser.require(2, null, str);
    }
}
