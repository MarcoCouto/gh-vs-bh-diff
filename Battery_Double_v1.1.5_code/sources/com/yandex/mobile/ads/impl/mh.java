package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeGenericAd;
import com.yandex.mobile.ads.nativeads.ao;
import com.yandex.mobile.ads.nativeads.c;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.v;

final class mh implements mk {
    mh() {
    }

    public final NativeGenericAd a(@NonNull Context context, @NonNull np npVar, @NonNull v vVar, @NonNull i iVar, @NonNull c cVar) {
        ao aoVar = new ao(context, npVar, vVar, iVar, cVar);
        return aoVar;
    }
}
