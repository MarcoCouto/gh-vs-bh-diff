package com.yandex.mobile.ads.impl;

import com.yandex.mobile.ads.impl.ru.a;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class ti extends tg<VideoAdRequest, List<VideoAd>> {
    /* access modifiers changed from: protected */
    public final /* synthetic */ qt c(Object obj) {
        List list = (List) obj;
        if (!list.isEmpty()) {
            return qt.a(list, null);
        }
        return qt.a(new sm());
    }

    public ti(VideoAdRequest videoAdRequest, String str, a<List<VideoAd>> aVar, gv<VideoAdRequest, List<VideoAd>> gvVar) {
        super(str, aVar, videoAdRequest.getContext(), videoAdRequest, gvVar, 0);
    }

    /* access modifiers changed from: protected */
    public final qt<List<VideoAd>> a(Exception exc) {
        return qt.a(new sm());
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(String str) throws Exception {
        return new tk().b(str);
    }
}
