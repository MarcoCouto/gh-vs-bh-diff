package com.yandex.mobile.ads.impl;

import com.yandex.mobile.ads.impl.qt.b;
import java.util.concurrent.TimeUnit;

public abstract class ru<T> extends qr<T> {

    /* renamed from: a reason: collision with root package name */
    private static final int f5746a = ((int) TimeUnit.SECONDS.toMillis(10));
    private final a<T> b;

    public interface a<T> extends com.yandex.mobile.ads.impl.qt.a, b<T> {
    }

    public ru(int i, String str, a<T> aVar) {
        super(i, str, aVar);
        l();
        a((qv) new qm(f5746a, 0, 1.0f));
        this.b = aVar;
    }

    /* access modifiers changed from: protected */
    public final void b(T t) {
        if (this.b != null) {
            this.b.a(t);
        }
    }
}
