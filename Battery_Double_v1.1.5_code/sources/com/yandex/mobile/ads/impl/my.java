package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.lang.ref.WeakReference;

public final class my implements db {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<Context> f5637a;
    @NonNull
    private final fc b;
    @NonNull
    private final ResultReceiver c;

    public my(@NonNull Context context, @NonNull fc fcVar, @NonNull ResultReceiver resultReceiver) {
        this.f5637a = new WeakReference<>(context);
        this.b = fcVar;
        this.c = resultReceiver;
    }

    public final void a(@NonNull cg cgVar, @Nullable String str) {
        p.a((Context) this.f5637a.get(), cgVar, str, this.c, this.b.j());
    }
}
