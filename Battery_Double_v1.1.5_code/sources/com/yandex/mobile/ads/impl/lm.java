package com.yandex.mobile.ads.impl;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.HashSet;
import java.util.Map;
import java.util.WeakHashMap;

@TargetApi(14)
final class lm implements lk {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5605a = new Object();
    private static volatile lm b;
    /* access modifiers changed from: private */
    public final Object c = new Object();
    private boolean d;
    /* access modifiers changed from: private */
    public final Map<ln, Void> e = new WeakHashMap();
    private final ActivityLifecycleCallbacks f = new ActivityLifecycleCallbacks() {
        public final void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public final void onActivityDestroyed(Activity activity) {
        }

        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public final void onActivityStarted(Activity activity) {
        }

        public final void onActivityResumed(Activity activity) {
            synchronized (lm.this.c) {
                for (ln a2 : new HashSet(lm.this.e.keySet())) {
                    a2.a(activity);
                }
            }
        }

        public final void onActivityPaused(Activity activity) {
            synchronized (lm.this.c) {
                for (ln b : new HashSet(lm.this.e.keySet())) {
                    b.b(activity);
                }
            }
        }

        public final void onActivityStopped(Activity activity) {
            synchronized (lm.this.c) {
                if (lm.this.e.isEmpty()) {
                    lm.this.a((Context) activity);
                }
            }
        }
    };

    lm() {
    }

    @NonNull
    public static lm a() {
        if (b == null) {
            synchronized (f5605a) {
                if (b == null) {
                    b = new lm();
                }
            }
        }
        return b;
    }

    private boolean b() {
        boolean z;
        synchronized (this.c) {
            z = this.d;
        }
        return z;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:2|3|(2:5|6)|7|8) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001d */
    public final void a(@NonNull Context context, @NonNull ln lnVar) {
        synchronized (this.c) {
            this.e.put(lnVar, null);
            if (!b()) {
                ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(this.f);
                this.d = true;
            }
        }
    }

    public final void b(@NonNull Context context, @NonNull ln lnVar) {
        synchronized (this.c) {
            this.e.remove(lnVar);
            if (this.e.isEmpty()) {
                a(context);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final void a(@NonNull Context context) {
        try {
            if (b()) {
                ((Application) context.getApplicationContext()).unregisterActivityLifecycleCallbacks(this.f);
                this.d = false;
            }
        } catch (Throwable unused) {
        }
    }
}
