package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.yandex.mobile.ads.impl.qt.b;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public final class eo implements ev {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final em f5465a;
    /* access modifiers changed from: private */
    public final eg b;
    /* access modifiers changed from: private */
    public final es c = new es(new a(this, 0));
    private final eq d;
    @NonNull
    private fa e;
    @NonNull
    private final et f;
    @NonNull
    private final eu g;
    @NonNull
    private final String h;
    @NonNull
    private final hu i;
    @Nullable
    private er j;
    @Nullable
    private ij k;
    @Nullable
    private ii l;
    @Nullable
    private ef m;
    @Nullable
    private dy n;
    @Nullable
    private ew o;

    private class a implements eb {
        private a() {
        }

        /* synthetic */ a(eo eoVar, byte b) {
            this();
        }

        public final void d() {
            eo.this.b.d();
        }

        public final void a(@NonNull Context context, @NonNull String str) {
            eo.this.b.a(context, str);
        }
    }

    public eo(@NonNull eg egVar) {
        this.b = egVar;
        this.b.setWebViewClient(this.c);
        this.f5465a = new em(this.b);
        this.d = new eq();
        this.i = new hu();
        this.e = fa.LOADING;
        this.f = new et();
        this.g = new eu(egVar, this.f, this);
        this.h = di.a(this);
    }

    public final void a(@NonNull er erVar) {
        this.j = erVar;
    }

    public final void a(@NonNull ij ijVar) {
        this.k = ijVar;
    }

    public final void a(@NonNull ii iiVar) {
        this.l = iiVar;
    }

    public final void a(@NonNull ef efVar) {
        this.m = efVar;
    }

    public final void a(@NonNull dy dyVar) {
        this.n = dyVar;
    }

    public final void a(@NonNull final String str) {
        Context context = this.b.getContext();
        eq eqVar = this.d;
        String str2 = this.h;
        AnonymousClass1 r3 = new com.yandex.mobile.ads.impl.eq.a() {
            public final void a(@NonNull String str) {
                eo.this.c.a(str);
                eo.this.f5465a.a(str);
            }
        };
        fg a2 = ff.a().a(context);
        if (a2 == null || TextUtils.isEmpty(a2.e())) {
            r3.a(eq.f5470a);
            return;
        }
        rp rpVar = new rp(a2.e(), new b<String>(r3) {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ a f5471a;

            {
                this.f5471a = r2;
            }

            public final /* bridge */ /* synthetic */ void a(Object obj) {
                this.f5471a.a((String) obj);
            }
        }, new com.yandex.mobile.ads.impl.qt.a(r3) {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ a f5472a;

            {
                this.f5472a = r2;
            }

            public final void a(@NonNull re reVar) {
                this.f5472a.a(eq.f5470a);
            }
        });
        rpVar.a((Object) str2);
        ah.a().a(context, (qr) rpVar);
    }

    public final void a() {
        ez ezVar = new ez(this.b);
        fb fbVar = new fb(hu.a(this.b));
        ex c2 = c(et.a(this.b));
        this.e = fa.DEFAULT;
        this.f5465a.a(this.e, fbVar, c2, ezVar);
        this.f5465a.a();
        if (this.j != null) {
            this.j.a();
        }
    }

    public final void a(boolean z) {
        fb fbVar = new fb(z);
        this.f5465a.a(fbVar);
        if (z) {
            this.g.a();
            return;
        }
        this.g.b();
        b(et.a(this.b));
    }

    public final void a(@NonNull ew ewVar) {
        b(ewVar);
    }

    private void b(@NonNull ew ewVar) {
        if (!ewVar.equals(this.o)) {
            this.o = ewVar;
            ex c2 = c(ewVar);
            this.f5465a.a(c2);
        }
    }

    @NonNull
    private static ex c(@NonNull ew ewVar) {
        return new ex(ewVar.a(), ewVar.b());
    }

    public final void b() {
        if (fa.DEFAULT == this.e) {
            a(fa.HIDDEN);
        }
    }

    public final void b(String str) {
        try {
            URI uri = new URI(str);
            String scheme = uri.getScheme();
            String host = uri.getHost();
            if (CampaignEx.JSON_KEY_MRAID.equals(scheme) || "mobileads".equals(scheme)) {
                HashMap hashMap = new HashMap();
                for (NameValuePair nameValuePair : URLEncodedUtils.parse(uri, "UTF-8")) {
                    hashMap.put(nameValuePair.getName(), nameValuePair.getValue());
                }
                ep a2 = ep.a(host);
                try {
                    if (this.j != null) {
                        switch (a2) {
                            case CLOSE:
                                if (fa.DEFAULT == this.e) {
                                    a(fa.HIDDEN);
                                    if (this.m != null) {
                                        this.m.h();
                                        break;
                                    }
                                }
                                break;
                            case USE_CUSTOM_CLOSE:
                                if (this.m != null) {
                                    this.m.a(Boolean.parseBoolean((String) hashMap.get("shouldUseCustomClose")));
                                    break;
                                }
                                break;
                            case OPEN:
                                if (this.j != null) {
                                    String str2 = (String) hashMap.get("url");
                                    if (!TextUtils.isEmpty(str2)) {
                                        this.j.a(str2);
                                        new Object[1][0] = str2;
                                        break;
                                    } else {
                                        throw new en(String.format("Mraid open command sent an invalid URL: %s", new Object[]{str2}));
                                    }
                                }
                                break;
                            case AD_VIDEO_COMPLETE:
                                if (this.n != null) {
                                    this.n.a();
                                    break;
                                }
                                break;
                            case IMPRESSION_TRACKING_START:
                                if (this.l != null) {
                                    this.l.b();
                                    break;
                                }
                                break;
                            case IMPRESSION_TRACKING_SUCCESS:
                                if (this.l != null) {
                                    this.l.d_();
                                    break;
                                }
                                break;
                            case REWARDED_AD_COMPLETE:
                                if (this.k != null) {
                                    this.k.i();
                                    break;
                                }
                                break;
                            default:
                                throw new en("Unspecified MRAID Javascript command");
                        }
                        this.f5465a.a(a2);
                    } else {
                        throw new en("Invalid state to execute this command");
                    }
                } catch (en e2) {
                    this.f5465a.a(a2, e2.getMessage());
                }
            }
        } catch (URISyntaxException unused) {
            this.f5465a.a(ep.UNSPECIFIED, "Mraid command sent an invalid URL");
        }
    }

    private void a(@NonNull fa faVar) {
        this.e = faVar;
        this.f5465a.a(this.e);
    }

    public final void c() {
        this.g.b();
        eq.a(this.b.getContext(), this.h);
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
    }
}
