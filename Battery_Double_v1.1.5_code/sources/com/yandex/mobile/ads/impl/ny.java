package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.List;

public final class ny {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<nz> f5663a;

    public ny(@NonNull List<nz> list) {
        this.f5663a = list;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f5663a.equals(((ny) obj).f5663a);
    }

    public final int hashCode() {
        return this.f5663a.hashCode();
    }
}
