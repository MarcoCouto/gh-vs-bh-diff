package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.b;
import java.util.HashMap;

public final class ue implements gv<VastRequestConfiguration, sk> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final b f5805a;

    public ue(@NonNull b bVar) {
        this.f5805a = bVar;
    }

    public final /* synthetic */ gu a(@Nullable qt qtVar, int i, @NonNull Object obj) {
        HashMap hashMap = new HashMap();
        String b = this.f5805a.b();
        String c = this.f5805a.c();
        String str = "page_id";
        if (TextUtils.isEmpty(c)) {
            c = "null";
        }
        hashMap.put(str, c);
        String str2 = "imp_id";
        if (TextUtils.isEmpty(b)) {
            b = "null";
        }
        hashMap.put(str2, b);
        if (i != -1) {
            hashMap.put("code", Integer.valueOf(i));
        }
        return new gu(gu.b.VAST_RESPONSE, hashMap);
    }

    public final /* synthetic */ gu a(Object obj) {
        HashMap hashMap = new HashMap();
        String b = this.f5805a.b();
        String c = this.f5805a.c();
        String str = "page_id";
        if (TextUtils.isEmpty(c)) {
            c = "null";
        }
        hashMap.put(str, c);
        String str2 = "imp_id";
        if (TextUtils.isEmpty(b)) {
            b = "null";
        }
        hashMap.put(str2, b);
        return new gu(gu.b.VAST_REQUEST, hashMap);
    }
}
