package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.widget.Button;
import android.widget.PopupMenu;
import com.yandex.mobile.ads.impl.nu.a;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.q;
import java.util.List;

public final class pq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5697a;
    @NonNull
    private final cg b;
    @NonNull
    private final af c;
    @NonNull
    private final pp d = new pp();
    @NonNull
    private final q e;

    public pq(@NonNull fc fcVar, @NonNull cg cgVar, @NonNull af afVar, @NonNull q qVar) {
        this.f5697a = fcVar;
        this.b = cgVar;
        this.c = afVar;
        this.e = qVar;
    }

    public final void a(@NonNull Context context, @NonNull nu nuVar) {
        PopupMenu popupMenu;
        Button h = this.c.c().h();
        if (h != null && VERSION.SDK_INT >= 11) {
            List b2 = nuVar.b();
            if (!b2.isEmpty()) {
                try {
                    cv cvVar = new cv(context, this.f5697a);
                    if (VERSION.SDK_INT >= 19) {
                        popupMenu = new PopupMenu(context, h, 5);
                    } else {
                        popupMenu = new PopupMenu(context, h);
                    }
                    Menu menu = popupMenu.getMenu();
                    for (int i = 0; i < b2.size(); i++) {
                        menu.add(0, i, 0, ((a) b2.get(i)).a());
                    }
                    pr prVar = new pr(context, cvVar, b2, this.b, this.e);
                    popupMenu.setOnMenuItemClickListener(prVar);
                    popupMenu.show();
                } catch (Exception unused) {
                }
            }
        }
    }
}
