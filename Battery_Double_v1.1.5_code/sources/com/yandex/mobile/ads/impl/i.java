package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.View;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

public final class i {

    /* renamed from: a reason: collision with root package name */
    private static final Comparator<Entry<Integer, Integer>> f5544a = new Comparator<Entry<Integer, Integer>>() {
        public final /* synthetic */ int compare(Object obj, Object obj2) {
            return ((Integer) ((Entry) obj).getValue()).compareTo((Integer) ((Entry) obj2).getValue());
        }
    };

    public static void a(View view, View view2) {
        Integer num;
        Bitmap c = dv.c(view2);
        if (c == null) {
            num = Integer.valueOf(0);
        } else {
            ArrayList arrayList = new ArrayList();
            int[][][] iArr = (int[][][]) Array.newInstance(int.class, new int[]{16, 16, 16});
            for (int i = 0; i < 16; i++) {
                for (int i2 = 0; i2 < 16; i2++) {
                    for (int i3 = 0; i3 < 16; i3++) {
                        iArr[i3][i2][i] = 0;
                    }
                }
            }
            int width = c.getWidth();
            int height = c.getHeight();
            int i4 = width * height;
            int[] iArr2 = new int[i4];
            c.getPixels(iArr2, 0, width, 0, 0, width, height);
            int i5 = 0;
            loop3:
            while (true) {
                if (i5 < height) {
                    for (int i6 = 0; i6 < width; i6++) {
                        int i7 = iArr2[(i5 * width) + i6];
                        if (Color.alpha(i7) != 255) {
                            arrayList.add(Integer.valueOf(i7));
                            if (((float) arrayList.size()) >= ((float) i4) * 0.1f) {
                                num = Integer.valueOf(0);
                                break loop3;
                            }
                        }
                        int red = Color.red(i7) / 16;
                        int green = Color.green(i7) / 16;
                        int blue = Color.blue(i7) / 16;
                        iArr[red][green][blue] = iArr[red][green][blue] + 1;
                    }
                    i5++;
                } else {
                    HashMap hashMap = new HashMap(4096);
                    for (int i8 = 0; i8 < 16; i8++) {
                        for (int i9 = 0; i9 < 16; i9++) {
                            for (int i10 = 0; i10 < 16; i10++) {
                                int i11 = iArr[i10][i9][i8];
                                if (i11 > 0) {
                                    hashMap.put(Integer.valueOf(Color.rgb(i10 * 16, i9 * 16, i8 * 16)), Integer.valueOf(i11));
                                }
                            }
                        }
                    }
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.addAll(hashMap.entrySet());
                    num = (Integer) ((Entry) Collections.max(arrayList2, f5544a)).getKey();
                }
            }
        }
        view.setBackgroundColor(num.intValue());
    }
}
