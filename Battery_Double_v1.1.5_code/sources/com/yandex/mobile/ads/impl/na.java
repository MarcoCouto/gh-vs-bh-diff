package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

final class na {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cv f5640a;

    na(@NonNull cv cvVar) {
        this.f5640a = cvVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f5640a.a(str);
        }
    }
}
