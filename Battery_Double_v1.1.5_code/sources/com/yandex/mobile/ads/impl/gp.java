package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

final class gp {

    /* renamed from: a reason: collision with root package name */
    private static final Set<String> f5517a = new HashSet(Collections.singletonList("gps"));
    private static final Set<String> b = new HashSet(Arrays.asList(new String[]{"gps", "passive"}));
    @Nullable
    private final LocationManager c;
    @NonNull
    private final gi d;

    gp(@NonNull Context context, @Nullable LocationManager locationManager) {
        this.c = locationManager;
        this.d = new gi(context);
    }

    @Nullable
    private Location b(@NonNull String str) {
        try {
            if (this.c != null) {
                Location lastKnownLocation = this.c.getLastKnownLocation(str);
                try {
                    new Object[1][0] = lastKnownLocation;
                    return lastKnownLocation;
                } catch (Throwable unused) {
                    return lastKnownLocation;
                }
            }
        } catch (Throwable unused2) {
        }
        return null;
    }

    @Nullable
    public final Location a(@NonNull String str) {
        boolean a2 = this.d.a();
        boolean b2 = this.d.b();
        boolean z = true;
        boolean z2 = !f5517a.contains(str);
        if (!b.contains(str) ? !z2 || !a2 : !z2 || !a2 || !b2) {
            z = false;
        }
        if (z) {
            return b(str);
        }
        return null;
    }
}
