package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class sq implements RequestListener<sk> {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ua f5772a;
    @NonNull
    private final sv b;
    @NonNull
    private final RequestListener<sk> c;

    class a implements RequestListener<List<VideoAd>> {
        @NonNull
        private final sk b;
        @NonNull
        private final RequestListener<sk> c;

        public final /* synthetic */ void onSuccess(@NonNull Object obj) {
            List list = (List) obj;
            sq.this.f5772a.a();
            this.c.onSuccess(new sk(new sj(this.b.a().a(), list), this.b.b()));
        }

        a(sk skVar, @NonNull RequestListener<sk> requestListener) {
            this.b = skVar;
            this.c = requestListener;
        }

        public final void onFailure(@NonNull VideoAdError videoAdError) {
            sq.this.f5772a.a(videoAdError);
            this.c.onFailure(videoAdError);
        }
    }

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        sk skVar = (sk) obj;
        this.b.a(skVar.a().b(), new a(skVar, this.c));
    }

    sq(@NonNull Context context, @NonNull ub ubVar, @NonNull RequestListener<sk> requestListener) {
        this.c = requestListener;
        this.f5772a = new ua(context, ubVar);
        this.b = new sv(context, ubVar);
    }

    public final void onFailure(@NonNull VideoAdError videoAdError) {
        this.c.onFailure(videoAdError);
    }
}
