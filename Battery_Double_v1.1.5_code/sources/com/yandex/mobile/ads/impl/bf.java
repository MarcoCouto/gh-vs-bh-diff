package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;
import java.util.Map;

public class bf {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5389a;
    @NonNull
    private final Map<String, String> b;
    @Nullable
    private final List<String> c;
    @Nullable
    private final List<String> d;
    @Nullable
    private final List<String> e;

    public static class a {
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final String f5390a;
        /* access modifiers changed from: private */
        @NonNull
        public final Map<String, String> b;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> c;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> d;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> e;

        public a(@NonNull String str, @NonNull Map<String, String> map) {
            this.f5390a = str;
            this.b = map;
        }

        @NonNull
        public final bf a() {
            return new bf(this, 0);
        }

        @NonNull
        public final a a(@Nullable List<String> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<String> list) {
            this.e = list;
            return this;
        }
    }

    /* synthetic */ bf(a aVar, byte b2) {
        this(aVar);
    }

    private bf(@NonNull a aVar) {
        this.f5389a = aVar.f5390a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
    }

    @NonNull
    public final String a() {
        return this.f5389a;
    }

    @NonNull
    public final Map<String, String> b() {
        return this.b;
    }

    @Nullable
    public final List<String> c() {
        return this.c;
    }

    @Nullable
    public final List<String> d() {
        return this.d;
    }

    @Nullable
    public final List<String> e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bf bfVar = (bf) obj;
        if (!this.f5389a.equals(bfVar.f5389a) || !this.b.equals(bfVar.b)) {
            return false;
        }
        if (this.c == null ? bfVar.c != null : !this.c.equals(bfVar.c)) {
            return false;
        }
        if (this.d == null ? bfVar.d != null : !this.d.equals(bfVar.d)) {
            return false;
        }
        if (this.e != null) {
            return this.e.equals(bfVar.e);
        }
        return bfVar.e == null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((this.f5389a.hashCode() * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31;
        if (this.e != null) {
            i = this.e.hashCode();
        }
        return hashCode + i;
    }
}
