package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class iw {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final x<String> f5567a;
    @NonNull
    private final String b;
    @NonNull
    private final fc c;

    iw(@NonNull x<String> xVar, @NonNull String str, @NonNull fc fcVar) {
        this.f5567a = xVar;
        this.b = str;
        this.c = fcVar;
    }

    @NonNull
    public final x<String> a() {
        return this.f5567a;
    }

    @NonNull
    public final fc b() {
        return this.c;
    }

    @NonNull
    public final String c() {
        return this.b;
    }
}
