package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class gj implements gf {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5511a = new Object();
    private static volatile gj b;
    @NonNull
    private final gn c;

    private gj(@NonNull Context context) {
        this.c = new gn(context);
    }

    @NonNull
    public static gj a(@NonNull Context context) {
        if (b == null) {
            synchronized (f5511a) {
                if (b == null) {
                    b = new gj(context);
                }
            }
        }
        return b;
    }

    @Nullable
    public final Location a() {
        Location location;
        synchronized (f5511a) {
            location = null;
            gm a2 = this.c.a();
            if (a2 != null && a2.a()) {
                location = a2.b();
                this.c.b();
            }
        }
        return location;
    }
}
