package com.yandex.mobile.ads.impl;

import android.os.Handler;
import java.util.concurrent.Executor;

public final class qn implements qu {

    /* renamed from: a reason: collision with root package name */
    private final Executor f5716a;

    private class a implements Runnable {
        private final qr b;
        private final qt c;
        private final Runnable d;

        public a(qr qrVar, qt qtVar, Runnable runnable) {
            this.b = qrVar;
            this.c = qtVar;
            this.d = runnable;
        }

        public final void run() {
            if (this.b.j()) {
                this.b.g();
                return;
            }
            if (this.c.c == null) {
                this.b.b(this.c.f5724a);
            } else {
                this.b.b(this.c.c);
            }
            if (!this.c.d) {
                this.b.g();
            }
            if (this.d != null) {
                this.d.run();
            }
        }
    }

    public qn(final Handler handler) {
        this.f5716a = new Executor() {
            public final void execute(Runnable runnable) {
                handler.post(runnable);
            }
        };
    }

    public final void a(qr<?> qrVar, qt<?> qtVar) {
        a(qrVar, qtVar, null);
    }

    public final void a(qr<?> qrVar, qt<?> qtVar, Runnable runnable) {
        qrVar.r();
        this.f5716a.execute(new a(qrVar, qtVar, runnable));
    }

    public final void a(qr<?> qrVar, re reVar) {
        this.f5716a.execute(new a(qrVar, qt.a(reVar), null));
    }
}
