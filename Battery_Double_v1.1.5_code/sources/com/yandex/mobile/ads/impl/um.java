package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import com.yandex.mobile.ads.video.models.ad.VideoAdConfigurator;
import com.yandex.mobile.ads.video.models.ad.WrapperConfigurationProvider;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class um {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final uj f5809a = new uj();
    @NonNull
    private final uk b;
    @NonNull
    private final VideoAd c;
    @NonNull
    private final WrapperConfigurationProvider d;

    public um(@NonNull VideoAd videoAd) {
        this.c = videoAd;
        this.d = new WrapperConfigurationProvider(videoAd);
        this.b = new uk(videoAd);
    }

    @NonNull
    public final List<VideoAd> a(@NonNull List<VideoAd> list) {
        sl wrapperConfiguration = this.d.getWrapperConfiguration();
        if (wrapperConfiguration != null) {
            if (!wrapperConfiguration.b()) {
                list = uj.a(list).a();
            }
            if (!wrapperConfiguration.a() && !list.isEmpty()) {
                list = Collections.singletonList(list.get(0));
            }
            for (VideoAd a2 : list) {
                this.b.a(a2);
            }
            Map trackingEvents = this.c.getTrackingEvents();
            for (VideoAd videoAdConfigurator : list) {
                new VideoAdConfigurator(videoAdConfigurator).addTrackingEvents(trackingEvents);
            }
        }
        return list;
    }
}
