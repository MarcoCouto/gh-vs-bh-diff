package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import org.json.JSONObject;

final class em {

    /* renamed from: a reason: collision with root package name */
    private final eg f5464a;

    em(@NonNull eg egVar) {
        this.f5464a = egVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str) {
        this.f5464a.b(str);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        b("notifyReadyEvent();");
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull ep epVar, @NonNull String str) {
        StringBuilder sb = new StringBuilder("notifyErrorEvent(");
        sb.append(JSONObject.quote(epVar.a()));
        sb.append(", ");
        sb.append(JSONObject.quote(str));
        sb.append(")");
        b(sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull ep epVar) {
        StringBuilder sb = new StringBuilder("nativeCallComplete(");
        sb.append(JSONObject.quote(epVar.a()));
        sb.append(")");
        b(sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull ey... eyVarArr) {
        if (eyVarArr.length > 0) {
            String str = "";
            StringBuilder sb = new StringBuilder("fireChangeEvent({");
            for (ey eyVar : eyVarArr) {
                sb.append(str);
                sb.append(eyVar.a());
                str = ", ";
            }
            sb.append("})");
            b(sb.toString());
        }
    }

    private void b(@NonNull String str) {
        c(String.format("window.mraidbridge.%s", new Object[]{str}));
    }

    private void c(@NonNull String str) {
        this.f5464a.loadUrl("javascript: ".concat(String.valueOf(str)));
        new Object[1][0] = str;
    }
}
