package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.AdRequestError;
import java.util.Map;

public final class ig implements ec {

    /* renamed from: a reason: collision with root package name */
    private final eo f5546a;
    private final ec b;

    ig(@NonNull eo eoVar, @NonNull ec ecVar) {
        this.f5546a = eoVar;
        this.b = ecVar;
    }

    public final void onAdLoaded() {
        this.f5546a.a();
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        this.b.onAdFailedToLoad(adRequestError);
    }

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        this.b.a(webView, map);
    }

    public final void a(@NonNull String str) {
        this.f5546a.b(str);
    }

    public final void b(boolean z) {
        this.f5546a.a(z);
    }
}
