package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;
import java.util.Map;

public final class ch implements cg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cn f5416a = new cn();
    @NonNull
    private final gs b;
    @NonNull
    private final x c;
    @Nullable
    private final fc d;
    @Nullable
    private final a e;

    public ch(@NonNull Context context, @NonNull x xVar, @Nullable fc fcVar, @Nullable a aVar) {
        this.c = xVar;
        this.d = fcVar;
        this.e = aVar;
        this.b = gs.a(context);
    }

    public final void a(@NonNull b bVar) {
        this.b.a(b(bVar, new HashMap()));
    }

    public final void a(@NonNull b bVar, @NonNull Map<String, Object> map) {
        this.b.a(b(bVar, map));
    }

    private gu b(@NonNull b bVar, @NonNull Map<String, Object> map) {
        dr drVar = new dr(map);
        com.yandex.mobile.ads.b a2 = this.c.a();
        if (a2 != null) {
            drVar.a("ad_type", a2.a());
        } else {
            drVar.a("ad_type");
        }
        drVar.a("block_id", this.c.b());
        drVar.a("adapter", "Yandex");
        drVar.a("ad_type_format", this.c.c());
        drVar.a("product_type", this.c.d());
        if (this.d != null) {
            map.putAll(cn.a(this.d.c()));
        }
        if (this.e != null) {
            map.putAll(this.e.a());
        }
        return new gu(bVar, drVar.a());
    }
}
