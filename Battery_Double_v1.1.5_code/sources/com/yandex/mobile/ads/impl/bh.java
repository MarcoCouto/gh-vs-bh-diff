package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public class bh implements Parcelable {
    public static final Creator<bh> CREATOR = new Creator<bh>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new bh[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new bh(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final int f5392a;
    @NonNull
    private final String b;

    public int describeContents() {
        return 0;
    }

    public bh(int i, @NonNull String str) {
        this.f5392a = i;
        this.b = str;
    }

    public final int a() {
        return this.f5392a;
    }

    @NonNull
    public final String b() {
        return this.b;
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeInt(this.f5392a);
        parcel.writeString(this.b);
    }

    protected bh(@NonNull Parcel parcel) {
        this.f5392a = parcel.readInt();
        this.b = parcel.readString();
    }
}
