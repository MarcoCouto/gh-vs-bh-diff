package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class bb {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5386a;
    @NonNull
    private final cn b = new cn();

    public bb(@NonNull fc fcVar) {
        this.f5386a = fcVar;
    }

    public final void a(@NonNull Context context, @NonNull bf bfVar, @NonNull Map<String, Object> map) {
        a(context, b.CLICK, bfVar, map);
    }

    public final void b(@NonNull Context context, @NonNull bf bfVar, @NonNull Map<String, Object> map) {
        a(context, b.IMPRESSION_TRACKING_START, bfVar, map);
        a(context, b.IMPRESSION_TRACKING_SUCCESS, bfVar, map);
    }

    public final void c(@NonNull Context context, @NonNull bf bfVar, @NonNull Map<String, Object> map) {
        a(context, b.ADAPTER_AUTO_REFRESH, bfVar, map);
    }

    public final void a(@NonNull Context context, @NonNull bf bfVar) {
        a(context, b.ADAPTER_REQUEST, bfVar, Collections.emptyMap());
    }

    public final void d(@NonNull Context context, @NonNull bf bfVar, @NonNull Map<String, Object> map) {
        a(context, b.ADAPTER_RESPONSE, bfVar, map);
    }

    public final void e(@NonNull Context context, @NonNull bf bfVar, @NonNull Map<String, Object> map) {
        a(context, b.ADAPTER_ACTION, bfVar, map);
    }

    public final void f(@NonNull Context context, @NonNull bf bfVar, @NonNull Map<String, Object> map) {
        a(context, b.ADAPTER_INVALID, bfVar, map);
    }

    public final void a(@NonNull Context context, @NonNull bf bfVar, @Nullable x xVar) {
        HashMap hashMap = new HashMap();
        new bc();
        hashMap.put("reward_info", bc.a(xVar));
        a(context, b.REWARD, bfVar, hashMap);
    }

    private void a(@NonNull Context context, @NonNull b bVar, @NonNull bf bfVar, @NonNull Map<String, Object> map) {
        Map a2 = a(bfVar);
        a2.putAll(map);
        gs.a(context).a(new gu(bVar, a2));
    }

    @NonNull
    private Map<String, Object> a(@NonNull bf bfVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("block_id", this.f5386a.e());
        hashMap.put("ad_type", this.f5386a.a().a());
        hashMap.put("adapter", bfVar.a());
        hashMap.put("adapter_parameters", bfVar.b());
        hashMap.putAll(cn.a(this.f5386a.c()));
        return hashMap;
    }
}
