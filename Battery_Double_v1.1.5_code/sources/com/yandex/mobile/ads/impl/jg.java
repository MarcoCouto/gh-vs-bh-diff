package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;

public interface jg {
    @NonNull
    View a(@NonNull View view, @NonNull x<String> xVar);

    void a();

    void a(@NonNull Context context, @NonNull o oVar, @NonNull al alVar);

    void a(@NonNull RelativeLayout relativeLayout);

    void a(boolean z);

    void b();

    boolean c();
}
