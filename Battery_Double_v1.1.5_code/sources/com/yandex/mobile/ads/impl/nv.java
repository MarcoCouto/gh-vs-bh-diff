package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class nv extends ns {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5660a;
    @NonNull
    private final String b;
    @NonNull
    private final String c;

    public nv(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull String str4) {
        super(str);
        this.f5660a = str2;
        this.b = str3;
        this.c = str4;
    }

    @NonNull
    public final String b() {
        return this.f5660a;
    }

    @NonNull
    public final String c() {
        return this.b;
    }

    @NonNull
    public final String d() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        nv nvVar = (nv) obj;
        if (this.f5660a.equals(nvVar.f5660a) && this.b.equals(nvVar.b)) {
            return this.c.equals(nvVar.c);
        }
        return false;
    }

    public final int hashCode() {
        return (((((super.hashCode() * 31) + this.f5660a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }
}
