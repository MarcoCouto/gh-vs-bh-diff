package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.fg.a;
import java.util.Collection;

public final class hc {
    public static <T> T[] a(T[] tArr) {
        return tArr == null ? (Object[]) new Object[0] : tArr;
    }

    public static boolean a(Collection... collectionArr) {
        Collection[] collectionArr2;
        for (Collection collection : (Collection[]) a((T[]) collectionArr)) {
            if (collection == null || collection.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(int i) {
        return VERSION.SDK_INT >= i;
    }

    public static boolean a(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    public static synchronized void a(@NonNull Context context, @NonNull fg fgVar) {
        synchronized (hc.class) {
            Editor putBoolean = context.getSharedPreferences("YadPreferenceFile", 0).edit().putString("SdkConfigurationLibraryVersion", fgVar.j()).putBoolean("SdkConfigurationSensitiveModeDisabled", fgVar.c()).putLong("SdkConfigurationExpiredDate", fgVar.a()).putString("SdkConfigurationMraidUrl", fgVar.e()).putBoolean("CustomClickHandlingEnabled", fgVar.b()).putBoolean("UrlCorrectionEnabled", fgVar.l());
            Boolean k = fgVar.k();
            Boolean f = fgVar.f();
            Boolean valueOf = Boolean.valueOf(fgVar.g());
            Boolean valueOf2 = Boolean.valueOf(fgVar.h());
            Boolean valueOf3 = Boolean.valueOf(fgVar.i());
            Boolean d = fgVar.d();
            a(putBoolean, "SdkConfigurationVisibilityErrorIndicatorEnabled", k);
            a(putBoolean, "SdkConfigurationMediationSensitiveModeDisabled", f);
            a(putBoolean, "SdkConfigurationCustomUserAgentEnabled", valueOf);
            a(putBoolean, "SdkConfigurationFusedLocationProviderDisabled", valueOf2);
            a(putBoolean, "SdkConfigurationLockScreenEnabled", valueOf3);
            a(putBoolean, "SdkConfigurationUserConsent", d);
            putBoolean.apply();
        }
    }

    @Nullable
    public static synchronized fg b(@NonNull Context context) {
        fg fgVar;
        synchronized (hc.class) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("YadPreferenceFile", 0);
            long j = sharedPreferences.getLong("SdkConfigurationExpiredDate", 0);
            Boolean a2 = a(sharedPreferences, "SdkConfigurationVisibilityErrorIndicatorEnabled");
            fgVar = null;
            if (j != 0) {
                Boolean a3 = a(sharedPreferences, "SdkConfigurationMediationSensitiveModeDisabled");
                boolean z = sharedPreferences.getBoolean("SdkConfigurationCustomUserAgentEnabled", false);
                boolean z2 = sharedPreferences.getBoolean("SdkConfigurationFusedLocationProviderDisabled", false);
                boolean z3 = sharedPreferences.getBoolean("SdkConfigurationLockScreenEnabled", false);
                Boolean a4 = a(sharedPreferences, "SdkConfigurationUserConsent");
                String string = sharedPreferences.getString("SdkConfigurationLibraryVersion", null);
                String string2 = sharedPreferences.getString("SdkConfigurationMraidUrl", null);
                boolean z4 = sharedPreferences.getBoolean("CustomClickHandlingEnabled", false);
                boolean z5 = sharedPreferences.getBoolean("SdkConfigurationSensitiveModeDisabled", false);
                fgVar = new a().b(string).c(a4).a(j).b(a3).d(z).e(z2).f(z3).c(z5).a(string2).a(z4).a(a2).b(sharedPreferences.getBoolean("UrlCorrectionEnabled", false)).a();
            }
        }
        return fgVar;
    }

    private static void a(@NonNull Editor editor, @NonNull String str, @Nullable Boolean bool) {
        if (bool != null) {
            editor.putBoolean(str, bool.booleanValue());
        } else {
            editor.remove(str);
        }
    }

    @Nullable
    private static Boolean a(@NonNull SharedPreferences sharedPreferences, @NonNull String str) {
        if (sharedPreferences.contains(str)) {
            return Boolean.valueOf(sharedPreferences.getBoolean(str, false));
        }
        return null;
    }
}
