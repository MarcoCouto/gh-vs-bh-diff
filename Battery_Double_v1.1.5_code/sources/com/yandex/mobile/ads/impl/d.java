package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.hs.a;

public final class d extends hs {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    final int f5432a;
    @VisibleForTesting
    int b;
    @NonNull
    private final al g;
    @Nullable
    private al h;
    private boolean i = true;

    d(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar, @NonNull al alVar) {
        super(context, xVar, fcVar);
        this.g = alVar;
        if (k()) {
            this.f5432a = alVar.b(context);
            this.b = alVar.a(context);
            return;
        }
        this.f5432a = xVar.f() == 0 ? alVar.b(context) : xVar.f();
        this.b = xVar.g();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.i) {
            this.h = new al(this.f5432a, this.b, this.g.c());
            boolean a2 = iq.a(getContext(), this.h, this.g);
            if (this.e != null && a2) {
                this.e.a(this, j());
            }
            if (this.e != null) {
                if (a2) {
                    this.e.onAdLoaded();
                } else {
                    this.e.onAdFailedToLoad(v.c);
                }
            }
            this.i = false;
        }
    }

    /* access modifiers changed from: protected */
    public final String b() {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        if (this.f.u()) {
            str = eh.a(this.f5432a);
        } else {
            str = "";
        }
        sb.append(str);
        Context context = getContext();
        int b2 = this.g.b(context);
        int a2 = this.g.a(context);
        if (k()) {
            str2 = eh.a(b2, a2);
        } else {
            str2 = "";
        }
        sb.append(str2);
        sb.append(super.b());
        return sb.toString();
    }

    @Nullable
    public final al c() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, String str) {
        super.a(i2, str);
        this.b = i2;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"AddJavascriptInterface"})
    public final void a(@NonNull Context context) {
        addJavascriptInterface(new a(context), "AdPerformActionsJSI");
    }

    @VisibleForTesting
    private boolean k() {
        Context context = getContext();
        return i() && this.f.f() == 0 && this.f.g() == 0 && this.g.b(context) > 0 && this.g.a(context) > 0;
    }
}
