package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class gl {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5513a;
    @NonNull
    private final he b = new he();

    gl(@NonNull Context context) {
        this.f5513a = context.getApplicationContext();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final gk a() {
        Class a2 = he.a("com.google.android.gms.location.LocationServices");
        if (a2 != null) {
            Object a3 = he.a(a2, "getFusedLocationProviderClient", this.f5513a);
            if (a3 != null) {
                return new gk(a3);
            }
        }
        return null;
    }
}
