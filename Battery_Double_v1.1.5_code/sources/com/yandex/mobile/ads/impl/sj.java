package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class sj implements Parcelable {
    public static final Creator<sj> CREATOR = new Creator<sj>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new sj[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new sj(parcel);
        }
    };
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5769a;
    @NonNull
    private final List<VideoAd> b;

    public final int describeContents() {
        return 0;
    }

    public sj(@NonNull String str, @NonNull List<VideoAd> list) {
        this.f5769a = str;
        this.b = list;
    }

    @NonNull
    public final String a() {
        return this.f5769a;
    }

    @NonNull
    public final List<VideoAd> b() {
        return this.b;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.f5769a);
        parcel.writeTypedList(this.b);
    }

    protected sj(@NonNull Parcel parcel) {
        this.f5769a = parcel.readString();
        this.b = parcel.createTypedArrayList(VideoAd.CREATOR);
    }
}
