package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.y;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class oo {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ds f5678a;
    @NonNull
    private final oq b = new oq(this.f5678a);

    public oo(@NonNull ds dsVar) {
        this.f5678a = dsVar;
    }

    @Nullable
    private String a(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, y {
        return this.f5678a.a(b(jSONObject, str));
    }

    @Nullable
    private static String b(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, y {
        if (jSONObject.has(str)) {
            return oh.a(jSONObject, str);
        }
        return null;
    }

    @NonNull
    public final nm a(@NonNull JSONObject jSONObject) throws JSONException, y {
        List list;
        JSONArray optJSONArray = jSONObject.optJSONArray("actions");
        cz czVar = null;
        if (optJSONArray != null) {
            list = new ArrayList();
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                op a2 = this.b.a(jSONObject2);
                if (a2 != null) {
                    list.add(a2.a(jSONObject2));
                }
            }
        } else {
            list = null;
        }
        String a3 = a(jSONObject, "falseClickUrl");
        Long valueOf = Long.valueOf(jSONObject.optLong("falseClickInterval", 0));
        if (!(a3 == null || valueOf == null)) {
            czVar = new cz(a3, valueOf.longValue());
        }
        return new nm(list, czVar, a(jSONObject, "trackingUrl"), a(jSONObject, "url"));
    }
}
