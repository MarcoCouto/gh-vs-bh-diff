package com.yandex.mobile.ads.impl;

import android.graphics.RectF;
import android.support.annotation.Nullable;

public final class ew {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final RectF f5477a;
    private final int b;

    public ew(int i, @Nullable RectF rectF) {
        this.b = i;
        this.f5477a = rectF;
    }

    public final int a() {
        return this.b;
    }

    @Nullable
    public final RectF b() {
        return this.f5477a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ew ewVar = (ew) obj;
        if (this.b != ewVar.b) {
            return false;
        }
        if (this.f5477a != null) {
            return this.f5477a.equals(ewVar.f5477a);
        }
        return ewVar.f5477a == null;
    }

    public final int hashCode() {
        return ((this.f5477a != null ? this.f5477a.hashCode() : 0) * 31) + this.b;
    }
}
