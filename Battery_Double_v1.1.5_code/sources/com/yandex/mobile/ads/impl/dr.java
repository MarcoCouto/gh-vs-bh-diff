package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;

public final class dr {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Map<String, Object> f5443a;

    public dr(@NonNull Map<String, Object> map) {
        this.f5443a = map;
    }

    public final void a(@NonNull String str, @Nullable Object obj) {
        if (obj == null) {
            a(str);
        } else {
            this.f5443a.put(str, obj);
        }
    }

    public final void b(@NonNull String str, @Nullable Object obj) {
        if (obj != null) {
            this.f5443a.put(str, obj);
        }
    }

    public final void a(@NonNull String str) {
        this.f5443a.put(str, "undefined");
    }

    public final void a(@NonNull Map<String, Object> map) {
        this.f5443a.putAll(map);
    }

    @NonNull
    public final Map<String, Object> a() {
        return this.f5443a;
    }
}
