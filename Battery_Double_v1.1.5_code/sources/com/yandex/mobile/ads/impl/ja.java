package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class ja {
    @NonNull
    public static iz a(@NonNull x xVar, @NonNull View view, boolean z) {
        if (z) {
            return new jc(xVar, view, new jd());
        }
        return new jb(view);
    }
}
