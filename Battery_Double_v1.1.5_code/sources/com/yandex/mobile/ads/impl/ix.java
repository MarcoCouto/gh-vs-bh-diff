package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.webkit.WebSettings;
import com.yandex.mobile.ads.impl.hs.a;
import java.io.File;

public class ix extends hs {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ei f5568a = new ei(this);

    public ix(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar) {
        super(context, xVar, fcVar);
    }

    /* access modifiers changed from: protected */
    public final void a_(Context context) {
        super.a_(context);
        WebSettings settings = getSettings();
        settings.setAppCachePath(new File(getContext().getCacheDir(), "com.yandex.mobile.ads.cache").getAbsolutePath());
        settings.setDatabasePath(getContext().getDatabasePath("com.yandex.mobile.ads.db").getAbsolutePath());
        settings.setAppCacheMaxSize(8388608);
        settings.setAppCacheEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setCacheMode(-1);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"AddJavascriptInterface"})
    public final void a(@NonNull Context context) {
        addJavascriptInterface(new a(context), "AdPerformActionsJSI");
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f5568a.a(j());
    }
}
