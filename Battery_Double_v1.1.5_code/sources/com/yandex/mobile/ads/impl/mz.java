package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class mz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cv f5638a;
    @NonNull
    private final x b;
    @NonNull
    private final db c;
    @NonNull
    private final cg d;

    public mz(@NonNull cv cvVar, @NonNull x xVar, @NonNull db dbVar, @NonNull cg cgVar) {
        this.f5638a = cvVar;
        this.b = xVar;
        this.c = dbVar;
        this.d = cgVar;
    }

    public final void a(@Nullable String str) {
        this.f5638a.a(str, this.b, this.c, this.d);
    }
}
