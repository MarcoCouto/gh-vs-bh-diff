package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.y;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class oq {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final oj f5679a;
    @NonNull
    private final Map<String, op> b = new HashMap<String, op>() {
        {
            put("close", new or());
            put("deeplink", new ou(oq.this.f5679a));
            put("feedback", new os(oq.this.f5679a));
            put("shortcut", new ot(oq.this.f5679a));
        }
    };

    public oq(@NonNull ds dsVar) {
        this.f5679a = new oj(dsVar);
    }

    @Nullable
    public final op a(@NonNull JSONObject jSONObject) throws JSONException, y {
        return (op) this.b.get(oh.a(jSONObject, "type"));
    }
}
