package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ai.a;

final class sd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5752a;
    @NonNull
    private final fe b = new fe();

    public sd(@NonNull fc fcVar) {
        this.f5752a = fcVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull final sc scVar) {
        new ai(context, this.f5752a).a(context, new a() {
            public final void a() {
                scVar.a();
            }

            public final void a(@NonNull re reVar) {
                scVar.b();
            }
        });
    }
}
