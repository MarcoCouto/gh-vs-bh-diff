package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import com.yandex.mobile.ads.nativeads.a;
import com.yandex.mobile.ads.nativeads.af;

public final class lv implements OnClickListener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ni f5612a;
    @NonNull
    private final a b;
    @NonNull
    private final af c;
    @Nullable
    private final nm d;
    @Nullable
    private final as e;

    public lv(@NonNull ni niVar, @NonNull a aVar, @NonNull af afVar, @Nullable nm nmVar, @Nullable as asVar) {
        this.f5612a = niVar;
        this.b = aVar;
        this.c = afVar;
        this.d = nmVar;
        this.e = asVar;
    }

    public final void onClick(View view) {
        if (this.d != null && this.f5612a.e()) {
            if (this.e != null) {
                this.e.c();
            }
            this.b.a(view.getContext(), this.d, this.c);
        }
    }
}
