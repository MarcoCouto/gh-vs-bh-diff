package com.yandex.mobile.ads.impl;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public final class gh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gd f5509a = new gd();

    @Nullable
    public static Location a(@NonNull List<Location> list) {
        Location location = null;
        for (Location location2 : list) {
            if (gd.a(location2, location)) {
                location = location2;
            }
        }
        return location;
    }
}
