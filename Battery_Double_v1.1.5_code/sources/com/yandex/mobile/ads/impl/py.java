package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class py implements cq<nq> {

    /* renamed from: a reason: collision with root package name */
    private final bv<nq> f5704a;

    public final boolean a() {
        return true;
    }

    public py(@NonNull Context context) {
        this.f5704a = new og(context);
    }

    @Nullable
    public final /* synthetic */ Object a(@NonNull qq qqVar) {
        return (nq) this.f5704a.b(qqVar);
    }
}
