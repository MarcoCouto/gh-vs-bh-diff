package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class fe {
    public static boolean a(@NonNull Context context) {
        fg a2 = ff.a().a(context);
        return a2 == null || !a2.c();
    }
}
