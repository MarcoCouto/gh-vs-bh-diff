package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface dh {

    public enum a {
        ;
        

        /* renamed from: a reason: collision with root package name */
        public static final int f5438a = 1;
        public static final int b = 2;

        static {
            c = new int[]{f5438a, b};
        }
    }

    @NonNull
    int a();

    @NonNull
    String a(@NonNull Context context, @NonNull fc fcVar);

    @Nullable
    String a(@NonNull fc fcVar);
}
