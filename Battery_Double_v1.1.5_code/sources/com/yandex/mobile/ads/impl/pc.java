package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class pc implements oy<no> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final pi f5689a = new pi();
    @NonNull
    private final ph b;

    public pc(@NonNull Context context) {
        this.b = new ph(context);
    }

    @Nullable
    private static <T> T a(@NonNull JSONObject jSONObject, @NonNull String str, @NonNull pg<T> pgVar) throws JSONException, y {
        if (a(jSONObject, str)) {
            return pgVar.a(jSONObject.getJSONObject(str));
        }
        return null;
    }

    private static boolean a(@NonNull JSONObject jSONObject, @NonNull String str) {
        return jSONObject.has(str) && !jSONObject.isNull(str);
    }

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, y {
        if (a(jSONObject, "value")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("value");
            nn nnVar = (nn) a(jSONObject2, "media", this.f5689a);
            nl nlVar = (nl) a(jSONObject2, MessengerShareContentUtility.MEDIA_IMAGE, this.b);
            if (nnVar != null || nlVar != null) {
                return new no(nnVar, nlVar);
            }
            throw new y("Native Ad json has not required attributes");
        }
        throw new y("Native Ad json has not required attributes");
    }
}
