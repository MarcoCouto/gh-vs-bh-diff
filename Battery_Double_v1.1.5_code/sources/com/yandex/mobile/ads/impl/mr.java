package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.am.a;
import java.util.HashMap;
import java.util.Map;

public final class mr {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Map<a, String> f5630a = new HashMap<a, String>() {
        {
            put(a.AD_NOT_LOADED, "Ad is not loaded");
            put(a.APPLICATION_INACTIVE, "Screen is locked");
            put(a.INCONSISTENT_ASSET_VALUE, "Asset value %s doesn't match view value");
            put(a.NO_AD_VIEW, "No ad view");
            put(a.NO_VISIBLE_ADS, "No valid ads in ad unit");
            put(a.NO_VISIBLE_REQUIRED_ASSETS, "No visible required assets");
            put(a.NOT_ADDED_TO_HIERARCHY, "Ad view is not added to hierarchy");
            put(a.NOT_VISIBLE_FOR_PERCENT, "Ad is not visible for percent");
            put(a.REQUIRED_ASSET_CAN_NOT_BE_VISIBLE, "Required asset %s is not visible in ad view");
            put(a.REQUIRED_ASSET_IS_NOT_SUBVIEW, "Required asset %s is not subview of ad view");
            put(a.SUCCESS, "Unknown error, that shouldn't happen");
            put(a.SUPERVIEW_HIDDEN, "Ad view is hidden");
            put(a.TOO_SMALL, "View is too small");
        }
    };

    @NonNull
    public final String a(@NonNull am amVar) {
        String a2 = amVar.a();
        String str = (String) this.f5630a.get(amVar.b());
        if (str == null) {
            return "Visibility error";
        }
        return String.format(str, new Object[]{a2});
    }
}
