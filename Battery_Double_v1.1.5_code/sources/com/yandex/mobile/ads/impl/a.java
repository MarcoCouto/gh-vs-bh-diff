package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.webkit.WebView;
import android.widget.RelativeLayout.LayoutParams;
import com.yandex.mobile.ads.AdEventListener;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.VideoController;
import com.yandex.mobile.ads.impl.hq.d;
import com.yandex.mobile.ads.mediation.banner.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

public final class a extends h {
    @NonNull
    private final c g;
    @NonNull
    private final VideoController h;
    @NonNull
    private final dx i;
    @NonNull
    private final ji j;
    @Nullable
    private e k;
    @Nullable
    private b l;
    @Nullable
    private b m;
    /* access modifiers changed from: private */
    public final OnPreDrawListener n = new OnPreDrawListener() {
        public final boolean onPreDraw() {
            new StringBuilder("onPreDraw(), clazz = ").append(this);
            a.this.C();
            a.this.f5352a.postDelayed(new Runnable() {
                public final void run() {
                    a.this.c(false);
                }
            }, 50);
            return true;
        }
    };

    public final /* bridge */ /* synthetic */ void a(@NonNull Intent intent) {
        super.a(intent);
    }

    public final /* bridge */ /* synthetic */ void b(int i2) {
        super.b(i2);
    }

    public final /* bridge */ /* synthetic */ void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        super.onAdFailedToLoad(adRequestError);
    }

    public a(@NonNull Context context, @NonNull e eVar, @NonNull c cVar) {
        super(context, new b(eVar), com.yandex.mobile.ads.b.BANNER);
        this.g = cVar;
        eVar.setHorizontalScrollBarEnabled(false);
        eVar.setVerticalScrollBarEnabled(false);
        eVar.setVisibility(8);
        eVar.setBackgroundColor(0);
        this.k = eVar;
        this.i = new dx();
        this.h = new VideoController(this.i);
        this.j = new ji();
    }

    @Nullable
    public final e a() {
        return this.k;
    }

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        if (webView != null) {
            final d dVar = (d) webView;
            if (this.k != null && c(dVar.c())) {
                this.k.setVisibility(0);
                this.f5352a.post(new Runnable() {
                    public final void run() {
                        e a2 = a.this.a();
                        if (a2 != null && a2.indexOfChild(dVar) == -1) {
                            LayoutParams a3 = d.a(a.this.b, dVar.c());
                            a.a(a.this, a2, dVar);
                            a2.addView(dVar, a3);
                            dv.a((View) dVar, a.this.n);
                        }
                    }
                });
                super.a(webView, (Map) map);
            }
        }
    }

    private boolean c(@Nullable al alVar) {
        if (alVar != null) {
            al b = this.e.b();
            if (b != null) {
                return a(alVar, b);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        if (this.k == null) {
            return false;
        }
        return dv.a(this.b, this.k.findViewById(2));
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i2) {
        if (this.k != null) {
            return dv.a(this.k.findViewById(2), i2);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        x y = y();
        al e = y != null ? y.e() : null;
        return e != null && c(e);
    }

    public final void d() {
        super.d();
        this.g.a((AdEventListener) null);
        if (this.k != null) {
            c(true);
            this.k.setVisibility(8);
            dv.a((View) this.k);
            this.k = null;
        }
    }

    public final void e() {
        a(this.b, this.m, this.l);
        super.e();
    }

    /* access modifiers changed from: protected */
    public final boolean a(@NonNull al alVar) {
        return alVar.b(this.b) >= 0 && alVar.a(this.b) >= 0;
    }

    public final void onAdLoaded() {
        super.onAdLoaded();
        if (this.m != this.l) {
            a(this.b, this.m);
            this.m = this.l;
        }
    }

    private static void a(@NonNull Context context, @NonNull b... bVarArr) {
        for (b bVar : new HashSet(Arrays.asList(bVarArr))) {
            if (bVar != null) {
                bVar.a(context);
            }
        }
    }

    public final void a(@NonNull x<String> xVar) {
        super.a((x) xVar);
        this.l = ji.a(xVar).a(this);
        this.l.a(this.b, xVar);
    }

    public final void f() {
        this.g.a();
    }

    @NonNull
    public final VideoController g() {
        return this.h;
    }

    public final void a(@Nullable AdEventListener adEventListener) {
        super.a((AdEventListener) this.g);
        this.g.a(adEventListener);
    }

    @Nullable
    public final AdEventListener h() {
        return this.g.b();
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        e eVar = this.k;
        if (eVar != null && eVar.getChildCount() > 0) {
            int childCount = eVar.getChildCount() - (z ^ true ? 1 : 0);
            if (childCount > 0) {
                ArrayList arrayList = new ArrayList(childCount);
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = eVar.getChildAt(i2);
                    if (childAt instanceof ab) {
                        arrayList.add((ab) childAt);
                    }
                }
                eVar.removeViews(0, childCount);
                for (int i3 = 0; i3 < arrayList.size(); i3++) {
                    ((ab) arrayList.get(i3)).g();
                }
                arrayList.clear();
            }
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final hy a(@NonNull String str, @NonNull x<String> xVar, @NonNull al alVar, @NonNull ap apVar) {
        d dVar = new d(this.b, xVar, this.e, alVar);
        new hz();
        boolean a2 = hz.a(str);
        ic.a();
        return ic.a(a2).a(dVar, this, this.i, apVar);
    }

    static /* synthetic */ void a(a aVar, final e eVar, final d dVar) {
        al c = dVar.c();
        if (c == null || c.c() == com.yandex.mobile.ads.impl.al.a.FIXED) {
            dVar.setVisibility(0);
        } else if (dVar.d != null) {
            eVar.setBackgroundColor(dVar.d.intValue());
        } else {
            aVar.f5352a.postDelayed(new Runnable() {
                public final void run() {
                    i.a(eVar, dVar);
                    dVar.setVisibility(0);
                }
            }, 200);
        }
    }
}
