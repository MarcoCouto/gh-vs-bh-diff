package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.models.ad.Creative;
import com.yandex.mobile.ads.video.models.ad.CreativeConfigurator;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.HashMap;
import java.util.List;

final class uk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ul f5808a = new ul();
    @NonNull
    private final VideoAd b;

    uk(@NonNull VideoAd videoAd) {
        this.b = videoAd;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull VideoAd videoAd) {
        List<Creative> creatives = this.b.getCreatives();
        HashMap hashMap = new HashMap();
        for (Creative trackingEvents : creatives) {
            hashMap.putAll(trackingEvents.getTrackingEvents());
        }
        for (Creative creative : videoAd.getCreatives()) {
            CreativeConfigurator creativeConfigurator = new CreativeConfigurator(creative);
            creativeConfigurator.addIcons(ul.a(creative, creatives));
            creativeConfigurator.addTrackingEvents(hashMap);
        }
    }
}
