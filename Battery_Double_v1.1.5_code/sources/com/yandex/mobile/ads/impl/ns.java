package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public abstract class ns {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5657a;

    protected ns(@NonNull String str) {
        this.f5657a = str;
    }

    @NonNull
    public final String a() {
        return this.f5657a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f5657a.equals(((ns) obj).f5657a);
    }

    public int hashCode() {
        return this.f5657a.hashCode();
    }
}
