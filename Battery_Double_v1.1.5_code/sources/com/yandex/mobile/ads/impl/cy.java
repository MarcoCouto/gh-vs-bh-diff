package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.a;

public final class cy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5430a;
    @NonNull
    private final fc b;
    @Nullable
    private cx c;

    public cy(@NonNull Context context, @NonNull fc fcVar) {
        this.f5430a = context.getApplicationContext();
        this.b = fcVar;
    }

    public final void a(@Nullable cz czVar) {
        if (czVar != null) {
            this.c = new cx(this.f5430a, this.b, czVar);
        }
    }

    public final void a() {
        if (this.c != null) {
            this.c.a();
        }
    }

    public final void b() {
        if (this.c != null) {
            this.c.b();
        }
    }

    public final void c() {
        if (this.c != null) {
            this.c.c();
        }
    }

    public final void d() {
        if (this.c != null) {
            this.c.e();
        }
    }

    public final void e() {
        if (this.c != null) {
            this.c.f();
        }
    }

    public final void a(@NonNull a aVar) {
        if (this.c != null) {
            this.c.a(aVar);
        }
    }
}
