package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.i;

public final class kr {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5595a;
    @NonNull
    private final ap b;
    @NonNull
    private final ks c;

    public kr(@NonNull fc fcVar, @NonNull ap apVar, @NonNull ks ksVar) {
        this.f5595a = fcVar;
        this.b = apVar;
        this.c = ksVar;
    }

    public final ky a(@NonNull MediaView mediaView, @NonNull i iVar) {
        return this.c.a(mediaView, this.f5595a, iVar, this.b);
    }
}
