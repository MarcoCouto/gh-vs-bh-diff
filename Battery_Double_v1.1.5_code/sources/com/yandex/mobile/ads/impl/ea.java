package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ea extends WebViewClient {

    /* renamed from: a reason: collision with root package name */
    private final eb f5453a;

    public ea(@NonNull eb ebVar) {
        this.f5453a = ebVar;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.f5453a.d();
    }

    public boolean shouldOverrideUrlLoading(@NonNull WebView webView, @NonNull String str) {
        this.f5453a.a(webView.getContext(), str);
        return true;
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        Object[] objArr = {Integer.valueOf(i), str};
    }
}
