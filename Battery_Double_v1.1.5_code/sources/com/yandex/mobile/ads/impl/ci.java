package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;
import java.util.Map;

public final class ci implements cg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5417a;
    @NonNull
    private final x b;
    @NonNull
    private final gs c;
    @NonNull
    private final cn d = new cn();

    public ci(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar) {
        this.b = xVar;
        this.f5417a = fcVar;
        this.c = gs.a(context);
    }

    public final void a(@NonNull b bVar) {
        a(bVar, new HashMap());
    }

    public final void a(@NonNull b bVar, @NonNull Map<String, Object> map) {
        this.c.a(b(bVar, map));
    }

    @NonNull
    private gu b(@NonNull b bVar, @NonNull Map<String, Object> map) {
        dr drVar = new dr(map);
        com.yandex.mobile.ads.b a2 = this.b.a();
        if (a2 != null) {
            drVar.a("ad_type", a2.a());
        } else {
            drVar.a("ad_type");
        }
        drVar.a("ad_type_format", this.b.c());
        drVar.a("product_type", this.b.d());
        drVar.a("block_id", this.b.b());
        drVar.a("adapter", "Yandex");
        drVar.a(cn.a(this.f5417a.c()));
        return new gu(bVar, drVar.a());
    }
}
