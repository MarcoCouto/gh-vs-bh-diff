package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public final class bc {
    @NonNull
    static Map<String, String> a(@Nullable x xVar) {
        HashMap hashMap = new HashMap();
        if (xVar != null) {
            bi r = xVar.r();
            if (r != null) {
                hashMap.put("rewarding_side", r.c() ? "server_side" : "client_side");
            }
        }
        return hashMap;
    }
}
