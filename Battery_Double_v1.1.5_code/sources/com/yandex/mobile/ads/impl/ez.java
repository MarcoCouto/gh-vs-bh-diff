package com.yandex.mobile.ads.impl;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;

public final class ez implements ey {

    /* renamed from: a reason: collision with root package name */
    private final boolean f5479a = false;
    private final boolean b = false;
    private final boolean c = false;
    private final boolean d = false;
    private final boolean e;

    public ez(@NonNull View view) {
        boolean z = false;
        if (VERSION.SDK_INT >= 11) {
            z = view.isHardwareAccelerated();
        }
        this.e = z;
    }

    @NonNull
    public final String a() {
        return String.format("supports: {inlineVideo: %s}", new Object[]{Boolean.valueOf(this.e)});
    }
}
