package com.yandex.mobile.ads.impl;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;

public final class ll {
    @NonNull
    public static lk a() {
        if (VERSION.SDK_INT >= 14) {
            return lm.a();
        }
        return lo.a();
    }
}
