package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.cb.b;
import com.yandex.mobile.ads.impl.gu.a;
import java.util.List;

public final class cd implements b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cc f5415a;
    @NonNull
    private final cb b;

    public cd(@NonNull Context context, @NonNull ca caVar, @NonNull x xVar, @NonNull fc fcVar, @Nullable List<String> list) {
        this.f5415a = new cc(context, xVar, fcVar, list);
        this.b = new cb(caVar, this);
    }

    public final void b() {
        this.b.a();
    }

    public final void c() {
        this.b.b();
    }

    public final void a(@NonNull a aVar) {
        this.f5415a.a(aVar);
    }

    public final void a() {
        this.f5415a.a();
    }
}
