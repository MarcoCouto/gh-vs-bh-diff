package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.a;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class pu implements a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final px f5701a = new px();
    @Nullable
    private x<nq> b;

    public final void a(@NonNull x<nq> xVar) {
        this.b = xVar;
    }

    @NonNull
    public final Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        x<nq> xVar = this.b;
        if (xVar != null) {
            List a2 = px.a(xVar);
            if (!a2.isEmpty()) {
                hashMap.put("image_sizes", a2);
            }
            List b2 = px.b(xVar);
            if (!b2.isEmpty()) {
                hashMap.put("native_ad_types", b2);
            }
        }
        return hashMap;
    }
}
