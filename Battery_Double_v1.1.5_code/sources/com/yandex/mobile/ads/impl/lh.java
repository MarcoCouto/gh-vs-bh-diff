package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.e;

public final class lh implements li {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final af f5603a;
    @NonNull
    private final e b;
    @NonNull
    private final nm c;
    @NonNull
    private final du d = new du();

    public lh(@NonNull af afVar, @NonNull e eVar, @NonNull nm nmVar) {
        this.f5603a = afVar;
        this.b = eVar;
        this.c = nmVar;
    }

    public final void a(@NonNull ni niVar, @NonNull View view) {
        if (view.getTag() == null) {
            view.setTag(du.a(niVar.a()));
        }
    }

    public final void a(@NonNull ni niVar, @NonNull lp lpVar) {
        nm d2 = niVar.d();
        if (d2 == null) {
            d2 = this.c;
        }
        this.b.a(niVar, d2, this.f5603a, lpVar);
    }
}
