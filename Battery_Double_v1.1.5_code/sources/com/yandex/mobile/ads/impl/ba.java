package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.lang.ref.WeakReference;

public final class ba {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final dh f5385a;
    @NonNull
    private final WeakReference<aa> b;

    public ba(@NonNull aa aaVar, @NonNull be beVar) {
        this.b = new WeakReference<>(aaVar);
        this.f5385a = new dg(beVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        aa aaVar = (aa) this.b.get();
        if (aaVar != null && !aaVar.l()) {
            aaVar.b(this.f5385a);
        }
    }
}
