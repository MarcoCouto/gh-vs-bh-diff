package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class pa implements oy<nk> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final pb f5687a;

    public pa(@NonNull pb pbVar) {
        this.f5687a = pbVar;
    }

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, y {
        if (jSONObject.has("value")) {
            nl nlVar = null;
            if (!jSONObject.isNull("value")) {
                nlVar = this.f5687a.a(jSONObject);
            }
            return new nk(nlVar);
        }
        throw new y("Native Ad json has not required attributes");
    }
}
