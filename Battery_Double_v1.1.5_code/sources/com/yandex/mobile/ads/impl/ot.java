package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.y;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import org.json.JSONException;
import org.json.JSONObject;

final class ot implements op<nv> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final oj f5682a;

    public ot(@NonNull oj ojVar) {
        this.f5682a = ojVar;
    }

    @NonNull
    public final /* synthetic */ ns a(@NonNull JSONObject jSONObject) throws JSONException, y {
        return new nv(oh.a(jSONObject, "type"), oh.a(jSONObject, SettingsJsonConstants.APP_ICON_KEY), oh.a(jSONObject, "title"), this.f5682a.a(jSONObject, "url"));
    }
}
