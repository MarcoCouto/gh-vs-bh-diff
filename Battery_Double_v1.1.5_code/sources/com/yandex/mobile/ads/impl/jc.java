package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.view.View;

final class jc implements iz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final x f5574a;
    /* access modifiers changed from: private */
    @NonNull
    public final View b;
    @NonNull
    private final Handler c = new Handler(Looper.getMainLooper());
    @NonNull
    private final jd d;
    private boolean e;

    jc(@NonNull x xVar, @NonNull View view, @NonNull jd jdVar) {
        this.f5574a = xVar;
        this.b = view;
        this.b.setVisibility(8);
        this.d = jdVar;
    }

    @NonNull
    public final View a() {
        return this.b;
    }

    public final void a(boolean z) {
        this.e = z;
        e();
        this.b.setVisibility(z ? 8 : 0);
    }

    public final void c() {
        e();
    }

    public final boolean d() {
        return this.e && this.f5574a.x();
    }

    private void e() {
        this.c.removeCallbacksAndMessages(null);
    }

    public final void b() {
        this.c.postDelayed(new Runnable() {
            public final void run() {
                jc.this.b.setVisibility(0);
            }
        }, 200);
    }
}
