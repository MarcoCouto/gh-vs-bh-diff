package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import com.yandex.mobile.ads.nativeads.MediaView;

public final class lf extends ky {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final le<ImageView, nl> f5601a;
    @NonNull
    private final le<ed, nn> b;

    public final /* bridge */ /* synthetic */ void a(@NonNull View view) {
        MediaView mediaView = (MediaView) view;
        this.f5601a.a();
        this.b.a();
        super.a(mediaView);
    }

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        no noVar = (no) obj;
        if (noVar.a() != null) {
            return this.b.b(noVar.a());
        }
        if (noVar.b() != null) {
            return this.f5601a.b(noVar.b());
        }
        return false;
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        no noVar = (no) obj;
        if (noVar.a() != null) {
            this.b.a(noVar.a());
            return;
        }
        if (noVar.b() != null) {
            this.f5601a.a(noVar.b());
        }
    }

    public lf(@NonNull MediaView mediaView, @NonNull la laVar, @NonNull kx kxVar) {
        super(mediaView);
        this.f5601a = new le<>(kxVar);
        this.b = new le<>(laVar);
    }

    public final void a(@NonNull ni niVar, @NonNull li liVar) {
        this.f5601a.a(niVar, liVar);
        this.b.a(niVar, liVar);
    }

    public final void a(@NonNull no noVar) {
        if (noVar.a() == null && noVar.b() != null) {
            this.f5601a.a(noVar.b());
        }
    }
}
