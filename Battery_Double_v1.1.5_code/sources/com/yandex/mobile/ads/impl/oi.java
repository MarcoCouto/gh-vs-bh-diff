package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.HashSet;
import java.util.List;
import org.json.JSONObject;

public final class oi {
    static boolean a(@NonNull List<ni> list) {
        HashSet hashSet = new HashSet();
        for (ni a2 : list) {
            hashSet.add(a2.a());
        }
        return list.size() > hashSet.size();
    }

    public static boolean a(JSONObject jSONObject, String... strArr) {
        for (String has : strArr) {
            if (!jSONObject.has(has)) {
                return false;
            }
        }
        return true;
    }
}
