package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class ir {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5555a = new Object();
    private static volatile ir b;
    @Nullable
    private x<String> c;
    @Nullable
    private fc d;

    @NonNull
    public static ir a() {
        if (b == null) {
            synchronized (f5555a) {
                if (b == null) {
                    b = new ir();
                }
            }
        }
        return b;
    }

    private ir() {
    }

    public final void a(@NonNull x<String> xVar) {
        synchronized (f5555a) {
            this.c = xVar;
        }
    }

    public final void a(@NonNull fc fcVar) {
        synchronized (f5555a) {
            this.d = fcVar;
        }
    }

    @Nullable
    public final x<String> b() {
        x<String> xVar;
        synchronized (f5555a) {
            xVar = this.c;
        }
        return xVar;
    }

    @Nullable
    public final fc c() {
        fc fcVar;
        synchronized (f5555a) {
            fcVar = this.d;
        }
        return fcVar;
    }
}
