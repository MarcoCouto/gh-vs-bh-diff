package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

final class un {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final su f5810a = new su(sx.a(null));
    @NonNull
    private final ub b;

    un(@NonNull ub ubVar) {
        this.b = ubVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull VideoAd videoAd, @NonNull RequestListener<List<VideoAd>> requestListener) {
        this.f5810a.a(context, videoAd, this.b, (RequestListener<List<VideoAd>>) new uo<List<VideoAd>>(videoAd, requestListener));
    }
}
