package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.Vmap;
import java.util.HashMap;

public final class ug implements gv<VmapRequestConfiguration, Vmap> {
    public final /* synthetic */ gu a(@Nullable qt qtVar, int i, @NonNull Object obj) {
        VmapRequestConfiguration vmapRequestConfiguration = (VmapRequestConfiguration) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("page_id", vmapRequestConfiguration.getPageId());
        hashMap.put("category_id", vmapRequestConfiguration.getCategoryId());
        if (i != -1) {
            hashMap.put("code", Integer.valueOf(i));
        }
        return new gu(b.VMAP_RESPONSE, hashMap);
    }

    public final /* synthetic */ gu a(Object obj) {
        VmapRequestConfiguration vmapRequestConfiguration = (VmapRequestConfiguration) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("page_id", vmapRequestConfiguration.getPageId());
        hashMap.put("category_id", vmapRequestConfiguration.getCategoryId());
        return new gu(b.VMAP_REQUEST, hashMap);
    }
}
