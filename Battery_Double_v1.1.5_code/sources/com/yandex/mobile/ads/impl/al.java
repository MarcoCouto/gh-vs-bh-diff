package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import java.util.Locale;

public class al implements Parcelable {
    public static final Creator<al> CREATOR = new Creator<al>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new al[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new al(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final int f5371a;
    private final int b;
    @NonNull
    private final String c;
    @NonNull
    private final a d;

    public enum a {
        FIXED,
        FLEXIBLE,
        SCREEN
    }

    public int describeContents() {
        return 0;
    }

    public al(int i, int i2, @NonNull a aVar) {
        this.f5371a = (i >= 0 || -1 == i) ? i : 0;
        this.b = (i2 >= 0 || -2 == i2) ? i2 : 0;
        this.d = aVar;
        this.c = String.format(Locale.US, "%dx%d", new Object[]{Integer.valueOf(i), Integer.valueOf(i2)});
    }

    public final int a() {
        return this.f5371a;
    }

    public final int b() {
        return this.b;
    }

    public final int a(@NonNull Context context) {
        if (-2 == this.b) {
            return dv.d(context);
        }
        return this.b;
    }

    public final int b(@NonNull Context context) {
        if (-1 == this.f5371a) {
            return dv.c(context);
        }
        return this.f5371a;
    }

    public final int c(@NonNull Context context) {
        if (-2 == this.b) {
            return dv.b(context);
        }
        return dv.a(context, (float) this.b);
    }

    public final int d(@NonNull Context context) {
        if (-1 == this.f5371a) {
            return dv.a(context);
        }
        return dv.a(context, (float) this.f5371a);
    }

    @NonNull
    public final a c() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        al alVar = (al) obj;
        return this.f5371a == alVar.f5371a && this.b == alVar.b && this.d == alVar.d;
    }

    public int hashCode() {
        return (((((this.f5371a * 31) + this.b) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    public String toString() {
        return this.c;
    }

    protected al(@NonNull Parcel parcel) {
        this.f5371a = parcel.readInt();
        this.b = parcel.readInt();
        this.d = a.values()[parcel.readInt()];
        this.c = parcel.readString();
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeInt(this.f5371a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.d.ordinal());
        parcel.writeString(this.c);
    }
}
