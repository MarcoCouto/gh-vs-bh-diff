package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.concurrent.ThreadFactory;

/* renamed from: com.yandex.mobile.ads.impl.do reason: invalid class name */
public final class Cdo implements ThreadFactory {

    /* renamed from: a reason: collision with root package name */
    private final String f5441a;

    public Cdo(String str) {
        this.f5441a = str;
    }

    public final Thread newThread(@NonNull Runnable runnable) {
        return new Thread(runnable, this.f5441a);
    }
}
