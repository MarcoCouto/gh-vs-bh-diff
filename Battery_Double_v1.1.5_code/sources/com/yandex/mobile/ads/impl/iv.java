package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.InterstitialEventListener;
import com.yandex.mobile.ads.b;

public final class iv extends hm {
    @NonNull
    private final iu g;

    public iv(@NonNull Context context, @NonNull iu iuVar) {
        super(context, b.INTERSTITIAL, iuVar);
        this.g = iuVar;
    }

    @Nullable
    public final InterstitialEventListener E() {
        return this.g.h();
    }

    public final void a(@Nullable InterstitialEventListener interstitialEventListener) {
        this.g.a(interstitialEventListener);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final jm a(@NonNull jn jnVar) {
        return jnVar.a((hm) this);
    }
}
