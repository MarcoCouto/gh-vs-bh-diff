package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.yandex.mobile.ads.impl.hq.b;
import com.yandex.mobile.ads.impl.hq.c;
import com.yandex.mobile.ads.impl.hq.d;
import com.yandex.mobile.ads.impl.hq.f;

final class je implements jg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final iz f5576a;
    @NonNull
    private final RelativeLayout b;
    @NonNull
    private final f c;

    je(@NonNull Context context, @NonNull iz izVar) {
        this.f5576a = izVar;
        this.b = c.a(context);
        this.c = new f(context);
    }

    public final void a(@NonNull RelativeLayout relativeLayout) {
        if (VERSION.SDK_INT >= 16) {
            relativeLayout.setBackground(b.f5534a);
        } else {
            relativeLayout.setBackgroundDrawable(b.f5534a);
        }
    }

    public final void a(@NonNull Context context, @NonNull o oVar, @NonNull al alVar) {
        int i = context.getResources().getConfiguration().orientation;
        boolean a2 = iq.a(context, alVar);
        boolean b2 = iq.b(context, alVar);
        int i2 = 1;
        if (a2 == b2) {
            i2 = -1;
        } else if (!b2 ? 1 != i : 1 == i) {
            i2 = 0;
        }
        if (-1 != i2) {
            oVar.a(i2);
        }
    }

    @NonNull
    public final View a(@NonNull View view, @NonNull x xVar) {
        Context context = view.getContext();
        LayoutParams a2 = d.a(context, xVar);
        this.b.addView(view, a2);
        LayoutParams a3 = d.a(context, view);
        this.b.addView(this.f5576a.a(), a3);
        LayoutParams b2 = d.b(context, xVar);
        RelativeLayout b3 = c.b(context);
        this.c.setBackFace(this.b, b2);
        this.c.setFrontFace(b3, a2);
        this.c.setLayoutParams(d.a(context, (x) null));
        return this.c;
    }

    public final void a() {
        this.f5576a.b();
        hp.a(this.c, dv.b((View) this.b));
    }

    public final void a(boolean z) {
        this.f5576a.a(z);
    }

    public final void b() {
        this.f5576a.c();
    }

    public final boolean c() {
        return this.f5576a.d();
    }
}
