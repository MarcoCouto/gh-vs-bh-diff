package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.FrameLayout;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.bc;
import java.util.List;

public final class mm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ms f5625a = new ms();
    /* access modifiers changed from: private */
    @NonNull
    public final mp b;
    /* access modifiers changed from: private */
    @NonNull
    public final mn c;
    /* access modifiers changed from: private */
    @NonNull
    public final Handler d;

    private class a implements Runnable {
        @NonNull
        private final af b;

        a(af afVar) {
            this.b = afVar;
        }

        public final void run() {
            View b2 = this.b.b();
            if (b2 instanceof FrameLayout) {
                mm.this.c.a(mm.this.b.a(b2.getContext()), (FrameLayout) b2);
                mm.this.d.postDelayed(new a(this.b), 300);
            }
        }
    }

    public mm(@NonNull bc bcVar, @NonNull List<bg> list) {
        new mq();
        this.b = mq.a(bcVar, list);
        this.c = new mn();
        this.d = new Handler(Looper.getMainLooper());
    }

    public final void a() {
        this.d.removeCallbacksAndMessages(null);
    }

    public final void a(@NonNull af afVar) {
        a();
        View b2 = afVar.b();
        if (b2 instanceof FrameLayout) {
            mo.a((FrameLayout) b2);
        }
    }

    public final void a(@NonNull Context context, @NonNull af afVar) {
        ff a2 = ff.a();
        fg a3 = a2.a(context);
        Boolean k = a3 != null ? a3.k() : null;
        boolean z = k != null ? k.booleanValue() : a2.d() && hc.a(context);
        if (z) {
            this.d.post(new a(afVar));
        }
    }
}
