package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.yandex.mobile.ads.nativeads.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class ly {
    ly() {
    }

    static void a(@NonNull o oVar, @NonNull Map<String, Bitmap> map) {
        for (np npVar : oVar.c().c()) {
            List<ni> c = npVar.c();
            if (c != null && !c.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (ni niVar : c) {
                    Object c2 = niVar.c();
                    String b = niVar.b();
                    if (!MessengerShareContentUtility.MEDIA_IMAGE.equals(b) || !(c2 instanceof nl)) {
                        if ("media".equals(b) && (c2 instanceof no) && ((no) c2).b() != null) {
                            nl b2 = ((no) c2).b();
                            if (b2 != null && a(b2, map)) {
                                arrayList.add(niVar);
                            }
                        } else {
                            arrayList.add(niVar);
                        }
                    } else if (a((nl) c2, map)) {
                        arrayList.add(niVar);
                    }
                }
                npVar.a((List<ni>) arrayList);
            }
        }
    }

    private static boolean a(@NonNull nl nlVar, @NonNull Map<String, Bitmap> map) {
        return a((Bitmap) map.get(nlVar.c()));
    }

    private static boolean a(@Nullable Bitmap bitmap) {
        return bitmap != null && bitmap.getWidth() > 1 && bitmap.getHeight() > 1;
    }
}
