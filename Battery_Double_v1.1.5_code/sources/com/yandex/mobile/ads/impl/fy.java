package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.WeakHashMap;

public final class fy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Object f5500a = new Object();
    @NonNull
    private final WeakHashMap<ga, Object> b = new WeakHashMap<>();

    fy() {
    }

    public final void a(@NonNull ga gaVar) {
        synchronized (this.f5500a) {
            this.b.put(gaVar, null);
        }
    }

    public final void b(@NonNull ga gaVar) {
        synchronized (this.f5500a) {
            this.b.remove(gaVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull fu fuVar) {
        synchronized (this.f5500a) {
            b(fuVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        synchronized (this.f5500a) {
            b((fu) null);
        }
    }

    private void b(@Nullable fu fuVar) {
        for (ga a2 : this.b.keySet()) {
            a2.a(fuVar);
        }
        this.b.clear();
    }
}
