package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.i;

public final class lj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final i f5604a;
    @NonNull
    private final qc b = new qc();

    public lj(@NonNull i iVar) {
        this.f5604a = iVar;
    }

    public final boolean a(@Nullable Drawable drawable, @NonNull nl nlVar) {
        Bitmap a2 = this.f5604a.a(nlVar);
        if (drawable == null || a2 == null) {
            return false;
        }
        return qc.a(drawable).a(drawable, a2);
    }
}
