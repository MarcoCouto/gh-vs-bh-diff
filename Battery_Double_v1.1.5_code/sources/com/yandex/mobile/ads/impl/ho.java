package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.AdRequestError;
import java.util.Map;

public final class ho implements ec {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ec f5530a;

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
    }

    public final void a(@NonNull String str) {
    }

    public final void b(boolean z) {
    }

    public ho(@NonNull ec ecVar) {
        this.f5530a = ecVar;
    }

    public final void onAdLoaded() {
        this.f5530a.onAdLoaded();
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        this.f5530a.onAdFailedToLoad(adRequestError);
    }
}
