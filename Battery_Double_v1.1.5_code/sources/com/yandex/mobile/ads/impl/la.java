package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;

public final class la extends ld<ed, nn> {
    public final /* bridge */ /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        return true;
    }

    public final /* bridge */ /* synthetic */ void a(@NonNull View view) {
        super.a((ed) view);
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        ed edVar = (ed) view;
        nn nnVar = (nn) obj;
        String a2 = nnVar.a();
        if (!TextUtils.isEmpty(a2)) {
            edVar.setAspectRatio(nnVar.b());
            edVar.c(a2);
        }
    }

    public la(@NonNull ed edVar) {
        super(edVar);
    }

    public final void a(@NonNull ni niVar, @NonNull li liVar) {
        ed edVar = (ed) a();
        if (edVar != null) {
            liVar.a(niVar, (View) edVar);
            liVar.a(niVar, (lp) new lr(edVar));
        }
    }
}
