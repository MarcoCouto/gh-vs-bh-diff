package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.i;

public final class pt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cg f5700a;
    @NonNull
    private final i b;
    @NonNull
    private final ps c;
    @NonNull
    private final pz d;

    public pt(@NonNull Context context, @NonNull cg cgVar, @NonNull af afVar) {
        this.f5700a = cgVar;
        this.b = afVar.e();
        this.c = new ps(context);
        this.d = new pz(context);
    }

    public final void a(@NonNull Context context, @NonNull nv nvVar) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(nvVar.d()));
        if (this.d.a(intent)) {
            Bitmap a2 = this.b.a(this.c.a(nvVar.b()));
            if (a2 != null) {
                this.f5700a.a(b.SHORTCUT);
                String c2 = nvVar.c();
                Intent intent2 = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
                intent2.putExtra("android.intent.extra.shortcut.NAME", c2);
                intent2.putExtra("android.intent.extra.shortcut.ICON", a2);
                intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
                intent2.putExtra("duplicate", false);
                try {
                    context.sendBroadcast(intent2);
                } catch (Exception unused) {
                }
            }
        }
    }
}
