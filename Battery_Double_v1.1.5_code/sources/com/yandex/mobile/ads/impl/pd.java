package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class pd implements oy<String> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final pe f5690a = new pe();

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, y {
        String a2 = oh.a(jSONObject, "name");
        String a3 = oh.a(jSONObject, "value");
        return "review_count".equals(a2) ? this.f5690a.a(a3) : a3;
    }
}
