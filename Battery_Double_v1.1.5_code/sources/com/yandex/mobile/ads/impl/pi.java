package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class pi implements pg<nn> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bs f5693a = new bs();

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, y {
        String a2 = bs.a(oh.a(jSONObject, String.HTML));
        if (!TextUtils.isEmpty(a2)) {
            float f = (float) jSONObject.getDouble("aspectRatio");
            if (f == 0.0f) {
                f = 1.7777778f;
            }
            return new nn(a2, f);
        }
        throw new y("Native Ad json has attribute with broken base64 encoding");
    }
}
