package com.yandex.mobile.ads.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public final class ro extends ByteArrayOutputStream {

    /* renamed from: a reason: collision with root package name */
    private final rg f5742a;

    public ro(rg rgVar, int i) {
        this.f5742a = rgVar;
        this.buf = this.f5742a.a(Math.max(i, 256));
    }

    public final void close() throws IOException {
        this.f5742a.a(this.buf);
        this.buf = null;
        super.close();
    }

    public final void finalize() {
        this.f5742a.a(this.buf);
    }

    private void a(int i) {
        if (this.count + i > this.buf.length) {
            byte[] a2 = this.f5742a.a((this.count + i) * 2);
            System.arraycopy(this.buf, 0, a2, 0, this.count);
            this.f5742a.a(this.buf);
            this.buf = a2;
        }
    }

    public final synchronized void write(byte[] bArr, int i, int i2) {
        a(i2);
        super.write(bArr, i, i2);
    }

    public final synchronized void write(int i) {
        a(1);
        super.write(i);
    }
}
