package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class ui {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<VideoAd> f5807a;
    @NonNull
    private final List<VideoAd> b;

    ui(@NonNull List<VideoAd> list, @NonNull List<VideoAd> list2) {
        this.f5807a = list;
        this.b = list2;
    }

    @NonNull
    public final List<VideoAd> a() {
        return this.f5807a;
    }

    @NonNull
    public final List<VideoAd> b() {
        return this.b;
    }
}
