package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import android.text.TextUtils;

public final class fv {
    @Nullable
    public static fu a(@Nullable String str, @Nullable Boolean bool) {
        if (bool == null || TextUtils.isEmpty(str)) {
            return null;
        }
        return new fu(str, bool.booleanValue());
    }
}
