package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class by implements bv<String> {
    @Nullable
    public final /* synthetic */ Object b(@NonNull qq qqVar) {
        return a(qqVar);
    }

    @Nullable
    private static String a(@NonNull qq qqVar) {
        if (qqVar.b == null) {
            return null;
        }
        try {
            return new String(qqVar.b, ri.a(qqVar.c));
        } catch (Exception unused) {
            return new String(qqVar.b);
        }
    }
}
