package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.af;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public final class qi {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final gs f5710a;
    @NonNull
    private final List<ni> b;
    @NonNull
    private final b c;
    @Nullable
    private final String d;
    @Nullable
    private a e;

    public qi(@NonNull Context context, @NonNull b bVar, @Nullable String str, @Nullable List<ni> list) {
        this.c = bVar;
        this.d = str;
        this.f5710a = gs.a(context);
        if (list == null) {
            list = Collections.emptyList();
        }
        this.b = list;
    }

    public final void a(@NonNull a aVar) {
        this.e = aVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    public final void a(@NonNull af afVar) {
        boolean z;
        MediaView k = afVar.c().k();
        String str = "media";
        ni niVar = null;
        for (ni niVar2 : this.b) {
            if (str.equals(niVar2.a())) {
                niVar = niVar2;
            }
        }
        if (k == null) {
            if ((niVar == null || !(niVar.c() instanceof no) || ((no) niVar.c()).a() == null) ? false : true) {
                z = true;
                if (!z) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("ad_type", this.c.a());
                    hashMap.put("block_id", this.d);
                    hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, "expected_view_missing");
                    hashMap.put(ParametersKeys.VIEW, "mediaview");
                    hashMap.put("asset_name", "media");
                    if (this.e != null) {
                        hashMap.putAll(this.e.a());
                    }
                    this.f5710a.a(new gu(gu.b.BINDING_FAILURE, hashMap));
                    new Object[1][0] = "mediaview";
                    return;
                }
                return;
            }
        }
        z = false;
        if (!z) {
        }
    }
}
