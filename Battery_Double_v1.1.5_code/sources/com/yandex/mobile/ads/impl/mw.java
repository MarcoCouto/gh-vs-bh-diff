package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v4.internal.view.SupportMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public final class mw extends LinearLayout {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final dk f5634a = new dk();
    /* access modifiers changed from: private */
    @NonNull
    public mv b;
    /* access modifiers changed from: private */
    @NonNull
    public TextView c;
    private final OnClickListener d = new OnClickListener() {
        public final void onClick(View view) {
            boolean z = !mw.this.b.isSelected();
            mw.this.b.setSelected(z);
            mw.this.c.setVisibility(z ? 0 : 8);
        }
    };

    public mw(@NonNull Context context) {
        super(context);
        setOrientation(0);
        int a2 = dk.a(context, 4.0f);
        setPadding(a2, a2, a2, a2);
        this.b = new mv(context, this.f5634a);
        this.b.setOnClickListener(this.d);
        addView(this.b);
        this.c = new TextView(context);
        int a3 = dk.a(context, 3.0f);
        this.c.setPadding(a3, a3, a3, a3);
        int a4 = dk.a(context, 2.0f);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(-1);
        gradientDrawable.setStroke(a4, SupportMenu.CATEGORY_MASK);
        this.c.setBackgroundDrawable(gradientDrawable);
        addView(this.c);
        int a5 = dk.a(context, 2.0f);
        LayoutParams layoutParams = (LayoutParams) this.c.getLayoutParams();
        layoutParams.setMargins(a5, 0, a5, a5);
        this.c.setLayoutParams(layoutParams);
        this.c.setVisibility(8);
    }

    public final void setDescription(@NonNull String str) {
        this.c.setText(str);
    }
}
