package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.video.VideoAdError;
import java.util.HashMap;
import java.util.Map;

public final class ua {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ub f5803a;
    @NonNull
    private final gs b;

    public ua(@NonNull Context context, @NonNull ub ubVar) {
        this.f5803a = ubVar;
        this.b = gs.a(context);
    }

    @NonNull
    private Map<String, Object> b() {
        HashMap hashMap = new HashMap();
        hashMap.put("block_id", c());
        return hashMap;
    }

    @NonNull
    private String c() {
        return String.format("R-V-%s-%s", new Object[]{this.f5803a.a(), this.f5803a.b()});
    }

    public final void a() {
        Map b2 = b();
        b2.put("status", "success");
        this.b.a(new gu(b.AD_LOADING_RESULT, b2));
    }

    public final void a(@NonNull VideoAdError videoAdError) {
        Map b2 = b();
        b2.put("failure_reason", videoAdError.getCode() == 3 ? "no_ads" : "null");
        b2.put("status", "error");
        this.b.a(new gu(b.AD_LOADING_RESULT, b2));
    }
}
