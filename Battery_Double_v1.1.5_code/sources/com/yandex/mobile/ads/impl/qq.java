package com.yandex.mobile.ads.impl;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import java.util.Map;

public final class qq {

    /* renamed from: a reason: collision with root package name */
    public final int f5720a;
    public final byte[] b;
    public final Map<String, String> c;
    public final boolean d;
    public final long e;

    public qq(int i, byte[] bArr, Map<String, String> map, boolean z, long j) {
        this.f5720a = i;
        this.b = bArr;
        this.c = map;
        this.d = z;
        this.e = j;
    }

    public qq(byte[] bArr, Map<String, String> map) {
        this(Callback.DEFAULT_DRAG_ANIMATION_DURATION, bArr, map, false, 0);
    }
}
