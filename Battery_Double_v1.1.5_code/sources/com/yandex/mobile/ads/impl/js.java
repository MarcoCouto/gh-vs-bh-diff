package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.mintegral.msdk.base.entity.CampaignEx;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.Map;

public final class js {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, jr> f5582a = new HashMap<String, jr>() {
        {
            put(MessengerShareContentUtility.MEDIA_IMAGE, new ju());
            put("number", new jw());
            put("close_button", new jt());
            put("media", new jv());
            put("string", new jx());
        }
    };

    @NonNull
    public final jr a(@NonNull String str) {
        char c;
        String str2;
        switch (str.hashCode()) {
            case -1678958759:
                if (str.equals("close_button")) {
                    c = 5;
                    break;
                }
            case -1074675180:
                if (str.equals("favicon")) {
                    c = 0;
                    break;
                }
            case -938102371:
                if (str.equals(CampaignEx.JSON_KEY_STAR)) {
                    c = 3;
                    break;
                }
            case -807286424:
                if (str.equals("review_count")) {
                    c = 4;
                    break;
                }
            case 3226745:
                if (str.equals(SettingsJsonConstants.APP_ICON_KEY)) {
                    c = 1;
                    break;
                }
            case 103772132:
                if (str.equals("media")) {
                    c = 2;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
            case 1:
                str2 = MessengerShareContentUtility.MEDIA_IMAGE;
                break;
            case 2:
                str2 = "media";
                break;
            case 3:
            case 4:
                str2 = "number";
                break;
            case 5:
                str2 = "close_button";
                break;
            default:
                str2 = "string";
                break;
        }
        return (jr) this.f5582a.get(str2);
    }
}
