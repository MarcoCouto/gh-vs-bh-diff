package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.fs.a;
import com.yandex.mobile.ads.video.BlocksInfoRequest;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.Vmap;
import com.yandex.mobile.ads.video.tracking.Tracker.ErrorListener;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class sg {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5755a = new Object();
    private static volatile sg b;
    /* access modifiers changed from: private */
    @NonNull
    public final fc c = new fc(b.VASTVIDEO);
    /* access modifiers changed from: private */
    @NonNull
    public final fs d = new fs(this.c);
    /* access modifiers changed from: private */
    @NonNull
    public su e;
    /* access modifiers changed from: private */
    @NonNull
    public final sd f = new sd(this.c);
    @NonNull
    private final Executor g = Executors.newSingleThreadExecutor(new Cdo("YandexMobileAds.VideoAdsImpl"));
    /* access modifiers changed from: private */
    @NonNull
    public final st h = new st();
    /* access modifiers changed from: private */
    @NonNull
    public final sr i = new sr();
    /* access modifiers changed from: private */
    @NonNull
    public final fw j = new fw();

    private sg(@Nullable Context context) {
        this.e = new su(sx.a(context));
    }

    @NonNull
    public static sg a(@Nullable Context context) {
        if (b == null) {
            synchronized (f5755a) {
                if (b == null) {
                    b = new sg(context);
                }
            }
        }
        return b;
    }

    public final void a(@NonNull final Context context, @NonNull final VmapRequestConfiguration vmapRequestConfiguration, @NonNull fr frVar, @NonNull final RequestListener<Vmap> requestListener) {
        a(context, frVar, (sc) new sc() {
            public final void a() {
                sg.this.e.a(context, sg.this.c, vmapRequestConfiguration, requestListener);
            }

            public final void b() {
                requestListener.onFailure(VideoAdError.createInternalError("Internal state wasn't completely configured"));
            }
        });
    }

    public final void a(@NonNull final Context context, @NonNull final VastRequestConfiguration vastRequestConfiguration, @NonNull fr frVar, @NonNull final RequestListener<sk> requestListener) {
        a(context, frVar, (sc) new sc() {
            public final void a() {
                sg.this.i;
                sg.this.e.a(context, sg.this.c, vastRequestConfiguration, (RequestListener<sk>) sr.a(context, vastRequestConfiguration, requestListener));
            }

            public final void b() {
                requestListener.onFailure(VideoAdError.createInternalError("Internal state wasn't completely configured"));
            }
        });
    }

    public final void a(@NonNull Context context, @NonNull final BlocksInfoRequest blocksInfoRequest, @NonNull fr frVar) {
        a(context, frVar, (sc) new sc() {
            public final void a() {
                sg.this.e.a(blocksInfoRequest, sg.this.c);
            }

            public final void b() {
                RequestListener requestListener = blocksInfoRequest.getRequestListener();
                if (requestListener != null) {
                    requestListener.onFailure(VideoAdError.createInternalError("Internal state wasn't completely configured"));
                }
            }
        });
    }

    public final void a(@NonNull Context context, @NonNull final VideoAdRequest videoAdRequest, @NonNull fr frVar) {
        a(context, frVar, (sc) new sc() {
            public final void a() {
                sg.this.h;
                sg.this.e.a(st.a(videoAdRequest), sg.this.c);
            }

            public final void b() {
                RequestListener requestListener = videoAdRequest.getRequestListener();
                if (requestListener != null) {
                    requestListener.onFailure(VideoAdError.createInternalError("Internal state wasn't completely configured"));
                }
            }
        });
    }

    private void a(@NonNull final Context context, @NonNull final fr frVar, @NonNull final sc scVar) {
        this.g.execute(new Runnable() {
            public final void run() {
                sg.this.d.a(frVar, (a) new a() {
                    public final void a() {
                        sg.this.g.execute(new Runnable(context, scVar) {
                            public final void run() {
                                sg.this.j.a(r3, new ga() {
                                    public final void a(@Nullable fu fuVar) {
                                        if (fuVar != null) {
                                            sg.this.c.a(fuVar);
                                        }
                                        sg.this.g.execute(new Runnable(r3, r4) {
                                            public final void run() {
                                                sg.this.f.a(r3, r4);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    public final void b() {
                        scVar.b();
                    }
                });
            }
        });
    }

    public final void a(@NonNull String str, @NonNull ErrorListener errorListener) {
        this.e.a(str, errorListener);
    }
}
