package com.yandex.mobile.ads.impl;

import com.yandex.mobile.ads.nativeads.at.a;

public final class nj {

    /* renamed from: a reason: collision with root package name */
    private final String f5648a;
    private final a b;

    public nj(a aVar, String str) {
        this.b = aVar;
        this.f5648a = str;
    }

    public final String a() {
        return this.f5648a;
    }

    public final a b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nj njVar = (nj) obj;
        if (this.f5648a == null ? njVar.f5648a == null : this.f5648a.equals(njVar.f5648a)) {
            return this.b == njVar.b;
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.f5648a != null ? this.f5648a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }
}
