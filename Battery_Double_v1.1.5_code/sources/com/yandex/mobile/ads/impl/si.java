package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.VmapLoader.OnVmapLoadedListener;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class si implements RequestListener<Vmap> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Object f5766a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @Nullable
    public OnVmapLoadedListener c;

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        final Vmap vmap = (Vmap) obj;
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (si.f5766a) {
                    if (si.this.c != null) {
                        si.this.c.onVmapLoaded(vmap);
                    }
                }
            }
        });
    }

    public final void onFailure(@NonNull final VideoAdError videoAdError) {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (si.f5766a) {
                    if (si.this.c != null) {
                        si.this.c.onVmapFailedToLoad(new sh(videoAdError.getCode(), videoAdError.getDescription()));
                    }
                }
            }
        });
    }

    public final void a() {
        this.b.removeCallbacksAndMessages(null);
    }

    public final void a(@Nullable OnVmapLoadedListener onVmapLoadedListener) {
        synchronized (f5766a) {
            this.c = onVmapLoadedListener;
        }
    }
}
