package com.yandex.mobile.ads.impl;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class gm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Object f5514a;

    gm(@NonNull Object obj) {
        this.f5514a = obj;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        Object a2 = he.a(this.f5514a, "isComplete", new Object[0]);
        if (a2 instanceof Boolean) {
            return ((Boolean) a2).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final Location b() {
        Object a2 = he.a(this.f5514a, "getResult", new Object[0]);
        if (a2 instanceof Location) {
            return (Location) a2;
        }
        return null;
    }
}
