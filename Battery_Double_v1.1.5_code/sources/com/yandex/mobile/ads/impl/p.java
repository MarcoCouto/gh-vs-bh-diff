package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdActivity;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public final class p {

    static final class a {

        /* renamed from: a reason: collision with root package name */
        static final AtomicLong f5686a = new AtomicLong(SystemClock.elapsedRealtime() - 2000);

        static boolean a() {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            long andSet = elapsedRealtime - f5686a.getAndSet(elapsedRealtime);
            return andSet < 0 || andSet > 1000;
        }
    }

    private static void a(Context context, String str, ResultReceiver resultReceiver) {
        if (context != null) {
            try {
                context.startActivity(b(context, str, resultReceiver));
            } catch (Exception e) {
                new StringBuilder("Failed to show Browser. Exception: ").append(e);
            }
        }
    }

    private static Intent b(Context context, String str, ResultReceiver resultReceiver) {
        Intent intent = new Intent(context, AdActivity.class);
        intent.putExtra("window_type", "window_type_browser");
        intent.putExtra("extra_receiver", di.a(resultReceiver));
        intent.putExtra("extra_browser_url", str);
        intent.addFlags(268435456);
        return intent;
    }

    public static void a(@Nullable Context context, @NonNull cg cgVar, @Nullable String str, @Nullable ResultReceiver resultReceiver, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("click_type", "default");
        cgVar.a(b.CLICK, hashMap);
        if (resultReceiver != null) {
            resultReceiver.send(9, null);
        }
        if (context != null && a.a()) {
            if (z && dt.d(str)) {
                a(context, str, resultReceiver);
            } else if (!dt.a(context, str, true)) {
                a(context, str, resultReceiver);
            } else if (resultReceiver != null) {
                resultReceiver.send(7, null);
            }
        }
    }
}
