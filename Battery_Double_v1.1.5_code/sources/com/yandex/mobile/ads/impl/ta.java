package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.b;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Random;

public final class ta {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final so f5784a = new so();
    @NonNull
    private final sz b = new sz();

    @NonNull
    public static qr<sk> a(@NonNull Context context, @NonNull fc fcVar, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull RequestListener<sk> requestListener) {
        b bVar = new b(vastRequestConfiguration);
        ue ueVar = new ue(bVar);
        Builder appendQueryParameter = Uri.parse(bVar.a().a()).buildUpon().appendQueryParameter(HttpRequest.PARAM_CHARSET, "UTF-8").appendQueryParameter("rnd", Integer.toString(new Random().nextInt(89999999) + 10000000));
        sz.a(appendQueryParameter, vastRequestConfiguration.getParameters());
        sz.a(appendQueryParameter, "video-session-id", bVar.d());
        sz.a(appendQueryParameter, "uuid", fcVar.d());
        new sy(context, fcVar).a(context, appendQueryParameter);
        tf tfVar = new tf(context, appendQueryParameter.build().toString(), new sw.b(requestListener), vastRequestConfiguration, ueVar);
        return tfVar;
    }
}
