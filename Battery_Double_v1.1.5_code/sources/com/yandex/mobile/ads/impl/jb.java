package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

final class jb implements iz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final View f5573a;

    public final void a(boolean z) {
    }

    public final void b() {
    }

    public final void c() {
    }

    public final boolean d() {
        return false;
    }

    jb(@NonNull View view) {
        this.f5573a = view;
        this.f5573a.setVisibility(0);
    }

    @NonNull
    public final View a() {
        return this.f5573a;
    }
}
