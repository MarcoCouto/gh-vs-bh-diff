package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public class bj implements Parcelable {
    public static final Creator<bj> CREATOR = new Creator<bj>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new bj[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new bj(parcel);
        }
    };
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5395a;

    public int describeContents() {
        return 0;
    }

    public bj(@NonNull String str) {
        this.f5395a = str;
    }

    @NonNull
    public final String a() {
        return this.f5395a;
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.f5395a);
    }

    protected bj(@NonNull Parcel parcel) {
        this.f5395a = parcel.readString();
    }
}
