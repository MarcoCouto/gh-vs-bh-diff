package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class nz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5664a;
    private final int b;
    private final int c;

    public nz(@NonNull String str, int i, int i2) {
        this.f5664a = str;
        this.b = i;
        this.c = i2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nz nzVar = (nz) obj;
        if (this.b == nzVar.b && this.c == nzVar.c) {
            return this.f5664a.equals(nzVar.f5664a);
        }
        return false;
    }

    public final int hashCode() {
        return (((this.f5664a.hashCode() * 31) + this.b) * 31) + this.c;
    }
}
