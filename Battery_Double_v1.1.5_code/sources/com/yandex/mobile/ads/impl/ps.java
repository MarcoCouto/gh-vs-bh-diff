package com.yandex.mobile.ads.impl;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public final class ps {

    /* renamed from: a reason: collision with root package name */
    private final int f5699a;

    public ps(@NonNull Context context) {
        this.f5699a = a(context);
    }

    private static int a(@NonNull Context context) {
        int round = Math.round(context.getResources().getDimension(17104896));
        try {
            return VERSION.SDK_INT >= 11 ? ((ActivityManager) context.getSystemService("activity")).getLauncherLargeIconSize() : round;
        } catch (Exception unused) {
            return round;
        }
    }

    @NonNull
    public final List<nl> a(@NonNull np npVar) {
        ArrayList arrayList = new ArrayList();
        a(npVar.a(), arrayList);
        List<ni> c = npVar.c();
        if (c != null) {
            for (ni d : c) {
                a(d.d(), arrayList);
            }
        }
        return arrayList;
    }

    @NonNull
    private List<nl> a(@Nullable nm nmVar, @NonNull List<nl> list) {
        if (nmVar != null) {
            List<ns> a2 = nmVar.a();
            if (a2 != null) {
                for (ns nsVar : a2) {
                    if (nsVar instanceof nv) {
                        list.add(a(((nv) nsVar).b()));
                    }
                }
            }
        }
        return list;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final nl a(@NonNull String str) {
        nl nlVar = new nl();
        nlVar.a(str);
        nlVar.b(this.f5699a);
        nlVar.a(this.f5699a);
        return nlVar;
    }
}
