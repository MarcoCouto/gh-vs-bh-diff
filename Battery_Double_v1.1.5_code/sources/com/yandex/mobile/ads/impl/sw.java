package com.yandex.mobile.ads.impl;

import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.MobileAds;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.models.blocksinfo.BlocksInfo;
import com.yandex.mobile.ads.video.tracking.Tracker.ErrorListener;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Random;

public final class sw {

    /* renamed from: a reason: collision with root package name */
    private static Random f5779a;

    public static class a implements com.yandex.mobile.ads.impl.ru.a<Void> {

        /* renamed from: a reason: collision with root package name */
        private final ErrorListener f5780a;

        public final /* bridge */ /* synthetic */ void a(Object obj) {
        }

        public a(ErrorListener errorListener) {
            this.f5780a = errorListener;
        }

        public final void a(@NonNull re reVar) {
            VideoAdError videoAdError;
            if (this.f5780a != null) {
                if (reVar == null) {
                    this.f5780a.onTrackingError(VideoAdError.createInternalError("Tracking error"));
                    return;
                }
                if (reVar.f5728a == null) {
                    videoAdError = VideoAdError.createConnectionError(reVar.getMessage());
                } else {
                    videoAdError = VideoAdError.createInternalError("Tracking error");
                }
                this.f5780a.onTrackingError(videoAdError);
            }
        }
    }

    public static class b<T> implements com.yandex.mobile.ads.impl.ru.a<T> {

        /* renamed from: a reason: collision with root package name */
        private final RequestListener<T> f5781a;

        public b(RequestListener<T> requestListener) {
            this.f5781a = requestListener;
        }

        public final void a(T t) {
            if (this.f5781a != null) {
                this.f5781a.onSuccess(t);
            }
        }

        public final void a(@NonNull re reVar) {
            VideoAdError videoAdError;
            if (this.f5781a != null) {
                if (reVar instanceof sm) {
                    videoAdError = VideoAdError.createNoAdError((sm) reVar);
                } else if (reVar instanceof sn) {
                    videoAdError = VideoAdError.createInternalError((sn) reVar);
                } else {
                    qq qqVar = reVar.f5728a;
                    if (qqVar == null) {
                        videoAdError = VideoAdError.createConnectionError(reVar.getMessage());
                    } else if (qqVar.f5720a < 500 || qqVar.f5720a >= 599) {
                        String str = "Network Error. ";
                        if (qqVar != null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append(" Code: ");
                            sb.append(qqVar.f5720a);
                            sb.append(".");
                            String sb2 = sb.toString();
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(sb2);
                            sb3.append(" Data: \n");
                            sb3.append(new String(qqVar.b));
                            str = sb3.toString();
                        }
                        videoAdError = VideoAdError.createInternalError(str);
                    } else {
                        videoAdError = VideoAdError.createRetriableError("Server temporarily unavailable. Please, try again later.");
                    }
                }
                this.f5781a.onFailure(videoAdError);
            }
        }
    }

    private static class c {
        private static void a(Builder builder, String str, String str2) {
            if (!TextUtils.isEmpty(str2)) {
                builder.appendQueryParameter(str, str2);
            }
        }

        static /* synthetic */ ti a(ss ssVar, fc fcVar) {
            VideoAdRequest a2 = ssVar.a();
            BlocksInfo blocksInfo = a2.getBlocksInfo();
            Builder buildUpon = Uri.parse(sw.a(fcVar)).buildUpon();
            buildUpon.appendPath("v1").appendPath("getvideo").appendQueryParameter("page_id", blocksInfo.getPartnerId()).appendQueryParameter("imp-id", a2.getBlockId()).appendQueryParameter("target-ref", a2.getTargetRef()).appendQueryParameter("page-ref", a2.getPageRef()).appendQueryParameter("rnd", Integer.toString(sw.a().nextInt(89999999) + 10000000)).appendQueryParameter("video-session-id", a2.getBlocksInfo().getSessionId()).appendQueryParameter(HttpRequest.PARAM_CHARSET, a2.getCharset().getValue());
            a(buildUpon, "video-api-version", String.format("android-v%s", new Object[]{MobileAds.getLibraryVersion()}));
            a(buildUpon, "video-width", a2.getPlayerWidthPix());
            a(buildUpon, "video-height", a2.getPlayerHeightPix());
            a(buildUpon, "video-content-id", a2.getVideoContentId());
            a(buildUpon, "video-content-name", a2.getVideoContentName());
            a(buildUpon, "video-publisher-id", a2.getPublisherId());
            a(buildUpon, "video-publisher-name", a2.getPublisherName());
            a(buildUpon, "video-maxbitrate", a2.getMaxBitrate());
            a(buildUpon, "video-genre-id", a2.getGenreId());
            a(buildUpon, "video-genre-name", a2.getGenreName());
            a(buildUpon, "tags-list", a2.getTagsList());
            a(buildUpon, "ext-param", a2.getExtParams());
            buildUpon.appendQueryParameter("uuid", fcVar.d());
            return new ti(a2, buildUpon.build().toString(), new b(ssVar), new uf());
        }
    }

    static Random a() {
        if (f5779a == null) {
            return new Random();
        }
        return f5779a;
    }

    static /* synthetic */ String a(fc fcVar) {
        String f = fcVar.f();
        return TextUtils.isEmpty(f) ? "https://mobile.yandexadexchange.net" : f;
    }
}
