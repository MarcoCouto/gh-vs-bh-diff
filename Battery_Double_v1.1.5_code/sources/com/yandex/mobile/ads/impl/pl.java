package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.gu.b;

public final class pl {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cg f5695a;
    @NonNull
    private final mz b;
    @NonNull
    private final qa c = new qa();
    @NonNull
    private final pn d;

    public pl(@NonNull Context context, @NonNull cg cgVar, @NonNull mz mzVar) {
        this.f5695a = cgVar;
        this.b = mzVar;
        this.d = new pn(context);
    }

    public final void a(@NonNull Context context, @NonNull nw nwVar) {
        Intent a2 = this.d.a(nwVar.c());
        if (a2 != null) {
            Context a3 = qa.a(context);
            if (a3 != null) {
                this.f5695a.a(b.DEEPLINK);
                a3.startActivity(a2);
            }
            return;
        }
        this.b.a(nwVar.b());
    }
}
