package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.concurrent.TimeUnit;

final class gb implements fx {

    /* renamed from: a reason: collision with root package name */
    private static final long f5504a = TimeUnit.SECONDS.toMillis(1);
    private static final Object b = new Object();
    private static volatile gb c;
    @NonNull
    private final Handler d = new Handler(Looper.getMainLooper());
    @NonNull
    private final fz e;
    @NonNull
    private final fy f;
    @Nullable
    private fu g;
    private boolean h;

    @NonNull
    static gb a(@NonNull Context context) {
        if (c == null) {
            synchronized (b) {
                if (c == null) {
                    c = new gb(context);
                }
            }
        }
        return c;
    }

    private gb(@NonNull Context context) {
        this.e = new fz(context);
        this.f = new fy();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull ga gaVar) {
        synchronized (b) {
            fu fuVar = this.g;
            if (fuVar != null) {
                gaVar.a(fuVar);
            } else {
                this.f.a(gaVar);
                if (!this.h) {
                    this.h = true;
                    this.d.postDelayed(new Runnable() {
                        public final void run() {
                            gb.this.a();
                        }
                    }, f5504a);
                    this.e.a((fx) this);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b(@NonNull ga gaVar) {
        synchronized (b) {
            this.f.b(gaVar);
        }
    }

    public final void a(@NonNull fu fuVar) {
        synchronized (b) {
            this.g = fuVar;
            b();
            this.f.a(fuVar);
        }
    }

    public final void a() {
        synchronized (b) {
            b();
            this.f.a();
        }
    }

    private void b() {
        this.d.removeCallbacksAndMessages(null);
        this.h = false;
    }
}
