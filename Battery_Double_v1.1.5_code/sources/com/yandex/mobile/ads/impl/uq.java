package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.ArrayList;
import java.util.List;

final class uq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final un f5813a;
    /* access modifiers changed from: private */
    @NonNull
    public final List<VideoAd> b = new ArrayList();
    /* access modifiers changed from: private */
    @Nullable
    public RequestListener<List<VideoAd>> c;
    /* access modifiers changed from: private */
    public int d;

    private class a implements RequestListener<List<VideoAd>> {
        private a() {
        }

        /* synthetic */ a(uq uqVar, byte b) {
            this();
        }

        public final /* synthetic */ void onSuccess(@NonNull Object obj) {
            List list = (List) obj;
            uq.this.d = uq.this.d - 1;
            uq.this.b.addAll(list);
            a();
        }

        public final void onFailure(@NonNull VideoAdError videoAdError) {
            uq.this.d = uq.this.d - 1;
            a();
        }

        private void a() {
            if (uq.this.d == 0 && uq.this.c != null) {
                uq.this.c.onSuccess(uq.this.b);
            }
        }
    }

    uq(@NonNull ub ubVar) {
        this.f5813a = new un(ubVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull List<VideoAd> list, @NonNull RequestListener<List<VideoAd>> requestListener) {
        if (list.isEmpty()) {
            requestListener.onSuccess(this.b);
            return;
        }
        this.c = requestListener;
        for (VideoAd videoAd : list) {
            this.d++;
            this.f5813a.a(context, videoAd, new a(this, 0));
        }
    }
}
