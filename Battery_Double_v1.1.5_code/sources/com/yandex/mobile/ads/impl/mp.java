package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.bc;

public final class mp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bc f5629a;
    private final int b;

    mp(@NonNull bc bcVar, int i) {
        this.f5629a = bcVar;
        this.b = i;
    }

    @NonNull
    public final am a(@NonNull Context context) {
        return this.f5629a.a(context, this.b);
    }
}
