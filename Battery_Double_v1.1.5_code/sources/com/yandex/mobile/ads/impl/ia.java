package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

final class ia implements ib {
    ia() {
    }

    @NonNull
    public final hy a(@NonNull eg egVar, @NonNull ec ecVar, @NonNull dy dyVar, @NonNull ii iiVar) {
        hw hwVar = new hw(egVar);
        hwVar.a(ecVar);
        return hwVar;
    }

    @NonNull
    public final hx a(@NonNull eg egVar, @NonNull ec ecVar, @NonNull ij ijVar, @NonNull ef efVar, @NonNull ii iiVar) {
        hv hvVar = new hv(egVar);
        hvVar.a(ecVar);
        return hvVar;
    }

    @NonNull
    public final hy a(@NonNull eg egVar, @NonNull ec ecVar) {
        hw hwVar = new hw(egVar);
        hwVar.a(ecVar);
        return hwVar;
    }
}
