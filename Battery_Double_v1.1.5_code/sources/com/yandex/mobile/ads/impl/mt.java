package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.annotation.NonNull;
import android.support.v4.internal.view.SupportMenu;
import android.view.View;

public final class mt extends View {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final dk f5632a;
    @NonNull
    private Paint b = new Paint();
    private int c;
    private int d;

    public mt(@NonNull Context context, @NonNull dk dkVar) {
        super(context);
        this.f5632a = dkVar;
        this.c = dk.a(context, 1.0f);
        this.d = dk.a(context, 0.5f);
        this.b.setStyle(Style.STROKE);
        this.b.setStrokeWidth((float) this.c);
        this.b.setColor(SupportMenu.CATEGORY_MASK);
        setClickable(false);
        setFocusable(false);
    }

    public final void setColor(int i) {
        if (this.b.getColor() != i) {
            this.b.setColor(i);
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect((float) this.d, (float) this.d, (float) (getWidth() - this.d), (float) (getHeight() - this.d), this.b);
    }
}
