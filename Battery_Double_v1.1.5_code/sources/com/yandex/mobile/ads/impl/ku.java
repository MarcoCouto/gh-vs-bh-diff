package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.i;

public final class ku implements ks {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final x f5596a;
    @NonNull
    private final kn b = new kn();

    public ku(@NonNull x xVar) {
        this.f5596a = xVar;
    }

    @NonNull
    public final ky a(@NonNull MediaView mediaView, @NonNull fc fcVar, @NonNull i iVar, @NonNull ap apVar) {
        ko koVar = new ko(mediaView.getContext(), this.f5596a, fcVar, apVar);
        mediaView.removeAllViews();
        ImageView a2 = koVar.a();
        a2.setAdjustViewBounds(true);
        a2.setScaleType(ScaleType.CENTER_CROP);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        mediaView.addView(a2, layoutParams);
        mediaView.addView(koVar.b(), layoutParams);
        return new lf(mediaView, new la(koVar.b()), new kx(koVar.a(), iVar));
    }
}
