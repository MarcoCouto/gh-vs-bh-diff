package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.a;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.b;
import com.yandex.mobile.ads.nativeads.bj;
import com.yandex.mobile.ads.nativeads.q;

public final class mx implements a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final x f5636a;
    @NonNull
    private final fc b;
    @NonNull
    private final q c;
    @NonNull
    private final na d;
    @NonNull
    private final b e;
    @NonNull
    private final bj f;
    @Nullable
    private gu.a g;

    public mx(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar, @NonNull q qVar, @NonNull bj bjVar) {
        this.f5636a = xVar;
        this.b = fcVar;
        this.c = qVar;
        this.f = bjVar;
        this.d = new na(new cv(context, fcVar));
        this.e = new b(qVar);
    }

    public final void a(@NonNull gu.a aVar) {
        this.g = aVar;
    }

    public final void a(@NonNull Context context, @NonNull nm nmVar, @NonNull af afVar) {
        this.c.a(nmVar);
        cv cvVar = new cv(context, this.b);
        y a2 = this.e.a();
        ch chVar = new ch(context, this.f5636a, this.b, this.g);
        mz mzVar = new mz(cvVar, this.f5636a, this.f.a(context, this.b, a2), chVar);
        nd ndVar = new nd(this.b, chVar, mzVar, afVar, this.c);
        this.d.a(nmVar.c());
        ndVar.a(context, nmVar.a());
        mzVar.a(nmVar.d());
    }
}
