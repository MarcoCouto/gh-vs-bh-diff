package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import com.yandex.mobile.ads.nativeads.i;

public final class kx extends ld<ImageView, nl> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final i f5598a;
    @NonNull
    private final lj b;

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        nl nlVar = (nl) obj;
        return this.b.a(((ImageView) view).getDrawable(), nlVar);
    }

    public kx(@NonNull ImageView imageView, @NonNull i iVar) {
        super(imageView);
        this.f5598a = iVar;
        this.b = new lj(iVar);
    }

    public final void a(@NonNull ImageView imageView) {
        imageView.setImageDrawable(null);
        super.a(imageView);
    }

    /* renamed from: a */
    public final void b(@NonNull ImageView imageView, @NonNull nl nlVar) {
        Bitmap a2 = this.f5598a.a(nlVar);
        if (a2 != null) {
            imageView.setImageBitmap(a2);
        }
    }
}
