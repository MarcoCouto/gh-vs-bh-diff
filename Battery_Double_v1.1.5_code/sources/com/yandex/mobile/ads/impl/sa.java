package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.rewarded.a;

public final class sa {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f5750a;

    public sa(@NonNull a aVar) {
        this.f5750a = aVar;
    }

    @Nullable
    public final rz a(@NonNull Context context, @NonNull fc fcVar, @Nullable x<String> xVar) {
        bi r = xVar != null ? xVar.r() : null;
        if (r == null) {
            return null;
        }
        if (r.c()) {
            bj b = r.b();
            if (b != null) {
                return new sb(context, fcVar, b);
            }
            return null;
        }
        bh a2 = r.a();
        if (a2 != null) {
            return new ry(a2, this.f5750a);
        }
        return null;
    }
}
