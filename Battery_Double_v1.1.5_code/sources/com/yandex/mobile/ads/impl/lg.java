package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.e;

public final class lg implements li {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final af f5602a;
    @NonNull
    private final e b;

    public final void a(@NonNull ni niVar, @NonNull View view) {
    }

    public lg(@NonNull af afVar, @NonNull e eVar) {
        this.f5602a = afVar;
        this.b = eVar;
    }

    public final void a(@NonNull ni niVar, @NonNull lp lpVar) {
        this.b.a(niVar, niVar.d(), this.f5602a, lpVar);
    }
}
