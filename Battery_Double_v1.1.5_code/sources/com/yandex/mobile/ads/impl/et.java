package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.impl.hu.a;

public final class et {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final hu f5474a = new hu();

    @NonNull
    public static <T extends View & a> ew a(@NonNull T t) {
        int i;
        RectF rectF = null;
        if (hu.a(t)) {
            i = dv.e((View) t);
            Rect rect = new Rect();
            if (t.getLocalVisibleRect(rect)) {
                rect.offset(t.getLeft(), t.getTop());
            } else {
                rect = null;
            }
            Context context = t.getContext();
            if (rect != null) {
                int a2 = dv.a(context, rect.left);
                int a3 = dv.a(context, rect.top);
                int a4 = dv.a(context, rect.right);
                int a5 = dv.a(context, rect.bottom);
                int i2 = a5 - a3;
                if (a4 - a2 > 0 && i2 > 0) {
                    rectF = new RectF((float) a2, (float) a3, (float) a4, (float) a5);
                }
            }
        } else {
            i = 0;
        }
        return new ew(i, rectF);
    }
}
