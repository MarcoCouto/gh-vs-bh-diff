package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

enum ep {
    AD_VIDEO_COMPLETE("advideocomplete"),
    IMPRESSION_TRACKING_START("impressionTrackingStart"),
    IMPRESSION_TRACKING_SUCCESS("impressionTrackingSuccess"),
    CLOSE("close"),
    OPEN("open"),
    REWARDED_AD_COMPLETE("rewardedAdComplete"),
    USE_CUSTOM_CLOSE("usecustomclose"),
    UNSPECIFIED("");
    
    @NonNull
    private final String i;

    private ep(String str) {
        this.i = str;
    }

    static ep a(@NonNull String str) {
        ep[] values;
        for (ep epVar : values()) {
            if (epVar.i.equals(str)) {
                return epVar;
            }
        }
        return UNSPECIFIED;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String a() {
        return this.i;
    }
}
