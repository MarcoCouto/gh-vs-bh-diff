package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ru.a;

public final class ak implements a<fg> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5370a;
    @NonNull
    private final aj b;
    @NonNull
    private final ai.a c;

    public final /* bridge */ /* synthetic */ void a(@NonNull Object obj) {
        this.b.a(this.f5370a, (fg) obj);
        this.c.a();
    }

    ak(@NonNull Context context, @NonNull aj ajVar, @NonNull ai.a aVar) {
        this.f5370a = context.getApplicationContext();
        this.b = ajVar;
        this.c = aVar;
    }

    public final void a(re reVar) {
        this.c.a(reVar);
    }
}
