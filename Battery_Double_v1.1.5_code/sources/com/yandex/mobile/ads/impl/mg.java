package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.h;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.j;
import com.yandex.mobile.ads.nativeads.o;
import com.yandex.mobile.ads.nativeads.s;
import java.util.Map;
import java.util.Set;

final class mg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ly f5622a = new ly();
    /* access modifiers changed from: private */
    @NonNull
    public final md b;
    @NonNull
    private final h c;

    mg(@NonNull md mdVar, @NonNull h hVar) {
        this.b = mdVar;
        this.c = hVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull o oVar, @NonNull i iVar, @NonNull s sVar, @NonNull mb mbVar) {
        Set a2 = this.c.a(oVar.c().c());
        h hVar = this.c;
        final o oVar2 = oVar;
        final i iVar2 = iVar;
        final Context context2 = context;
        final s sVar2 = sVar;
        final mb mbVar2 = mbVar;
        AnonymousClass1 r2 = new j() {
            public final void a(@NonNull Map<String, Bitmap> map) {
                ly.a(oVar2, map);
                iVar2.a(map);
                mg.this.b.a(context2, oVar2, iVar2, sVar2, mbVar2);
            }
        };
        hVar.a(a2, r2);
    }
}
