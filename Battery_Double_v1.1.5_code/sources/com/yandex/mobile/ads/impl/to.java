package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.vast.tags.VastTagName;
import com.yandex.mobile.ads.video.models.vmap.AdBreak;
import com.yandex.mobile.ads.video.models.vmap.AdSource;
import com.yandex.mobile.ads.video.models.vmap.TimeOffset;
import com.yandex.mobile.ads.video.models.vmap.d;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class to {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tn f5793a;
    @NonNull
    private final tp b;
    @NonNull
    private final tr c = new tr();
    @NonNull
    private final tt d;
    @NonNull
    private final tu e = new tu();
    @NonNull
    private final tv f = new tv();

    public to(@NonNull tn tnVar, @NonNull tt ttVar) {
        this.f5793a = tnVar;
        this.d = ttVar;
        this.b = new tp(tnVar);
    }

    @Nullable
    static AdBreak a(@NonNull XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        TimeOffset timeOffset;
        tn.a(xmlPullParser, "AdBreak");
        String attributeValue = xmlPullParser.getAttributeValue(null, "breakId");
        Long a2 = tu.a(xmlPullParser);
        String attributeValue2 = xmlPullParser.getAttributeValue(null, "timeOffset");
        if (!TextUtils.isEmpty(attributeValue2)) {
            timeOffset = d.a(attributeValue2);
        } else {
            timeOffset = null;
        }
        List a3 = tr.a(xmlPullParser);
        ArrayList arrayList = new ArrayList();
        AdSource adSource = null;
        while (tn.b(xmlPullParser)) {
            if (tn.a(xmlPullParser)) {
                String name = xmlPullParser.getName();
                if ("AdSource".equals(name)) {
                    adSource = tp.a(xmlPullParser);
                } else if (VastTagName.EXTENSIONS.equals(name)) {
                    arrayList.addAll(tt.a(xmlPullParser));
                } else {
                    tn.d(xmlPullParser);
                }
            }
        }
        if (adSource == null || timeOffset == null || a3.isEmpty()) {
            return null;
        }
        return d.a(adSource, attributeValue, a2, timeOffset, a3, arrayList);
    }
}
