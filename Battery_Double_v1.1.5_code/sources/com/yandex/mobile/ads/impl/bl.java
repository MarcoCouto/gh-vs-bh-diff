package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class bl {

    public interface a<T> {
        @Nullable
        T a(String str);
    }

    public static String a(Map<String, String> map, qj qjVar) {
        return (String) map.get(qjVar.a());
    }

    public static boolean b(@NonNull Map<String, String> map, @NonNull qj qjVar) {
        String a2 = a(map, qjVar);
        if (a2 == null) {
            return false;
        }
        return Boolean.parseBoolean(a2);
    }

    @Nullable
    public static String d(@NonNull Map<String, String> map, @NonNull qj qjVar) {
        List e = e(map, qjVar);
        if (e.isEmpty()) {
            return null;
        }
        return (String) e.get(0);
    }

    @NonNull
    public static List<String> e(Map<String, String> map, qj qjVar) {
        return a(map, qjVar, new a<String>() {
            @Nullable
            public final /* bridge */ /* synthetic */ Object a(String str) {
                return str;
            }
        });
    }

    @NonNull
    public static <T> List<T> a(Map<String, String> map, qj qjVar, a<T> aVar) {
        ArrayList arrayList = new ArrayList();
        String a2 = a(map, qjVar);
        if (!TextUtils.isEmpty(a2)) {
            for (String trim : (String[]) hc.a((T[]) a2.split(","))) {
                try {
                    String decode = URLDecoder.decode(trim.trim(), "UTF-8");
                    if (aVar.a(decode) != null) {
                        arrayList.add(aVar.a(decode));
                    }
                } catch (Exception unused) {
                }
            }
        }
        return arrayList;
    }

    public static int c(Map<String, String> map, qj qjVar) {
        return di.b(a(map, qjVar));
    }
}
