package com.yandex.mobile.ads.impl;

import android.app.Activity;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class q implements o {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Activity f5706a;
    @Nullable
    private final ResultReceiver b;

    public q(@NonNull Activity activity, @Nullable ResultReceiver resultReceiver) {
        this.f5706a = activity;
        this.b = resultReceiver;
    }

    public final void a(int i) {
        this.f5706a.setRequestedOrientation(i);
    }

    public final void a(int i, @Nullable Bundle bundle) {
        if (this.b != null) {
            this.b.send(i, bundle);
        }
    }

    public final void a() {
        this.f5706a.finish();
    }
}
