package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class gk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Object f5512a;

    gk(@NonNull Object obj) {
        this.f5512a = obj;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final gm a() {
        Object a2 = he.a(this.f5512a, "getLastLocation", new Object[0]);
        if (a2 != null) {
            return new gm(a2);
        }
        return null;
    }
}
