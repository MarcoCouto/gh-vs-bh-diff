package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View.OnClickListener;
import com.yandex.mobile.ads.nativeads.a;
import com.yandex.mobile.ads.nativeads.af;

public final class lq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ni f5608a;
    @NonNull
    private final a b;
    @NonNull
    private final af c;
    @Nullable
    private final as d;

    public lq(@NonNull ni niVar, @NonNull a aVar, @NonNull af afVar, @Nullable as asVar) {
        this.f5608a = niVar;
        this.b = aVar;
        this.c = afVar;
        this.d = asVar;
    }

    @NonNull
    public final OnClickListener a(@NonNull nm nmVar) {
        return this.c.d().a(this.f5608a, nmVar, this.b, this.c, this.d);
    }
}
