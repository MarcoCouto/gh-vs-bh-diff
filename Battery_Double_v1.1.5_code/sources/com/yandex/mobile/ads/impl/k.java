package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Window;
import android.widget.RelativeLayout;

final class k implements m {
    k() {
    }

    @Nullable
    public final l a(@NonNull Context context, @NonNull RelativeLayout relativeLayout, @Nullable ResultReceiver resultReceiver, @NonNull o oVar, @NonNull Intent intent, @NonNull Window window) {
        String stringExtra = intent.getStringExtra("extra_browser_url");
        if (TextUtils.isEmpty(stringExtra)) {
            return null;
        }
        j jVar = new j(context, relativeLayout, oVar, window, stringExtra);
        return jVar;
    }
}
