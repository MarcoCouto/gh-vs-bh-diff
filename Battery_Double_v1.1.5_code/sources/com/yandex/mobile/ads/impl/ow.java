package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.y;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

final class ow {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ov f5684a = new ov();
    @NonNull
    private final oj b;

    ow(@NonNull oj ojVar) {
        this.b = ojVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final nx a(@NonNull JSONObject jSONObject) throws JSONException, y {
        HashMap hashMap;
        String a2 = oh.a(jSONObject, "package");
        String a3 = this.b.a(jSONObject, "url");
        JSONObject optJSONObject = jSONObject.optJSONObject("extras");
        if (optJSONObject != null) {
            Iterator keys = optJSONObject.keys();
            if (keys.hasNext()) {
                hashMap = new HashMap();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if (!optJSONObject.isNull(str)) {
                        hashMap.put(str, optJSONObject.get(str));
                    }
                }
                return new nx(a2, a3, hashMap);
            }
        }
        hashMap = null;
        return new nx(a2, a3, hashMap);
    }
}
