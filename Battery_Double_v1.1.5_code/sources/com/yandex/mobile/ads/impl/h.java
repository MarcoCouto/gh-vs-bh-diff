package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.b;

abstract class h extends ht {
    private boolean g;
    private Runnable h = new Runnable() {
        public final void run() {
            h.this.a(h.this.j());
        }
    };

    h(@NonNull Context context, @NonNull ca caVar, @NonNull b bVar) {
        super(context, caVar, bVar);
        if (p() && dl.b()) {
            this.g = true;
        }
    }

    public void d() {
        super.d();
        a(false);
    }

    private void a() {
        this.f5352a.removeCallbacks(this.h);
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        this.g = z;
        if (this.g) {
            g();
        } else {
            a();
        }
    }

    private void g() {
        a();
        x y = y();
        if (y != null && y.v() && this.g && !D()) {
            this.f5352a.postDelayed(this.h, (long) y.o());
            new Object[1][0] = Integer.valueOf(y.m());
        }
    }

    public void b(int i) {
        super.b(i);
        g();
    }

    public void a(@NonNull Intent intent) {
        super.a(intent);
        g();
    }

    /* access modifiers changed from: protected */
    public final void i() {
        super.i();
        g();
    }

    public void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        super.onAdFailedToLoad(adRequestError);
        if (5 != adRequestError.getCode() && 2 != adRequestError.getCode()) {
            g();
        }
    }
}
