package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.b;

public abstract class hr extends aa<String> implements ec {
    @Nullable
    private hy g;

    /* access modifiers changed from: protected */
    public abstract hy a(@NonNull String str, @NonNull x<String> xVar, @NonNull al alVar);

    /* access modifiers changed from: protected */
    public abstract boolean a(@NonNull al alVar);

    public final void b(boolean z) {
    }

    hr(@NonNull Context context, @NonNull b bVar) {
        super(context, bVar);
    }

    public void b(@NonNull x<String> xVar) {
        al b = this.e.b();
        if (b == null) {
            onAdFailedToLoad(v.d);
        } else if (!a(xVar.e(), b)) {
            onAdFailedToLoad(v.c);
        } else {
            String str = (String) xVar.t();
            if (!TextUtils.isEmpty(str)) {
                this.g = a(str, xVar, b);
                this.g.a(str);
                return;
            }
            onAdFailedToLoad(v.e);
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final bp<String> a(String str, String str2) {
        ik ikVar = new ik(this.b, this.e, str, str2, this);
        return ikVar;
    }

    /* access modifiers changed from: protected */
    public final boolean a(@NonNull al alVar, @NonNull al alVar2) {
        return a(alVar) && iq.a(this.b, alVar, alVar2);
    }

    public void d() {
        super.d();
        if (this.g != null) {
            this.g.b();
        }
        this.g = null;
    }

    public final void b(@NonNull dh dhVar) {
        a(this.e.c(), dhVar);
    }
}
