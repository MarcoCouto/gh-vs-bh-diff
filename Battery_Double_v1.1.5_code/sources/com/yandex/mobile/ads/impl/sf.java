package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.a;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class sf {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private fr f5754a;

    public sf(@NonNull Context context) {
        this.f5754a = fp.a(context);
    }

    public final void a(@NonNull Context context, @NonNull VmapRequestConfiguration vmapRequestConfiguration, @NonNull si siVar) {
        sg.a(context).a(context, vmapRequestConfiguration, this.f5754a, (RequestListener<Vmap>) siVar);
    }

    public final void a(@NonNull Context context, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull a aVar) {
        sg.a(context).a(context, vastRequestConfiguration, this.f5754a, (RequestListener<sk>) aVar);
    }
}
