package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.base.a;
import java.util.Map;

public final class au<T extends a> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final T f5380a;
    @NonNull
    private final bf b;
    @NonNull
    private final at c;

    au(@NonNull T t, @NonNull bf bfVar, @NonNull at atVar) {
        this.f5380a = t;
        this.b = bfVar;
        this.c = atVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final T a() {
        return this.f5380a;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bf b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, Object> a(@NonNull Context context) {
        return this.c.a(context);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> c() {
        return this.c.a(this.b);
    }
}
