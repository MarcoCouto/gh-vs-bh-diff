package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;

public final class pj {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5694a = new Object();
    @Nullable
    private static volatile pj b;
    @NonNull
    private final pz c;
    @Nullable
    private String[] d;

    private pj(@NonNull Context context) {
        this.c = new pz(context);
    }

    public static pj a(@NonNull Context context) {
        if (b == null) {
            synchronized (f5694a) {
                if (b == null) {
                    b = new pj(context.getApplicationContext());
                }
            }
        }
        return b;
    }

    @NonNull
    public final String[] a() {
        if (this.d == null) {
            ArrayList arrayList = new ArrayList();
            if (this.c.a("com.android.launcher.permission.INSTALL_SHORTCUT") && this.c.a("com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
                arrayList.add("shortcut");
            }
            this.d = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        return this.d;
    }
}
