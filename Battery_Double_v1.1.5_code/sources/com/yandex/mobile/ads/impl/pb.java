package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class pb implements oy<nl> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final oj f5688a;

    public pb(@NonNull Context context) {
        this.f5688a = new oj(new ds(context));
    }

    @NonNull
    /* renamed from: b */
    public final nl a(@NonNull JSONObject jSONObject) throws JSONException, y {
        if (!jSONObject.has("value") || jSONObject.isNull("value")) {
            throw new y("Native Ad json has not required attributes");
        }
        nl nlVar = new nl();
        JSONObject jSONObject2 = jSONObject.getJSONObject("value");
        nlVar.a(this.f5688a.a(jSONObject2, "url"));
        nlVar.a(jSONObject2.getInt("w"));
        nlVar.b(jSONObject2.getInt("h"));
        String optString = jSONObject2.optString("sizeType");
        if (!TextUtils.isEmpty(optString)) {
            nlVar.b(optString);
        }
        return nlVar;
    }
}
