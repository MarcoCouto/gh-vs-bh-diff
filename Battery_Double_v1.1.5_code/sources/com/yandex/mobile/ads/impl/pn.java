package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

final class pn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final pz f5696a;
    @NonNull
    private final pm b = new pm();

    pn(@NonNull Context context) {
        this.f5696a = new pz(context);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final Intent a(@NonNull List<nx> list) {
        for (nx a2 : list) {
            Intent a3 = pm.a(a2);
            if (this.f5696a.a(a3)) {
                return a3;
            }
        }
        return null;
    }
}
