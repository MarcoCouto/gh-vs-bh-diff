package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class bi implements Parcelable {
    public static final Creator<bi> CREATOR = new Creator<bi>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new bi[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new bi(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final boolean f5393a;
    @Nullable
    private final bh b;
    @Nullable
    private final bj c;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public boolean f5394a;
        /* access modifiers changed from: private */
        @Nullable
        public bh b;
        /* access modifiers changed from: private */
        @Nullable
        public bj c;

        @NonNull
        public final bi a() {
            return new bi(this, 0);
        }

        @NonNull
        public final a a(boolean z) {
            this.f5394a = z;
            return this;
        }

        @NonNull
        public final a a(@Nullable bh bhVar) {
            this.b = bhVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable bj bjVar) {
            this.c = bjVar;
            return this;
        }
    }

    public int describeContents() {
        return 0;
    }

    /* synthetic */ bi(a aVar, byte b2) {
        this(aVar);
    }

    private bi(@NonNull a aVar) {
        this.b = aVar.b;
        this.c = aVar.c;
        this.f5393a = aVar.f5394a;
    }

    @Nullable
    public final bh a() {
        return this.b;
    }

    @Nullable
    public final bj b() {
        return this.c;
    }

    public final boolean c() {
        return this.f5393a;
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeByte(this.f5393a ? (byte) 1 : 0);
        parcel.writeParcelable(this.b, i);
        parcel.writeParcelable(this.c, i);
    }

    protected bi(@NonNull Parcel parcel) {
        this.f5393a = parcel.readByte() != 0;
        this.b = (bh) parcel.readParcelable(bh.class.getClassLoader());
        this.c = (bj) parcel.readParcelable(bj.class.getClassLoader());
    }
}
