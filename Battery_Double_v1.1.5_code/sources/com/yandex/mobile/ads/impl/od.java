package com.yandex.mobile.ads.impl;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;

public final class od {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private static final Object f5667a = new Object();
    private static volatile od b;
    @NonNull
    private final rl c;
    @NonNull
    private final b d;
    @NonNull
    private final oc e;

    @TargetApi(12)
    static class a implements com.yandex.mobile.ads.impl.rl.b {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final LruCache<String, Bitmap> f5669a;

        a(@NonNull LruCache<String, Bitmap> lruCache) {
            this.f5669a = lruCache;
        }

        public final Bitmap a(String str) {
            return (Bitmap) this.f5669a.get(str);
        }

        public final void a(String str, Bitmap bitmap) {
            this.f5669a.put(str, bitmap);
        }
    }

    public interface b {
        @Nullable
        Bitmap a(@NonNull String str);

        void a(@NonNull String str, @NonNull Bitmap bitmap);
    }

    @TargetApi(12)
    @NonNull
    public static od a(@NonNull Context context) {
        if (b == null) {
            synchronized (f5667a) {
                if (b == null) {
                    b = new od(context);
                }
            }
        }
        return b;
    }

    private od(@NonNull Context context) {
        AnonymousClass1 r1 = new LruCache<String, Bitmap>(dn.a(context)) {
            /* access modifiers changed from: protected */
            public final /* synthetic */ int sizeOf(Object obj, Object obj2) {
                String str = (String) obj;
                Bitmap bitmap = (Bitmap) obj2;
                if (bitmap != null) {
                    return bitmap.getByteCount() / 1024;
                }
                return super.sizeOf(str, null);
            }
        };
        qs a2 = bo.a(context);
        a2.a();
        a aVar = new a(r1);
        oa oaVar = new oa();
        this.d = new oe(r1, oaVar);
        this.e = new oc(dp.i(context));
        this.c = new ob(a2, aVar, oaVar, this.e);
    }

    @NonNull
    public final b a() {
        return this.d;
    }

    @NonNull
    public final rl b() {
        return this.c;
    }
}
