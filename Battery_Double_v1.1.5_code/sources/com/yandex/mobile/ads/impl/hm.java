package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.AdActivity;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.AdSize;
import com.yandex.mobile.ads.a;
import com.yandex.mobile.ads.b;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

public abstract class hm extends ht {
    @NonNull
    private final hl g;
    @NonNull
    private final jo h = new jo();
    @Nullable
    private jm i;
    @Nullable
    private jm j;
    @NonNull
    private final m k = new it();

    /* access modifiers changed from: protected */
    @NonNull
    public abstract jm a(@NonNull jn jnVar);

    /* access modifiers changed from: protected */
    public final boolean a(int i2) {
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return true;
    }

    public hm(@NonNull Context context, @NonNull b bVar, @NonNull hl hlVar) {
        super(context, new hk(), bVar);
        this.g = hlVar;
        b(a.a(AdSize.FULL_SCREEN));
        n.a().a("window_type_interstitial", this.k);
    }

    public void a() {
        if (this.i != null && !a_()) {
            this.i.b();
        }
    }

    public final void g() {
        Context context = this.b;
        x y = y();
        fc s = s();
        y yVar = this.d;
        if (context != null) {
            Intent intent = new Intent(context, AdActivity.class);
            intent.putExtra("window_type", "window_type_interstitial");
            intent.putExtra("extra_receiver", di.a((ResultReceiver) yVar));
            intent.putExtra("extra_interstitial_isShouldOpenLinksInApp", s.j());
            intent.addFlags(268435456);
            ir a2 = ir.a();
            a2.a(y);
            a2.a(s);
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                hi.c("Failed to show Interstitial Ad. Exception: ".concat(String.valueOf(e)), new Object[0]);
            }
        }
    }

    public final void a(AdRequest adRequest) {
        u();
        super.a(adRequest);
    }

    public final boolean z() {
        return this.i != null && this.i.a();
    }

    /* access modifiers changed from: protected */
    public final boolean a(@NonNull al alVar) {
        return alVar.b(this.b) > 0 && alVar.a(this.b) > 0;
    }

    public void a(int i2, @Nullable Bundle bundle) {
        StringBuilder sb = new StringBuilder("onReceiveResult(), resultCode = ");
        sb.append(i2);
        sb.append(", clazz = ");
        sb.append(getClass());
        if (i2 == 0) {
            a((WebView) null, bundle != null ? (Map) bundle.getSerializable("extra_tracking_parameters") : null);
        } else if (i2 != 8) {
            switch (i2) {
                case 2:
                    b(0);
                    return;
                case 3:
                    b(8);
                    return;
                case 4:
                    B();
                    return;
                case 5:
                    return;
                case 6:
                    onAdOpened();
                    return;
                default:
                    super.a(i2, bundle);
                    return;
            }
        } else {
            onAdClosed();
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final hy a(@NonNull String str, @NonNull x<String> xVar, @NonNull al alVar, @NonNull ap apVar) {
        ho hoVar = new ho(this);
        hn hnVar = new hn(this.b, xVar, s());
        new hz();
        boolean a2 = hz.a(str);
        ic.a();
        return ic.a(a2).a(hnVar, hoVar);
    }

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        A();
        super.a(webView, map);
    }

    public final void e() {
        a(this.b, this.j, this.i);
        super.e();
    }

    public final synchronized void d() {
        super.d();
    }

    public final void A() {
        this.g.g();
        if (this.j != this.i) {
            a(this.b, this.j);
            this.j = this.i;
        }
    }

    private static void a(@NonNull Context context, @NonNull jm... jmVarArr) {
        for (jm jmVar : new HashSet(Arrays.asList(jmVarArr))) {
            if (jmVar != null) {
                jmVar.a(context);
            }
        }
    }

    public void onAdClosed() {
        super.onAdClosed();
        this.g.a();
    }

    public void onAdOpened() {
        super.onAdOpened();
        this.g.f();
    }

    /* access modifiers changed from: protected */
    public final void i() {
        this.g.e();
    }

    /* access modifiers changed from: protected */
    public final void a(@NonNull AdRequestError adRequestError) {
        this.g.a(adRequestError);
    }

    public void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        super.onAdFailedToLoad(adRequestError);
        v();
    }

    public void onAdLeftApplication() {
        super.onAdLeftApplication();
        this.g.d();
    }

    public final void B() {
        v();
        this.g.b();
    }

    public final void f() {
        this.g.c();
    }

    public void a(@NonNull x<String> xVar) {
        super.a(xVar);
        this.i = a(jo.a(xVar));
        this.i.a(this.b, xVar);
    }
}
