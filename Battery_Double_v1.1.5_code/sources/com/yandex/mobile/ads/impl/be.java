package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class be implements Parcelable {
    public static final Creator<be> CREATOR = new Creator<be>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new be[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new be(parcel);
        }
    };
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<bf> f5388a;
    @NonNull
    private final Map<String, String> b;

    public int describeContents() {
        return 0;
    }

    public be(@NonNull List<bf> list, @NonNull Map<String, String> map) {
        this.f5388a = list;
        this.b = map;
    }

    @NonNull
    public final List<bf> a() {
        return this.f5388a;
    }

    @NonNull
    public final Map<String, String> b() {
        return this.b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.f5388a);
        parcel.writeInt(this.b.size());
        for (Entry entry : this.b.entrySet()) {
            parcel.writeString((String) entry.getKey());
            parcel.writeString((String) entry.getValue());
        }
    }

    protected be(Parcel parcel) {
        this.f5388a = new ArrayList();
        parcel.readList(this.f5388a, bf.class.getClassLoader());
        int readInt = parcel.readInt();
        this.b = new HashMap(readInt);
        for (int i = 0; i < readInt; i++) {
            this.b.put(parcel.readString(), parcel.readString());
        }
    }
}
