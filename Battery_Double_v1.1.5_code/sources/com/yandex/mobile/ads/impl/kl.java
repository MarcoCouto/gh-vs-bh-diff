package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class kl<V extends View, T> extends kj<V, T> implements kk<T> {
    public kl(@NonNull ld<V, T> ldVar) {
        super(ldVar);
    }

    public final void c(@NonNull T t) {
        a(t);
    }
}
