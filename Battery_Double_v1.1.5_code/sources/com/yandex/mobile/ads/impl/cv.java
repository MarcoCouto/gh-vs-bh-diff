package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class cv {

    /* renamed from: a reason: collision with root package name */
    private static final ExecutorService f5424a = Executors.newCachedThreadPool(new Cdo("YandexMobileAds.UrlTracker"));
    @NonNull
    private final Context b;
    @Nullable
    private fc c;
    @NonNull
    private final ds d = new ds(this.b);

    private static class a implements Runnable {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final String f5425a;
        @NonNull
        private final de b;
        @NonNull
        private final ds c;

        a(@NonNull String str, @NonNull de deVar, @NonNull ds dsVar) {
            this.f5425a = str;
            this.b = deVar;
            this.c = dsVar;
        }

        public final void run() {
            String a2 = this.c.a(this.f5425a);
            if (!TextUtils.isEmpty(a2)) {
                this.b.a(a2);
            }
        }
    }

    public cv(@NonNull Context context, @NonNull fc fcVar) {
        this.b = context.getApplicationContext();
        this.c = fcVar;
    }

    public final void a(@Nullable String str) {
        a(str, new dc(this.b));
    }

    public final void a(@Nullable String str, @NonNull x xVar, @NonNull db dbVar) {
        a(str, xVar, dbVar, new ch(this.b, xVar, this.c, null));
    }

    public final void a(@Nullable String str, @NonNull x xVar, @NonNull db dbVar, @NonNull cg cgVar) {
        a(str, new dd(this.b, xVar, cgVar, dbVar));
    }

    private void a(@Nullable String str, @NonNull de deVar) {
        if (!TextUtils.isEmpty(str)) {
            f5424a.execute(new a(str, deVar, this.d));
        }
    }
}
