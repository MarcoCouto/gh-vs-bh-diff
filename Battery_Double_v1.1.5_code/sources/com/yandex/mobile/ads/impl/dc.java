package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class dc implements de {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5434a;
    @NonNull
    private final ah b = ah.a();

    private static class a implements com.yandex.mobile.ads.impl.ru.a<qq> {

        /* renamed from: a reason: collision with root package name */
        private final String f5435a;

        public final /* synthetic */ void a(Object obj) {
            Object[] objArr = {this.f5435a, Integer.valueOf(((qq) obj).f5720a)};
        }

        a(String str) {
            this.f5435a = str;
        }

        public final void a(@NonNull re reVar) {
            Object[] objArr = {this.f5435a, reVar.toString()};
        }
    }

    public dc(@NonNull Context context) {
        this.f5434a = context.getApplicationContext();
    }

    public final void a(@NonNull String str) {
        this.b.a(this.f5434a, (qr) new bq(str, new a(str)));
    }
}
