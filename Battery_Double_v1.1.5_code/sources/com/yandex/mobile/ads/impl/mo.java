package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.v4.internal.view.SupportMenu;
import android.view.View;
import android.widget.FrameLayout;

final class mo {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final mr f5628a = new mr();
    @NonNull
    private final mu b = new mu();
    @NonNull
    private final dk c = new dk();

    mo() {
    }

    private static void b(@NonNull FrameLayout frameLayout) {
        View findViewById = frameLayout.findViewById(7845);
        if (findViewById != null) {
            frameLayout.removeView(findViewById);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull am amVar, @NonNull FrameLayout frameLayout, boolean z) {
        mt mtVar = (mt) frameLayout.findViewById(7846);
        if (mtVar == null) {
            mtVar = new mt(frameLayout.getContext(), this.c);
            mtVar.setId(7846);
            frameLayout.addView(mtVar);
        }
        mtVar.setColor(z ? SupportMenu.CATEGORY_MASK : -16711936);
        if (z) {
            mw mwVar = (mw) frameLayout.findViewById(7845);
            if (mwVar == null) {
                mwVar = new mw(frameLayout.getContext());
                mwVar.setId(7845);
                frameLayout.addView(mwVar);
            }
            mwVar.setDescription(this.f5628a.a(amVar));
            return;
        }
        b(frameLayout);
    }

    static void a(@NonNull FrameLayout frameLayout) {
        View findViewById = frameLayout.findViewById(7846);
        if (findViewById != null) {
            frameLayout.removeView(findViewById);
        }
        b(frameLayout);
    }
}
