package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.p;
import java.util.ArrayList;
import java.util.List;

public final class px {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final p f5703a = new p();

    @NonNull
    public static List<String> a(@NonNull x<nq> xVar) {
        List<np> c = c(xVar);
        ArrayList arrayList = new ArrayList();
        for (np a2 : c) {
            arrayList.addAll(p.a(a2));
        }
        return arrayList;
    }

    @NonNull
    public static List<String> b(@NonNull x<nq> xVar) {
        List<np> c = c(xVar);
        ArrayList arrayList = new ArrayList();
        for (np b : c) {
            NativeAdType b2 = b.b();
            if (b2 != null) {
                arrayList.add(b2.getValue());
            }
        }
        return arrayList;
    }

    @NonNull
    private static List<np> c(@NonNull x<nq> xVar) {
        nq nqVar = (nq) xVar.t();
        List<np> c = nqVar != null ? nqVar.c() : null;
        if (c != null) {
            return c;
        }
        return new ArrayList();
    }
}
