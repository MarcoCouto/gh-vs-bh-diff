package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.yandex.mobile.ads.AdRequestError;
import java.lang.ref.WeakReference;

public final class t implements ac {
    private static final SparseArray<AdRequestError> c;

    /* renamed from: a reason: collision with root package name */
    private final ah f5783a = ah.a();
    private final WeakReference<aa> b;

    public final boolean a_() {
        return false;
    }

    public t(aa aaVar) {
        this.b = new WeakReference<>(aaVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull bp bpVar) {
        aa aaVar = (aa) this.b.get();
        if (aaVar != null) {
            this.f5783a.a(aaVar.m(), (qr) bpVar);
        }
    }

    public final void a() {
        aa aaVar = (aa) this.b.get();
        if (aaVar != null) {
            this.f5783a.a(aaVar.m(), di.a(aaVar));
        }
    }

    public final void b() {
        a();
        this.b.clear();
    }

    public static AdRequestError a(int i) {
        return (AdRequestError) c.get(i, v.s);
    }

    static {
        SparseArray<AdRequestError> sparseArray = new SparseArray<>();
        c = sparseArray;
        sparseArray.put(6, v.j);
        c.put(2, v.g);
        c.put(5, v.e);
        c.put(8, v.f);
        c.put(10, v.m);
        c.put(4, v.m);
        c.put(9, v.h);
        c.put(7, v.l);
    }
}
