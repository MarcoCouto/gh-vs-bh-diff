package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class fb implements ey {

    /* renamed from: a reason: collision with root package name */
    private final boolean f5481a;

    public fb(boolean z) {
        this.f5481a = z;
    }

    @NonNull
    public final String a() {
        return String.format("viewable: %s", new Object[]{Boolean.valueOf(this.f5481a)});
    }
}
