package com.yandex.mobile.ads.impl;

import android.util.Log;
import java.util.Locale;

public class qw {

    /* renamed from: a reason: collision with root package name */
    public static String f5725a = "Volley";
    public static boolean b = Log.isLoggable("Volley", 2);

    public static void a(String str) {
        b("Changing log tag to %s", str);
        f5725a = str;
        b = Log.isLoggable(str, 2);
    }

    public static void a(String str, Object... objArr) {
        if (b) {
            Log.v(f5725a, d(str, objArr));
        }
    }

    public static void b(String str, Object... objArr) {
        Log.d(f5725a, d(str, objArr));
    }

    public static void c(String str, Object... objArr) {
        Log.e(f5725a, d(str, objArr));
    }

    public static void a(Throwable th, String str, Object... objArr) {
        Log.e(f5725a, d(str, objArr), th);
    }

    private static String d(String str, Object... objArr) {
        if (objArr != null) {
            str = String.format(Locale.US, str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        String str2 = "<unknown>";
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                break;
            } else if (!stackTrace[i].getClass().equals(qw.class)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                String substring2 = substring.substring(substring.lastIndexOf(36) + 1);
                StringBuilder sb = new StringBuilder();
                sb.append(substring2);
                sb.append(".");
                sb.append(stackTrace[i].getMethodName());
                str2 = sb.toString();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", new Object[]{Long.valueOf(Thread.currentThread().getId()), str2, str});
    }
}
