package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.impl.gu.c;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class uh implements gv<VideoAd, List<VideoAd>> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ub f5806a;

    public uh(@NonNull ub ubVar) {
        this.f5806a = ubVar;
    }

    @NonNull
    private Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("page_id", this.f5806a.a());
        hashMap.put("imp_id", this.f5806a.b());
        return hashMap;
    }

    public final /* synthetic */ gu a(@Nullable qt qtVar, int i, @NonNull Object obj) {
        Map a2 = a();
        c cVar = 204 == i ? c.NO_ADS : (qtVar == null || qtVar.f5724a == null || i != 200) ? c.ERROR : ((List) qtVar.f5724a).isEmpty() ? c.NO_ADS : c.SUCCESS;
        a2.put("status", cVar.a());
        return new gu(b.VAST_WRAPPER_RESPONSE, a2);
    }

    public final /* synthetic */ gu a(Object obj) {
        return new gu(b.VAST_WRAPPER_REQUEST, a());
    }
}
