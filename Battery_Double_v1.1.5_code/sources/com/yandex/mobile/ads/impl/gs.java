package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.ReporterConfig.Builder;
import com.yandex.metrica.YandexMetrica;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class gs {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5519a = new Object();
    private static volatile gs b;
    @Nullable
    private final IReporter c;
    @NonNull
    private final gt d;
    @NonNull
    private final gr e;

    private gs(@Nullable IReporter iReporter, @NonNull gt gtVar, @NonNull gr grVar) {
        this.c = iReporter;
        this.d = gtVar;
        this.e = grVar;
        this.d.a(iReporter);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:20|21) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        b = new com.yandex.mobile.ads.impl.gs(null, r2, r3);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0056 */
    @NonNull
    public static gs a(@NonNull Context context) {
        if (b == null) {
            synchronized (f5519a) {
                if (b == null) {
                    String str = hc.a(context) ? "322a737a-a0ca-44e0-bc85-649b1c7c1db6" : "478cb909-6ad1-4e12-84cc-b3629a789f93";
                    gt gtVar = new gt(aj.a());
                    gr grVar = new gr();
                    Builder newConfigBuilder = ReporterConfig.newConfigBuilder(str);
                    fg a2 = ff.a().a(context);
                    YandexMetrica.activateReporter(context, newConfigBuilder.withStatisticsSending(a2 != null && a2.c()).build());
                    b = new gs(YandexMetrica.getReporter(context.getApplicationContext(), str), gtVar, grVar);
                }
            }
        }
        return b;
    }

    public final void a(@NonNull gu guVar) {
        if (ff.a().b() && this.c != null) {
            String b2 = guVar.b();
            Map a2 = guVar.a();
            try {
                HashMap hashMap = new HashMap();
                for (Entry entry : a2.entrySet()) {
                    hashMap.put((String) entry.getKey(), Arrays.deepToString(new Object[]{entry.getValue()}));
                }
                StringBuilder sb = new StringBuilder("reportEvent(), eventName = ");
                sb.append(b2);
                sb.append(", reportData = ");
                sb.append(hashMap);
                this.c.reportEvent(b2, a2);
            } catch (Throwable unused) {
            }
        }
    }
}
