package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class uo implements RequestListener<List<VideoAd>> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final um f5811a;
    @NonNull
    private final RequestListener<List<VideoAd>> b;

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        this.b.onSuccess(this.f5811a.a((List) obj));
    }

    uo(@NonNull VideoAd videoAd, @NonNull RequestListener<List<VideoAd>> requestListener) {
        this.b = requestListener;
        this.f5811a = new um(videoAd);
    }

    public final void onFailure(@NonNull VideoAdError videoAdError) {
        this.b.onFailure(videoAdError);
    }
}
