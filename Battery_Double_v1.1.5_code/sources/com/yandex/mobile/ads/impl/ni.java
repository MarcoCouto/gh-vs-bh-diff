package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class ni<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final T f5647a;
    @NonNull
    private final String b;
    @NonNull
    private final String c;
    @Nullable
    private final nm d;
    private final boolean e;
    private final boolean f;

    public ni(@NonNull String str, @NonNull String str2, @NonNull T t, @Nullable nm nmVar, boolean z, boolean z2) {
        this.b = str;
        this.c = str2;
        this.f5647a = t;
        this.d = nmVar;
        this.f = z;
        this.e = z2;
    }

    @NonNull
    public final String a() {
        return this.b;
    }

    @NonNull
    public final String b() {
        return this.c;
    }

    @NonNull
    public final T c() {
        return this.f5647a;
    }

    @Nullable
    public final nm d() {
        return this.d;
    }

    public final boolean e() {
        return this.f;
    }

    public final boolean f() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ni niVar = (ni) obj;
        if (this.e != niVar.e || this.f != niVar.f || !this.f5647a.equals(niVar.f5647a) || !this.b.equals(niVar.b) || !this.c.equals(niVar.c)) {
            return false;
        }
        if (this.d != null) {
            return this.d.equals(niVar.d);
        }
        return niVar.d == null;
    }

    public final int hashCode() {
        return (((((((((this.f5647a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0);
    }
}
