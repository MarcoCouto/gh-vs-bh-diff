package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ImageView;

public final class ko {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ImageView f5592a;
    @NonNull
    private final ed b;

    public ko(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar, @NonNull ap apVar) {
        this.f5592a = new ImageView(context);
        this.b = new ed(context, xVar, fcVar, apVar);
    }

    @NonNull
    public final ImageView a() {
        return this.f5592a;
    }

    @NonNull
    public final ed b() {
        return this.b;
    }
}
