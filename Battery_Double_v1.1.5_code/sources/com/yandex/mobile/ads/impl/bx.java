package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.MobileAds;
import com.yandex.mobile.ads.impl.fg.a;
import org.json.JSONException;
import org.json.JSONObject;

public final class bx implements bv<fg> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bv<String> f5404a = new by();
    @NonNull
    private final ct b = new ct();

    @Nullable
    private static fg a(@NonNull String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            boolean optBoolean = jSONObject.optBoolean("custom_click_handling_enabled");
            boolean optBoolean2 = jSONObject.optBoolean("url_correction_enabled");
            String str2 = "visibility_error_indicator_enabled";
            Boolean valueOf = jSONObject.has(str2) ? Boolean.valueOf(jSONObject.optBoolean(str2)) : null;
            String optString = jSONObject.optString("mraid_controller", null);
            boolean optBoolean3 = jSONObject.optBoolean("sensitive_mode_disabled");
            Boolean valueOf2 = jSONObject.has("mediation_sensitive_mode_disabled") ? Boolean.valueOf(jSONObject.optBoolean("mediation_sensitive_mode_disabled")) : null;
            boolean optBoolean4 = jSONObject.optBoolean("custom_user_agent_enabled");
            boolean optBoolean5 = jSONObject.optBoolean("fused_location_provider_disabled");
            boolean optBoolean6 = jSONObject.optBoolean("lock_screen_enabled");
            long optLong = jSONObject.optLong("reload_timeout");
            return new a().a(System.currentTimeMillis() + (optLong > 0 ? optLong * 1000 : 86400000)).a(optString).a(optBoolean).a(valueOf).b(MobileAds.getLibraryVersion()).f(optBoolean6).b(valueOf2).c(optBoolean3).d(optBoolean4).e(optBoolean5).c(ff.a().e()).b(optBoolean2).a();
        } catch (JSONException unused) {
            return null;
        }
    }

    @Nullable
    public final /* synthetic */ Object b(@NonNull qq qqVar) {
        String str = (String) this.f5404a.b(qqVar);
        if (!TextUtils.isEmpty(str)) {
            return a(str);
        }
        return null;
    }
}
