package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public final class hq {

    private static final class a {

        /* renamed from: a reason: collision with root package name */
        static final int f5533a = Color.parseColor("#cc000000");
    }

    public static final class b {

        /* renamed from: a reason: collision with root package name */
        public static final GradientDrawable f5534a;
        public static final ColorDrawable b = new ColorDrawable(Color.parseColor("#ff000000"));
        static final GradientDrawable c;

        static {
            GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{Color.parseColor("#ff303030"), Color.parseColor("#ff181818"), Color.parseColor("#ff000000")});
            gradientDrawable.setCornerRadius(0.0f);
            f5534a = gradientDrawable;
            GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{Color.parseColor("#ff949494"), Color.parseColor("#ff5a5f62")});
            gradientDrawable2.setCornerRadius(0.0f);
            c = gradientDrawable2;
        }
    }

    public static final class c {
        @NonNull
        public static RelativeLayout a(@NonNull Context context) {
            RelativeLayout relativeLayout = new RelativeLayout(context);
            relativeLayout.setBackgroundColor(0);
            return relativeLayout;
        }

        @NonNull
        public static RelativeLayout b(@NonNull Context context) {
            RelativeLayout relativeLayout = new RelativeLayout(context);
            relativeLayout.setBackgroundDrawable(b.c);
            return relativeLayout;
        }
    }

    public static final class d {
        @NonNull
        public static LayoutParams a(@NonNull Context context, @Nullable al alVar) {
            LayoutParams layoutParams;
            if (alVar != null) {
                layoutParams = a(context, alVar.b(context), alVar.a(context));
            } else {
                layoutParams = new LayoutParams(-2, -2);
            }
            layoutParams.addRule(13);
            return layoutParams;
        }

        @NonNull
        public static LayoutParams a(@NonNull Context context, @Nullable x xVar) {
            LayoutParams a2 = xVar != null ? a(context, xVar.f(), xVar.g()) : new LayoutParams(-2, -2);
            a2.addRule(13);
            return a2;
        }

        public static LayoutParams a() {
            return new LayoutParams(-1, -1);
        }

        @NonNull
        public static LayoutParams b(@NonNull Context context, @NonNull x xVar) {
            LayoutParams a2 = a(context, xVar);
            int a3 = dv.a(context, 45.0f);
            a2.width = Math.min(a2.width + a3, dv.a(context));
            a2.height = Math.min(a2.height + a3, dv.b(context));
            return a2;
        }

        @NonNull
        public static LayoutParams a(@NonNull Context context, @NonNull View view) {
            int a2 = dv.a(context, 25.0f);
            int a3 = dv.a(context, 45.0f);
            int i = a3 >> 1;
            LayoutParams layoutParams = new LayoutParams(a3, a3);
            layoutParams.addRule(7, view.getId());
            layoutParams.addRule(6, view.getId());
            ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
            boolean z = true;
            boolean z2 = layoutParams2.width == -1 || layoutParams2.width + a2 >= dv.a(context);
            if (layoutParams2.height != -1 && layoutParams2.height + a2 < dv.b(context)) {
                z = false;
            }
            int i2 = (a2 >> 1) - ((a3 - a2) / 2);
            if (!z2 && !z) {
                i2 = -i;
            }
            layoutParams.setMargins(0, i2, i2, 0);
            return layoutParams;
        }

        @NonNull
        private static LayoutParams a(@NonNull Context context, int i, int i2) {
            return new LayoutParams(dv.a(context, (float) i), dv.a(context, (float) i2));
        }
    }

    static class e extends View {

        /* renamed from: a reason: collision with root package name */
        Paint f5535a;
        Paint b;
        Paint c;
        int d;
        int e;
        int f;
        float g;

        private e(Context context, Integer num) {
            super(context);
            this.d = a.f5533a;
            this.e = a.f5533a;
            this.f = -1;
            this.g = -1.0f;
            if (num != null) {
                this.e = num.intValue();
                a(num.intValue());
            }
            this.f5535a = new Paint(0);
            this.f5535a.setAntiAlias(true);
            this.f5535a.setStyle(Style.FILL);
            this.b = new Paint(0);
            this.b.setAntiAlias(true);
            this.b.setStrokeWidth(2.0f);
            this.b.setStyle(Style.STROKE);
            this.c = new Paint(0);
            this.c.setAntiAlias(true);
            this.c.setStyle(Style.STROKE);
            a();
        }

        public e(Context context) {
            this(context, Integer.valueOf(a.f5533a));
        }

        private void a(int i) {
            this.d = dv.a(i, 20.0f);
            if (dv.a(this.d)) {
                this.f = -1;
            } else {
                this.f = ViewCompat.MEASURED_STATE_MASK;
            }
        }

        public final void setBackgroundColor(int i) {
            this.e = i;
            a(this.e);
            a();
            invalidate();
        }

        private void a() {
            this.f5535a.setColor(this.d);
            this.b.setColor(this.f);
            this.c.setColor(this.f);
        }

        /* access modifiers changed from: protected */
        public final void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawColor(0);
            float min = (float) Math.min(getWidth(), getHeight());
            float f2 = min / 2.0f;
            canvas.drawCircle(f2, f2, f2, this.f5535a);
            float f3 = min / 5.0f;
            float f4 = f2 - f3;
            float f5 = f3 + f2;
            Canvas canvas2 = canvas;
            float f6 = f4;
            float f7 = f5;
            canvas2.drawLine(f6, f4, f7, f5, this.b);
            canvas2.drawLine(f6, f5, f7, f4, this.b);
            if (this.g > 0.0f) {
                this.c.setStrokeWidth(this.g);
                canvas.drawCircle(f2, f2, f2 - this.g, this.c);
            }
        }

        private void b(int i) {
            a(i);
            a();
            invalidate();
        }

        @SuppressLint({"ClickableViewAccessibility"})
        public final boolean onTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() == 0) {
                b(com.yandex.mobile.ads.impl.w.a.f5816a);
            } else if (1 == motionEvent.getAction()) {
                b(this.e);
            }
            return super.onTouchEvent(motionEvent);
        }
    }

    public static class f extends RelativeLayout {

        /* renamed from: a reason: collision with root package name */
        private boolean f5536a = true;
        /* access modifiers changed from: private */
        public boolean b = false;
        private long c = 500;
        private RelativeLayout d;
        private RelativeLayout e;
        private Interpolator f;
        private Interpolator g;
        private float h;
        private float i;
        private float j;

        private final class a implements AnimationListener {
            public final void onAnimationRepeat(Animation animation) {
            }

            public final void onAnimationStart(Animation animation) {
            }

            private a() {
            }

            /* synthetic */ a(f fVar, byte b) {
                this();
            }

            public final void onAnimationEnd(Animation animation) {
                f.this.b = false;
            }
        }

        private final class b implements AnimationListener {
            private boolean b;

            public final void onAnimationRepeat(Animation animation) {
            }

            public final void onAnimationStart(Animation animation) {
            }

            public b(boolean z) {
                this.b = z;
            }

            public final void onAnimationEnd(Animation animation) {
                f.a(f.this, this.b);
            }
        }

        public f(Context context) {
            super(context);
            setBackgroundColor(0);
        }

        public final void setPivot(float f2, float f3) {
            this.h = f2;
            this.i = f3;
        }

        public final void setDepthZ(float f2) {
            this.j = f2;
        }

        public final void setFrontFace(RelativeLayout relativeLayout, LayoutParams layoutParams) {
            if (this.d != null) {
                removeView(this.d);
            }
            this.d = relativeLayout;
            if (this.d != null) {
                addView(this.d, layoutParams);
                this.d.setVisibility(this.f5536a ? 0 : 4);
            }
        }

        public final void setBackFace(RelativeLayout relativeLayout, LayoutParams layoutParams) {
            if (this.e != null) {
                removeView(this.e);
            }
            this.e = relativeLayout;
            if (this.e != null) {
                addView(this.e, layoutParams);
                this.e.setVisibility(this.f5536a ? 4 : 0);
            }
        }

        public final void setAnimationDuration(long j2) {
            this.c = j2 / 2;
        }

        public final void setFromInterpolator(Interpolator interpolator) {
            this.f = interpolator;
        }

        public final void setToInterpolator(Interpolator interpolator) {
            this.g = interpolator;
        }

        public final void a() {
            float f2 = (float) ((this.f5536a ? 1 : -1) * 90);
            this.b = true;
            b bVar = new b(0.0f, f2, this.h, this.i, this.j, 0.0f);
            bVar.setDuration(this.c);
            bVar.setFillAfter(true);
            bVar.setInterpolator(this.f);
            bVar.setAnimationListener(new b(this.f5536a));
            if (this.f5536a) {
                this.d.startAnimation(bVar);
            } else {
                this.e.startAnimation(bVar);
            }
            this.f5536a = !this.f5536a;
        }

        static /* synthetic */ void a(f fVar, boolean z) {
            b bVar;
            if (z) {
                fVar.d.setVisibility(4);
                fVar.e.setVisibility(0);
                fVar.e.requestFocus();
                bVar = new b(-90.0f, 0.0f, fVar.h, fVar.i, fVar.j, 1.0f);
            } else {
                fVar.e.setVisibility(4);
                fVar.d.setVisibility(0);
                fVar.d.requestFocus();
                bVar = new b(90.0f, 0.0f, fVar.h, fVar.i, fVar.j, 1.0f);
            }
            bVar.setDuration(fVar.c);
            bVar.setFillAfter(true);
            bVar.setInterpolator(fVar.g);
            bVar.setAnimationListener(new a(fVar, 0));
            if (z) {
                fVar.e.startAnimation(bVar);
            } else {
                fVar.d.startAnimation(bVar);
            }
        }
    }

    public static final class g {
        @NonNull
        public static View a(@NonNull Context context) {
            e eVar = new e(context);
            FrameLayout frameLayout = new FrameLayout(context);
            int a2 = dv.a(context, 25.0f);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2);
            layoutParams.gravity = 17;
            int a3 = dv.a(context, 10.0f);
            layoutParams.setMargins(a3, a3, a3, a3);
            frameLayout.addView(eVar, layoutParams);
            new du();
            eVar.setTag(du.a("close_button"));
            eVar.g = 1.0f;
            eVar.invalidate();
            return frameLayout;
        }
    }
}
