package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.dh.a;

public final class df implements dh {
    @Nullable
    public final String a(@NonNull fc fcVar) {
        return ad.a(fcVar);
    }

    @NonNull
    public final String a(@NonNull Context context, @NonNull fc fcVar) {
        return ad.a(context, fcVar).c();
    }

    @NonNull
    public final int a() {
        return a.f5438a;
    }
}
