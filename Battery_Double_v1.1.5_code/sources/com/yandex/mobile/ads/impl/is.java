package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.hq.g;
import java.io.Serializable;
import java.util.Map;

public final class is implements ec, ef, ii, ij, l {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final RelativeLayout f5556a;
    @NonNull
    private final db b;
    @NonNull
    private final o c;
    @NonNull
    private final Context d;
    @NonNull
    private final Window e;
    @NonNull
    private final x<String> f;
    @NonNull
    private final String g;
    @NonNull
    private final cv h;
    @NonNull
    private final bd i;
    @NonNull
    private final hs j;
    @NonNull
    private final jg k;
    @NonNull
    private final hx l;

    public final void b(boolean z) {
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
    }

    public final void onAdLoaded() {
    }

    is(@NonNull Context context, @NonNull RelativeLayout relativeLayout, @NonNull db dbVar, @NonNull o oVar, @NonNull Window window, @NonNull iw iwVar) {
        this.d = context;
        this.f5556a = relativeLayout;
        this.b = dbVar;
        this.c = oVar;
        this.e = window;
        this.f = iwVar.a();
        this.g = iwVar.c();
        fc b2 = iwVar.b();
        this.h = new cv(context, b2);
        this.i = new bd(b2);
        this.j = new ix(this.d, this.f, b2);
        new hz();
        boolean a2 = hz.a(this.g);
        ic.a();
        this.l = ic.a(a2).a(this.j, this, this, this, this);
        boolean w = this.f.w();
        final hx hxVar = this.l;
        View a3 = g.a(this.d);
        a3.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                hxVar.a();
                is.this.h();
            }
        });
        new jh(new ja());
        this.k = jh.a(this.f, a3, a2, w);
    }

    public final void a() {
        this.e.requestFeature(1);
        this.e.addFlags(1024);
        if (hc.a(11)) {
            this.e.addFlags(16777216);
        }
        this.k.a(this.d, this.c, this.f.e());
    }

    public final void c_() {
        this.l.a(this.g);
        this.k.a(this.f5556a);
        this.j.setId(2);
        this.f5556a.addView(this.k.a(this.j, this.f));
        this.c.a(5, null);
    }

    public final boolean c() {
        return !this.k.c();
    }

    public final void d() {
        this.c.a(4, null);
    }

    public final void e() {
        this.j.f();
        this.c.a(2, null);
    }

    public final void f() {
        this.j.e();
        this.c.a(3, null);
    }

    public final void g() {
        this.j.g();
        this.l.b();
        this.k.b();
    }

    public final void h() {
        this.c.a();
    }

    public final void a(boolean z) {
        this.k.a(z);
    }

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        this.k.a();
        Bundle bundle = new Bundle();
        bundle.putSerializable("extra_tracking_parameters", (Serializable) map);
        this.c.a(0, bundle);
    }

    public final void a(@NonNull String str) {
        this.h.a(str, this.f, this.b);
    }

    public final void i() {
        this.i.c(this.d, this.f);
        this.c.a(13, null);
    }

    public final void b() {
        this.c.a(14, null);
    }

    public final void d_() {
        this.c.a(15, null);
    }
}
