package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

public interface qb<T extends Drawable> {
    boolean a(@NonNull T t, @NonNull Bitmap bitmap);
}
