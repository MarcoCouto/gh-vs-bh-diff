package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.InterstitialEventListener;

public final class iu implements hl {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Object f5558a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    @NonNull
    private final ck c;
    /* access modifiers changed from: private */
    @Nullable
    public InterstitialEventListener d;

    public iu(@NonNull Context context) {
        this.c = new ck(context);
    }

    public final void a(@NonNull fc fcVar) {
        this.c.a(fcVar);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final InterstitialEventListener h() {
        InterstitialEventListener interstitialEventListener;
        synchronized (f5558a) {
            interstitialEventListener = this.d;
        }
        return interstitialEventListener;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable InterstitialEventListener interstitialEventListener) {
        synchronized (f5558a) {
            this.d = interstitialEventListener;
        }
    }

    public final void b() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (iu.f5558a) {
                    if (iu.this.d != null) {
                        iu.this.d.onInterstitialDismissed();
                    }
                }
            }
        });
    }

    public final void c() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (iu.f5558a) {
                    if (iu.this.d != null) {
                        he.a((Object) iu.this.d, "onAdImpressionTracked", new Object[0]);
                    }
                }
            }
        });
    }

    public final void a(@NonNull final AdRequestError adRequestError) {
        this.c.a(adRequestError);
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (iu.f5558a) {
                    if (iu.this.d != null) {
                        iu.this.d.onInterstitialFailedToLoad(adRequestError);
                    }
                }
            }
        });
    }

    public final void e() {
        this.c.a();
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (iu.f5558a) {
                    if (iu.this.d != null) {
                        iu.this.d.onInterstitialLoaded();
                    }
                }
            }
        });
    }

    public final void g() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (iu.f5558a) {
                    if (iu.this.d != null) {
                        iu.this.d.onInterstitialShown();
                    }
                }
            }
        });
    }

    public final void a() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (iu.f5558a) {
                    if (iu.this.d != null) {
                        iu.this.d.onAdClosed();
                    }
                }
            }
        });
    }

    public final void d() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (iu.f5558a) {
                    if (iu.this.d != null) {
                        iu.this.d.onAdLeftApplication();
                    }
                }
            }
        });
    }

    public final void f() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (iu.f5558a) {
                    if (iu.this.d != null) {
                        iu.this.d.onAdOpened();
                    }
                }
            }
        });
    }
}
