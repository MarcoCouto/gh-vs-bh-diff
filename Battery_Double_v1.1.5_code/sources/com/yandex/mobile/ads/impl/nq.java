package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class nq {

    /* renamed from: a reason: collision with root package name */
    private String f5655a;
    private List<ni> b;
    private List<np> c;
    private nr d;
    private List<bg> e;
    @Nullable
    private List<String> f;
    private Map<String, Object> g = new HashMap();

    public final void a(List<ni> list) {
        this.b = list;
    }

    public final void b(List<np> list) {
        this.c = list;
    }

    public final void a(nr nrVar) {
        this.d = nrVar;
    }

    public final void c(List<bg> list) {
        this.e = list;
    }

    public final void d(@NonNull List<String> list) {
        this.f = list;
    }

    public final void a(String str) {
        this.f5655a = str;
    }

    public final void a(String str, Object obj) {
        this.g.put(str, obj);
    }

    @NonNull
    public final Map<String, Object> a() {
        return this.g;
    }

    public final List<ni> b() {
        return this.b;
    }

    public final List<np> c() {
        return this.c;
    }

    public final nr d() {
        return this.d;
    }

    @Nullable
    public final List<bg> e() {
        return this.e;
    }

    @Nullable
    public final List<String> f() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nq nqVar = (nq) obj;
        if (this.f5655a == null ? nqVar.f5655a != null : !this.f5655a.equals(nqVar.f5655a)) {
            return false;
        }
        if (this.b == null ? nqVar.b != null : !this.b.equals(nqVar.b)) {
            return false;
        }
        if (this.c == null ? nqVar.c != null : !this.c.equals(nqVar.c)) {
            return false;
        }
        if (this.d == null ? nqVar.d != null : !this.d.equals(nqVar.d)) {
            return false;
        }
        if (this.e == null ? nqVar.e != null : !this.e.equals(nqVar.e)) {
            return false;
        }
        if (this.f == null ? nqVar.f != null : !this.f.equals(nqVar.f)) {
            return false;
        }
        if (this.g != null) {
            return this.g.equals(nqVar.g);
        }
        return nqVar.g == null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((((((this.f5655a != null ? this.f5655a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31;
        if (this.g != null) {
            i = this.g.hashCode();
        }
        return hashCode + i;
    }
}
