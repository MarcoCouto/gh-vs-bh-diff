package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.hu.a;

public abstract class eg extends ab implements b, eb, a {

    /* renamed from: a reason: collision with root package name */
    private static boolean f5456a = false;
    @NonNull
    private final cg b;
    @Nullable
    protected ec e;
    @NonNull
    private final hu f = new hu();
    @NonNull
    private final iy g = new iy();
    @NonNull
    private final ag h = ag.a();
    @NonNull
    private final hb i = new hb();
    private boolean j;
    private boolean k;

    /* access modifiers changed from: protected */
    public abstract void a();

    public eg(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar) {
        super(context.getApplicationContext());
        this.b = new ci(context, xVar, fcVar);
        a_(context);
        if (!f5456a) {
            Context context2 = getContext();
            if (VERSION.SDK_INT == 19) {
                WebView webView = new WebView(context2.getApplicationContext());
                webView.setBackgroundColor(0);
                webView.loadDataWithBaseURL(null, "", WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
                LayoutParams layoutParams = new LayoutParams();
                layoutParams.width = 1;
                layoutParams.height = 1;
                layoutParams.type = IronSourceConstants.IS_INSTANCE_OPENED;
                layoutParams.flags = 16777240;
                layoutParams.format = -2;
                layoutParams.gravity = 8388659;
                try {
                    ((WindowManager) context2.getSystemService("window")).addView(webView, layoutParams);
                } catch (Exception unused) {
                }
            }
            f5456a = true;
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void a_(Context context) {
        super.a_(context);
        setBackgroundColor(0);
        setVisibility(4);
        setHorizontalScrollBarEnabled(false);
        setHorizontalScrollbarOverlay(false);
        setVerticalScrollBarEnabled(false);
        setVerticalScrollbarOverlay(false);
        setScrollBarStyle(0);
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        settings.setMinimumFontSize(1);
        settings.setMinimumLogicalFontSize(1);
        if (hc.a(21)) {
            settings.setMixedContentMode(0);
        }
        if (VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        WebSettings settings2 = getSettings();
        fg a2 = ff.a().a(context);
        if (a2 != null && a2.g()) {
            settings2.setUserAgentString(hb.a(context));
        }
        setWebViewClient(new ea(this));
        setWebChromeClient(new dz(this.b));
    }

    public void d() {
        this.g.a(new Runnable() {
            public final void run() {
                eg.this.a();
            }
        });
    }

    public final void a(@NonNull Context context, @NonNull String str) {
        if (this.e != null) {
            this.e.a(str);
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i2) {
        super.onVisibilityChanged(view, i2);
        a(hu.a(this));
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        a(hu.a(this));
    }

    /* access modifiers changed from: protected */
    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.b());
        sb.append("<style type='text/css'> \n  * { \n      -webkit-tap-highlight-color: rgba(0, 0, 0, 0) !important; \n      -webkit-focus-ring-color: rgba(0, 0, 0, 0) !important; \n      outline: none !important; \n    } \n</style> \n");
        sb.append(eh.d);
        return sb.toString();
    }

    public void setHtmlWebViewListener(@NonNull ec ecVar) {
        this.e = ecVar;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.k = true;
        this.h.a(this, getContext());
        a(hu.a(this));
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.k = false;
        a(hu.a(this));
        this.h.b(this, getContext());
        super.onDetachedFromWindow();
    }

    public final boolean h() {
        return this.k;
    }

    public final void a(@NonNull Intent intent) {
        a(!"android.intent.action.SCREEN_OFF".equals(intent.getAction()) && hu.a(this) && this.h.a(getContext()));
    }

    private void a(boolean z) {
        if (this.j != z) {
            this.j = z;
            if (this.e != null) {
                this.e.b(this.j);
            }
        }
    }

    public void g() {
        this.e = null;
        super.g();
    }
}
