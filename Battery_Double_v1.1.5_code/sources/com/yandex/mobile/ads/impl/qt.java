package com.yandex.mobile.ads.impl;

public final class qt<T> {

    /* renamed from: a reason: collision with root package name */
    public final T f5724a;
    public final com.yandex.mobile.ads.impl.qk.a b;
    public final re c;
    public boolean d;

    public interface a {
        void a(re reVar);
    }

    public interface b<T> {
        void a(T t);
    }

    public static <T> qt<T> a(T t, com.yandex.mobile.ads.impl.qk.a aVar) {
        return new qt<>(t, aVar);
    }

    public static <T> qt<T> a(re reVar) {
        return new qt<>(reVar);
    }

    private qt(T t, com.yandex.mobile.ads.impl.qk.a aVar) {
        this.d = false;
        this.f5724a = t;
        this.b = aVar;
        this.c = null;
    }

    private qt(re reVar) {
        this.d = false;
        this.f5724a = null;
        this.b = null;
        this.c = reVar;
    }
}
