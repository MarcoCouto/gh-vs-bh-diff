package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.tapjoy.TapjoyConstants;
import com.yandex.mobile.ads.AdRequest;
import java.util.HashMap;
import java.util.Map;

public final class cn {
    @NonNull
    public static Map<String, Object> a(@Nullable AdRequest adRequest) {
        dr drVar = new dr(new HashMap());
        if (adRequest != null) {
            Map parameters = adRequest.getParameters();
            if (parameters != null) {
                String str = (String) parameters.get("adapter_network_name");
                String str2 = (String) parameters.get(TapjoyConstants.TJC_ADAPTER_VERSION);
                String str3 = (String) parameters.get("adapter_network_sdk_version");
                drVar.b("adapter_network_name", str);
                drVar.b(TapjoyConstants.TJC_ADAPTER_VERSION, str2);
                drVar.b("adapter_network_sdk_version", str3);
            }
        }
        return drVar.a();
    }
}
