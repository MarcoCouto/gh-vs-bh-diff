package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.HashSet;
import java.util.Set;

public final class pk {
    @NonNull
    public static Set<nl> a(@NonNull np npVar) {
        HashSet hashSet = new HashSet();
        for (ni c : npVar.c()) {
            Object c2 = c.c();
            if (c2 instanceof nl) {
                hashSet.add((nl) c2);
            }
            if (c2 instanceof no) {
                nl b = ((no) c2).b();
                if (b != null) {
                    hashSet.add(b);
                }
            }
        }
        return hashSet;
    }
}
