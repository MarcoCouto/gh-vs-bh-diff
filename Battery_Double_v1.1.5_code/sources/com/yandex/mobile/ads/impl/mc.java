package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration;
import com.yandex.mobile.ads.nativeads.bh;
import com.yandex.mobile.ads.nativeads.d;
import com.yandex.mobile.ads.nativeads.h;
import com.yandex.mobile.ads.nativeads.o;
import com.yandex.mobile.ads.nativeads.s;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class mc {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Executor f5618a = Executors.newSingleThreadExecutor(new Cdo("YandexMobileAds.BaseController"));
    /* access modifiers changed from: private */
    @NonNull
    public final fc b;
    /* access modifiers changed from: private */
    @NonNull
    public final md c;
    /* access modifiers changed from: private */
    @NonNull
    public final mg d;
    /* access modifiers changed from: private */
    @NonNull
    public final NativeAdLoaderConfiguration e;

    @VisibleForTesting
    class a implements Runnable {
        @NonNull
        private final s b;
        @NonNull
        private final WeakReference<Context> c;
        @NonNull
        private final x<nq> d;
        @NonNull
        private final mb e;

        a(Context context, @NonNull x<nq> xVar, @NonNull s sVar, @NonNull mb mbVar) {
            this.d = xVar;
            this.b = sVar;
            this.c = new WeakReference<>(context);
            this.e = mbVar;
        }

        public final void run() {
            Context context = (Context) this.c.get();
            if (context != null) {
                try {
                    nq nqVar = (nq) this.d.t();
                    if (nqVar == null) {
                        this.e.a(v.e);
                        return;
                    }
                    if (hc.a(nqVar.c())) {
                        this.e.a(v.j);
                        return;
                    }
                    o oVar = new o(nqVar, this.d, mc.this.b);
                    mb mbVar = this.e;
                    if (mc.this.e.shouldLoadImagesAutomatically()) {
                        mc.this.d.a(context, oVar, new bh(), this.b, mbVar);
                        return;
                    }
                    mc.this.c.a(context, oVar, new d(context), this.b, mbVar);
                } catch (Exception unused) {
                    this.e.a(v.e);
                }
            }
        }
    }

    public mc(@NonNull Context context, @NonNull fc fcVar, @NonNull NativeAdLoaderConfiguration nativeAdLoaderConfiguration) {
        this.b = fcVar;
        this.e = nativeAdLoaderConfiguration;
        this.c = new md(fcVar);
        this.d = new mg(this.c, new h(context));
    }

    public final void a(@NonNull Context context, @NonNull x<nq> xVar, @NonNull s sVar, @NonNull mb mbVar) {
        Executor executor = this.f5618a;
        a aVar = new a(context, xVar, sVar, mbVar);
        executor.execute(aVar);
    }
}
