package com.yandex.mobile.ads.impl;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.mobile.ads.impl.qk.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Map;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;

public final class ri {
    public static a a(qq qqVar) {
        boolean z;
        long j;
        long j2;
        long j3;
        long j4;
        qq qqVar2 = qqVar;
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> map = qqVar2.c;
        String str = (String) map.get(HttpRequest.HEADER_DATE);
        long a2 = str != null ? a(str) : 0;
        String str2 = (String) map.get(HttpRequest.HEADER_CACHE_CONTROL);
        int i = 0;
        if (str2 != null) {
            String[] split = str2.split(",");
            j2 = 0;
            int i2 = 0;
            j = 0;
            while (i < split.length) {
                String trim = split[i].trim();
                if (trim.equals("no-cache") || trim.equals("no-store")) {
                    return null;
                }
                if (trim.startsWith("max-age=")) {
                    try {
                        j2 = Long.parseLong(trim.substring(8));
                    } catch (Exception unused) {
                    }
                } else if (trim.startsWith("stale-while-revalidate=")) {
                    j = Long.parseLong(trim.substring(23));
                } else if (trim.equals("must-revalidate") || trim.equals("proxy-revalidate")) {
                    i2 = 1;
                }
                i++;
            }
            i = i2;
            z = true;
        } else {
            j2 = 0;
            j = 0;
            z = false;
        }
        String str3 = (String) map.get(HttpRequest.HEADER_EXPIRES);
        long a3 = str3 != null ? a(str3) : 0;
        String str4 = (String) map.get(HttpRequest.HEADER_LAST_MODIFIED);
        long a4 = str4 != null ? a(str4) : 0;
        String str5 = (String) map.get(HttpRequest.HEADER_ETAG);
        if (z) {
            j4 = currentTimeMillis + (j2 * 1000);
            if (i == 0) {
                j3 = (j * 1000) + j4;
                a aVar = new a();
                aVar.f5712a = qqVar2.b;
                aVar.b = str5;
                aVar.f = j4;
                aVar.e = j3;
                aVar.c = a2;
                aVar.d = a4;
                aVar.g = map;
                return aVar;
            }
        } else if (a2 <= 0 || a3 < a2) {
            j4 = 0;
        } else {
            j3 = (a3 - a2) + currentTimeMillis;
            j4 = j3;
            a aVar2 = new a();
            aVar2.f5712a = qqVar2.b;
            aVar2.b = str5;
            aVar2.f = j4;
            aVar2.e = j3;
            aVar2.c = a2;
            aVar2.d = a4;
            aVar2.g = map;
            return aVar2;
        }
        j3 = j4;
        a aVar22 = new a();
        aVar22.f5712a = qqVar2.b;
        aVar22.b = str5;
        aVar22.f = j4;
        aVar22.e = j3;
        aVar22.c = a2;
        aVar22.d = a4;
        aVar22.g = map;
        return aVar22;
    }

    private static long a(String str) {
        try {
            return DateUtils.parseDate(str).getTime();
        } catch (DateParseException unused) {
            return 0;
        }
    }

    public static String a(Map<String, String> map) {
        String str = "ISO-8859-1";
        String str2 = (String) map.get("Content-Type");
        if (str2 != null) {
            String[] split = str2.split(";");
            for (int i = 1; i < split.length; i++) {
                String[] split2 = split[i].trim().split(RequestParameters.EQUAL);
                if (split2.length == 2 && split2[0].equals(HttpRequest.PARAM_CHARSET)) {
                    return split2[1];
                }
            }
        }
        return str;
    }
}
