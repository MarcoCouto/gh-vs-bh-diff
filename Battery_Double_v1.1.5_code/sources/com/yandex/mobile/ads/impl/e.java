package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.yandex.mobile.ads.AdEventListener;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdSize;
import com.yandex.mobile.ads.VideoController;
import com.yandex.mobile.ads.a;

public abstract class e extends RelativeLayout {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f5452a;
    @NonNull
    private final f b;
    @Nullable
    private AdSize c;

    /* access modifiers changed from: protected */
    @NonNull
    public abstract a a(@NonNull Context context, @NonNull c cVar);

    public void pause() {
    }

    public void resume() {
    }

    public e(@NonNull Context context) {
        this(context, null);
    }

    public e(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public e(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        f fVar;
        super(context, attributeSet, i);
        c cVar = new c(context);
        this.f5452a = a(context, cVar);
        cVar.a(this.f5452a.s());
        a aVar = this.f5452a;
        if (isInEditMode()) {
            fVar = new b();
        } else {
            fVar = new a(aVar);
        }
        this.b = fVar;
        this.b.a(context, this);
    }

    public void loadAd(AdRequest adRequest) {
        this.f5452a.a(adRequest);
    }

    public void setAdEventListener(AdEventListener adEventListener) {
        this.f5452a.a(adEventListener);
    }

    @NonNull
    public VideoController getVideoController() {
        return this.f5452a.g();
    }

    public AdEventListener getAdEventListener() {
        return this.f5452a.h();
    }

    public void setAutoRefreshEnabled(boolean z) {
        this.f5452a.a(z);
    }

    public void setAdSize(@Nullable AdSize adSize) {
        this.c = adSize;
        this.f5452a.b(a.a(adSize));
    }

    @Nullable
    public AdSize getAdSize() {
        return this.c;
    }

    public void setBlockId(String str) {
        this.f5452a.a_(str);
    }

    public String getBlockId() {
        return this.f5452a.r();
    }

    public void destroy() {
        if (!di.a((ac) this.f5452a)) {
            this.f5452a.e();
        }
    }

    public void shouldOpenLinksInApp(boolean z) {
        this.f5452a.a_(z);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        new StringBuilder("onAttachedToWindow(), clazz = ").append(getClass());
        f fVar = this.b;
        getContext();
        fVar.a();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        new StringBuilder("onDetachedFromWindow(), clazz = ").append(getClass());
        f fVar = this.b;
        getContext();
        fVar.b();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        StringBuilder sb = new StringBuilder("onWindowVisibilityChanged(), windowVisibility = ");
        sb.append(i);
        sb.append(", this.getVisibility = ");
        sb.append(getVisibility());
        a((i == 0 && getVisibility() == 0) ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(view, i);
        StringBuilder sb = new StringBuilder("onVisibilityChanged(), changedView = ");
        sb.append(view);
        sb.append(", viewVisibility = ");
        sb.append(i);
        if (this == view) {
            a(i);
        }
    }

    private void a(int i) {
        if (!di.a((ac) this.f5452a)) {
            this.b.a(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (!di.a((ac) this.f5452a)) {
            setVisibility(this.f5452a.c() ? 0 : 8);
        }
        new Object[1][0] = configuration.toString();
    }
}
