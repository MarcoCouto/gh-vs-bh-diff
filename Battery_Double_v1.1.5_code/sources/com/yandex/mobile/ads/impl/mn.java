package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.widget.FrameLayout;
import com.yandex.mobile.ads.impl.am.a;
import java.util.ArrayList;
import java.util.List;

public final class mn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private static final List<a> f5627a = new ArrayList<a>() {
        {
            add(a.SUCCESS);
            add(a.APPLICATION_INACTIVE);
            add(a.NOT_ADDED_TO_HIERARCHY);
        }
    };
    @NonNull
    private final mo b = new mo();

    public final void a(@NonNull am amVar, @NonNull FrameLayout frameLayout) {
        this.b.a(amVar, frameLayout, !f5627a.contains(amVar.b()));
    }
}
