package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.mobile.ads.AdEventListener;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.ru.a;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public abstract class aa<T> implements AdEventListener, ac, b, a<x<T>>, y.a {
    /* access modifiers changed from: protected */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Handler f5352a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: protected */
    @NonNull
    public final Context b;
    @NonNull
    protected final t c = new t(this);
    @NonNull
    protected final y d = new y(this.f5352a);
    @NonNull
    protected final fc e;
    @Nullable
    protected x<T> f;
    @NonNull
    private final fw g;
    @NonNull
    private final Executor h;
    /* access modifiers changed from: private */
    @NonNull
    public final ai i;
    @NonNull
    private final ag j;
    @NonNull
    private final dh k;
    @NonNull
    private final fs l;
    @NonNull
    private final cv m;
    /* access modifiers changed from: private */
    @NonNull
    public final fe n;
    @NonNull
    private u o;
    private boolean p;
    private long q;
    @Nullable
    private AdEventListener r;
    @NonNull
    private fr s;

    /* access modifiers changed from: protected */
    @NonNull
    public abstract bp<T> a(String str, String str2);

    protected aa(@NonNull Context context, @NonNull com.yandex.mobile.ads.b bVar) {
        this.b = context;
        this.d.a(this);
        this.o = u.NOT_STARTED;
        this.j = ag.a();
        this.k = new df();
        this.e = new fc(bVar);
        this.h = Executors.newSingleThreadExecutor(new Cdo("YandexMobileAds.BaseController"));
        this.i = new ai(context, this.e);
        this.l = new fs(this.e);
        this.s = fp.a(context);
        this.g = new fw();
        this.m = new cv(context, this.e);
        this.n = new fe();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
        return;
     */
    public synchronized void a(@Nullable AdRequest adRequest) {
        if (b()) {
            if (d(adRequest)) {
                a(u.NOT_STARTED);
                b(adRequest);
                return;
            }
            i();
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void b(@Nullable AdRequest adRequest) {
        a(adRequest, this.k);
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(@Nullable final AdRequest adRequest, @NonNull final dh dhVar) {
        this.f5352a.post(new Runnable() {
            public final void run() {
                aa.this.c(adRequest);
                if (aa.this.n()) {
                    aa.this.a(u.LOADING);
                    aa.this.l.a(aa.this.s, (fs.a) new fs.a(dhVar) {
                        public final void a() {
                            aa.this.g.a(aa.this.b, new ga(r4) {
                                public final void a(@Nullable fu fuVar) {
                                    if (fuVar != null) {
                                        aa.this.e.a(fuVar);
                                    }
                                    aa.this.h.execute(new Runnable(r4) {
                                        public final void run() {
                                            ai b2 = aa.this.i;
                                            Context context = aa.this.b;
                                            aa.this.n;
                                            b2.a(context, new ai.a() {
                                                public final void a() {
                                                    aa.this.a(r3);
                                                }

                                                public final void a(@NonNull re reVar) {
                                                    aa.this.b(reVar);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }

                        public final void b() {
                            aa.this.onAdFailedToLoad(v.d);
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void a(@NonNull final dh dhVar) {
        this.h.execute(new Runnable() {
            public final void run() {
                if (!aa.this.a_()) {
                    String a2 = dhVar.a(aa.this.e);
                    if (!TextUtils.isEmpty(a2)) {
                        aa.this.e.a(dhVar.a());
                        dh dhVar = dhVar;
                        Context context = aa.this.b;
                        fc fcVar = aa.this.e;
                        aa.this.n;
                        bp a3 = aa.this.a(a2, dhVar.a(context, fcVar));
                        a3.a((Object) di.a(this));
                        aa.this.c.a(a3);
                        return;
                    }
                    aa.this.onAdFailedToLoad(v.n);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final synchronized void c(AdRequest adRequest) {
        this.e.a(adRequest);
    }

    /* access modifiers changed from: protected */
    public final synchronized AdRequest j() {
        return this.e.c();
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(@NonNull u uVar) {
        new StringBuilder("assignLoadingState, state = ").append(uVar);
        this.o = uVar;
    }

    private synchronized boolean a() {
        return this.o == u.ERRONEOUSLY_LOADED;
    }

    @VisibleForTesting
    private synchronized boolean b() {
        new StringBuilder("isLoading, state = ").append(this.o);
        return this.o != u.LOADING;
    }

    public final synchronized boolean k() {
        return this.o == u.SUCCESSFULLY_LOADED;
    }

    public final synchronized boolean l() {
        return this.o == u.CANCELLED;
    }

    @NonNull
    public final Context m() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public boolean n() {
        boolean z;
        if (o()) {
            if (this.e.b() == null) {
                onAdFailedToLoad(v.p);
                z = false;
            } else {
                z = true;
            }
            return z && q();
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public final boolean o() {
        return z() && p() && A() && g();
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public final boolean p() {
        try {
            dl.a().a(this.b);
            return true;
        } catch (dl.a e2) {
            onAdFailedToLoad(v.a(1, e2.getMessage()));
            return false;
        }
    }

    /* access modifiers changed from: protected */
    @MainThread
    public final boolean q() {
        if (di.b(this.b)) {
            return true;
        }
        onAdFailedToLoad(v.b);
        return false;
    }

    private boolean g() {
        if (this.e.c() != null) {
            return true;
        }
        onAdFailedToLoad(v.n);
        return false;
    }

    private boolean A() {
        if (this.e.m()) {
            return true;
        }
        onAdFailedToLoad(v.o);
        return false;
    }

    public final void a_(String str) {
        this.e.b(str);
    }

    public final String r() {
        return this.e.e();
    }

    public void a(@Nullable AdEventListener adEventListener) {
        this.r = adEventListener;
    }

    @Nullable
    public AdEventListener h() {
        return this.r;
    }

    public final void b(@Nullable al alVar) {
        this.e.a(alVar);
    }

    @NonNull
    public final fc s() {
        return this.e;
    }

    public void onAdFailedToLoad(@NonNull final AdRequestError adRequestError) {
        hi.b(adRequestError.getDescription(), new Object[0]);
        a(u.ERRONEOUSLY_LOADED);
        this.f5352a.post(new Runnable() {
            public final void run() {
                aa.this.a(adRequestError);
            }
        });
    }

    public void onAdLoaded() {
        t();
        i();
    }

    public final void t() {
        a(u.SUCCESSFULLY_LOADED);
        this.q = SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: protected */
    public synchronized void i() {
        if (this.r != null) {
            this.r.onAdLoaded();
        }
    }

    public synchronized void onAdClosed() {
        if (this.r != null) {
            this.r.onAdClosed();
        }
    }

    public synchronized void onAdOpened() {
        if (this.r != null) {
            this.r.onAdOpened();
        }
    }

    public synchronized void onAdLeftApplication() {
        if (this.r != null) {
            this.r.onAdLeftApplication();
        }
    }

    public synchronized void d() {
        if (!a_()) {
            this.p = true;
            v();
            this.l.a(this.s);
            w();
            this.d.a(null);
            this.c.b();
            this.f = null;
            new Object[1][0] = getClass().toString();
        }
    }

    public final synchronized boolean a_() {
        return this.p;
    }

    public void a(int i2, @Nullable Bundle bundle) {
        switch (i2) {
            case 6:
                onAdOpened();
                return;
            case 7:
                onAdLeftApplication();
                break;
            case 8:
                onAdClosed();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void b(String str) {
        if (this.f != null) {
            this.m.a(str, this.f, new da(this.b, this.e.j(), this.d));
        }
    }

    public void e() {
        d();
        new StringBuilder("onDestroy(), clazz = ").append(getClass());
    }

    /* access modifiers changed from: protected */
    public synchronized boolean d(AdRequest adRequest) {
        if (this.f == null || this.q <= 0 || SystemClock.elapsedRealtime() - this.q > ((long) this.f.p()) || ((adRequest != null && !adRequest.equals(this.e.c())) || a())) {
            return true;
        }
        return false;
    }

    public void a(@NonNull Intent intent) {
        new StringBuilder("action = ").append(intent.getAction());
    }

    public final void u() {
        new StringBuilder("registerPhoneStateTracker(), clazz = ").append(getClass());
        this.j.a(this, this.b);
    }

    public final void v() {
        new StringBuilder("unregisterPhoneStateTracker(), clazz = ").append(getClass());
        this.j.b(this, this.b);
    }

    /* access modifiers changed from: protected */
    public final void w() {
        this.g.a(this.b);
    }

    /* access modifiers changed from: protected */
    public final boolean x() {
        return !this.j.a(this.b);
    }

    public final void a_(boolean z) {
        this.e.a(z);
    }

    @Nullable
    public final x<T> y() {
        return this.f;
    }

    public synchronized void a(@NonNull x<T> xVar) {
        this.f = xVar;
    }

    public final void a(@NonNull re reVar) {
        b(reVar);
    }

    /* access modifiers changed from: private */
    public void b(@NonNull re reVar) {
        if (reVar instanceof s) {
            onAdFailedToLoad(t.a(((s) reVar).a()));
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void a(@NonNull AdRequestError adRequestError) {
        if (this.r != null) {
            this.r.onAdFailedToLoad(adRequestError);
        }
    }

    public void b(@NonNull dh dhVar) {
        a(this.e.c(), dhVar);
    }

    @VisibleForTesting
    private boolean z() {
        if (!dl.b()) {
            onAdFailedToLoad(v.r);
            return false;
        } else if (an.a()) {
            return true;
        } else {
            onAdFailedToLoad(v.q);
            return false;
        }
    }
}
