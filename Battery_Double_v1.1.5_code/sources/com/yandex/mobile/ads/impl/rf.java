package com.yandex.mobile.ads.impl;

import android.os.SystemClock;
import android.support.graphics.drawable.PathInterpolatorCompat;
import com.yandex.mobile.ads.impl.qk.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.cookie.DateUtils;

public final class rf implements qo {

    /* renamed from: a reason: collision with root package name */
    protected static final boolean f5729a = qw.b;
    private static int d = PathInterpolatorCompat.MAX_NUM_POINTS;
    private static int e = 4096;
    protected final rj b;
    protected final rg c;

    public rf(rj rjVar) {
        this(rjVar, new rg(e));
    }

    private rf(rj rjVar, rg rgVar) {
        this.b = rjVar;
        this.c = rgVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01ac, code lost:
        throw new com.yandex.mobile.ads.impl.ra(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01ad, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01ae, code lost:
        r4 = new java.lang.StringBuilder("Bad URL ");
        r4.append(r21.b());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01c5, code lost:
        throw new java.lang.RuntimeException(r4.toString(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01c6, code lost:
        a("connection", r2, new com.yandex.mobile.ads.impl.rd());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01d2, code lost:
        a("socket", r2, new com.yandex.mobile.ads.impl.rd());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b2, code lost:
        if (r13 > ((long) d)) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b5, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b6, code lost:
        r13 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0110, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0112, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0113, code lost:
        r7 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0114, code lost:
        r13 = r5;
        r14 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0117, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0118, code lost:
        r14 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x011b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x011c, code lost:
        r14 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x011d, code lost:
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x011f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0120, code lost:
        r14 = r5;
        r10 = null;
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0125, code lost:
        r0 = r10.getStatusLine().getStatusCode();
        com.yandex.mobile.ads.impl.qw.c("Unexpected response code %d for %s", java.lang.Integer.valueOf(r0), r21.b());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0141, code lost:
        if (r13 != null) goto L_0x0143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0143, code lost:
        r11 = new com.yandex.mobile.ads.impl.qq(r0, r13, r14, false, android.os.SystemClock.elapsedRealtime() - r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0153, code lost:
        if (r0 == 401) goto L_0x018f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x015c, code lost:
        if (r0 < 400) goto L_0x0169;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0168, code lost:
        throw new com.yandex.mobile.ads.impl.qy(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x016b, code lost:
        if (r0 < 500) goto L_0x0189;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0175, code lost:
        if (r21.n() != false) goto L_0x0177;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0177, code lost:
        a(com.ironsource.mediationsdk.logger.ServerLogger.NAME, r2, new com.yandex.mobile.ads.impl.rc(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0188, code lost:
        throw new com.yandex.mobile.ads.impl.rc(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x018e, code lost:
        throw new com.yandex.mobile.ads.impl.rc(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x018f, code lost:
        a("auth", r2, new com.yandex.mobile.ads.impl.qx(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x019b, code lost:
        a("network", r2, new com.yandex.mobile.ads.impl.qz());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01ad A[ExcHandler: MalformedURLException (r0v2 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:106:? A[ExcHandler: ConnectTimeoutException (unused org.apache.http.conn.ConnectTimeoutException), SYNTHETIC, Splitter:B:2:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:108:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01a7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0125  */
    public final qq a(qr<?> qrVar) throws re {
        HttpResponse httpResponse;
        Map a2;
        byte[] bArr;
        qr<?> qrVar2 = qrVar;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            Map emptyMap = Collections.emptyMap();
            try {
                HashMap hashMap = new HashMap();
                a h = qrVar.h();
                if (h != null) {
                    if (h.b != null) {
                        hashMap.put(HttpRequest.HEADER_IF_NONE_MATCH, h.b);
                    }
                    if (h.d > 0) {
                        hashMap.put("If-Modified-Since", DateUtils.formatDate(new Date(h.d)));
                    }
                }
                httpResponse = this.b.a(qrVar2, hashMap);
                StatusLine statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                a2 = a(httpResponse.getAllHeaders());
                if (statusCode == 304) {
                    a h2 = qrVar.h();
                    if (h2 == null) {
                        qq qqVar = new qq(304, null, a2, true, SystemClock.elapsedRealtime() - elapsedRealtime);
                        return qqVar;
                    }
                    h2.g.putAll(a2);
                    qq qqVar2 = new qq(304, h2.f5712a, h2.g, true, SystemClock.elapsedRealtime() - elapsedRealtime);
                    return qqVar2;
                }
                if (httpResponse.getEntity() != null) {
                    bArr = a(httpResponse.getEntity());
                } else {
                    bArr = new byte[0];
                }
                long elapsedRealtime2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                if (!f5729a) {
                }
                String str = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]";
                Object[] objArr = new Object[5];
                objArr[0] = qrVar2;
                objArr[1] = Long.valueOf(elapsedRealtime2);
                objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
                objArr[3] = Integer.valueOf(statusLine.getStatusCode());
                objArr[4] = Integer.valueOf(qrVar.q().b());
                qw.b(str, objArr);
                if (statusCode < 200 || statusCode > 299) {
                    Map map = a2;
                } else {
                    Map map2 = a2;
                    r11 = r11;
                    qq qqVar3 = new qq(statusCode, bArr, map2, false, SystemClock.elapsedRealtime() - elapsedRealtime);
                    return qqVar3;
                }
            } catch (SocketTimeoutException unused) {
            } catch (ConnectTimeoutException unused2) {
            } catch (MalformedURLException e2) {
            } catch (IOException e3) {
                e = e3;
                byte[] bArr2 = null;
                Map map3 = a2;
                if (httpResponse != null) {
                }
            }
        }
        Map map4 = a2;
        throw new IOException();
    }

    private static void a(String str, qr<?> qrVar, re reVar) throws re {
        qv q = qrVar.q();
        int p = qrVar.p();
        try {
            q.a(reVar);
            String.format("%s-retry [timeout=%s]", new Object[]{str, Integer.valueOf(p)});
        } catch (re e2) {
            String.format("%s-timeout-giveup [timeout=%s]", new Object[]{str, Integer.valueOf(p)});
            throw e2;
        }
    }

    private byte[] a(HttpEntity httpEntity) throws IOException, rc {
        ro roVar = new ro(this.c, (int) httpEntity.getContentLength());
        byte[] bArr = null;
        try {
            InputStream content = httpEntity.getContent();
            if (content != null) {
                byte[] a2 = this.c.a(1024);
                while (true) {
                    try {
                        int read = content.read(a2);
                        if (read == -1) {
                            break;
                        }
                        roVar.write(a2, 0, read);
                    } catch (Throwable th) {
                        th = th;
                        bArr = a2;
                        try {
                            httpEntity.consumeContent();
                        } catch (IOException unused) {
                            qw.a("Error occured when calling consumingContent", new Object[0]);
                        }
                        this.c.a(bArr);
                        roVar.close();
                        throw th;
                    }
                }
                byte[] byteArray = roVar.toByteArray();
                try {
                    httpEntity.consumeContent();
                } catch (IOException unused2) {
                    qw.a("Error occured when calling consumingContent", new Object[0]);
                }
                this.c.a(a2);
                roVar.close();
                return byteArray;
            }
            throw new rc();
        } catch (Throwable th2) {
            th = th2;
            httpEntity.consumeContent();
            this.c.a(bArr);
            roVar.close();
            throw th;
        }
    }

    private static Map<String, String> a(Header[] headerArr) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < headerArr.length; i++) {
            treeMap.put(headerArr[i].getName(), headerArr[i].getValue());
        }
        return treeMap;
    }
}
