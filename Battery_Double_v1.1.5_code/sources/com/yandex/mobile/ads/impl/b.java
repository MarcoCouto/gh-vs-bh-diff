package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import java.lang.ref.WeakReference;

public final class b implements ca {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private WeakReference<e> f5384a;

    b(@NonNull e eVar) {
        this.f5384a = new WeakReference<>(eVar);
    }

    public final boolean a() {
        e eVar = (e) this.f5384a.get();
        return eVar != null && !dv.d((View) eVar);
    }
}
