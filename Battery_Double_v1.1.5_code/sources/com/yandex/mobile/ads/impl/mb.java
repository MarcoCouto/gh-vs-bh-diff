package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.nativeads.NativeGenericAd;
import com.yandex.mobile.ads.nativeads.z;

public interface mb {
    void a(@NonNull AdRequestError adRequestError);

    void a(@NonNull NativeGenericAd nativeGenericAd);

    void a(@NonNull z zVar);
}
