package com.yandex.mobile.ads.impl;

import android.content.Intent;

public final class qx extends re {
    private Intent b;

    public qx() {
    }

    public qx(qq qqVar) {
        super(qqVar);
    }

    public final String getMessage() {
        if (this.b != null) {
            return "User needs to (re)enter credentials.";
        }
        return super.getMessage();
    }
}
