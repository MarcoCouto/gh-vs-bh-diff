package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

final class ma {

    /* renamed from: a reason: collision with root package name */
    private static final Map<ae, lz> f5617a = new HashMap<ae, lz>() {
        {
            put(ae.AD, new mf());
            put(ae.AD_UNIT, new me());
        }
    };

    ma() {
    }

    @Nullable
    static lz a(@Nullable ae aeVar) {
        return (lz) f5617a.get(aeVar);
    }
}
