package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

public final class ai {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5368a;
    @NonNull
    private final cr b;
    @NonNull
    private final aj c = aj.a();

    public interface a {
        void a();

        void a(@NonNull re reVar);
    }

    public ai(@NonNull Context context, @NonNull fc fcVar) {
        this.f5368a = fcVar;
        this.b = new cr(context);
    }

    public final void a(@NonNull Context context, @NonNull a aVar) {
        String str;
        if (this.b.a()) {
            ak akVar = new ak(context, this.c, aVar);
            String f = this.f5368a.f();
            if (!TextUtils.isEmpty(f)) {
                String c2 = ad.a(fe.a(context)).b(this.f5368a.d()).a(this.f5368a.i()).b(context).a(context, this.f5368a.g()).f(context).a().b().c();
                StringBuilder sb = new StringBuilder(f);
                sb.append(f.endsWith("/") ? "" : "/");
                sb.append("v1/startup");
                sb.append("?");
                sb.append(c2);
                str = sb.toString();
            } else {
                str = null;
            }
            if (!TextUtils.isEmpty(str)) {
                ah.a().a(context, (qr) new br(str, this.b, akVar));
            } else {
                akVar.a(new s());
            }
        } else {
            aVar.a();
        }
    }
}
