package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.i;

public final class kw extends ld<TextView, nk> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final i f5597a;
    @NonNull
    private final lj b;

    public final /* bridge */ /* synthetic */ void a(@NonNull View view) {
        super.a((TextView) view);
    }

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        TextView textView = (TextView) view;
        nl a2 = ((nk) obj).a();
        if (a2 == null) {
            return true;
        }
        return this.b.a(textView.getBackground(), a2);
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        TextView textView = (TextView) view;
        nl a2 = ((nk) obj).a();
        if (a2 != null) {
            Bitmap a3 = this.f5597a.a(a2);
            if (a3 != null) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(textView.getResources(), a3);
                if (VERSION.SDK_INT >= 16) {
                    textView.setBackground(bitmapDrawable);
                    return;
                }
                textView.setBackgroundDrawable(bitmapDrawable);
            }
        }
    }

    public kw(@NonNull TextView textView, @NonNull i iVar) {
        super(textView);
        this.f5597a = iVar;
        this.b = new lj(iVar);
    }
}
