package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ru.a;
import com.yandex.mobile.ads.video.VastRequestConfiguration;

public final class tf extends rv<VastRequestConfiguration, sk> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tl f5786a = new tl();

    public tf(@NonNull Context context, @NonNull String str, @NonNull a<sk> aVar, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull gv<VastRequestConfiguration, sk> gvVar) {
        super(context, 0, str, aVar, vastRequestConfiguration, gvVar);
        new Object[1][0] = str;
    }

    /* access modifiers changed from: protected */
    public final qt<sk> a(@NonNull qq qqVar, int i) {
        sk a2 = this.f5786a.a(qqVar);
        if (a2 == null) {
            return qt.a(new sn("Can't parse VAST response."));
        }
        if (!a2.a().b().isEmpty()) {
            return qt.a(a2, null);
        }
        return qt.a(new sm());
    }
}
