package com.yandex.mobile.ads.impl;

public final class z {

    /* renamed from: a reason: collision with root package name */
    private final float f5822a;

    public z(float f) {
        if (f == 0.0f) {
            f = 1.7777778f;
        }
        this.f5822a = f;
    }

    public final int a(int i) {
        return Math.round(((float) i) * this.f5822a);
    }

    public final int b(int i) {
        return Math.round(((float) i) / this.f5822a);
    }
}
