package com.yandex.mobile.ads.impl;

public final class qm implements qv {

    /* renamed from: a reason: collision with root package name */
    private int f5715a;
    private int b;
    private final int c;
    private final float d;

    public qm() {
        this(2500, 1, 1.0f);
    }

    public qm(int i, int i2, float f) {
        this.f5715a = i;
        this.c = i2;
        this.d = f;
    }

    public final int a() {
        return this.f5715a;
    }

    public final int b() {
        return this.b;
    }

    public final void a(re reVar) throws re {
        boolean z = true;
        this.b++;
        this.f5715a = (int) (((float) this.f5715a) + (((float) this.f5715a) * this.d));
        if (this.b > this.c) {
            z = false;
        }
        if (!z) {
            throw reVar;
        }
    }
}
