package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.nativeads.l;
import com.yandex.mobile.ads.mediation.nativeads.t;
import com.yandex.mobile.ads.nativeads.u;

final class ke implements kg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final be f5589a;

    ke(@NonNull be beVar) {
        this.f5589a = beVar;
    }

    @NonNull
    public final t a(@NonNull u uVar) {
        return new l(uVar, this.f5589a);
    }
}
