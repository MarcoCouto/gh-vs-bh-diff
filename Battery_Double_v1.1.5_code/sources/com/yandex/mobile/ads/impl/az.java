package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.yandex.mobile.ads.AdRequest;
import java.util.HashMap;
import java.util.Map;

public class az implements at {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    protected final fc f5383a;

    public az(@NonNull fc fcVar) {
        this.f5383a = fcVar;
    }

    @NonNull
    public Map<String, Object> a(@NonNull Context context) {
        HashMap hashMap = new HashMap();
        AdRequest c = this.f5383a.c();
        if (c != null) {
            hashMap.put(IronSourceSegment.AGE, c.getAge());
            hashMap.put("context_tags", c.getContextTags());
            hashMap.put("gender", c.getGender());
            hashMap.put("location", c.getLocation());
            fg a2 = ff.a().a(context);
            Boolean f = a2 != null ? a2.f() : null;
            if (f != null) {
                hashMap.put("user_consent", f);
            }
        }
        return hashMap;
    }

    @NonNull
    public final Map<String, String> a(@NonNull bf bfVar) {
        return bfVar.b();
    }
}
