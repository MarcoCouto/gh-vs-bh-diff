package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.impl.gu.c;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class bd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cn f5387a = new cn();
    @NonNull
    private fc b;

    public bd(@NonNull fc fcVar) {
        this.b = fcVar;
    }

    public final void a(@NonNull Context context, @NonNull x xVar) {
        a(context, xVar, b.ADAPTER_REQUEST, Collections.emptyMap());
    }

    public final void b(@NonNull Context context, @NonNull x xVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("status", c.SUCCESS.a());
        a(context, xVar, b.ADAPTER_RESPONSE, hashMap);
    }

    public final void c(@NonNull Context context, @NonNull x xVar) {
        HashMap hashMap = new HashMap();
        new bc();
        hashMap.put("reward_info", bc.a(xVar));
        a(context, xVar, b.REWARD, hashMap);
    }

    private void a(@NonNull Context context, @NonNull x xVar, @NonNull b bVar, @NonNull Map<String, Object> map) {
        Map a2 = a(xVar);
        a2.putAll(map);
        gs.a(context).a(new gu(bVar, a2));
    }

    @NonNull
    private Map<String, Object> a(@NonNull x xVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("block_id", xVar.b());
        hashMap.put("adapter", "Yandex");
        com.yandex.mobile.ads.b a2 = xVar.a();
        hashMap.put("ad_type", a2 != null ? a2.a() : null);
        hashMap.putAll(cn.a(this.b.c()));
        if (xVar.t() != null && (xVar.t() instanceof nq)) {
            hashMap.put("native_ad_type", a(((nq) xVar.t()).c()));
        }
        return hashMap;
    }

    @NonNull
    private static String a(@Nullable List<np> list) {
        String str = "";
        if (list == null || list.isEmpty()) {
            return str;
        }
        NativeAdType b2 = ((np) list.get(0)).b();
        return b2 != null ? b2.getValue() : str;
    }
}
