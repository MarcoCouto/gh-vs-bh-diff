package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.Rating;

public final class lw implements OnTouchListener {

    /* renamed from: a reason: collision with root package name */
    private static volatile lw f5613a;
    private static final Object b = new Object();
    private final Handler c = new Handler();
    private final GestureDetector d;
    private boolean e;

    public static lw a(@NonNull Context context) {
        if (f5613a == null) {
            synchronized (b) {
                if (f5613a == null) {
                    f5613a = new lw(context);
                }
            }
        }
        return f5613a;
    }

    private lw(@NonNull Context context) {
        this.d = new GestureDetector(context, new SimpleOnGestureListener()) {
            public final boolean onTouchEvent(MotionEvent motionEvent) {
                return super.onTouchEvent(motionEvent);
            }
        };
    }

    public final boolean onTouch(final View view, MotionEvent motionEvent) {
        if (!(view instanceof TextView) && !(view instanceof Rating)) {
            return false;
        }
        if (motionEvent.getAction() == 1) {
            if (VERSION.SDK_INT >= 11 && view != null && !this.e) {
                view.setAlpha(view.getAlpha() / 2.0f);
                this.e = true;
            }
            this.c.postDelayed(new Runnable() {
                public final void run() {
                    lw.a(lw.this, view);
                }
            }, 100);
        }
        return this.d.onTouchEvent(motionEvent);
    }

    static /* synthetic */ void a(lw lwVar, View view) {
        if (VERSION.SDK_INT >= 11 && view != null && lwVar.e) {
            view.setAlpha(view.getAlpha() * 2.0f);
            lwVar.e = false;
        }
    }
}
