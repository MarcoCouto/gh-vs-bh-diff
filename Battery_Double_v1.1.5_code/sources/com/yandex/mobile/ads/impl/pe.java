package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.nativeads.y;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public final class pe {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final DecimalFormat f5691a;

    public pe() {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.US);
        decimalFormatSymbols.setGroupingSeparator(' ');
        this.f5691a = new DecimalFormat("#,###,###", decimalFormatSymbols);
    }

    @NonNull
    public final String a(@NonNull String str) throws y {
        String str2;
        try {
            if (!TextUtils.isEmpty(str)) {
                str2 = str.replaceAll(" ", "");
            } else {
                str2 = str;
            }
            return this.f5691a.format(Integer.valueOf(str2));
        } catch (NumberFormatException e) {
            String.format("Could not parse review count value. Review Count value is %s", new Object[]{str});
            new Object[1][0] = e;
            throw new y("Native Ad json has not required attributes");
        }
    }
}
