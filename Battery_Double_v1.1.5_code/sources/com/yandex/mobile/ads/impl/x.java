package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.b;
import java.util.ArrayList;
import java.util.List;

public final class x<T> implements Parcelable {
    public static final Creator<x> CREATOR = new Creator<x>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new x[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new x(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    public static final Integer f5819a = Integer.valueOf(100);
    @Nullable
    private final b b;
    @Nullable
    private final String c;
    @Nullable
    private final String d;
    @Nullable
    private final String e;
    @NonNull
    private final al f;
    private final int g;
    private final int h;
    @Nullable
    private final List<String> i;
    @Nullable
    private final List<String> j;
    @Nullable
    private cz k;
    @Nullable
    private final List<Long> l;
    @Nullable
    private final List<Integer> m;
    private final int n;
    private final int o;
    private final int p;
    private final int q;
    @Nullable
    private final String r;
    @Nullable
    private final be s;
    @Nullable
    private final bi t;
    @Nullable
    private final T u;
    private final boolean v;
    private final boolean w;

    public static class a<T> {
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public b f5820a;
        /* access modifiers changed from: private */
        @Nullable
        public String b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;
        /* access modifiers changed from: private */
        @Nullable
        public String d;
        /* access modifiers changed from: private */
        public int e;
        /* access modifiers changed from: private */
        public int f;
        /* access modifiers changed from: private */
        @Nullable
        public com.yandex.mobile.ads.impl.al.a g;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> h;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> i;
        /* access modifiers changed from: private */
        @Nullable
        public cz j;
        /* access modifiers changed from: private */
        @Nullable
        public List<Long> k;
        /* access modifiers changed from: private */
        @Nullable
        public List<Integer> l;
        /* access modifiers changed from: private */
        public int m;
        /* access modifiers changed from: private */
        public int n;
        /* access modifiers changed from: private */
        public int o;
        /* access modifiers changed from: private */
        public int p;
        /* access modifiers changed from: private */
        @Nullable
        public String q;
        /* access modifiers changed from: private */
        @Nullable
        public be r;
        /* access modifiers changed from: private */
        @Nullable
        public bi s;
        /* access modifiers changed from: private */
        @Nullable
        public T t;
        /* access modifiers changed from: private */
        public boolean u;
        /* access modifiers changed from: private */
        public boolean v;

        @NonNull
        public final a<T> a(@NonNull b bVar) {
            this.f5820a = bVar;
            return this;
        }

        @NonNull
        public final a<T> a(@NonNull String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a<T> b(@NonNull String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final a<T> c(@Nullable String str) {
            this.d = str;
            return this;
        }

        @NonNull
        public final a<T> a(int i2) {
            this.e = i2;
            return this;
        }

        @NonNull
        public final a<T> b(int i2) {
            this.f = i2;
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable com.yandex.mobile.ads.impl.al.a aVar) {
            this.g = aVar;
            return this;
        }

        @NonNull
        public final a<T> a(@NonNull List<String> list) {
            this.h = list;
            return this;
        }

        @NonNull
        public final a<T> b(@NonNull List<String> list) {
            this.i = list;
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable cz czVar) {
            this.j = czVar;
            return this;
        }

        @NonNull
        public final a<T> c(@NonNull List<Long> list) {
            this.k = list;
            return this;
        }

        @NonNull
        public final a<T> d(@NonNull List<Integer> list) {
            this.l = list;
            return this;
        }

        @NonNull
        public final a<T> c(int i2) {
            this.n = i2;
            return this;
        }

        @NonNull
        public final a<T> d(int i2) {
            this.o = i2;
            return this;
        }

        @NonNull
        public final a<T> e(int i2) {
            this.p = i2;
            return this;
        }

        @NonNull
        public final a<T> f(int i2) {
            this.m = i2;
            return this;
        }

        @NonNull
        public final a<T> d(@Nullable String str) {
            this.q = str;
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable be beVar) {
            this.r = beVar;
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable T t2) {
            this.t = t2;
            return this;
        }

        @NonNull
        public final a<T> a(@NonNull bi biVar) {
            this.s = biVar;
            return this;
        }

        @NonNull
        public final a<T> a(boolean z) {
            this.u = z;
            return this;
        }

        @NonNull
        public final a<T> b(boolean z) {
            this.v = z;
            return this;
        }

        @NonNull
        public final x<T> a() {
            return new x<>(this, 0);
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ x(a aVar, byte b2) {
        this(aVar);
    }

    private x(@NonNull a<T> aVar) {
        this.b = aVar.f5820a;
        this.e = aVar.d;
        this.c = aVar.b;
        this.d = aVar.c;
        this.g = aVar.e;
        this.h = aVar.f;
        this.f = new al(this.g, this.h, aVar.g != null ? aVar.g : com.yandex.mobile.ads.impl.al.a.FIXED);
        this.i = aVar.h;
        this.j = aVar.i;
        this.l = aVar.k;
        this.m = aVar.l;
        this.k = aVar.j;
        this.n = aVar.m;
        this.o = aVar.n;
        this.p = aVar.o;
        this.q = aVar.p;
        this.r = aVar.q;
        this.u = aVar.t;
        this.s = aVar.r;
        this.t = aVar.s;
        this.v = aVar.u;
        this.w = aVar.v;
    }

    @Nullable
    public final b a() {
        return this.b;
    }

    @Nullable
    public final String b() {
        return this.e;
    }

    @Nullable
    public final String c() {
        return this.c;
    }

    @Nullable
    public final String d() {
        return this.d;
    }

    @NonNull
    public final al e() {
        return this.f;
    }

    public final int f() {
        return this.g;
    }

    public final int g() {
        return this.h;
    }

    @Nullable
    public final List<String> h() {
        return this.i;
    }

    @Nullable
    public final List<String> i() {
        return this.j;
    }

    @Nullable
    public final cz j() {
        return this.k;
    }

    @Nullable
    public final List<Long> k() {
        return this.l;
    }

    @Nullable
    public final List<Integer> l() {
        return this.m;
    }

    public final int m() {
        return this.o;
    }

    public final int n() {
        return this.n;
    }

    public final int o() {
        return this.o * 1000;
    }

    public final int p() {
        return this.p * 1000;
    }

    @Nullable
    public final String q() {
        return this.r;
    }

    @Nullable
    public final bi r() {
        return this.t;
    }

    @Nullable
    public final be s() {
        return this.s;
    }

    @Nullable
    public final T t() {
        return this.u;
    }

    public final boolean u() {
        return this.h == 0;
    }

    public final boolean v() {
        return this.o > 0;
    }

    public final boolean w() {
        return this.v;
    }

    public final boolean x() {
        return this.w;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i2) {
        parcel.writeInt(this.b == null ? -1 : this.b.ordinal());
        parcel.writeString(this.e);
        parcel.writeString(this.c);
        parcel.writeString(this.r);
        parcel.writeParcelable(this.f, i2);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h);
        parcel.writeStringList(this.i);
        parcel.writeStringList(this.j);
        parcel.writeList(this.l);
        parcel.writeList(this.m);
        parcel.writeInt(this.n);
        parcel.writeInt(this.o);
        parcel.writeInt(this.p);
        parcel.writeInt(this.q);
        parcel.writeString(this.r);
        parcel.writeParcelable(this.s, i2);
        parcel.writeParcelable(this.t, i2);
        parcel.writeSerializable(this.u.getClass());
        parcel.writeValue(this.u);
        parcel.writeByte(this.v ? (byte) 1 : 0);
        parcel.writeByte(this.w ? (byte) 1 : 0);
    }

    protected x(@NonNull Parcel parcel) {
        b bVar;
        int readInt = parcel.readInt();
        T t2 = null;
        if (readInt == -1) {
            bVar = null;
        } else {
            bVar = b.values()[readInt];
        }
        this.b = bVar;
        this.e = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.f = (al) parcel.readParcelable(al.class.getClassLoader());
        this.g = parcel.readInt();
        this.h = parcel.readInt();
        this.i = parcel.createStringArrayList();
        this.j = parcel.createStringArrayList();
        this.l = new ArrayList();
        parcel.readList(this.l, Long.class.getClassLoader());
        this.m = new ArrayList();
        parcel.readList(this.m, Integer.class.getClassLoader());
        this.n = parcel.readInt();
        this.o = parcel.readInt();
        this.p = parcel.readInt();
        this.q = parcel.readInt();
        this.r = parcel.readString();
        this.s = (be) parcel.readParcelable(be.class.getClassLoader());
        this.t = (bi) parcel.readParcelable(bi.class.getClassLoader());
        Class cls = (Class) parcel.readSerializable();
        if (cls != null) {
            t2 = parcel.readValue(cls.getClassLoader());
        }
        this.u = t2;
        boolean z = false;
        this.v = parcel.readByte() != 0;
        if (parcel.readByte() != 0) {
            z = true;
        }
        this.w = z;
    }
}
