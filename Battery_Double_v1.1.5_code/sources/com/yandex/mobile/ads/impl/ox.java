package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.y;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class ox {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ow f5685a;

    ox(@NonNull oj ojVar) {
        this.f5685a = new ow(ojVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final List<nx> a(@Nullable JSONArray jSONArray) throws JSONException, y {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject optJSONObject = jSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    arrayList.add(this.f5685a.a(optJSONObject));
                }
            }
        }
        return arrayList;
    }
}
