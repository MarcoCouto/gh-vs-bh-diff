package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class hu {

    public interface a {
        boolean h();
    }

    public static <T extends View & a> boolean a(@NonNull T t) {
        return ((a) t).h() && t.hasWindowFocus() && !dv.d((View) t);
    }
}
