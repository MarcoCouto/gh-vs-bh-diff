package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public final class tl {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bm f5792a = new bm();
    @NonNull
    private final tk b = new tk();

    @Nullable
    public final sk a(@NonNull qq qqVar) {
        String a2 = bm.a(qqVar);
        if (!TextUtils.isEmpty(a2)) {
            try {
                sj a3 = this.b.a(a2);
                if (a3 != null) {
                    boolean z = false;
                    if (qqVar.c != null) {
                        z = bl.b(qqVar.c, qj.YMAD_RAW_VAST_ENABLED);
                    }
                    if (!z) {
                        a2 = null;
                    }
                    return new sk(a3, a2);
                }
            } catch (Exception unused) {
            }
        }
        return null;
    }
}
