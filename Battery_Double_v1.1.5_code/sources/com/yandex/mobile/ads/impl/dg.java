package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ad.a;

public final class dg implements dh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final be f5437a;

    public dg(@NonNull be beVar) {
        this.f5437a = beVar;
    }

    @Nullable
    public final String a(@NonNull fc fcVar) {
        return ad.a(fcVar);
    }

    @NonNull
    public final String a(@NonNull Context context, @NonNull fc fcVar) {
        a a2 = ad.a(context, fcVar);
        a2.a(this.f5437a.b());
        return a2.c();
    }

    @NonNull
    public final int a() {
        return dh.a.b;
    }
}
