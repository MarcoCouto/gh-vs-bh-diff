package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.qs.a;
import com.yandex.mobile.ads.impl.sw.b;
import com.yandex.mobile.ads.video.BlocksInfoRequest;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import com.yandex.mobile.ads.video.models.vmap.Vmap;
import com.yandex.mobile.ads.video.tracking.Tracker.ErrorListener;
import java.util.List;

public final class su {

    /* renamed from: a reason: collision with root package name */
    public static final a f5776a = new a() {
        public final boolean a(qr<?> qrVar) {
            return true;
        }
    };
    private final qs b;

    public su(qs qsVar) {
        this.b = qsVar;
    }

    public final void a(@NonNull BlocksInfoRequest blocksInfoRequest, @NonNull fc fcVar) {
        String partnerId = blocksInfoRequest.getPartnerId();
        String categoryId = blocksInfoRequest.getCategoryId();
        Builder buildUpon = Uri.parse(sw.a(fcVar)).buildUpon();
        buildUpon.appendPath("v1").appendPath("vcset").appendPath(partnerId).appendQueryParameter("video-category-id", categoryId).appendQueryParameter("uuid", fcVar.d());
        this.b.a((qr<T>) new td<T>(blocksInfoRequest, buildUpon.build().toString(), new b(blocksInfoRequest.getRequestListener()), new ud()));
    }

    public final void a(@NonNull ss ssVar, @NonNull fc fcVar) {
        this.b.a((qr<T>) c.a(ssVar, fcVar));
    }

    public final void a(String str, ErrorListener errorListener) {
        this.b.a((qr<T>) new te<T>(str, new sw.a(errorListener)));
    }

    public final void a(@NonNull Context context, @NonNull fc fcVar, @NonNull VmapRequestConfiguration vmapRequestConfiguration, @NonNull RequestListener<Vmap> requestListener) {
        this.b.a(new tb().a(context, fcVar, vmapRequestConfiguration, requestListener));
    }

    public final void a(@NonNull Context context, @NonNull fc fcVar, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull RequestListener<sk> requestListener) {
        new ta();
        this.b.a(ta.a(context, fcVar, vastRequestConfiguration, requestListener));
    }

    public final void a(@NonNull Context context, @NonNull VideoAd videoAd, @NonNull ub ubVar, @NonNull RequestListener<List<VideoAd>> requestListener) {
        new tc();
        Context context2 = context;
        th thVar = new th(context2, videoAd.getVastAdTagUri(), new b(requestListener), videoAd, new uh(ubVar));
        this.b.a((qr<T>) thVar);
    }
}
