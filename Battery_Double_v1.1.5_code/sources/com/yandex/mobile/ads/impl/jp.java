package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.interstitial.d;
import com.yandex.mobile.ads.mediation.rewarded.a;
import com.yandex.mobile.ads.rewarded.b;

final class jp implements jn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final be f5580a;

    jp(@NonNull be beVar) {
        this.f5580a = beVar;
    }

    @NonNull
    public final jm a(@NonNull hm hmVar) {
        return new d(hmVar, this.f5580a);
    }

    @NonNull
    public final jm a(@NonNull b bVar) {
        return new a(bVar, this.f5580a);
    }
}
