package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public final class fs {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5496a;
    /* access modifiers changed from: private */
    @NonNull
    public final List<fq> b = new CopyOnWriteArrayList();

    public interface a {
        void a();

        void b();
    }

    public fs(@NonNull fc fcVar) {
        this.f5496a = fcVar;
    }

    public final void a(@NonNull fr frVar, @NonNull final a aVar) {
        AnonymousClass1 r0 = new fq() {
            public final void a(@NonNull Map<String, String> map) {
                fs.this.b.remove(this);
                fs.a(fs.this, (Map) map);
                aVar.a();
            }

            public final void a() {
                fs.this.b.remove(this);
                aVar.b();
            }
        };
        this.b.add(r0);
        frVar.a(r0);
    }

    public final void a(@NonNull fr frVar) {
        for (fq b2 : this.b) {
            frVar.b(b2);
        }
    }

    static /* synthetic */ void a(fs fsVar, Map map) {
        new Object[1][0] = map;
        fsVar.f5496a.a((String) map.get("yandex_mobile_metrica_uuid"));
        fsVar.f5496a.c((String) map.get("yandex_mobile_metrica_get_ad_url"));
        fsVar.f5496a.d((String) map.get("yandex_mobile_metrica_device_id"));
    }
}
