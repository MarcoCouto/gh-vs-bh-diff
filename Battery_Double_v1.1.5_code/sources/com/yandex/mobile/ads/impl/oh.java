package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.ironsource.sdk.precache.DownloadManager;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.tapjoy.TJAdUnitConstants.String;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.y;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class oh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ok f5673a;
    @NonNull
    private final oo b = new oo(this.d);
    @NonNull
    private final oj c = new oj(this.d);
    @NonNull
    private final ds d;

    public oh(@NonNull Context context) {
        this.d = new ds(context);
        this.f5673a = new ok(context, this.b);
    }

    @NonNull
    public final nq a(@NonNull String str) throws JSONException, y {
        nq a2 = a(new JSONObject(str));
        boolean z = false;
        if (a2 != null) {
            List c2 = a2.c();
            if (c2 != null && !c2.isEmpty()) {
                z = true;
            }
        }
        if (z) {
            return a2;
        }
        throw new y("Native Ad json has not required attributes");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0074 A[LOOP:1: B:10:0x0044->B:24:0x0074, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x007a A[SYNTHETIC] */
    @VisibleForTesting
    private nq a(JSONObject jSONObject) throws JSONException, y {
        boolean z;
        nq nqVar = (nq) he.a(nq.class, new Object[0]);
        if (nqVar != null) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("native");
            if (oi.a(jSONObject2, CampaignUnit.JSON_KEY_ADS)) {
                Iterator keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if (CampaignUnit.JSON_KEY_ADS.equals(str)) {
                        ArrayList arrayList = new ArrayList();
                        JSONArray jSONArray = jSONObject2.getJSONArray(CampaignUnit.JSON_KEY_ADS);
                        int i = 0;
                        while (i < jSONArray.length()) {
                            np f = f(jSONArray.getJSONObject(i));
                            if (f != null) {
                                List c2 = f.c();
                                nm a2 = f.a();
                                NativeAdType b2 = f.b();
                                if (!c2.isEmpty()) {
                                    if ((a2 != null) && b2 != null) {
                                        z = true;
                                        if (!z) {
                                            arrayList.add(f);
                                            i++;
                                        } else {
                                            throw new y("Native Ad json has not required attributes");
                                        }
                                    }
                                }
                            }
                            z = false;
                            if (!z) {
                            }
                        }
                        nqVar.b(arrayList);
                    } else if ("assets".equals(str)) {
                        List g = g(jSONObject2);
                        if (g.isEmpty()) {
                            g = null;
                        }
                        nqVar.a(g);
                    } else if (DownloadManager.SETTINGS.equals(str)) {
                        nqVar.a(b(jSONObject2));
                    } else if ("showNotices".equals(str)) {
                        nqVar.c(c(jSONObject2));
                    } else if ("ver".equals(str)) {
                        nqVar.a(a(jSONObject2, str));
                    } else if ("renderTrackingUrls".equals(str)) {
                        nqVar.d(d(jSONObject2));
                    }
                }
            } else {
                throw new y("Native Ad json has not required attributes");
            }
        }
        return nqVar;
    }

    private static nr b(JSONObject jSONObject) throws JSONException, y {
        nr nrVar = (nr) he.a(nr.class, new Object[0]);
        if (nrVar != null) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(DownloadManager.SETTINGS);
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                if ("templateType".equals(str)) {
                    nrVar.a(a(jSONObject2, str));
                } else if ("highlightingEnabled".equals(str)) {
                    nrVar.a(jSONObject2.getBoolean(str));
                }
            }
        }
        return nrVar;
    }

    private List<bg> c(JSONObject jSONObject) throws JSONException, y {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("showNotices");
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(e(jSONArray.getJSONObject(i)));
        }
        return arrayList;
    }

    @NonNull
    private List<String> d(@NonNull JSONObject jSONObject) throws JSONException {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("renderTrackingUrls");
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(this.d.a(jSONArray.getString(i)));
        }
        return arrayList;
    }

    @VisibleForTesting
    private bg e(JSONObject jSONObject) throws y, JSONException {
        bg bgVar = (bg) he.a(bg.class, new Object[0]);
        if (bgVar != null) {
            if (oi.a(jSONObject, AdvertisementColumns.COLUMN_DELAY, "url")) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if (AdvertisementColumns.COLUMN_DELAY.equals(str)) {
                        bgVar.a(jSONObject.getLong(str));
                    } else if ("url".equals(str)) {
                        bgVar.a(this.c.a(jSONObject, str));
                    } else if ("visibilityPercent".equals(str)) {
                        bgVar.a(Math.max(Math.min(jSONObject.optInt(str, 0), 100), 0));
                    }
                }
            } else {
                throw new y("Native Ad json has not required attributes");
            }
        }
        return bgVar;
    }

    @VisibleForTesting
    private np f(JSONObject jSONObject) throws JSONException, y {
        np npVar = (np) he.a(np.class, new Object[0]);
        if (npVar != null) {
            if (oi.a(jSONObject, "adType", "assets", "link")) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if ("adType".equals(str)) {
                        npVar.a(a(jSONObject, str));
                    } else if ("assets".equals(str)) {
                        npVar.a(g(jSONObject));
                    } else if ("link".equals(str)) {
                        npVar.a(this.b.a(jSONObject.getJSONObject(str)));
                    } else if ("showNotice".equals(str)) {
                        npVar.a(e(jSONObject.getJSONObject(str)));
                    } else if (String.VIDEO_INFO.equals(str)) {
                        npVar.d(jSONObject.optString(str, null));
                    } else if ("hideConditions".equals(str)) {
                        new ol();
                        npVar.a(ol.a(jSONObject, str));
                    } else if ("showConditions".equals(str)) {
                        new ol();
                        npVar.b(ol.a(jSONObject, str));
                    } else if ("renderTrackingUrl".equals(str)) {
                        npVar.c(this.c.a(jSONObject, str));
                    }
                }
            } else {
                throw new y("Native Ad json has not required attributes");
            }
        }
        return npVar;
    }

    @NonNull
    private List<ni> g(JSONObject jSONObject) throws JSONException, y {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("assets");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            boolean z = jSONObject2.getBoolean(VastAttributes.REQUIRED);
            try {
                arrayList.add(this.f5673a.a(jSONObject2));
            } catch (y | JSONException e) {
                if (z) {
                    throw e;
                }
            }
        }
        if (!oi.a(arrayList)) {
            return arrayList;
        }
        throw new y("Native Ad json has not required attributes");
    }

    @NonNull
    public static String a(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, y {
        String string = jSONObject.getString(str);
        if (!TextUtils.isEmpty(string) && !"null".equals(string)) {
            return string;
        }
        throw new y("Native Ad json has not required attributes");
    }
}
