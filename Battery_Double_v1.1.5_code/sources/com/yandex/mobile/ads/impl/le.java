package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class le<V extends View, T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ld<V, T> f5600a;

    le(@NonNull ld<V, T> ldVar) {
        this.f5600a = ldVar;
    }

    public final void a() {
        View a2 = this.f5600a.a();
        if (a2 != null) {
            this.f5600a.a(a2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull ni niVar, @NonNull li liVar) {
        if (this.f5600a.a() != null) {
            this.f5600a.a(niVar, liVar);
        }
    }

    public final void a(@NonNull T t) {
        View a2 = this.f5600a.a();
        if (a2 != null) {
            this.f5600a.b(a2, t);
            a2.setVisibility(0);
        }
    }

    public final boolean b(@NonNull T t) {
        View a2 = this.f5600a.a();
        return a2 != null && this.f5600a.a(a2, t);
    }
}
