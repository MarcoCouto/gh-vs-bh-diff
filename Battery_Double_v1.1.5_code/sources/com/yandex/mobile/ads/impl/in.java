package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class in {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final im f5551a;
    /* access modifiers changed from: private */
    @NonNull
    public final Handler b = new Handler(Looper.getMainLooper());
    @NonNull
    private final iy c = new iy();
    /* access modifiers changed from: private */
    @NonNull
    public final il d = new il();
    private boolean e;

    public in(@NonNull im imVar) {
        this.f5551a = imVar;
    }

    public final void a(int i, String str) {
        this.e = true;
        this.b.removeCallbacks(this.d);
        this.b.post(new io(i, str, this.f5551a));
    }

    public final void a() {
        if (!this.e) {
            this.c.a(new Runnable() {
                public final void run() {
                    in.this.b.postDelayed(in.this.d, 10000);
                }
            });
        }
    }

    public final void a(@Nullable ec ecVar) {
        this.d.a(ecVar);
    }

    public final void b() {
        this.b.removeCallbacksAndMessages(null);
        this.d.a(null);
    }
}
