package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public interface ki<T> {
    void a();

    void a(@NonNull ni niVar, @NonNull li liVar);

    void a(@NonNull T t);

    boolean b();

    boolean b(@NonNull T t);

    boolean c();

    boolean d();
}
