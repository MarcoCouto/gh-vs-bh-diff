package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.yandex.mobile.ads.impl.hu.a;
import java.lang.ref.WeakReference;

public final class eu<T extends View & com.yandex.mobile.ads.impl.hu.a> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final T f5475a;
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    @NonNull
    private final et c;
    @NonNull
    private final ev d;
    @Nullable
    private Runnable e;

    @VisibleForTesting
    static class a<T extends View & com.yandex.mobile.ads.impl.hu.a> implements Runnable {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final WeakReference<ev> f5476a;
        @NonNull
        private final WeakReference<T> b;
        @NonNull
        private final Handler c;
        @NonNull
        private final et d;

        a(@NonNull T t, @NonNull ev evVar, @NonNull Handler handler, @NonNull et etVar) {
            this.b = new WeakReference<>(t);
            this.f5476a = new WeakReference<>(evVar);
            this.c = handler;
            this.d = etVar;
        }

        public final void run() {
            View view = (View) this.b.get();
            ev evVar = (ev) this.f5476a.get();
            if (view != null && evVar != null) {
                evVar.a(et.a(view));
                this.c.postDelayed(this, 200);
            }
        }
    }

    public eu(@NonNull T t, @NonNull et etVar, @NonNull ev evVar) {
        this.f5475a = t;
        this.c = etVar;
        this.d = evVar;
    }

    public final void a() {
        if (this.e == null) {
            this.e = new a(this.f5475a, this.d, this.b, this.c);
            this.b.post(this.e);
        }
    }

    public final void b() {
        this.b.removeCallbacksAndMessages(null);
        this.e = null;
    }
}
