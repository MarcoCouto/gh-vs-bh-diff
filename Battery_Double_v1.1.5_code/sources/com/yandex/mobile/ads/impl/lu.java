package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class lu implements lt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final nm f5611a;
    @NonNull
    private final lq b;

    public lu(@NonNull nm nmVar, @NonNull lq lqVar) {
        this.f5611a = nmVar;
        this.b = lqVar;
    }

    public final void a(@NonNull View view, @NonNull String str) {
        this.b.a(new nm(this.f5611a.a(), this.f5611a.b(), this.f5611a.c(), str)).onClick(view);
    }
}
