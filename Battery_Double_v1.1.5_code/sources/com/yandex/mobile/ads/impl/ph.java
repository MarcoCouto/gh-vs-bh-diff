package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class ph implements pg<nl> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final oj f5692a;

    public ph(@NonNull Context context) {
        this.f5692a = new oj(new ds(context));
    }

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, y {
        nl nlVar = new nl();
        nlVar.a(this.f5692a.a(jSONObject, "url"));
        nlVar.a(jSONObject.getInt("w"));
        nlVar.b(jSONObject.getInt("h"));
        String optString = jSONObject.optString("sizeType");
        if (!TextUtils.isEmpty(optString)) {
            nlVar.b(optString);
        }
        return nlVar;
    }
}
