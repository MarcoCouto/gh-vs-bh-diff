package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class ou implements op<nw> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ox f5683a = new ox(this.b);
    @NonNull
    private final oj b;

    public ou(@NonNull oj ojVar) {
        this.b = ojVar;
    }

    @NonNull
    public final /* synthetic */ ns a(@NonNull JSONObject jSONObject) throws JSONException, y {
        return new nw(oh.a(jSONObject, "type"), this.b.a(jSONObject, "fallbackUrl"), this.f5683a.a(jSONObject.optJSONArray("preferredPackages")));
    }
}
