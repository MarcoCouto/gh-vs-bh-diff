package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;

final class ar {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cj f5377a;
    private boolean b;
    private boolean c;

    ar(@NonNull cj cjVar) {
        this.f5377a = cjVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.b = false;
        this.c = false;
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        if (!this.b) {
            this.b = true;
            this.f5377a.a(b.IMPRESSION_TRACKING_START);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        if (!this.c) {
            this.c = true;
            HashMap hashMap = new HashMap();
            hashMap.put("failure_tracked", Boolean.FALSE);
            this.f5377a.a(b.IMPRESSION_TRACKING_SUCCESS, hashMap);
        }
    }
}
