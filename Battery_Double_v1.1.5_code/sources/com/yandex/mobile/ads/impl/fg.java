package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class fg {

    /* renamed from: a reason: collision with root package name */
    private final long f5484a;
    private final boolean b;
    private final boolean c;
    private final boolean d;
    private final boolean e;
    private final boolean f;
    private final boolean g;
    @Nullable
    private final Boolean h;
    @Nullable
    private final Boolean i;
    @Nullable
    private final String j;
    @Nullable
    private final String k;
    @Nullable
    private final Boolean l;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public long f5485a;
        /* access modifiers changed from: private */
        public boolean b;
        /* access modifiers changed from: private */
        public boolean c;
        /* access modifiers changed from: private */
        public boolean d;
        /* access modifiers changed from: private */
        public boolean e;
        /* access modifiers changed from: private */
        public boolean f;
        /* access modifiers changed from: private */
        public boolean g;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean h;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean i;
        /* access modifiers changed from: private */
        @Nullable
        public String j;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean k;
        /* access modifiers changed from: private */
        @Nullable
        public String l;

        @NonNull
        public final fg a() {
            return new fg(this, 0);
        }

        @NonNull
        public final a a(long j2) {
            this.f5485a = j2;
            return this;
        }

        @NonNull
        public final a a(boolean z) {
            this.b = z;
            return this;
        }

        @NonNull
        public final a b(boolean z) {
            this.g = z;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.j = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Boolean bool) {
            this.i = bool;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.l = str;
            return this;
        }

        @NonNull
        public final a c(boolean z) {
            this.f = z;
            return this;
        }

        @NonNull
        public final a b(@Nullable Boolean bool) {
            this.k = bool;
            return this;
        }

        @NonNull
        public final a d(boolean z) {
            this.c = z;
            return this;
        }

        @NonNull
        public final a e(boolean z) {
            this.d = z;
            return this;
        }

        @NonNull
        public final a f(boolean z) {
            this.e = z;
            return this;
        }

        @NonNull
        public final a c(@Nullable Boolean bool) {
            this.h = bool;
            return this;
        }
    }

    /* synthetic */ fg(a aVar, byte b2) {
        this(aVar);
    }

    private fg(@NonNull a aVar) {
        this.f5484a = aVar.f5485a;
        this.j = aVar.j;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.i = aVar.i;
        this.k = aVar.l;
        this.l = aVar.k;
        this.f = aVar.f;
        this.h = aVar.h;
        this.g = aVar.g;
    }

    public final long a() {
        return this.f5484a;
    }

    public final boolean b() {
        return this.b;
    }

    public final boolean c() {
        return this.f;
    }

    @Nullable
    public final Boolean d() {
        return this.h;
    }

    @Nullable
    public final String e() {
        return this.j;
    }

    @Nullable
    public final Boolean f() {
        return this.l;
    }

    public final boolean g() {
        return this.c;
    }

    public final boolean h() {
        return this.d;
    }

    public final boolean i() {
        return this.e;
    }

    @Nullable
    public final String j() {
        return this.k;
    }

    @Nullable
    public final Boolean k() {
        return this.i;
    }

    public final boolean l() {
        return this.g;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        fg fgVar = (fg) obj;
        if (this.f5484a != fgVar.f5484a || this.b != fgVar.b || this.c != fgVar.c || this.d != fgVar.d || this.e != fgVar.e || this.f != fgVar.f || this.g != fgVar.g) {
            return false;
        }
        if (this.h == null ? fgVar.h != null : !this.h.equals(fgVar.h)) {
            return false;
        }
        if (this.i == null ? fgVar.i != null : !this.i.equals(fgVar.i)) {
            return false;
        }
        if (this.j == null ? fgVar.j != null : !this.j.equals(fgVar.j)) {
            return false;
        }
        if (this.k == null ? fgVar.k != null : !this.k.equals(fgVar.k)) {
            return false;
        }
        if (this.l != null) {
            return this.l.equals(fgVar.l);
        }
        return fgVar.l == null;
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = ((((((((((((((((((((((int) (this.f5484a ^ (this.f5484a >>> 32))) * 31) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0)) * 31) + (this.g ? 1 : 0)) * 31) + (this.h != null ? this.h.hashCode() : 0)) * 31) + (this.i != null ? this.i.hashCode() : 0)) * 31) + (this.j != null ? this.j.hashCode() : 0)) * 31) + (this.k != null ? this.k.hashCode() : 0)) * 31;
        if (this.l != null) {
            i2 = this.l.hashCode();
        }
        return hashCode + i2;
    }
}
