package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.nu.a;
import com.yandex.mobile.ads.nativeads.y;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class os implements op<nu> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final oj f5681a;

    public os(@NonNull oj ojVar) {
        this.f5681a = ojVar;
    }

    @NonNull
    public final /* synthetic */ ns a(@NonNull JSONObject jSONObject) throws JSONException, y {
        String a2 = oh.a(jSONObject, "type");
        JSONArray jSONArray = jSONObject.getJSONArray("items");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            arrayList.add(new a(oh.a(jSONObject2, "title"), this.f5681a.a(jSONObject2, "url")));
        }
        return new nu(a2, arrayList);
    }
}
