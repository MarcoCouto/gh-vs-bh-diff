package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.HashMap;
import java.util.List;

public final class uf implements gv<VideoAdRequest, List<VideoAd>> {
    public final /* synthetic */ gu a(@Nullable qt qtVar, int i, @NonNull Object obj) {
        VideoAdRequest videoAdRequest = (VideoAdRequest) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("partner_id", videoAdRequest.getBlocksInfo().getPartnerId());
        hashMap.put("block_id", videoAdRequest.getBlockId());
        hashMap.put("page_ref", videoAdRequest.getPageRef());
        hashMap.put("target_ref", videoAdRequest.getTargetRef());
        if (i != -1) {
            hashMap.put("code", Integer.valueOf(i));
        }
        return new gu(b.VAST_RESPONSE, hashMap);
    }

    public final /* synthetic */ gu a(Object obj) {
        VideoAdRequest videoAdRequest = (VideoAdRequest) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("partner_id", videoAdRequest.getBlocksInfo().getPartnerId());
        hashMap.put("block_id", videoAdRequest.getBlockId());
        hashMap.put("page_ref", videoAdRequest.getPageRef());
        hashMap.put("target_ref", videoAdRequest.getTargetRef());
        return new gu(b.VAST_REQUEST, hashMap);
    }
}
