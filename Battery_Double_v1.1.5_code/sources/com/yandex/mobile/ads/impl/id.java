package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

final class id implements ib {
    id() {
    }

    @NonNull
    public final hy a(@NonNull eg egVar, @NonNull ec ecVar, @NonNull dy dyVar, @NonNull ii iiVar) {
        Cif ifVar = new Cif(egVar, ecVar, dyVar, iiVar);
        ifVar.a(ecVar);
        return ifVar;
    }

    @NonNull
    public final hx a(@NonNull eg egVar, @NonNull ec ecVar, @NonNull ij ijVar, @NonNull ef efVar, @NonNull ii iiVar) {
        ie ieVar = new ie(egVar, ecVar, ijVar, efVar, iiVar);
        ieVar.a(ecVar);
        return ieVar;
    }

    @NonNull
    public final hy a(@NonNull eg egVar, @NonNull ec ecVar) {
        ih ihVar = new ih(egVar, ecVar);
        ihVar.a(ecVar);
        return ihVar;
    }
}
