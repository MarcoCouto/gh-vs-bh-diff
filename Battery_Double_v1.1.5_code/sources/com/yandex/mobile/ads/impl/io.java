package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class io implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final int f5553a;
    @Nullable
    private final String b;
    @NonNull
    private final im c;

    io(int i, @Nullable String str, @NonNull im imVar) {
        this.f5553a = i;
        this.b = str;
        this.c = imVar;
    }

    public final void run() {
        this.c.b(this.f5553a, this.b);
    }
}
