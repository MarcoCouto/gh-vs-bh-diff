package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.ap.a;
import com.yandex.mobile.ads.impl.as.c;
import java.util.List;
import java.util.Map;

public abstract class ht extends hr implements a {
    @NonNull
    private final as g;
    @NonNull
    private final ca h;
    @NonNull
    private final ap i;
    @Nullable
    private cd j;
    @NonNull
    private final cy k;
    private final c l = new c() {
        @NonNull
        public final am a(int i) {
            am.a aVar;
            if (ht.this.x()) {
                aVar = am.a.APPLICATION_INACTIVE;
            } else if (!ht.this.k()) {
                aVar = am.a.AD_NOT_LOADED;
            } else if (ht.this.g()) {
                aVar = am.a.SUPERVIEW_HIDDEN;
            } else if (!ht.this.a(i) || !ht.this.b()) {
                aVar = am.a.NOT_VISIBLE_FOR_PERCENT;
            } else {
                aVar = am.a.SUCCESS;
            }
            return new am(aVar, new ce());
        }
    };

    /* access modifiers changed from: protected */
    @NonNull
    public abstract hy a(@NonNull String str, @NonNull x<String> xVar, @NonNull al alVar, @NonNull ap apVar);

    /* access modifiers changed from: protected */
    public abstract boolean a(int i2);

    /* access modifiers changed from: protected */
    public abstract boolean b();

    protected ht(@NonNull Context context, @NonNull ca caVar, @NonNull b bVar) {
        super(context, bVar);
        this.h = caVar;
        cj cjVar = new cj(context, bVar, s());
        this.i = new ap(this, cjVar);
        as asVar = new as(this.b, s(), cjVar, this.l, di.a(this));
        this.g = asVar;
        this.g.a((aq) this.i);
        this.k = new cy(this.b, s());
    }

    public final void b(@NonNull x<String> xVar) {
        if (a(xVar.e())) {
            super.b(xVar);
        } else {
            onAdFailedToLoad(v.e);
        }
    }

    public void a(@NonNull Intent intent) {
        StringBuilder sb = new StringBuilder("onPhoneStateChanged(), intent.getAction = ");
        sb.append(intent.getAction());
        sb.append(", isAdVisible = ");
        sb.append(a());
        this.g.a(intent, a());
    }

    public void b(int i2) {
        if (i2 == 0) {
            this.g.a();
        } else {
            this.g.b();
        }
        Object[] objArr = {getClass().toString(), Integer.valueOf(i2)};
    }

    public void d() {
        new StringBuilder("cleanOut(), clazz = ").append(this);
        super.d();
        this.g.b();
    }

    /* access modifiers changed from: protected */
    public final hy a(@NonNull String str, @NonNull x<String> xVar, @NonNull al alVar) {
        return a(str, xVar, alVar, this.i);
    }

    public synchronized void a(@NonNull x<String> xVar) {
        super.a(xVar);
        x<String> xVar2 = xVar;
        cd cdVar = new cd(this.b, this.h, xVar2, this.e, xVar.i());
        this.j = cdVar;
    }

    public void a(int i2, @Nullable Bundle bundle) {
        if (i2 != 9) {
            switch (i2) {
                case 14:
                    this.i.b();
                    return;
                case 15:
                    this.i.d_();
                    return;
                default:
                    super.a(i2, bundle);
                    return;
            }
        } else {
            this.k.d();
            this.g.c();
        }
    }

    public void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        new StringBuilder("onAdDisplayed(), clazz = ").append(this);
        if (this.f != null) {
            List a2 = di.a(this.f, map);
            this.i.a(a2);
            this.g.a(this.f, a2);
        }
        this.k.a(this.f != null ? this.f.j() : null);
        C();
    }

    public final void a(@NonNull String str) {
        b(str);
    }

    public void onAdOpened() {
        new StringBuilder("onAdOpened(), clazz = ").append(this);
        super.onAdOpened();
        this.k.a();
    }

    public void onAdClosed() {
        super.onAdClosed();
        new StringBuilder("onAdClosed(), clazz = ").append(getClass());
        this.k.b();
    }

    public synchronized void onAdLeftApplication() {
        super.onAdLeftApplication();
        this.k.c();
    }

    public void e() {
        super.e();
        this.k.e();
    }

    /* access modifiers changed from: protected */
    public final synchronized void C() {
        if (a()) {
            new StringBuilder("trackAdOnDisplayed(), clazz = ").append(this);
            this.g.a();
            if (this.j != null) {
                this.j.b();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean D() {
        return g() || x();
    }

    private boolean a() {
        return this.h.a();
    }

    /* access modifiers changed from: private */
    public boolean g() {
        return !a();
    }
}
