package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class bm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bs f5396a = new bs();

    @Nullable
    public static String a(@NonNull qq qqVar) {
        if (qqVar.b != null) {
            return bs.a(qqVar.b);
        }
        return null;
    }
}
