package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public final class ap implements aq, ii {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f5376a;
    @NonNull
    private final ar b;
    @Nullable
    private List<bg> c;

    public interface a {
        void f();
    }

    public ap(@NonNull a aVar, @NonNull cj cjVar) {
        this.f5376a = aVar;
        this.b = new ar(cjVar);
    }

    public final void a(@NonNull List<bg> list) {
        this.c = list;
        this.b.a();
    }

    public final void a() {
        if (d()) {
            this.f5376a.f();
        }
    }

    public final void b() {
        if (!d()) {
            this.b.b();
        }
    }

    public final void d_() {
        if (!d()) {
            this.b.c();
            this.f5376a.f();
        }
    }

    private boolean d() {
        return this.c != null && !this.c.isEmpty();
    }
}
