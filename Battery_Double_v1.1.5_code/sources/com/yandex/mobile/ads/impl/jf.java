package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.yandex.mobile.ads.impl.hq.b;
import com.yandex.mobile.ads.impl.hq.c;
import com.yandex.mobile.ads.impl.hq.d;

final class jf implements jg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final iz f5577a;

    public final void a(@NonNull Context context, @NonNull o oVar, @NonNull al alVar) {
    }

    jf(@NonNull iz izVar) {
        this.f5577a = izVar;
    }

    public final void a(@NonNull RelativeLayout relativeLayout) {
        if (VERSION.SDK_INT >= 16) {
            relativeLayout.setBackground(b.b);
        } else {
            relativeLayout.setBackgroundDrawable(b.b);
        }
    }

    @NonNull
    public final View a(@NonNull View view, @NonNull x<String> xVar) {
        Context context = view.getContext();
        LayoutParams a2 = d.a();
        RelativeLayout a3 = c.a(context);
        a3.setLayoutParams(a2);
        a3.addView(view, d.a());
        a3.addView(this.f5577a.a(), d.a(context, view));
        return a3;
    }

    public final void a() {
        this.f5577a.b();
    }

    public final void a(boolean z) {
        this.f5577a.a(z);
    }

    public final void b() {
        this.f5577a.c();
    }

    public final boolean c() {
        return this.f5577a.d();
    }
}
