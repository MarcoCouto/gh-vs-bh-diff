package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeGenericAd;
import com.yandex.mobile.ads.nativeads.bd;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.n;
import com.yandex.mobile.ads.nativeads.o;
import com.yandex.mobile.ads.nativeads.s;
import com.yandex.mobile.ads.nativeads.v;
import java.util.List;

final class mf implements lz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ml f5621a = new ml();

    mf() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    public final void a(@NonNull Context context, @NonNull o oVar, @NonNull i iVar, @NonNull s sVar, @NonNull mb mbVar) {
        NativeGenericAd nativeGenericAd;
        List c = oVar.c().c();
        if (c != null && !c.isEmpty()) {
            np npVar = (np) c.get(0);
            if (npVar != null) {
                mk a2 = ml.a(npVar.b());
                if (a2 != null) {
                    bd a3 = sVar.b().a(npVar);
                    Context context2 = context;
                    nativeGenericAd = a2.a(context2, npVar, new v(context, npVar, iVar, a3), iVar, n.a(oVar, n.a(oVar, npVar), a3, sVar));
                    if (nativeGenericAd == null) {
                        mbVar.a(nativeGenericAd);
                        return;
                    } else {
                        mbVar.a(v.f5815a);
                        return;
                    }
                }
            }
        }
        nativeGenericAd = null;
        if (nativeGenericAd == null) {
        }
    }
}
