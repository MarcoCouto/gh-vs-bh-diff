package com.yandex.mobile.ads.impl;

import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;

final class gw implements gy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ha f5524a = new ha();

    gw() {
    }

    @NonNull
    public final String a() {
        String str;
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("com.yandex.mobile.metrica.ads.sdk/");
        StringBuilder sb3 = new StringBuilder("2.110");
        if (sb3.length() - sb3.indexOf(".") < 3) {
            sb3.append("0");
        }
        sb2.append(sb3.toString());
        sb2.append(".23");
        sb.append(sb2.toString());
        StringBuilder sb4 = new StringBuilder("(");
        if (Build.MODEL.startsWith(Build.MANUFACTURER)) {
            str = hg.a(Build.MODEL);
        } else {
            StringBuilder sb5 = new StringBuilder();
            sb5.append(hg.a(Build.MANUFACTURER));
            sb5.append(" ");
            sb5.append(Build.MODEL);
            str = sb5.toString();
        }
        sb4.append(str);
        sb4.append("; Android ");
        sb4.append(VERSION.RELEASE);
        sb4.append(")");
        sb.append(sb4.toString());
        return sb.toString();
    }
}
