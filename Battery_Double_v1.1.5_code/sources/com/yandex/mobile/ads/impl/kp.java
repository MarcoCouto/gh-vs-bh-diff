package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.i;

final class kp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final i f5593a;
    @NonNull
    private final kr b;

    kp(@NonNull i iVar, @NonNull kr krVar) {
        this.f5593a = iVar;
        this.b = krVar;
    }

    @Nullable
    static ki<String> a(@Nullable TextView textView) {
        ld lcVar = textView != null ? new lc(textView) : null;
        if (lcVar != null) {
            return new kj(lcVar);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final ki<nl> a(@Nullable ImageView imageView) {
        ld kxVar = imageView != null ? new kx(imageView, this.f5593a) : null;
        if (kxVar != null) {
            return new kl(kxVar);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final ki<no> a(@Nullable ImageView imageView, @Nullable MediaView mediaView) {
        kx kxVar = imageView != null ? new kx(imageView, this.f5593a) : null;
        ky a2 = mediaView != null ? this.b.a(mediaView, this.f5593a) : null;
        if (kxVar == null && a2 == null) {
            return null;
        }
        return new km(kxVar, a2);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final ki<nk> b(@Nullable TextView textView) {
        ld kwVar = textView != null ? new kw(textView, this.f5593a) : null;
        if (kwVar != null) {
            return new kl(kwVar);
        }
        return null;
    }
}
