package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class hn extends ix {
    public hn(@NonNull Context context, @NonNull x xVar, @NonNull fc fcVar) {
        super(context, xVar, fcVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.e != null) {
            this.e.onAdLoaded();
        }
    }
}
