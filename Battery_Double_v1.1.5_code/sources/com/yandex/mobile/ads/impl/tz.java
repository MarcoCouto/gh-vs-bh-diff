package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.b;

public final class tz implements ub {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final b f5801a;

    public tz(@NonNull VastRequestConfiguration vastRequestConfiguration) {
        this.f5801a = new b(vastRequestConfiguration);
    }

    @NonNull
    public final String a() {
        String c = this.f5801a.c();
        return TextUtils.isEmpty(c) ? "null" : c;
    }

    @NonNull
    public final String b() {
        String b = this.f5801a.b();
        return TextUtils.isEmpty(b) ? "null" : b;
    }
}
