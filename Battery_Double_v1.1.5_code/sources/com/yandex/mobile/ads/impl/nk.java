package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class nk {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final nl f5649a;

    public nk(@Nullable nl nlVar) {
        this.f5649a = nlVar;
    }

    @Nullable
    public final nl a() {
        return this.f5649a;
    }
}
