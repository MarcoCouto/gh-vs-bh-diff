package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class ff {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5483a = new Object();
    @Nullable
    private static volatile ff b;
    @Nullable
    private fg c;
    @NonNull
    private rr d = new rt();
    @Nullable
    private Boolean e;
    private boolean f = true;
    private boolean g;

    public static ff a() {
        if (b == null) {
            synchronized (f5483a) {
                if (b == null) {
                    b = new ff();
                }
            }
        }
        return b;
    }

    private ff() {
    }

    @Nullable
    public final fg a(@NonNull Context context) {
        fg fgVar;
        synchronized (f5483a) {
            if (this.c == null) {
                this.c = hc.b(context);
            }
            fgVar = this.c;
        }
        return fgVar;
    }

    public final void a(@NonNull Context context, @NonNull fg fgVar) {
        synchronized (f5483a) {
            this.c = fgVar;
            hc.a(context, fgVar);
        }
    }

    public final boolean b() {
        boolean z;
        synchronized (f5483a) {
            z = this.f;
        }
        return z;
    }

    @NonNull
    public final synchronized rr c() {
        rr rrVar;
        synchronized (f5483a) {
            rrVar = this.d;
        }
        return rrVar;
    }

    public final void a(boolean z) {
        synchronized (f5483a) {
            this.g = z;
        }
    }

    public final boolean d() {
        boolean z;
        synchronized (f5483a) {
            z = this.g;
        }
        return z;
    }

    public final void b(boolean z) {
        synchronized (f5483a) {
            this.e = Boolean.valueOf(z);
        }
    }

    @Nullable
    public final Boolean e() {
        Boolean bool;
        synchronized (f5483a) {
            bool = this.e;
        }
        return bool;
    }
}
