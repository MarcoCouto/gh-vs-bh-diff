package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.lang.ref.WeakReference;

public class ih extends hw {

    /* renamed from: a reason: collision with root package name */
    final eo f5547a;

    static class a implements er {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final WeakReference<ec> f5548a;

        a(@NonNull ec ecVar) {
            this.f5548a = new WeakReference<>(ecVar);
        }

        public final void a() {
            ec ecVar = (ec) this.f5548a.get();
            if (ecVar != null) {
                ecVar.onAdLoaded();
            }
        }

        public final void a(@NonNull String str) {
            ec ecVar = (ec) this.f5548a.get();
            if (ecVar != null) {
                ecVar.a(str);
            }
        }
    }

    public ih(@NonNull eg egVar, @NonNull ec ecVar) {
        super(egVar);
        this.f5547a = new eo(egVar);
        this.f5547a.a((er) new a(ecVar));
    }

    public final void a(@NonNull String str) {
        this.f5547a.a(str);
    }

    public final void a(@NonNull ec ecVar) {
        super.a((ec) new ig(this.f5547a, ecVar));
    }

    public final void b() {
        super.b();
        this.f5547a.c();
    }
}
