package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.sdk.constants.Constants.RequestParameters;

public final class gc {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5506a;
    @NonNull
    private final he b = new he();
    @NonNull
    private final fv c = new fv();

    public gc(@NonNull Context context) {
        this.f5506a = context.getApplicationContext();
    }

    @Nullable
    public final fu a() {
        try {
            Class a2 = he.a("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            if (a2 == null) {
                return null;
            }
            Object a3 = he.a(a2, "getAdvertisingIdInfo", this.f5506a);
            return fv.a((String) he.a(a3, "getId", new Object[0]), (Boolean) he.a(a3, RequestParameters.isLAT, new Object[0]));
        } catch (Throwable unused) {
            return null;
        }
    }
}
