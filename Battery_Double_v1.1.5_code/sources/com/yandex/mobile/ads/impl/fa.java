package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import org.json.JSONObject;

public enum fa implements ey {
    DEFAULT("default"),
    LOADING("loading"),
    HIDDEN("hidden");
    
    private final String d;

    private fa(String str) {
        this.d = str;
    }

    @NonNull
    public final String a() {
        return String.format("state: %s", new Object[]{JSONObject.quote(this.d)});
    }
}
