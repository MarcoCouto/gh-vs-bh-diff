package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public final class dd implements de {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cg f5436a;
    @NonNull
    private final db b;
    @NonNull
    private final Context c;
    private final int d;

    public dd(@NonNull Context context, @NonNull x xVar, @NonNull cg cgVar, @NonNull db dbVar) {
        this.f5436a = cgVar;
        this.b = dbVar;
        this.c = context.getApplicationContext();
        this.d = xVar.n();
    }

    public final void a(@NonNull String str) {
        this.b.a(this.f5436a, b(str));
    }

    @VisibleForTesting
    private String b(@NonNull String str) {
        HttpURLConnection httpURLConnection = null;
        String str2 = str;
        int i = 0;
        while (true) {
            if (i >= this.d) {
                break;
            }
            boolean z = true;
            try {
                if (dt.a(str2)) {
                    break;
                } else if (!dt.c(str2)) {
                    break;
                } else {
                    Context context = this.c;
                    int i2 = gq.f5518a;
                    HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str2).openConnection();
                    new hb();
                    httpURLConnection2.setRequestProperty("User-Agent", hb.a(context));
                    if (i2 > 0) {
                        httpURLConnection2.setConnectTimeout(i2);
                        httpURLConnection2.setReadTimeout(i2);
                    }
                    if (hc.a(21) && (httpURLConnection2 instanceof HttpsURLConnection)) {
                        SSLSocketFactory a2 = rw.a();
                        if (a2 != null) {
                            ((HttpsURLConnection) httpURLConnection2).setSSLSocketFactory(a2);
                        }
                    }
                    httpURLConnection2.setInstanceFollowRedirects(false);
                    try {
                        int responseCode = httpURLConnection2.getResponseCode();
                        Object[] objArr = {str2, Integer.valueOf(responseCode)};
                        String headerField = httpURLConnection2.getHeaderField(qj.LOCATION.a());
                        new Object[1][0] = headerField;
                        if (responseCode < 300 || responseCode >= 400 || TextUtils.isEmpty(headerField)) {
                            z = false;
                        }
                        if (!z) {
                            di.a(httpURLConnection2);
                            break;
                        }
                        di.a(httpURLConnection2);
                        str2 = headerField;
                        httpURLConnection = httpURLConnection2;
                        i++;
                    } catch (Exception e) {
                        e = e;
                        httpURLConnection = httpURLConnection2;
                        try {
                            Object[] objArr2 = {str2, e.toString()};
                            di.a(httpURLConnection);
                            i++;
                        } catch (Throwable th) {
                            th = th;
                            di.a(httpURLConnection);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        httpURLConnection = httpURLConnection2;
                        di.a(httpURLConnection);
                        throw th;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                Object[] objArr22 = {str2, e.toString()};
                di.a(httpURLConnection);
                i++;
            }
        }
        di.a(httpURLConnection);
        return str2;
    }
}
