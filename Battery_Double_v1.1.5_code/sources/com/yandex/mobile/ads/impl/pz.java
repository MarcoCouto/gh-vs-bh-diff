package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import java.util.List;

public final class pz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5705a;
    @NonNull
    private final PackageManager b;

    public pz(@NonNull Context context) {
        this.f5705a = context.getPackageName();
        this.b = context.getPackageManager();
    }

    public final boolean a(@NonNull String str) {
        try {
            return this.b.checkPermission(str, this.f5705a) == 0;
        } catch (Exception unused) {
            return false;
        }
    }

    public final boolean a(@NonNull Intent intent) {
        List queryIntentActivities = this.b.queryIntentActivities(intent, 0);
        if (queryIntentActivities == null || queryIntentActivities.isEmpty()) {
            return false;
        }
        return true;
    }
}
