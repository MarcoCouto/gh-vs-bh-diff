package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;

public final class ei {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final eg f5459a;
    @NonNull
    private final iy b = new iy();

    public ei(@NonNull eg egVar) {
        this.f5459a = egVar;
    }

    public final void a(@Nullable final Map<String, String> map) {
        this.b.a(new Runnable() {
            public final void run() {
                ei.this.f5459a.setVisibility(0);
                ei.a(ei.this, map);
            }
        });
    }

    static /* synthetic */ void a(ei eiVar, Map map) {
        ec ecVar = eiVar.f5459a.e;
        if (ecVar != null) {
            ecVar.onAdLoaded();
            ecVar.a(eiVar.f5459a, map);
        }
    }
}
