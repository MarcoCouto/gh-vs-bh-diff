package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ns;

public interface nb<T extends ns> {
    void a(@NonNull Context context, @NonNull T t);
}
