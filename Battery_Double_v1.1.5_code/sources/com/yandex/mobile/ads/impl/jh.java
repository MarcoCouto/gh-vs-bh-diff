package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

public final class jh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ja f5578a;

    public jh(@NonNull ja jaVar) {
        this.f5578a = jaVar;
    }

    @NonNull
    public static jg a(@NonNull x xVar, @NonNull View view, boolean z, boolean z2) {
        Context context = view.getContext();
        iz a2 = ja.a(xVar, view, z);
        if (z2) {
            return new jf(a2);
        }
        return new je(context, a2);
    }
}
