package com.yandex.mobile.ads.impl;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import java.io.ByteArrayInputStream;
import java.util.Locale;

final class es extends ea {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private String f5473a;

    es(@NonNull eb ebVar) {
        super(ebVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str) {
        this.f5473a = str;
    }

    public final WebResourceResponse shouldInterceptRequest(@NonNull WebView webView, @NonNull String str) {
        if (VERSION.SDK_INT < 11 || VERSION.SDK_INT >= 21 || TextUtils.isEmpty(str) || TextUtils.isEmpty(this.f5473a) || !a(Uri.parse(str.toLowerCase(Locale.US)))) {
            return super.shouldInterceptRequest(webView, str);
        }
        return b(this.f5473a);
    }

    @TargetApi(21)
    public final WebResourceResponse shouldInterceptRequest(@NonNull WebView webView, @NonNull WebResourceRequest webResourceRequest) {
        if (!TextUtils.isEmpty(this.f5473a)) {
            Uri url = webResourceRequest.getUrl();
            if (url != null && a(url)) {
                return b(this.f5473a);
            }
        }
        return super.shouldInterceptRequest(webView, webResourceRequest);
    }

    private static boolean a(@NonNull Uri uri) {
        return "mraid.js".equals(uri.getLastPathSegment());
    }

    @RequiresApi(api = 11)
    private static WebResourceResponse b(@NonNull String str) {
        return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(str.getBytes()));
    }
}
