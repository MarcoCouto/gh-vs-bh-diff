package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.mediation.base.a;
import java.util.HashMap;
import java.util.List;

public final class ay<T extends a> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final be f5382a;
    @NonNull
    private final at b;
    @NonNull
    private final bb c;
    private int d;

    public ay(@NonNull be beVar, @NonNull at atVar, @NonNull bb bbVar) {
        this.f5382a = beVar;
        this.b = atVar;
        this.c = bbVar;
    }

    @Nullable
    public final au<T> a(@NonNull Context context, @NonNull Class<T> cls) {
        List a2 = this.f5382a.a();
        au<T> auVar = null;
        while (auVar == null && this.d < a2.size()) {
            int i = this.d;
            this.d = i + 1;
            bf bfVar = (bf) a2.get(i);
            try {
                a aVar = (a) cls.cast(he.a(Class.forName(bfVar.a()), new Object[0]));
                if (aVar == null) {
                    a(context, bfVar, "could_not_create_adapter");
                } else {
                    auVar = new au<>(aVar, bfVar, this.b);
                }
            } catch (ClassCastException unused) {
                a(context, bfVar, "does_not_conform_to_protocol");
            } catch (Exception unused2) {
                a(context, bfVar, "could_not_create_adapter");
            }
        }
        return auVar;
    }

    private void a(@NonNull Context context, @NonNull bf bfVar, @NonNull String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, str);
        this.c.f(context, bfVar, hashMap);
    }
}
