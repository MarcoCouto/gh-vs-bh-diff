package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ru.a;

public abstract class rv<R, T> extends ru<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final R f5747a;
    @NonNull
    private final gv<R, T> b;
    @NonNull
    private final gs c;

    /* access modifiers changed from: protected */
    public abstract qt<T> a(@NonNull qq qqVar, int i);

    public rv(@NonNull Context context, int i, @NonNull String str, @NonNull a<T> aVar, @NonNull R r, @NonNull gv<R, T> gvVar) {
        super(i, str, aVar);
        this.f5747a = r;
        this.b = gvVar;
        this.c = gs.a(context);
        this.c.a(this.b.a(this.f5747a));
    }

    /* access modifiers changed from: protected */
    public final qt<T> a(@NonNull qq qqVar) {
        int i = qqVar.f5720a;
        qt<T> a2 = a(qqVar, i);
        a(a2, i);
        return a2;
    }

    /* access modifiers changed from: protected */
    public re a(re reVar) {
        a(null, reVar.f5728a != null ? reVar.f5728a.f5720a : -1);
        return super.a(reVar);
    }

    private void a(@Nullable qt<T> qtVar, int i) {
        this.c.a(this.b.a(qtVar, i, this.f5747a));
    }
}
