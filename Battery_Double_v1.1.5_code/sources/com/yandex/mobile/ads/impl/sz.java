package com.yandex.mobile.ads.impl;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.Map;
import java.util.Map.Entry;

public final class sz {
    public static void a(@NonNull Builder builder, @NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(str, str2);
        }
    }

    public static void a(@NonNull Builder builder, @Nullable Map<String, String> map) {
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String str = (String) entry.getKey();
                String str2 = (String) entry.getValue();
                if (!TextUtils.isEmpty(str)) {
                    a(builder, str, str2);
                }
            }
        }
    }
}
