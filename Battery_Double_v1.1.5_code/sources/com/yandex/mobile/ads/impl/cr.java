package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.MobileAds;

public final class cr implements cq<fg> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5423a;
    @NonNull
    private final cp b = new cp();
    @NonNull
    private final cs c = new cs();
    @NonNull
    private final cu d = new cu();
    @NonNull
    private final bv<fg> e = new bx();

    public cr(@NonNull Context context) {
        this.f5423a = context.getApplicationContext();
    }

    public final boolean a() {
        fg a2 = ff.a().a(this.f5423a);
        if (a2 != null) {
            if (!(System.currentTimeMillis() >= a2.a()) && !(!MobileAds.getLibraryVersion().equals(a2.j()))) {
                if (ff.a().e() != a2.d()) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    @Nullable
    public final /* synthetic */ Object a(@NonNull qq qqVar) {
        return (fg) this.e.b(qqVar);
    }
}
