package com.yandex.mobile.ads.impl;

public final class s extends re {
    private static final long serialVersionUID = 9076708591501334094L;
    private final int b;

    public s(qq qqVar, int i) {
        super(qqVar);
        this.b = i;
    }

    s() {
        this.b = 9;
    }

    public final int a() {
        return this.b;
    }

    public static s a(qq qqVar) {
        s sVar;
        int i = qqVar != null ? qqVar.f5720a : -1;
        if (204 == i) {
            sVar = new s(qqVar, 6);
        } else if (403 == i) {
            sVar = new s(qqVar, 10);
        } else if (404 == i) {
            sVar = new s(qqVar, 4);
        } else {
            if (500 <= i && i < 600) {
                sVar = new s(qqVar, 9);
            } else if (-1 == i) {
                sVar = new s(qqVar, 7);
            } else {
                sVar = new s(qqVar, 8);
            }
        }
        new Object[1][0] = Integer.valueOf(i);
        return sVar;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.b == ((s) obj).b;
    }

    public final int hashCode() {
        return this.b;
    }
}
