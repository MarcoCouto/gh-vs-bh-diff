package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

final class cb {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ca f5412a;
    /* access modifiers changed from: private */
    @NonNull
    public final b b;
    @NonNull
    private final Handler c = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public boolean d;

    private final class a implements Runnable {
        private a() {
        }

        /* synthetic */ a(cb cbVar, byte b) {
            this();
        }

        public final void run() {
            if (!cb.this.d) {
                if (cb.this.f5412a.a()) {
                    cb.this.d = true;
                    cb.this.b.a();
                    return;
                }
                cb.this.c();
            }
        }
    }

    public interface b {
        void a();
    }

    cb(@NonNull ca caVar, @NonNull b bVar) {
        this.f5412a = caVar;
        this.b = bVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        c();
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.c.removeCallbacksAndMessages(null);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.c.postDelayed(new a(this, 0), 300);
    }
}
