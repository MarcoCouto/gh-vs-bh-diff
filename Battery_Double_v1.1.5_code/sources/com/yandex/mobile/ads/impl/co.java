package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.impl.dh.a;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;
import java.util.Map;

public abstract class co<T> implements gv<fc, x<T>> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cn f5421a = new cn();
    @NonNull
    private final cm b = new cm();

    public final /* synthetic */ gu a(@Nullable qt qtVar, int i, @NonNull Object obj) {
        return new gu(b.RESPONSE, a((fc) obj, qtVar, i));
    }

    public final /* synthetic */ gu a(@NonNull Object obj) {
        return new gu(b.REQUEST, a((fc) obj));
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Map<String, Object> a(@NonNull fc fcVar) {
        HashMap hashMap = new HashMap();
        a(hashMap, fcVar);
        hashMap.put("block_id", fcVar.e());
        hashMap.put("ad_type", fcVar.a().a());
        hashMap.put("is_passback", Boolean.valueOf(fcVar.q() == a.b));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Map<String, Object> a(@NonNull fc fcVar, @Nullable qt<x<T>> qtVar, int i) {
        HashMap hashMap = new HashMap();
        dr drVar = new dr(new HashMap());
        drVar.a("block_id", fcVar.e());
        drVar.a("ad_type", fcVar.a().a());
        if (!(qtVar == null || qtVar.f5724a == null)) {
            drVar.a("ad_type_format", ((x) qtVar.f5724a).c());
            drVar.a("product_type", ((x) qtVar.f5724a).d());
        }
        drVar.a(i == -1 ? NativeProtocol.BRIDGE_ARG_ERROR_CODE : "code", Integer.valueOf(i));
        String str = "empty";
        if (!(qtVar == null || qtVar.f5724a == null)) {
            if (((x) qtVar.f5724a).s() != null) {
                str = "mediation";
            } else if (((x) qtVar.f5724a).t() != null) {
                str = "ad";
            }
        }
        drVar.a(ServerProtocol.DIALOG_PARAM_RESPONSE_TYPE, str);
        hashMap.putAll(drVar.a());
        a(hashMap, fcVar);
        return hashMap;
    }

    private static void a(@NonNull Map<String, Object> map, @NonNull fc fcVar) {
        AdRequest c = fcVar.c();
        if (c != null) {
            map.putAll(cn.a(c));
        }
    }
}
