package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import java.util.List;

public class np {

    /* renamed from: a reason: collision with root package name */
    private nm f5654a;
    private NativeAdType b;
    private List<ni> c;
    private bg d;
    @Nullable
    private String e;
    private String f;
    private ny g;
    private ny h;

    public final nm a() {
        return this.f5654a;
    }

    public final NativeAdType b() {
        return this.b;
    }

    public final List<ni> c() {
        return this.c;
    }

    public final void a(List<ni> list) {
        this.c = list;
    }

    public final void a(@Nullable nm nmVar) {
        if (nmVar != null) {
            this.f5654a = nmVar;
        }
    }

    @Nullable
    public final ni b(@NonNull String str) {
        if (this.c != null) {
            for (ni niVar : this.c) {
                if (niVar.a().equals(str)) {
                    return niVar;
                }
            }
        }
        return null;
    }

    public final void a(bg bgVar) {
        this.d = bgVar;
    }

    public final bg d() {
        return this.d;
    }

    public final void c(@Nullable String str) {
        this.e = str;
    }

    @Nullable
    public final String e() {
        return this.e;
    }

    public final void d(String str) {
        this.f = str;
    }

    public final void a(ny nyVar) {
        this.g = nyVar;
    }

    public final void b(ny nyVar) {
        this.h = nyVar;
    }

    public final String f() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        np npVar = (np) obj;
        if (this.f5654a == null ? npVar.f5654a != null : !this.f5654a.equals(npVar.f5654a)) {
            return false;
        }
        if (this.b != npVar.b) {
            return false;
        }
        if (this.c == null ? npVar.c != null : !this.c.equals(npVar.c)) {
            return false;
        }
        if (this.d == null ? npVar.d != null : !this.d.equals(npVar.d)) {
            return false;
        }
        if (this.e == null ? npVar.e != null : !this.e.equals(npVar.e)) {
            return false;
        }
        if (this.f == null ? npVar.f != null : !this.f.equals(npVar.f)) {
            return false;
        }
        if (this.g == null ? npVar.g != null : !this.g.equals(npVar.g)) {
            return false;
        }
        if (this.h != null) {
            return this.h.equals(npVar.h);
        }
        return npVar.h == null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((this.f5654a != null ? this.f5654a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.g != null ? this.g.hashCode() : 0)) * 31;
        if (this.h != null) {
            i = this.h.hashCode();
        }
        return hashCode + i;
    }

    public final void a(String str) {
        NativeAdType nativeAdType;
        NativeAdType[] values = NativeAdType.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                nativeAdType = null;
                break;
            }
            nativeAdType = values[i];
            if (nativeAdType.getValue().equals(str)) {
                break;
            }
            i++;
        }
        this.b = nativeAdType;
    }
}
