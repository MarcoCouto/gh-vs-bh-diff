package com.yandex.mobile.ads.impl;

import android.net.TrafficStats;
import android.os.Build.VERSION;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;

public final class qp extends Thread {

    /* renamed from: a reason: collision with root package name */
    private final BlockingQueue<qr<?>> f5719a;
    private final qo b;
    private final qk c;
    private final qu d;
    private volatile boolean e = false;

    public qp(BlockingQueue<qr<?>> blockingQueue, qo qoVar, qk qkVar, qu quVar) {
        this.f5719a = blockingQueue;
        this.b = qoVar;
        this.c = qkVar;
        this.d = quVar;
    }

    public final void a() {
        this.e = true;
        interrupt();
    }

    public final void run() {
        Process.setThreadPriority(10);
        while (true) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                qr qrVar = (qr) this.f5719a.take();
                try {
                    if (qrVar.j()) {
                        qrVar.g();
                    } else {
                        if (VERSION.SDK_INT >= 14) {
                            TrafficStats.setThreadStatsTag(qrVar.f());
                        }
                        qq a2 = this.b.a(qrVar);
                        if (!a2.d || !qrVar.s()) {
                            qt a3 = qrVar.a(a2);
                            if (qrVar.m() && a3.b != null) {
                                this.c.a(qrVar.b(), a3.b);
                            }
                            qrVar.r();
                            this.d.a(qrVar, a3);
                        } else {
                            qrVar.g();
                        }
                    }
                } catch (re e2) {
                    e2.a(SystemClock.elapsedRealtime() - elapsedRealtime);
                    this.d.a(qrVar, qrVar.a(e2));
                } catch (Exception e3) {
                    qw.a(e3, "Unhandled exception %s", e3.toString());
                    re reVar = new re((Throwable) e3);
                    reVar.a(SystemClock.elapsedRealtime() - elapsedRealtime);
                    this.d.a(qrVar, reVar);
                }
            } catch (InterruptedException unused) {
                if (this.e) {
                    return;
                }
            }
        }
    }
}
