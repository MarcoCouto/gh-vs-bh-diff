package com.yandex.mobile.ads.impl;

public enum ae {
    AD("ad"),
    AD_UNIT("ad_unit");
    
    private final String c;

    private ae(String str) {
        this.c = str;
    }

    public final String a() {
        return this.c;
    }
}
