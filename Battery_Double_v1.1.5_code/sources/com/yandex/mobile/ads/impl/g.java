package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

final class g {

    static final class a implements f {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final a f5503a;

        public final void a(@NonNull Context context, @NonNull View view) {
        }

        a(@NonNull a aVar) {
            this.f5503a = aVar;
        }

        public final void a() {
            if (!di.a((ac) this.f5503a)) {
                this.f5503a.u();
            }
        }

        public final void b() {
            if (!di.a((ac) this.f5503a)) {
                this.f5503a.v();
            }
        }

        public final void a(int i) {
            this.f5503a.b(i);
        }
    }

    static final class b implements f {
        public final void a() {
        }

        public final void a(int i) {
        }

        public final void b() {
        }

        b() {
        }

        public final void a(@NonNull Context context, @NonNull View view) {
            view.setVisibility(0);
            view.setMinimumHeight(dv.a(context, 50.0f));
        }
    }
}
