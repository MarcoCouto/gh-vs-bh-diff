package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.gu.a;
import java.util.HashMap;

public final class cl {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final x f5420a;
    @NonNull
    private final gs b;
    @Nullable
    private a c;

    public cl(@NonNull Context context, @NonNull x xVar) {
        this.f5420a = xVar;
        this.b = gs.a(context);
    }

    public final void a(@Nullable a aVar) {
        this.c = aVar;
    }

    public final void a() {
        dr drVar = new dr(new HashMap());
        drVar.a("adapter", "Yandex");
        drVar.a("block_id", this.f5420a.b());
        drVar.a("ad_type_format", this.f5420a.c());
        drVar.a("product_type", this.f5420a.d());
        b a2 = this.f5420a.a();
        drVar.a("ad_type", a2 != null ? a2.a() : null);
        if (this.c != null) {
            drVar.a(this.c.a());
        }
        this.b.a(new gu(gu.b.RENDERING_START, drVar.a()));
    }
}
