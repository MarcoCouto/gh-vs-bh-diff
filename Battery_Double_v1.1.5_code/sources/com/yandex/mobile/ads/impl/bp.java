package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.ru.a;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public abstract class bp<T> extends rv<fc, x<T>> {

    /* renamed from: a reason: collision with root package name */
    private final String f5398a;
    private final Context b;
    private final fc c;
    @NonNull
    private final bu d = new bu();
    @NonNull
    private final bw e = new bw();
    @NonNull
    private final bt f = new bt();
    @NonNull
    private final ds g;

    protected static boolean b(int i) {
        return 204 == i;
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public boolean a(int i) {
        return 200 == i;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public abstract T a_(@NonNull qq qqVar);

    public bp(@NonNull Context context, @NonNull fc fcVar, @NonNull String str, @NonNull String str2, @NonNull a<x<T>> aVar, @NonNull gv<fc, x<T>> gvVar) {
        super(context, fcVar.k(), str, aVar, fcVar, gvVar);
        Object[] objArr = {str, str2};
        a((qv) new qm(fcVar.l(), 0, 1.0f));
        this.f5398a = str2;
        this.c = fcVar;
        this.b = context.getApplicationContext();
        this.g = new ds(context);
    }

    public final Map<String, String> a() throws qx {
        HashMap hashMap = new HashMap();
        String a2 = di.a(this.b);
        if (a2 != null) {
            new Object[1][0] = a2;
            hashMap.put(qj.YMAD_SESSION_DATA.a(), a2);
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final qt<x<T>> a(@NonNull qq qqVar, int i) {
        if (a(i)) {
            Map<String, String> map = qqVar.c;
            b a2 = b.a(bl.a(map, qj.YMAD_TYPE));
            if (a2 == this.c.a()) {
                x.a aVar = new x.a();
                aVar.c(this.c.e());
                aVar.a(a2);
                int c2 = bl.c(map, qj.YMAD_HEADER_WIDTH);
                int c3 = bl.c(map, qj.YMAD_HEADER_HEIGHT);
                aVar.a(c2);
                aVar.b(c3);
                String a3 = bl.a(map, qj.YMAD_TYPE_FORMAT);
                String a4 = bl.a(map, qj.YMAD_PRODUCT_TYPE);
                aVar.a(a3);
                aVar.b(a4);
                al b2 = this.c.b();
                cz czVar = null;
                aVar.a(b2 != null ? b2.c() : null);
                aVar.a(bl.e(map, qj.YMAD_SHOW_NOTICE));
                aVar.c(bl.a(map, qj.YMAD_NOTICE_DELAY, new bl.a<Long>() {
                    @Nullable
                    public final /* synthetic */ Object a(String str) {
                        return di.a(str, Long.valueOf(0));
                    }
                }));
                aVar.d(bl.a(map, qj.YMAD_VISIBILITY_PERCENT, new bl.a<Integer>() {
                    @NonNull
                    public final /* synthetic */ Object a(String str) {
                        return Integer.valueOf(Math.min(di.b(str), x.f5819a.intValue()));
                    }
                }));
                aVar.b(bl.e(map, qj.YMAD_RENDER_TRACKING_URLS));
                aVar.f(bl.c(map, qj.YMAD_PREFETCH_COUNT));
                aVar.c(bl.c(map, qj.YMAD_REFRESH_PERIOD));
                aVar.d(bl.c(map, qj.YMAD_RELOAD_TIMEOUT));
                aVar.e(bl.c(map, qj.YMAD_EMPTY_INTERVAL));
                aVar.d(bl.a(map, qj.YMAD_RENDERER));
                Map<String, String> map2 = qqVar.c;
                Integer c4 = di.c(bl.a(map2, qj.YMAD_REWARD_AMOUNT));
                String a5 = bl.a(map2, qj.YMAD_REWARD_TYPE);
                String a6 = a5 != null ? bs.a(a5.getBytes()) : null;
                bh bhVar = (c4 == null || TextUtils.isEmpty(a6)) ? null : new bh(c4.intValue(), a6);
                String d2 = bl.d(map2, qj.YMAD_REWARD_URL);
                aVar.a(new bi.a().a(bhVar).a(!TextUtils.isEmpty(d2) ? new bj(d2) : null).a(bl.b(map2, qj.YMAD_SERVER_SIDE_REWARD)).a());
                Map<String, String> map3 = qqVar.c;
                String d3 = bl.d(map3, qj.YMAD_FALSE_CLICK_URL);
                Long a7 = di.a(bl.a(map3, qj.YMAD_FALSE_CLICK_INTERVAL), (Long) null);
                if (!(d3 == null || a7 == null)) {
                    czVar = new cz(d3, a7.longValue());
                }
                aVar.a(czVar);
                String a8 = bl.a(map, qj.YMAD_SESSION_DATA);
                Object[] objArr = {qj.YMAD_SESSION_DATA.a(), a8};
                di.a(this.b, a8);
                aVar.a(bl.b(map, qj.YMAD_ROTATION_ENABLED));
                aVar.b(bl.b(map, qj.YMAD_NON_SKIPPABLE_AD_ENABLED));
                if (bl.b(map, qj.YMAD_MEDIATION)) {
                    aVar.a(bu.a(qqVar));
                } else {
                    aVar.a(a_(qqVar));
                }
                x a9 = aVar.a();
                if (!b(i)) {
                    return qt.a(a9, ri.a(qqVar));
                }
            }
        }
        return qt.a(s.a(qqVar));
    }

    /* access modifiers changed from: protected */
    public final re a(re reVar) {
        return super.a((re) s.a(reVar.f5728a));
    }

    public final String b() {
        String b2 = super.b();
        if (d() == 0) {
            b2 = Uri.parse(b2).buildUpon().encodedQuery(this.f5398a).build().toString();
        }
        return this.g.a(b2);
    }

    public final byte[] c() throws qx {
        byte[] c2 = super.c();
        if (1 != d()) {
            return c2;
        }
        try {
            if (this.f5398a != null) {
                return this.f5398a.getBytes("UTF-8");
            }
            return c2;
        } catch (UnsupportedEncodingException unused) {
            Object[] objArr = {this.f5398a, "UTF-8"};
            return c2;
        }
    }
}
