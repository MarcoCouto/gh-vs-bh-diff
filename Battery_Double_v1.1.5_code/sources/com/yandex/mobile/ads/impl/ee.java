package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.AdRequestError;
import java.util.Collections;
import java.util.Map;

public final class ee implements ec {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final eg f5455a;
    @NonNull
    private final ap b;
    @NonNull
    private final dx c = new dx();
    @NonNull
    private final ei d;
    @NonNull
    private final hz e = new hz();
    @Nullable
    private lt f;

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
    }

    public final void b(boolean z) {
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
    }

    public final void onAdLoaded() {
    }

    ee(@NonNull ed edVar, @NonNull ap apVar) {
        this.f5455a = edVar;
        this.b = apVar;
        this.d = new ei(edVar);
    }

    /* access modifiers changed from: 0000 */
    public final void b(@NonNull String str) {
        boolean a2 = hz.a(str);
        ic.a();
        ic.a(a2).a(this.f5455a, this, this.c, this.b).a(str);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull lt ltVar) {
        this.f = ltVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.d.a(Collections.emptyMap());
    }

    public final void a(@NonNull String str) {
        if (this.f != null) {
            this.f.a(this.f5455a, str);
        }
    }
}
