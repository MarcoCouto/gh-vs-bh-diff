package com.yandex.mobile.ads.impl;

public final class sl {

    /* renamed from: a reason: collision with root package name */
    private final boolean f5771a;
    private final boolean b;

    public sl(boolean z, boolean z2) {
        this.f5771a = z;
        this.b = z2;
    }

    public final boolean a() {
        return this.f5771a;
    }

    public final boolean b() {
        return this.b;
    }
}
