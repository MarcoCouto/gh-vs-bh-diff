package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class bn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private static final Object f5397a = new Object();
    private static volatile qs b;

    @NonNull
    public static qs a(@NonNull Context context) {
        if (b == null) {
            synchronized (f5397a) {
                if (b == null) {
                    qs a2 = bo.a(context);
                    b = a2;
                    a2.a();
                }
            }
        }
        return b;
    }
}
