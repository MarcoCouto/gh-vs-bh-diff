package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.yandex.mobile.ads.video.models.vmap.AdSource;
import com.yandex.mobile.ads.video.models.vmap.c;
import com.yandex.mobile.ads.video.models.vmap.d;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

final class tp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tn f5794a;
    @NonNull
    private final tq b;

    public tp(@NonNull tn tnVar) {
        this.f5794a = tnVar;
        this.b = new tq(tnVar);
    }

    @Nullable
    static AdSource a(@NonNull XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        c cVar;
        tn.a(xmlPullParser, "AdSource");
        Boolean b2 = tn.b(xmlPullParser, VastAttributes.ALLOW_MULTIPLE_ADS);
        Boolean b3 = tn.b(xmlPullParser, "followRedirects");
        String attributeValue = xmlPullParser.getAttributeValue(null, "id");
        AdSource adSource = null;
        while (tn.b(xmlPullParser)) {
            if (tn.a(xmlPullParser)) {
                if ("AdTagURI".equals(xmlPullParser.getName())) {
                    tn.a(xmlPullParser, "AdTagURI");
                    String attributeValue2 = xmlPullParser.getAttributeValue(null, "templateType");
                    String c = tn.c(xmlPullParser);
                    if (!TextUtils.isEmpty(c)) {
                        cVar = d.a(c, attributeValue2);
                    } else {
                        cVar = null;
                    }
                    if (cVar != null) {
                        adSource = d.a(cVar, b2, b3, attributeValue);
                    }
                } else {
                    tn.d(xmlPullParser);
                }
            }
        }
        return adSource;
    }
}
