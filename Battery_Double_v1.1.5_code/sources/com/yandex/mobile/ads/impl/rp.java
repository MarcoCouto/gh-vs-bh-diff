package com.yandex.mobile.ads.impl;

import com.yandex.mobile.ads.impl.qt.a;
import com.yandex.mobile.ads.impl.qt.b;
import java.io.UnsupportedEncodingException;

public final class rp extends qr<String> {

    /* renamed from: a reason: collision with root package name */
    private final b<String> f5743a;

    /* access modifiers changed from: protected */
    public final /* synthetic */ void b(Object obj) {
        this.f5743a.a((String) obj);
    }

    public rp(String str, b<String> bVar, a aVar) {
        super(0, str, aVar);
        this.f5743a = bVar;
    }

    /* access modifiers changed from: protected */
    public final qt<String> a(qq qqVar) {
        String str;
        try {
            str = new String(qqVar.b, ri.a(qqVar.c));
        } catch (UnsupportedEncodingException unused) {
            str = new String(qqVar.b);
        }
        return qt.a(str, ri.a(qqVar));
    }
}
