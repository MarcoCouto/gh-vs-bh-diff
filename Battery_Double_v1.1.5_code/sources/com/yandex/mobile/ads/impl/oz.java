package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.at.a;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public final class oz implements oy<nj> {
    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, y {
        if (!jSONObject.has("value") || !jSONObject.isNull("value")) {
            return new nj(a.TEXT, oh.a(jSONObject, "value"));
        }
        return new nj(a.IMAGE, null);
    }
}
