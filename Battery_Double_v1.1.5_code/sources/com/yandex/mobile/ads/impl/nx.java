package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;

public final class nx {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5662a;
    @NonNull
    private final String b;
    @Nullable
    private final Map<String, Object> c;

    public nx(@NonNull String str, @NonNull String str2, @Nullable Map<String, Object> map) {
        this.f5662a = str;
        this.b = str2;
        this.c = map;
    }

    @NonNull
    public final String a() {
        return this.f5662a;
    }

    @NonNull
    public final String b() {
        return this.b;
    }

    @Nullable
    public final Map<String, Object> c() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nx nxVar = (nx) obj;
        if (!this.f5662a.equals(nxVar.f5662a) || !this.b.equals(nxVar.b)) {
            return false;
        }
        if (this.c != null) {
            return this.c.equals(nxVar.c);
        }
        return nxVar.c == null;
    }

    public final int hashCode() {
        return (((this.f5662a.hashCode() * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0);
    }
}
