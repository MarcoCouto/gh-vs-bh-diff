package com.yandex.mobile.ads.impl;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.PopupMenu.OnMenuItemClickListener;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.impl.nu.a;
import com.yandex.mobile.ads.nativeads.q;
import java.util.List;

@TargetApi(11)
final class pr implements OnMenuItemClickListener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5698a;
    @NonNull
    private final cg b;
    @NonNull
    private final cv c;
    @NonNull
    private final List<a> d;
    @NonNull
    private final q e;

    pr(@NonNull Context context, @NonNull cv cvVar, @NonNull List<a> list, @NonNull cg cgVar, @NonNull q qVar) {
        this.d = list;
        this.c = cvVar;
        this.b = cgVar;
        this.f5698a = context.getApplicationContext();
        this.e = qVar;
    }

    public final boolean onMenuItemClick(@NonNull MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId < this.d.size()) {
            this.c.a(((a) this.d.get(itemId)).b());
            this.b.a(b.FEEDBACK);
            this.e.g();
        }
        return true;
    }
}
