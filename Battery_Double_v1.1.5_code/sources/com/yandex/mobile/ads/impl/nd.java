package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.q;
import java.util.List;

public final class nd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final nc f5642a;

    public nd(@NonNull fc fcVar, @NonNull cg cgVar, @NonNull mz mzVar, @NonNull af afVar, @NonNull q qVar) {
        nc ncVar = new nc(fcVar, cgVar, mzVar, afVar, qVar);
        this.f5642a = ncVar;
    }

    public final void a(@NonNull Context context, @Nullable List<ns> list) {
        if (list != null && !list.isEmpty()) {
            for (ns nsVar : list) {
                nb a2 = this.f5642a.a(context, nsVar);
                if (a2 != null) {
                    a2.a(context, nsVar);
                }
            }
        }
    }
}
