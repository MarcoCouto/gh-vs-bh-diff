package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.pm.ActivityInfo;
import com.yandex.mobile.ads.AdActivity;

public final class fd {
    public static void a(Context context) {
        try {
            boolean z = true;
            ActivityInfo[] activityInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 1).activities;
            ActivityInfo activityInfo = null;
            int length = activityInfoArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                ActivityInfo activityInfo2 = activityInfoArr[i];
                if (AdActivity.f5336a.equals(activityInfo2.name)) {
                    activityInfo = activityInfo2;
                    break;
                }
                i++;
            }
            if (activityInfo != null) {
                boolean z2 = ((activityInfo.configChanges & 16) > 0) & true & ((activityInfo.configChanges & 32) > 0) & ((activityInfo.configChanges & 128) > 0) & ((activityInfo.configChanges & 256) > 0) & ((activityInfo.configChanges & 512) > 0);
                if (hc.a(13)) {
                    boolean z3 = z2 & ((activityInfo.configChanges & 1024) > 0);
                    if ((activityInfo.configChanges & 2048) <= 0) {
                        z = false;
                    }
                    z2 = z3 & z;
                }
                if (z2) {
                    return;
                }
            }
        } catch (Exception unused) {
        }
        throw new IllegalStateException("Ad Activity into the AndroidManifest.xml is integrated wrong");
    }
}
