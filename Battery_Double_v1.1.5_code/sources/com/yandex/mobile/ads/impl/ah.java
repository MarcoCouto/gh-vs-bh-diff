package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.qs.a;

public final class ah {

    /* renamed from: a reason: collision with root package name */
    private static volatile ah f5366a;
    private static final Object b = new Object();

    public static ah a() {
        if (f5366a == null) {
            synchronized (b) {
                if (f5366a == null) {
                    f5366a = new ah();
                }
            }
        }
        return f5366a;
    }

    public final synchronized void a(Context context, qr qrVar) {
        bn.a(context).a(qrVar);
    }

    public final void a(@NonNull Context context, @NonNull final String str) {
        bn.a(context).a((a) new a() {
            public final boolean a(qr<?> qrVar) {
                return str.equals(qrVar.e());
            }
        });
    }
}
