package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.yandex.mobile.ads.nativeads.aj;
import com.yandex.mobile.ads.nativeads.i;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.Map;

public final class kq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final kp f5594a;
    @NonNull
    private final aj b;

    public kq(@NonNull aj ajVar, @NonNull i iVar, @NonNull kr krVar) {
        this.b = ajVar;
        this.f5594a = new kp(iVar, krVar);
    }

    @NonNull
    public final Map<String, ki> a() {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceSegment.AGE, kp.a(this.b.b()));
        hashMap.put(TtmlNode.TAG_BODY, kp.a(this.b.c()));
        hashMap.put("call_to_action", kp.a(this.b.d()));
        String str = "close_button";
        TextView e = this.b.e();
        kj kjVar = null;
        ld kvVar = e != null ? new kv(e) : null;
        hashMap.put(str, kvVar != null ? new kj(kvVar) : null);
        hashMap.put("domain", kp.a(this.b.f()));
        hashMap.put("favicon", this.f5594a.a(this.b.g()));
        hashMap.put("feedback", this.f5594a.b(this.b.h()));
        hashMap.put(SettingsJsonConstants.APP_ICON_KEY, this.f5594a.a(this.b.i()));
        hashMap.put("media", this.f5594a.a(this.b.j(), this.b.k()));
        String str2 = CampaignEx.JSON_KEY_STAR;
        View m = this.b.m();
        ld lbVar = m != null ? new lb(m) : null;
        if (lbVar != null) {
            kjVar = new kj(lbVar);
        }
        hashMap.put(str2, kjVar);
        hashMap.put("review_count", kp.a(this.b.n()));
        hashMap.put("price", kp.a(this.b.l()));
        hashMap.put("sponsored", kp.a(this.b.o()));
        hashMap.put("title", kp.a(this.b.p()));
        hashMap.put("warning", kp.a(this.b.q()));
        return hashMap;
    }
}
