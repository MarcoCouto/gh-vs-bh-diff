package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.rewarded.Reward;
import com.yandex.mobile.ads.rewarded.a;
import com.yandex.mobile.ads.rewarded.c;

public final class ry implements rz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final c f5749a;
    @NonNull
    private final a b;

    public ry(@NonNull bh bhVar, @NonNull a aVar) {
        this.b = aVar;
        this.f5749a = new c(bhVar.a(), bhVar.b());
    }

    public final void a() {
        this.b.a((Reward) this.f5749a);
    }
}
