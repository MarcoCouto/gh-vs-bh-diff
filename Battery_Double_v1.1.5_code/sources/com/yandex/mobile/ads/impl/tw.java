package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Xml;
import com.explorestack.iab.vast.tags.VastTagName;
import com.yandex.mobile.ads.video.models.vmap.AdBreak;
import com.yandex.mobile.ads.video.models.vmap.Vmap;
import com.yandex.mobile.ads.video.models.vmap.a;
import com.yandex.mobile.ads.video.models.vmap.d;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class tw {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final to f5798a = new to(this.c, this.b);
    @NonNull
    private final tt b = new tt(this.c);
    @NonNull
    private final tn c = new tn();
    @NonNull
    private final a d = new a();
    @NonNull
    private final ty e = new ty();

    @Nullable
    public final Vmap a(@NonNull String str) throws IOException, XmlPullParserException {
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
        newPullParser.setInput(new StringReader(str));
        newPullParser.nextTag();
        return a(newPullParser);
    }

    @Nullable
    private static Vmap a(@NonNull XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        tn.a(xmlPullParser, "VMAP");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        String attributeValue = xmlPullParser.getAttributeValue(null, "version");
        while (tn.b(xmlPullParser)) {
            if (tn.a(xmlPullParser)) {
                String name = xmlPullParser.getName();
                if ("AdBreak".equals(name)) {
                    AdBreak a2 = to.a(xmlPullParser);
                    if (a2 != null) {
                        arrayList.add(a2);
                    }
                } else if (VastTagName.EXTENSIONS.equals(name)) {
                    arrayList2.addAll(tt.a(xmlPullParser));
                } else {
                    tn.d(xmlPullParser);
                }
            }
        }
        if (TextUtils.isEmpty(attributeValue) || arrayList.isEmpty()) {
            return null;
        }
        a.a(arrayList, ty.a(arrayList2));
        return d.a(attributeValue, arrayList, arrayList2);
    }
}
