package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.y;
import org.json.JSONException;
import org.json.JSONObject;

public interface oy<T> {
    @NonNull
    T a(@NonNull JSONObject jSONObject) throws JSONException, y;
}
