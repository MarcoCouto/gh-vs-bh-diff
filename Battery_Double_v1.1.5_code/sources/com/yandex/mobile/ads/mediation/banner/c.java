package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.au;
import com.yandex.mobile.ads.impl.aw;
import com.yandex.mobile.ads.impl.ay;

final class c implements aw<MediatedBannerAdapter> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ay<MediatedBannerAdapter> f5824a;

    c(@NonNull ay<MediatedBannerAdapter> ayVar) {
        this.f5824a = ayVar;
    }

    @Nullable
    public final au<MediatedBannerAdapter> a(@NonNull Context context) {
        return this.f5824a.a(context, MediatedBannerAdapter.class);
    }
}
