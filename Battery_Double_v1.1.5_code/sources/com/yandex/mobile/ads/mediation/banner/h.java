package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.a;
import com.yandex.mobile.ads.impl.bd;
import com.yandex.mobile.ads.impl.x;
import java.lang.ref.WeakReference;

public final class h implements b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<a> f5830a;
    @NonNull
    private final bd b;

    public final void a(@NonNull Context context) {
    }

    public h(@NonNull a aVar) {
        this.f5830a = new WeakReference<>(aVar);
        this.b = new bd(aVar.s());
    }

    public final void a(@NonNull Context context, @NonNull x<String> xVar) {
        a aVar = (a) this.f5830a.get();
        if (aVar != null) {
            this.b.a(context, xVar);
            this.b.b(context, xVar);
            aVar.b(xVar);
        }
    }
}
