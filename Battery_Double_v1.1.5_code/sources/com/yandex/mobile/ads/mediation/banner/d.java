package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ax;
import java.util.Map;

final class d implements ax<MediatedBannerAdapter, MediatedBannerAdapterListener> {
    d() {
    }

    public final /* synthetic */ void a(@NonNull Context context, @NonNull Object obj, @NonNull Object obj2, @NonNull Map map, @NonNull Map map2) {
        ((MediatedBannerAdapter) obj).loadBanner(context, (MediatedBannerAdapterListener) obj2, map, map2);
    }

    public final /* synthetic */ void a(@NonNull Object obj) {
        ((MediatedBannerAdapter) obj).onInvalidate();
    }
}
