package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.al;
import com.yandex.mobile.ads.impl.az;
import com.yandex.mobile.ads.impl.fc;
import java.util.Map;

final class f extends az {
    f(@NonNull fc fcVar) {
        super(fcVar);
    }

    @NonNull
    public final Map<String, Object> a(@NonNull Context context) {
        Map<String, Object> a2 = super.a(context);
        al b = this.f5383a.b();
        if (b != null) {
            a2.put("width", Integer.valueOf(b.a()));
            a2.put("height", Integer.valueOf(b.b()));
        }
        return a2;
    }
}
