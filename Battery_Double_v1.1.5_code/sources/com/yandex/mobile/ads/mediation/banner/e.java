package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.a;
import com.yandex.mobile.ads.impl.av;
import com.yandex.mobile.ads.impl.ay;
import com.yandex.mobile.ads.impl.ba;
import com.yandex.mobile.ads.impl.bb;
import com.yandex.mobile.ads.impl.be;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.x;

public final class e implements b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final MediatedBannerAdapterListener f5825a;
    @NonNull
    private final av<MediatedBannerAdapter, MediatedBannerAdapterListener> b;

    public e(@NonNull a aVar, @NonNull be beVar) {
        fc s = aVar.s();
        f fVar = new f(s);
        bb bbVar = new bb(s);
        av avVar = new av(s, new d(), bbVar, new c(new ay(beVar, fVar, bbVar)), new ba(aVar, beVar));
        this.b = avVar;
        this.f5825a = new a(aVar, this.b, new g(aVar.a(), this.b));
    }

    public final void a(@NonNull Context context) {
        this.b.a(context);
    }

    public final void a(@NonNull Context context, @NonNull x<String> xVar) {
        this.b.a(context, this.f5825a);
    }
}
