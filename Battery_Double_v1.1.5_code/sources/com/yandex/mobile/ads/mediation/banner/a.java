package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.av;
import java.lang.ref.WeakReference;

final class a implements MediatedBannerAdapterListener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<com.yandex.mobile.ads.impl.a> f5823a;
    @NonNull
    private final g b;
    @NonNull
    private final av<MediatedBannerAdapter, MediatedBannerAdapterListener> c;
    private boolean d;

    a(@NonNull com.yandex.mobile.ads.impl.a aVar, @NonNull av<MediatedBannerAdapter, MediatedBannerAdapterListener> avVar, @NonNull g gVar) {
        this.c = avVar;
        this.b = gVar;
        this.f5823a = new WeakReference<>(aVar);
    }

    public final void onAdClicked() {
        com.yandex.mobile.ads.impl.a aVar = (com.yandex.mobile.ads.impl.a) this.f5823a.get();
        if (aVar != null) {
            this.c.b(aVar.m());
        }
    }

    public final void onAdLeftApplication() {
        com.yandex.mobile.ads.impl.a aVar = (com.yandex.mobile.ads.impl.a) this.f5823a.get();
        if (aVar != null) {
            aVar.onAdLeftApplication();
        }
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        com.yandex.mobile.ads.impl.a aVar = (com.yandex.mobile.ads.impl.a) this.f5823a.get();
        if (aVar != null) {
            Context m = aVar.m();
            if (!this.d) {
                this.c.a(m, adRequestError, this);
                return;
            }
            this.c.b(m, adRequestError, this);
        }
    }

    public final void onAdLoaded(@NonNull View view) {
        com.yandex.mobile.ads.impl.a aVar = (com.yandex.mobile.ads.impl.a) this.f5823a.get();
        if (aVar != null) {
            Context context = view.getContext();
            if (!this.d) {
                this.d = true;
                this.c.e(context);
            } else {
                this.c.d(context);
            }
            this.b.a(view);
            aVar.onAdLoaded();
        }
    }
}
