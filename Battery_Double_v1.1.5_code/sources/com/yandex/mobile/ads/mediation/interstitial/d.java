package com.yandex.mobile.ads.mediation.interstitial;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.av;
import com.yandex.mobile.ads.impl.ay;
import com.yandex.mobile.ads.impl.az;
import com.yandex.mobile.ads.impl.ba;
import com.yandex.mobile.ads.impl.bb;
import com.yandex.mobile.ads.impl.be;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.hm;
import com.yandex.mobile.ads.impl.jm;
import com.yandex.mobile.ads.impl.x;

public final class d implements jm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final av<MediatedInterstitialAdapter, MediatedInterstitialAdapterListener> f5836a;
    @NonNull
    private final c b = new c();
    @NonNull
    private final MediatedInterstitialAdapterListener c;

    public d(@NonNull hm hmVar, @NonNull be beVar) {
        fc s = hmVar.s();
        az azVar = new az(s);
        bb bbVar = new bb(s);
        b bVar = new b(new ay(beVar, azVar, bbVar));
        ba baVar = new ba(hmVar, beVar);
        av avVar = new av(s, this.b, bbVar, bVar, baVar);
        this.f5836a = avVar;
        this.c = new a(hmVar, this.f5836a);
    }

    public final void a(@NonNull Context context) {
        this.f5836a.a(context);
    }

    public final boolean a() {
        return this.b.b();
    }

    public final void a(@NonNull Context context, @NonNull x<String> xVar) {
        this.f5836a.a(context, this.c);
    }

    public final void b() {
        MediatedInterstitialAdapter a2 = this.b.a();
        if (a2 != null) {
            a2.showInterstitial();
        }
    }
}
