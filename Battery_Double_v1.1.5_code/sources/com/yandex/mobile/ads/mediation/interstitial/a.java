package com.yandex.mobile.ads.mediation.interstitial;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.av;
import com.yandex.mobile.ads.impl.hm;
import java.lang.ref.WeakReference;

final class a implements MediatedInterstitialAdapterListener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<hm> f5833a;
    @NonNull
    private final av<MediatedInterstitialAdapter, MediatedInterstitialAdapterListener> b;

    a(@NonNull hm hmVar, @NonNull av<MediatedInterstitialAdapter, MediatedInterstitialAdapterListener> avVar) {
        this.f5833a = new WeakReference<>(hmVar);
        this.b = avVar;
    }

    public final void onInterstitialDismissed() {
        hm hmVar = (hm) this.f5833a.get();
        if (hmVar != null) {
            hmVar.B();
        }
    }

    public final void onInterstitialFailedToLoad(@NonNull AdRequestError adRequestError) {
        hm hmVar = (hm) this.f5833a.get();
        if (hmVar != null) {
            this.b.a(hmVar.m(), adRequestError, this);
        }
    }

    public final void onInterstitialLoaded() {
        hm hmVar = (hm) this.f5833a.get();
        if (hmVar != null) {
            this.b.e(hmVar.m());
            hmVar.onAdLoaded();
        }
    }

    public final void onInterstitialShown() {
        hm hmVar = (hm) this.f5833a.get();
        if (hmVar != null) {
            this.b.c(hmVar.m());
            hmVar.A();
        }
    }

    public final void onInterstitialClicked() {
        hm hmVar = (hm) this.f5833a.get();
        if (hmVar != null) {
            this.b.b(hmVar.m());
        }
    }

    public final void onInterstitialLeftApplication() {
        hm hmVar = (hm) this.f5833a.get();
        if (hmVar != null) {
            hmVar.onAdLeftApplication();
        }
    }
}
