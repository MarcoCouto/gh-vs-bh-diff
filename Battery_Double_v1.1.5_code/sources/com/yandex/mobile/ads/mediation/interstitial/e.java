package com.yandex.mobile.ads.mediation.interstitial;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.bd;
import com.yandex.mobile.ads.impl.hm;
import com.yandex.mobile.ads.impl.jm;
import com.yandex.mobile.ads.impl.x;
import java.lang.ref.WeakReference;

public final class e implements jm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<hm> f5837a;
    @NonNull
    private final bd b;

    public final void a(@NonNull Context context) {
    }

    public e(@NonNull hm hmVar) {
        this.f5837a = new WeakReference<>(hmVar);
        this.b = new bd(hmVar.s());
    }

    public final boolean a() {
        hm hmVar = (hm) this.f5837a.get();
        return hmVar != null && hmVar.k();
    }

    public final void a(@NonNull Context context, @NonNull x<String> xVar) {
        hm hmVar = (hm) this.f5837a.get();
        if (hmVar != null) {
            this.b.a(context, xVar);
            this.b.b(context, xVar);
            hmVar.b(xVar);
        }
    }

    public final void b() {
        hm hmVar = (hm) this.f5837a.get();
        if (hmVar != null) {
            hmVar.g();
        }
    }
}
