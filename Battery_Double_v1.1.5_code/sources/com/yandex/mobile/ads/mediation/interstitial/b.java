package com.yandex.mobile.ads.mediation.interstitial;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.au;
import com.yandex.mobile.ads.impl.aw;
import com.yandex.mobile.ads.impl.ay;

final class b implements aw<MediatedInterstitialAdapter> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ay<MediatedInterstitialAdapter> f5834a;

    b(@NonNull ay<MediatedInterstitialAdapter> ayVar) {
        this.f5834a = ayVar;
    }

    @Nullable
    public final au<MediatedInterstitialAdapter> a(@NonNull Context context) {
        return this.f5834a.a(context, MediatedInterstitialAdapter.class);
    }
}
