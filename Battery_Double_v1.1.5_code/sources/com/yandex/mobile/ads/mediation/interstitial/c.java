package com.yandex.mobile.ads.mediation.interstitial;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ax;
import java.util.Map;

final class c implements ax<MediatedInterstitialAdapter, MediatedInterstitialAdapterListener> {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private MediatedInterstitialAdapter f5835a;

    c() {
    }

    public final /* synthetic */ void a(@NonNull Context context, @NonNull Object obj, @NonNull Object obj2, @NonNull Map map, @NonNull Map map2) {
        MediatedInterstitialAdapter mediatedInterstitialAdapter = (MediatedInterstitialAdapter) obj;
        MediatedInterstitialAdapterListener mediatedInterstitialAdapterListener = (MediatedInterstitialAdapterListener) obj2;
        this.f5835a = mediatedInterstitialAdapter;
        mediatedInterstitialAdapter.loadInterstitial(context, mediatedInterstitialAdapterListener, map, map2);
    }

    public final /* synthetic */ void a(@NonNull Object obj) {
        ((MediatedInterstitialAdapter) obj).onInvalidate();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final MediatedInterstitialAdapter a() {
        return this.f5835a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.f5835a != null && this.f5835a.isLoaded();
    }
}
