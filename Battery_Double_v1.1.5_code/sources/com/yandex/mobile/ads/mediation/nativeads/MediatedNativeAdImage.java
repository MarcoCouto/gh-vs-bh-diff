package com.yandex.mobile.ads.mediation.nativeads;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class MediatedNativeAdImage {

    /* renamed from: a reason: collision with root package name */
    private final int f5840a;
    private final int b;
    @NonNull
    private final String c;
    @Nullable
    private final Drawable d;

    static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public int f5841a;
        /* access modifiers changed from: private */
        public int b;
        /* access modifiers changed from: private */
        @NonNull
        public final String c;
        /* access modifiers changed from: private */
        @Nullable
        public Drawable d;

        Builder(@NonNull String str) {
            this.c = str;
        }

        @NonNull
        public final MediatedNativeAdImage build() {
            return new MediatedNativeAdImage(this);
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setWidth(int i) {
            this.f5841a = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setHeight(int i) {
            this.b = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setDrawable(@Nullable Drawable drawable) {
            this.d = drawable;
            return this;
        }
    }

    private MediatedNativeAdImage(@NonNull Builder builder) {
        this.c = builder.c;
        this.f5840a = builder.f5841a;
        this.b = builder.b;
        this.d = builder.d;
    }

    /* access modifiers changed from: 0000 */
    public final int getWidth() {
        return this.f5840a;
    }

    /* access modifiers changed from: 0000 */
    public final int getHeight() {
        return this.b;
    }

    @Nullable
    public final Drawable getDrawable() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String getUrl() {
        return this.c;
    }
}
