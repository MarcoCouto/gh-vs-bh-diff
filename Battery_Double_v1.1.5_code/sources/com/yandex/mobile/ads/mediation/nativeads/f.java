package com.yandex.mobile.ads.mediation.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.q;
import java.util.WeakHashMap;

final class f {

    /* renamed from: a reason: collision with root package name */
    private final WeakHashMap<q, Object> f5848a = new WeakHashMap<>();

    f() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull q qVar) {
        this.f5848a.put(qVar, null);
    }

    public final void a() {
        for (q a2 : this.f5848a.keySet()) {
            a2.a();
        }
    }

    public final void b() {
        for (q b : this.f5848a.keySet()) {
            b.b();
        }
    }

    public final void c() {
        for (q c : this.f5848a.keySet()) {
            c.c();
        }
    }

    public final void d() {
        for (q d : this.f5848a.keySet()) {
            d.d();
        }
    }

    public final void e() {
        for (q e : this.f5848a.keySet()) {
            e.e();
        }
    }
}
