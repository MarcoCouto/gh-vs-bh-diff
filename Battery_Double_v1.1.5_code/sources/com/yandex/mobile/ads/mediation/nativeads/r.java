package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.np;
import com.yandex.mobile.ads.impl.nq;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.impl.x.a;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import java.util.Collections;
import java.util.List;
import java.util.Map;

final class r {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final q f5857a;

    r(@NonNull Context context) {
        this.f5857a = new q(context);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final x<nq> a(@NonNull MediatedNativeAd mediatedNativeAd, @NonNull Map<String, Bitmap> map, @NonNull NativeAdType nativeAdType) {
        List a2 = this.f5857a.a(mediatedNativeAd.getMediatedNativeAdAssets(), map);
        np npVar = new np();
        npVar.a(nativeAdType.getValue());
        npVar.a(a2);
        nq nqVar = new nq();
        nqVar.b(Collections.singletonList(npVar));
        return new a().a(nqVar).a();
    }
}
