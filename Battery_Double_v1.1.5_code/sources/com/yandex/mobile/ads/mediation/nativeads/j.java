package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.au;
import com.yandex.mobile.ads.impl.aw;
import com.yandex.mobile.ads.impl.ay;

final class j implements aw<MediatedNativeAdapter> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ay<MediatedNativeAdapter> f5852a;

    j(@NonNull ay<MediatedNativeAdapter> ayVar) {
        this.f5852a = ayVar;
    }

    @Nullable
    public final au<MediatedNativeAdapter> a(@NonNull Context context) {
        return this.f5852a.a(context, MediatedNativeAdapter.class);
    }
}
