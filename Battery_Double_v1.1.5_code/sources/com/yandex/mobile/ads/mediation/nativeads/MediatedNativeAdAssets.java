package com.yandex.mobile.ads.mediation.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class MediatedNativeAdAssets {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f5838a;
    @Nullable
    private final String b;
    @Nullable
    private final String c;
    @Nullable
    private final String d;
    @Nullable
    private final MediatedNativeAdImage e;
    @Nullable
    private final MediatedNativeAdImage f;
    @Nullable
    private final MediatedNativeAdImage g;
    @Nullable
    private final MediatedNativeAdMedia h;
    @Nullable
    private final String i;
    @Nullable
    private final String j;
    @Nullable
    private final String k;
    @Nullable
    private final String l;
    @Nullable
    private final String m;
    @Nullable
    private final String n;

    static final class Builder {
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public String f5839a;
        /* access modifiers changed from: private */
        @Nullable
        public String b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;
        /* access modifiers changed from: private */
        @Nullable
        public String d;
        /* access modifiers changed from: private */
        @Nullable
        public MediatedNativeAdImage e;
        /* access modifiers changed from: private */
        @Nullable
        public MediatedNativeAdImage f;
        /* access modifiers changed from: private */
        @Nullable
        public MediatedNativeAdImage g;
        /* access modifiers changed from: private */
        @Nullable
        public MediatedNativeAdMedia h;
        /* access modifiers changed from: private */
        @Nullable
        public String i;
        /* access modifiers changed from: private */
        @Nullable
        public String j;
        /* access modifiers changed from: private */
        @Nullable
        public String k;
        /* access modifiers changed from: private */
        @Nullable
        public String l;
        /* access modifiers changed from: private */
        @Nullable
        public String m;
        /* access modifiers changed from: private */
        @Nullable
        public String n;

        Builder() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final MediatedNativeAdAssets build() {
            return new MediatedNativeAdAssets(this);
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setAge(@Nullable String str) {
            this.f5839a = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setBody(@Nullable String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setCallToAction(@Nullable String str) {
            this.c = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setDomain(@Nullable String str) {
            this.d = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setFavicon(@Nullable MediatedNativeAdImage mediatedNativeAdImage) {
            this.e = mediatedNativeAdImage;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setIcon(@Nullable MediatedNativeAdImage mediatedNativeAdImage) {
            this.f = mediatedNativeAdImage;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setImage(@Nullable MediatedNativeAdImage mediatedNativeAdImage) {
            this.g = mediatedNativeAdImage;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setMedia(@Nullable MediatedNativeAdMedia mediatedNativeAdMedia) {
            this.h = mediatedNativeAdMedia;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setPrice(@Nullable String str) {
            this.i = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setRating(@Nullable String str) {
            this.j = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setReviewCount(@Nullable String str) {
            this.k = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setSponsored(@Nullable String str) {
            this.l = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setTitle(@Nullable String str) {
            this.m = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setWarning(@Nullable String str) {
            this.n = str;
            return this;
        }
    }

    private MediatedNativeAdAssets(@NonNull Builder builder) {
        this.f5838a = builder.f5839a;
        this.b = builder.b;
        this.c = builder.c;
        this.d = builder.d;
        this.e = builder.e;
        this.f = builder.f;
        this.g = builder.g;
        this.h = builder.h;
        this.i = builder.i;
        this.j = builder.j;
        this.k = builder.k;
        this.l = builder.l;
        this.m = builder.m;
        this.n = builder.n;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getAge() {
        return this.f5838a;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getBody() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getCallToAction() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getDomain() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public MediatedNativeAdImage getFavicon() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public MediatedNativeAdImage getIcon() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public MediatedNativeAdImage getImage() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public MediatedNativeAdMedia getMedia() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getPrice() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getRating() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getReviewCount() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getSponsored() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getTitle() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getWarning() {
        return this.n;
    }
}
