package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.bd;
import com.yandex.mobile.ads.impl.ku;
import com.yandex.mobile.ads.impl.nq;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.nativeads.bn;
import com.yandex.mobile.ads.nativeads.bp;
import com.yandex.mobile.ads.nativeads.bq;
import com.yandex.mobile.ads.nativeads.s;
import java.lang.ref.WeakReference;

public final class u implements t {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<com.yandex.mobile.ads.nativeads.u> f5861a;
    @NonNull
    private final bd b;

    public u(@NonNull com.yandex.mobile.ads.nativeads.u uVar) {
        this.f5861a = new WeakReference<>(uVar);
        this.b = new bd(uVar.s());
    }

    public final void a(@NonNull Context context, @NonNull x<nq> xVar) {
        com.yandex.mobile.ads.nativeads.u uVar = (com.yandex.mobile.ads.nativeads.u) this.f5861a.get();
        if (uVar != null) {
            this.b.a(context, xVar);
            this.b.b(context, xVar);
            uVar.a(xVar, new s(new bn(), new bq(), new ku(xVar), new bp()));
        }
    }
}
