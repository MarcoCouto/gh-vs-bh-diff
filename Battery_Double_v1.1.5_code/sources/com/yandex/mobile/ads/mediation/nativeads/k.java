package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ax;
import java.util.Map;

final class k implements ax<MediatedNativeAdapter, MediatedNativeAdapterListener> {
    public final /* bridge */ /* synthetic */ void a(@NonNull Object obj) {
    }

    k() {
    }

    public final /* synthetic */ void a(@NonNull Context context, @NonNull Object obj, @NonNull Object obj2, @NonNull Map map, @NonNull Map map2) {
        ((MediatedNativeAdapter) obj).loadAd(context, (MediatedNativeAdapterListener) obj2, map, map2);
    }
}
