package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.av;
import com.yandex.mobile.ads.impl.gu.b;
import com.yandex.mobile.ads.impl.kt;
import com.yandex.mobile.ads.impl.nq;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.h;
import com.yandex.mobile.ads.nativeads.q;
import com.yandex.mobile.ads.nativeads.u;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class s implements MediatedNativeAdapterListener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final av<MediatedNativeAdapter, MediatedNativeAdapterListener> f5858a;
    @NonNull
    private final WeakReference<u> b;
    /* access modifiers changed from: private */
    @NonNull
    public final f c;
    @NonNull
    private final h d;
    @NonNull
    private final g e;
    @NonNull
    private final Map<String, Object> f = new HashMap();
    @NonNull
    private final Map<String, Object> g = new HashMap();
    @NonNull
    private final i h;
    @NonNull
    private final h i;
    private boolean j;

    s(@NonNull u uVar, @NonNull av<MediatedNativeAdapter, MediatedNativeAdapterListener> avVar) {
        Context m = uVar.m();
        this.f5858a = avVar;
        this.b = new WeakReference<>(uVar);
        this.c = new f();
        this.d = new h(m);
        this.h = new i();
        this.i = new h(m);
        this.e = new g(m, this.d, this.i);
    }

    public final void onAdClicked() {
        u uVar = (u) this.b.get();
        if (uVar != null) {
            Context m = uVar.m();
            this.f5858a.a(m, this.f);
            a(m, b.CLICK);
        }
        this.c.a();
    }

    public final void onAdClosed() {
        this.c.b();
    }

    public final void onAdImpression() {
        if (!this.j) {
            this.j = true;
            u uVar = (u) this.b.get();
            if (uVar != null) {
                Context m = uVar.m();
                this.f5858a.c(m, this.f);
                a(m, b.IMPRESSION_TRACKING_SUCCESS);
            }
            this.c.c();
        }
    }

    private void a(@NonNull Context context, @NonNull b bVar) {
        HashMap hashMap = new HashMap(this.f);
        hashMap.put("event_type", bVar.a());
        hashMap.put("ad_info", this.g);
        this.f5858a.b(context, (Map<String, Object>) hashMap);
    }

    public final void onAdLeftApplication() {
        this.c.d();
    }

    public final void onAdOpened() {
        this.c.e();
    }

    public final void onAppInstallAdLoaded(@NonNull MediatedNativeAd mediatedNativeAd) {
        a(mediatedNativeAd, NativeAdType.APP_INSTALL);
    }

    public final void onContentAdLoaded(@NonNull MediatedNativeAd mediatedNativeAd) {
        a(mediatedNativeAd, NativeAdType.CONTENT);
    }

    private void a(@NonNull final MediatedNativeAd mediatedNativeAd, @NonNull NativeAdType nativeAdType) {
        final u uVar = (u) this.b.get();
        if (uVar != null) {
            Context m = uVar.m();
            this.f.put("native_ad_type", nativeAdType.getValue());
            this.f5858a.d(m, this.f);
            this.g.putAll(a(mediatedNativeAd));
            List a2 = i.a(mediatedNativeAd);
            a(a2);
            this.e.a(mediatedNativeAd, nativeAdType, a2, new a() {
                public final void a(@NonNull x<nq> xVar) {
                    uVar.a(xVar, new com.yandex.mobile.ads.nativeads.s(new e(new a() {
                        public final void a(@NonNull q qVar) {
                            s.this.c.a(qVar);
                        }
                    }), new p(mediatedNativeAd), new kt(), new o()));
                }
            });
        }
    }

    private void a(@NonNull List<MediatedNativeAdImage> list) {
        this.d.a(h.a(list));
    }

    @NonNull
    private static Map<String, Object> a(@NonNull MediatedNativeAd mediatedNativeAd) {
        HashMap hashMap = new HashMap();
        hashMap.put("title", mediatedNativeAd.getMediatedNativeAdAssets().getTitle());
        return hashMap;
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        u uVar = (u) this.b.get();
        if (uVar != null) {
            this.f5858a.a(uVar.m(), adRequestError, this);
        }
    }
}
