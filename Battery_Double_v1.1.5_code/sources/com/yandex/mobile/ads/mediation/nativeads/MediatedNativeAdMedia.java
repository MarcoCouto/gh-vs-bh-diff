package com.yandex.mobile.ads.mediation.nativeads;

import android.support.annotation.NonNull;

final class MediatedNativeAdMedia {

    /* renamed from: a reason: collision with root package name */
    private final float f5842a;

    static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final float f5843a;

        Builder(float f) {
            this.f5843a = f;
        }

        @NonNull
        public final MediatedNativeAdMedia build() {
            return new MediatedNativeAdMedia(this);
        }
    }

    private MediatedNativeAdMedia(@NonNull Builder builder) {
        this.f5842a = builder.f5843a;
    }

    /* access modifiers changed from: 0000 */
    public final float getAspectRatio() {
        return this.f5842a;
    }
}
