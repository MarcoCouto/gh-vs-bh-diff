package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.nq;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.h;
import com.yandex.mobile.ads.nativeads.j;
import java.util.List;
import java.util.Map;

final class g {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final h f5849a;
    @NonNull
    private final h b;
    @NonNull
    private final r c;

    interface a {
        void a(@NonNull x<nq> xVar);
    }

    g(@NonNull Context context, @NonNull h hVar, @NonNull h hVar2) {
        this.f5849a = hVar;
        this.b = hVar2;
        this.c = new r(context);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final MediatedNativeAd mediatedNativeAd, @NonNull final NativeAdType nativeAdType, @NonNull List<MediatedNativeAdImage> list, @NonNull final a aVar) {
        this.f5849a.a(this.b.b(list), new j() {
            public final void a(@NonNull Map<String, Bitmap> map) {
                aVar.a(g.this.c.a(mediatedNativeAd, map, nativeAdType));
            }
        });
    }
}
