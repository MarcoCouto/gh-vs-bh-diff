package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.dp;
import com.yandex.mobile.ads.impl.nl;
import com.yandex.mobile.ads.impl.qg;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class h {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final qg f5851a = new qg();
    @NonNull
    private final c b = new c();
    @NonNull
    private final Point c;

    h(@NonNull Context context) {
        this.c = dp.j(context);
    }

    @NonNull
    static Map<String, Bitmap> a(@NonNull List<MediatedNativeAdImage> list) {
        HashMap hashMap = new HashMap();
        for (MediatedNativeAdImage mediatedNativeAdImage : list) {
            Drawable drawable = mediatedNativeAdImage.getDrawable();
            String url = mediatedNativeAdImage.getUrl();
            if (drawable != null && !TextUtils.isEmpty(url)) {
                Bitmap a2 = qg.a(drawable);
                if (a2 != null) {
                    hashMap.put(url, a2);
                }
            }
        }
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Set<nl> b(@NonNull List<MediatedNativeAdImage> list) {
        HashSet hashSet = new HashSet();
        for (MediatedNativeAdImage mediatedNativeAdImage : list) {
            String url = mediatedNativeAdImage.getUrl();
            int width = mediatedNativeAdImage.getWidth();
            int height = mediatedNativeAdImage.getHeight();
            if (!TextUtils.isEmpty(url) && !c.a(width, height)) {
                nl nlVar = new nl();
                nlVar.a(url);
                nlVar.a(this.c.x);
                nlVar.b(this.c.y);
                hashSet.add(nlVar);
            }
        }
        return hashSet;
    }
}
