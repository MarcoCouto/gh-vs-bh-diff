package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.yandex.mobile.ads.impl.js;
import com.yandex.mobile.ads.impl.jy;
import com.yandex.mobile.ads.impl.jz;
import com.yandex.mobile.ads.impl.ni;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class q {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f5856a;
    @NonNull
    private final d b = new d(this.f5856a);
    @NonNull
    private final js c = new js();
    @NonNull
    private final jz d = new jz();

    q(@NonNull Context context) {
        this.f5856a = new a(context, new c());
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final List<ni> a(@NonNull MediatedNativeAdAssets mediatedNativeAdAssets, @NonNull Map<String, Bitmap> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(a(IronSourceSegment.AGE, (T) mediatedNativeAdAssets.getAge()));
        arrayList.add(a(TtmlNode.TAG_BODY, (T) mediatedNativeAdAssets.getBody()));
        arrayList.add(a("call_to_action", (T) mediatedNativeAdAssets.getCallToAction()));
        arrayList.add(a("domain", (T) mediatedNativeAdAssets.getDomain()));
        arrayList.add(a("favicon", (T) this.f5856a.a(map, mediatedNativeAdAssets.getFavicon())));
        arrayList.add(a(SettingsJsonConstants.APP_ICON_KEY, (T) this.f5856a.a(map, mediatedNativeAdAssets.getIcon())));
        arrayList.add(a("media", (T) this.b.a(map, mediatedNativeAdAssets.getImage(), mediatedNativeAdAssets.getMedia())));
        arrayList.add(a("price", (T) mediatedNativeAdAssets.getPrice()));
        arrayList.add(a(CampaignEx.JSON_KEY_STAR, (T) String.valueOf(mediatedNativeAdAssets.getRating())));
        arrayList.add(a("review_count", (T) mediatedNativeAdAssets.getReviewCount()));
        arrayList.add(a("sponsored", (T) mediatedNativeAdAssets.getSponsored()));
        arrayList.add(a("title", (T) mediatedNativeAdAssets.getTitle()));
        arrayList.add(a("warning", (T) mediatedNativeAdAssets.getWarning()));
        return a(arrayList);
    }

    @NonNull
    private static List<ni> a(List<ni> list) {
        ArrayList arrayList = new ArrayList();
        for (ni niVar : list) {
            if (niVar != null) {
                arrayList.add(niVar);
            }
        }
        return arrayList;
    }

    @Nullable
    private <T> ni a(@NonNull String str, @Nullable T t) {
        jy a2 = this.d.a(str);
        if (t == null || !a2.a(t)) {
            return null;
        }
        return this.c.a(str).a(str, t);
    }
}
