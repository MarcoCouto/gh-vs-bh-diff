package com.yandex.mobile.ads.mediation.nativeads;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.nl;
import com.yandex.mobile.ads.impl.nn;
import com.yandex.mobile.ads.impl.no;
import java.util.Map;

final class d {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f5846a;

    d(@NonNull a aVar) {
        this.f5846a = aVar;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final no a(@NonNull Map<String, Bitmap> map, @Nullable MediatedNativeAdImage mediatedNativeAdImage, @Nullable MediatedNativeAdMedia mediatedNativeAdMedia) {
        nl a2 = this.f5846a.a(map, mediatedNativeAdImage);
        nn nnVar = mediatedNativeAdMedia != null ? new nn(null, mediatedNativeAdMedia.getAspectRatio()) : null;
        if (a2 == null && nnVar == null) {
            return null;
        }
        return new no(nnVar, a2);
    }
}
