package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.nl;
import java.util.Map;

final class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final c f5844a;
    @NonNull
    private final b b;

    a(@NonNull Context context, @NonNull c cVar) {
        this.f5844a = cVar;
        this.b = new b(context);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final nl a(@NonNull Map<String, Bitmap> map, @Nullable MediatedNativeAdImage mediatedNativeAdImage) {
        if (mediatedNativeAdImage == null) {
            return null;
        }
        String url = mediatedNativeAdImage.getUrl();
        int width = mediatedNativeAdImage.getWidth();
        int height = mediatedNativeAdImage.getHeight();
        if (c.a(width, height)) {
            return a(width, height, url, this.b.a(width, height));
        }
        Bitmap bitmap = (Bitmap) map.get(url);
        if (bitmap == null) {
            return null;
        }
        int width2 = bitmap.getWidth();
        int height2 = bitmap.getHeight();
        return a(width2, height2, url, this.b.a(width2, height2));
    }

    @NonNull
    private static nl a(int i, int i2, @NonNull String str, @NonNull String str2) {
        nl nlVar = new nl();
        nlVar.a(str);
        nlVar.a(i);
        nlVar.b(i2);
        nlVar.b(str2);
        return nlVar;
    }
}
