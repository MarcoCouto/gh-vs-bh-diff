package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.dv;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration;

final class b {

    /* renamed from: a reason: collision with root package name */
    private final Context f5845a;

    b(@NonNull Context context) {
        this.f5845a = context.getApplicationContext();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String a(int i, int i2) {
        int a2 = dv.a(this.f5845a, i);
        int a3 = dv.a(this.f5845a, i2);
        Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(a2), Integer.valueOf(a3)};
        if (a2 >= 320 || a3 >= 240) {
            return NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_LARGE;
        }
        return (a2 >= 160 || a3 >= 160) ? "medium" : NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_SMALL;
    }
}
