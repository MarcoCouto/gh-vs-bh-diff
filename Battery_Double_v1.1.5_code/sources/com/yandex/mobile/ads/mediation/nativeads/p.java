package com.yandex.mobile.ads.mediation.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.np;
import com.yandex.mobile.ads.nativeads.bd;
import com.yandex.mobile.ads.nativeads.be;
import com.yandex.mobile.ads.nativeads.bq;

public final class p implements be {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final MediatedNativeAd f5855a;
    @NonNull
    private final be b = new bq();

    p(@NonNull MediatedNativeAd mediatedNativeAd) {
        this.f5855a = mediatedNativeAd;
    }

    @NonNull
    public final bd a(@NonNull np npVar) {
        return new m(this.b.a(npVar), this.f5855a);
    }
}
