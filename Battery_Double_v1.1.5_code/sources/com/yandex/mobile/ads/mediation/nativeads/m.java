package com.yandex.mobile.ads.mediation.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.bd;
import com.yandex.mobile.ads.nativeads.e;

final class m implements bd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bd f5854a;
    @NonNull
    private final MediatedNativeAd b;

    m(@NonNull bd bdVar, @NonNull MediatedNativeAd mediatedNativeAd) {
        this.f5854a = bdVar;
        this.b = mediatedNativeAd;
    }

    public final void a(@NonNull af afVar) {
        this.f5854a.a(afVar);
        this.b.unbindNativeAd(afVar.f());
    }

    public final void a(@NonNull af afVar, @NonNull e eVar) {
        this.f5854a.a(afVar, eVar);
        this.b.bindNativeAd(afVar.f());
    }

    public final void a() {
        this.f5854a.a();
    }
}
