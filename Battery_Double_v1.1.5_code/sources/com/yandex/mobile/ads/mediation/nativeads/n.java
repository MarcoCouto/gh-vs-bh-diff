package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.aq;
import com.yandex.mobile.ads.impl.bg;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.nativeads.af;
import com.yandex.mobile.ads.nativeads.bk;
import java.util.List;

public final class n implements bk {
    public final void a(@NonNull Context context, @NonNull b bVar) {
    }

    public final void a(@NonNull Context context, @NonNull b bVar, @Nullable af afVar) {
    }

    public final void a(@NonNull aq aqVar) {
    }

    public final void a(@NonNull a aVar) {
    }

    public final void a(@NonNull x xVar, @NonNull List<bg> list) {
    }

    public final void a(@NonNull af afVar) {
    }
}
