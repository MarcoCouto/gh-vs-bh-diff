package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.av;
import com.yandex.mobile.ads.impl.ay;
import com.yandex.mobile.ads.impl.az;
import com.yandex.mobile.ads.impl.ba;
import com.yandex.mobile.ads.impl.bb;
import com.yandex.mobile.ads.impl.be;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.nq;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.nativeads.u;

public final class l implements t {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final MediatedNativeAdapterListener f5853a;
    @NonNull
    private final av<MediatedNativeAdapter, MediatedNativeAdapterListener> b;

    public l(@NonNull u uVar, @NonNull be beVar) {
        fc s = uVar.s();
        az azVar = new az(s);
        bb bbVar = new bb(s);
        av avVar = new av(s, new k(), bbVar, new j(new ay(beVar, azVar, bbVar)), new ba(uVar, beVar));
        this.b = avVar;
        this.f5853a = new s(uVar, this.b);
    }

    public final void a(@NonNull Context context, @NonNull x<nq> xVar) {
        this.b.a(context, this.f5853a);
    }
}
