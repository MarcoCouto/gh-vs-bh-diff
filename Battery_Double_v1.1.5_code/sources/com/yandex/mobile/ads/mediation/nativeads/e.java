package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.nativeads.q;
import com.yandex.mobile.ads.nativeads.r;

public final class e implements r {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f5847a;

    interface a {
        void a(@NonNull q qVar);
    }

    e(@NonNull a aVar) {
        this.f5847a = aVar;
    }

    @NonNull
    public final q a(@NonNull Context context, @NonNull fc fcVar) {
        q qVar = new q(context, fcVar);
        this.f5847a.a(qVar);
        return qVar;
    }
}
