package com.yandex.mobile.ads.mediation.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class MediatedAdapterInfo {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f5831a;
    @Nullable
    private final String b;
    @Nullable
    private final String c;

    static final class Builder {
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public String f5832a;
        /* access modifiers changed from: private */
        @Nullable
        public String b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;

        Builder() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final MediatedAdapterInfo build() {
            return new MediatedAdapterInfo(this);
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setAdapterVersion(@NonNull String str) {
            this.f5832a = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setNetworkName(@NonNull String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setNetworkSdkVersion(@NonNull String str) {
            this.c = str;
            return this;
        }
    }

    private MediatedAdapterInfo(@NonNull Builder builder) {
        this.f5831a = builder.f5832a;
        this.b = builder.b;
        this.c = builder.c;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getAdapterVersion() {
        return this.f5831a;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getNetworkName() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String getNetworkSdkVersion() {
        return this.c;
    }
}
