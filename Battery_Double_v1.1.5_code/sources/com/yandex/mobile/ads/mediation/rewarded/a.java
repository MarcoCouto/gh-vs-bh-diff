package com.yandex.mobile.ads.mediation.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.av;
import com.yandex.mobile.ads.impl.ay;
import com.yandex.mobile.ads.impl.az;
import com.yandex.mobile.ads.impl.ba;
import com.yandex.mobile.ads.impl.bb;
import com.yandex.mobile.ads.impl.be;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.jm;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.rewarded.b;

public final class a implements jm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final av<MediatedRewardedAdapter, MediatedRewardedAdapterListener> f5863a;
    @NonNull
    private final c b = new c();
    @NonNull
    private final MediatedRewardedAdapterListener c;

    public a(@NonNull b bVar, @NonNull be beVar) {
        fc s = bVar.s();
        az azVar = new az(s);
        bb bbVar = new bb(s);
        b bVar2 = new b(new ay(beVar, azVar, bbVar));
        ba baVar = new ba(bVar, beVar);
        av avVar = new av(s, this.b, bbVar, bVar2, baVar);
        this.f5863a = avVar;
        this.c = new d(bVar, this.f5863a);
    }

    public final void a(@NonNull Context context) {
        this.f5863a.a(context);
    }

    public final boolean a() {
        return this.b.b();
    }

    public final void a(@NonNull Context context, @NonNull x<String> xVar) {
        this.f5863a.a(context, this.c);
    }

    public final void b() {
        MediatedRewardedAdapter a2 = this.b.a();
        if (a2 != null) {
            a2.showRewardedAd();
        }
    }
}
