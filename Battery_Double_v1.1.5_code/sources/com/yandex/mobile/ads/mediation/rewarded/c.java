package com.yandex.mobile.ads.mediation.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ax;
import java.util.Map;

final class c implements ax<MediatedRewardedAdapter, MediatedRewardedAdapterListener> {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private MediatedRewardedAdapter f5865a;

    c() {
    }

    public final /* synthetic */ void a(@NonNull Context context, @NonNull Object obj, @NonNull Object obj2, @NonNull Map map, @NonNull Map map2) {
        MediatedRewardedAdapter mediatedRewardedAdapter = (MediatedRewardedAdapter) obj;
        MediatedRewardedAdapterListener mediatedRewardedAdapterListener = (MediatedRewardedAdapterListener) obj2;
        this.f5865a = mediatedRewardedAdapter;
        mediatedRewardedAdapter.loadRewardedAd(context, mediatedRewardedAdapterListener, map, map2);
    }

    public final /* synthetic */ void a(@NonNull Object obj) {
        ((MediatedRewardedAdapter) obj).onInvalidate();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final MediatedRewardedAdapter a() {
        return this.f5865a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.f5865a != null && this.f5865a.isLoaded();
    }
}
