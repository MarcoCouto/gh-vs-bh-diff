package com.yandex.mobile.ads.mediation.rewarded;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.rewarded.Reward;

final class MediatedReward implements Reward {

    /* renamed from: a reason: collision with root package name */
    private final int f5862a;
    @NonNull
    private final String b;

    public MediatedReward(int i, @NonNull String str) {
        this.f5862a = i;
        this.b = str;
    }

    public final int getAmount() {
        return this.f5862a;
    }

    @NonNull
    public final String getType() {
        return this.b;
    }
}
