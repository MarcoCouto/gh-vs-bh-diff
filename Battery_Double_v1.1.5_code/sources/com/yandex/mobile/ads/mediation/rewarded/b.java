package com.yandex.mobile.ads.mediation.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.au;
import com.yandex.mobile.ads.impl.aw;
import com.yandex.mobile.ads.impl.ay;

final class b implements aw<MediatedRewardedAdapter> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ay<MediatedRewardedAdapter> f5864a;

    b(@NonNull ay<MediatedRewardedAdapter> ayVar) {
        this.f5864a = ayVar;
    }

    @Nullable
    public final au<MediatedRewardedAdapter> a(@NonNull Context context) {
        return this.f5864a.a(context, MediatedRewardedAdapter.class);
    }
}
