package com.yandex.mobile.ads.mediation.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.base.a;
import java.util.Map;

abstract class MediatedRewardedAdapter extends a {
    /* access modifiers changed from: 0000 */
    public abstract boolean isLoaded();

    /* access modifiers changed from: 0000 */
    public abstract void loadRewardedAd(@NonNull Context context, @NonNull MediatedRewardedAdapterListener mediatedRewardedAdapterListener, @NonNull Map<String, Object> map, @NonNull Map<String, String> map2);

    /* access modifiers changed from: 0000 */
    public abstract void onInvalidate();

    /* access modifiers changed from: 0000 */
    public abstract void showRewardedAd();

    MediatedRewardedAdapter() {
    }
}
