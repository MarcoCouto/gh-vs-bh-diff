package com.yandex.mobile.ads.mediation.rewarded;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.av;
import com.yandex.mobile.ads.rewarded.b;
import java.lang.ref.WeakReference;

final class d implements MediatedRewardedAdapterListener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<b> f5866a;
    @NonNull
    private final av<MediatedRewardedAdapter, MediatedRewardedAdapterListener> b;

    d(@NonNull b bVar, @NonNull av<MediatedRewardedAdapter, MediatedRewardedAdapterListener> avVar) {
        this.f5866a = new WeakReference<>(bVar);
        this.b = avVar;
    }

    public final void onRewardedAdClicked() {
        b bVar = (b) this.f5866a.get();
        if (bVar != null) {
            this.b.b(bVar.m());
        }
    }

    public final void onRewardedAdDismissed() {
        b bVar = (b) this.f5866a.get();
        if (bVar != null) {
            bVar.B();
        }
    }

    public final void onRewardedAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        b bVar = (b) this.f5866a.get();
        if (bVar != null) {
            this.b.a(bVar.m(), adRequestError, this);
        }
    }

    public final void onRewardedAdLeftApplication() {
        b bVar = (b) this.f5866a.get();
        if (bVar != null) {
            bVar.onAdLeftApplication();
        }
    }

    public final void onRewardedAdLoaded() {
        b bVar = (b) this.f5866a.get();
        if (bVar != null) {
            this.b.e(bVar.m());
            bVar.onAdLoaded();
        }
    }

    public final void onRewardedAdShown() {
        b bVar = (b) this.f5866a.get();
        if (bVar != null) {
            this.b.c(bVar.m());
            bVar.A();
        }
    }

    public final void onRewarded(@Nullable MediatedReward mediatedReward) {
        b bVar = (b) this.f5866a.get();
        if (bVar != null) {
            this.b.a(bVar.m(), bVar.y());
            bVar.E();
        }
    }
}
