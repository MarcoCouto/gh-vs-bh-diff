package com.yandex.mobile.ads.core.identifiers.ad.service;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.fu;
import com.yandex.mobile.ads.impl.hf;

public final class f {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5347a;
    @NonNull
    private final hf b = new hf();
    @NonNull
    private final d c = new d();
    @NonNull
    private final e d = new e();

    public f(@NonNull Context context) {
        this.f5347a = context.getApplicationContext();
    }

    @Nullable
    private fu a(@NonNull Intent intent) {
        try {
            c cVar = new c();
            if (this.f5347a.bindService(intent, cVar, 1)) {
                fu a2 = d.a(cVar);
                try {
                    this.f5347a.unbindService(cVar);
                    return a2;
                } catch (Throwable unused) {
                    return a2;
                }
            }
        } catch (Throwable unused2) {
        }
        return null;
    }

    @Nullable
    public final fu a() {
        Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
        intent.setPackage("com.google.android.gms");
        if (hf.a(this.f5347a, intent) != null) {
            return a(intent);
        }
        return null;
    }
}
