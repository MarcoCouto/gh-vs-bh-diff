package com.yandex.mobile.ads.core.identifiers.ad.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

final class c implements ServiceConnection {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final BlockingQueue<IBinder> f5345a = new LinkedBlockingQueue();

    c() {
    }

    public final void onServiceConnected(@NonNull ComponentName componentName, @NonNull IBinder iBinder) {
        try {
            this.f5345a.put(iBinder);
        } catch (InterruptedException unused) {
        }
    }

    public final void onServiceDisconnected(@NonNull ComponentName componentName) {
        try {
            this.f5345a.clear();
        } catch (UnsupportedOperationException unused) {
        }
    }

    @Nullable
    public final IBinder a() throws InterruptedException {
        return (IBinder) this.f5345a.poll(5, TimeUnit.SECONDS);
    }
}
