package com.yandex.mobile.ads.core.identifiers.ad.service;

import android.os.IBinder;
import android.os.IInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.fu;
import com.yandex.mobile.ads.impl.fv;

final class d {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fv f5346a = new fv();
    @NonNull
    private final b b = new b();

    d() {
    }

    @Nullable
    static fu a(@NonNull c cVar) {
        a aVar;
        try {
            IBinder a2 = cVar.a();
            if (a2 == null) {
                return null;
            }
            IInterface queryLocalInterface = a2.queryLocalInterface(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
            if (queryLocalInterface instanceof a) {
                aVar = (a) queryLocalInterface;
            } else {
                aVar = new GmsServiceAdvertisingInfoReader(a2);
            }
            return fv.a(aVar.readAdvertisingId(), aVar.readAdTrackingLimited());
        } catch (InterruptedException unused) {
            return null;
        }
    }
}
