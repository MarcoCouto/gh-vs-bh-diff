package com.yandex.mobile.ads.core.identifiers.ad.service;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class GmsServiceAdvertisingInfoReader implements IInterface, a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final IBinder f5344a;

    GmsServiceAdvertisingInfoReader(@NonNull IBinder iBinder) {
        this.f5344a = iBinder;
    }

    @NonNull
    public IBinder asBinder() {
        return this.f5344a;
    }

    /* JADX INFO: finally extract failed */
    @Nullable
    public String readAdvertisingId() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
            this.f5344a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            String readString = obtain2.readString();
            obtain2.recycle();
            obtain.recycle();
            return readString;
        } catch (Throwable th) {
            obtain2.recycle();
            obtain.recycle();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    @Nullable
    public Boolean readAdTrackingLimited() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
            boolean z = true;
            obtain.writeInt(1);
            this.f5344a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = false;
            }
            Boolean valueOf = Boolean.valueOf(z);
            obtain2.recycle();
            obtain.recycle();
            return valueOf;
        } catch (Throwable th) {
            obtain2.recycle();
            obtain.recycle();
            throw th;
        }
    }
}
