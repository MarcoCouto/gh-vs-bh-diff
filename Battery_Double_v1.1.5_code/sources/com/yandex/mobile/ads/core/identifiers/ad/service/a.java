package com.yandex.mobile.ads.core.identifiers.ad.service;

import android.support.annotation.Nullable;

interface a {
    @Nullable
    Boolean readAdTrackingLimited();

    @Nullable
    String readAdvertisingId();
}
