package com.yandex.mobile.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.al;
import com.yandex.mobile.ads.impl.al.a;
import java.io.Serializable;

public abstract class c implements Serializable {
    private static final long serialVersionUID = -7571518881522543353L;
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final al f5343a;

    protected c(int i, int i2, @NonNull a aVar) {
        this.f5343a = new al(i, i2, aVar);
    }

    public int getHeight() {
        return this.f5343a.b();
    }

    public int getHeightInPixels(@NonNull Context context) {
        return this.f5343a.c(context);
    }

    public int getWidth() {
        return this.f5343a.a();
    }

    public int getWidthInPixels(@NonNull Context context) {
        return this.f5343a.d(context);
    }

    public int getHeight(@NonNull Context context) {
        return this.f5343a.a(context);
    }

    public int getWidth(@NonNull Context context) {
        return this.f5343a.b(context);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final al a() {
        return this.f5343a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f5343a.equals(((c) obj).f5343a);
    }

    public int hashCode() {
        return this.f5343a.hashCode();
    }

    public String toString() {
        return this.f5343a.toString();
    }
}
