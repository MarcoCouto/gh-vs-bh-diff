package com.yandex.mobile.ads;

import com.smaato.sdk.core.api.VideoType;

public enum b {
    BANNER("banner"),
    INTERSTITIAL(VideoType.INTERSTITIAL),
    REWARDED("rewarded"),
    NATIVE("native"),
    VASTVIDEO("vastvideo");
    
    private final String f;

    private b(String str) {
        this.f = str;
    }

    public final String a() {
        return this.f;
    }

    public static b a(String str) {
        b[] values;
        for (b bVar : values()) {
            if (bVar.f.equals(str)) {
                return bVar;
            }
        }
        return null;
    }
}
