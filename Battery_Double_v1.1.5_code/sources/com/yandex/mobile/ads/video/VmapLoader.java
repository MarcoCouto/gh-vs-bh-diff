package com.yandex.mobile.ads.video;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.sf;
import com.yandex.mobile.ads.impl.si;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class VmapLoader {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final sf f5983a;
    @NonNull
    private final si b = new si();

    public static abstract class OnVmapLoadedListener {
        public abstract void onVmapFailedToLoad(@NonNull VmapError vmapError);

        public abstract void onVmapLoaded(@NonNull Vmap vmap);
    }

    public VmapLoader(@NonNull Context context) {
        this.f5983a = new sf(context);
    }

    public final void cancelLoading() {
        this.b.a();
    }

    public final void loadVmap(@NonNull Context context, @NonNull VmapRequestConfiguration vmapRequestConfiguration) {
        this.f5983a.a(context, vmapRequestConfiguration, this.b);
    }

    public final void setOnVmapLoadedListener(@Nullable OnVmapLoadedListener onVmapLoadedListener) {
        this.b.a(onVmapLoadedListener);
    }
}
