package com.yandex.mobile.ads.video;

import com.yandex.mobile.ads.impl.sm;
import com.yandex.mobile.ads.impl.sn;

public class VideoAdError {

    /* renamed from: a reason: collision with root package name */
    private final int f5978a;
    private final String b;
    private final String c;

    public interface Code {
    }

    private VideoAdError(int i, String str) {
        this(i, str, 0);
    }

    private VideoAdError(int i, String str, byte b2) {
        this.f5978a = i;
        this.b = str;
        this.c = null;
    }

    public int getCode() {
        return this.f5978a;
    }

    public String getDescription() {
        return this.b;
    }

    public String getRawResponse() {
        return this.c;
    }

    public static VideoAdError createInternalError(sn snVar) {
        return new VideoAdError(1, "Internal error. Failed to parse response");
    }

    public static VideoAdError createRetriableError(String str) {
        return new VideoAdError(4, str);
    }

    public static VideoAdError createInternalError(String str) {
        return new VideoAdError(1, str);
    }

    public static final VideoAdError createNoAdError(sm smVar) {
        return new VideoAdError(3, smVar.getMessage());
    }

    public static VideoAdError createConnectionError(String str) {
        return new VideoAdError(2, str);
    }
}
