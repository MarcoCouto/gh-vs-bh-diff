package com.yandex.mobile.ads.video.models.vmap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.tx;
import com.yandex.mobile.ads.video.VastRequestConfiguration;

public final class b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final AdBreak f5995a;

    public b(@NonNull VastRequestConfiguration vastRequestConfiguration) {
        this.f5995a = vastRequestConfiguration.getAdBreak();
    }

    @NonNull
    public final c a() {
        return this.f5995a.getAdSource().a();
    }

    @Nullable
    public final String b() {
        return this.f5995a.getAdSource().getId();
    }

    @Nullable
    public final String c() {
        tx a2 = this.f5995a.a();
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    @Nullable
    public final String d() {
        tx a2 = this.f5995a.a();
        if (a2 != null) {
            return a2.b();
        }
        return null;
    }
}
