package com.yandex.mobile.ads.video.models.ad;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.sl;

public class WrapperConfigurationProvider {
    @NonNull
    private final VideoAd mWrapperAd;

    public WrapperConfigurationProvider(@NonNull VideoAd videoAd) {
        this.mWrapperAd = videoAd;
    }

    @Nullable
    public sl getWrapperConfiguration() {
        return this.mWrapperAd.getWrapperConfiguration();
    }
}
