package com.yandex.mobile.ads.video.models.vmap;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class AdSource implements Parcelable {
    public static final Creator<AdSource> CREATOR = new Creator<AdSource>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new AdSource[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new AdSource(parcel);
        }
    };
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final c f5992a;
    @Nullable
    private final Boolean b;
    @Nullable
    private final Boolean c;
    @Nullable
    private final String d;

    public final int describeContents() {
        return 0;
    }

    AdSource(@NonNull c cVar, @Nullable Boolean bool, @Nullable Boolean bool2, @Nullable String str) {
        this.d = str;
        this.f5992a = cVar;
        this.b = bool;
        this.c = bool2;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final c a() {
        return this.f5992a;
    }

    @Nullable
    public final String getId() {
        return this.d;
    }

    @Nullable
    public final Boolean isAllowMultipleAds() {
        return this.b;
    }

    @Nullable
    public final Boolean isFollowRedirects() {
        return this.c;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeParcelable(this.f5992a, i);
        parcel.writeValue(this.b);
        parcel.writeValue(this.c);
        parcel.writeString(this.d);
    }

    protected AdSource(@NonNull Parcel parcel) {
        this.f5992a = (c) parcel.readParcelable(c.class.getClassLoader());
        this.b = (Boolean) parcel.readValue(Boolean.class.getClassLoader());
        this.c = (Boolean) parcel.readValue(Boolean.class.getClassLoader());
        this.d = parcel.readString();
    }
}
