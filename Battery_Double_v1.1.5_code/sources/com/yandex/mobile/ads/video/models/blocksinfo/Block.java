package com.yandex.mobile.ads.video.models.blocksinfo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;

public final class Block implements Parcelable {
    public static final Creator<Block> CREATOR = new Creator<Block>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new Block[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new Block(parcel);
        }
    };
    private int mDuration;
    private String mId;
    private int mPositionsCount;
    private int mStartTime;
    private Type mType;

    public enum Type {
        PREROLL(BreakId.PREROLL),
        MIDROLL(BreakId.MIDROLL),
        POSTROLL("postroll"),
        PAUSEROLL(BreakId.PAUSEROLL),
        OVERLAYROLL("overlayroll");
        
        private final String mType;

        private Type(String str) {
            this.mType = str;
        }

        public final String toString() {
            return this.mType;
        }

        public static Type getByType(String str) {
            Type[] values;
            for (Type type : values()) {
                if (type.mType.equals(str)) {
                    return type;
                }
            }
            return null;
        }
    }

    public final int describeContents() {
        return 0;
    }

    public final String getId() {
        return this.mId;
    }

    private void setId(String str) {
        this.mId = str;
    }

    public final Type getType() {
        return this.mType;
    }

    private void setType(String str) {
        this.mType = Type.getByType(str);
    }

    public final int getStartTime() {
        return this.mStartTime;
    }

    private void setStartTime(String str) {
        try {
            this.mStartTime = Integer.valueOf(str).intValue();
        } catch (Exception unused) {
        }
    }

    public final int getDuration() {
        return this.mDuration;
    }

    private void setDuration(String str) {
        try {
            this.mDuration = Integer.valueOf(str).intValue();
        } catch (Exception unused) {
        }
    }

    public final int getPositionsCount() {
        return this.mPositionsCount;
    }

    private void setPositionsCount(String str) {
        try {
            this.mPositionsCount = Integer.valueOf(str).intValue();
        } catch (Exception unused) {
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Block block = (Block) obj;
        if (this.mDuration != block.mDuration || this.mPositionsCount != block.mPositionsCount || this.mStartTime != block.mStartTime) {
            return false;
        }
        if (this.mId == null ? block.mId == null : this.mId.equals(block.mId)) {
            return this.mType == block.mType;
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.mId != null ? this.mId.hashCode() : 0) * 31;
        if (this.mType != null) {
            i = this.mType.hashCode();
        }
        return ((((((hashCode + i) * 31) + this.mStartTime) * 31) + this.mDuration) * 31) + this.mPositionsCount;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mId);
        parcel.writeInt(this.mType == null ? -1 : this.mType.ordinal());
        parcel.writeInt(this.mStartTime);
        parcel.writeInt(this.mDuration);
        parcel.writeInt(this.mPositionsCount);
    }

    private Block() {
    }

    private Block(Parcel parcel) {
        Type type;
        this.mId = parcel.readString();
        int readInt = parcel.readInt();
        if (readInt == -1) {
            type = null;
        } else {
            type = Type.values()[readInt];
        }
        this.mType = type;
        this.mStartTime = parcel.readInt();
        this.mDuration = parcel.readInt();
        this.mPositionsCount = parcel.readInt();
    }
}
