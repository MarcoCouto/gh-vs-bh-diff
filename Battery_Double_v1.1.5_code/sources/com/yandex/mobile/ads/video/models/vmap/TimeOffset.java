package com.yandex.mobile.ads.video.models.vmap;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public final class TimeOffset implements Parcelable {
    public static final Creator<TimeOffset> CREATOR = new Creator<TimeOffset>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new TimeOffset[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new TimeOffset(parcel);
        }
    };
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5993a;

    public final int describeContents() {
        return 0;
    }

    TimeOffset(@NonNull String str) {
        this.f5993a = str;
    }

    @NonNull
    public final String getRawValue() {
        return this.f5993a;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.f5993a);
    }

    protected TimeOffset(@NonNull Parcel parcel) {
        this.f5993a = parcel.readString();
    }
}
