package com.yandex.mobile.ads.video.models.vmap;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public final class c implements Parcelable {
    public static final Creator<c> CREATOR = new Creator<c>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new c[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new c(parcel);
        }
    };
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5996a;
    @NonNull
    private final String b;

    public final int describeContents() {
        return 0;
    }

    c(@NonNull String str, @NonNull String str2) {
        this.b = str2;
        this.f5996a = str;
    }

    @NonNull
    public final String a() {
        return this.f5996a;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.f5996a);
        parcel.writeString(this.b);
    }

    protected c(@NonNull Parcel parcel) {
        this.f5996a = parcel.readString();
        this.b = parcel.readString();
    }
}
