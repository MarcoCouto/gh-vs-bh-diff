package com.yandex.mobile.ads.video.models.ad;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Pair;
import com.yandex.mobile.ads.impl.hd;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Creative implements Parcelable {
    public static final Creator<Creative> CREATOR = new Creator<Creative>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new Creative[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new Creative(parcel);
        }
    };
    private final ArrayList<String> mClickThroughEvents = new ArrayList<>();
    private int mDurationMillis;
    private List<Icon> mIcons = new ArrayList();
    private String mId;
    private List<MediaFile> mMediaFiles = new ArrayList();
    private Map<String, List<String>> mTrackingEvents = new HashMap();

    public int describeContents() {
        return 0;
    }

    private Creative() {
    }

    public String getId() {
        return this.mId;
    }

    private void setId(String str) {
        this.mId = str;
    }

    public int getDurationMillis() {
        return this.mDurationMillis;
    }

    private void setDurationMillis(int i) {
        this.mDurationMillis = i;
    }

    private void setDurationHHMMSS(String str) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
            this.mDurationMillis = (int) (simpleDateFormat.parse(str).getTime() - simpleDateFormat.parse("00:00:00").getTime());
        } catch (ParseException unused) {
        }
    }

    public List<MediaFile> getMediaFiles() {
        return Collections.unmodifiableList(this.mMediaFiles);
    }

    public List<Icon> getIcons() {
        return Collections.unmodifiableList(this.mIcons);
    }

    public String getClickThroughUrl() {
        if (this.mClickThroughEvents.isEmpty()) {
            return null;
        }
        return (String) this.mClickThroughEvents.get(0);
    }

    /* access modifiers changed from: 0000 */
    public void addTrackingEvent(String str, String str2) {
        List list = (List) this.mTrackingEvents.get(str);
        if (list == null) {
            list = new ArrayList();
            this.mTrackingEvents.put(str, list);
        }
        list.add(str2);
    }

    private void addTrackingEvents(Collection<Pair<String, String>> collection) {
        for (Pair pair : hd.a(collection)) {
            addTrackingEvent((String) pair.first, (String) pair.second);
        }
    }

    private void addMediaFile(MediaFile mediaFile) {
        this.mMediaFiles.add(mediaFile);
    }

    private void addMediaFiles(Collection<MediaFile> collection) {
        for (MediaFile addMediaFile : hd.a(collection)) {
            addMediaFile(addMediaFile);
        }
    }

    /* access modifiers changed from: 0000 */
    public void addIcons(Collection<Icon> collection) {
        this.mIcons.addAll(hd.a(collection));
    }

    private void addClickThroughUrls(Collection<String> collection) {
        this.mClickThroughEvents.addAll(hd.a(collection));
    }

    public Map<String, List<String>> getTrackingEvents() {
        return Collections.unmodifiableMap(this.mTrackingEvents);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Creative creative = (Creative) obj;
        if (this.mDurationMillis != creative.mDurationMillis) {
            return false;
        }
        if (this.mClickThroughEvents == null ? creative.mClickThroughEvents != null : !this.mClickThroughEvents.equals(creative.mClickThroughEvents)) {
            return false;
        }
        if (this.mId == null ? creative.mId != null : !this.mId.equals(creative.mId)) {
            return false;
        }
        if (this.mMediaFiles == null ? creative.mMediaFiles == null : this.mMediaFiles.equals(creative.mMediaFiles)) {
            return this.mTrackingEvents == null ? creative.mTrackingEvents == null : this.mTrackingEvents.equals(creative.mTrackingEvents);
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.mId != null ? this.mId.hashCode() : 0) * 31) + this.mDurationMillis) * 31) + (this.mMediaFiles != null ? this.mMediaFiles.hashCode() : 0)) * 31) + (this.mTrackingEvents != null ? this.mTrackingEvents.hashCode() : 0)) * 31;
        if (this.mClickThroughEvents != null) {
            i = this.mClickThroughEvents.hashCode();
        }
        return hashCode + i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mId);
        parcel.writeInt(this.mDurationMillis);
        parcel.writeTypedList(this.mMediaFiles);
        parcel.writeInt(this.mTrackingEvents.size());
        for (Entry entry : this.mTrackingEvents.entrySet()) {
            parcel.writeString((String) entry.getKey());
            parcel.writeList((List) entry.getValue());
        }
        parcel.writeInt(this.mClickThroughEvents.size());
        Iterator it = this.mClickThroughEvents.iterator();
        while (it.hasNext()) {
            parcel.writeString((String) it.next());
        }
    }

    public Creative(Parcel parcel) {
        this.mId = parcel.readString();
        this.mDurationMillis = parcel.readInt();
        parcel.readTypedList(this.mMediaFiles, MediaFile.CREATOR);
        this.mTrackingEvents = new HashMap();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            this.mTrackingEvents.put(parcel.readString(), parcel.readArrayList(getClass().getClassLoader()));
        }
        int readInt2 = parcel.readInt();
        for (int i2 = 0; i2 < readInt2; i2++) {
            this.mClickThroughEvents.add(parcel.readString());
        }
    }
}
