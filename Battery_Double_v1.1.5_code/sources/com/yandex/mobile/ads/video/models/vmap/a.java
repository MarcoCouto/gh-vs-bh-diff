package com.yandex.mobile.ads.video.models.vmap;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.tx;
import java.util.List;

public final class a {
    public static void a(@NonNull List<AdBreak> list, @NonNull tx txVar) {
        for (AdBreak a2 : list) {
            a2.a(txVar);
        }
    }
}
