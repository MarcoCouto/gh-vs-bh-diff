package com.yandex.mobile.ads.video.models.vmap;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.tx;
import com.yandex.mobile.ads.video.models.common.Extension;
import java.util.List;

public final class AdBreak implements Parcelable {
    public static final Creator<AdBreak> CREATOR = new Creator<AdBreak>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new AdBreak[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new AdBreak(parcel);
        }
    };
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final AdSource f5991a;
    @Nullable
    private final String b;
    @Nullable
    private final Long c;
    @NonNull
    private final TimeOffset d;
    @NonNull
    private final List<String> e;
    @NonNull
    private final List<Extension> f;
    @Nullable
    private tx g;

    public static final class BreakId {
        public static final String INPAGE = "inpage";
        public static final String MIDROLL = "midroll";
        public static final String PAUSEROLL = "pauseroll";
        public static final String POSTROLL = "postroll";
        public static final String PREROLL = "preroll";
    }

    public static final class BreakType {
        public static final String DISPLAY = "display";
        public static final String LINEAR = "linear";
        public static final String NONLINEAR = "nonlinear";
    }

    public final int describeContents() {
        return 0;
    }

    AdBreak(@NonNull AdSource adSource, @Nullable String str, @Nullable Long l, @NonNull TimeOffset timeOffset, @NonNull List<String> list, @NonNull List<Extension> list2) {
        this.f5991a = adSource;
        this.b = str;
        this.e = list;
        this.c = l;
        this.d = timeOffset;
        this.f = list2;
    }

    @Nullable
    public final String getBreakId() {
        return this.b;
    }

    @Nullable
    public final Long getRepeatAfterMillis() {
        return this.c;
    }

    @NonNull
    public final TimeOffset getTimeOffset() {
        return this.d;
    }

    @NonNull
    public final List<String> getBreakTypes() {
        return this.e;
    }

    @NonNull
    public final List<Extension> getExtensions() {
        return this.f;
    }

    @NonNull
    public final AdSource getAdSource() {
        return this.f5991a;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull tx txVar) {
        this.g = txVar;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final tx a() {
        return this.g;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeParcelable(this.f5991a, i);
        parcel.writeString(this.b);
        parcel.writeValue(this.c);
        parcel.writeParcelable(this.d, i);
        parcel.writeStringList(this.e);
        parcel.writeTypedList(this.f);
        parcel.writeParcelable(this.g, i);
    }

    protected AdBreak(@NonNull Parcel parcel) {
        this.f5991a = (AdSource) parcel.readParcelable(AdSource.class.getClassLoader());
        this.b = parcel.readString();
        this.c = (Long) parcel.readValue(Long.class.getClassLoader());
        this.d = (TimeOffset) parcel.readParcelable(TimeOffset.class.getClassLoader());
        this.e = parcel.createStringArrayList();
        this.f = parcel.createTypedArrayList(Extension.CREATOR);
        this.g = (tx) parcel.readParcelable(tx.class.getClassLoader());
    }
}
