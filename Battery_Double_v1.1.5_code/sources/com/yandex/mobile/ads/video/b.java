package com.yandex.mobile.ads.video;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.fp;
import com.yandex.mobile.ads.impl.fr;
import com.yandex.mobile.ads.impl.sg;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5989a = new Object();
    private static volatile b b;
    @NonNull
    private final sg c;
    @NonNull
    private final fr d;
    @Nullable
    private fr e;

    @NonNull
    public static b a(@NonNull Context context) {
        if (b == null) {
            synchronized (f5989a) {
                if (b == null) {
                    b = new b(context);
                }
            }
        }
        return b;
    }

    private b(@NonNull Context context) {
        this.c = sg.a(context);
        this.d = fp.a(context);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull BlocksInfoRequest blocksInfoRequest) {
        this.c.a(context, blocksInfoRequest, a());
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull VideoAdRequest videoAdRequest) {
        this.c.a(context, videoAdRequest, a());
    }

    @NonNull
    private fr a() {
        return this.e != null ? this.e : this.d;
    }
}
