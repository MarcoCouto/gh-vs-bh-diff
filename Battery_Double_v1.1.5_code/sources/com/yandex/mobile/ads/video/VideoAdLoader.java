package com.yandex.mobile.ads.video;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.sf;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class VideoAdLoader {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final sf f5979a;
    @NonNull
    private final a b = new a();

    public static abstract class OnVideoAdLoadedListener {
        /* access modifiers changed from: 0000 */
        public void onRawVideoAdLoaded(@Nullable String str) {
        }

        public abstract void onVideoAdFailedToLoad(@NonNull VideoAdError videoAdError);

        public abstract void onVideoAdLoaded(@NonNull List<VideoAd> list);
    }

    public VideoAdLoader(@NonNull Context context) {
        this.f5979a = new sf(context);
    }

    public final void cancelLoading() {
        this.b.a();
    }

    public final void loadAd(@NonNull Context context, @NonNull VastRequestConfiguration vastRequestConfiguration) {
        this.f5979a.a(context, vastRequestConfiguration, this.b);
    }

    public final void setOnVideoAdLoadedListener(@Nullable OnVideoAdLoadedListener onVideoAdLoadedListener) {
        this.b.a(onVideoAdLoadedListener);
    }
}
