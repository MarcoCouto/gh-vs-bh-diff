package com.yandex.mobile.ads.video;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.sk;
import com.yandex.mobile.ads.video.VideoAdLoader.OnVideoAdLoadedListener;

public final class a implements RequestListener<sk> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Object f5986a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @Nullable
    public OnVideoAdLoadedListener c;

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        final sk skVar = (sk) obj;
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.f5986a) {
                    if (a.this.c != null) {
                        a.this.c.onRawVideoAdLoaded(skVar.b());
                        a.this.c.onVideoAdLoaded(skVar.a().b());
                    }
                }
            }
        });
    }

    a() {
    }

    public final void onFailure(@NonNull final VideoAdError videoAdError) {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.f5986a) {
                    if (a.this.c != null) {
                        a.this.c.onVideoAdFailedToLoad(videoAdError);
                    }
                }
            }
        });
    }

    public final void a() {
        this.b.removeCallbacksAndMessages(null);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable OnVideoAdLoadedListener onVideoAdLoadedListener) {
        synchronized (f5986a) {
            this.c = onVideoAdLoadedListener;
        }
    }
}
