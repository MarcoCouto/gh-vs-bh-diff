package com.yandex.mobile.ads.video;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.se;
import com.yandex.mobile.ads.video.models.vmap.AdBreak;
import java.util.Map;

public final class VastRequestConfiguration {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final AdBreak f5976a;
    @Nullable
    private final Map<String, String> b;

    public static final class Builder {
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final AdBreak f5977a;
        /* access modifiers changed from: private */
        @Nullable
        public Map<String, String> b;

        public Builder(@NonNull AdBreak adBreak) {
            this.f5977a = adBreak;
            se.a((Object) this.f5977a, "AdBreak");
        }

        public final VastRequestConfiguration build() {
            return new VastRequestConfiguration(this, 0);
        }

        @NonNull
        public final Builder setParameters(@NonNull Map<String, String> map) {
            this.b = map;
            return this;
        }
    }

    /* synthetic */ VastRequestConfiguration(Builder builder, byte b2) {
        this(builder);
    }

    private VastRequestConfiguration(@NonNull Builder builder) {
        this.f5976a = builder.f5977a;
        this.b = builder.b;
    }

    @NonNull
    public final AdBreak getAdBreak() {
        return this.f5976a;
    }

    @Nullable
    public final Map<String, String> getParameters() {
        return this.b;
    }
}
