package com.yandex.mobile.ads.video;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.se;

public final class VmapRequestConfiguration {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5984a;
    @NonNull
    private final String b;

    public static final class Builder {
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final String f5985a;
        /* access modifiers changed from: private */
        @NonNull
        public String b = "0";

        public Builder(@NonNull String str) {
            this.f5985a = str;
            se.a(this.f5985a, "PageId");
        }

        public final VmapRequestConfiguration build() {
            return new VmapRequestConfiguration(this, 0);
        }

        public final Builder setCategory(@NonNull String str) {
            if (!TextUtils.isEmpty(str)) {
                this.b = str;
                return this;
            }
            throw new IllegalArgumentException("Passed categoryId is empty");
        }
    }

    /* synthetic */ VmapRequestConfiguration(Builder builder, byte b2) {
        this(builder);
    }

    private VmapRequestConfiguration(@NonNull Builder builder) {
        this.f5984a = builder.b;
        this.b = builder.f5985a;
    }

    @NonNull
    public final String getCategoryId() {
        return this.f5984a;
    }

    @NonNull
    public final String getPageId() {
        return this.b;
    }
}
