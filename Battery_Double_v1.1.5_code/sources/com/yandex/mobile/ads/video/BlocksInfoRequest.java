package com.yandex.mobile.ads.video;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.se;
import com.yandex.mobile.ads.video.models.blocksinfo.BlocksInfo;

public final class BlocksInfoRequest {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5974a;
    @NonNull
    private final String b;
    @NonNull
    private final String c;
    @Nullable
    private final RequestListener<BlocksInfo> d;

    public static final class Builder {
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final Context f5975a;
        /* access modifiers changed from: private */
        @NonNull
        public final String b;
        /* access modifiers changed from: private */
        @Nullable
        public final RequestListener<BlocksInfo> c;
        /* access modifiers changed from: private */
        @NonNull
        public String d = "0";

        public Builder(@NonNull Context context, @NonNull String str, @Nullable RequestListener<BlocksInfo> requestListener) {
            this.f5975a = context.getApplicationContext();
            this.b = str;
            this.c = requestListener;
            se.a(this.b, "PageId");
        }

        public final BlocksInfoRequest build() {
            return new BlocksInfoRequest(this, 0);
        }

        public final Builder setCategory(@NonNull String str) {
            if (!TextUtils.isEmpty(str)) {
                this.d = str;
                return this;
            }
            throw new IllegalArgumentException("categoryId is empty");
        }
    }

    /* synthetic */ BlocksInfoRequest(Builder builder, byte b2) {
        this(builder);
    }

    private BlocksInfoRequest(@NonNull Builder builder) {
        this.f5974a = builder.f5975a;
        this.b = builder.b;
        this.c = builder.d;
        this.d = builder.c;
    }

    @Nullable
    public final RequestListener<BlocksInfo> getRequestListener() {
        return this.d;
    }

    @NonNull
    public final String getPartnerId() {
        return this.b;
    }

    @NonNull
    public final String getCategoryId() {
        return this.c;
    }

    @NonNull
    public final Context getContext() {
        return this.f5974a;
    }
}
