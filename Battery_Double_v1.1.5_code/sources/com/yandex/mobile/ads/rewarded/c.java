package com.yandex.mobile.ads.rewarded;

import android.support.annotation.NonNull;

public final class c implements Reward {

    /* renamed from: a reason: collision with root package name */
    private final int f5973a;
    @NonNull
    private final String b;

    public c(int i, @NonNull String str) {
        this.f5973a = i;
        this.b = str;
    }

    public final int getAmount() {
        return this.f5973a;
    }

    @NonNull
    public final String getType() {
        return this.b;
    }
}
