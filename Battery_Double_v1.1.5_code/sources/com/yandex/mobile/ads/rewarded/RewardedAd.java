package com.yandex.mobile.ads.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.impl.ac;
import com.yandex.mobile.ads.impl.di;
import com.yandex.mobile.ads.impl.hj;

public final class RewardedAd extends hj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final b f5962a;

    public RewardedAd(@NonNull Context context) {
        super(context);
        a aVar = new a(context);
        this.f5962a = new b(context, aVar);
        aVar.a(this.f5962a.s());
    }

    public final String getBlockId() {
        return this.f5962a.r();
    }

    public final void loadAd(AdRequest adRequest) {
        this.f5962a.a(adRequest);
    }

    public final boolean isLoaded() {
        return this.f5962a.z();
    }

    public final void setBlockId(String str) {
        this.f5962a.a_(str);
    }

    public final void setUserId(String str) {
        this.f5962a.c(str);
    }

    public final void setRewardedAdEventListener(@Nullable RewardedAdEventListener rewardedAdEventListener) {
        this.f5962a.a(rewardedAdEventListener);
    }

    public final void shouldOpenLinksInApp(boolean z) {
        this.f5962a.a_(z);
    }

    public final void show() {
        if (this.f5962a.z()) {
            this.f5962a.a();
        }
    }

    public final void destroy() {
        if (!di.a((ac) this.f5962a)) {
            this.f5962a.e();
        }
    }
}
