package com.yandex.mobile.ads.rewarded;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.bi;
import com.yandex.mobile.ads.impl.dw;
import com.yandex.mobile.ads.impl.hm;
import com.yandex.mobile.ads.impl.jm;
import com.yandex.mobile.ads.impl.jn;
import com.yandex.mobile.ads.impl.rz;
import com.yandex.mobile.ads.impl.sa;
import com.yandex.mobile.ads.impl.v;
import com.yandex.mobile.ads.impl.x;

public final class b extends hm {
    @NonNull
    private final a g;
    @NonNull
    private final dw h = new dw();
    @NonNull
    private final sa i;
    @Nullable
    private rz j;

    public b(@NonNull Context context, @NonNull a aVar) {
        super(context, com.yandex.mobile.ads.b.REWARDED, aVar);
        this.g = aVar;
        this.i = new sa(aVar);
    }

    public final void a(int i2, @Nullable Bundle bundle) {
        if (i2 != 13) {
            super.a(i2, bundle);
        } else {
            E();
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final jm a(@NonNull jn jnVar) {
        return jnVar.a(this);
    }

    public final void a(@NonNull x<String> xVar) {
        bi r = xVar.r();
        boolean z = false;
        if (r != null && (!r.c() ? r.a() != null : r.b() != null)) {
            z = true;
        }
        if (z) {
            super.a(xVar);
        } else {
            onAdFailedToLoad(v.e);
        }
    }

    public final void a() {
        this.j = this.i.a(this.b, this.e, this.f);
        super.a();
    }

    public final void E() {
        if (this.j != null) {
            this.j.a();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable RewardedAdEventListener rewardedAdEventListener) {
        this.g.a(rewardedAdEventListener);
    }

    /* access modifiers changed from: 0000 */
    public final void c(@Nullable String str) {
        this.e.e(str);
    }
}
