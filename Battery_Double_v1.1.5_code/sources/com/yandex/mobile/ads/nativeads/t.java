package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.yandex.mobile.ads.nativeads.ay;

public abstract class t<T extends ay> extends FrameLayout {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private T f5930a;

    public t(Context context) {
        super(context);
    }

    public t(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public t(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public final void a(@NonNull T t) {
        this.f5930a = t;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f5930a != null) {
            this.f5930a.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.f5930a != null) {
            this.f5930a.c();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        StringBuilder sb = new StringBuilder("onWindowVisibilityChanged(), windowVisibility = ");
        sb.append(i);
        sb.append(", this.getVisibility = ");
        sb.append(getVisibility());
        a((i == 0 && getVisibility() == 0) ? 0 : 8);
        super.onWindowVisibilityChanged(i);
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(view, i);
        StringBuilder sb = new StringBuilder("onVisibilityChanged(), changedView = ");
        sb.append(view);
        sb.append(", viewVisibility = ");
        sb.append(i);
        a((i != 0 || !isShown()) ? 8 : 0);
    }

    private void a(int i) {
        if (this.f5930a != null) {
            this.f5930a.a(i);
        }
    }

    @Nullable
    public T getNativeAd() {
        return this.f5930a;
    }
}
