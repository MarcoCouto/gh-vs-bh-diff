package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ki;
import com.yandex.mobile.ads.impl.kk;
import com.yandex.mobile.ads.impl.lh;
import com.yandex.mobile.ads.impl.ni;
import com.yandex.mobile.ads.impl.np;

public final class ak implements bd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final np f5888a;
    @Nullable
    private af b;

    ak(@NonNull np npVar) {
        this.f5888a = npVar;
    }

    public final void a(@NonNull af afVar) {
        afVar.a();
    }

    public final void a(@NonNull af afVar, @NonNull e eVar) {
        this.b = afVar;
        lh lhVar = new lh(afVar, eVar, this.f5888a.a());
        for (ni niVar : this.f5888a.c()) {
            ki a2 = afVar.a(niVar);
            if (a2 != null) {
                a2.a(niVar.c());
                a2.a(niVar, lhVar);
            }
        }
    }

    public final void a() {
        if (this.b != null) {
            for (ni niVar : this.f5888a.c()) {
                ki a2 = this.b.a(niVar);
                if (a2 instanceof kk) {
                    ((kk) a2).c(niVar.c());
                }
            }
        }
    }
}
