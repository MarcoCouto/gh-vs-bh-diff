package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class NativeAdViewBinder {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final View f5875a;
    @Nullable
    private final TextView b;
    @Nullable
    private final TextView c;
    @Nullable
    private final Button d;
    @Nullable
    private final TextView e;
    @Nullable
    private final ImageView f;
    @Nullable
    private final Button g;
    @Nullable
    private final ImageView h;
    @Nullable
    private final ImageView i;
    @Nullable
    private final MediaView j;
    @Nullable
    private final TextView k;
    @Nullable
    private final View l;
    @Nullable
    private final TextView m;
    @Nullable
    private final TextView n;
    @Nullable
    private final TextView o;
    @Nullable
    private final TextView p;

    public static final class Builder {
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final View f5876a;
        /* access modifiers changed from: private */
        @Nullable
        public TextView b;
        /* access modifiers changed from: private */
        @Nullable
        public TextView c;
        /* access modifiers changed from: private */
        @Nullable
        public Button d;
        /* access modifiers changed from: private */
        @Nullable
        public TextView e;
        /* access modifiers changed from: private */
        @Nullable
        public ImageView f;
        /* access modifiers changed from: private */
        @Nullable
        public Button g;
        /* access modifiers changed from: private */
        @Nullable
        public ImageView h;
        /* access modifiers changed from: private */
        @Nullable
        public ImageView i;
        /* access modifiers changed from: private */
        @Nullable
        public MediaView j;
        /* access modifiers changed from: private */
        @Nullable
        public TextView k;
        /* access modifiers changed from: private */
        @Nullable
        public View l;
        /* access modifiers changed from: private */
        @Nullable
        public TextView m;
        /* access modifiers changed from: private */
        @Nullable
        public TextView n;
        /* access modifiers changed from: private */
        @Nullable
        public TextView o;
        /* access modifiers changed from: private */
        @Nullable
        public TextView p;

        @Deprecated
        public Builder(@NonNull View view) {
            this.f5876a = view;
        }

        public Builder(@NonNull NativeAdView nativeAdView) {
            this.f5876a = nativeAdView;
        }

        @NonNull
        public final NativeAdViewBinder build() {
            return new NativeAdViewBinder(this);
        }

        @NonNull
        public final Builder setAgeView(@Nullable TextView textView) {
            this.b = textView;
            return this;
        }

        @NonNull
        public final Builder setBodyView(@Nullable TextView textView) {
            this.c = textView;
            return this;
        }

        @NonNull
        public final Builder setCallToActionView(@Nullable Button button) {
            this.d = button;
            return this;
        }

        @NonNull
        public final Builder setDomainView(@Nullable TextView textView) {
            this.e = textView;
            return this;
        }

        @NonNull
        public final Builder setFaviconView(@Nullable ImageView imageView) {
            this.f = imageView;
            return this;
        }

        @NonNull
        public final Builder setFeedbackView(@Nullable Button button) {
            this.g = button;
            return this;
        }

        @NonNull
        public final Builder setIconView(@Nullable ImageView imageView) {
            this.h = imageView;
            return this;
        }

        @NonNull
        public final Builder setImageView(@Nullable ImageView imageView) {
            this.i = imageView;
            return this;
        }

        @NonNull
        public final Builder setMediaView(@Nullable MediaView mediaView) {
            this.j = mediaView;
            return this;
        }

        @NonNull
        public final Builder setPriceView(@Nullable TextView textView) {
            this.k = textView;
            return this;
        }

        @NonNull
        public final <T extends View & Rating> Builder setRatingView(@Nullable T t) {
            this.l = t;
            return this;
        }

        @NonNull
        public final Builder setReviewCountView(@Nullable TextView textView) {
            this.m = textView;
            return this;
        }

        @NonNull
        public final Builder setSponsoredView(@Nullable TextView textView) {
            this.n = textView;
            return this;
        }

        @NonNull
        public final Builder setTitleView(@Nullable TextView textView) {
            this.o = textView;
            return this;
        }

        @NonNull
        public final Builder setWarningView(@Nullable TextView textView) {
            this.p = textView;
            return this;
        }
    }

    private NativeAdViewBinder(@NonNull Builder builder) {
        this.f5875a = builder.f5876a;
        this.b = builder.b;
        this.c = builder.c;
        this.d = builder.d;
        this.e = builder.e;
        this.f = builder.f;
        this.g = builder.g;
        this.h = builder.h;
        this.i = builder.i;
        this.j = builder.j;
        this.k = builder.k;
        this.l = builder.l;
        this.m = builder.m;
        this.n = builder.n;
        this.o = builder.o;
        this.p = builder.p;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public View getNativeAdView() {
        return this.f5875a;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public TextView getAgeView() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public TextView getBodyView() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public Button getCallToActionView() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public TextView getDomainView() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public ImageView getFaviconView() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public Button getFeedbackView() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public ImageView getIconView() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public ImageView getImageView() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public MediaView getMediaView() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public TextView getPriceView() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public View getRatingView() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public TextView getReviewCountView() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public TextView getSponsoredView() {
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public TextView getTitleView() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public TextView getWarningView() {
        return this.p;
    }
}
