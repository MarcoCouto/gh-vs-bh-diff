package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ap.a;
import com.yandex.mobile.ads.impl.cy;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.gu;
import com.yandex.mobile.ads.impl.he;
import com.yandex.mobile.ads.impl.nm;

public final class q implements a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cy f5928a;
    @Nullable
    private NativeAdEventListener b;

    public q(@NonNull Context context, @NonNull fc fcVar) {
        this.f5928a = new cy(context, fcVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable NativeAdEventListener nativeAdEventListener) {
        this.b = nativeAdEventListener;
    }

    public final void a(@NonNull gu.a aVar) {
        this.f5928a.a(aVar);
    }

    public final void a() {
        this.f5928a.d();
    }

    public final void b() {
        if (this.b != null) {
            this.b.onAdClosed();
        }
        this.f5928a.b();
    }

    public final void c() {
        if (this.b != null) {
            he.a((Object) this.b, "onAdImpressionTracked", new Object[0]);
        }
    }

    public final void d() {
        if (this.b != null) {
            this.b.onAdLeftApplication();
        }
        this.f5928a.c();
    }

    public final void e() {
        if (this.b != null) {
            this.b.onAdOpened();
        }
        this.f5928a.a();
    }

    public final void g() {
        if (this.b instanceof ClosableNativeAdEventListener) {
            ((ClosableNativeAdEventListener) this.b).closeNativeAd();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        this.f5928a.e();
    }

    public final void a(@NonNull nm nmVar) {
        this.f5928a.a(nmVar.b());
    }

    public final void f() {
        c();
    }
}
