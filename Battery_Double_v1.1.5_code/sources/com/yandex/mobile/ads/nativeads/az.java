package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.np;
import java.util.ArrayList;
import java.util.List;

public final class az extends m implements NativeImageAd {
    public az(@NonNull Context context, @NonNull np npVar, @NonNull v vVar, @NonNull i iVar, @NonNull c cVar) {
        super(context, npVar, vVar, iVar, cVar);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("media");
        return arrayList;
    }

    public final void bindImageAd(@Nullable NativeImageAdView nativeImageAdView) throws NativeAdException {
        if (nativeImageAdView != null) {
            nativeImageAdView.a(this);
            a(nativeImageAdView, new ba());
        }
    }
}
