package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.aj.a;

final class aw implements ai<NativeContentAdView> {
    aw() {
    }

    @NonNull
    public final /* synthetic */ aj a(@NonNull Object obj) {
        NativeContentAdView nativeContentAdView = (NativeContentAdView) obj;
        return new a(nativeContentAdView).a(nativeContentAdView.a()).b(nativeContentAdView.b()).c((TextView) nativeContentAdView.c()).d(nativeContentAdView.d()).a(nativeContentAdView.f()).a(nativeContentAdView.e()).c(nativeContentAdView.g()).a(nativeContentAdView.h()).g(nativeContentAdView.i()).h(nativeContentAdView.j()).i(nativeContentAdView.k()).a();
    }
}
