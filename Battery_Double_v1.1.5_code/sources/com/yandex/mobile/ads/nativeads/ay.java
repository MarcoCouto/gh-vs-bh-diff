package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.yandex.mobile.ads.impl.ag;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.impl.ap;
import com.yandex.mobile.ads.impl.aq;
import com.yandex.mobile.ads.impl.as;
import com.yandex.mobile.ads.impl.as.c;
import com.yandex.mobile.ads.impl.cd;
import com.yandex.mobile.ads.impl.cj;
import com.yandex.mobile.ads.impl.di;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.mm;
import com.yandex.mobile.ads.impl.mx;
import com.yandex.mobile.ads.impl.qh;
import com.yandex.mobile.ads.impl.qi;
import com.yandex.mobile.ads.impl.x;
import java.util.List;

public abstract class ay {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Context f5895a;
    @NonNull
    private final bd b;
    /* access modifiers changed from: private */
    @NonNull
    public final bc c;
    @NonNull
    private final bf d;
    /* access modifiers changed from: private */
    @NonNull
    public final as e;
    @NonNull
    private final ag f;
    @NonNull
    private final bk g;
    @NonNull
    private final q h;
    @NonNull
    private final s i;
    @NonNull
    private final e j;
    @NonNull
    private final mx k;
    @NonNull
    private final fc l;
    @NonNull
    private final x m;
    @NonNull
    private final bj n;
    @NonNull
    private final qi o;
    @NonNull
    private final qh p;
    @NonNull
    private final ap q;
    @NonNull
    private final cj r;
    @Nullable
    private af s;
    private final b t = new b() {
        public final void a(@NonNull Intent intent) {
            boolean z = !ay.this.c.a();
            StringBuilder sb = new StringBuilder("onPhoneStateChanged(), intent.getAction = ");
            sb.append(intent.getAction());
            sb.append(", isNativeAdViewShown = ");
            sb.append(z);
            sb.append(", clazz = ");
            sb.append(getClass());
            ay.this.e.a(intent, z);
        }
    };
    @NonNull
    private final c u = new c() {
        @NonNull
        public final am a(int i) {
            return ay.this.c.a(ay.this.f5895a, i);
        }
    };

    enum a {
        CUSTOM("custom"),
        TEMPLATE("template");
        
        final String c;

        private a(String str) {
            this.c = str;
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public abstract List<String> a();

    public ay(@NonNull Context context, @NonNull c cVar) {
        Context context2 = context;
        this.f5895a = context2;
        this.b = cVar.d();
        this.c = cVar.b();
        this.d = cVar.c();
        o a2 = cVar.a();
        this.l = a2.a();
        this.m = a2.b();
        String e2 = this.l.e();
        com.yandex.mobile.ads.b a3 = this.l.a();
        this.i = cVar.e();
        this.h = this.i.a().a(context2, this.l);
        this.r = new cj(context2, a3, this.l);
        this.q = new ap(this.h, this.r);
        List b2 = this.d.b();
        this.q.a(b2);
        this.n = new bj();
        mx mxVar = new mx(context2, this.m, this.l, this.h, this.n);
        this.k = mxVar;
        as asVar = new as(this.f5895a, this.l, this.r, this.u, di.a(this));
        this.e = asVar;
        this.j = new e(this.k, this.e);
        this.f = ag.a();
        cd cdVar = new cd(this.f5895a, new al(this.c), this.m, this.l, this.d.c());
        this.g = this.i.d().a(this.e, cdVar, new mm(this.c, b2), this.f);
        this.g.a((aq) this.q);
        this.g.a(this.m, b2);
        this.o = new qi(this.f5895a, a3, e2, this.d.a());
        this.p = new qh(this.f5895a, this.l, this.d.a());
        this.p.a(a());
    }

    /* access modifiers changed from: 0000 */
    public final <T extends View> void a(@NonNull T t2, @NonNull i iVar, @NonNull ai<T> aiVar, @NonNull f fVar) throws NativeAdException {
        ah a2 = ah.a();
        ay a3 = a2.a((View) t2);
        if (!equals(a3)) {
            Context context = t2.getContext();
            if (a3 != null) {
                a3.a(context);
            }
            if (a2.a(this)) {
                a(context);
            }
            a2.a(t2, this);
            af afVar = new af(t2, aiVar, this.l, iVar, this.q, fVar, this.i);
            afVar.a();
            this.o.a(afVar);
            this.s = afVar;
            this.c.a(afVar);
            bi b2 = this.c.b();
            if (b2.b()) {
                a(afVar);
                this.b.a(afVar, this.j);
                new StringBuilder("renderAdView(), BIND, clazz = ").append(di.a(this));
                b();
                return;
            }
            throw new NativeAdException(String.format("Resource for required view %s is not present", new Object[]{b2.a()}));
        }
    }

    private void a(@NonNull af afVar) {
        this.b.a(afVar);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        boolean z = !this.c.a();
        StringBuilder sb = new StringBuilder("registerTrackers(), isNativeAdViewShown = ");
        sb.append(z);
        sb.append(", clazz = ");
        sb.append(di.a(this));
        this.g.a(this.f5895a, this.t, this.s);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        new StringBuilder("unregisterTrackers(), clazz = ").append(di.a(this));
        this.g.a(this.f5895a, this.t);
    }

    public final void a(int i2) {
        StringBuilder sb = new StringBuilder("onVisibilityChanged(), visibility = ");
        sb.append(i2);
        sb.append(", clazz = ");
        sb.append(di.a(this));
        if (i2 == 0) {
            b();
        } else {
            c();
        }
    }

    public void setAdEventListener(NativeAdEventListener nativeAdEventListener) {
        this.h.a(nativeAdEventListener);
    }

    public void shouldOpenLinksInApp(boolean z) {
        this.l.a(z);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull Context context) {
        c();
        this.h.h();
        if (this.s != null) {
            a(this.s);
            this.g.a(this.s);
        }
    }

    public void setAdTapHandler(@Nullable AdTapHandler adTapHandler) {
        this.n.a(adTapHandler);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull com.yandex.mobile.ads.impl.gu.a aVar) {
        this.k.a(aVar);
        this.r.a(aVar);
        this.o.a(aVar);
        this.h.a(aVar);
        this.g.a(aVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bc d() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bf e() {
        return this.d;
    }
}
