package com.yandex.mobile.ads.nativeads;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.nl;
import java.util.Map;

public final class bh implements i {

    /* renamed from: a reason: collision with root package name */
    private Map<String, Bitmap> f5901a;

    public final void a(@NonNull Map<String, Bitmap> map) {
        this.f5901a = map;
    }

    @Nullable
    public final Bitmap a(@NonNull nl nlVar) {
        String c = nlVar.c();
        if (this.f5901a != null) {
            return (Bitmap) this.f5901a.get(c);
        }
        return null;
    }
}
