package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ag;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.aq;
import com.yandex.mobile.ads.impl.as;
import com.yandex.mobile.ads.impl.bg;
import com.yandex.mobile.ads.impl.cd;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.impl.mm;
import com.yandex.mobile.ads.impl.x;
import java.util.List;

public final class bo implements bk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final as f5907a;
    @NonNull
    private final ag b;
    @NonNull
    private final mm c;
    @NonNull
    private final cd d;

    bo(@NonNull as asVar, @NonNull cd cdVar, @NonNull mm mmVar, @NonNull ag agVar) {
        this.f5907a = asVar;
        this.d = cdVar;
        this.c = mmVar;
        this.b = agVar;
    }

    public final void a(@NonNull Context context, @NonNull b bVar, @Nullable af afVar) {
        this.d.b();
        this.f5907a.a();
        this.b.a(bVar, context);
        if (afVar != null) {
            this.c.a(context, afVar);
        }
    }

    public final void a(@NonNull Context context, @NonNull b bVar) {
        this.d.c();
        this.f5907a.b();
        this.b.b(bVar, context);
        this.c.a();
    }

    public final void a(@NonNull x xVar, @NonNull List<bg> list) {
        this.f5907a.a(xVar, list);
    }

    public final void a(@NonNull af afVar) {
        this.c.a(afVar);
    }

    public final void a(@NonNull aq aqVar) {
        this.f5907a.a(aqVar);
    }

    public final void a(@NonNull a aVar) {
        this.d.a(aVar);
    }
}
