package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.nl;
import com.yandex.mobile.ads.impl.nn;

public abstract class NativeAdAssets {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private NativeAdMedia f5868a;
    private String b;
    private String c;
    private String d;
    private String e;
    private NativeAdImage f;
    private NativeAdImage g;
    private NativeAdImage h;
    private String i;
    private Float j;
    private String k;
    private String l;
    private String m;
    private String n;

    NativeAdAssets() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable String str) {
        this.b = str;
    }

    /* access modifiers changed from: 0000 */
    public final void b(@Nullable String str) {
        this.c = str;
    }

    /* access modifiers changed from: 0000 */
    public final void c(@Nullable String str) {
        this.d = str;
    }

    /* access modifiers changed from: 0000 */
    public final void d(@Nullable String str) {
        this.e = str;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable nl nlVar, @NonNull i iVar) {
        this.f = d(nlVar, iVar);
    }

    /* access modifiers changed from: 0000 */
    public final void b(@Nullable nl nlVar, @NonNull i iVar) {
        this.g = d(nlVar, iVar);
    }

    /* access modifiers changed from: 0000 */
    public final void c(@Nullable nl nlVar, @NonNull i iVar) {
        this.h = d(nlVar, iVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable nn nnVar) {
        this.f5868a = nnVar != null ? new NativeAdMedia(nnVar.b()) : null;
    }

    /* access modifiers changed from: 0000 */
    public final void e(@Nullable String str) {
        this.i = str;
    }

    /* access modifiers changed from: 0000 */
    public final void f(@Nullable String str) {
        if (str != null) {
            try {
                this.j = Float.valueOf(Float.parseFloat(str));
            } catch (NumberFormatException unused) {
                String.format("Could not parse rating value. Rating value is %s", new Object[]{str});
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void g(@Nullable String str) {
        this.k = str;
    }

    /* access modifiers changed from: 0000 */
    public final void h(@Nullable String str) {
        this.l = str;
    }

    /* access modifiers changed from: 0000 */
    public final void i(@Nullable String str) {
        this.m = str;
    }

    /* access modifiers changed from: 0000 */
    public final void j(@Nullable String str) {
        this.n = str;
    }

    public String getAge() {
        return this.b;
    }

    public String getBody() {
        return this.c;
    }

    public String getCallToAction() {
        return this.d;
    }

    public String getDomain() {
        return this.e;
    }

    public NativeAdImage getFavicon() {
        return this.f;
    }

    public NativeAdImage getIcon() {
        return this.g;
    }

    public NativeAdImage getImage() {
        return this.h;
    }

    @Nullable
    public NativeAdMedia getMedia() {
        return this.f5868a;
    }

    public String getPrice() {
        return this.i;
    }

    public Float getRating() {
        return this.j;
    }

    public String getReviewCount() {
        return this.k;
    }

    public String getSponsored() {
        return this.l;
    }

    public String getTitle() {
        return this.m;
    }

    public String getWarning() {
        return this.n;
    }

    @Nullable
    @VisibleForTesting
    private static NativeAdImage d(@Nullable nl nlVar, @NonNull i iVar) {
        if (nlVar == null) {
            return null;
        }
        NativeAdImage nativeAdImage = new NativeAdImage();
        nativeAdImage.a(nlVar.b());
        nativeAdImage.b(nlVar.a());
        nativeAdImage.a(nlVar.d());
        nativeAdImage.a(iVar.a(nlVar));
        return nativeAdImage;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NativeAdAssets nativeAdAssets = (NativeAdAssets) obj;
        if (this.f5868a == null ? nativeAdAssets.f5868a != null : !this.f5868a.equals(nativeAdAssets.f5868a)) {
            return false;
        }
        if (this.b == null ? nativeAdAssets.b != null : !this.b.equals(nativeAdAssets.b)) {
            return false;
        }
        if (this.c == null ? nativeAdAssets.c != null : !this.c.equals(nativeAdAssets.c)) {
            return false;
        }
        if (this.d == null ? nativeAdAssets.d != null : !this.d.equals(nativeAdAssets.d)) {
            return false;
        }
        if (this.e == null ? nativeAdAssets.e != null : !this.e.equals(nativeAdAssets.e)) {
            return false;
        }
        if (this.f == null ? nativeAdAssets.f != null : !this.f.equals(nativeAdAssets.f)) {
            return false;
        }
        if (this.g == null ? nativeAdAssets.g != null : !this.g.equals(nativeAdAssets.g)) {
            return false;
        }
        if (this.h == null ? nativeAdAssets.h != null : !this.h.equals(nativeAdAssets.h)) {
            return false;
        }
        if (this.i == null ? nativeAdAssets.i != null : !this.i.equals(nativeAdAssets.i)) {
            return false;
        }
        if (this.j == null ? nativeAdAssets.j != null : !this.j.equals(nativeAdAssets.j)) {
            return false;
        }
        if (this.k == null ? nativeAdAssets.k != null : !this.k.equals(nativeAdAssets.k)) {
            return false;
        }
        if (this.l == null ? nativeAdAssets.l != null : !this.l.equals(nativeAdAssets.l)) {
            return false;
        }
        if (this.m == null ? nativeAdAssets.m != null : !this.m.equals(nativeAdAssets.m)) {
            return false;
        }
        if (this.n != null) {
            return this.n.equals(nativeAdAssets.n);
        }
        return nativeAdAssets.n == null;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = (((((((((((((((((((((((((this.f5868a != null ? this.f5868a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.g != null ? this.g.hashCode() : 0)) * 31) + (this.h != null ? this.h.hashCode() : 0)) * 31) + (this.i != null ? this.i.hashCode() : 0)) * 31) + (this.j != null ? this.j.hashCode() : 0)) * 31) + (this.k != null ? this.k.hashCode() : 0)) * 31) + (this.l != null ? this.l.hashCode() : 0)) * 31) + (this.m != null ? this.m.hashCode() : 0)) * 31;
        if (this.n != null) {
            i2 = this.n.hashCode();
        }
        return hashCode + i2;
    }
}
