package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View.OnClickListener;
import com.yandex.mobile.ads.impl.as;
import com.yandex.mobile.ads.impl.lv;
import com.yandex.mobile.ads.impl.lx;
import com.yandex.mobile.ads.impl.ni;
import com.yandex.mobile.ads.impl.nm;
import com.yandex.mobile.ads.impl.nr;

public abstract class f {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static final f f5911a = new f() {
        @NonNull
        public final OnClickListener a(@NonNull ni niVar, @Nullable nm nmVar, @NonNull a aVar, @NonNull af afVar, @Nullable as asVar) {
            lv lvVar = new lv(niVar, aVar, afVar, nmVar, asVar);
            return lvVar;
        }
    };
    @VisibleForTesting
    static final f b = new f() {
        @NonNull
        public final OnClickListener a(@NonNull ni niVar, @Nullable nm nmVar, @NonNull a aVar, @NonNull af afVar, @Nullable as asVar) {
            if (!"call_to_action".equals(niVar.a())) {
                return new lx(afVar.c().d());
            }
            lv lvVar = new lv(niVar, aVar, afVar, nmVar, asVar);
            return lvVar;
        }
    };

    @NonNull
    public abstract OnClickListener a(@NonNull ni niVar, @Nullable nm nmVar, @NonNull a aVar, @NonNull af afVar, @Nullable as asVar);

    @NonNull
    static f a() {
        return f5911a;
    }

    @NonNull
    static f a(@Nullable nr nrVar) {
        if (nrVar == null || !"button_click_only".equals(nrVar.a())) {
            return f5911a;
        }
        return b;
    }
}
