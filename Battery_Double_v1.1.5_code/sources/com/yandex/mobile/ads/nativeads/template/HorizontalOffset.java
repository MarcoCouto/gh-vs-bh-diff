package com.yandex.mobile.ads.nativeads.template;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class HorizontalOffset implements Parcelable {
    public static final Creator<HorizontalOffset> CREATOR = new Creator<HorizontalOffset>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new HorizontalOffset[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new HorizontalOffset(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final float f5931a;
    private final float b;

    public final int describeContents() {
        return 0;
    }

    public HorizontalOffset(float f, float f2) {
        this.f5931a = f;
        this.b = f2;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f5931a);
        sb.append(", ");
        sb.append(this.b);
        return sb.toString();
    }

    public final float getLeft() {
        return this.f5931a;
    }

    public final float getRight() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        HorizontalOffset horizontalOffset = (HorizontalOffset) obj;
        return Float.compare(horizontalOffset.f5931a, this.f5931a) == 0 && Float.compare(horizontalOffset.b, this.b) == 0;
    }

    public final int hashCode() {
        int i = 0;
        int floatToIntBits = (this.f5931a != 0.0f ? Float.floatToIntBits(this.f5931a) : 0) * 31;
        if (this.b != 0.0f) {
            i = Float.floatToIntBits(this.b);
        }
        return floatToIntBits + i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.f5931a);
        parcel.writeFloat(this.b);
    }

    protected HorizontalOffset(Parcel parcel) {
        this.f5931a = parcel.readFloat();
        this.b = parcel.readFloat();
    }
}
