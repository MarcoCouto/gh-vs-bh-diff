package com.yandex.mobile.ads.nativeads.template;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.text.Layout;
import android.widget.TextView;
import com.yandex.mobile.ads.impl.dv;

final class g extends TextView {

    /* renamed from: a reason: collision with root package name */
    private Rect f5954a = new Rect();
    private Paint b = new Paint();
    private int c = dv.a(getContext(), 1.0f);
    private int d = dv.a(getContext(), 4.0f);

    public g(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        int a2 = dv.a(getCurrentTextColor(), 85.0f);
        Paint paint = this.b;
        paint.setStyle(Style.STROKE);
        paint.setStrokeWidth((float) this.c);
        paint.setColor(a2);
        int lineCount = getLineCount();
        Layout layout = getLayout();
        for (int i = 0; i < lineCount; i++) {
            int lineBounds = getLineBounds(i, this.f5954a);
            int lineStart = layout.getLineStart(i);
            int lineEnd = layout.getLineEnd(i);
            float primaryHorizontal = layout.getPrimaryHorizontal(lineStart);
            float primaryHorizontal2 = layout.getPrimaryHorizontal(lineEnd - 1) + (layout.getPrimaryHorizontal(lineStart + 1) - primaryHorizontal);
            canvas.drawLine(primaryHorizontal, (float) (this.d + lineBounds), primaryHorizontal2, (float) (lineBounds + this.d), paint);
        }
        super.onDraw(canvas);
    }
}
