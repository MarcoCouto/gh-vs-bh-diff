package com.yandex.mobile.ads.nativeads.template.appearance;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class TextAppearance implements Parcelable {
    public static final Creator<TextAppearance> CREATOR = new Creator<TextAppearance>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new TextAppearance[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new TextAppearance(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final int f5949a;
    private final float b;
    private final String c;
    private final int d;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public int f5950a;
        /* access modifiers changed from: private */
        public float b;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public int d;

        public final TextAppearance build() {
            return new TextAppearance(this, 0);
        }

        public final Builder setTextColor(int i) {
            this.f5950a = i;
            return this;
        }

        public final Builder setTextSize(float f) {
            this.b = f;
            return this;
        }

        public final Builder setFontFamilyName(String str) {
            this.c = str;
            return this;
        }

        public final Builder setFontStyle(int i) {
            this.d = i;
            return this;
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ TextAppearance(Builder builder, byte b2) {
        this(builder);
    }

    private TextAppearance(Builder builder) {
        this.f5949a = builder.f5950a;
        this.b = builder.b;
        this.c = builder.c;
        this.d = builder.d;
    }

    public final int getTextColor() {
        return this.f5949a;
    }

    public final float getTextSize() {
        return this.b;
    }

    public final String getFontFamilyName() {
        return this.c;
    }

    public final int getFontStyle() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        TextAppearance textAppearance = (TextAppearance) obj;
        if (this.f5949a == textAppearance.f5949a && Float.compare(textAppearance.b, this.b) == 0 && this.d == textAppearance.d) {
            return this.c == null ? textAppearance.c == null : this.c.equals(textAppearance.c);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int floatToIntBits = ((this.f5949a * 31) + (this.b != 0.0f ? Float.floatToIntBits(this.b) : 0)) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return ((floatToIntBits + i) * 31) + this.d;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f5949a);
        parcel.writeFloat(this.b);
        parcel.writeString(this.c);
        parcel.writeInt(this.d);
    }

    protected TextAppearance(Parcel parcel) {
        this.f5949a = parcel.readInt();
        this.b = parcel.readFloat();
        this.c = parcel.readString();
        this.d = parcel.readInt();
    }
}
