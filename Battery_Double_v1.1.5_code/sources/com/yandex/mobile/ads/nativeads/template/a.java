package com.yandex.mobile.ads.nativeads.template;

import android.content.Context;
import com.yandex.mobile.ads.impl.dv;

abstract class a {

    /* renamed from: a reason: collision with root package name */
    protected final float f5937a;

    /* renamed from: com.yandex.mobile.ads.nativeads.template.a$a reason: collision with other inner class name */
    static class C0133a extends a {
        /* access modifiers changed from: protected */
        public final float a(float f) {
            if (f < 10.0f) {
                return 10.0f;
            }
            return f;
        }

        public C0133a(float f) {
            super(f);
        }

        public final d a(Context context, int i, int i2, int i3) {
            int min = Math.min(dv.a(context, this.f5937a), i);
            return new d(min, Math.round(((float) i3) * (((float) min) / ((float) i2))));
        }
    }

    static class b extends a {
        public b(float f) {
            super(f);
        }

        public final d a(Context context, int i, int i2, int i3) {
            int round = Math.round(((float) i) * this.f5937a);
            return new d(round, Math.round(((float) i3) * (((float) round) / ((float) i2))));
        }

        /* access modifiers changed from: protected */
        public final float a(float f) {
            return b(f);
        }
    }

    static class c extends a {
        public c(float f) {
            super(f);
        }

        public final d a(Context context, int i, int i2, int i3) {
            int a2 = dv.a(context, 140.0f);
            int round = Math.round(((float) i) * this.f5937a);
            if (i2 > round) {
                i3 = Math.round(((float) i3) / (((float) i2) / ((float) round)));
            } else {
                round = i2;
            }
            if (i3 > a2) {
                round = Math.round(((float) round) / (((float) i3) / ((float) a2)));
            } else {
                a2 = i3;
            }
            return new d(round, a2);
        }

        /* access modifiers changed from: protected */
        public final float a(float f) {
            return b(f);
        }
    }

    static class d {

        /* renamed from: a reason: collision with root package name */
        private final int f5938a;
        private final int b;

        public d(int i, int i2) {
            this.f5938a = i;
            this.b = i2;
        }

        public final int a() {
            return this.f5938a;
        }

        public final int b() {
            return this.b;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            return this.f5938a == dVar.f5938a && this.b == dVar.b;
        }

        public final int hashCode() {
            return (this.f5938a * 31) + this.b;
        }
    }

    protected static float b(float f) {
        if (f < 0.01f) {
            return 0.01f;
        }
        if (f > 1.0f) {
            return 1.0f;
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public abstract float a(float f);

    public abstract d a(Context context, int i, int i2, int i3);

    a(float f) {
        this.f5937a = a(f);
    }
}
