package com.yandex.mobile.ads.nativeads.template.appearance;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class RatingAppearance implements Parcelable {
    public static final Creator<RatingAppearance> CREATOR = new Creator<RatingAppearance>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new RatingAppearance[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new RatingAppearance(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final int f5947a;
    private final int b;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public int f5948a;
        /* access modifiers changed from: private */
        public int b;

        public final RatingAppearance build() {
            return new RatingAppearance(this, 0);
        }

        public final Builder setBackgroundStarColor(int i) {
            this.f5948a = i;
            return this;
        }

        public final Builder setProgressStarColor(int i) {
            this.b = i;
            return this;
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ RatingAppearance(Builder builder, byte b2) {
        this(builder);
    }

    private RatingAppearance(Builder builder) {
        this.f5947a = builder.f5948a;
        this.b = builder.b;
    }

    public final int getBackgroundStarColor() {
        return this.f5947a;
    }

    public final int getProgressStarColor() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RatingAppearance ratingAppearance = (RatingAppearance) obj;
        return this.f5947a == ratingAppearance.f5947a && this.b == ratingAppearance.b;
    }

    public final int hashCode() {
        return (this.f5947a * 31) + this.b;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f5947a);
        parcel.writeInt(this.b);
    }

    protected RatingAppearance(Parcel parcel) {
        this.f5947a = parcel.readInt();
        this.b = parcel.readInt();
    }
}
