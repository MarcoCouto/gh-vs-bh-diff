package com.yandex.mobile.ads.nativeads.template;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.yandex.mobile.ads.impl.dv;
import com.yandex.mobile.ads.impl.z;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.NativeAdAssets;
import com.yandex.mobile.ads.nativeads.NativeAdException;
import com.yandex.mobile.ads.nativeads.NativeAdImage;
import com.yandex.mobile.ads.nativeads.NativeAdImageLoadingListener;
import com.yandex.mobile.ads.nativeads.NativeAdMedia;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.NativeGenericAd;
import com.yandex.mobile.ads.nativeads.ay;
import com.yandex.mobile.ads.nativeads.t;
import com.yandex.mobile.ads.nativeads.template.appearance.NativeTemplateAppearance;
import com.yandex.mobile.ads.nativeads.template.appearance.NativeTemplateAppearance.Builder;
import com.yandex.mobile.ads.nativeads.x;
import java.util.Arrays;

public final class NativeBannerView extends t<ay> {

    /* renamed from: a reason: collision with root package name */
    private static final int f5932a = Color.parseColor("#eaeaea");
    private NativeAdType A;
    private NativeAdAssets B;
    private e C;
    private d D;
    /* access modifiers changed from: private */
    public NativeGenericAd E;
    /* access modifiers changed from: private */
    public b F;
    private final NativeAdImageLoadingListener G = new NativeAdImageLoadingListener() {
        public final void onFinishLoadingImages() {
            if (NativeBannerView.this.E != null) {
                NativeBannerView.this.E.removeImageLoadingListener(this);
            }
            NativeBannerView.this.F.a();
        }
    };
    private NativeTemplateAppearance b;
    private TextView c;
    private TextView d;
    private Button e;
    private TextView f;
    private ImageView g;
    private g h;
    private TextView i;
    private TextView j;
    private ImageView k;
    private f l;
    private TextView m;
    private ImageView n;
    private ImageView o;
    private LinearLayout p;
    private final int q = dv.a(getContext(), 4.0f);
    private final int r = dv.a(getContext(), 8.0f);
    private final int s = dv.a(getContext(), 12.0f);
    private LinearLayout t;
    private LinearLayout u;
    private LinearLayout v;
    private ImageView w;
    private LinearLayout x;
    private MediaView y;
    private LinearLayout z;

    public NativeBannerView(Context context) {
        super(context);
        n();
    }

    public NativeBannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        n();
    }

    public NativeBannerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        n();
    }

    public final void applyAppearance(@Nullable NativeTemplateAppearance nativeTemplateAppearance) {
        if (nativeTemplateAppearance != null && !nativeTemplateAppearance.equals(this.b)) {
            this.b = nativeTemplateAppearance;
            M();
        }
    }

    public final void setAd(NativeGenericAd nativeGenericAd) {
        if (this.E != nativeGenericAd) {
            try {
                if (this.E != null) {
                    this.E.removeImageLoadingListener(this.G);
                }
                nativeGenericAd.addImageLoadingListener(this.G);
                this.A = nativeGenericAd.getAdType();
                this.B = nativeGenericAd.getAdAssets();
                this.C = new e(this.B, this.A);
                this.D = new d(this.B, this.A);
                ((x) nativeGenericAd).a(this);
                int i2 = 0;
                if (this.D != null) {
                    NativeAdImage image = this.B.getImage();
                    if (image != null && d.b(image)) {
                        this.o.setVisibility(0);
                        i2 = 8;
                    }
                }
                this.p.setVisibility(i2);
                this.E = nativeGenericAd;
            } catch (NativeAdException unused) {
            }
        }
    }

    private void n() {
        this.b = new Builder().build();
        this.o = F();
        this.p = p();
        addView(this.o, new LayoutParams(-1, -2));
        addView(this.p, new LayoutParams(-1, -1));
        o();
        this.F = new b(this.n, this.k, this.g, this.w, this.o);
        M();
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.E != null) {
            this.E.addImageLoadingListener(this.G);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        if (this.E != null) {
            this.E.removeImageLoadingListener(this.G);
        }
        super.onDetachedFromWindow();
    }

    @VisibleForTesting
    private void o() {
        this.o.setVisibility(8);
        this.p.setVisibility(8);
    }

    private LinearLayout p() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(1);
        View v2 = v();
        View B2 = B();
        View q2 = q();
        View r2 = r();
        View K = K();
        linearLayout.addView(v2);
        linearLayout.addView(B2);
        linearLayout.addView(q2);
        linearLayout.addView(r2);
        linearLayout.addView(K);
        return linearLayout;
    }

    private View q() {
        this.x = s();
        this.w = t();
        this.x.addView(this.w);
        return this.x;
    }

    private View r() {
        this.z = s();
        this.y = u();
        this.z.addView(this.y);
        return this.z;
    }

    private LinearLayout s() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        return linearLayout;
    }

    private ImageView t() {
        ImageView imageView = new ImageView(getContext());
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        return imageView;
    }

    private MediaView u() {
        MediaView mediaView = new MediaView(getContext());
        mediaView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        return mediaView;
    }

    private View v() {
        this.t = new LinearLayout(getContext());
        this.t.setOrientation(0);
        this.t.setGravity(17);
        this.t.setWeightSum(4.0f);
        View w2 = w();
        View x2 = x();
        View z2 = z();
        this.t.addView(w2);
        this.t.addView(x2);
        this.t.addView(z2);
        return this.t;
    }

    private View w() {
        View view = new View(getContext());
        view.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 1.0f));
        return view;
    }

    private View x() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setGravity(17);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 2.0f));
        this.h = y();
        linearLayout.addView(this.h);
        return linearLayout;
    }

    private g y() {
        g gVar = new g(getContext());
        gVar.setEllipsize(TruncateAt.END);
        gVar.setMaxLines(1);
        gVar.setGravity(17);
        gVar.setPadding(0, 0, 0, dv.a(getContext(), 4.0f));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        gVar.setLayoutParams(layoutParams);
        return gVar;
    }

    private View z() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
        this.c = A();
        linearLayout.addView(this.c);
        return linearLayout;
    }

    private TextView A() {
        TextView textView = new TextView(getContext());
        textView.setEllipsize(TruncateAt.END);
        textView.setMaxLines(1);
        textView.setGravity(GravityCompat.END);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.leftMargin = this.q;
        textView.setLayoutParams(layoutParams);
        return textView;
    }

    private View B() {
        this.u = new LinearLayout(getContext());
        this.u.setOrientation(0);
        this.u.setBaselineAligned(false);
        View D2 = D();
        View G2 = G();
        this.u.addView(D2);
        this.u.addView(G2);
        return this.u;
    }

    private View C() {
        Button button;
        this.v = new LinearLayout(getContext());
        this.v.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = this.r;
        this.v.setLayoutParams(layoutParams);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        f fVar = new f(getContext());
        fVar.setNumStars(5);
        fVar.setStepSize(0.5f);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = this.r;
        fVar.setLayoutParams(layoutParams2);
        this.l = fVar;
        TextView textView = new TextView(getContext());
        textView.setEllipsize(TruncateAt.END);
        textView.setMaxLines(1);
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        this.m = textView;
        linearLayout.addView(this.l);
        linearLayout.addView(this.m);
        LinearLayout linearLayout2 = new LinearLayout(getContext());
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(5);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        if (VERSION.SDK_INT >= 11) {
            button = new Button(getContext(), null, 16843563);
        } else {
            button = new Button(getContext());
        }
        button.setEllipsize(TruncateAt.END);
        button.setMaxLines(1);
        button.setTransformationMethod(null);
        int a2 = dv.a(getContext(), 26.0f);
        button.setMinimumHeight(a2);
        button.setMinHeight(a2);
        button.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.e = button;
        linearLayout2.addView(this.e);
        this.v.addView(linearLayout);
        this.v.addView(linearLayout2);
        return this.v;
    }

    private View D() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.topMargin = this.q;
        FrameLayout frameLayout = new FrameLayout(getContext());
        frameLayout.setLayoutParams(layoutParams);
        this.k = F();
        this.g = F();
        this.n = E();
        frameLayout.addView(this.k);
        frameLayout.addView(this.g);
        frameLayout.addView(this.n);
        return frameLayout;
    }

    private ImageView E() {
        ImageView imageView = new ImageView(getContext());
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(ScaleType.CENTER_CROP);
        return imageView;
    }

    private ImageView F() {
        ImageView imageView = new ImageView(getContext());
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(ScaleType.CENTER_CROP);
        return imageView;
    }

    private View G() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(1);
        this.i = H();
        this.d = I();
        this.f = J();
        linearLayout.addView(this.i);
        linearLayout.addView(this.d);
        linearLayout.addView(this.f);
        linearLayout.addView(C());
        return linearLayout;
    }

    private TextView H() {
        TextView textView = new TextView(getContext());
        textView.setEllipsize(TruncateAt.END);
        textView.setMaxLines(2);
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        return textView;
    }

    private TextView I() {
        TextView textView = new TextView(getContext());
        textView.setEllipsize(TruncateAt.END);
        textView.setMaxLines(3);
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        return textView;
    }

    private TextView J() {
        TextView textView = new TextView(getContext());
        textView.setEllipsize(TruncateAt.END);
        textView.setMaxLines(1);
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        return textView;
    }

    private View K() {
        this.j = L();
        return this.j;
    }

    private TextView L() {
        TextView textView = new TextView(getContext());
        textView.setEllipsize(TruncateAt.END);
        return textView;
    }

    private void M() {
        N();
        O();
        Q();
        R();
        invalidate();
        requestLayout();
    }

    private static a a(SizeConstraint sizeConstraint) {
        switch (sizeConstraint.getSizeConstraintType()) {
            case FIXED:
                return new C0133a(sizeConstraint.getValue());
            case FIXED_RATIO:
                return new b(sizeConstraint.getValue());
            case PREFERRED_RATIO:
                return new c(sizeConstraint.getValue());
            default:
                return new c(sizeConstraint.getValue());
        }
    }

    private void N() {
        Z();
        aa();
        ab();
        ac();
    }

    private void O() {
        this.e.setTextColor(this.b.getCallToActionAppearance().getTextAppearance().getTextColor());
        this.e.setTextSize(this.b.getCallToActionAppearance().getTextAppearance().getTextSize());
        this.e.setTypeface(Typeface.create(this.b.getCallToActionAppearance().getTextAppearance().getFontFamilyName(), this.b.getCallToActionAppearance().getTextAppearance().getFontStyle()));
        P();
    }

    private void P() {
        float[] fArr = new float[8];
        Arrays.fill(fArr, (float) dv.a(getContext(), 5.0f));
        RoundRectShape roundRectShape = new RoundRectShape(fArr, null, fArr);
        ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
        shapeDrawable.getPaint().setColor(this.b.getCallToActionAppearance().getPressedColor());
        ShapeDrawable shapeDrawable2 = new ShapeDrawable(roundRectShape);
        shapeDrawable2.getPaint().setColor(this.b.getCallToActionAppearance().getNormalColor());
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842913}, shapeDrawable);
        stateListDrawable.addState(new int[]{16842919}, shapeDrawable);
        stateListDrawable.addState(new int[0], shapeDrawable2);
        int a2 = dv.a(getContext(), this.b.getCallToActionAppearance().getBorderWidth());
        ShapeDrawable shapeDrawable3 = new ShapeDrawable();
        shapeDrawable3.setShape(roundRectShape);
        Paint paint = shapeDrawable3.getPaint();
        paint.setColor(this.b.getCallToActionAppearance().getBorderColor());
        paint.setStrokeWidth((float) a2);
        paint.setStyle(Style.STROKE);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{stateListDrawable, shapeDrawable3});
        if (VERSION.SDK_INT >= 16) {
            this.e.setBackground(layerDrawable);
        } else {
            this.e.setBackgroundDrawable(layerDrawable);
        }
    }

    private void Q() {
        LayerDrawable layerDrawable = (LayerDrawable) this.l.getProgressDrawable();
        layerDrawable.getDrawable(2).setColorFilter(this.b.getRatingAppearance().getProgressStarColor(), Mode.SRC_ATOP);
        layerDrawable.getDrawable(1).setColorFilter(this.b.getRatingAppearance().getBackgroundStarColor(), Mode.SRC_ATOP);
        layerDrawable.getDrawable(0).setColorFilter(this.b.getRatingAppearance().getBackgroundStarColor(), Mode.SRC_ATOP);
    }

    private void R() {
        S();
        T();
        U();
        V();
        W();
        X();
        Y();
    }

    private void S() {
        this.c.setTypeface(Typeface.create(this.b.getAgeAppearance().getFontFamilyName(), this.b.getAgeAppearance().getFontStyle()));
        this.c.setTextColor(this.b.getAgeAppearance().getTextColor());
        this.c.setTextSize(2, this.b.getAgeAppearance().getTextSize());
    }

    private void T() {
        this.d.setTypeface(Typeface.create(this.b.getBodyAppearance().getFontFamilyName(), this.b.getBodyAppearance().getFontStyle()));
        this.d.setTextColor(this.b.getBodyAppearance().getTextColor());
        this.d.setTextSize(2, this.b.getBodyAppearance().getTextSize());
    }

    private void U() {
        this.f.setTypeface(Typeface.create(this.b.getDomainAppearance().getFontFamilyName(), this.b.getDomainAppearance().getFontStyle()));
        this.f.setTextColor(this.b.getDomainAppearance().getTextColor());
        this.f.setTextSize(2, this.b.getDomainAppearance().getTextSize());
    }

    private void V() {
        this.m.setTypeface(Typeface.create(this.b.getReviewCountAppearance().getFontFamilyName(), this.b.getReviewCountAppearance().getFontStyle()));
        this.m.setTextColor(this.b.getReviewCountAppearance().getTextColor());
        this.m.setTextSize(2, this.b.getReviewCountAppearance().getTextSize());
    }

    private void W() {
        this.h.setTypeface(Typeface.create(this.b.getSponsoredAppearance().getFontFamilyName(), this.b.getSponsoredAppearance().getFontStyle()));
        this.h.setTextColor(this.b.getSponsoredAppearance().getTextColor());
        this.h.setTextSize(2, this.b.getSponsoredAppearance().getTextSize());
    }

    private void X() {
        this.i.setTypeface(Typeface.create(this.b.getTitleAppearance().getFontFamilyName(), this.b.getTitleAppearance().getFontStyle()));
        this.i.setTextColor(this.b.getTitleAppearance().getTextColor());
        this.i.setTextSize(2, this.b.getTitleAppearance().getTextSize());
    }

    private void Y() {
        this.j.setTypeface(Typeface.create(this.b.getWarningAppearance().getFontFamilyName(), this.b.getWarningAppearance().getFontStyle()));
        this.j.setTextColor(this.b.getWarningAppearance().getTextColor());
        this.j.setTextSize(2, this.b.getWarningAppearance().getTextSize());
    }

    private void Z() {
        int a2 = dv.a(getContext(), this.b.getBannerAppearance().getBorderWidth());
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.setShape(new RectShape());
        Paint paint = shapeDrawable.getPaint();
        paint.setColor(this.b.getBannerAppearance().getBackgroundColor());
        paint.setStyle(Style.FILL);
        ShapeDrawable shapeDrawable2 = new ShapeDrawable();
        shapeDrawable2.setShape(new RectShape());
        Paint paint2 = shapeDrawable2.getPaint();
        paint2.setColor(this.b.getBannerAppearance().getBorderColor());
        paint2.setStrokeWidth(((float) a2) * 2.0f);
        paint2.setStyle(Style.STROKE);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{shapeDrawable, shapeDrawable2});
        if (VERSION.SDK_INT >= 16) {
            setBackground(layerDrawable);
        } else {
            setBackgroundDrawable(layerDrawable);
        }
        setPadding(a2, a2, a2, a2);
    }

    private void aa() {
        int a2 = dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getLeft());
        int a3 = dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getRight());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(a2, this.r, a3, this.q);
        this.t.setLayoutParams(layoutParams);
        this.t.invalidate();
    }

    private void ab() {
        int a2 = dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getLeft());
        int a3 = dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getRight());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.leftMargin = a2;
        layoutParams.rightMargin = a3;
        layoutParams.bottomMargin = this.q;
        this.u.setLayoutParams(layoutParams);
        this.u.invalidate();
    }

    private void a(int i2) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.u.getLayoutParams();
        layoutParams.leftMargin = i2;
        this.u.setLayoutParams(layoutParams);
    }

    private void ac() {
        this.j.setPadding(dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getLeft()), this.q, dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getRight()), this.q);
        this.j.invalidate();
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        LinearLayout.LayoutParams layoutParams;
        int i4;
        int i5;
        if (this.D != null) {
            int size = MeasureSpec.getSize(i2);
            d dVar = this.D;
            Context context = getContext();
            int a2 = dv.a(context, this.b.getBannerAppearance().getContentPadding().getLeft());
            int a3 = dv.a(context, this.b.getBannerAppearance().getContentPadding().getRight());
            if (dVar.a() || dVar.b() || dVar.c()) {
                int round = Math.round((float) ((size - a2) - a3));
                int a4 = dv.a(getContext(), this.b.getBannerAppearance().getImageMargins().getLeft()) + dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getLeft());
                int a5 = dv.a(getContext(), this.b.getBannerAppearance().getImageMargins().getRight());
                FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(0, 0);
                if (dVar.a()) {
                    NativeAdImage favicon = this.B.getFavicon();
                    d a6 = a(this.b.getFaviconAppearance().getWidthConstraint()).a(getContext(), round, favicon.getWidth(), favicon.getHeight());
                    int a7 = dv.a(getContext(), 5.0f);
                    FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(a6.a(), a6.b());
                    layoutParams3.leftMargin = a4;
                    layoutParams3.rightMargin = a7;
                    layoutParams2 = layoutParams3;
                }
                this.n.setLayoutParams(layoutParams2);
                FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(0, 0);
                if (dVar.b()) {
                    layoutParams4 = a(this.B.getIcon(), round, a4, a5);
                }
                this.k.setLayoutParams(layoutParams4);
                FrameLayout.LayoutParams layoutParams5 = new FrameLayout.LayoutParams(0, 0);
                if (dVar.c()) {
                    layoutParams5 = a(this.B.getImage(), round, a4, a5);
                }
                this.g.setLayoutParams(layoutParams5);
                a(0);
            } else {
                FrameLayout.LayoutParams layoutParams6 = new FrameLayout.LayoutParams(0, 0);
                this.n.setLayoutParams(layoutParams6);
                this.k.setLayoutParams(layoutParams6);
                this.g.setLayoutParams(layoutParams6);
                a(a2);
            }
            LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(0, 0);
            if (dVar.d()) {
                NativeAdImage image = this.B.getImage();
                int width = image.getWidth();
                int height = image.getHeight();
                float height2 = (float) image.getHeight();
                if (height2 != 0.0f && ((float) image.getWidth()) / height2 < 1.0f) {
                    i4 = Math.round((float) ((size * 3) / 4));
                    i5 = Math.round((((float) i4) / ((float) height)) * ((float) width));
                } else {
                    i4 = a(size, width, height);
                    i5 = size;
                }
                float width2 = (float) image.getWidth();
                float height3 = (float) image.getHeight();
                if (!(height3 != 0.0f && width2 / height3 > 1.5f)) {
                    i4 = Math.round(((float) i4) * 0.8f);
                }
                layoutParams7 = new LinearLayout.LayoutParams(i5, i4);
                layoutParams7.topMargin = this.q;
                layoutParams7.gravity = 1;
            }
            this.x.setLayoutParams(layoutParams7);
            if (VERSION.SDK_INT <= 17) {
                FrameLayout.LayoutParams layoutParams8 = new FrameLayout.LayoutParams(0, 0);
                if (dVar.e()) {
                    NativeAdImage image2 = this.B.getImage();
                    layoutParams8 = new FrameLayout.LayoutParams(size, a(size, image2.getWidth(), image2.getHeight()));
                }
                this.o.setLayoutParams(layoutParams8);
            }
            d dVar2 = this.D;
            LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(0, 0);
            NativeAdMedia media = this.B.getMedia();
            if (media != null && dVar2.f()) {
                layoutParams9 = new LinearLayout.LayoutParams(size, new z(media.getAspectRatio()).b(size));
            }
            this.z.setLayoutParams(layoutParams9);
            if (this.C.c()) {
                if (this.C.b()) {
                    ((ViewManager) this.v.getParent()).removeView(this.v);
                    LinearLayout.LayoutParams layoutParams10 = new LinearLayout.LayoutParams(-1, -2);
                    if (this.C.a()) {
                        layoutParams10.topMargin = this.s;
                    } else {
                        layoutParams10.topMargin = this.q;
                    }
                    layoutParams10.bottomMargin = this.r;
                    this.v.setLayoutParams(layoutParams10);
                    this.v.setPadding(dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getLeft()), 0, dv.a(getContext(), this.b.getBannerAppearance().getContentPadding().getRight()), 0);
                    this.p.addView(this.v, this.p.getChildCount() - 1);
                } else {
                    ((ViewManager) this.v.getParent()).removeView(this.v);
                    this.v.setPadding(0, 0, 0, 0);
                    LinearLayout.LayoutParams layoutParams11 = new LinearLayout.LayoutParams(-1, -2);
                    layoutParams11.topMargin = this.r;
                    layoutParams11.bottomMargin = this.r;
                    ((ViewManager) this.f.getParent()).addView(this.v, layoutParams11);
                }
                if (this.C.d()) {
                    layoutParams = new LinearLayout.LayoutParams(-1, -2);
                } else {
                    layoutParams = new LinearLayout.LayoutParams(-2, -2);
                }
                this.e.setLayoutParams(layoutParams);
            } else {
                this.v.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
            }
            int round2 = Math.round(((float) size) * 0.4f);
            this.e.setMinWidth(round2);
            this.e.setMinimumWidth(round2);
            if (this.C.f()) {
                TextView textView = this.j;
                if (this.C.e()) {
                    textView.setBackgroundColor(0);
                } else {
                    textView.setBackgroundColor(dv.a(textView.getCurrentTextColor(), 92.0f));
                }
                this.j.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            }
        }
        super.onMeasure(i2, i3);
    }

    private FrameLayout.LayoutParams a(NativeAdImage nativeAdImage, int i2, int i3, int i4) {
        d a2 = a(this.b.getImageAppearance().getWidthConstraint()).a(getContext(), i2, nativeAdImage.getWidth(), nativeAdImage.getHeight());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2.a(), a2.b());
        layoutParams.leftMargin = i3;
        layoutParams.rightMargin = i4;
        return layoutParams;
    }

    private static int a(int i2, int i3, int i4) {
        if (i3 == 0) {
            return i4;
        }
        return Math.round(((float) i4) * (((float) i2) / ((float) i3)));
    }

    /* access modifiers changed from: 0000 */
    public final TextView a() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public final TextView b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public final Button c() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public final TextView d() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public final ImageView e() {
        ImageView imageView = this.g;
        if (this.D == null) {
            return imageView;
        }
        NativeAdImage image = this.B.getImage();
        if (image == null) {
            return imageView;
        }
        if (d.b(image)) {
            return this.o;
        }
        return d.a(image) ? this.w : imageView;
    }

    /* access modifiers changed from: 0000 */
    public final MediaView f() {
        return this.y;
    }

    /* access modifiers changed from: 0000 */
    public final TextView g() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public final TextView h() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public final TextView i() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public final ImageView j() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public final View k() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public final TextView l() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public final ImageView m() {
        return this.n;
    }
}
