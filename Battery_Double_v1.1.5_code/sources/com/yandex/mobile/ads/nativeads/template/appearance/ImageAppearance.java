package com.yandex.mobile.ads.nativeads.template.appearance;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.yandex.mobile.ads.nativeads.template.SizeConstraint;

public final class ImageAppearance implements Parcelable {
    public static final Creator<ImageAppearance> CREATOR = new Creator<ImageAppearance>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new ImageAppearance[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new ImageAppearance(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final SizeConstraint f5943a;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public SizeConstraint f5944a;

        public final ImageAppearance build() {
            return new ImageAppearance(this, 0);
        }

        public final Builder setWidthConstraint(SizeConstraint sizeConstraint) {
            this.f5944a = sizeConstraint;
            return this;
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ ImageAppearance(Builder builder, byte b) {
        this(builder);
    }

    private ImageAppearance(Builder builder) {
        this.f5943a = builder.f5944a;
    }

    public final SizeConstraint getWidthConstraint() {
        return this.f5943a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ImageAppearance imageAppearance = (ImageAppearance) obj;
        return this.f5943a == null ? imageAppearance.f5943a == null : this.f5943a.equals(imageAppearance.f5943a);
    }

    public final int hashCode() {
        if (this.f5943a != null) {
            return this.f5943a.hashCode();
        }
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f5943a, i);
    }

    protected ImageAppearance(Parcel parcel) {
        this.f5943a = (SizeConstraint) parcel.readParcelable(SizeConstraint.class.getClassLoader());
    }
}
