package com.yandex.mobile.ads.nativeads.template;

import android.content.Context;
import android.widget.RatingBar;
import com.yandex.mobile.ads.nativeads.Rating;

final class f extends RatingBar implements Rating {
    f(Context context) {
        super(context, null, 16842877);
    }

    public final void setRating(Float f) {
        super.setRating(f.floatValue());
    }

    public final float getRating() {
        return super.getRating();
    }
}
