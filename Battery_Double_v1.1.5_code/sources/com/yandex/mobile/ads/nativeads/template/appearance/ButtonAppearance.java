package com.yandex.mobile.ads.nativeads.template.appearance;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class ButtonAppearance implements Parcelable {
    public static final Creator<ButtonAppearance> CREATOR = new Creator<ButtonAppearance>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new ButtonAppearance[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new ButtonAppearance(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final int f5941a;
    private final float b;
    private final int c;
    private final int d;
    private final TextAppearance e;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public int f5942a;
        /* access modifiers changed from: private */
        public float b;
        /* access modifiers changed from: private */
        public int c;
        /* access modifiers changed from: private */
        public int d;
        /* access modifiers changed from: private */
        public TextAppearance e;

        public final ButtonAppearance build() {
            return new ButtonAppearance(this, 0);
        }

        public final Builder setBorderColor(int i) {
            this.f5942a = i;
            return this;
        }

        public final Builder setBorderWidth(float f) {
            this.b = f;
            return this;
        }

        public final Builder setNormalColor(int i) {
            this.c = i;
            return this;
        }

        public final Builder setPressedColor(int i) {
            this.d = i;
            return this;
        }

        public final Builder setTextAppearance(TextAppearance textAppearance) {
            this.e = textAppearance;
            return this;
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ ButtonAppearance(Builder builder, byte b2) {
        this(builder);
    }

    private ButtonAppearance(Builder builder) {
        this.f5941a = builder.f5942a;
        this.b = builder.b;
        this.c = builder.c;
        this.d = builder.d;
        this.e = builder.e;
    }

    public final int getBorderColor() {
        return this.f5941a;
    }

    public final float getBorderWidth() {
        return this.b;
    }

    public final int getNormalColor() {
        return this.c;
    }

    public final int getPressedColor() {
        return this.d;
    }

    public final TextAppearance getTextAppearance() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ButtonAppearance buttonAppearance = (ButtonAppearance) obj;
        if (this.f5941a == buttonAppearance.f5941a && Float.compare(buttonAppearance.b, this.b) == 0 && this.c == buttonAppearance.c && this.d == buttonAppearance.d) {
            return this.e == null ? buttonAppearance.e == null : this.e.equals(buttonAppearance.e);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int floatToIntBits = ((((((this.f5941a * 31) + (this.b != 0.0f ? Float.floatToIntBits(this.b) : 0)) * 31) + this.c) * 31) + this.d) * 31;
        if (this.e != null) {
            i = this.e.hashCode();
        }
        return floatToIntBits + i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f5941a);
        parcel.writeFloat(this.b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeParcelable(this.e, 0);
    }

    protected ButtonAppearance(Parcel parcel) {
        this.f5941a = parcel.readInt();
        this.b = parcel.readFloat();
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = (TextAppearance) parcel.readParcelable(TextAppearance.class.getClassLoader());
    }
}
