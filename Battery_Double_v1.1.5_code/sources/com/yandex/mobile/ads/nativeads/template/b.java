package com.yandex.mobile.ads.nativeads.template;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.widget.ImageView;

final class b {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static final int f5951a = Color.parseColor("#eaeaea");
    private final ImageView[] b;

    b(@NonNull ImageView... imageViewArr) {
        this.b = imageViewArr;
        b();
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        ImageView[] imageViewArr;
        for (ImageView imageView : this.b) {
            if (VERSION.SDK_INT >= 11) {
                ColorDrawable colorDrawable = (ColorDrawable) imageView.getBackground();
                Drawable drawable = imageView.getDrawable();
                if (drawable != null && colorDrawable.getAlpha() == 255) {
                    a(colorDrawable, 255, 0).start();
                    a(drawable, 0, 255).start();
                }
            }
        }
    }

    private void b() {
        ImageView[] imageViewArr;
        for (ImageView imageView : this.b) {
            if (imageView != null) {
                if (imageView.getDrawable() != null) {
                    imageView.setBackgroundColor(0);
                } else {
                    imageView.setBackgroundColor(f5951a);
                }
            }
        }
    }

    @TargetApi(11)
    private static ObjectAnimator a(Drawable drawable, int i, int i2) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(drawable, new PropertyValuesHolder[]{PropertyValuesHolder.ofInt("alpha", new int[]{i, i2})});
        ofPropertyValuesHolder.setTarget(drawable);
        ofPropertyValuesHolder.setDuration(500);
        return ofPropertyValuesHolder;
    }
}
