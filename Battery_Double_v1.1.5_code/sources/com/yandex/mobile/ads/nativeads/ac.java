package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.impl.am.a;
import com.yandex.mobile.ads.impl.bg;
import com.yandex.mobile.ads.impl.pv;
import java.util.Iterator;
import java.util.List;

final class ac extends ae {
    private final List<? extends ay> b;

    public ac(@NonNull List<? extends ay> list, @NonNull as asVar) {
        super(asVar);
        this.b = list;
    }

    /* access modifiers changed from: protected */
    public final am a(@NonNull Context context, a aVar) {
        boolean z;
        if (a.SUCCESS == aVar) {
            Iterator it = this.b.iterator();
            while (true) {
                z = true;
                boolean z2 = false;
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                ay ayVar = (ay) it.next();
                bc d = ayVar.d();
                Iterator it2 = ayVar.e().b().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (d.a(context, ((bg) it2.next()).c()).b() != a.SUCCESS) {
                            continue;
                            break;
                        }
                    } else {
                        z2 = true;
                        continue;
                        break;
                    }
                }
                if (z2) {
                    break;
                }
            }
            if (!z) {
                aVar = a.NO_VISIBLE_ADS;
            }
        }
        return new am(aVar, new pv());
    }
}
