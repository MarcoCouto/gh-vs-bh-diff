package com.yandex.mobile.ads.nativeads;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.cg;
import com.yandex.mobile.ads.impl.db;
import com.yandex.mobile.ads.impl.gu.b;
import java.util.HashMap;

public final class g implements db {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Handler f5912a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @NonNull
    public final AdTapHandler b;

    public g(@NonNull AdTapHandler adTapHandler) {
        this.b = adTapHandler;
    }

    public final void a(@NonNull cg cgVar, @Nullable final String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("click_type", "custom");
        cgVar.a(b.CLICK, hashMap);
        this.f5912a.post(new Runnable() {
            public final void run() {
                g.this.b.handleAdTapWithURL(str);
            }
        });
    }
}
