package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.nativeads.as.a;

final class ar implements a {

    /* renamed from: a reason: collision with root package name */
    private final String f5892a;
    private final am.a b;

    ar(@NonNull am.a aVar, @Nullable String str) {
        this.f5892a = str;
        this.b = aVar;
    }

    public final String a() {
        return this.f5892a;
    }

    public final am.a b() {
        return this.b;
    }
}
