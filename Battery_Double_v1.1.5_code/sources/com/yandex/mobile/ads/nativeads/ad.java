package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ki;
import com.yandex.mobile.ads.impl.lg;
import com.yandex.mobile.ads.impl.ni;
import java.util.List;

final class ad implements bd {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final List<ni> f5881a;

    public final void a() {
    }

    public final void a(@NonNull af afVar) {
    }

    ad(@Nullable List<ni> list) {
        this.f5881a = list;
    }

    public final void a(@NonNull af afVar, @NonNull e eVar) {
        if (this.f5881a != null) {
            lg lgVar = new lg(afVar, eVar);
            for (ni niVar : this.f5881a) {
                ki a2 = afVar.a(niVar);
                if (a2 != null) {
                    a2.a(niVar.c());
                    a2.a(niVar, lgVar);
                }
            }
        }
    }
}
