package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.ck;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.impl.v;
import com.yandex.mobile.ads.nativeads.NativeAdLoader.OnImageAdLoadListener;
import com.yandex.mobile.ads.nativeads.NativeAdLoader.OnLoadListener;

public final class w {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Object f5958a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @NonNull
    public final ck c;
    /* access modifiers changed from: private */
    @Nullable
    public OnLoadListener d;

    w(@NonNull Context context) {
        this.c = new ck(context);
    }

    public final void a(@NonNull fc fcVar) {
        this.c.a(fcVar);
    }

    public final void a(@NonNull a aVar) {
        this.c.a(aVar);
    }

    public final void a(@Nullable OnLoadListener onLoadListener) {
        synchronized (f5958a) {
            this.d = onLoadListener;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.b.removeCallbacksAndMessages(null);
    }

    public final void a(@NonNull AdRequestError adRequestError) {
        b(adRequestError);
    }

    public final void a(@NonNull final NativeGenericAd nativeGenericAd) {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (w.f5958a) {
                    if (w.this.d != null) {
                        if (nativeGenericAd instanceof NativeContentAd) {
                            w.this.c.a();
                            w.this.d.onContentAdLoaded((NativeContentAd) nativeGenericAd);
                        } else if (nativeGenericAd instanceof NativeAppInstallAd) {
                            w.this.c.a();
                            w.this.d.onAppInstallAdLoaded((NativeAppInstallAd) nativeGenericAd);
                        } else if ((nativeGenericAd instanceof NativeImageAd) && (w.this.d instanceof OnImageAdLoadListener)) {
                            w.this.c.a();
                            ((OnImageAdLoadListener) w.this.d).onImageAdLoaded((NativeImageAd) nativeGenericAd);
                        } else if (!(nativeGenericAd instanceof bb) || !(w.this.d instanceof bg)) {
                            w.this.b(v.f5815a);
                        } else {
                            w.this.c.a();
                            w.this.d;
                        }
                    }
                }
            }
        });
    }

    public final void a(@NonNull final z zVar) {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (w.f5958a) {
                    if (w.this.d != null) {
                        if (w.this.d instanceof bg) {
                            w.this.c.a();
                            w.this.d;
                        } else {
                            w.this.b(v.f5815a);
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull final AdRequestError adRequestError) {
        this.c.a(adRequestError);
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (w.f5958a) {
                    if (w.this.d != null) {
                        w.this.d.onAdFailedToLoad(adRequestError);
                    }
                }
            }
        });
    }
}
