package com.yandex.mobile.ads.nativeads;

import android.graphics.Bitmap;

public final class NativeAdImage {

    /* renamed from: a reason: collision with root package name */
    private Bitmap f5869a;
    private String b;
    private int c;
    private int d;

    NativeAdImage() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(Bitmap bitmap) {
        this.f5869a = bitmap;
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i) {
        this.c = i;
    }

    /* access modifiers changed from: 0000 */
    public final void b(int i) {
        this.d = i;
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str) {
        this.b = str;
    }

    public final String a() {
        return this.b;
    }

    public final Bitmap getBitmap() {
        return this.f5869a;
    }

    public final int getHeight() {
        return this.c;
    }

    public final int getWidth() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NativeAdImage nativeAdImage = (NativeAdImage) obj;
        if (this.c != nativeAdImage.c || this.d != nativeAdImage.d) {
            return false;
        }
        if (this.f5869a == null ? nativeAdImage.f5869a == null : this.f5869a.equals(nativeAdImage.f5869a)) {
            return this.b == null ? nativeAdImage.b == null : this.b.equals(nativeAdImage.b);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.f5869a != null ? this.f5869a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return ((((hashCode + i) * 31) + this.c) * 31) + this.d;
    }
}
