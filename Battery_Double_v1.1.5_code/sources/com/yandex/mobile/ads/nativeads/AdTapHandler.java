package com.yandex.mobile.ads.nativeads;

import android.support.annotation.Nullable;

interface AdTapHandler {
    void handleAdTapWithURL(@Nullable String str);
}
