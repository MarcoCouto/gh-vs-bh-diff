package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.aj.a;

final class ag implements ai<View> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final NativeAdViewBinder f5884a;

    @NonNull
    public final /* synthetic */ aj a(@NonNull Object obj) {
        return new a((View) obj).a(this.f5884a.getAgeView()).b(this.f5884a.getBodyView()).c((TextView) this.f5884a.getCallToActionView()).d(this.f5884a.getDomainView()).a(this.f5884a.getFaviconView()).a(this.f5884a.getFeedbackView()).b(this.f5884a.getIconView()).c(this.f5884a.getImageView()).a(this.f5884a.getMediaView()).e(this.f5884a.getPriceView()).a(this.f5884a.getRatingView()).f(this.f5884a.getReviewCountView()).g(this.f5884a.getSponsoredView()).h(this.f5884a.getTitleView()).i(this.f5884a.getWarningView()).a();
    }

    ag(@NonNull NativeAdViewBinder nativeAdViewBinder) {
        this.f5884a = nativeAdViewBinder;
    }
}
