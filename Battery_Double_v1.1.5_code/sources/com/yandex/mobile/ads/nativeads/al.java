package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ca;

public final class al implements ca {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bc f5889a;

    public al(@NonNull bc bcVar) {
        this.f5889a = bcVar;
    }

    public final boolean a() {
        return !this.f5889a.a();
    }
}
