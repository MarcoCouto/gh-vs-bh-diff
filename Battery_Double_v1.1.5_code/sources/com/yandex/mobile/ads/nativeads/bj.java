package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.db;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.ff;
import com.yandex.mobile.ads.impl.fg;
import com.yandex.mobile.ads.impl.my;

public final class bj {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private AdTapHandler f5903a;

    @NonNull
    public final db a(@NonNull Context context, @NonNull fc fcVar, @NonNull ResultReceiver resultReceiver) {
        if (this.f5903a != null) {
            fg a2 = ff.a().a(context);
            if (a2 != null && a2.b()) {
                return new g(this.f5903a);
            }
        }
        return new my(context, fcVar, resultReceiver);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable AdTapHandler adTapHandler) {
        this.f5903a = adTapHandler;
    }
}
