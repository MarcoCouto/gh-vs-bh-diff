package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.yandex.mobile.ads.impl.np;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.List;

public final class ao extends m implements ap {
    public ao(@NonNull Context context, @NonNull np npVar, @NonNull v vVar, @NonNull i iVar, @NonNull c cVar) {
        super(context, npVar, vVar, iVar, cVar);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(IronSourceSegment.AGE);
        arrayList.add(TtmlNode.TAG_BODY);
        arrayList.add("call_to_action");
        arrayList.add(SettingsJsonConstants.APP_ICON_KEY);
        arrayList.add(CampaignEx.JSON_KEY_STAR);
        arrayList.add("sponsored");
        arrayList.add("title");
        return arrayList;
    }

    public final void bindAppInstallAd(@Nullable NativeAppInstallAdView nativeAppInstallAdView) throws NativeAdException {
        if (nativeAppInstallAdView != null) {
            nativeAppInstallAdView.a(this);
            a(nativeAppInstallAdView, new aq());
        }
    }
}
