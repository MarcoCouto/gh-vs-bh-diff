package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.as;
import com.yandex.mobile.ads.impl.lp;
import com.yandex.mobile.ads.impl.lq;
import com.yandex.mobile.ads.impl.ni;
import com.yandex.mobile.ads.impl.nm;

public final class e {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f5910a;
    @NonNull
    private final as b;

    e(@NonNull a aVar, @NonNull as asVar) {
        this.f5910a = aVar;
        this.b = asVar;
    }

    public final void a(@NonNull ni niVar, @Nullable nm nmVar, @NonNull af afVar, @NonNull lp lpVar) {
        if (niVar.e() && nmVar != null) {
            lpVar.a(nmVar, new lq(niVar, this.f5910a, afVar, this.b));
        }
    }
}
