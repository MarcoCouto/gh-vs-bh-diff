package com.yandex.mobile.ads.nativeads;

public final class NativeAdMedia {

    /* renamed from: a reason: collision with root package name */
    private final float f5873a;

    NativeAdMedia(float f) {
        this.f5873a = f;
    }

    public final float getAspectRatio() {
        return this.f5873a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && getClass() == obj.getClass() && Float.compare(((NativeAdMedia) obj).f5873a, this.f5873a) == 0;
    }

    public final int hashCode() {
        if (this.f5873a != 0.0f) {
            return Float.floatToIntBits(this.f5873a);
        }
        return 0;
    }
}
