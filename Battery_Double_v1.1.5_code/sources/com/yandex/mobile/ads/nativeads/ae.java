package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import android.view.View;
import com.yandex.mobile.ads.impl.ag;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.impl.am.a;
import com.yandex.mobile.ads.impl.ce;
import com.yandex.mobile.ads.impl.dv;

class ae implements bc {
    @VisibleForTesting
    @NonNull

    /* renamed from: a reason: collision with root package name */
    final as f5882a;
    @NonNull
    private final ag b = ag.a();
    @Nullable
    private af c;

    ae(@NonNull as asVar) {
        this.f5882a = asVar;
    }

    public final void a(@NonNull af afVar) {
        this.c = afVar;
        this.f5882a.a(afVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003b  */
    @NonNull
    public final am a(@NonNull Context context, int i) {
        a aVar;
        boolean z;
        boolean z2 = true;
        String str = null;
        if (!this.b.a(context)) {
            aVar = a.APPLICATION_INACTIVE;
        } else if (a()) {
            aVar = a.SUPERVIEW_HIDDEN;
        } else {
            if (this.c != null) {
                View b2 = this.c.b();
                if (b2 != null) {
                    int height = b2.getHeight();
                    if (b2.getWidth() >= 10 && height >= 10) {
                        z = false;
                        if (!z) {
                            aVar = a.TOO_SMALL;
                        } else {
                            if (this.c != null && dv.a(this.c.b(), i)) {
                                z2 = false;
                            }
                            if (z2) {
                                aVar = a.NOT_VISIBLE_FOR_PERCENT;
                            } else {
                                as.a a2 = this.f5882a.a();
                                a b3 = a2.b();
                                str = a2.a();
                                aVar = b3;
                            }
                        }
                    }
                }
            }
            z = true;
            if (!z) {
            }
        }
        Pair pair = new Pair(aVar, str);
        am a3 = a(context, (a) pair.first);
        a3.a((String) pair.second);
        return a3;
    }

    /* access modifiers changed from: protected */
    public am a(@NonNull Context context, a aVar) {
        return new am(aVar, new ce());
    }

    @VisibleForTesting
    public final boolean a() {
        if (this.c != null) {
            View b2 = this.c.b();
            if (b2 != null) {
                return dv.d(b2);
            }
        }
        return true;
    }

    @NonNull
    public final bi b() {
        return this.f5882a.e();
    }
}
