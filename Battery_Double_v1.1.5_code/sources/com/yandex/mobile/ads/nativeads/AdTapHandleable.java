package com.yandex.mobile.ads.nativeads;

import android.support.annotation.Nullable;

interface AdTapHandleable {
    void setAdTapHandler(@Nullable AdTapHandler adTapHandler);
}
