package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.nl;
import com.yandex.mobile.ads.impl.od;
import com.yandex.mobile.ads.impl.od.b;
import java.util.Map;

public final class d implements i {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5909a;

    public final void a(@NonNull Map<String, Bitmap> map) {
    }

    public d(@NonNull Context context) {
        this.f5909a = context.getApplicationContext();
    }

    @Nullable
    public final Bitmap a(@NonNull nl nlVar) {
        b a2 = od.a(this.f5909a).a();
        String c = nlVar.c();
        if (c == null) {
            return null;
        }
        Bitmap a3 = a2.a(c);
        if (a3 == null || a3.getWidth() != 1 || a3.getHeight() != 1) {
            return a3;
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(a3, nlVar.a(), nlVar.b(), false);
        a2.a(c, createScaledBitmap);
        return createScaledBitmap;
    }
}
