package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.np;
import com.yandex.mobile.ads.impl.nq;
import java.util.ArrayList;
import java.util.List;

public final class n {
    @NonNull
    public static bf a(@NonNull o oVar, @NonNull np npVar) {
        return new bf(npVar.c(), a((T) npVar.d(), oVar.c().e()), a((T) npVar.e(), oVar.c().f()));
    }

    @NonNull
    public static c a(@NonNull o oVar, @NonNull bf bfVar, @NonNull bd bdVar, @NonNull s sVar) {
        c cVar = new c(oVar, new ae(new l(bfVar.a(), am.a())), bfVar, bdVar, sVar);
        return cVar;
    }

    @NonNull
    private static List<ay> a(@NonNull List<? extends NativeGenericAd> list) {
        ArrayList arrayList = new ArrayList();
        for (NativeGenericAd nativeGenericAd : list) {
            arrayList.add((ay) nativeGenericAd);
        }
        return arrayList;
    }

    @NonNull
    private static <T> List<T> a(@Nullable T t, @Nullable List<T> list) {
        ArrayList arrayList = new ArrayList();
        if (t != null) {
            arrayList.add(t);
        }
        if (list != null) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    @NonNull
    public static z a(@NonNull Context context, @NonNull o oVar, @NonNull i iVar, @NonNull s sVar) {
        ArrayList arrayList = new ArrayList();
        List<np> c = oVar.c().c();
        be b = sVar.b();
        for (np npVar : c) {
            bd a2 = b.a(npVar);
            v vVar = new v(context, npVar, iVar, a2);
            c a3 = a(oVar, new bf(npVar.c(), a((T) npVar.d(), null), a((T) npVar.e(), null)), a2, sVar);
            if (NativeAdType.CONTENT == npVar.b()) {
                au auVar = new au(context, npVar, vVar, iVar, a3);
                arrayList.add(auVar);
            } else if (NativeAdType.APP_INSTALL == npVar.b()) {
                ao aoVar = new ao(context, npVar, vVar, iVar, a3);
                arrayList.add(aoVar);
            } else if (NativeAdType.IMAGE == npVar.b()) {
                az azVar = new az(context, npVar, vVar, iVar, a3);
                arrayList.add(azVar);
            }
        }
        List a4 = a(arrayList);
        nq c2 = oVar.c();
        bf bfVar = new bf(c2.b(), a((T) null, c2.e()), a((T) null, c2.f()));
        c cVar = new c(oVar, new ac(a4, new aa(bfVar.a(), am.a())), bfVar, new ad(bfVar.a()), sVar);
        return new ab(context, arrayList, iVar, cVar);
    }
}
