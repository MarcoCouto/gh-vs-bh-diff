package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

final class ah {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5885a = new Object();
    private static volatile ah b;
    @NonNull
    private final Map<View, ay> c = new WeakHashMap();

    ah() {
    }

    @NonNull
    static ah a() {
        if (b == null) {
            synchronized (f5885a) {
                if (b == null) {
                    b = new ah();
                }
            }
        }
        return b;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final ay a(@NonNull View view) {
        ay ayVar;
        synchronized (f5885a) {
            ayVar = (ay) this.c.get(view);
        }
        return ayVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull View view, @NonNull ay ayVar) {
        synchronized (f5885a) {
            this.c.put(view, ayVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@NonNull ay ayVar) {
        Iterator it = this.c.entrySet().iterator();
        boolean z = false;
        while (it.hasNext()) {
            if (((ay) ((Entry) it.next()).getValue()) == ayVar) {
                it.remove();
                z = true;
            }
        }
        return z;
    }
}
