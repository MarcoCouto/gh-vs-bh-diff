package com.yandex.mobile.ads.nativeads;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.yandex.mobile.ads.impl.lk;
import com.yandex.mobile.ads.impl.ll;
import com.yandex.mobile.ads.impl.ln;
import com.yandex.mobile.ads.impl.qa;
import java.lang.ref.WeakReference;

final class bm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final qa f5904a = new qa();
    @NonNull
    private final lk b = ll.a();
    /* access modifiers changed from: private */
    @Nullable
    public b c;
    @Nullable
    private ln d;
    @Nullable
    private a e;

    private class a implements OnGlobalLayoutListener {
        @NonNull
        private final WeakReference<View> b;

        a(View view) {
            this.b = new WeakReference<>(view);
        }

        public final void onGlobalLayout() {
            View view = (View) this.b.get();
            if (view != null) {
                Integer valueOf = Integer.valueOf(view.getVisibility());
                if (view.getTag() != valueOf) {
                    view.setTag(valueOf);
                    if (bm.this.c != null) {
                        if (valueOf.intValue() == 0) {
                            bm.this.c.a();
                            return;
                        }
                        bm.this.c.b();
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            if (VERSION.SDK_INT >= 16) {
                View view = (View) this.b.get();
                if (view != null) {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        }
    }

    interface b {
        void a();

        void b();
    }

    private class c implements ln {
        private final WeakReference<Context> b;

        c(Context context) {
            this.b = new WeakReference<>(context);
        }

        public final void a(@NonNull Activity activity) {
            Context context = (Context) this.b.get();
            if (context != null && context.equals(activity) && bm.this.c != null) {
                bm.this.c.a();
            }
        }

        public final void b(@NonNull Activity activity) {
            Context context = (Context) this.b.get();
            if (context != null && context.equals(activity) && bm.this.c != null) {
                bm.this.c.b();
            }
        }
    }

    bm() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull View view, @NonNull b bVar) {
        this.c = bVar;
        b(view.getContext());
        Context a2 = qa.a(view.getContext());
        if (a2 != null) {
            this.d = new c(a2);
            this.e = new a(view);
            this.b.a(a2, this.d);
            view.getViewTreeObserver().addOnGlobalLayoutListener(this.e);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context) {
        this.c = null;
        b(context);
    }

    private void b(@NonNull Context context) {
        if (this.d != null) {
            this.b.b(context, this.d);
        }
        if (this.e != null) {
            this.e.a();
        }
    }
}
