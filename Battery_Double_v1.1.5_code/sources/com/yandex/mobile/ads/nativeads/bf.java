package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.bg;
import com.yandex.mobile.ads.impl.ni;
import java.util.List;

public final class bf {

    /* renamed from: a reason: collision with root package name */
    private final List<ni> f5900a;
    private final List<bg> b;
    @NonNull
    private final List<String> c;

    public bf(@Nullable List<ni> list, @NonNull List<bg> list2, @NonNull List<String> list3) {
        this.f5900a = list;
        this.b = list2;
        this.c = list3;
    }

    @Nullable
    public final List<ni> a() {
        return this.f5900a;
    }

    @NonNull
    public final List<bg> b() {
        return this.b;
    }

    @NonNull
    public final List<String> c() {
        return this.c;
    }
}
