package com.yandex.mobile.ads.nativeads;

import com.yandex.mobile.ads.nativeads.NativeAdEventListener.SimpleNativeAdEventListener;

public interface ClosableNativeAdEventListener extends NativeAdEventListener {

    public static class SimpleClosableNativeAdEventListener extends SimpleNativeAdEventListener implements ClosableNativeAdEventListener {
        public void closeNativeAd() {
        }
    }

    void closeNativeAd();
}
