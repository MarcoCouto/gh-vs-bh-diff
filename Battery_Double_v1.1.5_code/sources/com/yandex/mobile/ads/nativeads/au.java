package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.np;
import java.util.ArrayList;
import java.util.List;

public final class au extends m implements av {
    public au(@NonNull Context context, @NonNull np npVar, @NonNull v vVar, @NonNull i iVar, @NonNull c cVar) {
        super(context, npVar, vVar, iVar, cVar);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(TtmlNode.TAG_BODY);
        arrayList.add("domain");
        arrayList.add("sponsored");
        arrayList.add("title");
        return arrayList;
    }

    public final void bindContentAd(@Nullable NativeContentAdView nativeContentAdView) throws NativeAdException {
        if (nativeContentAdView != null) {
            nativeContentAdView.a(this);
            a(nativeContentAdView, new aw());
        }
    }
}
