package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.cn;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.impl.np;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class an implements a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fc f5891a;
    @NonNull
    private final List<np> b;
    @NonNull
    private final p c = new p();
    @NonNull
    private final cn d = new cn();
    @Nullable
    private String e;
    @Nullable
    private a f;

    an(@NonNull List<np> list, @NonNull fc fcVar) {
        this.b = list;
        this.f5891a = fcVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull a aVar) {
        this.f = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str) {
        this.e = str;
    }

    @NonNull
    public final Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        if (this.f != null) {
            hashMap.put("bind_type", this.f.c);
        }
        if (this.e != null) {
            hashMap.put("native_ad_type", this.e);
        }
        hashMap.putAll(cn.a(this.f5891a.c()));
        List a2 = p.a(this.b);
        if (a2.size() > 0) {
            hashMap.put("image_sizes", a2.toArray(new String[a2.size()]));
        }
        return hashMap;
    }
}
