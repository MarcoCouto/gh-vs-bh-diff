package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.aj.a;

final class ba implements ai<NativeImageAdView> {
    ba() {
    }

    @NonNull
    public final /* synthetic */ aj a(@NonNull Object obj) {
        NativeImageAdView nativeImageAdView = (NativeImageAdView) obj;
        return new a(nativeImageAdView).a(nativeImageAdView.a()).c(nativeImageAdView.b()).a(nativeImageAdView.c()).a();
    }
}
