package com.yandex.mobile.ads.nativeads;

import android.support.annotation.Nullable;

public final class bi {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f5902a;
    private final boolean b;

    bi(boolean z, @Nullable String str) {
        this.b = z;
        this.f5902a = str;
    }

    @Nullable
    public final String a() {
        return this.f5902a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.b;
    }
}
