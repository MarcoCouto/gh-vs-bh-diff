package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;

public class NativeImageAdView extends t<az> {

    /* renamed from: a reason: collision with root package name */
    private Button f5879a;
    private ImageView b;
    private MediaView c;

    public NativeImageAdView(Context context) {
        super(context);
    }

    public NativeImageAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeImageAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setFeedbackView(Button button) {
        this.f5879a = button;
    }

    public void setImageView(ImageView imageView) {
        this.b = imageView;
    }

    public void setMediaView(MediaView mediaView) {
        this.c = mediaView;
    }

    /* access modifiers changed from: 0000 */
    public final Button a() {
        return this.f5879a;
    }

    /* access modifiers changed from: 0000 */
    public final ImageView b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final MediaView c() {
        return this.c;
    }
}
