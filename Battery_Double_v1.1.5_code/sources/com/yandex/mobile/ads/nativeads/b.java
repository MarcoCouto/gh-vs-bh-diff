package com.yandex.mobile.ads.nativeads;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.y;
import com.yandex.mobile.ads.impl.y.a;
import java.lang.ref.WeakReference;

public final class b implements a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final y f5899a = new y(new Handler(Looper.getMainLooper()));
    @NonNull
    private final WeakReference<q> b;

    public b(@NonNull q qVar) {
        this.b = new WeakReference<>(qVar);
        this.f5899a.a(this);
    }

    public final void a(int i, @Nullable Bundle bundle) {
        q qVar = (q) this.b.get();
        if (qVar != null) {
            switch (i) {
                case 6:
                    qVar.e();
                    break;
                case 7:
                    qVar.d();
                    return;
                case 8:
                    qVar.b();
                    return;
                case 9:
                    qVar.a();
                    return;
            }
        }
    }

    @NonNull
    public final y a() {
        return this.f5899a;
    }
}
