package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.ni;
import java.util.List;

interface as {

    public interface a {
        String a();

        com.yandex.mobile.ads.impl.am.a b();
    }

    @VisibleForTesting
    public interface b {
        boolean a(@NonNull List<ni> list);
    }

    a a();

    void a(af afVar);

    @NonNull
    bi e();
}
