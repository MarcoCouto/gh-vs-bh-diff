package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.impl.np;
import com.yandex.mobile.ads.nativeads.template.NativeBannerView;
import com.yandex.mobile.ads.nativeads.template.c;
import java.util.Collections;

public abstract class m extends ay implements ax, x {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    protected i f5924a;
    @NonNull
    private final v b;
    @NonNull
    private final f c;
    @NonNull
    private final bm d;
    @NonNull
    private final an e;

    m(@NonNull Context context, @NonNull np npVar, @NonNull v vVar, @NonNull i iVar, @NonNull c cVar) {
        super(context, cVar);
        this.b = vVar;
        this.f5924a = iVar;
        o a2 = cVar.a();
        this.c = f.a(a2.c().d());
        an anVar = new an(Collections.singletonList(npVar), a2.a());
        NativeAdType b2 = npVar.b();
        if (b2 != null) {
            anVar.a(b2.getValue());
        }
        this.e = anVar;
        a((a) this.e);
        this.d = new bm();
    }

    public final void a(@NonNull NativeBannerView nativeBannerView) throws NativeAdException {
        c cVar = new c();
        nativeBannerView.a(this);
        a(nativeBannerView, cVar, this.c, a.TEMPLATE);
    }

    public void bindNativeAd(@NonNull NativeAdViewBinder nativeAdViewBinder) throws NativeAdException {
        View nativeAdView = nativeAdViewBinder.getNativeAdView();
        this.d.a(nativeAdView, new b() {
            public final void a() {
                m.this.b();
            }

            public final void b() {
                m.this.c();
            }
        });
        a(nativeAdView, new ag(nativeAdViewBinder), f.a(), a.CUSTOM);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull t tVar, @NonNull ai aiVar) throws NativeAdException {
        a(tVar, aiVar, f.a(), a.CUSTOM);
    }

    private <T extends View> void a(@NonNull T t, @NonNull ai<T> aiVar, @NonNull f fVar, @NonNull a aVar) throws NativeAdException {
        this.e.a(aVar);
        a(t, this.f5924a, aiVar, fVar);
    }

    public NativeAdType getAdType() {
        return this.b.a();
    }

    public String getInfo() {
        return this.b.b();
    }

    public NativeAdAssets getAdAssets() {
        return this.b.d();
    }

    public void loadImages() {
        this.b.c();
    }

    public void addImageLoadingListener(NativeAdImageLoadingListener nativeAdImageLoadingListener) {
        this.b.a(nativeAdImageLoadingListener);
    }

    public void removeImageLoadingListener(NativeAdImageLoadingListener nativeAdImageLoadingListener) {
        this.b.b(nativeAdImageLoadingListener);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context) {
        this.d.a(context);
        super.a(context);
    }
}
