package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ks;

public final class s {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final r f5929a;
    @NonNull
    private final be b;
    @NonNull
    private final ks c;
    @NonNull
    private final bl d;

    public s(@NonNull r rVar, @NonNull be beVar, @NonNull ks ksVar, @NonNull bl blVar) {
        this.f5929a = rVar;
        this.b = beVar;
        this.c = ksVar;
        this.d = blVar;
    }

    @NonNull
    public final r a() {
        return this.f5929a;
    }

    @NonNull
    public final be b() {
        return this.b;
    }

    @NonNull
    public final ks c() {
        return this.c;
    }

    @NonNull
    public final bl d() {
        return this.d;
    }
}
