package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;

public final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final o f5908a;
    @NonNull
    private final bc b;
    @NonNull
    private final bf c;
    @NonNull
    private final bd d;
    @NonNull
    private final s e;

    c(@NonNull o oVar, @NonNull bc bcVar, @NonNull bf bfVar, @NonNull bd bdVar, @NonNull s sVar) {
        this.f5908a = oVar;
        this.b = bcVar;
        this.c = bfVar;
        this.d = bdVar;
        this.e = sVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final o a() {
        return this.f5908a;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bc b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bf c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bd d() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final s e() {
        return this.e;
    }
}
