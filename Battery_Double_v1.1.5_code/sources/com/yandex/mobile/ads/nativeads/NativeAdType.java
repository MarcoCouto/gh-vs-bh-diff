package com.yandex.mobile.ads.nativeads;

import com.facebook.share.internal.MessengerShareContentUtility;

public enum NativeAdType {
    CONTENT("content"),
    APP_INSTALL("app"),
    IMAGE(MessengerShareContentUtility.MEDIA_IMAGE);
    

    /* renamed from: a reason: collision with root package name */
    private final String f5874a;

    private NativeAdType(String str) {
        this.f5874a = str;
    }

    public final String getValue() {
        return this.f5874a;
    }
}
