package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.aq;
import com.yandex.mobile.ads.impl.bg;
import com.yandex.mobile.ads.impl.gu.a;
import com.yandex.mobile.ads.impl.x;
import java.util.List;

public interface bk {
    void a(@NonNull Context context, @NonNull b bVar);

    void a(@NonNull Context context, @NonNull b bVar, @Nullable af afVar);

    void a(@NonNull aq aqVar);

    void a(@NonNull a aVar);

    void a(@NonNull x xVar, @NonNull List<bg> list);

    void a(@NonNull af afVar);
}
