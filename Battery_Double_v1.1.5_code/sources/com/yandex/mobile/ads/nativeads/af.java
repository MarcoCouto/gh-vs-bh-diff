package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.yandex.mobile.ads.impl.ap;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.ki;
import com.yandex.mobile.ads.impl.kq;
import com.yandex.mobile.ads.impl.kr;
import com.yandex.mobile.ads.impl.ni;
import com.yandex.mobile.ads.nativeads.NativeAdViewBinder.Builder;
import java.util.Map;

public final class af<T extends View> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final f f5883a;
    @NonNull
    private final i b;
    @NonNull
    private final aj c;
    @NonNull
    private final Map<String, ki> d;
    @NonNull
    private final NativeAdViewBinder e;

    public af(@NonNull T t, @NonNull ai<T> aiVar, @NonNull fc fcVar, @NonNull i iVar, @NonNull ap apVar, @NonNull f fVar, @NonNull s sVar) {
        this.f5883a = fVar;
        this.b = iVar;
        kr krVar = new kr(fcVar, apVar, sVar.c());
        this.c = aiVar.a(t);
        this.d = new kq(this.c, this.b, krVar).a();
        this.e = a(t);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        for (ki kiVar : this.d.values()) {
            if (kiVar != null) {
                kiVar.a();
            }
        }
    }

    @NonNull
    private NativeAdViewBinder a(@NonNull T t) {
        Builder builder = new Builder((View) t);
        try {
            Builder bodyView = builder.setAgeView(this.c.b()).setBodyView(this.c.c());
            TextView d2 = this.c.d();
            bodyView.setCallToActionView(d2 instanceof Button ? (Button) d2 : null).setDomainView(this.c.f()).setFaviconView(this.c.g()).setFeedbackView(this.c.h()).setIconView(this.c.i()).setImageView(this.c.j()).setMediaView(this.c.k()).setPriceView(this.c.l()).setRatingView(this.c.m()).setReviewCountView(this.c.n()).setSponsoredView(this.c.o()).setTitleView(this.c.p()).setWarningView(this.c.q());
        } catch (Exception unused) {
        }
        return builder.build();
    }

    @Nullable
    public final View b() {
        return this.c.a();
    }

    @NonNull
    public final aj c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final ki a(@Nullable ni niVar) {
        if (niVar != null) {
            return (ki) this.d.get(niVar.a());
        }
        return null;
    }

    @NonNull
    public final f d() {
        return this.f5883a;
    }

    @NonNull
    public final i e() {
        return this.b;
    }

    @NonNull
    public final NativeAdViewBinder f() {
        return this.e;
    }
}
