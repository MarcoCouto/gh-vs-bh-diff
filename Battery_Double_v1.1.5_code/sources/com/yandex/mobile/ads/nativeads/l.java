package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.impl.ki;
import com.yandex.mobile.ads.impl.ni;
import com.yandex.mobile.ads.nativeads.as.a;
import com.yandex.mobile.ads.nativeads.as.b;
import java.util.List;

class l implements as {

    /* renamed from: a reason: collision with root package name */
    final am f5919a;
    private final List<ni> b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public af d;

    l(@Nullable List<ni> list, @NonNull am amVar) {
        this.b = list;
        this.f5919a = amVar;
    }

    public void a(af afVar) {
        this.d = afVar;
    }

    @VisibleForTesting
    public boolean b() {
        return !a((b) new b() {
            public final boolean a(@NonNull List<ni> list) {
                for (ni niVar : list) {
                    if (niVar.f()) {
                        ki a2 = l.this.d.a(niVar);
                        if (a2 != null && a2.d()) {
                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }

    @VisibleForTesting
    public boolean c() {
        return !a((b) new b() {
            public final boolean a(@NonNull List<ni> list) {
                for (ni niVar : list) {
                    if (niVar.f()) {
                        ki a2 = l.this.d.a(niVar);
                        if (a2 == null || !a2.c()) {
                            l.this.c = niVar.a();
                            return false;
                        }
                    }
                }
                return true;
            }
        });
    }

    public boolean d() {
        return !a((b) new b() {
            public final boolean a(@NonNull List<ni> list) {
                for (ni niVar : list) {
                    if (niVar.f()) {
                        ki a2 = l.this.d.a(niVar);
                        Object c = niVar.c();
                        if (a2 == null || !a2.b(c)) {
                            l.this.c = niVar.a();
                            return false;
                        }
                    }
                }
                return true;
            }
        });
    }

    private boolean a(@NonNull b bVar) {
        return this.d != null && a(bVar, this.b);
    }

    /* access modifiers changed from: protected */
    public boolean a(@NonNull b bVar, @Nullable List<ni> list) {
        if (!this.f5919a.b()) {
            return true;
        }
        if (list == null || !bVar.a(list)) {
            return false;
        }
        return true;
    }

    @NonNull
    public bi e() {
        return new bi(a((b) new b() {
            public final boolean a(@NonNull List<ni> list) {
                for (ni niVar : list) {
                    if (niVar.f()) {
                        ki a2 = l.this.d.a(niVar);
                        if (a2 == null || !a2.b()) {
                            l.this.c = niVar.a();
                            return false;
                        }
                    }
                }
                return true;
            }
        }), this.c);
    }

    public a a() {
        int i;
        am.a aVar;
        boolean z = false;
        if (this.b != null) {
            i = 0;
            for (ni f : this.b) {
                if (f.f()) {
                    i++;
                }
            }
        } else {
            i = 0;
        }
        if ((i >= 2) && b()) {
            z = true;
        }
        if (z) {
            aVar = am.a.NO_VISIBLE_REQUIRED_ASSETS;
        } else if (c()) {
            aVar = am.a.REQUIRED_ASSET_CAN_NOT_BE_VISIBLE;
        } else if (d()) {
            aVar = am.a.INCONSISTENT_ASSET_VALUE;
        } else {
            aVar = am.a.SUCCESS;
        }
        return new ar(aVar, this.c);
    }
}
