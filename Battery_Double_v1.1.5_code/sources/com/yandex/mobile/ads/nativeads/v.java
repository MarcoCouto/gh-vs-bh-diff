package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.yandex.mobile.ads.impl.ni;
import com.yandex.mobile.ads.impl.nj;
import com.yandex.mobile.ads.impl.nl;
import com.yandex.mobile.ads.impl.nn;
import com.yandex.mobile.ads.impl.no;
import com.yandex.mobile.ads.impl.np;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public final class v {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final np f5956a;
    @NonNull
    private final i b;
    @NonNull
    private final h c;
    /* access modifiers changed from: private */
    @NonNull
    public final bd d;
    /* access modifiers changed from: private */
    @NonNull
    public final Set<NativeAdImageLoadingListener> e = new CopyOnWriteArraySet();

    public v(@NonNull Context context, @NonNull np npVar, @NonNull i iVar, @NonNull bd bdVar) {
        this.f5956a = npVar;
        this.b = iVar;
        this.d = bdVar;
        this.c = new h(context);
    }

    public final NativeAdType a() {
        return this.f5956a.b();
    }

    public final String b() {
        return this.f5956a.f();
    }

    public final void c() {
        this.c.a(this.c.a(Collections.singletonList(this.f5956a)), new j() {
            public final void a(@NonNull Map<String, Bitmap> map) {
                v.this.d.a();
                for (NativeAdImageLoadingListener nativeAdImageLoadingListener : v.this.e) {
                    if (nativeAdImageLoadingListener != null) {
                        nativeAdImageLoadingListener.onFinishLoadingImages();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(NativeAdImageLoadingListener nativeAdImageLoadingListener) {
        this.e.add(nativeAdImageLoadingListener);
    }

    /* access modifiers changed from: 0000 */
    public final void b(NativeAdImageLoadingListener nativeAdImageLoadingListener) {
        this.e.remove(nativeAdImageLoadingListener);
    }

    @Nullable
    @VisibleForTesting
    private static <T> T a(@Nullable ni<T> niVar) {
        if (niVar != null) {
            return niVar.c();
        }
        return null;
    }

    public final NativeAdAssets d() {
        k kVar = new k();
        List<ni> c2 = this.f5956a.c();
        HashMap hashMap = new HashMap();
        for (ni niVar : c2) {
            hashMap.put(niVar.a(), niVar);
        }
        no noVar = (no) a((ni) hashMap.get("media"));
        kVar.a((String) a((ni) hashMap.get(IronSourceSegment.AGE)));
        kVar.b((String) a((ni) hashMap.get(TtmlNode.TAG_BODY)));
        kVar.c((String) a((ni) hashMap.get("call_to_action")));
        kVar.a((nj) a((ni) hashMap.get("close_button")));
        kVar.d((String) a((ni) hashMap.get("domain")));
        kVar.a((nl) a((ni) hashMap.get("favicon")), this.b);
        kVar.b((nl) a((ni) hashMap.get(SettingsJsonConstants.APP_ICON_KEY)), this.b);
        nn nnVar = null;
        kVar.c(noVar != null ? noVar.b() : null, this.b);
        if (noVar != null) {
            nnVar = noVar.a();
        }
        kVar.a(nnVar);
        kVar.e((String) a((ni) hashMap.get("price")));
        kVar.f((String) a((ni) hashMap.get(CampaignEx.JSON_KEY_STAR)));
        kVar.g((String) a((ni) hashMap.get("review_count")));
        kVar.h((String) a((ni) hashMap.get("sponsored")));
        kVar.i((String) a((ni) hashMap.get("title")));
        kVar.j((String) a((ni) hashMap.get("warning")));
        return kVar;
    }
}
