package com.yandex.mobile.ads.nativeads;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.nl;
import java.util.Map;

public interface i {
    @Nullable
    Bitmap a(@NonNull nl nlVar);

    void a(@NonNull Map<String, Bitmap> map);
}
