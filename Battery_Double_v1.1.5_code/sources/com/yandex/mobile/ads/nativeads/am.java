package com.yandex.mobile.ads.nativeads;

public final class am {

    /* renamed from: a reason: collision with root package name */
    private static final Object f5890a = new Object();
    private static volatile am b;
    private boolean c = true;

    public static am a() {
        if (b == null) {
            synchronized (f5890a) {
                if (b == null) {
                    b = new am();
                }
            }
        }
        return b;
    }

    private am() {
    }

    public final boolean b() {
        return this.c;
    }
}
