package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.nq;
import com.yandex.mobile.ads.impl.x;

public final class o {

    /* renamed from: a reason: collision with root package name */
    private final x f5926a;
    private final fc b;
    private final nq c;

    public o(@NonNull nq nqVar, @NonNull x xVar, @NonNull fc fcVar) {
        this.f5926a = xVar;
        this.b = fcVar;
        this.c = nqVar;
    }

    @NonNull
    public final fc a() {
        return this.b;
    }

    @NonNull
    public final x b() {
        return this.f5926a;
    }

    @NonNull
    public final nq c() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        o oVar = (o) obj;
        if (this.f5926a == null ? oVar.f5926a != null : !this.f5926a.equals(oVar.f5926a)) {
            return false;
        }
        if (this.b == null ? oVar.b != null : !this.b.equals(oVar.b)) {
            return false;
        }
        if (this.c != null) {
            return this.c.equals(oVar.c);
        }
        return oVar.c == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((this.f5926a != null ? this.f5926a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return hashCode + i;
    }
}
