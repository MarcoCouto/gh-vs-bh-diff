package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.am;

public interface bc {
    @NonNull
    am a(@NonNull Context context, int i);

    void a(@NonNull af afVar);

    boolean a();

    @NonNull
    bi b();
}
