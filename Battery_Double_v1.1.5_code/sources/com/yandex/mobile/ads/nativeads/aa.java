package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.ni;
import com.yandex.mobile.ads.nativeads.as.a;
import com.yandex.mobile.ads.nativeads.as.b;
import java.util.List;

public final class aa extends l {
    public final /* bridge */ /* synthetic */ a a() {
        return super.a();
    }

    public final /* bridge */ /* synthetic */ void a(af afVar) {
        super.a(afVar);
    }

    @VisibleForTesting
    public final /* bridge */ /* synthetic */ boolean b() {
        return super.b();
    }

    @VisibleForTesting
    public final /* bridge */ /* synthetic */ boolean c() {
        return super.c();
    }

    public final /* bridge */ /* synthetic */ boolean d() {
        return super.d();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ bi e() {
        return super.e();
    }

    aa(@Nullable List<ni> list, @NonNull am amVar) {
        super(list, amVar);
    }

    /* access modifiers changed from: protected */
    public final boolean a(@NonNull b bVar, @Nullable List<ni> list) {
        if (!this.f5919a.b() || list == null || bVar.a(list)) {
            return true;
        }
        return false;
    }
}
