package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.dn;
import com.yandex.mobile.ads.impl.ni;
import com.yandex.mobile.ads.impl.nk;
import com.yandex.mobile.ads.impl.nl;
import com.yandex.mobile.ads.impl.np;
import com.yandex.mobile.ads.impl.od;
import com.yandex.mobile.ads.impl.od.b;
import com.yandex.mobile.ads.impl.pk;
import com.yandex.mobile.ads.impl.po;
import com.yandex.mobile.ads.impl.ps;
import com.yandex.mobile.ads.impl.re;
import com.yandex.mobile.ads.impl.rl;
import com.yandex.mobile.ads.impl.rl.c;
import com.yandex.mobile.ads.impl.rl.d;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class h {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final po f5914a = new po();
    @NonNull
    private final ps b;
    @NonNull
    private final pk c;
    @NonNull
    private final rl d;
    @NonNull
    private final b e;

    static class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final Handler f5915a = new Handler(Looper.getMainLooper());
        /* access modifiers changed from: private */
        @NonNull
        public final rl b;
        @NonNull
        private final AtomicInteger c;
        @NonNull
        private final Set<nl> d;
        @NonNull
        private final j e;
        @NonNull
        private final dn f;

        a(@NonNull rl rlVar, @NonNull Set<nl> set, @NonNull j jVar) {
            this.b = rlVar;
            this.d = set;
            this.e = jVar;
            this.c = new AtomicInteger(set.size());
            this.f = new dn();
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            HashMap hashMap = new HashMap();
            for (final nl nlVar : this.d) {
                final String c2 = nlVar.c();
                final int b2 = nlVar.b();
                final int a2 = nlVar.a();
                boolean z = true;
                new Object[1][0] = c2;
                float maxMemory = ((float) Runtime.getRuntime().maxMemory()) - ((float) Runtime.getRuntime().totalMemory());
                float b3 = ((float) (nlVar.b() * nlVar.a() * 4)) + 1048576.0f;
                if (maxMemory < b3) {
                    StringBuilder sb = new StringBuilder("Not enough free memory to create bitmap. FreeMemory = ");
                    sb.append(maxMemory);
                    sb.append(", RequiredMemory = ");
                    sb.append(b3);
                    z = false;
                }
                if (z) {
                    Handler handler = this.f5915a;
                    final HashMap hashMap2 = hashMap;
                    AnonymousClass1 r0 = new Runnable() {
                        public final void run() {
                            a.this.b.a(c2, (d) new d() {
                                public final void a(@NonNull re reVar) {
                                    new Object[1][0] = reVar;
                                    a.this.a(hashMap2);
                                }

                                public final void a(c cVar) {
                                    String c = nlVar.c();
                                    Bitmap a2 = cVar.a();
                                    if (a2 != null) {
                                        if (c != null) {
                                            hashMap2.put(c, a2);
                                        }
                                        a.this.a(hashMap2);
                                    }
                                }
                            }, a2, b2);
                        }
                    };
                    handler.post(r0);
                } else {
                    a((Map<String, Bitmap>) hashMap);
                }
            }
        }

        /* access modifiers changed from: private */
        public void a(@NonNull Map<String, Bitmap> map) {
            if (this.c.decrementAndGet() == 0) {
                this.e.a(map);
            }
        }
    }

    public h(Context context) {
        this.b = new ps(context);
        this.c = new pk();
        od a2 = od.a(context);
        this.d = a2.b();
        this.e = a2.a();
    }

    public final void a(@NonNull Set<nl> set, @NonNull j jVar) {
        if (set.size() == 0) {
            jVar.a(Collections.emptyMap());
        } else {
            new a(this.d, set, jVar).a();
        }
    }

    public final void a(@NonNull Map<String, Bitmap> map) {
        for (Entry entry : map.entrySet()) {
            String str = (String) entry.getKey();
            Bitmap bitmap = (Bitmap) entry.getValue();
            if (bitmap != null) {
                this.e.a(str, bitmap);
            }
        }
    }

    @NonNull
    public final Set<nl> a(@NonNull List<np> list) {
        HashSet hashSet = new HashSet();
        for (np npVar : list) {
            hashSet.addAll(pk.a(npVar));
            ArrayList arrayList = new ArrayList();
            ni b2 = npVar.b("feedback");
            if (b2 != null && (b2.c() instanceof nk)) {
                nl a2 = ((nk) b2.c()).a();
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
            hashSet.addAll(arrayList);
            hashSet.addAll(this.b.a(npVar));
        }
        return hashSet;
    }
}
