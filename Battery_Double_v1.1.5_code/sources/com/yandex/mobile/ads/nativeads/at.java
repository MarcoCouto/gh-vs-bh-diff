package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.nj;

public final class at {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f5893a;
    @NonNull
    private final a b;

    public enum a {
        TEXT,
        IMAGE
    }

    at(@NonNull nj njVar) {
        this.f5893a = njVar.a();
        this.b = njVar.b();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        at atVar = (at) obj;
        if (this.f5893a == null ? atVar.f5893a == null : this.f5893a.equals(atVar.f5893a)) {
            return this.b == atVar.b;
        }
        return false;
    }

    public final int hashCode() {
        return ((this.f5893a != null ? this.f5893a.hashCode() : 0) * 31) + this.b.hashCode();
    }
}
