package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ae;
import com.yandex.mobile.ads.impl.fc;
import com.yandex.mobile.ads.impl.gu.a;
import java.util.ArrayList;
import java.util.List;

final class ab extends ay implements z {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final i f5880a;
    @NonNull
    private final List<? extends NativeGenericAd> b;
    @NonNull
    private final bf c;

    ab(@NonNull Context context, @NonNull List<? extends NativeGenericAd> list, @NonNull i iVar, @NonNull c cVar) {
        super(context, cVar);
        this.b = list;
        this.f5880a = iVar;
        this.c = cVar.c();
        o a2 = cVar.a();
        List c2 = a2.c().c();
        fc a3 = a2.a();
        String a4 = ae.AD_UNIT.a();
        an anVar = new an(c2, a3);
        anVar.a(a.CUSTOM);
        anVar.a(a4);
        a((a) anVar);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("sponsored");
        return arrayList;
    }
}
