package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.nl;
import com.yandex.mobile.ads.impl.np;
import com.yandex.mobile.ads.impl.pk;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public final class p {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final pk f5927a = new pk();

    @NonNull
    static List<String> a(@NonNull List<np> list) {
        HashSet hashSet = new HashSet();
        for (np a2 : list) {
            hashSet.addAll(a(a2));
        }
        return new ArrayList(hashSet);
    }

    @NonNull
    public static List<String> a(@NonNull np npVar) {
        HashSet hashSet = new HashSet();
        for (nl nlVar : pk.a(npVar)) {
            if (!TextUtils.isEmpty(nlVar.d())) {
                hashSet.add(nlVar.d());
            }
        }
        return new ArrayList(hashSet);
    }
}
