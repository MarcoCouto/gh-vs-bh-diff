package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.aa;
import com.yandex.mobile.ads.impl.ae;
import com.yandex.mobile.ads.impl.af;
import com.yandex.mobile.ads.impl.bp;
import com.yandex.mobile.ads.impl.cq;
import com.yandex.mobile.ads.impl.kf;
import com.yandex.mobile.ads.impl.mb;
import com.yandex.mobile.ads.impl.mc;
import com.yandex.mobile.ads.impl.nq;
import com.yandex.mobile.ads.impl.of;
import com.yandex.mobile.ads.impl.pj;
import com.yandex.mobile.ads.impl.pu;
import com.yandex.mobile.ads.impl.v;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.nativeads.NativeAdLoader.OnLoadListener;

public final class u extends aa<nq> {
    @NonNull
    private final mb g = new a();
    @NonNull
    private final mc h;
    @NonNull
    private final kf i;
    @NonNull
    private final pu j;
    /* access modifiers changed from: private */
    @NonNull
    public final w k;
    @Nullable
    private cq<nq> l;

    @VisibleForTesting
    class a implements mb {
        a() {
        }

        public final void a(@NonNull AdRequestError adRequestError) {
            u.this.onAdFailedToLoad(adRequestError);
        }

        public final void a(@NonNull NativeGenericAd nativeGenericAd) {
            u.this.t();
            u.this.k.a(nativeGenericAd);
        }

        public final void a(@NonNull z zVar) {
            u.this.t();
            u.this.k.a(zVar);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean d(@Nullable AdRequest adRequest) {
        return true;
    }

    public u(@NonNull Context context, @NonNull NativeAdLoaderConfiguration nativeAdLoaderConfiguration, @NonNull w wVar) {
        super(context, b.NATIVE);
        this.k = wVar;
        a_(nativeAdLoaderConfiguration.getBlockId());
        this.e.a(nativeAdLoaderConfiguration.getImageSizes());
        this.e.b(nativeAdLoaderConfiguration.shouldLoadImagesAutomatically());
        this.e.b(pj.a(context).a());
        this.h = new mc(context, s(), nativeAdLoaderConfiguration);
        this.i = new kf();
        this.j = new pu();
        this.k.a((com.yandex.mobile.ads.impl.gu.a) this.j);
    }

    public final void a(@NonNull x<nq> xVar) {
        this.j.a(xVar);
        if (!l()) {
            kf.a(xVar).a(this).a(this.b, xVar);
        }
    }

    public final void a(@NonNull x<nq> xVar, @NonNull s sVar) {
        if (!l()) {
            this.h.a(this.b, xVar, sVar, this.g);
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final bp<nq> a(String str, String str2) {
        of ofVar = new of(this.b, this.l, this.e, str, str2, this);
        return ofVar;
    }

    public final void a(@Nullable AdRequest adRequest, @NonNull cq<nq> cqVar, @NonNull ae aeVar, @NonNull af afVar) {
        this.l = cqVar;
        if (cqVar.a()) {
            this.e.a(aeVar);
            this.e.a(afVar);
            a(adRequest);
            return;
        }
        onAdFailedToLoad(v.j);
    }

    public final synchronized void a(@Nullable AdRequest adRequest) {
        b(adRequest);
    }

    /* access modifiers changed from: protected */
    public final void a(@NonNull AdRequestError adRequestError) {
        this.k.a(adRequestError);
    }

    /* access modifiers changed from: protected */
    public final boolean n() {
        return o() && q();
    }

    public final void a(@Nullable OnLoadListener onLoadListener) {
        this.k.a(onLoadListener);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        super.w();
        this.c.a();
        this.k.a();
        a(com.yandex.mobile.ads.impl.u.CANCELLED);
    }
}
