package com.yandex.mobile.ads.nativeads;

import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.nj;

public final class k extends NativeAdAssets {

    /* renamed from: a reason: collision with root package name */
    private at f5918a;

    k() {
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        k kVar = (k) obj;
        if (this.f5918a != null) {
            return this.f5918a.equals(kVar.f5918a);
        }
        return kVar.f5918a == null;
    }

    public final int hashCode() {
        return (super.hashCode() * 31) + (this.f5918a != null ? this.f5918a.hashCode() : 0);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable nj njVar) {
        this.f5918a = njVar != null ? new at(njVar) : null;
    }
}
