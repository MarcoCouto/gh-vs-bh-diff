package com.yandex.mobile.ads;

public interface InterstitialEventListener {

    public static class SimpleInterstitialEventListener implements InterstitialEventListener {
        public void onAdClosed() {
        }

        public void onAdLeftApplication() {
        }

        public void onAdOpened() {
        }

        public void onInterstitialDismissed() {
        }

        public void onInterstitialFailedToLoad(AdRequestError adRequestError) {
        }

        public void onInterstitialLoaded() {
        }

        public void onInterstitialShown() {
        }
    }

    void onAdClosed();

    void onAdLeftApplication();

    void onAdOpened();

    void onInterstitialDismissed();

    void onInterstitialFailedToLoad(AdRequestError adRequestError);

    void onInterstitialLoaded();

    void onInterstitialShown();
}
