package com.amazon.device.ads;

import android.annotation.SuppressLint;
import com.amazon.device.ads.AdError.ErrorCode;
import com.amazon.device.ads.ThreadUtils.ExecutionStyle;
import com.amazon.device.ads.ThreadUtils.ExecutionThread;
import com.amazon.device.ads.ThreadUtils.ThreadRunner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

class AdLoadStarter {
    private static final String LOGTAG = "AdLoadStarter";
    private final AdLoaderFactory adLoaderFactory;
    private final AdRequestBuilder adRequestBuilder;
    private final AdvertisingIdentifier advertisingIdentifier;
    private final Configuration configuration;
    /* access modifiers changed from: private */
    public final MobileAdsInfoStore infoStore;
    private final MobileAdsLogger logger;
    private final PermissionChecker permissionChecker;
    private final Settings settings;
    private final SystemTime systemTime;
    /* access modifiers changed from: private */
    public final ThreadRunner threadRunner;
    private final ViewabilityJavascriptFetcherListener viewabilityJavascriptFetcherListener;

    public AdLoadStarter() {
        this(new AdLoaderFactory(), new AdvertisingIdentifier(), ThreadUtils.getThreadRunner(), MobileAdsInfoStore.getInstance(), Settings.getInstance(), Configuration.getInstance(), new MobileAdsLoggerFactory(), new SystemTime(), new AdRequestBuilder(), new PermissionChecker(), new ViewabilityJavascriptFetcherListener());
    }

    AdLoadStarter(AdLoaderFactory adLoaderFactory2, AdvertisingIdentifier advertisingIdentifier2, ThreadRunner threadRunner2, MobileAdsInfoStore mobileAdsInfoStore, Settings settings2, Configuration configuration2, MobileAdsLoggerFactory mobileAdsLoggerFactory, SystemTime systemTime2, AdRequestBuilder adRequestBuilder2, PermissionChecker permissionChecker2, ViewabilityJavascriptFetcherListener viewabilityJavascriptFetcherListener2) {
        this.adLoaderFactory = adLoaderFactory2;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        this.advertisingIdentifier = advertisingIdentifier2;
        this.infoStore = mobileAdsInfoStore;
        this.settings = settings2;
        this.configuration = configuration2;
        this.threadRunner = threadRunner2;
        this.systemTime = systemTime2;
        this.adRequestBuilder = adRequestBuilder2;
        this.permissionChecker = permissionChecker2;
        this.viewabilityJavascriptFetcherListener = viewabilityJavascriptFetcherListener2;
    }

    public void loadAds(int i, AdTargetingOptions adTargetingOptions, AdSlot... adSlotArr) {
        if (!isNoRetry(adSlotArr)) {
            if (adTargetingOptions != null && adTargetingOptions.isGeoLocationEnabled() && !this.permissionChecker.hasLocationPermission(this.infoStore.getApplicationContext())) {
                this.logger.w("Geolocation for ad targeting has been disabled. To enable geolocation, add at least one of the following permissions to the app manifest: 1. ACCESS_FINE_LOCATION; 2. ACCESS_COARSE_LOCATION.");
            }
            long nanoTime = this.systemTime.nanoTime();
            final ArrayList arrayList = new ArrayList();
            for (AdSlot adSlot : adSlotArr) {
                if (adSlot.prepareForAdLoad(nanoTime)) {
                    arrayList.add(adSlot);
                }
            }
            this.configuration.queueConfigurationListener(this.viewabilityJavascriptFetcherListener);
            final int i2 = i;
            final AdTargetingOptions adTargetingOptions2 = adTargetingOptions;
            AnonymousClass1 r2 = new StartUpWaiter(this.settings, this.configuration) {
                /* access modifiers changed from: protected */
                public void startUpReady() {
                    AdLoadStarter.this.infoStore.register();
                    AdLoadStarter.this.beginFetchAds(i2, adTargetingOptions2, arrayList);
                }

                /* access modifiers changed from: protected */
                public void startUpFailed() {
                    AdLoadStarter.this.threadRunner.execute(new Runnable() {
                        public void run() {
                            AdLoadStarter.this.failAds(new AdError(ErrorCode.NETWORK_ERROR, "The configuration was unable to be loaded"), arrayList);
                        }
                    }, ExecutionStyle.RUN_ASAP, ExecutionThread.MAIN_THREAD);
                }
            };
            r2.start();
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"UseSparseArrays"})
    public void beginFetchAds(int i, AdTargetingOptions adTargetingOptions, List<AdSlot> list) {
        Info advertisingIdentifierInfo = this.advertisingIdentifier.getAdvertisingIdentifierInfo();
        if (!advertisingIdentifierInfo.canDo()) {
            failAds(new AdError(ErrorCode.INTERNAL_ERROR, "An internal request was not made on a background thread."), list);
            return;
        }
        if (adTargetingOptions == null) {
            adTargetingOptions = new AdTargetingOptions();
        }
        AdRequest build = this.adRequestBuilder.withAdTargetingOptions(adTargetingOptions).withAdvertisingIdentifierInfo(advertisingIdentifierInfo).build();
        HashMap hashMap = new HashMap();
        int i2 = 1;
        for (AdSlot adSlot : list) {
            if (adSlot.isValid()) {
                adSlot.setSlotNumber(i2);
                hashMap.put(Integer.valueOf(i2), adSlot);
                build.putSlot(adSlot);
                i2++;
            }
        }
        if (hashMap.size() > 0) {
            AdLoader createAdLoader = this.adLoaderFactory.createAdLoader(build, hashMap);
            createAdLoader.setTimeout(i);
            createAdLoader.beginFetchAd();
        }
    }

    /* access modifiers changed from: private */
    public void failAds(AdError adError, List<AdSlot> list) {
        int i = 0;
        for (AdSlot adSlot : list) {
            if (adSlot.getSlotNumber() != -1) {
                adSlot.adFailed(adError);
                i++;
            }
        }
        if (i > 0) {
            this.logger.e("%s; code: %s", adError.getMessage(), adError.getCode());
        }
    }

    private boolean isNoRetry(AdSlot[] adSlotArr) {
        ErrorCode errorCode;
        String str;
        int noRetryTtlRemainingMillis = this.infoStore.getNoRetryTtlRemainingMillis();
        if (noRetryTtlRemainingMillis <= 0) {
            return false;
        }
        int i = noRetryTtlRemainingMillis / 1000;
        String str2 = "SDK Message: ";
        if (this.infoStore.getIsAppDisabled()) {
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(AdLoader.DISABLED_APP_SERVER_MESSAGE);
            str = sb.toString();
            errorCode = ErrorCode.INTERNAL_ERROR;
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append("no results. Try again in ");
            sb2.append(i);
            sb2.append(" seconds.");
            str = sb2.toString();
            errorCode = ErrorCode.NO_FILL;
        }
        failAds(new AdError(errorCode, str), new ArrayList(Arrays.asList(adSlotArr)));
        return true;
    }
}
