package com.amazon.device.ads;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import com.amazon.device.ads.Configuration.ConfigOption;

class AdLocation {
    private static final int ARCMINUTE_PRECISION = 6;
    private static final String LOGTAG = "AdLocation";
    private static final float MAX_DISTANCE_IN_KILOMETERS = 3.0f;
    private final Configuration configuration;
    private final Context context;
    private final MobileAdsLogger logger;

    private enum LocationAwareness {
        LOCATION_AWARENESS_NORMAL,
        LOCATION_AWARENESS_TRUNCATED,
        LOCATION_AWARENESS_DISABLED
    }

    public AdLocation(Context context2) {
        this(context2, Configuration.getInstance());
    }

    AdLocation(Context context2, Configuration configuration2) {
        this.logger = new MobileAdsLoggerFactory().createMobileAdsLogger(LOGTAG);
        this.context = context2;
        this.configuration = configuration2;
    }

    private static double roundToArcminutes(double d) {
        double round = (double) Math.round(d * 60.0d);
        Double.isNaN(round);
        return round / 60.0d;
    }

    private LocationAwareness getLocationAwareness() {
        if (this.configuration.getBoolean(ConfigOption.TRUNCATE_LAT_LON)) {
            return LocationAwareness.LOCATION_AWARENESS_TRUNCATED;
        }
        return LocationAwareness.LOCATION_AWARENESS_NORMAL;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0048 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004d A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00c2  */
    public Location getLocation() {
        Location location;
        Location location2;
        LocationAwareness locationAwareness = getLocationAwareness();
        if (LocationAwareness.LOCATION_AWARENESS_DISABLED.equals(locationAwareness)) {
            return null;
        }
        LocationManager locationManager = (LocationManager) this.context.getSystemService("location");
        try {
            location = locationManager.getLastKnownLocation("gps");
        } catch (SecurityException unused) {
            this.logger.d("Failed to retrieve GPS location: No permissions to access GPS");
            location = null;
            location2 = locationManager.getLastKnownLocation("network");
            if (location != null) {
            }
            if (location != null) {
            }
            if (location != null) {
            }
        } catch (IllegalArgumentException unused2) {
            this.logger.d("Failed to retrieve GPS location: No GPS found");
            location = null;
            location2 = locationManager.getLastKnownLocation("network");
            if (location != null) {
            }
            if (location != null) {
            }
            if (location != null) {
            }
        }
        try {
            location2 = locationManager.getLastKnownLocation("network");
        } catch (SecurityException unused3) {
            this.logger.d("Failed to retrieve network location: No permissions to access network location");
            location2 = null;
            if (location != null) {
            }
            if (location != null) {
            }
            if (location != null) {
            }
        } catch (IllegalArgumentException unused4) {
            this.logger.d("Failed to retrieve network location: No network provider found");
            location2 = null;
            if (location != null) {
            }
            if (location != null) {
            }
            if (location != null) {
            }
        }
        if (location != null && location2 == null) {
            return null;
        }
        if (location != null || location2 == null) {
            if (location != null) {
                this.logger.d("Setting lat/long using GPS, not network");
                if (LocationAwareness.LOCATION_AWARENESS_TRUNCATED.equals(locationAwareness)) {
                    double round = (double) Math.round(Math.pow(10.0d, 6.0d) * roundToArcminutes(location.getLatitude()));
                    double pow = Math.pow(10.0d, 6.0d);
                    Double.isNaN(round);
                    location.setLatitude(round / pow);
                    double round2 = (double) Math.round(Math.pow(10.0d, 6.0d) * roundToArcminutes(location.getLongitude()));
                    double pow2 = Math.pow(10.0d, 6.0d);
                    Double.isNaN(round2);
                    location.setLongitude(round2 / pow2);
                }
                return location;
            }
            this.logger.d("Setting lat/long using network location, not GPS");
        } else if (location.distanceTo(location2) / 1000.0f <= MAX_DISTANCE_IN_KILOMETERS) {
            float f = Float.MAX_VALUE;
            float accuracy = location.hasAccuracy() ? location.getAccuracy() : Float.MAX_VALUE;
            if (location2.hasAccuracy()) {
                f = location2.getAccuracy();
            }
            if (accuracy < f) {
                this.logger.d("Setting lat/long using GPS determined by distance");
                location2 = location;
            } else {
                this.logger.d("Setting lat/long using network determined by distance");
            }
        } else if (location.getTime() > location2.getTime()) {
            this.logger.d("Setting lat/long using GPS");
            if (LocationAwareness.LOCATION_AWARENESS_TRUNCATED.equals(locationAwareness)) {
            }
            return location;
        } else {
            this.logger.d("Setting lat/long using network");
        }
        location = location2;
        if (LocationAwareness.LOCATION_AWARENESS_TRUNCATED.equals(locationAwareness)) {
        }
        return location;
    }
}
