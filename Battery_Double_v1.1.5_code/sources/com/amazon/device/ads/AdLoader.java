package com.amazon.device.ads;

import com.amazon.device.ads.AdError.ErrorCode;
import com.amazon.device.ads.ThreadUtils.ExecutionStyle;
import com.amazon.device.ads.ThreadUtils.ExecutionThread;
import com.amazon.device.ads.ThreadUtils.ThreadRunner;
import com.amazon.device.ads.WebRequest.WebRequestException;
import com.amazon.device.ads.WebRequest.WebRequestStatus;
import com.amazon.device.ads.WebRequest.WebResponse;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.smaato.sdk.core.api.VideoType;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

class AdLoader {
    public static final int AD_FAILED = -1;
    public static final int AD_LOAD_DEFERRED = 1;
    public static final int AD_READY_TO_LOAD = 0;
    public static final String DISABLED_APP_SERVER_MESSAGE = "DISABLED_APP";
    private static final String LOGTAG = "AdLoader";
    private final AdRequest adRequest;
    private final Assets assets;
    private CompositeMetricsCollector compositeMetricsCollector;
    private final DebugProperties debugProperties;
    private AdError error;
    private final MobileAdsInfoStore infoStore;
    private final MobileAdsLogger logger;
    private final Map<Integer, AdSlot> slots;
    private final SystemTime systemTime;
    private final ThreadRunner threadRunner;
    private int timeout;

    protected class AdFetchException extends Exception {
        private static final long serialVersionUID = 1;
        private final AdError adError;

        public AdFetchException(AdError adError2) {
            this.adError = adError2;
        }

        public AdFetchException(AdError adError2, Throwable th) {
            super(th);
            this.adError = adError2;
        }

        public AdError getAdError() {
            return this.adError;
        }
    }

    protected static class AdLoaderFactory {
        protected AdLoaderFactory() {
        }

        public AdLoader createAdLoader(AdRequest adRequest, Map<Integer, AdSlot> map) {
            return new AdLoader(adRequest, map);
        }
    }

    public AdLoader(AdRequest adRequest2, Map<Integer, AdSlot> map) {
        this(adRequest2, map, ThreadUtils.getThreadRunner(), new SystemTime(), Assets.getInstance(), MobileAdsInfoStore.getInstance(), new MobileAdsLoggerFactory(), DebugProperties.getInstance());
    }

    AdLoader(AdRequest adRequest2, Map<Integer, AdSlot> map, ThreadRunner threadRunner2, SystemTime systemTime2, Assets assets2, MobileAdsInfoStore mobileAdsInfoStore, MobileAdsLoggerFactory mobileAdsLoggerFactory, DebugProperties debugProperties2) {
        this.timeout = 20000;
        this.error = null;
        this.compositeMetricsCollector = null;
        this.adRequest = adRequest2;
        this.slots = map;
        this.threadRunner = threadRunner2;
        this.systemTime = systemTime2;
        this.assets = assets2;
        this.infoStore = mobileAdsInfoStore;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        this.debugProperties = debugProperties2;
    }

    public void setTimeout(int i) {
        this.timeout = i;
    }

    public void beginFetchAd() {
        getCompositeMetricsCollector().stopMetric(MetricType.AD_LOAD_LATENCY_LOADAD_TO_FETCH_THREAD_REQUEST_START);
        getCompositeMetricsCollector().startMetric(MetricType.AD_LOAD_LATENCY_FETCH_THREAD_SPIN_UP);
        startFetchAdThread();
    }

    /* access modifiers changed from: protected */
    public void startFetchAdThread() {
        this.threadRunner.execute(new Runnable() {
            public void run() {
                AdLoader.this.fetchAd();
                AdLoader.this.beginFinalizeFetchAd();
            }
        }, ExecutionStyle.SCHEDULE, ExecutionThread.BACKGROUND_THREAD);
    }

    /* access modifiers changed from: private */
    public void beginFinalizeFetchAd() {
        this.threadRunner.execute(new Runnable() {
            public void run() {
                AdLoader.this.finalizeFetchAd();
            }
        }, ExecutionStyle.SCHEDULE, ExecutionThread.MAIN_THREAD);
    }

    /* access modifiers changed from: protected */
    public void fetchAd() {
        getCompositeMetricsCollector().stopMetric(MetricType.AD_LOAD_LATENCY_FETCH_THREAD_SPIN_UP);
        getCompositeMetricsCollector().startMetric(MetricType.AD_LOAD_LATENCY_FETCH_THREAD_START_TO_AAX_GET_AD_START);
        if (!this.assets.ensureAssetsCreated()) {
            this.error = new AdError(ErrorCode.REQUEST_ERROR, "Unable to create the assets needed to display ads");
            this.logger.e("Unable to create the assets needed to display ads");
            setErrorForAllSlots(this.error);
            return;
        }
        try {
            WebResponse fetchResponseFromNetwork = fetchResponseFromNetwork();
            if (!fetchResponseFromNetwork.isHttpStatusCodeOK()) {
                StringBuilder sb = new StringBuilder();
                sb.append(fetchResponseFromNetwork.getHttpStatusCode());
                sb.append(" - ");
                sb.append(fetchResponseFromNetwork.getHttpStatus());
                String sb2 = sb.toString();
                this.error = new AdError(ErrorCode.NETWORK_ERROR, sb2);
                this.logger.e(sb2);
                setErrorForAllSlots(this.error);
                return;
            }
            JSONObject readAsJSON = fetchResponseFromNetwork.getResponseReader().readAsJSON();
            if (readAsJSON == null) {
                this.error = new AdError(ErrorCode.INTERNAL_ERROR, "Unable to parse response");
                this.logger.e("Unable to parse response");
                setErrorForAllSlots(this.error);
                return;
            }
            parseResponse(readAsJSON);
            getCompositeMetricsCollector().stopMetric(MetricType.AD_LOAD_LATENCY_AAX_GET_AD_END_TO_FETCH_THREAD_END);
            getCompositeMetricsCollector().startMetric(MetricType.AD_LOAD_LATENCY_FINALIZE_FETCH_SPIN_UP);
        } catch (AdFetchException e) {
            this.error = e.getAdError();
            this.logger.e(e.getAdError().getMessage());
            setErrorForAllSlots(this.error);
        }
    }

    private WebRequest getAdRequest() throws AdFetchException {
        getCompositeMetricsCollector().startMetric(MetricType.AD_LOAD_LATENCY_CREATE_AAX_GET_AD_URL);
        WebRequest webRequest = this.adRequest.getWebRequest();
        getCompositeMetricsCollector().stopMetric(MetricType.AD_LOAD_LATENCY_CREATE_AAX_GET_AD_URL);
        return webRequest;
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x0177  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0199  */
    private void parseResponse(JSONObject jSONObject) {
        HashSet hashSet;
        AdError adError;
        String str;
        JSONArray jSONArray;
        int i;
        int i2;
        long longFromJSON;
        boolean z;
        HashSet hashSet2;
        AdError adError2;
        String str2;
        JSONObject jSONObject2 = jSONObject;
        long currentTimeMillis = this.systemTime.currentTimeMillis();
        String str3 = null;
        String stringFromJSON = JSONUtils.getStringFromJSON(jSONObject2, "status", null);
        HashSet hashSet3 = new HashSet(this.slots.keySet());
        AdError adError3 = getAdError(jSONObject);
        String stringFromJSON2 = JSONUtils.getStringFromJSON(jSONObject2, IronSourceConstants.EVENTS_ERROR_CODE, "No Ad Received");
        this.adRequest.setInstrumentationPixelURL(JSONUtils.getStringFromJSON(jSONObject2, "instrPixelURL", null));
        int i3 = 0;
        if (stringFromJSON != null && stringFromJSON.equals("ok")) {
            JSONArray jSONArrayFromJSON = JSONUtils.getJSONArrayFromJSON(jSONObject2, CampaignUnit.JSON_KEY_ADS);
            int i4 = 0;
            while (i4 < jSONArrayFromJSON.length()) {
                JSONObject jSONObjectFromJSONArray = JSONUtils.getJSONObjectFromJSONArray(jSONArrayFromJSON, i4);
                if (jSONObjectFromJSONArray != null) {
                    int integerFromJSON = JSONUtils.getIntegerFromJSON(jSONObjectFromJSONArray, "slotId", -1);
                    AdSlot adSlot = (AdSlot) this.slots.get(Integer.valueOf(integerFromJSON));
                    if (adSlot != null) {
                        hashSet3.remove(Integer.valueOf(integerFromJSON));
                        String stringFromJSON3 = JSONUtils.getStringFromJSON(jSONObjectFromJSONArray, "instrPixelURL", this.adRequest.getInstrumentationPixelURL());
                        AdData adData = new AdData();
                        adData.setInstrumentationPixelUrl(stringFromJSON3);
                        adData.setImpressionPixelUrl(JSONUtils.getStringFromJSON(jSONObjectFromJSONArray, "impPixelURL", str3));
                        if (adSlot.getRequestedAdSize().isAuto()) {
                            adSlot.getMetricsCollector().incrementMetric(MetricType.AD_COUNTER_AUTO_AD_SIZE);
                        }
                        String stringFromJSON4 = JSONUtils.getStringFromJSON(jSONObjectFromJSONArray, String.HTML, "");
                        JSONArray jSONArrayFromJSON2 = JSONUtils.getJSONArrayFromJSON(jSONObjectFromJSONArray, "creativeTypes");
                        HashSet hashSet4 = new HashSet();
                        if (jSONArrayFromJSON2 != null) {
                            jSONArray = jSONArrayFromJSON;
                            int i5 = 0;
                            while (i5 < jSONArrayFromJSON2.length()) {
                                int integerFromJSONArray = JSONUtils.getIntegerFromJSONArray(jSONArrayFromJSON2, i5, i3);
                                AAXCreative creativeType = AAXCreative.getCreativeType(integerFromJSONArray);
                                if (creativeType != null) {
                                    hashSet4.add(creativeType);
                                    hashSet2 = hashSet3;
                                    adError2 = adError3;
                                    str2 = stringFromJSON2;
                                } else {
                                    str2 = stringFromJSON2;
                                    hashSet2 = hashSet3;
                                    adError2 = adError3;
                                    this.logger.w("%d is not a recognized creative type.", Integer.valueOf(integerFromJSONArray));
                                }
                                i5++;
                                stringFromJSON2 = str2;
                                adError3 = adError2;
                                hashSet3 = hashSet2;
                                i3 = 0;
                            }
                        } else {
                            jSONArray = jSONArrayFromJSON;
                        }
                        hashSet = hashSet3;
                        adError = adError3;
                        str = stringFromJSON2;
                        if (!AAXCreative.containsPrimaryCreativeType(hashSet4)) {
                            adSlot.setAdError(new AdError(ErrorCode.INTERNAL_ERROR, "No valid creative types found"));
                            this.logger.e("No valid creative types found");
                        } else {
                            String stringFromJSON5 = JSONUtils.getStringFromJSON(jSONObjectFromJSONArray, "size", "");
                            if (stringFromJSON5 != null && ((stringFromJSON5.equals("9999x9999") || stringFromJSON5.equals(VideoType.INTERSTITIAL)) && !hashSet4.contains(AAXCreative.INTERSTITIAL))) {
                                hashSet4.add(AAXCreative.INTERSTITIAL);
                            }
                            if (!hashSet4.contains(AAXCreative.INTERSTITIAL)) {
                                String[] split = stringFromJSON5 != null ? stringFromJSON5.split(AvidJSONUtil.KEY_X) : null;
                                if (split == null || split.length != 2) {
                                    z = true;
                                    i2 = 0;
                                    i = 0;
                                } else {
                                    try {
                                        i = Integer.parseInt(split[0]);
                                        try {
                                            i2 = Integer.parseInt(split[1]);
                                            z = false;
                                        } catch (NumberFormatException unused) {
                                            z = true;
                                            i2 = 0;
                                            if (z) {
                                            }
                                            longFromJSON = JSONUtils.getLongFromJSON(jSONObjectFromJSONArray, "cacheTTL", -1);
                                            if (longFromJSON > -1) {
                                            }
                                            AdProperties adProperties = new AdProperties(jSONArrayFromJSON2);
                                            adData.setHeight(i2);
                                            adData.setWidth(i);
                                            adData.setCreative(stringFromJSON4);
                                            adData.setCreativeTypes(hashSet4);
                                            adData.setProperties(adProperties);
                                            adData.setFetched(true);
                                            adSlot.setAdData(adData);
                                            i4++;
                                            jSONArrayFromJSON = jSONArray;
                                            stringFromJSON2 = str;
                                            adError3 = adError;
                                            hashSet3 = hashSet;
                                            str3 = null;
                                            i3 = 0;
                                        }
                                    } catch (NumberFormatException unused2) {
                                        i = 0;
                                        z = true;
                                        i2 = 0;
                                        if (z) {
                                        }
                                        longFromJSON = JSONUtils.getLongFromJSON(jSONObjectFromJSONArray, "cacheTTL", -1);
                                        if (longFromJSON > -1) {
                                        }
                                        AdProperties adProperties2 = new AdProperties(jSONArrayFromJSON2);
                                        adData.setHeight(i2);
                                        adData.setWidth(i);
                                        adData.setCreative(stringFromJSON4);
                                        adData.setCreativeTypes(hashSet4);
                                        adData.setProperties(adProperties2);
                                        adData.setFetched(true);
                                        adSlot.setAdData(adData);
                                        i4++;
                                        jSONArrayFromJSON = jSONArray;
                                        stringFromJSON2 = str;
                                        adError3 = adError;
                                        hashSet3 = hashSet;
                                        str3 = null;
                                        i3 = 0;
                                    }
                                }
                                if (z) {
                                    adSlot.setAdError(new AdError(ErrorCode.INTERNAL_ERROR, "Server returned an invalid ad size"));
                                    this.logger.e("Server returned an invalid ad size");
                                }
                            } else {
                                i2 = 0;
                                i = 0;
                            }
                            longFromJSON = JSONUtils.getLongFromJSON(jSONObjectFromJSONArray, "cacheTTL", -1);
                            if (longFromJSON > -1) {
                                adData.setExpirationTimeMillis(currentTimeMillis + (longFromJSON * 1000));
                            }
                            AdProperties adProperties22 = new AdProperties(jSONArrayFromJSON2);
                            adData.setHeight(i2);
                            adData.setWidth(i);
                            adData.setCreative(stringFromJSON4);
                            adData.setCreativeTypes(hashSet4);
                            adData.setProperties(adProperties22);
                            adData.setFetched(true);
                            adSlot.setAdData(adData);
                        }
                        i4++;
                        jSONArrayFromJSON = jSONArray;
                        stringFromJSON2 = str;
                        adError3 = adError;
                        hashSet3 = hashSet;
                        str3 = null;
                        i3 = 0;
                    }
                }
                jSONArray = jSONArrayFromJSON;
                hashSet = hashSet3;
                adError = adError3;
                str = stringFromJSON2;
                i4++;
                jSONArrayFromJSON = jSONArray;
                stringFromJSON2 = str;
                adError3 = adError;
                hashSet3 = hashSet;
                str3 = null;
                i3 = 0;
            }
        }
        AdError adError4 = adError3;
        String str4 = stringFromJSON2;
        Iterator it = hashSet3.iterator();
        while (it.hasNext()) {
            Integer num = (Integer) it.next();
            AdError adError5 = adError4;
            ((AdSlot) this.slots.get(num)).setAdError(adError5);
            AdData adData2 = new AdData();
            adData2.setInstrumentationPixelUrl(this.adRequest.getInstrumentationPixelURL());
            ((AdSlot) this.slots.get(num)).setAdData(adData2);
            this.logger.w("%s; code: %s", adError5.getMessage(), str4);
        }
    }

    /* access modifiers changed from: protected */
    public AdError getAdError(JSONObject jSONObject) {
        int retrieveNoRetryTtlSeconds = retrieveNoRetryTtlSeconds(jSONObject);
        this.infoStore.setNoRetryTtl(retrieveNoRetryTtlSeconds);
        String stringFromJSON = JSONUtils.getStringFromJSON(jSONObject, "errorMessage", "No Ad Received");
        this.infoStore.setIsAppDisabled(stringFromJSON.equalsIgnoreCase(DISABLED_APP_SERVER_MESSAGE));
        StringBuilder sb = new StringBuilder();
        sb.append("Server Message: ");
        sb.append(stringFromJSON);
        String sb2 = sb.toString();
        if (retrieveNoRetryTtlSeconds > 0) {
            getCompositeMetricsCollector().publishMetricInMilliseconds(MetricType.AD_NO_RETRY_TTL_RECEIVED, (long) (retrieveNoRetryTtlSeconds * 1000));
        }
        if (retrieveNoRetryTtlSeconds > 0 && !this.infoStore.getIsAppDisabled()) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb2);
            sb3.append(". Try again in ");
            sb3.append(retrieveNoRetryTtlSeconds);
            sb3.append(" seconds");
            return new AdError(ErrorCode.NO_FILL, sb3.toString());
        } else if (stringFromJSON.equals("no results")) {
            return new AdError(ErrorCode.NO_FILL, sb2);
        } else {
            return new AdError(ErrorCode.INTERNAL_ERROR, sb2);
        }
    }

    private void setErrorForAllSlots(AdError adError) {
        for (AdSlot adError2 : this.slots.values()) {
            adError2.setAdError(adError);
        }
    }

    /* access modifiers changed from: protected */
    public int retrieveNoRetryTtlSeconds(JSONObject jSONObject) {
        return this.debugProperties.getDebugPropertyAsInteger(DebugProperties.DEBUG_NORETRYTTL, Integer.valueOf(JSONUtils.getIntegerFromJSON(jSONObject, "noretryTTL", 0))).intValue();
    }

    /* access modifiers changed from: protected */
    public void finalizeFetchAd() {
        for (Entry value : this.slots.entrySet()) {
            AdSlot adSlot = (AdSlot) value.getValue();
            if (!adSlot.canBeUsed()) {
                this.logger.w("Ad object was destroyed before ad fetching could be finalized. Ad fetching has been aborted.");
            } else {
                adSlot.getMetricsCollector().stopMetric(MetricType.AD_LOAD_LATENCY_FINALIZE_FETCH_SPIN_UP);
                if (!adSlot.isFetched()) {
                    adSlot.getMetricsCollector().startMetric(MetricType.AD_LOAD_LATENCY_FINALIZE_FETCH_START_TO_FAILURE);
                    if (adSlot.getAdError() != null) {
                        adSlot.adFailed(adSlot.getAdError());
                    } else {
                        adSlot.adFailed(new AdError(ErrorCode.INTERNAL_ERROR, "Unknown error occurred."));
                    }
                } else {
                    adSlot.getMetricsCollector().startMetric(MetricType.AD_LOAD_LATENCY_FINALIZE_FETCH_START_TO_RENDER_START);
                    adSlot.initializeAd();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public WebResponse fetchResponseFromNetwork() throws AdFetchException {
        WebRequest adRequest2 = getAdRequest();
        adRequest2.setMetricsCollector(getCompositeMetricsCollector());
        adRequest2.setServiceCallLatencyMetric(MetricType.AAX_LATENCY_GET_AD);
        adRequest2.setTimeout(this.timeout);
        adRequest2.setDisconnectEnabled(false);
        getCompositeMetricsCollector().stopMetric(MetricType.AD_LOAD_LATENCY_FETCH_THREAD_START_TO_AAX_GET_AD_START);
        getCompositeMetricsCollector().incrementMetric(MetricType.TLS_ENABLED);
        try {
            WebResponse makeCall = adRequest2.makeCall();
            getCompositeMetricsCollector().startMetric(MetricType.AD_LOAD_LATENCY_AAX_GET_AD_END_TO_FETCH_THREAD_END);
            return makeCall;
        } catch (WebRequestException e) {
            AdError adError = e.getStatus() != WebRequestStatus.NETWORK_FAILURE ? e.getStatus() == WebRequestStatus.NETWORK_TIMEOUT ? new AdError(ErrorCode.NETWORK_TIMEOUT, "Connection to Ad Server timed out") : new AdError(ErrorCode.INTERNAL_ERROR, e.getMessage()) : new AdError(ErrorCode.NETWORK_ERROR, "Could not contact Ad Server");
            throw new AdFetchException(adError);
        }
    }

    private MetricsCollector getCompositeMetricsCollector() {
        if (this.compositeMetricsCollector == null) {
            ArrayList arrayList = new ArrayList();
            for (Entry value : this.slots.entrySet()) {
                arrayList.add(((AdSlot) value.getValue()).getMetricsCollector());
            }
            this.compositeMetricsCollector = new CompositeMetricsCollector(arrayList);
        }
        return this.compositeMetricsCollector;
    }
}
