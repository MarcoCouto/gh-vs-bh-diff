package com.amazon.device.ads;

class WebRequestUserId {
    private final AdvertisingIdParameter adIdParam;
    private final Settings settings;
    private UserIdParameter userIdParam;

    public WebRequestUserId() {
        this(Settings.getInstance(), new AdvertisingIdParameter());
    }

    WebRequestUserId(Settings settings2, AdvertisingIdParameter advertisingIdParameter) {
        this.settings = settings2;
        this.adIdParam = advertisingIdParameter;
    }

    private void setupUserIdParam() {
        if (this.userIdParam == null) {
            this.userIdParam = (UserIdParameter) this.settings.getObject(UserIdParameter.SETTINGS_KEY, this.adIdParam, UserIdParameter.class);
        }
    }

    public boolean populateWebRequestUserId(WebRequest webRequest) {
        setupUserIdParam();
        boolean evaluate = this.userIdParam.evaluate(webRequest);
        return (evaluate || this.userIdParam == this.adIdParam) ? evaluate : this.adIdParam.evaluate(webRequest);
    }
}
