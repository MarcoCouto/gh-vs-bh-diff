package com.amazon.device.ads;

import android.content.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

public class GDPRInfo {
    private static final String LOGTAG = "GDPRInfo";
    private MobileAdsLogger logger = new MobileAdsLoggerFactory().createMobileAdsLogger(LOGTAG);
    private SharedPreferences prefs;

    public GDPRInfo(SharedPreferences sharedPreferences) {
        this.prefs = sharedPreferences;
    }

    public JSONObject toJsonObject() {
        return toJsonObject(this.prefs);
    }

    public JSONObject toJsonObject(SharedPreferences sharedPreferences) {
        JSONObject jSONObject;
        String string = sharedPreferences.getString(AdConstants.IABCONSENT_SUBJECT_TO_GDPR, null);
        String string2 = sharedPreferences.getString(AdConstants.APS_GDPR_PUB_PREF_LI, null);
        String string3 = sharedPreferences.getString(AdConstants.IABCONSENT_CONSENT_STRING, null);
        if (string3 != null) {
            try {
                jSONObject = new JSONObject();
                jSONObject.put("c", string3);
            } catch (JSONException unused) {
                this.logger.w("INVALID JSON formed for GDPR clause");
                return null;
            }
        } else {
            jSONObject = null;
        }
        if (string != null) {
            String trim = string.trim();
            if (jSONObject == null) {
                jSONObject = new JSONObject();
            }
            jSONObject.put("e", trim);
        }
        if (string2 != null) {
            String trim2 = string2.trim();
            if (jSONObject == null) {
                jSONObject = new JSONObject();
            }
            jSONObject.put("i", trim2);
        }
        return jSONObject;
    }
}
