package com.amazon.device.ads;

import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;

class Version {
    private static String buildVersion = "5.9.0";
    private static String devBuild = "(DEV)";
    private static String prefixVersion = "amznAdSDK-android-";
    private static String sdkVersion = null;
    private static String userAgentPrefixVersion = "AmazonAdSDK-Android/";
    private static String userAgentSDKVersion;

    Version() {
    }

    public static String getRawSDKVersion() {
        String str = buildVersion;
        if (str == null || str.equals("")) {
            return devBuild;
        }
        if (!str.endsWith(AvidJSONUtil.KEY_X)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(devBuild);
        return sb.toString();
    }

    public static String getSDKVersion() {
        if (sdkVersion == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(prefixVersion);
            sb.append(getRawSDKVersion());
            sdkVersion = sb.toString();
        }
        return sdkVersion;
    }

    static void setSDKVersion(String str) {
        sdkVersion = str;
    }

    public static String getUserAgentSDKVersion() {
        if (userAgentSDKVersion == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(userAgentPrefixVersion);
            sb.append(getRawSDKVersion());
            userAgentSDKVersion = sb.toString();
        }
        return userAgentSDKVersion;
    }
}
