package com.amazon.device.ads;

import com.amazon.device.ads.SDKEvent.SDKEventType;
import java.util.concurrent.atomic.AtomicBoolean;

class AdCloser {
    private static final String LOGTAG = "AdCloser";
    private final AdController adController;
    private final AtomicBoolean isClosing;
    private final MobileAdsLogger logger;

    public AdCloser(AdController adController2) {
        this(adController2, new MobileAdsLoggerFactory());
    }

    AdCloser(AdController adController2, MobileAdsLoggerFactory mobileAdsLoggerFactory) {
        this.isClosing = new AtomicBoolean(false);
        this.adController = adController2;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
    }

    public boolean closeAd() {
        boolean z;
        boolean z2;
        this.logger.d("Ad is attempting to close.");
        if (this.adController.getAdState().equals(AdState.READY_TO_LOAD)) {
            return false;
        }
        boolean z3 = true;
        if (this.isClosing.getAndSet(true)) {
            return false;
        }
        switch (this.adController.getAdControlCallback().adClosing()) {
            case 0:
                z2 = true;
                break;
            case 1:
                z2 = true;
                z = true;
                break;
            default:
                z2 = false;
                break;
        }
        z = false;
        if (z2) {
            this.adController.fireSDKEvent(new SDKEvent(SDKEventType.CLOSED));
        } else {
            z3 = false;
        }
        if (z) {
            this.adController.resetToReady();
        }
        this.isClosing.set(false);
        return z3;
    }
}
