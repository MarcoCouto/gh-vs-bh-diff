package com.amazon.device.ads;

import com.ironsource.sdk.constants.Constants.ForceClosePosition;
import java.util.HashMap;

enum RelativePosition {
    TOP_LEFT,
    TOP_RIGHT,
    CENTER,
    BOTTOM_LEFT,
    BOTTOM_RIGHT,
    TOP_CENTER,
    BOTTOM_CENTER;
    
    private static final HashMap<String, RelativePosition> POSITIONS = null;

    static {
        POSITIONS = new HashMap<>();
        POSITIONS.put(ForceClosePosition.TOP_LEFT, TOP_LEFT);
        POSITIONS.put(ForceClosePosition.TOP_RIGHT, TOP_RIGHT);
        POSITIONS.put("top-center", TOP_CENTER);
        POSITIONS.put(ForceClosePosition.BOTTOM_LEFT, BOTTOM_LEFT);
        POSITIONS.put(ForceClosePosition.BOTTOM_RIGHT, BOTTOM_RIGHT);
        POSITIONS.put("bottom-center", BOTTOM_CENTER);
        POSITIONS.put(TtmlNode.CENTER, CENTER);
    }

    public static RelativePosition fromString(String str) {
        return (RelativePosition) POSITIONS.get(str);
    }
}
