package com.amazon.device.ads;

import java.util.HashMap;
import org.json.JSONObject;

/* compiled from: SISRequests */
abstract class SISDeviceRequest extends SISRequest {
    private AdvertisingIdentifier advertisingIdentifier;
    private Info advertisingIdentifierInfo = this.advertisingIdentifier.getAdvertisingIdentifierInfo();

    private static String convertOptOutBooleanToStringInt(boolean z) {
        return z ? "1" : "0";
    }

    public HashMap<String, String> getPostParameters() {
        return null;
    }

    SISDeviceRequest(MobileAdsLoggerFactory mobileAdsLoggerFactory, String str, MetricType metricType, String str2, AdvertisingIdentifier advertisingIdentifier2, MobileAdsInfoStore mobileAdsInfoStore, Configuration configuration) {
        super(mobileAdsLoggerFactory, str, metricType, str2, mobileAdsInfoStore, configuration);
        this.advertisingIdentifier = advertisingIdentifier2;
    }

    public QueryStringParameters getQueryParameters() {
        QueryStringParameters queryParameters = super.getQueryParameters();
        DeviceInfo deviceInfo = this.mobileAdsInfoStore.getDeviceInfo();
        queryParameters.putUnencoded("ua", deviceInfo.getUserAgentString());
        queryParameters.putUnencoded("dinfo", deviceInfo.getDInfoProperty().toString());
        if (this.advertisingIdentifierInfo.hasAdvertisingIdentifier()) {
            queryParameters.putUrlEncoded("idfa", this.advertisingIdentifierInfo.getAdvertisingIdentifier());
            queryParameters.putUrlEncoded("oo", convertOptOutBooleanToStringInt(this.advertisingIdentifierInfo.isLimitAdTrackingEnabled()));
        } else {
            queryParameters.putUrlEncoded("sha1_mac", deviceInfo.getMacSha1());
            queryParameters.putUrlEncoded("sha1_serial", deviceInfo.getSerialSha1());
            queryParameters.putUrlEncoded("sha1_udid", deviceInfo.getUdidSha1());
            queryParameters.putUrlEncodedIfTrue("badMac", "true", deviceInfo.isMacBad());
            queryParameters.putUrlEncodedIfTrue("badSerial", "true", deviceInfo.isSerialBad());
            queryParameters.putUrlEncodedIfTrue("badUdid", "true", deviceInfo.isUdidBad());
        }
        String andClearTransition = this.advertisingIdentifier.getAndClearTransition();
        queryParameters.putUrlEncodedIfTrue("aidts", andClearTransition, andClearTransition != null);
        return queryParameters;
    }

    /* access modifiers changed from: protected */
    public Info getAdvertisingIdentifierInfo() {
        return this.advertisingIdentifierInfo;
    }

    public void onResponseReceived(JSONObject jSONObject) {
        String stringFromJSON = JSONUtils.getStringFromJSON(jSONObject, "adId", "");
        if (stringFromJSON.length() > 0) {
            this.mobileAdsInfoStore.getRegistrationInfo().putAdId(stringFromJSON, getAdvertisingIdentifierInfo());
        }
    }
}
