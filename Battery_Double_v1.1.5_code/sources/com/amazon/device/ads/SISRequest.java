package com.amazon.device.ads;

import com.amazon.device.ads.Configuration.ConfigOption;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: SISRequests */
abstract class SISRequest {
    private final MetricType callMetricType;
    private final Configuration configuration;
    private final String logTag;
    protected final MobileAdsLogger logger;
    protected MobileAdsInfoStore mobileAdsInfoStore;
    private final String path;

    /* compiled from: SISRequests */
    enum SISDeviceRequestType {
        GENERATE_DID,
        UPDATE_DEVICE_INFO
    }

    /* compiled from: SISRequests */
    static class SISRequestFactory {
        SISRequestFactory() {
        }

        public SISDeviceRequest createDeviceRequest(SISDeviceRequestType sISDeviceRequestType, AdvertisingIdentifier advertisingIdentifier) {
            switch (sISDeviceRequestType) {
                case GENERATE_DID:
                    return new SISGenerateDIDRequest(advertisingIdentifier);
                case UPDATE_DEVICE_INFO:
                    return new SISUpdateDeviceInfoRequest(advertisingIdentifier);
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("SISRequestType ");
                    sb.append(sISDeviceRequestType);
                    sb.append(" is not a SISDeviceRequest");
                    throw new IllegalArgumentException(sb.toString());
            }
        }

        public SISRegisterEventRequest createRegisterEventRequest(Info info, JSONArray jSONArray) {
            return new SISRegisterEventRequest(info, jSONArray);
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract HashMap<String, String> getPostParameters();

    /* access modifiers changed from: 0000 */
    public abstract void onResponseReceived(JSONObject jSONObject);

    SISRequest(MobileAdsLoggerFactory mobileAdsLoggerFactory, String str, MetricType metricType, String str2, MobileAdsInfoStore mobileAdsInfoStore2, Configuration configuration2) {
        this.logTag = str;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(this.logTag);
        this.callMetricType = metricType;
        this.path = str2;
        this.mobileAdsInfoStore = mobileAdsInfoStore2;
        this.configuration = configuration2;
    }

    /* access modifiers changed from: 0000 */
    public MobileAdsLogger getLogger() {
        return this.logger;
    }

    /* access modifiers changed from: 0000 */
    public String getLogTag() {
        return this.logTag;
    }

    /* access modifiers changed from: 0000 */
    public MetricType getCallMetricType() {
        return this.callMetricType;
    }

    /* access modifiers changed from: 0000 */
    public String getPath() {
        return this.path;
    }

    /* access modifiers changed from: 0000 */
    public QueryStringParameters getQueryParameters() {
        QueryStringParameters queryStringParameters = new QueryStringParameters();
        queryStringParameters.putUrlEncoded("dt", this.mobileAdsInfoStore.getDeviceInfo().getDeviceType());
        queryStringParameters.putUrlEncoded("app", this.mobileAdsInfoStore.getRegistrationInfo().getAppName());
        queryStringParameters.putUrlEncoded("appId", this.mobileAdsInfoStore.getRegistrationInfo().getAppKey());
        queryStringParameters.putUrlEncoded("sdkVer", Version.getSDKVersion());
        queryStringParameters.putUrlEncoded("aud", this.configuration.getString(ConfigOption.SIS_DOMAIN));
        queryStringParameters.putUnencoded("pkg", this.mobileAdsInfoStore.getAppInfo().getPackageInfoJSONString());
        return queryStringParameters;
    }
}
