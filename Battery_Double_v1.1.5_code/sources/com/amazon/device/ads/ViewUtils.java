package com.amazon.device.ads;

import android.annotation.SuppressLint;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

class ViewUtils {
    private final AndroidBuildInfo androidBuildInfo;

    public ViewUtils() {
        this(new AndroidBuildInfo());
    }

    ViewUtils(AndroidBuildInfo androidBuildInfo2) {
        this.androidBuildInfo = androidBuildInfo2;
    }

    @SuppressLint({"NewApi"})
    public boolean removeOnGlobalLayoutListener(ViewTreeObserver viewTreeObserver, OnGlobalLayoutListener onGlobalLayoutListener) {
        if (!viewTreeObserver.isAlive()) {
            return false;
        }
        if (AndroidTargetUtils.isAtLeastAndroidAPI(this.androidBuildInfo, 16)) {
            viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener);
        } else {
            viewTreeObserver.removeGlobalOnLayoutListener(onGlobalLayoutListener);
        }
        return true;
    }
}
