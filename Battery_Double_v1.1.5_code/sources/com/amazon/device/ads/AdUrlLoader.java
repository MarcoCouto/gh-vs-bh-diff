package com.amazon.device.ads;

import com.amazon.device.ads.ThreadUtils.ExecutionStyle;
import com.amazon.device.ads.ThreadUtils.ExecutionThread;
import com.amazon.device.ads.ThreadUtils.ThreadRunner;
import com.amazon.device.ads.WebRequest.WebRequestException;
import com.amazon.device.ads.WebRequest.WebRequestFactory;
import com.amazon.device.ads.WebRequest.WebResponse;

class AdUrlLoader {
    private static final String LOGTAG = "AdUrlLoader";
    /* access modifiers changed from: private */
    public final AdControlAccessor adControlAccessor;
    private final AdWebViewClient adWebViewClient;
    private final DeviceInfo deviceInfo;
    private final MobileAdsLogger logger;
    private final ThreadRunner threadRunner;
    private final WebRequestFactory webRequestFactory;
    private final WebUtils2 webUtils;

    public AdUrlLoader(ThreadRunner threadRunner2, AdWebViewClient adWebViewClient2, WebRequestFactory webRequestFactory2, AdControlAccessor adControlAccessor2, WebUtils2 webUtils2, MobileAdsLoggerFactory mobileAdsLoggerFactory, DeviceInfo deviceInfo2) {
        this.threadRunner = threadRunner2;
        this.adWebViewClient = adWebViewClient2;
        this.webRequestFactory = webRequestFactory2;
        this.adControlAccessor = adControlAccessor2;
        this.webUtils = webUtils2;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        this.deviceInfo = deviceInfo2;
    }

    public void putUrlExecutorInAdWebViewClient(String str, UrlExecutor urlExecutor) {
        this.adWebViewClient.putUrlExecutor(str, urlExecutor);
    }

    public void setAdWebViewClientListener(AdWebViewClientListener adWebViewClientListener) {
        this.adWebViewClient.setListener(adWebViewClientListener);
    }

    public AdWebViewClient getAdWebViewClient() {
        return this.adWebViewClient;
    }

    public void loadUrl(final String str, final boolean z, final PreloadCallback preloadCallback) {
        String scheme = this.webUtils.getScheme(str);
        if (scheme.equals("http") || scheme.equals("https")) {
            this.threadRunner.execute(new Runnable() {
                public void run() {
                    AdUrlLoader.this.loadUrlInThread(str, z, preloadCallback);
                }
            }, ExecutionStyle.RUN_ASAP, ExecutionThread.BACKGROUND_THREAD);
        } else {
            openUrl(str);
        }
    }

    /* access modifiers changed from: private */
    public void loadUrlInThread(String str, boolean z, PreloadCallback preloadCallback) {
        WebResponse webResponse;
        WebRequest createWebRequest = this.webRequestFactory.createWebRequest();
        createWebRequest.setExternalLogTag(LOGTAG);
        createWebRequest.enableLogUrl(true);
        createWebRequest.setUrlString(str);
        createWebRequest.putHeader("User-Agent", this.deviceInfo.getUserAgentString());
        try {
            webResponse = createWebRequest.makeCall();
        } catch (WebRequestException e) {
            this.logger.e("Could not load URL (%s) into AdContainer: %s", str, e.getMessage());
            webResponse = null;
        }
        if (webResponse != null) {
            final String readAsString = webResponse.getResponseReader().readAsString();
            if (readAsString != null) {
                ThreadRunner threadRunner2 = this.threadRunner;
                final String str2 = str;
                final boolean z2 = z;
                final PreloadCallback preloadCallback2 = preloadCallback;
                AnonymousClass2 r3 = new Runnable() {
                    public void run() {
                        AdUrlLoader.this.adControlAccessor.loadHtml(str2, readAsString, z2, preloadCallback2);
                    }
                };
                threadRunner2.execute(r3, ExecutionStyle.RUN_ASAP, ExecutionThread.MAIN_THREAD);
                return;
            }
            this.logger.e("Could not load URL (%s) into AdContainer.", str);
        }
    }

    public void openUrl(String str) {
        this.adWebViewClient.openUrl(str);
    }
}
