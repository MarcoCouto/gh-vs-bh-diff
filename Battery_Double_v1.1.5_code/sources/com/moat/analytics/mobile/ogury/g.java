package com.moat.analytics.mobile.ogury;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class g {

    /* renamed from: ˋ reason: contains not printable characters */
    private static final g f529 = new g();

    /* renamed from: ˊ reason: contains not printable characters */
    private final ScheduledExecutorService f530 = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */

    /* renamed from: ˎ reason: contains not printable characters */
    public final Map<f, String> f531 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ˏ reason: contains not printable characters */
    public final Map<c, String> f532 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public ScheduledFuture<?> f533;
    /* access modifiers changed from: private */

    /* renamed from: ᐝ reason: contains not printable characters */
    public ScheduledFuture<?> f534;

    /* renamed from: ˋ reason: contains not printable characters */
    static g m483() {
        return f529;
    }

    private g() {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m489(final Context context, f fVar) {
        this.f531.put(fVar, "");
        if (this.f534 == null || this.f534.isDone()) {
            AnonymousClass1.m449(3, "JSUpdateLooper", this, "Starting metadata reporting loop");
            this.f534 = this.f530.scheduleWithFixedDelay(new Runnable() {
                public final void run() {
                    try {
                        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_METADATA"));
                        if (g.this.f531.isEmpty()) {
                            g.this.f534.cancel(true);
                        }
                    } catch (Exception e) {
                        l.m516(e);
                    }
                }
            }, 0, 50, TimeUnit.MILLISECONDS);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m487(f fVar) {
        StringBuilder sb = new StringBuilder("removeSetupNeededBridge");
        sb.append(fVar.hashCode());
        AnonymousClass1.m449(3, "JSUpdateLooper", this, sb.toString());
        this.f531.remove(fVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m488(final Context context, c cVar) {
        if (cVar != null) {
            StringBuilder sb = new StringBuilder("addActiveTracker");
            sb.append(cVar.hashCode());
            AnonymousClass1.m449(3, "JSUpdateLooper", this, sb.toString());
            if (!this.f532.containsKey(cVar)) {
                this.f532.put(cVar, "");
                if (this.f533 == null || this.f533.isDone()) {
                    AnonymousClass1.m449(3, "JSUpdateLooper", this, "Starting view update loop");
                    this.f533 = this.f530.scheduleWithFixedDelay(new Runnable() {
                        public final void run() {
                            try {
                                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_VIEW_INFO"));
                                if (g.this.f532.isEmpty()) {
                                    AnonymousClass1.m449(3, "JSUpdateLooper", g.this, "No more active trackers");
                                    g.this.f533.cancel(true);
                                }
                            } catch (Exception e) {
                                l.m516(e);
                            }
                        }
                    }, 0, (long) q.m549().f610, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m490(c cVar) {
        if (cVar != null) {
            StringBuilder sb = new StringBuilder("removeActiveTracker");
            sb.append(cVar.hashCode());
            AnonymousClass1.m449(3, "JSUpdateLooper", this, sb.toString());
            this.f532.remove(cVar);
        }
    }
}
