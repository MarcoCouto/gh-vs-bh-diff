package com.moat.analytics.mobile.ogury;

import android.support.annotation.CallSuper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class h extends e {

    /* renamed from: ˊॱ reason: contains not printable characters */
    private int f539 = Integer.MIN_VALUE;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private int f540 = Integer.MIN_VALUE;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private double f541 = Double.NaN;

    /* renamed from: ͺ reason: contains not printable characters */
    private int f542 = d.f548;

    /* renamed from: ॱˊ reason: contains not printable characters */
    private int f543 = Integer.MIN_VALUE;

    /* renamed from: ॱˋ reason: contains not printable characters */
    private int f544 = 0;

    enum d {
        ;
        

        /* renamed from: ˊ reason: contains not printable characters */
        public static final int f547 = 2;

        /* renamed from: ˋ reason: contains not printable characters */
        public static final int f548 = 1;

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f549 = 3;

        /* renamed from: ˏ reason: contains not printable characters */
        public static final int f550 = 5;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f551 = 4;

        static {
            f546 = new int[]{1, 2, 3, 4, 5};
        }

        public static int[] values$48d63df8() {
            return (int[]) f546.clone();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻॱ reason: contains not printable characters */
    public abstract Integer m491();

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋॱ reason: contains not printable characters */
    public abstract boolean m494();

    /* access modifiers changed from: 0000 */
    /* renamed from: ͺ reason: contains not printable characters */
    public abstract Integer m495();

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝॱ reason: contains not printable characters */
    public abstract boolean m497();

    h(String str) {
        super(str);
    }

    /* renamed from: ˋ reason: contains not printable characters */
    public final boolean m493(Map<String, String> map, View view) {
        try {
            boolean r4 = super.m444(map, view);
            if (!r4) {
                return r4;
            }
            this.f499.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (!h.this.m494() || h.this.m447()) {
                            h.this.m446();
                        } else if (h.this.m496()) {
                            h.this.f499.postDelayed(this, 200);
                        } else {
                            h.this.m446();
                        }
                    } catch (Exception e) {
                        h.this.m446();
                        l.m516(e);
                    }
                }
            }, 200);
            return r4;
        } catch (Exception e) {
            AnonymousClass1.m449(3, "IntervalVideoTracker", this, "Problem with video loop");
            m428("trackVideoAd", e);
            return false;
        }
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            l.m516(e);
        }
    }

    public void setPlayerVolume(Double d2) {
        super.setPlayerVolume(d2);
        this.f541 = m440().doubleValue();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final JSONObject m492(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.f450.equals(MoatAdEvent.f446)) {
            num = moatAdEvent.f450;
        } else {
            try {
                num = m495();
            } catch (Exception unused) {
                num = Integer.valueOf(this.f543);
            }
            moatAdEvent.f450 = num;
        }
        if (moatAdEvent.f450.intValue() < 0 || (moatAdEvent.f450.intValue() == 0 && moatAdEvent.f449 == MoatAdEventType.AD_EVT_COMPLETE && this.f543 > 0)) {
            num = Integer.valueOf(this.f543);
            moatAdEvent.f450 = num;
        }
        if (moatAdEvent.f449 == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.f539 == Integer.MIN_VALUE || !m439(num, Integer.valueOf(this.f539))) {
                this.f542 = d.f551;
                moatAdEvent.f449 = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.f542 = d.f550;
            }
        }
        return super.m441(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ॱᐝ reason: contains not printable characters */
    public final boolean m496() throws l {
        if (!m494() || m447()) {
            return false;
        }
        try {
            int intValue = m495().intValue();
            if (this.f543 >= 0 && intValue < 0) {
                return false;
            }
            this.f543 = intValue;
            if (intValue == 0) {
                return true;
            }
            int intValue2 = m491().intValue();
            boolean r4 = m497();
            double d2 = (double) intValue2;
            Double.isNaN(d2);
            double d3 = d2 / 4.0d;
            double doubleValue = m440().doubleValue();
            MoatAdEventType moatAdEventType = null;
            if (intValue > this.f540) {
                this.f540 = intValue;
            }
            if (this.f539 == Integer.MIN_VALUE) {
                this.f539 = intValue2;
            }
            if (r4) {
                if (this.f542 == d.f548) {
                    moatAdEventType = MoatAdEventType.AD_EVT_START;
                    this.f542 = d.f549;
                } else if (this.f542 == d.f547) {
                    moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                    this.f542 = d.f549;
                } else {
                    double d4 = (double) intValue;
                    Double.isNaN(d4);
                    int floor = ((int) Math.floor(d4 / d3)) - 1;
                    if (floor >= 0 && floor < 3) {
                        MoatAdEventType moatAdEventType2 = f490[floor];
                        if (!this.f491.containsKey(moatAdEventType2)) {
                            this.f491.put(moatAdEventType2, Integer.valueOf(1));
                            moatAdEventType = moatAdEventType2;
                        }
                    }
                }
            } else if (this.f542 != d.f547) {
                moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                this.f542 = d.f547;
            }
            boolean z = moatAdEventType != null;
            if (!z && !Double.isNaN(this.f541) && Math.abs(this.f541 - doubleValue) > 0.05d) {
                moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                z = true;
            }
            if (z) {
                dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), m443()));
            }
            this.f541 = doubleValue;
            this.f544 = 0;
            return true;
        } catch (Exception unused) {
            int i = this.f544;
            this.f544 = i + 1;
            return i < 5;
        }
    }
}
