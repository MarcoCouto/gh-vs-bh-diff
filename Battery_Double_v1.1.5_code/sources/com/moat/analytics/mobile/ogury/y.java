package com.moat.analytics.mobile.ogury;

import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class y extends e implements ReactiveVideoTracker {

    /* renamed from: ˋॱ reason: contains not printable characters */
    private Integer f674;

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final String m610() {
        return "ReactiveVideoTracker";
    }

    public y(String str) {
        super(str);
        AnonymousClass1.m449(3, "ReactiveVideoTracker", this, "Initializing.");
        AnonymousClass1.m453("[SUCCESS] ", "ReactiveVideoTracker created");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public final Map<String, Object> m611() throws l {
        HashMap hashMap = new HashMap();
        View view = (View) this.f492.get();
        Integer valueOf = Integer.valueOf(0);
        Integer valueOf2 = Integer.valueOf(0);
        if (view != null) {
            valueOf = Integer.valueOf(view.getWidth());
            valueOf2 = Integer.valueOf(view.getHeight());
        }
        hashMap.put(IronSourceConstants.EVENTS_DURATION, this.f674);
        hashMap.put("width", valueOf);
        hashMap.put("height", valueOf2);
        return hashMap;
    }

    public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            m431();
            m426();
            this.f674 = num;
            return super.m444(map, view);
        } catch (Exception e) {
            m428("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final JSONObject m608(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.f449 == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.f450.equals(MoatAdEvent.f446) && !m439(moatAdEvent.f450, this.f674)) {
            moatAdEvent.f449 = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.m441(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m609(List<String> list) throws l {
        if (this.f674.intValue() >= 1000) {
            super.m442(list);
            return;
        }
        throw new l(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", new Object[]{this.f674}));
    }
}
