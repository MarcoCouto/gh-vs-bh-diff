package com.moat.analytics.mobile.ogury;

import android.media.MediaPlayer;
import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class r extends h implements NativeVideoTracker {

    /* renamed from: ͺ reason: contains not printable characters */
    private WeakReference<MediaPlayer> f634;

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final String m571() {
        return "NativeVideoTracker";
    }

    r(String str) {
        super(str);
        AnonymousClass1.m449(3, "NativeVideoTracker", this, "In initialization method.");
        if (str == null || str.isEmpty()) {
            StringBuilder sb = new StringBuilder("PartnerCode is ");
            sb.append(str == null ? "null" : "empty");
            String obj = sb.toString();
            String concat = "NativeDisplayTracker creation problem, ".concat(String.valueOf(obj));
            AnonymousClass1.m449(3, "NativeVideoTracker", this, concat);
            AnonymousClass1.m453("[ERROR] ", concat);
            this.f473 = new l(obj);
        }
        AnonymousClass1.m453("[SUCCESS] ", "NativeVideoTracker created");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋॱ reason: contains not printable characters */
    public final boolean m570() {
        return (this.f634 == null || this.f634.get() == null) ? false : true;
    }

    public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
        try {
            m431();
            m426();
            if (mediaPlayer != null) {
                mediaPlayer.getCurrentPosition();
                this.f634 = new WeakReference<>(mediaPlayer);
                return super.m493(map, view);
            }
            throw new l("Null player instance");
        } catch (Exception unused) {
            throw new l("Playback has already completed");
        } catch (Exception e) {
            l.m516(e);
            String r2 = l.m514("trackVideoAd", e);
            if (this.f474 != null) {
                this.f474.onTrackingFailedToStart(r2);
            }
            AnonymousClass1.m449(3, "NativeVideoTracker", this, r2);
            AnonymousClass1.m453("[ERROR] ", "NativeVideoTracker ".concat(String.valueOf(r2)));
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ͺ reason: contains not printable characters */
    public final Integer m572() {
        return Integer.valueOf(((MediaPlayer) this.f634.get()).getCurrentPosition());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝॱ reason: contains not printable characters */
    public final boolean m574() {
        return ((MediaPlayer) this.f634.get()).isPlaying();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻॱ reason: contains not printable characters */
    public final Integer m568() {
        return Integer.valueOf(((MediaPlayer) this.f634.get()).getDuration());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public final Map<String, Object> m573() throws l {
        MediaPlayer mediaPlayer = (MediaPlayer) this.f634.get();
        HashMap hashMap = new HashMap();
        hashMap.put("width", Integer.valueOf(mediaPlayer.getVideoWidth()));
        hashMap.put("height", Integer.valueOf(mediaPlayer.getVideoHeight()));
        hashMap.put(IronSourceConstants.EVENTS_DURATION, Integer.valueOf(mediaPlayer.getDuration()));
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m569(List<String> list) throws l {
        if (!((this.f634 == null || this.f634.get() == null) ? false : true)) {
            list.add("Player is null");
        }
        super.m442(list);
    }
}
