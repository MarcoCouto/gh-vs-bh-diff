package com.moat.analytics.mobile.ogury;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class q {

    /* renamed from: ʻ reason: contains not printable characters */
    private static q f605;
    /* access modifiers changed from: private */

    /* renamed from: ʼ reason: contains not printable characters */
    public static final Queue<a> f606 = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */

    /* renamed from: ʽ reason: contains not printable characters */
    public Handler f607;

    /* renamed from: ˊ reason: contains not printable characters */
    volatile int f608 = 10;
    /* access modifiers changed from: private */

    /* renamed from: ˊॱ reason: contains not printable characters */
    public final AtomicInteger f609 = new AtomicInteger(0);

    /* renamed from: ˋ reason: contains not printable characters */
    volatile int f610 = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
    /* access modifiers changed from: private */

    /* renamed from: ˋॱ reason: contains not printable characters */
    public volatile long f611 = 0;

    /* renamed from: ˎ reason: contains not printable characters */
    volatile boolean f612 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    volatile boolean f613 = false;
    /* access modifiers changed from: private */

    /* renamed from: ͺ reason: contains not printable characters */
    public final AtomicBoolean f614 = new AtomicBoolean(false);

    /* renamed from: ॱ reason: contains not printable characters */
    volatile int f615 = e.f631;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ reason: contains not printable characters */
    public final AtomicBoolean f616 = new AtomicBoolean(false);
    /* access modifiers changed from: private */

    /* renamed from: ॱॱ reason: contains not printable characters */
    public long f617 = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;

    /* renamed from: ᐝ reason: contains not printable characters */
    private long f618 = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;

    class a {

        /* renamed from: ˋ reason: contains not printable characters */
        final Long f622;

        /* renamed from: ॱ reason: contains not printable characters */
        final b f624;

        a(Long l, b bVar) {
            this.f622 = l;
            this.f624 = bVar;
        }
    }

    interface b {
        /* renamed from: ˋ reason: contains not printable characters */
        void m564() throws l;
    }

    interface c {
        /* renamed from: ॱ reason: contains not printable characters */
        void m565(j jVar) throws l;
    }

    class d implements Runnable {

        /* renamed from: ˋ reason: contains not printable characters */
        private final String f626;
        /* access modifiers changed from: private */

        /* renamed from: ˎ reason: contains not printable characters */
        public final c f627;

        /* renamed from: ॱ reason: contains not printable characters */
        private final Handler f628;

        /* synthetic */ d(q qVar, String str, Handler handler, c cVar, byte b) {
            this(str, handler, cVar);
        }

        private d(String str, Handler handler, c cVar) {
            this.f627 = cVar;
            this.f628 = handler;
            StringBuilder sb = new StringBuilder("https://z.moatads.com/");
            sb.append(str);
            sb.append("/android/");
            sb.append(BuildConfig.REVISION.substring(0, 7));
            sb.append("/status.json");
            this.f626 = sb.toString();
        }

        /* renamed from: ˊ reason: contains not printable characters */
        private String m566() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f626);
            sb.append("?ts=");
            sb.append(System.currentTimeMillis());
            sb.append("&v=2.4.3");
            try {
                return (String) k.m511(sb.toString()).get();
            } catch (Exception unused) {
                return null;
            }
        }

        public final void run() {
            try {
                String r0 = m566();
                final j jVar = new j(r0);
                q.this.f613 = jVar.m507();
                q.this.f612 = jVar.m509();
                q.this.f610 = jVar.m506();
                q.this.f608 = jVar.m508();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            d.this.f627.m565(jVar);
                        } catch (Exception e) {
                            l.m516(e);
                        }
                    }
                });
                q.this.f611 = System.currentTimeMillis();
                q.this.f614.compareAndSet(true, false);
                if (r0 != null) {
                    q.this.f609.set(0);
                } else if (q.this.f609.incrementAndGet() < 10) {
                    q.this.m560(q.this.f617);
                }
            } catch (Exception e) {
                l.m516(e);
            }
            this.f628.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    enum e {
        ;
        

        /* renamed from: ˊ reason: contains not printable characters */
        public static final int f631 = 1;

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f632 = 2;

        static {
            f633 = new int[]{1, 2};
        }

        public static int[] values$160b2863() {
            return (int[]) f633.clone();
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static synchronized q m549() {
        q qVar;
        synchronized (q.class) {
            if (f605 == null) {
                f605 = new q();
            }
            qVar = f605;
        }
        return qVar;
    }

    private q() {
        try {
            this.f607 = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            l.m516(e2);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m562() {
        if (System.currentTimeMillis() - this.f611 > this.f618) {
            m560(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ reason: contains not printable characters */
    public void m560(final long j) {
        if (this.f614.compareAndSet(false, true)) {
            AnonymousClass1.m449(3, "OnOff", this, "Performing status check.");
            new c() {
                public final void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    d dVar = new d(q.this, BuildConfig.NAMESPACE, handler, this, 0);
                    handler.postDelayed(dVar, j);
                    Looper.loop();
                }

                /* renamed from: ॱ reason: contains not printable characters */
                public final void m563(j jVar) throws l {
                    synchronized (q.f606) {
                        boolean z = ((i) MoatAnalytics.getInstance()).f556;
                        if (q.this.f615 != jVar.m510() || (q.this.f615 == e.f631 && z)) {
                            q.this.f615 = jVar.m510();
                            if (q.this.f615 == e.f631 && z) {
                                q.this.f615 = e.f632;
                            }
                            if (q.this.f615 == e.f632) {
                                AnonymousClass1.m449(3, "OnOff", this, "Moat enabled - Version 2.4.3");
                            }
                            for (a aVar : q.f606) {
                                if (q.this.f615 == e.f632) {
                                    aVar.f624.m564();
                                }
                            }
                        }
                        while (!q.f606.isEmpty()) {
                            q.f606.remove();
                        }
                    }
                }
            }.start();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m561(b bVar) throws l {
        if (this.f615 == e.f632) {
            bVar.m564();
            return;
        }
        m556();
        f606.add(new a(Long.valueOf(System.currentTimeMillis()), bVar));
        if (this.f616.compareAndSet(false, true)) {
            this.f607.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (q.f606.size() > 0) {
                            q.m556();
                            q.this.f607.postDelayed(this, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                            return;
                        }
                        q.this.f616.compareAndSet(true, false);
                        q.this.f607.removeCallbacks(this);
                    } catch (Exception e) {
                        l.m516(e);
                    }
                }
            }, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ reason: contains not printable characters */
    public static void m556() {
        synchronized (f606) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator it = f606.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - ((a) it.next()).f622.longValue() >= ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS) {
                    it.remove();
                }
            }
            if (f606.size() >= 15) {
                for (int i = 0; i < 5; i++) {
                    f606.remove();
                }
            }
        }
    }
}
