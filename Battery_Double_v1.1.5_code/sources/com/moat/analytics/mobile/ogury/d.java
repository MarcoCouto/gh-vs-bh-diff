package com.moat.analytics.mobile.ogury;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class d implements ValueCallback<String>, Runnable {

    /* renamed from: ʼ reason: contains not printable characters */
    private Handler f478;

    /* renamed from: ʽ reason: contains not printable characters */
    private Runnable f479;
    /* access modifiers changed from: private */

    /* renamed from: ˊ reason: contains not printable characters */
    public boolean f480 = false;

    /* renamed from: ˋ reason: contains not printable characters */
    f f481;

    /* renamed from: ˎ reason: contains not printable characters */
    WebView f482;

    /* renamed from: ˏ reason: contains not printable characters */
    private final int f483;

    /* renamed from: ॱ reason: contains not printable characters */
    final String f484 = String.format(Locale.ROOT, "_moatTracker%d", new Object[]{Integer.valueOf((int) (Math.random() * 1.0E8d))});

    enum b {
        ;
        

        /* renamed from: ˏ reason: contains not printable characters */
        public static final int f488 = 1;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f489 = 2;

        static {
            f487 = new int[]{1, 2};
        }

        public static int[] values$5b4351f2() {
            return (int[]) f487.clone();
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    d(Context context, int i) {
        this.f483 = i;
        this.f482 = new WebView(context);
        WebSettings settings = this.f482.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setDatabaseEnabled(false);
        settings.setDomStorageEnabled(false);
        settings.setGeolocationEnabled(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setSaveFormData(false);
        if (VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(1);
        }
        int i2 = a.f527;
        if (i == b.f489) {
            i2 = a.f526;
        }
        try {
            this.f481 = new f(this.f482, i2);
        } catch (l e) {
            l.m516(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m435(String str) {
        if (this.f483 == b.f488) {
            this.f482.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!d.this.f480) {
                        try {
                            d.this.f480 = true;
                            d.this.f481.m473();
                        } catch (Exception e) {
                            l.m516(e);
                        }
                    }
                }
            });
            WebView webView = this.f482;
            StringBuilder sb = new StringBuilder("<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n   <meta charset=\"UTF-8\">\n   <title></title>\n</head>\n<body style=\"margin:0;padding:0;\">\n    <script src=\"https://z.moatads.com/");
            sb.append(str);
            sb.append("/moatad.js\" type=\"text/javascript\"></script>\n</body>\n</html>");
            webView.loadData(sb.toString(), WebRequest.CONTENT_TYPE_HTML, "utf-8");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final void m436(String str, Map<String, String> map, Integer num, Integer num2, Integer num3) {
        if (this.f483 == b.f489) {
            if (VERSION.SDK_INT >= 19) {
                AnonymousClass1.m449(3, "GlobalWebView", this, "Starting off polling interval to check for Video API instance presence");
                this.f478 = new Handler();
                this.f479 = this;
                this.f478.post(this.f479);
            } else {
                StringBuilder sb = new StringBuilder("Android API version is less than KitKat: ");
                sb.append(VERSION.SDK_INT);
                AnonymousClass1.m449(3, "GlobalWebView", this, sb.toString());
                this.f482.setWebViewClient(new WebViewClient() {
                    public final void onPageFinished(WebView webView, String str) {
                        if (!d.this.f480) {
                            AnonymousClass1.m449(3, "GlobalWebView", this, "onPageFinished is called for the first time. Flushing event queue");
                            try {
                                d.this.f480 = true;
                                d.this.f481.m473();
                                d.this.f481.m474(d.this.f484);
                            } catch (Exception e) {
                                l.m516(e);
                            }
                        }
                    }
                });
            }
            JSONObject jSONObject = new JSONObject(map);
            this.f482.loadData(String.format(Locale.ROOT, "<html><head></head><body><div id=\"%s\" style=\"width: %dpx; height: %dpx;\"></div><script>(function initMoatTracking(apiname, pcode, ids, duration) {var events = [];window[pcode + '_moatElToTrack'] = document.getElementById('%s');var moatapi = {'dropTime':%d,'adData': {'ids': ids, 'duration': duration, 'url': 'n/a'},'dispatchEvent': function(ev) {if (this.sendEvent) {if (events) { events.push(ev); ev = events; events = false; }this.sendEvent(ev);} else {events.push(ev);}},'dispatchMany': function(evs){for (var i=0, l=evs.length; i<l; i++) {this.dispatchEvent(evs[i]);}}};Object.defineProperty(window, apiname, {'value': moatapi});var s = document.createElement('script');s.src = 'https://z.moatads.com/' + pcode + '/moatvideo.js?' + apiname + '#' + apiname;document.body.appendChild(s);})('%s', '%s', %s, %s);</script></body></html>", new Object[]{"mianahwvc", num, num2, "mianahwvc", Long.valueOf(System.currentTimeMillis()), this.f484, str, jSONObject.toString(), num3}), WebRequest.CONTENT_TYPE_HTML, null);
        }
    }

    public final void run() {
        try {
            if (this.f482 != null && VERSION.SDK_INT >= 19) {
                WebView webView = this.f482;
                StringBuilder sb = new StringBuilder("typeof ");
                sb.append(this.f484);
                sb.append(" !== 'undefined'");
                webView.evaluateJavascript(sb.toString(), this);
            }
        } catch (Exception e) {
            l.m516(e);
        }
    }

    public final /* synthetic */ void onReceiveValue(Object obj) {
        if ("true".equals((String) obj)) {
            AnonymousClass1.m449(3, "GlobalWebView", this, String.format("Video API instance %s detected. Flushing event queue", new Object[]{this.f484}));
            try {
                this.f480 = true;
                this.f481.m473();
                this.f481.m474(this.f484);
            } catch (Exception e) {
                l.m516(e);
            }
        } else {
            this.f478.postDelayed(this.f479, 200);
        }
    }
}
