package com.moat.analytics.mobile.ogury;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.webkit.WebView;

final class w extends c implements WebAdTracker {
    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final String m607() {
        return "WebAdTracker";
    }

    w(@Nullable ViewGroup viewGroup) {
        this((WebView) u.m594(viewGroup, false).orElse(null));
        if (viewGroup == null) {
            String str = "Target ViewGroup is null";
            String concat = "WebAdTracker initialization not successful, ".concat(String.valueOf(str));
            AnonymousClass1.m449(3, "WebAdTracker", this, concat);
            AnonymousClass1.m453("[ERROR] ", concat);
            this.f473 = new l(str);
        }
        if (this.f470 == null) {
            String str2 = "No WebView to track inside of ad container";
            String concat2 = "WebAdTracker initialization not successful, ".concat(String.valueOf(str2));
            AnonymousClass1.m449(3, "WebAdTracker", this, concat2);
            AnonymousClass1.m453("[ERROR] ", concat2);
            this.f473 = new l(str2);
        }
    }

    w(@Nullable WebView webView) {
        super(webView, false, false);
        AnonymousClass1.m449(3, "WebAdTracker", this, "Initializing.");
        if (webView == null) {
            String str = "WebView is null";
            String concat = "WebAdTracker initialization not successful, ".concat(String.valueOf(str));
            AnonymousClass1.m449(3, "WebAdTracker", this, concat);
            AnonymousClass1.m453("[ERROR] ", concat);
            this.f473 = new l(str);
            return;
        }
        try {
            super.m427(webView);
            StringBuilder sb = new StringBuilder("WebAdTracker created for ");
            sb.append(m422());
            AnonymousClass1.m453("[SUCCESS] ", sb.toString());
        } catch (l e) {
            this.f473 = e;
        }
    }
}
