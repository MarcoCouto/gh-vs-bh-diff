package com.moat.analytics.mobile.ogury;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class a {

    /* renamed from: ˊ reason: contains not printable characters */
    private static Application f458 = null;

    /* renamed from: ˋ reason: contains not printable characters */
    private static boolean f459 = false;

    /* renamed from: ˎ reason: contains not printable characters */
    static WeakReference<Activity> f460 = null;
    /* access modifiers changed from: private */

    /* renamed from: ˏ reason: contains not printable characters */
    public static int f461 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public static boolean f462 = false;

    /* renamed from: com.moat.analytics.mobile.ogury.a$a reason: collision with other inner class name */
    static class C0070a implements ActivityLifecycleCallbacks {
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        C0070a() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            a.f461 = 1;
        }

        public final void onActivityStarted(Activity activity) {
            try {
                a.f460 = new WeakReference<>(activity);
                a.f461 = 2;
                if (!a.f462) {
                    m420(true);
                }
                a.f462 = true;
                StringBuilder sb = new StringBuilder("Activity started: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m449(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                l.m516(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                a.f460 = new WeakReference<>(activity);
                a.f461 = 3;
                q.m549().m562();
                StringBuilder sb = new StringBuilder("Activity resumed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m449(3, "ActivityState", this, sb.toString());
                if (((i) MoatAnalytics.getInstance()).f558) {
                    b.m421(activity);
                }
            } catch (Exception e) {
                l.m516(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                a.f461 = 4;
                if (a.m419(activity)) {
                    a.f460 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity paused: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m449(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                l.m516(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (a.f461 != 3) {
                    a.f462 = false;
                    m420(false);
                }
                a.f461 = 5;
                if (a.m419(activity)) {
                    a.f460 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity stopped: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m449(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                l.m516(e);
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(a.f461 == 3 || a.f461 == 5)) {
                    if (a.f462) {
                        m420(false);
                    }
                    a.f462 = false;
                }
                a.f461 = 6;
                StringBuilder sb = new StringBuilder("Activity destroyed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m449(3, "ActivityState", this, sb.toString());
                if (a.m419(activity)) {
                    a.f460 = new WeakReference<>(null);
                }
            } catch (Exception e) {
                l.m516(e);
            }
        }

        /* renamed from: ˎ reason: contains not printable characters */
        private static void m420(boolean z) {
            if (z) {
                AnonymousClass1.m449(3, "ActivityState", null, "App became visible");
                if (q.m549().f615 == e.f632 && !((i) MoatAnalytics.getInstance()).f555) {
                    m.m524().m533();
                }
            } else {
                AnonymousClass1.m449(3, "ActivityState", null, "App became invisible");
                if (q.m549().f615 == e.f632 && !((i) MoatAnalytics.getInstance()).f555) {
                    m.m524().m534();
                }
            }
        }
    }

    a() {
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static void m413(Application application) {
        f458 = application;
        if (!f459) {
            f459 = true;
            f458.registerActivityLifecycleCallbacks(new C0070a());
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static Application m416() {
        return f458;
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static /* synthetic */ boolean m419(Activity activity) {
        return f460 != null && f460.get() == activity;
    }
}
