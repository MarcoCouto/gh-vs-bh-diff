package com.moat.analytics.mobile.ogury;

import android.os.Handler;
import android.support.annotation.CallSuper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

abstract class e extends c {

    /* renamed from: ʽ reason: contains not printable characters */
    static final MoatAdEventType[] f490 = {MoatAdEventType.AD_EVT_FIRST_QUARTILE, MoatAdEventType.AD_EVT_MID_POINT, MoatAdEventType.AD_EVT_THIRD_QUARTILE};

    /* renamed from: ʻ reason: contains not printable characters */
    final Map<MoatAdEventType, Integer> f491;

    /* renamed from: ʼ reason: contains not printable characters */
    WeakReference<View> f492;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private Map<String, String> f493;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private final Set<MoatAdEventType> f494;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ reason: contains not printable characters */
    public VideoTrackerListener f495;

    /* renamed from: ͺ reason: contains not printable characters */
    private Double f496;

    /* renamed from: ॱˊ reason: contains not printable characters */
    private boolean f497;

    /* renamed from: ॱˎ reason: contains not printable characters */
    private final String f498;

    /* renamed from: ॱॱ reason: contains not printable characters */
    final Handler f499;
    /* access modifiers changed from: private */

    /* renamed from: ॱᐝ reason: contains not printable characters */
    public final d f500 = new d(a.m416(), b.f489);

    /* renamed from: com.moat.analytics.mobile.ogury.e$1 reason: invalid class name */
    class AnonymousClass1 implements Runnable {
        AnonymousClass1() {
        }

        public final void run() {
            try {
                m449(3, "BaseVideoTracker", this, "Shutting down.");
                d r0 = e.this.f500;
                m449(3, "GlobalWebView", r0, "Cleaning up");
                r0.f481.m475();
                r0.f481 = null;
                r0.f482.destroy();
                r0.f482 = null;
                e.this.f495 = null;
            } catch (Exception e) {
                l.m516(e);
            }
        }

        AnonymousClass1() {
        }

        /* renamed from: ˋ reason: contains not printable characters */
        static void m449(int i, String str, Object obj, String str2) {
            if (q.m549().f613) {
                if (obj == null) {
                    Log.println(i, "Moat".concat(String.valueOf(str)), String.format("message = %s", new Object[]{str2}));
                    return;
                }
                Log.println(i, "Moat".concat(String.valueOf(str)), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}));
            }
        }

        /* renamed from: ˎ reason: contains not printable characters */
        static void m450(String str, Object obj, String str2) {
            Object obj2;
            if (q.m549().f612) {
                String concat = "Moat".concat(String.valueOf(str));
                String str3 = "id = %s, message = %s";
                Object[] objArr = new Object[2];
                if (obj == null) {
                    obj2 = "null";
                } else {
                    obj2 = Integer.valueOf(obj.hashCode());
                }
                objArr[0] = obj2;
                objArr[1] = str2;
                Log.println(2, concat, String.format(str3, objArr));
            }
        }

        /* renamed from: ˏ reason: contains not printable characters */
        static void m452(String str, Object obj, String str2, Throwable th) {
            if (q.m549().f613) {
                Log.e("Moat".concat(String.valueOf(str)), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}), th);
            }
        }

        /* renamed from: ॱ reason: contains not printable characters */
        static void m453(String str, String str2) {
            if (!q.m549().f613 && ((i) MoatAnalytics.getInstance()).f556) {
                int i = 2;
                if (str.equals("[ERROR] ")) {
                    i = 6;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(str2);
                Log.println(i, "MoatAnalytics", sb.toString());
            }
        }

        /* renamed from: ˏ reason: contains not printable characters */
        static String m451(View view) {
            if (view == null) {
                return "null";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(view.getClass().getSimpleName());
            sb.append("@");
            sb.append(view.hashCode());
            return sb.toString();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public abstract Map<String, Object> m448() throws l;

    e(String str) {
        super(null, false, true);
        AnonymousClass1.m449(3, "BaseVideoTracker", this, "Initializing.");
        this.f498 = str;
        try {
            super.m427(this.f500.f482);
        } catch (l e) {
            this.f473 = e;
        }
        this.f491 = new HashMap();
        this.f494 = new HashSet();
        this.f499 = new Handler();
        this.f497 = false;
        this.f496 = Double.valueOf(1.0d);
    }

    public void setVideoListener(VideoTrackerListener videoTrackerListener) {
        this.f495 = videoTrackerListener;
    }

    public void removeVideoListener() {
        this.f495 = null;
    }

    @CallSuper
    /* renamed from: ˋ reason: contains not printable characters */
    public boolean m444(Map<String, String> map, View view) {
        try {
            m431();
            m426();
            if (view == null) {
                AnonymousClass1.m449(3, "BaseVideoTracker", this, "trackVideoAd received null video view instance");
            }
            this.f493 = map;
            this.f492 = new WeakReference<>(view);
            m445();
            String format = String.format("trackVideoAd tracking ids: %s | view: %s", new Object[]{new JSONObject(map).toString(), AnonymousClass1.m451(view)});
            AnonymousClass1.m449(3, "BaseVideoTracker", this, format);
            StringBuilder sb = new StringBuilder();
            sb.append(m430());
            sb.append(" ");
            sb.append(format);
            AnonymousClass1.m453("[SUCCESS] ", sb.toString());
            if (this.f474 != null) {
                this.f474.onTrackingStarted(m422());
            }
            return true;
        } catch (Exception e) {
            m428("trackVideoAd", e);
            return false;
        }
    }

    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(AnonymousClass1.m451(view));
        AnonymousClass1.m449(3, "BaseVideoTracker", this, sb.toString());
        this.f492 = new WeakReference<>(view);
        try {
            super.changeTargetView(view);
        } catch (Exception e) {
            l.m516(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public void m442(List<String> list) throws l {
        if (this.f493 == null) {
            list.add("Null adIds object");
        }
        if (list.isEmpty()) {
            super.m424(list);
            return;
        }
        throw new l(TextUtils.join(" and ", list));
    }

    public void stopTracking() {
        try {
            super.stopTracking();
            m446();
            if (this.f495 != null) {
                this.f495 = null;
            }
        } catch (Exception e) {
            l.m516(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊॱ reason: contains not printable characters */
    public final Double m443() {
        return this.f496;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m445() throws l {
        super.changeTargetView((View) this.f492.get());
        super.m429();
        Map r0 = m448();
        Integer num = (Integer) r0.get("width");
        Integer num2 = (Integer) r0.get("height");
        Integer num3 = (Integer) r0.get(IronSourceConstants.EVENTS_DURATION);
        AnonymousClass1.m449(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "Player metadata: height = %d, width = %d, duration = %d", new Object[]{num2, num, num3}));
        this.f500.m436(this.f498, this.f493, num, num2, num3);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public JSONObject m441(MoatAdEvent moatAdEvent) {
        if (Double.isNaN(moatAdEvent.f448.doubleValue())) {
            moatAdEvent.f448 = this.f496;
        }
        return new JSONObject(moatAdEvent.m410());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏॱ reason: contains not printable characters */
    public final void m446() {
        if (!this.f497) {
            this.f497 = true;
            this.f499.postDelayed(new AnonymousClass1(), 500);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˊ reason: contains not printable characters */
    public final boolean m447() {
        return this.f491.containsKey(MoatAdEventType.AD_EVT_COMPLETE) || this.f491.containsKey(MoatAdEventType.AD_EVT_STOPPED) || this.f491.containsKey(MoatAdEventType.AD_EVT_SKIPPED);
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static boolean m439(Integer num, Integer num2) {
        int abs = Math.abs(num2.intValue() - num.intValue());
        double intValue = (double) num2.intValue();
        Double.isNaN(intValue);
        return ((double) abs) <= Math.min(750.0d, intValue * 0.05d);
    }

    public void setPlayerVolume(Double d) {
        Double valueOf = Double.valueOf(this.f496.doubleValue() * s.m579());
        if (!d.equals(this.f496)) {
            AnonymousClass1.m449(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "player volume changed to %f ", new Object[]{d}));
            this.f496 = d;
            if (!valueOf.equals(Double.valueOf(this.f496.doubleValue() * s.m579()))) {
                dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_VOLUME_CHANGE, MoatAdEvent.f446, this.f496));
            }
        }
    }

    public void dispatchEvent(MoatAdEvent moatAdEvent) {
        try {
            JSONObject r0 = m441(moatAdEvent);
            boolean z = false;
            AnonymousClass1.m449(3, "BaseVideoTracker", this, String.format("Received event: %s", new Object[]{r0.toString()}));
            StringBuilder sb = new StringBuilder();
            sb.append(m430());
            sb.append(String.format(" Received event: %s", new Object[]{r0.toString()}));
            AnonymousClass1.m453("[SUCCESS] ", sb.toString());
            if (m425() && this.f471 != null) {
                this.f471.m481(this.f500.f484, r0);
                if (!this.f494.contains(moatAdEvent.f449)) {
                    this.f494.add(moatAdEvent.f449);
                    if (this.f495 != null) {
                        this.f495.onVideoEventReported(moatAdEvent.f449);
                    }
                }
            }
            MoatAdEventType moatAdEventType = moatAdEvent.f449;
            if (moatAdEventType == MoatAdEventType.AD_EVT_COMPLETE || moatAdEventType == MoatAdEventType.AD_EVT_STOPPED || moatAdEventType == MoatAdEventType.AD_EVT_SKIPPED) {
                z = true;
            }
            if (z) {
                this.f491.put(moatAdEventType, Integer.valueOf(1));
                if (this.f471 != null) {
                    this.f471.m479((c) this);
                }
                m446();
            }
        } catch (Exception e) {
            l.m516(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʼ reason: contains not printable characters */
    public final Double m440() {
        return Double.valueOf(this.f496.doubleValue() * s.m579());
    }
}
