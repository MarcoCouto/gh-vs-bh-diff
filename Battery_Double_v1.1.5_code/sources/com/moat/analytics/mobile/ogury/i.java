package com.moat.analytics.mobile.ogury;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import java.lang.ref.WeakReference;

final class i extends MoatAnalytics implements b {

    /* renamed from: ʻ reason: contains not printable characters */
    private MoatOptions f552;

    /* renamed from: ʼ reason: contains not printable characters */
    private String f553;
    @Nullable

    /* renamed from: ˊ reason: contains not printable characters */
    d f554;

    /* renamed from: ˋ reason: contains not printable characters */
    boolean f555 = false;

    /* renamed from: ˎ reason: contains not printable characters */
    boolean f556 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    WeakReference<Context> f557;

    /* renamed from: ॱ reason: contains not printable characters */
    boolean f558 = false;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private boolean f559 = false;

    i() {
    }

    public final void start(Application application) {
        start(new MoatOptions(), application);
    }

    @UiThread
    public final void prepareNativeDisplayTracking(String str) {
        this.f553 = str;
        if (q.m549().f615 != e.f631) {
            try {
                m498();
            } catch (Exception e) {
                l.m516(e);
            }
        }
    }

    @UiThread
    /* renamed from: ॱ reason: contains not printable characters */
    private void m498() {
        if (this.f554 == null) {
            this.f554 = new d(a.m416(), b.f488);
            this.f554.m435(this.f553);
            StringBuilder sb = new StringBuilder("Preparing native display tracking with partner code ");
            sb.append(this.f553);
            AnonymousClass1.m449(3, "Analytics", this, sb.toString());
            StringBuilder sb2 = new StringBuilder("Prepared for native display tracking with partner code ");
            sb2.append(this.f553);
            AnonymousClass1.m453("[SUCCESS] ", sb2.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final boolean m501() {
        return this.f559;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final boolean m500() {
        return this.f552 != null && this.f552.disableLocationServices;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    public final void m499() throws l {
        l.m515();
        m.m524();
        if (this.f553 != null) {
            try {
                m498();
            } catch (Exception e) {
                l.m516(e);
            }
        }
    }

    public final void start(MoatOptions moatOptions, Application application) {
        try {
            if (this.f559) {
                AnonymousClass1.m449(3, "Analytics", this, "Moat SDK has already been started.");
                return;
            }
            this.f552 = moatOptions;
            q.m549().m562();
            this.f555 = moatOptions.disableLocationServices;
            if (application != null) {
                if (moatOptions.loggingEnabled && s.m586(application.getApplicationContext())) {
                    this.f556 = true;
                }
                this.f557 = new WeakReference<>(application.getApplicationContext());
                this.f559 = true;
                this.f558 = moatOptions.autoTrackGMAInterstitials;
                a.m413(application);
                q.m549().m561((b) this);
                if (!moatOptions.disableAdIdCollection) {
                    s.m583((Context) application);
                }
                AnonymousClass1.m453("[SUCCESS] ", "Moat Analytics SDK Version 2.4.3 started");
                return;
            }
            throw new l("Moat Analytics SDK didn't start, application was null");
        } catch (Exception e) {
            l.m516(e);
        }
    }
}
