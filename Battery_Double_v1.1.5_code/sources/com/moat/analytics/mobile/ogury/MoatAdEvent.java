package com.moat.analytics.mobile.ogury;

import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf(Utils.DOUBLE_EPSILON);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: ˋ reason: contains not printable characters */
    private static final Double f445 = Double.valueOf(Double.NaN);

    /* renamed from: ˏ reason: contains not printable characters */
    static final Integer f446 = Integer.valueOf(Integer.MIN_VALUE);

    /* renamed from: ʻ reason: contains not printable characters */
    private final Double f447;

    /* renamed from: ˊ reason: contains not printable characters */
    Double f448;

    /* renamed from: ˎ reason: contains not printable characters */
    MoatAdEventType f449;

    /* renamed from: ॱ reason: contains not printable characters */
    Integer f450;

    /* renamed from: ᐝ reason: contains not printable characters */
    private final Long f451;

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d) {
        this.f451 = Long.valueOf(System.currentTimeMillis());
        this.f449 = moatAdEventType;
        this.f448 = d;
        this.f450 = num;
        this.f447 = Double.valueOf(s.m579());
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, f445);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f446, f445);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final Map<String, Object> m410() {
        HashMap hashMap = new HashMap();
        hashMap.put("adVolume", this.f448);
        hashMap.put("playhead", this.f450);
        hashMap.put("aTimeStamp", this.f451);
        hashMap.put("type", this.f449.toString());
        hashMap.put(RequestParameters.DEVICE_VOLUME, this.f447);
        return hashMap;
    }
}
