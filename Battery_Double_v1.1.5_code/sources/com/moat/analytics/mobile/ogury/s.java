package com.moat.analytics.mobile.ogury;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.provider.Settings.Global;
import android.support.annotation.FloatRange;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.presage.CamembertauCalvados;
import java.lang.ref.WeakReference;

final class s {

    /* renamed from: ˊ reason: contains not printable characters */
    private static long f635 = 5996206772453036003L;

    /* renamed from: ˋ reason: contains not printable characters */
    private static String f636 = null;

    /* renamed from: ˎ reason: contains not printable characters */
    private static b f637 = null;

    /* renamed from: ˏ reason: contains not printable characters */
    private static a f638 = null;

    /* renamed from: ॱ reason: contains not printable characters */
    private static int f639 = 0;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private static int f640 = 1;

    static class a {

        /* renamed from: ˋ reason: contains not printable characters */
        private String f642;

        /* renamed from: ˎ reason: contains not printable characters */
        private String f643;

        /* renamed from: ˏ reason: contains not printable characters */
        private String f644;
        /* access modifiers changed from: private */

        /* renamed from: ॱ reason: contains not printable characters */
        public boolean f645;

        /* synthetic */ a(byte b) {
            this();
        }

        private a() {
            this.f645 = false;
            this.f643 = "_unknown_";
            this.f642 = "_unknown_";
            this.f644 = "_unknown_";
            try {
                Context r0 = s.m578();
                if (r0 != null) {
                    this.f645 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f642 = r0.getPackageName();
                    this.f643 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f644 = packageManager.getInstallerPackageName(this.f642);
                    return;
                }
                AnonymousClass1.m449(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                l.m516(e);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ॱ reason: contains not printable characters */
        public final String m590() {
            return this.f643;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˏ reason: contains not printable characters */
        public final String m589() {
            return this.f642;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˋ reason: contains not printable characters */
        public final String m588() {
            return this.f644 != null ? this.f644 : "_unknown_";
        }
    }

    static class b {

        /* renamed from: ʽ reason: contains not printable characters */
        boolean f646;

        /* renamed from: ˊ reason: contains not printable characters */
        String f647;

        /* renamed from: ˋ reason: contains not printable characters */
        Integer f648;

        /* renamed from: ˎ reason: contains not printable characters */
        String f649;

        /* renamed from: ˏ reason: contains not printable characters */
        boolean f650;

        /* renamed from: ॱ reason: contains not printable characters */
        boolean f651;

        /* synthetic */ b(byte b) {
            this();
        }

        private b() {
            this.f649 = "_unknown_";
            this.f647 = "_unknown_";
            this.f648 = Integer.valueOf(-1);
            this.f650 = false;
            this.f651 = false;
            this.f646 = false;
            try {
                Context r0 = s.m578();
                if (r0 != null) {
                    this.f646 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService(PlaceFields.PHONE);
                    this.f649 = telephonyManager.getSimOperatorName();
                    this.f647 = telephonyManager.getNetworkOperatorName();
                    this.f648 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f650 = s.m576();
                    this.f651 = s.m586(r0);
                }
            } catch (Exception e) {
                l.m516(e);
            }
        }
    }

    s() {
    }

    /* renamed from: ʻ reason: contains not printable characters */
    static /* synthetic */ String m575() {
        int i = f639 + 27;
        f640 = i % 128;
        int i2 = i % 2;
        String str = f636;
        int i3 = f640 + 115;
        f639 = i3 % 128;
        if ((i3 % 2 != 0 ? '*' : 'M') != '*') {
        }
        return str;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static /* synthetic */ String m580(String str) {
        int i = f640 + 125;
        f639 = i % 128;
        if (!(i % 2 != 0)) {
            f636 = str;
        } else {
            f636 = str;
            Object obj = null;
            super.hashCode();
        }
        int i2 = f639 + 43;
        f640 = i2 % 128;
        if (i2 % 2 == 0) {
        }
        return str;
    }

    static {
        int i = f639 + 121;
        f640 = i % 128;
        if (i % 2 == 0) {
            Object obj = null;
            super.hashCode();
        }
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    /* renamed from: ˋ reason: contains not printable characters */
    static double m579() {
        int i = f639 + 81;
        f640 = i % 128;
        if ((i % 2 == 0 ? '0' : '!') != '0') {
            try {
                double r1 = (double) m577();
                double streamMaxVolume = (double) ((AudioManager) a.m416().getSystemService(m582("꙼㞉ꘝꭠ㓂鵕馟").intern())).getStreamMaxVolume(3);
                Double.isNaN(r1);
                Double.isNaN(streamMaxVolume);
                return r1 / streamMaxVolume;
            } catch (Exception e) {
                l.m516(e);
                return Utils.DOUBLE_EPSILON;
            }
        } else {
            double r12 = (double) m577();
            double streamMaxVolume2 = (double) ((AudioManager) a.m416().getSystemService(m582("꙼㞉ꘝꭠ㓂鵕馟").intern())).getStreamMaxVolume(2);
            Double.isNaN(r12);
            Double.isNaN(streamMaxVolume2);
            return r12 * streamMaxVolume2;
        }
    }

    /* renamed from: ʽ reason: contains not printable characters */
    private static int m577() {
        int i = f640 + 39;
        f639 = i % 128;
        int i2 = i % 2;
        try {
            int streamVolume = ((AudioManager) a.m416().getSystemService(m582("꙼㞉ꘝꭠ㓂鵕馟").intern())).getStreamVolume(3);
            int i3 = f640 + 57;
            f639 = i3 % 128;
            if (i3 % 2 != 0) {
            }
            return streamVolume;
        } catch (Exception e) {
            l.m516(e);
            return 0;
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static void m583(final Context context) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
                        Class cls2 = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
                        Object invoke = cls.getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{context});
                        if (!((Boolean) cls2.getMethod(RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0])).booleanValue()) {
                            s.m580((String) cls2.getMethod("getId", new Class[0]).invoke(invoke, new Object[0]));
                            StringBuilder sb = new StringBuilder("Retrieved Advertising ID = ");
                            sb.append(s.m575());
                            AnonymousClass1.m449(3, "Util", this, sb.toString());
                            return;
                        }
                        AnonymousClass1.m449(3, "Util", this, "User has limited ad tracking");
                    } catch (ClassNotFoundException e) {
                        AnonymousClass1.m452("Util", this, "ClassNotFoundException while retrieving Advertising ID", e);
                    } catch (NoSuchMethodException e2) {
                        AnonymousClass1.m452("Util", this, "NoSuchMethodException while retrieving Advertising ID", e2);
                    } catch (Exception e3) {
                        l.m516(e3);
                    }
                }
            });
            int i = f640 + 103;
            f639 = i % 128;
            if (i % 2 != 0) {
            }
        } catch (Exception e) {
            l.m516(e);
        }
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static String m585() {
        int i = f639 + 117;
        f640 = i % 128;
        int i2 = i % 2;
        String str = f636;
        int i3 = f639 + 1;
        f640 = i3 % 128;
        if (!(i3 % 2 == 0)) {
        }
        return str;
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static Context m578() {
        int i = f640 + 39;
        f639 = i % 128;
        int i2 = i % 2;
        WeakReference<Context> weakReference = ((i) MoatAnalytics.getInstance()).f557;
        if ((weakReference != null ? 'c' : 1) != 'c') {
            return null;
        }
        int i3 = f640 + 103;
        f639 = i3 % 128;
        if ((i3 % 2 != 0 ? '7' : '*') != '7') {
            return (Context) weakReference.get();
        }
        return (Context) weakReference.get();
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static a m581() {
        int i = f639 + 103;
        f640 = i % 128;
        int i2 = i % 2;
        if (!(f638 != null) || !f638.f645) {
            f638 = new a(0);
            int i3 = f639 + 65;
            f640 = i3 % 128;
            int i4 = i3 % 2;
        }
        return f638;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (f637.f646 == false) goto L_0x001a;
     */
    /* renamed from: ˏ reason: contains not printable characters */
    static b m584() {
        if (f637 != null) {
            int i = f640 + 31;
            f639 = i % 128;
            int i2 = i % 2;
        }
        f637 = new b(0);
        int i3 = f640 + 45;
        f639 = i3 % 128;
        int i4 = i3 % 2;
        return f637;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0016, code lost:
        if ((r3.getApplicationInfo().flags & 4) != 0) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
        if (((r3.getApplicationInfo().flags & 2) == 0) != true) goto L_0x0028;
     */
    /* renamed from: ॱ reason: contains not printable characters */
    static boolean m586(Context context) {
        int i = f639 + 83;
        f640 = i % 128;
        if (i % 2 == 0) {
        }
        int i2 = f639 + 97;
        f640 = i2 % 128;
        if ((i2 % 2 == 0 ? '\'' : 'D') != 'D') {
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004f, code lost:
        if (android.os.Build.VERSION.SDK_INT >= 108) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005f, code lost:
        if ((android.os.Build.VERSION.SDK_INT >= 17 ? '7' : 'c') != 'c') goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0074, code lost:
        r0 = android.provider.Settings.Secure.getInt(r1.getContentResolver(), m582("낑ᗰ냰?⮲됖䤘⛮轸᫱欥쿥孮ꪬ").intern(), 0);
     */
    /* renamed from: ʼ reason: contains not printable characters */
    static /* synthetic */ boolean m576() {
        int i;
        int i2 = f640 + 107;
        f639 = i2 % 128;
        int i3 = i2 % 2;
        WeakReference<Context> weakReference = ((i) MoatAnalytics.getInstance()).f557;
        Context context = null;
        if (weakReference != null) {
            int i4 = f640 + 3;
            f639 = i4 % 128;
            if (i4 % 2 != 0) {
                Context context2 = (Context) weakReference.get();
                super.hashCode();
                context = context2;
            } else {
                context = (Context) weakReference.get();
            }
        }
        if ((context != null ? 'P' : 'A') != 'A') {
            int i5 = f639 + 9;
            f640 = i5 % 128;
            if (i5 % 2 == 0) {
            }
            i = Global.getInt(context.getContentResolver(), m582("낑ᗰ냰?⮲됖䤘⛮轸᫱欥쿥孮ꪬ").intern(), 0);
        } else {
            i = 0;
        }
        if (i == 1) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: type inference failed for: r10v1 */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000c, code lost:
        if (r10 != 0) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0014, code lost:
        if ((r10 == 0) != false) goto L_0x0033;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: ˎ reason: contains not printable characters */
    private static String m582(String str) {
        int i = f640 + 31;
        f639 = i % 128;
        if (i % 2 != 0) {
        }
        int i2 = f639 + 121;
        f640 = i2 % 128;
        str = (i2 % 2 == 0 ? ':' : 2) != ':' ? str.toCharArray() : str.toCharArray();
        char[] a2 = CamembertauCalvados.a(f635, (char[]) str);
        int i3 = 4;
        while (i3 < a2.length) {
            a2[i3] = (char) ((int) (((long) (a2[i3] ^ a2[i3 % 4])) ^ (((long) (i3 - 4)) * f635)));
            i3++;
            int i4 = f640 + 107;
            f639 = i4 % 128;
            if (i4 % 2 != 0) {
            }
        }
        return new String(a2, 4, a2.length - 4);
    }
}
