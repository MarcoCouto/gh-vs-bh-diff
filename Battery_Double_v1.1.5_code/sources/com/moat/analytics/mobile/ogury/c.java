package com.moat.analytics.mobile.ogury;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class c {

    /* renamed from: ʻ reason: contains not printable characters */
    private final v f467;

    /* renamed from: ʼ reason: contains not printable characters */
    private boolean f468;

    /* renamed from: ʽ reason: contains not printable characters */
    private WeakReference<View> f469;

    /* renamed from: ˊ reason: contains not printable characters */
    WeakReference<WebView> f470;

    /* renamed from: ˋ reason: contains not printable characters */
    f f471 = this.f500.f481;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private boolean f472;

    /* renamed from: ˎ reason: contains not printable characters */
    l f473 = null;

    /* renamed from: ˏ reason: contains not printable characters */
    TrackerListener f474;

    /* renamed from: ॱ reason: contains not printable characters */
    final String f475;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private final boolean f476;

    /* renamed from: ᐝ reason: contains not printable characters */
    final boolean f477;

    @Deprecated
    public void setActivity(Activity activity) {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public abstract String m430();

    c(@Nullable View view, boolean z, boolean z2) {
        String str;
        AnonymousClass1.m449(3, "BaseTracker", this, "Initializing.");
        if (z) {
            StringBuilder sb = new StringBuilder("m");
            sb.append(hashCode());
            str = sb.toString();
        } else {
            str = "";
        }
        this.f475 = str;
        this.f469 = new WeakReference<>(view);
        this.f476 = z;
        this.f477 = z2;
        this.f468 = false;
        this.f472 = false;
        this.f467 = new v();
    }

    public void setListener(TrackerListener trackerListener) {
        this.f474 = trackerListener;
    }

    public void removeListener() {
        this.f474 = null;
    }

    public void startTracking() {
        try {
            AnonymousClass1.m449(3, "BaseTracker", this, "In startTracking method.");
            m429();
            if (this.f474 != null) {
                TrackerListener trackerListener = this.f474;
                StringBuilder sb = new StringBuilder("Tracking started on ");
                sb.append(AnonymousClass1.m451((View) this.f469.get()));
                trackerListener.onTrackingStarted(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder("startTracking succeeded for ");
            sb2.append(AnonymousClass1.m451((View) this.f469.get()));
            String obj = sb2.toString();
            AnonymousClass1.m449(3, "BaseTracker", this, obj);
            StringBuilder sb3 = new StringBuilder();
            sb3.append(m430());
            sb3.append(" ");
            sb3.append(obj);
            AnonymousClass1.m453("[SUCCESS] ", sb3.toString());
        } catch (Exception e) {
            m428("startTracking", e);
        }
    }

    @CallSuper
    public void stopTracking() {
        boolean z = false;
        try {
            AnonymousClass1.m449(3, "BaseTracker", this, "In stopTracking method.");
            this.f472 = true;
            if (this.f471 != null) {
                this.f471.m479(this);
                z = true;
            }
        } catch (Exception e) {
            l.m516(e);
        }
        String str = "BaseTracker";
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        AnonymousClass1.m449(3, str, this, sb.toString());
        String str2 = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m430());
        sb2.append(" stopTracking ");
        sb2.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : ParametersKeys.FAILED);
        sb2.append(" for ");
        sb2.append(AnonymousClass1.m451((View) this.f469.get()));
        AnonymousClass1.m453(str2, sb2.toString());
        if (this.f474 != null) {
            this.f474.onTrackingStopped("");
            this.f474 = null;
        }
    }

    @CallSuper
    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(AnonymousClass1.m451(view));
        AnonymousClass1.m449(3, "BaseTracker", this, sb.toString());
        this.f469 = new WeakReference<>(view);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˎ reason: contains not printable characters */
    public void m429() throws l {
        AnonymousClass1.m449(3, "BaseTracker", this, "Attempting to start impression.");
        m431();
        if (this.f468) {
            throw new l("Tracker already started");
        } else if (!this.f472) {
            m424(new ArrayList());
            if (this.f471 != null) {
                this.f471.m476(this);
                this.f468 = true;
                AnonymousClass1.m449(3, "BaseTracker", this, "Impression started.");
                return;
            }
            AnonymousClass1.m449(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new l("Bridge is null");
        } else {
            throw new l("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m427(WebView webView) throws l {
        if (webView != null) {
            this.f470 = new WeakReference<>(webView);
            if (this.f471 == null) {
                if (!(this.f476 || this.f477)) {
                    AnonymousClass1.m449(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f470.get() != null) {
                        this.f471 = new f((WebView) this.f470.get(), a.f525);
                        AnonymousClass1.m449(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f471 = null;
                        AnonymousClass1.m449(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            if (this.f471 != null) {
                this.f471.m478(this);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m431() throws l {
        if (this.f473 != null) {
            StringBuilder sb = new StringBuilder("Tracker initialization failed: ");
            sb.append(this.f473.getMessage());
            throw new l(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final boolean m425() {
        return this.f468 && !this.f472;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱॱ reason: contains not printable characters */
    public final View m432() {
        return (View) this.f469.get();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʽ reason: contains not printable characters */
    public final String m423() {
        this.f467.m606(this.f475, (View) this.f469.get());
        return this.f467.f661;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m428(String str, Exception exc) {
        try {
            l.m516(exc);
            String r3 = l.m514(str, exc);
            if (this.f474 != null) {
                this.f474.onTrackingFailedToStart(r3);
            }
            AnonymousClass1.m449(3, "BaseTracker", this, r3);
            StringBuilder sb = new StringBuilder();
            sb.append(m430());
            sb.append(" ");
            sb.append(r3);
            AnonymousClass1.m453("[ERROR] ", sb.toString());
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m426() throws l {
        if (this.f468) {
            throw new l("Tracker already started");
        } else if (this.f472) {
            throw new l("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˊ reason: contains not printable characters */
    public void m424(List<String> list) throws l {
        if (((View) this.f469.get()) == null && !this.f477) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new l(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻ reason: contains not printable characters */
    public final String m422() {
        return AnonymousClass1.m451((View) this.f469.get());
    }
}
