package com.moat.analytics.mobile.ogury;

import android.graphics.Rect;
import android.view.View;
import com.moat.analytics.mobile.ogury.NativeDisplayTracker.MoatUserInteractionType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class t extends c implements NativeDisplayTracker {

    /* renamed from: ʼ reason: contains not printable characters */
    private final Map<String, String> f652;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private final Set<MoatUserInteractionType> f653 = new HashSet();

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final String m593() {
        return "NativeDisplayTracker";
    }

    t(View view, Map<String, String> map) {
        super(view, true, false);
        AnonymousClass1.m449(3, "NativeDisplayTracker", this, "Initializing.");
        this.f652 = map;
        if (view == null) {
            String str = "Target view is null";
            String concat = "NativeDisplayTracker initialization not successful, ".concat(String.valueOf(str));
            AnonymousClass1.m449(3, "NativeDisplayTracker", this, concat);
            AnonymousClass1.m453("[ERROR] ", concat);
            this.f473 = new l(str);
        } else if (map == null || map.isEmpty()) {
            String concat2 = "NativeDisplayTracker initialization not successful, ".concat(String.valueOf("AdIds is null or empty"));
            AnonymousClass1.m449(3, "NativeDisplayTracker", this, concat2);
            AnonymousClass1.m453("[ERROR] ", concat2);
            this.f473 = new l("AdIds is null or empty");
        } else {
            d dVar = ((i) i.getInstance()).f554;
            if (dVar == null) {
                String str2 = "prepareNativeDisplayTracking was not called successfully";
                String concat3 = "NativeDisplayTracker initialization not successful, ".concat(String.valueOf(str2));
                AnonymousClass1.m449(3, "NativeDisplayTracker", this, concat3);
                AnonymousClass1.m453("[ERROR] ", concat3);
                this.f473 = new l(str2);
                return;
            }
            this.f471 = dVar.f481;
            try {
                super.m427(dVar.f482);
                if (this.f471 != null) {
                    this.f471.m480(m592());
                }
                StringBuilder sb = new StringBuilder("NativeDisplayTracker created for ");
                sb.append(m422());
                sb.append(", with adIds:");
                sb.append(map.toString());
                AnonymousClass1.m453("[SUCCESS] ", sb.toString());
            } catch (l e) {
                this.f473 = e;
            }
        }
    }

    public final void reportUserInteractionEvent(MoatUserInteractionType moatUserInteractionType) {
        String str = "NativeDisplayTracker";
        try {
            StringBuilder sb = new StringBuilder("reportUserInteractionEvent:");
            sb.append(moatUserInteractionType.name());
            AnonymousClass1.m449(3, str, this, sb.toString());
            if (!this.f653.contains(moatUserInteractionType)) {
                this.f653.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.f475);
                jSONObject.accumulate("event", moatUserInteractionType.name().toLowerCase());
                if (this.f471 != null) {
                    this.f471.m477(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            AnonymousClass1.m450("NativeDisplayTracker", this, "Got JSON exception");
            l.m516(e);
        } catch (Exception e2) {
            l.m516(e2);
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private String m592() {
        String str = "";
        try {
            Map<String, String> map = this.f652;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 0; i < 8; i++) {
                String concat = "moatClientLevel".concat(String.valueOf(i));
                if (map.containsKey(concat)) {
                    linkedHashMap.put(concat, map.get(concat));
                }
            }
            for (int i2 = 0; i2 < 8; i2++) {
                String concat2 = "moatClientSlicer".concat(String.valueOf(i2));
                if (map.containsKey(concat2)) {
                    linkedHashMap.put(concat2, map.get(concat2));
                }
            }
            for (String str2 : map.keySet()) {
                if (!linkedHashMap.containsKey(str2)) {
                    linkedHashMap.put(str2, (String) map.get(str2));
                }
            }
            String obj = new JSONObject(linkedHashMap).toString();
            AnonymousClass1.m449(3, "NativeDisplayTracker", this, "Parsed ad ids = ".concat(String.valueOf(obj)));
            StringBuilder sb = new StringBuilder("{\"adIds\":");
            sb.append(obj);
            sb.append(", \"adKey\":\"");
            sb.append(this.f475);
            sb.append("\", \"adSize\":");
            sb.append(m591());
            sb.append("}");
            return sb.toString();
        } catch (Exception e) {
            l.m516(e);
            return str;
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private String m591() {
        try {
            Rect r0 = v.m603(super.m432());
            int width = r0.width();
            int height = r0.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            l.m516(e);
            return null;
        }
    }
}
