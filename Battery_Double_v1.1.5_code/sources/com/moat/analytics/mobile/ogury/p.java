package com.moat.analytics.mobile.ogury;

import android.support.annotation.VisibleForTesting;
import com.moat.analytics.mobile.ogury.base.asserts.Asserts;
import com.moat.analytics.mobile.ogury.base.functional.Optional;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class p<T> implements InvocationHandler {
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public static final Object[] f594 = new Object[0];

    /* renamed from: ʼ reason: contains not printable characters */
    private T f595;

    /* renamed from: ˊ reason: contains not printable characters */
    private final b<T> f596;

    /* renamed from: ˎ reason: contains not printable characters */
    private final Class<T> f597;

    /* renamed from: ˏ reason: contains not printable characters */
    private boolean f598;

    /* renamed from: ॱ reason: contains not printable characters */
    private final LinkedList<d> f599 = new LinkedList<>();

    interface b<T> {
        /* renamed from: ˏ reason: contains not printable characters */
        Optional<T> m546() throws l;
    }

    class d {

        /* renamed from: ˊ reason: contains not printable characters */
        private final LinkedList<Object> f601;
        /* access modifiers changed from: private */

        /* renamed from: ˎ reason: contains not printable characters */
        public final WeakReference[] f602;
        /* access modifiers changed from: private */

        /* renamed from: ˏ reason: contains not printable characters */
        public final Method f603;

        /* synthetic */ d(p pVar, Method method, Object[] objArr, byte b) {
            this(method, objArr);
        }

        private d(Method method, Object... objArr) {
            this.f601 = new LinkedList<>();
            if (objArr == null) {
                objArr = p.f594;
            }
            WeakReference[] weakReferenceArr = new WeakReference[objArr.length];
            int length = objArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Object obj = objArr[i];
                if ((obj instanceof Map) || (obj instanceof Integer) || (obj instanceof Double)) {
                    this.f601.add(obj);
                }
                int i3 = i2 + 1;
                weakReferenceArr[i2] = new WeakReference(obj);
                i++;
                i2 = i3;
            }
            this.f602 = weakReferenceArr;
            this.f603 = method;
        }
    }

    @VisibleForTesting
    private p(b<T> bVar, Class<T> cls) throws l {
        Asserts.checkNotNull(bVar);
        Asserts.checkNotNull(cls);
        this.f596 = bVar;
        this.f597 = cls;
        q.m549().m561((b) new b() {
            /* renamed from: ˋ reason: contains not printable characters */
            public final void m545() throws l {
                p.this.m540();
            }
        });
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static <T> T m541(b<T> bVar, Class<T> cls) throws l {
        ClassLoader classLoader = cls.getClassLoader();
        p pVar = new p(bVar, cls);
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, pVar);
    }

    /* renamed from: ˏ reason: contains not printable characters */
    private static Object m542(Method method) {
        try {
            if (Boolean.TYPE.equals(method.getReturnType())) {
                return Boolean.TRUE;
            }
        } catch (Exception e) {
            l.m516(e);
        }
        return null;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            Class declaringClass = method.getDeclaringClass();
            q r0 = q.m549();
            if (Object.class.equals(declaringClass)) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    return this.f597;
                }
                if (!"toString".equals(name)) {
                    return method.invoke(this, objArr);
                }
                Object invoke = method.invoke(this, objArr);
                return String.valueOf(invoke).replace(p.class.getName(), this.f597.getName());
            } else if (!this.f598 || this.f595 != null) {
                if (r0.f615 == e.f632) {
                    m540();
                    if (this.f595 != null) {
                        return method.invoke(this.f595, objArr);
                    }
                }
                if (r0.f615 == e.f631 && (!this.f598 || this.f595 != null)) {
                    if (this.f599.size() >= 15) {
                        this.f599.remove(5);
                    }
                    this.f599.add(new d(this, method, objArr, 0));
                }
                return m542(method);
            } else {
                this.f599.clear();
                return m542(method);
            }
        } catch (Exception e) {
            l.m516(e);
            return m542(method);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ reason: contains not printable characters */
    public void m540() throws l {
        if (!this.f598) {
            try {
                this.f595 = this.f596.m546().orElse(null);
            } catch (Exception e) {
                AnonymousClass1.m452("OnOffTrackerProxy", this, "Could not create instance", e);
                l.m516(e);
            }
            this.f598 = true;
        }
        if (this.f595 != null) {
            Iterator it = this.f599.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                try {
                    Object[] objArr = new Object[dVar.f602.length];
                    WeakReference[] r3 = dVar.f602;
                    int length = r3.length;
                    int i = 0;
                    int i2 = 0;
                    while (i < length) {
                        int i3 = i2 + 1;
                        objArr[i2] = r3[i].get();
                        i++;
                        i2 = i3;
                    }
                    dVar.f603.invoke(this.f595, objArr);
                } catch (Exception e2) {
                    l.m516(e2);
                }
            }
            this.f599.clear();
        }
    }
}
