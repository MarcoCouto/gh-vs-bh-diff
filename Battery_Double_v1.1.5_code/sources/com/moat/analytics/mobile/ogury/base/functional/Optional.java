package com.moat.analytics.mobile.ogury.base.functional;

import java.util.NoSuchElementException;

public final class Optional<T> {

    /* renamed from: ˏ reason: contains not printable characters */
    private static final Optional<?> f465 = new Optional<>();

    /* renamed from: ˎ reason: contains not printable characters */
    private final T f466;

    private Optional() {
        this.f466 = null;
    }

    public static <T> Optional<T> empty() {
        return f465;
    }

    private Optional(T t) {
        if (t != null) {
            this.f466 = t;
            return;
        }
        throw new NullPointerException("Optional of null value.");
    }

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public final T get() {
        if (this.f466 != null) {
            return this.f466;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.f466 != null;
    }

    public final T orElse(T t) {
        return this.f466 != null ? this.f466 : t;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        Optional optional = (Optional) obj;
        return this.f466 == optional.f466 || !(this.f466 == null || optional.f466 == null || !this.f466.equals(optional.f466));
    }

    public final int hashCode() {
        if (this.f466 == null) {
            return 0;
        }
        return this.f466.hashCode();
    }

    public final String toString() {
        if (this.f466 == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", new Object[]{this.f466});
    }
}
