package com.moat.analytics.mobile.ogury;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.github.mikephil.charting.utils.Utils;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class m implements LocationListener {

    /* renamed from: ˊ reason: contains not printable characters */
    private static m f575;

    /* renamed from: ʻ reason: contains not printable characters */
    private boolean f576;

    /* renamed from: ʽ reason: contains not printable characters */
    private Location f577;

    /* renamed from: ˋ reason: contains not printable characters */
    private ScheduledFuture<?> f578;

    /* renamed from: ˎ reason: contains not printable characters */
    private ScheduledFuture<?> f579;

    /* renamed from: ˏ reason: contains not printable characters */
    private ScheduledExecutorService f580;

    /* renamed from: ॱ reason: contains not printable characters */
    private LocationManager f581;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private boolean f582;

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static m m524() {
        if (f575 == null) {
            f575 = new m();
        }
        return f575;
    }

    private m() {
        try {
            this.f576 = ((i) MoatAnalytics.getInstance()).f555;
            if (this.f576) {
                AnonymousClass1.m449(3, "LocationManager", this, "Moat location services disabled");
                return;
            }
            this.f580 = Executors.newScheduledThreadPool(1);
            this.f581 = (LocationManager) a.m416().getSystemService("location");
            if (this.f581.getAllProviders().size() == 0) {
                AnonymousClass1.m449(3, "LocationManager", this, "Device has no location providers");
            } else {
                m528();
            }
        } catch (Exception e) {
            l.m516(e);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: ˏ reason: contains not printable characters */
    public final Location m535() {
        if (this.f576 || this.f581 == null) {
            return null;
        }
        return this.f577;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m533() {
        m528();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m534() {
        m529(false);
    }

    public final void onLocationChanged(Location location) {
        String str = "LocationManager";
        try {
            StringBuilder sb = new StringBuilder("Received an updated location = ");
            sb.append(location.toString());
            AnonymousClass1.m449(3, str, this, sb.toString());
            float currentTimeMillis = (float) ((System.currentTimeMillis() - location.getTime()) / 1000);
            if (location.hasAccuracy() && location.getAccuracy() <= 100.0f && currentTimeMillis < 600.0f) {
                this.f577 = m523(this.f577, location);
                AnonymousClass1.m449(3, "LocationManager", this, "fetchCompleted");
                m529(true);
            }
        } catch (Exception e) {
            l.m516(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ reason: contains not printable characters */
    public void m528() {
        try {
            if (!this.f576) {
                if (this.f581 != null) {
                    if (this.f582) {
                        AnonymousClass1.m449(3, "LocationManager", this, "already updating location");
                    }
                    AnonymousClass1.m449(3, "LocationManager", this, "starting location fetch");
                    this.f577 = m523(this.f577, m531());
                    if (this.f577 != null) {
                        StringBuilder sb = new StringBuilder("Have a valid location, won't fetch = ");
                        sb.append(this.f577.toString());
                        AnonymousClass1.m449(3, "LocationManager", this, sb.toString());
                        m520();
                        return;
                    }
                    m519();
                }
            }
        } catch (Exception e) {
            l.m516(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ reason: contains not printable characters */
    public void m529(boolean z) {
        try {
            AnonymousClass1.m449(3, "LocationManager", this, "stopping location fetch");
            m532();
            m518();
            if (z) {
                m520();
            } else {
                m517();
            }
        } catch (Exception e) {
            l.m516(e);
        }
    }

    /* renamed from: ॱॱ reason: contains not printable characters */
    private Location m531() {
        Location location;
        try {
            boolean r1 = m522();
            boolean r2 = m527();
            if (r1 && r2) {
                location = m523(this.f581.getLastKnownLocation("gps"), this.f581.getLastKnownLocation("network"));
            } else if (r1) {
                location = this.f581.getLastKnownLocation("gps");
            } else if (!r2) {
                return null;
            } else {
                location = this.f581.getLastKnownLocation("network");
            }
            return location;
        } catch (SecurityException e) {
            l.m516(e);
            return null;
        }
    }

    /* renamed from: ʽ reason: contains not printable characters */
    private void m519() {
        try {
            if (!this.f582) {
                AnonymousClass1.m449(3, "LocationManager", this, "Attempting to start update");
                if (m522()) {
                    AnonymousClass1.m449(3, "LocationManager", this, "start updating gps location");
                    this.f581.requestLocationUpdates("gps", 0, 0.0f, this, Looper.getMainLooper());
                    this.f582 = true;
                }
                if (m527()) {
                    AnonymousClass1.m449(3, "LocationManager", this, "start updating network location");
                    this.f581.requestLocationUpdates("network", 0, 0.0f, this, Looper.getMainLooper());
                    this.f582 = true;
                }
                if (this.f582) {
                    m518();
                    this.f579 = this.f580.schedule(new Runnable() {
                        public final void run() {
                            try {
                                AnonymousClass1.m449(3, "LocationManager", this, "fetchTimedOut");
                                m.this.m529(true);
                            } catch (Exception e) {
                                l.m516(e);
                            }
                        }
                    }, 60, TimeUnit.SECONDS);
                }
            }
        } catch (SecurityException e) {
            l.m516(e);
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private void m532() {
        try {
            AnonymousClass1.m449(3, "LocationManager", this, "Stopping to update location");
            boolean z = true;
            if (!(ContextCompat.checkSelfPermission(a.m416().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
                if (!(ContextCompat.checkSelfPermission(a.m416().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                    z = false;
                }
            }
            if (z && this.f581 != null) {
                this.f581.removeUpdates(this);
                this.f582 = false;
            }
        } catch (SecurityException e) {
            l.m516(e);
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private void m518() {
        if (this.f579 != null && !this.f579.isCancelled()) {
            this.f579.cancel(true);
            this.f579 = null;
        }
    }

    /* renamed from: ʻ reason: contains not printable characters */
    private void m517() {
        if (this.f578 != null && !this.f578.isCancelled()) {
            this.f578.cancel(true);
            this.f578 = null;
        }
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    private void m520() {
        AnonymousClass1.m449(3, "LocationManager", this, "Resetting fetch timer");
        m517();
        float f = 600.0f;
        if (this.f577 != null) {
            f = Math.max(600.0f - ((float) ((System.currentTimeMillis() - this.f577.getTime()) / 1000)), 0.0f);
        }
        this.f578 = this.f580.schedule(new Runnable() {
            public final void run() {
                try {
                    AnonymousClass1.m449(3, "LocationManager", this, "fetchTimerCompleted");
                    m.this.m528();
                } catch (Exception e) {
                    l.m516(e);
                }
            }
        }, (long) f, TimeUnit.SECONDS);
    }

    /* renamed from: ˎ reason: contains not printable characters */
    private static Location m523(Location location, Location location2) {
        boolean r0 = m521(location);
        boolean r1 = m521(location2);
        if (r0) {
            return (r1 && location.getAccuracy() >= location.getAccuracy()) ? location2 : location;
        }
        if (!r1) {
            return null;
        }
        return location2;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    private static boolean m521(Location location) {
        if (location == null) {
            return false;
        }
        if ((location.getLatitude() != Utils.DOUBLE_EPSILON || location.getLongitude() != Utils.DOUBLE_EPSILON) && location.getAccuracy() >= 0.0f && ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)) < 600.0f) {
            return true;
        }
        return false;
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static boolean m530(Location location, Location location2) {
        if (location == location2) {
            return true;
        }
        return (location == null || location2 == null || location.getTime() != location2.getTime()) ? false : true;
    }

    /* renamed from: ˋॱ reason: contains not printable characters */
    private boolean m522() {
        return (ContextCompat.checkSelfPermission(a.m416().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) && this.f581.getProvider("gps") != null && this.f581.isProviderEnabled("gps");
    }

    /* renamed from: ˏॱ reason: contains not printable characters */
    private boolean m527() {
        boolean z;
        if (!(ContextCompat.checkSelfPermission(a.m416().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
            if (!(ContextCompat.checkSelfPermission(a.m416().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                z = false;
                return !z && this.f581.getProvider("network") != null && this.f581.isProviderEnabled("network");
            }
        }
        z = true;
        if (!z) {
        }
    }
}
