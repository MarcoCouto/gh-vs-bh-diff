package com.moat.analytics.mobile.vng;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.ads.AdActivity;
import com.moat.analytics.mobile.vng.a.b.a;
import java.lang.ref.WeakReference;

class f {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private static WebAdTracker f3147a;
    private static WeakReference<Activity> b = new WeakReference<>(null);

    f() {
    }

    private static void a() {
        if (f3147a != null) {
            o.a(3, "GMAInterstitialHelper", b.get(), "Stopping to track GMA interstitial");
            f3147a.stopTracking();
            f3147a = null;
        }
    }

    static void a(Activity activity) {
        try {
            if (v.a().f3176a != d.OFF) {
                if (!b(activity)) {
                    a();
                    b = new WeakReference<>(null);
                } else if (b.get() == null || b.get() != activity) {
                    View decorView = activity.getWindow().getDecorView();
                    if (decorView instanceof ViewGroup) {
                        a a2 = aa.a((ViewGroup) decorView, true);
                        if (a2.c()) {
                            b = new WeakReference<>(activity);
                            a((WebView) a2.b());
                        } else {
                            o.a(3, "GMAInterstitialHelper", (Object) activity, "Sorry, no WebView in this activity");
                        }
                    }
                }
            }
        } catch (Exception e) {
            m.a(e);
        }
    }

    private static void a(WebView webView) {
        o.a(3, "GMAInterstitialHelper", b.get(), "Starting to track GMA interstitial");
        f3147a = MoatFactory.create().createWebAdTracker(webView);
        f3147a.startTracking();
    }

    private static boolean b(Activity activity) {
        String name = activity.getClass().getName();
        StringBuilder sb = new StringBuilder();
        sb.append("Activity name: ");
        sb.append(name);
        o.a(3, "GMAInterstitialHelper", (Object) activity, sb.toString());
        return name.contains(AdActivity.CLASS_NAME);
    }
}
