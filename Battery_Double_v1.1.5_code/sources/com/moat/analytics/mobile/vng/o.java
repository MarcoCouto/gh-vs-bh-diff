package com.moat.analytics.mobile.vng;

import android.util.Log;
import android.view.View;

class o {
    o() {
    }

    static String a(View view) {
        if (view == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(view.getClass().getSimpleName());
        sb.append("@");
        sb.append(view.hashCode());
        return sb.toString();
    }

    private static String a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Moat");
        sb.append(str);
        return sb.toString();
    }

    static void a(int i, String str, Object obj, String str2) {
        String a2;
        String format;
        if (v.a().b) {
            if (obj == null) {
                a2 = a(str);
                format = String.format("message = %s", new Object[]{str2});
            } else {
                a2 = a(str);
                format = String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2});
            }
            Log.println(i, a2, format);
        }
    }

    static void a(String str, int i, String str2, Object obj, String str3) {
        a(i, str2, obj, str3);
        a(str, str3);
    }

    static void a(String str, Object obj, String str2, Throwable th) {
        if (v.a().b) {
            Log.e(a(str), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}), th);
        }
    }

    static void a(String str, String str2) {
        if (!v.a().b && ((k) MoatAnalytics.getInstance()).f3164a) {
            int i = 2;
            if (str.equals("[ERROR] ")) {
                i = 6;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            Log.println(i, "MoatAnalytics", sb.toString());
        }
    }

    static void b(int i, String str, Object obj, String str2) {
        if (v.a().c) {
            String a2 = a(str);
            String str3 = "id = %s, message = %s";
            Object[] objArr = new Object[2];
            objArr[0] = obj == null ? "null" : Integer.valueOf(obj.hashCode());
            objArr[1] = str2;
            Log.println(i, a2, String.format(str3, objArr));
        }
    }
}
