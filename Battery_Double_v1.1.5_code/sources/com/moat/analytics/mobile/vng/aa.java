package com.moat.analytics.mobile.vng;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.vng.a.b.a;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;

class aa {

    /* renamed from: a reason: collision with root package name */
    private static final LinkedHashSet<String> f3144a = new LinkedHashSet<>();

    aa() {
    }

    @NonNull
    static a<WebView> a(ViewGroup viewGroup, boolean z) {
        if (viewGroup == null) {
            try {
                return a.a();
            } catch (Exception unused) {
                return a.a();
            }
        } else if (viewGroup instanceof WebView) {
            return a.a((WebView) viewGroup);
        } else {
            LinkedList linkedList = new LinkedList();
            linkedList.add(viewGroup);
            Object obj = null;
            int i = 0;
            while (!linkedList.isEmpty() && i < 100) {
                i++;
                ViewGroup viewGroup2 = (ViewGroup) linkedList.poll();
                int childCount = viewGroup2.getChildCount();
                Object obj2 = obj;
                int i2 = 0;
                while (true) {
                    if (i2 >= childCount) {
                        obj = obj2;
                        break;
                    }
                    View childAt = viewGroup2.getChildAt(i2);
                    if (childAt instanceof WebView) {
                        o.a(3, "WebViewHound", (Object) childAt, "Found WebView");
                        if (z || a(String.valueOf(childAt.hashCode()))) {
                            if (obj2 != null) {
                                o.a(3, "WebViewHound", (Object) childAt, "Ambiguous ad container: multiple WebViews reside within it.");
                                o.a("[ERROR] ", "WebAdTracker not created, ambiguous ad container: multiple WebViews reside within it");
                                obj = null;
                                break;
                            }
                            obj2 = (WebView) childAt;
                        }
                    }
                    if (childAt instanceof ViewGroup) {
                        linkedList.add((ViewGroup) childAt);
                    }
                    i2++;
                }
            }
            return a.b(obj);
        }
    }

    private static boolean a(String str) {
        try {
            boolean add = f3144a.add(str);
            if (f3144a.size() > 50) {
                Iterator it = f3144a.iterator();
                for (int i = 0; i < 25 && it.hasNext(); i++) {
                    it.next();
                    it.remove();
                }
            }
            o.a(3, "WebViewHound", (Object) null, add ? "Newly Found WebView" : "Already Found WebView");
            return add;
        } catch (Exception e) {
            m.a(e);
            return false;
        }
    }
}
