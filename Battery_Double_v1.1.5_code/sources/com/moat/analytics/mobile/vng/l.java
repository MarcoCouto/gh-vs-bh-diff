package com.moat.analytics.mobile.vng;

import android.os.Build.VERSION;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONArray;
import org.json.JSONObject;

class l {

    /* renamed from: a reason: collision with root package name */
    private boolean f3165a = false;
    private boolean b = false;
    private boolean c = false;
    private int d = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
    private int e = 10;

    l(String str) {
        a(str);
    }

    private void a(String str) {
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                String string = jSONObject.getString("sa");
                boolean equals = string.equals("fd28fb8353d87dc1a1db3246752e21ccc3328cbf");
                boolean equals2 = string.equals("8f1d08a2d6496191a5ebae8f0590f513e2619489");
                if ((string.equals(String.SPLIT_VIEW_TRIGGER_ON) || equals || equals2) && !a(jSONObject) && !b(jSONObject)) {
                    this.f3165a = true;
                    this.b = equals;
                    this.c = equals2;
                    if (this.c) {
                        this.b = true;
                    }
                }
                if (jSONObject.has("in")) {
                    int i = jSONObject.getInt("in");
                    if (i >= 100 && i <= 1000) {
                        this.d = i;
                    }
                }
                if (jSONObject.has("es")) {
                    this.e = jSONObject.getInt("es");
                }
            } catch (Exception e2) {
                this.f3165a = false;
                this.b = false;
                this.d = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
                m.a(e2);
            }
        }
    }

    private boolean a(JSONObject jSONObject) {
        try {
            if (16 > VERSION.SDK_INT) {
                return true;
            }
            if (jSONObject.has("ob")) {
                JSONArray jSONArray = jSONObject.getJSONArray("ob");
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    if (jSONArray.getInt(i) == VERSION.SDK_INT) {
                        return true;
                    }
                }
            }
            return false;
        } catch (Exception unused) {
            return true;
        }
    }

    private boolean b(JSONObject jSONObject) {
        try {
            if (jSONObject.has("ap")) {
                String b2 = q.d().b();
                JSONArray jSONArray = jSONObject.getJSONArray("ap");
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    if (jSONArray.getString(i).contentEquals(b2)) {
                        return true;
                    }
                }
            }
        } catch (Exception e2) {
            m.a(e2);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public int d() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public d e() {
        return this.f3165a ? d.ON : d.OFF;
    }
}
