package com.moat.analytics.mobile.vng;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

class a {

    /* renamed from: a reason: collision with root package name */
    static WeakReference<Activity> f3142a = null;
    private static boolean b = false;
    private static Application c = null;
    /* access modifiers changed from: private */
    public static int d = 0;
    /* access modifiers changed from: private */
    public static boolean e = false;

    /* renamed from: com.moat.analytics.mobile.vng.a$a reason: collision with other inner class name */
    private static class C0071a implements ActivityLifecycleCallbacks {
        C0071a() {
        }

        private static void a(boolean z) {
            String str;
            String str2;
            if (z) {
                str = "ActivityState";
                str2 = "App became visible";
            } else {
                str = "ActivityState";
                str2 = "App became invisible";
            }
            o.a(3, str, (Object) null, str2);
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            a.d = 1;
        }

        public void onActivityDestroyed(Activity activity) {
            try {
                if (!(a.d == 3 || a.d == 5)) {
                    if (a.e) {
                        a(false);
                    }
                    a.e = false;
                }
                a.d = 6;
                StringBuilder sb = new StringBuilder();
                sb.append("Activity destroyed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                o.a(3, "ActivityState", (Object) this, sb.toString());
                if (a.b(activity)) {
                    a.f3142a = new WeakReference<>(null);
                }
            } catch (Exception e) {
                m.a(e);
            }
        }

        public void onActivityPaused(Activity activity) {
            try {
                a.d = 4;
                if (a.b(activity)) {
                    a.f3142a = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder();
                sb.append("Activity paused: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                o.a(3, "ActivityState", (Object) this, sb.toString());
            } catch (Exception e) {
                m.a(e);
            }
        }

        public void onActivityResumed(Activity activity) {
            try {
                a.f3142a = new WeakReference<>(activity);
                a.d = 3;
                v.a().b();
                StringBuilder sb = new StringBuilder();
                sb.append("Activity resumed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                o.a(3, "ActivityState", (Object) this, sb.toString());
                if (((k) MoatAnalytics.getInstance()).b) {
                    f.a(activity);
                }
            } catch (Exception e) {
                m.a(e);
            }
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
            try {
                a.f3142a = new WeakReference<>(activity);
                a.d = 2;
                if (!a.e) {
                    a(true);
                }
                a.e = true;
                StringBuilder sb = new StringBuilder();
                sb.append("Activity started: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                o.a(3, "ActivityState", (Object) this, sb.toString());
            } catch (Exception e) {
                m.a(e);
            }
        }

        public void onActivityStopped(Activity activity) {
            try {
                if (a.d != 3) {
                    a.e = false;
                    a(false);
                }
                a.d = 5;
                if (a.b(activity)) {
                    a.f3142a = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder();
                sb.append("Activity stopped: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                o.a(3, "ActivityState", (Object) this, sb.toString());
            } catch (Exception e) {
                m.a(e);
            }
        }
    }

    a() {
    }

    static Application a() {
        return c;
    }

    static void a(Application application) {
        c = application;
        if (!b) {
            b = true;
            c.registerActivityLifecycleCallbacks(new C0071a());
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(Activity activity) {
        return f3142a != null && f3142a.get() == activity;
    }
}
