package com.moat.analytics.mobile.vng;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import com.appodeal.ads.utils.LogConstants;
import com.facebook.places.model.PlaceFields;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

class j {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public int f3159a = 0;
    private boolean b = false;
    private boolean c = false;
    private final AtomicBoolean d = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    private boolean g = false;
    @NonNull
    private final WeakReference<WebView> h;
    private final Map<b, String> i;
    private final LinkedList<String> j;
    /* access modifiers changed from: private */
    public final long k;
    private final String l;
    private final List<String> m;
    private final a n;
    private final BroadcastReceiver o = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                j.this.d();
            } catch (Exception e) {
                m.a(e);
            }
            if (System.currentTimeMillis() - j.this.k > 30000) {
                j.this.i();
            }
        }
    };
    private final BroadcastReceiver p = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                j.this.e();
            } catch (Exception e) {
                m.a(e);
            }
        }
    };

    enum a {
        WEBVIEW,
        NATIVE_DISPLAY,
        NATIVE_VIDEO
    }

    j(WebView webView, a aVar) {
        this.h = new WeakReference<>(webView);
        this.n = aVar;
        this.j = new LinkedList<>();
        this.m = new ArrayList();
        this.i = new WeakHashMap();
        this.k = System.currentTimeMillis();
        this.l = String.format("javascript:(function(d,k){function l(){function d(a,b){var c=ipkn[b]||ipkn[kuea];if(c){var h=function(b){var c=b.b;c.ts=b.i;c.ticks=b.g;c.buffered=!0;a(c)};h(c.first);c.a.forEach(function(a){h(a)})}}function e(a){var b=a.a,c=a.c,h=a.b;a=a.f;var d=[];if(c)b[c]&&d.push(b[c].fn[0]);else for(key in b)if(b[key])for(var g=0,e=b[key].fn.length;g<e;g++)d.push(b[key].fn[g]);g=0;for(e=d.length;g<e;g++){var f=d[g];if('function'===typeof f)try{h?f(h):f()}catch(k){}a&&delete b[c]}}function f(a,b,c){'function'===typeof a&& (b===kuea&&c[b]?c[b].fn.push(a):c[b]={ts:+new Date,fn:[a]},c===yhgt&&d(a,b))}kuea=+new Date;iymv={};briz=!1;ewat=+new Date;bnkr=[];bjmk={};dptk={};uqaj={};ryup={};yhgt={};ipkn={};csif={};this.h=function(a){this.namespace=a.namespace;this.version=a.version;this.appName=a.appName;this.deviceOS=a.deviceOS;this.isNative=a.isNative;this.versionHash=a.versionHash;this.aqzx=a.aqzx;this.appId=a.appId;this.metadata=a};this.nvsj=function(a){briz||(f(a,ewat,iymv),briz=!0)};this.bpsy=function(a,b){var c=b||kuea; c!==kuea&&bjmk[c]||f(a,c,bjmk)};this.qmrv=function(a,b){var c=b||kuea;c!==kuea&&uqaj[c]||f(a,c,uqaj)};this.lgpr=function(a,b){f(a,b||kuea,yhgt)};this.hgen=function(a,b){f(a,b||kuea,csif)};this.xrnk=function(a){delete yhgt[a||kuea]};this.vgft=function(a){return dptk[a||kuea]||!1};this.lkpu=function(a){return ryup[a||kuea]||!1};this.crts=function(a){var b={a:iymv,b:a,c:ewat};briz?e(b):bnkr.push(a)};this.mqjh=function(a){var b=a||kuea;dptk[b]=!0;var c={a:bjmk,f:!0};b!==kuea&&(c.b=a,c.c=a);e(c)};this.egpw= function(a){var b=a||kuea;ryup[b]=!0;var c={a:uqaj,f:!0};b!==kuea&&(c.b=a,c.c=a);e(c)};this.sglu=function(a){var b=a.adKey||kuea,c={a:yhgt,b:a.event||a,g:1,i:+new Date,f:!1};b!==kuea&&(c.c=a.adKey);a=0<Object.keys(yhgt).length;if(!a||!this.isNative)if(ipkn[b]){var d=ipkn[b].a.slice(-1)[0]||ipkn[b].first;JSON.stringify(c.b)==JSON.stringify(d.b)?d.g+=1:(5<=ipkn[b].a.length&&ipkn[b].a.shift(),ipkn[b].a.push(c))}else ipkn[b]={first:c,a:[]};a&&e(c);return a};this.ucbx=function(a){e({c:a.adKey||kuea,a:csif, b:a.event,f:!1})}}'undefined'===typeof d.MoatMAK&&(d.MoatMAK=new l,d.MoatMAK.h(k),d.__zMoatInit__=!0)})(window,%s);", new Object[]{h()});
        if (d(LogConstants.EVENT_INITIALIZE)) {
            IntentFilter intentFilter = new IntentFilter("UPDATE_METADATA");
            IntentFilter intentFilter2 = new IntentFilter("UPDATE_VIEW_INFO");
            LocalBroadcastManager.getInstance(q.c()).registerReceiver(this.o, intentFilter);
            LocalBroadcastManager.getInstance(q.c()).registerReceiver(this.p, intentFilter2);
            d();
            i.a().a(q.c(), this);
            o.a(3, "JavaScriptBridge", (Object) this, "bridge initialization succeeded");
        }
    }

    private boolean a(WebView webView) {
        return webView.getSettings().getJavaScriptEnabled();
    }

    private void c() {
        for (Entry key : this.i.entrySet()) {
            b bVar = (b) key.getKey();
            if (bVar.e()) {
                g(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.mqjh(\"%s\");}", new Object[]{bVar.e}));
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        try {
            if (v.a().f3176a != d.OFF) {
                if (!this.c) {
                    o.a(3, "JavaScriptBridge", (Object) this, "Attempting to establish communication (setting environment variables).");
                    this.c = true;
                }
                g(this.l);
            }
        } catch (Exception e2) {
            o.a("JavaScriptBridge", (Object) this, "Attempt failed to establish communication (did not set environment variables).", (Throwable) e2);
        }
    }

    private void d(b bVar) {
        o.a(3, "JavaScriptBridge", (Object) this, "Stopping view update loop");
        if (bVar != null) {
            i.a().a(bVar);
        }
    }

    private boolean d(String str) {
        WebView g2 = g();
        if (g2 == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("WebView is null. Can't ");
            sb.append(str);
            o.a(6, "JavaScriptBridge", (Object) this, sb.toString());
            throw new m("WebView is null");
        } else if (a(g2)) {
            return true;
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("JavaScript is not enabled in the given WebView. Can't ");
            sb2.append(str);
            o.a(6, "JavaScriptBridge", (Object) this, sb2.toString());
            throw new m("JavaScript is not enabled in the WebView");
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    public void e() {
        try {
            if (v.a().f3176a != d.OFF) {
                if (this.g) {
                    o.a(3, "JavaScriptBridge", (Object) this, "Can't send info, already cleaned up");
                    return;
                }
                if (f()) {
                    if (!this.b || g().getUrl() != null) {
                        if (g().getUrl() != null) {
                            this.b = true;
                        }
                        for (Entry key : this.i.entrySet()) {
                            b bVar = (b) key.getKey();
                            if (bVar == null || bVar.f() == null) {
                                o.a(3, "JavaScriptBridge", (Object) this, "Tracker has no subject");
                                if (bVar != null) {
                                    if (!bVar.f) {
                                    }
                                }
                                c(bVar);
                            }
                            if (bVar.e()) {
                                if (!this.d.get()) {
                                    g(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.mqjh(\"%s\");}", new Object[]{bVar.e}));
                                }
                                String format = String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.sglu(%s);}", new Object[]{bVar.h()});
                                if (VERSION.SDK_INT >= 19) {
                                    g().evaluateJavascript(format, new ValueCallback<String>() {
                                        /* renamed from: a */
                                        public void onReceiveValue(String str) {
                                            String str2;
                                            if (str == null || str.equalsIgnoreCase("null") || str.equalsIgnoreCase("false")) {
                                                String str3 = "JavaScriptBridge";
                                                j jVar = j.this;
                                                StringBuilder sb = new StringBuilder();
                                                sb.append("Received value is:");
                                                if (str == null) {
                                                    str2 = "null";
                                                } else {
                                                    StringBuilder sb2 = new StringBuilder();
                                                    sb2.append("(String)");
                                                    sb2.append(str);
                                                    str2 = sb2.toString();
                                                }
                                                sb.append(str2);
                                                o.a(3, str3, (Object) jVar, sb.toString());
                                                if (j.this.f3159a >= 150) {
                                                    o.a(3, "JavaScriptBridge", (Object) j.this, "Giving up on finding ad");
                                                    j.this.b();
                                                }
                                                j.this.f3159a = j.this.f3159a + 1;
                                                if (str != null && str.equalsIgnoreCase("false") && !j.this.e) {
                                                    j.this.e = true;
                                                    o.a(3, "JavaScriptBridge", (Object) j.this, "Bridge connection established");
                                                }
                                            } else if (str.equalsIgnoreCase("true")) {
                                                if (!j.this.f) {
                                                    j.this.f = true;
                                                    o.a(3, "JavaScriptBridge", (Object) j.this, "Javascript has found ad");
                                                    j.this.a();
                                                }
                                                j.this.f3159a = 0;
                                            } else {
                                                j jVar2 = j.this;
                                                StringBuilder sb3 = new StringBuilder();
                                                sb3.append("Received unusual value from Javascript:");
                                                sb3.append(str);
                                                o.a(3, "JavaScriptBridge", (Object) jVar2, sb3.toString());
                                            }
                                        }
                                    });
                                } else {
                                    g().loadUrl(format);
                                }
                            }
                        }
                        return;
                    }
                }
                String str = "JavaScriptBridge";
                StringBuilder sb = new StringBuilder();
                sb.append("WebView became null");
                sb.append(g() == null ? "" : "based on null url");
                sb.append(", stopping tracking loop");
                o.a(3, str, (Object) this, sb.toString());
                b();
            }
        } catch (Exception e2) {
            m.a(e2);
            b();
        }
    }

    private void e(String str) {
        if (this.m.size() >= 50) {
            this.m.subList(0, 25).clear();
        }
        this.m.add(str);
    }

    private void f(String str) {
        if (this.d.get()) {
            g(str);
        } else {
            e(str);
        }
    }

    private boolean f() {
        return g() != null;
    }

    private WebView g() {
        return (WebView) this.h.get();
    }

    @UiThread
    private void g(String str) {
        if (this.g) {
            o.a(3, "JavaScriptBridge", (Object) this, "Can't send, already cleaned up");
            return;
        }
        if (f()) {
            o.b(2, "JavaScriptBridge", this, str);
            if (VERSION.SDK_INT >= 19) {
                g().evaluateJavascript(str, null);
            } else {
                g().loadUrl(str);
            }
        }
    }

    private String h() {
        try {
            a d2 = q.d();
            b e2 = q.e();
            HashMap hashMap = new HashMap();
            String a2 = d2.a();
            String b2 = d2.b();
            String c2 = d2.c();
            String num = Integer.toString(VERSION.SDK_INT);
            String b3 = q.b();
            String str = this.n == a.WEBVIEW ? "0" : "1";
            String str2 = e2.e ? "1" : "0";
            String str3 = e2.d ? "1" : "0";
            hashMap.put("versionHash", "fd28fb8353d87dc1a1db3246752e21ccc3328cbf");
            hashMap.put("appName", a2);
            hashMap.put("namespace", "VNG");
            hashMap.put("version", "2.5.1");
            hashMap.put("deviceOS", num);
            hashMap.put("isNative", str);
            hashMap.put("appId", b2);
            hashMap.put("source", c2);
            hashMap.put("carrier", e2.b);
            hashMap.put("sim", e2.f3175a);
            hashMap.put(PlaceFields.PHONE, String.valueOf(e2.c));
            hashMap.put("buildFp", Build.FINGERPRINT);
            hashMap.put("buildModel", Build.MODEL);
            hashMap.put("buildMfg", Build.MANUFACTURER);
            hashMap.put("buildBrand", Build.BRAND);
            hashMap.put("buildProduct", Build.PRODUCT);
            hashMap.put("buildTags", Build.TAGS);
            hashMap.put("f1", str3);
            hashMap.put("f2", str2);
            if (b3 != null) {
                hashMap.put("aqzx", b3);
            }
            return new JSONObject(hashMap).toString();
        } catch (Exception unused) {
            return "{}";
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        o.a(3, "JavaScriptBridge", (Object) this, "Stopping metadata reporting loop");
        i.a().a(this);
        LocalBroadcastManager.getInstance(q.c()).unregisterReceiver(this.o);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        o.a(3, "JavaScriptBridge", (Object) this, "webViewReady");
        if (this.d.compareAndSet(false, true)) {
            o.a(3, "JavaScriptBridge", (Object) this, "webViewReady first time");
            i();
            for (String g2 : this.m) {
                g(g2);
            }
            this.m.clear();
        }
        c();
    }

    /* access modifiers changed from: 0000 */
    public void a(b bVar) {
        if (bVar != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("adding tracker");
            sb.append(bVar.e);
            o.a(3, "JavaScriptBridge", (Object) this, sb.toString());
            this.i.put(bVar, "");
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        f(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.crts(%s);}", new Object[]{str}));
    }

    /* access modifiers changed from: 0000 */
    @UiThread
    public void a(String str, JSONObject jSONObject) {
        if (this.g) {
            o.a(3, "JavaScriptBridge", (Object) this, "Can't dispatch, already cleaned up");
            return;
        }
        String jSONObject2 = jSONObject.toString();
        if (!this.d.get() || !f()) {
            this.j.add(jSONObject2);
        } else {
            g(String.format("javascript:%s.dispatchEvent(%s);", new Object[]{str, jSONObject2}));
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        o.a(3, "JavaScriptBridge", (Object) this, "Cleaning up");
        this.g = true;
        i();
        for (Entry key : this.i.entrySet()) {
            d((b) key.getKey());
        }
        this.i.clear();
        LocalBroadcastManager.getInstance(q.c()).unregisterReceiver(this.p);
    }

    /* access modifiers changed from: 0000 */
    public void b(b bVar) {
        if (d("startTracking")) {
            StringBuilder sb = new StringBuilder();
            sb.append("Starting tracking on tracker");
            sb.append(bVar.e);
            o.a(3, "JavaScriptBridge", (Object) this, sb.toString());
            g(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.mqjh(\"%s\");}", new Object[]{bVar.e}));
            i.a().a(q.c(), bVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("markUserInteractionEvent:");
        sb.append(str);
        o.a(3, "JavaScriptBridge", (Object) this, sb.toString());
        f(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.ucbx(%s);}", new Object[]{str}));
    }

    /* access modifiers changed from: 0000 */
    public void c(b bVar) {
        Throwable th = null;
        if (!this.g) {
            try {
                if (d("stopTracking")) {
                    String str = "JavaScriptBridge";
                    try {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Ending tracking on tracker");
                        sb.append(bVar.e);
                        o.a(3, str, (Object) this, sb.toString());
                        g(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.egpw(\"%s\");}", new Object[]{bVar.e}));
                    } catch (Exception e2) {
                        o.a("JavaScriptBridge", (Object) this, "Failed to end impression.", (Throwable) e2);
                    }
                }
            } catch (m e3) {
                th = e3;
            }
            if (this.n == a.NATIVE_DISPLAY) {
                d(bVar);
            } else {
                b();
            }
            this.i.remove(bVar);
        }
        if (th != null) {
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(String str) {
        o.a(3, "JavaScriptBridge", (Object) this, "flushDispatchQueue");
        if (this.j.size() >= 200) {
            LinkedList linkedList = new LinkedList();
            for (int i2 = 0; i2 < 10; i2++) {
                linkedList.addFirst((String) this.j.removeFirst());
            }
            int min = Math.min(Math.min(this.j.size() / Callback.DEFAULT_DRAG_ANIMATION_DURATION, 10) + Callback.DEFAULT_DRAG_ANIMATION_DURATION, this.j.size());
            for (int i3 = 0; i3 < min; i3++) {
                this.j.removeFirst();
            }
            Iterator it = linkedList.iterator();
            while (it.hasNext()) {
                this.j.addFirst((String) it.next());
            }
        }
        if (!this.j.isEmpty()) {
            String str2 = "javascript:%s.dispatchMany([%s])";
            StringBuilder sb = new StringBuilder();
            String str3 = "";
            int i4 = 1;
            while (!this.j.isEmpty() && i4 < 200) {
                i4++;
                String str4 = (String) this.j.removeFirst();
                if (sb.length() + str4.length() > 2000) {
                    break;
                }
                sb.append(str3);
                sb.append(str4);
                str3 = ",";
            }
            g(String.format(str2, new Object[]{str, sb.toString()}));
        }
        this.j.clear();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            super.finalize();
            o.a(3, "JavaScriptBridge", (Object) this, "finalize");
            b();
        } catch (Exception e2) {
            m.a(e2);
        }
    }
}
