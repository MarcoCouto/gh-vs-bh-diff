package com.moat.analytics.mobile.vng;

import android.os.Build.VERSION;
import android.util.Base64;
import android.util.Log;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import java.net.URLEncoder;
import java.util.Locale;

class m extends Exception {

    /* renamed from: a reason: collision with root package name */
    private static final Long f3166a = Long.valueOf(ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
    private static Long b;
    private static Exception c = null;

    m(String str) {
        super(str);
    }

    static String a(String str, Exception exc) {
        if (exc instanceof m) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" failed: ");
            sb.append(exc.getMessage());
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(" failed unexpectedly");
        return sb2.toString();
    }

    static void a() {
        if (c != null) {
            b(c);
            c = null;
        }
    }

    static void a(Exception exc) {
        if (v.a().b) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            b(exc);
        }
    }

    private static void b(Exception exc) {
        String str;
        String str2;
        String str3;
        Long valueOf;
        try {
            if (v.a().f3176a == d.ON) {
                int i = v.a().e;
                if (i != 0) {
                    if (i < 100) {
                        double d = (double) i;
                        Double.isNaN(d);
                        if (d / 100.0d < Math.random()) {
                            return;
                        }
                    }
                    String str4 = "";
                    String str5 = "";
                    String str6 = "";
                    String str7 = "";
                    StringBuilder sb = new StringBuilder("https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1");
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("&zt=");
                    sb2.append(exc instanceof m ? 1 : 0);
                    sb.append(sb2.toString());
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("&zr=");
                    sb3.append(i);
                    sb.append(sb3.toString());
                    try {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("&zm=");
                        sb4.append(exc.getMessage() == null ? "null" : URLEncoder.encode(Base64.encodeToString(exc.getMessage().getBytes("UTF-8"), 0), "UTF-8"));
                        sb.append(sb4.toString());
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("&k=");
                        sb5.append(URLEncoder.encode(Base64.encodeToString(Log.getStackTraceString(exc).getBytes("UTF-8"), 0), "UTF-8"));
                        sb.append(sb5.toString());
                    } catch (Exception unused) {
                    }
                    String str8 = "VNG";
                    try {
                        sb.append("&zMoatMMAKv=fd28fb8353d87dc1a1db3246752e21ccc3328cbf");
                        str3 = "2.5.1";
                        try {
                            a d2 = q.d();
                            StringBuilder sb6 = new StringBuilder();
                            sb6.append("&zMoatMMAKan=");
                            sb6.append(d2.a());
                            sb.append(sb6.toString());
                            str2 = d2.b();
                            try {
                                str = Integer.toString(VERSION.SDK_INT);
                            } catch (Exception unused2) {
                                str = str7;
                                StringBuilder sb7 = new StringBuilder();
                                sb7.append("&d=Android:");
                                sb7.append(str8);
                                sb7.append(":");
                                sb7.append(str2);
                                sb7.append(":-");
                                sb.append(sb7.toString());
                                StringBuilder sb8 = new StringBuilder();
                                sb8.append("&bo=");
                                sb8.append(str3);
                                sb.append(sb8.toString());
                                StringBuilder sb9 = new StringBuilder();
                                sb9.append("&bd=");
                                sb9.append(str);
                                sb.append(sb9.toString());
                                valueOf = Long.valueOf(System.currentTimeMillis());
                                StringBuilder sb10 = new StringBuilder();
                                sb10.append("&t=");
                                sb10.append(valueOf);
                                sb.append(sb10.toString());
                                StringBuilder sb11 = new StringBuilder();
                                sb11.append("&de=");
                                sb11.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                                sb.append(sb11.toString());
                                sb.append("&cs=0");
                                p.b(sb.toString());
                                b = valueOf;
                            }
                        } catch (Exception unused3) {
                            str2 = str5;
                            str = str7;
                            StringBuilder sb72 = new StringBuilder();
                            sb72.append("&d=Android:");
                            sb72.append(str8);
                            sb72.append(":");
                            sb72.append(str2);
                            sb72.append(":-");
                            sb.append(sb72.toString());
                            StringBuilder sb82 = new StringBuilder();
                            sb82.append("&bo=");
                            sb82.append(str3);
                            sb.append(sb82.toString());
                            StringBuilder sb92 = new StringBuilder();
                            sb92.append("&bd=");
                            sb92.append(str);
                            sb.append(sb92.toString());
                            valueOf = Long.valueOf(System.currentTimeMillis());
                            StringBuilder sb102 = new StringBuilder();
                            sb102.append("&t=");
                            sb102.append(valueOf);
                            sb.append(sb102.toString());
                            StringBuilder sb112 = new StringBuilder();
                            sb112.append("&de=");
                            sb112.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                            sb.append(sb112.toString());
                            sb.append("&cs=0");
                            p.b(sb.toString());
                            b = valueOf;
                        }
                    } catch (Exception unused4) {
                        str2 = str5;
                        str3 = str6;
                        str = str7;
                        StringBuilder sb722 = new StringBuilder();
                        sb722.append("&d=Android:");
                        sb722.append(str8);
                        sb722.append(":");
                        sb722.append(str2);
                        sb722.append(":-");
                        sb.append(sb722.toString());
                        StringBuilder sb822 = new StringBuilder();
                        sb822.append("&bo=");
                        sb822.append(str3);
                        sb.append(sb822.toString());
                        StringBuilder sb922 = new StringBuilder();
                        sb922.append("&bd=");
                        sb922.append(str);
                        sb.append(sb922.toString());
                        valueOf = Long.valueOf(System.currentTimeMillis());
                        StringBuilder sb1022 = new StringBuilder();
                        sb1022.append("&t=");
                        sb1022.append(valueOf);
                        sb.append(sb1022.toString());
                        StringBuilder sb1122 = new StringBuilder();
                        sb1122.append("&de=");
                        sb1122.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                        sb.append(sb1122.toString());
                        sb.append("&cs=0");
                        p.b(sb.toString());
                        b = valueOf;
                    }
                    StringBuilder sb7222 = new StringBuilder();
                    sb7222.append("&d=Android:");
                    sb7222.append(str8);
                    sb7222.append(":");
                    sb7222.append(str2);
                    sb7222.append(":-");
                    sb.append(sb7222.toString());
                    StringBuilder sb8222 = new StringBuilder();
                    sb8222.append("&bo=");
                    sb8222.append(str3);
                    sb.append(sb8222.toString());
                    StringBuilder sb9222 = new StringBuilder();
                    sb9222.append("&bd=");
                    sb9222.append(str);
                    sb.append(sb9222.toString());
                    valueOf = Long.valueOf(System.currentTimeMillis());
                    StringBuilder sb10222 = new StringBuilder();
                    sb10222.append("&t=");
                    sb10222.append(valueOf);
                    sb.append(sb10222.toString());
                    StringBuilder sb11222 = new StringBuilder();
                    sb11222.append("&de=");
                    sb11222.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                    sb.append(sb11222.toString());
                    sb.append("&cs=0");
                    if (b == null || valueOf.longValue() - b.longValue() > f3166a.longValue()) {
                        p.b(sb.toString());
                        b = valueOf;
                    }
                }
            } else {
                c = exc;
            }
        } catch (Exception unused5) {
        }
    }
}
