package com.moat.analytics.mobile.vng;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import java.lang.ref.WeakReference;

class k extends MoatAnalytics implements b {

    /* renamed from: a reason: collision with root package name */
    boolean f3164a = false;
    boolean b = false;
    @Nullable
    g c;
    WeakReference<Context> d;
    private boolean e = false;
    private String f;
    private MoatOptions g;

    k() {
    }

    private void a(MoatOptions moatOptions, Application application) {
        if (this.e) {
            o.a(3, "Analytics", (Object) this, "Moat SDK has already been started.");
            return;
        }
        this.g = moatOptions;
        v.a().b();
        if (application != null) {
            if (moatOptions.loggingEnabled && q.b(application.getApplicationContext())) {
                this.f3164a = true;
            }
            this.d = new WeakReference<>(application.getApplicationContext());
            this.e = true;
            this.b = moatOptions.autoTrackGMAInterstitials;
            a.a(application);
            v.a().a((b) this);
            if (!moatOptions.disableAdIdCollection) {
                q.a((Context) application);
            }
            o.a("[SUCCESS] ", "Moat Analytics SDK Version 2.5.1 started");
            return;
        }
        throw new m("Moat Analytics SDK didn't start, application was null");
    }

    @UiThread
    private void d() {
        if (this.c == null) {
            this.c = new g(a.a(), a.DISPLAY);
            this.c.a(this.f);
            StringBuilder sb = new StringBuilder();
            sb.append("Preparing native display tracking with partner code ");
            sb.append(this.f);
            o.a(3, "Analytics", (Object) this, sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Prepared for native display tracking with partner code ");
            sb2.append(this.f);
            o.a("[SUCCESS] ", sb2.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.e;
    }

    public void b() {
        m.a();
        if (this.f != null) {
            try {
                d();
            } catch (Exception e2) {
                m.a(e2);
            }
        }
    }

    public void c() {
    }

    @UiThread
    public void prepareNativeDisplayTracking(String str) {
        this.f = str;
        if (v.a().f3176a != d.OFF) {
            try {
                d();
            } catch (Exception e2) {
                m.a(e2);
            }
        }
    }

    public void start(Application application) {
        start(new MoatOptions(), application);
    }

    public void start(MoatOptions moatOptions, Application application) {
        try {
            a(moatOptions, application);
        } catch (Exception e2) {
            m.a(e2);
        }
    }
}
