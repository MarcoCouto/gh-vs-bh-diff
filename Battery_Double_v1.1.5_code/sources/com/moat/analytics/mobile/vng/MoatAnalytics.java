package com.moat.analytics.mobile.vng;

import android.app.Application;
import android.support.annotation.UiThread;
import com.moat.analytics.mobile.vng.u.a;

public abstract class MoatAnalytics {

    /* renamed from: a reason: collision with root package name */
    private static MoatAnalytics f3139a;

    public static synchronized MoatAnalytics getInstance() {
        MoatAnalytics moatAnalytics;
        synchronized (MoatAnalytics.class) {
            if (f3139a == null) {
                try {
                    f3139a = new k();
                } catch (Exception e) {
                    m.a(e);
                    f3139a = new a();
                }
            }
            moatAnalytics = f3139a;
        }
        return moatAnalytics;
    }

    @UiThread
    public abstract void prepareNativeDisplayTracking(String str);

    public abstract void start(Application application);

    public abstract void start(MoatOptions moatOptions, Application application);
}
