package com.moat.analytics.mobile.vng;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class v {
    private static v h;
    /* access modifiers changed from: private */
    public static final Queue<c> i = new ConcurrentLinkedQueue();

    /* renamed from: a reason: collision with root package name */
    volatile d f3176a = d.OFF;
    volatile boolean b = false;
    volatile boolean c = false;
    volatile int d = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
    volatile int e = 10;
    private long f = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;
    /* access modifiers changed from: private */
    public long g = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
    /* access modifiers changed from: private */
    public Handler j;
    /* access modifiers changed from: private */
    public final AtomicBoolean k = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public volatile long l = 0;
    /* access modifiers changed from: private */
    public final AtomicInteger m = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean(false);

    private class a implements Runnable {
        private final Handler b;
        private final String c;
        /* access modifiers changed from: private */
        public final e d;

        private a(String str, Handler handler, e eVar) {
            this.d = eVar;
            this.b = handler;
            StringBuilder sb = new StringBuilder();
            sb.append("https://z.moatads.com/");
            sb.append(str);
            sb.append("/android/");
            sb.append("fd28fb8353d87dc1a1db3246752e21ccc3328cbf".substring(0, 7));
            sb.append("/status.json");
            this.c = sb.toString();
        }

        private void a() {
            String b2 = b();
            final l lVar = new l(b2);
            v.this.b = lVar.a();
            v.this.c = lVar.b();
            v.this.d = lVar.c();
            v.this.e = lVar.d();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    try {
                        a.this.d.a(lVar);
                    } catch (Exception e) {
                        m.a(e);
                    }
                }
            });
            v.this.l = System.currentTimeMillis();
            v.this.n.compareAndSet(true, false);
            if (b2 != null) {
                v.this.m.set(0);
            } else if (v.this.m.incrementAndGet() < 10) {
                v.this.a(v.this.g);
            }
        }

        private String b() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.c);
            sb.append("?ts=");
            sb.append(System.currentTimeMillis());
            sb.append("&v=");
            sb.append("2.5.1");
            try {
                return (String) p.a(sb.toString()).b();
            } catch (Exception unused) {
                return null;
            }
        }

        public void run() {
            try {
                a();
            } catch (Exception e) {
                m.a(e);
            }
            this.b.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    interface b {
        void b();

        void c();
    }

    private class c {

        /* renamed from: a reason: collision with root package name */
        final Long f3182a;
        final b b;

        c(Long l, b bVar) {
            this.f3182a = l;
            this.b = bVar;
        }
    }

    enum d {
        OFF,
        ON
    }

    interface e {
        void a(l lVar);
    }

    private v() {
        try {
            this.j = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            m.a(e2);
        }
    }

    static synchronized v a() {
        v vVar;
        synchronized (v.class) {
            if (h == null) {
                h = new v();
            }
            vVar = h;
        }
        return vVar;
    }

    /* access modifiers changed from: private */
    public void a(final long j2) {
        if (this.n.compareAndSet(false, true)) {
            o.a(3, "OnOff", (Object) this, "Performing status check.");
            new Thread() {
                public void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    a aVar = new a("VNG", handler, new e() {
                        public void a(l lVar) {
                            synchronized (v.i) {
                                boolean z = ((k) MoatAnalytics.getInstance()).f3164a;
                                if (v.this.f3176a != lVar.e() || (v.this.f3176a == d.OFF && z)) {
                                    v.this.f3176a = lVar.e();
                                    if (v.this.f3176a == d.OFF && z) {
                                        v.this.f3176a = d.ON;
                                    }
                                    if (v.this.f3176a == d.ON) {
                                        o.a(3, "OnOff", (Object) this, "Moat enabled - Version 2.5.1");
                                    }
                                    for (c cVar : v.i) {
                                        if (v.this.f3176a == d.ON) {
                                            cVar.b.b();
                                        } else {
                                            cVar.b.c();
                                        }
                                    }
                                }
                                while (!v.i.isEmpty()) {
                                    v.i.remove();
                                }
                            }
                        }
                    });
                    handler.postDelayed(aVar, j2);
                    Looper.loop();
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        synchronized (i) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator it = i.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - ((c) it.next()).f3182a.longValue() >= ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS) {
                    it.remove();
                }
            }
            if (i.size() >= 15) {
                for (int i2 = 0; i2 < 5; i2++) {
                    i.remove();
                }
            }
        }
    }

    private void e() {
        if (this.k.compareAndSet(false, true)) {
            this.j.postDelayed(new Runnable() {
                public void run() {
                    try {
                        if (v.i.size() > 0) {
                            v.this.d();
                            v.this.j.postDelayed(this, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                            return;
                        }
                        v.this.k.compareAndSet(true, false);
                        v.this.j.removeCallbacks(this);
                    } catch (Exception e) {
                        m.a(e);
                    }
                }
            }, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(b bVar) {
        if (this.f3176a == d.ON) {
            bVar.b();
            return;
        }
        d();
        i.add(new c(Long.valueOf(System.currentTimeMillis()), bVar));
        e();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (System.currentTimeMillis() - this.l > this.f) {
            a(0);
        }
    }
}
