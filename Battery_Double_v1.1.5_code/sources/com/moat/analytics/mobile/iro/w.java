package com.moat.analytics.mobile.iro;

import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class w extends d implements ReactiveVideoTracker {

    /* renamed from: ˏॱ reason: contains not printable characters */
    private Integer f427;

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final String m396() {
        return "ReactiveVideoTracker";
    }

    public w(String str) {
        super(str);
        b.m223(3, "ReactiveVideoTracker", this, "Initializing.");
        b.m221("[SUCCESS] ", "ReactiveVideoTracker created");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊॱ reason: contains not printable characters */
    public final Map<String, Object> m397() throws o {
        HashMap hashMap = new HashMap();
        View view = (View) this.f268.get();
        Integer valueOf = Integer.valueOf(0);
        Integer valueOf2 = Integer.valueOf(0);
        if (view != null) {
            valueOf = Integer.valueOf(view.getWidth());
            valueOf2 = Integer.valueOf(view.getHeight());
        }
        hashMap.put(IronSourceConstants.EVENTS_DURATION, this.f427);
        hashMap.put("width", valueOf);
        hashMap.put("height", valueOf2);
        return hashMap;
    }

    public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            m232();
            m235();
            this.f427 = num;
            return super.m244(map, view);
        } catch (Exception e) {
            m237("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final JSONObject m398(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.f232 == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.f230.equals(MoatAdEvent.f227) && !m240(moatAdEvent.f230, this.f427)) {
            moatAdEvent.f232 = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.m243(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final void m399(List<String> list) throws o {
        if (this.f427.intValue() >= 1000) {
            super.m246(list);
            return;
        }
        throw new o(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", new Object[]{this.f427}));
    }
}
