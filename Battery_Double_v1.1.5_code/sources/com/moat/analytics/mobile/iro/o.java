package com.moat.analytics.mobile.iro;

import android.os.Build.VERSION;
import android.util.Base64;
import android.util.Log;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import java.net.URLEncoder;
import java.util.Locale;

final class o extends Exception {

    /* renamed from: ˋ reason: contains not printable characters */
    private static Exception f365 = null;

    /* renamed from: ˎ reason: contains not printable characters */
    private static Long f366;

    /* renamed from: ˏ reason: contains not printable characters */
    private static final Long f367 = Long.valueOf(ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);

    o(String str) {
        super(str);
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static String m338(String str, Exception exc) {
        if (exc instanceof o) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" failed: ");
            sb.append(exc.getMessage());
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(" failed unexpectedly");
        return sb2.toString();
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static void m339(Exception exc) {
        if (t.m378().f404) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            m337(exc);
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static void m337(Exception exc) {
        String str;
        String str2;
        String str3;
        Long valueOf;
        try {
            if (t.m378().f402 == c.f419) {
                int i = t.m378().f400;
                if (i != 0) {
                    if (i < 100) {
                        double d = (double) i;
                        Double.isNaN(d);
                        if (d / 100.0d < Math.random()) {
                            return;
                        }
                    }
                    String str4 = "";
                    String str5 = "";
                    String str6 = "";
                    String str7 = "";
                    StringBuilder sb = new StringBuilder("https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1");
                    StringBuilder sb2 = new StringBuilder("&zt=");
                    sb2.append(exc instanceof o ? 1 : 0);
                    sb.append(sb2.toString());
                    StringBuilder sb3 = new StringBuilder("&zr=");
                    sb3.append(i);
                    sb.append(sb3.toString());
                    try {
                        StringBuilder sb4 = new StringBuilder("&zm=");
                        sb4.append(exc.getMessage() == null ? "null" : URLEncoder.encode(Base64.encodeToString(exc.getMessage().getBytes("UTF-8"), 0), "UTF-8"));
                        sb.append(sb4.toString());
                        StringBuilder sb5 = new StringBuilder("&k=");
                        sb5.append(URLEncoder.encode(Base64.encodeToString(Log.getStackTraceString(exc).getBytes("UTF-8"), 0), "UTF-8"));
                        sb.append(sb5.toString());
                    } catch (Exception unused) {
                    }
                    String str8 = "IRO";
                    try {
                        sb.append("&zMoatMMAKv=2bc3418b93f01686fcbd1ebebcc04694651821b2");
                        str3 = "2.4.0";
                        try {
                            b r1 = p.m346();
                            StringBuilder sb6 = new StringBuilder("&zMoatMMAKan=");
                            sb6.append(r1.m354());
                            sb.append(sb6.toString());
                            str2 = r1.m353();
                        } catch (Exception unused2) {
                            str2 = str5;
                            str = str7;
                            StringBuilder sb7 = new StringBuilder("&d=Android:");
                            sb7.append(str8);
                            sb7.append(":");
                            sb7.append(str2);
                            sb7.append(":-");
                            sb.append(sb7.toString());
                            StringBuilder sb8 = new StringBuilder("&bo=");
                            sb8.append(str3);
                            sb.append(sb8.toString());
                            StringBuilder sb9 = new StringBuilder("&bd=");
                            sb9.append(str);
                            sb.append(sb9.toString());
                            valueOf = Long.valueOf(System.currentTimeMillis());
                            StringBuilder sb10 = new StringBuilder("&t=");
                            sb10.append(valueOf);
                            sb.append(sb10.toString());
                            StringBuilder sb11 = new StringBuilder("&de=");
                            sb11.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                            sb.append(sb11.toString());
                            sb.append("&cs=0");
                            new Thread(sb.toString()) {

                                /* renamed from: ˎ reason: contains not printable characters */
                                private /* synthetic */ String f355;

                                {
                                    this.f355 = r1;
                                }

                                public final void run() {
                                    try {
                                        l.m331(this.f355);
                                    } catch (Exception unused) {
                                    }
                                }
                            }.start();
                            f366 = valueOf;
                            return;
                        }
                        try {
                            str = Integer.toString(VERSION.SDK_INT);
                        } catch (Exception unused3) {
                            str = str7;
                            StringBuilder sb72 = new StringBuilder("&d=Android:");
                            sb72.append(str8);
                            sb72.append(":");
                            sb72.append(str2);
                            sb72.append(":-");
                            sb.append(sb72.toString());
                            StringBuilder sb82 = new StringBuilder("&bo=");
                            sb82.append(str3);
                            sb.append(sb82.toString());
                            StringBuilder sb92 = new StringBuilder("&bd=");
                            sb92.append(str);
                            sb.append(sb92.toString());
                            valueOf = Long.valueOf(System.currentTimeMillis());
                            StringBuilder sb102 = new StringBuilder("&t=");
                            sb102.append(valueOf);
                            sb.append(sb102.toString());
                            StringBuilder sb112 = new StringBuilder("&de=");
                            sb112.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                            sb.append(sb112.toString());
                            sb.append("&cs=0");
                            new Thread(sb.toString()) {

                                /* renamed from: ˎ reason: contains not printable characters */
                                private /* synthetic */ String f355;

                                {
                                    this.f355 = r1;
                                }

                                public final void run() {
                                    try {
                                        l.m331(this.f355);
                                    } catch (Exception unused) {
                                    }
                                }
                            }.start();
                            f366 = valueOf;
                            return;
                        }
                    } catch (Exception unused4) {
                        str2 = str5;
                        str3 = str6;
                        str = str7;
                        StringBuilder sb722 = new StringBuilder("&d=Android:");
                        sb722.append(str8);
                        sb722.append(":");
                        sb722.append(str2);
                        sb722.append(":-");
                        sb.append(sb722.toString());
                        StringBuilder sb822 = new StringBuilder("&bo=");
                        sb822.append(str3);
                        sb.append(sb822.toString());
                        StringBuilder sb922 = new StringBuilder("&bd=");
                        sb922.append(str);
                        sb.append(sb922.toString());
                        valueOf = Long.valueOf(System.currentTimeMillis());
                        StringBuilder sb1022 = new StringBuilder("&t=");
                        sb1022.append(valueOf);
                        sb.append(sb1022.toString());
                        StringBuilder sb1122 = new StringBuilder("&de=");
                        sb1122.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                        sb.append(sb1122.toString());
                        sb.append("&cs=0");
                        new Thread(sb.toString()) {

                            /* renamed from: ˎ reason: contains not printable characters */
                            private /* synthetic */ String f355;

                            {
                                this.f355 = r1;
                            }

                            public final void run() {
                                try {
                                    l.m331(this.f355);
                                } catch (Exception unused) {
                                }
                            }
                        }.start();
                        f366 = valueOf;
                        return;
                    }
                    StringBuilder sb7222 = new StringBuilder("&d=Android:");
                    sb7222.append(str8);
                    sb7222.append(":");
                    sb7222.append(str2);
                    sb7222.append(":-");
                    sb.append(sb7222.toString());
                    StringBuilder sb8222 = new StringBuilder("&bo=");
                    sb8222.append(str3);
                    sb.append(sb8222.toString());
                    StringBuilder sb9222 = new StringBuilder("&bd=");
                    sb9222.append(str);
                    sb.append(sb9222.toString());
                    valueOf = Long.valueOf(System.currentTimeMillis());
                    StringBuilder sb10222 = new StringBuilder("&t=");
                    sb10222.append(valueOf);
                    sb.append(sb10222.toString());
                    StringBuilder sb11222 = new StringBuilder("&de=");
                    sb11222.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                    sb.append(sb11222.toString());
                    sb.append("&cs=0");
                    if (f366 == null || valueOf.longValue() - f366.longValue() > f367.longValue()) {
                        new Thread(sb.toString()) {

                            /* renamed from: ˎ reason: contains not printable characters */
                            private /* synthetic */ String f355;

                            {
                                this.f355 = r1;
                            }

                            public final void run() {
                                try {
                                    l.m331(this.f355);
                                } catch (Exception unused) {
                                }
                            }
                        }.start();
                        f366 = valueOf;
                    }
                    return;
                }
                return;
            }
            f365 = exc;
        } catch (Exception unused5) {
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static void m336() {
        if (f365 != null) {
            m337(f365);
            f365 = null;
        }
    }
}
