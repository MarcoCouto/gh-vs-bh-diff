package com.moat.analytics.mobile.iro;

import android.graphics.Rect;
import android.view.View;
import com.moat.analytics.mobile.iro.NativeDisplayTracker.MoatUserInteractionType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class r extends c implements NativeDisplayTracker {

    /* renamed from: ʻ reason: contains not printable characters */
    private final Set<MoatUserInteractionType> f384 = new HashSet();

    /* renamed from: ʼ reason: contains not printable characters */
    private final Map<String, String> f385;

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final String m365() {
        return "NativeDisplayTracker";
    }

    r(View view, Map<String, String> map) {
        super(view, true, false);
        b.m223(3, "NativeDisplayTracker", this, "Initializing.");
        this.f385 = map;
        if (view == null) {
            String str = "Target view is null";
            StringBuilder sb = new StringBuilder("NativeDisplayTracker initialization not successful, ");
            sb.append(str);
            String sb2 = sb.toString();
            b.m223(3, "NativeDisplayTracker", this, sb2);
            b.m221("[ERROR] ", sb2);
            this.f260 = new o(str);
        } else if (map == null || map.isEmpty()) {
            StringBuilder sb3 = new StringBuilder("NativeDisplayTracker initialization not successful, ");
            sb3.append("AdIds is null or empty");
            String sb4 = sb3.toString();
            b.m223(3, "NativeDisplayTracker", this, sb4);
            b.m221("[ERROR] ", sb4);
            this.f260 = new o("AdIds is null or empty");
        } else {
            b bVar = ((j) j.getInstance()).f338;
            if (bVar == null) {
                String str2 = "prepareNativeDisplayTracking was not called successfully";
                StringBuilder sb5 = new StringBuilder("NativeDisplayTracker initialization not successful, ");
                sb5.append(str2);
                String sb6 = sb5.toString();
                b.m223(3, "NativeDisplayTracker", this, sb6);
                b.m221("[ERROR] ", sb6);
                this.f260 = new o(str2);
                return;
            }
            this.f262 = bVar.f245;
            try {
                super.m233(bVar.f247);
                if (this.f262 != null) {
                    this.f262.m273(m364());
                }
                StringBuilder sb7 = new StringBuilder("NativeDisplayTracker created for ");
                sb7.append(m230());
                sb7.append(", with adIds:");
                sb7.append(map.toString());
                b.m221("[SUCCESS] ", sb7.toString());
            } catch (o e) {
                this.f260 = e;
            }
        }
    }

    public final void reportUserInteractionEvent(MoatUserInteractionType moatUserInteractionType) {
        String str = "NativeDisplayTracker";
        try {
            StringBuilder sb = new StringBuilder("reportUserInteractionEvent:");
            sb.append(moatUserInteractionType.name());
            b.m223(3, str, this, sb.toString());
            if (!this.f384.contains(moatUserInteractionType)) {
                this.f384.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.f261);
                jSONObject.accumulate("event", moatUserInteractionType.name().toLowerCase());
                if (this.f262 != null) {
                    this.f262.m275(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            b.m224("NativeDisplayTracker", this, "Got JSON exception");
            o.m339(e);
        } catch (Exception e2) {
            o.m339(e2);
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private String m364() {
        String str = "";
        try {
            Map<String, String> map = this.f385;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 0; i < 8; i++) {
                StringBuilder sb = new StringBuilder("moatClientLevel");
                sb.append(i);
                String sb2 = sb.toString();
                if (map.containsKey(sb2)) {
                    linkedHashMap.put(sb2, map.get(sb2));
                }
            }
            for (int i2 = 0; i2 < 8; i2++) {
                StringBuilder sb3 = new StringBuilder("moatClientSlicer");
                sb3.append(i2);
                String sb4 = sb3.toString();
                if (map.containsKey(sb4)) {
                    linkedHashMap.put(sb4, map.get(sb4));
                }
            }
            for (String str2 : map.keySet()) {
                if (!linkedHashMap.containsKey(str2)) {
                    linkedHashMap.put(str2, (String) map.get(str2));
                }
            }
            String jSONObject = new JSONObject(linkedHashMap).toString();
            StringBuilder sb5 = new StringBuilder("Parsed ad ids = ");
            sb5.append(jSONObject);
            b.m223(3, "NativeDisplayTracker", this, sb5.toString());
            StringBuilder sb6 = new StringBuilder("{\"adIds\":");
            sb6.append(jSONObject);
            sb6.append(", \"adKey\":\"");
            sb6.append(this.f261);
            sb6.append("\", \"adSize\":");
            sb6.append(m363());
            sb6.append("}");
            return sb6.toString();
        } catch (Exception e) {
            o.m339(e);
            return str;
        }
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    private String m363() {
        try {
            Rect r0 = y.m404(super.m228());
            int width = r0.width();
            int height = r0.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            o.m339(e);
            return null;
        }
    }
}
