package com.moat.analytics.mobile.iro;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.iro.base.functional.Optional;
import java.lang.ref.WeakReference;
import java.util.Map;

final class n extends MoatFactory {
    n() throws o {
        if (!((j) j.getInstance()).m308()) {
            String str = "Failed to initialize MoatFactory";
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(", SDK was not started");
            String sb2 = sb.toString();
            b.m223(3, "Factory", this, sb2);
            b.m221("[ERROR] ", sb2);
            throw new o(str);
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull WebView webView) {
        try {
            final WeakReference weakReference = new WeakReference(webView);
            return (WebAdTracker) s.m367(new a<WebAdTracker>() {
                /* renamed from: ˏ reason: contains not printable characters */
                public final Optional<WebAdTracker> m333() {
                    WebView webView = (WebView) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create WebAdTracker for ");
                    sb.append(b.m225(webView));
                    String sb2 = sb.toString();
                    b.m223(3, "Factory", this, sb2);
                    b.m221("[INFO] ", sb2);
                    return Optional.of(new x(webView));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m339(e);
            return new b();
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull ViewGroup viewGroup) {
        try {
            final WeakReference weakReference = new WeakReference(viewGroup);
            return (WebAdTracker) s.m367(new a<WebAdTracker>() {
                /* renamed from: ˏ reason: contains not printable characters */
                public final Optional<WebAdTracker> m334() throws o {
                    ViewGroup viewGroup = (ViewGroup) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create WebAdTracker for adContainer ");
                    sb.append(b.m225(viewGroup));
                    String sb2 = sb.toString();
                    b.m223(3, "Factory", this, sb2);
                    b.m221("[INFO] ", sb2);
                    return Optional.of(new x(viewGroup));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m339(e);
            return new b();
        }
    }

    public final NativeDisplayTracker createNativeDisplayTracker(@NonNull View view, @NonNull final Map<String, String> map) {
        try {
            final WeakReference weakReference = new WeakReference(view);
            return (NativeDisplayTracker) s.m367(new a<NativeDisplayTracker>() {
                /* renamed from: ˏ reason: contains not printable characters */
                public final Optional<NativeDisplayTracker> m335() {
                    View view = (View) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create NativeDisplayTracker for ");
                    sb.append(b.m225(view));
                    String sb2 = sb.toString();
                    b.m223(3, "Factory", this, sb2);
                    b.m221("[INFO] ", sb2);
                    return Optional.of(new r(view, map));
                }
            }, NativeDisplayTracker.class);
        } catch (Exception e) {
            o.m339(e);
            return new a();
        }
    }

    public final NativeVideoTracker createNativeVideoTracker(final String str) {
        try {
            return (NativeVideoTracker) s.m367(new a<NativeVideoTracker>() {
                /* renamed from: ˏ reason: contains not printable characters */
                public final Optional<NativeVideoTracker> m332() {
                    String str = "Attempting to create NativeVideoTracker";
                    b.m223(3, "Factory", this, str);
                    b.m221("[INFO] ", str);
                    return Optional.of(new q(str));
                }
            }, NativeVideoTracker.class);
        } catch (Exception e) {
            o.m339(e);
            return new c();
        }
    }

    public final <T> T createCustomTracker(m<T> mVar) {
        try {
            return mVar.create();
        } catch (Exception e) {
            o.m339(e);
            return mVar.createNoOp();
        }
    }
}
