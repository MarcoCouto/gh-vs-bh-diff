package com.moat.analytics.mobile.iro;

import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf(Utils.DOUBLE_EPSILON);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: ˋ reason: contains not printable characters */
    private static final Double f226 = Double.valueOf(Double.NaN);

    /* renamed from: ˏ reason: contains not printable characters */
    static final Integer f227 = Integer.valueOf(Integer.MIN_VALUE);

    /* renamed from: ʼ reason: contains not printable characters */
    private final Long f228;

    /* renamed from: ʽ reason: contains not printable characters */
    private final Double f229;

    /* renamed from: ˊ reason: contains not printable characters */
    Integer f230;

    /* renamed from: ˎ reason: contains not printable characters */
    Double f231;

    /* renamed from: ॱ reason: contains not printable characters */
    MoatAdEventType f232;

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d) {
        this.f228 = Long.valueOf(System.currentTimeMillis());
        this.f232 = moatAdEventType;
        this.f231 = d;
        this.f230 = num;
        this.f229 = Double.valueOf(p.m350());
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, f226);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f227, f226);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final Map<String, Object> m208() {
        HashMap hashMap = new HashMap();
        hashMap.put("adVolume", this.f231);
        hashMap.put("playhead", this.f230);
        hashMap.put("aTimeStamp", this.f228);
        hashMap.put("type", this.f232.toString());
        hashMap.put(RequestParameters.DEVICE_VOLUME, this.f229);
        return hashMap;
    }
}
