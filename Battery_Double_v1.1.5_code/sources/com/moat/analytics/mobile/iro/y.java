package com.moat.analytics.mobile.iro;

import android.app.Activity;
import android.graphics.Rect;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ListView;
import com.github.mikephil.charting.utils.Utils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

final class y {

    /* renamed from: ˊॱ reason: contains not printable characters */
    private static int f428 = 0;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private static int f429 = 1;

    /* renamed from: ʻ reason: contains not printable characters */
    private Location f430;

    /* renamed from: ʼ reason: contains not printable characters */
    private Map<String, Object> f431 = new HashMap();

    /* renamed from: ʽ reason: contains not printable characters */
    private JSONObject f432;

    /* renamed from: ˊ reason: contains not printable characters */
    private a f433 = new a();

    /* renamed from: ˋ reason: contains not printable characters */
    private Rect f434;

    /* renamed from: ˎ reason: contains not printable characters */
    String f435 = "{}";

    /* renamed from: ˏ reason: contains not printable characters */
    private JSONObject f436;

    /* renamed from: ॱ reason: contains not printable characters */
    private Rect f437;

    /* renamed from: ᐝ reason: contains not printable characters */
    private JSONObject f438;

    static class a {

        /* renamed from: ˊ reason: contains not printable characters */
        Rect f439 = new Rect(0, 0, 0, 0);

        /* renamed from: ˋ reason: contains not printable characters */
        double f440 = Utils.DOUBLE_EPSILON;

        /* renamed from: ˎ reason: contains not printable characters */
        double f441 = Utils.DOUBLE_EPSILON;

        a() {
        }
    }

    static class b {

        /* renamed from: ˊ reason: contains not printable characters */
        final View f442;

        /* renamed from: ˋ reason: contains not printable characters */
        final boolean f443;

        /* renamed from: ॱ reason: contains not printable characters */
        final int f444;

        b(View view, int i, boolean z) {
            this.f442 = view;
            this.f444 = i;
            this.f443 = z;
        }
    }

    y() {
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x012d, code lost:
        if ((!r20.isShown() ? 'W' : '4') != '4') goto L_0x012f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0207, code lost:
        if ((!r4) != true) goto L_0x0215;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0213, code lost:
        if ((r4 ? 'H' : 'Z') != 'Z') goto L_0x0215;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x03a4, code lost:
        if (r14.equals(r1.f437) == false) goto L_0x03a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:292:0x03c8, code lost:
        if (r9.equals(r1.f434) == false) goto L_0x03ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00be, code lost:
        if ((r20.getWindowToken() != null ? 'B' : '&') != '&') goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00dd, code lost:
        if ((r20.isAttachedToWindow() ? '!' : '2') != '!') goto L_0x00df;
     */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x013d  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0149 A[SYNTHETIC, Splitter:B:115:0x0149] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0172  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0177  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x018d A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x018f A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0192 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0194 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x01a0 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x01a2 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x01a5 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x01a7 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x01c3 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x01c5 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x01c8 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x01cd A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x01e3  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x01e8  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x01f1  */
    /* JADX WARNING: Removed duplicated region for block: B:233:0x02f2  */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x02f4  */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x02f7  */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x02fa  */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x037a  */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x037c  */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x037f  */
    /* JADX WARNING: Removed duplicated region for block: B:287:0x03bc A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:288:0x03be A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:290:0x03c1 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x03c2 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:296:0x03e0  */
    /* JADX WARNING: Removed duplicated region for block: B:297:0x03e2  */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x03e6  */
    /* JADX WARNING: Removed duplicated region for block: B:315:0x041e  */
    /* JADX WARNING: Removed duplicated region for block: B:316:0x0420  */
    /* JADX WARNING: Removed duplicated region for block: B:318:0x0423  */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x0432  */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x0434  */
    /* JADX WARNING: Removed duplicated region for block: B:326:0x0439  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009f A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a1 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00a8 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00c4 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00e2 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00e4 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00e7 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00e8 A[Catch:{ Exception -> 0x04f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x00fe  */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m409(String str, View view) {
        DisplayMetrics displayMetrics;
        boolean z;
        boolean z2;
        boolean z3;
        float f;
        int i;
        boolean z4;
        char c;
        Location r0;
        HashMap hashMap;
        JSONObject jSONObject;
        char c2;
        char c3;
        Activity activity;
        View view2 = view;
        HashMap hashMap2 = new HashMap();
        String str2 = "{}";
        if ((view2 != null ? 'N' : 0) == 'N') {
            int i2 = f429 + 31;
            f428 = i2 % 128;
            int i3 = i2 % 2;
            try {
                if (!(VERSION.SDK_INT < 17)) {
                    int i4 = f428 + 93;
                    f429 = i4 % 128;
                    int i5 = i4 % 2;
                    if (!(a.f239 == null)) {
                        int i6 = f429 + 85;
                        f428 = i6 % 128;
                        if (!(i6 % 2 != 0)) {
                            activity = (Activity) a.f239.get();
                            if ((activity != null ? '`' : ';') != ';') {
                            }
                        } else {
                            activity = (Activity) a.f239.get();
                            if ((activity != null ? 'V' : '[') != 'V') {
                            }
                        }
                        displayMetrics = new DisplayMetrics();
                        activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
                        if (!(VERSION.SDK_INT >= 19)) {
                            if ((view2 != null ? (char) 25 : 26) != 26) {
                            }
                            z = false;
                            if (!(view2 == null)) {
                                if (view.hasWindowFocus()) {
                                    z2 = true;
                                    if (!(view2 == null)) {
                                        int i7 = f428 + 69;
                                        f429 = i7 % 128;
                                        if ((i7 % 2 == 0 ? 'B' : '.') != 'B') {
                                            if ((!view.isShown() ? 'E' : 'Y') != 'E') {
                                            }
                                        }
                                        z3 = false;
                                        if ((view2 != null ? '0' : '%') == '%') {
                                            int i8 = f429 + 67;
                                            f428 = i8 % 128;
                                            int i9 = i8 % 2;
                                            f = 0.0f;
                                        } else {
                                            f = m401(view);
                                        }
                                        hashMap2.put("dr", Float.valueOf(displayMetrics.density));
                                        hashMap2.put("dv", Double.valueOf(p.m350()));
                                        hashMap2.put("adKey", str);
                                        String str3 = "isAttached";
                                        if (!z) {
                                            i = 0;
                                        } else {
                                            int i10 = f428 + 77;
                                            f429 = i10 % 128;
                                            int i11 = i10 % 2;
                                            i = 1;
                                        }
                                        hashMap2.put(str3, Integer.valueOf(i));
                                        hashMap2.put("inFocus", Integer.valueOf(!z2 ? 0 : 1));
                                        hashMap2.put("isHidden", Integer.valueOf(!z3 ? 0 : 1));
                                        hashMap2.put("opacity", Float.valueOf(f));
                                        Rect rect = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                        Rect r9 = view2 != null ? m407(view) : new Rect(0, 0, 0, 0);
                                        a aVar = new a();
                                        int width = r9.width() * r9.height();
                                        if ((view2 == null ? '\"' : '7') != '7') {
                                            int i12 = f428 + 45;
                                            f429 = i12 % 128;
                                            if (i12 % 2 == 0) {
                                            }
                                            if (!(!z2)) {
                                                if ((!z3 ? 18 : '[') != '[') {
                                                    int i13 = f429 + 17;
                                                    f428 = i13 % 128;
                                                    int i14 = i13 % 2;
                                                    if (!(width <= 0)) {
                                                        int i15 = f429 + 121;
                                                        f428 = i15 % 128;
                                                        int i16 = i15 % 2;
                                                        Rect rect2 = new Rect(0, 0, 0, 0);
                                                        if (view2.getGlobalVisibleRect(rect2)) {
                                                            int width2 = rect2.width() * rect2.height();
                                                            if (!(width2 >= width)) {
                                                                int i17 = f429 + 39;
                                                                f428 = i17 % 128;
                                                                int i18 = i17 % 2;
                                                                b.m224("VisibilityInfo", null, "Ad is clipped");
                                                            }
                                                            HashSet hashSet = new HashSet();
                                                            if ((view.getRootView() instanceof ViewGroup ? ')' : 'T') == ')') {
                                                                int i19 = f428 + 49;
                                                                f429 = i19 % 128;
                                                                int i20 = i19 % 2;
                                                                aVar.f439 = rect2;
                                                                boolean r02 = m406(rect2, view2, (Set<Rect>) hashSet);
                                                                if ((r02 ? 'Y' : '\\') != '\\') {
                                                                    aVar.f441 = 1.0d;
                                                                }
                                                                if ((!r02 ? (char) 21 : 18) == 21) {
                                                                    int r03 = m402(rect2, hashSet);
                                                                    if ((r03 > 0 ? ']' : 26) == ']') {
                                                                        int i21 = f428 + 87;
                                                                        f429 = i21 % 128;
                                                                        int i22 = i21 % 2;
                                                                        double d = (double) r03;
                                                                        double d2 = (double) width2;
                                                                        Double.isNaN(d);
                                                                        Double.isNaN(d2);
                                                                        aVar.f441 = d / d2;
                                                                    }
                                                                    double d3 = (double) (width2 - r03);
                                                                    double d4 = (double) width;
                                                                    Double.isNaN(d3);
                                                                    Double.isNaN(d4);
                                                                    aVar.f440 = d3 / d4;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (!(this.f436 != null)) {
                                            c = 'B';
                                        } else {
                                            int i23 = f428 + 99;
                                            f429 = i23 % 128;
                                            c = 'B';
                                            if ((i23 % 2 == 0 ? 'B' : 16) != 'B') {
                                                if (aVar.f440 == this.f433.f440) {
                                                }
                                            } else {
                                                if ((aVar.f440 == this.f433.f440 ? 'R' : 3) != 'R') {
                                                }
                                            }
                                            if ((aVar.f439.equals(this.f433.f439) ? 24 : '*') != '*') {
                                                if (aVar.f441 == this.f433.f441) {
                                                    z4 = false;
                                                    hashMap2.put("coveredPercent", Double.valueOf(aVar.f441));
                                                    if (this.f432 != null) {
                                                        int i24 = f428 + 9;
                                                        f429 = i24 % 128;
                                                        if (i24 % 2 == 0) {
                                                            c2 = 7;
                                                            c3 = 'Y';
                                                        } else {
                                                            c3 = 'Y';
                                                            c2 = 'Y';
                                                        }
                                                        if (c2 != c3) {
                                                            if (!rect.equals(this.f437)) {
                                                            }
                                                            if (!(this.f438 != null)) {
                                                            }
                                                            this.f434 = r9;
                                                            this.f438 = new JSONObject(m408(m403(r9, displayMetrics)));
                                                            z4 = true;
                                                            if (!(this.f431 != null)) {
                                                                int i25 = f429 + 99;
                                                                f428 = i25 % 128;
                                                                if (i25 % 2 != 0) {
                                                                    if (!hashMap2.equals(this.f431)) {
                                                                    }
                                                                    r0 = k.m320().m329();
                                                                    if (k.m321(r0, this.f430)) {
                                                                        int i26 = f429 + 103;
                                                                        f428 = i26 % 128;
                                                                        int i27 = i26 % 2;
                                                                        this.f430 = r0;
                                                                        z4 = true;
                                                                    }
                                                                    if (!(z4)) {
                                                                        int i28 = f428 + 57;
                                                                        f429 = i28 % 128;
                                                                        int i29 = i28 % 2;
                                                                        JSONObject jSONObject2 = new JSONObject(this.f431);
                                                                        jSONObject2.accumulate("screen", this.f432);
                                                                        jSONObject2.accumulate(ParametersKeys.VIEW, this.f438);
                                                                        jSONObject2.accumulate(String.VISIBLE, this.f436);
                                                                        jSONObject2.accumulate("maybe", this.f436);
                                                                        jSONObject2.accumulate("visiblePercent", Double.valueOf(this.f433.f440));
                                                                        if (r0 != null) {
                                                                            String str4 = "location";
                                                                            if (r0 != null) {
                                                                                c = 'X';
                                                                            }
                                                                            if (c != 'X') {
                                                                                int i30 = f429 + 95;
                                                                                f428 = i30 % 128;
                                                                                int i31 = i30 % 2;
                                                                                hashMap = null;
                                                                            } else {
                                                                                hashMap = new HashMap();
                                                                                hashMap.put(LocationConst.LATITUDE, Double.toString(r0.getLatitude()));
                                                                                hashMap.put(LocationConst.LONGITUDE, Double.toString(r0.getLongitude()));
                                                                                hashMap.put("timestamp", Long.toString(r0.getTime()));
                                                                                hashMap.put("horizontalAccuracy", Float.toString(r0.getAccuracy()));
                                                                            }
                                                                            if (!(hashMap != null)) {
                                                                                int i32 = f428 + 1;
                                                                                f429 = i32 % 128;
                                                                                int i33 = i32 % 2;
                                                                                jSONObject = null;
                                                                            } else {
                                                                                jSONObject = new JSONObject(hashMap);
                                                                            }
                                                                            jSONObject2.accumulate(str4, jSONObject);
                                                                        }
                                                                        String jSONObject3 = jSONObject2.toString();
                                                                        try {
                                                                            this.f435 = jSONObject3;
                                                                        } catch (Exception e) {
                                                                            e = e;
                                                                            str2 = jSONObject3;
                                                                            o.m339(e);
                                                                            this.f435 = str2;
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (!(!hashMap2.equals(this.f431))) {
                                                                        r0 = k.m320().m329();
                                                                        if (k.m321(r0, this.f430)) {
                                                                        }
                                                                        if (!(z4)) {
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            this.f431 = hashMap2;
                                                            z4 = true;
                                                            r0 = k.m320().m329();
                                                            if (k.m321(r0, this.f430)) {
                                                            }
                                                            if (!(z4)) {
                                                            }
                                                        }
                                                    }
                                                    this.f437 = rect;
                                                    this.f432 = new JSONObject(m408(m403(rect, displayMetrics)));
                                                    z4 = true;
                                                    if (!(this.f438 != null)) {
                                                    }
                                                    this.f434 = r9;
                                                    this.f438 = new JSONObject(m408(m403(r9, displayMetrics)));
                                                    z4 = true;
                                                    if (!(this.f431 != null)) {
                                                    }
                                                    this.f431 = hashMap2;
                                                    z4 = true;
                                                    r0 = k.m320().m329();
                                                    if (k.m321(r0, this.f430)) {
                                                    }
                                                    if (!(z4)) {
                                                    }
                                                }
                                            }
                                        }
                                        this.f433 = aVar;
                                        this.f436 = new JSONObject(m408(m403(this.f433.f439, displayMetrics)));
                                        z4 = true;
                                        hashMap2.put("coveredPercent", Double.valueOf(aVar.f441));
                                        if (this.f432 != null) {
                                        }
                                        this.f437 = rect;
                                        this.f432 = new JSONObject(m408(m403(rect, displayMetrics)));
                                        z4 = true;
                                        if (!(this.f438 != null)) {
                                        }
                                        this.f434 = r9;
                                        this.f438 = new JSONObject(m408(m403(r9, displayMetrics)));
                                        z4 = true;
                                        if (!(this.f431 != null)) {
                                        }
                                        this.f431 = hashMap2;
                                        z4 = true;
                                        r0 = k.m320().m329();
                                        if (k.m321(r0, this.f430)) {
                                        }
                                        if (!(z4)) {
                                        }
                                    }
                                    z3 = true;
                                    if ((view2 != null ? '0' : '%') == '%') {
                                    }
                                    hashMap2.put("dr", Float.valueOf(displayMetrics.density));
                                    hashMap2.put("dv", Double.valueOf(p.m350()));
                                    hashMap2.put("adKey", str);
                                    String str32 = "isAttached";
                                    if (!z) {
                                    }
                                    hashMap2.put(str32, Integer.valueOf(i));
                                    hashMap2.put("inFocus", Integer.valueOf(!z2 ? 0 : 1));
                                    hashMap2.put("isHidden", Integer.valueOf(!z3 ? 0 : 1));
                                    hashMap2.put("opacity", Float.valueOf(f));
                                    Rect rect3 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                    if (view2 != null) {
                                    }
                                    a aVar2 = new a();
                                    int width3 = r9.width() * r9.height();
                                    if ((view2 == null ? '\"' : '7') != '7') {
                                    }
                                    if (!(this.f436 != null)) {
                                    }
                                    this.f433 = aVar2;
                                    this.f436 = new JSONObject(m408(m403(this.f433.f439, displayMetrics)));
                                    z4 = true;
                                    hashMap2.put("coveredPercent", Double.valueOf(aVar2.f441));
                                    if (this.f432 != null) {
                                    }
                                    this.f437 = rect3;
                                    this.f432 = new JSONObject(m408(m403(rect3, displayMetrics)));
                                    z4 = true;
                                    if (!(this.f438 != null)) {
                                    }
                                    this.f434 = r9;
                                    this.f438 = new JSONObject(m408(m403(r9, displayMetrics)));
                                    z4 = true;
                                    if (!(this.f431 != null)) {
                                    }
                                    this.f431 = hashMap2;
                                    z4 = true;
                                    r0 = k.m320().m329();
                                    if (k.m321(r0, this.f430)) {
                                    }
                                    if (!(z4)) {
                                    }
                                }
                            }
                            z2 = false;
                            if (!(view2 == null)) {
                            }
                            z3 = true;
                            if ((view2 != null ? '0' : '%') == '%') {
                            }
                            hashMap2.put("dr", Float.valueOf(displayMetrics.density));
                            hashMap2.put("dv", Double.valueOf(p.m350()));
                            hashMap2.put("adKey", str);
                            String str322 = "isAttached";
                            if (!z) {
                            }
                            hashMap2.put(str322, Integer.valueOf(i));
                            hashMap2.put("inFocus", Integer.valueOf(!z2 ? 0 : 1));
                            hashMap2.put("isHidden", Integer.valueOf(!z3 ? 0 : 1));
                            hashMap2.put("opacity", Float.valueOf(f));
                            Rect rect32 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                            if (view2 != null) {
                            }
                            a aVar22 = new a();
                            int width32 = r9.width() * r9.height();
                            if ((view2 == null ? '\"' : '7') != '7') {
                            }
                            if (!(this.f436 != null)) {
                            }
                            this.f433 = aVar22;
                            this.f436 = new JSONObject(m408(m403(this.f433.f439, displayMetrics)));
                            z4 = true;
                            hashMap2.put("coveredPercent", Double.valueOf(aVar22.f441));
                            if (this.f432 != null) {
                            }
                            this.f437 = rect32;
                            this.f432 = new JSONObject(m408(m403(rect32, displayMetrics)));
                            z4 = true;
                            if (!(this.f438 != null)) {
                            }
                            this.f434 = r9;
                            this.f438 = new JSONObject(m408(m403(r9, displayMetrics)));
                            z4 = true;
                            if (!(this.f431 != null)) {
                            }
                            this.f431 = hashMap2;
                            z4 = true;
                            r0 = k.m320().m329();
                            if (k.m321(r0, this.f430)) {
                            }
                            if (!(z4)) {
                            }
                        } else {
                            if ((view2 != null ? 12 : '+') != 12) {
                            }
                            z = false;
                            if (!(view2 == null)) {
                            }
                            z2 = false;
                            if (!(view2 == null)) {
                            }
                            z3 = true;
                            if ((view2 != null ? '0' : '%') == '%') {
                            }
                            hashMap2.put("dr", Float.valueOf(displayMetrics.density));
                            hashMap2.put("dv", Double.valueOf(p.m350()));
                            hashMap2.put("adKey", str);
                            String str3222 = "isAttached";
                            if (!z) {
                            }
                            hashMap2.put(str3222, Integer.valueOf(i));
                            hashMap2.put("inFocus", Integer.valueOf(!z2 ? 0 : 1));
                            hashMap2.put("isHidden", Integer.valueOf(!z3 ? 0 : 1));
                            hashMap2.put("opacity", Float.valueOf(f));
                            Rect rect322 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                            if (view2 != null) {
                            }
                            a aVar222 = new a();
                            int width322 = r9.width() * r9.height();
                            if ((view2 == null ? '\"' : '7') != '7') {
                            }
                            if (!(this.f436 != null)) {
                            }
                            this.f433 = aVar222;
                            this.f436 = new JSONObject(m408(m403(this.f433.f439, displayMetrics)));
                            z4 = true;
                            hashMap2.put("coveredPercent", Double.valueOf(aVar222.f441));
                            if (this.f432 != null) {
                            }
                            this.f437 = rect322;
                            this.f432 = new JSONObject(m408(m403(rect322, displayMetrics)));
                            z4 = true;
                            if (!(this.f438 != null)) {
                            }
                            this.f434 = r9;
                            this.f438 = new JSONObject(m408(m403(r9, displayMetrics)));
                            z4 = true;
                            if (!(this.f431 != null)) {
                            }
                            this.f431 = hashMap2;
                            z4 = true;
                            r0 = k.m320().m329();
                            if (k.m321(r0, this.f430)) {
                            }
                            if (!(z4)) {
                            }
                        }
                        z = true;
                        if (!(view2 == null)) {
                        }
                        z2 = false;
                        if (!(view2 == null)) {
                        }
                        z3 = true;
                        if ((view2 != null ? '0' : '%') == '%') {
                        }
                        hashMap2.put("dr", Float.valueOf(displayMetrics.density));
                        hashMap2.put("dv", Double.valueOf(p.m350()));
                        hashMap2.put("adKey", str);
                        String str32222 = "isAttached";
                        if (!z) {
                        }
                        hashMap2.put(str32222, Integer.valueOf(i));
                        hashMap2.put("inFocus", Integer.valueOf(!z2 ? 0 : 1));
                        hashMap2.put("isHidden", Integer.valueOf(!z3 ? 0 : 1));
                        hashMap2.put("opacity", Float.valueOf(f));
                        Rect rect3222 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                        if (view2 != null) {
                        }
                        a aVar2222 = new a();
                        int width3222 = r9.width() * r9.height();
                        if ((view2 == null ? '\"' : '7') != '7') {
                        }
                        if (!(this.f436 != null)) {
                        }
                        this.f433 = aVar2222;
                        this.f436 = new JSONObject(m408(m403(this.f433.f439, displayMetrics)));
                        z4 = true;
                        hashMap2.put("coveredPercent", Double.valueOf(aVar2222.f441));
                        if (this.f432 != null) {
                        }
                        this.f437 = rect3222;
                        this.f432 = new JSONObject(m408(m403(rect3222, displayMetrics)));
                        z4 = true;
                        if (!(this.f438 != null)) {
                        }
                        this.f434 = r9;
                        this.f438 = new JSONObject(m408(m403(r9, displayMetrics)));
                        z4 = true;
                        if (!(this.f431 != null)) {
                        }
                        this.f431 = hashMap2;
                        z4 = true;
                        r0 = k.m320().m329();
                        if (k.m321(r0, this.f430)) {
                        }
                        if (!(z4)) {
                        }
                    }
                }
                displayMetrics = view.getContext().getResources().getDisplayMetrics();
                if (!(VERSION.SDK_INT >= 19)) {
                }
                z = true;
                if (!(view2 == null)) {
                }
                z2 = false;
                if (!(view2 == null)) {
                }
                z3 = true;
                if ((view2 != null ? '0' : '%') == '%') {
                }
                hashMap2.put("dr", Float.valueOf(displayMetrics.density));
                hashMap2.put("dv", Double.valueOf(p.m350()));
                hashMap2.put("adKey", str);
                String str322222 = "isAttached";
                if (!z) {
                }
                hashMap2.put(str322222, Integer.valueOf(i));
                hashMap2.put("inFocus", Integer.valueOf(!z2 ? 0 : 1));
                hashMap2.put("isHidden", Integer.valueOf(!z3 ? 0 : 1));
                hashMap2.put("opacity", Float.valueOf(f));
                Rect rect32222 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                if (view2 != null) {
                }
                a aVar22222 = new a();
                int width32222 = r9.width() * r9.height();
                if ((view2 == null ? '\"' : '7') != '7') {
                }
                if (!(this.f436 != null)) {
                }
                this.f433 = aVar22222;
                this.f436 = new JSONObject(m408(m403(this.f433.f439, displayMetrics)));
                z4 = true;
                hashMap2.put("coveredPercent", Double.valueOf(aVar22222.f441));
                if (this.f432 != null) {
                }
                this.f437 = rect32222;
                this.f432 = new JSONObject(m408(m403(rect32222, displayMetrics)));
                z4 = true;
                if (!(this.f438 != null)) {
                }
                this.f434 = r9;
                this.f438 = new JSONObject(m408(m403(r9, displayMetrics)));
                z4 = true;
                if (!(this.f431 != null)) {
                }
                this.f431 = hashMap2;
                z4 = true;
                r0 = k.m320().m329();
                if (k.m321(r0, this.f430)) {
                }
                if (!(z4)) {
                }
            } catch (Exception e2) {
                e = e2;
                o.m339(e);
                this.f435 = str2;
            }
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static float m401(View view) {
        float alpha = view.getAlpha();
        while (true) {
            if ((view != null ? 'S' : 'c') != 'S') {
                break;
            }
            if ((view.getParent() != null ? (char) 13 : 18) != 18) {
                if ((((double) alpha) != Utils.DOUBLE_EPSILON ? 'D' : 5) == 5) {
                    break;
                }
                int i = f428 + 121;
                f429 = i % 128;
                int i2 = i % 2;
                if ((view.getParent() instanceof View ? 'H' : 1) != 'H') {
                    break;
                }
                int i3 = f428 + 117;
                f429 = i3 % 128;
                int i4 = i3 % 2;
                alpha *= ((View) view.getParent()).getAlpha();
                view = (View) view.getParent();
            } else {
                break;
            }
        }
        return alpha;
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static Rect m404(View view) {
        return (view != null ? 10 : 'Z') != 'Z' ? m407(view) : new Rect(0, 0, 0, 0);
    }

    /* renamed from: ˏ reason: contains not printable characters */
    private static void m405(ArrayDeque<b> arrayDeque, b bVar, boolean z) {
        if ((bVar.f442 instanceof ViewGroup ? '?' : '/') != '/') {
            int i = f429 + 107;
            f428 = i % 128;
            int i2 = i % 2;
            if (!(bVar.f442 instanceof ListView)) {
                ViewGroup viewGroup = (ViewGroup) bVar.f442;
                for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                    arrayDeque.add(new b(viewGroup.getChildAt(childCount), bVar.f444 + 1, z));
                }
            }
        }
    }

    @VisibleForTesting
    /* renamed from: ˊ reason: contains not printable characters */
    private static int m402(Rect rect, Set<Rect> set) {
        if (set.isEmpty()) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(set);
        Collections.sort(arrayList, new Comparator<Rect>() {
            public final /* synthetic */ int compare(Object obj, Object obj2) {
                return Integer.valueOf(((Rect) obj).top).compareTo(Integer.valueOf(((Rect) obj2).top));
            }
        });
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (true) {
            if (!(it.hasNext())) {
                break;
            }
            int i = f428 + 101;
            f429 = i % 128;
            int i2 = i % 2;
            Rect rect2 = (Rect) it.next();
            arrayList2.add(Integer.valueOf(rect2.left));
            arrayList2.add(Integer.valueOf(rect2.right));
        }
        Collections.sort(arrayList2);
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= arrayList2.size() - 1) {
                return i4;
            }
            int i5 = f429 + 99;
            f428 = i5 % 128;
            int i6 = i5 % 2;
            int i7 = i3 + 1;
            if ((!((Integer) arrayList2.get(i3)).equals(arrayList2.get(i7)) ? 'a' : '1') != '1') {
                Rect rect3 = new Rect(((Integer) arrayList2.get(i3)).intValue(), rect.top, ((Integer) arrayList2.get(i7)).intValue(), rect.bottom);
                int i8 = rect.top;
                Iterator it2 = arrayList.iterator();
                while (true) {
                    if ((it2.hasNext() ? '^' : 17) == 17) {
                        break;
                    }
                    Rect rect4 = (Rect) it2.next();
                    if (Rect.intersects(rect4, rect3)) {
                        if ((rect4.bottom > i8 ? 3 : '/') == 3) {
                            i4 += rect3.width() * (rect4.bottom - Math.max(i8, rect4.top));
                            i8 = rect4.bottom;
                        }
                        if (rect4.bottom == rect3.bottom) {
                            break;
                        }
                    }
                }
            }
            i3 = i7;
        }
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static Map<String, String> m408(Rect rect) {
        HashMap hashMap = new HashMap();
        hashMap.put(AvidJSONUtil.KEY_X, String.valueOf(rect.left));
        hashMap.put(AvidJSONUtil.KEY_Y, String.valueOf(rect.top));
        hashMap.put("w", String.valueOf(rect.right - rect.left));
        hashMap.put("h", String.valueOf(rect.bottom - rect.top));
        return hashMap;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    private static Rect m403(Rect rect, DisplayMetrics displayMetrics) {
        float f = displayMetrics.density;
        if (!(f != 0.0f)) {
            return rect;
        }
        return new Rect(Math.round(((float) rect.left) / f), Math.round(((float) rect.top) / f), Math.round(((float) rect.right) / f), Math.round(((float) rect.bottom) / f));
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static Rect m407(View view) {
        int[] iArr = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationInWindow(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        return new Rect(i, i2, view.getWidth() + i, view.getHeight() + i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:139:0x01af, code lost:
        if ((((double) r15.getAlpha()) > com.github.mikephil.charting.utils.Utils.DOUBLE_EPSILON ? '>' : 'D') != '>') goto L_0x01b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x01fc, code lost:
        if ((android.os.Build.VERSION.SDK_INT >= 21) != false) goto L_0x01fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x0296, code lost:
        if ((android.os.Build.VERSION.SDK_INT < 19) != true) goto L_0x0298;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00d0, code lost:
        if ((!r3.isEmpty()) != false) goto L_0x00d2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x01c0  */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x026b  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x026e  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x02f4 A[Catch:{ Exception -> 0x0366 }] */
    @VisibleForTesting
    /* renamed from: ˏ reason: contains not printable characters */
    private static boolean m406(Rect rect, @NonNull View view, Set<Rect> set) {
        Rect r10;
        char c;
        char c2;
        char c3;
        char c4;
        boolean z;
        ViewParent parent;
        Rect rect2 = rect;
        View view2 = view;
        try {
            ArrayList arrayList = new ArrayList();
            View view3 = view2;
            int i = 0;
            while (true) {
                boolean z2 = true;
                if ((view3.getParent() == null ? ':' : 30) == ':') {
                    int i2 = f429 + 9;
                    f428 = i2 % 128;
                    if (i2 % 2 == 0) {
                        if (!(view3 == view.getRootView())) {
                            break;
                        }
                    } else {
                        if (!(view3 == view.getRootView())) {
                            break;
                        }
                    }
                }
                i++;
                if ((i > 50 ? 'a' : ']') == ']') {
                    arrayList.add(0, view3);
                    if ((view3.getParent() instanceof View ? '\'' : 13) == 13) {
                        break;
                    }
                    int i3 = f429 + 67;
                    f428 = i3 % 128;
                    if (i3 % 2 != 0) {
                        z2 = false;
                    }
                    if (z2) {
                        parent = view3.getParent();
                    } else {
                        parent = view3.getParent();
                    }
                    view3 = (View) parent;
                } else {
                    int i4 = f429 + 115;
                    f428 = i4 % 128;
                    if (i4 % 2 != 0) {
                    }
                    arrayList = null;
                }
            }
            if ((arrayList != null ? 9 : 'M') != 'M') {
                int i5 = f428 + 81;
                f429 = i5 % 128;
                if ((i5 % 2 == 0 ? '^' : '%') != '%') {
                    if (arrayList.isEmpty()) {
                    }
                }
                View rootView = view.getRootView();
                ArrayDeque arrayDeque = new ArrayDeque();
                arrayDeque.add(new b(rootView, 0, true));
                b.m224("VisibilityInfo", view2, "starting covering rect search");
                int i6 = 0;
                boolean z3 = false;
                while (true) {
                    if ((!arrayDeque.isEmpty() ? '(' : '5') == '5') {
                        break;
                    }
                    int i7 = f429 + 25;
                    f428 = i7 % 128;
                    int i8 = i7 % 2;
                    if ((i6 < 250 ? 22 : '2') != 22) {
                        break;
                    }
                    i6++;
                    b bVar = (b) arrayDeque.pollLast();
                    if ((bVar.f442.equals(view2) ? '@' : 'A') != '@') {
                        if (!bVar.f443) {
                            m405(arrayDeque, bVar, false);
                            if (ViewGroup.class.equals(bVar.f442.getClass().getSuperclass())) {
                                int i9 = f428 + 17;
                                f429 = i9 % 128;
                                if (i9 % 2 == 0) {
                                    if ((VERSION.SDK_INT >= 19 ? '*' : 'I') != 'I') {
                                    }
                                }
                                if (bVar.f442.getBackground() != null) {
                                    int i10 = f429 + 71;
                                    f428 = i10 % 128;
                                    if ((i10 % 2 != 0 ? 11 : '%') != 11) {
                                        if (!(bVar.f442.getBackground().getAlpha() != 0)) {
                                        }
                                    } else {
                                        if (bVar.f442.getBackground().getAlpha() != 0) {
                                            c4 = '%';
                                            c3 = '%';
                                        } else {
                                            c3 = 'Y';
                                            c4 = '%';
                                        }
                                        if (c3 != c4) {
                                        }
                                        r10 = m407(bVar.f442);
                                        if (r10.setIntersect(rect2, r10)) {
                                            View view4 = bVar.f442;
                                            StringBuilder sb = new StringBuilder("Covered by ");
                                            sb.append(bVar.f442.getClass().getSimpleName());
                                            sb.append("-");
                                            sb.append(r10.toString());
                                            b.m224("VisibilityInfo", view4, sb.toString());
                                            set.add(r10);
                                            if (r10.contains(rect2)) {
                                                c2 = '(';
                                                c = '(';
                                            } else {
                                                c = 'D';
                                                c2 = '(';
                                            }
                                            if (c == c2) {
                                                int i11 = f429 + 85;
                                                f428 = i11 % 128;
                                                if ((i11 % 2 != 0 ? '$' : 'E') != 'E') {
                                                }
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                            r10 = m407(bVar.f442);
                            if (r10.setIntersect(rect2, r10)) {
                            }
                        } else {
                            View view5 = arrayList.size() > bVar.f444 ? (View) arrayList.get(bVar.f444) : null;
                            if (!(bVar.f442 != view5)) {
                                m405(arrayDeque, bVar, true);
                            } else {
                                View view6 = bVar.f442;
                                if ((view6.isShown() ? 16 : '/') == 16) {
                                    int i12 = f429 + 121;
                                    f428 = i12 % 128;
                                    if ((i12 % 2 != 0 ? 18 : '\"') != '\"') {
                                        if (((double) view6.getAlpha()) <= Utils.DOUBLE_EPSILON) {
                                        }
                                    }
                                    z = true;
                                    if ((!z ? 'T' : '$') == 'T') {
                                        int i13 = f429 + 43;
                                        f428 = i13 % 128;
                                        int i14 = i13 % 2;
                                        if ((z3 ? 'R' : 23) != 23) {
                                            int i15 = f429 + 9;
                                            f428 = i15 % 128;
                                            if (i15 % 2 == 0) {
                                                if ((VERSION.SDK_INT >= 21 ? 'I' : 29) != 29) {
                                                }
                                                m405(arrayDeque, bVar, false);
                                                if (ViewGroup.class.equals(bVar.f442.getClass().getSuperclass())) {
                                                }
                                                r10 = m407(bVar.f442);
                                                if (r10.setIntersect(rect2, r10)) {
                                                }
                                            }
                                            if (bVar.f442.getElevation() < view5.getElevation()) {
                                            }
                                            m405(arrayDeque, bVar, false);
                                            if (ViewGroup.class.equals(bVar.f442.getClass().getSuperclass())) {
                                            }
                                            r10 = m407(bVar.f442);
                                            if (r10.setIntersect(rect2, r10)) {
                                            }
                                        } else {
                                            if (!(VERSION.SDK_INT < 21)) {
                                                int i16 = f428 + 13;
                                                f429 = i16 % 128;
                                                if ((i16 % 2 == 0 ? 26 : '\\') != '\\') {
                                                    if (bVar.f442.getElevation() > view5.getElevation()) {
                                                        m405(arrayDeque, bVar, false);
                                                        if (ViewGroup.class.equals(bVar.f442.getClass().getSuperclass())) {
                                                        }
                                                        r10 = m407(bVar.f442);
                                                        if (r10.setIntersect(rect2, r10)) {
                                                        }
                                                    }
                                                } else {
                                                    if (bVar.f442.getElevation() <= view5.getElevation()) {
                                                    }
                                                    m405(arrayDeque, bVar, false);
                                                    if (ViewGroup.class.equals(bVar.f442.getClass().getSuperclass())) {
                                                    }
                                                    r10 = m407(bVar.f442);
                                                    if (r10.setIntersect(rect2, r10)) {
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                z = false;
                                if ((!z ? 'T' : '$') == 'T') {
                                }
                            }
                        }
                        Set<Rect> set2 = set;
                    } else {
                        Set<Rect> set3 = set;
                        int i17 = f429 + 73;
                        f428 = i17 % 128;
                        int i18 = i17 % 2;
                        b.m224("VisibilityInfo", rect2, "found target");
                        z3 = true;
                    }
                }
                return false;
            }
            return false;
        } catch (Exception e) {
            o.m339(e);
        }
    }
}
