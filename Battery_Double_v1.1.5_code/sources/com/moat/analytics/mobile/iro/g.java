package com.moat.analytics.mobile.iro;

import android.support.annotation.CallSuper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class g extends d {

    /* renamed from: ˋॱ reason: contains not printable characters */
    private int f306 = Integer.MIN_VALUE;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private double f307 = Double.NaN;

    /* renamed from: ͺ reason: contains not printable characters */
    private int f308 = Integer.MIN_VALUE;

    /* renamed from: ॱˊ reason: contains not printable characters */
    private int f309 = a.f315;

    /* renamed from: ॱˋ reason: contains not printable characters */
    private int f310 = Integer.MIN_VALUE;

    /* renamed from: ॱˎ reason: contains not printable characters */
    private int f311 = 0;

    enum a {
        ;
        

        /* renamed from: ˊ reason: contains not printable characters */
        public static final int f313 = 3;

        /* renamed from: ˋ reason: contains not printable characters */
        public static final int f314 = 4;

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f315 = 1;

        /* renamed from: ˏ reason: contains not printable characters */
        public static final int f316 = 5;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f317 = 2;

        static {
            int[] iArr = {1, 2, 3, 4, 5};
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻॱ reason: contains not printable characters */
    public abstract Integer m281();

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋॱ reason: contains not printable characters */
    public abstract Integer m285();

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˊ reason: contains not printable characters */
    public abstract boolean m286();

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˎ reason: contains not printable characters */
    public abstract boolean m287();

    g(String str) {
        super(str);
    }

    /* renamed from: ˋ reason: contains not printable characters */
    public final boolean m284(Map<String, String> map, View view) {
        try {
            boolean r4 = super.m244(map, view);
            if (!r4) {
                return r4;
            }
            this.f269.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (!g.this.m286() || g.this.m248()) {
                            g.this.m249();
                        } else if (Boolean.valueOf(g.this.m282()).booleanValue()) {
                            g.this.f269.postDelayed(this, 200);
                        } else {
                            g.this.m249();
                        }
                    } catch (Exception e) {
                        g.this.m249();
                        o.m339(e);
                    }
                }
            }, 200);
            return r4;
        } catch (Exception e) {
            b.m223(3, "IntervalVideoTracker", this, "Problem with video loop");
            m237("trackVideoAd", e);
            return false;
        }
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            o.m339(e);
        }
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.f307 = m250().doubleValue();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final JSONObject m283(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.f230.equals(MoatAdEvent.f227)) {
            num = moatAdEvent.f230;
        } else {
            try {
                num = m285();
            } catch (Exception unused) {
                num = Integer.valueOf(this.f308);
            }
            moatAdEvent.f230 = num;
        }
        if (moatAdEvent.f230.intValue() < 0 || (moatAdEvent.f230.intValue() == 0 && moatAdEvent.f232 == MoatAdEventType.AD_EVT_COMPLETE && this.f308 > 0)) {
            num = Integer.valueOf(this.f308);
            moatAdEvent.f230 = num;
        }
        if (moatAdEvent.f232 == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.f310 == Integer.MIN_VALUE || !m240(num, Integer.valueOf(this.f310))) {
                this.f309 = a.f314;
                moatAdEvent.f232 = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.f309 = a.f316;
            }
        }
        return super.m243(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ʼॱ reason: contains not printable characters */
    public final boolean m282() throws o {
        if (!m286() || m248()) {
            return false;
        }
        try {
            int intValue = m285().intValue();
            if (this.f308 >= 0 && intValue < 0) {
                return false;
            }
            this.f308 = intValue;
            if (intValue == 0) {
                return true;
            }
            int intValue2 = m281().intValue();
            boolean r4 = m287();
            double d = (double) intValue2;
            Double.isNaN(d);
            double d2 = d / 4.0d;
            double doubleValue = m250().doubleValue();
            MoatAdEventType moatAdEventType = null;
            if (intValue > this.f306) {
                this.f306 = intValue;
            }
            if (this.f310 == Integer.MIN_VALUE) {
                this.f310 = intValue2;
            }
            if (r4) {
                if (this.f309 == a.f315) {
                    moatAdEventType = MoatAdEventType.AD_EVT_START;
                    this.f309 = a.f313;
                } else if (this.f309 == a.f317) {
                    moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                    this.f309 = a.f313;
                } else {
                    double d3 = (double) intValue;
                    Double.isNaN(d3);
                    int floor = ((int) Math.floor(d3 / d2)) - 1;
                    if (floor >= 0 && floor < 3) {
                        MoatAdEventType moatAdEventType2 = f266[floor];
                        if (!this.f267.containsKey(moatAdEventType2)) {
                            this.f267.put(moatAdEventType2, Integer.valueOf(1));
                            moatAdEventType = moatAdEventType2;
                        }
                    }
                }
            } else if (this.f309 != a.f317) {
                moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                this.f309 = a.f317;
            }
            boolean z = moatAdEventType != null;
            if (!z && !Double.isNaN(this.f307) && Math.abs(this.f307 - doubleValue) > 0.05d) {
                moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                z = true;
            }
            if (z) {
                dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), m247()));
            }
            this.f307 = doubleValue;
            this.f311 = 0;
            return true;
        } catch (Exception unused) {
            int i = this.f311;
            this.f311 = i + 1;
            return i < 5;
        }
    }
}
