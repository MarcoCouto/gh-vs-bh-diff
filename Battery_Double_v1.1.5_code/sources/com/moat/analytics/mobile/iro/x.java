package com.moat.analytics.mobile.iro;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.webkit.WebView;

final class x extends c implements WebAdTracker {
    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final String m400() {
        return "WebAdTracker";
    }

    x(@Nullable ViewGroup viewGroup) {
        this((WebView) v.m394(viewGroup, false).orElse(null));
        if (viewGroup == null) {
            String str = "Target ViewGroup is null";
            StringBuilder sb = new StringBuilder("WebAdTracker initialization not successful, ");
            sb.append(str);
            String sb2 = sb.toString();
            b.m223(3, "WebAdTracker", this, sb2);
            b.m221("[ERROR] ", sb2);
            this.f260 = new o(str);
        }
        if (this.f264 == null) {
            String str2 = "No WebView to track inside of ad container";
            StringBuilder sb3 = new StringBuilder("WebAdTracker initialization not successful, ");
            sb3.append(str2);
            String sb4 = sb3.toString();
            b.m223(3, "WebAdTracker", this, sb4);
            b.m221("[ERROR] ", sb4);
            this.f260 = new o(str2);
        }
    }

    x(@Nullable WebView webView) {
        super(webView, false, false);
        b.m223(3, "WebAdTracker", this, "Initializing.");
        if (webView == null) {
            String str = "WebView is null";
            StringBuilder sb = new StringBuilder("WebAdTracker initialization not successful, ");
            sb.append(str);
            String sb2 = sb.toString();
            b.m223(3, "WebAdTracker", this, sb2);
            b.m221("[ERROR] ", sb2);
            this.f260 = new o(str);
            return;
        }
        try {
            super.m233(webView);
            StringBuilder sb3 = new StringBuilder("WebAdTracker created for ");
            sb3.append(m230());
            b.m221("[SUCCESS] ", sb3.toString());
        } catch (o e) {
            this.f260 = e;
        }
    }
}
