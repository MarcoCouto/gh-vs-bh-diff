package com.moat.analytics.mobile.iro;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class t {
    /* access modifiers changed from: private */

    /* renamed from: ʻ reason: contains not printable characters */
    public static final Queue<b> f397 = new ConcurrentLinkedQueue();

    /* renamed from: ʽ reason: contains not printable characters */
    private static t f398;
    /* access modifiers changed from: private */

    /* renamed from: ʼ reason: contains not printable characters */
    public long f399 = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;

    /* renamed from: ˊ reason: contains not printable characters */
    volatile int f400 = 10;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private long f401 = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;

    /* renamed from: ˋ reason: contains not printable characters */
    volatile int f402 = c.f418;
    /* access modifiers changed from: private */

    /* renamed from: ˋॱ reason: contains not printable characters */
    public final AtomicInteger f403 = new AtomicInteger(0);

    /* renamed from: ˎ reason: contains not printable characters */
    volatile boolean f404 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    volatile boolean f405 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ reason: contains not printable characters */
    public final AtomicBoolean f406 = new AtomicBoolean(false);

    /* renamed from: ॱ reason: contains not printable characters */
    volatile int f407 = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ reason: contains not printable characters */
    public final AtomicBoolean f408 = new AtomicBoolean(false);
    /* access modifiers changed from: private */

    /* renamed from: ॱˋ reason: contains not printable characters */
    public volatile long f409 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ᐝ reason: contains not printable characters */
    public Handler f410;

    interface a {
        /* renamed from: ॱ reason: contains not printable characters */
        void m390() throws o;
    }

    class b {

        /* renamed from: ˋ reason: contains not printable characters */
        final a f415;

        /* renamed from: ॱ reason: contains not printable characters */
        final Long f417;

        b(Long l, a aVar) {
            this.f417 = l;
            this.f415 = aVar;
        }
    }

    enum c {
        ;
        

        /* renamed from: ˊ reason: contains not printable characters */
        public static final int f418 = 1;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f419 = 2;

        static {
            int[] iArr = {1, 2};
        }
    }

    class d implements Runnable {

        /* renamed from: ˋ reason: contains not printable characters */
        private final Handler f420;
        /* access modifiers changed from: private */

        /* renamed from: ˎ reason: contains not printable characters */
        public final AnonymousClass2 f421;

        /* renamed from: ˏ reason: contains not printable characters */
        private final String f422;

        private d(String str, Handler handler, AnonymousClass2 r4) {
            this.f421 = r4;
            this.f420 = handler;
            StringBuilder sb = new StringBuilder("https://z.moatads.com/");
            sb.append(str);
            sb.append("/android/");
            sb.append("2bc3418b93f01686fcbd1ebebcc04694651821b2".substring(0, 7));
            sb.append("/status.json");
            this.f422 = sb.toString();
        }

        /* renamed from: ॱ reason: contains not printable characters */
        private String m392() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f422);
            sb.append("?ts=");
            sb.append(System.currentTimeMillis());
            sb.append("&v=2.4.0");
            try {
                return (String) l.m331(sb.toString()).get();
            } catch (Exception unused) {
                return null;
            }
        }

        public final void run() {
            try {
                String r0 = m392();
                final i iVar = new i(r0);
                t.this.f404 = iVar.m302();
                t.this.f405 = iVar.m304();
                t.this.f407 = iVar.m303();
                t.this.f400 = iVar.m306();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            d.this.f421.m393(iVar);
                        } catch (Exception e) {
                            o.m339(e);
                        }
                    }
                });
                t.this.f409 = System.currentTimeMillis();
                t.this.f406.compareAndSet(true, false);
                if (r0 != null) {
                    t.this.f403.set(0);
                } else if (t.this.f403.incrementAndGet() < 10) {
                    t.this.m380(t.this.f399);
                }
            } catch (Exception e) {
                o.m339(e);
            }
            this.f420.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    interface e {
        /* renamed from: ˋ reason: contains not printable characters */
        void m393(i iVar) throws o;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static synchronized t m378() {
        t tVar;
        synchronized (t.class) {
            if (f398 == null) {
                f398 = new t();
            }
            tVar = f398;
        }
        return tVar;
    }

    private t() {
        try {
            this.f410 = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            o.m339(e2);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final void m387() {
        if (System.currentTimeMillis() - this.f409 > this.f401) {
            m380(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˋ reason: contains not printable characters */
    public void m380(final long j) {
        if (this.f406.compareAndSet(false, true)) {
            b.m223(3, "OnOff", this, "Performing status check.");
            new Thread() {
                public final void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    d dVar = new d("IRO", handler, new e() {
                        /* renamed from: ˋ reason: contains not printable characters */
                        public final void m389(i iVar) throws o {
                            synchronized (t.f397) {
                                boolean z = ((j) MoatAnalytics.getInstance()).f343;
                                if (t.this.f402 != iVar.m305() || (t.this.f402 == c.f418 && z)) {
                                    t.this.f402 = iVar.m305();
                                    if (t.this.f402 == c.f418 && z) {
                                        t.this.f402 = c.f419;
                                    }
                                    if (t.this.f402 == c.f419) {
                                        b.m223(3, "OnOff", this, "Moat enabled - Version 2.4.0");
                                    }
                                    for (b bVar : t.f397) {
                                        if (t.this.f402 == c.f419) {
                                            bVar.f415.m390();
                                        }
                                    }
                                }
                                while (!t.f397.isEmpty()) {
                                    t.f397.remove();
                                }
                            }
                        }
                    });
                    handler.postDelayed(dVar, j);
                    Looper.loop();
                }
            }.start();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m388(a aVar) throws o {
        if (this.f402 == c.f419) {
            aVar.m390();
            return;
        }
        m376();
        f397.add(new b(Long.valueOf(System.currentTimeMillis()), aVar));
        if (this.f408.compareAndSet(false, true)) {
            this.f410.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (t.f397.size() > 0) {
                            t.m376();
                            t.this.f410.postDelayed(this, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                            return;
                        }
                        t.this.f408.compareAndSet(true, false);
                        t.this.f410.removeCallbacks(this);
                    } catch (Exception e) {
                        o.m339(e);
                    }
                }
            }, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ reason: contains not printable characters */
    public static void m376() {
        synchronized (f397) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator it = f397.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - ((b) it.next()).f417.longValue() >= ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS) {
                    it.remove();
                }
            }
            if (f397.size() >= 15) {
                for (int i = 0; i < 5; i++) {
                    f397.remove();
                }
            }
        }
    }
}
