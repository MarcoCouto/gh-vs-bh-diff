package com.moat.analytics.mobile.iro;

interface m<T> {
    T create() throws o;

    T createNoOp();
}
