package com.moat.analytics.mobile.iro;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class h {

    /* renamed from: ˎ reason: contains not printable characters */
    private static final h f318 = new h();
    /* access modifiers changed from: private */

    /* renamed from: ʽ reason: contains not printable characters */
    public ScheduledFuture<?> f319;

    /* renamed from: ˊ reason: contains not printable characters */
    private final ScheduledExecutorService f320 = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public final Map<f, String> f321 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ˏ reason: contains not printable characters */
    public ScheduledFuture<?> f322;
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public final Map<c, String> f323 = new WeakHashMap();

    /* renamed from: ˋ reason: contains not printable characters */
    static h m289() {
        return f318;
    }

    private h() {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m294(final Context context, f fVar) {
        if (fVar != null) {
            this.f321.put(fVar, "");
            if (this.f319 == null || this.f319.isDone()) {
                b.m223(3, "JSUpdateLooper", this, "Starting metadata reporting loop");
                this.f319 = this.f320.scheduleWithFixedDelay(new Runnable() {
                    public final void run() {
                        try {
                            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_METADATA"));
                            if (h.this.f321.isEmpty()) {
                                h.this.f319.cancel(true);
                            }
                        } catch (Exception e) {
                            o.m339(e);
                        }
                    }
                }, 0, 50, TimeUnit.MILLISECONDS);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m295(f fVar) {
        if (fVar != null) {
            StringBuilder sb = new StringBuilder("removeSetupNeededBridge");
            sb.append(fVar.hashCode());
            b.m223(3, "JSUpdateLooper", this, sb.toString());
            this.f321.remove(fVar);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m293(final Context context, c cVar) {
        if (cVar != null) {
            StringBuilder sb = new StringBuilder("addActiveTracker");
            sb.append(cVar.hashCode());
            b.m223(3, "JSUpdateLooper", this, sb.toString());
            if (!this.f323.containsKey(cVar)) {
                this.f323.put(cVar, "");
                if (this.f322 == null || this.f322.isDone()) {
                    b.m223(3, "JSUpdateLooper", this, "Starting view update loop");
                    this.f322 = this.f320.scheduleWithFixedDelay(new Runnable() {
                        public final void run() {
                            try {
                                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_VIEW_INFO"));
                                if (h.this.f323.isEmpty()) {
                                    b.m223(3, "JSUpdateLooper", h.this, "No more active trackers");
                                    h.this.f322.cancel(true);
                                }
                            } catch (Exception e) {
                                o.m339(e);
                            }
                        }
                    }, 0, (long) t.m378().f407, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m296(c cVar) {
        if (cVar != null) {
            StringBuilder sb = new StringBuilder("removeActiveTracker");
            sb.append(cVar.hashCode());
            b.m223(3, "JSUpdateLooper", this, sb.toString());
            this.f323.remove(cVar);
        }
    }
}
