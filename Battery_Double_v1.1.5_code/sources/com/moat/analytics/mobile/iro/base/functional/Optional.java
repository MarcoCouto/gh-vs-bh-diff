package com.moat.analytics.mobile.iro.base.functional;

import java.util.NoSuchElementException;

public final class Optional<T> {

    /* renamed from: ˏ reason: contains not printable characters */
    private static final Optional<?> f253 = new Optional<>();

    /* renamed from: ˋ reason: contains not printable characters */
    private final T f254;

    private Optional() {
        this.f254 = null;
    }

    public static <T> Optional<T> empty() {
        return f253;
    }

    private Optional(T t) {
        if (t != null) {
            this.f254 = t;
            return;
        }
        throw new NullPointerException("Optional of null value.");
    }

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public final T get() {
        if (this.f254 != null) {
            return this.f254;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.f254 != null;
    }

    public final T orElse(T t) {
        return this.f254 != null ? this.f254 : t;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        Optional optional = (Optional) obj;
        return this.f254 == optional.f254 || !(this.f254 == null || optional.f254 == null || !this.f254.equals(optional.f254));
    }

    public final int hashCode() {
        if (this.f254 == null) {
            return 0;
        }
        return this.f254.hashCode();
    }

    public final String toString() {
        if (this.f254 == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", new Object[]{this.f254});
    }
}
