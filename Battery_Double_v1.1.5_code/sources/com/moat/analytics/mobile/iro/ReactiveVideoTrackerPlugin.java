package com.moat.analytics.mobile.iro;

import android.app.Activity;
import android.view.View;
import com.moat.analytics.mobile.iro.base.functional.Optional;
import java.util.Map;

public class ReactiveVideoTrackerPlugin implements m<ReactiveVideoTracker> {
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public final String f237;

    static class b implements ReactiveVideoTracker {
        public final void changeTargetView(View view) {
        }

        public final void dispatchEvent(MoatAdEvent moatAdEvent) {
        }

        public final void removeListener() {
        }

        public final void removeVideoListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void setPlayerVolume(Double d) {
        }

        public final void setVideoListener(VideoTrackerListener videoTrackerListener) {
        }

        public final void stopTracking() {
        }

        public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
            return false;
        }

        b() {
        }
    }

    public ReactiveVideoTrackerPlugin(String str) {
        this.f237 = str;
    }

    public ReactiveVideoTracker create() throws o {
        return (ReactiveVideoTracker) s.m367(new a<ReactiveVideoTracker>() {
            /* renamed from: ˏ reason: contains not printable characters */
            public final Optional<ReactiveVideoTracker> m210() {
                b.m221("[INFO] ", "Attempting to create ReactiveVideoTracker");
                return Optional.of(new w(ReactiveVideoTrackerPlugin.this.f237));
            }
        }, ReactiveVideoTracker.class);
    }

    public ReactiveVideoTracker createNoOp() {
        return new b();
    }
}
