package com.moat.analytics.mobile.iro;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class a {

    /* renamed from: ˊ reason: contains not printable characters */
    static WeakReference<Activity> f239 = null;

    /* renamed from: ˋ reason: contains not printable characters */
    private static boolean f240 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˎ reason: contains not printable characters */
    public static boolean f241 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    private static Application f242;
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public static int f243;

    static class c implements ActivityLifecycleCallbacks {
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        c() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            a.f243 = 1;
        }

        public final void onActivityStarted(Activity activity) {
            try {
                a.f239 = new WeakReference<>(activity);
                a.f243 = 2;
                if (!a.f241) {
                    m218(true);
                }
                a.f241 = true;
                StringBuilder sb = new StringBuilder("Activity started: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                b.m223(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m339(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                a.f239 = new WeakReference<>(activity);
                a.f243 = 3;
                t.m378().m387();
                StringBuilder sb = new StringBuilder("Activity resumed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                b.m223(3, "ActivityState", this, sb.toString());
                if (((j) MoatAnalytics.getInstance()).f341) {
                    e.m251(activity);
                }
            } catch (Exception e) {
                o.m339(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                a.f243 = 4;
                if (a.m215(activity)) {
                    a.f239 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity paused: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                b.m223(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m339(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (a.f243 != 3) {
                    a.f241 = false;
                    m218(false);
                }
                a.f243 = 5;
                if (a.m215(activity)) {
                    a.f239 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity stopped: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                b.m223(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m339(e);
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(a.f243 == 3 || a.f243 == 5)) {
                    if (a.f241) {
                        m218(false);
                    }
                    a.f241 = false;
                }
                a.f243 = 6;
                StringBuilder sb = new StringBuilder("Activity destroyed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                b.m223(3, "ActivityState", this, sb.toString());
                if (a.m215(activity)) {
                    a.f239 = new WeakReference<>(null);
                }
            } catch (Exception e) {
                o.m339(e);
            }
        }

        /* renamed from: ˋ reason: contains not printable characters */
        private static void m218(boolean z) {
            if (z) {
                b.m223(3, "ActivityState", null, "App became visible");
                if (t.m378().f402 == c.f419 && !((j) MoatAnalytics.getInstance()).f342) {
                    k.m320().m328();
                }
            } else {
                b.m223(3, "ActivityState", null, "App became invisible");
                if (t.m378().f402 == c.f419 && !((j) MoatAnalytics.getInstance()).f342) {
                    k.m320().m327();
                }
            }
        }
    }

    a() {
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static void m212(Application application) {
        f242 = application;
        if (!f240) {
            f240 = true;
            f242.registerActivityLifecycleCallbacks(new c());
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static Application m214() {
        return f242;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static /* synthetic */ boolean m215(Activity activity) {
        return f239 != null && f239.get() == activity;
    }
}
