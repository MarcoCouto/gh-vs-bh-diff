package com.moat.analytics.mobile.iro;

import android.support.annotation.VisibleForTesting;
import com.moat.analytics.mobile.iro.base.asserts.Asserts;
import com.moat.analytics.mobile.iro.base.functional.Optional;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class s<T> implements InvocationHandler {
    /* access modifiers changed from: private */

    /* renamed from: ˊ reason: contains not printable characters */
    public static final Object[] f386 = new Object[0];

    /* renamed from: ˋ reason: contains not printable characters */
    private final LinkedList<e> f387 = new LinkedList<>();

    /* renamed from: ˎ reason: contains not printable characters */
    private final Class<T> f388;

    /* renamed from: ˏ reason: contains not printable characters */
    private boolean f389;

    /* renamed from: ॱ reason: contains not printable characters */
    private final a<T> f390;

    /* renamed from: ᐝ reason: contains not printable characters */
    private T f391;

    interface a<T> {
        /* renamed from: ˏ reason: contains not printable characters */
        Optional<T> m372() throws o;
    }

    class e {
        /* access modifiers changed from: private */

        /* renamed from: ˊ reason: contains not printable characters */
        public final Method f393;

        /* renamed from: ˋ reason: contains not printable characters */
        private final LinkedList<Object> f394;
        /* access modifiers changed from: private */

        /* renamed from: ˎ reason: contains not printable characters */
        public final WeakReference[] f395;

        /* synthetic */ e(s sVar, Method method, Object[] objArr, byte b) {
            this(method, objArr);
        }

        private e(Method method, Object... objArr) {
            this.f394 = new LinkedList<>();
            if (objArr == null) {
                objArr = s.f386;
            }
            WeakReference[] weakReferenceArr = new WeakReference[objArr.length];
            int length = objArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Object obj = objArr[i];
                if ((obj instanceof Map) || (obj instanceof Integer) || (obj instanceof Double)) {
                    this.f394.add(obj);
                }
                int i3 = i2 + 1;
                weakReferenceArr[i2] = new WeakReference(obj);
                i++;
                i2 = i3;
            }
            this.f395 = weakReferenceArr;
            this.f393 = method;
        }
    }

    @VisibleForTesting
    private s(a<T> aVar, Class<T> cls) throws o {
        Asserts.checkNotNull(aVar);
        Asserts.checkNotNull(cls);
        this.f390 = aVar;
        this.f388 = cls;
        t.m378().m388((a) new a() {
            /* renamed from: ॱ reason: contains not printable characters */
            public final void m371() throws o {
                s.this.m369();
            }
        });
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static <T> T m367(a<T> aVar, Class<T> cls) throws o {
        ClassLoader classLoader = cls.getClassLoader();
        s sVar = new s(aVar, cls);
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, sVar);
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static Boolean m366(Method method) {
        try {
            if (Boolean.TYPE.equals(method.getReturnType())) {
                return Boolean.valueOf(true);
            }
        } catch (Exception e2) {
            o.m339(e2);
        }
        return null;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            Class declaringClass = method.getDeclaringClass();
            t r0 = t.m378();
            if (Object.class.equals(declaringClass)) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    return this.f388;
                }
                if (!"toString".equals(name)) {
                    return method.invoke(this, objArr);
                }
                Object invoke = method.invoke(this, objArr);
                return String.valueOf(invoke).replace(s.class.getName(), this.f388.getName());
            } else if (!this.f389 || this.f391 != null) {
                if (r0.f402 == c.f419) {
                    m369();
                    if (this.f391 != null) {
                        return method.invoke(this.f391, objArr);
                    }
                }
                if (r0.f402 == c.f418 && (!this.f389 || this.f391 != null)) {
                    if (this.f387.size() >= 15) {
                        this.f387.remove(5);
                    }
                    this.f387.add(new e(this, method, objArr, 0));
                }
                return m366(method);
            } else {
                this.f387.clear();
                return m366(method);
            }
        } catch (Exception e2) {
            o.m339(e2);
            return m366(method);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˋ reason: contains not printable characters */
    public void m369() throws o {
        if (!this.f389) {
            try {
                this.f391 = this.f390.m372().orElse(null);
            } catch (Exception e2) {
                b.m219("OnOffTrackerProxy", this, "Could not create instance", e2);
                o.m339(e2);
            }
            this.f389 = true;
        }
        if (this.f391 != null) {
            Iterator it = this.f387.iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                try {
                    Object[] objArr = new Object[eVar.f395.length];
                    WeakReference[] r3 = eVar.f395;
                    int length = r3.length;
                    int i = 0;
                    int i2 = 0;
                    while (i < length) {
                        int i3 = i2 + 1;
                        objArr[i2] = r3[i].get();
                        i++;
                        i2 = i3;
                    }
                    eVar.f393.invoke(this.f391, objArr);
                } catch (Exception e3) {
                    o.m339(e3);
                }
            }
            this.f387.clear();
        }
    }
}
