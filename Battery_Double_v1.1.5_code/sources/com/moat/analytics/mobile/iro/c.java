package com.moat.analytics.mobile.iro;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class c {

    /* renamed from: ʻ reason: contains not printable characters */
    private WeakReference<View> f255;

    /* renamed from: ʼ reason: contains not printable characters */
    private final boolean f256;

    /* renamed from: ʽ reason: contains not printable characters */
    private boolean f257;

    /* renamed from: ˊ reason: contains not printable characters */
    TrackerListener f258;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private final y f259;

    /* renamed from: ˋ reason: contains not printable characters */
    o f260 = null;

    /* renamed from: ˎ reason: contains not printable characters */
    final String f261;

    /* renamed from: ˏ reason: contains not printable characters */
    f f262 = this.f276.f245;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private boolean f263;

    /* renamed from: ॱ reason: contains not printable characters */
    WeakReference<WebView> f264;

    /* renamed from: ᐝ reason: contains not printable characters */
    final boolean f265;

    @Deprecated
    public void setActivity(Activity activity) {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public abstract String m231();

    c(@Nullable View view, boolean z, boolean z2) {
        String str;
        b.m223(3, "BaseTracker", this, "Initializing.");
        if (z) {
            StringBuilder sb = new StringBuilder("m");
            sb.append(hashCode());
            str = sb.toString();
        } else {
            str = "";
        }
        this.f261 = str;
        this.f255 = new WeakReference<>(view);
        this.f256 = z;
        this.f265 = z2;
        this.f257 = false;
        this.f263 = false;
        this.f259 = new y();
    }

    public void setListener(TrackerListener trackerListener) {
        this.f258 = trackerListener;
    }

    public void removeListener() {
        this.f258 = null;
    }

    public void startTracking() {
        try {
            b.m223(3, "BaseTracker", this, "In startTracking method.");
            m234();
            if (this.f258 != null) {
                TrackerListener trackerListener = this.f258;
                StringBuilder sb = new StringBuilder("Tracking started on ");
                sb.append(b.m225((View) this.f255.get()));
                trackerListener.onTrackingStarted(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder("startTracking succeeded for ");
            sb2.append(b.m225((View) this.f255.get()));
            String sb3 = sb2.toString();
            b.m223(3, "BaseTracker", this, sb3);
            StringBuilder sb4 = new StringBuilder();
            sb4.append(m231());
            sb4.append(" ");
            sb4.append(sb3);
            b.m221("[SUCCESS] ", sb4.toString());
        } catch (Exception e) {
            m237("startTracking", e);
        }
    }

    @CallSuper
    public void stopTracking() {
        boolean z = false;
        try {
            b.m223(3, "BaseTracker", this, "In stopTracking method.");
            this.f263 = true;
            if (this.f262 != null) {
                this.f262.m277(this);
                z = true;
            }
        } catch (Exception e) {
            o.m339(e);
        }
        String str = "BaseTracker";
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        b.m223(3, str, this, sb.toString());
        String str2 = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m231());
        sb2.append(" stopTracking ");
        sb2.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : ParametersKeys.FAILED);
        sb2.append(" for ");
        sb2.append(b.m225((View) this.f255.get()));
        b.m221(str2, sb2.toString());
        if (this.f258 != null) {
            this.f258.onTrackingStopped("");
            this.f258 = null;
        }
    }

    @CallSuper
    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(b.m225(view));
        b.m223(3, "BaseTracker", this, sb.toString());
        this.f255 = new WeakReference<>(view);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˎ reason: contains not printable characters */
    public void m234() throws o {
        b.m223(3, "BaseTracker", this, "Attempting to start impression.");
        m232();
        if (this.f257) {
            throw new o("Tracker already started");
        } else if (!this.f263) {
            m236(new ArrayList());
            if (this.f262 != null) {
                this.f262.m279(this);
                this.f257 = true;
                b.m223(3, "BaseTracker", this, "Impression started.");
                return;
            }
            b.m223(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new o("Bridge is null");
        } else {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m233(WebView webView) throws o {
        if (webView != null) {
            this.f264 = new WeakReference<>(webView);
            if (this.f262 == null) {
                if (!(this.f256 || this.f265)) {
                    b.m223(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f264.get() != null) {
                        this.f262 = new f((WebView) this.f264.get(), b.f305);
                        b.m223(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f262 = null;
                        b.m223(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            if (this.f262 != null) {
                this.f262.m276(this);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m232() throws o {
        if (this.f260 != null) {
            StringBuilder sb = new StringBuilder("Tracker initialization failed: ");
            sb.append(this.f260.getMessage());
            throw new o(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final boolean m238() {
        return this.f257 && !this.f263;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻ reason: contains not printable characters */
    public final View m228() {
        return (View) this.f255.get();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʼ reason: contains not printable characters */
    public final String m229() {
        this.f259.m409(this.f261, (View) this.f255.get());
        return this.f259.f435;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m237(String str, Exception exc) {
        try {
            o.m339(exc);
            String r3 = o.m338(str, exc);
            if (this.f258 != null) {
                this.f258.onTrackingFailedToStart(r3);
            }
            b.m223(3, "BaseTracker", this, r3);
            StringBuilder sb = new StringBuilder();
            sb.append(m231());
            sb.append(" ");
            sb.append(r3);
            b.m221("[ERROR] ", sb.toString());
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final void m235() throws o {
        if (this.f257) {
            throw new o("Tracker already started");
        } else if (this.f263) {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˏ reason: contains not printable characters */
    public void m236(List<String> list) throws o {
        if (((View) this.f255.get()) == null && !this.f265) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new o(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʽ reason: contains not printable characters */
    public final String m230() {
        return b.m225((View) this.f255.get());
    }
}
