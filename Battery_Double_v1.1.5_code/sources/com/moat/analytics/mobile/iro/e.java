package com.moat.analytics.mobile.iro;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.ads.AdActivity;
import com.moat.analytics.mobile.iro.base.functional.Optional;
import java.lang.ref.WeakReference;

final class e {

    /* renamed from: ˊ reason: contains not printable characters */
    private static WeakReference<Activity> f278 = new WeakReference<>(null);
    @Nullable

    /* renamed from: ˏ reason: contains not printable characters */
    private static WebAdTracker f279;

    e() {
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static void m251(Activity activity) {
        try {
            if (t.m378().f402 != c.f418) {
                String name = activity.getClass().getName();
                StringBuilder sb = new StringBuilder("Activity name: ");
                sb.append(name);
                b.m223(3, "GMAInterstitialHelper", activity, sb.toString());
                if (!name.contains(AdActivity.CLASS_NAME)) {
                    if (f279 != null) {
                        b.m223(3, "GMAInterstitialHelper", f278.get(), "Stopping to track GMA interstitial");
                        f279.stopTracking();
                        f279 = null;
                    }
                    f278 = new WeakReference<>(null);
                } else if (f278.get() == null || f278.get() != activity) {
                    View decorView = activity.getWindow().getDecorView();
                    if (decorView instanceof ViewGroup) {
                        Optional r0 = v.m394((ViewGroup) decorView, true);
                        if (r0.isPresent()) {
                            f278 = new WeakReference<>(activity);
                            WebView webView = (WebView) r0.get();
                            b.m223(3, "GMAInterstitialHelper", f278.get(), "Starting to track GMA interstitial");
                            WebAdTracker createWebAdTracker = MoatFactory.create().createWebAdTracker(webView);
                            f279 = createWebAdTracker;
                            createWebAdTracker.startTracking();
                            return;
                        }
                        b.m223(3, "GMAInterstitialHelper", activity, "Sorry, no WebView in this activity");
                    }
                }
            }
        } catch (Exception e) {
            o.m339(e);
        }
    }
}
