package com.moat.analytics.mobile.iro;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import java.lang.ref.WeakReference;

final class j extends MoatAnalytics implements a {

    /* renamed from: ʼ reason: contains not printable characters */
    private String f337;
    @Nullable

    /* renamed from: ˊ reason: contains not printable characters */
    b f338;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private MoatOptions f339;

    /* renamed from: ˋ reason: contains not printable characters */
    WeakReference<Context> f340;

    /* renamed from: ˎ reason: contains not printable characters */
    boolean f341 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    boolean f342 = false;

    /* renamed from: ॱ reason: contains not printable characters */
    boolean f343 = false;

    /* renamed from: ᐝ reason: contains not printable characters */
    private boolean f344 = false;

    j() {
    }

    public final void start(Application application) {
        start(new MoatOptions(), application);
    }

    @UiThread
    public final void prepareNativeDisplayTracking(String str) {
        this.f337 = str;
        if (t.m378().f402 != c.f418) {
            try {
                m307();
            } catch (Exception e) {
                o.m339(e);
            }
        }
    }

    @UiThread
    /* renamed from: ˋ reason: contains not printable characters */
    private void m307() {
        if (this.f338 == null) {
            this.f338 = new b(a.m214(), a.f252);
            this.f338.m226(this.f337);
            StringBuilder sb = new StringBuilder("Preparing native display tracking with partner code ");
            sb.append(this.f337);
            b.m223(3, "Analytics", this, sb.toString());
            StringBuilder sb2 = new StringBuilder("Prepared for native display tracking with partner code ");
            sb2.append(this.f337);
            b.m221("[SUCCESS] ", sb2.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final boolean m308() {
        return this.f344;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final boolean m309() {
        return this.f339 != null && this.f339.disableLocationServices;
    }

    /* renamed from: ॱ reason: contains not printable characters */
    public final void m310() throws o {
        o.m336();
        k.m320();
        if (this.f337 != null) {
            try {
                m307();
            } catch (Exception e) {
                o.m339(e);
            }
        }
    }

    public final void start(MoatOptions moatOptions, Application application) {
        try {
            if (this.f344) {
                b.m223(3, "Analytics", this, "Moat SDK has already been started.");
                return;
            }
            this.f339 = moatOptions;
            t.m378().m387();
            this.f342 = moatOptions.disableLocationServices;
            if (application != null) {
                if (moatOptions.loggingEnabled && p.m348(application.getApplicationContext())) {
                    this.f343 = true;
                }
                this.f340 = new WeakReference<>(application.getApplicationContext());
                this.f344 = true;
                this.f341 = moatOptions.autoTrackGMAInterstitials;
                a.m212(application);
                t.m378().m388((a) this);
                if (!moatOptions.disableAdIdCollection) {
                    p.m343(application);
                }
                b.m221("[SUCCESS] ", "Moat Analytics SDK Version 2.4.0 started");
                return;
            }
            throw new o("Moat Analytics SDK didn't start, application was null");
        } catch (Exception e) {
            o.m339(e);
        }
    }
}
