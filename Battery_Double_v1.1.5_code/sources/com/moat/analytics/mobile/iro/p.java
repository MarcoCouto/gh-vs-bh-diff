package com.moat.analytics.mobile.iro;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.support.annotation.FloatRange;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import java.lang.ref.WeakReference;

final class p {

    /* renamed from: ˊ reason: contains not printable characters */
    private static long f368 = 9141242330850693853L;
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public static String f369;

    /* renamed from: ˏ reason: contains not printable characters */
    private static a f370;

    /* renamed from: ॱ reason: contains not printable characters */
    private static b f371;

    static class a {

        /* renamed from: ˊ reason: contains not printable characters */
        String f373;

        /* renamed from: ˋ reason: contains not printable characters */
        String f374;

        /* renamed from: ˎ reason: contains not printable characters */
        Integer f375;

        /* renamed from: ˏ reason: contains not printable characters */
        boolean f376;

        /* renamed from: ॱ reason: contains not printable characters */
        boolean f377;

        /* renamed from: ᐝ reason: contains not printable characters */
        boolean f378;

        /* synthetic */ a(byte b) {
            this();
        }

        private a() {
            this.f374 = "_unknown_";
            this.f373 = "_unknown_";
            this.f375 = Integer.valueOf(-1);
            this.f376 = false;
            this.f377 = false;
            this.f378 = false;
            try {
                Context r0 = p.m345();
                if (r0 != null) {
                    this.f378 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService(PlaceFields.PHONE);
                    this.f374 = telephonyManager.getSimOperatorName();
                    this.f373 = telephonyManager.getNetworkOperatorName();
                    this.f375 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f376 = p.m344();
                    this.f377 = p.m348(r0);
                }
            } catch (Exception e) {
                o.m339(e);
            }
        }
    }

    static class b {

        /* renamed from: ˋ reason: contains not printable characters */
        private String f379;

        /* renamed from: ˎ reason: contains not printable characters */
        private String f380;

        /* renamed from: ˏ reason: contains not printable characters */
        private String f381;
        /* access modifiers changed from: private */

        /* renamed from: ॱ reason: contains not printable characters */
        public boolean f382;

        /* synthetic */ b(byte b) {
            this();
        }

        private b() {
            this.f382 = false;
            this.f380 = "_unknown_";
            this.f379 = "_unknown_";
            this.f381 = "_unknown_";
            try {
                Context r0 = p.m345();
                if (r0 != null) {
                    this.f382 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f379 = r0.getPackageName();
                    this.f380 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f381 = packageManager.getInstallerPackageName(this.f379);
                    return;
                }
                b.m223(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                o.m339(e);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˏ reason: contains not printable characters */
        public final String m354() {
            return this.f380;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˋ reason: contains not printable characters */
        public final String m353() {
            return this.f379;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ॱ reason: contains not printable characters */
        public final String m355() {
            return this.f381 != null ? this.f381 : "_unknown_";
        }
    }

    p() {
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    /* renamed from: ॱ reason: contains not printable characters */
    static double m350() {
        try {
            double r1 = (double) m341();
            double streamMaxVolume = (double) ((AudioManager) a.m214().getSystemService(m351("㇕ꍩ߆嗠殛").intern())).getStreamMaxVolume(3);
            Double.isNaN(r1);
            Double.isNaN(streamMaxVolume);
            return r1 / streamMaxVolume;
        } catch (Exception e) {
            o.m339(e);
            return Utils.DOUBLE_EPSILON;
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private static int m341() {
        try {
            return ((AudioManager) a.m214().getSystemService(m351("㇕ꍩ߆嗠殛").intern())).getStreamVolume(3);
        } catch (Exception e) {
            o.m339(e);
            return 0;
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static void m343(final Application application) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(application);
                        if (!advertisingIdInfo.isLimitAdTrackingEnabled()) {
                            p.f369 = advertisingIdInfo.getId();
                            StringBuilder sb = new StringBuilder("Retrieved Advertising ID = ");
                            sb.append(p.f369);
                            b.m223(3, "Util", this, sb.toString());
                            return;
                        }
                        b.m223(3, "Util", this, "User has limited ad tracking");
                    } catch (Exception e) {
                        o.m339(e);
                    }
                }
            });
        } catch (Exception e) {
            o.m339(e);
        }
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static String m351(String str) {
        char[] charArray = str.toCharArray();
        char c = charArray[0];
        int i = 1;
        char[] cArr = new char[(charArray.length - 1)];
        while (true) {
            if ((i < charArray.length ? 'K' : 11) != 'K') {
                return new String(cArr);
            }
            cArr[i - 1] = (char) ((int) (((long) (charArray[i] ^ (i * c))) ^ f368));
            i++;
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static String m349() {
        return f369;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static Context m345() {
        WeakReference<Context> weakReference = ((j) MoatAnalytics.getInstance()).f340;
        if (weakReference != null) {
            return (Context) weakReference.get();
        }
        return null;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static b m346() {
        if (f371 == null || !f371.f382) {
            f371 = new b(0);
        }
        return f371;
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static a m342() {
        if (f370 == null || !f370.f378) {
            f370 = new a(0);
        }
        return f370;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static boolean m348(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    static /* synthetic */ boolean m344() {
        int i;
        WeakReference<Context> weakReference = ((j) MoatAnalytics.getInstance()).f340;
        Context context = weakReference != null ? (Context) weakReference.get() : null;
        if ((context == null ? '9' : 'X') != '9') {
            i = (VERSION.SDK_INT < 17 ? 14 : 'J') != 'J' ? Secure.getInt(context.getContentResolver(), m351("涓Ｏ䦟?Ⓨ녧ρ涹︧䢚픆⟨").intern(), 0) : Global.getInt(context.getContentResolver(), m351("涓Ｏ䦟?Ⓨ녧ρ涹︧䢚픆⟨").intern(), 0);
        } else {
            i = 0;
        }
        if ((i != 1 ? 'c' : '/') != 'c') {
            return true;
        }
        return false;
    }
}
