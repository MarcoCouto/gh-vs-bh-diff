package com.moat.analytics.mobile.iro;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.github.mikephil.charting.utils.Utils;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class k implements LocationListener {

    /* renamed from: ˏ reason: contains not printable characters */
    private static k f345;

    /* renamed from: ʼ reason: contains not printable characters */
    private boolean f346;

    /* renamed from: ʽ reason: contains not printable characters */
    private Location f347;

    /* renamed from: ˊ reason: contains not printable characters */
    private ScheduledFuture<?> f348;

    /* renamed from: ˋ reason: contains not printable characters */
    private ScheduledFuture<?> f349;

    /* renamed from: ˎ reason: contains not printable characters */
    private LocationManager f350;

    /* renamed from: ॱ reason: contains not printable characters */
    private ScheduledExecutorService f351;

    /* renamed from: ᐝ reason: contains not printable characters */
    private boolean f352;

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static k m320() {
        if (f345 == null) {
            f345 = new k();
        }
        return f345;
    }

    private k() {
        try {
            this.f352 = ((j) MoatAnalytics.getInstance()).f342;
            if (this.f352) {
                b.m223(3, "LocationManager", this, "Moat location services disabled");
                return;
            }
            this.f351 = Executors.newScheduledThreadPool(1);
            this.f350 = (LocationManager) a.m214().getSystemService("location");
            if (this.f350.getAllProviders().size() == 0) {
                b.m223(3, "LocationManager", this, "Device has no location providers");
            } else {
                m322();
            }
        } catch (Exception e) {
            o.m339(e);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: ॱ reason: contains not printable characters */
    public final Location m329() {
        if (this.f352 || this.f350 == null) {
            return null;
        }
        return this.f347;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m328() {
        m322();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m327() {
        m317(false);
    }

    public final void onLocationChanged(Location location) {
        String str = "LocationManager";
        try {
            StringBuilder sb = new StringBuilder("Received an updated location = ");
            sb.append(location.toString());
            b.m223(3, str, this, sb.toString());
            float currentTimeMillis = (float) ((System.currentTimeMillis() - location.getTime()) / 1000);
            if (location.hasAccuracy() && location.getAccuracy() <= 100.0f && currentTimeMillis < 600.0f) {
                this.f347 = m314(this.f347, location);
                b.m223(3, "LocationManager", this, "fetchCompleted");
                m317(true);
            }
        } catch (Exception e) {
            o.m339(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ reason: contains not printable characters */
    public void m322() {
        try {
            if (!this.f352) {
                if (this.f350 != null) {
                    if (this.f346) {
                        b.m223(3, "LocationManager", this, "already updating location");
                    }
                    b.m223(3, "LocationManager", this, "starting location fetch");
                    this.f347 = m314(this.f347, m311());
                    if (this.f347 != null) {
                        StringBuilder sb = new StringBuilder("Have a valid location, won't fetch = ");
                        sb.append(this.f347.toString());
                        b.m223(3, "LocationManager", this, sb.toString());
                        m323();
                        return;
                    }
                    m326();
                }
            }
        } catch (Exception e) {
            o.m339(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˋ reason: contains not printable characters */
    public void m317(boolean z) {
        try {
            b.m223(3, "LocationManager", this, "stopping location fetch");
            m313();
            m316();
            if (z) {
                m323();
            } else {
                m312();
            }
        } catch (Exception e) {
            o.m339(e);
        }
    }

    /* renamed from: ʻ reason: contains not printable characters */
    private Location m311() {
        Location location;
        try {
            boolean r1 = m319();
            boolean r2 = m325();
            if (r1 && r2) {
                location = m314(this.f350.getLastKnownLocation("gps"), this.f350.getLastKnownLocation("network"));
            } else if (r1) {
                location = this.f350.getLastKnownLocation("gps");
            } else if (!r2) {
                return null;
            } else {
                location = this.f350.getLastKnownLocation("network");
            }
            return location;
        } catch (SecurityException e) {
            o.m339(e);
            return null;
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private void m326() {
        try {
            if (!this.f346) {
                b.m223(3, "LocationManager", this, "Attempting to start update");
                if (m319()) {
                    b.m223(3, "LocationManager", this, "start updating gps location");
                    this.f350.requestLocationUpdates("gps", 0, 0.0f, this, Looper.getMainLooper());
                    this.f346 = true;
                }
                if (m325()) {
                    b.m223(3, "LocationManager", this, "start updating network location");
                    this.f350.requestLocationUpdates("network", 0, 0.0f, this, Looper.getMainLooper());
                    this.f346 = true;
                }
                if (this.f346) {
                    m316();
                    this.f349 = this.f351.schedule(new Runnable() {
                        public final void run() {
                            try {
                                b.m223(3, "LocationManager", this, "fetchTimedOut");
                                k.this.m317(true);
                            } catch (Exception e) {
                                o.m339(e);
                            }
                        }
                    }, 60, TimeUnit.SECONDS);
                }
            }
        } catch (SecurityException e) {
            o.m339(e);
        }
    }

    /* renamed from: ʽ reason: contains not printable characters */
    private void m313() {
        try {
            b.m223(3, "LocationManager", this, "Stopping to update location");
            boolean z = true;
            if (!(ContextCompat.checkSelfPermission(a.m214().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
                if (!(ContextCompat.checkSelfPermission(a.m214().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                    z = false;
                }
            }
            if (z && this.f350 != null) {
                this.f350.removeUpdates(this);
                this.f346 = false;
            }
        } catch (SecurityException e) {
            o.m339(e);
        }
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    private void m316() {
        if (this.f349 != null && !this.f349.isCancelled()) {
            this.f349.cancel(true);
            this.f349 = null;
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private void m312() {
        if (this.f348 != null && !this.f348.isCancelled()) {
            this.f348.cancel(true);
            this.f348 = null;
        }
    }

    /* renamed from: ˏॱ reason: contains not printable characters */
    private void m323() {
        b.m223(3, "LocationManager", this, "Resetting fetch timer");
        m312();
        float f = 600.0f;
        if (this.f347 != null) {
            f = Math.max(600.0f - ((float) ((System.currentTimeMillis() - this.f347.getTime()) / 1000)), 0.0f);
        }
        this.f348 = this.f351.schedule(new Runnable() {
            public final void run() {
                try {
                    b.m223(3, "LocationManager", this, "fetchTimerCompleted");
                    k.this.m322();
                } catch (Exception e) {
                    o.m339(e);
                }
            }
        }, (long) f, TimeUnit.SECONDS);
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static Location m314(Location location, Location location2) {
        boolean r0 = m318(location);
        boolean r1 = m318(location2);
        if (r0) {
            return (r1 && location.getAccuracy() >= location.getAccuracy()) ? location2 : location;
        }
        if (!r1) {
            return null;
        }
        return location2;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    private static boolean m318(Location location) {
        if (location == null) {
            return false;
        }
        if ((location.getLatitude() != Utils.DOUBLE_EPSILON || location.getLongitude() != Utils.DOUBLE_EPSILON) && location.getAccuracy() >= 0.0f && ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)) < 600.0f) {
            return true;
        }
        return false;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static boolean m321(Location location, Location location2) {
        if (location == location2) {
            return true;
        }
        return (location == null || location2 == null || location.getTime() != location2.getTime()) ? false : true;
    }

    /* renamed from: ˋॱ reason: contains not printable characters */
    private boolean m319() {
        return (ContextCompat.checkSelfPermission(a.m214().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) && this.f350.getProvider("gps") != null && this.f350.isProviderEnabled("gps");
    }

    /* renamed from: ॱˋ reason: contains not printable characters */
    private boolean m325() {
        boolean z;
        if (!(ContextCompat.checkSelfPermission(a.m214().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
            if (!(ContextCompat.checkSelfPermission(a.m214().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                z = false;
                return !z && this.f350.getProvider("network") != null && this.f350.isProviderEnabled("network");
            }
        }
        z = true;
        if (!z) {
        }
    }
}
