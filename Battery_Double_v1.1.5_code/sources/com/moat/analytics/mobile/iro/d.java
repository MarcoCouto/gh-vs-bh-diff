package com.moat.analytics.mobile.iro;

import android.os.Handler;
import android.support.annotation.CallSuper;
import android.text.TextUtils;
import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

abstract class d extends c {

    /* renamed from: ʽ reason: contains not printable characters */
    static final MoatAdEventType[] f266 = {MoatAdEventType.AD_EVT_FIRST_QUARTILE, MoatAdEventType.AD_EVT_MID_POINT, MoatAdEventType.AD_EVT_THIRD_QUARTILE};

    /* renamed from: ʻ reason: contains not printable characters */
    final Map<MoatAdEventType, Integer> f267;

    /* renamed from: ʼ reason: contains not printable characters */
    WeakReference<View> f268;

    /* renamed from: ˊॱ reason: contains not printable characters */
    final Handler f269;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private Double f270;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private Map<String, String> f271;

    /* renamed from: ͺ reason: contains not printable characters */
    private boolean f272;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ reason: contains not printable characters */
    public VideoTrackerListener f273;

    /* renamed from: ॱˋ reason: contains not printable characters */
    private final Set<MoatAdEventType> f274;

    /* renamed from: ॱˎ reason: contains not printable characters */
    private final String f275;
    /* access modifiers changed from: private */

    /* renamed from: ᐝॱ reason: contains not printable characters */
    public final b f276 = new b(a.m214(), a.f251);

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊॱ reason: contains not printable characters */
    public abstract Map<String, Object> m242() throws o;

    d(String str) {
        super(null, false, true);
        b.m223(3, "BaseVideoTracker", this, "Initializing.");
        this.f275 = str;
        try {
            super.m233(this.f276.f247);
        } catch (o e) {
            this.f260 = e;
        }
        this.f267 = new HashMap();
        this.f274 = new HashSet();
        this.f269 = new Handler();
        this.f272 = false;
        this.f270 = Double.valueOf(1.0d);
    }

    public void setVideoListener(VideoTrackerListener videoTrackerListener) {
        this.f273 = videoTrackerListener;
    }

    public void removeVideoListener() {
        this.f273 = null;
    }

    @CallSuper
    /* renamed from: ˋ reason: contains not printable characters */
    public boolean m244(Map<String, String> map, View view) {
        try {
            m232();
            m235();
            if (view == null) {
                b.m223(3, "BaseVideoTracker", this, "trackVideoAd received null video view instance");
            }
            this.f271 = map;
            this.f268 = new WeakReference<>(view);
            m245();
            String format = String.format("trackVideoAd tracking ids: %s | view: %s", new Object[]{new JSONObject(map).toString(), b.m225(view)});
            b.m223(3, "BaseVideoTracker", this, format);
            StringBuilder sb = new StringBuilder();
            sb.append(m231());
            sb.append(" ");
            sb.append(format);
            b.m221("[SUCCESS] ", sb.toString());
            if (this.f258 != null) {
                this.f258.onTrackingStarted(m230());
            }
            return true;
        } catch (Exception e) {
            m237("trackVideoAd", e);
            return false;
        }
    }

    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(b.m225(view));
        b.m223(3, "BaseVideoTracker", this, sb.toString());
        this.f268 = new WeakReference<>(view);
        try {
            super.changeTargetView(view);
        } catch (Exception e) {
            o.m339(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public void m246(List<String> list) throws o {
        if (this.f271 == null) {
            list.add("Null adIds object");
        }
        if (list.isEmpty()) {
            super.m236(list);
            return;
        }
        throw new o(TextUtils.join(" and ", list));
    }

    public void stopTracking() {
        try {
            super.stopTracking();
            m249();
            if (this.f273 != null) {
                this.f273 = null;
            }
        } catch (Exception e) {
            o.m339(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏॱ reason: contains not printable characters */
    public final Double m247() {
        return this.f270;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m245() throws o {
        super.changeTargetView((View) this.f268.get());
        super.m234();
        HashMap r0 = m242();
        Integer num = (Integer) r0.get("width");
        Integer num2 = (Integer) r0.get("height");
        Integer num3 = (Integer) r0.get(IronSourceConstants.EVENTS_DURATION);
        b.m223(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "Player metadata: height = %d, width = %d, duration = %d", new Object[]{num2, num, num3}));
        this.f276.m227(this.f275, this.f271, num, num2, num3);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public JSONObject m243(MoatAdEvent moatAdEvent) {
        if (Double.isNaN(moatAdEvent.f231.doubleValue())) {
            moatAdEvent.f231 = this.f270;
        }
        return new JSONObject(moatAdEvent.m208());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˋ reason: contains not printable characters */
    public final void m249() {
        if (!this.f272) {
            this.f272 = true;
            this.f269.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        b.m223(3, "BaseVideoTracker", this, "Shutting down.");
                        b r0 = d.this.f276;
                        b.m223(3, "GlobalWebView", r0, "Cleaning up");
                        r0.f245.m272();
                        r0.f245 = null;
                        r0.f247.destroy();
                        r0.f247 = null;
                        d.this.f273 = null;
                    } catch (Exception e) {
                        o.m339(e);
                    }
                }
            }, 500);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ͺ reason: contains not printable characters */
    public final boolean m248() {
        return this.f267.containsKey(MoatAdEventType.AD_EVT_COMPLETE) || this.f267.containsKey(MoatAdEventType.AD_EVT_STOPPED) || this.f267.containsKey(MoatAdEventType.AD_EVT_SKIPPED);
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static boolean m240(Integer num, Integer num2) {
        int abs = Math.abs(num2.intValue() - num.intValue());
        double intValue = (double) num2.intValue();
        Double.isNaN(intValue);
        return ((double) abs) <= Math.min(750.0d, intValue * 0.05d);
    }

    public void setPlayerVolume(Double d) {
        Double valueOf = Double.valueOf(this.f270.doubleValue() * p.m350());
        if (!d.equals(this.f270)) {
            b.m223(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "player volume changed to %f ", new Object[]{d}));
            this.f270 = d;
            if (!valueOf.equals(Double.valueOf(this.f270.doubleValue() * p.m350()))) {
                dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_VOLUME_CHANGE, MoatAdEvent.f227, this.f270));
            }
        }
    }

    public void dispatchEvent(MoatAdEvent moatAdEvent) {
        try {
            JSONObject r0 = m243(moatAdEvent);
            boolean z = false;
            b.m223(3, "BaseVideoTracker", this, String.format("Received event: %s", new Object[]{r0.toString()}));
            StringBuilder sb = new StringBuilder();
            sb.append(m231());
            sb.append(String.format(" Received event: %s", new Object[]{r0.toString()}));
            b.m221("[SUCCESS] ", sb.toString());
            if (m238() && this.f262 != null) {
                this.f262.m274(this.f276.f244, r0);
                if (!this.f274.contains(moatAdEvent.f232)) {
                    this.f274.add(moatAdEvent.f232);
                    if (this.f273 != null) {
                        this.f273.onVideoEventReported(moatAdEvent.f232);
                    }
                }
            }
            MoatAdEventType moatAdEventType = moatAdEvent.f232;
            if (moatAdEventType == MoatAdEventType.AD_EVT_COMPLETE || moatAdEventType == MoatAdEventType.AD_EVT_STOPPED || moatAdEventType == MoatAdEventType.AD_EVT_SKIPPED) {
                z = true;
            }
            if (z) {
                this.f267.put(moatAdEventType, Integer.valueOf(1));
                if (this.f262 != null) {
                    this.f262.m277((c) this);
                }
                m249();
            }
        } catch (Exception e) {
            o.m339(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public final Double m250() {
        return Double.valueOf(this.f270.doubleValue() * p.m350());
    }
}
