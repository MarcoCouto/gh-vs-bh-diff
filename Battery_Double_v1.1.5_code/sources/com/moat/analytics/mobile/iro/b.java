package com.moat.analytics.mobile.iro;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class b {

    /* renamed from: ˊ reason: contains not printable characters */
    final String f244;

    /* renamed from: ˋ reason: contains not printable characters */
    f f245;

    /* renamed from: ˎ reason: contains not printable characters */
    private final int f246;

    /* renamed from: ˏ reason: contains not printable characters */
    WebView f247;
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public boolean f248;

    enum a {
        ;
        

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f251 = 2;

        /* renamed from: ˏ reason: contains not printable characters */
        public static final int f252 = 1;

        static {
            int[] iArr = {1, 2};
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    b(Application application, int i) {
        this.f246 = i;
        this.f248 = false;
        this.f244 = String.format(Locale.ROOT, "_moatTracker%d", new Object[]{Integer.valueOf((int) (Math.random() * 1.0E8d))});
        this.f247 = new WebView(application);
        WebSettings settings = this.f247.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setDatabaseEnabled(false);
        settings.setDomStorageEnabled(false);
        settings.setGeolocationEnabled(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setSaveFormData(false);
        if (VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(1);
        }
        int i2 = b.f304;
        if (i == a.f251) {
            i2 = b.f303;
        }
        try {
            this.f245 = new f(this.f247, i2);
        } catch (o e) {
            o.m339(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m226(String str) {
        if (this.f246 == a.f252) {
            this.f247.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!b.this.f248) {
                        try {
                            b.this.f248 = true;
                            b.this.f245.m278();
                        } catch (Exception e) {
                            o.m339(e);
                        }
                    }
                }
            });
            WebView webView = this.f247;
            StringBuilder sb = new StringBuilder("<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n   <meta charset=\"UTF-8\">\n   <title></title>\n</head>\n<body style=\"margin:0;padding:0;\">\n    <script src=\"https://z.moatads.com/");
            sb.append(str);
            sb.append("/moatad.js\" type=\"text/javascript\"></script>\n</body>\n</html>");
            webView.loadData(sb.toString(), WebRequest.CONTENT_TYPE_HTML, "utf-8");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m227(String str, Map<String, String> map, Integer num, Integer num2, Integer num3) {
        if (this.f246 == a.f251) {
            this.f247.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!b.this.f248) {
                        try {
                            b.this.f248 = true;
                            b.this.f245.m278();
                            b.this.f245.m280(b.this.f244);
                        } catch (Exception e) {
                            o.m339(e);
                        }
                    }
                }
            });
            JSONObject jSONObject = new JSONObject(map);
            this.f247.loadData(String.format(Locale.ROOT, "<html><head></head><body><div id=\"%s\" style=\"width: %dpx; height: %dpx;\"></div><script>(function initMoatTracking(apiname, pcode, ids, duration) {var events = [];window[pcode + '_moatElToTrack'] = document.getElementById('%s');var moatapi = {'dropTime':%d,'adData': {'ids': ids, 'duration': duration, 'url': 'n/a'},'dispatchEvent': function(ev) {if (this.sendEvent) {if (events) { events.push(ev); ev = events; events = false; }this.sendEvent(ev);} else {events.push(ev);}},'dispatchMany': function(evs){for (var i=0, l=evs.length; i<l; i++) {this.dispatchEvent(evs[i]);}}};Object.defineProperty(window, apiname, {'value': moatapi});var s = document.createElement('script');s.src = 'https://z.moatads.com/' + pcode + '/moatvideo.js?' + apiname + '#' + apiname;document.body.appendChild(s);})('%s', '%s', %s, %s);</script></body></html>", new Object[]{"mianahwvc", num, num2, "mianahwvc", Long.valueOf(System.currentTimeMillis()), this.f244, str, jSONObject.toString(), num3}), WebRequest.CONTENT_TYPE_HTML, null);
        }
    }

    b() {
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static void m223(int i, String str, Object obj, String str2) {
        if (t.m378().f404) {
            if (obj == null) {
                StringBuilder sb = new StringBuilder("Moat");
                sb.append(str);
                Log.println(i, sb.toString(), String.format("message = %s", new Object[]{str2}));
                return;
            }
            StringBuilder sb2 = new StringBuilder("Moat");
            sb2.append(str);
            Log.println(i, sb2.toString(), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}));
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static void m224(String str, Object obj, String str2) {
        Object obj2;
        if (t.m378().f405) {
            StringBuilder sb = new StringBuilder("Moat");
            sb.append(str);
            String sb2 = sb.toString();
            String str3 = "id = %s, message = %s";
            Object[] objArr = new Object[2];
            if (obj == null) {
                obj2 = "null";
            } else {
                obj2 = Integer.valueOf(obj.hashCode());
            }
            objArr[0] = obj2;
            objArr[1] = str2;
            Log.println(2, sb2, String.format(str3, objArr));
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static void m219(String str, Object obj, String str2, Exception exc) {
        if (t.m378().f404) {
            StringBuilder sb = new StringBuilder("Moat");
            sb.append(str);
            Log.e(sb.toString(), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}), exc);
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static void m221(String str, String str2) {
        if (!t.m378().f404 && ((j) MoatAnalytics.getInstance()).f343) {
            int i = 2;
            if (str.equals("[ERROR] ")) {
                i = 6;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            Log.println(i, "MoatAnalytics", sb.toString());
        }
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static String m225(View view) {
        if (view == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(view.getClass().getSimpleName());
        sb.append("@");
        sb.append(view.hashCode());
        return sb.toString();
    }
}
