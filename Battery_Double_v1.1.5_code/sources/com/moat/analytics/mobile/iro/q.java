package com.moat.analytics.mobile.iro;

import android.media.MediaPlayer;
import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class q extends g implements NativeVideoTracker {

    /* renamed from: ͺ reason: contains not printable characters */
    private WeakReference<MediaPlayer> f383;

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final String m357() {
        return "NativeVideoTracker";
    }

    q(String str) {
        super(str);
        b.m223(3, "NativeVideoTracker", this, "In initialization method.");
        if (str == null || str.isEmpty()) {
            StringBuilder sb = new StringBuilder("PartnerCode is ");
            sb.append(str == null ? "null" : "empty");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder("NativeDisplayTracker creation problem, ");
            sb3.append(sb2);
            String sb4 = sb3.toString();
            b.m223(3, "NativeVideoTracker", this, sb4);
            b.m221("[ERROR] ", sb4);
            this.f260 = new o(sb2);
        }
        b.m221("[SUCCESS] ", "NativeVideoTracker created");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˊ reason: contains not printable characters */
    public final boolean m361() {
        return (this.f383 == null || this.f383.get() == null) ? false : true;
    }

    public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
        try {
            m232();
            m235();
            if (mediaPlayer != null) {
                mediaPlayer.getCurrentPosition();
                this.f383 = new WeakReference<>(mediaPlayer);
                return super.m284(map, view);
            }
            throw new o("Null player instance");
        } catch (Exception unused) {
            throw new o("Playback has already completed");
        } catch (Exception e) {
            o.m339(e);
            String r2 = o.m338("trackVideoAd", e);
            if (this.f258 != null) {
                this.f258.onTrackingFailedToStart(r2);
            }
            b.m223(3, "NativeVideoTracker", this, r2);
            StringBuilder sb = new StringBuilder("NativeVideoTracker ");
            sb.append(r2);
            b.m221("[ERROR] ", sb.toString());
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋॱ reason: contains not printable characters */
    public final Integer m359() {
        return Integer.valueOf(((MediaPlayer) this.f383.get()).getCurrentPosition());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˎ reason: contains not printable characters */
    public final boolean m362() {
        return ((MediaPlayer) this.f383.get()).isPlaying();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻॱ reason: contains not printable characters */
    public final Integer m356() {
        return Integer.valueOf(((MediaPlayer) this.f383.get()).getDuration());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊॱ reason: contains not printable characters */
    public final Map<String, Object> m358() throws o {
        MediaPlayer mediaPlayer = (MediaPlayer) this.f383.get();
        HashMap hashMap = new HashMap();
        hashMap.put("width", Integer.valueOf(mediaPlayer.getVideoWidth()));
        hashMap.put("height", Integer.valueOf(mediaPlayer.getVideoHeight()));
        hashMap.put(IronSourceConstants.EVENTS_DURATION, Integer.valueOf(mediaPlayer.getDuration()));
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final void m360(List<String> list) throws o {
        if (!((this.f383 == null || this.f383.get() == null) ? false : true)) {
            list.add("Player is null");
        }
        super.m246(list);
    }
}
