package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.support.annotation.FloatRange;
import android.telephony.TelephonyManager;
import com.chartboost.sdk.impl.b;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import java.lang.ref.WeakReference;

final class r {

    /* renamed from: ʻ reason: contains not printable characters */
    private static int f156 = 1;

    /* renamed from: ˊ reason: contains not printable characters */
    private static e f157 = null;

    /* renamed from: ˋ reason: contains not printable characters */
    private static d f158 = null;

    /* renamed from: ˎ reason: contains not printable characters */
    private static int f159 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˏ reason: contains not printable characters */
    public static String f160;

    /* renamed from: ॱ reason: contains not printable characters */
    private static int[] f161 = {-39340411, 1646369784, -593413711, -1069164445, -50787683, -1327220997, 423245644, -742130253, 54775946, -495304555, 1880137505, 1742082653, 65717847, 1497802820, 828947133, -614454858, 941569790, -1897799303};

    static class d {

        /* renamed from: ʽ reason: contains not printable characters */
        boolean f163;

        /* renamed from: ˊ reason: contains not printable characters */
        boolean f164;

        /* renamed from: ˋ reason: contains not printable characters */
        boolean f165;

        /* renamed from: ˎ reason: contains not printable characters */
        String f166;

        /* renamed from: ˏ reason: contains not printable characters */
        String f167;

        /* renamed from: ॱ reason: contains not printable characters */
        Integer f168;

        /* synthetic */ d(byte b) {
            this();
        }

        private d() {
            this.f166 = "_unknown_";
            this.f167 = "_unknown_";
            this.f168 = Integer.valueOf(-1);
            this.f165 = false;
            this.f164 = false;
            this.f163 = false;
            try {
                Context r0 = r.m157();
                if (r0 != null) {
                    this.f163 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService(PlaceFields.PHONE);
                    this.f166 = telephonyManager.getSimOperatorName();
                    this.f167 = telephonyManager.getNetworkOperatorName();
                    this.f168 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f165 = r.m148();
                    this.f164 = r.m153(r0);
                }
            } catch (Exception e) {
                o.m134(e);
            }
        }
    }

    static class e {

        /* renamed from: ˊ reason: contains not printable characters */
        private String f169;

        /* renamed from: ˋ reason: contains not printable characters */
        private String f170;
        /* access modifiers changed from: private */

        /* renamed from: ˏ reason: contains not printable characters */
        public boolean f171;

        /* renamed from: ॱ reason: contains not printable characters */
        private String f172;

        /* synthetic */ e(byte b) {
            this();
        }

        private e() {
            this.f171 = false;
            this.f169 = "_unknown_";
            this.f170 = "_unknown_";
            this.f172 = "_unknown_";
            try {
                Context r0 = r.m157();
                if (r0 != null) {
                    this.f171 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f170 = r0.getPackageName();
                    this.f169 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f172 = packageManager.getInstallerPackageName(this.f170);
                    return;
                }
                a.m10(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                o.m134(e);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˎ reason: contains not printable characters */
        public final String m162() {
            return this.f169;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˋ reason: contains not printable characters */
        public final String m161() {
            return this.f170;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ॱ reason: contains not printable characters */
        public final String m163() {
            return this.f172 != null ? this.f172 : "_unknown_";
        }
    }

    r() {
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    /* renamed from: ॱ reason: contains not printable characters */
    static double m158() {
        try {
            double r1 = (double) m149();
            double streamMaxVolume = (double) ((AudioManager) c.m31().getSystemService(m155(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamMaxVolume(3);
            Double.isNaN(r1);
            Double.isNaN(streamMaxVolume);
            return r1 / streamMaxVolume;
        } catch (Exception e2) {
            o.m134(e2);
            return Utils.DOUBLE_EPSILON;
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private static int m149() {
        try {
            return ((AudioManager) c.m31().getSystemService(m155(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamVolume(3);
        } catch (Exception e2) {
            o.m134(e2);
            return 0;
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static void m156(final Application application) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(application);
                        if (!advertisingIdInfo.isLimitAdTrackingEnabled()) {
                            r.f160 = advertisingIdInfo.getId();
                            StringBuilder sb = new StringBuilder("Retrieved Advertising ID = ");
                            sb.append(r.f160);
                            a.m10(3, "Util", this, sb.toString());
                            return;
                        }
                        a.m10(3, "Util", this, "User has limited ad tracking");
                    } catch (Exception e) {
                        o.m134(e);
                    }
                }
            });
        } catch (Exception e2) {
            o.m134(e2);
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static String m154() {
        return f160;
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static Context m157() {
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f56;
        if (weakReference != null) {
            return (Context) weakReference.get();
        }
        return null;
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static e m151() {
        if (f157 == null || !f157.f171) {
            f157 = new e(0);
        }
        return f157;
    }

    /* JADX WARNING: type inference failed for: r0v1, types: [char[]] */
    /* JADX WARNING: type inference failed for: r1v2, types: [char[]] */
    /* JADX WARNING: type inference failed for: r6v3, types: [char, int] */
    /* JADX WARNING: type inference failed for: r7v1, types: [char, int] */
    /* JADX WARNING: type inference failed for: r9v0, types: [char] */
    /* JADX WARNING: type inference failed for: r10v0, types: [char] */
    /* JADX WARNING: type inference failed for: r8v1, types: [char] */
    /* JADX WARNING: type inference failed for: r7v3, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=null, for r10v0, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=null, for r7v3, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=null, for r8v1, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=null, for r9v0, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char[], code=null, for r0v1, types: [char[]] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char[], code=null, for r1v2, types: [char[]] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=null, for r6v3, types: [char, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=null, for r7v1, types: [char, int] */
    /* JADX WARNING: Unknown variable types count: 8 */
    /* renamed from: ˎ reason: contains not printable characters */
    private static String m155(int[] iArr, int i) {
        ? r0 = new char[4];
        ? r1 = new char[(iArr.length << 1)];
        int[] iArr2 = (int[]) f161.clone();
        int i2 = 0;
        while (true) {
            if (i2 >= iArr.length) {
                return new String(r1, 0, i);
            }
            r0[0] = iArr[i2] >>> 16;
            r0[1] = (char) iArr[i2];
            int i3 = i2 + 1;
            r0[2] = iArr[i3] >>> 16;
            r0[3] = (char) iArr[i3];
            b.a(r0, iArr2, false);
            int i4 = i2 << 1;
            r1[i4] = r0[0];
            r1[i4 + 1] = r0[1];
            r1[i4 + 2] = r0[2];
            r1[i4 + 3] = r0[3];
            i2 += 2;
        }
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static d m152() {
        if (f158 == null || !f158.f163) {
            f158 = new d(0);
        }
        return f158;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static boolean m153(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    /* renamed from: ʻ reason: contains not printable characters */
    static /* synthetic */ boolean m148() {
        int i;
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f56;
        Context context = weakReference != null ? (Context) weakReference.get() : null;
        if (context != null) {
            int i2 = f156 + 27;
            f159 = i2 % 128;
            int i3 = i2 % 2;
            if ((VERSION.SDK_INT < 17 ? (char) 22 : 19) != 22) {
                int i4 = f156 + 87;
                f159 = i4 % 128;
                int i5 = i4 % 2;
                i = Global.getInt(context.getContentResolver(), m155(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            } else {
                i = Secure.getInt(context.getContentResolver(), m155(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            }
        } else {
            i = 0;
        }
        if (!(i == 1)) {
            return false;
        }
        int i6 = f159 + 33;
        f156 = i6 % 128;
        int i7 = i6 % 2;
        return true;
    }
}
