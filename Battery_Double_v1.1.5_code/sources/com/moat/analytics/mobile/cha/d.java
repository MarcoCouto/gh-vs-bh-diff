package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class d {

    /* renamed from: ʻ reason: contains not printable characters */
    private WeakReference<View> f40;

    /* renamed from: ʼ reason: contains not printable characters */
    private final boolean f41;

    /* renamed from: ʽ reason: contains not printable characters */
    final boolean f42;

    /* renamed from: ˊ reason: contains not printable characters */
    TrackerListener f43;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private boolean f44;

    /* renamed from: ˋ reason: contains not printable characters */
    final String f45;

    /* renamed from: ˎ reason: contains not printable characters */
    j f46 = this.f32.f17;

    /* renamed from: ˏ reason: contains not printable characters */
    WeakReference<WebView> f47;

    /* renamed from: ͺ reason: contains not printable characters */
    private boolean f48;

    /* renamed from: ॱ reason: contains not printable characters */
    o f49 = null;

    /* renamed from: ᐝ reason: contains not printable characters */
    private final u f50;

    @Deprecated
    public void setActivity(Activity activity) {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public abstract String m40();

    d(@Nullable View view, boolean z, boolean z2) {
        String str;
        a.m10(3, "BaseTracker", this, "Initializing.");
        if (z) {
            StringBuilder sb = new StringBuilder("m");
            sb.append(hashCode());
            str = sb.toString();
        } else {
            str = "";
        }
        this.f45 = str;
        this.f40 = new WeakReference<>(view);
        this.f41 = z;
        this.f42 = z2;
        this.f44 = false;
        this.f48 = false;
        this.f50 = new u();
    }

    public void setListener(TrackerListener trackerListener) {
        this.f43 = trackerListener;
    }

    public void removeListener() {
        this.f43 = null;
    }

    public void startTracking() {
        try {
            a.m10(3, "BaseTracker", this, "In startTracking method.");
            m43();
            if (this.f43 != null) {
                TrackerListener trackerListener = this.f43;
                StringBuilder sb = new StringBuilder("Tracking started on ");
                sb.append(a.m9((View) this.f40.get()));
                trackerListener.onTrackingStarted(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder("startTracking succeeded for ");
            sb2.append(a.m9((View) this.f40.get()));
            String sb3 = sb2.toString();
            a.m10(3, "BaseTracker", this, sb3);
            StringBuilder sb4 = new StringBuilder();
            sb4.append(m40());
            sb4.append(" ");
            sb4.append(sb3);
            a.m7("[SUCCESS] ", sb4.toString());
        } catch (Exception e) {
            m46("startTracking", e);
        }
    }

    @CallSuper
    public void stopTracking() {
        boolean z = false;
        try {
            a.m10(3, "BaseTracker", this, "In stopTracking method.");
            this.f48 = true;
            if (this.f46 != null) {
                this.f46.m100(this);
                z = true;
            }
        } catch (Exception e) {
            o.m134(e);
        }
        String str = "BaseTracker";
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        a.m10(3, str, this, sb.toString());
        String str2 = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m40());
        sb2.append(" stopTracking ");
        sb2.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : ParametersKeys.FAILED);
        sb2.append(" for ");
        sb2.append(a.m9((View) this.f40.get()));
        a.m7(str2, sb2.toString());
        if (this.f43 != null) {
            this.f43.onTrackingStopped("");
            this.f43 = null;
        }
    }

    @CallSuper
    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(a.m9(view));
        a.m10(3, "BaseTracker", this, sb.toString());
        this.f40 = new WeakReference<>(view);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˏ reason: contains not printable characters */
    public void m43() throws o {
        a.m10(3, "BaseTracker", this, "Attempting to start impression.");
        m42();
        if (this.f44) {
            throw new o("Tracker already started");
        } else if (!this.f48) {
            m41(new ArrayList());
            if (this.f46 != null) {
                this.f46.m101(this);
                this.f44 = true;
                a.m10(3, "BaseTracker", this, "Impression started.");
                return;
            }
            a.m10(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new o("Bridge is null");
        } else {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m45(WebView webView) throws o {
        if (webView != null) {
            this.f47 = new WeakReference<>(webView);
            if (this.f46 == null) {
                if (!(this.f41 || this.f42)) {
                    a.m10(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f47.get() != null) {
                        this.f46 = new j((WebView) this.f47.get(), e.f119);
                        a.m10(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f46 = null;
                        a.m10(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            if (this.f46 != null) {
                this.f46.m99(this);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m42() throws o {
        if (this.f49 != null) {
            StringBuilder sb = new StringBuilder("Tracker initialization failed: ");
            sb.append(this.f49.getMessage());
            throw new o(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final boolean m39() {
        return this.f44 && !this.f48;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʼ reason: contains not printable characters */
    public final View m37() {
        return (View) this.f40.get();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʽ reason: contains not printable characters */
    public final String m38() {
        this.f50.m200(this.f45, (View) this.f40.get());
        return this.f50.f212;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m46(String str, Exception exc) {
        try {
            o.m134(exc);
            String r3 = o.m133(str, exc);
            if (this.f43 != null) {
                this.f43.onTrackingFailedToStart(r3);
            }
            a.m10(3, "BaseTracker", this, r3);
            StringBuilder sb = new StringBuilder();
            sb.append(m40());
            sb.append(" ");
            sb.append(r3);
            a.m7("[ERROR] ", sb.toString());
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m44() throws o {
        if (this.f44) {
            throw new o("Tracker already started");
        } else if (this.f48) {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˋ reason: contains not printable characters */
    public void m41(List<String> list) throws o {
        if (((View) this.f40.get()) == null && !this.f42) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new o(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻ reason: contains not printable characters */
    public final String m36() {
        return a.m9((View) this.f40.get());
    }
}
