package com.moat.analytics.mobile.cha;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class a {

    /* renamed from: ˊ reason: contains not printable characters */
    final String f14;

    /* renamed from: ˋ reason: contains not printable characters */
    WebView f15;
    /* access modifiers changed from: private */

    /* renamed from: ˎ reason: contains not printable characters */
    public boolean f16;

    /* renamed from: ˏ reason: contains not printable characters */
    j f17;

    /* renamed from: ॱ reason: contains not printable characters */
    private final int f18;

    enum d {
        ;
        

        /* renamed from: ˋ reason: contains not printable characters */
        public static final int f21 = 2;

        /* renamed from: ˏ reason: contains not printable characters */
        public static final int f22 = 1;

        static {
            int[] iArr = {1, 2};
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    a(Application application, int i) {
        this.f18 = i;
        this.f16 = false;
        this.f14 = String.format(Locale.ROOT, "_moatTracker%d", new Object[]{Integer.valueOf((int) (Math.random() * 1.0E8d))});
        this.f15 = new WebView(application);
        WebSettings settings = this.f15.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setDatabaseEnabled(false);
        settings.setDomStorageEnabled(false);
        settings.setGeolocationEnabled(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setSaveFormData(false);
        if (VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(1);
        }
        int i2 = e.f118;
        if (i == d.f21) {
            i2 = e.f117;
        }
        try {
            this.f17 = new j(this.f15, i2);
        } catch (o e) {
            o.m134(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m14(String str) {
        if (this.f18 == d.f22) {
            this.f15.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f16) {
                        try {
                            a.this.f16 = true;
                            a.this.f17.m103();
                        } catch (Exception e) {
                            o.m134(e);
                        }
                    }
                }
            });
            WebView webView = this.f15;
            StringBuilder sb = new StringBuilder("<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n   <meta charset=\"UTF-8\">\n   <title></title>\n</head>\n<body style=\"margin:0;padding:0;\">\n    <script src=\"https://z.moatads.com/");
            sb.append(str);
            sb.append("/moatad.js\" type=\"text/javascript\"></script>\n</body>\n</html>");
            webView.loadData(sb.toString(), WebRequest.CONTENT_TYPE_HTML, "utf-8");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m15(String str, Map<String, String> map, Integer num, Integer num2, Integer num3) {
        if (this.f18 == d.f21) {
            this.f15.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f16) {
                        try {
                            a.this.f16 = true;
                            a.this.f17.m103();
                            a.this.f17.m102(a.this.f14);
                        } catch (Exception e) {
                            o.m134(e);
                        }
                    }
                }
            });
            JSONObject jSONObject = new JSONObject(map);
            this.f15.loadData(String.format(Locale.ROOT, "<html><head></head><body><div id=\"%s\" style=\"width: %dpx; height: %dpx;\"></div><script>(function initMoatTracking(apiname, pcode, ids, duration) {var events = [];window[pcode + '_moatElToTrack'] = document.getElementById('%s');var moatapi = {'dropTime':%d,'adData': {'ids': ids, 'duration': duration, 'url': 'n/a'},'dispatchEvent': function(ev) {if (this.sendEvent) {if (events) { events.push(ev); ev = events; events = false; }this.sendEvent(ev);} else {events.push(ev);}},'dispatchMany': function(evs){for (var i=0, l=evs.length; i<l; i++) {this.dispatchEvent(evs[i]);}}};Object.defineProperty(window, apiname, {'value': moatapi});var s = document.createElement('script');s.src = 'https://z.moatads.com/' + pcode + '/moatvideo.js?' + apiname + '#' + apiname;document.body.appendChild(s);})('%s', '%s', %s, %s);</script></body></html>", new Object[]{"mianahwvc", num, num2, "mianahwvc", Long.valueOf(System.currentTimeMillis()), this.f14, str, jSONObject.toString(), num3}), WebRequest.CONTENT_TYPE_HTML, null);
        }
    }

    a() {
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static void m10(int i, String str, Object obj, String str2) {
        if (t.m178().f182) {
            if (obj == null) {
                StringBuilder sb = new StringBuilder("Moat");
                sb.append(str);
                Log.println(i, sb.toString(), String.format("message = %s", new Object[]{str2}));
                return;
            }
            StringBuilder sb2 = new StringBuilder("Moat");
            sb2.append(str);
            Log.println(i, sb2.toString(), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}));
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static void m11(String str, Object obj, String str2) {
        Object obj2;
        if (t.m178().f179) {
            StringBuilder sb = new StringBuilder("Moat");
            sb.append(str);
            String sb2 = sb.toString();
            String str3 = "id = %s, message = %s";
            Object[] objArr = new Object[2];
            if (obj == null) {
                obj2 = "null";
            } else {
                obj2 = Integer.valueOf(obj.hashCode());
            }
            objArr[0] = obj2;
            objArr[1] = str2;
            Log.println(2, sb2, String.format(str3, objArr));
        }
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static void m12(String str, Object obj, String str2, Exception exc) {
        if (t.m178().f182) {
            StringBuilder sb = new StringBuilder("Moat");
            sb.append(str);
            Log.e(sb.toString(), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}), exc);
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static void m7(String str, String str2) {
        if (!t.m178().f182 && ((f) MoatAnalytics.getInstance()).f58) {
            int i = 2;
            if (str.equals("[ERROR] ")) {
                i = 6;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            Log.println(i, "MoatAnalytics", sb.toString());
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static String m9(View view) {
        if (view == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(view.getClass().getSimpleName());
        sb.append("@");
        sb.append(view.hashCode());
        return sb.toString();
    }
}
