package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class c {
    /* access modifiers changed from: private */

    /* renamed from: ˊ reason: contains not printable characters */
    public static boolean f35 = false;

    /* renamed from: ˋ reason: contains not printable characters */
    private static Application f36 = null;

    /* renamed from: ˎ reason: contains not printable characters */
    private static boolean f37 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    static WeakReference<Activity> f38;
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public static int f39;

    static class a implements ActivityLifecycleCallbacks {
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        a() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            c.f39 = 1;
        }

        public final void onActivityStarted(Activity activity) {
            try {
                c.f38 = new WeakReference<>(activity);
                c.f39 = 2;
                if (!c.f35) {
                    m35(true);
                }
                c.f35 = true;
                StringBuilder sb = new StringBuilder("Activity started: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m10(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m134(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                c.f38 = new WeakReference<>(activity);
                c.f39 = 3;
                t.m178().m184();
                StringBuilder sb = new StringBuilder("Activity resumed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m10(3, "ActivityState", this, sb.toString());
                if (((f) MoatAnalytics.getInstance()).f59) {
                    e.m47(activity);
                }
            } catch (Exception e) {
                o.m134(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                c.f39 = 4;
                if (c.m29(activity)) {
                    c.f38 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity paused: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m10(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m134(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (c.f39 != 3) {
                    c.f35 = false;
                    m35(false);
                }
                c.f39 = 5;
                if (c.m29(activity)) {
                    c.f38 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity stopped: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m10(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m134(e);
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(c.f39 == 3 || c.f39 == 5)) {
                    if (c.f35) {
                        m35(false);
                    }
                    c.f35 = false;
                }
                c.f39 = 6;
                StringBuilder sb = new StringBuilder("Activity destroyed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m10(3, "ActivityState", this, sb.toString());
                if (c.m29(activity)) {
                    c.f38 = new WeakReference<>(null);
                }
            } catch (Exception e) {
                o.m134(e);
            }
        }

        /* renamed from: ॱ reason: contains not printable characters */
        private static void m35(boolean z) {
            if (z) {
                a.m10(3, "ActivityState", null, "App became visible");
                if (t.m178().f181 == a.f192 && !((f) MoatAnalytics.getInstance()).f57) {
                    n.m120().m130();
                }
            } else {
                a.m10(3, "ActivityState", null, "App became invisible");
                if (t.m178().f181 == a.f192 && !((f) MoatAnalytics.getInstance()).f57) {
                    n.m120().m131();
                }
            }
        }
    }

    c() {
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static void m33(Application application) {
        f36 = application;
        if (!f37) {
            f37 = true;
            f36.registerActivityLifecycleCallbacks(new a());
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static Application m31() {
        return f36;
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static /* synthetic */ boolean m29(Activity activity) {
        return f38 != null && f38.get() == activity;
    }
}
