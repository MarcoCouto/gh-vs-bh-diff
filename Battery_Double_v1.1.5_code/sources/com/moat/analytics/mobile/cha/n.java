package com.moat.analytics.mobile.cha;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.github.mikephil.charting.utils.Utils;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class n implements LocationListener {

    /* renamed from: ˎ reason: contains not printable characters */
    private static n f130;

    /* renamed from: ʻ reason: contains not printable characters */
    private Location f131;

    /* renamed from: ˊ reason: contains not printable characters */
    private ScheduledExecutorService f132;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private boolean f133;

    /* renamed from: ˋ reason: contains not printable characters */
    private ScheduledFuture<?> f134;

    /* renamed from: ˏ reason: contains not printable characters */
    private ScheduledFuture<?> f135;

    /* renamed from: ॱ reason: contains not printable characters */
    private LocationManager f136;

    /* renamed from: ᐝ reason: contains not printable characters */
    private boolean f137;

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static n m120() {
        if (f130 == null) {
            f130 = new n();
        }
        return f130;
    }

    private n() {
        try {
            this.f133 = ((f) MoatAnalytics.getInstance()).f57;
            if (this.f133) {
                a.m10(3, "LocationManager", this, "Moat location services disabled");
                return;
            }
            this.f132 = Executors.newScheduledThreadPool(1);
            this.f136 = (LocationManager) c.m31().getSystemService("location");
            if (this.f136.getAllProviders().size() == 0) {
                a.m10(3, "LocationManager", this, "Device has no location providers");
            } else {
                m118();
            }
        } catch (Exception e) {
            o.m134(e);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: ˊ reason: contains not printable characters */
    public final Location m129() {
        if (this.f133 || this.f136 == null) {
            return null;
        }
        return this.f131;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m130() {
        m118();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m131() {
        m125(false);
    }

    public final void onLocationChanged(Location location) {
        String str = "LocationManager";
        try {
            StringBuilder sb = new StringBuilder("Received an updated location = ");
            sb.append(location.toString());
            a.m10(3, str, this, sb.toString());
            float currentTimeMillis = (float) ((System.currentTimeMillis() - location.getTime()) / 1000);
            if (location.hasAccuracy() && location.getAccuracy() <= 100.0f && currentTimeMillis < 600.0f) {
                this.f131 = m123(this.f131, location);
                a.m10(3, "LocationManager", this, "fetchCompleted");
                m125(true);
            }
        } catch (Exception e) {
            o.m134(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ reason: contains not printable characters */
    public void m118() {
        try {
            if (!this.f133) {
                if (this.f136 != null) {
                    if (this.f137) {
                        a.m10(3, "LocationManager", this, "already updating location");
                    }
                    a.m10(3, "LocationManager", this, "starting location fetch");
                    this.f131 = m123(this.f131, m115());
                    if (this.f131 != null) {
                        StringBuilder sb = new StringBuilder("Have a valid location, won't fetch = ");
                        sb.append(this.f131.toString());
                        a.m10(3, "LocationManager", this, sb.toString());
                        m121();
                        return;
                    }
                    m113();
                }
            }
        } catch (Exception e) {
            o.m134(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ reason: contains not printable characters */
    public void m125(boolean z) {
        try {
            a.m10(3, "LocationManager", this, "stopping location fetch");
            m114();
            m117();
            if (z) {
                m121();
            } else {
                m128();
            }
        } catch (Exception e) {
            o.m134(e);
        }
    }

    /* renamed from: ʽ reason: contains not printable characters */
    private Location m115() {
        Location location;
        try {
            boolean r1 = m122();
            boolean r2 = m127();
            if (r1 && r2) {
                location = m123(this.f136.getLastKnownLocation("gps"), this.f136.getLastKnownLocation("network"));
            } else if (r1) {
                location = this.f136.getLastKnownLocation("gps");
            } else if (!r2) {
                return null;
            } else {
                location = this.f136.getLastKnownLocation("network");
            }
            return location;
        } catch (SecurityException e) {
            o.m134(e);
            return null;
        }
    }

    /* renamed from: ʻ reason: contains not printable characters */
    private void m113() {
        try {
            if (!this.f137) {
                a.m10(3, "LocationManager", this, "Attempting to start update");
                if (m122()) {
                    a.m10(3, "LocationManager", this, "start updating gps location");
                    this.f136.requestLocationUpdates("gps", 0, 0.0f, this, Looper.getMainLooper());
                    this.f137 = true;
                }
                if (m127()) {
                    a.m10(3, "LocationManager", this, "start updating network location");
                    this.f136.requestLocationUpdates("network", 0, 0.0f, this, Looper.getMainLooper());
                    this.f137 = true;
                }
                if (this.f137) {
                    m117();
                    this.f135 = this.f132.schedule(new Runnable() {
                        public final void run() {
                            try {
                                a.m10(3, "LocationManager", this, "fetchTimedOut");
                                n.this.m125(true);
                            } catch (Exception e) {
                                o.m134(e);
                            }
                        }
                    }, 60, TimeUnit.SECONDS);
                }
            }
        } catch (SecurityException e) {
            o.m134(e);
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private void m114() {
        try {
            a.m10(3, "LocationManager", this, "Stopping to update location");
            boolean z = true;
            if (!(ContextCompat.checkSelfPermission(c.m31().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
                if (!(ContextCompat.checkSelfPermission(c.m31().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                    z = false;
                }
            }
            if (z && this.f136 != null) {
                this.f136.removeUpdates(this);
                this.f137 = false;
            }
        } catch (SecurityException e) {
            o.m134(e);
        }
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    private void m117() {
        if (this.f135 != null && !this.f135.isCancelled()) {
            this.f135.cancel(true);
            this.f135 = null;
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private void m128() {
        if (this.f134 != null && !this.f134.isCancelled()) {
            this.f134.cancel(true);
            this.f134 = null;
        }
    }

    /* renamed from: ˏॱ reason: contains not printable characters */
    private void m121() {
        a.m10(3, "LocationManager", this, "Resetting fetch timer");
        m128();
        float f = 600.0f;
        if (this.f131 != null) {
            f = Math.max(600.0f - ((float) ((System.currentTimeMillis() - this.f131.getTime()) / 1000)), 0.0f);
        }
        this.f134 = this.f132.schedule(new Runnable() {
            public final void run() {
                try {
                    a.m10(3, "LocationManager", this, "fetchTimerCompleted");
                    n.this.m118();
                } catch (Exception e) {
                    o.m134(e);
                }
            }
        }, (long) f, TimeUnit.SECONDS);
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static Location m123(Location location, Location location2) {
        boolean r0 = m126(location);
        boolean r1 = m126(location2);
        if (r0) {
            return (r1 && location.getAccuracy() >= location.getAccuracy()) ? location2 : location;
        }
        if (!r1) {
            return null;
        }
        return location2;
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static boolean m126(Location location) {
        if (location == null) {
            return false;
        }
        if ((location.getLatitude() != Utils.DOUBLE_EPSILON || location.getLongitude() != Utils.DOUBLE_EPSILON) && location.getAccuracy() >= 0.0f && ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)) < 600.0f) {
            return true;
        }
        return false;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static boolean m119(Location location, Location location2) {
        if (location == location2) {
            return true;
        }
        return (location == null || location2 == null || location.getTime() != location2.getTime()) ? false : true;
    }

    /* renamed from: ͺ reason: contains not printable characters */
    private boolean m122() {
        return (ContextCompat.checkSelfPermission(c.m31().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) && this.f136.getProvider("gps") != null && this.f136.isProviderEnabled("gps");
    }

    /* renamed from: ॱˊ reason: contains not printable characters */
    private boolean m127() {
        boolean z;
        if (!(ContextCompat.checkSelfPermission(c.m31().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
            if (!(ContextCompat.checkSelfPermission(c.m31().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                z = false;
                return !z && this.f136.getProvider("network") != null && this.f136.isProviderEnabled("network");
            }
        }
        z = true;
        if (!z) {
        }
    }
}
