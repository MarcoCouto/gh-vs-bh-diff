package com.moat.analytics.mobile.cha;

import android.support.annotation.VisibleForTesting;
import com.moat.analytics.mobile.cha.base.asserts.Asserts;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class p<T> implements InvocationHandler {
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public static final Object[] f143 = new Object[0];

    /* renamed from: ˊ reason: contains not printable characters */
    private final c<T> f144;

    /* renamed from: ˎ reason: contains not printable characters */
    private boolean f145;

    /* renamed from: ˏ reason: contains not printable characters */
    private final LinkedList<d> f146 = new LinkedList<>();

    /* renamed from: ॱ reason: contains not printable characters */
    private final Class<T> f147;

    /* renamed from: ᐝ reason: contains not printable characters */
    private T f148;

    interface c<T> {
        /* renamed from: ˋ reason: contains not printable characters */
        Optional<T> m142() throws o;
    }

    class d {
        /* access modifiers changed from: private */

        /* renamed from: ˊ reason: contains not printable characters */
        public final WeakReference[] f150;
        /* access modifiers changed from: private */

        /* renamed from: ˋ reason: contains not printable characters */
        public final Method f151;

        /* renamed from: ˎ reason: contains not printable characters */
        private final LinkedList<Object> f152;

        /* synthetic */ d(p pVar, Method method, Object[] objArr, byte b) {
            this(method, objArr);
        }

        private d(Method method, Object... objArr) {
            this.f152 = new LinkedList<>();
            if (objArr == null) {
                objArr = p.f143;
            }
            WeakReference[] weakReferenceArr = new WeakReference[objArr.length];
            int length = objArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Object obj = objArr[i];
                if ((obj instanceof Map) || (obj instanceof Integer) || (obj instanceof Double)) {
                    this.f152.add(obj);
                }
                int i3 = i2 + 1;
                weakReferenceArr[i2] = new WeakReference(obj);
                i++;
                i2 = i3;
            }
            this.f150 = weakReferenceArr;
            this.f151 = method;
        }
    }

    @VisibleForTesting
    private p(c<T> cVar, Class<T> cls) throws o {
        Asserts.checkNotNull(cVar);
        Asserts.checkNotNull(cls);
        this.f144 = cVar;
        this.f147 = cls;
        t.m178().m183((b) new b() {
            /* renamed from: ˎ reason: contains not printable characters */
            public final void m141() throws o {
                p.this.m138();
            }
        });
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static <T> T m137(c<T> cVar, Class<T> cls) throws o {
        ClassLoader classLoader = cls.getClassLoader();
        p pVar = new p(cVar, cls);
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, pVar);
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static Boolean m136(Method method) {
        try {
            if (Boolean.TYPE.equals(method.getReturnType())) {
                return Boolean.valueOf(true);
            }
        } catch (Exception e) {
            o.m134(e);
        }
        return null;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            Class declaringClass = method.getDeclaringClass();
            t r0 = t.m178();
            if (Object.class.equals(declaringClass)) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    return this.f147;
                }
                if (!"toString".equals(name)) {
                    return method.invoke(this, objArr);
                }
                Object invoke = method.invoke(this, objArr);
                return String.valueOf(invoke).replace(p.class.getName(), this.f147.getName());
            } else if (!this.f145 || this.f148 != null) {
                if (r0.f181 == a.f192) {
                    m138();
                    if (this.f148 != null) {
                        return method.invoke(this.f148, objArr);
                    }
                }
                if (r0.f181 == a.f193 && (!this.f145 || this.f148 != null)) {
                    if (this.f146.size() >= 15) {
                        this.f146.remove(5);
                    }
                    this.f146.add(new d(this, method, objArr, 0));
                }
                return m136(method);
            } else {
                this.f146.clear();
                return m136(method);
            }
        } catch (Exception e) {
            o.m134(e);
            return m136(method);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ reason: contains not printable characters */
    public void m138() throws o {
        if (!this.f145) {
            try {
                this.f148 = this.f144.m142().orElse(null);
            } catch (Exception e) {
                a.m12("OnOffTrackerProxy", this, "Could not create instance", e);
                o.m134(e);
            }
            this.f145 = true;
        }
        if (this.f148 != null) {
            Iterator it = this.f146.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                try {
                    Object[] objArr = new Object[dVar.f150.length];
                    WeakReference[] r3 = dVar.f150;
                    int length = r3.length;
                    int i = 0;
                    int i2 = 0;
                    while (i < length) {
                        int i3 = i2 + 1;
                        objArr[i2] = r3[i].get();
                        i++;
                        i2 = i3;
                    }
                    dVar.f151.invoke(this.f148, objArr);
                } catch (Exception e2) {
                    o.m134(e2);
                }
            }
            this.f146.clear();
        }
    }
}
