package com.moat.analytics.mobile.cha;

interface l<T> {
    T create() throws o;

    T createNoOp();
}
