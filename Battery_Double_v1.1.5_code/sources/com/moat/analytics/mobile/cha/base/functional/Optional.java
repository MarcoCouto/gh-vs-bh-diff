package com.moat.analytics.mobile.cha.base.functional;

import java.util.NoSuchElementException;

public final class Optional<T> {

    /* renamed from: ˏ reason: contains not printable characters */
    private static final Optional<?> f224 = new Optional<>();

    /* renamed from: ॱ reason: contains not printable characters */
    private final T f225;

    private Optional() {
        this.f225 = null;
    }

    public static <T> Optional<T> empty() {
        return f224;
    }

    private Optional(T t) {
        if (t != null) {
            this.f225 = t;
            return;
        }
        throw new NullPointerException("Optional of null value.");
    }

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public final T get() {
        if (this.f225 != null) {
            return this.f225;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.f225 != null;
    }

    public final T orElse(T t) {
        return this.f225 != null ? this.f225 : t;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        Optional optional = (Optional) obj;
        return this.f225 == optional.f225 || !(this.f225 == null || optional.f225 == null || !this.f225.equals(optional.f225));
    }

    public final int hashCode() {
        if (this.f225 == null) {
            return 0;
        }
        return this.f225.hashCode();
    }

    public final String toString() {
        if (this.f225 == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", new Object[]{this.f225});
    }
}
