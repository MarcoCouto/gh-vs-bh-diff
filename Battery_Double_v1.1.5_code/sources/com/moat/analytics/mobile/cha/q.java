package com.moat.analytics.mobile.cha;

import android.graphics.Rect;
import android.view.View;
import com.moat.analytics.mobile.cha.NativeDisplayTracker.MoatUserInteractionType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class q extends d implements NativeDisplayTracker {

    /* renamed from: ˊॱ reason: contains not printable characters */
    private final Map<String, String> f154;

    /* renamed from: ᐝ reason: contains not printable characters */
    private final Set<MoatUserInteractionType> f155 = new HashSet();

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final String m147() {
        return "NativeDisplayTracker";
    }

    q(View view, Map<String, String> map) {
        super(view, true, false);
        a.m10(3, "NativeDisplayTracker", this, "Initializing.");
        this.f154 = map;
        if (view == null) {
            String str = "Target view is null";
            StringBuilder sb = new StringBuilder("NativeDisplayTracker initialization not successful, ");
            sb.append(str);
            String sb2 = sb.toString();
            a.m10(3, "NativeDisplayTracker", this, sb2);
            a.m7("[ERROR] ", sb2);
            this.f49 = new o(str);
        } else if (map == null || map.isEmpty()) {
            StringBuilder sb3 = new StringBuilder("NativeDisplayTracker initialization not successful, ");
            sb3.append("AdIds is null or empty");
            String sb4 = sb3.toString();
            a.m10(3, "NativeDisplayTracker", this, sb4);
            a.m7("[ERROR] ", sb4);
            this.f49 = new o("AdIds is null or empty");
        } else {
            a aVar = ((f) f.getInstance()).f60;
            if (aVar == null) {
                String str2 = "prepareNativeDisplayTracking was not called successfully";
                StringBuilder sb5 = new StringBuilder("NativeDisplayTracker initialization not successful, ");
                sb5.append(str2);
                String sb6 = sb5.toString();
                a.m10(3, "NativeDisplayTracker", this, sb6);
                a.m7("[ERROR] ", sb6);
                this.f49 = new o(str2);
                return;
            }
            this.f46 = aVar.f17;
            try {
                super.m45(aVar.f15);
                if (this.f46 != null) {
                    this.f46.m104(m145());
                }
                StringBuilder sb7 = new StringBuilder("NativeDisplayTracker created for ");
                sb7.append(m36());
                sb7.append(", with adIds:");
                sb7.append(map.toString());
                a.m7("[SUCCESS] ", sb7.toString());
            } catch (o e) {
                this.f49 = e;
            }
        }
    }

    public final void reportUserInteractionEvent(MoatUserInteractionType moatUserInteractionType) {
        String str = "NativeDisplayTracker";
        try {
            StringBuilder sb = new StringBuilder("reportUserInteractionEvent:");
            sb.append(moatUserInteractionType.name());
            a.m10(3, str, this, sb.toString());
            if (!this.f155.contains(moatUserInteractionType)) {
                this.f155.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.f45);
                jSONObject.accumulate("event", moatUserInteractionType.name().toLowerCase());
                if (this.f46 != null) {
                    this.f46.m105(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            a.m11("NativeDisplayTracker", this, "Got JSON exception");
            o.m134(e);
        } catch (Exception e2) {
            o.m134(e2);
        }
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    private String m145() {
        String str = "";
        try {
            Map<String, String> map = this.f154;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 0; i < 8; i++) {
                StringBuilder sb = new StringBuilder("moatClientLevel");
                sb.append(i);
                String sb2 = sb.toString();
                if (map.containsKey(sb2)) {
                    linkedHashMap.put(sb2, map.get(sb2));
                }
            }
            for (int i2 = 0; i2 < 8; i2++) {
                StringBuilder sb3 = new StringBuilder("moatClientSlicer");
                sb3.append(i2);
                String sb4 = sb3.toString();
                if (map.containsKey(sb4)) {
                    linkedHashMap.put(sb4, map.get(sb4));
                }
            }
            for (String str2 : map.keySet()) {
                if (!linkedHashMap.containsKey(str2)) {
                    linkedHashMap.put(str2, (String) map.get(str2));
                }
            }
            String jSONObject = new JSONObject(linkedHashMap).toString();
            StringBuilder sb5 = new StringBuilder("Parsed ad ids = ");
            sb5.append(jSONObject);
            a.m10(3, "NativeDisplayTracker", this, sb5.toString());
            StringBuilder sb6 = new StringBuilder("{\"adIds\":");
            sb6.append(jSONObject);
            sb6.append(", \"adKey\":\"");
            sb6.append(this.f45);
            sb6.append("\", \"adSize\":");
            sb6.append(m146());
            sb6.append("}");
            return sb6.toString();
        } catch (Exception e) {
            o.m134(e);
            return str;
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private String m146() {
        try {
            Rect r0 = u.m193(super.m37());
            int width = r0.width();
            int height = r0.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            o.m134(e);
            return null;
        }
    }
}
