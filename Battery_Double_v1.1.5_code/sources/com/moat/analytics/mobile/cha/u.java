package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.graphics.Rect;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.utils.Utils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

final class u {

    /* renamed from: ʼ reason: contains not printable characters */
    private static int f203 = 0;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private static int f204 = 1;

    /* renamed from: ʻ reason: contains not printable characters */
    private Location f205;

    /* renamed from: ʽ reason: contains not printable characters */
    private JSONObject f206;

    /* renamed from: ˊ reason: contains not printable characters */
    private Rect f207;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private JSONObject f208;

    /* renamed from: ˋ reason: contains not printable characters */
    private JSONObject f209;

    /* renamed from: ˎ reason: contains not printable characters */
    private Rect f210;

    /* renamed from: ˏ reason: contains not printable characters */
    private c f211 = new c();

    /* renamed from: ॱ reason: contains not printable characters */
    String f212 = "{}";

    /* renamed from: ᐝ reason: contains not printable characters */
    private Map<String, Object> f213 = new HashMap();

    static class a {

        /* renamed from: ˎ reason: contains not printable characters */
        final Rect f214;

        /* renamed from: ˏ reason: contains not printable characters */
        final View f215;

        a(View view, a aVar) {
            this.f215 = view;
            if (aVar != null) {
                int i = aVar.f214.left;
                int left = i + view.getLeft();
                int top = aVar.f214.top + view.getTop();
                this.f214 = new Rect(left, top, view.getWidth() + left, view.getHeight() + top);
                return;
            }
            this.f214 = u.m198(view);
        }
    }

    static class b {

        /* renamed from: ˊ reason: contains not printable characters */
        boolean f216 = false;

        /* renamed from: ˋ reason: contains not printable characters */
        final Set<Rect> f217 = new HashSet();

        /* renamed from: ˎ reason: contains not printable characters */
        int f218 = 0;

        b() {
        }
    }

    static class c {

        /* renamed from: ˊ reason: contains not printable characters */
        double f219 = Utils.DOUBLE_EPSILON;

        /* renamed from: ˋ reason: contains not printable characters */
        Rect f220 = new Rect(0, 0, 0, 0);

        /* renamed from: ॱ reason: contains not printable characters */
        double f221 = Utils.DOUBLE_EPSILON;

        c() {
        }
    }

    u() {
    }

    /* JADX WARNING: type inference failed for: r0v64, types: [java.util.HashSet, java.util.Set<android.graphics.Rect>] */
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x0351, code lost:
        if ((r9.f219 == r1.f211.f219 ? '&' : 'G') != 'G') goto L_0x0353;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x03ac, code lost:
        if (r10.equals(r1.f210) == false) goto L_0x03ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x0407, code lost:
        if ((r2.equals(r1.f213)) != true) goto L_0x0409;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0073, code lost:
        if ((r6 != null ? '<' : '0') != '0') goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00d8, code lost:
        if ((r20.getWindowToken() != null ? 'Z' : 20) != 'Z') goto L_0x00da;
     */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v64, types: [java.util.HashSet, java.util.Set<android.graphics.Rect>]
  assigns: [java.util.Set<android.graphics.Rect>]
  uses: [java.util.HashSet]
  mth insns count: 556
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0145 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0148 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x014b A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x015e A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0160 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0163 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0168 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x01be  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x01c0  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x01d9  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x01df  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x020a A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x020d A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0213 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0219 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0230  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0324  */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x0326  */
    /* JADX WARNING: Removed duplicated region for block: B:252:0x032b  */
    /* JADX WARNING: Removed duplicated region for block: B:275:0x0396  */
    /* JADX WARNING: Removed duplicated region for block: B:276:0x0398  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x039b  */
    /* JADX WARNING: Removed duplicated region for block: B:285:0x03c4 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:286:0x03c7 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:293:0x03e9  */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x03ec  */
    /* JADX WARNING: Removed duplicated region for block: B:297:0x03f0  */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x041c  */
    /* JADX WARNING: Removed duplicated region for block: B:309:0x041f  */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x0425  */
    /* JADX WARNING: Removed duplicated region for block: B:316:0x0434 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x0436 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x0439 A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x043b A[Catch:{ Exception -> 0x04db }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0110  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m200(String str, View view) {
        DisplayMetrics displayMetrics;
        boolean z;
        boolean z2;
        boolean z3;
        float f;
        int i;
        int i2;
        int i3;
        boolean z4;
        Location r0;
        char c2;
        char c3;
        Map map;
        Rect rect;
        char c4;
        char c5;
        Activity activity;
        View view2 = view;
        HashMap hashMap = new HashMap();
        String str2 = "{}";
        if (view2 != null) {
            int i4 = f203 + 5;
            f204 = i4 % 128;
            int i5 = i4 % 2;
            if (!(VERSION.SDK_INT < 17)) {
                int i6 = f204 + 113;
                f203 = i6 % 128;
                int i7 = i6 % 2;
                if ((c.f38 != null ? '9' : 25) != 25) {
                    int i8 = f203 + 57;
                    f204 = i8 % 128;
                    if (i8 % 2 == 0) {
                        activity = (Activity) c.f38.get();
                        if (!(activity != null)) {
                        }
                    } else {
                        activity = (Activity) c.f38.get();
                    }
                    displayMetrics = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
                    if (!(VERSION.SDK_INT >= 19)) {
                        if ((view2 != null ? '9' : 9) == '9') {
                            int i9 = f204 + 45;
                            f203 = i9 % 128;
                            if ((i9 % 2 != 0 ? 1 : 'A') != 1) {
                                if ((view.getWindowToken() != null ? '[' : 'E') != 'E') {
                                    z = true;
                                    if ((view2 == null ? '6' : '%') != '%') {
                                        int i10 = f204 + 101;
                                        f203 = i10 % 128;
                                        if ((i10 % 2 != 0 ? '[' : 31) != '[') {
                                            if ((view.hasWindowFocus() ? (char) 14 : 16) != 16) {
                                            }
                                        } else {
                                            if (!(view.hasWindowFocus())) {
                                            }
                                        }
                                        z2 = true;
                                        if ((view2 != null ? 'R' : 4) != 4) {
                                            if ((!view.isShown() ? '@' : '%') == '%') {
                                                z3 = false;
                                                if (view2 != null) {
                                                    f = m190(view);
                                                } else {
                                                    f = 0.0f;
                                                }
                                                hashMap.put("dr", Float.valueOf(displayMetrics.density));
                                                hashMap.put("dv", Double.valueOf(r.m158()));
                                                hashMap.put("adKey", str);
                                                String str3 = "isAttached";
                                                if ((!z ? (char) 23 : 30) == 23) {
                                                    i = 0;
                                                } else {
                                                    int i11 = f204 + 89;
                                                    f203 = i11 % 128;
                                                    if (i11 % 2 != 0) {
                                                    }
                                                    i = 1;
                                                }
                                                hashMap.put(str3, Integer.valueOf(i));
                                                String str4 = "inFocus";
                                                if ((!z2 ? 'U' : 1) == 'U') {
                                                    i2 = 0;
                                                } else {
                                                    int i12 = f203 + 19;
                                                    f204 = i12 % 128;
                                                    int i13 = i12 % 2;
                                                    i2 = 1;
                                                }
                                                hashMap.put(str4, Integer.valueOf(i2));
                                                String str5 = "isHidden";
                                                if ((!z3 ? '0' : '*') == '0') {
                                                    i3 = 0;
                                                } else {
                                                    int i14 = f204 + 27;
                                                    f203 = i14 % 128;
                                                    if (i14 % 2 != 0) {
                                                    }
                                                    i3 = 1;
                                                }
                                                hashMap.put(str5, Integer.valueOf(i3));
                                                hashMap.put("opacity", Float.valueOf(f));
                                                Rect rect2 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                                Rect rect3 = (view2 == null ? 'X' : '9') == 'X' ? new Rect(0, 0, 0, 0) : m198(view);
                                                c cVar = new c();
                                                int width = rect3.width() * rect3.height();
                                                if ((view2 == null ? 5 : 'J') == 5) {
                                                    int i15 = f204 + 21;
                                                    f203 = i15 % 128;
                                                    int i16 = i15 % 2;
                                                    if ((z ? 'C' : 'V') != 'V') {
                                                        if ((z2 ? 10 : '(') != '(') {
                                                            int i17 = f203 + 87;
                                                            f204 = i17 % 128;
                                                            int i18 = i17 % 2;
                                                            if ((!z3 ? ')' : 'N') != 'N') {
                                                                if ((width > 0 ? (char) 1 : 13) == 1) {
                                                                    int i19 = f204 + 101;
                                                                    f203 = i19 % 128;
                                                                    if ((i19 % 2 != 0 ? '&' : '9') != '&') {
                                                                        rect = new Rect(0, 0, 0, 0);
                                                                        if (m195(view2, rect)) {
                                                                            c5 = 9;
                                                                            c4 = 9;
                                                                        } else {
                                                                            c4 = '1';
                                                                            c5 = 9;
                                                                        }
                                                                        if (c4 != c5) {
                                                                        }
                                                                    } else {
                                                                        rect = new Rect(0, 0, 0, 0);
                                                                        if (!(m195(view2, rect))) {
                                                                        }
                                                                    }
                                                                    int width2 = rect.width() * rect.height();
                                                                    if (width2 < width) {
                                                                        a.m11("VisibilityInfo", null, "Ad is clipped");
                                                                    }
                                                                    if (view.getRootView() instanceof ViewGroup) {
                                                                        cVar.f220 = rect;
                                                                        b r02 = m194(rect, view2);
                                                                        if (!(r02.f216)) {
                                                                            int r03 = m191(rect, (Set<Rect>) r02.f217);
                                                                            if ((r03 > 0 ? 'Z' : 4) != 4) {
                                                                                int i20 = f204 + 35;
                                                                                f203 = i20 % 128;
                                                                                int i21 = i20 % 2;
                                                                                double d = (double) r03;
                                                                                double d2 = (double) width2;
                                                                                Double.isNaN(d);
                                                                                Double.isNaN(d2);
                                                                                cVar.f221 = d / d2;
                                                                            }
                                                                            double d3 = (double) (width2 - r03);
                                                                            double d4 = (double) width;
                                                                            Double.isNaN(d3);
                                                                            Double.isNaN(d4);
                                                                            cVar.f219 = d3 / d4;
                                                                        } else {
                                                                            cVar.f221 = 1.0d;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (this.f209 == null) {
                                                    int i22 = f204 + 39;
                                                    f203 = i22 % 128;
                                                    if (i22 % 2 != 0) {
                                                        if (cVar.f219 == this.f211.f219) {
                                                        }
                                                    }
                                                    if (cVar.f220.equals(this.f211.f220)) {
                                                        if (cVar.f221 == this.f211.f221) {
                                                            z4 = false;
                                                            hashMap.put("coveredPercent", Double.valueOf(cVar.f221));
                                                            if (this.f208 != null) {
                                                                int i23 = f204 + 37;
                                                                f203 = i23 % 128;
                                                                int i24 = i23 % 2;
                                                            }
                                                            this.f210 = rect2;
                                                            this.f208 = new JSONObject(m197(m192(rect2, displayMetrics)));
                                                            z4 = true;
                                                            if ((this.f206 != null ? 18 : 'G') == 'G' || !rect3.equals(this.f207)) {
                                                                this.f207 = rect3;
                                                                this.f206 = new JSONObject(m197(m192(rect3, displayMetrics)));
                                                                z4 = true;
                                                            }
                                                            boolean z5 = true;
                                                            if (!(this.f213 == null)) {
                                                                int i25 = f203 + 113;
                                                                f204 = i25 % 128;
                                                                int i26 = i25 % 2;
                                                                z5 = true;
                                                            }
                                                            this.f213 = hashMap;
                                                            z4 = true;
                                                            r0 = n.m120().m129();
                                                            if ((!n.m119(r0, this.f205) ? 21 : ']') != ']') {
                                                                int i27 = f204 + 81;
                                                                f203 = i27 % 128;
                                                                int i28 = i27 % 2;
                                                                this.f205 = r0;
                                                                z4 = true;
                                                            }
                                                            if (!(!z4)) {
                                                                JSONObject jSONObject = new JSONObject(this.f213);
                                                                jSONObject.accumulate("screen", this.f208);
                                                                jSONObject.accumulate(ParametersKeys.VIEW, this.f206);
                                                                jSONObject.accumulate(String.VISIBLE, this.f209);
                                                                jSONObject.accumulate("maybe", this.f209);
                                                                jSONObject.accumulate("visiblePercent", Double.valueOf(this.f211.f219));
                                                                if (r0 != null) {
                                                                    c2 = '>';
                                                                    c3 = '<';
                                                                } else {
                                                                    c3 = '<';
                                                                    c2 = '<';
                                                                }
                                                                if (c2 != c3) {
                                                                    String str6 = "location";
                                                                    if (r0 != null) {
                                                                        z5 = false;
                                                                    }
                                                                    if (z5) {
                                                                        map = null;
                                                                    } else {
                                                                        HashMap hashMap2 = new HashMap();
                                                                        hashMap2.put(LocationConst.LATITUDE, Double.toString(r0.getLatitude()));
                                                                        hashMap2.put(LocationConst.LONGITUDE, Double.toString(r0.getLongitude()));
                                                                        hashMap2.put("timestamp", Long.toString(r0.getTime()));
                                                                        hashMap2.put("horizontalAccuracy", Float.toString(r0.getAccuracy()));
                                                                        map = hashMap2;
                                                                    }
                                                                    jSONObject.accumulate(str6, (map == null ? 14 : 'U') != 14 ? new JSONObject(map) : null);
                                                                }
                                                                String jSONObject2 = jSONObject.toString();
                                                                try {
                                                                    this.f212 = jSONObject2;
                                                                } catch (Exception e) {
                                                                    e = e;
                                                                    str2 = jSONObject2;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                this.f211 = cVar;
                                                this.f209 = new JSONObject(m197(m192(this.f211.f220, displayMetrics)));
                                                z4 = true;
                                                hashMap.put("coveredPercent", Double.valueOf(cVar.f221));
                                                if (this.f208 != null) {
                                                }
                                                this.f210 = rect2;
                                                this.f208 = new JSONObject(m197(m192(rect2, displayMetrics)));
                                                z4 = true;
                                                this.f207 = rect3;
                                                this.f206 = new JSONObject(m197(m192(rect3, displayMetrics)));
                                                z4 = true;
                                                boolean z52 = true;
                                                if (!(this.f213 == null)) {
                                                }
                                                this.f213 = hashMap;
                                                z4 = true;
                                                r0 = n.m120().m129();
                                                if ((!n.m119(r0, this.f205) ? 21 : ']') != ']') {
                                                }
                                                if (!(!z4)) {
                                                }
                                            }
                                        }
                                        z3 = true;
                                        if (view2 != null) {
                                        }
                                        hashMap.put("dr", Float.valueOf(displayMetrics.density));
                                        hashMap.put("dv", Double.valueOf(r.m158()));
                                        hashMap.put("adKey", str);
                                        String str32 = "isAttached";
                                        if ((!z ? (char) 23 : 30) == 23) {
                                        }
                                        hashMap.put(str32, Integer.valueOf(i));
                                        String str42 = "inFocus";
                                        if ((!z2 ? 'U' : 1) == 'U') {
                                        }
                                        hashMap.put(str42, Integer.valueOf(i2));
                                        String str52 = "isHidden";
                                        if ((!z3 ? '0' : '*') == '0') {
                                        }
                                        hashMap.put(str52, Integer.valueOf(i3));
                                        hashMap.put("opacity", Float.valueOf(f));
                                        Rect rect22 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                        if ((view2 == null ? 'X' : '9') == 'X') {
                                        }
                                        c cVar2 = new c();
                                        int width3 = rect3.width() * rect3.height();
                                        if ((view2 == null ? 5 : 'J') == 5) {
                                        }
                                        if (this.f209 == null) {
                                        }
                                        this.f211 = cVar2;
                                        this.f209 = new JSONObject(m197(m192(this.f211.f220, displayMetrics)));
                                        z4 = true;
                                        hashMap.put("coveredPercent", Double.valueOf(cVar2.f221));
                                        if (this.f208 != null) {
                                        }
                                        this.f210 = rect22;
                                        this.f208 = new JSONObject(m197(m192(rect22, displayMetrics)));
                                        z4 = true;
                                        this.f207 = rect3;
                                        this.f206 = new JSONObject(m197(m192(rect3, displayMetrics)));
                                        z4 = true;
                                        boolean z522 = true;
                                        if (!(this.f213 == null)) {
                                        }
                                        this.f213 = hashMap;
                                        z4 = true;
                                        r0 = n.m120().m129();
                                        if ((!n.m119(r0, this.f205) ? 21 : ']') != ']') {
                                        }
                                        if (!(!z4)) {
                                        }
                                    }
                                    z2 = false;
                                    if ((view2 != null ? 'R' : 4) != 4) {
                                    }
                                    z3 = true;
                                    if (view2 != null) {
                                    }
                                    hashMap.put("dr", Float.valueOf(displayMetrics.density));
                                    hashMap.put("dv", Double.valueOf(r.m158()));
                                    hashMap.put("adKey", str);
                                    String str322 = "isAttached";
                                    if ((!z ? (char) 23 : 30) == 23) {
                                    }
                                    hashMap.put(str322, Integer.valueOf(i));
                                    String str422 = "inFocus";
                                    if ((!z2 ? 'U' : 1) == 'U') {
                                    }
                                    hashMap.put(str422, Integer.valueOf(i2));
                                    String str522 = "isHidden";
                                    if ((!z3 ? '0' : '*') == '0') {
                                    }
                                    hashMap.put(str522, Integer.valueOf(i3));
                                    hashMap.put("opacity", Float.valueOf(f));
                                    Rect rect222 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                    if ((view2 == null ? 'X' : '9') == 'X') {
                                    }
                                    c cVar22 = new c();
                                    int width32 = rect3.width() * rect3.height();
                                    if ((view2 == null ? 5 : 'J') == 5) {
                                    }
                                    if (this.f209 == null) {
                                    }
                                    this.f211 = cVar22;
                                    this.f209 = new JSONObject(m197(m192(this.f211.f220, displayMetrics)));
                                    z4 = true;
                                    hashMap.put("coveredPercent", Double.valueOf(cVar22.f221));
                                    if (this.f208 != null) {
                                    }
                                    this.f210 = rect222;
                                    this.f208 = new JSONObject(m197(m192(rect222, displayMetrics)));
                                    z4 = true;
                                    this.f207 = rect3;
                                    this.f206 = new JSONObject(m197(m192(rect3, displayMetrics)));
                                    z4 = true;
                                    boolean z5222 = true;
                                    if (!(this.f213 == null)) {
                                    }
                                    this.f213 = hashMap;
                                    z4 = true;
                                    r0 = n.m120().m129();
                                    if ((!n.m119(r0, this.f205) ? 21 : ']') != ']') {
                                    }
                                    if (!(!z4)) {
                                    }
                                }
                            }
                        }
                    } else {
                        int i29 = f203 + 51;
                        f204 = i29 % 128;
                        int i30 = i29 % 2;
                        if (view2 != null) {
                            if (view.isAttachedToWindow()) {
                                int i31 = f204 + 69;
                                f203 = i31 % 128;
                                int i32 = i31 % 2;
                                z = true;
                                if ((view2 == null ? '6' : '%') != '%') {
                                }
                                z2 = false;
                                if ((view2 != null ? 'R' : 4) != 4) {
                                }
                                z3 = true;
                                if (view2 != null) {
                                }
                                hashMap.put("dr", Float.valueOf(displayMetrics.density));
                                hashMap.put("dv", Double.valueOf(r.m158()));
                                hashMap.put("adKey", str);
                                String str3222 = "isAttached";
                                if ((!z ? (char) 23 : 30) == 23) {
                                }
                                hashMap.put(str3222, Integer.valueOf(i));
                                String str4222 = "inFocus";
                                if ((!z2 ? 'U' : 1) == 'U') {
                                }
                                hashMap.put(str4222, Integer.valueOf(i2));
                                String str5222 = "isHidden";
                                if ((!z3 ? '0' : '*') == '0') {
                                }
                                hashMap.put(str5222, Integer.valueOf(i3));
                                hashMap.put("opacity", Float.valueOf(f));
                                Rect rect2222 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                if ((view2 == null ? 'X' : '9') == 'X') {
                                }
                                c cVar222 = new c();
                                int width322 = rect3.width() * rect3.height();
                                if ((view2 == null ? 5 : 'J') == 5) {
                                }
                                if (this.f209 == null) {
                                }
                                this.f211 = cVar222;
                                this.f209 = new JSONObject(m197(m192(this.f211.f220, displayMetrics)));
                                z4 = true;
                                hashMap.put("coveredPercent", Double.valueOf(cVar222.f221));
                                if (this.f208 != null) {
                                }
                                this.f210 = rect2222;
                                this.f208 = new JSONObject(m197(m192(rect2222, displayMetrics)));
                                z4 = true;
                                this.f207 = rect3;
                                this.f206 = new JSONObject(m197(m192(rect3, displayMetrics)));
                                z4 = true;
                                boolean z52222 = true;
                                if (!(this.f213 == null)) {
                                }
                                this.f213 = hashMap;
                                z4 = true;
                                r0 = n.m120().m129();
                                if ((!n.m119(r0, this.f205) ? 21 : ']') != ']') {
                                }
                                if (!(!z4)) {
                                }
                            }
                        }
                    }
                    z = false;
                    if ((view2 == null ? '6' : '%') != '%') {
                    }
                    z2 = false;
                    if ((view2 != null ? 'R' : 4) != 4) {
                    }
                    z3 = true;
                    if (view2 != null) {
                    }
                    hashMap.put("dr", Float.valueOf(displayMetrics.density));
                    hashMap.put("dv", Double.valueOf(r.m158()));
                    hashMap.put("adKey", str);
                    String str32222 = "isAttached";
                    if ((!z ? (char) 23 : 30) == 23) {
                    }
                    hashMap.put(str32222, Integer.valueOf(i));
                    String str42222 = "inFocus";
                    if ((!z2 ? 'U' : 1) == 'U') {
                    }
                    hashMap.put(str42222, Integer.valueOf(i2));
                    String str52222 = "isHidden";
                    if ((!z3 ? '0' : '*') == '0') {
                    }
                    hashMap.put(str52222, Integer.valueOf(i3));
                    hashMap.put("opacity", Float.valueOf(f));
                    Rect rect22222 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                    if ((view2 == null ? 'X' : '9') == 'X') {
                    }
                    c cVar2222 = new c();
                    int width3222 = rect3.width() * rect3.height();
                    if ((view2 == null ? 5 : 'J') == 5) {
                    }
                    if (this.f209 == null) {
                    }
                    this.f211 = cVar2222;
                    this.f209 = new JSONObject(m197(m192(this.f211.f220, displayMetrics)));
                    z4 = true;
                    hashMap.put("coveredPercent", Double.valueOf(cVar2222.f221));
                    if (this.f208 != null) {
                    }
                    this.f210 = rect22222;
                    this.f208 = new JSONObject(m197(m192(rect22222, displayMetrics)));
                    z4 = true;
                    this.f207 = rect3;
                    this.f206 = new JSONObject(m197(m192(rect3, displayMetrics)));
                    z4 = true;
                    boolean z522222 = true;
                    if (!(this.f213 == null)) {
                    }
                    this.f213 = hashMap;
                    z4 = true;
                    r0 = n.m120().m129();
                    if ((!n.m119(r0, this.f205) ? 21 : ']') != ']') {
                    }
                    if (!(!z4)) {
                    }
                }
            }
            displayMetrics = view.getContext().getResources().getDisplayMetrics();
            if (!(VERSION.SDK_INT >= 19)) {
            }
            z = false;
            if ((view2 == null ? '6' : '%') != '%') {
            }
            z2 = false;
            if ((view2 != null ? 'R' : 4) != 4) {
            }
            z3 = true;
            if (view2 != null) {
            }
            hashMap.put("dr", Float.valueOf(displayMetrics.density));
            hashMap.put("dv", Double.valueOf(r.m158()));
            hashMap.put("adKey", str);
            String str322222 = "isAttached";
            if ((!z ? (char) 23 : 30) == 23) {
            }
            try {
                hashMap.put(str322222, Integer.valueOf(i));
                String str422222 = "inFocus";
                if ((!z2 ? 'U' : 1) == 'U') {
                }
                hashMap.put(str422222, Integer.valueOf(i2));
                String str522222 = "isHidden";
                if ((!z3 ? '0' : '*') == '0') {
                }
                hashMap.put(str522222, Integer.valueOf(i3));
                hashMap.put("opacity", Float.valueOf(f));
                Rect rect222222 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                if ((view2 == null ? 'X' : '9') == 'X') {
                }
                c cVar22222 = new c();
                int width32222 = rect3.width() * rect3.height();
                if ((view2 == null ? 5 : 'J') == 5) {
                }
                if (this.f209 == null) {
                }
                this.f211 = cVar22222;
                this.f209 = new JSONObject(m197(m192(this.f211.f220, displayMetrics)));
                z4 = true;
                hashMap.put("coveredPercent", Double.valueOf(cVar22222.f221));
                if (this.f208 != null) {
                }
                this.f210 = rect222222;
                this.f208 = new JSONObject(m197(m192(rect222222, displayMetrics)));
                z4 = true;
                this.f207 = rect3;
                this.f206 = new JSONObject(m197(m192(rect3, displayMetrics)));
                z4 = true;
                boolean z5222222 = true;
                if (!(this.f213 == null)) {
                }
                this.f213 = hashMap;
                z4 = true;
                r0 = n.m120().m129();
                if ((!n.m119(r0, this.f205) ? 21 : ']') != ']') {
                }
                if (!(!z4)) {
                }
            } catch (Exception e2) {
                e = e2;
                o.m134(e);
                this.f212 = str2;
            }
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static float m190(View view) {
        float alpha = view.getAlpha();
        while (true) {
            if ((view != null ? 25 : 'E') == 25) {
                boolean z = false;
                if (!(view.getParent() != null)) {
                    break;
                }
                if (((double) alpha) != Utils.DOUBLE_EPSILON) {
                    z = true;
                }
                if (!z) {
                    break;
                }
                if ((view.getParent() instanceof View ? 27 : ']') != 27) {
                    break;
                }
                alpha *= ((View) view.getParent()).getAlpha();
                view = (View) view.getParent();
            } else {
                break;
            }
        }
        return alpha;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static Rect m193(View view) {
        boolean z = false;
        if ((view != null ? '+' : '>') != '+') {
            return new Rect(0, 0, 0, 0);
        }
        int i = f204 + 39;
        f203 = i % 128;
        if (i % 2 == 0) {
            z = true;
        }
        return z ? m198(view) : m198(view);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008e, code lost:
        if ((r9.getBackground() != null ? 'Y' : '0') != 'Y') goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009c, code lost:
        if ((r9.getBackground() != null ? '=' : 6) != 6) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00d3, code lost:
        if ((r3) != false) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00df, code lost:
        if ((r3 ? 'S' : '9') != '9') goto L_0x00e1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0116 A[EDGE_INSN: B:123:0x0116->B:85:0x0116 ?: BREAK  
EDGE_INSN: B:123:0x0116->B:85:0x0116 ?: BREAK  
EDGE_INSN: B:123:0x0116->B:85:0x0116 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0116 A[EDGE_INSN: B:123:0x0116->B:85:0x0116 ?: BREAK  
EDGE_INSN: B:123:0x0116->B:85:0x0116 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0035 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00f8  */
    /* renamed from: ˏ reason: contains not printable characters */
    private static void m199(a aVar, Rect rect, b bVar) {
        boolean z;
        boolean z2;
        boolean z3;
        int childCount;
        int i;
        View view = aVar.f215;
        if ((view.isShown() ? 19 : 'J') != 'J') {
            if (!(((double) view.getAlpha()) <= Utils.DOUBLE_EPSILON)) {
                int i2 = f203 + 3;
                f204 = i2 % 128;
                int i3 = i2 % 2;
                z = true;
                if (!z) {
                    char c2 = '[';
                    char c3 = 'Z';
                    if (!(aVar.f215 instanceof ViewGroup)) {
                        z2 = true;
                    } else {
                        int i4 = f204 + 39;
                        f203 = i4 % 128;
                        int i5 = i4 % 2;
                        boolean equals = ViewGroup.class.equals(aVar.f215.getClass().getSuperclass());
                        View view2 = aVar.f215;
                        if ((VERSION.SDK_INT >= 19 ? (char) 4 : 1) == 4) {
                            int i6 = f204 + 113;
                            f203 = i6 % 128;
                            if (!(i6 % 2 != 0)) {
                            }
                            if ((view2.getBackground().getAlpha() == 0 ? (char) 31 : 13) != 31) {
                                z3 = false;
                                if ((!equals ? 'F' : '[') == 'F') {
                                    int i7 = f204 + 75;
                                    f203 = i7 % 128;
                                    if (!(i7 % 2 == 0)) {
                                    }
                                    z2 = false;
                                    ViewGroup viewGroup = (ViewGroup) aVar.f215;
                                    childCount = viewGroup.getChildCount();
                                    i = 0;
                                    while (true) {
                                        if ((i < childCount ? 'Z' : 'c') == 'c') {
                                            break;
                                        }
                                        int i8 = bVar.f218 + 1;
                                        bVar.f218 = i8;
                                        if (i8 <= 500) {
                                            m199(new a(viewGroup.getChildAt(i), aVar), rect, bVar);
                                            if (!bVar.f216) {
                                                i++;
                                            } else {
                                                return;
                                            }
                                        } else {
                                            return;
                                        }
                                    }
                                }
                                z2 = true;
                                ViewGroup viewGroup2 = (ViewGroup) aVar.f215;
                                childCount = viewGroup2.getChildCount();
                                i = 0;
                                while (true) {
                                    if ((i < childCount ? 'Z' : 'c') == 'c') {
                                    }
                                    i++;
                                }
                            }
                        }
                        z3 = true;
                        if ((!equals ? 'F' : '[') == 'F') {
                        }
                        z2 = true;
                        ViewGroup viewGroup22 = (ViewGroup) aVar.f215;
                        childCount = viewGroup22.getChildCount();
                        i = 0;
                        while (true) {
                            if ((i < childCount ? 'Z' : 'c') == 'c') {
                            }
                            i++;
                        }
                    }
                    if ((z2 ? '4' : 16) == '4') {
                        Rect rect2 = aVar.f214;
                        if (!rect2.setIntersect(rect, rect2)) {
                            c3 = '0';
                        }
                        if (c3 != '0') {
                            if (!(VERSION.SDK_INT < 22)) {
                                Rect rect3 = new Rect(0, 0, 0, 0);
                                if (!(!m195(aVar.f215, rect3))) {
                                    Rect rect4 = aVar.f214;
                                    if (rect4.setIntersect(rect3, rect4)) {
                                        rect2 = rect4;
                                    } else {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            }
                            if (t.m178().f179) {
                                a.m11("VisibilityInfo", aVar.f215, String.format(Locale.ROOT, "Covered by %s-%s alpha=%f", new Object[]{aVar.f215.getClass().getName(), rect2.toString(), Float.valueOf(aVar.f215.getAlpha())}));
                            }
                            bVar.f217.add(rect2);
                            if (!rect2.contains(rect)) {
                                c2 = 25;
                            }
                            if (c2 != 25) {
                                int i9 = f204 + 39;
                                f203 = i9 % 128;
                                int i10 = i9 % 2;
                                bVar.f216 = true;
                            }
                        }
                    }
                    return;
                }
                return;
            }
        }
        z = false;
        if (!z) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0149, code lost:
        com.moat.analytics.mobile.cha.a.m10(r8, "VisibilityInfo", null, "Short-circuiting cover retrieval, reached max");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x01ad, code lost:
        if ((r2.getZ() <= r4.getZ()) != false) goto L_0x01af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005c, code lost:
        r3 = f203 + 123;
        f204 = r3 % 128;
        r3 = r3 % 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        com.moat.analytics.mobile.cha.a.m10(3, "VisibilityInfo", null, "Short-circuiting chain retrieval, reached max");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x010a, code lost:
        if ((r4.getParent() instanceof android.view.ViewGroup ? 18 : 'S') != 'S') goto L_0x010c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0158 A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x015a A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x015d A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x01d3 A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x01d6 A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x01da A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x01ef  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x01f2  */
    @VisibleForTesting
    /* renamed from: ˋ reason: contains not printable characters */
    private static b m194(Rect rect, @NonNull View view) {
        char c2;
        int i;
        View childAt;
        boolean z;
        b bVar = new b();
        try {
            ArrayDeque arrayDeque = new ArrayDeque();
            View view2 = view;
            int i2 = 0;
            while (true) {
                c2 = 'I';
                i = 3;
                if ((view2.getParent() == null ? '_' : 'I') == '_') {
                    int i3 = f204 + 81;
                    f203 = i3 % 128;
                    if (i3 % 2 == 0) {
                        if (!(view2 == view.getRootView())) {
                            break;
                        }
                    } else {
                        if (!(view2 == view.getRootView())) {
                            break;
                        }
                    }
                }
                i2++;
                if ((i2 > 50 ? '5' : '\\') == '\\') {
                    arrayDeque.add(view2);
                    if ((view2.getParent() instanceof View ? 29 : '1') == '1') {
                        break;
                    }
                    view2 = (View) view2.getParent();
                } else {
                    break;
                }
            }
            if (!arrayDeque.isEmpty()) {
                c2 = '@';
            }
            if (c2 != '@') {
                int i4 = f204 + 125;
                f203 = i4 % 128;
                if (i4 % 2 == 0) {
                }
                return bVar;
            }
            a.m11("VisibilityInfo", view, "starting covering rect search");
            a aVar = null;
            loop1:
            while (true) {
                if (arrayDeque.isEmpty()) {
                    break;
                }
                View view3 = (View) arrayDeque.pollLast();
                a aVar2 = new a(view3, aVar);
                if (!(view3.getParent() == null)) {
                    int i5 = f203 + 19;
                    f204 = i5 % 128;
                    if (!(i5 % 2 != 0)) {
                        if ((view3.getParent() instanceof ViewGroup ? 'Z' : 'G') != 'G') {
                        }
                    }
                    ViewGroup viewGroup = (ViewGroup) view3.getParent();
                    int childCount = viewGroup.getChildCount();
                    int i6 = 0;
                    boolean z2 = false;
                    while (true) {
                        if (!(i6 < childCount)) {
                            break;
                        }
                        int i7 = f203 + 25;
                        f204 = i7 % 128;
                        if (!(i7 % 2 != 0)) {
                            if (bVar.f218 >= 500) {
                                break loop1;
                            }
                            childAt = viewGroup.getChildAt(i6);
                            if (childAt != view3) {
                                bVar.f218++;
                                if ((z2 ? 0 : '1') != 0) {
                                    if ((VERSION.SDK_INT >= 21 ? 'M' : 19) != 19) {
                                        int i8 = f203 + 115;
                                        f204 = i8 % 128;
                                        if ((i8 % 2 == 0 ? '$' : 'H') != 'H') {
                                            if (childAt.getZ() <= view3.getZ()) {
                                            }
                                            z = true;
                                            if ((z ? (char) 16 : 18) != 18) {
                                                m199(new a(childAt, aVar), rect, bVar);
                                                if (!(!bVar.f216)) {
                                                    return bVar;
                                                }
                                            } else {
                                                Rect rect2 = rect;
                                            }
                                        }
                                    }
                                } else {
                                    if (!(VERSION.SDK_INT < 21)) {
                                        if ((childAt.getZ() >= view3.getZ() ? '5' : '^') != '5') {
                                        }
                                    }
                                    z = true;
                                    if ((z ? (char) 16 : 18) != 18) {
                                    }
                                }
                                z = false;
                                if ((z ? (char) 16 : 18) != 18) {
                                }
                            } else {
                                Rect rect3 = rect;
                                z2 = true;
                            }
                            i6++;
                            i = 3;
                        } else {
                            if (!(bVar.f218 < 500)) {
                                break loop1;
                            }
                            childAt = viewGroup.getChildAt(i6);
                            if (childAt != view3) {
                            }
                            i6++;
                            i = 3;
                        }
                    }
                }
                Rect rect4 = rect;
                aVar = aVar2;
                i = 3;
            }
            return bVar;
        } catch (Exception e) {
            o.m134(e);
        }
    }

    @VisibleForTesting
    /* renamed from: ˋ reason: contains not printable characters */
    private static int m191(Rect rect, Set<Rect> set) {
        Object next;
        if (set.isEmpty()) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(set);
        Collections.sort(arrayList, new Comparator<Rect>() {
            public final /* synthetic */ int compare(Object obj, Object obj2) {
                return Integer.valueOf(((Rect) obj).top).compareTo(Integer.valueOf(((Rect) obj2).top));
            }
        });
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (true) {
            if (!(it.hasNext())) {
                break;
            }
            int i = f203 + 59;
            f204 = i % 128;
            if (!(i % 2 == 0)) {
                next = it.next();
            } else {
                next = it.next();
            }
            Rect rect2 = (Rect) next;
            arrayList2.add(Integer.valueOf(rect2.left));
            arrayList2.add(Integer.valueOf(rect2.right));
        }
        Collections.sort(arrayList2);
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if ((i2 < arrayList2.size() - 1 ? '&' : 'U') == 'U') {
                return i3;
            }
            int i4 = i2 + 1;
            if (!(((Integer) arrayList2.get(i2)).equals(arrayList2.get(i4)))) {
                Rect rect3 = new Rect(((Integer) arrayList2.get(i2)).intValue(), rect.top, ((Integer) arrayList2.get(i4)).intValue(), rect.bottom);
                int i5 = rect.top;
                Iterator it2 = arrayList.iterator();
                while (true) {
                    if ((it2.hasNext() ? (char) 25 : 9) == 9) {
                        break;
                    }
                    int i6 = f204 + 23;
                    f203 = i6 % 128;
                    int i7 = i6 % 2;
                    Rect rect4 = (Rect) it2.next();
                    if (Rect.intersects(rect4, rect3)) {
                        if ((rect4.bottom > i5 ? 16 : 'A') == 16) {
                            i3 += rect3.width() * (rect4.bottom - Math.max(i5, rect4.top));
                            i5 = rect4.bottom;
                        }
                        if (rect4.bottom == rect3.bottom) {
                            break;
                        }
                    }
                }
            }
            i2 = i4;
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    private static Map<String, String> m197(Rect rect) {
        HashMap hashMap = new HashMap();
        hashMap.put(AvidJSONUtil.KEY_X, String.valueOf(rect.left));
        hashMap.put(AvidJSONUtil.KEY_Y, String.valueOf(rect.top));
        hashMap.put("w", String.valueOf(rect.right - rect.left));
        hashMap.put("h", String.valueOf(rect.bottom - rect.top));
        return hashMap;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    private static Rect m192(Rect rect, DisplayMetrics displayMetrics) {
        float f = displayMetrics.density;
        if (!(f != 0.0f)) {
            return rect;
        }
        return new Rect(Math.round(((float) rect.left) / f), Math.round(((float) rect.top) / f), Math.round(((float) rect.right) / f), Math.round(((float) rect.bottom) / f));
    }

    /* renamed from: ˋ reason: contains not printable characters */
    private static boolean m195(View view, Rect rect) {
        if ((view.getGlobalVisibleRect(rect) ? 'Z' : 1) != 'Z') {
            return false;
        }
        int[] iArr = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationInWindow(iArr);
        int[] iArr2 = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationOnScreen(iArr2);
        rect.offset(iArr2[0] - iArr[0], iArr2[1] - iArr[1]);
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ reason: contains not printable characters */
    public static Rect m198(View view) {
        int[] iArr = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        return new Rect(i, i2, view.getWidth() + i, view.getHeight() + i2);
    }
}
