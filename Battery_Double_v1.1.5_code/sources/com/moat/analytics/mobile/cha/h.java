package com.moat.analytics.mobile.cha;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class h {

    /* renamed from: ˊ reason: contains not printable characters */
    private static final h f72 = new h();
    /* access modifiers changed from: private */

    /* renamed from: ʽ reason: contains not printable characters */
    public ScheduledFuture<?> f73;
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public final Map<j, String> f74 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ˎ reason: contains not printable characters */
    public ScheduledFuture<?> f75;

    /* renamed from: ˏ reason: contains not printable characters */
    private final ScheduledExecutorService f76 = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public final Map<d, String> f77 = new WeakHashMap();

    /* renamed from: ˊ reason: contains not printable characters */
    static h m62() {
        return f72;
    }

    private h() {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m68(final Context context, j jVar) {
        if (jVar != null) {
            this.f74.put(jVar, "");
            if (this.f73 == null || this.f73.isDone()) {
                a.m10(3, "JSUpdateLooper", this, "Starting metadata reporting loop");
                this.f73 = this.f76.scheduleWithFixedDelay(new Runnable() {
                    public final void run() {
                        try {
                            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_METADATA"));
                            if (h.this.f74.isEmpty()) {
                                h.this.f73.cancel(true);
                            }
                        } catch (Exception e) {
                            o.m134(e);
                        }
                    }
                }, 0, 50, TimeUnit.MILLISECONDS);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m67(j jVar) {
        if (jVar != null) {
            StringBuilder sb = new StringBuilder("removeSetupNeededBridge");
            sb.append(jVar.hashCode());
            a.m10(3, "JSUpdateLooper", this, sb.toString());
            this.f74.remove(jVar);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m69(final Context context, d dVar) {
        if (dVar != null) {
            StringBuilder sb = new StringBuilder("addActiveTracker");
            sb.append(dVar.hashCode());
            a.m10(3, "JSUpdateLooper", this, sb.toString());
            if (!this.f77.containsKey(dVar)) {
                this.f77.put(dVar, "");
                if (this.f75 == null || this.f75.isDone()) {
                    a.m10(3, "JSUpdateLooper", this, "Starting view update loop");
                    this.f75 = this.f76.scheduleWithFixedDelay(new Runnable() {
                        public final void run() {
                            try {
                                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_VIEW_INFO"));
                                if (h.this.f77.isEmpty()) {
                                    a.m10(3, "JSUpdateLooper", h.this, "No more active trackers");
                                    h.this.f75.cancel(true);
                                }
                            } catch (Exception e) {
                                o.m134(e);
                            }
                        }
                    }, 0, (long) t.m178().f177, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m70(d dVar) {
        if (dVar != null) {
            StringBuilder sb = new StringBuilder("removeActiveTracker");
            sb.append(dVar.hashCode());
            a.m10(3, "JSUpdateLooper", this, sb.toString());
            this.f77.remove(dVar);
        }
    }
}
