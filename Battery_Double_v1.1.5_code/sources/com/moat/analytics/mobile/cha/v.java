package com.moat.analytics.mobile.cha;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.webkit.WebView;

final class v extends d implements WebAdTracker {
    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final String m201() {
        return "WebAdTracker";
    }

    v(@Nullable ViewGroup viewGroup) {
        this((WebView) x.m206(viewGroup, false).orElse(null));
        if (viewGroup == null) {
            String str = "Target ViewGroup is null";
            StringBuilder sb = new StringBuilder("WebAdTracker initialization not successful, ");
            sb.append(str);
            String sb2 = sb.toString();
            a.m10(3, "WebAdTracker", this, sb2);
            a.m7("[ERROR] ", sb2);
            this.f49 = new o(str);
        }
        if (this.f47 == null) {
            String str2 = "No WebView to track inside of ad container";
            StringBuilder sb3 = new StringBuilder("WebAdTracker initialization not successful, ");
            sb3.append(str2);
            String sb4 = sb3.toString();
            a.m10(3, "WebAdTracker", this, sb4);
            a.m7("[ERROR] ", sb4);
            this.f49 = new o(str2);
        }
    }

    v(@Nullable WebView webView) {
        super(webView, false, false);
        a.m10(3, "WebAdTracker", this, "Initializing.");
        if (webView == null) {
            String str = "WebView is null";
            StringBuilder sb = new StringBuilder("WebAdTracker initialization not successful, ");
            sb.append(str);
            String sb2 = sb.toString();
            a.m10(3, "WebAdTracker", this, sb2);
            a.m7("[ERROR] ", sb2);
            this.f49 = new o(str);
            return;
        }
        try {
            super.m45(webView);
            StringBuilder sb3 = new StringBuilder("WebAdTracker created for ");
            sb3.append(m36());
            a.m7("[SUCCESS] ", sb3.toString());
        } catch (o e) {
            this.f49 = e;
        }
    }
}
