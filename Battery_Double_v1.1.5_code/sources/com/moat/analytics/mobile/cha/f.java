package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import java.lang.ref.WeakReference;

final class f extends MoatAnalytics implements b {

    /* renamed from: ʻ reason: contains not printable characters */
    private boolean f53 = false;

    /* renamed from: ʼ reason: contains not printable characters */
    private String f54;

    /* renamed from: ʽ reason: contains not printable characters */
    private MoatOptions f55;

    /* renamed from: ˊ reason: contains not printable characters */
    WeakReference<Context> f56;

    /* renamed from: ˋ reason: contains not printable characters */
    boolean f57 = false;

    /* renamed from: ˎ reason: contains not printable characters */
    boolean f58 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    boolean f59 = false;
    @Nullable

    /* renamed from: ॱ reason: contains not printable characters */
    a f60;

    f() {
    }

    public final void start(Application application) {
        start(new MoatOptions(), application);
    }

    @UiThread
    public final void prepareNativeDisplayTracking(String str) {
        this.f54 = str;
        if (t.m178().f181 != a.f193) {
            try {
                m48();
            } catch (Exception e) {
                o.m134(e);
            }
        }
    }

    @UiThread
    /* renamed from: ˏ reason: contains not printable characters */
    private void m48() {
        if (this.f60 == null) {
            this.f60 = new a(c.m31(), d.f22);
            this.f60.m14(this.f54);
            StringBuilder sb = new StringBuilder("Preparing native display tracking with partner code ");
            sb.append(this.f54);
            a.m10(3, "Analytics", this, sb.toString());
            StringBuilder sb2 = new StringBuilder("Prepared for native display tracking with partner code ");
            sb2.append(this.f54);
            a.m7("[SUCCESS] ", sb2.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final boolean m49() {
        return this.f53;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final boolean m50() {
        return this.f55 != null && this.f55.disableLocationServices;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    public final void m51() throws o {
        o.m135();
        n.m120();
        if (this.f54 != null) {
            try {
                m48();
            } catch (Exception e) {
                o.m134(e);
            }
        }
    }

    public final void start(MoatOptions moatOptions, Application application) {
        try {
            if (this.f53) {
                a.m10(3, "Analytics", this, "Moat SDK has already been started.");
                return;
            }
            this.f55 = moatOptions;
            t.m178().m184();
            this.f57 = moatOptions.disableLocationServices;
            if (application != null) {
                if (moatOptions.loggingEnabled && r.m153(application.getApplicationContext())) {
                    this.f58 = true;
                }
                this.f56 = new WeakReference<>(application.getApplicationContext());
                this.f53 = true;
                this.f59 = moatOptions.autoTrackGMAInterstitials;
                c.m33(application);
                t.m178().m183((b) this);
                if (!moatOptions.disableAdIdCollection) {
                    r.m156(application);
                }
                a.m7("[SUCCESS] ", "Moat Analytics SDK Version 2.4.1 started");
                return;
            }
            throw new o("Moat Analytics SDK didn't start, application was null");
        } catch (Exception e) {
            o.m134(e);
        }
    }
}
