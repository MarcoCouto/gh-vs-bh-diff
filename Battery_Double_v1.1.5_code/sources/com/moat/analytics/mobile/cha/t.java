package com.moat.analytics.mobile.cha;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class t {

    /* renamed from: ʻ reason: contains not printable characters */
    private static t f174;
    /* access modifiers changed from: private */

    /* renamed from: ʽ reason: contains not printable characters */
    public static final Queue<e> f175 = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */

    /* renamed from: ʼ reason: contains not printable characters */
    public long f176 = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;

    /* renamed from: ˊ reason: contains not printable characters */
    volatile int f177 = Callback.DEFAULT_DRAG_ANIMATION_DURATION;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private long f178 = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;

    /* renamed from: ˋ reason: contains not printable characters */
    volatile boolean f179 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˋॱ reason: contains not printable characters */
    public final AtomicBoolean f180 = new AtomicBoolean(false);

    /* renamed from: ˎ reason: contains not printable characters */
    volatile int f181 = a.f193;

    /* renamed from: ˏ reason: contains not printable characters */
    volatile boolean f182 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ reason: contains not printable characters */
    public final AtomicBoolean f183 = new AtomicBoolean(false);

    /* renamed from: ॱ reason: contains not printable characters */
    volatile int f184 = 10;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ reason: contains not printable characters */
    public final AtomicInteger f185 = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: ॱˋ reason: contains not printable characters */
    public volatile long f186 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ᐝ reason: contains not printable characters */
    public Handler f187;

    enum a {
        ;
        

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f192 = 2;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f193 = 1;

        static {
            int[] iArr = {1, 2};
        }
    }

    interface b {
        /* renamed from: ˎ reason: contains not printable characters */
        void m186() throws o;
    }

    interface c {
        /* renamed from: ˏ reason: contains not printable characters */
        void m187(g gVar) throws o;
    }

    class d implements Runnable {

        /* renamed from: ˎ reason: contains not printable characters */
        private final String f195;
        /* access modifiers changed from: private */

        /* renamed from: ˏ reason: contains not printable characters */
        public final AnonymousClass2 f196;

        /* renamed from: ॱ reason: contains not printable characters */
        private final Handler f197;

        private d(String str, Handler handler, AnonymousClass2 r4) {
            this.f196 = r4;
            this.f197 = handler;
            StringBuilder sb = new StringBuilder("https://z.moatads.com/");
            sb.append(str);
            sb.append("/android/");
            sb.append(BuildConfig.REVISION.substring(0, 7));
            sb.append("/status.json");
            this.f195 = sb.toString();
        }

        /* renamed from: ˎ reason: contains not printable characters */
        private String m188() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f195);
            sb.append("?ts=");
            sb.append(System.currentTimeMillis());
            sb.append("&v=2.4.1");
            try {
                return (String) m.m112(sb.toString()).get();
            } catch (Exception unused) {
                return null;
            }
        }

        public final void run() {
            try {
                String r0 = m188();
                final g gVar = new g(r0);
                t.this.f182 = gVar.m57();
                t.this.f179 = gVar.m61();
                t.this.f177 = gVar.m60();
                t.this.f184 = gVar.m59();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            d.this.f196.m187(gVar);
                        } catch (Exception e) {
                            o.m134(e);
                        }
                    }
                });
                t.this.f186 = System.currentTimeMillis();
                t.this.f183.compareAndSet(true, false);
                if (r0 != null) {
                    t.this.f185.set(0);
                } else if (t.this.f185.incrementAndGet() < 10) {
                    t.this.m179(t.this.f176);
                }
            } catch (Exception e) {
                o.m134(e);
            }
            this.f197.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    class e {

        /* renamed from: ˎ reason: contains not printable characters */
        final Long f200;

        /* renamed from: ॱ reason: contains not printable characters */
        final b f202;

        e(Long l, b bVar) {
            this.f200 = l;
            this.f202 = bVar;
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static synchronized t m178() {
        t tVar;
        synchronized (t.class) {
            if (f174 == null) {
                f174 = new t();
            }
            tVar = f174;
        }
        return tVar;
    }

    private t() {
        try {
            this.f187 = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            o.m134(e2);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m184() {
        if (System.currentTimeMillis() - this.f186 > this.f178) {
            m179(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ reason: contains not printable characters */
    public void m179(final long j) {
        if (this.f183.compareAndSet(false, true)) {
            a.m10(3, "OnOff", this, "Performing status check.");
            new Thread() {
                public final void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    d dVar = new d(BuildConfig.NAMESPACE, handler, new c() {
                        /* renamed from: ˏ reason: contains not printable characters */
                        public final void m185(g gVar) throws o {
                            synchronized (t.f175) {
                                boolean z = ((f) MoatAnalytics.getInstance()).f58;
                                if (t.this.f181 != gVar.m58() || (t.this.f181 == a.f193 && z)) {
                                    t.this.f181 = gVar.m58();
                                    if (t.this.f181 == a.f193 && z) {
                                        t.this.f181 = a.f192;
                                    }
                                    if (t.this.f181 == a.f192) {
                                        a.m10(3, "OnOff", this, "Moat enabled - Version 2.4.1");
                                    }
                                    for (e eVar : t.f175) {
                                        if (t.this.f181 == a.f192) {
                                            eVar.f202.m186();
                                        }
                                    }
                                }
                                while (!t.f175.isEmpty()) {
                                    t.f175.remove();
                                }
                            }
                        }
                    });
                    handler.postDelayed(dVar, j);
                    Looper.loop();
                }
            }.start();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m183(b bVar) throws o {
        if (this.f181 == a.f192) {
            bVar.m186();
            return;
        }
        m172();
        f175.add(new e(Long.valueOf(System.currentTimeMillis()), bVar));
        if (this.f180.compareAndSet(false, true)) {
            this.f187.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (t.f175.size() > 0) {
                            t.m172();
                            t.this.f187.postDelayed(this, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                            return;
                        }
                        t.this.f180.compareAndSet(true, false);
                        t.this.f187.removeCallbacks(this);
                    } catch (Exception e) {
                        o.m134(e);
                    }
                }
            }, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ reason: contains not printable characters */
    public static void m172() {
        synchronized (f175) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator it = f175.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - ((e) it.next()).f200.longValue() >= ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS) {
                    it.remove();
                }
            }
            if (f175.size() >= 15) {
                for (int i = 0; i < 5; i++) {
                    f175.remove();
                }
            }
        }
    }
}
