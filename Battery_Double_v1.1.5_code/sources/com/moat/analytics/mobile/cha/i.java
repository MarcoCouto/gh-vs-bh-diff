package com.moat.analytics.mobile.cha;

import android.support.annotation.CallSuper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class i extends b {

    /* renamed from: ˋॱ reason: contains not printable characters */
    private int f82 = c.f92;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private int f83 = Integer.MIN_VALUE;

    /* renamed from: ͺ reason: contains not printable characters */
    private int f84 = Integer.MIN_VALUE;

    /* renamed from: ॱˊ reason: contains not printable characters */
    private double f85 = Double.NaN;

    /* renamed from: ॱˋ reason: contains not printable characters */
    private int f86 = Integer.MIN_VALUE;

    /* renamed from: ॱˎ reason: contains not printable characters */
    private int f87 = 0;

    enum c {
        ;
        

        /* renamed from: ˊ reason: contains not printable characters */
        public static final int f89 = 2;

        /* renamed from: ˋ reason: contains not printable characters */
        public static final int f90 = 4;

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f91 = 3;

        /* renamed from: ˏ reason: contains not printable characters */
        public static final int f92 = 1;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f93 = 5;

        static {
            int[] iArr = {1, 2, 3, 4, 5};
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ͺ reason: contains not printable characters */
    public abstract boolean m73();

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˋ reason: contains not printable characters */
    public abstract Integer m75();

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˎ reason: contains not printable characters */
    public abstract boolean m76();

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱᐝ reason: contains not printable characters */
    public abstract Integer m77();

    i(String str) {
        super(str);
    }

    /* renamed from: ॱ reason: contains not printable characters */
    public final boolean m74(Map<String, String> map, View view) {
        try {
            boolean r4 = super.m25(map, view);
            if (!r4) {
                return r4;
            }
            this.f33.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (!i.this.m73() || i.this.m21()) {
                            i.this.m24();
                        } else if (Boolean.valueOf(i.this.m71()).booleanValue()) {
                            i.this.f33.postDelayed(this, 200);
                        } else {
                            i.this.m24();
                        }
                    } catch (Exception e) {
                        i.this.m24();
                        o.m134(e);
                    }
                }
            }, 200);
            return r4;
        } catch (Exception e) {
            a.m10(3, "IntervalVideoTracker", this, "Problem with video loop");
            m46("trackVideoAd", e);
            return false;
        }
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            o.m134(e);
        }
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.f85 = m19().doubleValue();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final JSONObject m72(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.f5.equals(MoatAdEvent.f1)) {
            num = moatAdEvent.f5;
        } else {
            try {
                num = m75();
            } catch (Exception unused) {
                num = Integer.valueOf(this.f83);
            }
            moatAdEvent.f5 = num;
        }
        if (moatAdEvent.f5.intValue() < 0 || (moatAdEvent.f5.intValue() == 0 && moatAdEvent.f6 == MoatAdEventType.AD_EVT_COMPLETE && this.f83 > 0)) {
            num = Integer.valueOf(this.f83);
            moatAdEvent.f5 = num;
        }
        if (moatAdEvent.f6 == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.f84 == Integer.MIN_VALUE || !m17(num, Integer.valueOf(this.f84))) {
                this.f82 = c.f90;
                moatAdEvent.f6 = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.f82 = c.f93;
            }
        }
        return super.m22(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ʻॱ reason: contains not printable characters */
    public final boolean m71() throws o {
        if (!m73() || m21()) {
            return false;
        }
        try {
            int intValue = m75().intValue();
            if (this.f83 >= 0 && intValue < 0) {
                return false;
            }
            this.f83 = intValue;
            if (intValue == 0) {
                return true;
            }
            int intValue2 = m77().intValue();
            boolean r4 = m76();
            double d = (double) intValue2;
            Double.isNaN(d);
            double d2 = d / 4.0d;
            double doubleValue = m19().doubleValue();
            MoatAdEventType moatAdEventType = null;
            if (intValue > this.f86) {
                this.f86 = intValue;
            }
            if (this.f84 == Integer.MIN_VALUE) {
                this.f84 = intValue2;
            }
            if (r4) {
                if (this.f82 == c.f92) {
                    moatAdEventType = MoatAdEventType.AD_EVT_START;
                    this.f82 = c.f91;
                } else if (this.f82 == c.f89) {
                    moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                    this.f82 = c.f91;
                } else {
                    double d3 = (double) intValue;
                    Double.isNaN(d3);
                    int floor = ((int) Math.floor(d3 / d2)) - 1;
                    if (floor >= 0 && floor < 3) {
                        MoatAdEventType moatAdEventType2 = f23[floor];
                        if (!this.f24.containsKey(moatAdEventType2)) {
                            this.f24.put(moatAdEventType2, Integer.valueOf(1));
                            moatAdEventType = moatAdEventType2;
                        }
                    }
                }
            } else if (this.f82 != c.f89) {
                moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                this.f82 = c.f89;
            }
            boolean z = moatAdEventType != null;
            if (!z && !Double.isNaN(this.f85) && Math.abs(this.f85 - doubleValue) > 0.05d) {
                moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                z = true;
            }
            if (z) {
                dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), m26()));
            }
            this.f85 = doubleValue;
            this.f87 = 0;
            return true;
        } catch (Exception unused) {
            int i = this.f87;
            this.f87 = i + 1;
            return i < 5;
        }
    }
}
