package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.ads.AdActivity;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import java.lang.ref.WeakReference;

final class e {

    /* renamed from: ˊ reason: contains not printable characters */
    private static WeakReference<Activity> f51 = new WeakReference<>(null);
    @Nullable

    /* renamed from: ˋ reason: contains not printable characters */
    private static WebAdTracker f52;

    e() {
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static void m47(Activity activity) {
        try {
            if (t.m178().f181 != a.f193) {
                String name = activity.getClass().getName();
                StringBuilder sb = new StringBuilder("Activity name: ");
                sb.append(name);
                a.m10(3, "GMAInterstitialHelper", activity, sb.toString());
                if (!name.contains(AdActivity.CLASS_NAME)) {
                    if (f52 != null) {
                        a.m10(3, "GMAInterstitialHelper", f51.get(), "Stopping to track GMA interstitial");
                        f52.stopTracking();
                        f52 = null;
                    }
                    f51 = new WeakReference<>(null);
                } else if (f51.get() == null || f51.get() != activity) {
                    View decorView = activity.getWindow().getDecorView();
                    if (decorView instanceof ViewGroup) {
                        Optional r0 = x.m206((ViewGroup) decorView, true);
                        if (r0.isPresent()) {
                            f51 = new WeakReference<>(activity);
                            WebView webView = (WebView) r0.get();
                            a.m10(3, "GMAInterstitialHelper", f51.get(), "Starting to track GMA interstitial");
                            WebAdTracker createWebAdTracker = MoatFactory.create().createWebAdTracker(webView);
                            f52 = createWebAdTracker;
                            createWebAdTracker.startTracking();
                            return;
                        }
                        a.m10(3, "GMAInterstitialHelper", activity, "Sorry, no WebView in this activity");
                    }
                }
            }
        } catch (Exception e) {
            o.m134(e);
        }
    }
}
