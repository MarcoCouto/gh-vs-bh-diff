package com.moat.analytics.mobile.inm;

import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf(Utils.DOUBLE_EPSILON);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: a reason: collision with root package name */
    static final Integer f3080a = Integer.valueOf(Integer.MIN_VALUE);
    private static final Double e = Double.valueOf(Double.NaN);
    Integer b;
    Double c;
    MoatAdEventType d;
    private final Double f;
    private final Long g;

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f3080a, e);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, e);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d2) {
        this.g = Long.valueOf(System.currentTimeMillis());
        this.d = moatAdEventType;
        this.c = d2;
        this.b = num;
        this.f = Double.valueOf(s.a());
    }

    /* access modifiers changed from: 0000 */
    public Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("adVolume", this.c);
        hashMap.put("playhead", this.b);
        hashMap.put("aTimeStamp", this.g);
        hashMap.put("type", this.d.toString());
        hashMap.put(RequestParameters.DEVICE_VOLUME, this.f);
        return hashMap;
    }
}
