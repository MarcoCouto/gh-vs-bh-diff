package com.moat.analytics.mobile.inm;

import android.app.Application;
import android.support.annotation.UiThread;
import com.moat.analytics.mobile.inm.v.a;

public abstract class MoatAnalytics {

    /* renamed from: a reason: collision with root package name */
    private static MoatAnalytics f3082a;

    public static synchronized MoatAnalytics getInstance() {
        MoatAnalytics moatAnalytics;
        synchronized (MoatAnalytics.class) {
            if (f3082a == null) {
                try {
                    f3082a = new k();
                } catch (Exception e) {
                    m.a(e);
                    f3082a = new a();
                }
            }
            moatAnalytics = f3082a;
        }
        return moatAnalytics;
    }

    @UiThread
    public abstract void prepareNativeDisplayTracking(String str);

    public abstract void start(Application application);

    public abstract void start(MoatOptions moatOptions, Application application);
}
