package com.getkeepsafe.relinker;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import com.getkeepsafe.relinker.ReLinker.LibraryInstaller;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ApkLibraryInstaller implements LibraryInstaller {
    private static final int COPY_BUFFER_SIZE = 4096;
    private static final int MAX_TRIES = 5;

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0034, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00de, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00df, code lost:
        r6 = null;
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00e8, code lost:
        r6 = null;
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00ee, code lost:
        r6 = null;
        r12 = null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:13:0x002b, B:45:0x009d] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0106 A[SYNTHETIC, Splitter:B:93:0x0106] */
    public void installLibrary(Context context, String[] strArr, String str, File file, ReLinkerInstance reLinkerInstance) {
        ZipFile zipFile;
        FileOutputStream fileOutputStream;
        String[] strArr2 = strArr;
        String str2 = str;
        File file2 = file;
        ReLinkerInstance reLinkerInstance2 = reLinkerInstance;
        try {
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            int i = 0;
            while (true) {
                int i2 = i + 1;
                if (i >= 5) {
                    zipFile = null;
                    break;
                }
                try {
                    zipFile = new ZipFile(new File(applicationInfo.sourceDir), 1);
                    break;
                } catch (IOException unused) {
                    i = i2;
                }
            }
            if (zipFile == null) {
                reLinkerInstance2.log("FATAL! Couldn't find application APK!");
                if (zipFile != null) {
                    try {
                        zipFile.close();
                    } catch (IOException unused2) {
                    }
                }
                return;
            }
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                if (i3 < 5) {
                    int length = strArr2.length;
                    String str3 = null;
                    ZipEntry zipEntry = null;
                    int i5 = 0;
                    while (true) {
                        if (i5 >= length) {
                            break;
                        }
                        String str4 = strArr2[i5];
                        StringBuilder sb = new StringBuilder();
                        sb.append("lib");
                        sb.append(File.separatorChar);
                        sb.append(str4);
                        sb.append(File.separatorChar);
                        sb.append(str2);
                        str3 = sb.toString();
                        zipEntry = zipFile.getEntry(str3);
                        if (zipEntry != null) {
                            break;
                        }
                        i5++;
                    }
                    if (str3 != null) {
                        reLinkerInstance2.log("Looking for %s in APK...", str3);
                    }
                    if (zipEntry != null) {
                        reLinkerInstance2.log("Found %s! Extracting...", str3);
                        try {
                            if (file.exists() || file.createNewFile()) {
                                InputStream inputStream = zipFile.getInputStream(zipEntry);
                                try {
                                    fileOutputStream = new FileOutputStream(file2);
                                    try {
                                        long copy = copy(inputStream, fileOutputStream);
                                        fileOutputStream.getFD().sync();
                                        if (copy != file.length()) {
                                            closeSilently(inputStream);
                                            closeSilently(fileOutputStream);
                                            i3 = i4;
                                        } else {
                                            closeSilently(inputStream);
                                            closeSilently(fileOutputStream);
                                            file2.setReadable(true, false);
                                            file2.setExecutable(true, false);
                                            file2.setWritable(true);
                                            if (zipFile != null) {
                                                try {
                                                    zipFile.close();
                                                } catch (IOException unused3) {
                                                }
                                            }
                                            return;
                                        }
                                    } catch (FileNotFoundException unused4) {
                                        closeSilently(inputStream);
                                        closeSilently(fileOutputStream);
                                        i3 = i4;
                                    } catch (IOException unused5) {
                                        closeSilently(inputStream);
                                        closeSilently(fileOutputStream);
                                        i3 = i4;
                                    } catch (Throwable th) {
                                        th = th;
                                        closeSilently(inputStream);
                                        closeSilently(fileOutputStream);
                                        throw th;
                                    }
                                } catch (FileNotFoundException unused6) {
                                    fileOutputStream = null;
                                    closeSilently(inputStream);
                                    closeSilently(fileOutputStream);
                                    i3 = i4;
                                } catch (IOException unused7) {
                                    fileOutputStream = null;
                                    closeSilently(inputStream);
                                    closeSilently(fileOutputStream);
                                    i3 = i4;
                                } catch (Throwable th2) {
                                    th = th2;
                                    fileOutputStream = null;
                                    closeSilently(inputStream);
                                    closeSilently(fileOutputStream);
                                    throw th;
                                }
                            } else {
                                i3 = i4;
                            }
                        } catch (IOException unused8) {
                        }
                    } else if (str3 != null) {
                        throw new MissingLibraryException(str3);
                    } else {
                        throw new MissingLibraryException(str2);
                    }
                } else {
                    reLinkerInstance2.log("FATAL! Couldn't extract the library from the APK!");
                    if (zipFile != null) {
                        try {
                            zipFile.close();
                        } catch (IOException unused9) {
                        }
                    }
                    return;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            zipFile = null;
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (IOException unused10) {
                }
            }
            throw th;
        }
    }

    private long copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[4096];
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                outputStream.flush();
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    private void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }
}
