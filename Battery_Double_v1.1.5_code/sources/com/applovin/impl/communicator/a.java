package com.applovin.impl.communicator;

import android.content.Context;
import android.content.IntentFilter;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.m;
import java.util.HashSet;
import java.util.Set;

public class a {

    /* renamed from: a reason: collision with root package name */
    private final Context f1142a;
    private final Set<b> b = new HashSet(32);
    private final Object c = new Object();

    public a(Context context) {
        this.f1142a = context;
    }

    private b a(String str, AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber) {
        for (b bVar : this.b) {
            if (str.equals(bVar.a()) && appLovinCommunicatorSubscriber.equals(bVar.b())) {
                return bVar;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x004e, code lost:
        return true;
     */
    public boolean a(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        if (appLovinCommunicatorSubscriber == null || !m.b(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to subscribe - invalid subscriber (");
            sb.append(appLovinCommunicatorSubscriber);
            sb.append(") or topic (");
            sb.append(str);
            sb.append(")");
            o.i("AppLovinCommunicator", sb.toString());
            return false;
        }
        synchronized (this.c) {
            b a2 = a(str, appLovinCommunicatorSubscriber);
            if (a2 != null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Attempting to re-subscribe subscriber (");
                sb2.append(appLovinCommunicatorSubscriber);
                sb2.append(") to topic (");
                sb2.append(str);
                sb2.append(")");
                o.f("AppLovinCommunicator", sb2.toString());
                if (!a2.c()) {
                    a2.a(true);
                    AppLovinBroadcastManager.getInstance(this.f1142a).registerReceiver(a2, new IntentFilter(str));
                }
            } else {
                b bVar = new b(str, appLovinCommunicatorSubscriber);
                this.b.add(bVar);
                AppLovinBroadcastManager.getInstance(this.f1142a).registerReceiver(bVar, new IntentFilter(str));
                return true;
            }
        }
    }

    public void b(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        b a2;
        if (m.b(str)) {
            synchronized (this.c) {
                a2 = a(str, appLovinCommunicatorSubscriber);
            }
            if (a2 != null) {
                a2.a(false);
                AppLovinBroadcastManager.getInstance(this.f1142a).unregisterReceiver(a2);
            }
        }
    }
}
