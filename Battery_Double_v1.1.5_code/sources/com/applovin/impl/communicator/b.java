package com.applovin.impl.communicator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.sdk.o;
import java.lang.ref.WeakReference;
import java.util.LinkedHashSet;
import java.util.Set;

public class b extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private boolean f1143a = true;
    private final String b;
    private final WeakReference<AppLovinCommunicatorSubscriber> c;
    private final Set<CommunicatorMessageImpl> d = new LinkedHashSet();
    private final Object e = new Object();

    b(String str, AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber) {
        this.b = str;
        this.c = new WeakReference<>(appLovinCommunicatorSubscriber);
    }

    public String a() {
        return this.b;
    }

    public void a(boolean z) {
        this.f1143a = z;
    }

    public AppLovinCommunicatorSubscriber b() {
        return (AppLovinCommunicatorSubscriber) this.c.get();
    }

    public boolean c() {
        return this.f1143a;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (!a().equals(bVar.a()) || (this.c.get() == null ? this.c.get() != bVar.c.get() : !((AppLovinCommunicatorSubscriber) this.c.get()).equals(bVar.c.get()))) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (this.b.hashCode() * 31) + (this.c.get() != null ? ((AppLovinCommunicatorSubscriber) this.c.get()).hashCode() : 0);
    }

    public void onReceive(Context context, Intent intent) {
        if (b() == null) {
            o.i("AppLovinCommunicator", "Message received for GC'd subscriber");
            return;
        }
        CommunicatorMessageImpl communicatorMessageImpl = (CommunicatorMessageImpl) intent;
        boolean z = false;
        synchronized (this.e) {
            if (!this.d.contains(communicatorMessageImpl)) {
                this.d.add(communicatorMessageImpl);
                z = true;
            }
        }
        if (z) {
            b().onMessageReceived((AppLovinCommunicatorMessage) communicatorMessageImpl);
        }
    }
}
