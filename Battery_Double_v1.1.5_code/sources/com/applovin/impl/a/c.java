package com.applovin.impl.a;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;

public class c {
    private static final List<String> c = Arrays.asList(new String[]{MimeTypes.VIDEO_MP4, MimeTypes.VIDEO_WEBM, MimeTypes.VIDEO_H263, "video/x-matroska"});

    /* renamed from: a reason: collision with root package name */
    protected List<r> f1024a = new ArrayList();
    private final i b;
    private final JSONObject d;
    private final JSONObject e;
    private final b f;
    private final long g = System.currentTimeMillis();

    public c(JSONObject jSONObject, JSONObject jSONObject2, b bVar, i iVar) {
        this.b = iVar;
        this.d = jSONObject;
        this.e = jSONObject2;
        this.f = bVar;
    }

    public int a() {
        return this.f1024a.size();
    }

    public List<r> b() {
        return this.f1024a;
    }

    public JSONObject c() {
        return this.d;
    }

    public JSONObject d() {
        return this.e;
    }

    public b e() {
        return this.f;
    }

    public long f() {
        return this.g;
    }

    public d g() {
        String b2 = com.applovin.impl.sdk.utils.i.b(this.e, "zone_id", (String) null, this.b);
        return d.a(AppLovinAdSize.fromString(com.applovin.impl.sdk.utils.i.b(this.e, "ad_size", (String) null, this.b)), AppLovinAdType.fromString(com.applovin.impl.sdk.utils.i.b(this.e, "ad_type", (String) null, this.b)), b2, this.b);
    }

    public List<String> h() {
        List<String> a2 = e.a(com.applovin.impl.sdk.utils.i.b(this.d, "vast_preferred_video_types", (String) null, (i) null));
        return !a2.isEmpty() ? a2 : c;
    }

    public int i() {
        return p.a(this.d);
    }
}
