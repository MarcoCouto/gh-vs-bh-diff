package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import com.explorestack.iab.vast.tags.VastAttributes;
import java.util.Locale;

public class k {

    /* renamed from: a reason: collision with root package name */
    private Uri f1035a;
    private Uri b;
    private a c;
    private String d;
    private int e;
    private int f;
    private int g;

    public enum a {
        Progressive,
        Streaming
    }

    private k() {
    }

    private static a a(String str) {
        if (m.b(str)) {
            if ("progressive".equalsIgnoreCase(str)) {
                return a.Progressive;
            }
            if ("streaming".equalsIgnoreCase(str)) {
                return a.Streaming;
            }
        }
        return a.Progressive;
    }

    public static k a(r rVar, i iVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (iVar != null) {
            try {
                String c2 = rVar.c();
                if (URLUtil.isValidUrl(c2)) {
                    Uri parse = Uri.parse(c2);
                    k kVar = new k();
                    kVar.f1035a = parse;
                    kVar.b = parse;
                    kVar.g = m.a((String) rVar.b().get(VastAttributes.BITRATE));
                    kVar.c = a((String) rVar.b().get(VastAttributes.DELIVERY));
                    kVar.f = m.a((String) rVar.b().get("height"));
                    kVar.e = m.a((String) rVar.b().get("width"));
                    kVar.d = ((String) rVar.b().get("type")).toLowerCase(Locale.ENGLISH);
                    return kVar;
                }
                iVar.v().e("VastVideoFile", "Unable to create video file. Could not find URL.");
                return null;
            } catch (Throwable th) {
                iVar.v().b("VastVideoFile", "Error occurred while initializing", th);
            }
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public Uri a() {
        return this.f1035a;
    }

    public void a(Uri uri) {
        this.b = uri;
    }

    public Uri b() {
        return this.b;
    }

    public boolean c() {
        return this.c == a.Streaming;
    }

    public String d() {
        return this.d;
    }

    public int e() {
        return this.g;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof k)) {
            return false;
        }
        k kVar = (k) obj;
        if (this.e != kVar.e || this.f != kVar.f || this.g != kVar.g) {
            return false;
        }
        if (this.f1035a == null ? kVar.f1035a != null : !this.f1035a.equals(kVar.f1035a)) {
            return false;
        }
        if (this.b == null ? kVar.b != null : !this.b.equals(kVar.b)) {
            return false;
        }
        if (this.c != kVar.c) {
            return false;
        }
        if (this.d != null) {
            z = this.d.equals(kVar.d);
        } else if (kVar.d != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((this.f1035a != null ? this.f1035a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return ((((((hashCode + i) * 31) + this.e) * 31) + this.f) * 31) + this.g;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastVideoFile{sourceVideoUri=");
        sb.append(this.f1035a);
        sb.append(", videoUri=");
        sb.append(this.b);
        sb.append(", deliveryType=");
        sb.append(this.c);
        sb.append(", fileType='");
        sb.append(this.d);
        sb.append('\'');
        sb.append(", width=");
        sb.append(this.e);
        sb.append(", height=");
        sb.append(this.f);
        sb.append(", bitrate=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }
}
