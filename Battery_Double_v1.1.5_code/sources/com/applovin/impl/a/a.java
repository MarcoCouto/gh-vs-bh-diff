package com.applovin.impl.a;

import android.net.Uri;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class a extends f {

    /* renamed from: a reason: collision with root package name */
    private final String f1019a;
    private final String b;
    private final f c;
    private final long d;
    private final j e;
    private final b f;
    private final Set<g> g;
    private final Set<g> h;

    /* renamed from: com.applovin.impl.a.a$a reason: collision with other inner class name */
    public static class C0007a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public JSONObject f1020a;
        /* access modifiers changed from: private */
        public JSONObject b;
        /* access modifiers changed from: private */
        public com.applovin.impl.sdk.ad.b c;
        /* access modifiers changed from: private */
        public i d;
        /* access modifiers changed from: private */
        public long e;
        /* access modifiers changed from: private */
        public String f;
        /* access modifiers changed from: private */
        public String g;
        /* access modifiers changed from: private */
        public f h;
        /* access modifiers changed from: private */
        public j i;
        /* access modifiers changed from: private */
        public b j;
        /* access modifiers changed from: private */
        public Set<g> k;
        /* access modifiers changed from: private */
        public Set<g> l;

        private C0007a() {
        }

        public C0007a a(long j2) {
            this.e = j2;
            return this;
        }

        public C0007a a(b bVar) {
            this.j = bVar;
            return this;
        }

        public C0007a a(f fVar) {
            this.h = fVar;
            return this;
        }

        public C0007a a(j jVar) {
            this.i = jVar;
            return this;
        }

        public C0007a a(com.applovin.impl.sdk.ad.b bVar) {
            this.c = bVar;
            return this;
        }

        public C0007a a(i iVar) {
            if (iVar != null) {
                this.d = iVar;
                return this;
            }
            throw new IllegalArgumentException("No sdk specified.");
        }

        public C0007a a(String str) {
            this.f = str;
            return this;
        }

        public C0007a a(Set<g> set) {
            this.k = set;
            return this;
        }

        public C0007a a(JSONObject jSONObject) {
            if (jSONObject != null) {
                this.f1020a = jSONObject;
                return this;
            }
            throw new IllegalArgumentException("No ad object specified.");
        }

        public a a() {
            return new a(this);
        }

        public C0007a b(String str) {
            this.g = str;
            return this;
        }

        public C0007a b(Set<g> set) {
            this.l = set;
            return this;
        }

        public C0007a b(JSONObject jSONObject) {
            if (jSONObject != null) {
                this.b = jSONObject;
                return this;
            }
            throw new IllegalArgumentException("No full ad response specified.");
        }
    }

    public enum b {
        COMPANION_AD,
        VIDEO
    }

    public enum c {
        IMPRESSION,
        VIDEO_CLICK,
        COMPANION_CLICK,
        VIDEO,
        COMPANION,
        ERROR
    }

    private a(C0007a aVar) {
        super(aVar.f1020a, aVar.b, aVar.c, aVar.d);
        this.f1019a = aVar.f;
        this.c = aVar.h;
        this.b = aVar.g;
        this.e = aVar.i;
        this.f = aVar.j;
        this.g = aVar.k;
        this.h = aVar.l;
        this.d = aVar.e;
    }

    private Set<g> a(b bVar, String[] strArr) {
        if (strArr == null || strArr.length <= 0) {
            return Collections.emptySet();
        }
        Map map = null;
        if (bVar == b.VIDEO && this.e != null) {
            map = this.e.e();
        } else if (bVar == b.COMPANION_AD && this.f != null) {
            map = this.f.d();
        }
        HashSet hashSet = new HashSet();
        if (map != null && !map.isEmpty()) {
            for (String str : strArr) {
                if (map.containsKey(str)) {
                    hashSet.addAll((Collection) map.get(str));
                }
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public static C0007a aH() {
        return new C0007a();
    }

    private String aI() {
        String stringFromAdObject = getStringFromAdObject("vimp_url", null);
        if (stringFromAdObject != null) {
            return stringFromAdObject.replace("{CLCODE}", getClCode());
        }
        return null;
    }

    private com.applovin.impl.a.j.a aJ() {
        com.applovin.impl.a.j.a[] values = com.applovin.impl.a.j.a.values();
        int intValue = ((Integer) this.sdk.a(com.applovin.impl.sdk.b.c.eG)).intValue();
        return (intValue < 0 || intValue >= values.length) ? com.applovin.impl.a.j.a.UNSPECIFIED : values[intValue];
    }

    private Set<g> aK() {
        return this.e != null ? this.e.d() : Collections.emptySet();
    }

    private Set<g> aL() {
        return this.f != null ? this.f.c() : Collections.emptySet();
    }

    public Set<g> a(c cVar, String str) {
        return a(cVar, new String[]{str});
    }

    public Set<g> a(c cVar, String[] strArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("Retrieving trackers of type '");
        sb.append(cVar);
        sb.append("' and events '");
        sb.append(strArr);
        sb.append("'...");
        this.sdk.v().b("VastAd", sb.toString());
        if (cVar == c.IMPRESSION) {
            return this.g;
        }
        if (cVar == c.VIDEO_CLICK) {
            return aK();
        }
        if (cVar == c.COMPANION_CLICK) {
            return aL();
        }
        if (cVar == c.VIDEO) {
            return a(b.VIDEO, strArr);
        }
        if (cVar == c.COMPANION) {
            return a(b.COMPANION_AD, strArr);
        }
        if (cVar == c.ERROR) {
            return this.h;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Failed to retrieve trackers of invalid type '");
        sb2.append(cVar);
        sb2.append("' and events '");
        sb2.append(strArr);
        sb2.append("'");
        this.sdk.v().e("VastAd", sb2.toString());
        return Collections.emptySet();
    }

    public void a(String str) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("html_template", str);
            }
        } catch (Throwable unused) {
        }
    }

    public boolean a() {
        return b();
    }

    public String aD() {
        return getStringFromAdObject("html_template", "");
    }

    public Uri aE() {
        String stringFromAdObject = getStringFromAdObject("html_template_url", null);
        if (m.b(stringFromAdObject)) {
            return Uri.parse(stringFromAdObject);
        }
        return null;
    }

    public boolean aF() {
        return getBooleanFromAdObject("cache_companion_ad", Boolean.valueOf(true));
    }

    public boolean aG() {
        return getBooleanFromAdObject("cache_video", Boolean.valueOf(true));
    }

    public List<com.applovin.impl.sdk.c.a> an() {
        List<com.applovin.impl.sdk.c.a> a2;
        synchronized (this.adObjectLock) {
            a2 = p.a("vimp_urls", this.adObject, getClCode(), aI(), this.sdk);
        }
        return a2;
    }

    public boolean b() {
        boolean z = false;
        if (containsKeyForAdObject("vast_is_streaming")) {
            return getBooleanFromAdObject("vast_is_streaming", Boolean.valueOf(false));
        }
        k i = i();
        if (i != null && i.c()) {
            z = true;
        }
        return z;
    }

    public b c() {
        return "companion_ad".equalsIgnoreCase(getStringFromAdObject("vast_first_caching_operation", "companion_ad")) ? b.COMPANION_AD : b.VIDEO;
    }

    public Uri d() {
        k i = i();
        if (i != null) {
            return i.b();
        }
        return null;
    }

    public boolean e() {
        return getBooleanFromAdObject("vast_immediate_ad_load", Boolean.valueOf(true));
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a) || !super.equals(obj)) {
            return false;
        }
        a aVar = (a) obj;
        if (this.f1019a == null ? aVar.f1019a != null : !this.f1019a.equals(aVar.f1019a)) {
            return false;
        }
        if (this.b == null ? aVar.b != null : !this.b.equals(aVar.b)) {
            return false;
        }
        if (this.c == null ? aVar.c != null : !this.c.equals(aVar.c)) {
            return false;
        }
        if (this.e == null ? aVar.e != null : !this.e.equals(aVar.e)) {
            return false;
        }
        if (this.f == null ? aVar.f != null : !this.f.equals(aVar.f)) {
            return false;
        }
        if (this.g == null ? aVar.g != null : !this.g.equals(aVar.g)) {
            return false;
        }
        if (this.h != null) {
            z = this.h.equals(aVar.h);
        } else if (aVar.h != null) {
            z = false;
        }
        return z;
    }

    public Uri f() {
        if (this.e != null) {
            return this.e.c();
        }
        return null;
    }

    public Uri g() {
        return f();
    }

    public long getCreatedAtMillis() {
        return this.d;
    }

    public j h() {
        return this.e;
    }

    public boolean hasVideoUrl() {
        if (this.e == null) {
            return false;
        }
        List a2 = this.e.a();
        return a2 != null && a2.size() > 0;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((super.hashCode() * 31) + (this.f1019a != null ? this.f1019a.hashCode() : 0)) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.g != null ? this.g.hashCode() : 0)) * 31;
        if (this.h != null) {
            i = this.h.hashCode();
        }
        return hashCode + i;
    }

    public k i() {
        if (this.e != null) {
            return this.e.a(aJ());
        }
        return null;
    }

    public b j() {
        return this.f;
    }

    public boolean k() {
        return getBooleanFromAdObject("vast_fire_click_trackers_on_html_clicks", Boolean.valueOf(false));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastAd{title='");
        sb.append(this.f1019a);
        sb.append('\'');
        sb.append(", adDescription='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", systemInfo=");
        sb.append(this.c);
        sb.append(", videoCreative=");
        sb.append(this.e);
        sb.append(", companionAd=");
        sb.append(this.f);
        sb.append(", impressionTrackers=");
        sb.append(this.g);
        sb.append(", errorTrackers=");
        sb.append(this.h);
        sb.append('}');
        return sb.toString();
    }

    public boolean u() {
        return f() != null;
    }
}
