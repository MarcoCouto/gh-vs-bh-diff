package com.applovin.impl.a;

import android.annotation.TargetApi;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import com.explorestack.iab.vast.tags.VastTagName;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class j {

    /* renamed from: a reason: collision with root package name */
    private List<k> f1032a = Collections.EMPTY_LIST;
    private List<String> b = Collections.EMPTY_LIST;
    private int c;
    private Uri d;
    private final Set<g> e = new HashSet();
    private final Map<String, Set<g>> f = new HashMap();

    public enum a {
        UNSPECIFIED,
        LOW,
        MEDIUM,
        HIGH
    }

    private j() {
    }

    private j(c cVar) {
        this.b = cVar.h();
    }

    private static int a(String str, i iVar) {
        try {
            List a2 = e.a(str, ":");
            if (a2.size() == 3) {
                return (int) (TimeUnit.HOURS.toSeconds((long) m.a((String) a2.get(0))) + TimeUnit.MINUTES.toSeconds((long) m.a((String) a2.get(1))) + ((long) m.a((String) a2.get(2))));
            }
        } catch (Throwable unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to parse duration from \"");
            sb.append(str);
            sb.append("\"");
            iVar.v().e("VastVideoCreative", sb.toString());
        }
        return 0;
    }

    public static j a(r rVar, j jVar, c cVar, i iVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (cVar == null) {
            throw new IllegalArgumentException("No context specified.");
        } else if (iVar != null) {
            if (jVar == null) {
                try {
                    jVar = new j(cVar);
                } catch (Throwable th) {
                    iVar.v().b("VastVideoCreative", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (jVar.c == 0) {
                r b2 = rVar.b(VastTagName.DURATION);
                if (b2 != null) {
                    int a2 = a(b2.c(), iVar);
                    if (a2 > 0) {
                        jVar.c = a2;
                    }
                }
            }
            r b3 = rVar.b(VastTagName.MEDIA_FILES);
            if (b3 != null) {
                List<k> a3 = a(b3, iVar);
                if (a3 != null && a3.size() > 0) {
                    if (jVar.f1032a != null) {
                        a3.addAll(jVar.f1032a);
                    }
                    jVar.f1032a = a3;
                }
            }
            r b4 = rVar.b(VastTagName.VIDEO_CLICKS);
            if (b4 != null) {
                if (jVar.d == null) {
                    r b5 = b4.b(VastTagName.CLICK_THROUGH);
                    if (b5 != null) {
                        String c2 = b5.c();
                        if (m.b(c2)) {
                            jVar.d = Uri.parse(c2);
                        }
                    }
                }
                i.a(b4.a(VastTagName.CLICK_TRACKING), jVar.e, cVar, iVar);
            }
            i.a(rVar, jVar.f, cVar, iVar);
            return jVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    private static List<k> a(r rVar, i iVar) {
        List<r> a2 = rVar.a(VastTagName.MEDIA_FILE);
        ArrayList arrayList = new ArrayList(a2.size());
        List a3 = e.a((String) iVar.a(c.eE));
        List a4 = e.a((String) iVar.a(c.eD));
        for (r a5 : a2) {
            k a6 = k.a(a5, iVar);
            if (a6 != null) {
                try {
                    String d2 = a6.d();
                    if (!m.b(d2) || a3.contains(d2)) {
                        if (((Boolean) iVar.a(c.eF)).booleanValue()) {
                            String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(a6.b().toString());
                            if (m.b(fileExtensionFromUrl) && !a4.contains(fileExtensionFromUrl)) {
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append("Video file not supported: ");
                        sb.append(a6);
                        iVar.v().d("VastVideoCreative", sb.toString());
                    }
                    arrayList.add(a6);
                } catch (Throwable th) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to validate vidoe file: ");
                    sb2.append(a6);
                    iVar.v().b("VastVideoCreative", sb2.toString(), th);
                }
            }
        }
        return arrayList;
    }

    public k a(a aVar) {
        if (this.f1032a == null || this.f1032a.size() == 0) {
            return null;
        }
        List arrayList = new ArrayList(3);
        for (String str : this.b) {
            for (k kVar : this.f1032a) {
                String d2 = kVar.d();
                if (m.b(d2) && str.equalsIgnoreCase(d2)) {
                    arrayList.add(kVar);
                }
            }
            if (!arrayList.isEmpty()) {
                break;
            }
        }
        if (arrayList.isEmpty()) {
            arrayList = this.f1032a;
        }
        if (g.e()) {
            Collections.sort(arrayList, new Comparator<k>() {
                @TargetApi(19)
                /* renamed from: a */
                public int compare(k kVar, k kVar2) {
                    return Integer.compare(kVar.e(), kVar2.e());
                }
            });
        }
        int size = aVar == a.LOW ? 0 : aVar == a.MEDIUM ? arrayList.size() / 2 : arrayList.size() - 1;
        return (k) arrayList.get(size);
    }

    public List<k> a() {
        return this.f1032a;
    }

    public int b() {
        return this.c;
    }

    public Uri c() {
        return this.d;
    }

    public Set<g> d() {
        return this.e;
    }

    public Map<String, Set<g>> e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        if (this.c != jVar.c) {
            return false;
        }
        if (this.f1032a == null ? jVar.f1032a != null : !this.f1032a.equals(jVar.f1032a)) {
            return false;
        }
        if (this.d == null ? jVar.d != null : !this.d.equals(jVar.d)) {
            return false;
        }
        if (this.e == null ? jVar.e != null : !this.e.equals(jVar.e)) {
            return false;
        }
        if (this.f != null) {
            z = this.f.equals(jVar.f);
        } else if (jVar.f != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.f1032a != null ? this.f1032a.hashCode() : 0) * 31) + this.c) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31;
        if (this.f != null) {
            i = this.f.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastVideoCreative{videoFiles=");
        sb.append(this.f1032a);
        sb.append(", durationSeconds=");
        sb.append(this.c);
        sb.append(", destinationUri=");
        sb.append(this.d);
        sb.append(", clickTrackers=");
        sb.append(this.e);
        sb.append(", eventTrackers=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
}
