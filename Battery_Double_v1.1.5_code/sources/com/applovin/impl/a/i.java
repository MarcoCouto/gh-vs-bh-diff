package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.explorestack.iab.vast.tags.VastTagName;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class i {

    /* renamed from: a reason: collision with root package name */
    private static DateFormat f1031a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
    private static Random b = new Random(System.currentTimeMillis());

    public static Uri a(String str, long j, Uri uri, d dVar, com.applovin.impl.sdk.i iVar) {
        if (URLUtil.isValidUrl(str)) {
            try {
                String replace = str.replace("[ERRORCODE]", Integer.toString(dVar.a()));
                if (j >= 0) {
                    replace = replace.replace("[CONTENTPLAYHEAD]", a(j));
                }
                if (uri != null) {
                    replace = replace.replace("[ASSETURI]", uri.toString());
                }
                return Uri.parse(replace.replace("[CACHEBUSTING]", a()).replace("[TIMESTAMP]", b()));
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to replace macros in URL string ");
                sb.append(str);
                iVar.v().b("VastUtils", sb.toString(), th);
                return null;
            }
        } else {
            iVar.v().e("VastUtils", "Unable to replace macros in invalid URL string.");
            return null;
        }
    }

    public static d a(a aVar) {
        if (b(aVar) || c(aVar)) {
            return null;
        }
        return d.GENERAL_WRAPPER_ERROR;
    }

    private static String a() {
        return Integer.toString(b.nextInt(89999999) + 10000000);
    }

    private static String a(long j) {
        if (j <= 0) {
            return "00:00:00.000";
        }
        return String.format(Locale.US, "%02d:%02d:%02d.000", new Object[]{Long.valueOf(TimeUnit.SECONDS.toHours(j)), Long.valueOf(TimeUnit.SECONDS.toMinutes(j) % TimeUnit.MINUTES.toSeconds(1)), Long.valueOf(j % TimeUnit.MINUTES.toSeconds(1))});
    }

    public static String a(c cVar) {
        if (cVar != null) {
            List b2 = cVar.b();
            int size = cVar.b().size();
            if (size > 0) {
                r c = ((r) b2.get(size - 1)).c(VastTagName.VAST_AD_TAG_URI);
                if (c != null) {
                    return c.c();
                }
            }
            return null;
        }
        throw new IllegalArgumentException("Unable to get resolution uri string for fetching the next wrapper or inline response in the chain");
    }

    public static String a(r rVar, String str, String str2) {
        r b2 = rVar.b(str);
        if (b2 != null) {
            String c = b2.c();
            if (m.b(c)) {
                return c;
            }
        }
        return str2;
    }

    private static Set<g> a(c cVar, com.applovin.impl.sdk.i iVar) {
        if (cVar == null) {
            return null;
        }
        List<r> b2 = cVar.b();
        Set<g> hashSet = new HashSet<>(b2.size());
        for (r rVar : b2) {
            r c = rVar.c(VastTagName.WRAPPER);
            if (c == null) {
                c = rVar.c(VastTagName.IN_LINE);
            }
            hashSet = a(hashSet, c != null ? c.a("Error") : rVar.a("Error"), cVar, iVar);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Retrieved ");
        sb.append(hashSet.size());
        sb.append(" top level error trackers: ");
        sb.append(hashSet);
        iVar.v().b("VastUtils", sb.toString());
        return hashSet;
    }

    private static Set<g> a(Set<g> set, List<r> list, c cVar, com.applovin.impl.sdk.i iVar) {
        if (list != null) {
            for (r a2 : list) {
                g a3 = g.a(a2, cVar, iVar);
                if (a3 != null) {
                    set.add(a3);
                }
            }
        }
        return set;
    }

    public static void a(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, d dVar, int i, com.applovin.impl.sdk.i iVar) {
        if (iVar != null) {
            p.a(appLovinAdLoadListener, cVar.g(), i, iVar);
            a(a(cVar, iVar), dVar, iVar);
            return;
        }
        throw new IllegalArgumentException("Unable to handle failure. No sdk specified.");
    }

    public static void a(r rVar, Map<String, Set<g>> map, c cVar, com.applovin.impl.sdk.i iVar) {
        o v;
        String str;
        String str2;
        if (iVar != null) {
            if (rVar == null) {
                v = iVar.v();
                str = "VastUtils";
                str2 = "Unable to render event trackers; null node provided";
            } else if (map == null) {
                v = iVar.v();
                str = "VastUtils";
                str2 = "Unable to render event trackers; null event trackers provided";
            } else {
                r b2 = rVar.b(VastTagName.TRACKING_EVENTS);
                if (b2 != null) {
                    List<r> a2 = b2.a(VastTagName.TRACKING);
                    if (a2 != null) {
                        for (r rVar2 : a2) {
                            String str3 = (String) rVar2.b().get("event");
                            if (m.b(str3)) {
                                g a3 = g.a(rVar2, cVar, iVar);
                                if (a3 != null) {
                                    Set set = (Set) map.get(str3);
                                    if (set != null) {
                                        set.add(a3);
                                    } else {
                                        HashSet hashSet = new HashSet();
                                        hashSet.add(a3);
                                        map.put(str3, hashSet);
                                    }
                                }
                            } else {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Could not find event for tracking node = ");
                                sb.append(rVar2);
                                iVar.v().e("VastUtils", sb.toString());
                            }
                        }
                    }
                }
                return;
            }
            v.e(str, str2);
            return;
        }
        throw new IllegalArgumentException("Unable to render event trackers. No sdk specified.");
    }

    public static void a(List<r> list, Set<g> set, c cVar, com.applovin.impl.sdk.i iVar) {
        o v;
        String str;
        String str2;
        if (iVar != null) {
            if (list == null) {
                v = iVar.v();
                str = "VastUtils";
                str2 = "Unable to render trackers; null nodes provided";
            } else if (set == null) {
                v = iVar.v();
                str = "VastUtils";
                str2 = "Unable to render trackers; null trackers provided";
            } else {
                for (r a2 : list) {
                    g a3 = g.a(a2, cVar, iVar);
                    if (a3 != null) {
                        set.add(a3);
                    }
                }
                return;
            }
            v.e(str, str2);
            return;
        }
        throw new IllegalArgumentException("Unable to render trackers. No sdk specified.");
    }

    public static void a(Set<g> set, long j, Uri uri, d dVar, com.applovin.impl.sdk.i iVar) {
        if (iVar == null) {
            throw new IllegalArgumentException("Unable to fire trackers. No sdk specified.");
        } else if (set != null && !set.isEmpty()) {
            for (g b2 : set) {
                Uri a2 = a(b2.b(), j, uri, dVar, iVar);
                if (a2 != null) {
                    iVar.N().a(f.k().a(a2.toString()).a(false).a(), false);
                }
            }
        }
    }

    public static void a(Set<g> set, d dVar, com.applovin.impl.sdk.i iVar) {
        a(set, -1, (Uri) null, dVar, iVar);
    }

    public static void a(Set<g> set, com.applovin.impl.sdk.i iVar) {
        a(set, -1, (Uri) null, d.UNSPECIFIED, iVar);
    }

    public static boolean a(r rVar) {
        if (rVar != null) {
            return rVar.c(VastTagName.WRAPPER) != null;
        }
        throw new IllegalArgumentException("Unable to check if a given XmlNode contains a wrapper response");
    }

    private static String b() {
        f1031a.setTimeZone(TimeZone.getDefault());
        return f1031a.format(new Date());
    }

    public static boolean b(a aVar) {
        boolean z = false;
        if (aVar == null) {
            return false;
        }
        j h = aVar.h();
        if (h != null) {
            List a2 = h.a();
            if (a2 != null && !a2.isEmpty()) {
                z = true;
            }
        }
        return z;
    }

    public static boolean b(r rVar) {
        if (rVar != null) {
            return rVar.c(VastTagName.IN_LINE) != null;
        }
        throw new IllegalArgumentException("Unable to check if a given XmlNode contains an inline response");
    }

    public static boolean c(a aVar) {
        boolean z = false;
        if (aVar == null) {
            return false;
        }
        b j = aVar.j();
        if (j != null) {
            e b2 = j.b();
            if (b2 != null && (b2.b() != null || m.b(b2.c()))) {
                z = true;
            }
        }
        return z;
    }
}
