package com.applovin.impl.a;

import android.net.Uri;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import com.explorestack.iab.vast.tags.VastTagName;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class b {

    /* renamed from: a reason: collision with root package name */
    private int f1023a;
    private int b;
    private Uri c;
    private e d;
    private Set<g> e = new HashSet();
    private Map<String, Set<g>> f = new HashMap();

    private b() {
    }

    public static b a(r rVar, b bVar, c cVar, i iVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (iVar != null) {
            if (bVar == null) {
                try {
                    bVar = new b();
                } catch (Throwable th) {
                    iVar.v().b("VastCompanionAd", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (bVar.f1023a == 0 && bVar.b == 0) {
                int a2 = m.a((String) rVar.b().get("width"));
                int a3 = m.a((String) rVar.b().get("height"));
                if (a2 > 0 && a3 > 0) {
                    bVar.f1023a = a2;
                    bVar.b = a3;
                }
            }
            bVar.d = e.a(rVar, bVar.d, iVar);
            if (bVar.c == null) {
                r b2 = rVar.b(VastTagName.COMPANION_CLICK_THROUGH);
                if (b2 != null) {
                    String c2 = b2.c();
                    if (m.b(c2)) {
                        bVar.c = Uri.parse(c2);
                    }
                }
            }
            i.a(rVar.a(VastTagName.COMPANION_CLICK_TRACKING), bVar.e, cVar, iVar);
            i.a(rVar, bVar.f, cVar, iVar);
            return bVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public Uri a() {
        return this.c;
    }

    public e b() {
        return this.d;
    }

    public Set<g> c() {
        return this.e;
    }

    public Map<String, Set<g>> d() {
        return this.f;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f1023a != bVar.f1023a || this.b != bVar.b) {
            return false;
        }
        if (this.c == null ? bVar.c != null : !this.c.equals(bVar.c)) {
            return false;
        }
        if (this.d == null ? bVar.d != null : !this.d.equals(bVar.d)) {
            return false;
        }
        if (this.e == null ? bVar.e != null : !this.e.equals(bVar.e)) {
            return false;
        }
        if (this.f != null) {
            z = this.f.equals(bVar.f);
        } else if (bVar.f != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.f1023a * 31) + this.b) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31;
        if (this.f != null) {
            i = this.f.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastCompanionAd{width=");
        sb.append(this.f1023a);
        sb.append(", height=");
        sb.append(this.b);
        sb.append(", destinationUri=");
        sb.append(this.c);
        sb.append(", nonVideoResource=");
        sb.append(this.d);
        sb.append(", clickTrackers=");
        sb.append(this.e);
        sb.append(", eventTrackers=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
}
