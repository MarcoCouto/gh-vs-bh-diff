package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import com.explorestack.iab.vast.tags.VastTagName;

public class e {

    /* renamed from: a reason: collision with root package name */
    private a f1026a;
    private Uri b;
    private String c;

    public enum a {
        UNSPECIFIED,
        STATIC,
        IFRAME,
        HTML
    }

    private e() {
    }

    static e a(r rVar, e eVar, i iVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (iVar != null) {
            if (eVar == null) {
                try {
                    eVar = new e();
                } catch (Throwable th) {
                    iVar.v().b("VastNonVideoResource", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (eVar.b == null && !m.b(eVar.c)) {
                String a2 = a(rVar, VastTagName.STATIC_RESOURCE);
                if (URLUtil.isValidUrl(a2)) {
                    eVar.b = Uri.parse(a2);
                    eVar.f1026a = a.STATIC;
                    return eVar;
                }
                String a3 = a(rVar, VastTagName.I_FRAME_RESOURCE);
                if (m.b(a3)) {
                    eVar.f1026a = a.IFRAME;
                    if (URLUtil.isValidUrl(a3)) {
                        eVar.b = Uri.parse(a3);
                    } else {
                        eVar.c = a3;
                    }
                    return eVar;
                }
                String a4 = a(rVar, VastTagName.HTML_RESOURCE);
                if (m.b(a4)) {
                    eVar.f1026a = a.HTML;
                    if (URLUtil.isValidUrl(a4)) {
                        eVar.b = Uri.parse(a4);
                    } else {
                        eVar.c = a4;
                    }
                }
            }
            return eVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    private static String a(r rVar, String str) {
        r b2 = rVar.b(str);
        if (b2 != null) {
            return b2.c();
        }
        return null;
    }

    public a a() {
        return this.f1026a;
    }

    public void a(Uri uri) {
        this.b = uri;
    }

    public void a(String str) {
        this.c = str;
    }

    public Uri b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        if (this.f1026a != eVar.f1026a) {
            return false;
        }
        if (this.b == null ? eVar.b != null : !this.b.equals(eVar.b)) {
            return false;
        }
        if (this.c != null) {
            z = this.c.equals(eVar.c);
        } else if (eVar.c != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((this.f1026a != null ? this.f1026a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastNonVideoResource{type=");
        sb.append(this.f1026a);
        sb.append(", resourceUri=");
        sb.append(this.b);
        sb.append(", resourceContents='");
        sb.append(this.c);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
