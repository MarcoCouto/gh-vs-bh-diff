package com.applovin.impl.a;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;

public class f {

    /* renamed from: a reason: collision with root package name */
    private String f1028a;
    private String b;

    private f() {
    }

    public static f a(r rVar, f fVar, i iVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (iVar != null) {
            if (fVar == null) {
                try {
                    fVar = new f();
                } catch (Throwable th) {
                    iVar.v().b("VastSystemInfo", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (!m.b(fVar.f1028a)) {
                String c = rVar.c();
                if (m.b(c)) {
                    fVar.f1028a = c;
                }
            }
            if (!m.b(fVar.b)) {
                String str = (String) rVar.b().get("version");
                if (m.b(str)) {
                    fVar.b = str;
                }
            }
            return fVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (this.f1028a == null ? fVar.f1028a != null : !this.f1028a.equals(fVar.f1028a)) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(fVar.b);
        } else if (fVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.f1028a != null ? this.f1028a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastSystemInfo{name='");
        sb.append(this.f1028a);
        sb.append('\'');
        sb.append(", version='");
        sb.append(this.b);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
