package com.applovin.impl.a;

import com.explorestack.iab.vast.VastError;
import com.ironsource.mediationsdk.logger.IronSourceError;

public enum d {
    UNSPECIFIED(-1),
    XML_PARSING(100),
    GENERAL_WRAPPER_ERROR(VastError.ERROR_CODE_GENERAL_WRAPPER),
    TIMED_OUT(VastError.ERROR_CODE_BAD_URI),
    WRAPPER_LIMIT_REACHED(302),
    NO_WRAPPER_RESPONSE(VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD),
    GENERAL_LINEAR_ERROR(400),
    NO_MEDIA_FILE_PROVIDED(VastError.ERROR_CODE_NO_FILE),
    MEDIA_FILE_TIMEOUT(402),
    MEDIA_FILE_ERROR(VastError.ERROR_CODE_ERROR_SHOWING),
    GENERAL_COMPANION_AD_ERROR(600),
    UNABLE_TO_FETCH_COMPANION_AD_RESOURCE(IronSourceError.ERROR_BN_LOAD_WHILE_LONG_INITIATION),
    CAN_NOT_FIND_COMPANION_AD_RESOURCE(IronSourceError.ERROR_BN_LOAD_PLACEMENT_CAPPED);
    
    private final int n;

    private d(int i) {
        this.n = i;
    }

    public int a() {
        return this.n;
    }
}
