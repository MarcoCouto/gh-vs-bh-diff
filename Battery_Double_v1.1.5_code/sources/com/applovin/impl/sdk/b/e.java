package com.applovin.impl.sdk.b;

import java.util.HashSet;

public class e<T> {

    /* renamed from: a reason: collision with root package name */
    public static final e<String> f1322a = new e<>("com.applovin.sdk.impl.isFirstRun", String.class);
    public static final e<Boolean> b = new e<>("com.applovin.sdk.launched_before", Boolean.class);
    public static final e<String> c = new e<>("com.applovin.sdk.user_id", String.class);
    public static final e<String> d = new e<>("com.applovin.sdk.compass_id", String.class);
    public static final e<String> e = new e<>("com.applovin.sdk.compass_random_token", String.class);
    public static final e<String> f = new e<>("com.applovin.sdk.applovin_random_token", String.class);
    public static final e<String> g = new e<>("com.applovin.sdk.device_test_group", String.class);
    public static final e<String> h = new e<>("com.applovin.sdk.variables", String.class);
    public static final e<Boolean> i = new e<>("com.applovin.sdk.compliance.has_user_consent", Boolean.class);
    public static final e<Boolean> j = new e<>("com.applovin.sdk.compliance.is_age_restricted_user", Boolean.class);
    public static final e<HashSet> k = new e<>("com.applovin.sdk.impl.postbackQueue.key", HashSet.class);
    public static final e<String> l = new e<>("com.applovin.sdk.stats", String.class);
    public static final e<String> m = new e<>("com.applovin.sdk.errors", String.class);
    public static final e<HashSet> n = new e<>("com.applovin.sdk.task.stats", HashSet.class);
    public static final e<String> o = new e<>("com.applovin.sdk.network_response_code_mapping", String.class);
    public static final e<String> p = new e<>("com.applovin.sdk.event_tracking.super_properties", String.class);
    public static final e<String> q = new e<>("com.applovin.sdk.request_tracker.counter", String.class);
    public static final e<String> r = new e<>("com.applovin.sdk.zones", String.class);
    public static final e<HashSet> s = new e<>("com.applovin.sdk.ad.stats", HashSet.class);
    public static final e<Integer> t = new e<>("com.applovin.sdk.last_video_position", Integer.class);
    public static final e<Boolean> u = new e<>("com.applovin.sdk.should_resume_video", Boolean.class);
    public static final e<String> v = new e<>("com.applovin.sdk.mediation.signal_providers", String.class);
    public static final e<String> w = new e<>("com.applovin.sdk.mediation.auto_init_adapters", String.class);
    public static final e<String> x = new e<>("com.applovin.sdk.persisted_data", String.class);
    public static final e<String> y = new e<>("com.applovin.sdk.mediation_provider", String.class);
    public static final e<String> z = new e<>("com.applovin.sdk.mediation.test_mode_network", String.class);
    private final String A;
    private final Class<T> B;

    public e(String str, Class<T> cls) {
        this.A = str;
        this.B = cls;
    }

    public String a() {
        return this.A;
    }

    public Class<T> b() {
        return this.B;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Key{name='");
        sb.append(this.A);
        sb.append('\'');
        sb.append(", type=");
        sb.append(this.B);
        sb.append('}');
        return sb.toString();
    }
}
