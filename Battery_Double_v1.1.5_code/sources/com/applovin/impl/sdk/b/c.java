package com.applovin.impl.sdk.b;

import android.net.Uri;
import com.applovin.impl.a.j.a;
import com.applovin.sdk.AppLovinAdSize;
import com.appodeal.ads.utils.LogConstants;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.tapjoy.TJAdUnitConstants;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class c<T> implements Comparable {
    public static final c<Boolean> R = a("is_disabled", Boolean.valueOf(false));
    public static final c<String> S = a("device_id", "");
    public static final c<String> T = a("device_token", "");
    public static final c<Boolean> U = a("is_verbose_logging", Boolean.valueOf(false));
    public static final c<String> V = a("sc", "");
    public static final c<String> W = a("sc2", "");
    public static final c<String> X = a("server_installed_at", "");
    public static final c<Boolean> Y = a("trn", Boolean.valueOf(false));
    public static final c<Boolean> Z = a("honor_publisher_settings", Boolean.valueOf(true));

    /* renamed from: a reason: collision with root package name */
    private static final List<?> f1320a = Arrays.asList(new Class[]{Boolean.class, Float.class, Integer.class, Long.class, String.class});
    public static final c<String> aA = a("text_alert_consent_no_option", LogConstants.EVENT_CANCEL);
    public static final c<Long> aB = a("ttc_max_click_duration_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final c<Integer> aC = a("ttc_max_click_distance_dp", Integer.valueOf(10));
    public static final c<String> aD = a("whitelisted_postback_endpoints", "https://prod-a.applovin.com,https://rt.applovin.com/4.0/pix, https://rt.applvn.com/4.0/pix,https://ms.applovin.com/,https://ms.applvn.com/");
    public static final c<String> aE = a("fetch_settings_endpoint", "https://ms.applovin.com/");
    public static final c<String> aF = a("fetch_settings_backup_endpoint", "https://ms.applvn.com/");
    public static final c<String> aG = a("adserver_endpoint", "https://a.applovin.com/");
    public static final c<String> aH = a("adserver_backup_endpoint", "https://a.applvn.com/");
    public static final c<String> aI = a("api_endpoint", "https://d.applovin.com/");
    public static final c<String> aJ = a("api_backup_endpoint", "https://d.applvn.com/");
    public static final c<String> aK = a("event_tracking_endpoint_v2", "https://rt.applovin.com/");
    public static final c<String> aL = a("event_tracking_backup_endpoint_v2", "https://rt.applvn.com/");
    public static final c<String> aM = a("fetch_variables_endpoint", "https://ms.applovin.com/");
    public static final c<String> aN = a("fetch_variables_backup_endpoint", "https://ms.applvn.com/");
    public static final c<Boolean> aO = a("bte", Boolean.valueOf(true));
    public static final c<String> aP = a("token_type_prefixes_r", "4!");
    public static final c<String> aQ = a("token_type_prefixes_arj", "json_v3!");
    public static final c<String> aR = a("top_level_events", "landing,paused,resumed,checkout,iap");
    public static final c<Boolean> aS = a("events_enabled", Boolean.valueOf(true));
    public static final c<String> aT;
    public static final c<Boolean> aU = a("persist_super_properties", Boolean.valueOf(true));
    public static final c<Integer> aV = a("super_property_string_max_length", Integer.valueOf(1024));
    public static final c<Integer> aW = a("super_property_url_max_length", Integer.valueOf(1024));
    public static final c<Integer> aX = a("preload_callback_timeout_seconds", Integer.valueOf(-1));
    public static final c<Boolean> aY = a("ad_preload_enabled", Boolean.valueOf(true));
    public static final c<String> aZ = a("ad_auto_preload_sizes", "");
    public static final c<Boolean> aa = a("track_network_response_codes", Boolean.valueOf(false));
    public static final c<Boolean> ab = a("submit_network_response_codes", Boolean.valueOf(false));
    public static final c<Boolean> ac = a("clear_network_response_codes_on_request", Boolean.valueOf(true));
    public static final c<Boolean> ad = a("preserve_network_response_codes", Boolean.valueOf(false));
    public static final c<Boolean> ae = a("clear_completion_callback_on_failure", Boolean.valueOf(false));
    public static final c<Long> af = a("sicd_ms", Long.valueOf(0));
    public static final c<Integer> ag = a("logcat_max_line_size", Integer.valueOf(1000));
    public static final c<Integer> ah = a("stps", Integer.valueOf(32));
    public static final c<Boolean> ai = a("ustp", Boolean.valueOf(false));
    public static final c<Boolean> aj = a("publisher_can_show_consent_dialog", Boolean.valueOf(true));
    public static final c<String> ak = a("consent_dialog_url", "https://assets.applovin.com/gdpr/flow_v1/gdpr-flow-1.html");
    public static final c<Boolean> al = a("consent_dialog_immersive_mode_on", Boolean.valueOf(false));
    public static final c<Long> am = a("consent_dialog_show_from_alert_delay_ms", Long.valueOf(450));
    public static final c<Boolean> an = a("alert_consent_for_dialog_rejected", Boolean.valueOf(false));
    public static final c<Boolean> ao = a("alert_consent_for_dialog_closed", Boolean.valueOf(false));
    public static final c<Boolean> ap = a("alert_consent_for_dialog_closed_with_back_button", Boolean.valueOf(false));
    public static final c<Boolean> aq = a("alert_consent_after_init", Boolean.valueOf(false));
    public static final c<Long> ar = a("alert_consent_after_init_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final c<Long> as = a("alert_consent_after_dialog_rejection_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(30)));
    public static final c<Long> at = a("alert_consent_after_dialog_close_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final c<Long> au = a("alert_consent_after_dialog_close_with_back_button_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final c<Long> av = a("alert_consent_after_cancel_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(10)));
    public static final c<Long> aw = a("alert_consent_reschedule_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final c<String> ax = a("text_alert_consent_title", "Make this App Better and Stay Free!");
    public static final c<String> ay = a("text_alert_consent_body", "If you don't give us consent to use your data, you will be making our ability to support this app harder, which may negatively affect the user experience.");
    public static final c<String> az = a("text_alert_consent_yes_option", "I Agree");
    private static final Map<String, c<?>> b = new HashMap(512);
    public static final c<Boolean> bA = a("honor_publisher_settings_auto_preload_ad_types", Boolean.valueOf(true));
    public static final c<String> bB = a("cdt", "external");
    public static final c<Boolean> bC = a("cache_cleanup_enabled", Boolean.valueOf(false));
    public static final c<Long> bD = a("cache_file_ttl_seconds", Long.valueOf(TimeUnit.DAYS.toSeconds(1)));
    public static final c<Integer> bE = a("cache_max_size_mb", Integer.valueOf(-1));
    public static final c<String> bF = a("precache_delimiters", ")]',");
    public static final c<Boolean> bG = a("native_auto_cache_preload_resources", Boolean.valueOf(true));
    public static final c<Boolean> bH = a("ad_resource_caching_enabled", Boolean.valueOf(true));
    public static final c<Boolean> bI = a("fail_ad_load_on_failed_video_cache", Boolean.valueOf(true));
    public static final c<String> bJ = a("resource_cache_prefix", "https://vid.applovin.com/,https://pdn.applovin.com/,https://img.applovin.com/,https://d.applovin.com/,https://assets.applovin.com/,https://cdnjs.cloudflare.com/,http://vid.applovin.com/,http://pdn.applovin.com/,http://img.applovin.com/,http://d.applovin.com/,http://assets.applovin.com/,http://cdnjs.cloudflare.com/");
    public static final c<Integer> bK = a("vr_retry_count_v1", Integer.valueOf(1));
    public static final c<Integer> bL = a("cr_retry_count_v1", Integer.valueOf(1));
    public static final c<Boolean> bM = a("incent_warning_enabled", Boolean.valueOf(false));
    public static final c<String> bN = a("text_incent_warning_title", "Attention!");
    public static final c<String> bO = a("text_incent_warning_body", "You won’t get your reward if the video hasn’t finished.");
    public static final c<String> bP = a("text_incent_warning_close_option", "Close");
    public static final c<String> bQ = a("text_incent_warning_continue_option", "Keep Watching");
    public static final c<Boolean> bR = a("incent_nonvideo_warning_enabled", Boolean.valueOf(false));
    public static final c<String> bS = a("text_incent_nonvideo_warning_title", "Attention!");
    public static final c<String> bT = a("text_incent_nonvideo_warning_body", "You won’t get your reward if the game hasn’t finished.");
    public static final c<String> bU = a("text_incent_nonvideo_warning_close_option", "Close");
    public static final c<String> bV = a("text_incent_nonvideo_warning_continue_option", "Keep Playing");
    public static final c<Boolean> bW = a("video_callbacks_for_incent_nonvideo_ads_enabled", Boolean.valueOf(true));
    public static final c<Boolean> bX = a("wrapped_zones", Boolean.valueOf(false));
    public static final c<String> bY = a("wrapped_sizes", "");
    public static final c<Boolean> bZ = a("return_wrapped_ad_on_empty_queue", Boolean.valueOf(false));
    public static final c<Boolean> ba = a("ad_auto_preload_incent", Boolean.valueOf(true));
    public static final c<Boolean> bb = a("ad_auto_preload_native", Boolean.valueOf(false));
    public static final c<Boolean> bc = a("preload_native_ad_on_dequeue", Boolean.valueOf(false));
    public static final c<Integer> bd = a("preload_capacity_banner_regular", Integer.valueOf(0));
    public static final c<Integer> be = a("preload_capacity_mrec_regular", Integer.valueOf(0));
    public static final c<Integer> bf = a("preload_capacity_leader_regular", Integer.valueOf(0));
    public static final c<Integer> bg = a("preload_capacity_inter_regular", Integer.valueOf(0));
    public static final c<Integer> bh = a("preload_capacity_inter_videoa", Integer.valueOf(0));
    public static final c<Boolean> bi = a("use_per_format_cache_queues", Boolean.valueOf(true));
    public static final c<Integer> bj = a("extended_preload_capacity_banner_regular", Integer.valueOf(15));
    public static final c<Integer> bk = a("extended_preload_capacity_mrec_regular", Integer.valueOf(15));
    public static final c<Integer> bl = a("extended_preload_capacity_leader_regular", Integer.valueOf(15));
    public static final c<Integer> bm = a("extended_preload_capacity_inter_regular", Integer.valueOf(15));
    public static final c<Integer> bn = a("extended_preload_capacity_inter_videoa", Integer.valueOf(15));
    public static final c<Integer> bo = a("preload_capacity_zone", Integer.valueOf(1));
    public static final c<Integer> bp = a("preload_capacity_zone_native", Integer.valueOf(1));
    public static final c<Integer> bq = a("extended_preload_capacity_zone", Integer.valueOf(15));
    public static final c<Integer> br = a("preload_capacity_native_native", Integer.valueOf(0));
    public static final c<Boolean> bs = a("preload_merge_init_tasks_inter_regular", Boolean.valueOf(false));
    public static final c<Boolean> bt = a("preload_merge_init_tasks_inter_videoa", Boolean.valueOf(false));
    public static final c<Boolean> bu = a("preload_merge_init_tasks_banner_regular", Boolean.valueOf(false));
    public static final c<Boolean> bv = a("preload_merge_init_tasks_mrec_regular", Boolean.valueOf(false));
    public static final c<Boolean> bw = a("preload_merge_init_tasks_leader_regular", Boolean.valueOf(false));
    public static final c<Boolean> bx = a("preload_merge_init_tasks_zones", Boolean.valueOf(false));
    public static final c<Boolean> by = a("honor_publisher_settings_verbose_logging", Boolean.valueOf(true));
    public static final c<Boolean> bz = a("honor_publisher_settings_auto_preload_ad_sizes", Boolean.valueOf(true));
    public static final c<Long> cA = a("fullscreen_ad_showing_state_timeout_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(2)));
    public static final c<Boolean> cB = a("lhs_close_button_video", Boolean.valueOf(false));
    public static final c<Integer> cC = a("close_button_right_margin_video", Integer.valueOf(4));
    public static final c<Integer> cD = a("close_button_size_video", Integer.valueOf(30));
    public static final c<Integer> cE = a("close_button_top_margin_video", Integer.valueOf(8));
    public static final c<Integer> cF = a("close_fade_in_time", Integer.valueOf(400));
    public static final c<Boolean> cG = a("show_close_on_exit", Boolean.valueOf(true));
    public static final c<Integer> cH = a("video_countdown_clock_margin", Integer.valueOf(10));
    public static final c<Integer> cI = a("video_countdown_clock_gravity", Integer.valueOf(83));
    public static final c<Integer> cJ = a("countdown_clock_size", Integer.valueOf(32));
    public static final c<Integer> cK = a("countdown_clock_stroke_size", Integer.valueOf(4));
    public static final c<Integer> cL = a("countdown_clock_text_size", Integer.valueOf(28));
    public static final c<Boolean> cM = a("draw_countdown_clock", Boolean.valueOf(true));
    public static final c<Boolean> cN = a("force_back_button_enabled_always", Boolean.valueOf(false));
    public static final c<Boolean> cO = a("force_back_button_enabled_close_button", Boolean.valueOf(false));
    public static final c<Boolean> cP = a("force_back_button_enabled_poststitial", Boolean.valueOf(false));
    public static final c<Long> cQ = a("force_hide_status_bar_delay_ms", Long.valueOf(0));
    public static final c<Boolean> cR = a("handle_window_actions", Boolean.valueOf(false));
    public static final c<Long> cS = a("inter_display_delay", Long.valueOf(200));
    public static final c<Boolean> cT = a("lock_specific_orientation", Boolean.valueOf(false));
    public static final c<Boolean> cU = a("lhs_skip_button", Boolean.valueOf(true));
    public static final c<String> cV = a("soft_buttons_resource_id", "config_showNavigationBar");
    public static final c<Boolean> cW = a("countdown_toggleable", Boolean.valueOf(false));
    public static final c<Boolean> cX = a("mute_controls_enabled", Boolean.valueOf(false));
    public static final c<Boolean> cY = a("allow_user_muting", Boolean.valueOf(true));
    public static final c<Boolean> cZ = a("mute_videos", Boolean.valueOf(false));
    public static final c<Boolean> ca = a("consider_wrapped_ad_preloaded", Boolean.valueOf(false));
    public static final c<Boolean> cb = a("check_webview_has_gesture", Boolean.valueOf(false));
    public static final c<Integer> cc = a("close_button_touch_area", Integer.valueOf(0));
    public static final c<Long> cd = a("viewability_adview_imp_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final c<Integer> ce = a("viewability_adview_banner_min_width", Integer.valueOf(ModuleDescriptor.MODULE_VERSION));
    public static final c<Integer> cf = a("viewability_adview_banner_min_height", Integer.valueOf(AppLovinAdSize.BANNER.getHeight()));
    public static final c<Integer> cg = a("viewability_adview_mrec_min_width", Integer.valueOf(AppLovinAdSize.MREC.getWidth()));
    public static final c<Integer> ch = a("viewability_adview_mrec_min_height", Integer.valueOf(AppLovinAdSize.MREC.getWidth()));
    public static final c<Integer> ci = a("viewability_adview_leader_min_width", Integer.valueOf(728));
    public static final c<Integer> cj = a("viewability_adview_leader_min_height", Integer.valueOf(AppLovinAdSize.LEADER.getWidth()));
    public static final c<Float> ck = a("viewability_adview_min_alpha", Float.valueOf(10.0f));
    public static final c<Long> cl = a("viewability_timer_min_visible_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final c<Long> cm = a("viewability_timer_interval_ms", Long.valueOf(100));
    public static final c<Boolean> cn = a("dismiss_expanded_adview_on_refresh", Boolean.valueOf(false));
    public static final c<Boolean> co = a("dismiss_expanded_adview_on_detach", Boolean.valueOf(false));
    public static final c<Boolean> cp = a("contract_expanded_ad_on_close", Boolean.valueOf(true));
    public static final c<Long> cq = a("expandable_close_button_animation_duration_ms", Long.valueOf(300));
    public static final c<Integer> cr = a("expandable_close_button_size", Integer.valueOf(27));
    public static final c<Integer> cs = a("expandable_h_close_button_margin", Integer.valueOf(10));
    public static final c<Integer> ct = a("expandable_t_close_button_margin", Integer.valueOf(10));
    public static final c<Boolean> cu = a("expandable_lhs_close_button", Boolean.valueOf(false));
    public static final c<Integer> cv = a("expandable_close_button_touch_area", Integer.valueOf(0));
    public static final c<Boolean> cw = a("click_failed_expand", Boolean.valueOf(false));
    public static final c<Integer> cx = a("auxiliary_operations_threads", Integer.valueOf(3));
    public static final c<Integer> cy = a("caching_operations_threads", Integer.valueOf(8));
    public static final c<Long> cz = a("fullscreen_ad_pending_display_state_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(10)));
    public static final c<Integer> dA = a("submit_data_retry_count_v1", Integer.valueOf(1));
    public static final c<Integer> dB = a("response_buffer_size", Integer.valueOf(16000));
    public static final c<Integer> dC = a("fetch_basic_settings_connection_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final c<Integer> dD = a("fetch_basic_settings_retry_count", Integer.valueOf(3));
    public static final c<Boolean> dE = a("fetch_basic_settings_on_reconnect", Boolean.valueOf(false));
    public static final c<Boolean> dF = a("skip_fetch_basic_settings_if_not_connected", Boolean.valueOf(false));
    public static final c<Integer> dG = a("fetch_basic_settings_retry_delay_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(2)));
    public static final c<Integer> dH = a("fetch_variables_connection_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(5)));
    public static final c<Boolean> dI = a("preload_persisted_zones", Boolean.valueOf(true));
    public static final c<Boolean> dJ = a("persist_zones", Boolean.valueOf(true));
    public static final c<Integer> dK = a("ad_session_minutes", Integer.valueOf(60));
    public static final c<Boolean> dL = a("session_tracking_cooldown_on_event_fire", Boolean.valueOf(true));
    public static final c<Long> dM = a("session_tracking_resumed_cooldown_minutes", Long.valueOf(90));
    public static final c<Long> dN = a("session_tracking_paused_cooldown_minutes", Long.valueOf(90));
    public static final c<Boolean> dO = a("track_app_paused", Boolean.valueOf(false));
    public static final c<Boolean> dP = a("qq", Boolean.valueOf(false));
    public static final c<Boolean> dQ = a("qq1", Boolean.valueOf(true));
    public static final c<Boolean> dR = a("qq3", Boolean.valueOf(true));
    public static final c<Boolean> dS = a("qq4", Boolean.valueOf(true));
    public static final c<Boolean> dT = a("qq5", Boolean.valueOf(true));
    public static final c<Boolean> dU = a("qq6", Boolean.valueOf(true));
    public static final c<Boolean> dV = a("qq7", Boolean.valueOf(true));
    public static final c<Boolean> dW = a("qq8", Boolean.valueOf(true));
    public static final c<Boolean> dX = a("pui", Boolean.valueOf(true));
    public static final c<String> dY = a("plugin_version", "");
    public static final c<Boolean> dZ = a("hgn", Boolean.valueOf(false));
    public static final c<Boolean> da = a("show_mute_by_default", Boolean.valueOf(false));
    public static final c<Boolean> db = a("mute_with_user_settings", Boolean.valueOf(true));
    public static final c<Integer> dc = a("mute_button_size", Integer.valueOf(32));
    public static final c<Integer> dd = a("mute_button_margin", Integer.valueOf(10));
    public static final c<Integer> de = a("mute_button_gravity", Integer.valueOf(85));
    public static final c<Boolean> df = a("video_immersive_mode_enabled", Boolean.valueOf(false));
    public static final c<Long> dg = a("progress_bar_step", Long.valueOf(25));
    public static final c<Integer> dh = a("progress_bar_scale", Integer.valueOf(10000));
    public static final c<Integer> di = a("progress_bar_vertical_padding", Integer.valueOf(-8));
    public static final c<Long> dj = a("video_resume_delay", Long.valueOf(250));
    public static final c<Boolean> dk = a("is_video_skippable", Boolean.valueOf(false));
    public static final c<Integer> dl = a("vs_buffer_indicator_size", Integer.valueOf(50));
    public static final c<Boolean> dm = a("video_zero_length_as_computed", Boolean.valueOf(false));
    public static final c<Long> dn = a("set_poststitial_muted_initial_delay_ms", Long.valueOf(500));

    /* renamed from: do reason: not valid java name */
    public static final c<Boolean> f0do = a("widget_fail_on_slot_count_diff", Boolean.valueOf(true));
    public static final c<Integer> dp = a("native_batch_precache_count", Integer.valueOf(1));
    public static final c<Integer> dq = a("submit_postback_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final c<Integer> dr = a("submit_postback_retries", Integer.valueOf(4));
    public static final c<Integer> ds = a("max_postback_attempts", Integer.valueOf(3));
    public static final c<Integer> dt = a("get_retry_delay_v1", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final c<Integer> du = a("http_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final c<Integer> dv = a("http_socket_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(20)));
    public static final c<Boolean> dw = a("force_ssl", Boolean.valueOf(false));
    public static final c<Boolean> dx = a("network_available_if_none_detected", Boolean.valueOf(true));
    public static final c<Integer> dy = a("fetch_ad_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final c<Integer> dz = a("fetch_ad_retry_count_v1", Integer.valueOf(1));
    public static final c<Integer> eA = a("vast_max_response_length", Integer.valueOf(640000));
    public static final c<Integer> eB = a("vast_max_wrapper_depth", Integer.valueOf(5));
    public static final c<Long> eC = a("vast_progress_tracking_countdown_step", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final c<String> eD = a("vast_unsupported_video_extensions", "ogv,flv");
    public static final c<String> eE = a("vast_unsupported_video_types", "video/ogg,video/x-flv");
    public static final c<Boolean> eF = a("vast_validate_with_extension_if_no_video_type", Boolean.valueOf(true));
    public static final c<Integer> eG = a("vast_video_selection_policy", Integer.valueOf(a.MEDIUM.ordinal()));
    public static final c<Integer> eH = a("vast_wrapper_resolution_retry_count_v1", Integer.valueOf(1));
    public static final c<Integer> eI = a("vast_wrapper_resolution_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final c<Boolean> eJ = a("ree", Boolean.valueOf(true));
    public static final c<Boolean> eK = a("btee", Boolean.valueOf(true));
    public static final c<Long> eL = a("server_timestamp_ms", Long.valueOf(0));
    public static final c<Long> eM = a("device_timestamp_ms", Long.valueOf(0));
    public static final c<Boolean> eN = a("immediate_render", Boolean.valueOf(false));
    public static final c<Boolean> eO = a("cleanup_webview", Boolean.valueOf(false));
    public static final c<Boolean> eP = a("sanitize_webview", Boolean.valueOf(false));
    public static final c<Boolean> eQ = a("force_rerender", Boolean.valueOf(false));
    public static final c<Boolean> eR = a("ignore_is_showing", Boolean.valueOf(false));
    public static final c<Boolean> eS = a("render_empty_adview", Boolean.valueOf(true));
    public static final c<Boolean> eT = a("daostr", Boolean.valueOf(false));
    public static final c<Boolean> eU = a("urrr", Boolean.valueOf(false));
    public static final c<String> eV = a("config_consent_dialog_state", "unknown");
    public static final c<Boolean> ea = a("citab", Boolean.valueOf(false));
    public static final c<Boolean> eb = a("cit", Boolean.valueOf(false));
    public static final c<Boolean> ec = a("cso", Boolean.valueOf(false));
    public static final c<Boolean> ed = a("cfs", Boolean.valueOf(false));
    public static final c<Boolean> ee = a("cmi", Boolean.valueOf(false));
    public static final c<Boolean> ef = a("cvs", Boolean.valueOf(false));
    public static final c<String> eg = a("emulator_hardware_list", "ranchu,goldfish,vbox");
    public static final c<String> eh = a("emulator_device_list", "generic,vbox");
    public static final c<String> ei = a("emulator_manufacturer_list", "Genymotion");
    public static final c<String> ej = a("emulator_model_list", "Android SDK built for x86");
    public static final c<Boolean> ek = a("adr", Boolean.valueOf(false));
    public static final c<Float> el = a("volume_normalization_factor", Float.valueOf(6.6666665f));
    public static final c<Boolean> em = a("user_agent_collection_enabled", Boolean.valueOf(false));
    public static final c<Long> en = a("user_agent_collection_timeout_ms", Long.valueOf(600));
    public static final c<String> eo = a("webview_package_name", "com.google.android.webview");
    public static final c<Boolean> ep = a("is_track_ad_info", Boolean.valueOf(true));
    public static final c<Boolean> eq = a("submit_ad_stats_enabled", Boolean.valueOf(false));
    public static final c<Integer> er = a("submit_ad_stats_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final c<Integer> es = a("submit_ad_stats_retry_count", Integer.valueOf(1));
    public static final c<Integer> et = a("submit_ad_stats_max_count", Integer.valueOf(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL));
    public static final c<Boolean> eu = a("asdm", Boolean.valueOf(false));
    public static final c<Boolean> ev = a("task_stats_enabled", Boolean.valueOf(false));
    public static final c<Boolean> ew = a("error_reporting_enabled", Boolean.valueOf(false));
    public static final c<Integer> ex = a("error_reporting_log_limit", Integer.valueOf(100));
    public static final c<String> ey = a("vast_image_html", "<html><head><style>html,body{height:100%;width:100%}body{background-image:url({SOURCE});background-repeat:no-repeat;background-size:contain;background-position:center;}a{position:absolute;top:0;bottom:0;left:0;right:0}</style></head><body><a href=\"applovin://com.applovin.sdk/adservice/track_click_now\"></a></body></html>");
    public static final c<String> ez = a("vast_link_html", "<html><head><style>html,body,iframe{height:100%;width:100%;}body{margin:0}iframe{border:0;overflow:hidden;position:absolute}</style></head><body><iframe src={SOURCE} frameborder=0></iframe></body></html>");
    private final String c;
    private final T d;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(String.class.getName());
        sb.append(",");
        sb.append(Integer.class.getName());
        sb.append(",");
        sb.append(Long.class.getName());
        sb.append(",");
        sb.append(Double.class.getName());
        sb.append(",");
        sb.append(Float.class.getName());
        sb.append(",");
        sb.append(Date.class.getName());
        sb.append(",");
        sb.append(Uri.class.getName());
        sb.append(",");
        sb.append(List.class.getName());
        sb.append(",");
        sb.append(Map.class.getName());
        aT = a("valid_super_property_types", sb.toString());
    }

    public c(String str, T t) {
        if (str == null) {
            throw new IllegalArgumentException("No name specified");
        } else if (t != null) {
            this.c = str;
            this.d = t;
        } else {
            throw new IllegalArgumentException("No default value specified");
        }
    }

    protected static <T> c<T> a(String str, T t) {
        if (t == null) {
            throw new IllegalArgumentException("No default value specified");
        } else if (f1320a.contains(t.getClass())) {
            c<T> cVar = new c<>(str, t);
            if (!b.containsKey(str)) {
                b.put(str, cVar);
                return cVar;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Setting has already been used: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Unsupported value type: ");
            sb2.append(t.getClass());
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    public static Collection<c<?>> c() {
        return Collections.unmodifiableCollection(b.values());
    }

    /* access modifiers changed from: 0000 */
    public T a(Object obj) {
        return this.d.getClass().cast(obj);
    }

    public String a() {
        return this.c;
    }

    public T b() {
        return this.d;
    }

    public int compareTo(Object obj) {
        if (!(obj instanceof c)) {
            return 0;
        }
        return this.c.compareTo(((c) obj).a());
    }
}
