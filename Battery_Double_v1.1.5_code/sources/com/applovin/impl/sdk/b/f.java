package com.applovin.impl.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import java.util.Set;

public final class f {

    /* renamed from: a reason: collision with root package name */
    private static SharedPreferences f1323a;
    private final SharedPreferences b;

    public f(i iVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("com.applovin.sdk.preferences.");
        sb.append(iVar.t());
        this.b = iVar.D().getSharedPreferences(sb.toString(), 0);
    }

    private static SharedPreferences a(Context context) {
        if (f1323a == null) {
            f1323a = context.getSharedPreferences("com.applovin.sdk.shared", 0);
        }
        return f1323a;
    }

    public static <T> T a(String str, T t, Class cls, SharedPreferences sharedPreferences) {
        T t2;
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            if (sharedPreferences.contains(str)) {
                if (Boolean.class.equals(cls)) {
                    t2 = Boolean.valueOf(t != null ? sharedPreferences.getBoolean(str, ((Boolean) t).booleanValue()) : sharedPreferences.getBoolean(str, false));
                } else if (Float.class.equals(cls)) {
                    t2 = Float.valueOf(t != null ? sharedPreferences.getFloat(str, ((Float) t).floatValue()) : sharedPreferences.getFloat(str, 0.0f));
                } else if (Integer.class.equals(cls)) {
                    t2 = Integer.valueOf(t != null ? sharedPreferences.getInt(str, ((Integer) t).intValue()) : sharedPreferences.getInt(str, 0));
                } else if (Long.class.equals(cls)) {
                    t2 = Long.valueOf(t != null ? sharedPreferences.getLong(str, ((Long) t).longValue()) : sharedPreferences.getLong(str, 0));
                } else {
                    t2 = String.class.equals(cls) ? sharedPreferences.getString(str, (String) t) : Set.class.isAssignableFrom(cls) ? sharedPreferences.getStringSet(str, (Set) t) : t;
                }
                if (t2 != null) {
                    return cls.cast(t2);
                }
                StrictMode.setThreadPolicy(allowThreadDiskReads);
                return t;
            }
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return t;
        } catch (Throwable th) {
            String str2 = "SharedPreferencesManager";
            StringBuilder sb = new StringBuilder();
            sb.append("Error getting value for key: ");
            sb.append(str);
            o.c(str2, sb.toString(), th);
            return t;
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    public static <T> void a(e<T> eVar, Context context) {
        a(context).edit().remove(eVar.a()).apply();
    }

    public static <T> void a(e<T> eVar, T t, Context context) {
        a(eVar.a(), t, a(context), (Editor) null);
    }

    private static <T> void a(String str, T t, SharedPreferences sharedPreferences, Editor editor) {
        boolean z = false;
        boolean z2 = editor != null;
        if (!z2) {
            editor = sharedPreferences.edit();
        }
        if (t instanceof Boolean) {
            editor.putBoolean(str, ((Boolean) t).booleanValue());
        } else if (t instanceof Float) {
            editor.putFloat(str, ((Float) t).floatValue());
        } else if (t instanceof Integer) {
            editor.putInt(str, ((Integer) t).intValue());
        } else if (t instanceof Long) {
            editor.putLong(str, ((Long) t).longValue());
        } else if (t instanceof String) {
            editor.putString(str, (String) t);
        } else if (t instanceof Set) {
            editor.putStringSet(str, (Set) t);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to put default value of invalid type: ");
            sb.append(t);
            o.i("SharedPreferencesManager", sb.toString());
            if (z && !z2) {
                editor.apply();
                return;
            }
        }
        z = true;
        if (z) {
        }
    }

    public static <T> T b(e<T> eVar, T t, Context context) {
        return a(eVar.a(), t, eVar.b(), a(context));
    }

    public void a(SharedPreferences sharedPreferences) {
        sharedPreferences.edit().clear().apply();
    }

    public <T> void a(e<T> eVar) {
        this.b.edit().remove(eVar.a()).apply();
    }

    public <T> void a(e<T> eVar, T t) {
        a(eVar, t, this.b);
    }

    public <T> void a(e<T> eVar, T t, SharedPreferences sharedPreferences) {
        a(eVar.a(), t, sharedPreferences);
    }

    public <T> void a(String str, T t, Editor editor) {
        a(str, t, (SharedPreferences) null, editor);
    }

    public <T> void a(String str, T t, SharedPreferences sharedPreferences) {
        a(str, t, sharedPreferences, (Editor) null);
    }

    public <T> T b(e<T> eVar, T t) {
        return b(eVar, t, this.b);
    }

    public <T> T b(e<T> eVar, T t, SharedPreferences sharedPreferences) {
        return a(eVar.a(), t, eVar.b(), sharedPreferences);
    }
}
