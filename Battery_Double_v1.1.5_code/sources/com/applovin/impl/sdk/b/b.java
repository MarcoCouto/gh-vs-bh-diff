package com.applovin.impl.sdk.b;

import com.applovin.mediation.adapters.AppLovinMediationAdapter;
import java.util.concurrent.TimeUnit;

public class b<T> extends c<T> {
    public static final c<Long> A = a("fullscreen_display_delay_ms", Long.valueOf(600));
    public static final c<Long> B = a("ahdm", Long.valueOf(500));
    public static final c<Long> C = a("ad_view_refresh_precache_request_viewability_undesired_flags", Long.valueOf(118));
    public static final c<Long> D = a("ad_view_refresh_precache_request_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(2)));
    public static final c<Boolean> E = a("ad_view_block_publisher_load_if_refresh_scheduled", Boolean.valueOf(true));
    public static final c<Boolean> F = a("fullscreen_ads_block_publisher_load_if_another_showing", Boolean.valueOf(true));
    public static final c<Long> G = a("ad_expiration_ms", Long.valueOf(TimeUnit.HOURS.toMillis(4)));
    public static final c<Boolean> H = a("saewib", Boolean.valueOf(false));
    public static final c<Boolean> I = a("utaoae", Boolean.valueOf(false));
    public static final c<Long> J = a("ad_hidden_timeout_ms", Long.valueOf(-1));
    public static final c<Boolean> K = a("schedule_ad_hidden_on_ad_dismiss", Boolean.valueOf(false));
    public static final c<Long> L = a("ad_hidden_on_ad_dismiss_callback_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final c<Boolean> M = a("proe", Boolean.valueOf(false));
    public static final c<String> N = a("fitaui", "");
    public static final c<String> O = a("finaui", "");
    public static final c<String> P = a("faespcn", AppLovinMediationAdapter.class.getName());
    public static final c<Long> Q = a("fard_s", Long.valueOf(3));

    /* renamed from: a reason: collision with root package name */
    public static final c<String> f1319a = a("afi", "");
    public static final c<Long> b = a("afi_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(5)));
    public static final c<String> c = a("mediation_endpoint", "https://ms.applovin.com/");
    public static final c<String> d = a("mediation_backup_endpoint", "https://ms.applvn.com/");
    public static final c<Long> e = a("fetch_next_ad_retry_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(2)));
    public static final c<Long> f = a("fetch_next_ad_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(5)));
    public static final c<Long> g = a("fetch_mediation_debugger_info_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(7)));
    public static final c<Boolean> h = a("pass_extra_parameters", Boolean.valueOf(true));
    public static final c<String> i = a("postback_macros", "{\"{MCODE}\":\"mcode\",\"{BCODE}\":\"bcode\",\"{ICODE}\":\"icode\",\"{SCODE}\":\"scode\"}");
    public static final c<Boolean> j = a("persistent_mediated_postbacks", Boolean.valueOf(false));
    public static final c<Long> k = a("max_signal_provider_latency_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(30)));
    public static final c<Integer> l = a("max_adapter_version_length", Integer.valueOf(20));
    public static final c<Integer> m = a("max_adapter_sdk_version_length", Integer.valueOf(20));
    public static final c<Integer> n = a("max_adapter_signal_length", Integer.valueOf(5120));
    public static final c<Long> o = a("default_adapter_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(10)));
    public static final c<Integer> p = a("default_ad_view_width", Integer.valueOf(-2));
    public static final c<Integer> q = a("default_ad_view_height", Integer.valueOf(-2));
    public static final c<Long> r = a("ad_refresh_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(30)));
    public static final c<Long> s = a("ad_load_failure_refresh_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(30)));
    public static final c<String> t = a("ad_load_failure_refresh_ignore_error_codes", "204");
    public static final c<Long> u = a("refresh_ad_on_app_resume_elapsed_threshold_ms", Long.valueOf(0));
    public static final c<Boolean> v = a("refresh_ad_view_timer_responds_to_background", Boolean.valueOf(true));
    public static final c<Boolean> w = a("refresh_ad_view_timer_responds_to_store_kit", Boolean.valueOf(true));
    public static final c<Boolean> x = a("refresh_ad_view_timer_responds_to_window_visibility_changed", Boolean.valueOf(false));
    public static final c<Long> y = a("ad_view_fade_in_animation_ms", Long.valueOf(150));
    public static final c<Long> z = a("ad_view_fade_out_animation_ms", Long.valueOf(150));
}
