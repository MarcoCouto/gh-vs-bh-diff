package com.applovin.impl.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkSettings;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class d {

    /* renamed from: a reason: collision with root package name */
    protected final i f1321a;
    protected final o b;
    protected final Context c;
    protected final SharedPreferences d;
    private final Map<String, Object> e = new HashMap();
    private Map<String, Object> f;

    /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0035 */
    public d(i iVar) {
        this.f1321a = iVar;
        this.b = iVar.v();
        this.c = iVar.D();
        this.d = this.c.getSharedPreferences("com.applovin.sdk.1", 0);
        Class.forName(c.class.getName());
        Class.forName(b.class.getName());
        Field a2 = p.a(iVar.l().getClass(), "localSettings");
        a2.setAccessible(true);
        this.f = (HashMap) a2.get(iVar.l());
    }

    private static Object a(String str, JSONObject jSONObject, Object obj) throws JSONException {
        if (obj instanceof Boolean) {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        }
        if (obj instanceof Float) {
            return Float.valueOf((float) jSONObject.getDouble(str));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        if (obj instanceof Long) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        if (obj instanceof String) {
            return jSONObject.getString(str);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SDK Error: unknown value type: ");
        sb.append(obj.getClass());
        throw new RuntimeException(sb.toString());
    }

    private <T> T c(c<T> cVar) {
        try {
            return cVar.a(this.f.get(cVar.a()));
        } catch (Throwable unused) {
            return null;
        }
    }

    private String e() {
        StringBuilder sb = new StringBuilder();
        sb.append("com.applovin.sdk.");
        sb.append(p.a(this.f1321a.t()));
        sb.append(".");
        return sb.toString();
    }

    public <ST> c<ST> a(String str, c<ST> cVar) {
        for (c<ST> cVar2 : c.c()) {
            if (cVar2.a().equals(str)) {
                return cVar2;
            }
        }
        return cVar;
    }

    public <T> T a(c<T> cVar) {
        if (cVar != null) {
            synchronized (this.e) {
                try {
                    T c2 = c(cVar);
                    if (c2 != null) {
                        return c2;
                    }
                    Object obj = this.e.get(cVar.a());
                    if (obj != null) {
                        T a2 = cVar.a(obj);
                        return a2;
                    }
                    T b2 = cVar.b();
                    return b2;
                } catch (Throwable th) {
                    throw th;
                }
            }
        } else {
            throw new IllegalArgumentException("No setting type specified");
        }
    }

    public void a() {
        if (this.c != null) {
            String e2 = e();
            synchronized (this.e) {
                Editor edit = this.d.edit();
                for (c cVar : c.c()) {
                    Object obj = this.e.get(cVar.a());
                    if (obj != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(e2);
                        sb.append(cVar.a());
                        this.f1321a.a(sb.toString(), obj, edit);
                    }
                }
                edit.apply();
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public <T> void a(c<?> cVar, Object obj) {
        if (cVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        } else if (obj != null) {
            synchronized (this.e) {
                this.e.put(cVar.a(), obj);
            }
        } else {
            throw new IllegalArgumentException("No new value specified");
        }
    }

    public void a(AppLovinSdkSettings appLovinSdkSettings) {
        boolean z;
        boolean z2;
        if (appLovinSdkSettings != null) {
            synchronized (this.e) {
                if (((Boolean) this.f1321a.a(c.U)).booleanValue()) {
                    this.e.put(c.U.a(), Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled()));
                }
                if (((Boolean) this.f1321a.a(c.bz)).booleanValue()) {
                    String autoPreloadSizes = appLovinSdkSettings.getAutoPreloadSizes();
                    if (!m.b(autoPreloadSizes)) {
                        autoPreloadSizes = "NONE";
                    }
                    if (autoPreloadSizes.equals("NONE")) {
                        this.e.put(c.aZ.a(), "");
                    } else {
                        this.e.put(c.aZ.a(), autoPreloadSizes);
                    }
                }
                if (((Boolean) this.f1321a.a(c.bA)).booleanValue()) {
                    String autoPreloadTypes = appLovinSdkSettings.getAutoPreloadTypes();
                    if (!m.b(autoPreloadTypes)) {
                        autoPreloadTypes = "NONE";
                    }
                    boolean z3 = false;
                    if (!"NONE".equals(autoPreloadTypes)) {
                        z2 = false;
                        z = false;
                        for (String str : e.a(autoPreloadTypes)) {
                            if (str.equals(AppLovinAdType.REGULAR.getLabel())) {
                                z3 = true;
                            } else {
                                if (!str.equals(AppLovinAdType.INCENTIVIZED.getLabel()) && !str.contains("INCENT")) {
                                    if (!str.contains("REWARD")) {
                                        if (str.equals(AppLovinAdType.NATIVE.getLabel())) {
                                            z = true;
                                        }
                                    }
                                }
                                z2 = true;
                            }
                        }
                    } else {
                        z2 = false;
                        z = false;
                    }
                    if (!z3) {
                        this.e.put(c.aZ.a(), "");
                    }
                    this.e.put(c.ba.a(), Boolean.valueOf(z2));
                    this.e.put(c.bb.a(), Boolean.valueOf(z));
                }
            }
        }
    }

    public void a(JSONObject jSONObject) {
        o oVar;
        String str;
        String str2;
        synchronized (this.e) {
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                if (str3 != null && str3.length() > 0) {
                    try {
                        c<Long> a2 = a(str3, null);
                        if (a2 != null) {
                            this.e.put(a2.a(), a(str3, jSONObject, a2.b()));
                            if (a2 == c.eL) {
                                this.e.put(c.eM.a(), Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    } catch (JSONException e2) {
                        th = e2;
                        oVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to parse JSON settingsValues array";
                        oVar.b(str, str2, th);
                    } catch (Throwable th) {
                        th = th;
                        oVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to convert setting object ";
                        oVar.b(str, str2, th);
                    }
                }
            }
        }
    }

    public List<String> b(c<String> cVar) {
        return e.a((String) a(cVar));
    }

    public void b() {
        if (this.c != null) {
            String e2 = e();
            synchronized (this.e) {
                for (c cVar : c.c()) {
                    try {
                        StringBuilder sb = new StringBuilder();
                        sb.append(e2);
                        sb.append(cVar.a());
                        Object a2 = this.f1321a.a(sb.toString(), null, cVar.b().getClass(), this.d);
                        if (a2 != null) {
                            this.e.put(cVar.a(), a2);
                        }
                    } catch (Exception e3) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unable to load \"");
                        sb2.append(cVar.a());
                        sb2.append("\"");
                        this.b.b("SettingsManager", sb2.toString(), e3);
                    }
                }
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public void c() {
        synchronized (this.e) {
            this.e.clear();
        }
        this.f1321a.a(this.d);
    }

    public boolean d() {
        return this.f1321a.l().isVerboseLoggingEnabled() || ((Boolean) a(c.U)).booleanValue();
    }
}
