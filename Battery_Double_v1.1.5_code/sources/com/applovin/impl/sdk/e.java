package com.applovin.impl.sdk;

import android.os.Bundle;
import com.applovin.communicator.AppLovinCommunicator;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorPublisher;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.communicator.AppLovinSdkTopic;
import com.applovin.impl.communicator.CommunicatorMessageImpl;
import com.applovin.impl.sdk.b.a;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.utils.BundleUtils;
import com.applovin.impl.sdk.utils.i;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.places.model.PlaceFields;
import com.ironsource.sdk.precache.DownloadManager;
import com.tapjoy.TapjoyConstants;
import java.util.Map;
import org.json.JSONObject;

public class e implements AppLovinCommunicatorPublisher, AppLovinCommunicatorSubscriber {

    /* renamed from: a reason: collision with root package name */
    private final i f1388a;
    private final AppLovinCommunicator b;

    e(i iVar) {
        this.f1388a = iVar;
        this.b = AppLovinCommunicator.getInstance(iVar.D());
        if (!"HSrCHRtOan6wp2kwOIGJC1RDtuSrF2mWVbio2aBcMHX9KF3iTJ1lLSzCKP1ZSo5yNolPNw1kCTtWpxELFF4ah1".equalsIgnoreCase(iVar.t())) {
            this.b.a(iVar);
            this.b.subscribe((AppLovinCommunicatorSubscriber) this, AppLovinSdkTopic.ALL_TOPICS);
        }
    }

    private void a(Bundle bundle, String str) {
        if (!"log".equals(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Sending message ");
            sb.append(bundle);
            sb.append(" for topic: ");
            sb.append(str);
            sb.append("...");
            this.f1388a.v().b("CommunicatorService", sb.toString());
        }
        this.b.getMessagingService().publish(CommunicatorMessageImpl.create(bundle, str, this, this.f1388a.b((c) a.f1318a).contains(str)));
    }

    public void a(com.applovin.impl.mediation.b.a aVar, String str) {
        boolean j = aVar instanceof com.applovin.impl.mediation.b.c ? ((com.applovin.impl.mediation.b.c) aVar).j() : false;
        Bundle bundle = new Bundle();
        bundle.putString("type", str);
        bundle.putString("id", aVar.b());
        bundle.putString("network_name", aVar.z());
        bundle.putString("max_ad_unit_id", aVar.getAdUnitId());
        bundle.putString("third_party_ad_placement_id", aVar.e());
        bundle.putString("ad_format", aVar.getFormat().getLabel());
        bundle.putString("is_fallback_ad", String.valueOf(j));
        a(bundle, "max_ad_events");
    }

    public void a(JSONObject jSONObject, boolean z) {
        Bundle c = i.c(i.b(i.b(jSONObject, "communicator_settings", new JSONObject(), this.f1388a), "safedk_settings", new JSONObject(), this.f1388a));
        Bundle bundle = new Bundle();
        bundle.putString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1388a.t());
        bundle.putString("applovin_random_token", this.f1388a.k());
        bundle.putString(TapjoyConstants.TJC_DEVICE_TYPE_NAME, AppLovinSdkUtils.isTablet(this.f1388a.D()) ? "tablet" : PlaceFields.PHONE);
        bundle.putString("init_success", String.valueOf(z));
        bundle.putBundle(DownloadManager.SETTINGS, c);
        bundle.putBoolean("debug_mode", ((Boolean) this.f1388a.a(c.eJ)).booleanValue());
        a(bundle, "safedk_init");
    }

    public String getCommunicatorId() {
        return "applovin_sdk";
    }

    public void onMessageReceived(AppLovinCommunicatorMessage appLovinCommunicatorMessage) {
        if (AppLovinSdkTopic.HTTP_REQUEST.equalsIgnoreCase(appLovinCommunicatorMessage.getTopic())) {
            Bundle messageData = appLovinCommunicatorMessage.getMessageData();
            Map a2 = i.a(messageData.getBundle("query_params"));
            Map map = BundleUtils.toMap(messageData.getBundle("post_body"));
            Map a3 = i.a(messageData.getBundle("headers"));
            if (!map.containsKey(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY)) {
                map.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1388a.t());
            }
            this.f1388a.N().a(new f.a().a(messageData.getString("url")).b(messageData.getString("backup_url")).a(a2).c(map).b(a3).a(((Boolean) this.f1388a.a(c.eJ)).booleanValue()).a());
        }
    }
}
