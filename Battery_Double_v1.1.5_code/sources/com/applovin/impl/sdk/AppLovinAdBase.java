package com.applovin.impl.sdk;

import android.text.TextUtils;
import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.util.Arrays;
import org.json.JSONObject;

public abstract class AppLovinAdBase implements j, AppLovinAd {

    /* renamed from: a reason: collision with root package name */
    private final int f1265a;
    protected final JSONObject adObject;
    protected final Object adObjectLock;
    private d b;
    private final long c;
    private g d;
    protected final JSONObject fullResponse;
    protected final Object fullResponseLock;
    protected final i sdk;
    protected final b source;

    protected AppLovinAdBase(JSONObject jSONObject, JSONObject jSONObject2, b bVar, i iVar) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No ad object specified");
        } else if (jSONObject2 == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (iVar != null) {
            this.adObject = jSONObject;
            this.fullResponse = jSONObject2;
            this.source = bVar;
            this.sdk = iVar;
            this.adObjectLock = new Object();
            this.fullResponseLock = new Object();
            this.c = System.currentTimeMillis();
            char[] charArray = jSONObject.toString().toCharArray();
            Arrays.sort(charArray);
            this.f1265a = new String(charArray).hashCode();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    /* access modifiers changed from: protected */
    public boolean containsKeyForAdObject(String str) {
        boolean has;
        synchronized (this.adObjectLock) {
            has = this.adObject.has(str);
        }
        return has;
    }

    public boolean equals(Object obj) {
        if (obj instanceof g) {
            Object b2 = ((g) obj).b();
            if (b2 != null) {
                obj = b2;
            }
        }
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) obj;
        if (this.b == null ? appLovinAdBase.b != null : !this.b.equals(appLovinAdBase.b)) {
            return false;
        }
        if (this.source != appLovinAdBase.source) {
            return false;
        }
        if (this.f1265a != appLovinAdBase.f1265a) {
            z = false;
        }
        return z;
    }

    public long getAdIdNumber() {
        return getLongFromAdObject("ad_id", -1);
    }

    public String getAdValue(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jsonObjectFromAdObject = getJsonObjectFromAdObject("ad_values", null);
        if (jsonObjectFromAdObject == null || jsonObjectFromAdObject.length() <= 0) {
            return null;
        }
        return i.b(jsonObjectFromAdObject, str, (String) null, this.sdk);
    }

    public d getAdZone() {
        if (this.b != null) {
            return this.b;
        }
        this.b = d.a(getSize(), getType(), getStringFromFullResponse("zone_id", null), this.sdk);
        return this.b;
    }

    /* access modifiers changed from: protected */
    public boolean getBooleanFromAdObject(String str, Boolean bool) {
        boolean booleanValue;
        synchronized (this.adObjectLock) {
            booleanValue = i.a(this.adObject, str, bool, this.sdk).booleanValue();
        }
        return booleanValue;
    }

    /* access modifiers changed from: protected */
    public boolean getBooleanFromFullResponse(String str, boolean z) {
        boolean booleanValue;
        synchronized (this.fullResponseLock) {
            booleanValue = i.a(this.fullResponse, str, Boolean.valueOf(z), this.sdk).booleanValue();
        }
        return booleanValue;
    }

    public String getClCode() {
        String stringFromAdObject = getStringFromAdObject("clcode", "");
        return m.b(stringFromAdObject) ? stringFromAdObject : getStringFromFullResponse("clcode", "");
    }

    public long getCreatedAtMillis() {
        return this.c;
    }

    public g getDummyAd() {
        return this.d;
    }

    public long getFetchLatencyMillis() {
        return getLongFromFullResponse("ad_fetch_latency_millis", -1);
    }

    public long getFetchResponseSize() {
        return getLongFromFullResponse("ad_fetch_response_size", -1);
    }

    /* access modifiers changed from: protected */
    public float getFloatFromAdObject(String str, float f) {
        float a2;
        synchronized (this.adObjectLock) {
            a2 = i.a(this.adObject, str, f, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public float getFloatFromFullResponse(String str, float f) {
        float a2;
        synchronized (this.fullResponseLock) {
            a2 = i.a(this.fullResponse, str, f, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public int getIntFromAdObject(String str, int i) {
        int b2;
        synchronized (this.adObjectLock) {
            b2 = i.b(this.adObject, str, i, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public int getIntFromFullResponse(String str, int i) {
        int b2;
        synchronized (this.fullResponseLock) {
            b2 = i.b(this.fullResponse, str, i, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject getJsonObjectFromAdObject(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.adObjectLock) {
            b2 = i.b(this.adObject, str, jSONObject, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject getJsonObjectFromFullResponse(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.fullResponseLock) {
            b2 = i.b(this.fullResponse, str, jSONObject, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public long getLongFromAdObject(String str, long j) {
        long a2;
        synchronized (this.adObjectLock) {
            a2 = i.a(this.adObject, str, j, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public long getLongFromFullResponse(String str, long j) {
        long a2;
        synchronized (this.fullResponseLock) {
            a2 = i.a(this.fullResponse, str, j, this.sdk);
        }
        return a2;
    }

    public String getPrimaryKey() {
        return getStringFromAdObject("pk", "NA");
    }

    public i getSdk() {
        return this.sdk;
    }

    public String getSecondaryKey1() {
        return getStringFromAdObject("sk1", null);
    }

    public String getSecondaryKey2() {
        return getStringFromAdObject("sk2", null);
    }

    public AppLovinAdSize getSize() {
        return AppLovinAdSize.fromString(getStringFromFullResponse("ad_size", null));
    }

    public b getSource() {
        return this.source;
    }

    /* access modifiers changed from: protected */
    public String getStringFromAdObject(String str, String str2) {
        String b2;
        synchronized (this.adObjectLock) {
            b2 = i.b(this.adObject, str, str2, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public String getStringFromFullResponse(String str, String str2) {
        String b2;
        synchronized (this.fullResponseLock) {
            b2 = i.b(this.fullResponse, str, str2, this.sdk);
        }
        return b2;
    }

    public AppLovinAdType getType() {
        return AppLovinAdType.fromString(getStringFromFullResponse("ad_type", null));
    }

    public String getZoneId() {
        if (getAdZone().j()) {
            return null;
        }
        return getStringFromFullResponse("zone_id", null);
    }

    public boolean hasShown() {
        return getBooleanFromAdObject("shown", Boolean.valueOf(false));
    }

    public boolean hasVideoUrl() {
        this.sdk.v().e("AppLovinAdBase", "Attempting to invoke hasVideoUrl() from base ad class");
        return false;
    }

    public int hashCode() {
        return this.f1265a;
    }

    public boolean isVideoAd() {
        return this.adObject.has("is_video_ad") ? getBooleanFromAdObject("is_video_ad", Boolean.valueOf(false)) : hasVideoUrl();
    }

    public void setDummyAd(g gVar) {
        this.d = gVar;
    }

    public void setHasShown(boolean z) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("shown", z);
            }
        } catch (Throwable unused) {
        }
    }

    public boolean shouldCancelHtmlCachingIfShown() {
        return getBooleanFromAdObject("chcis", Boolean.valueOf(false));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinAd{adIdNumber");
        sb.append(getAdIdNumber());
        sb.append(", source=");
        sb.append(getSource());
        sb.append(", zoneId='");
        sb.append(getZoneId());
        sb.append("'");
        sb.append('}');
        return sb.toString();
    }
}
