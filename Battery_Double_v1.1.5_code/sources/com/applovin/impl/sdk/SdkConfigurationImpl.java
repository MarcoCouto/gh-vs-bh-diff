package com.applovin.impl.sdk;

import com.applovin.impl.sdk.b.c;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkConfiguration.ConsentDialogState;

public class SdkConfigurationImpl implements AppLovinSdkConfiguration {

    /* renamed from: a reason: collision with root package name */
    private final i f1285a;

    public SdkConfigurationImpl(i iVar) {
        this.f1285a = iVar;
    }

    public ConsentDialogState getConsentDialogState() {
        String str = (String) this.f1285a.a(c.eV);
        return "applies".equalsIgnoreCase(str) ? ConsentDialogState.APPLIES : "does_not_apply".equalsIgnoreCase(str) ? ConsentDialogState.DOES_NOT_APPLY : ConsentDialogState.UNKNOWN;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinSdkConfiguration{consentDialogState=");
        sb.append(getConsentDialogState());
        sb.append('}');
        return sb.toString();
    }
}
