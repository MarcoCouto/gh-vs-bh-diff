package com.applovin.impl.sdk.c;

import android.text.TextUtils;
import java.util.HashSet;
import java.util.Set;

public class g {

    /* renamed from: a reason: collision with root package name */
    public static final g f1335a = a("ad_req");
    public static final g b = a("ad_imp");
    public static final g c = a("ad_session_start");
    public static final g d = a("ad_imp_session");
    public static final g e = a("cached_files_expired");
    public static final g f = a("cache_drop_count");
    public static final g g = a("sdk_reset_state_count", true);
    public static final g h = a("ad_response_process_failures", true);
    public static final g i = a("response_process_failures", true);
    public static final g j = a("incent_failed_to_display_count", true);
    public static final g k = a("app_paused_and_resumed");
    public static final g l = a("ad_rendered_with_mismatched_sdk_key", true);
    public static final g m = a("med_ad_req");
    public static final g n = a("med_ad_response_process_failures", true);
    public static final g o = a("med_adapters_failed_init_missing_activity", true);
    public static final g p = a("med_waterfall_ad_no_fill", true);
    public static final g q = a("med_waterfall_ad_adapter_load_failed", true);
    public static final g r = a("med_waterfall_ad_invalid_response", true);
    private static final Set<String> s = new HashSet(32);
    private static final Set<g> u = new HashSet(16);
    private final String t;

    static {
        a("fullscreen_ad_nil_vc_count");
        a("applovin_bundle_missing");
    }

    private g(String str) {
        this.t = str;
    }

    private static g a(String str) {
        return a(str, false);
    }

    private static g a(String str, boolean z) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No key name specified");
        } else if (!s.contains(str)) {
            s.add(str);
            g gVar = new g(str);
            if (z) {
                u.add(gVar);
            }
            return gVar;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Key has already been used: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public static Set<g> b() {
        return u;
    }

    public String a() {
        return this.t;
    }
}
