package com.applovin.impl.sdk.c;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a reason: collision with root package name */
    private final List<a> f1333a;
    private final Object b = new Object();
    private final i c;
    private final o d;

    private static class a {

        /* renamed from: a reason: collision with root package name */
        private final Long f1334a;
        private final String b;
        private final String c;
        private final String d;

        private a(String str, Throwable th) {
            this.b = str;
            this.f1334a = Long.valueOf(System.currentTimeMillis());
            String str2 = null;
            this.c = th != null ? th.getClass().getName() : null;
            if (th != null) {
                str2 = th.getMessage();
            }
            this.d = str2;
        }

        private a(JSONObject jSONObject) throws JSONException {
            this.b = jSONObject.getString("ms");
            this.f1334a = Long.valueOf(jSONObject.getLong(CampaignEx.JSON_KEY_ST_TS));
            JSONObject optJSONObject = jSONObject.optJSONObject("ex");
            String str = null;
            this.c = optJSONObject != null ? optJSONObject.getString("nm") : null;
            if (optJSONObject != null) {
                str = optJSONObject.getString("rn");
            }
            this.d = str;
        }

        /* access modifiers changed from: private */
        public JSONObject a() throws JSONException {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("ms", this.b);
            jSONObject.put(CampaignEx.JSON_KEY_ST_TS, this.f1334a);
            if (!TextUtils.isEmpty(this.c)) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("nm", this.c);
                if (!TextUtils.isEmpty(this.d)) {
                    jSONObject2.put("rn", this.d);
                }
                jSONObject.put("ex", jSONObject2);
            }
            return jSONObject;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ErrorLog{timestampMillis=");
            sb.append(this.f1334a);
            sb.append(",message='");
            sb.append(this.b);
            sb.append('\'');
            sb.append(",throwableName='");
            sb.append(this.c);
            sb.append('\'');
            sb.append(",throwableReason='");
            sb.append(this.d);
            sb.append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    public f(i iVar) {
        this.c = iVar;
        this.d = iVar.v();
        this.f1333a = new ArrayList();
    }

    private void d() {
        JSONArray jSONArray = new JSONArray();
        synchronized (this.b) {
            for (a aVar : this.f1333a) {
                try {
                    jSONArray.put(aVar.a());
                } catch (JSONException e) {
                    this.d.a("ErrorManager", Boolean.valueOf(false), "Failed to convert error log into json.", e);
                    this.f1333a.remove(aVar);
                }
            }
        }
        this.c.a(e.m, jSONArray.toString());
    }

    public JSONArray a() {
        JSONArray jSONArray;
        synchronized (this.b) {
            jSONArray = new JSONArray();
            for (a a2 : this.f1333a) {
                try {
                    jSONArray.put(a2.a());
                } catch (JSONException e) {
                    this.d.a("ErrorManager", Boolean.valueOf(false), "Failed to convert error log into json.", e);
                }
            }
        }
        return jSONArray;
    }

    public void a(String str, Throwable th) {
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.b) {
                if (this.f1333a.size() < ((Integer) this.c.a(c.ex)).intValue()) {
                    this.f1333a.add(new a(str, th));
                    d();
                }
            }
        }
    }

    public void b() {
        String str = (String) this.c.b(e.m, null);
        if (str != null) {
            synchronized (this.b) {
                try {
                    this.f1333a.clear();
                    JSONArray jSONArray = new JSONArray(str);
                    for (int i = 0; i < jSONArray.length(); i++) {
                        try {
                            this.f1333a.add(new a(jSONArray.getJSONObject(i)));
                        } catch (JSONException e) {
                            this.d.a("ErrorManager", Boolean.valueOf(false), "Failed to convert error json into a log.", e);
                        }
                    }
                } catch (JSONException e2) {
                    this.d.b("ErrorManager", "Unable to convert String to json.", e2);
                }
            }
        }
    }

    public void c() {
        synchronized (this.b) {
            this.f1333a.clear();
            this.c.b(e.m);
        }
    }
}
