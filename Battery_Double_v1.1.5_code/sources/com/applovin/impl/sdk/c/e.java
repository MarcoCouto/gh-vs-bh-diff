package com.applovin.impl.sdk.c;

public final class e {

    /* renamed from: a reason: collision with root package name */
    private long f1332a;
    private long b;
    private boolean c;
    private long d;
    private long e;

    public void a() {
        this.c = true;
    }

    public void a(long j) {
        this.f1332a += j;
    }

    public void b(long j) {
        this.b += j;
    }

    public boolean b() {
        return this.c;
    }

    public long c() {
        return this.f1332a;
    }

    public long d() {
        return this.b;
    }

    public void e() {
        this.d++;
    }

    public void f() {
        this.e++;
    }

    public long g() {
        return this.d;
    }

    public long h() {
        return this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CacheStatsTracker{totalDownloadedBytes=");
        sb.append(this.f1332a);
        sb.append(", totalCachedBytes=");
        sb.append(this.b);
        sb.append(", isHTMLCachingCancelled=");
        sb.append(this.c);
        sb.append(", htmlResourceCacheSuccessCount=");
        sb.append(this.d);
        sb.append(", htmlResourceCacheFailureCount=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
