package com.applovin.impl.sdk.c;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.github.mikephil.charting.utils.Utils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class j {

    /* renamed from: a reason: collision with root package name */
    private final i f1338a;
    private final o b;
    private final Object c = new Object();
    private final Map<String, b> d = new HashMap();

    private static class a {

        /* renamed from: a reason: collision with root package name */
        static final String f1339a = a("tk");
        static final String b = a("tc");
        static final String c = a("ec");
        static final String d = a("dm");
        static final String e = a("dv");
        static final String f = a("dh");
        static final String g = a("dl");
        private static final Set<String> h = new HashSet(7);

        private static String a(String str) {
            if (TextUtils.isEmpty(str)) {
                throw new IllegalArgumentException("No key name specified");
            } else if (!h.contains(str)) {
                h.add(str);
                return str;
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("Key has already been used: ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
        }
    }

    private static class b {

        /* renamed from: a reason: collision with root package name */
        private final String f1340a;
        private int b = 0;
        private int c = 0;
        private double d = Utils.DOUBLE_EPSILON;
        private double e = Utils.DOUBLE_EPSILON;
        private Long f = null;
        private Long g = null;

        b(String str) {
            this.f1340a = str;
        }

        b(JSONObject jSONObject) throws JSONException {
            this.f1340a = jSONObject.getString(a.f1339a);
            this.b = jSONObject.getInt(a.b);
            this.c = jSONObject.getInt(a.c);
            this.d = jSONObject.getDouble(a.d);
            this.e = jSONObject.getDouble(a.e);
            this.f = Long.valueOf(jSONObject.optLong(a.f));
            this.g = Long.valueOf(jSONObject.optLong(a.g));
        }

        /* access modifiers changed from: 0000 */
        public String a() {
            return this.f1340a;
        }

        /* access modifiers changed from: 0000 */
        public void a(long j) {
            int i = this.b;
            double d2 = this.d;
            double d3 = this.e;
            this.b++;
            double d4 = (double) i;
            Double.isNaN(d4);
            double d5 = d2 * d4;
            double d6 = (double) j;
            Double.isNaN(d6);
            double d7 = d5 + d6;
            double d8 = (double) this.b;
            Double.isNaN(d8);
            this.d = d7 / d8;
            double d9 = (double) this.b;
            Double.isNaN(d4);
            Double.isNaN(d9);
            double d10 = d4 / d9;
            Double.isNaN(d6);
            double pow = Math.pow(d2 - d6, 2.0d);
            double d11 = (double) this.b;
            Double.isNaN(d11);
            this.e = d10 * (d3 + (pow / d11));
            if (this.f == null || j > this.f.longValue()) {
                this.f = Long.valueOf(j);
            }
            if (this.g == null || j < this.g.longValue()) {
                this.g = Long.valueOf(j);
            }
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.c++;
        }

        /* access modifiers changed from: 0000 */
        public JSONObject c() throws JSONException {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(a.f1339a, this.f1340a);
            jSONObject.put(a.b, this.b);
            jSONObject.put(a.c, this.c);
            jSONObject.put(a.d, this.d);
            jSONObject.put(a.e, this.e);
            jSONObject.put(a.f, this.f);
            jSONObject.put(a.g, this.g);
            return jSONObject;
        }

        public String toString() {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("TaskStats{n='");
                sb.append(this.f1340a);
                sb.append('\'');
                sb.append(", stats=");
                sb.append(c().toString());
                sb.append('}');
                return sb.toString();
            } catch (JSONException unused) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("TaskStats{n='");
                sb2.append(this.f1340a);
                sb2.append('\'');
                sb2.append(", count=");
                sb2.append(this.b);
                sb2.append('}');
                return sb2.toString();
            }
        }
    }

    public j(i iVar) {
        this.f1338a = iVar;
        this.b = iVar.v();
        c();
    }

    private b b(i iVar) {
        b bVar;
        synchronized (this.c) {
            String a2 = iVar.a();
            bVar = (b) this.d.get(a2);
            if (bVar == null) {
                bVar = new b(a2);
                this.d.put(a2, bVar);
            }
        }
        return bVar;
    }

    private void c() {
        Set<String> set = (Set) this.f1338a.a(e.n);
        if (set != null) {
            synchronized (this.c) {
                try {
                    for (String jSONObject : set) {
                        b bVar = new b(new JSONObject(jSONObject));
                        this.d.put(bVar.a(), bVar);
                    }
                } catch (JSONException e) {
                    this.b.b("TaskStatsManager", "Failed to convert stats json.", e);
                }
            }
        }
    }

    private void d() {
        HashSet hashSet;
        synchronized (this.c) {
            hashSet = new HashSet(this.d.size());
            for (b bVar : this.d.values()) {
                try {
                    hashSet.add(bVar.c().toString());
                } catch (JSONException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to serialize ");
                    sb.append(bVar);
                    this.b.b("TaskStatsManager", sb.toString(), e);
                }
            }
        }
        this.f1338a.a(e.n, hashSet);
    }

    public JSONArray a() {
        JSONArray jSONArray;
        synchronized (this.c) {
            jSONArray = new JSONArray();
            for (b bVar : this.d.values()) {
                try {
                    jSONArray.put(bVar.c());
                } catch (JSONException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to serialize ");
                    sb.append(bVar);
                    this.b.b("TaskStatsManager", sb.toString(), e);
                }
            }
        }
        return jSONArray;
    }

    public void a(i iVar) {
        a(iVar, false, 0);
    }

    public void a(i iVar, long j) {
        if (iVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f1338a.a(c.ev)).booleanValue()) {
            synchronized (this.c) {
                b(iVar).a(j);
                d();
            }
        }
    }

    public void a(i iVar, boolean z, long j) {
        if (iVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f1338a.a(c.ev)).booleanValue()) {
            synchronized (this.c) {
                b b2 = b(iVar);
                b2.b();
                if (z) {
                    b2.a(j);
                }
                d();
            }
        }
    }

    public void b() {
        synchronized (this.c) {
            this.d.clear();
            this.f1338a.b(e.n);
        }
    }
}
