package com.applovin.impl.sdk.c;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.d.x;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import com.mintegral.msdk.base.entity.CampaignEx;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final i f1326a;
    /* access modifiers changed from: private */
    public final o b;
    private final Object c = new Object();
    private final C0011c d = new C0011c();

    private static class a {

        /* renamed from: a reason: collision with root package name */
        private final i f1328a;
        private final JSONObject b;

        private a(String str, String str2, String str3, i iVar) {
            this.b = new JSONObject();
            this.f1328a = iVar;
            com.applovin.impl.sdk.utils.i.a(this.b, "pk", str, iVar);
            com.applovin.impl.sdk.utils.i.b(this.b, CampaignEx.JSON_KEY_ST_TS, System.currentTimeMillis(), iVar);
            if (m.b(str2)) {
                com.applovin.impl.sdk.utils.i.a(this.b, "sk1", str2, iVar);
            }
            if (m.b(str3)) {
                com.applovin.impl.sdk.utils.i.a(this.b, "sk2", str3, iVar);
            }
        }

        /* access modifiers changed from: private */
        public String a() throws OutOfMemoryError {
            return this.b.toString();
        }

        /* access modifiers changed from: 0000 */
        public void a(String str, long j) {
            b(str, com.applovin.impl.sdk.utils.i.a(this.b, str, 0, this.f1328a) + j);
        }

        /* access modifiers changed from: 0000 */
        public void a(String str, String str2) {
            JSONArray b2 = com.applovin.impl.sdk.utils.i.b(this.b, str, new JSONArray(), this.f1328a);
            b2.put(str2);
            com.applovin.impl.sdk.utils.i.a(this.b, str, b2, this.f1328a);
        }

        /* access modifiers changed from: 0000 */
        public void b(String str, long j) {
            com.applovin.impl.sdk.utils.i.b(this.b, str, j, this.f1328a);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("AdEventStats{stats='");
            sb.append(this.b);
            sb.append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    public class b {
        private final AppLovinAdBase b;
        private final c c;

        public b(AppLovinAdBase appLovinAdBase, c cVar) {
            this.b = appLovinAdBase;
            this.c = cVar;
        }

        public b a(b bVar) {
            this.c.a(bVar, 1, this.b);
            return this;
        }

        public b a(b bVar, long j) {
            this.c.b(bVar, j, this.b);
            return this;
        }

        public b a(b bVar, String str) {
            this.c.a(bVar, str, this.b);
            return this;
        }

        public void a() {
            this.c.e();
        }
    }

    /* renamed from: com.applovin.impl.sdk.c.c$c reason: collision with other inner class name */
    private class C0011c extends LinkedHashMap<String, a> {
        private C0011c() {
        }

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Entry<String, a> entry) {
            return size() > ((Integer) c.this.f1326a.a(com.applovin.impl.sdk.b.c.et)).intValue();
        }
    }

    public c(i iVar) {
        if (iVar != null) {
            this.f1326a = iVar;
            this.b = iVar.v();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    /* access modifiers changed from: private */
    public void a(b bVar, long j, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f1326a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            synchronized (this.c) {
                b(appLovinAdBase).a(((Boolean) this.f1326a.a(com.applovin.impl.sdk.b.c.eu)).booleanValue() ? bVar.b() : bVar.a(), j);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar, String str, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f1326a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            synchronized (this.d) {
                b(appLovinAdBase).a(((Boolean) this.f1326a.a(com.applovin.impl.sdk.b.c.eu)).booleanValue() ? bVar.b() : bVar.a(), str);
            }
        }
    }

    private void a(JSONObject jSONObject) {
        AnonymousClass1 r0 = new x<Object>(com.applovin.impl.sdk.network.b.a(this.f1326a).a(c()).c(d()).a(h.e(this.f1326a)).b(HttpRequest.METHOD_POST).a(jSONObject).b(((Integer) this.f1326a.a(com.applovin.impl.sdk.b.c.er)).intValue()).a(((Integer) this.f1326a.a(com.applovin.impl.sdk.b.c.es)).intValue()).a(), this.f1326a) {
            public void a(int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to submitted ad stats: ");
                sb.append(i);
                c.this.b.e("AdEventStatsManager", sb.toString());
            }

            public void a(Object obj, int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Ad stats submitted: ");
                sb.append(i);
                c.this.b.b("AdEventStatsManager", sb.toString());
            }
        };
        r0.a(com.applovin.impl.sdk.b.c.aI);
        r0.b(com.applovin.impl.sdk.b.c.aJ);
        this.f1326a.K().a((com.applovin.impl.sdk.d.a) r0, com.applovin.impl.sdk.d.r.a.BACKGROUND);
    }

    private a b(AppLovinAdBase appLovinAdBase) {
        a aVar;
        synchronized (this.c) {
            String primaryKey = appLovinAdBase.getPrimaryKey();
            aVar = (a) this.d.get(primaryKey);
            if (aVar == null) {
                a aVar2 = new a(primaryKey, appLovinAdBase.getSecondaryKey1(), appLovinAdBase.getSecondaryKey2(), this.f1326a);
                this.d.put(primaryKey, aVar2);
                aVar = aVar2;
            }
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    public void b(b bVar, long j, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f1326a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            synchronized (this.c) {
                b(appLovinAdBase).b(((Boolean) this.f1326a.a(com.applovin.impl.sdk.b.c.eu)).booleanValue() ? bVar.b() : bVar.a(), j);
            }
        }
    }

    private String c() {
        return h.a("2.0/s", this.f1326a);
    }

    private String d() {
        return h.b("2.0/s", this.f1326a);
    }

    /* access modifiers changed from: private */
    public void e() {
        HashSet hashSet;
        if (((Boolean) this.f1326a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            synchronized (this.c) {
                hashSet = new HashSet(this.d.size());
                for (a aVar : this.d.values()) {
                    try {
                        hashSet.add(aVar.a());
                    } catch (OutOfMemoryError e) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to serialize ");
                        sb.append(aVar);
                        sb.append(" due to OOM error");
                        this.b.b("AdEventStatsManager", sb.toString(), e);
                        b();
                    }
                }
            }
            this.f1326a.a(e.s, hashSet);
        }
    }

    public b a(AppLovinAdBase appLovinAdBase) {
        return new b(appLovinAdBase, this);
    }

    public void a() {
        if (((Boolean) this.f1326a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            Set<String> set = (Set) this.f1326a.b(e.s, new HashSet(0));
            this.f1326a.b(e.s);
            if (set == null || set.isEmpty()) {
                this.b.b("AdEventStatsManager", "No serialized ad events found");
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("De-serializing ");
            sb.append(set.size());
            sb.append(" stat ad events");
            this.b.b("AdEventStatsManager", sb.toString());
            JSONArray jSONArray = new JSONArray();
            for (String str : set) {
                try {
                    jSONArray.put(new JSONObject(str));
                } catch (JSONException e) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to parse: ");
                    sb2.append(str);
                    this.b.b("AdEventStatsManager", sb2.toString(), e);
                }
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("stats", jSONArray);
                a(jSONObject);
            } catch (JSONException e2) {
                this.b.b("AdEventStatsManager", "Failed to create stats to submit", e2);
            }
        }
    }

    public void b() {
        synchronized (this.c) {
            this.b.b("AdEventStatsManager", "Clearing ad stats...");
            this.d.clear();
        }
    }
}
