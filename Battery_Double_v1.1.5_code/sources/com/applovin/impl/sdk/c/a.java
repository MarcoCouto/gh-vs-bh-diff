package com.applovin.impl.sdk.c;

public class a {

    /* renamed from: a reason: collision with root package name */
    private final String f1324a;
    private final String b;

    public a(String str, String str2) {
        this.f1324a = str;
        this.b = str2;
    }

    public String a() {
        return this.f1324a;
    }

    public String b() {
        return this.b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdEventPostback{url='");
        sb.append(this.f1324a);
        sb.append('\'');
        sb.append(", backupUrl='");
        sb.append(this.b);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
