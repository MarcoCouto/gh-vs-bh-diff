package com.applovin.impl.sdk;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.SensorManager;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.applovin.impl.sdk.d.i;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.l;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.places.model.PlaceFields;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class j {
    private static String e;
    private static String f;
    private static int g;

    /* renamed from: a reason: collision with root package name */
    private final i f1404a;
    /* access modifiers changed from: private */
    public final o b;
    /* access modifiers changed from: private */
    public final Context c;
    private final Map<Class, Object> d;
    /* access modifiers changed from: private */
    public final AtomicReference<a> h = new AtomicReference<>();

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public boolean f1407a;
        public String b = "";
    }

    public static class b {

        /* renamed from: a reason: collision with root package name */
        public String f1408a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public long g;
    }

    public static class c {

        /* renamed from: a reason: collision with root package name */
        public int f1409a = -1;
        public int b = -1;
    }

    public static class d {
        public boolean A;
        public boolean B;
        public int C = -1;
        public String D;
        public long E;
        public e F = new e();
        public Boolean G;
        public Boolean H;
        public boolean I;

        /* renamed from: a reason: collision with root package name */
        public String f1410a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public int h;
        public String i;
        public String j;
        public Locale k;
        public String l;
        public float m;
        public int n;
        public float o;
        public float p;
        public double q;
        public double r;
        public int s;
        public boolean t;
        public c u;
        public int v;
        public String w;
        public boolean x;
        public boolean y;
        public boolean z;
    }

    public static class e {

        /* renamed from: a reason: collision with root package name */
        public long f1411a = -1;
        public long b = -1;
        public long c = -1;
        public boolean d = false;
    }

    protected j(i iVar) {
        if (iVar != null) {
            this.f1404a = iVar;
            this.b = iVar.v();
            this.c = iVar.D();
            this.d = Collections.synchronizedMap(new HashMap());
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private d a(d dVar) {
        if (dVar == null) {
            dVar = new d();
        }
        dVar.G = f.a(this.c);
        dVar.H = f.b(this.c);
        dVar.u = ((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dZ)).booleanValue() ? j() : null;
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.ek)).booleanValue()) {
            dVar.t = n();
        }
        try {
            AudioManager audioManager = (AudioManager) this.c.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            if (audioManager != null) {
                dVar.v = (int) (((float) audioManager.getStreamVolume(3)) * ((Float) this.f1404a.a(com.applovin.impl.sdk.b.c.el)).floatValue());
            }
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect volume", th);
        }
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.em)).booleanValue()) {
            if (e == null) {
                String r = r();
                if (!m.b(r)) {
                    r = "";
                }
                e = r;
            }
            if (m.b(e)) {
                dVar.w = e;
            }
        }
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.ed)).booleanValue()) {
            try {
                dVar.E = Environment.getDataDirectory().getFreeSpace();
            } catch (Throwable th2) {
                dVar.E = -1;
                this.b.b("DataCollector", "Unable to collect free space.", th2);
            }
        }
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.ee)).booleanValue()) {
            try {
                ActivityManager activityManager = (ActivityManager) this.c.getSystemService("activity");
                MemoryInfo memoryInfo = new MemoryInfo();
                activityManager.getMemoryInfo(memoryInfo);
                dVar.F.b = memoryInfo.availMem;
                dVar.F.d = memoryInfo.lowMemory;
                dVar.F.c = memoryInfo.threshold;
                dVar.F.f1411a = memoryInfo.totalMem;
            } catch (Throwable th3) {
                this.b.b("DataCollector", "Unable to collect memory info.", th3);
            }
        }
        String str = (String) this.f1404a.C().a(com.applovin.impl.sdk.b.c.eo);
        if (!str.equalsIgnoreCase(f)) {
            try {
                f = str;
                PackageInfo packageInfo = this.c.getPackageManager().getPackageInfo(str, 0);
                dVar.s = packageInfo.versionCode;
                g = packageInfo.versionCode;
            } catch (Throwable unused) {
                g = 0;
            }
        } else {
            dVar.s = g;
        }
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.ea)).booleanValue()) {
            dVar.z = AppLovinSdkUtils.isTablet(this.c);
        }
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.eb)).booleanValue()) {
            dVar.A = m();
        }
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.ec)).booleanValue()) {
            String k = k();
            if (!TextUtils.isEmpty(k)) {
                dVar.D = k;
            }
        }
        dVar.l = g();
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.ef)).booleanValue()) {
            dVar.B = p.d();
        }
        if (g.f()) {
            try {
                dVar.C = ((PowerManager) this.c.getSystemService("power")).isPowerSaveMode() ? 1 : 0;
            } catch (Throwable th4) {
                this.b.b("DataCollector", "Unable to collect power saving mode", th4);
            }
        }
        return dVar;
    }

    private String a(int i) {
        if (i == 1) {
            return "receiver";
        }
        if (i == 2) {
            return "speaker";
        }
        if (i == 4 || i == 3) {
            return "headphones";
        }
        if (i == 8) {
            return "bluetootha2dpoutput";
        }
        if (i == 13 || i == 19 || i == 5 || i == 6 || i == 12 || i == 11) {
            return "lineout";
        }
        if (i == 9 || i == 10) {
            return "hdmioutput";
        }
        return null;
    }

    private boolean a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("No permission name specified");
        } else if (this.c != null) {
            return k.a(str, this.c.getPackageName(), this.c.getPackageManager()) == 0;
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    private boolean a(String str, com.applovin.impl.sdk.b.c<String> cVar) {
        for (String startsWith : com.applovin.impl.sdk.utils.e.a((String) this.f1404a.a(cVar))) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private String b(String str) {
        int length = str.length();
        int[] iArr = {11, 12, 10, 3, 2, 1, 15, 10, 15, 14};
        int length2 = iArr.length;
        char[] cArr = new char[length];
        for (int i = 0; i < length; i++) {
            cArr[i] = str.charAt(i);
            for (int i2 = length2 - 1; i2 >= 0; i2--) {
                cArr[i] = (char) (cArr[i] ^ iArr[i2]);
            }
        }
        return new String(cArr);
    }

    private Map<String, String> f() {
        return a(null, false, true);
    }

    private String g() {
        String str;
        String str2 = "none";
        try {
            int d2 = p.d(this.c);
            if (d2 == 1) {
                str = "portrait";
            } else if (d2 != 2) {
                return str2;
            } else {
                str = "landscape";
            }
            return str;
        } catch (Throwable th) {
            this.f1404a.v().b("DataCollector", "Encountered error while attempting to collect application orientation", th);
            return str2;
        }
    }

    private a h() {
        if (i()) {
            try {
                a aVar = new a();
                Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.c);
                aVar.f1407a = advertisingIdInfo.isLimitAdTrackingEnabled();
                aVar.b = advertisingIdInfo.getId();
                return aVar;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }", th);
            }
        } else {
            o.i("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }");
            return new a();
        }
    }

    private boolean i() {
        return p.e("com.google.android.gms.ads.identifier.AdvertisingIdClient");
    }

    private c j() {
        try {
            c cVar = new c();
            Intent registerReceiver = this.c.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = -1;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : -1;
            int intExtra2 = registerReceiver != null ? registerReceiver.getIntExtra("scale", -1) : -1;
            if (intExtra <= 0 || intExtra2 <= 0) {
                cVar.b = -1;
            } else {
                cVar.b = (int) ((((float) intExtra) / ((float) intExtra2)) * 100.0f);
            }
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("status", -1);
            }
            cVar.f1409a = i;
            return cVar;
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect battery info", th);
            return null;
        }
    }

    private String k() {
        try {
            AudioManager audioManager = (AudioManager) this.c.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            StringBuilder sb = new StringBuilder();
            if (g.g()) {
                for (AudioDeviceInfo type : audioManager.getDevices(2)) {
                    String a2 = a(type.getType());
                    if (!TextUtils.isEmpty(a2)) {
                        sb.append(a2);
                        sb.append(",");
                    }
                }
            } else {
                if (audioManager.isWiredHeadsetOn()) {
                    sb.append("headphones");
                    sb.append(",");
                }
                if (audioManager.isBluetoothA2dpOn()) {
                    sb.append("bluetootha2dpoutput");
                }
            }
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == ',') {
                sb.deleteCharAt(sb.length() - 1);
            }
            String sb2 = sb.toString();
            if (TextUtils.isEmpty(sb2)) {
                this.b.b("DataCollector", "No sound outputs detected");
            }
            return sb2;
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect sound outputs", th);
            return null;
        }
    }

    private double l() {
        double offset = (double) TimeZone.getDefault().getOffset(new Date().getTime());
        Double.isNaN(offset);
        double round = (double) Math.round((offset * 10.0d) / 3600000.0d);
        Double.isNaN(round);
        return round / 10.0d;
    }

    private boolean m() {
        try {
            PackageManager packageManager = this.c.getPackageManager();
            return g.f() ? packageManager.hasSystemFeature("android.software.leanback") : g.c() ? packageManager.hasSystemFeature("android.hardware.type.television") : !packageManager.hasSystemFeature("android.hardware.touchscreen");
        } catch (Throwable th) {
            this.b.b("DataCollector", "Failed to determine if device is TV.", th);
            return false;
        }
    }

    private boolean n() {
        try {
            return o() || p();
        } catch (Throwable unused) {
            return false;
        }
    }

    private boolean o() {
        String str = Build.TAGS;
        return str != null && str.contains(b("lz}$blpz"));
    }

    private boolean p() {
        for (String b2 : new String[]{"&zpz}ld&hyy&Z|yl{|zl{'hyb", "&zk`g&z|", "&zpz}ld&k`g&z|", "&zpz}ld&qk`g&z|", "&mh}h&efjhe&qk`g&z|", "&mh}h&efjhe&k`g&z|", "&zpz}ld&zm&qk`g&z|", "&zpz}ld&k`g&oh`ezhol&z|", "&mh}h&efjhe&z|"}) {
            if (new File(b(b2)).exists()) {
                return true;
            }
        }
        return false;
    }

    private boolean q() {
        return a(Build.DEVICE, com.applovin.impl.sdk.b.c.eh) || a(Build.HARDWARE, com.applovin.impl.sdk.b.c.eg) || a(Build.MANUFACTURER, com.applovin.impl.sdk.b.c.ei) || a(Build.MODEL, com.applovin.impl.sdk.b.c.ej);
    }

    private String r() {
        final AtomicReference atomicReference = new AtomicReference();
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        new Handler(this.c.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    atomicReference.set(new WebView(j.this.c).getSettings().getUserAgentString());
                } catch (Throwable th) {
                    countDownLatch.countDown();
                    throw th;
                }
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await(((Long) this.f1404a.a(com.applovin.impl.sdk.b.c.en)).longValue(), TimeUnit.MILLISECONDS);
        } catch (Throwable unused) {
        }
        return (String) atomicReference.get();
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        String encodeToString = Base64.encodeToString(new JSONObject(f()).toString().getBytes(Charset.defaultCharset()), 2);
        if (!((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.eK)).booleanValue()) {
            return encodeToString;
        }
        return l.a(encodeToString, this.f1404a.t(), p.a(this.f1404a));
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x03e5  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0405  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0420  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x043b  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0448  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0464  */
    public Map<String, String> a(Map<String, String> map, boolean z, boolean z2) {
        a aVar;
        String str;
        com.applovin.impl.sdk.network.a.b a2;
        HashMap hashMap = new HashMap();
        d b2 = b();
        hashMap.put("brand", m.d(b2.d));
        hashMap.put("brand_name", m.d(b2.e));
        hashMap.put("hardware", m.d(b2.f));
        hashMap.put("api_level", String.valueOf(b2.h));
        hashMap.put("carrier", m.d(b2.j));
        hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, m.d(b2.i));
        hashMap.put("locale", m.d(b2.k.toString()));
        hashMap.put("model", m.d(b2.f1410a));
        hashMap.put("os", m.d(b2.b));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, m.d(b2.c));
        hashMap.put("revision", m.d(b2.g));
        hashMap.put("orientation_lock", b2.l);
        hashMap.put("tz_offset", String.valueOf(b2.r));
        hashMap.put("aida", String.valueOf(b2.I));
        hashMap.put("wvvc", String.valueOf(b2.s));
        hashMap.put("adns", String.valueOf(b2.m));
        hashMap.put("adnsd", String.valueOf(b2.n));
        hashMap.put("xdpi", String.valueOf(b2.o));
        hashMap.put("ydpi", String.valueOf(b2.p));
        hashMap.put("screen_size_in", String.valueOf(b2.q));
        hashMap.put("sim", m.a(b2.x));
        hashMap.put("gy", m.a(b2.y));
        hashMap.put("is_tablet", m.a(b2.z));
        hashMap.put("tv", m.a(b2.A));
        hashMap.put("vs", m.a(b2.B));
        hashMap.put("lpm", String.valueOf(b2.C));
        hashMap.put("fs", String.valueOf(b2.E));
        hashMap.put("fm", String.valueOf(b2.F.b));
        hashMap.put("tm", String.valueOf(b2.F.f1411a));
        hashMap.put("lmt", String.valueOf(b2.F.c));
        hashMap.put("lm", String.valueOf(b2.F.d));
        hashMap.put("adr", m.a(b2.t));
        hashMap.put(AvidVideoPlaybackListenerImpl.VOLUME, String.valueOf(b2.v));
        p.a("ua", m.d(b2.w), (Map<String, String>) hashMap);
        p.a("so", m.d(b2.D), (Map<String, String>) hashMap);
        c cVar = b2.u;
        if (cVar != null) {
            hashMap.put("act", String.valueOf(cVar.f1409a));
            hashMap.put("acm", String.valueOf(cVar.b));
        }
        Boolean bool = b2.G;
        if (bool != null) {
            hashMap.put("huc", bool.toString());
        }
        Boolean bool2 = b2.H;
        if (bool2 != null) {
            hashMap.put("aru", bool2.toString());
        }
        Point a3 = g.a(this.c);
        hashMap.put("dx", Integer.toString(a3.x));
        hashMap.put("dy", Integer.toString(a3.y));
        hashMap.put("accept", "custom_size,launch_app,video");
        hashMap.put("api_did", this.f1404a.a(com.applovin.impl.sdk.b.c.S));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", Integer.toString(131));
        hashMap.put("format", "json");
        b c2 = c();
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, m.d(c2.b));
        hashMap.put("ia", Long.toString(c2.g));
        hashMap.put("tg", String.valueOf(c2.e));
        hashMap.put("installer_name", c2.d);
        hashMap.put("debug", c2.f);
        p.a("mediation_provider", m.d(this.f1404a.n()), (Map<String, String>) hashMap);
        hashMap.put("network", h.f(this.f1404a));
        p.a("plugin_version", (String) this.f1404a.a(com.applovin.impl.sdk.b.c.dY), (Map<String, String>) hashMap);
        hashMap.put("preloading", String.valueOf(z));
        p.a("test_ads", Boolean.valueOf(this.f1404a.l().isTestAdsEnabled()), (Map<String, String>) hashMap);
        hashMap.put("first_install", String.valueOf(this.f1404a.H()));
        hashMap.put("first_install_v2", String.valueOf(!this.f1404a.I()));
        if (!((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.eJ)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1404a.t());
        }
        hashMap.put("sc", this.f1404a.a(com.applovin.impl.sdk.b.c.V));
        hashMap.put("sc2", this.f1404a.a(com.applovin.impl.sdk.b.c.W));
        hashMap.put("server_installed_at", m.d((String) this.f1404a.a(com.applovin.impl.sdk.b.c.X)));
        p.a("persisted_data", m.d((String) this.f1404a.a(com.applovin.impl.sdk.b.e.x)), (Map<String, String>) hashMap);
        hashMap.put("v1", Boolean.toString(g.a("android.permission.WRITE_EXTERNAL_STORAGE", this.c)));
        hashMap.put("v2", "true");
        hashMap.put("v3", "true");
        hashMap.put("v4", "true");
        hashMap.put("v5", "true");
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.ep)).booleanValue()) {
            com.applovin.impl.sdk.c.h L = this.f1404a.L();
            hashMap.put("li", String.valueOf(L.b(com.applovin.impl.sdk.c.g.b)));
            hashMap.put("si", String.valueOf(L.b(com.applovin.impl.sdk.c.g.d)));
            hashMap.put("pf", String.valueOf(L.b(com.applovin.impl.sdk.c.g.h)));
            hashMap.put("mpf", String.valueOf(L.b(com.applovin.impl.sdk.c.g.n)));
            hashMap.put("gpf", String.valueOf(L.b(com.applovin.impl.sdk.c.g.i)));
        }
        hashMap.put("vz", m.e(this.c.getPackageName()));
        hashMap.put("pnr", Boolean.toString(this.f1404a.h()));
        if (z2) {
            aVar = (a) this.h.get();
            if (aVar != null) {
                e();
            } else if (p.b()) {
                aVar = new a();
                hashMap.put("inc", Boolean.toString(true));
            }
            str = aVar.b;
            if (m.b(str)) {
                hashMap.put("idfa", str);
            }
            hashMap.put("dnt", Boolean.toString(aVar.f1407a));
            if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dR)).booleanValue()) {
                p.a("cuid", this.f1404a.i(), (Map<String, String>) hashMap);
            }
            if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dU)).booleanValue()) {
                hashMap.put("compass_random_token", this.f1404a.j());
            }
            if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dW)).booleanValue()) {
                hashMap.put("applovin_random_token", this.f1404a.k());
            }
            if (map != null) {
                hashMap.putAll(map);
            }
            hashMap.put("rid", UUID.randomUUID().toString());
            a2 = this.f1404a.J().a();
            if (a2 != null) {
                hashMap.put("lrm_ts_ms", String.valueOf(a2.a()));
                hashMap.put("lrm_url", a2.b());
                hashMap.put("lrm_ct_ms", String.valueOf(a2.d()));
                hashMap.put("lrm_rs", String.valueOf(a2.c()));
            }
            return hashMap;
        }
        aVar = this.f1404a.O().d();
        str = aVar.b;
        if (m.b(str)) {
        }
        hashMap.put("dnt", Boolean.toString(aVar.f1407a));
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dR)).booleanValue()) {
        }
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dU)).booleanValue()) {
        }
        if (((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dW)).booleanValue()) {
        }
        if (map != null) {
        }
        hashMap.put("rid", UUID.randomUUID().toString());
        a2 = this.f1404a.J().a();
        if (a2 != null) {
        }
        return hashMap;
    }

    public d b() {
        d dVar;
        Object obj = this.d.get(d.class);
        if (obj != null) {
            dVar = (d) obj;
        } else {
            dVar = new d();
            dVar.k = Locale.getDefault();
            dVar.f1410a = Build.MODEL;
            dVar.b = VERSION.RELEASE;
            dVar.c = "android";
            dVar.d = Build.MANUFACTURER;
            dVar.e = Build.BRAND;
            dVar.f = Build.HARDWARE;
            dVar.h = VERSION.SDK_INT;
            dVar.g = Build.DEVICE;
            dVar.r = l();
            dVar.x = q();
            dVar.I = i();
            try {
                dVar.y = ((SensorManager) this.c.getSystemService("sensor")).getDefaultSensor(4) != null;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Unable to retrieve gyroscope availability", th);
            }
            if (a("android.permission.READ_PHONE_STATE")) {
                TelephonyManager telephonyManager = (TelephonyManager) this.c.getSystemService(PlaceFields.PHONE);
                if (telephonyManager != null) {
                    dVar.i = telephonyManager.getSimCountryIso().toUpperCase(Locale.ENGLISH);
                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                    try {
                        dVar.j = URLEncoder.encode(networkOperatorName, "UTF-8");
                    } catch (UnsupportedEncodingException unused) {
                        dVar.j = networkOperatorName;
                    }
                }
            }
            try {
                DisplayMetrics displayMetrics = this.c.getResources().getDisplayMetrics();
                dVar.m = displayMetrics.density;
                dVar.n = displayMetrics.densityDpi;
                dVar.o = displayMetrics.xdpi;
                dVar.p = displayMetrics.ydpi;
                Point a2 = g.a(this.c);
                double sqrt = Math.sqrt(Math.pow((double) a2.x, 2.0d) + Math.pow((double) a2.y, 2.0d));
                double d2 = (double) dVar.o;
                Double.isNaN(d2);
                dVar.q = sqrt / d2;
            } catch (Throwable unused2) {
            }
            this.d.put(d.class, dVar);
        }
        return a(dVar);
    }

    public b c() {
        PackageInfo packageInfo;
        Object obj = this.d.get(b.class);
        if (obj != null) {
            return (b) obj;
        }
        ApplicationInfo applicationInfo = this.c.getApplicationInfo();
        long lastModified = new File(applicationInfo.sourceDir).lastModified();
        PackageManager packageManager = this.c.getPackageManager();
        String str = null;
        try {
            packageInfo = packageManager.getPackageInfo(this.c.getPackageName(), 0);
            try {
                str = packageManager.getInstallerPackageName(applicationInfo.packageName);
            } catch (Throwable unused) {
            }
        } catch (Throwable unused2) {
            packageInfo = null;
        }
        b bVar = new b();
        bVar.c = applicationInfo.packageName;
        if (str == null) {
            str = "";
        }
        bVar.d = str;
        bVar.g = lastModified;
        bVar.f1408a = String.valueOf(packageManager.getApplicationLabel(applicationInfo));
        bVar.b = packageInfo != null ? packageInfo.versionName : "";
        bVar.e = (String) this.f1404a.a(com.applovin.impl.sdk.b.e.g);
        bVar.f = Boolean.toString(p.b(this.f1404a));
        this.d.put(b.class, bVar);
        return bVar;
    }

    public a d() {
        a h2 = h();
        if (!((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dQ)).booleanValue()) {
            return new a();
        }
        if (!h2.f1407a || ((Boolean) this.f1404a.a(com.applovin.impl.sdk.b.c.dP)).booleanValue()) {
            return h2;
        }
        h2.b = "";
        return h2;
    }

    public void e() {
        this.f1404a.K().a((com.applovin.impl.sdk.d.a) new i(this.f1404a, new com.applovin.impl.sdk.d.i.a() {
            public void a(a aVar) {
                j.this.h.set(aVar);
            }
        }), com.applovin.impl.sdk.d.r.a.ADVERTISING_INFO_COLLECTION);
    }
}
