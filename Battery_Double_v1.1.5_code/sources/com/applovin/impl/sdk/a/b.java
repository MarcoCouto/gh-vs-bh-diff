package com.applovin.impl.sdk.a;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import com.applovin.impl.adview.m;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final i f1297a;
    /* access modifiers changed from: private */
    public final m b;
    /* access modifiers changed from: private */
    public AlertDialog c;

    public b(m mVar, i iVar) {
        this.f1297a = iVar;
        this.b = mVar;
    }

    public void a() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                if (b.this.c != null) {
                    b.this.c.dismiss();
                }
            }
        });
    }

    public void b() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                Builder builder = new Builder(b.this.b);
                builder.setTitle((CharSequence) b.this.f1297a.a(c.bN));
                builder.setMessage((CharSequence) b.this.f1297a.a(c.bO));
                builder.setCancelable(false);
                builder.setPositiveButton((CharSequence) b.this.f1297a.a(c.bQ), new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.continueVideo();
                        b.this.b.resumeReportRewardTask();
                    }
                });
                builder.setNegativeButton((CharSequence) b.this.f1297a.a(c.bP), new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.skipVideo();
                        b.this.b.resumeReportRewardTask();
                    }
                });
                b.this.c = builder.show();
            }
        });
    }

    public void c() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                Builder builder = new Builder(b.this.b);
                builder.setTitle((CharSequence) b.this.f1297a.a(c.bS));
                builder.setMessage((CharSequence) b.this.f1297a.a(c.bT));
                builder.setCancelable(false);
                builder.setPositiveButton((CharSequence) b.this.f1297a.a(c.bV), null);
                builder.setNegativeButton((CharSequence) b.this.f1297a.a(c.bU), new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.dismiss();
                    }
                });
                b.this.c = builder.show();
            }
        });
    }

    public boolean d() {
        if (this.c != null) {
            return this.c.isShowing();
        }
        return false;
    }
}
