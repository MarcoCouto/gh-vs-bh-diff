package com.applovin.impl.sdk.a;

import java.util.Map;

public class c {

    /* renamed from: a reason: collision with root package name */
    private final String f1304a;
    private Map<String, String> b;

    private c(String str, Map<String, String> map) {
        this.f1304a = str;
        this.b = map;
    }

    public static c a(String str) {
        return a(str, null);
    }

    public static c a(String str, Map<String, String> map) {
        return new c(str, map);
    }

    public Map<String, String> a() {
        return this.b;
    }

    public String b() {
        return this.f1304a;
    }
}
