package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.utils.n;

public class b extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private final i f1316a;
    /* access modifiers changed from: private */
    public final a b;
    private n c;
    private final Object d = new Object();
    private long e;

    public interface a {
        void onAdExpired();
    }

    public b(i iVar, a aVar) {
        this.f1316a = iVar;
        this.b = aVar;
    }

    private void b() {
        if (this.c != null) {
            this.c.d();
            this.c = null;
        }
    }

    private void c() {
        synchronized (this.d) {
            b();
        }
    }

    private void d() {
        boolean z;
        synchronized (this.d) {
            long currentTimeMillis = this.e - System.currentTimeMillis();
            if (currentTimeMillis <= 0) {
                a();
                z = true;
            } else {
                a(currentTimeMillis);
                z = false;
            }
        }
        if (z) {
            this.b.onAdExpired();
        }
    }

    public void a() {
        synchronized (this.d) {
            b();
            this.f1316a.ad().unregisterReceiver(this);
        }
    }

    public void a(long j) {
        synchronized (this.d) {
            a();
            this.e = System.currentTimeMillis() + j;
            this.f1316a.ad().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
            this.f1316a.ad().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
            if (((Boolean) this.f1316a.a(com.applovin.impl.sdk.b.b.H)).booleanValue() || !this.f1316a.Y().a()) {
                this.c = n.a(j, this.f1316a, new Runnable() {
                    public void run() {
                        b.this.a();
                        b.this.b.onAdExpired();
                    }
                });
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            c();
        } else if ("com.applovin.application_resumed".equals(action)) {
            d();
        }
    }
}
