package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.r;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

abstract class q implements l, AppLovinNativeAdLoadListener {

    /* renamed from: a reason: collision with root package name */
    protected final i f1431a;
    protected final o b;
    /* access modifiers changed from: private */
    public final Object c = new Object();
    private final Map<d, r> d = new HashMap();
    private final Map<d, r> e = new HashMap();
    /* access modifiers changed from: private */
    public final Map<d, Object> f = new HashMap();
    private final Set<d> g = new HashSet();

    q(i iVar) {
        this.f1431a = iVar;
        this.b = iVar.v();
    }

    private void b(final d dVar, Object obj) {
        synchronized (this.c) {
            if (this.f.containsKey(dVar)) {
                this.b.d("PreloadManager", "Possibly missing prior registered preload callback.");
            }
            this.f.put(dVar, obj);
        }
        final int intValue = ((Integer) this.f1431a.a(c.aX)).intValue();
        if (intValue > 0) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    synchronized (q.this.c) {
                        Object obj = q.this.f.get(dVar);
                        if (obj != null) {
                            q.this.f.remove(dVar);
                            StringBuilder sb = new StringBuilder();
                            sb.append("Load callback for zone ");
                            sb.append(dVar);
                            sb.append(" timed out after ");
                            sb.append(intValue);
                            sb.append(" seconds");
                            q.this.b.e("PreloadManager", sb.toString());
                            q.this.a(obj, dVar, AppLovinErrorCodes.FETCH_AD_TIMEOUT);
                        }
                    }
                }
            }, TimeUnit.SECONDS.toMillis((long) intValue));
        }
    }

    private void c(j jVar) {
        i(a(jVar));
    }

    private r j(d dVar) {
        return (r) this.d.get(dVar);
    }

    private r k(d dVar) {
        return (r) this.e.get(dVar);
    }

    private boolean l(d dVar) {
        boolean z;
        synchronized (this.c) {
            r j = j(dVar);
            z = j != null && j.c();
        }
        return z;
    }

    private r m(d dVar) {
        synchronized (this.c) {
            r k = k(dVar);
            if (k != null && k.a() > 0) {
                return k;
            }
            r j = j(dVar);
            return j;
        }
    }

    private boolean n(d dVar) {
        boolean contains;
        synchronized (this.c) {
            contains = this.g.contains(dVar);
        }
        return contains;
    }

    /* access modifiers changed from: 0000 */
    public abstract d a(j jVar);

    /* access modifiers changed from: 0000 */
    public abstract a a(d dVar);

    /* access modifiers changed from: 0000 */
    public abstract void a(Object obj, d dVar, int i);

    /* access modifiers changed from: 0000 */
    public abstract void a(Object obj, j jVar);

    public void a(LinkedHashSet<d> linkedHashSet) {
        if (this.f != null && !this.f.isEmpty()) {
            synchronized (this.c) {
                Iterator it = this.f.keySet().iterator();
                while (it.hasNext()) {
                    d dVar = (d) it.next();
                    if (!dVar.j() && !linkedHashSet.contains(dVar)) {
                        Object obj = this.f.get(dVar);
                        it.remove();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to load ad for zone (");
                        sb.append(dVar.a());
                        sb.append("). Please check that the zone has been added to your AppLovin account and given at least 30 minutes to fully propagate.");
                        o.i("AppLovinAdService", sb.toString());
                        a(obj, dVar, -7);
                    }
                }
            }
        }
    }

    public boolean a(d dVar, Object obj) {
        boolean z;
        synchronized (this.c) {
            if (!n(dVar)) {
                b(dVar, obj);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public void b(d dVar, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            i(dVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(j jVar) {
        Object obj;
        o oVar;
        String str;
        String sb;
        d a2 = a(jVar);
        boolean i = a2.i();
        synchronized (this.c) {
            obj = this.f.get(a2);
            this.f.remove(a2);
            this.g.add(a2);
            if (obj != null) {
                if (!i) {
                    oVar = this.b;
                    str = "PreloadManager";
                    sb = "Additional callback found or dummy ads are enabled; skipping enqueue...";
                    oVar.b(str, sb);
                }
            }
            j(a2).a(jVar);
            oVar = this.b;
            str = "PreloadManager";
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Ad enqueued: ");
            sb2.append(jVar);
            sb = sb2.toString();
            oVar.b(str, sb);
        }
        if (obj != null) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Called additional callback regarding ");
            sb3.append(jVar);
            this.b.b("PreloadManager", sb3.toString());
            if (i) {
                try {
                    a(obj, (j) new g(a2, this.f1431a));
                } catch (Throwable th) {
                    o.c("PreloadManager", "Encountered throwable while notifying user callback", th);
                }
            } else {
                a(obj, jVar);
                c(jVar);
            }
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append("Pulled ad from network and saved to preload cache: ");
        sb4.append(jVar);
        this.b.b("PreloadManager", sb4.toString());
    }

    public boolean b(d dVar) {
        return this.f.containsKey(dVar);
    }

    public j c(d dVar) {
        j f2;
        synchronized (this.c) {
            r m = m(dVar);
            f2 = m != null ? m.f() : null;
        }
        return f2;
    }

    /* access modifiers changed from: 0000 */
    public void c(d dVar, int i) {
        Object remove;
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to pre-load an ad of zone ");
        sb.append(dVar);
        sb.append(", error code ");
        sb.append(i);
        this.b.b("PreloadManager", sb.toString());
        synchronized (this.c) {
            remove = this.f.remove(dVar);
            this.g.add(dVar);
        }
        if (remove != null) {
            try {
                a(remove, dVar, i);
            } catch (Throwable th) {
                o.c("PreloadManager", "Encountered exception while invoking user callback", th);
            }
        }
    }

    public j d(d dVar) {
        j e2;
        synchronized (this.c) {
            r m = m(dVar);
            e2 = m != null ? m.e() : null;
        }
        return e2;
    }

    public j e(d dVar) {
        j jVar;
        o oVar;
        String str;
        StringBuilder sb;
        String str2;
        j gVar;
        synchronized (this.c) {
            r j = j(dVar);
            jVar = null;
            if (j != null) {
                if (dVar.i()) {
                    r k = k(dVar);
                    if (k.c()) {
                        gVar = new g(dVar, this.f1431a);
                    } else if (j.a() > 0) {
                        k.a(j.e());
                        gVar = new g(dVar, this.f1431a);
                    } else if (k.a() > 0 && ((Boolean) this.f1431a.a(c.bZ)).booleanValue()) {
                        gVar = new g(dVar, this.f1431a);
                    }
                    jVar = gVar;
                } else {
                    jVar = j.e();
                }
            }
        }
        if (jVar != null) {
            oVar = this.b;
            str = "PreloadManager";
            sb = new StringBuilder();
            str2 = "Retrieved ad of zone ";
        } else {
            oVar = this.b;
            str = "PreloadManager";
            sb = new StringBuilder();
            str2 = "Unable to retrieve ad of zone ";
        }
        sb.append(str2);
        sb.append(dVar);
        sb.append("...");
        oVar.b(str, sb.toString());
        return jVar;
    }

    public void f(d dVar) {
        if (dVar != null) {
            int i = 0;
            synchronized (this.c) {
                r j = j(dVar);
                if (j != null) {
                    i = j.b() - j.a();
                }
            }
            b(dVar, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        return r3;
     */
    public boolean g(d dVar) {
        synchronized (this.c) {
            r k = k(dVar);
            boolean z = true;
            if (((Boolean) this.f1431a.a(c.ca)).booleanValue() && k != null && k.a() > 0) {
                return true;
            }
            r j = j(dVar);
            if (j == null || j.d()) {
                z = false;
            }
        }
    }

    public void h(d dVar) {
        synchronized (this.c) {
            r j = j(dVar);
            if (j != null) {
                j.a(dVar.e());
            } else {
                this.d.put(dVar, new r(dVar.e()));
            }
            r k = k(dVar);
            if (k != null) {
                k.a(dVar.f());
            } else {
                this.e.put(dVar, new r(dVar.f()));
            }
        }
    }

    public void i(d dVar) {
        if (((Boolean) this.f1431a.a(c.aY)).booleanValue() && !l(dVar)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Preloading ad for zone ");
            sb.append(dVar);
            sb.append("...");
            this.b.b("PreloadManager", sb.toString());
            this.f1431a.K().a(a(dVar), r.a.MAIN, 500);
        }
    }
}
