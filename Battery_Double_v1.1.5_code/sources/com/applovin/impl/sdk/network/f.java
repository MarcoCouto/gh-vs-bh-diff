package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.i;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a reason: collision with root package name */
    private String f1427a;
    private String b;
    private String c;
    private Map<String, String> d;
    private Map<String, String> e;
    private Map<String, Object> f;
    private boolean g;
    private int h;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public String f1428a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public Map<String, String> c;
        /* access modifiers changed from: private */
        public Map<String, String> d;
        /* access modifiers changed from: private */
        public Map<String, Object> e;
        /* access modifiers changed from: private */
        public boolean f;

        public a a(String str) {
            this.f1428a = str;
            return this;
        }

        public a a(Map<String, String> map) {
            this.c = map;
            return this;
        }

        public a a(boolean z) {
            this.f = z;
            return this;
        }

        public f a() {
            return new f(this);
        }

        public a b(String str) {
            this.b = str;
            return this;
        }

        public a b(Map<String, String> map) {
            this.d = map;
            return this;
        }

        public a c(Map<String, Object> map) {
            this.e = map;
            return this;
        }
    }

    private f(a aVar) {
        this.f1427a = UUID.randomUUID().toString();
        this.b = aVar.f1428a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.e = aVar.d;
        this.f = aVar.e;
        this.g = aVar.f;
        this.h = 0;
    }

    f(JSONObject jSONObject, i iVar) throws Exception {
        String b2 = com.applovin.impl.sdk.utils.i.b(jSONObject, "uniqueId", UUID.randomUUID().toString(), iVar);
        String string = jSONObject.getString("targetUrl");
        String b3 = com.applovin.impl.sdk.utils.i.b(jSONObject, "backupUrl", "", iVar);
        int i = jSONObject.getInt("attemptNumber");
        Map<String, String> a2 = com.applovin.impl.sdk.utils.i.a(jSONObject, "parameters") ? com.applovin.impl.sdk.utils.i.a(jSONObject.getJSONObject("parameters")) : Collections.emptyMap();
        Map<String, String> a3 = com.applovin.impl.sdk.utils.i.a(jSONObject, "httpHeaders") ? com.applovin.impl.sdk.utils.i.a(jSONObject.getJSONObject("httpHeaders")) : Collections.emptyMap();
        Map<String, Object> b4 = com.applovin.impl.sdk.utils.i.a(jSONObject, "requestBody") ? com.applovin.impl.sdk.utils.i.b(jSONObject.getJSONObject("requestBody")) : Collections.emptyMap();
        this.f1427a = b2;
        this.b = string;
        this.c = b3;
        this.d = a2;
        this.e = a3;
        this.f = b4;
        this.g = jSONObject.optBoolean("isEncodingEnabled", false);
        this.h = i;
    }

    public static a k() {
        return new a();
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> c() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> d() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, Object> e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f1427a.equals(((f) obj).f1427a);
    }

    /* access modifiers changed from: 0000 */
    public boolean f() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public int g() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public void h() {
        this.h++;
    }

    public int hashCode() {
        return this.f1427a.hashCode();
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        HashMap hashMap = new HashMap();
        if (this.d != null) {
            hashMap.putAll(this.d);
        }
        hashMap.put("postback_ts", String.valueOf(System.currentTimeMillis()));
        this.d = hashMap;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject j() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("uniqueId", this.f1427a);
        jSONObject.put("targetUrl", this.b);
        jSONObject.put("backupUrl", this.c);
        jSONObject.put("isEncodingEnabled", this.g);
        jSONObject.put("attemptNumber", this.h);
        if (this.d != null) {
            jSONObject.put("parameters", new JSONObject(this.d));
        }
        if (this.e != null) {
            jSONObject.put("httpHeaders", new JSONObject(this.e));
        }
        if (this.f != null) {
            jSONObject.put("requestBody", new JSONObject(this.f));
        }
        return jSONObject;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PostbackRequest{uniqueId='");
        sb.append(this.f1427a);
        sb.append('\'');
        sb.append("targetUrl='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", backupUrl='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", attemptNumber=");
        sb.append(this.h);
        sb.append(", isEncodingEnabled=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }
}
