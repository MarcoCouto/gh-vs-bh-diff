package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class b<T> {

    /* renamed from: a reason: collision with root package name */
    private String f1421a;
    private String b;
    private Map<String, String> c;
    private Map<String, String> d;
    private final JSONObject e;
    private String f;
    private final T g;
    private final boolean h;
    private final int i;
    private int j;
    private final int k;
    private final int l;
    private final boolean m;
    private final boolean n;

    public static class a<T> {

        /* renamed from: a reason: collision with root package name */
        String f1422a;
        String b;
        String c;
        Map<String, String> d;
        Map<String, String> e;
        JSONObject f;
        T g;
        boolean h = true;
        int i = 1;
        int j;
        int k;
        boolean l;
        boolean m;

        public a(i iVar) {
            this.j = ((Integer) iVar.a(c.du)).intValue();
            this.k = ((Integer) iVar.a(c.dt)).intValue();
            this.l = ((Boolean) iVar.a(c.eJ)).booleanValue();
            this.d = new HashMap();
        }

        public a<T> a(int i2) {
            this.i = i2;
            return this;
        }

        public a<T> a(T t) {
            this.g = t;
            return this;
        }

        public a<T> a(String str) {
            this.b = str;
            return this;
        }

        public a<T> a(Map<String, String> map) {
            this.d = map;
            return this;
        }

        public a<T> a(JSONObject jSONObject) {
            this.f = jSONObject;
            return this;
        }

        public a<T> a(boolean z) {
            this.l = z;
            return this;
        }

        public b<T> a() {
            return new b<>(this);
        }

        public a<T> b(int i2) {
            this.j = i2;
            return this;
        }

        public a<T> b(String str) {
            this.f1422a = str;
            return this;
        }

        public a<T> b(boolean z) {
            this.m = z;
            return this;
        }

        public a<T> c(int i2) {
            this.k = i2;
            return this;
        }

        public a<T> c(String str) {
            this.c = str;
            return this;
        }
    }

    protected b(a<T> aVar) {
        this.f1421a = aVar.b;
        this.b = aVar.f1422a;
        this.c = aVar.d;
        this.d = aVar.e;
        this.e = aVar.f;
        this.f = aVar.c;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.i;
        this.k = aVar.j;
        this.l = aVar.k;
        this.m = aVar.l;
        this.n = aVar.m;
    }

    public static <T> a<T> a(i iVar) {
        return new a<>(iVar);
    }

    public String a() {
        return this.f1421a;
    }

    public void a(int i2) {
        this.j = i2;
    }

    public void a(String str) {
        this.f1421a = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public Map<String, String> c() {
        return this.c;
    }

    public Map<String, String> d() {
        return this.d;
    }

    public JSONObject e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f1421a == null ? bVar.f1421a != null : !this.f1421a.equals(bVar.f1421a)) {
            return false;
        }
        if (this.c == null ? bVar.c != null : !this.c.equals(bVar.c)) {
            return false;
        }
        if (this.d == null ? bVar.d != null : !this.d.equals(bVar.d)) {
            return false;
        }
        if (this.f == null ? bVar.f != null : !this.f.equals(bVar.f)) {
            return false;
        }
        if (this.b == null ? bVar.b != null : !this.b.equals(bVar.b)) {
            return false;
        }
        if (this.e == null ? bVar.e != null : !this.e.equals(bVar.e)) {
            return false;
        }
        if (this.g == null ? bVar.g == null : this.g.equals(bVar.g)) {
            return this.h == bVar.h && this.i == bVar.i && this.j == bVar.j && this.k == bVar.k && this.l == bVar.l && this.m == bVar.m && this.n == bVar.n;
        }
        return false;
    }

    public String f() {
        return this.f;
    }

    public T g() {
        return this.g;
    }

    public boolean h() {
        return this.h;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((((((super.hashCode() * 31) + (this.f1421a != null ? this.f1421a.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.g != null) {
            i2 = this.g.hashCode();
        }
        int i3 = ((((((((((((((hashCode + i2) * 31) + (this.h ? 1 : 0)) * 31) + this.i) * 31) + this.j) * 31) + this.k) * 31) + this.l) * 31) + (this.m ? 1 : 0)) * 31) + (this.n ? 1 : 0);
        if (this.c != null) {
            i3 = (i3 * 31) + this.c.hashCode();
        }
        if (this.d != null) {
            i3 = (i3 * 31) + this.d.hashCode();
        }
        if (this.e == null) {
            return i3;
        }
        char[] charArray = this.e.toString().toCharArray();
        Arrays.sort(charArray);
        return (i3 * 31) + new String(charArray).hashCode();
    }

    public int i() {
        return this.i - this.j;
    }

    public int j() {
        return this.j;
    }

    public int k() {
        return this.k;
    }

    public int l() {
        return this.l;
    }

    public boolean m() {
        return this.m;
    }

    public boolean n() {
        return this.n;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("HttpRequest {endpoint=");
        sb.append(this.f1421a);
        sb.append(", backupEndpoint=");
        sb.append(this.f);
        sb.append(", httpMethod=");
        sb.append(this.b);
        sb.append(", httpHeaders=");
        sb.append(this.d);
        sb.append(", body=");
        sb.append(this.e);
        sb.append(", emptyResponse=");
        sb.append(this.g);
        sb.append(", requiresResponse=");
        sb.append(this.h);
        sb.append(", initialRetryAttempts=");
        sb.append(this.i);
        sb.append(", retryAttemptsLeft=");
        sb.append(this.j);
        sb.append(", timeoutMillis=");
        sb.append(this.k);
        sb.append(", retryDelayMillis=");
        sb.append(this.l);
        sb.append(", encodingEnabled=");
        sb.append(this.m);
        sb.append(", trackConnectionSpeed=");
        sb.append(this.n);
        sb.append('}');
        return sb.toString();
    }
}
