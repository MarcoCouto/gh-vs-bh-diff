package com.applovin.impl.sdk.network;

import android.content.Context;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.utils.p;
import org.json.JSONException;
import org.json.JSONObject;

public class d {

    /* renamed from: a reason: collision with root package name */
    private static final Object f1424a = new Object();

    private static JSONObject a(String str, Context context) {
        JSONObject b = b(context);
        if (b == null) {
            b = new JSONObject();
        }
        if (!b.has(str)) {
            try {
                b.put(str, new JSONObject());
            } catch (JSONException unused) {
            }
        }
        return b;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:5|6|7|8|9|10|11|12|13|14) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0023 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0026 */
    static void a(int i, String str, Context context) {
        if (!TextUtils.isEmpty(str)) {
            synchronized (f1424a) {
                String d = p.d(str);
                JSONObject a2 = a(d, context);
                String num = Integer.toString(i);
                JSONObject optJSONObject = a2.optJSONObject(d);
                optJSONObject.put(num, optJSONObject.optInt(num) + 1);
                a2.put(d, optJSONObject);
                a(a2, context);
            }
        }
    }

    public static void a(Context context) {
        synchronized (f1424a) {
            f.a(e.o, context);
        }
    }

    private static void a(JSONObject jSONObject, Context context) {
        f.a(e.o, jSONObject.toString(), context);
    }

    public static JSONObject b(Context context) {
        JSONObject jSONObject;
        synchronized (f1424a) {
            try {
                jSONObject = new JSONObject((String) f.b(e.o, "{}", context));
            } catch (JSONException unused) {
                return new JSONObject();
            } catch (Throwable th) {
                throw th;
            }
        }
        return jSONObject;
    }
}
