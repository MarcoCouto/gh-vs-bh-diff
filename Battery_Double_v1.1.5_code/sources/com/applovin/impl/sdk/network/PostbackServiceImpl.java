package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.d.j;
import com.applovin.impl.sdk.d.r.a;
import com.applovin.impl.sdk.i;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinPostbackService;

public class PostbackServiceImpl implements AppLovinPostbackService {

    /* renamed from: a reason: collision with root package name */
    private final i f1417a;

    public PostbackServiceImpl(i iVar) {
        this.f1417a = iVar;
    }

    public void dispatchPostbackAsync(String str, AppLovinPostbackListener appLovinPostbackListener) {
        dispatchPostbackRequest(g.b(this.f1417a).a(str).a(false).a(), appLovinPostbackListener);
    }

    public void dispatchPostbackRequest(g gVar, a aVar, AppLovinPostbackListener appLovinPostbackListener) {
        this.f1417a.K().a((com.applovin.impl.sdk.d.a) new j(gVar, aVar, this.f1417a, appLovinPostbackListener), aVar);
    }

    public void dispatchPostbackRequest(g gVar, AppLovinPostbackListener appLovinPostbackListener) {
        dispatchPostbackRequest(gVar, a.POSTBACKS, appLovinPostbackListener);
    }

    public String toString() {
        return "PostbackService{}";
    }
}
