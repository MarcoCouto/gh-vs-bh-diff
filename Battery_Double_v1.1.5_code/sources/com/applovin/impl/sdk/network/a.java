package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.l;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.r;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinErrorCodes;
import com.explorestack.iab.vast.VastError;
import io.fabric.sdk.android.services.network.UrlUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

public class a {

    /* renamed from: a reason: collision with root package name */
    private final i f1418a;
    private final o b;
    private b c;

    /* renamed from: com.applovin.impl.sdk.network.a$a reason: collision with other inner class name */
    public static class C0012a {

        /* renamed from: a reason: collision with root package name */
        private long f1419a;
        private long b;

        /* access modifiers changed from: private */
        public void a(long j) {
            this.f1419a = j;
        }

        /* access modifiers changed from: private */
        public void b(long j) {
            this.b = j;
        }

        public long a() {
            return this.f1419a;
        }

        public long b() {
            return this.b;
        }
    }

    public static class b {

        /* renamed from: a reason: collision with root package name */
        private final long f1420a = System.currentTimeMillis();
        private final String b;
        private final long c;
        private final long d;

        b(String str, long j, long j2) {
            this.b = str;
            this.c = j;
            this.d = j2;
        }

        public long a() {
            return this.f1420a;
        }

        public String b() {
            return this.b;
        }

        public long c() {
            return this.c;
        }

        public long d() {
            return this.d;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("RequestMeasurement{timestampMillis=");
            sb.append(this.f1420a);
            sb.append(", urlHostAndPathString='");
            sb.append(this.b);
            sb.append('\'');
            sb.append(", responseSize=");
            sb.append(this.c);
            sb.append(", connectionTimeMillis=");
            sb.append(this.d);
            sb.append('}');
            return sb.toString();
        }
    }

    public interface c<T> {
        void a(int i);

        void a(T t, int i);
    }

    public a(i iVar) {
        this.f1418a = iVar;
        this.b = iVar.v();
    }

    private int a(Throwable th) {
        if (th instanceof UnknownHostException) {
            return AppLovinErrorCodes.NO_NETWORK;
        }
        if (th instanceof SocketTimeoutException) {
            return AppLovinErrorCodes.FETCH_AD_TIMEOUT;
        }
        if (!(th instanceof IOException)) {
            return th instanceof JSONException ? -104 : -1;
        }
        if (((Boolean) this.f1418a.a(com.applovin.impl.sdk.b.c.ad)).booleanValue()) {
            return -100;
        }
        String message = th.getMessage();
        if (message == null || !message.toLowerCase(Locale.ENGLISH).contains("authentication challenge")) {
            return -100;
        }
        return VastError.ERROR_CODE_NO_FILE;
    }

    private <T> T a(String str, T t) throws JSONException, SAXException, ClassCastException {
        if (t == null) {
            return str;
        }
        if (str != null && str.length() >= 3) {
            if (t instanceof JSONObject) {
                return new JSONObject(str);
            }
            if (t instanceof r) {
                return s.a(str, this.f1418a);
            }
            if (t instanceof String) {
                return str;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to process response of type '");
            sb.append(t.getClass().getName());
            sb.append("'");
            this.b.e("ConnectionManager", sb.toString());
        }
        return t;
    }

    private HttpURLConnection a(String str, String str2, Map<String, String> map, int i) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setRequestMethod(str2);
        httpURLConnection.setConnectTimeout(i < 0 ? ((Integer) this.f1418a.a(com.applovin.impl.sdk.b.c.du)).intValue() : i);
        if (i < 0) {
            i = ((Integer) this.f1418a.a(com.applovin.impl.sdk.b.c.dv)).intValue();
        }
        httpURLConnection.setReadTimeout(i);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setDoInput(true);
        if (map != null && map.size() > 0) {
            for (String str3 : map.keySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append("AppLovin-");
                sb.append(str3);
                httpURLConnection.setRequestProperty(sb.toString(), (String) map.get(str3));
            }
        }
        return httpURLConnection;
    }

    private void a(int i, String str) {
        if (((Boolean) this.f1418a.a(com.applovin.impl.sdk.b.c.aa)).booleanValue()) {
            try {
                d.a(i, str, this.f1418a.D());
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to track response code for URL: ");
                sb.append(str);
                this.f1418a.v().b("ConnectionManager", sb.toString(), th);
            }
        }
    }

    private void a(String str) {
        h hVar;
        g gVar;
        if (m.a(str, com.applovin.impl.sdk.utils.h.g(this.f1418a)) || m.a(str, com.applovin.impl.sdk.utils.h.h(this.f1418a))) {
            hVar = this.f1418a.L();
            gVar = g.h;
        } else if (m.a(str, com.applovin.impl.mediation.d.b.a(this.f1418a)) || m.a(str, com.applovin.impl.mediation.d.b.b(this.f1418a))) {
            hVar = this.f1418a.L();
            gVar = g.n;
        } else {
            hVar = this.f1418a.L();
            gVar = g.i;
        }
        hVar.a(gVar);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.String, code=T, for r5v0, types: [T, java.lang.String] */
    private <T> void a(T t, int i, String str, T t2, boolean z, c<T> cVar) {
        o oVar;
        String str2;
        StringBuilder sb;
        String str3;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(i);
        sb2.append(" received from \"");
        sb2.append(str);
        this.b.b("ConnectionManager", sb2.toString());
        this.b.a("ConnectionManager", t);
        if (i < 200 || i >= 300) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(i);
            sb3.append(" error received from \"");
            sb3.append(str);
            sb3.append("\"");
            this.b.e("ConnectionManager", sb3.toString());
            cVar.a(i);
            return;
        }
        if (z) {
            t = l.a((String) t, this.f1418a.t());
        }
        boolean z2 = t != null && t.length() > 2;
        if (i != 204 && z2) {
            try {
                if (!(t2 instanceof String)) {
                    if (t2 instanceof r) {
                        t = s.a((String) t, this.f1418a);
                    } else if (t2 instanceof JSONObject) {
                        t2 = new JSONObject(t);
                    } else {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Unable to handle '");
                        sb4.append(t2.getClass().getName());
                        sb4.append("'");
                        this.b.e("ConnectionManager", sb4.toString());
                    }
                }
                t2 = t;
            } catch (JSONException e) {
                e = e;
                a(str);
                oVar = this.b;
                str3 = "ConnectionManager";
                sb = new StringBuilder();
                str2 = "Invalid JSON returned from \"";
                sb.append(str2);
                sb.append(str);
                sb.append("\"");
                oVar.b(str3, sb.toString(), e);
                cVar.a(t2, i);
            } catch (SAXException e2) {
                e = e2;
                a(str);
                oVar = this.b;
                str3 = "ConnectionManager";
                sb = new StringBuilder();
                str2 = "Invalid XML returned from \"";
                sb.append(str2);
                sb.append(str);
                sb.append("\"");
                oVar.b(str3, sb.toString(), e);
                cVar.a(t2, i);
            }
        }
        cVar.a(t2, i);
    }

    private void a(String str, String str2, int i, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("Successful ");
        sb.append(str);
        sb.append(" returned ");
        sb.append(i);
        sb.append(" in ");
        sb.append(((float) (System.currentTimeMillis() - j)) / 1000.0f);
        sb.append(" s over ");
        sb.append(com.applovin.impl.sdk.utils.h.f(this.f1418a));
        sb.append(" to \"");
        sb.append(str2);
        sb.append("\"");
        this.b.c("ConnectionManager", sb.toString());
    }

    private void a(String str, String str2, int i, long j, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed ");
        sb.append(str);
        sb.append(" returned ");
        sb.append(i);
        sb.append(" in ");
        sb.append(((float) (System.currentTimeMillis() - j)) / 1000.0f);
        sb.append(" s over ");
        sb.append(com.applovin.impl.sdk.utils.h.f(this.f1418a));
        sb.append(" to \"");
        sb.append(str2);
        sb.append("\"");
        this.b.b("ConnectionManager", sb.toString(), th);
    }

    public b a() {
        return this.c;
    }

    /* JADX WARNING: type inference failed for: r13v1, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r13v2, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r13v3 */
    /* JADX WARNING: type inference failed for: r13v4 */
    /* JADX WARNING: type inference failed for: r13v5, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r13v6 */
    /* JADX WARNING: type inference failed for: r13v7 */
    /* JADX WARNING: type inference failed for: r13v8 */
    /* JADX WARNING: type inference failed for: r13v9 */
    /* JADX WARNING: type inference failed for: r13v10 */
    /* JADX WARNING: type inference failed for: r13v11 */
    /* JADX WARNING: type inference failed for: r13v12 */
    /* JADX WARNING: type inference failed for: r13v13 */
    /* JADX WARNING: type inference failed for: r13v14 */
    /* JADX WARNING: type inference failed for: r6v2, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r13v15 */
    /* JADX WARNING: type inference failed for: r13v16 */
    /* JADX WARNING: type inference failed for: r13v17 */
    /* JADX WARNING: type inference failed for: r13v18 */
    /* JADX WARNING: type inference failed for: r13v19 */
    /* JADX WARNING: type inference failed for: r13v20, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r13v21 */
    /* JADX WARNING: type inference failed for: r13v22 */
    /* JADX WARNING: type inference failed for: r13v23 */
    /* JADX WARNING: type inference failed for: r13v24 */
    /* JADX WARNING: type inference failed for: r13v25 */
    /* JADX WARNING: type inference failed for: r13v26 */
    /* JADX WARNING: type inference failed for: r13v27, types: [long] */
    /* JADX WARNING: type inference failed for: r13v28, types: [long] */
    /* JADX WARNING: type inference failed for: r19v1, types: [long] */
    /* JADX WARNING: type inference failed for: r13v29 */
    /* JADX WARNING: type inference failed for: r13v30 */
    /* JADX WARNING: type inference failed for: r13v31 */
    /* JADX WARNING: type inference failed for: r13v32 */
    /* JADX WARNING: type inference failed for: r13v33 */
    /* JADX WARNING: type inference failed for: r13v34 */
    /* JADX WARNING: type inference failed for: r13v35 */
    /* JADX WARNING: type inference failed for: r13v36 */
    /* JADX WARNING: type inference failed for: r13v37 */
    /* JADX WARNING: type inference failed for: r13v38 */
    /* JADX WARNING: type inference failed for: r13v39 */
    /* JADX WARNING: type inference failed for: r13v40 */
    /* JADX WARNING: type inference failed for: r13v41 */
    /* JADX WARNING: type inference failed for: r13v42 */
    /* JADX WARNING: type inference failed for: r13v43 */
    /* JADX WARNING: type inference failed for: r13v44 */
    /* JADX WARNING: type inference failed for: r13v45 */
    /* JADX WARNING: type inference failed for: r13v46 */
    /* JADX WARNING: type inference failed for: r13v47 */
    /* JADX WARNING: type inference failed for: r13v48 */
    /* JADX WARNING: type inference failed for: r13v49 */
    /* JADX WARNING: type inference failed for: r13v50 */
    /* JADX WARNING: type inference failed for: r13v51 */
    /* JADX WARNING: type inference failed for: r13v52 */
    /* JADX WARNING: type inference failed for: r13v53 */
    /* JADX WARNING: type inference failed for: r13v54 */
    /* JADX WARNING: type inference failed for: r13v55 */
    /* JADX WARNING: type inference failed for: r13v56 */
    /* JADX WARNING: type inference failed for: r13v57 */
    /* JADX WARNING: type inference failed for: r13v58 */
    /* JADX WARNING: type inference failed for: r13v59 */
    /* JADX WARNING: type inference failed for: r13v60 */
    /* JADX WARNING: type inference failed for: r13v61 */
    /* JADX WARNING: type inference failed for: r13v62 */
    /* JADX WARNING: type inference failed for: r13v63 */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x02a6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x02a7, code lost:
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
        r3 = new java.lang.StringBuilder();
        r3.append("Unable to parse response from \"");
        r3.append(r12);
        r3.append("\"");
        r8.b.b("ConnectionManager", r3.toString(), r0);
        r8.f1418a.L().a(com.applovin.impl.sdk.c.g.i);
        r10.a(-800);
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x02ef, code lost:
        r0 = e;
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x02f1, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02f2, code lost:
        r13 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0331, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0332, code lost:
        r5 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0336, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0338, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x033a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x033b, code lost:
        r16 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x033f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0340, code lost:
        r23 = r13;
        r7 = r0;
        r9 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0349, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x034a, code lost:
        r16 = r5;
        r23 = r13;
        r7 = r0;
        r15 = 0;
        r13 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0366, code lost:
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x036a, code lost:
        if (r26.h() == false) goto L_0x0378;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x036d, code lost:
        r0 = th;
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x036e, code lost:
        r5 = r16;
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:?, code lost:
        a(r11, r12, r15, r23);
        r10.a(r26.g(), -901);
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0199, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x019a, code lost:
        r7 = r0;
        r23 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x019f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01a0, code lost:
        r7 = r0;
        r23 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01fd, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01fe, code lost:
        r23 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0200, code lost:
        r7 = r0;
        r13 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0204, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0205, code lost:
        r23 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0207, code lost:
        r7 = r0;
        r13 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0211, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0213, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x022b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x022e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:51:0x0195, B:144:0x0328] */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:88:0x0226, B:114:0x0299] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r13v6
  assigns: []
  uses: []
  mth insns count: 484
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x02ef A[ExcHandler: MalformedURLException (e java.net.MalformedURLException), PHI: r13 r23 
  PHI: (r13v19 ?) = (r13v52 ?), (r13v54 ?), (r13v56 ?), (r13v59 ?), (r13v60 ?) binds: [B:114:0x0299, B:117:0x02a7, B:115:?, B:85:0x0217, B:88:0x0226] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r23v21 long) = (r23v22 long), (r23v22 long), (r23v22 long), (r23v24 long), (r23v24 long) binds: [B:114:0x0299, B:117:0x02a7, B:115:?, B:85:0x0217, B:88:0x0226] A[DONT_GENERATE, DONT_INLINE], Splitter:B:88:0x0226] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x02f1 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:60:0x01ac] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0331 A[ExcHandler: all (th java.lang.Throwable), PHI: r16 
  PHI: (r16v9 java.net.HttpURLConnection) = (r16v10 java.net.HttpURLConnection), (r16v10 java.net.HttpURLConnection), (r16v19 java.net.HttpURLConnection), (r16v21 java.net.HttpURLConnection), (r16v21 java.net.HttpURLConnection) binds: [B:144:0x0328, B:145:?, B:58:0x01a8, B:51:0x0195, B:52:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:51:0x0195] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x033a A[ExcHandler: all (th java.lang.Throwable), Splitter:B:44:0x0176] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0366 A[SYNTHETIC, Splitter:B:161:0x0366] */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x036d A[ExcHandler: all (th java.lang.Throwable), PHI: r13 r16 
  PHI: (r13v9 ?) = (r13v38 ?), (r13v39 ?), (r13v43 ?), (r13v53 ?), (r13v57 ?) binds: [B:173:0x0385, B:174:?, B:161:0x0366, B:114:0x0299, B:115:?] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r16v4 java.net.HttpURLConnection) = (r16v5 java.net.HttpURLConnection), (r16v5 java.net.HttpURLConnection), (r16v5 java.net.HttpURLConnection), (r16v19 java.net.HttpURLConnection), (r16v19 java.net.HttpURLConnection) binds: [B:173:0x0385, B:174:?, B:161:0x0366, B:114:0x0299, B:115:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:114:0x0299] */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x038c  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:85:0x0217=Splitter:B:85:0x0217, B:114:0x0299=Splitter:B:114:0x0299} */
    /* JADX WARNING: Unknown variable types count: 21 */
    public <T> void a(b<T> bVar, C0012a aVar, c<T> cVar) {
        ? r13;
        HttpURLConnection httpURLConnection;
        long j;
        int i;
        ? r132;
        HttpURLConnection httpURLConnection2;
        Throwable th;
        long j2;
        Throwable th2;
        HttpURLConnection httpURLConnection3;
        ? r133;
        ? r134;
        ? r135;
        ? r136;
        Throwable th3;
        int responseCode;
        long j3;
        ? r137;
        String str;
        C0012a aVar2 = aVar;
        c<T> cVar2 = cVar;
        if (bVar != null) {
            String a2 = bVar.a();
            String b2 = bVar.b();
            if (a2 == null) {
                throw new IllegalArgumentException("No endpoint specified");
            } else if (b2 == null) {
                throw new IllegalArgumentException("No method specified");
            } else if (cVar2 == null) {
                throw new IllegalArgumentException("No callback specified");
            } else if (!a2.toLowerCase().startsWith("http")) {
                StringBuilder sb = new StringBuilder();
                sb.append("Requested postback submission to non HTTP endpoint ");
                sb.append(a2);
                sb.append("; skipping...");
                o.i("ConnectionManager", sb.toString());
                cVar2.a(AppLovinErrorCodes.INVALID_URL);
            } else {
                if (((Boolean) this.f1418a.a(com.applovin.impl.sdk.b.c.dw)).booleanValue() && !a2.contains("https://")) {
                    this.f1418a.v().d("ConnectionManager", "Plaintext HTTP operation requested; upgrading to HTTPS due to universal SSL setting...");
                    a2 = a2.replace("http://", "https://");
                }
                boolean m = bVar.m();
                long a3 = p.a(this.f1418a);
                if ((bVar.c() != null && !bVar.c().isEmpty()) || bVar.i() >= 0) {
                    Map c2 = bVar.c();
                    if (c2 != null && bVar.i() >= 0) {
                        c2.put("current_retry_attempt", String.valueOf(bVar.i()));
                    }
                    if (m) {
                        a2 = m.a(a2, TtmlNode.TAG_P, l.a(p.a(c2), this.f1418a.t(), a3));
                    } else {
                        a2 = m.b(a2, c2);
                    }
                }
                String str2 = a2;
                ? currentTimeMillis = System.currentTimeMillis();
                try {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Sending ");
                    sb2.append(b2);
                    sb2.append(" request to \"");
                    sb2.append(str2);
                    sb2.append("\"...");
                    this.b.c("ConnectionManager", sb2.toString());
                    httpURLConnection = a(str2, b2, bVar.d(), bVar.k());
                    try {
                        if (bVar.e() != null) {
                            if (m) {
                                try {
                                    str = l.a(bVar.e().toString(), this.f1418a.t(), a3);
                                } catch (Throwable th4) {
                                    th = th4;
                                    r13 = 0;
                                    p.a((Closeable) r13, this.f1418a);
                                    p.a(httpURLConnection, this.f1418a);
                                    throw th;
                                }
                            } else {
                                str = bVar.e().toString();
                            }
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Request to \"");
                            sb3.append(str2);
                            sb3.append("\" is ");
                            sb3.append(str);
                            this.b.b("ConnectionManager", sb3.toString());
                            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setFixedLengthStreamingMode(str.getBytes(Charset.forName("UTF-8")).length);
                            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(httpURLConnection.getOutputStream(), UrlUtils.UTF8));
                            printWriter.print(str);
                            printWriter.close();
                        }
                    } catch (Throwable th5) {
                        th = th5;
                        HttpURLConnection httpURLConnection4 = httpURLConnection;
                        r13 = 0;
                        p.a((Closeable) r13, this.f1418a);
                        p.a(httpURLConnection, this.f1418a);
                        throw th;
                    }
                    try {
                        responseCode = httpURLConnection.getResponseCode();
                        if (responseCode > 0) {
                            if (((Boolean) this.f1418a.a(com.applovin.impl.sdk.b.c.eU)).booleanValue()) {
                                httpURLConnection3 = httpURLConnection;
                                i = responseCode;
                                try {
                                    a(b2, str2, responseCode, currentTimeMillis);
                                } catch (MalformedURLException e) {
                                    e = e;
                                    j3 = currentTimeMillis;
                                    th3 = e;
                                    r136 = 0;
                                    try {
                                        a(-901, str2);
                                        if (((Boolean) this.f1418a.a(com.applovin.impl.sdk.b.c.eU)).booleanValue()) {
                                        }
                                        if (bVar.g() == null) {
                                        }
                                        r135 = r136;
                                        a(b2, str2, i, j, th3);
                                        cVar2.a(-901);
                                        r135 = r136;
                                        r133 = r136;
                                        p.a((Closeable) r133, this.f1418a);
                                        p.a(httpURLConnection3, this.f1418a);
                                    } catch (Throwable th6) {
                                        th = th6;
                                        httpURLConnection = httpURLConnection3;
                                        r13 = r136;
                                        p.a((Closeable) r13, this.f1418a);
                                        p.a(httpURLConnection, this.f1418a);
                                        throw th;
                                    }
                                } catch (Throwable th7) {
                                }
                            } else {
                                httpURLConnection3 = httpURLConnection;
                                i = responseCode;
                            }
                            ? inputStream = httpURLConnection3.getInputStream();
                            try {
                                a(i, str2);
                                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                                if (((Boolean) this.f1418a.a(com.applovin.impl.sdk.b.c.eU)).booleanValue()) {
                                    String a4 = com.applovin.impl.sdk.utils.h.a((InputStream) inputStream, this.f1418a);
                                    if (bVar.h()) {
                                        if (aVar2 != null) {
                                            if (a4 != null) {
                                                j = currentTimeMillis;
                                                currentTimeMillis = (long) a4.length();
                                                aVar2.b(currentTimeMillis);
                                                if (bVar.n()) {
                                                    ? length = (long) a4.length();
                                                    b bVar2 = new b(bVar.a(), length, currentTimeMillis2);
                                                    this.c = bVar2;
                                                    currentTimeMillis = length;
                                                }
                                            } else {
                                                j = currentTimeMillis;
                                            }
                                            aVar2.a(currentTimeMillis2);
                                            currentTimeMillis = currentTimeMillis;
                                        } else {
                                            j = currentTimeMillis;
                                        }
                                        r137 = currentTimeMillis;
                                        ? r138 = inputStream;
                                        try {
                                            r137 = r138;
                                            a(a4, httpURLConnection3.getResponseCode(), str2, bVar.g(), m, cVar);
                                            r133 = r138;
                                        } catch (MalformedURLException e2) {
                                        }
                                    } else {
                                        long j4 = currentTimeMillis;
                                        r133 = inputStream;
                                        if (aVar2 != null) {
                                            aVar2.a(currentTimeMillis2);
                                        }
                                        cVar2.a(a4, i);
                                    }
                                    p.a((Closeable) r133, this.f1418a);
                                    p.a(httpURLConnection3, this.f1418a);
                                }
                                j = currentTimeMillis;
                                ? r139 = inputStream;
                                if (i < 200 || i >= 400) {
                                    a(b2, str2, i, j, null);
                                    cVar2.a(i);
                                    r133 = r139;
                                    p.a((Closeable) r133, this.f1418a);
                                    p.a(httpURLConnection3, this.f1418a);
                                }
                                if (aVar2 != null) {
                                    aVar2.a(currentTimeMillis2);
                                }
                                long j5 = currentTimeMillis2;
                                a(b2, str2, i, j);
                                String a5 = com.applovin.impl.sdk.utils.h.a((InputStream) r139, this.f1418a);
                                if (a5 != null) {
                                    this.b.a("ConnectionManager", a5);
                                    if (aVar2 != null) {
                                        aVar2.b((long) a5.length());
                                    }
                                    if (bVar.n()) {
                                        b bVar3 = new b(bVar.a(), (long) a5.length(), j5);
                                        this.c = bVar3;
                                    }
                                    if (m) {
                                        a5 = l.a(a5, this.f1418a.t());
                                    }
                                    try {
                                        r137 = r139;
                                        r135 = r139;
                                        cVar2.a(a(a5, (T) bVar.g()), i);
                                        r137 = r139;
                                        r135 = r139;
                                        r133 = r139;
                                    } catch (Throwable th8) {
                                    }
                                } else {
                                    cVar2.a(bVar.g(), i);
                                    r133 = r139;
                                }
                                p.a((Closeable) r133, this.f1418a);
                                p.a(httpURLConnection3, this.f1418a);
                            } catch (MalformedURLException e3) {
                                e = e3;
                                j = currentTimeMillis;
                                ? r1310 = inputStream;
                                th3 = e;
                                r136 = r1310;
                                a(-901, str2);
                                if (((Boolean) this.f1418a.a(com.applovin.impl.sdk.b.c.eU)).booleanValue()) {
                                }
                                if (bVar.g() == null) {
                                }
                                r135 = r136;
                                a(b2, str2, i, j, th3);
                                cVar2.a(-901);
                                r135 = r136;
                                r133 = r136;
                                p.a((Closeable) r133, this.f1418a);
                                p.a(httpURLConnection3, this.f1418a);
                            } catch (Throwable th9) {
                            }
                        } else {
                            httpURLConnection3 = httpURLConnection;
                            i = responseCode;
                            j3 = currentTimeMillis;
                            a(b2, str2, i, j3, null);
                            cVar2.a(i);
                            r133 = 0;
                            p.a((Closeable) r133, this.f1418a);
                            p.a(httpURLConnection3, this.f1418a);
                        }
                    } catch (MalformedURLException e4) {
                        e = e4;
                        httpURLConnection3 = httpURLConnection;
                        i = responseCode;
                        j3 = currentTimeMillis;
                        th3 = e;
                        r136 = 0;
                        a(-901, str2);
                        if (((Boolean) this.f1418a.a(com.applovin.impl.sdk.b.c.eU)).booleanValue()) {
                        }
                        if (bVar.g() == null) {
                        }
                        r135 = r136;
                        a(b2, str2, i, j, th3);
                        cVar2.a(-901);
                        r135 = r136;
                        r133 = r136;
                        p.a((Closeable) r133, this.f1418a);
                        p.a(httpURLConnection3, this.f1418a);
                    } catch (Throwable th10) {
                    }
                } catch (Throwable th11) {
                    th = th11;
                    httpURLConnection = null;
                    r13 = 0;
                    p.a((Closeable) r13, this.f1418a);
                    p.a(httpURLConnection, this.f1418a);
                    throw th;
                }
            }
        } else {
            throw new IllegalArgumentException("No request specified");
        }
    }
}
