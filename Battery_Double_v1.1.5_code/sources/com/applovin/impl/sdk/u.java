package com.applovin.impl.sdk;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;
import android.view.animation.Animation;
import com.applovin.impl.mediation.b.b;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;

public class u {

    /* renamed from: a reason: collision with root package name */
    private final i f1439a;
    private final o b;
    private final MaxAdView c;

    public u(MaxAdView maxAdView, i iVar) {
        this.f1439a = iVar;
        this.b = iVar.v();
        this.c = maxAdView;
    }

    public long a(b bVar) {
        long j;
        this.b.b("ViewabilityTracker", "Checking visibility...");
        if (!this.c.isShown()) {
            this.b.e("ViewabilityTracker", "View is hidden");
            j = 2;
        } else {
            j = 0;
        }
        if (this.c.getAlpha() < bVar.p()) {
            this.b.e("ViewabilityTracker", "View is transparent");
            j |= 4;
        }
        Animation animation = this.c.getAnimation();
        if (animation != null && animation.hasStarted() && !animation.hasEnded()) {
            this.b.e("ViewabilityTracker", "View is animating");
            j |= 8;
        }
        if (this.c.getParent() == null) {
            this.b.e("ViewabilityTracker", "No parent view found");
            j |= 16;
        }
        int pxToDp = AppLovinSdkUtils.pxToDp(this.c.getContext(), this.c.getWidth());
        if (pxToDp < bVar.n()) {
            StringBuilder sb = new StringBuilder();
            sb.append("View has width (");
            sb.append(pxToDp);
            sb.append(") below threshold");
            this.b.e("ViewabilityTracker", sb.toString());
            j |= 32;
        }
        int pxToDp2 = AppLovinSdkUtils.pxToDp(this.c.getContext(), this.c.getHeight());
        if (pxToDp2 < bVar.o()) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("View has height (");
            sb2.append(pxToDp2);
            sb2.append(") below threshold");
            this.b.e("ViewabilityTracker", sb2.toString());
            j |= 64;
        }
        Point a2 = g.a(this.c.getContext());
        Rect rect = new Rect(0, 0, a2.x, a2.y);
        int[] iArr = {-1, -1};
        this.c.getLocationOnScreen(iArr);
        Rect rect2 = new Rect(iArr[0], iArr[1], iArr[0] + this.c.getWidth(), iArr[1] + this.c.getHeight());
        if (!Rect.intersects(rect, rect2)) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Rect (");
            sb3.append(rect2);
            sb3.append(") outside of screen's bounds (");
            sb3.append(rect);
            sb3.append(")");
            this.b.e("ViewabilityTracker", sb3.toString());
            j |= 128;
        }
        Activity a3 = this.f1439a.aa().a();
        if (a3 != null && !p.a((View) this.c, a3)) {
            this.b.e("ViewabilityTracker", "View is not in top activity's view hierarchy");
            j |= 256;
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append("Returning flags: ");
        sb4.append(Long.toBinaryString(j));
        this.b.b("ViewabilityTracker", sb4.toString());
        return j;
    }
}
