package com.applovin.impl.sdk;

import com.applovin.impl.sdk.b.c;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class n {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final i f1414a;
    /* access modifiers changed from: private */
    public final AtomicBoolean b = new AtomicBoolean();
    private final List<a> c = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public long d;
    private final Object e = new Object();
    /* access modifiers changed from: private */
    public final AtomicBoolean f = new AtomicBoolean();
    /* access modifiers changed from: private */
    public long g;

    interface a {
        void h();

        void i();
    }

    n(i iVar) {
        this.f1414a = iVar;
    }

    public void a(a aVar) {
        this.c.add(aVar);
    }

    public void a(boolean z) {
        synchronized (this.e) {
            this.f.set(z);
            if (z) {
                this.g = System.currentTimeMillis();
                StringBuilder sb = new StringBuilder();
                sb.append("Setting fullscreen ad pending display: ");
                sb.append(this.g);
                this.f1414a.v().b("FullScreenAdTracker", sb.toString());
                final long longValue = ((Long) this.f1414a.a(c.cz)).longValue();
                if (longValue >= 0) {
                    AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                        public void run() {
                            if (n.this.a() && System.currentTimeMillis() - n.this.g >= longValue) {
                                n.this.f1414a.v().b("FullScreenAdTracker", "Resetting \"pending display\" state...");
                                n.this.f.set(false);
                            }
                        }
                    }, longValue);
                }
            } else {
                this.g = 0;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Setting fullscreen ad not pending display: ");
                sb2.append(System.currentTimeMillis());
                this.f1414a.v().b("FullScreenAdTracker", sb2.toString());
            }
        }
    }

    public boolean a() {
        return this.f.get();
    }

    public void b(a aVar) {
        this.c.remove(aVar);
    }

    public boolean b() {
        return this.b.get();
    }

    public void c() {
        if (this.b.compareAndSet(false, true)) {
            this.d = System.currentTimeMillis();
            StringBuilder sb = new StringBuilder();
            sb.append("Setting fullscreen ad displayed: ");
            sb.append(this.d);
            this.f1414a.v().b("FullScreenAdTracker", sb.toString());
            Iterator it = new ArrayList(this.c).iterator();
            while (it.hasNext()) {
                ((a) it.next()).h();
            }
            final long longValue = ((Long) this.f1414a.a(c.cA)).longValue();
            if (longValue >= 0) {
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        if (n.this.b.get() && System.currentTimeMillis() - n.this.d >= longValue) {
                            n.this.f1414a.v().b("FullScreenAdTracker", "Resetting \"display\" state...");
                            n.this.d();
                        }
                    }
                }, longValue);
            }
        }
    }

    public void d() {
        if (this.b.compareAndSet(true, false)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Setting fullscreen ad hidden: ");
            sb.append(System.currentTimeMillis());
            this.f1414a.v().b("FullScreenAdTracker", sb.toString());
            Iterator it = new ArrayList(this.c).iterator();
            while (it.hasNext()) {
                ((a) it.next()).i();
            }
        }
    }
}
