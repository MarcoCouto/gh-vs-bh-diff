package com.applovin.impl.sdk;

import android.content.Intent;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.j.a;
import com.applovin.impl.sdk.j.b;
import com.applovin.impl.sdk.j.d;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinEventParameters;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinEventTypes;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class EventServiceImpl implements AppLovinEventService {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final i f1275a;
    /* access modifiers changed from: private */
    public final Map<String, Object> b;
    private final AtomicBoolean c = new AtomicBoolean();

    public EventServiceImpl(i iVar) {
        this.f1275a = iVar;
        if (((Boolean) iVar.a(c.aU)).booleanValue()) {
            this.b = i.a((String) this.f1275a.b(e.p, "{}"), (Map<String, Object>) new HashMap<String,Object>(), this.f1275a);
            return;
        }
        this.b = new HashMap();
        iVar.a(e.p, "{}");
    }

    /* access modifiers changed from: private */
    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) this.f1275a.a(c.aK));
        sb.append("4.0/pix");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> a(k kVar, a aVar) {
        j O = this.f1275a.O();
        d b2 = O.b();
        b c2 = O.c();
        boolean contains = this.f1275a.b((c) c.aR).contains(kVar.a());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("event", contains ? m.d(kVar.a()) : "postinstall");
        hashMap.put(CampaignEx.JSON_KEY_ST_TS, Long.toString(kVar.c()));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, m.d(b2.c));
        hashMap.put("model", m.d(b2.f1410a));
        hashMap.put(CampaignEx.JSON_KEY_PACKAGE_NAME, m.d(c2.c));
        hashMap.put("installer_name", m.d(c2.d));
        hashMap.put("ia", Long.toString(c2.g));
        hashMap.put("api_did", this.f1275a.a(c.S));
        hashMap.put("brand", m.d(b2.d));
        hashMap.put("brand_name", m.d(b2.e));
        hashMap.put("hardware", m.d(b2.f));
        hashMap.put("revision", m.d(b2.g));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("os", m.d(b2.b));
        hashMap.put("orientation_lock", b2.l);
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, m.d(c2.b));
        hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, m.d(b2.i));
        hashMap.put("carrier", m.d(b2.j));
        hashMap.put("tz_offset", String.valueOf(b2.r));
        hashMap.put("aida", String.valueOf(b2.I));
        hashMap.put("adr", b2.t ? "1" : "0");
        hashMap.put(AvidVideoPlaybackListenerImpl.VOLUME, String.valueOf(b2.v));
        hashMap.put("sim", b2.x ? "1" : "0");
        hashMap.put("gy", String.valueOf(b2.y));
        hashMap.put("is_tablet", String.valueOf(b2.z));
        hashMap.put("tv", String.valueOf(b2.A));
        hashMap.put("vs", String.valueOf(b2.B));
        hashMap.put("lpm", String.valueOf(b2.C));
        hashMap.put("tg", c2.e);
        hashMap.put("fs", String.valueOf(b2.E));
        hashMap.put("fm", String.valueOf(b2.F.b));
        hashMap.put("tm", String.valueOf(b2.F.f1411a));
        hashMap.put("lmt", String.valueOf(b2.F.c));
        hashMap.put("lm", String.valueOf(b2.F.d));
        hashMap.put("adns", String.valueOf(b2.m));
        hashMap.put("adnsd", String.valueOf(b2.n));
        hashMap.put("xdpi", String.valueOf(b2.o));
        hashMap.put("ydpi", String.valueOf(b2.p));
        hashMap.put("screen_size_in", String.valueOf(b2.q));
        hashMap.put("debug", Boolean.toString(p.b(this.f1275a)));
        if (!((Boolean) this.f1275a.a(c.eJ)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1275a.t());
        }
        a(aVar, (Map<String, String>) hashMap);
        if (((Boolean) this.f1275a.a(c.dR)).booleanValue()) {
            p.a("cuid", this.f1275a.i(), (Map<String, String>) hashMap);
        }
        if (((Boolean) this.f1275a.a(c.dU)).booleanValue()) {
            hashMap.put("compass_random_token", this.f1275a.j());
        }
        if (((Boolean) this.f1275a.a(c.dW)).booleanValue()) {
            hashMap.put("applovin_random_token", this.f1275a.k());
        }
        Boolean bool = b2.G;
        if (bool != null) {
            hashMap.put("huc", bool.toString());
        }
        Boolean bool2 = b2.H;
        if (bool2 != null) {
            hashMap.put("aru", bool2.toString());
        }
        j.c cVar = b2.u;
        if (cVar != null) {
            hashMap.put("act", String.valueOf(cVar.f1409a));
            hashMap.put("acm", String.valueOf(cVar.b));
        }
        String str = b2.w;
        if (m.b(str)) {
            hashMap.put("ua", m.d(str));
        }
        String str2 = b2.D;
        if (!TextUtils.isEmpty(str2)) {
            hashMap.put("so", m.d(str2));
        }
        if (!contains) {
            hashMap.put("sub_event", m.d(kVar.a()));
        }
        hashMap.put("sc", m.d((String) this.f1275a.a(c.V)));
        hashMap.put("sc2", m.d((String) this.f1275a.a(c.W)));
        hashMap.put("server_installed_at", m.d((String) this.f1275a.a(c.X)));
        p.a("persisted_data", m.d((String) this.f1275a.a(e.x)), (Map<String, String>) hashMap);
        p.a("plugin_version", m.d((String) this.f1275a.a(c.dY)), (Map<String, String>) hashMap);
        p.a("mediation_provider", m.d(this.f1275a.n()), (Map<String, String>) hashMap);
        return hashMap;
    }

    private void a(com.applovin.impl.sdk.d.i.a aVar) {
        this.f1275a.K().a((com.applovin.impl.sdk.d.a) new com.applovin.impl.sdk.d.i(this.f1275a, aVar), r.a.ADVERTISING_INFO_COLLECTION);
    }

    private void a(a aVar, Map<String, String> map) {
        String str = aVar.b;
        if (m.b(str)) {
            map.put("idfa", str);
        }
        map.put("dnt", Boolean.toString(aVar.f1407a));
    }

    /* access modifiers changed from: private */
    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) this.f1275a.a(c.aL));
        sb.append("4.0/pix");
        return sb.toString();
    }

    private void c() {
        if (((Boolean) this.f1275a.a(c.aU)).booleanValue()) {
            this.f1275a.a(e.p, i.a(this.b, "{}", this.f1275a));
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, boolean z) {
        trackEvent(str, new HashMap(), null, z);
    }

    public Map<String, Object> getSuperProperties() {
        return new HashMap(this.b);
    }

    public void maybeTrackAppOpenEvent() {
        if (this.c.compareAndSet(false, true)) {
            this.f1275a.q().trackEvent("landing");
        }
    }

    public void setSuperProperty(Object obj, String str) {
        if (TextUtils.isEmpty(str)) {
            o.i("AppLovinEventService", "Super property key cannot be null or empty");
        } else if (obj == null) {
            this.b.remove(str);
            c();
        } else {
            List b2 = this.f1275a.b((c) c.aT);
            if (!p.a(obj, b2, this.f1275a)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to set super property '");
                sb.append(obj);
                sb.append("' for key '");
                sb.append(str);
                sb.append("' - valid super property types include: ");
                sb.append(b2);
                o.i("AppLovinEventService", sb.toString());
                return;
            }
            this.b.put(str, p.a(obj, this.f1275a));
            c();
        }
    }

    public String toString() {
        return "EventService{}";
    }

    public void trackCheckout(String str, Map<String, String> map) {
        HashMap hashMap = map != null ? new HashMap(map) : new HashMap(1);
        hashMap.put("transaction_id", str);
        trackEvent(AppLovinEventTypes.USER_COMPLETED_CHECKOUT, hashMap);
    }

    public void trackEvent(String str) {
        trackEvent(str, new HashMap());
    }

    public void trackEvent(String str, Map<String, String> map) {
        trackEvent(str, map, null, true);
    }

    public void trackEvent(String str, Map<String, String> map, Map<String, String> map2, boolean z) {
        if (((Boolean) this.f1275a.a(c.aS)).booleanValue()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Tracking event: \"");
            sb.append(str);
            sb.append("\" with parameters: ");
            sb.append(map);
            this.f1275a.v().b("AppLovinEventService", sb.toString());
            final String str2 = str;
            final Map<String, String> map3 = map;
            final boolean z2 = z;
            final Map<String, String> map4 = map2;
            AnonymousClass1 r3 = new com.applovin.impl.sdk.d.i.a() {
                public void a(a aVar) {
                    k kVar = new k(str2, map3, EventServiceImpl.this.b);
                    try {
                        if (z2) {
                            EventServiceImpl.this.f1275a.N().a(f.k().a(EventServiceImpl.this.a()).b(EventServiceImpl.this.b()).a((Map<String, String>) EventServiceImpl.this.a(kVar, aVar)).b(map4).c(kVar.b()).a(((Boolean) EventServiceImpl.this.f1275a.a(c.eJ)).booleanValue()).a());
                            return;
                        }
                        EventServiceImpl.this.f1275a.R().dispatchPostbackRequest(g.b(EventServiceImpl.this.f1275a).a(EventServiceImpl.this.a()).c(EventServiceImpl.this.b()).a((Map<String, String>) EventServiceImpl.this.a(kVar, aVar)).c(map4).a(i.a(kVar.b())).a(((Boolean) EventServiceImpl.this.f1275a.a(c.eJ)).booleanValue()).a(), null);
                    } catch (Throwable th) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unable to track event: ");
                        sb.append(kVar);
                        EventServiceImpl.this.f1275a.v().b("AppLovinEventService", sb.toString(), th);
                    }
                }
            };
            a((com.applovin.impl.sdk.d.i.a) r3);
        }
    }

    public void trackInAppPurchase(Intent intent, Map<String, String> map) {
        HashMap hashMap = map != null ? new HashMap(map) : new HashMap();
        try {
            hashMap.put(AppLovinEventParameters.IN_APP_PURCHASE_DATA, intent.getStringExtra("INAPP_PURCHASE_DATA"));
            hashMap.put(AppLovinEventParameters.IN_APP_DATA_SIGNATURE, intent.getStringExtra("INAPP_DATA_SIGNATURE"));
        } catch (Throwable th) {
            o.c("AppLovinEventService", "Unable to track in app purchase - invalid purchase intent", th);
        }
        trackEvent("iap", hashMap);
    }
}
