package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Intent;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.g.a;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserService.OnConsentDialogDismissListener;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.applovin.sdk.AppLovinWebViewActivity.EventListener;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public class h implements a, EventListener {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final AtomicBoolean f1394a = new AtomicBoolean();
    /* access modifiers changed from: private */
    public static WeakReference<AppLovinWebViewActivity> b;
    /* access modifiers changed from: private */
    public final i c;
    /* access modifiers changed from: private */
    public final o d;
    /* access modifiers changed from: private */
    public OnConsentDialogDismissListener e;
    /* access modifiers changed from: private */
    public g f;
    /* access modifiers changed from: private */
    public WeakReference<Activity> g = new WeakReference<>(null);
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.utils.a h;

    h(i iVar) {
        this.c = iVar;
        this.d = iVar.v();
        if (iVar.F() != null) {
            this.g = new WeakReference<>(iVar.F());
        }
        iVar.aa().a(new com.applovin.impl.sdk.utils.a() {
            public void onActivityStarted(Activity activity) {
                h.this.g = new WeakReference(activity);
            }
        });
        this.f = new g(this, iVar);
    }

    private void a(boolean z, long j) {
        f();
        if (z) {
            a(j);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(i iVar) {
        if (c()) {
            o.i("AppLovinSdk", "Consent dialog already showing");
            return false;
        } else if (!com.applovin.impl.sdk.utils.h.a(iVar.D())) {
            o.i("AppLovinSdk", "No internet available, skip showing of consent dialog");
            return false;
        } else if (!((Boolean) iVar.a(c.aj)).booleanValue()) {
            this.d.e("ConsentDialogManager", "Blocked publisher from showing consent dialog");
            return false;
        } else if (m.b((String) iVar.a(c.ak))) {
            return true;
        } else {
            this.d.e("ConsentDialogManager", "AdServer returned empty consent dialog URL");
            return false;
        }
    }

    private void f() {
        this.c.aa().b(this.h);
        if (c()) {
            AppLovinWebViewActivity appLovinWebViewActivity = (AppLovinWebViewActivity) b.get();
            b = null;
            if (appLovinWebViewActivity != null) {
                appLovinWebViewActivity.finish();
                if (this.e != null) {
                    this.e.onDismiss();
                    this.e = null;
                }
            }
        }
    }

    public void a() {
        if (this.g.get() != null) {
            final Activity activity = (Activity) this.g.get();
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    h.this.a(activity, (OnConsentDialogDismissListener) null);
                }
            }, ((Long) this.c.a(c.am)).longValue());
        }
    }

    public void a(final long j) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                h.this.d.b("ConsentDialogManager", "Scheduling repeating consent alert");
                h.this.f.a(j, h.this.c, h.this);
            }
        });
    }

    public void a(final Activity activity, final OnConsentDialogDismissListener onConsentDialogDismissListener) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (!h.this.a(h.this.c) || h.f1394a.getAndSet(true)) {
                    if (onConsentDialogDismissListener != null) {
                        onConsentDialogDismissListener.onDismiss();
                    }
                    return;
                }
                h.this.g = new WeakReference(activity);
                h.this.e = onConsentDialogDismissListener;
                h.this.h = new com.applovin.impl.sdk.utils.a() {
                    public void onActivityStarted(Activity activity) {
                        if (activity instanceof AppLovinWebViewActivity) {
                            if (!h.this.c() || h.b.get() != activity) {
                                AppLovinWebViewActivity appLovinWebViewActivity = (AppLovinWebViewActivity) activity;
                                h.b = new WeakReference(appLovinWebViewActivity);
                                appLovinWebViewActivity.loadUrl((String) h.this.c.a(c.ak), h.this);
                            }
                            h.f1394a.set(false);
                        }
                    }
                };
                h.this.c.aa().a(h.this.h);
                Intent intent = new Intent(activity, AppLovinWebViewActivity.class);
                intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, h.this.c.t());
                intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON, (Serializable) h.this.c.a(c.al));
                activity.startActivity(intent);
            }
        });
    }

    public void b() {
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return (b == null || b.get() == null) ? false : true;
    }

    public void onReceivedEvent(String str) {
        boolean booleanValue;
        i iVar;
        c<Long> cVar;
        if ("accepted".equalsIgnoreCase(str)) {
            AppLovinPrivacySettings.setHasUserConsent(true, this.c.D());
            f();
            return;
        }
        if ("rejected".equalsIgnoreCase(str)) {
            AppLovinPrivacySettings.setHasUserConsent(false, this.c.D());
            booleanValue = ((Boolean) this.c.a(c.an)).booleanValue();
            iVar = this.c;
            cVar = c.as;
        } else if ("closed".equalsIgnoreCase(str)) {
            booleanValue = ((Boolean) this.c.a(c.ao)).booleanValue();
            iVar = this.c;
            cVar = c.at;
        } else if (AppLovinWebViewActivity.EVENT_DISMISSED_VIA_BACK_BUTTON.equalsIgnoreCase(str)) {
            booleanValue = ((Boolean) this.c.a(c.ap)).booleanValue();
            iVar = this.c;
            cVar = c.au;
        } else {
            return;
        }
        a(booleanValue, ((Long) iVar.a(cVar)).longValue());
    }
}
