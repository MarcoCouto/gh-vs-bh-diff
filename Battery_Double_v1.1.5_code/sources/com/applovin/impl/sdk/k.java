package com.applovin.impl.sdk;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class k {

    /* renamed from: a reason: collision with root package name */
    private final String f1412a = UUID.randomUUID().toString();
    private final String b;
    private final Map<String, Object> c;
    private final long d;

    public k(String str, Map<String, String> map, Map<String, Object> map2) {
        this.b = str;
        this.c = new HashMap();
        this.c.putAll(map);
        this.c.put("applovin_sdk_super_properties", map2);
        this.d = System.currentTimeMillis();
    }

    public String a() {
        return this.b;
    }

    public Map<String, Object> b() {
        return this.c;
    }

    public long c() {
        return this.d;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        k kVar = (k) obj;
        if (this.d != kVar.d) {
            return false;
        }
        if (this.b == null ? kVar.b != null : !this.b.equals(kVar.b)) {
            return false;
        }
        if (this.c == null ? kVar.c != null : !this.c.equals(kVar.c)) {
            return false;
        }
        if (this.f1412a == null ? kVar.f1412a != null : !this.f1412a.equals(kVar.f1412a)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((this.b != null ? this.b.hashCode() : 0) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + ((int) (this.d ^ (this.d >>> 32)))) * 31;
        if (this.f1412a != null) {
            i = this.f1412a.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Event{name='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", id='");
        sb.append(this.f1412a);
        sb.append('\'');
        sb.append(", creationTimestampMillis=");
        sb.append(this.d);
        sb.append(", parameters=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}
