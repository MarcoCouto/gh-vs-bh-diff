package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.j;
import java.util.LinkedList;
import java.util.Queue;

class r {

    /* renamed from: a reason: collision with root package name */
    private int f1433a;
    private final Queue<j> b = new LinkedList();
    private final Object c = new Object();

    r(int i) {
        a(i);
    }

    /* access modifiers changed from: 0000 */
    public int a() {
        int size;
        synchronized (this.c) {
            size = this.b.size();
        }
        return size;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        if (i > 25) {
            i = 25;
        }
        this.f1433a = i;
    }

    /* access modifiers changed from: 0000 */
    public void a(j jVar) {
        synchronized (this.c) {
            if (a() <= 25) {
                this.b.offer(jVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int b() {
        return this.f1433a;
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        boolean z;
        synchronized (this.c) {
            z = a() >= this.f1433a;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        boolean z;
        synchronized (this.c) {
            z = a() == 0;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public j e() {
        j jVar;
        try {
            synchronized (this.c) {
                jVar = !d() ? (j) this.b.poll() : null;
            }
            return jVar;
        } catch (Exception unused) {
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public j f() {
        j jVar;
        synchronized (this.c) {
            jVar = (j) this.b.peek();
        }
        return jVar;
    }
}
