package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.utils.m;
import com.ironsource.sdk.constants.Constants.RequestParameters;

public class o {

    /* renamed from: a reason: collision with root package name */
    private final i f1429a;

    o(i iVar) {
        this.f1429a = iVar;
    }

    private boolean a() {
        return this.f1429a.C().d();
    }

    public static void c(String str, String str2, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.e("AppLovinSdk", sb.toString(), th);
    }

    public static void f(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.d("AppLovinSdk", sb.toString());
    }

    public static void g(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.i("AppLovinSdk", sb.toString());
    }

    public static void h(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.w("AppLovinSdk", sb.toString());
    }

    public static void i(String str, String str2) {
        c(str, str2, null);
    }

    private void j(String str, String str2) {
    }

    public void a(String str, Boolean bool, String str2) {
        a(str, bool, str2, null);
    }

    public void a(String str, Boolean bool, String str2, Throwable th) {
        if (a()) {
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            String sb2 = sb.toString();
            Log.e("AppLovinSdk", sb2, th);
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb2);
            sb3.append(" : ");
            sb3.append(th);
            j("ERROR", sb3.toString());
        }
        if (bool.booleanValue() && ((Boolean) this.f1429a.a(c.ew)).booleanValue() && this.f1429a.P() != null) {
            this.f1429a.P().a(str2, th);
        }
    }

    public void a(String str, String str2) {
        if (a() && m.b(str2)) {
            int intValue = ((Integer) this.f1429a.a(c.ag)).intValue();
            if (intValue > 0) {
                int length = str2.length();
                int i = ((length + intValue) - 1) / intValue;
                for (int i2 = 0; i2 < i; i2++) {
                    int i3 = i2 * intValue;
                    b(str, str2.substring(i3, Math.min(length, i3 + intValue)));
                }
            }
        }
    }

    public void a(String str, String str2, Throwable th) {
        if (a()) {
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            String sb2 = sb.toString();
            Log.w("AppLovinSdk", sb2, th);
            j("WARN", sb2);
        }
    }

    public void b(String str, String str2) {
        if (a()) {
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            String sb2 = sb.toString();
            Log.d("AppLovinSdk", sb2);
            j("DEBUG", sb2);
        }
    }

    public void b(String str, String str2, Throwable th) {
        a(str, Boolean.valueOf(true), str2, th);
    }

    public void c(String str, String str2) {
        if (a()) {
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            String sb2 = sb.toString();
            Log.i("AppLovinSdk", sb2);
            j("INFO", sb2);
        }
    }

    public void d(String str, String str2) {
        a(str, str2, (Throwable) null);
    }

    public void e(String str, String str2) {
        b(str, str2, null);
    }
}
