package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.m;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import java.util.List;

abstract class f extends a {

    /* renamed from: a reason: collision with root package name */
    protected final AppLovinNativeAdPrecacheListener f1354a;
    private final List<NativeAdImpl> c;
    private final AppLovinNativeAdLoadListener d;
    private int e;

    f(String str, List<NativeAdImpl> list, i iVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super(str, iVar);
        this.c = list;
        this.d = appLovinNativeAdLoadListener;
        this.f1354a = null;
    }

    f(String str, List<NativeAdImpl> list, i iVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        super(str, iVar);
        if (list != null) {
            this.c = list;
            this.d = null;
            this.f1354a = appLovinNativeAdPrecacheListener;
            return;
        }
        throw new IllegalArgumentException("Native ads cannot be null");
    }

    private void a(int i) {
        if (this.d != null) {
            this.d.onNativeAdsFailedToLoad(i);
        }
    }

    private void a(List<AppLovinNativeAd> list) {
        if (this.d != null) {
            this.d.onNativeAdsLoaded(list);
        }
    }

    /* access modifiers changed from: protected */
    public String a(String str, m mVar, List<String> list) {
        if (!com.applovin.impl.sdk.utils.m.b(str)) {
            a("Asked to cache file with null/empty URL, nothing to do.");
            return null;
        } else if (!p.a(str, list)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Domain is not whitelisted, skipping precache for URL ");
            sb.append(str);
            a(sb.toString());
            return null;
        } else {
            try {
                String a2 = mVar.a(g(), str, null, list, true, true, null);
                if (a2 != null) {
                    return a2;
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to cache icon resource ");
                sb2.append(str);
                c(sb2.toString());
                return null;
            } catch (Exception e2) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to cache icon resource ");
                sb3.append(str);
                a(sb3.toString(), e2);
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(NativeAdImpl nativeAdImpl);

    /* access modifiers changed from: protected */
    public abstract void a(NativeAdImpl nativeAdImpl, int i);

    /* access modifiers changed from: protected */
    public abstract boolean a(NativeAdImpl nativeAdImpl, m mVar);

    public void run() {
        List list;
        for (NativeAdImpl nativeAdImpl : this.c) {
            a("Beginning resource caching phase...");
            if (a(nativeAdImpl, this.b.V())) {
                this.e++;
                a(nativeAdImpl);
            } else {
                d("Unable to cache resources");
            }
        }
        try {
            if (this.e == this.c.size()) {
                list = this.c;
            } else if (((Boolean) this.b.a(c.f0do)).booleanValue()) {
                d("Mismatch between successful populations and requested size");
                a(-6);
                return;
            } else {
                list = this.c;
            }
            a(list);
        } catch (Throwable th) {
            o.c(f(), "Encountered exception while notifying publisher code", th);
        }
    }
}
