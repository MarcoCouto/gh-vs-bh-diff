package com.applovin.impl.sdk.d;

import android.net.Uri;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.ad.a;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.sdk.AppLovinAdLoadListener;

public class d extends c {
    private final a c;
    private boolean d;
    private boolean e;

    public d(a aVar, i iVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super("TaskCacheAppLovinAd", aVar, iVar, appLovinAdLoadListener);
        this.c = aVar;
    }

    /* access modifiers changed from: private */
    public void j() {
        boolean b = this.c.b();
        boolean z = this.e;
        if (b || z) {
            StringBuilder sb = new StringBuilder();
            sb.append("Begin caching for streaming ad #");
            sb.append(this.c.getAdIdNumber());
            sb.append("...");
            a(sb.toString());
            d();
            if (b) {
                if (this.d) {
                    i();
                }
                k();
                if (!this.d) {
                    i();
                }
                l();
            } else {
                i();
                k();
            }
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Begin processing for non-streaming ad #");
            sb2.append(this.c.getAdIdNumber());
            sb2.append("...");
            a(sb2.toString());
            d();
            k();
            l();
            i();
        }
        long currentTimeMillis = System.currentTimeMillis() - this.c.getCreatedAtMillis();
        com.applovin.impl.sdk.c.d.a(this.c, this.b);
        com.applovin.impl.sdk.c.d.a(currentTimeMillis, (AppLovinAdBase) this.c, this.b);
        a((AppLovinAdBase) this.c);
        b();
    }

    private void k() {
        a("Caching HTML resources...");
        this.c.a(a(this.c.a(), this.c.F(), (f) this.c));
        this.c.a(true);
        StringBuilder sb = new StringBuilder();
        sb.append("Finish caching non-video resources for ad #");
        sb.append(this.c.getAdIdNumber());
        a(sb.toString());
        o v = this.b.v();
        String f = f();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Ad updated with cachedHTML = ");
        sb2.append(this.c.a());
        v.a(f, sb2.toString());
    }

    private void l() {
        if (!c()) {
            Uri e2 = e(this.c.e());
            if (e2 != null) {
                this.c.c();
                this.c.a(e2);
            }
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.i;
    }

    public /* bridge */ /* synthetic */ void a(com.applovin.impl.mediation.b.a aVar) {
        super.a(aVar);
    }

    public void a(boolean z) {
        this.d = z;
    }

    public void b(boolean z) {
        this.e = z;
    }

    public void run() {
        super.run();
        AnonymousClass1 r0 = new Runnable() {
            public void run() {
                d.this.j();
            }
        };
        if (this.f1351a.I()) {
            this.b.K().c().execute(r0);
        } else {
            r0.run();
        }
    }
}
