package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.a;
import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import org.json.JSONObject;

class u extends a {

    /* renamed from: a reason: collision with root package name */
    private final JSONObject f1381a;
    private final JSONObject c;
    private final AppLovinAdLoadListener d;
    private final b e;

    u(JSONObject jSONObject, JSONObject jSONObject2, b bVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super("TaskRenderAppLovinAd", iVar);
        this.f1381a = jSONObject;
        this.c = jSONObject2;
        this.e = bVar;
        this.d = appLovinAdLoadListener;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.u;
    }

    public void run() {
        a("Rendering ad...");
        a aVar = new a(this.f1381a, this.c, this.e, this.b);
        boolean booleanValue = com.applovin.impl.sdk.utils.i.a(this.f1381a, "gs_load_immediately", Boolean.valueOf(false), this.b).booleanValue();
        boolean booleanValue2 = com.applovin.impl.sdk.utils.i.a(this.f1381a, "vs_load_immediately", Boolean.valueOf(true), this.b).booleanValue();
        d dVar = new d(aVar, this.b, this.d);
        dVar.a(booleanValue2);
        dVar.b(booleanValue);
        r.a aVar2 = r.a.CACHING_OTHER;
        if (((Boolean) this.b.a(c.bi)).booleanValue()) {
            if (aVar.getSize() == AppLovinAdSize.INTERSTITIAL && aVar.getType() == AppLovinAdType.REGULAR) {
                aVar2 = r.a.CACHING_INTERSTITIAL;
            } else if (aVar.getSize() == AppLovinAdSize.INTERSTITIAL && aVar.getType() == AppLovinAdType.INCENTIVIZED) {
                aVar2 = r.a.CACHING_INCENTIVIZED;
            }
        }
        this.b.K().a((a) dVar, aVar2);
    }
}
