package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import java.util.Map;
import org.json.JSONObject;

public class n extends m {

    /* renamed from: a reason: collision with root package name */
    private final int f1365a;
    private final AppLovinNativeAdLoadListener c;

    public n(String str, int i, i iVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super(d.b(str, iVar), null, "TaskFetchNextNativeAd", iVar);
        this.f1365a = i;
        this.c = appLovinNativeAdLoadListener;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.o;
    }

    /* access modifiers changed from: protected */
    public a a(JSONObject jSONObject) {
        return new v(jSONObject, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (this.c != null) {
            this.c.onNativeAdsFailedToLoad(i);
        }
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> b() {
        Map<String, String> b = super.b();
        b.put("slot_count", Integer.toString(this.f1365a));
        return b;
    }

    /* access modifiers changed from: protected */
    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) this.b.a(c.aG));
        sb.append("4.0/nad");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String i() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) this.b.a(c.aH));
        sb.append("4.0/nad");
        return sb.toString();
    }
}
