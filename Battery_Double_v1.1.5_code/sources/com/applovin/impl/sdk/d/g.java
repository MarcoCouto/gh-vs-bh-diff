package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.m;
import com.applovin.impl.sdk.utils.h;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.List;

public class g extends f {
    public g(List<NativeAdImpl> list, i iVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super("TaskCacheNativeAdVideos", list, iVar, appLovinNativeAdLoadListener);
    }

    public g(List<NativeAdImpl> list, i iVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        super("TaskCacheNativeAdVideos", list, iVar, appLovinNativeAdPrecacheListener);
    }

    private boolean b(NativeAdImpl nativeAdImpl) {
        StringBuilder sb = new StringBuilder();
        sb.append("Unable to cache video resource ");
        sb.append(nativeAdImpl.getSourceVideoUrl());
        c(sb.toString());
        a(nativeAdImpl, !h.a(g()) ? AppLovinErrorCodes.NO_NETWORK : AppLovinErrorCodes.UNABLE_TO_PRECACHE_VIDEO_RESOURCES);
        return false;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.k;
    }

    /* access modifiers changed from: protected */
    public void a(NativeAdImpl nativeAdImpl) {
        if (this.f1354a != null) {
            this.f1354a.onNativeAdVideoPreceached(nativeAdImpl);
        }
    }

    /* access modifiers changed from: protected */
    public void a(NativeAdImpl nativeAdImpl, int i) {
        if (this.f1354a != null) {
            this.f1354a.onNativeAdVideoPrecachingFailed(nativeAdImpl, i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(NativeAdImpl nativeAdImpl, m mVar) {
        if (!com.applovin.impl.sdk.utils.m.b(nativeAdImpl.getSourceVideoUrl())) {
            return true;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Beginning native ad video caching");
        sb.append(nativeAdImpl.getAdId());
        a(sb.toString());
        if (((Boolean) this.b.a(c.bH)).booleanValue()) {
            String a2 = a(nativeAdImpl.getSourceVideoUrl(), mVar, nativeAdImpl.getResourcePrefixes());
            if (a2 == null) {
                return b(nativeAdImpl);
            }
            nativeAdImpl.setVideoUrl(a2);
        } else {
            a("Resource caching is disabled, skipping...");
        }
        return true;
    }

    public /* bridge */ /* synthetic */ void run() {
        super.run();
    }
}
