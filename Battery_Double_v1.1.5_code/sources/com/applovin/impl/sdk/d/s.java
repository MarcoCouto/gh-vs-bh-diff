package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.mintegral.msdk.base.entity.CampaignUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class s extends a implements AppLovinAdLoadListener {

    /* renamed from: a reason: collision with root package name */
    private final JSONObject f1377a;
    private final d c;
    private final b d;
    private final AppLovinAdLoadListener e;

    public s(JSONObject jSONObject, d dVar, b bVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super("TaskProcessAdResponse", iVar);
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (dVar != null) {
            this.f1377a = jSONObject;
            this.c = dVar;
            this.d = bVar;
            this.e = appLovinAdLoadListener;
        } else {
            throw new IllegalArgumentException("No zone specified");
        }
    }

    private void a(int i) {
        p.a(this.e, this.c, i, this.b);
    }

    private void a(AppLovinAd appLovinAd) {
        try {
            if (this.e != null) {
                this.e.adReceived(appLovinAd);
            }
        } catch (Throwable th) {
            a("Unable process a ad received notification", th);
        }
    }

    private void a(JSONObject jSONObject) {
        String b = com.applovin.impl.sdk.utils.i.b(jSONObject, "type", "undefined", this.b);
        if ("applovin".equalsIgnoreCase(b)) {
            a("Starting task for AppLovin ad...");
            r K = this.b.K();
            u uVar = new u(jSONObject, this.f1377a, this.d, this, this.b);
            K.a((a) uVar);
        } else if ("vast".equalsIgnoreCase(b)) {
            a("Starting task for VAST ad...");
            this.b.K().a((a) t.a(jSONObject, this.f1377a, this.d, this, this.b));
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to process ad of unknown type: ");
            sb.append(b);
            c(sb.toString());
            failedToReceiveAd(-800);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.r;
    }

    public void adReceived(AppLovinAd appLovinAd) {
        a(appLovinAd);
    }

    public void failedToReceiveAd(int i) {
        a(i);
    }

    public void run() {
        JSONArray b = com.applovin.impl.sdk.utils.i.b(this.f1377a, CampaignUnit.JSON_KEY_ADS, new JSONArray(), this.b);
        if (b.length() > 0) {
            a("Processing ad...");
            a(com.applovin.impl.sdk.utils.i.a(b, 0, new JSONObject(), this.b));
            return;
        }
        c("No ads were returned from the server");
        p.a(this.c.a(), this.f1377a, this.b);
        a(204);
    }
}
