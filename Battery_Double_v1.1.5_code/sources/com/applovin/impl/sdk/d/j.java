package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.d.r.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class j extends a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final g f1357a;
    /* access modifiers changed from: private */
    public final AppLovinPostbackListener c;
    private final a d;

    public j(g gVar, a aVar, i iVar, AppLovinPostbackListener appLovinPostbackListener) {
        super("TaskDispatchPostback", iVar);
        if (gVar != null) {
            this.f1357a = gVar;
            this.c = appLovinPostbackListener;
            this.d = aVar;
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.c;
    }

    public void run() {
        final String a2 = this.f1357a.a();
        if (!m.b(a2)) {
            b("Requested URL is not valid; nothing to do...");
            if (this.c != null) {
                this.c.onPostbackFailure(a2, AppLovinErrorCodes.INVALID_URL);
            }
            return;
        }
        AnonymousClass1 r1 = new x<Object>(this.f1357a, e()) {
            public void a(int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to dispatch postback. Error code: ");
                sb.append(i);
                sb.append(" URL: ");
                sb.append(a2);
                d(sb.toString());
                if (j.this.c != null) {
                    j.this.c.onPostbackFailure(a2, i);
                }
            }

            public void a(Object obj, int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Successfully dispatched postback to URL: ");
                sb.append(a2);
                a(sb.toString());
                if (((Boolean) this.b.a(c.eU)).booleanValue()) {
                    if (obj != null && (obj instanceof JSONObject)) {
                        JSONObject jSONObject = (JSONObject) obj;
                        Iterator it = this.b.b((c) c.aD).iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            if (j.this.f1357a.a().startsWith((String) it.next())) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("Updating settings from: ");
                                sb2.append(j.this.f1357a.a());
                                a(sb2.toString());
                                h.d(jSONObject, this.b);
                                h.c(jSONObject, this.b);
                                break;
                            }
                        }
                    }
                } else if (obj != null && (obj instanceof String)) {
                    for (String startsWith : this.b.b((c) c.aD)) {
                        if (j.this.f1357a.a().startsWith(startsWith)) {
                            String str = (String) obj;
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Updating settings from: ");
                                    sb3.append(j.this.f1357a.a());
                                    a(sb3.toString());
                                    JSONObject jSONObject2 = new JSONObject(str);
                                    h.d(jSONObject2, this.b);
                                    h.c(jSONObject2, this.b);
                                    break;
                                } catch (JSONException unused) {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
                if (j.this.c != null) {
                    j.this.c.onPostbackSuccess(a2);
                }
            }
        };
        r1.a(this.d);
        e().K().a((a) r1);
    }
}
