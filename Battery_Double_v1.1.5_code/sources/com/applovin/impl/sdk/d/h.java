package com.applovin.impl.sdk.d;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.a.a;
import com.applovin.impl.a.a.b;
import com.applovin.impl.a.e;
import com.applovin.impl.a.k;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.Collections;

class h extends c {
    private final a c;

    public h(a aVar, i iVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super("TaskCacheVastAd", aVar, iVar, appLovinAdLoadListener);
        this.c = aVar;
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.c.a()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Begin caching for VAST streaming ad #");
            sb.append(this.f1351a.getAdIdNumber());
            sb.append("...");
            a(sb.toString());
            d();
            if (this.c.e()) {
                i();
            }
            if (this.c.c() == b.COMPANION_AD) {
                k();
                m();
            } else {
                l();
            }
            if (!this.c.e()) {
                i();
            }
            if (this.c.c() == b.COMPANION_AD) {
                l();
            } else {
                k();
                m();
            }
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Begin caching for VAST ad #");
            sb2.append(this.f1351a.getAdIdNumber());
            sb2.append("...");
            a(sb2.toString());
            d();
            k();
            l();
            m();
            i();
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Finished caching VAST ad #");
        sb3.append(this.c.getAdIdNumber());
        a(sb3.toString());
        long currentTimeMillis = System.currentTimeMillis() - this.c.getCreatedAtMillis();
        d.a(this.c, this.b);
        d.a(currentTimeMillis, (AppLovinAdBase) this.c, this.b);
        a((AppLovinAdBase) this.c);
        b();
    }

    private void k() {
        String str;
        String str2;
        String sb;
        if (!c()) {
            if (this.c.aF()) {
                com.applovin.impl.a.b j = this.c.j();
                if (j != null) {
                    e b = j.b();
                    if (b != null) {
                        Uri b2 = b.b();
                        String uri = b2 != null ? b2.toString() : "";
                        String c2 = b.c();
                        if (URLUtil.isValidUrl(uri) || m.b(c2)) {
                            if (b.a() == e.a.STATIC) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("Caching static companion ad at ");
                                sb2.append(uri);
                                sb2.append("...");
                                a(sb2.toString());
                                Uri b3 = b(uri, Collections.emptyList(), false);
                                if (b3 != null) {
                                    b.a(b3);
                                } else {
                                    str2 = "Failed to cache static companion ad";
                                }
                            } else if (b.a() == e.a.HTML) {
                                if (m.b(uri)) {
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Begin caching HTML companion ad. Fetching from ");
                                    sb3.append(uri);
                                    sb3.append("...");
                                    a(sb3.toString());
                                    c2 = f(uri);
                                    if (m.b(c2)) {
                                        sb = "HTML fetched. Caching HTML now...";
                                    } else {
                                        StringBuilder sb4 = new StringBuilder();
                                        sb4.append("Unable to load companion ad resources from ");
                                        sb4.append(uri);
                                        str2 = sb4.toString();
                                    }
                                } else {
                                    StringBuilder sb5 = new StringBuilder();
                                    sb5.append("Caching provided HTML for companion ad. No fetch required. HTML: ");
                                    sb5.append(c2);
                                    sb = sb5.toString();
                                }
                                a(sb);
                                b.a(a(c2, Collections.emptyList(), (f) this.c));
                            } else {
                                if (b.a() == e.a.IFRAME) {
                                    str = "Skip caching of iFrame resource...";
                                }
                            }
                            this.c.a(true);
                        }
                        c("Companion ad does not have any resources attached. Skipping...");
                    }
                    str2 = "Failed to retrieve non-video resources from companion ad. Skipping...";
                    d(str2);
                }
                str = "No companion ad provided. Skipping...";
            } else {
                str = "Companion ad caching disabled. Skipping...";
            }
            a(str);
        }
    }

    private void l() {
        if (!c()) {
            if (!this.c.aG()) {
                a("Video caching disabled. Skipping...");
            } else if (this.c.h() != null) {
                k i = this.c.i();
                if (i != null) {
                    Uri b = i.b();
                    if (b != null) {
                        Uri a2 = a(b.toString(), Collections.emptyList(), false);
                        if (a2 != null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Video file successfully cached into: ");
                            sb.append(a2);
                            a(sb.toString());
                            i.a(a2);
                        } else {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Failed to cache video file: ");
                            sb2.append(i);
                            d(sb2.toString());
                        }
                    }
                }
            }
        }
    }

    private void m() {
        String str;
        String str2;
        if (!c()) {
            if (this.c.aE() != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Begin caching HTML template. Fetching from ");
                sb.append(this.c.aE());
                sb.append("...");
                a(sb.toString());
                str = a(this.c.aE().toString(), this.c.F());
            } else {
                str = this.c.aD();
            }
            if (m.b(str)) {
                this.c.a(a(str, this.c.F(), (f) this.c));
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Finish caching HTML template ");
                sb2.append(this.c.aD());
                sb2.append(" for ad #");
                sb2.append(this.c.getAdIdNumber());
                str2 = sb2.toString();
            } else {
                str2 = "Unable to load HTML template";
            }
            a(str2);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.l;
    }

    public void run() {
        super.run();
        AnonymousClass1 r0 = new Runnable() {
            public void run() {
                h.this.j();
            }
        };
        if (this.f1351a.I()) {
            this.b.K().c().execute(r0);
        } else {
            r0.run();
        }
    }
}
