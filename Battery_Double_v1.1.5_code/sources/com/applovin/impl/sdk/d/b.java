package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.j.a;
import com.applovin.impl.sdk.j.d;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.internal.NativeProtocol;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b extends a {
    b(i iVar) {
        super("TaskApiSubmitData", iVar);
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        try {
            this.b.P().c();
            JSONObject a2 = h.a(jSONObject);
            this.b.C().a(c.S, (Object) a2.getString("device_id"));
            this.b.C().a(c.T, (Object) a2.getString("device_token"));
            this.b.C().a();
            h.d(a2, this.b);
            h.e(a2, this.b);
            String b = com.applovin.impl.sdk.utils.i.b(a2, "latest_version", "", this.b);
            if (e(b)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Current SDK version (");
                sb.append(AppLovinSdk.VERSION);
                sb.append(") is outdated. Please integrate the latest version of the AppLovin SDK (");
                sb.append(b);
                sb.append("). Doing so will improve your CPMs and ensure you have access to the latest revenue earning features.");
                String sb2 = sb.toString();
                if (com.applovin.impl.sdk.utils.i.a(a2, "sdk_update_message")) {
                    sb2 = com.applovin.impl.sdk.utils.i.b(a2, "sdk_update_message", sb2, this.b);
                }
                o.h("AppLovinSdk", sb2);
            }
            this.b.L().b();
            this.b.M().b();
        } catch (Throwable th) {
            a("Unable to parse API response", th);
        }
    }

    private void b(JSONObject jSONObject) throws JSONException {
        j O = this.b.O();
        com.applovin.impl.sdk.j.b c = O.c();
        d b = O.b();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("model", b.f1410a);
        jSONObject2.put("os", b.b);
        jSONObject2.put("brand", b.d);
        jSONObject2.put("brand_name", b.e);
        jSONObject2.put("hardware", b.f);
        jSONObject2.put("sdk_version", b.h);
        jSONObject2.put("revision", b.g);
        jSONObject2.put("adns", (double) b.m);
        jSONObject2.put("adnsd", b.n);
        jSONObject2.put("xdpi", String.valueOf(b.o));
        jSONObject2.put("ydpi", String.valueOf(b.p));
        jSONObject2.put("screen_size_in", String.valueOf(b.q));
        jSONObject2.put("gy", m.a(b.y));
        jSONObject2.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, b.i);
        jSONObject2.put("carrier", b.j);
        jSONObject2.put("orientation_lock", b.l);
        jSONObject2.put("tz_offset", b.r);
        jSONObject2.put("aida", String.valueOf(b.I));
        jSONObject2.put("adr", m.a(b.t));
        jSONObject2.put("wvvc", b.s);
        jSONObject2.put(AvidVideoPlaybackListenerImpl.VOLUME, b.v);
        jSONObject2.put("type", "android");
        jSONObject2.put("sim", m.a(b.x));
        jSONObject2.put("is_tablet", m.a(b.z));
        jSONObject2.put("lpm", b.C);
        jSONObject2.put("tv", m.a(b.A));
        jSONObject2.put("vs", m.a(b.B));
        jSONObject2.put("fs", b.E);
        jSONObject2.put("fm", String.valueOf(b.F.b));
        jSONObject2.put("tm", String.valueOf(b.F.f1411a));
        jSONObject2.put("lmt", String.valueOf(b.F.c));
        jSONObject2.put("lm", String.valueOf(b.F.d));
        g(jSONObject2);
        Boolean bool = b.G;
        if (bool != null) {
            jSONObject2.put("huc", bool.toString());
        }
        Boolean bool2 = b.H;
        if (bool2 != null) {
            jSONObject2.put("aru", bool2.toString());
        }
        j.c cVar = b.u;
        if (cVar != null) {
            jSONObject2.put("act", cVar.f1409a);
            jSONObject2.put("acm", cVar.b);
        }
        String str = b.w;
        if (m.b(str)) {
            jSONObject2.put("ua", m.d(str));
        }
        String str2 = b.D;
        if (!TextUtils.isEmpty(str2)) {
            jSONObject2.put("so", m.d(str2));
        }
        Locale locale = b.k;
        if (locale != null) {
            jSONObject2.put("locale", m.d(locale.toString()));
        }
        jSONObject.put(DeviceRequestsHelper.DEVICE_INFO_PARAM, jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put(CampaignEx.JSON_KEY_PACKAGE_NAME, c.c);
        jSONObject3.put("installer_name", c.d);
        jSONObject3.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, c.f1408a);
        jSONObject3.put(TapjoyConstants.TJC_APP_VERSION_NAME, c.b);
        jSONObject3.put("installed_at", c.g);
        jSONObject3.put("tg", c.e);
        jSONObject3.put("applovin_sdk_version", AppLovinSdk.VERSION);
        jSONObject3.put("first_install", String.valueOf(this.b.H()));
        jSONObject3.put("first_install_v2", String.valueOf(!this.b.I()));
        jSONObject3.put("debug", Boolean.toString(p.b(this.b)));
        String str3 = (String) this.b.a(c.dY);
        if (m.b(str3)) {
            jSONObject3.put("plugin_version", str3);
        }
        if (((Boolean) this.b.a(c.dR)).booleanValue() && m.b(this.b.i())) {
            jSONObject3.put("cuid", this.b.i());
        }
        if (((Boolean) this.b.a(c.dU)).booleanValue()) {
            jSONObject3.put("compass_random_token", this.b.j());
        }
        if (((Boolean) this.b.a(c.dW)).booleanValue()) {
            jSONObject3.put("applovin_random_token", this.b.k());
        }
        jSONObject.put("app_info", jSONObject3);
    }

    private void c(JSONObject jSONObject) throws JSONException {
        if (((Boolean) this.b.a(c.ep)).booleanValue()) {
            jSONObject.put("stats", this.b.L().c());
        }
        if (((Boolean) this.b.a(c.ab)).booleanValue()) {
            JSONObject b = com.applovin.impl.sdk.network.d.b(g());
            if (b.length() > 0) {
                jSONObject.put("network_response_codes", b);
            }
            if (((Boolean) this.b.a(c.ac)).booleanValue()) {
                com.applovin.impl.sdk.network.d.a(g());
            }
        }
    }

    private void d(JSONObject jSONObject) throws JSONException {
        if (((Boolean) this.b.a(c.ew)).booleanValue()) {
            JSONArray a2 = this.b.P().a();
            if (a2 != null && a2.length() > 0) {
                jSONObject.put(ReportColumns.COLUMN_ERRORS, a2);
            }
        }
    }

    private void e(JSONObject jSONObject) throws JSONException {
        if (((Boolean) this.b.a(c.ev)).booleanValue()) {
            JSONArray a2 = this.b.M().a();
            if (a2 != null && a2.length() > 0) {
                jSONObject.put("tasks", a2);
            }
        }
    }

    private boolean e(String str) {
        return m.b(str) && !AppLovinSdk.VERSION.equals(str) && p.g(str) > AppLovinSdk.VERSION_CODE;
    }

    private void f(JSONObject jSONObject) {
        AnonymousClass1 r0 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(h.a("2.0/device", this.b)).c(h.b("2.0/device", this.b)).a(h.e(this.b)).b(HttpRequest.METHOD_POST).a(jSONObject).a(new JSONObject()).a(((Integer) this.b.a(c.dA)).intValue()).a(), this.b) {
            public void a(int i) {
                h.a(i, this.b);
            }

            public void a(JSONObject jSONObject, int i) {
                b.this.a(jSONObject);
            }
        };
        r0.a(c.aI);
        r0.b(c.aJ);
        this.b.K().a((a) r0);
    }

    private void g(JSONObject jSONObject) {
        try {
            a d = this.b.O().d();
            String str = d.b;
            if (m.b(str)) {
                jSONObject.put("idfa", str);
            }
            jSONObject.put("dnt", Boolean.toString(d.f1407a));
        } catch (Throwable th) {
            a("Failed to populate advertising info", th);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.h;
    }

    public void run() {
        try {
            b("Submitting user data...");
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            c(jSONObject);
            d(jSONObject);
            e(jSONObject);
            f(jSONObject);
        } catch (JSONException e) {
            a("Unable to build JSON message with collected data", e);
            this.b.M().a(a());
        }
    }
}
