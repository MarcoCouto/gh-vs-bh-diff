package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.utils.h;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class ae extends ab {
    protected ae(String str, i iVar) {
        super(str, iVar);
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        if (!c()) {
            c c = c(jSONObject);
            if (c != null) {
                a(c);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r3 = java.util.Collections.emptyMap();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001b */
    private c c(JSONObject jSONObject) {
        String str;
        try {
            JSONObject a2 = h.a(jSONObject);
            h.d(a2, this.b);
            h.c(jSONObject, this.b);
            Map map = com.applovin.impl.sdk.utils.i.a((JSONObject) a2.get("params"));
            try {
                str = a2.getString(IronSourceConstants.EVENTS_RESULT);
            } catch (Throwable unused) {
                str = "network_timeout";
            }
            return c.a(str, map);
        } catch (JSONException e) {
            a("Unable to parse API response", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    /* access modifiers changed from: protected */
    public abstract void a(c cVar);

    /* access modifiers changed from: protected */
    public abstract boolean c();

    public void run() {
        a(i(), new a.c<JSONObject>() {
            public void a(int i) {
                ae.this.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                ae.this.b(jSONObject);
            }
        });
    }
}
