package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.j.b;
import com.applovin.impl.sdk.j.d;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class p extends a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final a f1367a;

    public interface a {
        void a();
    }

    public p(i iVar, a aVar) {
        super("TaskFetchVariables", iVar);
        this.f1367a = aVar;
    }

    private void a(Map<String, String> map) {
        try {
            com.applovin.impl.sdk.j.a d = this.b.O().d();
            String str = d.b;
            if (m.b(str)) {
                map.put("idfa", str);
            }
            map.put("dnt", Boolean.toString(d.f1407a));
        } catch (Throwable th) {
            a("Failed to populate advertising info", th);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.q;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> b() {
        j O = this.b.O();
        d b = O.b();
        b c = O.c();
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_PLATFORM, m.d(b.c));
        hashMap.put("model", m.d(b.f1410a));
        hashMap.put(CampaignEx.JSON_KEY_PACKAGE_NAME, m.d(c.c));
        hashMap.put("installer_name", m.d(c.d));
        hashMap.put("ia", Long.toString(c.g));
        hashMap.put("api_did", this.b.a(c.S));
        hashMap.put("brand", m.d(b.d));
        hashMap.put("brand_name", m.d(b.e));
        hashMap.put("hardware", m.d(b.f));
        hashMap.put("revision", m.d(b.g));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("os", m.d(b.b));
        hashMap.put("orientation_lock", b.l);
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, m.d(c.b));
        hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, m.d(b.i));
        hashMap.put("carrier", m.d(b.j));
        hashMap.put("tz_offset", String.valueOf(b.r));
        hashMap.put("aida", String.valueOf(b.I));
        hashMap.put("adr", b.t ? "1" : "0");
        hashMap.put(AvidVideoPlaybackListenerImpl.VOLUME, String.valueOf(b.v));
        hashMap.put("sim", b.x ? "1" : "0");
        hashMap.put("gy", String.valueOf(b.y));
        hashMap.put("is_tablet", String.valueOf(b.z));
        hashMap.put("tv", String.valueOf(b.A));
        hashMap.put("vs", String.valueOf(b.B));
        hashMap.put("lpm", String.valueOf(b.C));
        hashMap.put("tg", c.e);
        hashMap.put("fs", String.valueOf(b.E));
        hashMap.put("fm", String.valueOf(b.F.b));
        hashMap.put("tm", String.valueOf(b.F.f1411a));
        hashMap.put("lmt", String.valueOf(b.F.c));
        hashMap.put("lm", String.valueOf(b.F.d));
        hashMap.put("adns", String.valueOf(b.m));
        hashMap.put("adnsd", String.valueOf(b.n));
        hashMap.put("xdpi", String.valueOf(b.o));
        hashMap.put("ydpi", String.valueOf(b.p));
        hashMap.put("screen_size_in", String.valueOf(b.q));
        hashMap.put("debug", Boolean.toString(com.applovin.impl.sdk.utils.p.b(this.b)));
        if (!((Boolean) this.b.a(c.eJ)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.t());
        }
        a((Map<String, String>) hashMap);
        if (((Boolean) this.b.a(c.dR)).booleanValue()) {
            com.applovin.impl.sdk.utils.p.a("cuid", this.b.i(), (Map<String, String>) hashMap);
        }
        if (((Boolean) this.b.a(c.dU)).booleanValue()) {
            hashMap.put("compass_random_token", this.b.j());
        }
        if (((Boolean) this.b.a(c.dW)).booleanValue()) {
            hashMap.put("applovin_random_token", this.b.k());
        }
        Boolean bool = b.G;
        if (bool != null) {
            hashMap.put("huc", bool.toString());
        }
        Boolean bool2 = b.H;
        if (bool2 != null) {
            hashMap.put("aru", bool2.toString());
        }
        j.c cVar = b.u;
        if (cVar != null) {
            hashMap.put("act", String.valueOf(cVar.f1409a));
            hashMap.put("acm", String.valueOf(cVar.b));
        }
        String str = b.w;
        if (m.b(str)) {
            hashMap.put("ua", m.d(str));
        }
        String str2 = b.D;
        if (!TextUtils.isEmpty(str2)) {
            hashMap.put("so", m.d(str2));
        }
        hashMap.put("sc", m.d((String) this.b.a(c.V)));
        hashMap.put("sc2", m.d((String) this.b.a(c.W)));
        hashMap.put("server_installed_at", m.d((String) this.b.a(c.X)));
        com.applovin.impl.sdk.utils.p.a("persisted_data", m.d((String) this.b.a(e.x)), (Map<String, String>) hashMap);
        return hashMap;
    }

    public void run() {
        AnonymousClass1 r1 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(h.i(this.b)).c(h.j(this.b)).a(b()).b(HttpRequest.METHOD_GET).a(new JSONObject()).b(((Integer) this.b.a(c.dH)).intValue()).a(), this.b) {
            public void a(int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to fetch variables: server returned ");
                sb.append(i);
                d(sb.toString());
                o.i("AppLovinVariableService", "Failed to load variables.");
                p.this.f1367a.a();
            }

            public void a(JSONObject jSONObject, int i) {
                h.d(jSONObject, this.b);
                h.c(jSONObject, this.b);
                h.f(jSONObject, this.b);
                p.this.f1367a.a();
            }
        };
        r1.a(c.aM);
        r1.b(c.aN);
        this.b.K().a((a) r1);
    }
}
