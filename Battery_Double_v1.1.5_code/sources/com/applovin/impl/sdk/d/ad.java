package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.Collections;
import java.util.Map;
import org.json.JSONObject;

public class ad extends ae {

    /* renamed from: a reason: collision with root package name */
    private final f f1348a;
    private final AppLovinAdRewardListener c;

    public ad(f fVar, AppLovinAdRewardListener appLovinAdRewardListener, i iVar) {
        super("TaskValidateAppLovinReward", iVar);
        this.f1348a = fVar;
        this.c = appLovinAdRewardListener;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.z;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (!c()) {
            String str = "network_timeout";
            if (i < 400 || i >= 500) {
                this.c.validationRequestFailed(this.f1348a, i);
            } else {
                this.c.userRewardRejected(this.f1348a, Collections.emptyMap());
                str = "rejected";
            }
            this.f1348a.a(c.a(str));
        }
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        if (!c()) {
            this.f1348a.a(cVar);
            String b = cVar.b();
            Map a2 = cVar.a();
            if (b.equals("accepted")) {
                this.c.userRewardVerified(this.f1348a, a2);
            } else if (b.equals("quota_exceeded")) {
                this.c.userOverQuota(this.f1348a, a2);
            } else if (b.equals("rejected")) {
                this.c.userRewardRejected(this.f1348a, a2);
            } else {
                this.c.validationRequestFailed(this.f1348a, AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.f1348a.getAdZone().a(), this.b);
        String clCode = this.f1348a.getClCode();
        String str = "clcode";
        if (!m.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str, clCode, this.b);
    }

    public String b() {
        return "2.0/vr";
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.f1348a.aA();
    }
}
