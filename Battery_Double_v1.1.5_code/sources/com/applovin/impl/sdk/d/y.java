package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONObject;

public class y extends z {

    /* renamed from: a reason: collision with root package name */
    private final f f1386a;

    public y(f fVar, i iVar) {
        super("TaskReportAppLovinReward", iVar);
        this.f1386a = fVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.x;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to report reward for ad: ");
        sb.append(this.f1386a);
        sb.append(" - error code: ");
        sb.append(i);
        d(sb.toString());
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.f1386a.getAdZone().a(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, "fire_percent", this.f1386a.ah(), this.b);
        String clCode = this.f1386a.getClCode();
        String str = "clcode";
        if (!m.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str, clCode, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/cr";
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append("Reported reward successfully for ad: ");
        sb.append(this.f1386a);
        a(sb.toString());
    }

    /* access modifiers changed from: protected */
    public c c() {
        return this.f1386a.aC();
    }

    /* access modifiers changed from: protected */
    public void d() {
        StringBuilder sb = new StringBuilder();
        sb.append("No reward result was found for ad: ");
        sb.append(this.f1386a);
        d(sb.toString());
    }
}
