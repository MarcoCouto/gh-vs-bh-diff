package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.d.r.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.a.C0012a;
import com.applovin.impl.sdk.network.a.c;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinErrorCodes;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.concurrent.TimeUnit;

public abstract class x<T> extends a implements c<T> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final b<T> f1384a;
    private final c<T> c;
    protected C0012a d;
    /* access modifiers changed from: private */
    public a e;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.b.c<String> f;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.b.c<String> g;

    public x(b<T> bVar, i iVar) {
        this(bVar, iVar, false);
    }

    public x(b<T> bVar, final i iVar, boolean z) {
        super("TaskRepeatRequest", iVar, z);
        this.e = a.BACKGROUND;
        this.f = null;
        this.g = null;
        if (bVar != null) {
            this.f1384a = bVar;
            this.d = new C0012a();
            this.c = new c<T>() {
                public void a(int i) {
                    com.applovin.impl.sdk.b.c cVar;
                    x xVar;
                    boolean z = false;
                    boolean z2 = i < 200 || i >= 500;
                    boolean z3 = i == 429;
                    if (i != -103) {
                        z = true;
                    }
                    if (z && (z2 || z3)) {
                        String f = x.this.f1384a.f();
                        if (x.this.f1384a.j() > 0) {
                            x xVar2 = x.this;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Unable to send request due to server failure (code ");
                            sb.append(i);
                            sb.append("). ");
                            sb.append(x.this.f1384a.j());
                            sb.append(" attempts left, retrying in ");
                            sb.append(TimeUnit.MILLISECONDS.toSeconds((long) x.this.f1384a.l()));
                            sb.append(" seconds...");
                            xVar2.c(sb.toString());
                            int j = x.this.f1384a.j() - 1;
                            x.this.f1384a.a(j);
                            if (j == 0) {
                                x.this.c(x.this.f);
                                if (m.b(f) && f.length() >= 4) {
                                    x.this.f1384a.a(f);
                                    x xVar3 = x.this;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Switching to backup endpoint ");
                                    sb2.append(f);
                                    xVar3.b(sb2.toString());
                                }
                            }
                            iVar.K().a(x.this, x.this.e, (long) x.this.f1384a.l());
                            return;
                        }
                        if (f == null || !f.equals(x.this.f1384a.a())) {
                            xVar = x.this;
                            cVar = x.this.f;
                        } else {
                            xVar = x.this;
                            cVar = x.this.g;
                        }
                        xVar.c(cVar);
                    }
                    x.this.a(i);
                }

                public void a(T t, int i) {
                    x.this.f1384a.a(0);
                    x.this.a(t, i);
                }
            };
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    /* access modifiers changed from: private */
    public <ST> void c(com.applovin.impl.sdk.b.c<ST> cVar) {
        if (cVar != null) {
            d C = e().C();
            C.a(cVar, cVar.b());
            C.a();
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.e;
    }

    public abstract void a(int i);

    public void a(com.applovin.impl.sdk.b.c<String> cVar) {
        this.f = cVar;
    }

    public void a(a aVar) {
        this.e = aVar;
    }

    public abstract void a(T t, int i);

    public void b(com.applovin.impl.sdk.b.c<String> cVar) {
        this.g = cVar;
    }

    public void run() {
        int i;
        com.applovin.impl.sdk.network.a J = e().J();
        if (!e().c() && !e().d()) {
            d("AppLovin SDK is disabled: please check your connection");
            o.i("AppLovinSdk", "AppLovin SDK is disabled: please check your connection");
            i = -22;
        } else if (!m.b(this.f1384a.a()) || this.f1384a.a().length() < 4) {
            d("Task has an invalid or null request endpoint.");
            i = AppLovinErrorCodes.INVALID_URL;
        } else {
            if (TextUtils.isEmpty(this.f1384a.b())) {
                this.f1384a.b(this.f1384a.e() != null ? HttpRequest.METHOD_POST : HttpRequest.METHOD_GET);
            }
            J.a(this.f1384a, this.d, this.c);
            return;
        }
        a(i);
    }
}
