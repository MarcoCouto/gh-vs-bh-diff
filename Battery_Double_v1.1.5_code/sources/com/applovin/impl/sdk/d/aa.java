package com.applovin.impl.sdk.d;

import com.applovin.impl.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAdLoadListener;
import io.fabric.sdk.android.services.network.HttpRequest;

class aa extends a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public c f1344a;
    /* access modifiers changed from: private */
    public final AppLovinAdLoadListener c;

    aa(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super("TaskResolveVastWrapper", iVar);
        this.c = appLovinAdLoadListener;
        this.f1344a = cVar;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to resolve VAST wrapper due to error code ");
        sb.append(i);
        d(sb.toString());
        if (i == -103) {
            p.a(this.c, this.f1344a.g(), i, this.b);
        } else {
            com.applovin.impl.a.i.a(this.f1344a, this.c, i == -102 ? d.TIMED_OUT : d.GENERAL_WRAPPER_ERROR, i, this.b);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.y;
    }

    public void run() {
        String a2 = com.applovin.impl.a.i.a(this.f1344a);
        if (m.b(a2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Resolving VAST ad with depth ");
            sb.append(this.f1344a.a());
            sb.append(" at ");
            sb.append(a2);
            a(sb.toString());
            try {
                this.b.K().a((a) new x<r>(b.a(this.b).a(a2).b(HttpRequest.METHOD_GET).a(r.f1475a).a(((Integer) this.b.a(com.applovin.impl.sdk.b.c.eH)).intValue()).b(((Integer) this.b.a(com.applovin.impl.sdk.b.c.eI)).intValue()).a(false).a(), this.b) {
                    public void a(int i) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unable to resolve VAST wrapper. Server returned ");
                        sb.append(i);
                        d(sb.toString());
                        aa.this.a(i);
                    }

                    public void a(r rVar, int i) {
                        this.b.K().a((a) t.a(rVar, aa.this.f1344a, aa.this.c, aa.this.b));
                    }
                });
            } catch (Throwable th) {
                a("Unable to resolve VAST wrapper", th);
                a(-1);
                this.b.M().a(a());
            }
        } else {
            d("Resolving VAST failed. Could not find resolution URL");
            a(-1);
        }
    }
}
