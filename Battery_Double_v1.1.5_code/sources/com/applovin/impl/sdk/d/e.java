package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.m;
import com.applovin.impl.sdk.utils.h;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.List;

public class e extends f {
    public e(List<NativeAdImpl> list, i iVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super("TaskCacheNativeAdImages", list, iVar, appLovinNativeAdLoadListener);
    }

    public e(List<NativeAdImpl> list, i iVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        super("TaskCacheNativeAdImages", list, iVar, appLovinNativeAdPrecacheListener);
    }

    private boolean b(NativeAdImpl nativeAdImpl) {
        c("Unable to cache image resource");
        a(nativeAdImpl, !h.a(g()) ? AppLovinErrorCodes.NO_NETWORK : AppLovinErrorCodes.UNABLE_TO_PRECACHE_IMAGE_RESOURCES);
        return false;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.j;
    }

    /* access modifiers changed from: protected */
    public void a(NativeAdImpl nativeAdImpl) {
        if (this.f1354a != null) {
            this.f1354a.onNativeAdImagesPrecached(nativeAdImpl);
        }
    }

    /* access modifiers changed from: protected */
    public void a(NativeAdImpl nativeAdImpl, int i) {
        if (this.f1354a != null) {
            this.f1354a.onNativeAdImagePrecachingFailed(nativeAdImpl, i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(NativeAdImpl nativeAdImpl, m mVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Beginning native ad image caching for #");
        sb.append(nativeAdImpl.getAdId());
        a(sb.toString());
        if (((Boolean) this.b.a(c.bH)).booleanValue()) {
            String a2 = a(nativeAdImpl.getSourceIconUrl(), mVar, nativeAdImpl.getResourcePrefixes());
            if (a2 == null) {
                return b(nativeAdImpl);
            }
            nativeAdImpl.setIconUrl(a2);
            String a3 = a(nativeAdImpl.getSourceImageUrl(), mVar, nativeAdImpl.getResourcePrefixes());
            if (a3 == null) {
                return b(nativeAdImpl);
            }
            nativeAdImpl.setImageUrl(a3);
        } else {
            a("Resource caching is disabled, skipping...");
        }
        return true;
    }

    public /* bridge */ /* synthetic */ void run() {
        super.run();
    }
}
