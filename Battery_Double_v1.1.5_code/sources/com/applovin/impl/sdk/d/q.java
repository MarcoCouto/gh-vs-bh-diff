package com.applovin.impl.sdk.d;

import android.app.Activity;
import com.applovin.impl.sdk.EventServiceImpl;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.d.r.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.e;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.TimeUnit;

public class q extends a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final i f1369a;

    public q(i iVar) {
        super("TaskInitializeSdk", iVar);
        this.f1369a = iVar;
    }

    private void a(c<Boolean> cVar) {
        if (((Boolean) this.f1369a.a(cVar)).booleanValue()) {
            this.f1369a.T().f(d.a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, this.f1369a));
        }
    }

    private void b() {
        if (!this.f1369a.x().a()) {
            Activity ae = this.f1369a.ae();
            if (ae != null) {
                this.f1369a.x().a(ae);
            } else {
                this.f1369a.K().a(new ac(this.f1369a, true, new Runnable() {
                    public void run() {
                        q.this.f1369a.x().a(q.this.f1369a.aa().a());
                    }
                }), a.MAIN, TimeUnit.SECONDS.toMillis(1));
            }
        }
    }

    private void c() {
        this.f1369a.K().a((a) new b(this.f1369a), a.MAIN);
    }

    private void d() {
        this.f1369a.T().a();
        this.f1369a.U().a();
    }

    private void i() {
        j();
        k();
        l();
    }

    private void j() {
        LinkedHashSet a2 = this.f1369a.W().a();
        if (!a2.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Scheduling preload(s) for ");
            sb.append(a2.size());
            sb.append(" zone(s)");
            a(sb.toString());
            Iterator it = a2.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                if (dVar.d()) {
                    this.f1369a.p().preloadAds(dVar);
                } else {
                    this.f1369a.o().preloadAds(dVar);
                }
            }
        }
    }

    private void k() {
        c<Boolean> cVar = c.ba;
        String str = (String) this.f1369a.a(c.aZ);
        boolean z = false;
        if (str.length() > 0) {
            for (String fromString : e.a(str)) {
                AppLovinAdSize fromString2 = AppLovinAdSize.fromString(fromString);
                if (fromString2 != null) {
                    this.f1369a.T().f(d.a(fromString2, AppLovinAdType.REGULAR, this.f1369a));
                    if (AppLovinAdSize.INTERSTITIAL.getLabel().equals(fromString2.getLabel())) {
                        a(cVar);
                        z = true;
                    }
                }
            }
        }
        if (!z) {
            a(cVar);
        }
    }

    private void l() {
        if (((Boolean) this.f1369a.a(c.bb)).booleanValue()) {
            this.f1369a.U().f(d.h(this.f1369a));
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.f1337a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x013c, code lost:
        if (r6.f1369a.d() == false) goto L_0x0141;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x013e, code lost:
        r3 = com.facebook.internal.AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0141, code lost:
        r3 = com.ironsource.sdk.constants.Constants.ParametersKeys.FAILED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0143, code lost:
        r2.append(r3);
        r2.append(" in ");
        r2.append(java.lang.System.currentTimeMillis() - r0);
        r2.append("ms");
        a(r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x015f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00e0, code lost:
        if (r6.f1369a.d() != false) goto L_0x013e;
     */
    public void run() {
        StringBuilder sb;
        long currentTimeMillis = System.currentTimeMillis();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Initializing AppLovin SDK ");
        sb2.append(AppLovinSdk.VERSION);
        sb2.append("...");
        a(sb2.toString());
        try {
            this.f1369a.L().d();
            this.f1369a.L().c(g.b);
            this.f1369a.V().a(g());
            this.f1369a.V().b(g());
            d();
            i();
            b();
            this.f1369a.X().a();
            c();
            this.f1369a.O().e();
            this.f1369a.a(true);
            this.f1369a.N().a();
            ((EventServiceImpl) this.f1369a.q()).maybeTrackAppOpenEvent();
            this.f1369a.B().a();
            if (this.f1369a.z().a()) {
                this.f1369a.z().b();
            }
            if (((Boolean) this.f1369a.a(c.aq)).booleanValue()) {
                this.f1369a.a(((Long) this.f1369a.a(c.ar)).longValue());
            }
            sb = new StringBuilder();
            sb.append("AppLovin SDK ");
            sb.append(AppLovinSdk.VERSION);
            sb.append(" initialization ");
        } catch (Throwable th) {
            if (((Boolean) this.f1369a.a(c.aq)).booleanValue()) {
                this.f1369a.a(((Long) this.f1369a.a(c.ar)).longValue());
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append("AppLovin SDK ");
            sb3.append(AppLovinSdk.VERSION);
            sb3.append(" initialization ");
            sb3.append(this.f1369a.d() ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : ParametersKeys.FAILED);
            sb3.append(" in ");
            sb3.append(System.currentTimeMillis() - currentTimeMillis);
            sb3.append("ms");
            a(sb3.toString());
            throw th;
        }
    }
}
