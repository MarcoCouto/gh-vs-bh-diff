package com.applovin.impl.sdk.d;

import com.applovin.impl.a.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinAdLoadListener;
import org.json.JSONObject;

abstract class t extends a {

    /* renamed from: a reason: collision with root package name */
    private final AppLovinAdLoadListener f1378a;
    private final a c;

    private static final class a extends com.applovin.impl.a.c {
        a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, i iVar) {
            super(jSONObject, jSONObject2, bVar, iVar);
        }

        /* access modifiers changed from: 0000 */
        public void a(r rVar) {
            if (rVar != null) {
                this.f1024a.add(rVar);
                return;
            }
            throw new IllegalArgumentException("No aggregated vast response specified");
        }
    }

    private static final class b extends t {

        /* renamed from: a reason: collision with root package name */
        private final JSONObject f1379a;

        b(com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
            super(cVar, appLovinAdLoadListener, iVar);
            if (appLovinAdLoadListener != null) {
                this.f1379a = cVar.c();
                return;
            }
            throw new IllegalArgumentException("No callback specified.");
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.s;
        }

        public void run() {
            d dVar;
            a("Processing SDK JSON response...");
            String b = com.applovin.impl.sdk.utils.i.b(this.f1379a, "xml", (String) null, this.b);
            if (!m.b(b)) {
                d("No VAST response received.");
                dVar = d.NO_WRAPPER_RESPONSE;
            } else if (b.length() < ((Integer) this.b.a(com.applovin.impl.sdk.b.c.eA)).intValue()) {
                try {
                    a(s.a(b, this.b));
                    return;
                } catch (Throwable th) {
                    a("Unable to parse VAST response", th);
                    a(d.XML_PARSING);
                    this.b.M().a(a());
                    return;
                }
            } else {
                d("VAST response is over max length");
                dVar = d.XML_PARSING;
            }
            a(dVar);
        }
    }

    private static final class c extends t {

        /* renamed from: a reason: collision with root package name */
        private final r f1380a;

        c(r rVar, com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
            super(cVar, appLovinAdLoadListener, iVar);
            if (rVar == null) {
                throw new IllegalArgumentException("No response specified.");
            } else if (cVar == null) {
                throw new IllegalArgumentException("No context specified.");
            } else if (appLovinAdLoadListener != null) {
                this.f1380a = rVar;
            } else {
                throw new IllegalArgumentException("No callback specified.");
            }
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.t;
        }

        public void run() {
            a("Processing VAST Wrapper response...");
            a(this.f1380a);
        }
    }

    t(com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super("TaskProcessVastResponse", iVar);
        if (cVar != null) {
            this.f1378a = appLovinAdLoadListener;
            this.c = (a) cVar;
            return;
        }
        throw new IllegalArgumentException("No context specified.");
    }

    public static t a(r rVar, com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        return new c(rVar, cVar, appLovinAdLoadListener, iVar);
    }

    public static t a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        return new b(new a(jSONObject, jSONObject2, bVar, iVar), appLovinAdLoadListener, iVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(d dVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to process VAST response due to VAST error code ");
        sb.append(dVar);
        d(sb.toString());
        com.applovin.impl.a.i.a((com.applovin.impl.a.c) this.c, this.f1378a, dVar, -6, this.b);
    }

    /* access modifiers changed from: 0000 */
    public void a(r rVar) {
        d dVar;
        a aVar;
        int a2 = this.c.a();
        StringBuilder sb = new StringBuilder();
        sb.append("Finished parsing XML at depth ");
        sb.append(a2);
        a(sb.toString());
        this.c.a(rVar);
        if (com.applovin.impl.a.i.a(rVar)) {
            int intValue = ((Integer) this.b.a(com.applovin.impl.sdk.b.c.eB)).intValue();
            if (a2 < intValue) {
                a("VAST response is wrapper. Resolving...");
                aVar = new aa(this.c, this.f1378a, this.b);
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Reached beyond max wrapper depth of ");
                sb2.append(intValue);
                d(sb2.toString());
                dVar = d.WRAPPER_LIMIT_REACHED;
                a(dVar);
                return;
            }
        } else if (com.applovin.impl.a.i.b(rVar)) {
            a("VAST response is inline. Rendering ad...");
            aVar = new w(this.c, this.f1378a, this.b);
        } else {
            d("VAST response is an error");
            dVar = d.NO_WRAPPER_RESPONSE;
            a(dVar);
            return;
        }
        this.b.K().a(aVar);
    }
}
