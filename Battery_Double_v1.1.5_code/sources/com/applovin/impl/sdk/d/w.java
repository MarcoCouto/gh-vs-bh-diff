package com.applovin.impl.sdk.d;

import com.applovin.impl.a.a;
import com.applovin.impl.a.b;
import com.applovin.impl.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.a.f;
import com.applovin.impl.a.g;
import com.applovin.impl.a.j;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdType;
import com.explorestack.iab.vast.tags.VastTagName;
import java.util.HashSet;
import java.util.Set;

class w extends a {

    /* renamed from: a reason: collision with root package name */
    private c f1383a;
    private final AppLovinAdLoadListener c;

    w(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super("TaskRenderVastAd", iVar);
        this.c = appLovinAdLoadListener;
        this.f1383a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.w;
    }

    public void run() {
        a("Rendering VAST ad...");
        String str = "";
        String str2 = "";
        int size = this.f1383a.b().size();
        HashSet hashSet = new HashSet(size);
        HashSet hashSet2 = new HashSet(size);
        f fVar = null;
        j jVar = null;
        b bVar = null;
        for (r rVar : this.f1383a.b()) {
            r c2 = rVar.c(com.applovin.impl.a.i.a(rVar) ? VastTagName.WRAPPER : VastTagName.IN_LINE);
            if (c2 != null) {
                r c3 = c2.c(VastTagName.AD_SYSTEM);
                if (c3 != null) {
                    fVar = f.a(c3, fVar, this.b);
                }
                str = com.applovin.impl.a.i.a(c2, "AdTitle", str);
                str2 = com.applovin.impl.a.i.a(c2, "Description", str2);
                com.applovin.impl.a.i.a(c2.a(VastTagName.IMPRESSION), (Set<g>) hashSet, this.f1383a, this.b);
                com.applovin.impl.a.i.a(c2.a("Error"), (Set<g>) hashSet2, this.f1383a, this.b);
                r b = c2.b(VastTagName.CREATIVES);
                if (b != null) {
                    for (r rVar2 : b.d()) {
                        r b2 = rVar2.b(VastTagName.LINEAR);
                        if (b2 != null) {
                            jVar = j.a(b2, jVar, this.f1383a, this.b);
                        } else {
                            r c4 = rVar2.c(VastTagName.COMPANION_ADS);
                            if (c4 != null) {
                                r c5 = c4.c(VastTagName.COMPANION);
                                if (c5 != null) {
                                    bVar = b.a(c5, bVar, this.f1383a, this.b);
                                }
                            } else {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Received and will skip rendering for an unidentified creative: ");
                                sb.append(rVar2);
                                d(sb.toString());
                            }
                        }
                    }
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Did not find wrapper or inline response for node: ");
                sb2.append(rVar);
                d(sb2.toString());
            }
        }
        a a2 = a.aH().a(this.b).a(this.f1383a.c()).b(this.f1383a.d()).a(this.f1383a.e()).a(this.f1383a.f()).a(str).b(str2).a(fVar).a(jVar).a(bVar).a((Set<g>) hashSet).b((Set<g>) hashSet2).a();
        d a3 = com.applovin.impl.a.i.a(a2);
        if (a3 == null) {
            h hVar = new h(a2, this.b, this.c);
            r.a aVar = r.a.CACHING_OTHER;
            if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.bi)).booleanValue()) {
                if (a2.getType() == AppLovinAdType.REGULAR) {
                    aVar = r.a.CACHING_INTERSTITIAL;
                } else if (a2.getType() == AppLovinAdType.INCENTIVIZED) {
                    aVar = r.a.CACHING_INCENTIVIZED;
                }
            }
            this.b.K().a((a) hVar, aVar);
            return;
        }
        com.applovin.impl.a.i.a(this.f1383a, this.c, a3, -6, this.b);
    }
}
