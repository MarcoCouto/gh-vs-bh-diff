package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class l extends m {

    /* renamed from: a reason: collision with root package name */
    private final List<String> f1362a;

    public l(List<String> list, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super(d.a(a(list), iVar), appLovinAdLoadListener, "TaskFetchMultizoneAd", iVar);
        this.f1362a = Collections.unmodifiableList(list);
    }

    private static String a(List<String> list) {
        if (list != null && !list.isEmpty()) {
            return (String) list.get(0);
        }
        throw new IllegalArgumentException("No zone identifiers specified");
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.m;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap(1);
        hashMap.put("zone_ids", m.d(e.a((Collection<String>) this.f1362a, this.f1362a.size())));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b c() {
        return b.APPLOVIN_MULTIZONE;
    }
}
