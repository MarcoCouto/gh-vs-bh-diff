package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.a.c;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import io.fabric.sdk.android.services.network.HttpRequest;
import org.json.JSONObject;

public abstract class ab extends a {
    protected ab(String str, i iVar) {
        super(str, iVar);
    }

    /* access modifiers changed from: protected */
    public abstract void a(JSONObject jSONObject);

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject, final c<JSONObject> cVar) {
        AnonymousClass1 r0 = new x<JSONObject>(b.a(this.b).a(h.a(b(), this.b)).c(h.b(b(), this.b)).a(h.e(this.b)).b(HttpRequest.METHOD_POST).a(jSONObject).a(new JSONObject()).a(((Integer) this.b.a(com.applovin.impl.sdk.b.c.bL)).intValue()).a(), this.b) {
            public void a(int i) {
                cVar.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                cVar.a(jSONObject, i);
            }
        };
        r0.a(com.applovin.impl.sdk.b.c.aI);
        r0.b(com.applovin.impl.sdk.b.c.aJ);
        this.b.K().a((a) r0);
    }

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        String i = this.b.i();
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dS)).booleanValue() && m.b(i)) {
            com.applovin.impl.sdk.utils.i.a(jSONObject, "cuid", i, this.b);
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dU)).booleanValue()) {
            com.applovin.impl.sdk.utils.i.a(jSONObject, "compass_random_token", this.b.j(), this.b);
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dW)).booleanValue()) {
            com.applovin.impl.sdk.utils.i.a(jSONObject, "applovin_random_token", this.b.k(), this.b);
        }
        a(jSONObject);
        return jSONObject;
    }
}
