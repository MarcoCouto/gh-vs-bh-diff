package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.a;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Map;
import org.json.JSONObject;

public abstract class z extends ab {
    protected z(String str, i iVar) {
        super(str, iVar);
    }

    private JSONObject a(c cVar) {
        JSONObject i = i();
        com.applovin.impl.sdk.utils.i.a(i, IronSourceConstants.EVENTS_RESULT, cVar.b(), this.b);
        Map a2 = cVar.a();
        if (a2 != null) {
            com.applovin.impl.sdk.utils.i.a(i, "params", new JSONObject(a2), this.b);
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    /* access modifiers changed from: protected */
    public abstract void b(JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public abstract c c();

    /* access modifiers changed from: protected */
    public abstract void d();

    public void run() {
        c c = c();
        if (c != null) {
            a(a(c), new a.c<JSONObject>() {
                public void a(int i) {
                    z.this.a(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    z.this.b(jSONObject);
                }
            });
        } else {
            d();
        }
    }
}
