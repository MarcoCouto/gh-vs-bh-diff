package com.applovin.impl.sdk;

import android.text.TextUtils;
import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.e;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.d.n;
import com.applovin.impl.sdk.d.r;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import com.applovin.nativeAds.AppLovinNativeAdService;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NativeAdServiceImpl implements AppLovinNativeAdService {

    /* renamed from: a reason: collision with root package name */
    private final i f1277a;
    private final o b;

    NativeAdServiceImpl(i iVar) {
        this.f1277a = iVar;
        this.b = iVar.v();
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAd appLovinNativeAd, final AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        if (appLovinNativeAd.isVideoPrecached()) {
            appLovinNativeAdPrecacheListener.onNativeAdVideoPreceached(appLovinNativeAd);
            return;
        }
        this.f1277a.K().a((a) new g(Arrays.asList(new NativeAdImpl[]{(NativeAdImpl) appLovinNativeAd}), this.f1277a, (AppLovinNativeAdPrecacheListener) new AppLovinNativeAdPrecacheListener() {
            public void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i) {
            }

            public void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd) {
            }

            public void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i) {
                NativeAdServiceImpl.this.a(appLovinNativeAdPrecacheListener, appLovinNativeAd, i, true);
            }

            public void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd) {
                NativeAdServiceImpl.this.a(appLovinNativeAdPrecacheListener, appLovinNativeAd, true);
            }
        }), r.a.CACHING_OTHER);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener, int i) {
        if (appLovinNativeAdLoadListener != null) {
            try {
                appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i);
            } catch (Exception e) {
                o.c("NativeAdService", "Encountered exception whilst notifying user callback", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener, List<AppLovinNativeAd> list) {
        if (appLovinNativeAdLoadListener != null) {
            try {
                appLovinNativeAdLoadListener.onNativeAdsLoaded(list);
            } catch (Exception e) {
                o.c("NativeAdService", "Encountered exception whilst notifying user callback", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener, AppLovinNativeAd appLovinNativeAd, int i, boolean z) {
        if (appLovinNativeAdPrecacheListener == null) {
            return;
        }
        if (z) {
            try {
                appLovinNativeAdPrecacheListener.onNativeAdVideoPrecachingFailed(appLovinNativeAd, i);
            } catch (Exception e) {
                o.c("NativeAdService", "Encountered exception whilst notifying user callback", e);
            }
        } else {
            appLovinNativeAdPrecacheListener.onNativeAdImagePrecachingFailed(appLovinNativeAd, i);
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener, AppLovinNativeAd appLovinNativeAd, boolean z) {
        if (appLovinNativeAdPrecacheListener == null) {
            return;
        }
        if (z) {
            try {
                appLovinNativeAdPrecacheListener.onNativeAdVideoPreceached(appLovinNativeAd);
            } catch (Exception e) {
                o.c("NativeAdService", "Encountered exception whilst notifying user callback", e);
            }
        } else {
            appLovinNativeAdPrecacheListener.onNativeAdImagesPrecached(appLovinNativeAd);
        }
    }

    private void a(String str, int i, final AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        this.f1277a.K().a((a) new n(str, i, this.f1277a, new AppLovinNativeAdLoadListener() {
            public void onNativeAdsFailedToLoad(int i) {
                NativeAdServiceImpl.this.a(appLovinNativeAdLoadListener, i);
            }

            public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
                NativeAdServiceImpl.this.a(list, appLovinNativeAdLoadListener);
            }
        }), r.a.MAIN);
    }

    /* access modifiers changed from: private */
    public void a(List<AppLovinNativeAd> list, final AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        int intValue = ((Integer) this.f1277a.a(c.dp)).intValue();
        if (intValue > 0) {
            List list2 = list;
            int size = list2.size();
            if (size == 0) {
                if (appLovinNativeAdLoadListener != null) {
                    appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(AppLovinErrorCodes.UNABLE_TO_PREPARE_NATIVE_AD);
                }
                return;
            }
            int min = Math.min(intValue, size);
            final List subList = list2.subList(0, min);
            final List subList2 = list2.subList(min, size);
            b(subList, new AppLovinNativeAdLoadListener() {
                public void onNativeAdsFailedToLoad(int i) {
                    if (appLovinNativeAdLoadListener != null) {
                        appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i);
                    }
                }

                public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
                    NativeAdServiceImpl.this.c(subList, new AppLovinNativeAdLoadListener() {
                        public void onNativeAdsFailedToLoad(int i) {
                            if (appLovinNativeAdLoadListener != null) {
                                appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i);
                            }
                        }

                        public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
                            if (appLovinNativeAdLoadListener != null) {
                                ArrayList arrayList = new ArrayList();
                                arrayList.addAll(subList);
                                arrayList.addAll(subList2);
                                appLovinNativeAdLoadListener.onNativeAdsLoaded(arrayList);
                            }
                        }
                    });
                }
            });
        } else if (appLovinNativeAdLoadListener != null) {
            appLovinNativeAdLoadListener.onNativeAdsLoaded(list);
        }
    }

    private void b(List<NativeAdImpl> list, final AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        this.f1277a.K().a((a) new e(list, this.f1277a, (AppLovinNativeAdLoadListener) new AppLovinNativeAdLoadListener() {
            public void onNativeAdsFailedToLoad(int i) {
                if (appLovinNativeAdLoadListener != null) {
                    appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i);
                }
            }

            public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
                if (appLovinNativeAdLoadListener != null) {
                    appLovinNativeAdLoadListener.onNativeAdsLoaded(list);
                }
            }
        }), r.a.CACHING_OTHER);
    }

    /* access modifiers changed from: private */
    public void c(List<NativeAdImpl> list, final AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        this.f1277a.K().a((a) new g(list, this.f1277a, (AppLovinNativeAdLoadListener) new AppLovinNativeAdLoadListener() {
            public void onNativeAdsFailedToLoad(int i) {
                NativeAdServiceImpl.this.a(appLovinNativeAdLoadListener, i);
            }

            public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
                NativeAdServiceImpl.this.a(appLovinNativeAdLoadListener, list);
            }
        }), r.a.CACHING_OTHER);
    }

    public boolean hasPreloadedAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            o.i("NativeAdService", "Unable to check if ad is preloaded - invalid zone id.");
            return false;
        }
        return this.f1277a.U().g(d.a(str, this.f1277a));
    }

    public void loadNativeAds(int i, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        loadNativeAds(i, null, appLovinNativeAdLoadListener);
    }

    public void loadNativeAds(int i, String str, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        if (i > 0) {
            this.f1277a.a();
            if (i == 1) {
                d b2 = d.b(str, this.f1277a);
                AppLovinNativeAd appLovinNativeAd = (AppLovinNativeAd) this.f1277a.U().e(b2);
                if (appLovinNativeAd != null) {
                    a(appLovinNativeAdLoadListener, Arrays.asList(new AppLovinNativeAd[]{appLovinNativeAd}));
                } else {
                    a(str, 1, appLovinNativeAdLoadListener);
                }
                if (((Boolean) this.f1277a.a(c.bc)).booleanValue()) {
                    this.f1277a.U().i(b2);
                    return;
                }
                return;
            }
            a(str, i, appLovinNativeAdLoadListener);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Requested invalid number of native ads: ");
        sb.append(i);
        o.i("NativeAdService", sb.toString());
    }

    public void loadNextAd(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        loadNativeAds(1, appLovinNativeAdLoadListener);
    }

    public void precacheResources(AppLovinNativeAd appLovinNativeAd, final AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        this.f1277a.a();
        if (appLovinNativeAd.isImagePrecached()) {
            appLovinNativeAdPrecacheListener.onNativeAdImagesPrecached(appLovinNativeAd);
            a(appLovinNativeAd, appLovinNativeAdPrecacheListener);
            return;
        }
        this.f1277a.K().a((a) new e(Arrays.asList(new NativeAdImpl[]{(NativeAdImpl) appLovinNativeAd}), this.f1277a, (AppLovinNativeAdPrecacheListener) new AppLovinNativeAdPrecacheListener() {
            public void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i) {
                NativeAdServiceImpl.this.a(appLovinNativeAdPrecacheListener, appLovinNativeAd, i, false);
            }

            public void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd) {
                NativeAdServiceImpl.this.a(appLovinNativeAdPrecacheListener, appLovinNativeAd, false);
                NativeAdServiceImpl.this.a(appLovinNativeAd, appLovinNativeAdPrecacheListener);
            }

            public void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i) {
            }

            public void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd) {
            }
        }), r.a.CACHING_OTHER);
    }

    public void preloadAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            o.i("NativeAdService", "Unable to preload zone for invalid zone id.");
            return;
        }
        this.f1277a.a();
        d a2 = d.a(str, this.f1277a);
        this.f1277a.T().h(a2);
        this.f1277a.T().i(a2);
    }

    public void preloadAds(d dVar) {
        this.f1277a.T().h(dVar);
        int g = dVar.g();
        if (g == 0 && this.f1277a.T().b(dVar)) {
            g = 1;
        }
        this.f1277a.T().b(dVar, g);
    }

    public String toString() {
        return "NativeAdServiceImpl{}";
    }
}
