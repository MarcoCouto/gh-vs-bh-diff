package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.mediation.g;
import com.applovin.impl.mediation.h;
import com.applovin.impl.mediation.j;
import com.applovin.impl.mediation.k;
import com.applovin.impl.sdk.b.b;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.c.c;
import com.applovin.impl.sdk.d.ac;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.network.PostbackServiceImpl;
import com.applovin.impl.sdk.network.e;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdk.SdkInitializationListener;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserService;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class i {

    /* renamed from: a reason: collision with root package name */
    protected static Context f1400a;
    private c A;
    private t B;
    private a C;
    private n D;
    private s E;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.network.c F;
    private e G;
    private PostbackServiceImpl H;
    private e I;
    private h J;
    private g K;
    private MediationServiceImpl L;
    private k M;
    private a N;
    private j O;
    /* access modifiers changed from: private */
    public final Object P = new Object();
    private final AtomicBoolean Q = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public boolean R = false;
    private boolean S = false;
    private boolean T = false;
    private boolean U = false;
    private boolean V = false;
    private String W = "";
    private SdkInitializationListener X;
    private SdkInitializationListener Y;
    /* access modifiers changed from: private */
    public AppLovinSdkConfiguration Z;
    protected d b;
    private String c;
    private WeakReference<Activity> d;
    private long e;
    private AppLovinSdkSettings f;
    private AppLovinAdServiceImpl g;
    private NativeAdServiceImpl h;
    private EventServiceImpl i;
    private UserServiceImpl j;
    private VariableServiceImpl k;
    private AppLovinSdk l;
    /* access modifiers changed from: private */
    public o m;
    /* access modifiers changed from: private */
    public r n;
    private com.applovin.impl.sdk.network.a o;
    private com.applovin.impl.sdk.c.h p;
    private com.applovin.impl.sdk.c.j q;
    private j r;
    private f s;
    private com.applovin.impl.sdk.c.f t;
    private h u;
    private o v;
    private c w;
    private p x;
    private m y;
    private com.applovin.impl.sdk.ad.e z;

    public static Context E() {
        return f1400a;
    }

    private void af() {
        this.F.a((com.applovin.impl.sdk.network.c.a) new com.applovin.impl.sdk.network.c.a() {
            public void a() {
                i.this.m.c("AppLovinSdk", "Connected to internet - re-initializing SDK");
                synchronized (i.this.P) {
                    if (!i.this.R) {
                        i.this.b();
                    }
                }
                i.this.F.b(this);
            }

            public void b() {
            }
        });
    }

    public k A() {
        return this.M;
    }

    public j B() {
        return this.O;
    }

    public d C() {
        return this.b;
    }

    public Context D() {
        return f1400a;
    }

    public Activity F() {
        if (this.d != null) {
            return (Activity) this.d.get();
        }
        return null;
    }

    public long G() {
        return this.e;
    }

    public boolean H() {
        return this.U;
    }

    public boolean I() {
        return this.V;
    }

    public com.applovin.impl.sdk.network.a J() {
        return this.o;
    }

    public r K() {
        return this.n;
    }

    public com.applovin.impl.sdk.c.h L() {
        return this.p;
    }

    public com.applovin.impl.sdk.c.j M() {
        return this.q;
    }

    public e N() {
        return this.I;
    }

    public j O() {
        return this.r;
    }

    public com.applovin.impl.sdk.c.f P() {
        return this.t;
    }

    public h Q() {
        return this.u;
    }

    public PostbackServiceImpl R() {
        return this.H;
    }

    public AppLovinSdk S() {
        return this.l;
    }

    public c T() {
        return this.w;
    }

    public p U() {
        return this.x;
    }

    public m V() {
        return this.y;
    }

    public com.applovin.impl.sdk.ad.e W() {
        return this.z;
    }

    public c X() {
        return this.A;
    }

    public t Y() {
        return this.B;
    }

    public n Z() {
        return this.D;
    }

    public <ST> com.applovin.impl.sdk.b.c<ST> a(String str, com.applovin.impl.sdk.b.c<ST> cVar) {
        return this.b.a(str, cVar);
    }

    public <T> T a(com.applovin.impl.sdk.b.c<T> cVar) {
        return this.b.a(cVar);
    }

    public <T> T a(com.applovin.impl.sdk.b.e<T> eVar) {
        return b(eVar, null);
    }

    public <T> T a(String str, T t2, Class cls, SharedPreferences sharedPreferences) {
        f fVar = this.s;
        return f.a(str, t2, cls, sharedPreferences);
    }

    public void a() {
        synchronized (this.P) {
            if (!this.R && !this.S) {
                b();
            }
        }
    }

    public void a(long j2) {
        this.u.a(j2);
    }

    public void a(SharedPreferences sharedPreferences) {
        this.s.a(sharedPreferences);
    }

    public void a(com.applovin.impl.mediation.b.e eVar) {
        if (!this.n.a()) {
            List b2 = b((com.applovin.impl.sdk.b.c) b.f1319a);
            if (b2.size() > 0 && this.K.b().containsAll(b2)) {
                this.m.b("AppLovinSdk", "All required adapters initialized");
                this.n.e();
                e();
            }
        }
    }

    public <T> void a(com.applovin.impl.sdk.b.e<T> eVar, T t2) {
        this.s.a(eVar, t2);
    }

    public <T> void a(com.applovin.impl.sdk.b.e<T> eVar, T t2, SharedPreferences sharedPreferences) {
        this.s.a(eVar, t2, sharedPreferences);
    }

    public void a(SdkInitializationListener sdkInitializationListener) {
        if (!d()) {
            this.X = sdkInitializationListener;
        } else if (sdkInitializationListener != null) {
            sdkInitializationListener.onSdkInitialized(this.Z);
        }
    }

    public void a(AppLovinSdk appLovinSdk) {
        this.l = appLovinSdk;
    }

    public void a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Setting plugin version: ");
        sb.append(str);
        o.f("AppLovinSdk", sb.toString());
        this.b.a(com.applovin.impl.sdk.b.c.dY, (Object) str);
        this.b.a();
    }

    public void a(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        f fVar;
        com.applovin.impl.sdk.b.e<String> eVar;
        String bool;
        this.c = str;
        this.e = System.currentTimeMillis();
        this.f = appLovinSdkSettings;
        this.Z = new SdkConfigurationImpl(this);
        f1400a = context.getApplicationContext();
        if (context instanceof Activity) {
            this.d = new WeakReference<>((Activity) context);
        }
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            this.m = new o(this);
            this.s = new f(this);
            this.b = new d(this);
            this.b.b();
            this.t = new com.applovin.impl.sdk.c.f(this);
            this.t.b();
            this.y = new m(this);
            this.w = new c(this);
            this.x = new p(this);
            this.z = new com.applovin.impl.sdk.ad.e(this);
            this.i = new EventServiceImpl(this);
            this.j = new UserServiceImpl(this);
            this.k = new VariableServiceImpl(this);
            this.A = new c(this);
            this.n = new r(this);
            this.o = new com.applovin.impl.sdk.network.a(this);
            this.p = new com.applovin.impl.sdk.c.h(this);
            this.q = new com.applovin.impl.sdk.c.j(this);
            this.r = new j(this);
            this.C = new a(context);
            this.g = new AppLovinAdServiceImpl(this);
            this.h = new NativeAdServiceImpl(this);
            this.B = new t(this);
            this.D = new n(this);
            this.H = new PostbackServiceImpl(this);
            this.I = new e(this);
            this.J = new h(this);
            this.K = new g(this);
            this.L = new MediationServiceImpl(this);
            this.N = new a(this);
            this.M = new k();
            this.O = new j(this);
            this.u = new h(this);
            this.v = new o(this);
            this.E = new s(this);
            this.G = new e(this);
            if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dE)).booleanValue()) {
                this.F = new com.applovin.impl.sdk.network.c(context);
            }
            if (TextUtils.isEmpty(str)) {
                this.T = true;
                o.i("AppLovinSdk", "Unable to find AppLovin SDK key. Please add  meta-data android:name=\"applovin.sdk.key\" android:value=\"YOUR_SDK_KEY_HERE\" into AndroidManifest.xml.");
                StringWriter stringWriter = new StringWriter();
                new Throwable("").printStackTrace(new PrintWriter(stringWriter));
                String stringWriter2 = stringWriter.toString();
                StringBuilder sb = new StringBuilder();
                sb.append("Called with an invalid SDK key from: ");
                sb.append(stringWriter2);
                o.i("AppLovinSdk", sb.toString());
            }
            if (!u()) {
                if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.Z)).booleanValue()) {
                    appLovinSdkSettings.setTestAdsEnabled(p.b(context));
                    appLovinSdkSettings.setVerboseLogging(p.c(context));
                    C().a(appLovinSdkSettings);
                    C().a();
                }
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                if (TextUtils.isEmpty((String) this.s.b(com.applovin.impl.sdk.b.e.f1322a, null, defaultSharedPreferences))) {
                    this.U = true;
                    fVar = this.s;
                    eVar = com.applovin.impl.sdk.b.e.f1322a;
                    bool = Boolean.toString(true);
                } else {
                    fVar = this.s;
                    eVar = com.applovin.impl.sdk.b.e.f1322a;
                    bool = Boolean.toString(false);
                }
                fVar.a(eVar, bool, defaultSharedPreferences);
                if (((Boolean) this.s.b(com.applovin.impl.sdk.b.e.b, Boolean.valueOf(false))).booleanValue()) {
                    this.m.b("AppLovinSdk", "Initializing SDK for non-maiden launch");
                    this.V = true;
                } else {
                    this.m.b("AppLovinSdk", "Initializing SDK for maiden launch");
                    this.s.a(com.applovin.impl.sdk.b.e.b, Boolean.valueOf(true));
                }
                if (TextUtils.isEmpty((String) a(com.applovin.impl.sdk.b.e.g))) {
                    a(com.applovin.impl.sdk.b.e.g, (T) String.valueOf(((int) (Math.random() * 100.0d)) + 1));
                }
                boolean a2 = com.applovin.impl.sdk.utils.h.a(D());
                if (!((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dF)).booleanValue() || a2) {
                    b();
                }
                if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dE)).booleanValue() && !a2) {
                    this.m.c("AppLovinSdk", "SDK initialized with no internet connection - listening for connection");
                    af();
                }
            } else {
                a(false);
            }
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    public <T> void a(String str, T t2, Editor editor) {
        this.s.a(str, t2, editor);
    }

    public void a(boolean z2) {
        synchronized (this.P) {
            this.R = false;
            this.S = z2;
        }
        List b2 = b((com.applovin.impl.sdk.b.c) b.f1319a);
        if (b2.isEmpty()) {
            this.n.e();
            e();
            return;
        }
        long longValue = ((Long) a(b.b)).longValue();
        ac acVar = new ac(this, true, new Runnable() {
            public void run() {
                if (!i.this.n.a()) {
                    i.this.m.b("AppLovinSdk", "Timing out adapters init...");
                    i.this.n.e();
                    i.this.e();
                }
            }
        });
        StringBuilder sb = new StringBuilder();
        sb.append("Waiting for required adapters to init: ");
        sb.append(b2);
        sb.append(" - timing out in ");
        sb.append(longValue);
        sb.append("ms...");
        this.m.b("AppLovinSdk", sb.toString());
        this.n.a((com.applovin.impl.sdk.d.a) acVar, r.a.MEDIATION_TIMEOUT, longValue, true);
    }

    public a aa() {
        return this.C;
    }

    public s ab() {
        return this.E;
    }

    public e ac() {
        return this.G;
    }

    public AppLovinBroadcastManager ad() {
        return AppLovinBroadcastManager.getInstance(f1400a);
    }

    public Activity ae() {
        Activity F2 = F();
        if (F2 != null) {
            return F2;
        }
        Activity a2 = aa().a();
        if (a2 != null) {
            return a2;
        }
        return null;
    }

    public <T> T b(com.applovin.impl.sdk.b.e<T> eVar, T t2) {
        return this.s.b(eVar, t2);
    }

    public <T> T b(com.applovin.impl.sdk.b.e<T> eVar, T t2, SharedPreferences sharedPreferences) {
        return this.s.b(eVar, t2, sharedPreferences);
    }

    public List<String> b(com.applovin.impl.sdk.b.c cVar) {
        return this.b.b(cVar);
    }

    public void b() {
        synchronized (this.P) {
            this.R = true;
            K().d();
            K().a((com.applovin.impl.sdk.d.a) new com.applovin.impl.sdk.d.k(this), r.a.MAIN);
        }
    }

    public <T> void b(com.applovin.impl.sdk.b.e<T> eVar) {
        this.s.a(eVar);
    }

    public void b(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Setting user id: ");
        sb.append(str);
        o.f("AppLovinSdk", sb.toString());
        this.v.a(str);
    }

    public void c(String str) {
        a(com.applovin.impl.sdk.b.e.y, (T) str);
    }

    public boolean c() {
        boolean z2;
        synchronized (this.P) {
            z2 = this.R;
        }
        return z2;
    }

    public boolean d() {
        boolean z2;
        synchronized (this.P) {
            z2 = this.S;
        }
        return z2;
    }

    public void e() {
        if (this.X != null) {
            final SdkInitializationListener sdkInitializationListener = this.X;
            if (d()) {
                this.X = null;
                this.Y = null;
            } else if (this.Y != sdkInitializationListener) {
                if (((Boolean) a(com.applovin.impl.sdk.b.c.ae)).booleanValue()) {
                    this.X = null;
                } else {
                    this.Y = sdkInitializationListener;
                }
            } else {
                return;
            }
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    i.this.m.b("AppLovinSdk", "Calling back publisher's initialization completion handler...");
                    sdkInitializationListener.onSdkInitialized(i.this.Z);
                }
            }, Math.max(0, ((Long) a(com.applovin.impl.sdk.b.c.af)).longValue()));
        }
    }

    public void f() {
        long b2 = this.p.b(com.applovin.impl.sdk.c.g.g);
        this.b.c();
        this.b.a();
        this.p.a();
        this.A.b();
        this.q.b();
        this.p.b(com.applovin.impl.sdk.c.g.g, b2 + 1);
        if (this.Q.compareAndSet(true, false)) {
            b();
        } else {
            this.Q.set(true);
        }
    }

    public void g() {
        this.N.b();
    }

    public boolean h() {
        return this.B.d();
    }

    public String i() {
        return this.v.a();
    }

    public String j() {
        return this.v.b();
    }

    public String k() {
        return this.v.c();
    }

    public AppLovinSdkSettings l() {
        return this.f;
    }

    public AppLovinSdkConfiguration m() {
        return this.Z;
    }

    public String n() {
        return (String) a(com.applovin.impl.sdk.b.e.y);
    }

    public AppLovinAdServiceImpl o() {
        return this.g;
    }

    public NativeAdServiceImpl p() {
        return this.h;
    }

    public AppLovinEventService q() {
        return this.i;
    }

    public AppLovinUserService r() {
        return this.j;
    }

    public VariableServiceImpl s() {
        return this.k;
    }

    public String t() {
        return this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CoreSdk{sdkKey='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", enabled=");
        sb.append(this.S);
        sb.append(", isFirstSession=");
        sb.append(this.U);
        sb.append('}');
        return sb.toString();
    }

    public boolean u() {
        return this.T;
    }

    public o v() {
        return this.m;
    }

    public h w() {
        return this.J;
    }

    public g x() {
        return this.K;
    }

    public MediationServiceImpl y() {
        return this.L;
    }

    public a z() {
        return this.N;
    }
}
