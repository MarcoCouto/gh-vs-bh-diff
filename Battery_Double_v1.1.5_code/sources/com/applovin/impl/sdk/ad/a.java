package com.applovin.impl.sdk.ad;

import android.net.Uri;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public final class a extends f {
    public a(JSONObject jSONObject, JSONObject jSONObject2, b bVar, i iVar) {
        super(jSONObject, jSONObject2, bVar, iVar);
    }

    private String aD() {
        return getStringFromAdObject("stream_url", "");
    }

    public String a() {
        String b;
        synchronized (this.adObjectLock) {
            b = com.applovin.impl.sdk.utils.i.b(this.adObject, String.HTML, (String) null, this.sdk);
        }
        return b;
    }

    public void a(Uri uri) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("video", uri.toString());
            }
        } catch (Throwable unused) {
        }
    }

    public void a(String str) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put(String.HTML, str);
            }
        } catch (Throwable unused) {
        }
    }

    public boolean b() {
        return this.adObject.has("stream_url");
    }

    public void c() {
        synchronized (this.adObjectLock) {
            this.adObject.remove("stream_url");
        }
    }

    public Uri d() {
        String aD = aD();
        if (m.b(aD)) {
            return Uri.parse(aD);
        }
        String e = e();
        if (m.b(e)) {
            return Uri.parse(e);
        }
        return null;
    }

    public String e() {
        return getStringFromAdObject("video", "");
    }

    public Uri f() {
        String stringFromAdObject = getStringFromAdObject("click_url", "");
        if (m.b(stringFromAdObject)) {
            return Uri.parse(stringFromAdObject);
        }
        return null;
    }

    public Uri g() {
        String stringFromAdObject = getStringFromAdObject("video_click_url", "");
        return m.b(stringFromAdObject) ? Uri.parse(stringFromAdObject) : f();
    }

    public float h() {
        return getFloatFromAdObject("mraid_close_delay_graphic", 0.0f);
    }

    public boolean hasVideoUrl() {
        return d() != null;
    }

    public boolean i() {
        return getBooleanFromAdObject("close_button_graphic_hidden", Boolean.valueOf(false));
    }

    public boolean j() {
        if (this.adObject.has("close_button_expandable_hidden")) {
            return getBooleanFromAdObject("close_button_expandable_hidden", Boolean.valueOf(false));
        }
        return true;
    }

    public com.applovin.impl.adview.h.a k() {
        return a(getIntFromAdObject("expandable_style", com.applovin.impl.adview.h.a.Invisible.a()));
    }
}
