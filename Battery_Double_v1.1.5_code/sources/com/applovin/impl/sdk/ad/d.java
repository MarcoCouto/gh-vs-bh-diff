package com.applovin.impl.sdk.ad;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

public final class d {

    /* renamed from: a reason: collision with root package name */
    private static final Map<String, d> f1310a = new HashMap();
    private static final Object b = new Object();
    private i c;
    private o d;
    private JSONObject e;
    private final String f;
    private String g;
    private AppLovinAdSize h;
    private AppLovinAdType i;

    private d(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str, i iVar) {
        if (!TextUtils.isEmpty(str) || !(appLovinAdType == null || appLovinAdSize == null)) {
            this.c = iVar;
            this.d = iVar != null ? iVar.v() : null;
            this.h = appLovinAdSize;
            this.i = appLovinAdType;
            if (!TextUtils.isEmpty(str)) {
                this.f = str.toLowerCase(Locale.ENGLISH);
                this.g = str.toLowerCase(Locale.ENGLISH);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(appLovinAdSize.getLabel());
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(appLovinAdType.getLabel());
            this.f = sb.toString().toLowerCase(Locale.ENGLISH);
            return;
        }
        throw new IllegalArgumentException("No zone identifier or type or size specified");
    }

    public static d a(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, i iVar) {
        return a(appLovinAdSize, appLovinAdType, null, iVar);
    }

    public static d a(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str, i iVar) {
        d dVar = new d(appLovinAdSize, appLovinAdType, str, iVar);
        synchronized (b) {
            String str2 = dVar.f;
            if (f1310a.containsKey(str2)) {
                dVar = (d) f1310a.get(str2);
            } else {
                f1310a.put(str2, dVar);
            }
        }
        return dVar;
    }

    public static d a(String str, i iVar) {
        return a(null, null, str, iVar);
    }

    public static d a(String str, JSONObject jSONObject, i iVar) {
        d a2 = a(str, iVar);
        a2.e = jSONObject;
        return a2;
    }

    private <ST> c<ST> a(String str, c<ST> cVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.f);
        return this.c.a(sb.toString(), cVar);
    }

    private boolean a(c<String> cVar, AppLovinAdSize appLovinAdSize) {
        return ((String) this.c.a(cVar)).toUpperCase(Locale.ENGLISH).contains(appLovinAdSize.getLabel());
    }

    public static d b(String str, i iVar) {
        return a(AppLovinAdSize.NATIVE, AppLovinAdType.NATIVE, str, iVar);
    }

    public static Collection<d> b(i iVar) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(8);
        Collections.addAll(linkedHashSet, new d[]{c(iVar), d(iVar), e(iVar), f(iVar), g(iVar), h(iVar)});
        return Collections.unmodifiableSet(linkedHashSet);
    }

    public static d c(i iVar) {
        return a(AppLovinAdSize.BANNER, AppLovinAdType.REGULAR, iVar);
    }

    public static d c(String str, i iVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, str, iVar);
    }

    public static d d(i iVar) {
        return a(AppLovinAdSize.MREC, AppLovinAdType.REGULAR, iVar);
    }

    public static d e(i iVar) {
        return a(AppLovinAdSize.LEADER, AppLovinAdType.REGULAR, iVar);
    }

    public static d f(i iVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.REGULAR, iVar);
    }

    public static d g(i iVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, iVar);
    }

    public static d h(i iVar) {
        return a(AppLovinAdSize.NATIVE, AppLovinAdType.NATIVE, iVar);
    }

    private boolean k() {
        try {
            if (!TextUtils.isEmpty(this.g)) {
                return true;
            }
            return AppLovinAdType.INCENTIVIZED.equals(c()) ? ((Boolean) this.c.a(c.ba)).booleanValue() : a(c.aZ, b());
        } catch (Throwable th) {
            this.d.b("AdZone", "Unable to safely test preload merge capability", th);
            return false;
        }
    }

    public String a() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public void a(i iVar) {
        this.c = iVar;
        this.d = iVar.v();
    }

    public AppLovinAdSize b() {
        if (this.h == null && com.applovin.impl.sdk.utils.i.a(this.e, "ad_size")) {
            this.h = AppLovinAdSize.fromString(com.applovin.impl.sdk.utils.i.b(this.e, "ad_size", (String) null, this.c));
        }
        return this.h;
    }

    public AppLovinAdType c() {
        if (this.i == null && com.applovin.impl.sdk.utils.i.a(this.e, "ad_type")) {
            this.i = AppLovinAdType.fromString(com.applovin.impl.sdk.utils.i.b(this.e, "ad_type", (String) null, this.c));
        }
        return this.i;
    }

    public boolean d() {
        return AppLovinAdSize.NATIVE.equals(b()) && AppLovinAdType.NATIVE.equals(c());
    }

    public int e() {
        if (com.applovin.impl.sdk.utils.i.a(this.e, "capacity")) {
            return com.applovin.impl.sdk.utils.i.b(this.e, "capacity", 0, this.c);
        }
        if (!TextUtils.isEmpty(this.g)) {
            return d() ? ((Integer) this.c.a(c.bp)).intValue() : ((Integer) this.c.a(c.bo)).intValue();
        }
        return ((Integer) this.c.a(a("preload_capacity_", c.bd))).intValue();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f.equalsIgnoreCase(((d) obj).f);
    }

    public int f() {
        if (com.applovin.impl.sdk.utils.i.a(this.e, "extended_capacity")) {
            return com.applovin.impl.sdk.utils.i.b(this.e, "extended_capacity", 0, this.c);
        }
        if (TextUtils.isEmpty(this.g)) {
            return ((Integer) this.c.a(a("extended_preload_capacity_", c.bj))).intValue();
        } else if (d()) {
            return 0;
        } else {
            return ((Integer) this.c.a(c.bq)).intValue();
        }
    }

    public int g() {
        return com.applovin.impl.sdk.utils.i.b(this.e, "preload_count", 0, this.c);
    }

    public boolean h() {
        boolean z = false;
        if (!((Boolean) this.c.a(c.aY)).booleanValue() || !k()) {
            return false;
        }
        if (TextUtils.isEmpty(this.g)) {
            c a2 = a("preload_merge_init_tasks_", null);
            if (a2 != null && ((Boolean) this.c.a(a2)).booleanValue() && e() > 0) {
                z = true;
            }
            return z;
        } else if (this.e != null && g() == 0) {
            return false;
        } else {
            String upperCase = ((String) this.c.a(c.aZ)).toUpperCase(Locale.ENGLISH);
            return (upperCase.contains(AppLovinAdSize.INTERSTITIAL.getLabel()) || upperCase.contains(AppLovinAdSize.BANNER.getLabel()) || upperCase.contains(AppLovinAdSize.MREC.getLabel()) || upperCase.contains(AppLovinAdSize.LEADER.getLabel())) ? ((Boolean) this.c.a(c.bx)).booleanValue() : this.c.W().a(this) && g() > 0 && ((Boolean) this.c.a(c.dI)).booleanValue();
        }
    }

    public int hashCode() {
        return this.f.hashCode();
    }

    public boolean i() {
        return com.applovin.impl.sdk.utils.i.a(this.e, "wrapped_ads_enabled") ? com.applovin.impl.sdk.utils.i.a(this.e, "wrapped_ads_enabled", Boolean.valueOf(false), this.c).booleanValue() : b() != null ? this.c.b((c) c.bY).contains(b().getLabel()) : ((Boolean) this.c.a(c.bX)).booleanValue();
    }

    public boolean j() {
        return b(this.c).contains(this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdZone{identifier=");
        sb.append(this.f);
        sb.append(", zoneObject=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
