package com.applovin.impl.sdk.ad;

import android.text.TextUtils;
import android.util.Base64;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a reason: collision with root package name */
    private final i f1308a;
    private final String b;

    public enum a {
        UNSPECIFIED("UNSPECIFIED"),
        REGULAR("REGULAR"),
        AD_RESPONSE_JSON("AD_RESPONSE_JSON");
        
        private final String d;

        private a(String str) {
            this.d = str;
        }

        public String toString() {
            return this.d;
        }
    }

    public c(String str, i iVar) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Identifier is empty");
        } else if (iVar != null) {
            this.b = str;
            this.f1308a = iVar;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    private String a(com.applovin.impl.sdk.b.c<String> cVar) {
        for (String str : this.f1308a.b((com.applovin.impl.sdk.b.c) cVar)) {
            if (this.b.startsWith(str)) {
                return str;
            }
        }
        return null;
    }

    public String a() {
        return this.b;
    }

    public a b() {
        return a(com.applovin.impl.sdk.b.c.aP) != null ? a.REGULAR : a(com.applovin.impl.sdk.b.c.aQ) != null ? a.AD_RESPONSE_JSON : a.UNSPECIFIED;
    }

    public String c() {
        String a2 = a(com.applovin.impl.sdk.b.c.aP);
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        String a3 = a(com.applovin.impl.sdk.b.c.aQ);
        if (!TextUtils.isEmpty(a3)) {
            return a3;
        }
        return null;
    }

    public JSONObject d() {
        if (b() == a.AD_RESPONSE_JSON) {
            try {
                JSONObject jSONObject = new JSONObject(new String(Base64.decode(this.b.substring(c().length()), 0), "UTF-8"));
                StringBuilder sb = new StringBuilder();
                sb.append("Decoded token into ad response: ");
                sb.append(jSONObject);
                this.f1308a.v().b("AdToken", sb.toString());
                return jSONObject;
            } catch (JSONException e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to decode token '");
                sb2.append(this.b);
                sb2.append("' into JSON");
                this.f1308a.v().b("AdToken", sb2.toString(), e);
            } catch (Throwable th) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to process ad response from token '");
                sb3.append(this.b);
                sb3.append("'");
                this.f1308a.v().b("AdToken", sb3.toString(), th);
            }
        }
        return null;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (this.b != null) {
            z = this.b.equals(cVar.b);
        } else if (cVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        if (this.b != null) {
            return this.b.hashCode();
        }
        return 0;
    }

    public String toString() {
        String a2 = m.a(32, this.b);
        StringBuilder sb = new StringBuilder();
        sb.append("AdToken{id=");
        sb.append(a2);
        sb.append(", type=");
        sb.append(b());
        sb.append('}');
        return sb.toString();
    }
}
