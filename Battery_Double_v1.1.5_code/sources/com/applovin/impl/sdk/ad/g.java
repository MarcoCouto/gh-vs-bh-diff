package com.applovin.impl.sdk.ad;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import org.json.JSONObject;

public final class g extends AppLovinAdBase {

    /* renamed from: a reason: collision with root package name */
    private AppLovinAd f1315a;
    private final d b;

    public g(d dVar, i iVar) {
        super(new JSONObject(), new JSONObject(), b.UNKNOWN, iVar);
        this.b = dVar;
    }

    private AppLovinAd c() {
        return (AppLovinAd) this.sdk.T().c(this.b);
    }

    private String d() {
        d adZone = getAdZone();
        if (adZone == null || adZone.j()) {
            return null;
        }
        return adZone.a();
    }

    public AppLovinAd a() {
        return this.f1315a;
    }

    public void a(AppLovinAd appLovinAd) {
        this.f1315a = appLovinAd;
    }

    public AppLovinAd b() {
        return this.f1315a != null ? this.f1315a : c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AppLovinAd b2 = b();
        return b2 != null ? b2.equals(obj) : super.equals(obj);
    }

    public long getAdIdNumber() {
        try {
            AppLovinAd b2 = b();
            if (b2 != null) {
                return b2.getAdIdNumber();
            }
            return 0;
        } catch (Throwable th) {
            o.c("AppLovinAd", "Failed to retrieve ad id number", th);
            return 0;
        }
    }

    public d getAdZone() {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) b();
        return appLovinAdBase != null ? appLovinAdBase.getAdZone() : this.b;
    }

    public AppLovinAdSize getSize() {
        AppLovinAdSize appLovinAdSize = AppLovinAdSize.INTERSTITIAL;
        try {
            return getAdZone().b();
        } catch (Throwable th) {
            o.c("AppLovinAd", "Failed to retrieve ad size", th);
            return appLovinAdSize;
        }
    }

    public b getSource() {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) b();
        return appLovinAdBase != null ? appLovinAdBase.getSource() : b.UNKNOWN;
    }

    public AppLovinAdType getType() {
        AppLovinAdType appLovinAdType = AppLovinAdType.REGULAR;
        try {
            return getAdZone().c();
        } catch (Throwable th) {
            o.c("AppLovinAd", "Failed to retrieve ad type", th);
            return appLovinAdType;
        }
    }

    public String getZoneId() {
        try {
            if (!this.b.j()) {
                return this.b.a();
            }
            return null;
        } catch (Throwable th) {
            o.c("AppLovinAd", "Failed to return zone id", th);
            return null;
        }
    }

    public int hashCode() {
        AppLovinAd b2 = b();
        return b2 != null ? b2.hashCode() : super.hashCode();
    }

    public boolean isVideoAd() {
        try {
            AppLovinAd b2 = b();
            if (b2 != null) {
                return b2.isVideoAd();
            }
            return false;
        } catch (Throwable th) {
            o.c("AppLovinAd", "Failed to return whether ad is video ad", th);
            return false;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinAd{ #");
        sb.append(getAdIdNumber());
        sb.append(", adType=");
        sb.append(getType());
        sb.append(", adSize=");
        sb.append(getSize());
        sb.append(", zoneId='");
        sb.append(d());
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
