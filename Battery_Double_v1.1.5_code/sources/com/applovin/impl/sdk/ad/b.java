package com.applovin.impl.sdk.ad;

public enum b {
    UNKNOWN,
    APPLOVIN_PRIMARY_ZONE,
    APPLOVIN_CUSTOM_ZONE,
    APPLOVIN_MULTIZONE,
    REGULAR_AD_TOKEN,
    DECODED_AD_TOKEN_JSON
}
