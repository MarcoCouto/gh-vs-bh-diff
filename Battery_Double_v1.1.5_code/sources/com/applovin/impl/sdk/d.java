package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.b.b;
import com.applovin.impl.sdk.utils.n;

public class d extends BroadcastReceiver implements a {

    /* renamed from: a reason: collision with root package name */
    private n f1341a;
    private final Object b = new Object();
    private final i c;
    /* access modifiers changed from: private */
    public final a d;
    private long e;

    public interface a {
        void onAdRefresh();
    }

    public d(i iVar, a aVar) {
        this.d = aVar;
        this.c = iVar;
    }

    /* access modifiers changed from: private */
    public void j() {
        synchronized (this.b) {
            this.f1341a = null;
            if (!((Boolean) this.c.a(b.x)).booleanValue()) {
                this.c.ad().unregisterReceiver(this);
                this.c.Z().b((a) this);
            }
        }
    }

    public void a(long j) {
        synchronized (this.b) {
            c();
            this.e = j;
            this.f1341a = n.a(j, this.c, new Runnable() {
                public void run() {
                    d.this.j();
                    d.this.d.onAdRefresh();
                }
            });
            if (!((Boolean) this.c.a(b.x)).booleanValue()) {
                this.c.ad().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
                this.c.ad().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
                this.c.Z().a((a) this);
            }
            if (((Boolean) this.c.a(b.w)).booleanValue() && (this.c.Z().b() || this.c.Y().a())) {
                this.f1341a.b();
            }
        }
    }

    public boolean a() {
        boolean z;
        synchronized (this.b) {
            z = this.f1341a != null;
        }
        return z;
    }

    public long b() {
        long a2;
        synchronized (this.b) {
            a2 = this.f1341a != null ? this.f1341a.a() : -1;
        }
        return a2;
    }

    public void c() {
        synchronized (this.b) {
            if (this.f1341a != null) {
                this.f1341a.d();
                j();
            }
        }
    }

    public void d() {
        synchronized (this.b) {
            if (this.f1341a != null) {
                this.f1341a.b();
            }
        }
    }

    public void e() {
        synchronized (this.b) {
            if (this.f1341a != null) {
                this.f1341a.c();
            }
        }
    }

    public void f() {
        if (((Boolean) this.c.a(b.v)).booleanValue()) {
            d();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005f, code lost:
        if (r2 == false) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0061, code lost:
        r9.d.onAdRefresh();
     */
    public void g() {
        if (((Boolean) this.c.a(b.v)).booleanValue()) {
            synchronized (this.b) {
                if (this.c.Z().b()) {
                    this.c.v().b("AdRefreshManager", "Waiting for the full screen ad to be dismissed to resume the timer.");
                    return;
                }
                boolean z = false;
                if (this.f1341a != null) {
                    long b2 = this.e - b();
                    long longValue = ((Long) this.c.a(b.u)).longValue();
                    if (longValue < 0 || b2 <= longValue) {
                        this.f1341a.c();
                    } else {
                        c();
                        z = true;
                    }
                }
            }
        }
    }

    public void h() {
        if (((Boolean) this.c.a(b.w)).booleanValue()) {
            d();
        }
    }

    public void i() {
        if (((Boolean) this.c.a(b.w)).booleanValue()) {
            synchronized (this.b) {
                if (this.c.Y().a()) {
                    this.c.v().b("AdRefreshManager", "Waiting for the application to enter foreground to resume the timer.");
                } else if (this.f1341a != null) {
                    this.f1341a.c();
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            f();
        } else if ("com.applovin.application_resumed".equals(action)) {
            g();
        }
    }
}
