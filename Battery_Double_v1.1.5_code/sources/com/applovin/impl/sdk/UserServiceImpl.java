package com.applovin.impl.sdk;

import android.app.Activity;
import com.applovin.sdk.AppLovinUserService;
import com.applovin.sdk.AppLovinUserService.OnConsentDialogDismissListener;

public class UserServiceImpl implements AppLovinUserService {

    /* renamed from: a reason: collision with root package name */
    private final i f1286a;

    UserServiceImpl(i iVar) {
        this.f1286a = iVar;
    }

    public void showConsentDialog(Activity activity, OnConsentDialogDismissListener onConsentDialogDismissListener) {
        this.f1286a.Q().a(activity, onConsentDialogDismissListener);
    }

    public String toString() {
        return "UserService{}";
    }
}
