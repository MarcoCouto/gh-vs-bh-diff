package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicBoolean;

class g extends BroadcastReceiver {
    /* access modifiers changed from: private */
    public static AlertDialog b;
    /* access modifiers changed from: private */
    public static final AtomicBoolean c = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final h f1389a;
    private n d;

    public interface a {
        void a();

        void b();
    }

    g(h hVar, i iVar) {
        this.f1389a = hVar;
        iVar.ad().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        iVar.ad().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public void a(long j, final i iVar, final a aVar) {
        if (j > 0) {
            if (b == null || !b.isShowing()) {
                if (c.getAndSet(true)) {
                    if (j < this.d.a()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Scheduling consent alert earlier (");
                        sb.append(j);
                        sb.append("ms) than remaining scheduled time (");
                        sb.append(this.d.a());
                        sb.append("ms)");
                        iVar.v().b("ConsentAlertManager", sb.toString());
                        this.d.d();
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Skip scheduling consent alert - one scheduled already with remaining time of ");
                        sb2.append(this.d.a());
                        sb2.append(" milliseconds");
                        iVar.v().d("ConsentAlertManager", sb2.toString());
                        return;
                    }
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Scheduling consent alert for ");
                sb3.append(j);
                sb3.append(" milliseconds");
                iVar.v().b("ConsentAlertManager", sb3.toString());
                this.d = n.a(j, iVar, new Runnable() {
                    public void run() {
                        o v;
                        String str;
                        String str2;
                        if (g.this.f1389a.c()) {
                            iVar.v().e("ConsentAlertManager", "Consent dialog already showing, skip showing of consent alert");
                            return;
                        }
                        Activity a2 = iVar.aa().a();
                        if (a2 == null || !h.a(iVar.D())) {
                            if (a2 == null) {
                                v = iVar.v();
                                str = "ConsentAlertManager";
                                str2 = "No parent Activity found - rescheduling consent alert...";
                            } else {
                                v = iVar.v();
                                str = "ConsentAlertManager";
                                str2 = "No internet available - rescheduling consent alert...";
                            }
                            v.e(str, str2);
                            g.c.set(false);
                            g.this.a(((Long) iVar.a(c.aw)).longValue(), iVar, aVar);
                            return;
                        }
                        AppLovinSdkUtils.runOnUiThread(new Runnable() {
                            public void run() {
                                g.b = new Builder(iVar.aa().a()).setTitle((CharSequence) iVar.a(c.ax)).setMessage((CharSequence) iVar.a(c.ay)).setCancelable(false).setPositiveButton((CharSequence) iVar.a(c.az), new OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.a();
                                        dialogInterface.dismiss();
                                        g.c.set(false);
                                    }
                                }).setNegativeButton((CharSequence) iVar.a(c.aA), new OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.b();
                                        dialogInterface.dismiss();
                                        g.c.set(false);
                                        g.this.a(((Long) iVar.a(c.av)).longValue(), iVar, aVar);
                                    }
                                }).create();
                                g.b.show();
                            }
                        });
                    }
                });
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (this.d != null) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                this.d.b();
            } else if ("com.applovin.application_resumed".equals(action)) {
                this.d.c();
            }
        }
    }
}
