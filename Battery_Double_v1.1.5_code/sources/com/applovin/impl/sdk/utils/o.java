package com.applovin.impl.sdk.utils;

import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.i;
import java.util.Locale;
import java.util.UUID;

public final class o {

    /* renamed from: a reason: collision with root package name */
    private final i f1473a;
    private String b = d();
    private final String c;
    private final String d;

    public o(i iVar) {
        this.f1473a = iVar;
        this.c = a(e.e, (String) f.b(e.d, null, iVar.D()));
        this.d = a(e.f, (String) iVar.a(c.S));
    }

    private String a(e<String> eVar, String str) {
        String str2 = (String) f.b(eVar, null, this.f1473a.D());
        if (m.b(str2)) {
            return str2;
        }
        if (!m.b(str)) {
            str = UUID.randomUUID().toString().toLowerCase(Locale.US);
        }
        f.a(eVar, str, this.f1473a.D());
        return str;
    }

    private String d() {
        if (!((Boolean) this.f1473a.a(c.dX)).booleanValue()) {
            this.f1473a.b(e.c);
        }
        String str = (String) this.f1473a.a(e.c);
        if (m.b(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Using identifier (");
            sb.append(str);
            sb.append(") from previous session");
            this.f1473a.v().b("AppLovinSdk", sb.toString());
            this.b = str;
        }
        return null;
    }

    public String a() {
        return this.b;
    }

    public void a(String str) {
        if (((Boolean) this.f1473a.a(c.dX)).booleanValue()) {
            this.f1473a.a(e.c, str);
        }
        this.b = str;
    }

    public String b() {
        return this.c;
    }

    public String c() {
        return this.d;
    }
}
