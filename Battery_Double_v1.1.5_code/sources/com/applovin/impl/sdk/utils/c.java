package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.content.res.XmlResourceParser;

public class c {

    /* renamed from: a reason: collision with root package name */
    private static c f1440a;
    private static final Object b = new Object();
    private final int c;

    private c() {
        this.c = 0;
    }

    private c(Context context) {
        int i;
        int i2 = 0;
        try {
            XmlResourceParser openXmlResourceParser = context.getAssets().openXmlResourceParser("AndroidManifest.xml");
            int eventType = openXmlResourceParser.getEventType();
            i = 0;
            do {
                if (2 == eventType) {
                    try {
                        if (openXmlResourceParser.getName().equals("application")) {
                            int i3 = 0;
                            while (true) {
                                if (i3 >= openXmlResourceParser.getAttributeCount()) {
                                    break;
                                }
                                String attributeName = openXmlResourceParser.getAttributeName(i3);
                                String attributeValue = openXmlResourceParser.getAttributeValue(i3);
                                if (attributeName.equals("networkSecurityConfig")) {
                                    i = Integer.valueOf(attributeValue).intValue();
                                    break;
                                }
                                i3++;
                            }
                        }
                    } catch (Throwable th) {
                        th = th;
                        this.c = i;
                        throw th;
                    }
                }
                eventType = openXmlResourceParser.next();
            } while (eventType != 1);
            this.c = i;
        } catch (Throwable th2) {
            th = th2;
            i = 0;
            this.c = i;
            throw th;
        }
    }

    public static c a(Context context) {
        c cVar;
        synchronized (b) {
            if (f1440a == null) {
                f1440a = new c(context);
            }
            cVar = f1440a;
        }
        return cVar;
    }

    public boolean a() {
        return this.c != 0;
    }
}
