package com.applovin.impl.sdk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.i;
import java.util.HashSet;
import java.util.Set;

public class d extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final Set<d> f1441a = new HashSet();
    private final n b;

    private d(long j, final i iVar, final Runnable runnable) {
        this.b = n.a(j, iVar, new Runnable() {
            public void run() {
                iVar.ad().unregisterReceiver(d.this);
                d.this.a();
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        f1441a.add(this);
        iVar.ad().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        iVar.ad().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public static d a(long j, i iVar, Runnable runnable) {
        return new d(j, iVar, runnable);
    }

    public void a() {
        this.b.d();
        f1441a.remove(this);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            this.b.b();
        } else if ("com.applovin.application_resumed".equals(action)) {
            this.b.c();
        }
    }
}
