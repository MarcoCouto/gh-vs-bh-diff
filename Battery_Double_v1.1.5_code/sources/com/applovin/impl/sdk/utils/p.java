package com.applovin.impl.sdk.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdk;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.smaato.sdk.core.api.VideoType;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

public abstract class p {

    /* renamed from: a reason: collision with root package name */
    private static ApplicationInfo f1474a;
    private static Boolean b;

    public static double a(long j) {
        double d = (double) j;
        Double.isNaN(d);
        return d / 1000.0d;
    }

    public static float a(float f) {
        return f * 1000.0f;
    }

    public static int a(JSONObject jSONObject) {
        int b2 = i.b(jSONObject, "video_completion_percent", -1, (i) null);
        if (b2 < 0 || b2 > 100) {
            return 95;
        }
        return b2;
    }

    public static long a(i iVar) {
        long longValue = ((Long) iVar.a(c.eL)).longValue();
        long longValue2 = ((Long) iVar.a(c.eM)).longValue();
        long currentTimeMillis = System.currentTimeMillis();
        return (longValue <= 0 || longValue2 <= 0) ? currentTimeMillis : currentTimeMillis + (longValue - longValue2);
    }

    public static Activity a(View view, i iVar) {
        if (view == null) {
            return null;
        }
        int i = 0;
        while (i < 1000) {
            i++;
            try {
                Context context = view.getContext();
                if (context instanceof Activity) {
                    return (Activity) context;
                }
                ViewParent parent = view.getParent();
                if (!(parent instanceof View)) {
                    return null;
                }
                view = (View) parent;
            } catch (Throwable th) {
                iVar.v().b("Utils", "Encountered error while retrieving activity from view", th);
            }
        }
        return null;
    }

    public static Bitmap a(Context context, int i, int i2) {
        try {
            Options options = new Options();
            int i3 = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(context.getResources(), i);
            if (options.outHeight > i2 || options.outWidth > i2) {
                double d = (double) i2;
                double max = (double) Math.max(options.outHeight, options.outWidth);
                Double.isNaN(d);
                Double.isNaN(max);
                i3 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log(d / max) / Math.log(0.5d))));
            }
            new Options().inSampleSize = i3;
            return BitmapFactory.decodeResource(context.getResources(), i);
        } catch (Exception unused) {
            return null;
        } finally {
            a((Closeable) null, (i) null);
            a((Closeable) null, (i) null);
        }
    }

    public static Bitmap a(File file, int i) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        Options options;
        try {
            Options options2 = new Options();
            int i2 = 1;
            options2.inJustDecodeBounds = true;
            fileInputStream = new FileInputStream(file);
            try {
                BitmapFactory.decodeStream(fileInputStream, null, options2);
                fileInputStream.close();
                if (options2.outHeight > i || options2.outWidth > i) {
                    double d = (double) i;
                    double max = (double) Math.max(options2.outHeight, options2.outWidth);
                    Double.isNaN(d);
                    Double.isNaN(max);
                    i2 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log(d / max) / Math.log(0.5d))));
                }
                options = new Options();
                options.inSampleSize = i2;
                fileInputStream2 = new FileInputStream(file);
            } catch (Exception unused) {
                fileInputStream2 = null;
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                return null;
            } catch (Throwable th) {
                th = th;
                fileInputStream2 = null;
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                throw th;
            }
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(fileInputStream2, null, options);
                fileInputStream2.close();
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                return decodeStream;
            } catch (Exception unused2) {
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                return null;
            } catch (Throwable th2) {
                th = th2;
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                throw th;
            }
        } catch (Exception unused3) {
            fileInputStream2 = null;
            fileInputStream = null;
            a((Closeable) fileInputStream, (i) null);
            a((Closeable) fileInputStream2, (i) null);
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream2 = null;
            fileInputStream = null;
            a((Closeable) fileInputStream, (i) null);
            a((Closeable) fileInputStream2, (i) null);
            throw th;
        }
    }

    public static View a(Context context, View view) {
        View e = e(context);
        return e != null ? e : a(view);
    }

    public static View a(View view) {
        if (view == null) {
            return null;
        }
        View rootView = view.getRootView();
        if (rootView == null) {
            return null;
        }
        View findViewById = rootView.findViewById(16908290);
        if (findViewById != null) {
            rootView = findViewById;
        }
        return rootView;
    }

    public static d a(JSONObject jSONObject, i iVar) {
        return d.a(AppLovinAdSize.fromString(i.b(jSONObject, "ad_size", (String) null, iVar)), AppLovinAdType.fromString(i.b(jSONObject, "ad_type", (String) null, iVar)), i.b(jSONObject, "zone_id", (String) null, iVar), iVar);
    }

    public static i a(AppLovinSdk appLovinSdk) {
        try {
            Field declaredField = appLovinSdk.getClass().getDeclaredField("mSdkImpl");
            declaredField.setAccessible(true);
            return (i) declaredField.get(appLovinSdk);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Internal error - unable to retrieve SDK implementation: ");
            sb.append(th);
            throw new IllegalStateException(sb.toString());
        }
    }

    public static AppLovinAd a(AppLovinAd appLovinAd, i iVar) {
        if (appLovinAd instanceof g) {
            g gVar = (g) appLovinAd;
            AppLovinAd dequeueAd = iVar.o().dequeueAd(gVar.getAdZone());
            StringBuilder sb = new StringBuilder();
            sb.append("Dequeued ad for dummy ad: ");
            sb.append(dequeueAd);
            iVar.v().b("Utils", sb.toString());
            if (dequeueAd != null) {
                gVar.a(dequeueAd);
                ((AppLovinAdBase) dequeueAd).setDummyAd(gVar);
                return dequeueAd;
            }
            appLovinAd = gVar.a();
        }
        return appLovinAd;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008f, code lost:
        if (r0.length() > r4) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a8, code lost:
        if (r0.length() > r4) goto L_0x00aa;
     */
    public static Object a(Object obj, i iVar) {
        int i;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            HashMap hashMap = new HashMap(map.size());
            for (Entry entry : map.entrySet()) {
                Object key = entry.getKey();
                hashMap.put(key instanceof String ? (String) key : String.valueOf(key), a(entry.getValue(), iVar));
            }
            return hashMap;
        } else if (obj instanceof List) {
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            for (Object a2 : list) {
                arrayList.add(a(a2, iVar));
            }
            return arrayList;
        } else if (obj instanceof Date) {
            return String.valueOf(((Date) obj).getTime());
        } else {
            String valueOf = String.valueOf(obj);
            if (obj instanceof String) {
                i = ((Integer) iVar.a(c.aV)).intValue();
                if (i > 0) {
                }
                return valueOf;
            }
            if (obj instanceof Uri) {
                i = ((Integer) iVar.a(c.aW)).intValue();
                if (i > 0) {
                }
            }
            return valueOf;
            valueOf = valueOf.substring(0, i);
            return valueOf;
        }
    }

    public static String a(Context context) {
        Bundle h = h(context);
        if (h == null) {
            return null;
        }
        String string = h.getString("applovin.sdk.key");
        if (string == null) {
            string = "";
        }
        return string;
    }

    public static String a(String str) {
        return (str == null || str.length() <= 4) ? "NOKEY" : str.substring(str.length() - 4);
    }

    public static String a(Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Entry entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append(RequestParameters.AMPERSAND);
            }
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue());
        }
        return sb.toString();
    }

    public static Field a(Class cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Class superclass = cls.getSuperclass();
            if (superclass == null) {
                return null;
            }
            return a(superclass, str);
        }
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, i iVar) {
        return a(str, jSONObject, str2, null, str3, iVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, String str4, i iVar) {
        HashMap hashMap = new HashMap(2);
        hashMap.put("{CLCODE}", str2);
        String str5 = "{EVENT_ID}";
        if (str3 == null) {
            str3 = "";
        }
        hashMap.put(str5, str3);
        return a(str, jSONObject, (Map<String, String>) hashMap, str4, iVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, Map<String, String> map, String str2, i iVar) {
        JSONObject b2 = i.b(jSONObject, str, new JSONObject(), iVar);
        ArrayList arrayList = new ArrayList(b2.length() + 1);
        if (m.b(str2)) {
            arrayList.add(new a(str2, null));
        }
        if (b2.length() > 0) {
            Iterator keys = b2.keys();
            while (keys.hasNext()) {
                try {
                    String str3 = (String) keys.next();
                    if (!TextUtils.isEmpty(str3)) {
                        String optString = b2.optString(str3);
                        String a2 = m.a(str3, map);
                        if (!TextUtils.isEmpty(optString)) {
                            optString = m.a(optString, map);
                        }
                        arrayList.add(new a(a2, optString));
                    }
                } catch (Throwable th) {
                    iVar.v().b("Utils", "Failed to create and add postback url.", th);
                }
            }
        }
        return arrayList;
    }

    private static List<Class> a(List<String> list, i iVar) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (String str : list) {
            try {
                arrayList.add(Class.forName(str));
            } catch (ClassNotFoundException unused) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to create class for name: ");
                sb.append(str);
                iVar.v().e("Utils", sb.toString());
            }
        }
        return arrayList;
    }

    public static void a(AppLovinAdLoadListener appLovinAdLoadListener, d dVar, int i, i iVar) {
        if (appLovinAdLoadListener != null) {
            try {
                if (appLovinAdLoadListener instanceof l) {
                    ((l) appLovinAdLoadListener).a(dVar, i);
                } else {
                    appLovinAdLoadListener.failedToReceiveAd(i);
                }
            } catch (Throwable th) {
                iVar.v().b("Utils", "Unable process a failure to receive an ad", th);
            }
        }
    }

    public static void a(Closeable closeable, i iVar) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                if (iVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable to close stream: ");
                    sb.append(closeable);
                    iVar.v().b("Utils", sb.toString(), th);
                }
            }
        }
    }

    public static void a(String str, Boolean bool, Map<String, String> map) {
        if (bool.booleanValue()) {
            map.put(str, Boolean.toString(true));
        }
    }

    public static void a(String str, String str2, Map<String, String> map) {
        if (!TextUtils.isEmpty(str2)) {
            map.put(str, str2);
        }
    }

    public static void a(String str, JSONObject jSONObject, i iVar) {
        if (jSONObject.has("no_fill_reason")) {
            Object a2 = i.a(jSONObject, "no_fill_reason", new Object(), iVar);
            StringBuilder sb = new StringBuilder();
            sb.append("\n**************************************************\nNO FILL received:\n..ID: \"");
            sb.append(str);
            sb.append("\"\n..SDK KEY: \"");
            sb.append(iVar.t());
            sb.append("\"\n..Reason: ");
            sb.append(a2);
            sb.append("\n**************************************************\n");
            o.i("AppLovinSdk", sb.toString());
        }
    }

    public static void a(HttpURLConnection httpURLConnection, i iVar) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Throwable th) {
                if (iVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable to disconnect connection: ");
                    sb.append(httpURLConnection);
                    iVar.v().b("Utils", sb.toString(), th);
                }
            }
        }
    }

    public static boolean a() {
        Context E = i.E();
        if (E == null) {
            return false;
        }
        Bundle h = h(E);
        return h != null && h.containsKey("applovin.sdk.verbose_logging");
    }

    public static boolean a(long j, long j2) {
        return (j & j2) != 0;
    }

    public static boolean a(Context context, Uri uri, i iVar) {
        boolean z;
        try {
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            if (!(context instanceof Activity)) {
                intent.setFlags(268435456);
            }
            iVar.Y().b();
            context.startActivity(intent);
            z = true;
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to open \"");
            sb.append(uri);
            sb.append("\".");
            iVar.v().b("Utils", sb.toString(), th);
            z = false;
        }
        if (!z) {
            iVar.Y().c();
        }
        return z;
    }

    public static boolean a(View view, Activity activity) {
        View rootView;
        if (!(activity == null || view == null)) {
            Window window = activity.getWindow();
            if (window != null) {
                rootView = window.getDecorView();
            } else {
                View findViewById = activity.findViewById(16908290);
                if (findViewById != null) {
                    rootView = findViewById.getRootView();
                }
            }
            return a(view, rootView);
        }
        return false;
    }

    public static boolean a(View view, View view2) {
        if (view == view2) {
            return true;
        }
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                if (a(view, viewGroup.getChildAt(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean a(Object obj, List<String> list, i iVar) {
        if (list == null) {
            return false;
        }
        for (Class isInstance : a(list, iVar)) {
            if (isInstance.isInstance(obj)) {
                if (obj instanceof Map) {
                    for (Entry entry : ((Map) obj).entrySet()) {
                        if (!(entry.getKey() instanceof String)) {
                            iVar.v().b("Utils", "Invalid key type used. Map keys should be of type String.");
                            return false;
                        } else if (!a(entry.getValue(), list, iVar)) {
                            return false;
                        }
                    }
                } else if (obj instanceof List) {
                    for (Object a2 : (List) obj) {
                        if (!a(a2, list, iVar)) {
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Object '");
        sb.append(obj);
        sb.append("' does not match any of the required types '");
        sb.append(list);
        sb.append("'.");
        iVar.v().b("Utils", sb.toString());
        return false;
    }

    public static boolean a(String str, List<String> list) {
        for (String startsWith : list) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static long b(float f) {
        return c(a(f));
    }

    public static String b(Class cls, String str) {
        try {
            Field a2 = a(cls, str);
            a2.setAccessible(true);
            return (String) a2.get(null);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static String b(String str) {
        return str.replace("{PLACEMENT}", "");
    }

    public static Map<String, String> b(Map<String, String> map) {
        HashMap hashMap = new HashMap(map);
        for (String str : hashMap.keySet()) {
            String str2 = (String) hashMap.get(str);
            if (str2 != null) {
                hashMap.put(str, m.d(str2));
            }
        }
        return hashMap;
    }

    public static void b(AppLovinAd appLovinAd, i iVar) {
        if (appLovinAd instanceof AppLovinAdBase) {
            AppLovinAdBase appLovinAdBase = (AppLovinAdBase) appLovinAd;
            String t = iVar.t();
            String t2 = appLovinAdBase.getSdk().t();
            if (!t.equals(t2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Ad was loaded from sdk with key: ");
                sb.append(t2);
                sb.append(", but is being rendered from sdk with key: ");
                sb.append(t);
                o.i("AppLovinAd", sb.toString());
                iVar.L().a(com.applovin.impl.sdk.c.g.l);
            }
        }
    }

    public static boolean b() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static boolean b(Context context) {
        if (context == null) {
            context = i.E();
        }
        if (context == null) {
            return false;
        }
        Bundle h = h(context);
        return h != null && h.getBoolean("applovin.sdk.test_ads", false);
    }

    public static boolean b(i iVar) {
        if (b == null) {
            try {
                Context E = i.E();
                StringBuilder sb = new StringBuilder();
                sb.append(E.getPackageName());
                sb.append(".BuildConfig");
                b = Boolean.valueOf(Class.forName(sb.toString()).getField("DEBUG").getBoolean(null));
            } catch (Throwable th) {
                iVar.v().b("Utils", "Failed to retrieve BuildConfig.DEBUG", th);
                b = Boolean.valueOf(false);
            }
        }
        return b.booleanValue();
    }

    private static long c(float f) {
        return (long) Math.round(f);
    }

    public static MaxAdFormat c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.equalsIgnoreCase("banner")) {
            return MaxAdFormat.BANNER;
        }
        if (str.equalsIgnoreCase("mrec")) {
            return MaxAdFormat.MREC;
        }
        if (str.equalsIgnoreCase("leaderboard") || str.equalsIgnoreCase("leader")) {
            return MaxAdFormat.LEADER;
        }
        if (str.equalsIgnoreCase(VideoType.INTERSTITIAL) || str.equalsIgnoreCase("inter")) {
            return MaxAdFormat.INTERSTITIAL;
        }
        if (str.equalsIgnoreCase("rewarded") || str.equalsIgnoreCase(MTGRewardVideoActivity.INTENT_REWARD)) {
            return MaxAdFormat.REWARDED;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Unknown format: ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }

    public static boolean c() {
        RunningAppProcessInfo runningAppProcessInfo = new RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(runningAppProcessInfo);
        return runningAppProcessInfo.importance == 100 || runningAppProcessInfo.importance == 200;
    }

    public static boolean c(Context context) {
        if (context == null) {
            context = i.E();
        }
        if (context == null) {
            return false;
        }
        Bundle h = h(context);
        return h != null && h.getBoolean("applovin.sdk.verbose_logging", false);
    }

    public static int d(Context context) {
        if (context != null) {
            Resources resources = context.getResources();
            if (resources != null) {
                Configuration configuration = resources.getConfiguration();
                if (configuration != null) {
                    return configuration.orientation;
                }
            }
        }
        return 0;
    }

    public static String d(String str) {
        Uri parse = Uri.parse(str);
        return new Builder().scheme(parse.getScheme()).authority(parse.getAuthority()).path(parse.getPath()).build().toString();
    }

    public static boolean d() {
        try {
            Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                String displayName = ((NetworkInterface) networkInterfaces.nextElement()).getDisplayName();
                if (displayName.contains("tun") || displayName.contains("ppp")) {
                    return true;
                }
                if (displayName.contains("ipsec")) {
                    return true;
                }
            }
        } catch (Throwable th) {
            o.c("Utils", "Unable to check Network Interfaces", th);
        }
        return false;
    }

    public static View e(Context context) {
        if (!(context instanceof Activity)) {
            return null;
        }
        return ((Activity) context).getWindow().getDecorView().findViewById(16908290);
    }

    public static boolean e(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            Class.forName(str);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    public static long f(String str) {
        if (m.b(str)) {
            try {
                return (long) Color.parseColor(str);
            } catch (Throwable unused) {
            }
        }
        return Long.MAX_VALUE;
    }

    public static String f(Context context) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setPackage(context.getPackageName());
        List queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (!queryIntentActivities.isEmpty()) {
            return ((ResolveInfo) queryIntentActivities.get(0)).activityInfo.name;
        }
        return null;
    }

    public static int g(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager == null) {
            return 0;
        }
        return windowManager.getDefaultDisplay().getRotation();
    }

    public static int g(String str) {
        String[] split;
        int i = 0;
        for (String str2 : str.split("\\.")) {
            if (str2.length() > 2) {
                StringBuilder sb = new StringBuilder();
                sb.append("Version number components cannot be longer than two digits -> ");
                sb.append(str);
                o.i("Utils", sb.toString());
                return i;
            }
            i = (i * 100) + Integer.valueOf(str2).intValue();
        }
        return i;
    }

    private static Bundle h(Context context) {
        if (f1474a != null) {
            return f1474a.metaData;
        }
        try {
            f1474a = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            return f1474a.metaData;
        } catch (NameNotFoundException unused) {
            return null;
        }
    }
}
