package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.view.WindowManager;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public class g {
    public static Point a(Context context) {
        Point point = new Point();
        point.x = 480;
        point.y = ModuleDescriptor.MODULE_VERSION;
        try {
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getSize(point);
        } catch (Throwable unused) {
        }
        return point;
    }

    public static void a() {
        try {
            StrictMode.setThreadPolicy(new Builder().permitAll().build());
        } catch (Throwable unused) {
        }
    }

    public static boolean a(String str, Context context) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    public static boolean b() {
        return VERSION.SDK_INT >= 14;
    }

    public static boolean c() {
        return VERSION.SDK_INT >= 16;
    }

    public static boolean d() {
        return VERSION.SDK_INT >= 17;
    }

    public static boolean e() {
        return VERSION.SDK_INT >= 19;
    }

    public static boolean f() {
        return VERSION.SDK_INT >= 21;
    }

    public static boolean g() {
        return VERSION.SDK_INT >= 23;
    }

    public static boolean h() {
        return VERSION.SDK_INT >= 24;
    }

    public static boolean i() {
        return VERSION.SDK_INT >= 29;
    }
}
