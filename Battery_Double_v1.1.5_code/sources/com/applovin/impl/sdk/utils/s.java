package com.applovin.impl.sdk.utils;

import android.util.Xml;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class s {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final o f1476a;
    /* access modifiers changed from: private */
    public Stack<a> b;
    /* access modifiers changed from: private */
    public StringBuilder c;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public a e;

    private static class a extends r {
        a(String str, Map<String, String> map, r rVar) {
            super(str, map, rVar);
        }

        /* access modifiers changed from: 0000 */
        public void a(r rVar) {
            if (rVar != null) {
                this.c.add(rVar);
                return;
            }
            throw new IllegalArgumentException("None specified.");
        }

        /* access modifiers changed from: 0000 */
        public void d(String str) {
            this.b = str;
        }
    }

    s(i iVar) {
        if (iVar != null) {
            this.f1476a = iVar.v();
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    public static r a(String str, i iVar) throws SAXException {
        return new s(iVar).a(str);
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(Attributes attributes) {
        if (attributes == null) {
            return Collections.emptyMap();
        }
        int length = attributes.getLength();
        HashMap hashMap = new HashMap(length);
        for (int i = 0; i < length; i++) {
            hashMap.put(attributes.getQName(i), attributes.getValue(i));
        }
        return hashMap;
    }

    public r a(String str) throws SAXException {
        if (str != null) {
            this.c = new StringBuilder();
            this.b = new Stack<>();
            this.e = null;
            Xml.parse(str, new ContentHandler() {
                public void characters(char[] cArr, int i, int i2) {
                    String trim = new String(Arrays.copyOfRange(cArr, i, i2)).trim();
                    if (m.b(trim)) {
                        s.this.c.append(trim);
                    }
                }

                public void endDocument() {
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - s.this.d;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Finished parsing in ");
                    sb.append(seconds);
                    sb.append(" seconds");
                    s.this.f1476a.b("XmlParser", sb.toString());
                }

                public void endElement(String str, String str2, String str3) {
                    s.this.e = (a) s.this.b.pop();
                    s.this.e.d(s.this.c.toString().trim());
                    s.this.c.setLength(0);
                }

                public void endPrefixMapping(String str) {
                }

                public void ignorableWhitespace(char[] cArr, int i, int i2) {
                }

                public void processingInstruction(String str, String str2) {
                }

                public void setDocumentLocator(Locator locator) {
                }

                public void skippedEntity(String str) {
                }

                public void startDocument() {
                    s.this.f1476a.b("XmlParser", "Begin parsing...");
                    s.this.d = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                }

                public void startElement(String str, String str2, String str3, Attributes attributes) throws SAXException {
                    a aVar = null;
                    try {
                        if (!s.this.b.isEmpty()) {
                            aVar = (a) s.this.b.peek();
                        }
                        a aVar2 = new a(str2, s.this.a(attributes), aVar);
                        if (aVar != null) {
                            aVar.a(aVar2);
                        }
                        s.this.b.push(aVar2);
                    } catch (Exception e) {
                        o a2 = s.this.f1476a;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unable to process element <");
                        sb.append(str2);
                        sb.append(">");
                        a2.b("XmlParser", sb.toString(), e);
                        throw new SAXException("Failed to start element", e);
                    }
                }

                public void startPrefixMapping(String str, String str2) {
                }
            });
            if (this.e != null) {
                return this.e;
            }
            throw new SAXException("Unable to parse XML into node");
        }
        throw new IllegalArgumentException("Unable to parse. No XML specified.");
    }
}
