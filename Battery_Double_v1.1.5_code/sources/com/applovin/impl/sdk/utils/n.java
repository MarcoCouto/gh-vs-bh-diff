package com.applovin.impl.sdk.utils;

import com.applovin.impl.sdk.i;
import java.util.Timer;
import java.util.TimerTask;

public class n {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final i f1471a;
    /* access modifiers changed from: private */
    public Timer b;
    private long c;
    private long d;
    /* access modifiers changed from: private */
    public final Runnable e;
    private long f;
    /* access modifiers changed from: private */
    public final Object g = new Object();

    private n(i iVar, Runnable runnable) {
        this.f1471a = iVar;
        this.e = runnable;
    }

    public static n a(long j, i iVar, Runnable runnable) {
        if (j < 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot create a scheduled timer. Invalid fire time passed in: ");
            sb.append(j);
            sb.append(".");
            throw new IllegalArgumentException(sb.toString());
        } else if (runnable != null) {
            n nVar = new n(iVar, runnable);
            nVar.c = System.currentTimeMillis();
            nVar.d = j;
            try {
                nVar.b = new Timer();
                nVar.b.schedule(nVar.e(), j);
            } catch (OutOfMemoryError e2) {
                iVar.v().b("Timer", "Failed to create timer due to OOM error", e2);
            }
            return nVar;
        } else {
            throw new IllegalArgumentException("Cannot create a scheduled timer. Runnable is null.");
        }
    }

    private TimerTask e() {
        return new TimerTask() {
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            public void run() {
                try {
                    n.this.e.run();
                    synchronized (n.this.g) {
                        n.this.b = null;
                    }
                } catch (Throwable th) {
                    try {
                        if (n.this.f1471a != null) {
                            n.this.f1471a.v().b("Timer", "Encountered error while executing timed task", th);
                        }
                        synchronized (n.this.g) {
                            n.this.b = null;
                        }
                    } catch (Throwable th2) {
                        synchronized (n.this.g) {
                            n.this.b = null;
                            throw th2;
                        }
                    }
                }
            }
        };
    }

    public long a() {
        if (this.b == null) {
            return this.d - this.f;
        }
        return this.d - (System.currentTimeMillis() - this.c);
    }

    public void b() {
        synchronized (this.g) {
            if (this.b != null) {
                try {
                    this.b.cancel();
                    this.f = System.currentTimeMillis() - this.c;
                } catch (Throwable th) {
                    try {
                        if (this.f1471a != null) {
                            this.f1471a.v().b("Timer", "Encountered error while pausing timer", th);
                        }
                    } catch (Throwable th2) {
                        this.b = null;
                        throw th2;
                    }
                }
                this.b = null;
            }
        }
    }

    public void c() {
        synchronized (this.g) {
            if (this.f > 0) {
                try {
                    this.d -= this.f;
                    if (this.d < 0) {
                        this.d = 0;
                    }
                    this.b = new Timer();
                    this.b.schedule(e(), this.d);
                    this.c = System.currentTimeMillis();
                } catch (Throwable th) {
                    try {
                        if (this.f1471a != null) {
                            this.f1471a.v().b("Timer", "Encountered error while resuming timer", th);
                        }
                    } catch (Throwable th2) {
                        this.f = 0;
                        throw th2;
                    }
                }
                this.f = 0;
            }
        }
    }

    public void d() {
        synchronized (this.g) {
            if (this.b != null) {
                try {
                    this.b.cancel();
                    this.b = null;
                } catch (Throwable th) {
                    try {
                        if (this.f1471a != null) {
                            this.f1471a.v().b("Timer", "Encountered error while cancelling timer", th);
                        }
                        this.b = null;
                    } catch (Throwable th2) {
                        this.b = null;
                        this.f = 0;
                        throw th2;
                    }
                }
                this.f = 0;
            }
        }
    }
}
