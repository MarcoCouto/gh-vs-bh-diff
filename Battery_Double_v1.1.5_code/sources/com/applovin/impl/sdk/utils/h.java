package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.security.NetworkSecurityPolicy;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.sdk.precache.DownloadManager;
import com.tapjoy.TapjoyConstants;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class h {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f1443a = {7, 4, 2, 1, 11};
    private static final int[] b = {5, 6, 10, 3, 9, 8, 14};
    private static final int[] c = {15, 12, 13};

    public static String a(InputStream inputStream, i iVar) {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[((Integer) iVar.a(c.dB)).intValue()];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    return byteArrayOutputStream.toString("UTF-8");
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (Throwable th) {
            iVar.v().b("ConnectionUtils", "Encountered error while reading stream", th);
            return null;
        }
    }

    public static String a(String str, i iVar) {
        return a((String) iVar.a(c.aI), str, iVar);
    }

    public static String a(String str, String str2, i iVar) {
        if (str == null || str.length() < 4) {
            throw new IllegalArgumentException("Invalid domain specified");
        } else if (str2 == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (iVar != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            return sb.toString();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static JSONObject a(JSONObject jSONObject) throws JSONException {
        return (JSONObject) jSONObject.getJSONArray("results").get(0);
    }

    public static void a(int i, i iVar) {
        String str;
        StringBuilder sb;
        String str2;
        d C = iVar.C();
        if (i == 401) {
            C.a(c.S, (Object) "");
            C.a(c.T, (Object) "");
            C.a();
            str = "AppLovinSdk";
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(iVar.t());
            str2 = "\" is rejected by AppLovin. Please make sure the SDK key is correct.";
        } else if (i == 418) {
            C.a(c.R, (Object) Boolean.valueOf(true));
            C.a();
            str = "AppLovinSdk";
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(iVar.t());
            str2 = "\" has been blocked. Please contact AppLovin support at support@applovin.com.";
        } else if ((i >= 400 && i < 500) || i == -1) {
            iVar.f();
            return;
        } else {
            return;
        }
        sb.append(str2);
        o.i(str, sb.toString());
    }

    public static void a(JSONObject jSONObject, boolean z, i iVar) {
        iVar.ac().a(jSONObject, z);
    }

    public static boolean a() {
        return a((String) null);
    }

    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(Context context) {
        if (context.getSystemService("connectivity") == null) {
            return true;
        }
        NetworkInfo b2 = b(context);
        if (b2 != null) {
            return b2.isConnected();
        }
        return false;
    }

    public static boolean a(String str) {
        if (g.g()) {
            return (!g.h() || TextUtils.isEmpty(str)) ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(str);
        }
        return true;
    }

    private static NetworkInfo b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            return connectivityManager.getActiveNetworkInfo();
        }
        return null;
    }

    public static String b(String str, i iVar) {
        return a((String) iVar.a(c.aJ), str, iVar);
    }

    public static void c(JSONObject jSONObject, i iVar) {
        String b2 = i.b(jSONObject, "persisted_data", (String) null, iVar);
        if (m.b(b2)) {
            iVar.a(e.x, b2);
            iVar.v().c("ConnectionUtils", "Updated persisted data");
        }
    }

    public static void d(JSONObject jSONObject, i iVar) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (iVar != null) {
            try {
                if (jSONObject.has(DownloadManager.SETTINGS)) {
                    d C = iVar.C();
                    if (!jSONObject.isNull(DownloadManager.SETTINGS)) {
                        C.a(jSONObject.getJSONObject(DownloadManager.SETTINGS));
                        C.a();
                        iVar.v().b("ConnectionUtils", "New settings processed");
                    }
                }
            } catch (JSONException e) {
                iVar.v().b("ConnectionUtils", "Unable to parse settings out of API response", e);
            }
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static Map<String, String> e(i iVar) {
        HashMap hashMap = new HashMap();
        String str = (String) iVar.a(c.T);
        if (m.b(str)) {
            hashMap.put("device_token", str);
        } else if (!((Boolean) iVar.a(c.eJ)).booleanValue()) {
            hashMap.put(TapjoyConstants.TJC_API_KEY, iVar.t());
        }
        hashMap.put("sc", m.d((String) iVar.a(c.V)));
        hashMap.put("sc2", m.d((String) iVar.a(c.W)));
        hashMap.put("server_installed_at", m.d((String) iVar.a(c.X)));
        p.a("persisted_data", m.d((String) iVar.a(e.x)), (Map<String, String>) hashMap);
        return hashMap;
    }

    public static void e(JSONObject jSONObject, i iVar) {
        JSONArray b2 = i.b(jSONObject, "zones", (JSONArray) null, iVar);
        if (b2 != null) {
            Iterator it = iVar.W().a(b2).iterator();
            while (it.hasNext()) {
                com.applovin.impl.sdk.ad.d dVar = (com.applovin.impl.sdk.ad.d) it.next();
                if (dVar.d()) {
                    iVar.p().preloadAds(dVar);
                } else {
                    iVar.o().preloadAds(dVar);
                }
            }
            iVar.T().a(iVar.W().a());
            iVar.U().a(iVar.W().a());
        }
    }

    public static String f(i iVar) {
        NetworkInfo b2 = b(iVar.D());
        if (b2 == null) {
            return "unknown";
        }
        int type = b2.getType();
        int subtype = b2.getSubtype();
        String str = type == 1 ? "wifi" : type == 0 ? a(subtype, f1443a) ? "2g" : a(subtype, b) ? ConnectivityService.NETWORK_TYPE_3G : a(subtype, c) ? "4g" : TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE : "unknown";
        return str;
    }

    public static void f(JSONObject jSONObject, i iVar) {
        JSONObject b2 = i.b(jSONObject, "variables", (JSONObject) null, iVar);
        if (b2 != null) {
            iVar.s().updateVariables(b2);
        }
    }

    public static String g(i iVar) {
        return a((String) iVar.a(c.aG), "4.0/ad", iVar);
    }

    public static String h(i iVar) {
        return a((String) iVar.a(c.aH), "4.0/ad", iVar);
    }

    public static String i(i iVar) {
        return a((String) iVar.a(c.aM), "1.0/variable_config", iVar);
    }

    public static String j(i iVar) {
        return a((String) iVar.a(c.aN), "1.0/variable_config", iVar);
    }
}
