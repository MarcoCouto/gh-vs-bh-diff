package com.applovin.impl.sdk.utils;

import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.applovin.adview.AppLovinAdView;
import com.applovin.sdk.AppLovinAdSize;

public class b {
    public static AppLovinAdSize a(AttributeSet attributeSet) {
        AppLovinAdSize appLovinAdSize = null;
        if (attributeSet == null) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(AppLovinAdView.NAMESPACE, "size");
        if (m.b(attributeValue)) {
            appLovinAdSize = AppLovinAdSize.fromString(attributeValue);
        }
        return appLovinAdSize;
    }

    public static void a(ViewGroup viewGroup, View view) {
        if (viewGroup != null) {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt != view) {
                    viewGroup.removeView(childAt);
                }
            }
        }
    }

    public static boolean b(AttributeSet attributeSet) {
        return attributeSet != null && attributeSet.getAttributeBooleanValue(AppLovinAdView.NAMESPACE, "loadAdOnCreate", false);
    }
}
