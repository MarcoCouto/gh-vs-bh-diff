package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.m;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.LinkedHashSet;
import java.util.List;

public class c extends q {
    c(i iVar) {
        super(iVar);
    }

    /* access modifiers changed from: 0000 */
    public d a(j jVar) {
        return ((AppLovinAdBase) jVar).getAdZone();
    }

    /* access modifiers changed from: 0000 */
    public a a(d dVar) {
        m mVar = new m(dVar, this, this.f1431a);
        mVar.a(true);
        return mVar;
    }

    public void a() {
        for (d dVar : d.b(this.f1431a)) {
            if (!dVar.d()) {
                h(dVar);
            }
        }
    }

    public void a(d dVar, int i) {
        c(dVar, i);
    }

    /* access modifiers changed from: 0000 */
    public void a(Object obj, d dVar, int i) {
        if (obj instanceof l) {
            ((l) obj).a(dVar, i);
        }
        if (obj instanceof AppLovinAdLoadListener) {
            ((AppLovinAdLoadListener) obj).failedToReceiveAd(i);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Object obj, j jVar) {
        ((AppLovinAdLoadListener) obj).adReceived((AppLovinAd) jVar);
    }

    public /* bridge */ /* synthetic */ void a(LinkedHashSet linkedHashSet) {
        super.a(linkedHashSet);
    }

    public /* bridge */ /* synthetic */ boolean a(d dVar, Object obj) {
        return super.a(dVar, obj);
    }

    public void adReceived(AppLovinAd appLovinAd) {
        b((j) appLovinAd);
    }

    public /* bridge */ /* synthetic */ void b(d dVar, int i) {
        super.b(dVar, i);
    }

    public /* bridge */ /* synthetic */ boolean b(d dVar) {
        return super.b(dVar);
    }

    public /* bridge */ /* synthetic */ j c(d dVar) {
        return super.c(dVar);
    }

    public /* bridge */ /* synthetic */ j d(d dVar) {
        return super.d(dVar);
    }

    public /* bridge */ /* synthetic */ j e(d dVar) {
        return super.e(dVar);
    }

    public /* bridge */ /* synthetic */ void f(d dVar) {
        super.f(dVar);
    }

    public void failedToReceiveAd(int i) {
    }

    public /* bridge */ /* synthetic */ boolean g(d dVar) {
        return super.g(dVar);
    }

    public /* bridge */ /* synthetic */ void h(d dVar) {
        super.h(dVar);
    }

    public /* bridge */ /* synthetic */ void i(d dVar) {
        super.i(dVar);
    }

    public void onNativeAdsFailedToLoad(int i) {
    }

    public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
    }
}
