package com.applovin.impl.adview;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.sdk.AppLovinSdkUtils;

class p extends FrameLayout implements SurfaceTextureListener, t {

    /* renamed from: a reason: collision with root package name */
    private final o f1131a;
    private final TextureView b;
    private final MediaPlayer c = new MediaPlayer();
    /* access modifiers changed from: private */
    public final a d;
    private int e;
    private int f;
    private int g;

    interface a {
        void a(String str);
    }

    p(i iVar, Context context, a aVar) {
        super(context);
        this.f1131a = iVar.v();
        this.d = aVar;
        this.b = new TextureView(context);
        this.b.setLayoutParams(new LayoutParams(-1, -1, 17));
        this.b.setSurfaceTextureListener(this);
        addView(this.b);
    }

    private void a(final String str) {
        this.f1131a.e("TextureVideoView", str);
        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
            public void run() {
                p.this.d.a(str);
            }
        }, 250);
    }

    public int getCurrentPosition() {
        return this.c.getCurrentPosition();
    }

    public int getDuration() {
        return this.c.getDuration();
    }

    public boolean isPlaying() {
        return this.c.isPlaying();
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        Surface surface = new Surface(surfaceTexture);
        try {
            this.c.setSurface(surface);
            this.c.setAudioStreamType(3);
            this.c.prepareAsync();
        } catch (Throwable unused) {
            surface.release();
            a("Failed to prepare media player");
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void pause() {
        this.c.pause();
    }

    public void seekTo(int i) {
        this.c.seekTo(i);
    }

    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {
        this.c.setOnCompletionListener(onCompletionListener);
    }

    public void setOnErrorListener(OnErrorListener onErrorListener) {
        this.c.setOnErrorListener(onErrorListener);
    }

    public void setOnPreparedListener(OnPreparedListener onPreparedListener) {
        this.c.setOnPreparedListener(onPreparedListener);
    }

    public void setVideoSize(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int d2 = com.applovin.impl.sdk.utils.p.d(getContext());
        if (this.e == 0) {
            i4 = this.b.getWidth();
            i3 = this.b.getHeight();
            this.e = d2;
            this.f = i4;
            this.g = i3;
        } else if (d2 == this.e) {
            i4 = this.f;
            i3 = this.g;
        } else {
            i4 = this.g;
            i3 = this.f;
        }
        float f2 = ((float) i2) / ((float) i);
        float f3 = (float) i4;
        int i6 = (int) (f3 * f2);
        if (i3 >= i6) {
            i5 = i4;
        } else {
            i5 = (int) (((float) i3) / f2);
            i6 = i3;
        }
        try {
            int i7 = (i4 - i5) / 2;
            int i8 = (i3 - i6) / 2;
            Matrix matrix = new Matrix();
            this.b.getTransform(matrix);
            matrix.setScale(((float) i5) / f3, ((float) i6) / ((float) i3));
            matrix.postTranslate((float) i7, (float) i8);
            this.b.setTransform(matrix);
            invalidate();
            requestLayout();
        } catch (Throwable unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to set video size to width: ");
            sb.append(i);
            sb.append(" height: ");
            sb.append(i2);
            a(sb.toString());
        }
    }

    public void setVideoURI(Uri uri) {
        try {
            this.c.setDataSource(uri.toString());
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to set video URI: ");
            sb.append(uri);
            sb.append(" at ");
            sb.append(th);
            a(sb.toString());
        }
    }

    public void start() {
        this.c.start();
    }

    public void stopPlayback() {
        this.c.stop();
    }
}
