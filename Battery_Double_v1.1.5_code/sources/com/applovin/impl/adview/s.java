package com.applovin.impl.adview;

import com.applovin.impl.sdk.i;
import com.tapjoy.TJAdUnitConstants;
import org.json.JSONObject;

public class s {

    /* renamed from: a reason: collision with root package name */
    private final int f1135a;
    private final int b;
    private final int c;
    private final int d;
    private final boolean e;
    private final int f;
    private final int g;
    private final int h;
    private final float i;
    private final float j;

    public s(JSONObject jSONObject, i iVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Updating video button properties with JSON = ");
        sb.append(com.applovin.impl.sdk.utils.i.d(jSONObject));
        iVar.v().c("VideoButtonProperties", sb.toString());
        this.f1135a = com.applovin.impl.sdk.utils.i.b(jSONObject, "width", 64, iVar);
        this.b = com.applovin.impl.sdk.utils.i.b(jSONObject, "height", 7, iVar);
        this.c = com.applovin.impl.sdk.utils.i.b(jSONObject, "margin", 20, iVar);
        this.d = com.applovin.impl.sdk.utils.i.b(jSONObject, "gravity", 85, iVar);
        this.e = com.applovin.impl.sdk.utils.i.a(jSONObject, "tap_to_fade", Boolean.valueOf(false), iVar).booleanValue();
        this.f = com.applovin.impl.sdk.utils.i.b(jSONObject, "tap_to_fade_duration_milliseconds", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL, iVar);
        this.g = com.applovin.impl.sdk.utils.i.b(jSONObject, "fade_in_duration_milliseconds", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL, iVar);
        this.h = com.applovin.impl.sdk.utils.i.b(jSONObject, "fade_out_duration_milliseconds", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL, iVar);
        this.i = com.applovin.impl.sdk.utils.i.a(jSONObject, "fade_in_delay_seconds", 1.0f, iVar);
        this.j = com.applovin.impl.sdk.utils.i.a(jSONObject, "fade_out_delay_seconds", 6.0f, iVar);
    }

    public int a() {
        return this.f1135a;
    }

    public int b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }

    public int d() {
        return this.d;
    }

    public boolean e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        s sVar = (s) obj;
        if (this.f1135a != sVar.f1135a || this.b != sVar.b || this.c != sVar.c || this.d != sVar.d || this.e != sVar.e || this.f != sVar.f || this.g != sVar.g || this.h != sVar.h || Float.compare(sVar.i, this.i) != 0) {
            return false;
        }
        if (Float.compare(sVar.j, this.j) != 0) {
            z = false;
        }
        return z;
    }

    public long f() {
        return (long) this.f;
    }

    public long g() {
        return (long) this.g;
    }

    public long h() {
        return (long) this.h;
    }

    public int hashCode() {
        int i2 = 0;
        int floatToIntBits = ((((((((((((((((this.f1135a * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + (this.e ? 1 : 0)) * 31) + this.f) * 31) + this.g) * 31) + this.h) * 31) + (this.i != 0.0f ? Float.floatToIntBits(this.i) : 0)) * 31;
        if (this.j != 0.0f) {
            i2 = Float.floatToIntBits(this.j);
        }
        return floatToIntBits + i2;
    }

    public float i() {
        return this.i;
    }

    public float j() {
        return this.j;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VideoButtonProperties{widthPercentOfScreen=");
        sb.append(this.f1135a);
        sb.append(", heightPercentOfScreen=");
        sb.append(this.b);
        sb.append(", margin=");
        sb.append(this.c);
        sb.append(", gravity=");
        sb.append(this.d);
        sb.append(", tapToFade=");
        sb.append(this.e);
        sb.append(", tapToFadeDurationMillis=");
        sb.append(this.f);
        sb.append(", fadeInDurationMillis=");
        sb.append(this.g);
        sb.append(", fadeOutDurationMillis=");
        sb.append(this.h);
        sb.append(", fadeInDelay=");
        sb.append(this.i);
        sb.append(", fadeOutDelay=");
        sb.append(this.j);
        sb.append('}');
        return sb.toString();
    }
}
