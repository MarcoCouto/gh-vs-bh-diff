package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import com.applovin.impl.a.b;
import com.applovin.impl.a.e;
import com.applovin.impl.sdk.ad.a;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.h;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;

class c extends g {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final o f1054a;
    private final i b;
    private d c;
    private AppLovinAd d = null;
    private boolean e = false;

    c(d dVar, i iVar, Context context) {
        super(context);
        if (iVar != null) {
            this.b = iVar;
            this.f1054a = iVar.v();
            setBackgroundColor(0);
            WebSettings settings = getSettings();
            settings.setSupportMultipleWindows(false);
            settings.setJavaScriptEnabled(true);
            setWebViewClient(dVar);
            setWebChromeClient(new b(iVar));
            setVerticalScrollBarEnabled(false);
            setHorizontalScrollBarEnabled(false);
            setScrollBarStyle(33554432);
            if (g.i()) {
                setWebViewRenderProcessClient(new e(iVar));
            }
            setOnTouchListener(new OnTouchListener() {
                @SuppressLint({"ClickableViewAccessibility"})
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (!view.hasFocus()) {
                        view.requestFocus();
                    }
                    return false;
                }
            });
            setOnLongClickListener(new OnLongClickListener() {
                public boolean onLongClick(View view) {
                    c.this.f1054a.b("AdWebView", "Received a LongClick event.");
                    return true;
                }
            });
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    private String a(String str, String str2) {
        if (m.b(str)) {
            return p.b(str).replace("{SOURCE}", str2);
        }
        return null;
    }

    private void a(final f fVar) {
        try {
            if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.eP)).booleanValue() || fVar.ap()) {
                a((Runnable) new Runnable() {
                    public void run() {
                        c.this.loadUrl("about:blank");
                    }
                });
            }
            if (g.d()) {
                a((Runnable) new Runnable() {
                    @TargetApi(17)
                    public void run() {
                        c.this.getSettings().setMediaPlaybackRequiresUserGesture(fVar.ao());
                    }
                });
            }
            if (g.e() && fVar.ar()) {
                a((Runnable) new Runnable() {
                    @TargetApi(19)
                    public void run() {
                        WebView.setWebContentsDebuggingEnabled(true);
                    }
                });
            }
            w as = fVar.as();
            if (as != null) {
                final WebSettings settings = getSettings();
                final PluginState b2 = as.b();
                if (b2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setPluginState(b2);
                        }
                    });
                }
                final Boolean c2 = as.c();
                if (c2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setAllowFileAccess(c2.booleanValue());
                        }
                    });
                }
                final Boolean d2 = as.d();
                if (d2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setLoadWithOverviewMode(d2.booleanValue());
                        }
                    });
                }
                final Boolean e2 = as.e();
                if (e2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setUseWideViewPort(e2.booleanValue());
                        }
                    });
                }
                final Boolean f = as.f();
                if (f != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setAllowContentAccess(f.booleanValue());
                        }
                    });
                }
                final Boolean g = as.g();
                if (g != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setBuiltInZoomControls(g.booleanValue());
                        }
                    });
                }
                final Boolean h = as.h();
                if (h != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setDisplayZoomControls(h.booleanValue());
                        }
                    });
                }
                final Boolean i = as.i();
                if (i != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setSaveFormData(i.booleanValue());
                        }
                    });
                }
                final Boolean j = as.j();
                if (j != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setGeolocationEnabled(j.booleanValue());
                        }
                    });
                }
                final Boolean k = as.k();
                if (k != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setNeedInitialFocus(k.booleanValue());
                        }
                    });
                }
                if (g.c()) {
                    final Boolean l = as.l();
                    if (l != null) {
                        a((Runnable) new Runnable() {
                            @TargetApi(16)
                            public void run() {
                                settings.setAllowFileAccessFromFileURLs(l.booleanValue());
                            }
                        });
                    }
                    final Boolean m = as.m();
                    if (m != null) {
                        a((Runnable) new Runnable() {
                            @TargetApi(16)
                            public void run() {
                                settings.setAllowUniversalAccessFromFileURLs(m.booleanValue());
                            }
                        });
                    }
                }
                if (g.f()) {
                    final Integer a2 = as.a();
                    if (a2 != null) {
                        a((Runnable) new Runnable() {
                            @TargetApi(21)
                            public void run() {
                                settings.setMixedContentMode(a2.intValue());
                            }
                        });
                    }
                }
                if (g.g()) {
                    final Boolean n = as.n();
                    if (n != null) {
                        a((Runnable) new Runnable() {
                            @TargetApi(23)
                            public void run() {
                                settings.setOffscreenPreRaster(n.booleanValue());
                            }
                        });
                    }
                }
            }
        } catch (Throwable th) {
            this.f1054a.b("AdWebView", "Unable to apply WebView settings", th);
        }
    }

    private void a(Runnable runnable) {
        try {
            runnable.run();
        } catch (Throwable th) {
            this.f1054a.b("AdWebView", "Unable to apply WebView setting", th);
        }
    }

    private void a(String str, String str2, String str3, i iVar) {
        o oVar;
        String str4;
        StringBuilder sb;
        String a2 = a(str3, str);
        if (m.b(a2)) {
            oVar = this.f1054a;
            str4 = "AdWebView";
            sb = new StringBuilder();
        } else {
            a2 = a((String) iVar.a(com.applovin.impl.sdk.b.c.ez), str);
            if (m.b(a2)) {
                oVar = this.f1054a;
                str4 = "AdWebView";
                sb = new StringBuilder();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Rendering webview for VAST ad with resourceURL : ");
                sb2.append(str);
                this.f1054a.b("AdWebView", sb2.toString());
                loadUrl(str);
                return;
            }
        }
        sb.append("Rendering webview for VAST ad with resourceContents : ");
        sb.append(a2);
        oVar.b(str4, sb.toString());
        loadDataWithBaseURL(str2, a2, WebRequest.CONTENT_TYPE_HTML, null, "");
    }

    /* access modifiers changed from: 0000 */
    public AppLovinAd a() {
        return this.d;
    }

    public void a(d dVar) {
        this.c = dVar;
    }

    public void a(AppLovinAd appLovinAd) {
        o oVar;
        String str;
        String str2;
        o oVar2;
        String str3;
        String str4;
        String str5;
        String aq;
        String str6;
        String str7;
        String str8;
        String aq2;
        i iVar;
        if (!this.e) {
            this.d = appLovinAd;
            try {
                if (appLovinAd instanceof h) {
                    loadDataWithBaseURL("/", ((h) appLovinAd).a(), WebRequest.CONTENT_TYPE_HTML, null, "");
                    oVar = this.f1054a;
                    str = "AdWebView";
                    str2 = "Empty ad rendered";
                } else {
                    f fVar = (f) appLovinAd;
                    a(fVar);
                    if (fVar.af()) {
                        setVisibility(0);
                    }
                    if (appLovinAd instanceof a) {
                        loadDataWithBaseURL(fVar.aq(), p.b(((a) appLovinAd).a()), WebRequest.CONTENT_TYPE_HTML, null, "");
                        oVar = this.f1054a;
                        str = "AdWebView";
                        str2 = "AppLovinAd rendered";
                    } else if (appLovinAd instanceof com.applovin.impl.a.a) {
                        com.applovin.impl.a.a aVar = (com.applovin.impl.a.a) appLovinAd;
                        b j = aVar.j();
                        if (j != null) {
                            e b2 = j.b();
                            Uri b3 = b2.b();
                            String uri = b3 != null ? b3.toString() : "";
                            String c2 = b2.c();
                            String aD = aVar.aD();
                            if (!m.b(uri)) {
                                if (!m.b(c2)) {
                                    oVar2 = this.f1054a;
                                    str3 = "AdWebView";
                                    str4 = "Unable to load companion ad. No resources provided.";
                                    oVar2.e(str3, str4);
                                    return;
                                }
                            }
                            if (b2.a() == e.a.STATIC) {
                                this.f1054a.b("AdWebView", "Rendering WebView for static VAST ad");
                                loadDataWithBaseURL(fVar.aq(), a((String) this.b.a(com.applovin.impl.sdk.b.c.ey), uri), WebRequest.CONTENT_TYPE_HTML, null, "");
                                return;
                            }
                            if (b2.a() == e.a.HTML) {
                                if (m.b(c2)) {
                                    String a2 = a(aD, c2);
                                    str5 = m.b(a2) ? a2 : c2;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Rendering WebView for HTML VAST ad with resourceContents: ");
                                    sb.append(str5);
                                    this.f1054a.b("AdWebView", sb.toString());
                                    aq = fVar.aq();
                                    str6 = WebRequest.CONTENT_TYPE_HTML;
                                    str7 = null;
                                    str8 = "";
                                } else if (m.b(uri)) {
                                    this.f1054a.b("AdWebView", "Preparing to load HTML VAST ad resourceUri");
                                    aq2 = fVar.aq();
                                    iVar = this.b;
                                    a(uri, aq2, aD, iVar);
                                    return;
                                } else {
                                    return;
                                }
                            } else if (b2.a() != e.a.IFRAME) {
                                oVar2 = this.f1054a;
                                str3 = "AdWebView";
                                str4 = "Failed to render VAST companion ad of invalid type";
                                oVar2.e(str3, str4);
                                return;
                            } else if (m.b(uri)) {
                                this.f1054a.b("AdWebView", "Preparing to load iFrame VAST ad resourceUri");
                                aq2 = fVar.aq();
                                iVar = this.b;
                                a(uri, aq2, aD, iVar);
                                return;
                            } else if (m.b(c2)) {
                                String a3 = a(aD, c2);
                                str5 = m.b(a3) ? a3 : c2;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("Rendering WebView for iFrame VAST ad with resourceContents: ");
                                sb2.append(str5);
                                this.f1054a.b("AdWebView", sb2.toString());
                                aq = fVar.aq();
                                str6 = WebRequest.CONTENT_TYPE_HTML;
                                str7 = null;
                                str8 = "";
                            } else {
                                return;
                            }
                            loadDataWithBaseURL(aq, str5, str6, str7, str8);
                            return;
                        }
                        oVar = this.f1054a;
                        str = "AdWebView";
                        str2 = "No companion ad provided.";
                    } else {
                        return;
                    }
                }
                oVar.b(str, str2);
            } catch (Throwable th) {
                this.f1054a.b("AdWebView", "Unable to render AppLovinAd", th);
            }
        } else {
            o.i("AdWebView", "Ad can not be loaded in a destroyed webview");
        }
    }

    public void a(String str) {
        a(str, (Runnable) null);
    }

    public void a(String str, Runnable runnable) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("Forwarding \"");
            sb.append(str);
            sb.append("\" to ad template");
            this.f1054a.b("AdWebView", sb.toString());
            loadUrl(str);
        } catch (Throwable th) {
            this.f1054a.b("AdWebView", "Unable to forward to template", th);
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    public d b() {
        return this.c;
    }

    public void computeScroll() {
    }

    public void destroy() {
        this.e = true;
        try {
            super.destroy();
            this.f1054a.b("AdWebView", "Web view destroyed");
        } catch (Throwable th) {
            if (this.f1054a != null) {
                this.f1054a.b("AdWebView", "destroy() threw exception", th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        try {
            super.onFocusChanged(z, i, rect);
        } catch (Exception e2) {
            this.f1054a.b("AdWebView", "onFocusChanged() threw exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
    }

    public void onWindowFocusChanged(boolean z) {
        try {
            super.onWindowFocusChanged(z);
        } catch (Exception e2) {
            this.f1054a.b("AdWebView", "onWindowFocusChanged() threw exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        try {
            super.onWindowVisibilityChanged(i);
        } catch (Exception e2) {
            this.f1054a.b("AdWebView", "onWindowVisibilityChanged() threw exception", e2);
        }
    }

    public boolean requestFocus(int i, Rect rect) {
        try {
            return super.requestFocus(i, rect);
        } catch (Exception e2) {
            this.f1054a.b("AdWebView", "requestFocus() threw exception", e2);
            return false;
        }
    }

    public void scrollTo(int i, int i2) {
    }
}
