package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.webkit.WebSettings.PluginState;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public final class w {

    /* renamed from: a reason: collision with root package name */
    private i f1137a;
    private JSONObject b;

    public w(JSONObject jSONObject, i iVar) {
        this.f1137a = iVar;
        this.b = jSONObject;
    }

    /* access modifiers changed from: 0000 */
    @TargetApi(21)
    public Integer a() {
        int i;
        String b2 = com.applovin.impl.sdk.utils.i.b(this.b, "mixed_content_mode", (String) null, this.f1137a);
        if (m.b(b2)) {
            if ("always_allow".equalsIgnoreCase(b2)) {
                i = 0;
            } else if ("never_allow".equalsIgnoreCase(b2)) {
                i = 1;
            } else if ("compatibility_mode".equalsIgnoreCase(b2)) {
                i = 2;
            }
            return Integer.valueOf(i);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public PluginState b() {
        String b2 = com.applovin.impl.sdk.utils.i.b(this.b, "plugin_state", (String) null, this.f1137a);
        if (m.b(b2)) {
            if (String.SPLIT_VIEW_TRIGGER_ON.equalsIgnoreCase(b2)) {
                return PluginState.ON;
            }
            if ("on_demand".equalsIgnoreCase(b2)) {
                return PluginState.ON_DEMAND;
            }
            if ("off".equalsIgnoreCase(b2)) {
                return PluginState.OFF;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public Boolean c() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "allow_file_access", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean d() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "load_with_overview_mode", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean e() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "use_wide_view_port", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean f() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "allow_content_access", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean g() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "use_built_in_zoom_controls", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean h() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "display_zoom_controls", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean i() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "save_form_data", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean j() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "geolocation_enabled", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean k() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "need_initial_focus", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean l() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "allow_file_access_from_file_urls", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean m() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "allow_universal_access_from_file_urls", (Boolean) null, this.f1137a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean n() {
        return com.applovin.impl.sdk.utils.i.a(this.b, "offscreen_pre_raster", (Boolean) null, this.f1137a);
    }
}
