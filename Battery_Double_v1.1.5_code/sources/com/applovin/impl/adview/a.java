package com.applovin.impl.adview;

import android.app.Activity;
import android.graphics.PorterDuff.Mode;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.applovin.sdk.AppLovinSdkUtils;

public class a extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    private final ProgressBar f1052a;

    public a(Activity activity, int i, int i2) {
        LayoutParams layoutParams;
        super(activity);
        setClickable(false);
        this.f1052a = new ProgressBar(activity, null, i2);
        this.f1052a.setIndeterminate(true);
        this.f1052a.setClickable(false);
        if (i == -2 || i == -1) {
            layoutParams = new LayoutParams(i, i);
        } else {
            int dpToPx = AppLovinSdkUtils.dpToPx(activity, i);
            layoutParams = new LayoutParams(dpToPx, dpToPx);
        }
        layoutParams.addRule(13);
        this.f1052a.setLayoutParams(layoutParams);
        addView(this.f1052a);
    }

    public void a() {
        setVisibility(0);
    }

    public void b() {
        setVisibility(8);
    }

    public void setColor(int i) {
        this.f1052a.getIndeterminateDrawable().setColorFilter(i, Mode.SRC_IN);
    }
}
