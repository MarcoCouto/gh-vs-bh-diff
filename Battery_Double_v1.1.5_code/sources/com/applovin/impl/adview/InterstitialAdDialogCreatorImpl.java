package com.applovin.impl.adview;

import android.content.Context;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.adview.InterstitialAdDialogCreator;
import com.applovin.sdk.AppLovinSdk;
import java.lang.ref.WeakReference;

public class InterstitialAdDialogCreatorImpl implements InterstitialAdDialogCreator {

    /* renamed from: a reason: collision with root package name */
    private static final Object f1051a = new Object();
    private static WeakReference<n> b = new WeakReference<>(null);
    private static WeakReference<Context> c = new WeakReference<>(null);

    public AppLovinInterstitialAdDialog createInterstitialAdDialog(AppLovinSdk appLovinSdk, Context context) {
        n nVar;
        if (appLovinSdk == null) {
            appLovinSdk = AppLovinSdk.getInstance(context);
        }
        synchronized (f1051a) {
            nVar = (n) b.get();
            if (nVar != null && nVar.isShowing()) {
                if (c.get() == context) {
                    appLovinSdk.getLogger().d("InterstitialAdDialogCreator", "An interstitial dialog is already showing, returning it");
                }
            }
            nVar = new n(appLovinSdk, context);
            b = new WeakReference<>(nVar);
            c = new WeakReference<>(context);
        }
        return nVar;
    }
}
