package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import com.applovin.impl.adview.h.a;
import com.applovin.impl.sdk.i;

@SuppressLint({"ViewConstructor"})
public final class o extends h {
    private float c = 1.0f;

    public o(i iVar, Context context) {
        super(iVar, context);
    }

    public void a(int i) {
        setViewScale(((float) i) / 30.0f);
    }

    public a getStyle() {
        return a.Invisible;
    }

    public float getViewScale() {
        return this.c;
    }

    public void setViewScale(float f) {
        this.c = f;
    }
}
