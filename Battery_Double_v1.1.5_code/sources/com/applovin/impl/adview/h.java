package com.applovin.impl.adview;

import android.content.Context;
import android.view.View;
import com.applovin.impl.sdk.i;

public abstract class h extends View {

    /* renamed from: a reason: collision with root package name */
    protected final i f1078a;
    protected final Context b;

    public enum a {
        WhiteXOnOpaqueBlack(0),
        WhiteXOnTransparentGrey(1),
        Invisible(2);
        
        private final int d;

        private a(int i) {
            this.d = i;
        }

        public int a() {
            return this.d;
        }
    }

    h(i iVar, Context context) {
        super(context);
        this.b = context;
        this.f1078a = iVar;
    }

    public static h a(i iVar, Context context, a aVar) {
        return aVar.equals(a.Invisible) ? new o(iVar, context) : aVar.equals(a.WhiteXOnTransparentGrey) ? new q(iVar, context) : new x(iVar, context);
    }

    public abstract void a(int i);

    public abstract a getStyle();

    public abstract float getViewScale();

    public abstract void setViewScale(float f);
}
