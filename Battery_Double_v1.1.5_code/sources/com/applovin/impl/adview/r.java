package com.applovin.impl.adview;

import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import com.applovin.impl.a.a;
import com.applovin.impl.a.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.a.g;
import com.applovin.impl.a.h;
import com.applovin.impl.a.i;
import com.applovin.impl.a.k;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class r extends m {

    /* renamed from: a reason: collision with root package name */
    private final Set<g> f1133a = new HashSet();

    private void a() {
        if (isFullyWatched() && !this.f1133a.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Firing ");
            sb.append(this.f1133a.size());
            sb.append(" un-fired video progress trackers when video was completed.");
            this.logger.d("InterstitialActivity", sb.toString());
            a(this.f1133a);
        }
    }

    private void a(c cVar) {
        a(cVar, d.UNSPECIFIED);
    }

    private void a(c cVar, d dVar) {
        a(cVar, "", dVar);
    }

    private void a(c cVar, String str) {
        a(cVar, str, d.UNSPECIFIED);
    }

    private void a(c cVar, String str, d dVar) {
        if (isVastAd()) {
            a(((a) this.currentAd).a(cVar, str), dVar);
        }
    }

    private void a(Set<g> set) {
        a(set, d.UNSPECIFIED);
    }

    private void a(Set<g> set, d dVar) {
        if (isVastAd() && set != null && !set.isEmpty()) {
            long seconds = TimeUnit.MILLISECONDS.toSeconds((long) this.videoView.getCurrentPosition());
            k i = b().i();
            Uri a2 = i != null ? i.a() : null;
            StringBuilder sb = new StringBuilder();
            sb.append("Firing ");
            sb.append(set.size());
            sb.append(" tracker(s): ");
            sb.append(set);
            this.logger.b("InterstitialActivity", sb.toString());
            i.a(set, seconds, a2, dVar, this.sdk);
        }
    }

    private a b() {
        if (this.currentAd instanceof a) {
            return (a) this.currentAd;
        }
        return null;
    }

    public void clickThroughFromVideo(PointF pointF) {
        super.clickThroughFromVideo(pointF);
        a(c.VIDEO_CLICK);
    }

    public void dismiss() {
        if (isVastAd()) {
            a(c.VIDEO, "close");
            a(c.COMPANION, "close");
        }
        super.dismiss();
    }

    public void handleCountdownStep() {
        if (isVastAd()) {
            long seconds = ((long) this.computedLengthSeconds) - TimeUnit.MILLISECONDS.toSeconds((long) (this.videoView.getDuration() - this.videoView.getCurrentPosition()));
            HashSet hashSet = new HashSet();
            for (g gVar : new HashSet(this.f1133a)) {
                if (gVar.a(seconds, getVideoPercentViewed())) {
                    hashSet.add(gVar);
                    this.f1133a.remove(gVar);
                }
            }
            a((Set<g>) hashSet);
        }
    }

    public void handleMediaError(String str) {
        a(c.ERROR, d.MEDIA_FILE_ERROR);
        super.handleMediaError(str);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (isVastAd()) {
            this.f1133a.addAll(b().a(c.VIDEO, h.f1030a));
            a(c.IMPRESSION);
            a(c.VIDEO, "creativeView");
        }
    }

    public void playVideo() {
        this.countdownManager.a("PROGRESS_TRACKING", ((Long) this.sdk.a(com.applovin.impl.sdk.b.c.eC)).longValue(), (a) new a() {
            public void a() {
                r.this.handleCountdownStep();
            }

            public boolean b() {
                return r.this.shouldContinueFullLengthVideoCountdown();
            }
        });
        super.playVideo();
    }

    public void showPoststitial() {
        if (isVastAd()) {
            a();
            if (!i.c(b())) {
                dismiss();
                return;
            } else if (!this.poststitialWasDisplayed) {
                a(c.COMPANION, "creativeView");
            } else {
                return;
            }
        }
        super.showPoststitial();
    }

    public void skipVideo() {
        a(c.VIDEO, "skip");
        super.skipVideo();
    }

    public void toggleMute() {
        c cVar;
        String str;
        super.toggleMute();
        if (this.videoMuted) {
            cVar = c.VIDEO;
            str = "mute";
        } else {
            cVar = c.VIDEO;
            str = "unmute";
        }
        a(cVar, str);
    }
}
