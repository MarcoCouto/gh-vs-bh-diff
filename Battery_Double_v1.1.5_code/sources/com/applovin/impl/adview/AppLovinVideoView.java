package com.applovin.impl.adview;

import android.content.Context;
import android.widget.VideoView;
import com.applovin.impl.sdk.i;

public class AppLovinVideoView extends VideoView implements t {

    /* renamed from: a reason: collision with root package name */
    private final i f1050a;

    public AppLovinVideoView(Context context, i iVar) {
        super(context, null, 0);
        this.f1050a = iVar;
    }

    public void setVideoSize(int i, int i2) {
        try {
            getHolder().setFixedSize(i, i2);
            requestLayout();
            invalidate();
        } catch (Exception unused) {
        }
    }
}
