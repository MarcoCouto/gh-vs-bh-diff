package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.webkit.WebView;
import android.webkit.WebViewRenderProcess;
import android.webkit.WebViewRenderProcessClient;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.i;
import com.applovin.sdk.AppLovinAd;

@TargetApi(29)
class e extends WebViewRenderProcessClient {

    /* renamed from: a reason: collision with root package name */
    private final i f1075a;

    e(i iVar) {
        this.f1075a = iVar;
    }

    public void onRenderProcessResponsive(WebView webView, WebViewRenderProcess webViewRenderProcess) {
    }

    public void onRenderProcessUnresponsive(WebView webView, WebViewRenderProcess webViewRenderProcess) {
        if (webView instanceof c) {
            AppLovinAd a2 = ((c) webView).a();
            if (a2 instanceof AppLovinAdBase) {
                this.f1075a.X().a((AppLovinAdBase) a2).a(b.F).a();
            }
            StringBuilder sb = new StringBuilder();
            sb.append("WebView render process unresponsive for ad: ");
            sb.append(a2);
            this.f1075a.v().e("AdWebViewRenderProcessClient", sb.toString());
        }
    }
}
