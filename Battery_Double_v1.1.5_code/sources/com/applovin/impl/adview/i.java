package com.applovin.impl.adview;

import android.os.Handler;
import com.applovin.impl.sdk.o;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class i {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final o f1080a;
    private final Handler b;
    private final Set<b> c = new HashSet();
    /* access modifiers changed from: private */
    public final AtomicInteger d = new AtomicInteger();

    interface a {
        void a();

        boolean b();
    }

    private static class b {

        /* renamed from: a reason: collision with root package name */
        private final String f1082a;
        private final a b;
        private final long c;

        private b(String str, long j, a aVar) {
            this.f1082a = str;
            this.c = j;
            this.b = aVar;
        }

        /* access modifiers changed from: private */
        public String a() {
            return this.f1082a;
        }

        /* access modifiers changed from: private */
        public long b() {
            return this.c;
        }

        /* access modifiers changed from: private */
        public a c() {
            return this.b;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.f1082a != null) {
                z = this.f1082a.equalsIgnoreCase(bVar.f1082a);
            } else if (bVar.f1082a != null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            if (this.f1082a != null) {
                return this.f1082a.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("CountdownProxy{identifier='");
            sb.append(this.f1082a);
            sb.append('\'');
            sb.append(", countdownStepMillis=");
            sb.append(this.c);
            sb.append('}');
            return sb.toString();
        }
    }

    public i(Handler handler, com.applovin.impl.sdk.i iVar) {
        if (handler == null) {
            throw new IllegalArgumentException("No handler specified.");
        } else if (iVar != null) {
            this.b = handler;
            this.f1080a = iVar.v();
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    /* access modifiers changed from: private */
    public void a(final b bVar, final int i) {
        this.b.postDelayed(new Runnable() {
            public void run() {
                a b2 = bVar.c();
                if (!b2.b()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Ending countdown for ");
                    sb.append(bVar.a());
                    i.this.f1080a.b("CountdownManager", sb.toString());
                } else if (i.this.d.get() == i) {
                    try {
                        b2.a();
                    } catch (Throwable th) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Encountered error on countdown step for: ");
                        sb2.append(bVar.a());
                        i.this.f1080a.b("CountdownManager", sb2.toString(), th);
                    }
                    i.this.a(bVar, i);
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Killing duplicate countdown from previous generation: ");
                    sb3.append(bVar.a());
                    i.this.f1080a.d("CountdownManager", sb3.toString());
                }
            }
        }, bVar.b());
    }

    public void a() {
        HashSet<b> hashSet = new HashSet<>(this.c);
        StringBuilder sb = new StringBuilder();
        sb.append("Starting ");
        sb.append(hashSet.size());
        sb.append(" countdowns...");
        this.f1080a.b("CountdownManager", sb.toString());
        int incrementAndGet = this.d.incrementAndGet();
        for (b bVar : hashSet) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Starting countdown: ");
            sb2.append(bVar.a());
            sb2.append(" for generation ");
            sb2.append(incrementAndGet);
            sb2.append("...");
            this.f1080a.b("CountdownManager", sb2.toString());
            a(bVar, incrementAndGet);
        }
    }

    public void a(String str, long j, a aVar) {
        if (j <= 0) {
            throw new IllegalArgumentException("Invalid step specified.");
        } else if (this.b != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Adding countdown: ");
            sb.append(str);
            this.f1080a.b("CountdownManager", sb.toString());
            b bVar = new b(str, j, aVar);
            this.c.add(bVar);
        } else {
            throw new IllegalArgumentException("No handler specified.");
        }
    }

    public void b() {
        this.f1080a.b("CountdownManager", "Removing all countdowns...");
        c();
        this.c.clear();
    }

    public void c() {
        this.f1080a.b("CountdownManager", "Stopping countdowns...");
        this.d.incrementAndGet();
        this.b.removeCallbacksAndMessages(null);
    }
}
