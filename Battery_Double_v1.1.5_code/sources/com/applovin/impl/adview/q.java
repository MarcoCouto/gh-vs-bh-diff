package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import com.applovin.impl.adview.h.a;
import com.applovin.impl.sdk.i;

@SuppressLint({"ViewConstructor"})
public final class q extends h {
    private static final Paint d = new Paint(1);
    private static final Paint e = new Paint(1);
    private float c = 1.0f;

    public q(i iVar, Context context) {
        super(iVar, context);
        d.setARGB(80, 0, 0, 0);
        e.setColor(-1);
        e.setStyle(Style.STROKE);
    }

    public void a(int i) {
        setViewScale(((float) i) / 30.0f);
    }

    /* access modifiers changed from: protected */
    public float getCenter() {
        return getSize() / 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getCrossOffset() {
        return this.c * 8.0f;
    }

    /* access modifiers changed from: protected */
    public float getInnerCircleOffset() {
        return this.c * 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getInnerCircleRadius() {
        return getCenter() - getInnerCircleOffset();
    }

    /* access modifiers changed from: protected */
    public float getSize() {
        return this.c * 30.0f;
    }

    /* access modifiers changed from: protected */
    public float getStrokeWidth() {
        return this.c * 2.0f;
    }

    public a getStyle() {
        return a.WhiteXOnTransparentGrey;
    }

    public float getViewScale() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float center = getCenter();
        canvas.drawCircle(center, center, center, d);
        float crossOffset = getCrossOffset();
        float size = getSize() - crossOffset;
        e.setStrokeWidth(getStrokeWidth());
        Canvas canvas2 = canvas;
        float f = crossOffset;
        float f2 = size;
        canvas2.drawLine(f, crossOffset, f2, size, e);
        canvas2.drawLine(f, size, f2, crossOffset, e);
    }

    public void setViewScale(float f) {
        this.c = f;
    }
}
