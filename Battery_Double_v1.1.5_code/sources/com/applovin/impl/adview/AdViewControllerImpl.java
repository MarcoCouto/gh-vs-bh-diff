package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.PointF;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.applovin.adview.AdViewController;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.impl.sdk.AppLovinAdServiceImpl;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.h;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicReference;

public class AdViewControllerImpl implements AdViewController {
    private volatile AppLovinAdClickListener A;
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public Context f1037a;
    /* access modifiers changed from: private */
    public ViewGroup b;
    /* access modifiers changed from: private */
    public i c;
    /* access modifiers changed from: private */
    public AppLovinAdServiceImpl d;
    /* access modifiers changed from: private */
    public o e;
    /* access modifiers changed from: private */
    public AppLovinAdSize f;
    private String g;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.c.d h;
    private d i;
    private d j;
    /* access modifiers changed from: private */
    public c k;
    private AppLovinAd l;
    private Runnable m;
    private Runnable n;
    /* access modifiers changed from: private */
    public volatile AppLovinAd o = null;
    private volatile AppLovinAd p = null;
    /* access modifiers changed from: private */
    public k q = null;
    /* access modifiers changed from: private */
    public k r = null;
    private final AtomicReference<AppLovinAd> s = new AtomicReference<>();
    private volatile boolean t = false;
    private volatile boolean u = true;
    /* access modifiers changed from: private */
    public volatile boolean v = false;
    private volatile boolean w = false;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener x;
    private volatile AppLovinAdDisplayListener y;
    /* access modifiers changed from: private */
    public volatile AppLovinAdViewEventListener z;

    private class a implements Runnable {
        private a() {
        }

        public void run() {
            if (AdViewControllerImpl.this.k != null) {
                AdViewControllerImpl.this.k.setVisibility(8);
            }
        }
    }

    private class b implements Runnable {
        private b() {
        }

        public void run() {
            if (AdViewControllerImpl.this.k != null) {
                try {
                    AdViewControllerImpl.this.k.loadDataWithBaseURL("/", "<html></html>", WebRequest.CONTENT_TYPE_HTML, null, "");
                } catch (Exception unused) {
                }
            }
        }
    }

    private class c implements Runnable {
        private c() {
        }

        public void run() {
            if (AdViewControllerImpl.this.o == null) {
                return;
            }
            if (AdViewControllerImpl.this.k != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Rendering advertisement ad for #");
                sb.append(AdViewControllerImpl.this.o.getAdIdNumber());
                sb.append("...");
                AdViewControllerImpl.this.e.b("AppLovinAdView", sb.toString());
                AdViewControllerImpl.b((View) AdViewControllerImpl.this.k, AdViewControllerImpl.this.o.getSize());
                AdViewControllerImpl.this.k.a(AdViewControllerImpl.this.o);
                if (AdViewControllerImpl.this.o.getSize() != AppLovinAdSize.INTERSTITIAL && !AdViewControllerImpl.this.v && (AdViewControllerImpl.this.o instanceof f)) {
                    f fVar = (f) AdViewControllerImpl.this.o;
                    AdViewControllerImpl.this.h = new com.applovin.impl.sdk.c.d(fVar, AdViewControllerImpl.this.c);
                    AdViewControllerImpl.this.h.a();
                    AdViewControllerImpl.this.k.a(AdViewControllerImpl.this.h);
                    fVar.setHasShown(true);
                }
                if (AdViewControllerImpl.this.k.b() != null && (AdViewControllerImpl.this.o instanceof f)) {
                    AdViewControllerImpl.this.k.b().a(((f) AdViewControllerImpl.this.o).r() ? 0 : 1);
                    return;
                }
                return;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to render advertisement for ad #");
            sb2.append(AdViewControllerImpl.this.o.getAdIdNumber());
            sb2.append(". Please make sure you are not calling AppLovinAdView.destroy() prematurely.");
            o.i("AppLovinAdView", sb2.toString());
        }
    }

    static class d implements AppLovinAdLoadListener {

        /* renamed from: a reason: collision with root package name */
        private final AppLovinAdService f1048a;
        private final o b;
        private final AdViewControllerImpl c;

        d(AdViewControllerImpl adViewControllerImpl, i iVar) {
            if (adViewControllerImpl == null) {
                throw new IllegalArgumentException("No view specified");
            } else if (iVar != null) {
                this.b = iVar.v();
                this.f1048a = iVar.o();
                this.c = adViewControllerImpl;
            } else {
                throw new IllegalArgumentException("No sdk specified");
            }
        }

        private AdViewControllerImpl a() {
            return this.c;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            AdViewControllerImpl a2 = a();
            if (a2 != null) {
                a2.a(appLovinAd);
            } else {
                o.i("AppLovinAdView", "Ad view has been garbage collected by the time an ad was received");
            }
        }

        public void failedToReceiveAd(int i) {
            AdViewControllerImpl a2 = a();
            if (a2 != null) {
                a2.a(i);
            }
        }
    }

    private void a(AppLovinAdView appLovinAdView, i iVar, AppLovinAdSize appLovinAdSize, String str, Context context) {
        if (appLovinAdView == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (iVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (appLovinAdSize != null) {
            this.c = iVar;
            this.d = iVar.o();
            this.e = iVar.v();
            this.f = appLovinAdSize;
            this.g = str;
            this.f1037a = context;
            this.b = appLovinAdView;
            this.l = new h();
            this.i = new d(this, iVar);
            this.n = new a();
            this.m = new c();
            this.j = new d(this, iVar);
            a(appLovinAdSize);
        } else {
            throw new IllegalArgumentException("No ad size specified");
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAdSize appLovinAdSize) {
        try {
            this.k = new c(this.i, this.c, this.f1037a);
            this.k.setBackgroundColor(0);
            this.k.setWillNotCacheDrawing(false);
            this.b.setBackgroundColor(0);
            this.b.addView(this.k);
            b((View) this.k, appLovinAdSize);
            if (!this.t) {
                a(this.n);
            }
            if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.eS)).booleanValue()) {
                a((Runnable) new b());
            }
            this.t = true;
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to create AdView: ");
            sb.append(th.getMessage());
            o.i("AppLovinAdView", sb.toString());
        }
    }

    private void a(Runnable runnable) {
        AppLovinSdkUtils.runOnUiThread(runnable);
    }

    private void b() {
        if (this.e != null) {
            this.e.b("AppLovinAdView", "Destroying...");
        }
        if (this.k != null) {
            try {
                ViewParent parent = this.k.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(this.k);
                }
                this.k.removeAllViews();
                if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.eO)).booleanValue()) {
                    this.k.loadUrl("about:blank");
                    this.k.onPause();
                    this.k.destroyDrawingCache();
                }
            } catch (Throwable th) {
                this.e.a("AppLovinAdView", "Unable to destroy ad view", th);
            }
            this.k.destroy();
            this.k = null;
        }
        this.v = true;
    }

    /* access modifiers changed from: private */
    public static void b(View view, AppLovinAdSize appLovinAdSize) {
        if (view != null) {
            DisplayMetrics displayMetrics = view.getResources().getDisplayMetrics();
            int i2 = -1;
            int i3 = appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel()) ? -1 : appLovinAdSize.getWidth() == -1 ? displayMetrics.widthPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getWidth(), displayMetrics);
            if (!appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel())) {
                i2 = appLovinAdSize.getHeight() == -1 ? displayMetrics.heightPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getHeight(), displayMetrics);
            }
            LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            }
            layoutParams.width = i3;
            layoutParams.height = i2;
            if (layoutParams instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
                layoutParams2.addRule(10);
                layoutParams2.addRule(9);
            }
            view.setLayoutParams(layoutParams);
        }
    }

    private void c() {
        a((Runnable) new Runnable() {
            public void run() {
                if (AdViewControllerImpl.this.q != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Detaching expanded ad: ");
                    sb.append(AdViewControllerImpl.this.q.a());
                    AdViewControllerImpl.this.e.b("AppLovinAdView", sb.toString());
                    AdViewControllerImpl.this.r = AdViewControllerImpl.this.q;
                    AdViewControllerImpl.this.q = null;
                    AdViewControllerImpl.this.a(AdViewControllerImpl.this.f);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void d() {
        a((Runnable) new Runnable() {
            public void run() {
                com.applovin.impl.sdk.ad.a aVar;
                if (AdViewControllerImpl.this.r != null || AdViewControllerImpl.this.q != null) {
                    if (AdViewControllerImpl.this.r != null) {
                        aVar = AdViewControllerImpl.this.r.a();
                        AdViewControllerImpl.this.r.dismiss();
                        AdViewControllerImpl.this.r = null;
                    } else {
                        aVar = AdViewControllerImpl.this.q.a();
                        AdViewControllerImpl.this.q.dismiss();
                        AdViewControllerImpl.this.q = null;
                    }
                    j.b(AdViewControllerImpl.this.z, (AppLovinAd) aVar, (AppLovinAdView) AdViewControllerImpl.this.b);
                }
            }
        });
    }

    private void e() {
        if (this.h != null) {
            this.h.c();
            this.h = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.q == null && this.r == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Ad: ");
            sb.append(this.o);
            sb.append(" closed.");
            this.e.b("AppLovinAdView", sb.toString());
            a(this.n);
            j.b(this.y, this.o);
            this.o = null;
        } else if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.cp)).booleanValue()) {
            contractAd();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(final int i2) {
        if (!this.v) {
            a(this.n);
        }
        a((Runnable) new Runnable() {
            public void run() {
                try {
                    if (AdViewControllerImpl.this.x != null) {
                        AdViewControllerImpl.this.x.failedToReceiveAd(i2);
                    }
                } catch (Throwable th) {
                    o.c("AppLovinAdView", "Exception while running app load  callback", th);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(final AppLovinAd appLovinAd) {
        if (appLovinAd != null) {
            this.w = true;
            if (!this.v) {
                renderAd(appLovinAd);
            } else {
                this.s.set(appLovinAd);
                this.e.b("AppLovinAdView", "Ad view has paused when an ad was received, ad saved for later");
            }
            a((Runnable) new Runnable() {
                public void run() {
                    try {
                        if (AdViewControllerImpl.this.x != null) {
                            AdViewControllerImpl.this.x.adReceived(appLovinAd);
                        }
                    } catch (Throwable th) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Exception while running ad load callback: ");
                        sb.append(th.getMessage());
                        o.i("AppLovinAdView", sb.toString());
                    }
                }
            });
            return;
        }
        this.e.e("AppLovinAdView", "No provided when to the view controller");
        a(-1);
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, Uri uri, PointF pointF) {
        String str;
        String str2;
        o oVar;
        j.a(this.A, appLovinAd);
        if (appLovinAdView == null) {
            oVar = this.e;
            str2 = "AppLovinAdView";
            str = "Unable to process ad click - AppLovinAdView destroyed prematurely";
        } else if (appLovinAd instanceof f) {
            this.d.trackAndLaunchClick(appLovinAd, appLovinAdView, this, uri, pointF);
            return;
        } else {
            oVar = this.e;
            str2 = "AppLovinAdView";
            str = "Unable to process ad click - EmptyAd is not supported.";
        }
        oVar.e(str2, str);
    }

    public void contractAd() {
        a((Runnable) new Runnable() {
            public void run() {
                AdViewControllerImpl.this.d();
                if (AdViewControllerImpl.this.b != null && AdViewControllerImpl.this.k != null && AdViewControllerImpl.this.k.getParent() == null) {
                    AdViewControllerImpl.this.b.addView(AdViewControllerImpl.this.k);
                    AdViewControllerImpl.b((View) AdViewControllerImpl.this.k, AdViewControllerImpl.this.o.getSize());
                }
            }
        });
    }

    public void destroy() {
        if (!(this.k == null || this.q == null)) {
            contractAd();
        }
        b();
    }

    public void dismissInterstitialIfRequired() {
        if ((this.f1037a instanceof m) && (this.o instanceof f)) {
            m mVar = (m) this.f1037a;
            if ((((f) this.o).E() == com.applovin.impl.sdk.ad.f.a.DISMISS) && mVar.getPoststitialWasDisplayed()) {
                mVar.dismiss();
            }
        }
    }

    public void expandAd(final PointF pointF) {
        a((Runnable) new Runnable() {
            public void run() {
                if (AdViewControllerImpl.this.q == null && (AdViewControllerImpl.this.o instanceof com.applovin.impl.sdk.ad.a) && AdViewControllerImpl.this.k != null) {
                    com.applovin.impl.sdk.ad.a aVar = (com.applovin.impl.sdk.ad.a) AdViewControllerImpl.this.o;
                    Activity a2 = AdViewControllerImpl.this.f1037a instanceof Activity ? (Activity) AdViewControllerImpl.this.f1037a : p.a((View) AdViewControllerImpl.this.k, AdViewControllerImpl.this.c);
                    if (a2 != null) {
                        if (AdViewControllerImpl.this.b != null) {
                            AdViewControllerImpl.this.b.removeView(AdViewControllerImpl.this.k);
                        }
                        AdViewControllerImpl.this.q = new k(aVar, AdViewControllerImpl.this.k, a2, AdViewControllerImpl.this.c);
                        AdViewControllerImpl.this.q.setOnDismissListener(new OnDismissListener() {
                            public void onDismiss(DialogInterface dialogInterface) {
                                AdViewControllerImpl.this.contractAd();
                            }
                        });
                        AdViewControllerImpl.this.q.show();
                        j.a(AdViewControllerImpl.this.z, AdViewControllerImpl.this.o, (AppLovinAdView) AdViewControllerImpl.this.b);
                        if (AdViewControllerImpl.this.h != null) {
                            AdViewControllerImpl.this.h.d();
                        }
                    } else {
                        o.i("AppLovinAdView", "Unable to expand ad. No Activity found.");
                        Uri f = aVar.f();
                        if (f != null && ((Boolean) AdViewControllerImpl.this.c.a(com.applovin.impl.sdk.b.c.cw)).booleanValue()) {
                            AdViewControllerImpl.this.d.trackAndLaunchClick(aVar, AdViewControllerImpl.this.getParentView(), AdViewControllerImpl.this, f, pointF);
                            if (AdViewControllerImpl.this.h != null) {
                                AdViewControllerImpl.this.h.b();
                            }
                        }
                        AdViewControllerImpl.this.k.a("javascript:al_onFailedExpand();");
                    }
                }
            }
        });
    }

    public AppLovinAdViewEventListener getAdViewEventListener() {
        return this.z;
    }

    public c getAdWebView() {
        return this.k;
    }

    public AppLovinAd getCurrentAd() {
        return this.o;
    }

    public AppLovinAdView getParentView() {
        return (AppLovinAdView) this.b;
    }

    public i getSdk() {
        return this.c;
    }

    public AppLovinAdSize getSize() {
        return this.f;
    }

    public String getZoneId() {
        return this.g;
    }

    public void initializeAdView(AppLovinAdView appLovinAdView, Context context, AppLovinAdSize appLovinAdSize, String str, AppLovinSdk appLovinSdk, AttributeSet attributeSet) {
        if (appLovinAdView == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (context == null) {
            o.i("AppLovinAdView", "Unable to build AppLovinAdView: no context provided. Please use a different constructor for this view.");
        } else {
            if (appLovinAdSize == null) {
                appLovinAdSize = com.applovin.impl.sdk.utils.b.a(attributeSet);
                if (appLovinAdSize == null) {
                    appLovinAdSize = AppLovinAdSize.BANNER;
                }
            }
            AppLovinAdSize appLovinAdSize2 = appLovinAdSize;
            if (appLovinSdk == null) {
                appLovinSdk = AppLovinSdk.getInstance(context);
            }
            if (appLovinSdk != null && !appLovinSdk.hasCriticalErrors()) {
                a(appLovinAdView, p.a(appLovinSdk), appLovinAdSize2, str, context);
                if (com.applovin.impl.sdk.utils.b.b(attributeSet)) {
                    loadNextAd();
                }
            }
        }
    }

    public boolean isAdReadyToDisplay() {
        return !TextUtils.isEmpty(this.g) ? this.d.hasPreloadedAdForZoneId(this.g) : this.d.hasPreloadedAd(this.f);
    }

    public boolean isAutoDestroy() {
        return this.u;
    }

    public void loadNextAd() {
        if (this.c == null || this.j == null || this.f1037a == null || !this.t) {
            o.g("AppLovinAdView", "Unable to load next ad: AppLovinAdView is not initialized.");
        } else {
            this.d.loadNextAd(this.g, this.f, this.j);
        }
    }

    public void onAdHtmlLoaded(WebView webView) {
        if (this.o instanceof f) {
            webView.setVisibility(0);
            try {
                if (this.o != this.p && this.y != null) {
                    this.p = this.o;
                    j.a(this.y, this.o);
                }
            } catch (Throwable th) {
                o.c("AppLovinAdView", "Exception while notifying ad display listener", th);
            }
        }
    }

    public void onDetachedFromWindow() {
        if (this.t) {
            if (this.o != this.l) {
                j.b(this.y, this.o);
            }
            if (this.k == null || this.q == null) {
                this.e.b("AppLovinAdView", "onDetachedFromWindowCalled without an expanded ad present");
            } else {
                this.e.b("AppLovinAdView", "onDetachedFromWindowCalled with expanded ad present");
                if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.co)).booleanValue()) {
                    contractAd();
                } else {
                    c();
                }
            }
            if (this.u) {
                b();
            }
        }
    }

    public void onVisibilityChanged(int i2) {
        if (this.t && this.u) {
            if (i2 == 8 || i2 == 4) {
                pause();
            } else if (i2 == 0) {
                resume();
            }
        }
    }

    public void pause() {
        if (this.t && !this.v) {
            AppLovinAd appLovinAd = this.o;
            renderAd(this.l);
            if (appLovinAd != null) {
                this.s.set(appLovinAd);
            }
            this.v = true;
        }
    }

    public void renderAd(AppLovinAd appLovinAd) {
        renderAd(appLovinAd, null);
    }

    public void renderAd(AppLovinAd appLovinAd, String str) {
        if (appLovinAd != null) {
            p.b(appLovinAd, this.c);
            if (this.t) {
                AppLovinAd a2 = p.a(appLovinAd, this.c);
                if (a2 != null && a2 != this.o) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Rendering ad #");
                    sb.append(a2.getAdIdNumber());
                    sb.append(" (");
                    sb.append(a2.getSize());
                    sb.append(")");
                    this.e.b("AppLovinAdView", sb.toString());
                    if (!(this.o instanceof h)) {
                        j.b(this.y, this.o);
                        if (!(a2 instanceof h) && a2.getSize() != AppLovinAdSize.INTERSTITIAL) {
                            e();
                        }
                    }
                    this.s.set(null);
                    this.p = null;
                    this.o = a2;
                    if ((appLovinAd instanceof f) && !this.v && (this.f == AppLovinAdSize.BANNER || this.f == AppLovinAdSize.MREC || this.f == AppLovinAdSize.LEADER)) {
                        this.c.o().trackImpression((f) appLovinAd);
                    }
                    boolean z2 = a2 instanceof h;
                    if (!z2 && this.q != null) {
                        if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.cn)).booleanValue()) {
                            d();
                            this.e.b("AppLovinAdView", "Fade out the old ad scheduled");
                        } else {
                            c();
                        }
                    }
                    if (!z2 || (this.q == null && this.r == null)) {
                        a(this.m);
                    } else {
                        this.e.b("AppLovinAdView", "Ignoring empty ad render with expanded ad");
                    }
                } else if (a2 == null) {
                    this.e.d("AppLovinAdView", "Unable to render ad. Ad is null. Internal inconsistency error.");
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Ad #");
                    sb2.append(a2.getAdIdNumber());
                    sb2.append(" is already showing, ignoring");
                    this.e.d("AppLovinAdView", sb2.toString());
                }
            } else {
                o.g("AppLovinAdView", "Unable to render ad: AppLovinAdView is not initialized.");
            }
        } else {
            throw new IllegalArgumentException("No ad specified");
        }
    }

    public void resume() {
        if (this.t) {
            AppLovinAd appLovinAd = (AppLovinAd) this.s.getAndSet(null);
            if (appLovinAd != null) {
                renderAd(appLovinAd);
            }
            this.v = false;
        }
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.A = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.y = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.x = appLovinAdLoadListener;
    }

    public void setAdVideoPlaybackListener(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
    }

    public void setAdViewEventListener(AppLovinAdViewEventListener appLovinAdViewEventListener) {
        this.z = appLovinAdViewEventListener;
    }

    public void setAutoDestroy(boolean z2) {
        this.u = z2;
    }

    public void setStatsManagerHelper(com.applovin.impl.sdk.c.d dVar) {
        if (this.k != null) {
            this.k.a(dVar);
        }
    }
}
