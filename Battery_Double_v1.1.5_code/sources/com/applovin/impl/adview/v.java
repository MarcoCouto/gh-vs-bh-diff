package com.applovin.impl.adview;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import java.lang.ref.WeakReference;

public class v extends WebViewClient {

    /* renamed from: a reason: collision with root package name */
    private final o f1136a;
    private WeakReference<a> b;

    public interface a {
        void a(u uVar);

        void b(u uVar);

        void c(u uVar);
    }

    public v(i iVar) {
        this.f1136a = iVar.v();
    }

    private void a(WebView webView, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Processing click on ad URL \"");
        sb.append(str);
        sb.append("\"");
        this.f1136a.c("WebViewButtonClient", sb.toString());
        if (str != null && (webView instanceof u)) {
            u uVar = (u) webView;
            Uri parse = Uri.parse(str);
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            a aVar = (a) this.b.get();
            if ("applovin".equalsIgnoreCase(scheme) && "com.applovin.sdk".equalsIgnoreCase(host) && aVar != null) {
                if ("/track_click".equals(path)) {
                    aVar.a(uVar);
                } else if ("/close_ad".equals(path)) {
                    aVar.b(uVar);
                } else if ("/skip_ad".equals(path)) {
                    aVar.c(uVar);
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unknown URL: ");
                    sb2.append(str);
                    this.f1136a.d("WebViewButtonClient", sb2.toString());
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Path: ");
                    sb3.append(path);
                    this.f1136a.d("WebViewButtonClient", sb3.toString());
                }
            }
        }
    }

    public void a(WeakReference<a> weakReference) {
        this.b = weakReference;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a(webView, str);
        return true;
    }
}
