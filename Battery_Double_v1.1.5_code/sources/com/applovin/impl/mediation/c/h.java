package com.applovin.impl.mediation.c;

import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.d.ae;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONObject;

public class h extends ae {

    /* renamed from: a reason: collision with root package name */
    private final c f1215a;

    public h(c cVar, i iVar) {
        super("TaskValidateMaxReward", iVar);
        this.f1215a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.K;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (!c()) {
            this.f1215a.a(com.applovin.impl.sdk.a.c.a((i < 400 || i >= 500) ? "network_timeout" : "rejected"));
        }
    }

    /* access modifiers changed from: protected */
    public void a(com.applovin.impl.sdk.a.c cVar) {
        if (!c()) {
            this.f1215a.a(cVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "ad_unit_id", this.f1215a.getAdUnitId(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, "placement", this.f1215a.J(), this.b);
        String s = this.f1215a.s();
        String str = "mcode";
        if (!m.b(s)) {
            s = "NO_MCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str, s, this.b);
        String r = this.f1215a.r();
        String str2 = "bcode";
        if (!m.b(r)) {
            r = "NO_BCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str2, r, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/mvr";
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.f1215a.t();
    }
}
