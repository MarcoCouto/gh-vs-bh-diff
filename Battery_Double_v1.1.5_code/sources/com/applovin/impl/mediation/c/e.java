package com.applovin.impl.mediation.c;

import android.app.Activity;
import com.applovin.impl.mediation.b.b;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.mediation.b.d;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import org.json.JSONException;
import org.json.JSONObject;

public class e extends a {

    /* renamed from: a reason: collision with root package name */
    private final String f1209a;
    private final JSONObject c;
    private final JSONObject d;
    private final MaxAdListener e;
    private final Activity f;

    e(String str, JSONObject jSONObject, JSONObject jSONObject2, i iVar, Activity activity, MaxAdListener maxAdListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("TaskLoadAdapterAd ");
        sb.append(str);
        super(sb.toString(), iVar);
        this.f1209a = str;
        this.c = jSONObject;
        this.d = jSONObject2;
        this.f = activity;
        this.e = maxAdListener;
    }

    private com.applovin.impl.mediation.b.a b() throws JSONException {
        String string = this.d.getString("ad_format");
        MaxAdFormat c2 = p.c(string);
        if (c2 == MaxAdFormat.BANNER || c2 == MaxAdFormat.MREC || c2 == MaxAdFormat.LEADER) {
            return new b(this.c, this.d, this.b);
        }
        if (c2 == MaxAdFormat.NATIVE) {
            return new d(this.c, this.d, this.b);
        }
        if (c2 == MaxAdFormat.INTERSTITIAL || c2 == MaxAdFormat.REWARDED) {
            return new c(this.c, this.d, this.b);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Unsupported ad format: ");
        sb.append(string);
        throw new IllegalArgumentException(sb.toString());
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.D;
    }

    public void run() {
        try {
            this.b.y().loadThirdPartyMediatedAd(this.f1209a, b(), this.f, this.e);
        } catch (Throwable th) {
            a("Unable to process adapter ad", th);
            this.b.M().a(a());
            j.a(this.e, this.f1209a, (int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED);
        }
    }
}
