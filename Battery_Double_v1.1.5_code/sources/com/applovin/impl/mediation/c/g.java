package com.applovin.impl.mediation.c;

import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.d.z;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONObject;

public class g extends z {

    /* renamed from: a reason: collision with root package name */
    private final c f1214a;

    public g(c cVar, i iVar) {
        super("TaskReportMaxReward", iVar);
        this.f1214a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.L;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to report reward for mediated ad: ");
        sb.append(this.f1214a);
        sb.append(" - error code: ");
        sb.append(i);
        a(sb.toString());
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "ad_unit_id", this.f1214a.getAdUnitId(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, "placement", this.f1214a.J(), this.b);
        String s = this.f1214a.s();
        String str = "mcode";
        if (!m.b(s)) {
            s = "NO_MCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str, s, this.b);
        String r = this.f1214a.r();
        String str2 = "bcode";
        if (!m.b(r)) {
            r = "NO_BCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str2, r, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/mcr";
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append("Reported reward successfully for mediated ad: ");
        sb.append(this.f1214a);
        a(sb.toString());
    }

    /* access modifiers changed from: protected */
    public com.applovin.impl.sdk.a.c c() {
        return this.f1214a.v();
    }

    /* access modifiers changed from: protected */
    public void d() {
        StringBuilder sb = new StringBuilder();
        sb.append("No reward result was found for mediated ad: ");
        sb.append(this.f1214a);
        d(sb.toString());
    }
}
