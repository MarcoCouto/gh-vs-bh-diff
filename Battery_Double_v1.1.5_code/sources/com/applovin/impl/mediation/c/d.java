package com.applovin.impl.mediation.c;

import com.applovin.impl.mediation.b.c;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.sdk.b.b;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class d extends a {

    /* renamed from: a reason: collision with root package name */
    private final String f1207a;
    private final String c;
    private final e d;
    private final Map<String, String> e;
    private final Map<String, String> f;
    private final com.applovin.impl.mediation.e g;

    public d(String str, Map<String, String> map, com.applovin.impl.mediation.e eVar, e eVar2, i iVar) {
        super("TaskFireMediationPostbacks", iVar);
        this.f1207a = str;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_urls");
        this.c = sb.toString();
        this.e = p.b(map);
        if (eVar == null) {
            eVar = com.applovin.impl.mediation.e.EMPTY;
        }
        this.g = eVar;
        this.d = eVar2;
        HashMap hashMap = new HashMap(4);
        hashMap.put("Ad-Network-Name", eVar2.z());
        if (eVar2 instanceof com.applovin.impl.mediation.b.a) {
            com.applovin.impl.mediation.b.a aVar = (com.applovin.impl.mediation.b.a) eVar2;
            hashMap.put("Ad-Unit-Id", aVar.getAdUnitId());
            hashMap.put("Ad-Format", aVar.getFormat().getLabel());
            if (aVar instanceof c) {
                hashMap.put("Ad-Is-Fallback", String.valueOf(((c) aVar).j()));
            }
        }
        this.f = hashMap;
    }

    private g a(String str, com.applovin.impl.mediation.e eVar, Map<String, String> map) {
        return g.b(e()).a(a(str, eVar)).a(false).c(map).a();
    }

    private String a(String str, com.applovin.impl.mediation.e eVar) {
        int i;
        String str2 = "";
        if (eVar instanceof MaxAdapterError) {
            MaxAdapterError maxAdapterError = (MaxAdapterError) eVar;
            i = maxAdapterError.getThirdPartySdkErrorCode();
            str2 = maxAdapterError.getThirdPartySdkErrorMessage();
        } else {
            i = 0;
        }
        return str.replace("{ERROR_CODE}", String.valueOf(eVar.getErrorCode())).replace("{ERROR_MESSAGE}", m.d(eVar.getErrorMessage())).replace("{THIRD_PARTY_SDK_ERROR_CODE}", String.valueOf(i)).replace("{THIRD_PARTY_SDK_ERROR_MESSAGE}", m.d(str2));
    }

    private f b(String str, com.applovin.impl.mediation.e eVar, Map<String, String> map) {
        return f.k().a(a(str, eVar)).a(false).b(map).a();
    }

    private void b() {
        List<String> a2 = this.d.a(this.c, this.e);
        if (!a2.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Firing ");
            sb.append(a2.size());
            sb.append(" '");
            sb.append(this.f1207a);
            sb.append("' postback(s)");
            a(sb.toString());
            for (String a3 : a2) {
                e().R().dispatchPostbackRequest(a(a3, this.g, this.f), r.a.MEDIATION_POSTBACKS, new AppLovinPostbackListener() {
                    public void onPostbackFailure(String str, int i) {
                        d dVar = d.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to fire postback with code: ");
                        sb.append(i);
                        sb.append(" and url: ");
                        sb.append(str);
                        dVar.d(sb.toString());
                    }

                    public void onPostbackSuccess(String str) {
                        d dVar = d.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Successfully fired postback: ");
                        sb.append(str);
                        dVar.a(sb.toString());
                    }
                });
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("No postbacks to fire for event: ");
        sb2.append(this.f1207a);
        a(sb2.toString());
    }

    private void c() {
        List<String> a2 = this.d.a(this.c, this.e);
        if (!a2.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Firing ");
            sb.append(a2.size());
            sb.append(" '");
            sb.append(this.f1207a);
            sb.append("' persistent postback(s)");
            a(sb.toString());
            for (String b : a2) {
                e().N().a(b(b, this.g, this.f));
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("No persistent postbacks to fire for event: ");
        sb2.append(this.f1207a);
        a(sb2.toString());
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.I;
    }

    public void run() {
        if (((Boolean) e().a(b.j)).booleanValue()) {
            c();
        } else {
            b();
        }
    }
}
