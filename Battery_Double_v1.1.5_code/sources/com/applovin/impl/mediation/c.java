package com.applovin.impl.mediation;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.d;

public class c {

    /* renamed from: a reason: collision with root package name */
    private final i f1197a;
    /* access modifiers changed from: private */
    public final o b;
    /* access modifiers changed from: private */
    public final a c;
    private d d;

    public interface a {
        void c(com.applovin.impl.mediation.b.c cVar);
    }

    c(i iVar, a aVar) {
        this.f1197a = iVar;
        this.b = iVar.v();
        this.c = aVar;
    }

    public void a() {
        this.b.b("AdHiddenCallbackTimeoutManager", "Cancelling timeout");
        if (this.d != null) {
            this.d.a();
            this.d = null;
        }
    }

    public void a(final com.applovin.impl.mediation.b.c cVar, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("Scheduling in ");
        sb.append(j);
        sb.append("ms...");
        this.b.b("AdHiddenCallbackTimeoutManager", sb.toString());
        this.d = d.a(j, this.f1197a, new Runnable() {
            public void run() {
                c.this.b.b("AdHiddenCallbackTimeoutManager", "Timing out...");
                c.this.c.c(cVar);
            }
        });
    }
}
