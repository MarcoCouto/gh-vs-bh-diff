package com.applovin.impl.mediation.a.c.a;

import android.app.Activity;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ListView;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.mediation.MaxDebuggerDetailActivity;
import com.applovin.sdk.R;

public class a extends Activity {

    /* renamed from: a reason: collision with root package name */
    private b f1165a;
    private DataSetObserver b;
    private FrameLayout c;
    private ListView d;
    private com.applovin.impl.adview.a e;

    /* access modifiers changed from: private */
    public void a() {
        startActivity(new Intent(this, MaxDebuggerDetailActivity.class));
    }

    private void b() {
        c();
        this.e = new com.applovin.impl.adview.a(this, 50, 16842874);
        this.e.setColor(-3355444);
        this.c.addView(this.e, new LayoutParams(-1, -1, 17));
        this.c.bringChildToFront(this.e);
        this.e.a();
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.e != null) {
            this.e.b();
            this.c.removeView(this.e);
            this.e = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle("MAX Mediation Debugger");
        setContentView(R.layout.mediation_debugger_activity);
        this.c = (FrameLayout) findViewById(16908290);
        this.d = (ListView) findViewById(R.id.listView);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.f1165a.unregisterDataSetObserver(this.b);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.d.setAdapter(this.f1165a);
        if (!this.f1165a.a()) {
            b();
        }
    }

    public void setListAdapter(b bVar, final com.applovin.impl.sdk.a aVar) {
        if (!(this.f1165a == null || this.b == null)) {
            this.f1165a.unregisterDataSetObserver(this.b);
        }
        this.f1165a = bVar;
        this.b = new DataSetObserver() {
            public void onChanged() {
                a.this.c();
            }
        };
        this.f1165a.registerDataSetObserver(this.b);
        this.f1165a.a((com.applovin.impl.mediation.a.c.a.b.a) new com.applovin.impl.mediation.a.c.a.b.a() {
            public void a(final c cVar) {
                aVar.a(new com.applovin.impl.sdk.utils.a() {
                    public void onActivityDestroyed(Activity activity) {
                        if (activity instanceof com.applovin.impl.mediation.a.c.b.a) {
                            aVar.b(this);
                        }
                    }

                    public void onActivityStarted(Activity activity) {
                        if (activity instanceof com.applovin.impl.mediation.a.c.b.a) {
                            ((com.applovin.impl.mediation.a.c.b.a) activity).setNetwork(cVar);
                        }
                    }
                });
                a.this.a();
            }
        });
    }
}
