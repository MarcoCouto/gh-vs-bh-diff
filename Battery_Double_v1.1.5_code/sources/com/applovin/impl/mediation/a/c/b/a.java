package com.applovin.impl.mediation.a.c.b;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.os.Bundle;
import android.widget.ListView;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.sdk.R;

public class a extends Activity {

    /* renamed from: a reason: collision with root package name */
    private ListView f1170a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.mediation_debugger_detail_activity);
        this.f1170a = (ListView) findViewById(R.id.listView);
    }

    public void setNetwork(c cVar) {
        setTitle(cVar.d());
        b bVar = new b(cVar, this);
        bVar.a((com.applovin.impl.mediation.a.c.b.b.a) new com.applovin.impl.mediation.a.c.b.b.a() {
            public void a(String str) {
                new Builder(a.this, 16974130).setTitle(R.string.applovin_instructions_dialog_title).setMessage(str).setNegativeButton(17039370, null).create().show();
            }
        });
        this.f1170a.setAdapter(bVar);
    }
}
