package com.applovin.impl.mediation.a.c.a;

import android.content.Context;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.impl.mediation.a.a.f;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class b extends com.applovin.impl.mediation.a.c.a {
    private final AtomicBoolean c = new AtomicBoolean();
    private final com.applovin.impl.mediation.a.a.b d = new f("INCOMPLETE INTEGRATIONS");
    private final com.applovin.impl.mediation.a.a.b e = new f("COMPLETED INTEGRATIONS");
    private final com.applovin.impl.mediation.a.a.b f = new f("MISSING INTEGRATIONS");
    private final com.applovin.impl.mediation.a.a.b g = new f("");
    private a h;

    public interface a {
        void a(c cVar);
    }

    public b(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a(com.applovin.impl.mediation.a.a.b bVar) {
        if (this.h != null && (bVar instanceof com.applovin.impl.mediation.a.c.a.a.a)) {
            this.h.a(((com.applovin.impl.mediation.a.c.a.a.a) bVar).i());
        }
    }

    public void a(a aVar) {
        this.h = aVar;
    }

    public void a(List<c> list) {
        if (list != null && this.c.compareAndSet(false, true)) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (c cVar : list) {
                com.applovin.impl.mediation.a.c.a.a.a aVar = new com.applovin.impl.mediation.a.c.a.a.a(cVar, this.f1164a);
                if (cVar.a() == com.applovin.impl.mediation.a.a.c.a.INCOMPLETE_INTEGRATION || cVar.a() == com.applovin.impl.mediation.a.a.c.a.INVALID_INTEGRATION) {
                    arrayList.add(aVar);
                } else if (cVar.a() == com.applovin.impl.mediation.a.a.c.a.COMPLETE) {
                    arrayList2.add(aVar);
                } else if (cVar.a() == com.applovin.impl.mediation.a.a.c.a.MISSING) {
                    arrayList3.add(aVar);
                }
            }
            if (arrayList.size() > 0) {
                this.b.add(this.d);
                this.b.addAll(arrayList);
            }
            if (arrayList2.size() > 0) {
                this.b.add(this.e);
                this.b.addAll(arrayList2);
            }
            if (arrayList3.size() > 0) {
                this.b.add(this.f);
                this.b.addAll(arrayList3);
            }
            this.b.add(this.g);
        }
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                b.this.notifyDataSetChanged();
            }
        });
    }

    public boolean a() {
        return this.c.get();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediationDebuggerListAdapter{networksInitialized=");
        sb.append(this.c.get());
        sb.append(", listItems=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}
