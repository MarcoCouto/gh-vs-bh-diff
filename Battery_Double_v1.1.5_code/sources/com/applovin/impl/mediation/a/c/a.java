package com.applovin.impl.mediation.a.c;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.applovin.impl.mediation.a.a.b;
import com.applovin.sdk.R;
import java.util.ArrayList;
import java.util.List;

public abstract class a extends BaseAdapter implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    protected final Context f1164a;
    protected final List<b> b = new ArrayList();
    private final LayoutInflater c;

    protected a(Context context) {
        this.f1164a = context;
        this.c = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* renamed from: a */
    public b getItem(int i) {
        return (b) this.b.get(i);
    }

    /* access modifiers changed from: protected */
    public abstract void a(b bVar);

    public boolean areAllItemsEnabled() {
        return false;
    }

    public int getCount() {
        return this.b.size();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public int getItemViewType(int i) {
        return getItem(i).e();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        com.applovin.impl.mediation.a.a.a aVar;
        b a2 = getItem(i);
        if (view == null) {
            view = this.c.inflate(a2.f(), viewGroup, false);
            aVar = new com.applovin.impl.mediation.a.a.a();
            aVar.f1155a = (TextView) view.findViewById(16908308);
            aVar.b = (TextView) view.findViewById(16908309);
            aVar.c = (ImageView) view.findViewById(R.id.imageView);
            view.setTag(aVar);
            view.setOnClickListener(this);
        } else {
            aVar = (com.applovin.impl.mediation.a.a.a) view.getTag();
        }
        aVar.a(a2);
        view.setEnabled(a2.b());
        return view;
    }

    public int getViewTypeCount() {
        return b.a();
    }

    public boolean isEnabled(int i) {
        return getItem(i).b();
    }

    public void onClick(View view) {
        a(((com.applovin.impl.mediation.a.a.a) view.getTag()).a());
    }
}
