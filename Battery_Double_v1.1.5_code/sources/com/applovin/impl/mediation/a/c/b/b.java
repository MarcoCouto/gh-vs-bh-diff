package com.applovin.impl.mediation.a.c.b;

import android.content.Context;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.impl.mediation.a.a.d;
import com.applovin.impl.mediation.a.a.e;
import com.applovin.impl.mediation.a.a.f;
import com.applovin.impl.mediation.a.c.b.a.a.C0009a;
import com.applovin.sdk.R;
import com.appodeal.ads.utils.LogConstants;
import java.util.ArrayList;
import java.util.List;

public class b extends com.applovin.impl.mediation.a.c.a {
    private final com.applovin.impl.mediation.a.a.b c = new f("INTEGRATIONS");
    private final com.applovin.impl.mediation.a.a.b d = new f("PERMISSIONS");
    private final com.applovin.impl.mediation.a.a.b e = new f("CONFIGURATION");
    private final com.applovin.impl.mediation.a.a.b f = new f("");
    private SpannedString g;
    private a h;

    public interface a {
        void a(String str);
    }

    b(c cVar, Context context) {
        super(context);
        if (cVar.a() == com.applovin.impl.mediation.a.a.c.a.INVALID_INTEGRATION) {
            SpannableString spannableString = new SpannableString("Tap for more information");
            spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, spannableString.length(), 33);
            this.g = new SpannedString(spannableString);
        } else {
            this.g = new SpannedString("");
        }
        this.b.add(this.c);
        this.b.add(a(cVar));
        this.b.add(b(cVar));
        this.b.addAll(a(cVar.g()));
        this.b.addAll(a(cVar.h()));
        this.b.add(this.f);
    }

    private int a(boolean z) {
        return z ? R.drawable.applovin_ic_check_mark : R.drawable.applovin_ic_x_mark;
    }

    private int b(boolean z) {
        return com.applovin.impl.sdk.utils.f.a(z ? R.color.applovin_sdk_checkmarkColor : R.color.applovin_sdk_xmarkColor, this.f1164a);
    }

    public com.applovin.impl.mediation.a.a.b a(c cVar) {
        C0009a a2 = com.applovin.impl.mediation.a.c.b.a.a.j().a(LogConstants.KEY_SDK).b(cVar.e()).a(TextUtils.isEmpty(cVar.e()) ? com.applovin.impl.mediation.a.a.b.a.DETAIL : com.applovin.impl.mediation.a.a.b.a.RIGHT_DETAIL);
        if (TextUtils.isEmpty(cVar.e())) {
            a2.a(a(cVar.b())).b(b(cVar.b()));
        }
        return a2.a();
    }

    public List<com.applovin.impl.mediation.a.a.b> a(d dVar) {
        ArrayList arrayList = new ArrayList(2);
        if (dVar.a()) {
            boolean b = dVar.b();
            arrayList.add(this.e);
            arrayList.add(com.applovin.impl.mediation.a.c.b.a.a.j().a("Cleartext Traffic").a(b ? null : this.g).c(dVar.c()).a(a(b)).b(b(b)).a(!b).a());
        }
        return arrayList;
    }

    public List<com.applovin.impl.mediation.a.a.b> a(List<e> list) {
        ArrayList arrayList = new ArrayList(list.size() + 1);
        if (list.size() > 0) {
            arrayList.add(this.d);
            for (e eVar : list) {
                boolean c2 = eVar.c();
                arrayList.add(com.applovin.impl.mediation.a.c.b.a.a.j().a(eVar.a()).a(c2 ? null : this.g).c(eVar.b()).a(a(c2)).b(b(c2)).a(!c2).a());
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(com.applovin.impl.mediation.a.a.b bVar) {
        if (this.h != null && (bVar instanceof com.applovin.impl.mediation.a.c.b.a.a)) {
            String i = ((com.applovin.impl.mediation.a.c.b.a.a) bVar).i();
            if (!TextUtils.isEmpty(i)) {
                this.h.a(i);
            }
        }
    }

    public void a(a aVar) {
        this.h = aVar;
    }

    public com.applovin.impl.mediation.a.a.b b(c cVar) {
        C0009a a2 = com.applovin.impl.mediation.a.c.b.a.a.j().a("Adapter").b(cVar.f()).a(TextUtils.isEmpty(cVar.f()) ? com.applovin.impl.mediation.a.a.b.a.DETAIL : com.applovin.impl.mediation.a.a.b.a.RIGHT_DETAIL);
        if (TextUtils.isEmpty(cVar.f())) {
            a2.a(a(cVar.c())).b(b(cVar.c()));
        }
        return a2.a();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedNetworkListAdapter{listItems=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}
