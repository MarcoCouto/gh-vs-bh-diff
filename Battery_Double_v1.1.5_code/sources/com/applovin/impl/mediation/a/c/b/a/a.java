package com.applovin.impl.mediation.a.c.b.a;

import android.text.SpannedString;
import com.applovin.impl.mediation.a.a.b;

public class a extends b {
    final String d;
    final int e;
    final int f;
    final boolean g;

    /* renamed from: com.applovin.impl.mediation.a.c.b.a.a$a reason: collision with other inner class name */
    public static class C0009a {

        /* renamed from: a reason: collision with root package name */
        SpannedString f1172a;
        SpannedString b;
        String c;
        com.applovin.impl.mediation.a.a.b.a d = com.applovin.impl.mediation.a.a.b.a.DETAIL;
        int e;
        int f;
        boolean g = false;

        public C0009a a(int i) {
            this.e = i;
            return this;
        }

        public C0009a a(SpannedString spannedString) {
            this.b = spannedString;
            return this;
        }

        public C0009a a(com.applovin.impl.mediation.a.a.b.a aVar) {
            this.d = aVar;
            return this;
        }

        public C0009a a(String str) {
            this.f1172a = new SpannedString(str);
            return this;
        }

        public C0009a a(boolean z) {
            this.g = z;
            return this;
        }

        public a a() {
            return new a(this);
        }

        public C0009a b(int i) {
            this.f = i;
            return this;
        }

        public C0009a b(String str) {
            return a(new SpannedString(str));
        }

        public C0009a c(String str) {
            this.c = str;
            return this;
        }
    }

    private a(C0009a aVar) {
        super(aVar.d);
        this.b = aVar.f1172a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
    }

    public static C0009a j() {
        return new C0009a();
    }

    public boolean b() {
        return this.g;
    }

    public int g() {
        return this.e;
    }

    public int h() {
        return this.f;
    }

    public String i() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("NetworkDetailListItemViewModel{text=");
        sb.append(this.b);
        sb.append(", detailText=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}
