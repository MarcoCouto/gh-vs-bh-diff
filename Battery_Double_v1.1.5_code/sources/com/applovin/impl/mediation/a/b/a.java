package com.applovin.impl.mediation.a.b;

import android.os.Build.VERSION;
import com.applovin.impl.sdk.d.x;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.j.b;
import com.applovin.impl.sdk.network.a.c;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class a extends com.applovin.impl.sdk.d.a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final c<JSONObject> f1162a;

    public a(c<JSONObject> cVar, i iVar) {
        super("TaskFetchMediationDebuggerInfo", iVar, true);
        this.f1162a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.J;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap();
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", String.valueOf(131));
        if (!((Boolean) this.b.a(com.applovin.impl.sdk.b.c.eJ)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.t());
        }
        b c = this.b.O().c();
        hashMap.put(CampaignEx.JSON_KEY_PACKAGE_NAME, m.d(c.c));
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, m.d(c.b));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, "android");
        hashMap.put("os", m.d(VERSION.RELEASE));
        return hashMap;
    }

    public void run() {
        AnonymousClass1 r1 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(com.applovin.impl.mediation.d.b.c(this.b)).c(com.applovin.impl.mediation.d.b.d(this.b)).a(b()).b(HttpRequest.METHOD_GET).a(new JSONObject()).b(((Long) this.b.a(com.applovin.impl.sdk.b.b.g)).intValue()).a(), this.b, h()) {
            public void a(int i) {
                a.this.f1162a.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                a.this.f1162a.a(jSONObject, i);
            }
        };
        r1.a(com.applovin.impl.sdk.b.b.c);
        r1.b(com.applovin.impl.sdk.b.b.d);
        this.b.K().a((com.applovin.impl.sdk.d.a) r1);
    }
}
