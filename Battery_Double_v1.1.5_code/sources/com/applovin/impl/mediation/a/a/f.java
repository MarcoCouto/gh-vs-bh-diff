package com.applovin.impl.mediation.a.a;

import android.text.SpannedString;
import com.applovin.impl.mediation.a.a.b.a;

public class f extends b {
    public f(String str) {
        super(a.SECTION);
        this.b = new SpannedString(str);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SectionListItemViewModel{text=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}
