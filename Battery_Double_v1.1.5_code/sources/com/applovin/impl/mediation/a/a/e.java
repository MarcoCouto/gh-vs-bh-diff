package com.applovin.impl.mediation.a.a;

import android.content.Context;
import com.applovin.impl.sdk.utils.g;

public class e {

    /* renamed from: a reason: collision with root package name */
    private final String f1161a;
    private final String b;
    private final boolean c;

    e(String str, String str2, Context context) {
        this.f1161a = str;
        this.b = str2;
        this.c = g.a(str, context);
    }

    public String a() {
        return this.f1161a;
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }
}
