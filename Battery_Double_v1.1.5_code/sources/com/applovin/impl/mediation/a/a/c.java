package com.applovin.impl.mediation.a.a;

import android.content.Context;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class c implements Comparable<c> {

    /* renamed from: a reason: collision with root package name */
    private static String f1158a = "MediatedNetwork";
    private final a b;
    private final boolean c;
    private final boolean d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;
    private final List<MaxAdFormat> i;
    private final List<e> j;
    private final d k;

    public enum a {
        MISSING,
        INCOMPLETE_INTEGRATION,
        INVALID_INTEGRATION,
        COMPLETE
    }

    public c(JSONObject jSONObject, i iVar) {
        this.e = com.applovin.impl.sdk.utils.i.b(jSONObject, "display_name", "", iVar);
        this.h = com.applovin.impl.sdk.utils.i.b(jSONObject, "name", "", iVar);
        JSONObject b2 = com.applovin.impl.sdk.utils.i.b(jSONObject, "configuration", new JSONObject(), iVar);
        this.j = a(b2, iVar, iVar.D());
        this.k = new d(b2, iVar);
        this.c = p.e(com.applovin.impl.sdk.utils.i.b(jSONObject, "existence_class", "", iVar));
        String str = "";
        String str2 = "";
        List<MaxAdFormat> emptyList = Collections.emptyList();
        MaxAdapter a2 = com.applovin.impl.mediation.d.c.a(com.applovin.impl.sdk.utils.i.b(jSONObject, "adapter_class", "", iVar), iVar);
        if (a2 != null) {
            this.d = true;
            try {
                String adapterVersion = a2.getAdapterVersion();
                try {
                    String sdkVersion = a2.getSdkVersion();
                    try {
                        emptyList = a(a2);
                        str2 = sdkVersion;
                        str = adapterVersion;
                    } catch (Throwable th) {
                        th = th;
                        str2 = sdkVersion;
                        str = adapterVersion;
                        String str3 = f1158a;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to load adapter for network ");
                        sb.append(this.e);
                        sb.append(". Please check that you have a compatible network SDK integrated. Error: ");
                        sb.append(th);
                        o.i(str3, sb.toString());
                        this.g = str;
                        this.f = str2;
                        this.i = emptyList;
                        this.b = i();
                    }
                } catch (Throwable th2) {
                    th = th2;
                    str = adapterVersion;
                    String str32 = f1158a;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to load adapter for network ");
                    sb2.append(this.e);
                    sb2.append(". Please check that you have a compatible network SDK integrated. Error: ");
                    sb2.append(th);
                    o.i(str32, sb2.toString());
                    this.g = str;
                    this.f = str2;
                    this.i = emptyList;
                    this.b = i();
                }
            } catch (Throwable th3) {
                th = th3;
                String str322 = f1158a;
                StringBuilder sb22 = new StringBuilder();
                sb22.append("Failed to load adapter for network ");
                sb22.append(this.e);
                sb22.append(". Please check that you have a compatible network SDK integrated. Error: ");
                sb22.append(th);
                o.i(str322, sb22.toString());
                this.g = str;
                this.f = str2;
                this.i = emptyList;
                this.b = i();
            }
        } else {
            this.d = false;
        }
        this.g = str;
        this.f = str2;
        this.i = emptyList;
        this.b = i();
    }

    private List<MaxAdFormat> a(MaxAdapter maxAdapter) {
        ArrayList arrayList = new ArrayList(5);
        if (maxAdapter instanceof MaxInterstitialAdapter) {
            arrayList.add(MaxAdFormat.INTERSTITIAL);
        }
        if (maxAdapter instanceof MaxRewardedAdapter) {
            arrayList.add(MaxAdFormat.REWARDED);
        }
        if (maxAdapter instanceof MaxAdViewAdapter) {
            arrayList.add(MaxAdFormat.BANNER);
            arrayList.add(MaxAdFormat.LEADER);
            arrayList.add(MaxAdFormat.MREC);
        }
        return arrayList;
    }

    private List<e> a(JSONObject jSONObject, i iVar, Context context) {
        ArrayList arrayList = new ArrayList();
        JSONObject b2 = com.applovin.impl.sdk.utils.i.b(jSONObject, NativeProtocol.RESULT_ARGS_PERMISSIONS, new JSONObject(), iVar);
        Iterator keys = b2.keys();
        while (keys.hasNext()) {
            try {
                String str = (String) keys.next();
                arrayList.add(new e(str, b2.getString(str), context));
            } catch (JSONException unused) {
            }
        }
        return arrayList;
    }

    private a i() {
        if (!this.c && !this.d) {
            return a.MISSING;
        }
        for (e c2 : this.j) {
            if (!c2.c()) {
                return a.INVALID_INTEGRATION;
            }
        }
        return (!this.k.a() || this.k.b()) ? (!this.c || !this.d) ? a.INCOMPLETE_INTEGRATION : a.COMPLETE : a.INVALID_INTEGRATION;
    }

    /* renamed from: a */
    public int compareTo(c cVar) {
        return this.e.compareToIgnoreCase(cVar.e);
    }

    public a a() {
        return this.b;
    }

    public boolean b() {
        return this.c;
    }

    public boolean c() {
        return this.d;
    }

    public String d() {
        return this.e;
    }

    public String e() {
        return this.f;
    }

    public String f() {
        return this.g;
    }

    public List<e> g() {
        return this.j;
    }

    public final d h() {
        return this.k;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedNetwork{name=");
        sb.append(this.e);
        sb.append(", sdkAvailable=");
        sb.append(this.c);
        sb.append(", sdkVersion=");
        sb.append(this.f);
        sb.append(", adapterAvailable=");
        sb.append(this.d);
        sb.append(", adapterVersion=");
        sb.append(this.g);
        sb.append("}");
        return sb.toString();
    }
}
