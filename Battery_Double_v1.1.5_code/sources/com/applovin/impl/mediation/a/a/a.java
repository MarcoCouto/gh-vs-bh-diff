package com.applovin.impl.mediation.a.a;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class a {

    /* renamed from: a reason: collision with root package name */
    public TextView f1155a;
    public TextView b;
    public ImageView c;
    private b d;

    public b a() {
        return this.d;
    }

    public void a(b bVar) {
        this.d = bVar;
        this.f1155a.setText(bVar.c());
        if (this.b != null) {
            if (!TextUtils.isEmpty(bVar.d())) {
                this.b.setVisibility(0);
                this.b.setText(bVar.d());
            } else {
                this.b.setVisibility(8);
            }
        }
        if (this.c == null) {
            return;
        }
        if (bVar.g() > 0) {
            this.c.setImageResource(bVar.g());
            this.c.setColorFilter(bVar.h());
            this.c.setVisibility(0);
            return;
        }
        this.c.setVisibility(8);
    }
}
