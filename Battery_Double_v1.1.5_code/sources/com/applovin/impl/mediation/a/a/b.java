package com.applovin.impl.mediation.a.a;

import android.support.v4.view.ViewCompat;
import android.text.SpannedString;
import com.applovin.sdk.R;

public abstract class b {

    /* renamed from: a reason: collision with root package name */
    protected a f1156a;
    protected SpannedString b;
    protected SpannedString c;

    public enum a {
        SECTION(0),
        SIMPLE(1),
        DETAIL(2),
        RIGHT_DETAIL(3),
        COUNT(4);
        
        private final int f;

        private a(int i) {
            this.f = i;
        }

        public int a() {
            return this.f;
        }

        public int b() {
            if (this == SECTION) {
                return R.layout.list_section;
            }
            if (this == SIMPLE) {
                return 17367043;
            }
            return this == DETAIL ? R.layout.list_item_detail : R.layout.list_item_right_detail;
        }
    }

    public b(a aVar) {
        this.f1156a = aVar;
    }

    public static int a() {
        return a.COUNT.a();
    }

    public boolean b() {
        return false;
    }

    public SpannedString c() {
        return this.b;
    }

    public SpannedString d() {
        return this.c;
    }

    public int e() {
        return this.f1156a.a();
    }

    public int f() {
        return this.f1156a.b();
    }

    public int g() {
        return 0;
    }

    public int h() {
        return ViewCompat.MEASURED_STATE_MASK;
    }
}
