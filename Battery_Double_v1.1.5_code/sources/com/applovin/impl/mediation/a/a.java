package com.applovin.impl.mediation.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.applovin.impl.mediation.a.c.a.b;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.a.c;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.MaxDebuggerActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class a implements c<JSONObject> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static WeakReference<MaxDebuggerActivity> f1153a;
    /* access modifiers changed from: private */
    public static final AtomicBoolean b = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final i c;
    private final o d;
    /* access modifiers changed from: private */
    public final b e;
    private final AtomicBoolean f = new AtomicBoolean();
    private boolean g;

    public a(i iVar) {
        this.c = iVar;
        this.d = iVar.v();
        this.e = new b(iVar.D());
    }

    private void a(JSONArray jSONArray) {
        this.d.b("MediationDebuggerService", "Updating networks...");
        try {
            ArrayList arrayList = new ArrayList(jSONArray.length());
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject a2 = com.applovin.impl.sdk.utils.i.a(jSONArray, i, (JSONObject) null, this.c);
                if (a2 != null) {
                    arrayList.add(new com.applovin.impl.mediation.a.a.c(a2, this.c));
                }
            }
            Collections.sort(arrayList);
            this.e.a((List<com.applovin.impl.mediation.a.a.c>) arrayList);
        } catch (Throwable th) {
            this.d.b("MediationDebuggerService", "Failed to parse mediated network json object.", th);
        }
    }

    private void e() {
        if (this.f.compareAndSet(false, true)) {
            this.c.K().a((com.applovin.impl.sdk.d.a) new com.applovin.impl.mediation.a.b.a(this, this.c), com.applovin.impl.sdk.d.r.a.MEDIATION_MAIN);
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        return (f1153a == null || f1153a.get() == null) ? false : true;
    }

    public void a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("Unable to fetch mediation debugger info: server returned ");
        sb.append(i);
        this.d.e("MediationDebuggerService", sb.toString());
        o.i("AppLovinSdk", "Unable to show mediation debugger.");
        this.e.a(null);
        this.f.set(false);
    }

    public void a(JSONObject jSONObject, int i) {
        a(com.applovin.impl.sdk.utils.i.b(jSONObject, "networks", new JSONArray(), this.c));
    }

    public void a(boolean z) {
        this.g = z;
    }

    public boolean a() {
        return this.g;
    }

    public void b() {
        e();
        if (f() || !b.compareAndSet(false, true)) {
            o.i("AppLovinSdk", "Mediation Debugger is already showing.");
            return;
        }
        this.c.aa().a(new com.applovin.impl.sdk.utils.a() {
            public void onActivityDestroyed(Activity activity) {
                if (activity instanceof MaxDebuggerActivity) {
                    a.this.c.aa().b(this);
                    a.f1153a = null;
                }
            }

            public void onActivityStarted(Activity activity) {
                if (activity instanceof MaxDebuggerActivity) {
                    if (!a.this.f() || a.f1153a.get() != activity) {
                        MaxDebuggerActivity maxDebuggerActivity = (MaxDebuggerActivity) activity;
                        a.f1153a = new WeakReference(maxDebuggerActivity);
                        maxDebuggerActivity.setListAdapter(a.this.e, a.this.c.aa());
                    }
                    a.b.set(false);
                }
            }
        });
        Context D = this.c.D();
        Intent intent = new Intent(D, MaxDebuggerActivity.class);
        intent.setFlags(268435456);
        D.startActivity(intent);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediationDebuggerService{, listAdapter=");
        sb.append(this.e);
        sb.append("}");
        return sb.toString();
    }
}
