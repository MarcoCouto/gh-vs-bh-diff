package com.applovin.impl.mediation.b;

import com.applovin.impl.sdk.i;
import org.json.JSONObject;

public class g extends e {
    public g(JSONObject jSONObject, JSONObject jSONObject2, i iVar) {
        super(jSONObject, jSONObject2, iVar);
    }

    /* access modifiers changed from: 0000 */
    public int a() {
        return a("max_signal_length", 2048);
    }

    public boolean b() {
        return b("only_collect_signal_when_initialized", Boolean.valueOf(false));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SignalProviderSpec{specObject=");
        sb.append(x());
        sb.append('}');
        return sb.toString();
    }
}
