package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.b.b;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class c extends a {
    private String c;
    private final AtomicReference<com.applovin.impl.sdk.a.c> d;
    private final AtomicBoolean e;

    private c(c cVar, i iVar) {
        super(cVar.x(), cVar.w(), iVar, cVar.b);
        this.d = cVar.d;
        this.e = cVar.e;
    }

    public c(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.i iVar) {
        super(jSONObject, jSONObject2, null, iVar);
        this.d = new AtomicReference<>();
        this.e = new AtomicBoolean();
    }

    public a a(i iVar) {
        return new c(this, iVar);
    }

    public void a(com.applovin.impl.sdk.a.c cVar) {
        this.d.set(cVar);
    }

    public void a(String str) {
        this.c = str;
    }

    public boolean j() {
        return b("fa", Boolean.valueOf(false));
    }

    public long k() {
        return b("ifacd_ms", -1);
    }

    public long l() {
        return b("fard_ms", TimeUnit.HOURS.toMillis(1));
    }

    public String m() {
        return this.c;
    }

    public long n() {
        long b = b("ad_expiration_ms", -1);
        return b >= 0 ? b : a("ad_expiration_ms", ((Long) this.b.a(b.G)).longValue());
    }

    public long o() {
        long b = b("ad_hidden_timeout_ms", -1);
        return b >= 0 ? b : a("ad_hidden_timeout_ms", ((Long) this.b.a(b.J)).longValue());
    }

    public boolean p() {
        if (b("schedule_ad_hidden_on_ad_dismiss", Boolean.valueOf(false))) {
            return true;
        }
        return a("schedule_ad_hidden_on_ad_dismiss", (Boolean) this.b.a(b.K));
    }

    public long q() {
        long b = b("ad_hidden_on_ad_dismiss_callback_delay_ms", -1);
        return b >= 0 ? b : a("ad_hidden_on_ad_dismiss_callback_delay_ms", ((Long) this.b.a(b.L)).longValue());
    }

    public String r() {
        return b("bcode", "");
    }

    public String s() {
        return a("mcode", "");
    }

    public boolean t() {
        return this.e.get();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedFullscreenAd{format=");
        sb.append(getFormat());
        sb.append(", adUnitId=");
        sb.append(getAdUnitId());
        sb.append(", isReady=");
        sb.append(a());
        sb.append(", adapterClass='");
        sb.append(y());
        sb.append("', adapterName='");
        sb.append(z());
        sb.append("', isTesting=");
        sb.append(A());
        sb.append(", isRefreshEnabled=");
        sb.append(E());
        sb.append(", getAdRefreshMillis=");
        sb.append(F());
        sb.append('}');
        return sb.toString();
    }

    public void u() {
        this.e.set(true);
    }

    public com.applovin.impl.sdk.a.c v() {
        return (com.applovin.impl.sdk.a.c) this.d.getAndSet(null);
    }
}
