package com.applovin.impl.mediation.b;

import android.content.Context;
import android.os.Bundle;
import com.applovin.impl.sdk.b.b;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinPrivacySettings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e {

    /* renamed from: a reason: collision with root package name */
    private final JSONObject f1195a;
    protected final i b;
    private final JSONObject c;
    private final Object d = new Object();
    private final Object e = new Object();
    private volatile String f;

    public e(JSONObject jSONObject, JSONObject jSONObject2, i iVar) {
        if (iVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (jSONObject2 == null) {
            throw new IllegalArgumentException("No full response specified");
        } else if (jSONObject != null) {
            this.b = iVar;
            this.f1195a = jSONObject2;
            this.c = jSONObject;
        } else {
            throw new IllegalArgumentException("No spec object specified");
        }
    }

    private List<String> a(String str) {
        try {
            return com.applovin.impl.sdk.utils.i.b(a(str, new JSONArray()));
        } catch (JSONException unused) {
            return Collections.EMPTY_LIST;
        }
    }

    private List<String> a(List<String> list, Map<String, String> map) {
        Map a2 = a();
        ArrayList arrayList = new ArrayList(list.size());
        for (String str : list) {
            for (String str2 : a2.keySet()) {
                str = str.replace(str2, f((String) a2.get(str2)));
            }
            for (String str3 : map.keySet()) {
                str = str.replace(str3, (CharSequence) map.get(str3));
            }
            arrayList.add(str);
        }
        return arrayList;
    }

    private Map<String, String> a() {
        try {
            return com.applovin.impl.sdk.utils.i.a(new JSONObject((String) this.b.a(b.i)));
        } catch (JSONException unused) {
            return Collections.EMPTY_MAP;
        }
    }

    private List<String> e(String str) {
        try {
            return com.applovin.impl.sdk.utils.i.b(b(str, new JSONArray()));
        } catch (JSONException unused) {
            return Collections.EMPTY_LIST;
        }
    }

    private String f(String str) {
        String b2 = b(str, "");
        return m.b(b2) ? b2 : a(str, "");
    }

    public boolean A() {
        return b("is_testing", Boolean.valueOf(false));
    }

    public boolean B() {
        return b("run_on_ui_thread", Boolean.valueOf(true));
    }

    public Bundle C() {
        if (c("server_parameters") instanceof JSONObject) {
            JSONObject a2 = a("server_parameters", (JSONObject) null);
            if (a2 != null) {
                return com.applovin.impl.sdk.utils.i.c(a2);
            }
        }
        return Bundle.EMPTY;
    }

    public long D() {
        return b("adapter_timeout_ms", ((Long) this.b.a(b.o)).longValue());
    }

    public boolean E() {
        return F() >= 0;
    }

    public long F() {
        long b2 = b("ad_refresh_ms", -1);
        return b2 >= 0 ? b2 : a("ad_refresh_ms", ((Long) this.b.a(b.r)).longValue());
    }

    public long G() {
        long b2 = b("fullscreen_display_delay_ms", -1);
        return b2 >= 0 ? b2 : ((Long) this.b.a(b.A)).longValue();
    }

    public long H() {
        return b("init_completion_delay_ms", -1);
    }

    public long I() {
        return b("ahdm", ((Long) this.b.a(b.B)).longValue());
    }

    public String J() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public float a(String str, float f2) {
        float a2;
        synchronized (this.d) {
            a2 = com.applovin.impl.sdk.utils.i.a(this.c, str, f2, this.b);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public int a(String str, int i) {
        int b2;
        synchronized (this.d) {
            b2 = com.applovin.impl.sdk.utils.i.b(this.c, str, i, this.b);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public long a(String str, long j) {
        long a2;
        synchronized (this.e) {
            a2 = com.applovin.impl.sdk.utils.i.a(this.f1195a, str, j, this.b);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public String a(String str, String str2) {
        String b2;
        synchronized (this.e) {
            b2 = com.applovin.impl.sdk.utils.i.b(this.f1195a, str, str2, this.b);
        }
        return b2;
    }

    public List<String> a(String str, Map<String, String> map) {
        if (str != null) {
            List a2 = a(str);
            List e2 = e(str);
            ArrayList arrayList = new ArrayList(a2.size() + e2.size());
            arrayList.addAll(a2);
            arrayList.addAll(e2);
            return a((List<String>) arrayList, map);
        }
        throw new IllegalArgumentException("No key specified");
    }

    /* access modifiers changed from: protected */
    public JSONArray a(String str, JSONArray jSONArray) {
        JSONArray b2;
        synchronized (this.e) {
            b2 = com.applovin.impl.sdk.utils.i.b(this.f1195a, str, jSONArray, this.b);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject a(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.d) {
            b2 = com.applovin.impl.sdk.utils.i.b(this.c, str, jSONObject, this.b);
        }
        return b2;
    }

    public boolean a(Context context) {
        return b("huc") ? b("huc", Boolean.valueOf(false)) : a("huc", Boolean.valueOf(AppLovinPrivacySettings.hasUserConsent(context)));
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, Boolean bool) {
        boolean booleanValue;
        synchronized (this.e) {
            booleanValue = com.applovin.impl.sdk.utils.i.a(this.f1195a, str, bool, this.b).booleanValue();
        }
        return booleanValue;
    }

    /* access modifiers changed from: protected */
    public long b(String str, long j) {
        long a2;
        synchronized (this.d) {
            a2 = com.applovin.impl.sdk.utils.i.a(this.c, str, j, this.b);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        String b2;
        synchronized (this.d) {
            b2 = com.applovin.impl.sdk.utils.i.b(this.c, str, str2, this.b);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONArray b(String str, JSONArray jSONArray) {
        JSONArray b2;
        synchronized (this.d) {
            b2 = com.applovin.impl.sdk.utils.i.b(this.c, str, jSONArray, this.b);
        }
        return b2;
    }

    public boolean b(Context context) {
        return b("aru") ? b("aru", Boolean.valueOf(false)) : a("aru", Boolean.valueOf(AppLovinPrivacySettings.isAgeRestrictedUser(context)));
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        boolean has;
        synchronized (this.d) {
            has = this.c.has(str);
        }
        return has;
    }

    /* access modifiers changed from: protected */
    public boolean b(String str, Boolean bool) {
        boolean booleanValue;
        synchronized (this.d) {
            booleanValue = com.applovin.impl.sdk.utils.i.a(this.c, str, bool, this.b).booleanValue();
        }
        return booleanValue;
    }

    /* access modifiers changed from: protected */
    public Object c(String str) {
        Object opt;
        synchronized (this.d) {
            opt = this.c.opt(str);
        }
        return opt;
    }

    /* access modifiers changed from: protected */
    public void c(String str, long j) {
        synchronized (this.d) {
            com.applovin.impl.sdk.utils.i.b(this.c, str, j, this.b);
        }
    }

    public void d(String str) {
        this.f = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediationAdapterSpec{adapterClass='");
        sb.append(y());
        sb.append("', adapterName='");
        sb.append(z());
        sb.append("', isTesting=");
        sb.append(A());
        sb.append(", isRefreshEnabled=");
        sb.append(E());
        sb.append(", getAdRefreshMillis=");
        sb.append(F());
        sb.append('}');
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public JSONObject w() {
        JSONObject jSONObject;
        synchronized (this.e) {
            jSONObject = this.f1195a;
        }
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public JSONObject x() {
        JSONObject jSONObject;
        synchronized (this.d) {
            jSONObject = this.c;
        }
        return jSONObject;
    }

    public String y() {
        return b("class", (String) null);
    }

    public String z() {
        return b("name", (String) null);
    }
}
