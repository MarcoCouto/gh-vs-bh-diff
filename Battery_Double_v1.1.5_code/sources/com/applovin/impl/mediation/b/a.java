package com.applovin.impl.mediation.b;

import android.os.SystemClock;
import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public abstract class a extends e implements MaxAd {

    /* renamed from: a reason: collision with root package name */
    protected i f1194a;
    private final AtomicBoolean c = new AtomicBoolean();

    protected a(JSONObject jSONObject, JSONObject jSONObject2, i iVar, com.applovin.impl.sdk.i iVar2) {
        super(jSONObject, jSONObject2, iVar2);
        this.f1194a = iVar;
    }

    private long j() {
        return b("load_started_time_ms", 0);
    }

    public abstract a a(i iVar);

    public boolean a() {
        return this.f1194a != null && this.f1194a.c() && this.f1194a.d();
    }

    public String b() {
        return a("event_id", "");
    }

    public i c() {
        return this.f1194a;
    }

    public String d() {
        return b("bid_response", (String) null);
    }

    public String e() {
        return b("third_party_ad_placement_id", (String) null);
    }

    public long f() {
        if (j() > 0) {
            return SystemClock.elapsedRealtime() - j();
        }
        return -1;
    }

    public void g() {
        c("load_started_time_ms", SystemClock.elapsedRealtime());
    }

    public String getAdUnitId() {
        return a("ad_unit_id", "");
    }

    public MaxAdFormat getFormat() {
        return p.c(a("ad_format", (String) null));
    }

    public AtomicBoolean h() {
        return this.c;
    }

    public void i() {
        this.f1194a = null;
    }
}
