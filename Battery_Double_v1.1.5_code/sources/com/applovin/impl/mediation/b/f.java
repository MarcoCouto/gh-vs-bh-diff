package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.i;

public class f {

    /* renamed from: a reason: collision with root package name */
    private final g f1196a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    public interface a {
        void a(f fVar);
    }

    private f(g gVar, i iVar, String str, String str2) {
        this.f1196a = gVar;
        this.e = str2;
        if (str != null) {
            this.d = str.substring(0, Math.min(str.length(), gVar.a()));
        } else {
            this.d = null;
        }
        if (iVar != null) {
            this.b = iVar.e();
            this.c = iVar.f();
            return;
        }
        this.b = null;
        this.c = null;
    }

    public static f a(g gVar, i iVar, String str) {
        if (gVar == null) {
            throw new IllegalArgumentException("No spec specified");
        } else if (iVar != null) {
            return new f(gVar, iVar, str, null);
        } else {
            throw new IllegalArgumentException("No adapterWrapper specified");
        }
    }

    public static f a(g gVar, String str) {
        return b(gVar, null, str);
    }

    public static f b(g gVar, i iVar, String str) {
        if (gVar != null) {
            return new f(gVar, iVar, null, str);
        }
        throw new IllegalArgumentException("No spec specified");
    }

    public g a() {
        return this.f1196a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SignalCollectionResult{mSignalProviderSpec=");
        sb.append(this.f1196a);
        sb.append(", mSdkVersion='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", mAdapterVersion='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", mSignalDataLength='");
        sb.append(this.d != null ? this.d.length() : 0);
        sb.append('\'');
        sb.append(", mErrorMessage=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
