package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.i;
import org.json.JSONObject;

public class d extends a {
    private d(d dVar, i iVar) {
        super(dVar.x(), dVar.w(), iVar, dVar.b);
    }

    public d(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.i iVar) {
        super(jSONObject, jSONObject2, null, iVar);
    }

    public a a(i iVar) {
        return new d(this, iVar);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedNativeAd{format=");
        sb.append(getFormat());
        sb.append(", adUnitId=");
        sb.append(getAdUnitId());
        sb.append(", isReady=");
        sb.append(a());
        sb.append(", adapterClass='");
        sb.append(y());
        sb.append("', adapterName='");
        sb.append(z());
        sb.append("', isTesting=");
        sb.append(A());
        sb.append(", isRefreshEnabled=");
        sb.append(E());
        sb.append(", getAdRefreshMillis=");
        sb.append(F());
        sb.append('}');
        return sb.toString();
    }
}
