package com.applovin.impl.mediation;

import android.app.Activity;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.adapter.MaxAdapter.InitializationStatus;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.facebook.internal.AnalyticsEvents;
import java.util.LinkedHashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class g {

    /* renamed from: a reason: collision with root package name */
    private final i f1221a;
    private final o b;
    private final AtomicBoolean c = new AtomicBoolean();
    private final JSONArray d = new JSONArray();
    private final LinkedHashSet<String> e = new LinkedHashSet<>();
    private final Object f = new Object();

    public g(i iVar) {
        this.f1221a = iVar;
        this.b = iVar.v();
    }

    public void a(Activity activity) {
        if (this.c.compareAndSet(false, true)) {
            this.f1221a.K().a((a) new com.applovin.impl.mediation.c.a(activity, this.f1221a), r.a.MEDIATION_MAIN);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(e eVar, long j, InitializationStatus initializationStatus, String str) {
        boolean z;
        if (initializationStatus != null && initializationStatus != InitializationStatus.INITIALIZING) {
            synchronized (this.f) {
                z = !a(eVar);
                if (z) {
                    this.e.add(eVar.y());
                    JSONObject jSONObject = new JSONObject();
                    com.applovin.impl.sdk.utils.i.a(jSONObject, "class", eVar.y(), this.f1221a);
                    com.applovin.impl.sdk.utils.i.a(jSONObject, "init_status", String.valueOf(initializationStatus.getCode()), this.f1221a);
                    com.applovin.impl.sdk.utils.i.a(jSONObject, AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE, JSONObject.quote(str), this.f1221a);
                    this.d.put(jSONObject);
                }
            }
            if (z) {
                this.f1221a.a(eVar);
                this.f1221a.y().maybeScheduleAdapterInitializationPostback(eVar, j, initializationStatus, str);
            }
        }
    }

    public void a(e eVar, Activity activity) {
        i a2 = this.f1221a.w().a(eVar);
        if (a2 != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Initializing adapter ");
            sb.append(eVar);
            this.b.c("MediationAdapterInitializationManager", sb.toString());
            a2.a((MaxAdapterInitializationParameters) MaxAdapterParametersImpl.a(eVar, activity.getApplicationContext()), activity);
        }
    }

    public boolean a() {
        return this.c.get();
    }

    /* access modifiers changed from: 0000 */
    public boolean a(e eVar) {
        boolean contains;
        synchronized (this.f) {
            contains = this.e.contains(eVar.y());
        }
        return contains;
    }

    public LinkedHashSet<String> b() {
        LinkedHashSet<String> linkedHashSet;
        synchronized (this.f) {
            linkedHashSet = this.e;
        }
        return linkedHashSet;
    }

    public JSONArray c() {
        JSONArray jSONArray;
        synchronized (this.f) {
            jSONArray = this.d;
        }
        return jSONArray;
    }
}
