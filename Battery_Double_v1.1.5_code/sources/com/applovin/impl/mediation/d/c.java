package com.applovin.impl.mediation.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.i;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.sdk.AppLovinSdk;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {

    /* renamed from: a reason: collision with root package name */
    private static final List<String> f1217a = new ArrayList();
    private static a b;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private final JSONArray f1218a;
        private final JSONArray b;

        private a(JSONArray jSONArray, JSONArray jSONArray2) {
            this.f1218a = jSONArray;
            this.b = jSONArray2;
        }

        public JSONArray a() {
            return this.f1218a;
        }

        public JSONArray b() {
            return this.b;
        }
    }

    static {
        f1217a.add("com.applovin.mediation.adapters.AdColonyMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.AmazonMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.AmazonBiddingMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.AppLovinMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.ChartboostMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.FacebookMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.GoogleMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.HyperMXMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.IMobileMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.InMobiMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.InneractiveMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.IronSourceMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.LeadboltMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.MadvertiseMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.MaioMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.MintegralMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.MoPubMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.MyTargetMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.NendMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.OguryMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.SmaatoMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.TapjoyMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.TencentMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.UnityAdsMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.VerizonAdsMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.VungleMediationAdapter");
        f1217a.add("com.applovin.mediation.adapters.YandexMediationAdapter");
    }

    public static a a(i iVar) {
        if (b != null) {
            return b;
        }
        JSONArray jSONArray = new JSONArray();
        JSONArray jSONArray2 = new JSONArray();
        for (String str : f1217a) {
            MaxAdapter a2 = a(str, iVar);
            if (a2 != null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("class", str);
                    jSONObject.put("sdk_version", a2.getSdkVersion());
                    jSONObject.put("version", a2.getAdapterVersion());
                } catch (Throwable unused) {
                }
                jSONArray.put(jSONObject);
            } else {
                jSONArray2.put(str);
            }
        }
        b = new a(jSONArray, jSONArray2);
        return b;
    }

    public static com.applovin.impl.sdk.d.r.a a(MaxAdFormat maxAdFormat) {
        return maxAdFormat == MaxAdFormat.INTERSTITIAL ? com.applovin.impl.sdk.d.r.a.MEDIATION_INTERSTITIAL : maxAdFormat == MaxAdFormat.REWARDED ? com.applovin.impl.sdk.d.r.a.MEDIATION_INCENTIVIZED : com.applovin.impl.sdk.d.r.a.MEDIATION_BANNER;
    }

    public static MaxAdapter a(String str, i iVar) {
        if (TextUtils.isEmpty(str)) {
            iVar.v().e("AppLovinSdk", "Failed to create adapter instance. No class name provided");
            return null;
        }
        try {
            Class cls = Class.forName(str);
            if (MaxAdapter.class.isAssignableFrom(cls)) {
                return (MaxAdapter) cls.getConstructor(new Class[]{AppLovinSdk.class}).newInstance(new Object[]{iVar.S()});
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" error: not an instance of '");
            sb.append(MaxAdapter.class.getName());
            sb.append("'.");
            iVar.v().e("AppLovinSdk", sb.toString());
            return null;
        } catch (ClassNotFoundException unused) {
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to load: ");
            sb2.append(str);
            iVar.v().b("AppLovinSdk", sb2.toString(), th);
        }
    }

    public static String b(MaxAdFormat maxAdFormat) {
        return maxAdFormat.getLabel();
    }
}
