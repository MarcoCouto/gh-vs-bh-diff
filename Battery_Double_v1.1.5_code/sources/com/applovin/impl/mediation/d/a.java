package com.applovin.impl.mediation.d;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;

public class a implements MaxAdListener, MaxAdViewAdListener, MaxRewardedAdListener {

    /* renamed from: a reason: collision with root package name */
    private final i f1216a;
    private final MaxAdListener b;

    public a(MaxAdListener maxAdListener, i iVar) {
        this.f1216a = iVar;
        this.b = maxAdListener;
    }

    public void onAdClicked(MaxAd maxAd) {
        j.d(this.b, maxAd);
    }

    public void onAdCollapsed(MaxAd maxAd) {
        j.h(this.b, maxAd);
    }

    public void onAdDisplayFailed(MaxAd maxAd, int i) {
        j.a(this.b, maxAd, i);
    }

    public void onAdDisplayed(MaxAd maxAd) {
        j.b(this.b, maxAd);
    }

    public void onAdExpanded(MaxAd maxAd) {
        j.g(this.b, maxAd);
    }

    public void onAdHidden(MaxAd maxAd) {
        j.c(this.b, maxAd);
    }

    public void onAdLoadFailed(String str, int i) {
        j.a(this.b, str, i);
    }

    public void onAdLoaded(MaxAd maxAd) {
        j.a(this.b, maxAd);
    }

    public void onRewardedVideoCompleted(MaxAd maxAd) {
        j.f(this.b, maxAd);
    }

    public void onRewardedVideoStarted(MaxAd maxAd) {
        j.e(this.b, maxAd);
    }

    public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
        j.a(this.b, maxAd, maxReward);
    }
}
