package com.applovin.impl.mediation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.n;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class j {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final List<b> f1259a = Collections.synchronizedList(new ArrayList());
    private final a b;
    private final a c;

    private static class a extends BroadcastReceiver implements MaxAdListener, MaxRewardedAdListener {

        /* renamed from: a reason: collision with root package name */
        private final i f1260a;
        private final j b;
        private final MaxAdFormat c;
        private final c<String> d;
        /* access modifiers changed from: private */
        public MaxAdListener e;
        /* access modifiers changed from: private */
        public com.applovin.impl.mediation.b.c f;
        private final Object g;
        private n h;
        private long i;
        private final AtomicBoolean j;
        private volatile boolean k;

        private a(c<String> cVar, MaxAdFormat maxAdFormat, j jVar, i iVar) {
            this.g = new Object();
            this.j = new AtomicBoolean();
            this.b = jVar;
            this.f1260a = iVar;
            this.d = cVar;
            this.c = maxAdFormat;
            iVar.ad().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
            iVar.ad().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
        }

        private void a(long j2) {
            if (j2 > 0) {
                this.i = System.currentTimeMillis() + j2;
                this.h = n.a(j2, this.f1260a, new Runnable() {
                    public void run() {
                        a.this.b(true);
                    }
                });
            }
        }

        private void a(boolean z) {
            if (this.f1260a.Y().a()) {
                this.k = z;
                this.j.set(true);
                return;
            }
            String str = (String) this.f1260a.a(this.d);
            if (m.b(str)) {
                this.f1260a.y().loadAd(str, this.c, new com.applovin.impl.mediation.f.a().a("fa", String.valueOf(true)).a("faie", String.valueOf(z)).a(), true, this.f1260a.ae(), this);
            }
        }

        /* access modifiers changed from: private */
        public void b() {
            a(false);
        }

        /* access modifiers changed from: private */
        public void b(boolean z) {
            synchronized (this.g) {
                this.i = 0;
                c();
                this.f = null;
            }
            a(z);
        }

        private void c() {
            synchronized (this.g) {
                if (this.h != null) {
                    this.h.d();
                    this.h = null;
                }
            }
        }

        public void a() {
            if (this.j.compareAndSet(true, false)) {
                a(this.k);
            } else if (this.i != 0) {
                long currentTimeMillis = this.i - System.currentTimeMillis();
                if (currentTimeMillis <= 0) {
                    b(true);
                } else {
                    a(currentTimeMillis);
                }
            }
        }

        public void onAdClicked(MaxAd maxAd) {
            this.e.onAdClicked(maxAd);
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i2) {
            this.e.onAdDisplayFailed(maxAd, i2);
        }

        public void onAdDisplayed(MaxAd maxAd) {
            this.e.onAdDisplayed(maxAd);
            b(false);
        }

        public void onAdHidden(MaxAd maxAd) {
            this.e.onAdHidden(maxAd);
            this.e = null;
        }

        public void onAdLoadFailed(String str, int i2) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    a.this.b();
                }
            }, TimeUnit.SECONDS.toMillis(((Long) this.f1260a.a(com.applovin.impl.sdk.b.b.Q)).longValue()));
        }

        public void onAdLoaded(MaxAd maxAd) {
            this.f = (com.applovin.impl.mediation.b.c) maxAd;
            a(this.f.l());
            Iterator it = new ArrayList(this.b.f1259a).iterator();
            while (it.hasNext()) {
                ((b) it.next()).a(this.f);
            }
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                c();
            } else if ("com.applovin.application_resumed".equals(action)) {
                a();
            }
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            if (this.e instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) this.e).onRewardedVideoCompleted(maxAd);
            }
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            if (this.e instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) this.e).onRewardedVideoStarted(maxAd);
            }
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            if (this.e instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) this.e).onUserRewarded(maxAd, maxReward);
            }
        }
    }

    public interface b {
        void a(com.applovin.impl.mediation.b.c cVar);
    }

    public j(i iVar) {
        a aVar = new a(com.applovin.impl.sdk.b.b.N, MaxAdFormat.INTERSTITIAL, this, iVar);
        this.b = aVar;
        a aVar2 = new a(com.applovin.impl.sdk.b.b.O, MaxAdFormat.REWARDED, this, iVar);
        this.c = aVar2;
    }

    private a b(MaxAdFormat maxAdFormat) {
        if (MaxAdFormat.INTERSTITIAL == maxAdFormat) {
            return this.b;
        }
        if (MaxAdFormat.REWARDED == maxAdFormat) {
            return this.c;
        }
        return null;
    }

    public com.applovin.impl.mediation.b.c a(MaxAdFormat maxAdFormat) {
        a b2 = b(maxAdFormat);
        if (b2 != null) {
            return b2.f;
        }
        return null;
    }

    public void a() {
        this.b.b();
        this.c.b();
    }

    public void a(b bVar) {
        this.f1259a.add(bVar);
    }

    public void a(MaxAdListener maxAdListener, MaxAdFormat maxAdFormat) {
        a b2 = b(maxAdFormat);
        if (b2 != null) {
            b2.e = maxAdListener;
        }
    }

    public void b(b bVar) {
        this.f1259a.remove(bVar);
    }
}
