package com.applovin.impl.mediation;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.mediation.b.g;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapter.InitializationStatus;
import com.applovin.mediation.adapter.MaxAdapter.OnCompletionListener;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.MaxSignalProvider;
import com.applovin.mediation.adapter.listeners.MaxAdViewAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;
import com.applovin.sdk.AppLovinSdkUtils;
import com.ironsource.sdk.constants.Constants.JSMethods;
import java.util.concurrent.atomic.AtomicBoolean;

public class i {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Handler f1223a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final com.applovin.impl.sdk.i b;
    /* access modifiers changed from: private */
    public final o c;
    private final String d;
    /* access modifiers changed from: private */
    public final e e;
    /* access modifiers changed from: private */
    public final String f;
    /* access modifiers changed from: private */
    public MaxAdapter g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.a i;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public final a k = new a();
    /* access modifiers changed from: private */
    public MaxAdapterResponseParameters l;
    private final AtomicBoolean m = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public final AtomicBoolean o = new AtomicBoolean(false);

    private class a implements MaxAdViewAdapterListener, MaxInterstitialAdapterListener, MaxRewardedAdapterListener {
        /* access modifiers changed from: private */
        public d b;

        private a() {
        }

        /* access modifiers changed from: private */
        public void a(d dVar) {
            if (dVar != null) {
                this.b = dVar;
                return;
            }
            throw new IllegalArgumentException("No listener specified");
        }

        private void a(String str) {
            i.this.o.set(true);
            a(str, (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    if (i.this.n.compareAndSet(false, true)) {
                        a.this.b.onAdLoaded(i.this.i);
                    }
                }
            });
        }

        /* access modifiers changed from: private */
        public void a(String str, int i) {
            a(str, new MaxAdapterError(i));
        }

        private void a(final String str, final MaxAdListener maxAdListener, final Runnable runnable) {
            i.this.f1223a.post(new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Exception e) {
                        String name = maxAdListener != null ? maxAdListener.getClass().getName() : null;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to forward call (");
                        sb.append(str);
                        sb.append(") to ");
                        sb.append(name);
                        i.this.c.b("MediationAdapterWrapper", sb.toString(), e);
                    }
                }
            });
        }

        private void a(String str, final MaxAdapterError maxAdapterError) {
            a(str, (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    if (i.this.n.compareAndSet(false, true)) {
                        a.this.b.a(i.this.h, (e) maxAdapterError);
                    }
                }
            });
        }

        private void b(String str) {
            if (i.this.i.h().compareAndSet(false, true)) {
                a(str, (MaxAdListener) this.b, (Runnable) new Runnable() {
                    public void run() {
                        a.this.b.onAdDisplayed(i.this.i);
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public void b(String str, int i) {
            b(str, new MaxAdapterError(i));
        }

        private void b(String str, final MaxAdapterError maxAdapterError) {
            a(str, (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    a.this.b.a((MaxAd) i.this.i, (e) maxAdapterError);
                }
            });
        }

        public void onAdViewAdClicked() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": adview ad clicked");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onAdViewAdClicked", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    a.this.b.onAdClicked(i.this.i);
                }
            });
        }

        public void onAdViewAdCollapsed() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": adview ad collapsed");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onAdViewAdCollapsed", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxAdViewAdListener) {
                        ((MaxAdViewAdListener) a.this.b).onAdCollapsed(i.this.i);
                    }
                }
            });
        }

        public void onAdViewAdDisplayFailed(MaxAdapterError maxAdapterError) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": adview ad failed to display with code: ");
            sb.append(maxAdapterError);
            i.this.c.d("MediationAdapterWrapper", sb.toString());
            b("onAdViewAdDisplayFailed", maxAdapterError);
        }

        public void onAdViewAdDisplayed() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": adview ad displayed");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            b("onAdViewAdDisplayed");
        }

        public void onAdViewAdExpanded() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": adview ad expanded");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onAdViewAdExpanded", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxAdViewAdListener) {
                        ((MaxAdViewAdListener) a.this.b).onAdExpanded(i.this.i);
                    }
                }
            });
        }

        public void onAdViewAdHidden() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": adview ad hidden");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onAdViewAdHidden", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    a.this.b.onAdHidden(i.this.i);
                }
            });
        }

        public void onAdViewAdLoadFailed(MaxAdapterError maxAdapterError) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": adview ad ad failed to load with code: ");
            sb.append(maxAdapterError);
            i.this.c.d("MediationAdapterWrapper", sb.toString());
            a("onAdViewAdLoadFailed", maxAdapterError);
        }

        public void onAdViewAdLoaded(View view) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": adview ad loaded");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            i.this.j = view;
            a("onAdViewAdLoaded");
        }

        public void onInterstitialAdClicked() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": interstitial ad clicked");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a(JSMethods.ON_INTERSTITIAL_AD_CLICKED, (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    a.this.b.onAdClicked(i.this.i);
                }
            });
        }

        public void onInterstitialAdDisplayFailed(MaxAdapterError maxAdapterError) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": interstitial ad failed to display with code ");
            sb.append(maxAdapterError);
            i.this.c.d("MediationAdapterWrapper", sb.toString());
            b("onInterstitialAdDisplayFailed", maxAdapterError);
        }

        public void onInterstitialAdDisplayed() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": interstitial ad displayed");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            b("onInterstitialAdDisplayed");
        }

        public void onInterstitialAdHidden() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": interstitial ad hidden");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onInterstitialAdHidden", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    a.this.b.onAdHidden(i.this.i);
                }
            });
        }

        public void onInterstitialAdLoadFailed(MaxAdapterError maxAdapterError) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": interstitial ad failed to load with error ");
            sb.append(maxAdapterError);
            i.this.c.d("MediationAdapterWrapper", sb.toString());
            a("onInterstitialAdLoadFailed", maxAdapterError);
        }

        public void onInterstitialAdLoaded() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": interstitial ad loaded");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onInterstitialAdLoaded");
        }

        public void onRewardedAdClicked() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": rewarded ad clicked");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onRewardedAdClicked", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    a.this.b.onAdClicked(i.this.i);
                }
            });
        }

        public void onRewardedAdDisplayFailed(MaxAdapterError maxAdapterError) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": rewarded ad display failed with error: ");
            sb.append(maxAdapterError);
            i.this.c.d("MediationAdapterWrapper", sb.toString());
            b("onRewardedAdDisplayFailed", maxAdapterError);
        }

        public void onRewardedAdDisplayed() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": rewarded ad displayed");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            b("onRewardedAdDisplayed");
        }

        public void onRewardedAdHidden() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": rewarded ad hidden");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onRewardedAdHidden", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    a.this.b.onAdHidden(i.this.i);
                }
            });
        }

        public void onRewardedAdLoadFailed(MaxAdapterError maxAdapterError) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": rewarded ad failed to load with code: ");
            sb.append(maxAdapterError);
            i.this.c.d("MediationAdapterWrapper", sb.toString());
            a("onRewardedAdLoadFailed", maxAdapterError);
        }

        public void onRewardedAdLoaded() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": rewarded ad loaded");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onRewardedAdLoaded");
        }

        public void onRewardedAdVideoCompleted() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": rewarded video completed");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onRewardedAdVideoCompleted", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxRewardedAdListener) {
                        ((MaxRewardedAdListener) a.this.b).onRewardedVideoCompleted(i.this.i);
                    }
                }
            });
        }

        public void onRewardedAdVideoStarted() {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": rewarded video started");
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onRewardedAdVideoStarted", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxRewardedAdListener) {
                        ((MaxRewardedAdListener) a.this.b).onRewardedVideoStarted(i.this.i);
                    }
                }
            });
        }

        public void onUserRewarded(final MaxReward maxReward) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.this.f);
            sb.append(": user was rewarded: ");
            sb.append(maxReward);
            i.this.c.c("MediationAdapterWrapper", sb.toString());
            a("onUserRewarded", (MaxAdListener) this.b, (Runnable) new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxRewardedAdListener) {
                        ((MaxRewardedAdListener) a.this.b).onUserRewarded(i.this.i, maxReward);
                    }
                }
            });
        }
    }

    private static class b {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final g f1256a;
        /* access modifiers changed from: private */
        public final MaxSignalCollectionListener b;
        /* access modifiers changed from: private */
        public final AtomicBoolean c = new AtomicBoolean();

        b(g gVar, MaxSignalCollectionListener maxSignalCollectionListener) {
            this.f1256a = gVar;
            this.b = maxSignalCollectionListener;
        }
    }

    private class c extends com.applovin.impl.sdk.d.a {
        private c() {
            super("TaskTimeoutMediatedAd", i.this.b);
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.G;
        }

        public void run() {
            if (!i.this.o.get()) {
                StringBuilder sb = new StringBuilder();
                sb.append(i.this.f);
                sb.append(" is timing out ");
                sb.append(i.this.i);
                sb.append("...");
                d(sb.toString());
                this.b.A().a(i.this.i);
                i.this.k.a(f(), (int) MaxErrorCodes.MEDIATION_ADAPTER_TIMEOUT);
            }
        }
    }

    private class d extends com.applovin.impl.sdk.d.a {
        private final b c;

        private d(b bVar) {
            super("TaskTimeoutSignalCollection", i.this.b);
            this.c = bVar;
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.H;
        }

        public void run() {
            if (!this.c.c.get()) {
                StringBuilder sb = new StringBuilder();
                sb.append(i.this.f);
                sb.append(" is timing out ");
                sb.append(this.c.f1256a);
                sb.append("...");
                d(sb.toString());
                i iVar = i.this;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("The adapter (");
                sb2.append(i.this.f);
                sb2.append(") timed out");
                iVar.b(sb2.toString(), this.c);
            }
        }
    }

    i(e eVar, MaxAdapter maxAdapter, com.applovin.impl.sdk.i iVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("No adapter name specified");
        } else if (maxAdapter == null) {
            throw new IllegalArgumentException("No adapter specified");
        } else if (iVar != null) {
            this.d = eVar.z();
            this.g = maxAdapter;
            this.b = iVar;
            this.c = iVar.v();
            this.e = eVar;
            this.f = maxAdapter.getClass().getSimpleName();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Marking ");
        sb.append(this.f);
        sb.append(" as disabled due to: ");
        sb.append(str);
        this.c.c("MediationAdapterWrapper", sb.toString());
        this.m.set(false);
    }

    /* access modifiers changed from: private */
    public void a(String str, b bVar) {
        if (bVar.c.compareAndSet(false, true) && bVar.b != null) {
            bVar.b.onSignalCollected(str);
        }
    }

    private void a(final String str, final Runnable runnable) {
        AnonymousClass3 r0 = new Runnable() {
            public void run() {
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(i.this.f);
                    sb.append(": running ");
                    sb.append(str);
                    sb.append("...");
                    i.this.c.b("MediationAdapterWrapper", sb.toString());
                    runnable.run();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(i.this.f);
                    sb2.append(": finished ");
                    sb2.append(str);
                    sb2.append("");
                    i.this.c.b("MediationAdapterWrapper", sb2.toString());
                } catch (Throwable th) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Unable to run adapter operation ");
                    sb3.append(str);
                    sb3.append(", marking ");
                    sb3.append(i.this.f);
                    sb3.append(" as disabled");
                    i.this.c.b("MediationAdapterWrapper", sb3.toString(), th);
                    i iVar = i.this;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("fail_");
                    sb4.append(str);
                    iVar.a(sb4.toString());
                }
            }
        };
        if (this.e.B()) {
            this.f1223a.post(r0);
        } else {
            r0.run();
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, b bVar) {
        if (bVar.c.compareAndSet(false, true) && bVar.b != null) {
            bVar.b.onSignalCollectionFailed(str);
        }
    }

    public View a() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public void a(final com.applovin.impl.mediation.b.a aVar, final Activity activity) {
        final Runnable runnable;
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (aVar.c() == null) {
            this.k.b("ad_show", -5201);
        } else if (aVar.c() != this) {
            throw new IllegalArgumentException("Mediated ad belongs to a different adapter");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (!this.m.get()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Mediation adapter '");
            sb.append(this.f);
            sb.append("' is disabled. Showing ads with this adapter is disabled.");
            o.i("MediationAdapterWrapper", sb.toString());
            this.k.b("ad_show", (int) MaxErrorCodes.MEDIATION_ADAPTER_DISABLED);
        } else if (!d()) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Mediation adapter '");
            sb2.append(this.f);
            sb2.append("' does not have an ad loaded. Please load an ad first");
            o.i("MediationAdapterWrapper", sb2.toString());
            this.k.b("ad_show", (int) MaxErrorCodes.MEDIATION_ADAPTER_AD_NOT_READY);
        } else {
            if (aVar.getFormat() == MaxAdFormat.INTERSTITIAL) {
                if (this.g instanceof MaxInterstitialAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxInterstitialAdapter) i.this.g).showInterstitialAd(i.this.l, activity, i.this.k);
                        }
                    };
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Mediation adapter '");
                    sb3.append(this.f);
                    sb3.append("' is not an interstitial adapter.");
                    o.i("MediationAdapterWrapper", sb3.toString());
                    this.k.b("showFullscreenAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (aVar.getFormat() != MaxAdFormat.REWARDED) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Failed to show ");
                sb4.append(aVar);
                sb4.append(": ");
                sb4.append(aVar.getFormat());
                sb4.append(" is not a supported ad format");
                o.i("MediationAdapterWrapper", sb4.toString());
                this.k.b("showFullscreenAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            } else if (this.g instanceof MaxRewardedAdapter) {
                runnable = new Runnable() {
                    public void run() {
                        ((MaxRewardedAdapter) i.this.g).showRewardedAd(i.this.l, activity, i.this.k);
                    }
                };
            } else {
                StringBuilder sb5 = new StringBuilder();
                sb5.append("Mediation adapter '");
                sb5.append(this.f);
                sb5.append("' is not an incentivized adapter.");
                o.i("MediationAdapterWrapper", sb5.toString());
                this.k.b("showFullscreenAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            }
            a("ad_render", (Runnable) new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Throwable th) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to start displaying ad");
                        sb.append(aVar);
                        i.this.c.b("MediationAdapterWrapper", sb.toString(), th);
                        i.this.k.b("ad_render", (int) MaxAdapterError.ERROR_CODE_UNSPECIFIED);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(final MaxAdapterInitializationParameters maxAdapterInitializationParameters, final Activity activity) {
        a("initialize", (Runnable) new Runnable() {
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                i.this.g.initialize(maxAdapterInitializationParameters, activity, new OnCompletionListener() {
                    public void onCompletion() {
                        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                            public void run() {
                                i.this.b.x().a(i.this.e, SystemClock.elapsedRealtime() - elapsedRealtime, InitializationStatus.ADAPTER_NOT_SUPPORTED, null);
                            }
                        }, i.this.e.H());
                    }

                    public void onCompletion(final InitializationStatus initializationStatus, final String str) {
                        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                            public void run() {
                                i.this.b.x().a(i.this.e, SystemClock.elapsedRealtime() - elapsedRealtime, initializationStatus, str);
                            }
                        }, i.this.e.H());
                    }
                });
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters, g gVar, Activity activity, MaxSignalCollectionListener maxSignalCollectionListener) {
        if (maxSignalCollectionListener == null) {
            throw new IllegalArgumentException("No callback specified");
        } else if (!this.m.get()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Mediation adapter '");
            sb.append(this.f);
            sb.append("' is disabled. Signal collection ads with this adapter is disabled.");
            o.i("MediationAdapterWrapper", sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("The adapter (");
            sb2.append(this.f);
            sb2.append(") is disabled");
            maxSignalCollectionListener.onSignalCollectionFailed(sb2.toString());
        } else {
            final b bVar = new b(gVar, maxSignalCollectionListener);
            if (this.g instanceof MaxSignalProvider) {
                final MaxSignalProvider maxSignalProvider = (MaxSignalProvider) this.g;
                final MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters2 = maxAdapterSignalCollectionParameters;
                final Activity activity2 = activity;
                final g gVar2 = gVar;
                AnonymousClass11 r0 = new Runnable() {
                    public void run() {
                        maxSignalProvider.collectSignal(maxAdapterSignalCollectionParameters2, activity2, new MaxSignalCollectionListener() {
                            public void onSignalCollected(String str) {
                                i.this.a(str, bVar);
                            }

                            public void onSignalCollectionFailed(String str) {
                                i.this.b(str, bVar);
                            }
                        });
                        if (bVar.c.get()) {
                            return;
                        }
                        if (gVar2.D() == 0) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Failing signal collection ");
                            sb.append(gVar2);
                            sb.append(" since it has 0 timeout");
                            i.this.c.b("MediationAdapterWrapper", sb.toString());
                            i iVar = i.this;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("The adapter (");
                            sb2.append(i.this.f);
                            sb2.append(") has 0 timeout");
                            iVar.b(sb2.toString(), bVar);
                        } else if (gVar2.D() > 0) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Setting timeout ");
                            sb3.append(gVar2.D());
                            sb3.append("ms. for ");
                            sb3.append(gVar2);
                            i.this.c.b("MediationAdapterWrapper", sb3.toString());
                            i.this.b.K().a(new d(bVar), com.applovin.impl.sdk.d.r.a.MEDIATION_TIMEOUT, gVar2.D());
                        } else {
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("Negative timeout set for ");
                            sb4.append(gVar2);
                            sb4.append(", not scheduling a timeout");
                            i.this.c.b("MediationAdapterWrapper", sb4.toString());
                        }
                    }
                };
                a("collect_signal", (Runnable) r0);
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("The adapter (");
                sb3.append(this.f);
                sb3.append(") does not support signal collection");
                b(sb3.toString(), bVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, com.applovin.impl.mediation.b.a aVar) {
        this.h = str;
        this.i = aVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, final MaxAdapterResponseParameters maxAdapterResponseParameters, final com.applovin.impl.mediation.b.a aVar, final Activity activity, d dVar) {
        final Runnable runnable;
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (!this.m.get()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Mediation adapter '");
            sb.append(this.f);
            sb.append("' was disabled due to earlier failures. Loading ads with this adapter is disabled.");
            o.i("MediationAdapterWrapper", sb.toString());
            dVar.onAdLoadFailed(str, MaxErrorCodes.MEDIATION_ADAPTER_DISABLED);
        } else {
            this.l = maxAdapterResponseParameters;
            this.k.a(dVar);
            if (aVar.getFormat() == MaxAdFormat.INTERSTITIAL) {
                if (this.g instanceof MaxInterstitialAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxInterstitialAdapter) i.this.g).loadInterstitialAd(maxAdapterResponseParameters, activity, i.this.k);
                        }
                    };
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Mediation adapter '");
                    sb2.append(this.f);
                    sb2.append("' is not an interstitial adapter.");
                    o.i("MediationAdapterWrapper", sb2.toString());
                    this.k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (aVar.getFormat() == MaxAdFormat.REWARDED) {
                if (this.g instanceof MaxRewardedAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxRewardedAdapter) i.this.g).loadRewardedAd(maxAdapterResponseParameters, activity, i.this.k);
                        }
                    };
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Mediation adapter '");
                    sb3.append(this.f);
                    sb3.append("' is not an incentivized adapter.");
                    o.i("MediationAdapterWrapper", sb3.toString());
                    this.k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (aVar.getFormat() != MaxAdFormat.BANNER && aVar.getFormat() != MaxAdFormat.LEADER && aVar.getFormat() != MaxAdFormat.MREC) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Failed to load ");
                sb4.append(aVar);
                sb4.append(": ");
                sb4.append(aVar.getFormat());
                sb4.append(" is not a supported ad format");
                o.i("MediationAdapterWrapper", sb4.toString());
                this.k.a("loadAd", (int) MaxErrorCodes.FORMAT_TYPE_NOT_SUPPORTED);
                return;
            } else if (this.g instanceof MaxAdViewAdapter) {
                runnable = new Runnable() {
                    public void run() {
                        ((MaxAdViewAdapter) i.this.g).loadAdViewAd(maxAdapterResponseParameters, aVar.getFormat(), activity, i.this.k);
                    }
                };
            } else {
                StringBuilder sb5 = new StringBuilder();
                sb5.append("Mediation adapter '");
                sb5.append(this.f);
                sb5.append("' is not an adview-based adapter.");
                o.i("MediationAdapterWrapper", sb5.toString());
                this.k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            }
            a("ad_load", (Runnable) new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Throwable th) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed start loading ");
                        sb.append(aVar);
                        i.this.c.b("MediationAdapterWrapper", sb.toString(), th);
                        i.this.k.a("loadAd", -1);
                    }
                    if (!i.this.n.get()) {
                        long D = i.this.e.D();
                        if (D == 0) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Failing ad ");
                            sb2.append(aVar);
                            sb2.append(" since it has 0 timeout");
                            i.this.c.b("MediationAdapterWrapper", sb2.toString());
                            i.this.k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_IMMEDIATE_TIMEOUT);
                        } else if (D > 0) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Setting timeout ");
                            sb3.append(D);
                            sb3.append("ms. for ");
                            sb3.append(aVar);
                            i.this.c.b("MediationAdapterWrapper", sb3.toString());
                            i.this.b.K().a(new c(), com.applovin.impl.sdk.d.r.a.MEDIATION_TIMEOUT, D);
                        } else {
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("Negative timeout set for ");
                            sb4.append(aVar);
                            sb4.append(", not scheduling a timeout");
                            i.this.c.b("MediationAdapterWrapper", sb4.toString());
                        }
                    }
                }
            });
        }
    }

    public String b() {
        return this.d;
    }

    public boolean c() {
        return this.m.get();
    }

    public boolean d() {
        return this.n.get() && this.o.get();
    }

    public String e() {
        if (this.g != null) {
            try {
                return this.g.getSdkVersion();
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to get adapter's SDK version, marking ");
                sb.append(this);
                sb.append(" as disabled");
                this.c.b("MediationAdapterWrapper", sb.toString(), th);
                a("fail_version");
            }
        }
        return null;
    }

    public String f() {
        if (this.g != null) {
            try {
                return this.g.getAdapterVersion();
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to get adapter version, marking ");
                sb.append(this);
                sb.append(" as disabled");
                this.c.b("MediationAdapterWrapper", sb.toString(), th);
                a("fail_version");
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        a("destroy", (Runnable) new Runnable() {
            public void run() {
                i.this.a("destroy");
                i.this.g.onDestroy();
                i.this.g = null;
            }
        });
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediationAdapterWrapper{adapterTag='");
        sb.append(this.f);
        sb.append("'");
        sb.append('}');
        return sb.toString();
    }
}
