package com.applovin.impl.mediation;

import android.text.TextUtils;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapters.MediationAdapterBase;
import com.applovin.sdk.AppLovinSdk;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class h {

    /* renamed from: a reason: collision with root package name */
    private final i f1222a;
    private final o b;
    private final Object c = new Object();
    private final Map<String, Class<? extends MaxAdapter>> d = new HashMap();
    private final Set<String> e = new HashSet();

    public h(i iVar) {
        if (iVar != null) {
            this.f1222a = iVar;
            this.b = iVar.v();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private i a(e eVar, Class<? extends MaxAdapter> cls) {
        try {
            i iVar = new i(eVar, (MediationAdapterBase) cls.getConstructor(new Class[]{AppLovinSdk.class}).newInstance(new Object[]{this.f1222a.S()}), this.f1222a);
            if (iVar.c()) {
                return iVar;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Adapter is disabled after initialization: ");
            sb.append(eVar);
            o.i("MediationAdapterManager", sb.toString());
            return null;
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to load adapter: ");
            sb2.append(eVar);
            o.c("MediationAdapterManager", sb2.toString(), th);
        }
    }

    private Class<? extends MaxAdapter> a(String str) {
        try {
            Class cls = Class.forName(str);
            if (MaxAdapter.class.isAssignableFrom(cls)) {
                return cls.asSubclass(MaxAdapter.class);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" error: not an instance of '");
            sb.append(MaxAdapter.class.getName());
            sb.append("'.");
            o.i("MediationAdapterManager", sb.toString());
            return null;
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to load: ");
            sb2.append(str);
            o.c("MediationAdapterManager", sb2.toString(), th);
        }
    }

    /* access modifiers changed from: 0000 */
    public i a(e eVar) {
        Class cls;
        if (eVar != null) {
            String z = eVar.z();
            String y = eVar.y();
            if (TextUtils.isEmpty(z)) {
                StringBuilder sb = new StringBuilder();
                sb.append("No adapter name provided for ");
                sb.append(y);
                sb.append(", not loading the adapter ");
                this.b.e("MediationAdapterManager", sb.toString());
                return null;
            } else if (TextUtils.isEmpty(y)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to find default classname for '");
                sb2.append(z);
                sb2.append("'");
                this.b.e("MediationAdapterManager", sb2.toString());
                return null;
            } else {
                synchronized (this.c) {
                    if (!this.e.contains(y)) {
                        if (this.d.containsKey(y)) {
                            cls = (Class) this.d.get(y);
                        } else {
                            cls = a(y);
                            if (cls == null) {
                                this.e.add(y);
                                return null;
                            }
                        }
                        i a2 = a(eVar, cls);
                        if (a2 != null) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Loaded ");
                            sb3.append(z);
                            this.b.b("MediationAdapterManager", sb3.toString());
                            this.d.put(y, cls);
                            return a2;
                        }
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Failed to load ");
                        sb4.append(z);
                        this.b.e("MediationAdapterManager", sb4.toString());
                        this.e.add(y);
                        return null;
                    }
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("Not attempting to load ");
                    sb5.append(z);
                    sb5.append(" due to prior errors");
                    this.b.b("MediationAdapterManager", sb5.toString());
                    return null;
                }
            }
        } else {
            throw new IllegalArgumentException("No adapter spec specified");
        }
    }

    public Collection<String> a() {
        Set unmodifiableSet;
        synchronized (this.c) {
            HashSet hashSet = new HashSet(this.d.size());
            for (Class name : this.d.values()) {
                hashSet.add(name.getName());
            }
            unmodifiableSet = Collections.unmodifiableSet(hashSet);
        }
        return unmodifiableSet;
    }

    public Collection<String> b() {
        Set unmodifiableSet;
        synchronized (this.c) {
            unmodifiableSet = Collections.unmodifiableSet(this.e);
        }
        return unmodifiableSet;
    }
}
