package com.applovin.impl.mediation;

import android.app.Activity;
import android.os.Bundle;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;

public class a extends com.applovin.impl.sdk.utils.a {

    /* renamed from: a reason: collision with root package name */
    private final com.applovin.impl.sdk.a f1152a;
    private final o b;
    private C0008a c;
    private c d;
    private int e;
    private boolean f;

    /* renamed from: com.applovin.impl.mediation.a$a reason: collision with other inner class name */
    public interface C0008a {
        void a(c cVar);
    }

    a(i iVar) {
        this.b = iVar.v();
        this.f1152a = iVar.aa();
    }

    public void a() {
        this.b.b("AdActivityObserver", "Cancelling...");
        this.f1152a.b(this);
        this.c = null;
        this.d = null;
        this.e = 0;
        this.f = false;
    }

    public void a(c cVar, C0008a aVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Starting for ad ");
        sb.append(cVar.getAdUnitId());
        sb.append("...");
        this.b.b("AdActivityObserver", sb.toString());
        a();
        this.c = aVar;
        this.d = cVar;
        this.f1152a.a(this);
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (!this.f) {
            this.f = true;
        }
        this.e++;
        StringBuilder sb = new StringBuilder();
        sb.append("Created Activity: ");
        sb.append(activity);
        sb.append(", counter is ");
        sb.append(this.e);
        this.b.b("AdActivityObserver", sb.toString());
    }

    public void onActivityDestroyed(Activity activity) {
        if (this.f) {
            this.e--;
            StringBuilder sb = new StringBuilder();
            sb.append("Destroyed Activity: ");
            sb.append(activity);
            sb.append(", counter is ");
            sb.append(this.e);
            this.b.b("AdActivityObserver", sb.toString());
            if (this.e <= 0) {
                this.b.b("AdActivityObserver", "Last ad Activity destroyed");
                if (this.c != null) {
                    this.b.b("AdActivityObserver", "Invoking callback...");
                    this.c.a(this.d);
                }
                a();
            }
        }
    }
}
