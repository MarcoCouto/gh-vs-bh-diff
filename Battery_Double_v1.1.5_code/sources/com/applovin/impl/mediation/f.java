package com.applovin.impl.mediation;

import android.os.Bundle;

public class f {

    /* renamed from: a reason: collision with root package name */
    private final Bundle f1219a;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final Bundle f1220a = new Bundle();

        public a a(String str) {
            if (str != null) {
                this.f1220a.remove(str);
                return this;
            }
            throw new IllegalArgumentException("No key specified.");
        }

        public a a(String str, String str2) {
            if (str != null) {
                this.f1220a.putString(str, str2);
                return this;
            }
            throw new IllegalArgumentException("No key specified");
        }

        public f a() {
            return new f(this);
        }
    }

    private f(a aVar) {
        this.f1219a = aVar.f1220a;
    }

    public Bundle a() {
        return this.f1219a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedRequestParameters{extraParameters=");
        sb.append(this.f1219a);
        sb.append('}');
        return sb.toString();
    }
}
