package com.applovin.impl.mediation.ads;

import android.app.Activity;
import com.applovin.impl.mediation.e;
import com.applovin.impl.mediation.l;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class MaxFullscreenAdImpl extends a implements com.applovin.impl.sdk.b.a {

    /* renamed from: a reason: collision with root package name */
    private final a f1181a;
    /* access modifiers changed from: private */
    public final com.applovin.impl.sdk.b b;
    /* access modifiers changed from: private */
    public final com.applovin.impl.mediation.b c;
    /* access modifiers changed from: private */
    public final Object d = new Object();
    private com.applovin.impl.mediation.b.c e = null;
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.c f = null;
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.c g = null;
    private c h = c.IDLE;
    /* access modifiers changed from: private */
    public final AtomicBoolean i = new AtomicBoolean();
    protected final b listenerWrapper;

    public interface a {
        Activity getActivity();
    }

    private class b implements com.applovin.impl.mediation.j.b, MaxAdListener, MaxRewardedAdListener {
        private b() {
        }

        public void a(com.applovin.impl.mediation.b.c cVar) {
            if (cVar.getFormat() == MaxFullscreenAdImpl.this.adFormat) {
                onAdLoaded(cVar);
            }
        }

        public void onAdClicked(MaxAd maxAd) {
            j.d(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
        }

        public void onAdDisplayFailed(MaxAd maxAd, final int i) {
            MaxFullscreenAdImpl.this.a(c.IDLE, (Runnable) new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.b.a();
                    MaxFullscreenAdImpl.this.b();
                    j.a(MaxFullscreenAdImpl.this.adListener, (MaxAd) MaxFullscreenAdImpl.this.d(), i);
                }
            });
        }

        public void onAdDisplayed(MaxAd maxAd) {
            if (!((com.applovin.impl.mediation.b.c) maxAd).j()) {
                MaxFullscreenAdImpl.this.b.a();
            }
            j.b(MaxFullscreenAdImpl.this.adListener, (MaxAd) MaxFullscreenAdImpl.this.d());
        }

        public void onAdHidden(MaxAd maxAd) {
            MaxFullscreenAdImpl.this.c.a(maxAd);
            MaxFullscreenAdImpl.this.a(c.IDLE, (Runnable) new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.b();
                    j.c(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
                }
            });
        }

        public void onAdLoadFailed(final String str, final int i) {
            MaxFullscreenAdImpl.this.c();
            if (MaxFullscreenAdImpl.this.g == null) {
                MaxFullscreenAdImpl.this.a(c.IDLE, (Runnable) new Runnable() {
                    public void run() {
                        j.a(MaxFullscreenAdImpl.this.adListener, str, i);
                    }
                });
            }
        }

        public void onAdLoaded(MaxAd maxAd) {
            com.applovin.impl.mediation.b.c cVar = (com.applovin.impl.mediation.b.c) maxAd;
            MaxFullscreenAdImpl.this.a(cVar);
            if (cVar.j() || !MaxFullscreenAdImpl.this.i.compareAndSet(true, false)) {
                MaxFullscreenAdImpl.this.a(c.READY, (Runnable) new Runnable() {
                    public void run() {
                        j.a(MaxFullscreenAdImpl.this.adListener, (MaxAd) MaxFullscreenAdImpl.this.d());
                    }
                });
            } else {
                MaxFullscreenAdImpl.this.loadRequestBuilder.a("expired_ad_ad_unit_id");
            }
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            j.f(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            j.e(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            j.a(MaxFullscreenAdImpl.this.adListener, (MaxAd) MaxFullscreenAdImpl.this.d(), maxReward);
        }
    }

    public enum c {
        IDLE,
        LOADING,
        READY,
        SHOWING,
        DESTROYED
    }

    public MaxFullscreenAdImpl(String str, MaxAdFormat maxAdFormat, a aVar, String str2, i iVar) {
        super(str, maxAdFormat, str2, iVar);
        this.f1181a = aVar;
        this.listenerWrapper = new b();
        this.b = new com.applovin.impl.sdk.b(iVar, this);
        this.c = new com.applovin.impl.mediation.b(iVar, this.listenerWrapper);
        StringBuilder sb = new StringBuilder();
        sb.append("Created new ");
        sb.append(str2);
        sb.append(" (");
        sb.append(this);
        sb.append(")");
        o.f(str2, sb.toString());
    }

    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.c a() {
        com.applovin.impl.mediation.b.c cVar;
        synchronized (this.d) {
            cVar = this.f != null ? this.f : this.g;
        }
        return cVar;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x016e  */
    public void a(c cVar, Runnable runnable) {
        boolean z;
        o oVar;
        String str;
        String sb;
        String str2;
        String str3;
        c cVar2 = this.h;
        synchronized (this.d) {
            o oVar2 = this.logger;
            String str4 = this.tag;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Attempting state transition from ");
            sb2.append(cVar2);
            sb2.append(" to ");
            sb2.append(cVar);
            oVar2.b(str4, sb2.toString());
            z = false;
            if (cVar2 == c.IDLE) {
                if (cVar != c.LOADING) {
                    if (cVar != c.DESTROYED) {
                        if (cVar == c.SHOWING) {
                            str2 = this.tag;
                            str3 = "No ad is loading or loaded";
                            o.i(str2, str3);
                            if (z) {
                                o oVar3 = this.logger;
                                String str5 = this.tag;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("Transitioning from ");
                                sb3.append(this.h);
                                sb3.append(" to ");
                                sb3.append(cVar);
                                sb3.append("...");
                                oVar3.b(str5, sb3.toString());
                                this.h = cVar;
                            } else {
                                o oVar4 = this.logger;
                                String str6 = this.tag;
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("Not allowed transition from ");
                                sb4.append(this.h);
                                sb4.append(" to ");
                                sb4.append(cVar);
                                oVar4.d(str6, sb4.toString());
                            }
                        } else {
                            oVar = this.logger;
                            str = this.tag;
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append("Unable to transition to: ");
                            sb5.append(cVar);
                            sb = sb5.toString();
                            oVar.e(str, sb);
                            if (z) {
                            }
                        }
                    }
                }
            } else if (cVar2 == c.LOADING) {
                if (cVar != c.IDLE) {
                    if (cVar == c.LOADING) {
                        str2 = this.tag;
                        str3 = "An ad is already loading";
                    } else if (cVar != c.READY) {
                        if (cVar == c.SHOWING) {
                            str2 = this.tag;
                            str3 = "An ad is not ready to be shown yet";
                        } else if (cVar != c.DESTROYED) {
                            oVar = this.logger;
                            str = this.tag;
                            StringBuilder sb6 = new StringBuilder();
                            sb6.append("Unable to transition to: ");
                            sb6.append(cVar);
                            sb = sb6.toString();
                            oVar.e(str, sb);
                            if (z) {
                            }
                        }
                    }
                    o.i(str2, str3);
                    if (z) {
                    }
                }
            } else if (cVar2 == c.READY) {
                if (cVar != c.IDLE) {
                    if (cVar == c.LOADING) {
                        str2 = this.tag;
                        str3 = "An ad is already loaded";
                        o.i(str2, str3);
                        if (z) {
                        }
                    } else {
                        if (cVar == c.READY) {
                            oVar = this.logger;
                            str = this.tag;
                            sb = "An ad is already marked as ready";
                        } else if (cVar != c.SHOWING) {
                            if (cVar != c.DESTROYED) {
                                oVar = this.logger;
                                str = this.tag;
                                StringBuilder sb7 = new StringBuilder();
                                sb7.append("Unable to transition to: ");
                                sb7.append(cVar);
                                sb = sb7.toString();
                            }
                        }
                        oVar.e(str, sb);
                        if (z) {
                        }
                    }
                }
            } else if (cVar2 == c.SHOWING) {
                if (cVar != c.IDLE) {
                    if (cVar == c.LOADING) {
                        str2 = this.tag;
                        str3 = "Can not load another ad while the ad is showing";
                    } else {
                        if (cVar == c.READY) {
                            oVar = this.logger;
                            str = this.tag;
                            sb = "An ad is already showing, ignoring";
                        } else if (cVar == c.SHOWING) {
                            str2 = this.tag;
                            str3 = "The ad is already showing, not showing another one";
                        } else if (cVar != c.DESTROYED) {
                            oVar = this.logger;
                            str = this.tag;
                            StringBuilder sb8 = new StringBuilder();
                            sb8.append("Unable to transition to: ");
                            sb8.append(cVar);
                            sb = sb8.toString();
                        }
                        oVar.e(str, sb);
                        if (z) {
                        }
                    }
                    o.i(str2, str3);
                    if (z) {
                    }
                }
            } else if (cVar2 == c.DESTROYED) {
                str2 = this.tag;
                str3 = "No operations are allowed on a destroyed instance";
                o.i(str2, str3);
                if (z) {
                }
            } else {
                oVar = this.logger;
                str = this.tag;
                StringBuilder sb9 = new StringBuilder();
                sb9.append("Unknown state: ");
                sb9.append(this.h);
                sb = sb9.toString();
                oVar.e(str, sb);
                if (z) {
                }
            }
            z = true;
            if (z) {
            }
        }
        if (z && runnable != null) {
            runnable.run();
        }
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.c cVar) {
        if (cVar.j()) {
            this.g = cVar;
            o oVar = this.logger;
            String str = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Handle ad loaded for fallback ad: ");
            sb.append(cVar);
            oVar.b(str, sb.toString());
            return;
        }
        this.f = cVar;
        o oVar2 = this.logger;
        String str2 = this.tag;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Handle ad loaded for regular ad: ");
        sb2.append(cVar);
        oVar2.b(str2, sb2.toString());
        b(cVar);
    }

    /* access modifiers changed from: private */
    public void a(String str, Activity activity) {
        synchronized (this.d) {
            this.e = a();
            this.sdk.B().b((com.applovin.impl.mediation.j.b) this.listenerWrapper);
            if (this.e.j()) {
                if (this.e.h().get()) {
                    o oVar = this.logger;
                    String str2 = this.tag;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to display ad: ");
                    sb.append(this.e);
                    sb.append(" - displayed already");
                    oVar.e(str2, sb.toString());
                    this.sdk.y().maybeScheduleAdDisplayErrorPostback(new e(-5201, "Ad displayed already"), this.e);
                    j.a(this.adListener, (MaxAd) d(), -5201);
                    return;
                }
                this.sdk.B().a(this.listenerWrapper, this.adFormat);
            }
            this.e.a(this.adUnitId);
            this.c.b(this.e);
            o oVar2 = this.logger;
            String str3 = this.tag;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Showing ad for '");
            sb2.append(this.adUnitId);
            sb2.append("'; loaded ad: ");
            sb2.append(this.e);
            sb2.append("...");
            oVar2.b(str3, sb2.toString());
            this.sdk.y().showFullscreenAd(this.e, str, activity);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        com.applovin.impl.mediation.b.c cVar;
        synchronized (this.d) {
            cVar = this.e;
            this.e = null;
            if (cVar == this.g) {
                this.g = null;
            } else if (cVar == this.f) {
                this.f = null;
            }
        }
        this.sdk.y().destroyAd(cVar);
    }

    private void b(com.applovin.impl.mediation.b.c cVar) {
        long n = cVar.n();
        if (n >= 0) {
            o oVar = this.logger;
            String str = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Scheduling ad expiration ");
            sb.append(TimeUnit.MILLISECONDS.toMinutes(n));
            sb.append(" minutes from now for ");
            sb.append(getAdUnitId());
            sb.append(" ...");
            oVar.b(str, sb.toString());
            this.b.a(n);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        com.applovin.impl.mediation.b.c cVar;
        if (this.i.compareAndSet(true, false)) {
            synchronized (this.d) {
                cVar = this.f;
                this.f = null;
            }
            this.sdk.y().destroyAd(cVar);
            this.loadRequestBuilder.a("expired_ad_ad_unit_id");
        }
    }

    /* access modifiers changed from: private */
    public l d() {
        return new l(this.adUnitId, this.adFormat);
    }

    public void destroy() {
        a(c.DESTROYED, (Runnable) new Runnable() {
            public void run() {
                synchronized (MaxFullscreenAdImpl.this.d) {
                    if (MaxFullscreenAdImpl.this.f != null) {
                        o oVar = MaxFullscreenAdImpl.this.logger;
                        String str = MaxFullscreenAdImpl.this.tag;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Destroying ad for '");
                        sb.append(MaxFullscreenAdImpl.this.adUnitId);
                        sb.append("'; current ad: ");
                        sb.append(MaxFullscreenAdImpl.this.f);
                        sb.append("...");
                        oVar.b(str, sb.toString());
                        MaxFullscreenAdImpl.this.sdk.y().destroyAd(MaxFullscreenAdImpl.this.f);
                    }
                }
            }
        });
    }

    public boolean isReady() {
        boolean z;
        synchronized (this.d) {
            z = a() != null && a().a() && this.h == c.READY;
        }
        return z;
    }

    public void loadAd(final Activity activity) {
        o oVar = this.logger;
        String str = this.tag;
        StringBuilder sb = new StringBuilder();
        sb.append("Loading ad for '");
        sb.append(this.adUnitId);
        sb.append("'...");
        oVar.b(str, sb.toString());
        if (isReady()) {
            o oVar2 = this.logger;
            String str2 = this.tag;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("An ad is already loaded for '");
            sb2.append(this.adUnitId);
            sb2.append("'");
            oVar2.b(str2, sb2.toString());
            j.a(this.adListener, (MaxAd) d());
            return;
        }
        a(c.LOADING, (Runnable) new Runnable() {
            public void run() {
                final com.applovin.impl.mediation.b.c c = MaxFullscreenAdImpl.this.a();
                if (c == null || c.j()) {
                    MaxFullscreenAdImpl.this.sdk.B().a((com.applovin.impl.mediation.j.b) MaxFullscreenAdImpl.this.listenerWrapper);
                    MaxFullscreenAdImpl.this.sdk.y().loadAd(MaxFullscreenAdImpl.this.adUnitId, MaxFullscreenAdImpl.this.adFormat, MaxFullscreenAdImpl.this.loadRequestBuilder.a(), false, activity, MaxFullscreenAdImpl.this.listenerWrapper);
                    return;
                }
                MaxFullscreenAdImpl.this.a(c.READY, (Runnable) new Runnable() {
                    public void run() {
                        j.a(MaxFullscreenAdImpl.this.adListener, (MaxAd) c);
                    }
                });
            }
        });
    }

    public void onAdExpired() {
        o oVar = this.logger;
        String str = this.tag;
        StringBuilder sb = new StringBuilder();
        sb.append("Ad expired ");
        sb.append(getAdUnitId());
        oVar.b(str, sb.toString());
        Activity activity = this.f1181a.getActivity();
        if (activity == null) {
            activity = this.sdk.aa().a();
            if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.b.I)).booleanValue() || activity == null) {
                this.listenerWrapper.onAdLoadFailed(this.adUnitId, MaxErrorCodes.NO_ACTIVITY);
                return;
            }
        }
        Activity activity2 = activity;
        this.i.set(true);
        this.loadRequestBuilder.a("expired_ad_ad_unit_id", getAdUnitId());
        this.sdk.y().loadAd(this.adUnitId, this.adFormat, this.loadRequestBuilder.a(), false, activity2, this.listenerWrapper);
    }

    public void showAd(final String str, final Activity activity) {
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.b.F)).booleanValue() || (!this.sdk.Z().a() && !this.sdk.Z().b())) {
            a(c.SHOWING, (Runnable) new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.a(str, activity);
                }
            });
            return;
        }
        o.i(this.tag, "Attempting to show ad when another fullscreen ad is already showing");
        j.a(this.adListener, (MaxAd) a(), -23);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.tag);
        sb.append("{adUnitId='");
        sb.append(this.adUnitId);
        sb.append('\'');
        sb.append(", adListener=");
        sb.append(this.adListener);
        sb.append(", isReady=");
        sb.append(isReady());
        sb.append('}');
        return sb.toString();
    }
}
