package com.applovin.impl.mediation.ads;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import com.applovin.impl.sdk.d;
import com.applovin.impl.sdk.d.ac;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.u;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.q;
import com.applovin.impl.sdk.v;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import java.util.concurrent.TimeUnit;

public class MaxAdViewImpl extends a implements com.applovin.impl.sdk.d.a, com.applovin.impl.sdk.v.a {
    private static final int[] q = {10, 14};
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Activity f1173a;
    /* access modifiers changed from: private */
    public final MaxAdView b;
    private final View c;
    private long d = Long.MAX_VALUE;
    private com.applovin.impl.mediation.b.b e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public final a g;
    /* access modifiers changed from: private */
    public final c h;
    /* access modifiers changed from: private */
    public final d i;
    /* access modifiers changed from: private */
    public final u j;
    /* access modifiers changed from: private */
    public final v k;
    /* access modifiers changed from: private */
    public final Object l = new Object();
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.b m = null;
    private boolean n;
    private boolean o;
    private boolean p = false;

    private class a extends b {
        private a() {
            super();
        }

        public void onAdLoadFailed(String str, int i) {
            j.a(MaxAdViewImpl.this.adListener, str, i);
            MaxAdViewImpl.this.a(i);
        }

        public void onAdLoaded(MaxAd maxAd) {
            if (maxAd instanceof com.applovin.impl.mediation.b.b) {
                com.applovin.impl.mediation.b.b bVar = (com.applovin.impl.mediation.b.b) maxAd;
                bVar.d(MaxAdViewImpl.this.f);
                MaxAdViewImpl.this.a(bVar);
                if (bVar.E()) {
                    long F = bVar.F();
                    o v = MaxAdViewImpl.this.sdk.v();
                    String str = MaxAdViewImpl.this.tag;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Scheduling banner ad refresh ");
                    sb.append(F);
                    sb.append(" milliseconds from now for '");
                    sb.append(MaxAdViewImpl.this.adUnitId);
                    sb.append("'...");
                    v.b(str, sb.toString());
                    MaxAdViewImpl.this.i.a(F);
                }
                j.a(MaxAdViewImpl.this.adListener, maxAd);
                return;
            }
            o oVar = MaxAdViewImpl.this.logger;
            String str2 = MaxAdViewImpl.this.tag;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Not a MediatedAdViewAd received: ");
            sb2.append(maxAd);
            oVar.e(str2, sb2.toString());
            onAdLoadFailed(MaxAdViewImpl.this.adUnitId, -5201);
        }
    }

    private abstract class b implements MaxAdListener, MaxAdViewAdListener {
        private b() {
        }

        public void onAdClicked(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.d(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdCollapsed(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                if (MaxAdViewImpl.this.m.t()) {
                    MaxAdViewImpl.this.startAutoRefresh();
                }
                j.h(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.a(MaxAdViewImpl.this.adListener, maxAd, i);
            }
        }

        public void onAdDisplayed(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.b(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdExpanded(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                if (MaxAdViewImpl.this.m.t()) {
                    MaxAdViewImpl.this.stopAutoRefresh();
                }
                j.g(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdHidden(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.c(MaxAdViewImpl.this.adListener, maxAd);
            }
        }
    }

    private class c extends b {
        private c() {
            super();
        }

        public void onAdLoadFailed(String str, int i) {
            o oVar = MaxAdViewImpl.this.logger;
            String str2 = MaxAdViewImpl.this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to pre-cache ad for refresh with error code ");
            sb.append(i);
            oVar.b(str2, sb.toString());
            MaxAdViewImpl.this.a(i);
        }

        public void onAdLoaded(MaxAd maxAd) {
            MaxAdViewImpl.this.logger.b(MaxAdViewImpl.this.tag, "Successfully pre-cached ad for refresh");
            MaxAdViewImpl.this.a(maxAd);
        }
    }

    public MaxAdViewImpl(String str, MaxAdView maxAdView, View view, i iVar, Activity activity) {
        super(str, MaxAdFormat.BANNER, "MaxAdView", iVar);
        if (activity != null) {
            this.f1173a = activity;
            this.b = maxAdView;
            this.c = view;
            this.g = new a();
            this.h = new c();
            this.i = new d(iVar, this);
            this.j = new u(maxAdView, iVar);
            this.k = new v(maxAdView, iVar, this);
            o oVar = this.logger;
            String str2 = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Created new MaxAdView (");
            sb.append(this);
            sb.append(")");
            oVar.b(str2, sb.toString());
            return;
        }
        throw new IllegalArgumentException("No activity specified");
    }

    /* access modifiers changed from: private */
    public void a() {
        com.applovin.impl.mediation.b.b bVar;
        MaxAdView maxAdView = this.b;
        if (maxAdView != null) {
            com.applovin.impl.sdk.utils.b.a(maxAdView, this.c);
        }
        this.k.a();
        synchronized (this.l) {
            bVar = this.m;
        }
        if (bVar != null) {
            this.sdk.y().destroyAd(bVar);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.sdk.b((com.applovin.impl.sdk.b.c) com.applovin.impl.sdk.b.b.t).contains(String.valueOf(i2))) {
            o v = this.sdk.v();
            String str = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Ignoring banner ad refresh for error code '");
            sb.append(i2);
            sb.append("'...");
            v.b(str, sb.toString());
            return;
        }
        this.n = true;
        long longValue = ((Long) this.sdk.a(com.applovin.impl.sdk.b.b.s)).longValue();
        if (longValue >= 0) {
            o v2 = this.sdk.v();
            String str2 = this.tag;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Scheduling failed banner ad refresh ");
            sb2.append(longValue);
            sb2.append(" milliseconds from now for '");
            sb2.append(this.adUnitId);
            sb2.append("'...");
            v2.b(str2, sb2.toString());
            this.i.a(longValue);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        if (p.a(j2, ((Long) this.sdk.a(com.applovin.impl.sdk.b.b.C)).longValue())) {
            o oVar = this.logger;
            String str = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Undesired flags matched - current: ");
            sb.append(Long.toBinaryString(j2));
            sb.append(", undesired: ");
            sb.append(Long.toBinaryString(j2));
            oVar.b(str, sb.toString());
            this.logger.b(this.tag, "Waiting for refresh timer to manually fire request");
            this.n = true;
            return;
        }
        this.logger.b(this.tag, "No undesired viewability flags matched - scheduling viewability");
        this.n = false;
        b();
    }

    /* access modifiers changed from: private */
    public void a(AnimatorListenerAdapter animatorListenerAdapter) {
        if (this.m == null || this.m.l() == null) {
            animatorListenerAdapter.onAnimationEnd(null);
            return;
        }
        View l2 = this.m.l();
        l2.animate().alpha(0.0f).setDuration(((Long) this.sdk.a(com.applovin.impl.sdk.b.b.z)).longValue()).setListener(animatorListenerAdapter).start();
    }

    private void a(View view, com.applovin.impl.mediation.b.b bVar) {
        int j2 = bVar.j();
        int k2 = bVar.k();
        int i2 = -1;
        int dpToPx = j2 == -1 ? -1 : AppLovinSdkUtils.dpToPx(view.getContext(), j2);
        if (k2 != -1) {
            i2 = AppLovinSdkUtils.dpToPx(view.getContext(), k2);
        }
        int height = this.b.getHeight();
        int width = this.b.getWidth();
        if ((height > 0 && height < i2) || (width > 0 && width < dpToPx)) {
            int pxToDp = AppLovinSdkUtils.pxToDp(view.getContext(), height);
            int pxToDp2 = AppLovinSdkUtils.pxToDp(view.getContext(), width);
            StringBuilder sb = new StringBuilder();
            sb.append("\n**************************************************\n`MaxAdView` size ");
            sb.append(pxToDp2);
            sb.append(AvidJSONUtil.KEY_X);
            sb.append(pxToDp);
            sb.append(" dp smaller than required size: ");
            sb.append(j2);
            sb.append(AvidJSONUtil.KEY_X);
            sb.append(k2);
            sb.append(" dp\n**************************************************\n");
            o.h("AppLovinSdk", sb.toString());
        }
        LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new RelativeLayout.LayoutParams(dpToPx, i2);
        } else {
            layoutParams.width = dpToPx;
            layoutParams.height = i2;
        }
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            o oVar = this.logger;
            String str = this.tag;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Pinning ad view to MAX ad view with width: ");
            sb2.append(dpToPx);
            sb2.append(" and height: ");
            sb2.append(i2);
            sb2.append(".");
            oVar.b(str, sb2.toString());
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
            for (int addRule : g.c() ? q.a(this.b.getGravity(), 10, 14) : q) {
                layoutParams2.addRule(addRule);
            }
        }
        view.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public void a(final com.applovin.impl.mediation.b.b bVar) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                String str;
                String str2;
                o oVar;
                if (bVar.l() != null) {
                    final MaxAdView d = MaxAdViewImpl.this.b;
                    if (d != null) {
                        MaxAdViewImpl.this.a((AnimatorListenerAdapter) new AnimatorListenerAdapter() {
                            public void onAnimationEnd(Animator animator) {
                                super.onAnimationEnd(animator);
                                MaxAdViewImpl.this.a();
                                if (bVar.r()) {
                                    MaxAdViewImpl.this.k.a((Context) MaxAdViewImpl.this.f1173a, bVar);
                                }
                                MaxAdViewImpl.this.a(bVar, d);
                                synchronized (MaxAdViewImpl.this.l) {
                                    MaxAdViewImpl.this.m = bVar;
                                }
                                MaxAdViewImpl.this.logger.b(MaxAdViewImpl.this.tag, "Scheduling impression for ad manually...");
                                MaxAdViewImpl.this.sdk.y().maybeScheduleRawAdImpressionPostback(bVar);
                                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                                    public void run() {
                                        long a2 = MaxAdViewImpl.this.j.a(bVar);
                                        if (!bVar.r()) {
                                            MaxAdViewImpl.this.a(bVar, a2);
                                        }
                                        MaxAdViewImpl.this.a(a2);
                                    }
                                }, bVar.m());
                            }
                        });
                        return;
                    }
                    oVar = MaxAdViewImpl.this.logger;
                    str2 = MaxAdViewImpl.this.tag;
                    str = "Max ad view does not have a parent View";
                } else {
                    oVar = MaxAdViewImpl.this.logger;
                    str2 = MaxAdViewImpl.this.tag;
                    str = "Max ad does not have a loaded ad view";
                }
                oVar.e(str2, str);
                MaxAdViewImpl.this.g.onAdDisplayFailed(bVar, -5201);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.b bVar, long j2) {
        this.logger.b(this.tag, "Scheduling viewability impression for ad...");
        this.sdk.y().maybeScheduleViewabilityAdImpressionPostback(bVar, j2);
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.b bVar, MaxAdView maxAdView) {
        View view;
        int i2;
        long j2;
        View l2 = bVar.l();
        l2.setAlpha(0.0f);
        if (bVar.u() != Long.MAX_VALUE) {
            view = this.c;
            j2 = bVar.u();
        } else if (this.d != Long.MAX_VALUE) {
            view = this.c;
            j2 = this.d;
        } else {
            view = this.c;
            i2 = 0;
            view.setBackgroundColor(i2);
            maxAdView.addView(l2);
            a(l2, bVar);
            l2.animate().alpha(1.0f).setDuration(((Long) this.sdk.a(com.applovin.impl.sdk.b.b.y)).longValue()).start();
        }
        i2 = (int) j2;
        view.setBackgroundColor(i2);
        maxAdView.addView(l2);
        a(l2, bVar);
        l2.animate().alpha(1.0f).setDuration(((Long) this.sdk.a(com.applovin.impl.sdk.b.b.y)).longValue()).start();
    }

    /* access modifiers changed from: private */
    public void a(MaxAd maxAd) {
        if (this.o) {
            this.o = false;
            o oVar = this.logger;
            String str = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Rendering precache request ad: ");
            sb.append(maxAd.getAdUnitId());
            sb.append("...");
            oVar.b(str, sb.toString());
            this.g.onAdLoaded(maxAd);
            return;
        }
        this.e = (com.applovin.impl.mediation.b.b) maxAd;
    }

    /* access modifiers changed from: private */
    public void a(final MaxAdListener maxAdListener) {
        if (d()) {
            o.i(this.tag, "Unable to load new ad; ad is already destroyed");
            j.a(this.adListener, this.adUnitId, -1);
            return;
        }
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (MaxAdViewImpl.this.m != null) {
                    MaxAdViewImpl.this.loadRequestBuilder.a("visible_ad_ad_unit_id", MaxAdViewImpl.this.m.getAdUnitId()).a("viewability_flags", String.valueOf(MaxAdViewImpl.this.j.a(MaxAdViewImpl.this.m)));
                } else {
                    MaxAdViewImpl.this.loadRequestBuilder.a("visible_ad_ad_unit_id").a("viewability_flags");
                }
                o oVar = MaxAdViewImpl.this.logger;
                String str = MaxAdViewImpl.this.tag;
                StringBuilder sb = new StringBuilder();
                sb.append("Loading banner ad for '");
                sb.append(MaxAdViewImpl.this.adUnitId);
                sb.append("' and notifying ");
                sb.append(maxAdListener);
                sb.append("...");
                oVar.b(str, sb.toString());
                MaxAdViewImpl.this.sdk.y().loadAd(MaxAdViewImpl.this.adUnitId, MaxAdViewImpl.this.adFormat, MaxAdViewImpl.this.loadRequestBuilder.a(), false, MaxAdViewImpl.this.f1173a, maxAdListener);
            }
        });
    }

    private void b() {
        if (c()) {
            long longValue = ((Long) this.sdk.a(com.applovin.impl.sdk.b.b.D)).longValue();
            o oVar = this.logger;
            String str = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Scheduling refresh precache request in ");
            sb.append(TimeUnit.MICROSECONDS.toSeconds(longValue));
            sb.append(" seconds...");
            oVar.b(str, sb.toString());
            this.sdk.K().a(new ac(this.sdk, new Runnable() {
                public void run() {
                    MaxAdViewImpl.this.a((MaxAdListener) MaxAdViewImpl.this.h);
                }
            }), com.applovin.impl.mediation.d.c.a(this.adFormat), longValue);
        }
    }

    private boolean c() {
        return ((Long) this.sdk.a(com.applovin.impl.sdk.b.b.D)).longValue() > 0;
    }

    private boolean d() {
        boolean z;
        synchronized (this.l) {
            z = this.p;
        }
        return z;
    }

    public void destroy() {
        a();
        synchronized (this.l) {
            this.p = true;
        }
        this.i.c();
    }

    public String getPlacement() {
        return this.f;
    }

    public void loadAd() {
        o oVar = this.logger;
        String str = this.tag;
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this);
        sb.append(" Loading ad for ");
        sb.append(this.adUnitId);
        sb.append("...");
        oVar.b(str, sb.toString());
        if (d()) {
            o.i(this.tag, "Unable to load new ad; ad is already destroyed");
            j.a(this.adListener, this.adUnitId, -1);
        } else if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.b.E)).booleanValue() || !this.i.a()) {
            a((MaxAdListener) this.g);
        } else {
            String str2 = this.tag;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to load a new ad. An ad refresh has already been scheduled in ");
            sb2.append(TimeUnit.MILLISECONDS.toSeconds(this.i.b()));
            sb2.append(" seconds.");
            o.i(str2, sb2.toString());
        }
    }

    public void onAdRefresh() {
        String str;
        String str2;
        o oVar;
        this.o = false;
        if (this.e != null) {
            o oVar2 = this.logger;
            String str3 = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Refreshing for cached ad: ");
            sb.append(this.e.getAdUnitId());
            sb.append("...");
            oVar2.b(str3, sb.toString());
            this.g.onAdLoaded(this.e);
            this.e = null;
            return;
        }
        if (!c()) {
            oVar = this.logger;
            str2 = this.tag;
            str = "Refreshing ad from network...";
        } else if (this.n) {
            oVar = this.logger;
            str2 = this.tag;
            str = "Refreshing ad from network due to viewability requirements not met for refresh request...";
        } else {
            this.logger.e(this.tag, "Ignoring attempt to refresh ad - either still waiting for precache or did not attempt request due to visibility requirement not met");
            this.o = true;
            return;
        }
        oVar.b(str2, str);
        loadAd();
    }

    public void onLogVisibilityImpression() {
        a(this.m, this.j.a(this.m));
    }

    public void onWindowVisibilityChanged(int i2) {
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.b.x)).booleanValue() && this.i.a()) {
            if (q.a(i2)) {
                this.logger.b(this.tag, "Ad view visible");
                this.i.g();
                return;
            }
            this.logger.b(this.tag, "Ad view hidden");
            this.i.f();
        }
    }

    public void setPlacement(String str) {
        this.f = str;
    }

    public void setPublisherBackgroundColor(int i2) {
        this.d = (long) i2;
    }

    public void startAutoRefresh() {
        this.i.e();
        o oVar = this.logger;
        String str = this.tag;
        StringBuilder sb = new StringBuilder();
        sb.append("Resumed auto-refresh with remaining time: ");
        sb.append(this.i.b());
        oVar.b(str, sb.toString());
    }

    public void stopAutoRefresh() {
        if (this.m != null) {
            o oVar = this.logger;
            String str = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append("Pausing auto-refresh with remaining time: ");
            sb.append(this.i.b());
            oVar.b(str, sb.toString());
            this.i.d();
            return;
        }
        o.h(this.tag, "Stopping auto-refresh has no effect until after the first ad has been loaded.");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MaxAdView{adUnitId='");
        sb.append(this.adUnitId);
        sb.append('\'');
        sb.append(", adListener=");
        sb.append(this.adListener);
        sb.append(", isDestroyed=");
        sb.append(d());
        sb.append('}');
        return sb.toString();
    }
}
