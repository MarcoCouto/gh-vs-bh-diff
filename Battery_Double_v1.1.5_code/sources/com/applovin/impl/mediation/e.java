package com.applovin.impl.mediation;

public class e {
    public static final e EMPTY = new e(0);
    private final int errorCode;
    private final String errorMessage;

    public e(int i) {
        this(i, "");
    }

    public e(int i, String str) {
        this.errorCode = i;
        this.errorMessage = str;
    }

    public e(String str) {
        this(-1, str);
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MaxError{errorCode=");
        sb.append(getErrorCode());
        sb.append(", errorMessage='");
        sb.append(getErrorMessage());
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
