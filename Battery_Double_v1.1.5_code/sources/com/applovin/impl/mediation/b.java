package com.applovin.impl.mediation;

import com.applovin.impl.mediation.a.C0008a;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.mediation.c.a;
import com.applovin.impl.sdk.i;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinSdkUtils;

public class b implements C0008a, a {

    /* renamed from: a reason: collision with root package name */
    private final a f1192a;
    private final c b;
    /* access modifiers changed from: private */
    public final MaxAdListener c;

    public b(i iVar, MaxAdListener maxAdListener) {
        this.c = maxAdListener;
        this.f1192a = new a(iVar);
        this.b = new c(iVar, this);
    }

    public void a(final c cVar) {
        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
            public void run() {
                b.this.c.onAdHidden(cVar);
            }
        }, cVar.q());
    }

    public void a(MaxAd maxAd) {
        this.b.a();
        this.f1192a.a();
    }

    public void b(c cVar) {
        long o = cVar.o();
        if (o >= 0) {
            this.b.a(cVar, o);
        }
        if (cVar.p()) {
            this.f1192a.a(cVar, this);
        }
    }

    public void c(c cVar) {
        this.c.onAdHidden(cVar);
    }
}
