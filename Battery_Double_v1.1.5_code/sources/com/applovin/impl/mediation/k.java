package com.applovin.impl.mediation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class k {

    /* renamed from: a reason: collision with root package name */
    private final List<a> f1263a = Collections.synchronizedList(new ArrayList());

    public interface a {
        void a(com.applovin.impl.mediation.b.a aVar);
    }

    public void a(com.applovin.impl.mediation.b.a aVar) {
        Iterator it = new ArrayList(this.f1263a).iterator();
        while (it.hasNext()) {
            ((a) it.next()).a(aVar);
        }
    }

    public void a(a aVar) {
        this.f1263a.add(aVar);
    }

    public void b(a aVar) {
        this.f1263a.remove(aVar);
    }
}
