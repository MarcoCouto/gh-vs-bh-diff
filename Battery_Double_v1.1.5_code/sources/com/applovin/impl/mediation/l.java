package com.applovin.impl.mediation;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;

public class l implements MaxAd {

    /* renamed from: a reason: collision with root package name */
    private final String f1264a;
    private final MaxAdFormat b;

    public l(String str, MaxAdFormat maxAdFormat) {
        this.f1264a = str;
        this.b = maxAdFormat;
    }

    public String getAdUnitId() {
        return this.f1264a;
    }

    public MaxAdFormat getFormat() {
        return this.b;
    }
}
