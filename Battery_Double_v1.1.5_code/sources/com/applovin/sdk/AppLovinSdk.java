package com.applovin.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.nativeAds.AppLovinNativeAdService;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AppLovinSdk {
    private static final String TAG = "AppLovinSdk";
    public static final String VERSION = getVersion();
    public static final int VERSION_CODE = getVersionCode();
    private static final Map<String, AppLovinSdk> sdkInstances = new HashMap();
    private static final Object sdkInstancesLock = new Object();
    private final i mSdkImpl;

    public interface SdkInitializationListener {
        void onSdkInitialized(AppLovinSdkConfiguration appLovinSdkConfiguration);
    }

    private static class a extends AppLovinSdkSettings {
        a(Context context) {
            super(context);
        }
    }

    private AppLovinSdk(i iVar) {
        this.mSdkImpl = iVar;
    }

    public static List<AppLovinSdk> a() {
        return new ArrayList(sdkInstances.values());
    }

    public static AppLovinSdk getInstance(Context context) {
        return getInstance(new a(context), context);
    }

    public static AppLovinSdk getInstance(AppLovinSdkSettings appLovinSdkSettings, Context context) {
        if (context != null) {
            return getInstance(p.a(context), appLovinSdkSettings, context);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public static AppLovinSdk getInstance(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        if (appLovinSdkSettings == null) {
            throw new IllegalArgumentException("No userSettings specified");
        } else if (context != null) {
            synchronized (sdkInstancesLock) {
                if (sdkInstances.containsKey(str)) {
                    AppLovinSdk appLovinSdk = (AppLovinSdk) sdkInstances.get(str);
                    return appLovinSdk;
                }
                if (!TextUtils.isEmpty(str) && str.contains(File.separator)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("\n**************************************************\nINVALID SDK KEY: ");
                    sb.append(str);
                    sb.append("\n**************************************************\n");
                    o.i(TAG, sb.toString());
                    if (!sdkInstances.isEmpty()) {
                        AppLovinSdk appLovinSdk2 = (AppLovinSdk) sdkInstances.values().iterator().next();
                        return appLovinSdk2;
                    }
                    str = str.replace(File.separator, "");
                }
                i iVar = new i();
                iVar.a(str, appLovinSdkSettings, context);
                AppLovinSdk appLovinSdk3 = new AppLovinSdk(iVar);
                iVar.a(appLovinSdk3);
                sdkInstances.put(str, appLovinSdk3);
                return appLovinSdk3;
            }
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    private static String getVersion() {
        return "9.9.2";
    }

    private static int getVersionCode() {
        return 90902;
    }

    public static void initializeSdk(Context context) {
        initializeSdk(context, null);
    }

    public static void initializeSdk(Context context, SdkInitializationListener sdkInitializationListener) {
        if (context != null) {
            AppLovinSdk instance = getInstance(context);
            if (instance != null) {
                instance.initializeSdk(sdkInitializationListener);
            } else {
                o.i(TAG, "Unable to initialize AppLovin SDK: SDK object not created");
            }
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    static void reinitializeAll() {
        reinitializeAll(null);
    }

    static void reinitializeAll(Boolean bool) {
        synchronized (sdkInstancesLock) {
            for (AppLovinSdk appLovinSdk : sdkInstances.values()) {
                appLovinSdk.mSdkImpl.b();
                if (bool != null && bool.booleanValue()) {
                    HashMap hashMap = new HashMap(1);
                    hashMap.put("value", bool.toString());
                    appLovinSdk.getEventService().trackEvent("huc", hashMap);
                }
            }
        }
    }

    public AppLovinAdService getAdService() {
        return this.mSdkImpl.o();
    }

    /* access modifiers changed from: 0000 */
    @Deprecated
    public Context getApplicationContext() {
        return this.mSdkImpl.D();
    }

    public AppLovinSdkConfiguration getConfiguration() {
        return this.mSdkImpl.m();
    }

    public AppLovinEventService getEventService() {
        return this.mSdkImpl.q();
    }

    @Deprecated
    public o getLogger() {
        return this.mSdkImpl.v();
    }

    public String getMediationProvider() {
        return this.mSdkImpl.n();
    }

    public AppLovinNativeAdService getNativeAdService() {
        return this.mSdkImpl.p();
    }

    public AppLovinPostbackService getPostbackService() {
        return this.mSdkImpl.R();
    }

    public String getSdkKey() {
        return this.mSdkImpl.t();
    }

    public AppLovinSdkSettings getSettings() {
        return this.mSdkImpl.l();
    }

    public String getUserIdentifier() {
        return this.mSdkImpl.i();
    }

    public AppLovinUserService getUserService() {
        return this.mSdkImpl.r();
    }

    public AppLovinVariableService getVariableService() {
        return this.mSdkImpl.s();
    }

    public boolean hasCriticalErrors() {
        return this.mSdkImpl.u();
    }

    public void initializeSdk() {
    }

    public void initializeSdk(SdkInitializationListener sdkInitializationListener) {
        this.mSdkImpl.a(sdkInitializationListener);
    }

    public boolean isEnabled() {
        return this.mSdkImpl.d();
    }

    public void setMediationProvider(String str) {
        this.mSdkImpl.c(str);
    }

    public void setPluginVersion(String str) {
        this.mSdkImpl.a(str);
    }

    public void setUserIdentifier(String str) {
        this.mSdkImpl.b(str);
    }

    public void showMediationDebugger() {
        this.mSdkImpl.g();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinSdk{sdkKey='");
        sb.append(getSdkKey());
        sb.append("', isEnabled=");
        sb.append(isEnabled());
        sb.append(", isFirstSession=");
        sb.append(this.mSdkImpl.H());
        sb.append('}');
        return sb.toString();
    }
}
