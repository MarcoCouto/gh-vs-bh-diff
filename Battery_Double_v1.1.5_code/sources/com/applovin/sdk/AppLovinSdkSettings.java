package com.applovin.sdk;

import android.content.Context;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.p;
import java.util.HashMap;
import java.util.Map;

public class AppLovinSdkSettings {

    /* renamed from: a reason: collision with root package name */
    private boolean f1489a;
    private boolean b;
    private long c;
    private String d;
    private String e;
    private boolean f;
    private final Map<String, Object> localSettings;

    public AppLovinSdkSettings() {
        this(null);
    }

    public AppLovinSdkSettings(Context context) {
        this.localSettings = new HashMap();
        this.b = p.c(context);
        this.f1489a = p.b(context);
        this.c = -1;
        StringBuilder sb = new StringBuilder();
        sb.append(AppLovinAdSize.INTERSTITIAL.getLabel());
        sb.append(",");
        sb.append(AppLovinAdSize.BANNER.getLabel());
        sb.append(",");
        sb.append(AppLovinAdSize.MREC.getLabel());
        this.d = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(AppLovinAdType.INCENTIVIZED.getLabel());
        sb2.append(",");
        sb2.append(AppLovinAdType.REGULAR.getLabel());
        sb2.append(",");
        sb2.append(AppLovinAdType.NATIVE.getLabel());
        this.e = sb2.toString();
    }

    @Deprecated
    public String getAutoPreloadSizes() {
        return this.d;
    }

    @Deprecated
    public String getAutoPreloadTypes() {
        return this.e;
    }

    @Deprecated
    public long getBannerAdRefreshSeconds() {
        return this.c;
    }

    public boolean isMuted() {
        return this.f;
    }

    public boolean isTestAdsEnabled() {
        return this.f1489a;
    }

    public boolean isVerboseLoggingEnabled() {
        return this.b;
    }

    @Deprecated
    public void setAutoPreloadSizes(String str) {
        this.d = str;
    }

    @Deprecated
    public void setAutoPreloadTypes(String str) {
        this.e = str;
    }

    @Deprecated
    public void setBannerAdRefreshSeconds(long j) {
        this.c = j;
    }

    public void setMuted(boolean z) {
        this.f = z;
    }

    public void setTestAdsEnabled(boolean z) {
        this.f1489a = z;
    }

    public void setVerboseLogging(boolean z) {
        if (p.a()) {
            o.i("AppLovinSdkSettings", "Ignoring setting of verbose logging - it is configured from Android manifest already or AppLovinSdkSettings was initialized without a context.");
        } else {
            this.b = z;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinSdkSettings{isTestAdsEnabled=");
        sb.append(this.f1489a);
        sb.append(", isVerboseLoggingEnabled=");
        sb.append(this.b);
        sb.append(", muted=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
}
