package com.applovin.sdk;

public final class R {

    public static final class color {
        public static final int applovin_sdk_actionBarBackground = 2131099680;
        public static final int applovin_sdk_checkmarkColor = 2131099681;
        public static final int applovin_sdk_colorEdgeEffect = 2131099682;
        public static final int applovin_sdk_disclosureButtonColor = 2131099683;
        public static final int applovin_sdk_listViewBackground = 2131099684;
        public static final int applovin_sdk_listViewSectionTextColor = 2131099685;
        public static final int applovin_sdk_textColorPrimary = 2131099686;
        public static final int applovin_sdk_xmarkColor = 2131099687;

        private color() {
        }
    }

    public static final class dimen {
        public static final int applovin_sdk_actionBarHeight = 2131165261;
        public static final int applovin_sdk_mediationDebuggerDetailListItemTextSize = 2131165262;
        public static final int applovin_sdk_mediationDebuggerSectionHeight = 2131165263;
        public static final int applovin_sdk_mediationDebuggerSectionTextSize = 2131165264;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int applovin_ic_check_mark = 2131230814;
        public static final int applovin_ic_disclosure_arrow = 2131230815;
        public static final int applovin_ic_x_mark = 2131230816;

        private drawable() {
        }
    }

    public static final class id {
        public static final int imageView = 2131296424;
        public static final int listView = 2131296449;

        private id() {
        }
    }

    public static final class layout {
        public static final int list_item_detail = 2131427396;
        public static final int list_item_right_detail = 2131427397;
        public static final int list_section = 2131427398;
        public static final int mediation_debugger_activity = 2131427400;
        public static final int mediation_debugger_detail_activity = 2131427401;

        private layout() {
        }
    }

    public static final class string {
        public static final int applovin_instructions_dialog_title = 2131623979;
        public static final int applovin_list_item_image_description = 2131623980;

        private string() {
        }
    }

    public static final class style {
        public static final int com_applovin_mediation_MaxDebuggerActivity_ActionBar = 2131689885;
        public static final int com_applovin_mediation_MaxDebuggerActivity_Theme = 2131689886;

        private style() {
        }
    }

    private R() {
    }
}
