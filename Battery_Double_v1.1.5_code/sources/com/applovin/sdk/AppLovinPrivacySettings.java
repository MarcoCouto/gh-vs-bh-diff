package com.applovin.sdk;

import android.content.Context;
import com.applovin.impl.sdk.f;

public class AppLovinPrivacySettings {
    public static boolean hasUserConsent(Context context) {
        Boolean a2 = f.a(context);
        if (a2 != null) {
            return a2.booleanValue();
        }
        return false;
    }

    public static boolean isAgeRestrictedUser(Context context) {
        Boolean b = f.b(context);
        if (b != null) {
            return b.booleanValue();
        }
        return false;
    }

    public static void setHasUserConsent(boolean z, Context context) {
        if (f.a(z, context)) {
            AppLovinSdk.reinitializeAll(Boolean.valueOf(z));
        }
    }

    public static void setIsAgeRestrictedUser(boolean z, Context context) {
        if (f.b(z, context)) {
            AppLovinSdk.reinitializeAll();
        }
    }
}
