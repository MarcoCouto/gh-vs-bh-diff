package com.applovin.sdk;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.utils.m;
import java.util.Set;

public class AppLovinWebViewActivity extends Activity {
    public static final String EVENT_DISMISSED_VIA_BACK_BUTTON = "dismissed_via_back_button";
    public static final String INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON = "immersive_mode_on";
    public static final String INTENT_EXTRA_KEY_SDK_KEY = "sdk_key";

    /* renamed from: a reason: collision with root package name */
    private WebView f1491a;
    /* access modifiers changed from: private */
    public EventListener b;

    public interface EventListener {
        void onReceivedEvent(String str);
    }

    public void loadUrl(String str, EventListener eventListener) {
        this.b = eventListener;
        this.f1491a.loadUrl(str);
    }

    public void onBackPressed() {
        if (this.b != null) {
            this.b.onReceivedEvent(EVENT_DISMISSED_VIA_BACK_BUTTON);
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra(INTENT_EXTRA_KEY_SDK_KEY);
        if (!TextUtils.isEmpty(stringExtra)) {
            final AppLovinSdk instance = AppLovinSdk.getInstance(stringExtra, new AppLovinSdkSettings(), getApplicationContext());
            this.f1491a = new WebView(this);
            setContentView(this.f1491a);
            WebSettings settings = this.f1491a.getSettings();
            settings.setSupportMultipleWindows(false);
            settings.setJavaScriptEnabled(true);
            this.f1491a.setVerticalScrollBarEnabled(true);
            this.f1491a.setHorizontalScrollBarEnabled(true);
            this.f1491a.setScrollBarStyle(33554432);
            this.f1491a.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    Uri parse = Uri.parse(str);
                    String scheme = parse.getScheme();
                    String host = parse.getHost();
                    String path = parse.getPath();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Handling url load: ");
                    sb.append(str);
                    instance.getLogger().b("AppLovinWebViewActivity", sb.toString());
                    if (!"applovin".equalsIgnoreCase(scheme) || !"com.applovin.sdk".equalsIgnoreCase(host) || AppLovinWebViewActivity.this.b == null) {
                        return super.shouldOverrideUrlLoading(webView, str);
                    }
                    if (path.endsWith("webview_event")) {
                        Set queryParameterNames = parse.getQueryParameterNames();
                        String str2 = queryParameterNames.isEmpty() ? "" : (String) queryParameterNames.toArray()[0];
                        if (m.b(str2)) {
                            String queryParameter = parse.getQueryParameter(str2);
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Parsed WebView event parameter name: ");
                            sb2.append(str2);
                            sb2.append(" and value: ");
                            sb2.append(queryParameter);
                            instance.getLogger().b("AppLovinWebViewActivity", sb2.toString());
                            AppLovinWebViewActivity.this.b.onReceivedEvent(queryParameter);
                        } else {
                            instance.getLogger().e("AppLovinWebViewActivity", "Failed to parse WebView event parameter");
                        }
                    }
                    return true;
                }
            });
            if (getIntent().getBooleanExtra(INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON, false)) {
                getWindow().getDecorView().setSystemUiVisibility(5894);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("No SDK key specified");
    }
}
