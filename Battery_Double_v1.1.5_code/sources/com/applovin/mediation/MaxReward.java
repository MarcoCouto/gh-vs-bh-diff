package com.applovin.mediation;

public class MaxReward {
    public static final int DEFAULT_AMOUNT = 0;
    public static final String DEFAULT_LABEL = "";

    /* renamed from: a reason: collision with root package name */
    private final String f1482a;
    private final int b;

    private MaxReward(int i, String str) {
        if (i >= 0) {
            this.f1482a = str;
            this.b = i;
            return;
        }
        throw new IllegalArgumentException("Reward amount must be greater than or equal to 0");
    }

    public static MaxReward create(int i, String str) {
        return new MaxReward(i, str);
    }

    public static MaxReward createDefault() {
        return create(0, "");
    }

    public final int getAmount() {
        return this.b;
    }

    public final String getLabel() {
        return this.f1482a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MaxReward{amount=");
        sb.append(this.b);
        sb.append(", label='");
        sb.append(this.f1482a);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
