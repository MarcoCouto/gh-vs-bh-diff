package com.applovin.mediation.ads;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.ads.MaxFullscreenAdImpl;
import com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.a;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinSdk;
import java.lang.ref.WeakReference;

public class MaxInterstitialAd implements a {

    /* renamed from: a reason: collision with root package name */
    private static WeakReference<Activity> f1484a = new WeakReference<>(null);
    private final MaxFullscreenAdImpl b;

    public MaxInterstitialAd(String str, Activity activity) {
        this(str, AppLovinSdk.getInstance(activity), activity);
    }

    public MaxInterstitialAd(String str, AppLovinSdk appLovinSdk, Activity activity) {
        if (str == null) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Empty ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (appLovinSdk != null) {
            f1484a = new WeakReference<>(activity);
            MaxFullscreenAdImpl maxFullscreenAdImpl = new MaxFullscreenAdImpl(str, MaxAdFormat.INTERSTITIAL, this, "MaxInterstitialAd", p.a(appLovinSdk));
            this.b = maxFullscreenAdImpl;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public void destroy() {
        this.b.destroy();
    }

    public Activity getActivity() {
        return (Activity) f1484a.get();
    }

    public boolean isReady() {
        return this.b.isReady();
    }

    public void loadAd() {
        this.b.loadAd(getActivity());
    }

    public void setExtraParameter(String str, String str2) {
        this.b.setExtraParameter(str, str2);
    }

    public void setListener(MaxAdListener maxAdListener) {
        this.b.setListener(maxAdListener);
    }

    public void showAd() {
        showAd(null);
    }

    public void showAd(String str) {
        this.b.showAd(str, getActivity());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this.b);
        return sb.toString();
    }
}
