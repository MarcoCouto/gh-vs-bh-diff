package com.applovin.mediation.ads;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.ads.MaxFullscreenAdImpl;
import com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.a;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.sdk.AppLovinSdk;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class MaxRewardedAd implements a {

    /* renamed from: a reason: collision with root package name */
    private static final Map<String, MaxRewardedAd> f1485a = new HashMap();
    private static final Object b = new Object();
    private static WeakReference<Activity> c = new WeakReference<>(null);
    private final MaxFullscreenAdImpl d;

    private MaxRewardedAd(String str, AppLovinSdk appLovinSdk) {
        MaxFullscreenAdImpl maxFullscreenAdImpl = new MaxFullscreenAdImpl(str, MaxAdFormat.REWARDED, this, "MaxRewardedAd", p.a(appLovinSdk));
        this.d = maxFullscreenAdImpl;
    }

    public static MaxRewardedAd getInstance(String str, Activity activity) {
        return getInstance(str, AppLovinSdk.getInstance(activity), activity);
    }

    public static MaxRewardedAd getInstance(String str, AppLovinSdk appLovinSdk, Activity activity) {
        if (str == null) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Empty ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (appLovinSdk != null) {
            updateActivity(activity);
            synchronized (b) {
                MaxRewardedAd maxRewardedAd = (MaxRewardedAd) f1485a.get(str);
                if (maxRewardedAd != null) {
                    return maxRewardedAd;
                }
                MaxRewardedAd maxRewardedAd2 = new MaxRewardedAd(str, appLovinSdk);
                f1485a.put(str, maxRewardedAd2);
                return maxRewardedAd2;
            }
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static void updateActivity(Activity activity) {
        if (activity != null) {
            c = new WeakReference<>(activity);
        }
    }

    public void destroy() {
        synchronized (b) {
            f1485a.remove(this.d.getAdUnitId());
        }
        this.d.destroy();
    }

    public Activity getActivity() {
        return (Activity) c.get();
    }

    public boolean isReady() {
        return this.d.isReady();
    }

    public void loadAd() {
        this.d.loadAd(getActivity());
    }

    public void setExtrasParameter(String str, String str2) {
        this.d.setExtraParameter(str, str2);
    }

    public void setListener(MaxRewardedAdListener maxRewardedAdListener) {
        this.d.setListener(maxRewardedAdListener);
    }

    public void showAd() {
        showAd(null);
    }

    public void showAd(String str) {
        this.d.showAd(str, getActivity());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this.d);
        return sb.toString();
    }
}
