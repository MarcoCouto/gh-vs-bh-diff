package com.applovin.mediation;

import com.startapp.android.publish.common.model.AdPreferences;

public class MaxAdFormat {
    public static final MaxAdFormat BANNER = new MaxAdFormat(AdPreferences.TYPE_BANNER);
    public static final MaxAdFormat INTERSTITIAL = new MaxAdFormat("INTER");
    public static final MaxAdFormat LEADER = new MaxAdFormat("LEADER");
    public static final MaxAdFormat MREC = new MaxAdFormat("MREC");
    public static final MaxAdFormat NATIVE = new MaxAdFormat("NATIVE");
    public static final MaxAdFormat REWARDED = new MaxAdFormat("REWARDED");

    /* renamed from: a reason: collision with root package name */
    private final String f1481a;

    private MaxAdFormat(String str) {
        this.f1481a = str;
    }

    public String getLabel() {
        return this.f1481a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MaxAdFormat{label='");
        sb.append(this.f1481a);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
