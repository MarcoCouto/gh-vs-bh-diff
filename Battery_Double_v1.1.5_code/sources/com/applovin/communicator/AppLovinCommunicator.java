package com.applovin.communicator;

import android.content.Context;
import com.applovin.impl.communicator.MessagingServiceImpl;
import com.applovin.impl.communicator.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import java.util.Collections;
import java.util.List;

public final class AppLovinCommunicator {

    /* renamed from: a reason: collision with root package name */
    private static AppLovinCommunicator f1018a;
    private static final Object b = new Object();
    private i c;
    private final a d;
    private final MessagingServiceImpl e;

    private AppLovinCommunicator(Context context) {
        this.d = new a(context);
        this.e = new MessagingServiceImpl(context);
    }

    public static AppLovinCommunicator getInstance(Context context) {
        synchronized (b) {
            if (f1018a == null) {
                f1018a = new AppLovinCommunicator(context.getApplicationContext());
            }
        }
        return f1018a;
    }

    public void a(i iVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Attaching SDK instance: ");
        sb.append(iVar);
        sb.append("...");
        o.f("AppLovinCommunicator", sb.toString());
        this.c = iVar;
    }

    public AppLovinCommunicatorMessagingService getMessagingService() {
        return this.e;
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        subscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String str : list) {
            StringBuilder sb = new StringBuilder();
            sb.append("Subscribing ");
            sb.append(appLovinCommunicatorSubscriber);
            sb.append(" to topic: ");
            sb.append(str);
            o.f("AppLovinCommunicator", sb.toString());
            if (this.d.a(appLovinCommunicatorSubscriber, str)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Subscribed ");
                sb2.append(appLovinCommunicatorSubscriber);
                sb2.append(" to topic: ");
                sb2.append(str);
                o.f("AppLovinCommunicator", sb2.toString());
                this.e.maybeFlushStickyMessages(str);
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to subscribe ");
                sb3.append(appLovinCommunicatorSubscriber);
                sb3.append(" to topic: ");
                sb3.append(str);
                o.f("AppLovinCommunicator", sb3.toString());
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinCommunicator{sdk=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        unsubscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String str : list) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unsubscribing ");
            sb.append(appLovinCommunicatorSubscriber);
            sb.append(" from topic: ");
            sb.append(str);
            o.f("AppLovinCommunicator", sb.toString());
            this.d.b(appLovinCommunicatorSubscriber, str);
        }
    }
}
