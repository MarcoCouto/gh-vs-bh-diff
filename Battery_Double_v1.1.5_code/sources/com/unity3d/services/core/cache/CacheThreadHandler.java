package com.unity3d.services.core.cache;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import com.unity3d.services.core.api.Request;
import com.unity3d.services.core.device.Device;
import com.unity3d.services.core.log.DeviceLog;
import com.unity3d.services.core.request.IWebRequestProgressListener;
import com.unity3d.services.core.request.NetworkIOException;
import com.unity3d.services.core.request.WebRequest;
import com.unity3d.services.core.webview.WebViewApp;
import com.unity3d.services.core.webview.WebViewEventCategory;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class CacheThreadHandler extends Handler {
    private boolean _active = false;
    private boolean _canceled = false;
    private WebRequest _currentRequest = null;

    CacheThreadHandler() {
    }

    public void handleMessage(Message message) {
        HashMap hashMap;
        Bundle data = message.getData();
        String string = data.getString("source");
        data.remove("source");
        String string2 = data.getString("target");
        data.remove("target");
        int i = data.getInt("connectTimeout");
        data.remove("connectTimeout");
        int i2 = data.getInt("readTimeout");
        data.remove("readTimeout");
        int i3 = data.getInt("progressInterval");
        data.remove("progressInterval");
        boolean z = data.getBoolean("append", false);
        data.remove("append");
        if (data.size() > 0) {
            DeviceLog.debug("There are headers left in data, reading them");
            HashMap hashMap2 = new HashMap();
            for (String str : data.keySet()) {
                hashMap2.put(str, Arrays.asList(data.getStringArray(str)));
            }
            hashMap = hashMap2;
        } else {
            hashMap = null;
        }
        File file = new File(string2);
        if ((!z || file.exists()) && (z || !file.exists())) {
            if (message.what == 1) {
                downloadFile(string, string2, i, i2, i3, hashMap, z);
            }
            return;
        }
        this._active = false;
        WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_ERROR, CacheError.FILE_STATE_WRONG, string, string2, Boolean.valueOf(z), Boolean.valueOf(file.exists()));
    }

    public void setCancelStatus(boolean z) {
        this._canceled = z;
        if (z && this._currentRequest != null) {
            this._active = false;
            this._currentRequest.cancel();
        }
    }

    public boolean isActive() {
        return this._active;
    }

    /* JADX WARNING: type inference failed for: r6v0 */
    /* JADX WARNING: type inference failed for: r6v2 */
    /* JADX WARNING: type inference failed for: r6v3, types: [boolean] */
    /* JADX WARNING: type inference failed for: r6v4 */
    /* JADX WARNING: type inference failed for: r6v5, types: [boolean] */
    /* JADX WARNING: type inference failed for: r6v6 */
    /* JADX WARNING: type inference failed for: r6v7, types: [boolean] */
    /* JADX WARNING: type inference failed for: r6v8 */
    /* JADX WARNING: type inference failed for: r6v9, types: [boolean] */
    /* JADX WARNING: type inference failed for: r6v10 */
    /* JADX WARNING: type inference failed for: r6v11, types: [boolean] */
    /* JADX WARNING: type inference failed for: r6v12 */
    /* JADX WARNING: type inference failed for: r6v13 */
    /* JADX WARNING: type inference failed for: r6v14 */
    /* JADX WARNING: type inference failed for: r6v15 */
    /* JADX WARNING: type inference failed for: r6v16 */
    /* JADX WARNING: type inference failed for: r6v17 */
    /* JADX WARNING: type inference failed for: r6v18 */
    /* JADX WARNING: type inference failed for: r6v19 */
    /* JADX WARNING: type inference failed for: r6v20 */
    /* JADX WARNING: type inference failed for: r6v21 */
    /* JADX WARNING: type inference failed for: r6v22 */
    /* JADX WARNING: type inference failed for: r6v23 */
    /* JADX WARNING: type inference failed for: r6v24 */
    /* JADX WARNING: type inference failed for: r6v25 */
    /* JADX WARNING: type inference failed for: r6v26 */
    /* JADX WARNING: type inference failed for: r6v28 */
    /* JADX WARNING: type inference failed for: r6v29 */
    /* JADX WARNING: type inference failed for: r6v30 */
    /* JADX WARNING: type inference failed for: r6v31 */
    /* JADX WARNING: type inference failed for: r6v32 */
    /* JADX WARNING: type inference failed for: r6v33 */
    /* JADX WARNING: type inference failed for: r6v34 */
    /* JADX WARNING: type inference failed for: r6v35 */
    /* JADX WARNING: type inference failed for: r6v36 */
    /* JADX WARNING: type inference failed for: r6v37 */
    /* JADX WARNING: type inference failed for: r6v38 */
    /* JADX WARNING: type inference failed for: r6v39 */
    /* JADX WARNING: type inference failed for: r6v40 */
    /* JADX WARNING: type inference failed for: r6v41 */
    /* JADX WARNING: type inference failed for: r6v42 */
    /* JADX WARNING: type inference failed for: r6v43 */
    /* JADX WARNING: type inference failed for: r6v44 */
    /* JADX WARNING: type inference failed for: r6v45 */
    /* JADX WARNING: type inference failed for: r6v46 */
    /* JADX WARNING: type inference failed for: r6v47 */
    /* JADX WARNING: type inference failed for: r6v48 */
    /* JADX WARNING: type inference failed for: r6v49 */
    /* JADX WARNING: type inference failed for: r6v50 */
    /* JADX WARNING: type inference failed for: r6v51 */
    /* JADX WARNING: type inference failed for: r6v52 */
    /* JADX WARNING: type inference failed for: r6v53 */
    /* JADX WARNING: type inference failed for: r6v54 */
    /* JADX WARNING: type inference failed for: r6v55 */
    /* JADX WARNING: type inference failed for: r6v56 */
    /* JADX WARNING: type inference failed for: r6v57 */
    /* JADX WARNING: type inference failed for: r6v58 */
    /* JADX WARNING: type inference failed for: r6v59 */
    /* JADX WARNING: type inference failed for: r6v60 */
    /* JADX WARNING: type inference failed for: r6v61 */
    /* JADX WARNING: type inference failed for: r6v62 */
    /* JADX WARNING: type inference failed for: r6v63 */
    /* JADX WARNING: type inference failed for: r6v64 */
    /* JADX WARNING: type inference failed for: r6v65 */
    /* JADX WARNING: type inference failed for: r6v66 */
    /* JADX WARNING: type inference failed for: r6v67 */
    /* JADX WARNING: type inference failed for: r6v68 */
    /* JADX WARNING: type inference failed for: r6v69 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r6v2
  assigns: []
  uses: []
  mth insns count: 374
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x02d0 A[SYNTHETIC, Splitter:B:110:0x02d0] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x02fe A[SYNTHETIC, Splitter:B:119:0x02fe] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x018e A[SYNTHETIC, Splitter:B:66:0x018e] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01df A[SYNTHETIC, Splitter:B:77:0x01df] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0230 A[SYNTHETIC, Splitter:B:88:0x0230] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0281 A[SYNTHETIC, Splitter:B:99:0x0281] */
    /* JADX WARNING: Unknown variable types count: 27 */
    private void downloadFile(String str, String str2, int i, int i2, int i3, HashMap<String, List<String>> hashMap, boolean z) {
        char c;
        WebRequest webRequest;
        ? r6;
        Throwable th;
        ? r62;
        Object[] objArr;
        CacheEvent cacheEvent;
        WebViewEventCategory webViewEventCategory;
        WebViewApp webViewApp;
        ? r63;
        ? r64;
        ? r65;
        ? r66;
        ? r67;
        ? r68;
        FileOutputStream fileOutputStream;
        ? r69;
        ? r610;
        ? r611;
        ? r612;
        ? r613;
        ? r614;
        String str3 = str;
        String str4 = str2;
        boolean z2 = z;
        if (!this._canceled && str3 != null && str4 != null) {
            final File file = new File(str4);
            if (z2) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unity Ads cache: resuming download ");
                sb.append(str3);
                sb.append(" to ");
                sb.append(str4);
                sb.append(" at ");
                sb.append(file.length());
                sb.append(" bytes");
                DeviceLog.debug(sb.toString());
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unity Ads cache: start downloading ");
                sb2.append(str3);
                sb2.append(" to ");
                sb2.append(str4);
                DeviceLog.debug(sb2.toString());
            }
            if (!Device.isActiveNetworkConnected()) {
                DeviceLog.debug("Unity Ads cache: download cancelled, no internet connection available");
                WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_ERROR, CacheError.NO_INTERNET, str3);
                return;
            }
            this._active = true;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            FileOutputStream fileOutputStream2 = null;
            try {
                FileOutputStream fileOutputStream3 = new FileOutputStream(file, z2);
                try {
                    this._currentRequest = getWebRequest(str3, i, i2, hashMap);
                    final int i4 = i3;
                    this._currentRequest.setProgressListener(new IWebRequestProgressListener() {
                        private long lastProgressEventTime = System.currentTimeMillis();

                        public void onRequestStart(String str, long j, int i, Map<String, List<String>> map) {
                            WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_STARTED, str, Long.valueOf(file.length()), Long.valueOf(j + file.length()), Integer.valueOf(i), Request.getResponseHeadersMap(map));
                        }

                        public void onRequestProgress(String str, long j, long j2) {
                            if (i4 > 0 && System.currentTimeMillis() - this.lastProgressEventTime > ((long) i4)) {
                                this.lastProgressEventTime = System.currentTimeMillis();
                                WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_PROGRESS, str, Long.valueOf(j), Long.valueOf(j2));
                            }
                        }
                    });
                    long makeStreamRequest = this._currentRequest.makeStreamRequest(fileOutputStream3);
                    this._active = false;
                    long contentLength = this._currentRequest.getContentLength();
                    fileOutputStream = fileOutputStream3;
                    long j = contentLength;
                    webRequest = null;
                    c = 1;
                    try {
                        postProcessDownload(elapsedRealtime, str, file, makeStreamRequest, j, this._currentRequest.isCanceled(), this._currentRequest.getResponseCode(), this._currentRequest.getResponseHeaders());
                        this._currentRequest = null;
                        try {
                            fileOutputStream.close();
                        } catch (Exception e) {
                            Exception exc = e;
                            DeviceLog.exception("Error closing stream", exc);
                            webViewApp = WebViewApp.getCurrentApp();
                            webViewEventCategory = WebViewEventCategory.CACHE;
                            cacheEvent = CacheEvent.DOWNLOAD_ERROR;
                            objArr = new Object[]{CacheError.FILE_IO_ERROR, str3, exc.getMessage()};
                        }
                    } catch (FileNotFoundException e2) {
                        e = e2;
                        r69 = 0;
                        fileOutputStream2 = fileOutputStream;
                        r63 = r69;
                        r62 = r63;
                        DeviceLog.exception("Couldn't create target file", e);
                        this._active = r63;
                        WebViewApp currentApp = WebViewApp.getCurrentApp();
                        WebViewEventCategory webViewEventCategory2 = WebViewEventCategory.CACHE;
                        CacheEvent cacheEvent2 = CacheEvent.DOWNLOAD_ERROR;
                        Object[] objArr2 = new Object[3];
                        objArr2[r63] = CacheError.FILE_IO_ERROR;
                        objArr2[c] = str3;
                        objArr2[2] = e.getMessage();
                        currentApp.sendEvent(webViewEventCategory2, cacheEvent2, objArr2);
                        r62 = r63;
                        this._currentRequest = webRequest;
                        if (fileOutputStream2 != null) {
                        }
                    } catch (MalformedURLException e3) {
                        e = e3;
                        r610 = 0;
                        fileOutputStream2 = fileOutputStream;
                        r64 = r610;
                        r62 = r64;
                        DeviceLog.exception("Malformed URL", e);
                        this._active = r64;
                        WebViewApp currentApp2 = WebViewApp.getCurrentApp();
                        WebViewEventCategory webViewEventCategory3 = WebViewEventCategory.CACHE;
                        CacheEvent cacheEvent3 = CacheEvent.DOWNLOAD_ERROR;
                        Object[] objArr3 = new Object[3];
                        objArr3[r64] = CacheError.MALFORMED_URL;
                        objArr3[c] = str3;
                        objArr3[2] = e.getMessage();
                        currentApp2.sendEvent(webViewEventCategory3, cacheEvent3, objArr3);
                        r62 = r64;
                        this._currentRequest = webRequest;
                        if (fileOutputStream2 != null) {
                        }
                    } catch (IOException e4) {
                        e = e4;
                        r611 = 0;
                        fileOutputStream2 = fileOutputStream;
                        r65 = r611;
                        r62 = r65;
                        DeviceLog.exception("Couldn't request stream", e);
                        this._active = r65;
                        WebViewApp currentApp3 = WebViewApp.getCurrentApp();
                        WebViewEventCategory webViewEventCategory4 = WebViewEventCategory.CACHE;
                        CacheEvent cacheEvent4 = CacheEvent.DOWNLOAD_ERROR;
                        Object[] objArr4 = new Object[3];
                        objArr4[r65] = CacheError.FILE_IO_ERROR;
                        objArr4[c] = str3;
                        objArr4[2] = e.getMessage();
                        currentApp3.sendEvent(webViewEventCategory4, cacheEvent4, objArr4);
                        r62 = r65;
                        this._currentRequest = webRequest;
                        if (fileOutputStream2 != null) {
                        }
                    } catch (IllegalStateException e5) {
                        e = e5;
                        r612 = 0;
                        fileOutputStream2 = fileOutputStream;
                        r66 = r612;
                        r62 = r66;
                        DeviceLog.exception("Illegal state", e);
                        this._active = r66;
                        WebViewApp currentApp4 = WebViewApp.getCurrentApp();
                        WebViewEventCategory webViewEventCategory5 = WebViewEventCategory.CACHE;
                        CacheEvent cacheEvent5 = CacheEvent.DOWNLOAD_ERROR;
                        Object[] objArr5 = new Object[3];
                        objArr5[r66] = CacheError.ILLEGAL_STATE;
                        objArr5[c] = str3;
                        objArr5[2] = e.getMessage();
                        currentApp4.sendEvent(webViewEventCategory5, cacheEvent5, objArr5);
                        r62 = r66;
                        this._currentRequest = webRequest;
                        if (fileOutputStream2 != null) {
                        }
                    } catch (NetworkIOException e6) {
                        e = e6;
                        r613 = 0;
                        fileOutputStream2 = fileOutputStream;
                        r67 = r613;
                        try {
                            r62 = r67;
                            DeviceLog.exception("Network error", e);
                            this._active = r67;
                            WebViewApp currentApp5 = WebViewApp.getCurrentApp();
                            WebViewEventCategory webViewEventCategory6 = WebViewEventCategory.CACHE;
                            CacheEvent cacheEvent6 = CacheEvent.DOWNLOAD_ERROR;
                            Object[] objArr6 = new Object[3];
                            objArr6[r67] = CacheError.NETWORK_ERROR;
                            objArr6[c] = str3;
                            objArr6[2] = e.getMessage();
                            currentApp5.sendEvent(webViewEventCategory6, cacheEvent6, objArr6);
                            r62 = r67;
                            this._currentRequest = webRequest;
                            if (fileOutputStream2 != null) {
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            r68 = r62;
                            th = th;
                            r6 = r68;
                            this._currentRequest = webRequest;
                            if (fileOutputStream2 != null) {
                            }
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        r614 = 0;
                        th = th;
                        fileOutputStream2 = fileOutputStream;
                        r6 = r614;
                        this._currentRequest = webRequest;
                        if (fileOutputStream2 != null) {
                        }
                        throw th;
                    }
                } catch (FileNotFoundException e7) {
                    e = e7;
                    fileOutputStream = fileOutputStream3;
                    webRequest = null;
                    c = 1;
                    r69 = 0;
                    fileOutputStream2 = fileOutputStream;
                    r63 = r69;
                    r62 = r63;
                    DeviceLog.exception("Couldn't create target file", e);
                    this._active = r63;
                    WebViewApp currentApp6 = WebViewApp.getCurrentApp();
                    WebViewEventCategory webViewEventCategory22 = WebViewEventCategory.CACHE;
                    CacheEvent cacheEvent22 = CacheEvent.DOWNLOAD_ERROR;
                    Object[] objArr22 = new Object[3];
                    objArr22[r63] = CacheError.FILE_IO_ERROR;
                    objArr22[c] = str3;
                    objArr22[2] = e.getMessage();
                    currentApp6.sendEvent(webViewEventCategory22, cacheEvent22, objArr22);
                    r62 = r63;
                    this._currentRequest = webRequest;
                    if (fileOutputStream2 != null) {
                    }
                } catch (MalformedURLException e8) {
                    e = e8;
                    fileOutputStream = fileOutputStream3;
                    webRequest = null;
                    c = 1;
                    r610 = 0;
                    fileOutputStream2 = fileOutputStream;
                    r64 = r610;
                    r62 = r64;
                    DeviceLog.exception("Malformed URL", e);
                    this._active = r64;
                    WebViewApp currentApp22 = WebViewApp.getCurrentApp();
                    WebViewEventCategory webViewEventCategory32 = WebViewEventCategory.CACHE;
                    CacheEvent cacheEvent32 = CacheEvent.DOWNLOAD_ERROR;
                    Object[] objArr32 = new Object[3];
                    objArr32[r64] = CacheError.MALFORMED_URL;
                    objArr32[c] = str3;
                    objArr32[2] = e.getMessage();
                    currentApp22.sendEvent(webViewEventCategory32, cacheEvent32, objArr32);
                    r62 = r64;
                    this._currentRequest = webRequest;
                    if (fileOutputStream2 != null) {
                    }
                } catch (IOException e9) {
                    e = e9;
                    fileOutputStream = fileOutputStream3;
                    webRequest = null;
                    c = 1;
                    r611 = 0;
                    fileOutputStream2 = fileOutputStream;
                    r65 = r611;
                    r62 = r65;
                    DeviceLog.exception("Couldn't request stream", e);
                    this._active = r65;
                    WebViewApp currentApp32 = WebViewApp.getCurrentApp();
                    WebViewEventCategory webViewEventCategory42 = WebViewEventCategory.CACHE;
                    CacheEvent cacheEvent42 = CacheEvent.DOWNLOAD_ERROR;
                    Object[] objArr42 = new Object[3];
                    objArr42[r65] = CacheError.FILE_IO_ERROR;
                    objArr42[c] = str3;
                    objArr42[2] = e.getMessage();
                    currentApp32.sendEvent(webViewEventCategory42, cacheEvent42, objArr42);
                    r62 = r65;
                    this._currentRequest = webRequest;
                    if (fileOutputStream2 != null) {
                    }
                } catch (IllegalStateException e10) {
                    e = e10;
                    fileOutputStream = fileOutputStream3;
                    webRequest = null;
                    c = 1;
                    r612 = 0;
                    fileOutputStream2 = fileOutputStream;
                    r66 = r612;
                    r62 = r66;
                    DeviceLog.exception("Illegal state", e);
                    this._active = r66;
                    WebViewApp currentApp42 = WebViewApp.getCurrentApp();
                    WebViewEventCategory webViewEventCategory52 = WebViewEventCategory.CACHE;
                    CacheEvent cacheEvent52 = CacheEvent.DOWNLOAD_ERROR;
                    Object[] objArr52 = new Object[3];
                    objArr52[r66] = CacheError.ILLEGAL_STATE;
                    objArr52[c] = str3;
                    objArr52[2] = e.getMessage();
                    currentApp42.sendEvent(webViewEventCategory52, cacheEvent52, objArr52);
                    r62 = r66;
                    this._currentRequest = webRequest;
                    if (fileOutputStream2 != null) {
                    }
                } catch (NetworkIOException e11) {
                    e = e11;
                    fileOutputStream = fileOutputStream3;
                    webRequest = null;
                    c = 1;
                    r613 = 0;
                    fileOutputStream2 = fileOutputStream;
                    r67 = r613;
                    r62 = r67;
                    DeviceLog.exception("Network error", e);
                    this._active = r67;
                    WebViewApp currentApp52 = WebViewApp.getCurrentApp();
                    WebViewEventCategory webViewEventCategory62 = WebViewEventCategory.CACHE;
                    CacheEvent cacheEvent62 = CacheEvent.DOWNLOAD_ERROR;
                    Object[] objArr62 = new Object[3];
                    objArr62[r67] = CacheError.NETWORK_ERROR;
                    objArr62[c] = str3;
                    objArr62[2] = e.getMessage();
                    currentApp52.sendEvent(webViewEventCategory62, cacheEvent62, objArr62);
                    r62 = r67;
                    this._currentRequest = webRequest;
                    if (fileOutputStream2 != null) {
                    }
                } catch (Throwable th4) {
                    th = th4;
                    fileOutputStream = fileOutputStream3;
                    webRequest = null;
                    c = 1;
                    r614 = 0;
                    th = th;
                    fileOutputStream2 = fileOutputStream;
                    r6 = r614;
                    this._currentRequest = webRequest;
                    if (fileOutputStream2 != null) {
                    }
                    throw th;
                }
            } catch (FileNotFoundException e12) {
                e = e12;
                webRequest = null;
                c = 1;
                r63 = 0;
                r62 = r63;
                DeviceLog.exception("Couldn't create target file", e);
                this._active = r63;
                WebViewApp currentApp62 = WebViewApp.getCurrentApp();
                WebViewEventCategory webViewEventCategory222 = WebViewEventCategory.CACHE;
                CacheEvent cacheEvent222 = CacheEvent.DOWNLOAD_ERROR;
                Object[] objArr222 = new Object[3];
                objArr222[r63] = CacheError.FILE_IO_ERROR;
                objArr222[c] = str3;
                objArr222[2] = e.getMessage();
                currentApp62.sendEvent(webViewEventCategory222, cacheEvent222, objArr222);
                r62 = r63;
                this._currentRequest = webRequest;
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (Exception e13) {
                        Exception exc2 = e13;
                        DeviceLog.exception("Error closing stream", exc2);
                        webViewApp = WebViewApp.getCurrentApp();
                        webViewEventCategory = WebViewEventCategory.CACHE;
                        cacheEvent = CacheEvent.DOWNLOAD_ERROR;
                        objArr = new Object[3];
                        objArr[r63] = CacheError.FILE_IO_ERROR;
                        objArr[c] = str3;
                        objArr[2] = exc2.getMessage();
                    }
                }
            } catch (MalformedURLException e14) {
                e = e14;
                webRequest = null;
                c = 1;
                r64 = 0;
                r62 = r64;
                DeviceLog.exception("Malformed URL", e);
                this._active = r64;
                WebViewApp currentApp222 = WebViewApp.getCurrentApp();
                WebViewEventCategory webViewEventCategory322 = WebViewEventCategory.CACHE;
                CacheEvent cacheEvent322 = CacheEvent.DOWNLOAD_ERROR;
                Object[] objArr322 = new Object[3];
                objArr322[r64] = CacheError.MALFORMED_URL;
                objArr322[c] = str3;
                objArr322[2] = e.getMessage();
                currentApp222.sendEvent(webViewEventCategory322, cacheEvent322, objArr322);
                r62 = r64;
                this._currentRequest = webRequest;
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (Exception e15) {
                        Exception exc3 = e15;
                        DeviceLog.exception("Error closing stream", exc3);
                        webViewApp = WebViewApp.getCurrentApp();
                        webViewEventCategory = WebViewEventCategory.CACHE;
                        cacheEvent = CacheEvent.DOWNLOAD_ERROR;
                        objArr = new Object[3];
                        objArr[r64] = CacheError.FILE_IO_ERROR;
                        objArr[c] = str3;
                        objArr[2] = exc3.getMessage();
                    }
                }
            } catch (IOException e16) {
                e = e16;
                webRequest = null;
                c = 1;
                r65 = 0;
                r62 = r65;
                DeviceLog.exception("Couldn't request stream", e);
                this._active = r65;
                WebViewApp currentApp322 = WebViewApp.getCurrentApp();
                WebViewEventCategory webViewEventCategory422 = WebViewEventCategory.CACHE;
                CacheEvent cacheEvent422 = CacheEvent.DOWNLOAD_ERROR;
                Object[] objArr422 = new Object[3];
                objArr422[r65] = CacheError.FILE_IO_ERROR;
                objArr422[c] = str3;
                objArr422[2] = e.getMessage();
                currentApp322.sendEvent(webViewEventCategory422, cacheEvent422, objArr422);
                r62 = r65;
                this._currentRequest = webRequest;
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (Exception e17) {
                        Exception exc4 = e17;
                        DeviceLog.exception("Error closing stream", exc4);
                        webViewApp = WebViewApp.getCurrentApp();
                        webViewEventCategory = WebViewEventCategory.CACHE;
                        cacheEvent = CacheEvent.DOWNLOAD_ERROR;
                        objArr = new Object[3];
                        objArr[r65] = CacheError.FILE_IO_ERROR;
                        objArr[c] = str3;
                        objArr[2] = exc4.getMessage();
                    }
                }
            } catch (IllegalStateException e18) {
                e = e18;
                webRequest = null;
                c = 1;
                r66 = 0;
                r62 = r66;
                DeviceLog.exception("Illegal state", e);
                this._active = r66;
                WebViewApp currentApp422 = WebViewApp.getCurrentApp();
                WebViewEventCategory webViewEventCategory522 = WebViewEventCategory.CACHE;
                CacheEvent cacheEvent522 = CacheEvent.DOWNLOAD_ERROR;
                Object[] objArr522 = new Object[3];
                objArr522[r66] = CacheError.ILLEGAL_STATE;
                objArr522[c] = str3;
                objArr522[2] = e.getMessage();
                currentApp422.sendEvent(webViewEventCategory522, cacheEvent522, objArr522);
                r62 = r66;
                this._currentRequest = webRequest;
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (Exception e19) {
                        Exception exc5 = e19;
                        DeviceLog.exception("Error closing stream", exc5);
                        webViewApp = WebViewApp.getCurrentApp();
                        webViewEventCategory = WebViewEventCategory.CACHE;
                        cacheEvent = CacheEvent.DOWNLOAD_ERROR;
                        objArr = new Object[3];
                        objArr[r66] = CacheError.FILE_IO_ERROR;
                        objArr[c] = str3;
                        objArr[2] = exc5.getMessage();
                    }
                }
            } catch (NetworkIOException e20) {
                e = e20;
                webRequest = null;
                c = 1;
                r67 = 0;
                r62 = r67;
                DeviceLog.exception("Network error", e);
                this._active = r67;
                WebViewApp currentApp522 = WebViewApp.getCurrentApp();
                WebViewEventCategory webViewEventCategory622 = WebViewEventCategory.CACHE;
                CacheEvent cacheEvent622 = CacheEvent.DOWNLOAD_ERROR;
                Object[] objArr622 = new Object[3];
                objArr622[r67] = CacheError.NETWORK_ERROR;
                objArr622[c] = str3;
                objArr622[2] = e.getMessage();
                currentApp522.sendEvent(webViewEventCategory622, cacheEvent622, objArr622);
                r62 = r67;
                this._currentRequest = webRequest;
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (Exception e21) {
                        Exception exc6 = e21;
                        DeviceLog.exception("Error closing stream", exc6);
                        webViewApp = WebViewApp.getCurrentApp();
                        webViewEventCategory = WebViewEventCategory.CACHE;
                        cacheEvent = CacheEvent.DOWNLOAD_ERROR;
                        objArr = new Object[3];
                        objArr[r67] = CacheError.FILE_IO_ERROR;
                        objArr[c] = str3;
                        objArr[2] = exc6.getMessage();
                    }
                }
            } catch (Throwable th5) {
                th = th5;
                webRequest = null;
                c = 1;
                r68 = 0;
                th = th;
                r6 = r68;
                this._currentRequest = webRequest;
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (Exception e22) {
                        Exception exc7 = e22;
                        DeviceLog.exception("Error closing stream", exc7);
                        WebViewApp currentApp7 = WebViewApp.getCurrentApp();
                        WebViewEventCategory webViewEventCategory7 = WebViewEventCategory.CACHE;
                        CacheEvent cacheEvent7 = CacheEvent.DOWNLOAD_ERROR;
                        Object[] objArr7 = new Object[3];
                        objArr7[r6] = CacheError.FILE_IO_ERROR;
                        objArr7[c] = str3;
                        objArr7[2] = exc7.getMessage();
                        currentApp7.sendEvent(webViewEventCategory7, cacheEvent7, objArr7);
                    }
                }
                throw th;
            }
        }
        return;
        webViewApp.sendEvent(webViewEventCategory, cacheEvent, objArr);
    }

    private void postProcessDownload(long j, String str, File file, long j2, long j3, boolean z, int i, Map<String, List<String>> map) {
        String str2 = str;
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        if (!file.setReadable(true, false)) {
            DeviceLog.debug("Unity Ads cache: could not set file readable!");
        }
        if (!z) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unity Ads cache: File ");
            sb.append(file.getName());
            sb.append(" of ");
            sb.append(j2);
            sb.append(" bytes downloaded in ");
            sb.append(elapsedRealtime);
            sb.append("ms");
            DeviceLog.debug(sb.toString());
            WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_END, str2, Long.valueOf(j2), Long.valueOf(j3), Long.valueOf(elapsedRealtime), Integer.valueOf(i), Request.getResponseHeadersMap(map));
            return;
        }
        long j4 = j2;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Unity Ads cache: downloading of ");
        sb2.append(str2);
        sb2.append(" stopped");
        DeviceLog.debug(sb2.toString());
        WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_STOPPED, str2, Long.valueOf(j2), Long.valueOf(j3), Long.valueOf(elapsedRealtime), Integer.valueOf(i), Request.getResponseHeadersMap(map));
    }

    private WebRequest getWebRequest(String str, int i, int i2, HashMap<String, List<String>> hashMap) throws MalformedURLException {
        HashMap hashMap2 = new HashMap();
        if (hashMap != null) {
            hashMap2.putAll(hashMap);
        }
        WebRequest webRequest = new WebRequest(str, HttpRequest.METHOD_GET, hashMap2, i, i2);
        return webRequest;
    }
}
