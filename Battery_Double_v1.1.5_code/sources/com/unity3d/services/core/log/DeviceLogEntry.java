package com.unity3d.services.core.log;

class DeviceLogEntry {
    private DeviceLogLevel _logLevel = null;
    private String _originalMessage = null;
    private StackTraceElement _stackTraceElement = null;

    public DeviceLogEntry(DeviceLogLevel deviceLogLevel, String str, StackTraceElement stackTraceElement) {
        this._logLevel = deviceLogLevel;
        this._originalMessage = str;
        this._stackTraceElement = stackTraceElement;
    }

    public DeviceLogLevel getLogLevel() {
        return this._logLevel;
    }

    public String getParsedMessage() {
        int i;
        String str = this._originalMessage;
        String str2 = "UnknownClass";
        String str3 = "unknownMethod";
        if (this._stackTraceElement != null) {
            str2 = this._stackTraceElement.getClassName();
            str3 = this._stackTraceElement.getMethodName();
            i = this._stackTraceElement.getLineNumber();
        } else {
            i = -1;
        }
        if (str != null && !str.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(" :: ");
            sb.append(str);
            str = sb.toString();
        }
        if (str == null) {
            str = "";
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(" (line:");
        sb2.append(i);
        sb2.append(")");
        String sb3 = sb2.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append(str2);
        sb4.append(".");
        sb4.append(str3);
        sb4.append("()");
        sb4.append(sb3);
        sb4.append(str);
        return sb4.toString();
    }
}
