package com.unity3d.services.banners.view;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.unity3d.services.ads.webplayer.WebPlayer;
import com.unity3d.services.core.log.DeviceLog;
import com.unity3d.services.core.misc.ViewUtilities;
import com.unity3d.services.core.properties.ClientProperties;
import com.unity3d.services.core.webview.WebViewApp;
import com.unity3d.services.core.webview.WebViewEventCategory;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class BannerView extends RelativeLayout {
    private static final String VIEW_BANNER = "banner";
    private static final String VIEW_BANNER_PLAYER = "bannerplayer";
    private static final String VIEW_WEB_VIEW = "webview";
    private static BannerView _instance;
    private static JSONObject _webPlayerEventSettings = new JSONObject();
    private static JSONObject _webPlayerSettings = new JSONObject();
    private static JSONObject _webSettings = new JSONObject();
    private int _lastVisibility = -1;
    private List<String> _views;
    private WebPlayer _webPlayer;
    private int height;
    private BannerPosition position;
    private int width;

    public BannerView(Context context) {
        super(context);
        this._webPlayer = new WebPlayer(context, VIEW_BANNER_PLAYER, _webSettings, _webPlayerSettings);
        this._webPlayer.setEventSettings(_webPlayerEventSettings);
        if (VERSION.SDK_INT >= 11) {
            addOnLayoutChangeListener(new OnLayoutChangeListener() {
                public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    BannerView.this.onLayoutChange(view, i, i2, i3, i4, i5, i6, i7, i8);
                }
            });
        }
    }

    public static void setWebPlayerEventSettings(JSONObject jSONObject) {
        _webPlayerEventSettings = jSONObject;
    }

    public static void setWebPlayerSettings(JSONObject jSONObject, JSONObject jSONObject2) {
        _webSettings = jSONObject;
        _webPlayerSettings = jSONObject2;
    }

    public void destroy() {
        removeAllViews();
        ViewParent parent = getParent();
        if (parent != null && (parent instanceof ViewGroup)) {
            ((ViewGroup) parent).removeView(this);
        }
        if (this._webPlayer != null) {
            this._webPlayer.destroy();
        }
        this._webPlayer = null;
        _instance = null;
    }

    public static BannerView getOrCreateInstance() {
        if (_instance == null) {
            _instance = new BannerView(ClientProperties.getApplicationContext());
        }
        return _instance;
    }

    public static BannerView getInstance() {
        return _instance;
    }

    public void setViews(List<String> list) {
        ArrayList<String> arrayList = new ArrayList<>(list);
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (this._views != null) {
            arrayList2.addAll(this._views);
            arrayList2.removeAll(list);
            arrayList.removeAll(this._views);
        }
        this._views = list;
        for (String removeView : arrayList2) {
            removeView(removeView);
        }
        for (String addView : arrayList) {
            addView(addView);
        }
    }

    private void removeView(String str) {
        View viewForName = getViewForName(str);
        if (viewForName != null) {
            ViewUtilities.removeViewFromParent(viewForName);
        }
        char c = 65535;
        if (str.hashCode() == 1497041165 && str.equals(VIEW_BANNER_PLAYER)) {
            c = 0;
        }
        if (c == 0) {
            this._webPlayer = null;
        }
    }

    private void addView(String str) {
        View viewForName = getViewForName(str);
        if (viewForName != null) {
            addView(viewForName, new LayoutParams(-1, -1));
            return;
        }
        DeviceLog.warning("No view defined for viewName: %s", str);
    }

    public WebPlayer getWebPlayer() {
        return this._webPlayer;
    }

    public void setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams != null) {
            layoutParams.width = this.width;
            layoutParams.height = this.height;
            super.setLayoutParams(LayoutParamsHelper.updateLayoutParamsForPosition(layoutParams, this.position));
        }
    }

    public void setViewFrame(String str, Integer num, Integer num2, Integer num3, Integer num4) {
        View viewForName = getViewForName(str);
        if (viewForName != null) {
            if (viewForName == this) {
                DeviceLog.warning("Not setting viewFrame for banner, use `setLayoutParams` instead.");
            } else {
                LayoutParams layoutParams = new LayoutParams(num3.intValue(), num4.intValue());
                layoutParams.setMargins(num.intValue(), num2.intValue(), 0, 0);
                viewForName.setLayoutParams(layoutParams);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        WebViewApp currentApp = WebViewApp.getCurrentApp();
        if (currentApp != null) {
            currentApp.sendEvent(WebViewEventCategory.BANNER, BannerEvent.BANNER_ATTACHED, new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        WebViewApp currentApp = WebViewApp.getCurrentApp();
        if (currentApp != null) {
            currentApp.sendEvent(WebViewEventCategory.BANNER, BannerEvent.BANNER_DETACHED, new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        if (i3 != 0 || i4 != 0) {
            int left = getLeft();
            int right = getRight();
            float f = 1.0f;
            if (VERSION.SDK_INT >= 11) {
                f = getAlpha();
            }
            WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.BANNER, BannerEvent.BANNER_RESIZED, Integer.valueOf(left), Integer.valueOf(right), Integer.valueOf(i), Integer.valueOf(i2), Float.valueOf(f));
            Rect rect = new Rect();
            getHitRect(rect);
            if (((View) getParent()).getLocalVisibleRect(rect)) {
                onVisibilityChanged(this, 8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        if (view != this) {
            return;
        }
        if (this._lastVisibility == -1) {
            this._lastVisibility = i;
            return;
        }
        if (i != 0 && this._lastVisibility == 0) {
            WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.BANNER, BannerEvent.BANNER_VISIBILITY_CHANGED, Integer.valueOf(i));
        }
        this._lastVisibility = i;
    }

    private View getViewForName(String str) {
        if (str.equals(VIEW_BANNER_PLAYER)) {
            return this._webPlayer;
        }
        if (str.equals("webview")) {
            return WebViewApp.getCurrentApp().getWebView();
        }
        if (str.equals(VIEW_BANNER)) {
            return this;
        }
        return null;
    }

    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.BANNER, BannerEvent.BANNER_RESIZED, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Float.valueOf(VERSION.SDK_INT >= 11 ? getAlpha() : 1.0f));
        if (getParent() != null) {
            Rect rect = new Rect();
            getHitRect(rect);
            if ((getParent() instanceof View) && !((View) getParent()).getLocalVisibleRect(rect)) {
                onVisibilityChanged(this, 8);
            }
        }
    }

    public void setAlpha(float f) {
        super.setAlpha(f);
        onLayoutChange(this, getLeft(), getTop(), getRight(), getBottom(), getLeft(), getTop(), getRight(), getBottom());
    }

    public void setBannerDimensions(int i, int i2, BannerPosition bannerPosition) {
        this.width = i;
        this.height = i2;
        this.position = bannerPosition;
    }
}
