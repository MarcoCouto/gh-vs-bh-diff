package com.unity3d.services.banners.api;

import android.view.ViewGroup;
import android.view.ViewParent;
import com.unity3d.services.banners.properties.BannerProperties;
import com.unity3d.services.banners.view.BannerEvent;
import com.unity3d.services.banners.view.BannerPosition;
import com.unity3d.services.banners.view.BannerView;
import com.unity3d.services.core.log.DeviceLog;
import com.unity3d.services.core.misc.Utilities;
import com.unity3d.services.core.properties.ClientProperties;
import com.unity3d.services.core.webview.WebViewApp;
import com.unity3d.services.core.webview.WebViewEventCategory;
import com.unity3d.services.core.webview.bridge.WebViewCallback;
import com.unity3d.services.core.webview.bridge.WebViewExposed;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class Banner {
    @WebViewExposed
    public static void load(final JSONArray jSONArray, final String str, final Integer num, final Integer num2, WebViewCallback webViewCallback) {
        Utilities.runOnUiThread(new Runnable() {
            public synchronized void run() {
                BannerView orCreateInstance = BannerView.getOrCreateInstance();
                BannerPosition bannerPosition = ClientProperties.getbannerDefaultPosition();
                if (bannerPosition == null) {
                    bannerPosition = BannerPosition.fromString(str);
                }
                orCreateInstance.setBannerDimensions(num.intValue(), num2.intValue(), bannerPosition);
                orCreateInstance.setViews(Banner.getArrayFromJSONArray(jSONArray));
                WebViewApp currentApp = WebViewApp.getCurrentApp();
                if (currentApp != null) {
                    currentApp.sendEvent(WebViewEventCategory.BANNER, BannerEvent.BANNER_LOADED, new Object[0]);
                }
            }
        });
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void destroy(WebViewCallback webViewCallback) {
        Utilities.runOnUiThread(new Runnable() {
            public synchronized void run() {
                BannerView instance = BannerView.getInstance();
                if (instance != null) {
                    instance.destroy();
                    if (BannerProperties.getBannerParent() != null) {
                        ViewParent parent = BannerProperties.getBannerParent().getParent();
                        if (parent != null && (parent instanceof ViewGroup)) {
                            ((ViewGroup) parent).removeView(BannerProperties.getBannerParent());
                        }
                    }
                    BannerProperties.setBannerParent(null);
                    WebViewApp currentApp = WebViewApp.getCurrentApp();
                    if (currentApp != null) {
                        currentApp.sendEvent(WebViewEventCategory.BANNER, BannerEvent.BANNER_DESTROYED, new Object[0]);
                    }
                }
            }
        });
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void setViewFrame(String str, Integer num, Integer num2, Integer num3, Integer num4, WebViewCallback webViewCallback) {
        final String str2 = str;
        final Integer num5 = num;
        final Integer num6 = num2;
        final Integer num7 = num3;
        final Integer num8 = num4;
        AnonymousClass3 r0 = new Runnable() {
            public void run() {
                BannerView instance = BannerView.getInstance();
                if (instance != null) {
                    instance.setViewFrame(str2, num5, num6, num7, num8);
                }
            }
        };
        Utilities.runOnUiThread(r0);
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void setViews(final JSONArray jSONArray, WebViewCallback webViewCallback) {
        Utilities.runOnUiThread(new Runnable() {
            public void run() {
                BannerView instance = BannerView.getInstance();
                if (instance != null) {
                    instance.setViews(Banner.getArrayFromJSONArray(jSONArray));
                }
            }
        });
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void setBannerFrame(final String str, final Integer num, final Integer num2, WebViewCallback webViewCallback) {
        Utilities.runOnUiThread(new Runnable() {
            public void run() {
                BannerView instance = BannerView.getInstance();
                if (instance != null) {
                    BannerPosition bannerPosition = ClientProperties.getbannerDefaultPosition();
                    if (bannerPosition == null) {
                        bannerPosition = BannerPosition.fromString(str);
                    }
                    instance.setBannerDimensions(num.intValue(), num2.intValue(), bannerPosition);
                    instance.setLayoutParams(instance.getLayoutParams());
                }
            }
        });
        webViewCallback.invoke(new Object[0]);
    }

    /* access modifiers changed from: private */
    public static List<String> getArrayFromJSONArray(JSONArray jSONArray) {
        String[] strArr = new String[jSONArray.length()];
        for (int i = 0; i < strArr.length; i++) {
            try {
                strArr[i] = jSONArray.getString(i);
            } catch (JSONException e) {
                DeviceLog.warning("Exception converting JSON Array to String Array: %s", e);
            }
        }
        return Arrays.asList(strArr);
    }
}
