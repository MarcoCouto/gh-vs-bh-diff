package com.unity3d.services.ads.adunit;

import android.os.Bundle;
import android.view.View;
import com.unity3d.services.ads.webplayer.WebPlayer;
import com.unity3d.services.core.misc.ViewUtilities;

public class WebPlayerHandler implements IAdUnitViewHandler {
    private WebPlayer _webPlayer;

    public void onPause(AdUnitActivity adUnitActivity) {
    }

    public void onResume(AdUnitActivity adUnitActivity) {
    }

    public void onStart(AdUnitActivity adUnitActivity) {
    }

    public void onStop(AdUnitActivity adUnitActivity) {
    }

    public boolean create(AdUnitActivity adUnitActivity) {
        if (this._webPlayer == null) {
            this._webPlayer = new WebPlayer(adUnitActivity, "webplayer", com.unity3d.services.ads.api.WebPlayer.getWebSettings(), com.unity3d.services.ads.api.WebPlayer.getWebPlayerSettings());
            this._webPlayer.setEventSettings(com.unity3d.services.ads.api.WebPlayer.getWebPlayerEventSettings());
        }
        return true;
    }

    public boolean destroy() {
        if (this._webPlayer != null) {
            ViewUtilities.removeViewFromParent(this._webPlayer);
            this._webPlayer.destroy();
        }
        this._webPlayer = null;
        return true;
    }

    public View getView() {
        return this._webPlayer;
    }

    public void onCreate(AdUnitActivity adUnitActivity, Bundle bundle) {
        create(adUnitActivity);
    }

    public void onDestroy(AdUnitActivity adUnitActivity) {
        if (adUnitActivity.isFinishing()) {
            destroy();
        }
    }
}
