package com.unity3d.services.ads.api;

import android.view.View;
import com.unity3d.services.ads.adunit.IAdUnitViewHandler;
import com.unity3d.services.ads.webplayer.WebPlayerError;
import com.unity3d.services.banners.view.BannerView;
import com.unity3d.services.core.log.DeviceLog;
import com.unity3d.services.core.misc.Utilities;
import com.unity3d.services.core.webview.bridge.WebViewCallback;
import com.unity3d.services.core.webview.bridge.WebViewExposed;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebPlayer {
    private static JSONObject _webPlayerEventSettings;
    private static JSONObject _webPlayerSettings;
    private static JSONObject _webSettings;

    public static JSONObject getWebPlayerSettings() {
        return _webPlayerSettings;
    }

    public static JSONObject getWebSettings() {
        return _webSettings;
    }

    public static JSONObject getWebPlayerEventSettings() {
        return _webPlayerEventSettings;
    }

    @WebViewExposed
    public static void setUrl(final String str, String str2, WebViewCallback webViewCallback) {
        final com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str2);
        if (webPlayer != null) {
            Utilities.runOnUiThread(new Runnable() {
                public void run() {
                    webPlayer.loadUrl(str);
                }
            });
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    @WebViewExposed
    public static void setData(final String str, final String str2, final String str3, String str4, WebViewCallback webViewCallback) {
        final com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str4);
        if (webPlayer != null) {
            Utilities.runOnUiThread(new Runnable() {
                public void run() {
                    webPlayer.loadData(str, str2, str3);
                }
            });
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    @WebViewExposed
    public static void setDataWithUrl(String str, String str2, String str3, String str4, String str5, WebViewCallback webViewCallback) {
        final com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str5);
        if (webPlayer != null) {
            final String str6 = str;
            final String str7 = str2;
            final String str8 = str3;
            final String str9 = str4;
            AnonymousClass3 r0 = new Runnable() {
                public void run() {
                    webPlayer.loadDataWithBaseURL(str6, str7, str8, str9, null);
                }
            };
            Utilities.runOnUiThread(r0);
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002e  */
    @WebViewExposed
    public static void setSettings(final JSONObject jSONObject, final JSONObject jSONObject2, String str, WebViewCallback webViewCallback) {
        char c;
        int hashCode = str.hashCode();
        if (hashCode != -318269643) {
            if (hashCode == 1497041165 && str.equals("bannerplayer")) {
                c = 0;
                switch (c) {
                    case 0:
                        Utilities.runOnUiThread(new Runnable() {
                            public void run() {
                                com.unity3d.services.ads.webplayer.WebPlayer access$000 = WebPlayer.getBannerWebPlayer();
                                if (access$000 != null) {
                                    access$000.setSettings(jSONObject, jSONObject2);
                                } else {
                                    BannerView.setWebPlayerSettings(jSONObject, jSONObject2);
                                }
                            }
                        });
                        break;
                    case 1:
                        _webSettings = jSONObject;
                        _webPlayerSettings = jSONObject2;
                        break;
                }
                webViewCallback.invoke(new Object[0]);
            }
        } else if (str.equals("webplayer")) {
            c = 1;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
            }
            webViewCallback.invoke(new Object[0]);
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
        }
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void setEventSettings(final JSONObject jSONObject, String str, WebViewCallback webViewCallback) {
        if (str.equals("webplayer")) {
            _webPlayerEventSettings = jSONObject;
        } else {
            Utilities.runOnUiThread(new Runnable() {
                public void run() {
                    com.unity3d.services.ads.webplayer.WebPlayer access$000 = WebPlayer.getBannerWebPlayer();
                    if (access$000 != null) {
                        access$000.setEventSettings(jSONObject);
                    } else {
                        BannerView.setWebPlayerEventSettings(jSONObject);
                    }
                }
            });
        }
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void clearSettings(WebViewCallback webViewCallback) {
        _webSettings = null;
        _webPlayerSettings = null;
        _webPlayerEventSettings = null;
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void getErroredSettings(String str, WebViewCallback webViewCallback) {
        com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str);
        if (webPlayer != null) {
            Map erroredSettings = webPlayer.getErroredSettings();
            JSONObject jSONObject = new JSONObject();
            try {
                for (Entry entry : erroredSettings.entrySet()) {
                    jSONObject.put((String) entry.getKey(), entry.getValue());
                }
            } catch (Exception e) {
                DeviceLog.exception("Error forming JSON object", e);
            }
            webViewCallback.invoke(jSONObject);
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    @WebViewExposed
    public static void sendEvent(JSONArray jSONArray, String str, WebViewCallback webViewCallback) {
        com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str);
        if (webPlayer != null) {
            webPlayer.sendEvent(jSONArray);
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002e  */
    private static com.unity3d.services.ads.webplayer.WebPlayer getWebPlayer(String str) {
        char c;
        int hashCode = str.hashCode();
        if (hashCode == -318269643) {
            if (str.equals("webplayer")) {
                c = 0;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }
        } else if (hashCode == 1497041165 && str.equals("bannerplayer")) {
            c = 1;
            switch (c) {
                case 0:
                    return getAdUnitWebPlayer();
                case 1:
                    return getBannerWebPlayer();
                default:
                    return null;
            }
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
        }
    }

    private static com.unity3d.services.ads.webplayer.WebPlayer getAdUnitWebPlayer() {
        if (AdUnit.getAdUnitActivity() != null) {
            IAdUnitViewHandler viewHandler = AdUnit.getAdUnitActivity().getViewHandler("webplayer");
            if (viewHandler != null) {
                View view = viewHandler.getView();
                if (view != null) {
                    return (com.unity3d.services.ads.webplayer.WebPlayer) view;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static com.unity3d.services.ads.webplayer.WebPlayer getBannerWebPlayer() {
        if (BannerView.getInstance() == null) {
            return null;
        }
        return BannerView.getInstance().getWebPlayer();
    }
}
