package com.appodealx.vast;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.explorestack.iab.utils.Logger.LogLevel;
import com.explorestack.iab.vast.VastLog;
import com.explorestack.iab.vast.VideoType;
import com.mintegral.msdk.MIntegralConstans;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Vast extends InternalAdapterInterface {
    public void setLogging(boolean z) {
        if (z) {
            VastLog.setLoggingLevel(LogLevel.debug);
        } else {
            VastLog.setLoggingLevel(LogLevel.none);
        }
    }

    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    public void loadInterstitial(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        loadFullScreenAd(activity, jSONObject, fullScreenAd, fullScreenAdListener, VideoType.NonRewarded);
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    public void loadRewardedVideo(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        loadFullScreenAd(activity, jSONObject, fullScreenAd, fullScreenAdListener, VideoType.Rewarded);
    }

    private void loadFullScreenAd(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener, VideoType videoType) {
        VastInterstitial vastInterstitial = new VastInterstitial(jSONObject.optString("creative"), jSONObject.optInt("close_time"), jSONObject.optBoolean("autoclose"), videoType, fullScreenAdListener);
        vastInterstitial.load(activity);
        fullScreenAd.setAd(vastInterstitial);
    }

    @Nullable
    private JSONObject getRequestInfoFromSettings(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, "vast");
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, MIntegralConstans.NATIVE_VIDEO_VERSION);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", jSONObject.getString("id"));
            jSONObject3.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM));
            jSONObject2.put("appodeal", jSONObject3);
            return jSONObject2;
        } catch (JSONException e) {
            Log.e("Appodealx-Vast", e.getMessage());
            return null;
        }
    }
}
