package com.appodealx.vast;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.VideoType;

public class VastInterstitial extends FullScreenAdObject {
    private boolean autoClose;
    private int closeTime;
    private String creative;
    private FullScreenAdListener listener;
    private VideoType type;
    @VisibleForTesting
    VastInterstitialListener vastListener;
    @VisibleForTesting
    VastRequest vastRequest;

    VastInterstitial(String str, int i, boolean z, VideoType videoType, @NonNull FullScreenAdListener fullScreenAdListener) {
        this.creative = str;
        this.closeTime = i;
        this.autoClose = z;
        this.type = videoType;
        this.listener = fullScreenAdListener;
    }

    /* access modifiers changed from: 0000 */
    public void load(@NonNull Activity activity) {
        this.vastRequest = VastRequest.newBuilder().setPreCache(true).setCloseTime(this.closeTime).setAutoClose(this.autoClose).build();
        this.vastListener = new VastInterstitialListener(this, this.listener);
        this.vastRequest.loadVideoWithData(activity, this.creative, this.vastListener);
    }

    public void show(@NonNull Activity activity) {
        if (this.vastRequest.checkFile()) {
            this.vastRequest.display(activity, this.type, this.vastListener);
        } else {
            this.listener.onFullScreenAdFailedToShow(AdError.InternalError);
        }
    }

    public void destroy() {
        if (this.vastRequest != null) {
            this.vastRequest = null;
        }
    }
}
