package com.appodealx.adcolony;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.adcolony.sdk.AdColonyInterstitial;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import org.json.JSONException;
import org.json.JSONObject;

final class AdColonyFullscreenAd extends FullScreenAdObject {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    @Nullable
    private AdColonyInterstitial adColonyInterstitial;
    @NonNull
    private FullScreenAdListener callback;
    private boolean isRewarded;
    @Nullable
    private AdColonyFullscreenAdListener listener;

    AdColonyFullscreenAd(@NonNull FullScreenAdListener fullScreenAdListener, boolean z) {
        this.callback = fullScreenAdListener;
        this.isRewarded = z;
    }

    /* access modifiers changed from: 0000 */
    public void load(@NonNull JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("zone_id");
            if (TextUtils.isEmpty(string)) {
                this.callback.onFullScreenAdFailedToLoad(AdError.InternalError);
                return;
            }
            this.listener = new AdColonyFullscreenAdListener(string, this, this.callback);
            if (this.isRewarded) {
                AdColonyRewardListenerWrapper.get().addListener(this.listener);
            }
            AdColonyAdapter.getOrRequestInterstitial(string, this.listener);
        } catch (JSONException unused) {
            this.callback.onFullScreenAdFailedToLoad(AdError.InternalError);
        }
    }

    public void show(@NonNull Activity activity) {
        if (this.adColonyInterstitial == null) {
            this.callback.onFullScreenAdFailedToShow(AdError.RendererNotReady);
        } else if (this.adColonyInterstitial.isExpired()) {
            this.callback.onFullScreenAdExpired();
        } else {
            this.adColonyInterstitial.show();
        }
    }

    public void destroy() {
        if (this.adColonyInterstitial != null) {
            if (this.isRewarded && this.listener != null) {
                AdColonyRewardListenerWrapper.get().removeListener(this.listener);
            }
            this.adColonyInterstitial.destroy();
            this.adColonyInterstitial = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void setAdColonyInterstitial(@Nullable AdColonyInterstitial adColonyInterstitial2) {
        this.adColonyInterstitial = adColonyInterstitial2;
    }
}
