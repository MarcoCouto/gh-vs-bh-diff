package com.appodealx.adcolony;

import android.support.annotation.NonNull;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.adcolony.sdk.AdColonyZone;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;

final class AdColonyFullscreenAdListener extends AdColonyInterstitialListener implements AdColonyRewardListener {
    @NonNull
    private AdColonyFullscreenAd adColonyFullscreenAd;
    @NonNull
    private FullScreenAdListener callback;
    private boolean isFinished;
    private boolean isShown;
    @NonNull
    private String zoneId;

    AdColonyFullscreenAdListener(@NonNull String str, @NonNull AdColonyFullscreenAd adColonyFullscreenAd2, @NonNull FullScreenAdListener fullScreenAdListener) {
        this.zoneId = str;
        this.adColonyFullscreenAd = adColonyFullscreenAd2;
        this.callback = fullScreenAdListener;
    }

    public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
        this.adColonyFullscreenAd.setAdColonyInterstitial(adColonyInterstitial);
        this.callback.onFullScreenAdLoaded(this.adColonyFullscreenAd);
    }

    public void onRequestNotFilled(AdColonyZone adColonyZone) {
        this.callback.onFullScreenAdFailedToLoad(AdError.NoFill);
    }

    public void onOpened(AdColonyInterstitial adColonyInterstitial) {
        this.isShown = true;
        this.callback.onFullScreenAdShown();
    }

    public void onClicked(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onFullScreenAdClicked();
    }

    public void onClosed(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onFullScreenAdClosed(this.isFinished);
    }

    public void onExpiring(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onFullScreenAdExpired();
    }

    public void onReward(AdColonyReward adColonyReward) {
        if ((adColonyReward == null || adColonyReward.success()) && this.isShown) {
            this.isFinished = true;
            this.callback.onFullScreenAdCompleted();
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public String getZoneId() {
        return this.zoneId;
    }
}
