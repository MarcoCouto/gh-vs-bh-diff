package com.appodealx.adcolony;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;

class AdColonyInterstitialWrapper {
    @NonNull
    private AdColonyInterstitial adColonyInterstitial;
    /* access modifiers changed from: private */
    @Nullable
    public AdColonyInterstitialListener externalListener;

    AdColonyInterstitialWrapper(@NonNull AdColonyInterstitial adColonyInterstitial2) {
        this.adColonyInterstitial = adColonyInterstitial2;
        adColonyInterstitial2.setListener(new AdColonyInterstitialListener() {
            public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
                if (AdColonyInterstitialWrapper.this.externalListener != null) {
                    AdColonyInterstitialWrapper.this.externalListener.onRequestFilled(adColonyInterstitial);
                }
            }

            public void onRequestNotFilled(AdColonyZone adColonyZone) {
                super.onRequestNotFilled(adColonyZone);
                if (AdColonyInterstitialWrapper.this.externalListener != null) {
                    AdColonyInterstitialWrapper.this.externalListener.onRequestNotFilled(adColonyZone);
                }
            }

            public void onClicked(AdColonyInterstitial adColonyInterstitial) {
                super.onClicked(adColonyInterstitial);
                if (AdColonyInterstitialWrapper.this.externalListener != null) {
                    AdColonyInterstitialWrapper.this.externalListener.onClicked(adColonyInterstitial);
                }
            }

            public void onClosed(AdColonyInterstitial adColonyInterstitial) {
                super.onClosed(adColonyInterstitial);
                if (AdColonyInterstitialWrapper.this.externalListener != null) {
                    AdColonyInterstitialWrapper.this.externalListener.onClosed(adColonyInterstitial);
                }
            }

            public void onExpiring(AdColonyInterstitial adColonyInterstitial) {
                super.onExpiring(adColonyInterstitial);
                if (AdColonyInterstitialWrapper.this.externalListener != null) {
                    AdColonyInterstitialWrapper.this.externalListener.onExpiring(adColonyInterstitial);
                }
                AdColonyAdapter.remove(AdColonyInterstitialWrapper.this);
            }

            public void onOpened(AdColonyInterstitial adColonyInterstitial) {
                super.onOpened(adColonyInterstitial);
                if (AdColonyInterstitialWrapper.this.externalListener != null) {
                    AdColonyInterstitialWrapper.this.externalListener.onOpened(adColonyInterstitial);
                }
                AdColonyAdapter.remove(AdColonyInterstitialWrapper.this);
            }

            public void onLeftApplication(AdColonyInterstitial adColonyInterstitial) {
                super.onLeftApplication(adColonyInterstitial);
                if (AdColonyInterstitialWrapper.this.externalListener != null) {
                    AdColonyInterstitialWrapper.this.externalListener.onLeftApplication(adColonyInterstitial);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public AdColonyInterstitial getAdColonyInterstitial() {
        return this.adColonyInterstitial;
    }

    /* access modifiers changed from: 0000 */
    public String getZoneId() {
        return this.adColonyInterstitial.getZoneID();
    }

    /* access modifiers changed from: 0000 */
    public boolean isExpired() {
        return this.adColonyInterstitial.isExpired();
    }

    /* access modifiers changed from: 0000 */
    public void setExternalListener(@Nullable AdColonyInterstitialListener adColonyInterstitialListener) {
        this.externalListener = adColonyInterstitialListener;
    }
}
