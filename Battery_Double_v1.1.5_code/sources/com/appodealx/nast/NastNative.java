package com.appodealx.nast;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Log;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.NativeAdObject;
import com.appodealx.sdk.NativeListener;
import com.appodealx.sdk.utils.HttpTools;
import com.appodealx.sdk.utils.HttpTools.TrackingListener;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import com.mintegral.msdk.base.entity.CampaignEx;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class NastNative extends NativeAdObject {
    private static final int CTA_ID = 8;
    private static final int DESCRIPTION_ID = 127;
    private static final int ICON_ID = 124;
    private static final int IMAGE_ID = 128;
    private static final int RATING_ID = 7;
    private static final int TITLE_ID = 123;
    private static final int VIDEO_ID = 4;
    private JSONObject adInfo;
    @VisibleForTesting
    List<String> clickTrackers = new ArrayList();
    @VisibleForTesting
    List<String> finishTrackers = new ArrayList();
    @VisibleForTesting
    List<String> impTrackers = new ArrayList();
    private NativeListener listener;

    public void destroy() {
    }

    NastNative(JSONObject jSONObject, NativeListener nativeListener) {
        this.adInfo = jSONObject;
        this.listener = nativeListener;
    }

    /* access modifiers changed from: 0000 */
    public void load() {
        try {
            if (this.adInfo == null) {
                this.listener.onNativeFailedToLoad(AdError.InternalError);
                return;
            }
            JSONObject adObject = getAdObject();
            if (adObject == null) {
                this.listener.onNativeFailedToLoad(AdError.InternalError);
                return;
            }
            JSONArray optJSONArray = adObject.optJSONArray("impTrackers");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    this.impTrackers.add(optJSONArray.getString(i));
                }
            }
            JSONArray optJSONArray2 = adObject.optJSONArray("clickTrackers");
            if (optJSONArray2 != null) {
                for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                    this.clickTrackers.add(optJSONArray2.getString(i2));
                }
            }
            JSONArray optJSONArray3 = adObject.optJSONArray("finishTrackers");
            if (optJSONArray3 != null) {
                for (int i3 = 0; i3 < optJSONArray3.length(); i3++) {
                    this.finishTrackers.add(optJSONArray3.getString(i3));
                }
            }
            if (TextUtils.isEmpty(adObject.getString("url"))) {
                this.listener.onNativeFailedToLoad(AdError.InternalError);
            }
            setTitle(adObject.getString("title"));
            setDescription(adObject.optString("description", ""));
            setCta(adObject.getString("cta"));
            setRating(adObject.getDouble(CampaignEx.JSON_KEY_STAR));
            setImage(adObject.getString(MessengerShareContentUtility.MEDIA_IMAGE));
            setIcon(adObject.getString(SettingsJsonConstants.APP_ICON_KEY));
            setUrl(adObject.getString("url"));
            setVideoTag(adObject.optString("videoTag"));
            if (isNativeValid()) {
                this.listener.onNativeLoaded(this);
            } else {
                this.listener.onNativeFailedToLoad(AdError.InternalError);
            }
        } catch (Exception e) {
            Log.e("Appodealx-Nast", e.getMessage());
            this.listener.onNativeFailedToLoad(AdError.InternalError);
        }
    }

    private JSONObject getAdObject() {
        JSONArray jSONArray;
        JSONArray jSONArray2;
        try {
            JSONObject jSONObject = new JSONObject();
            String str = "";
            String str2 = "";
            String str3 = "Learn more";
            float f = 0.0f;
            String str4 = "";
            String str5 = "";
            String str6 = "";
            JSONObject optJson = optJson(this.adInfo, "creative");
            JSONObject jSONObject2 = optJson.getJSONObject("link");
            String string = jSONObject2.getString("url");
            JSONArray optJSONArray = jSONObject2.optJSONArray("clicktrackers");
            JSONArray optJSONArray2 = optJson.optJSONArray("imptrackers");
            JSONArray jSONArray3 = new JSONArray();
            if (optJson.has(RequestInfoKeys.EXT) && !optJson.isNull(RequestInfoKeys.EXT)) {
                jSONArray3 = optJson.getJSONObject(RequestInfoKeys.EXT).optJSONArray("finishtrackers");
            }
            JSONArray jSONArray4 = optJson.getJSONArray("assets");
            int i = 0;
            while (i < jSONArray4.length()) {
                JSONObject optJSONObject = jSONArray4.optJSONObject(i);
                if (optJSONObject != null) {
                    int i2 = optJSONObject.getInt("id");
                    jSONArray2 = jSONArray4;
                    if (i2 == TITLE_ID) {
                        str = optJSONObject.getJSONObject("title").getString(MimeTypes.BASE_TYPE_TEXT);
                    } else if (i2 == ICON_ID) {
                        JSONObject jSONObject3 = optJSONObject.getJSONObject("img");
                        if (jSONObject3 != null) {
                            str5 = jSONObject3.getString("url");
                        }
                    } else if (i2 == 128) {
                        JSONObject optJSONObject2 = optJSONObject.optJSONObject("img");
                        if (optJSONObject2 != null) {
                            str4 = optJSONObject2.optString("url");
                        }
                    } else if (i2 == 4) {
                        JSONObject optJSONObject3 = optJSONObject.optJSONObject("video");
                        if (optJSONObject3 != null) {
                            str6 = optJSONObject3.optString("vasttag");
                        }
                    } else if (i2 == DESCRIPTION_ID) {
                        str2 = optJSONObject.getJSONObject("data").optString("value");
                    } else {
                        if (i2 == 7) {
                            jSONArray = optJSONArray;
                            f = (float) optJSONObject.getJSONObject("data").optDouble("value", Utils.DOUBLE_EPSILON);
                        } else {
                            jSONArray = optJSONArray;
                            if (i2 == 8) {
                                str3 = optJSONObject.getJSONObject("data").getString("value");
                            }
                        }
                        i++;
                        jSONArray4 = jSONArray2;
                        optJSONArray = jSONArray;
                    }
                } else {
                    jSONArray2 = jSONArray4;
                }
                jSONArray = optJSONArray;
                i++;
                jSONArray4 = jSONArray2;
                optJSONArray = jSONArray;
            }
            JSONArray jSONArray5 = optJSONArray;
            jSONObject.put("title", str);
            jSONObject.put("description", str2);
            jSONObject.put(SettingsJsonConstants.APP_ICON_KEY, str5);
            jSONObject.put(MessengerShareContentUtility.MEDIA_IMAGE, str4);
            jSONObject.put(CampaignEx.JSON_KEY_STAR, (double) f);
            jSONObject.put("cta", str3);
            jSONObject.put("impTrackers", optJSONArray2);
            jSONObject.put("clickTrackers", jSONArray5);
            jSONObject.put("finishTrackers", jSONArray3);
            jSONObject.put("url", string);
            jSONObject.put("videoTag", str6);
            return jSONObject;
        } catch (Exception e) {
            Log.e("Appodealx-Nast", e.getMessage());
            return null;
        }
    }

    @VisibleForTesting
    static JSONObject optJson(@Nullable JSONObject jSONObject, @Nullable String str) {
        if (jSONObject == null) {
            return null;
        }
        if (TextUtils.isEmpty(str)) {
            return jSONObject;
        }
        Object opt = jSONObject.opt(str);
        if (opt instanceof JSONObject) {
            return (JSONObject) opt;
        }
        if (opt instanceof String) {
            try {
                return new JSONObject(opt.toString());
            } catch (Exception e) {
                Log.e("Appodealx-Nast", e.getMessage());
            }
        }
        return jSONObject;
    }

    public void onImpression(int i) {
        super.onImpression(i);
        for (String fireUrl : this.impTrackers) {
            HttpTools.fireUrl(fireUrl, new TrackingListener() {
                public void onComplete(boolean z) {
                }
            });
        }
    }

    public void onAdClick() {
        super.onAdClick();
        for (String fireUrl : this.clickTrackers) {
            HttpTools.fireUrl(fireUrl, new TrackingListener() {
                public void onComplete(boolean z) {
                }
            });
        }
    }

    public boolean containsVideo() {
        return !TextUtils.isEmpty(getVideoTag());
    }

    private boolean isNativeValid() {
        return !TextUtils.isEmpty(getTitle()) && !TextUtils.isEmpty(getDescription()) && !TextUtils.isEmpty(getCta());
    }
}
