package com.appodealx.nast;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.NativeAd;
import com.appodealx.sdk.NativeListener;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Nast extends InternalAdapterInterface {
    public void loadNative(@NonNull Activity activity, JSONObject jSONObject, @NonNull Map<String, String> map, NativeAd nativeAd, NativeListener nativeListener) {
        NastNative nastNative = new NastNative(jSONObject, nativeListener);
        nastNative.load();
        nativeAd.setAd(nastNative);
    }

    @NonNull
    public JSONArray getNativeRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    @Nullable
    private JSONObject getRequestInfoFromSettings(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, "nast");
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, "1.0");
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", jSONObject.getString("id"));
            jSONObject3.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM));
            jSONObject2.put("appodeal", jSONObject3);
            return jSONObject2;
        } catch (JSONException e) {
            Log.e("Appodealx-Nast", e.getMessage());
            return null;
        }
    }
}
