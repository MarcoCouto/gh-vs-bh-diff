package com.appodealx.s2s;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.appodealx.sdk.AppodealX;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Adapter extends InternalAdapterInterface {
    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getAdapterRequestInfo(jSONObject);
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getAdapterRequestInfo(jSONObject);
    }

    @NonNull
    public JSONArray getBannerRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getAdapterRequestInfo(jSONObject);
    }

    @NonNull
    public JSONArray getNativeRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getAdapterRequestInfo(jSONObject);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public JSONArray getAdapterRequestInfo(@NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    @Nullable
    private JSONObject getRequestInfoFromSettings(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, jSONObject.getString("status"));
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, AppodealX.getVersion());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", jSONObject.getString("id"));
            jSONObject3.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM));
            jSONObject2.put("appodeal", jSONObject3);
            JSONObject jSONObject4 = new JSONObject();
            JSONObject optJSONObject = jSONObject.optJSONObject(RequestInfoKeys.EXTRA_PARALLEL_BIDDING_INFO);
            if (optJSONObject != null) {
                Iterator keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    jSONObject4.put(str, optJSONObject.get(str));
                }
            }
            jSONObject2.put(RequestInfoKeys.EXT, jSONObject4);
            return jSONObject2;
        } catch (JSONException e) {
            Log.e("Appodealx-S2S", e.getMessage());
            return null;
        }
    }
}
