package com.appodealx.sdk;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.view.View;
import java.util.List;

public abstract class NativeAdObject extends Ad {

    /* renamed from: a reason: collision with root package name */
    private String f1761a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private double h;
    private c i;

    public boolean containsVideo() {
        return hasVideo();
    }

    public String getAgeRestrictions() {
        return null;
    }

    public String getCta() {
        return this.c;
    }

    public String getDescription() {
        return this.b;
    }

    public int getHash() {
        return hashCode();
    }

    public String getIcon() {
        return this.e;
    }

    public View getIconView(Context context) {
        return null;
    }

    public String getImage() {
        return this.d;
    }

    public View getMediaView(Context context) {
        return null;
    }

    public View getProviderView(Context context) {
        return null;
    }

    public double getRating() {
        return this.h;
    }

    public String getTitle() {
        return this.f1761a;
    }

    public String getUrl() {
        return this.f;
    }

    public String getVideoTag() {
        return this.g;
    }

    public boolean hasVideo() {
        return false;
    }

    @CallSuper
    public void onAdClick() {
        if (this.i != null) {
            this.i.b();
        }
    }

    @CallSuper
    public void onAdError(String str) {
        if (this.i != null) {
            this.i.a(str);
        }
    }

    @CallSuper
    public void onImpression(int i2) {
        if (this.i != null) {
            this.i.a(i2);
        }
    }

    public void registerViewForInteraction(View view, List<View> list) {
    }

    public void setCta(String str) {
        this.c = str;
    }

    public void setDescription(String str) {
        this.b = str;
    }

    public void setEventTracker(c cVar) {
        this.i = cVar;
    }

    public void setIcon(String str) {
        this.e = str;
    }

    public void setImage(String str) {
        this.d = str;
    }

    public void setRating(double d2) {
        this.h = d2;
    }

    public void setTitle(String str) {
        this.f1761a = str;
    }

    public void setUrl(String str) {
        this.f = str;
    }

    public void setVideoTag(String str) {
        this.g = str;
    }

    public void unregisterViewForInteraction() {
    }
}
