package com.appodealx.sdk;

import android.support.annotation.NonNull;

public interface BannerListener {
    void onBannerClicked();

    void onBannerExpired();

    void onBannerFailedToLoad(@NonNull AdError adError);

    void onBannerLoaded(BannerView bannerView);
}
