package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

class h extends a {
    private final Map<String, String> b;
    private final NativeAd c;
    private final NativeListener d;

    h(@NonNull Activity activity, long j, @NonNull List<JSONObject> list, @NonNull Map<String, String> map, @NonNull NativeAd nativeAd, @NonNull NativeListener nativeListener) {
        super(activity, j, list);
        this.b = map;
        this.c = nativeAd;
        this.d = nativeListener;
    }

    /* access modifiers changed from: 0000 */
    public JSONArray a(@NonNull Activity activity, @NonNull InternalAdapterInterface internalAdapterInterface, @NonNull JSONObject jSONObject) {
        return internalAdapterInterface.getNativeRequestInfo(activity, jSONObject);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull Activity activity, @NonNull j jVar) {
        InternalAdapterInterface internalAdapterInterface = (InternalAdapterInterface) AppodealX.a().get(jVar.b());
        c cVar = new c(jVar, this.f1762a);
        if (internalAdapterInterface != null) {
            this.c.a(cVar);
            internalAdapterInterface.setLogging(AppodealX.isLoggingEnabled());
            internalAdapterInterface.loadNative(activity, jVar.k(), this.b, this.c, new f(this.d, cVar));
            return;
        }
        a(AdError.InternalError);
        cVar.a("1008");
    }

    /* access modifiers changed from: 0000 */
    public void a(AdError adError) {
        this.d.onNativeFailedToLoad(adError);
    }
}
