package com.appodealx.sdk;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import org.json.JSONArray;
import org.json.JSONObject;

class i extends AsyncTask<a, Void, j> {

    /* renamed from: a reason: collision with root package name */
    private final String f1772a;
    private final b b;

    interface a {
        @NonNull
        JSONArray a();
    }

    interface b {
        void a(@Nullable j jVar);
    }

    i(@Nullable String str, @NonNull b bVar) {
        this.f1772a = str;
        this.b = bVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058 A[SYNTHETIC, Splitter:B:25:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0068 A[SYNTHETIC, Splitter:B:32:0x0068] */
    private JSONObject a(InputStream inputStream) {
        BufferedReader bufferedReader;
        try {
            StringBuilder sb = new StringBuilder(inputStream.available());
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    sb.append(10);
                } catch (Exception e) {
                    e = e;
                    try {
                        Log.e("AppodealX-Request", "", e);
                        if (bufferedReader != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (Exception e2) {
                                Log.e("AppodealX-Request", "", e2);
                            }
                        }
                        throw th;
                    }
                }
            }
            if (sb.length() > 0) {
                sb.setLength(sb.length() - 1);
            }
            JSONObject jSONObject = new JSONObject(sb.toString());
            try {
                bufferedReader.close();
            } catch (Exception e3) {
                Log.e("AppodealX-Request", "", e3);
            }
            return jSONObject;
        } catch (Exception e4) {
            e = e4;
            bufferedReader = null;
            Log.e("AppodealX-Request", "", e);
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (Exception e5) {
                    Log.e("AppodealX-Request", "", e5);
                }
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            if (bufferedReader != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008e A[SYNTHETIC, Splitter:B:37:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0093 A[Catch:{ Exception -> 0x0099 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x009e A[SYNTHETIC, Splitter:B:46:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00a3 A[Catch:{ Exception -> 0x00a9 }] */
    /* renamed from: a */
    public j doInBackground(a... aVarArr) {
        HttpURLConnection httpURLConnection;
        DataOutputStream dataOutputStream;
        DataOutputStream dataOutputStream2 = null;
        try {
            httpURLConnection = (HttpURLConnection) new URL(this.f1772a).openConnection();
            try {
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.setReadTimeout(5000);
                httpURLConnection.setDoOutput(true);
                dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            } catch (Exception e) {
                e = e;
                dataOutputStream = null;
                try {
                    Log.e("AppodealX", "", e);
                    if (httpURLConnection != null) {
                    }
                    if (dataOutputStream != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    dataOutputStream2 = dataOutputStream;
                    if (httpURLConnection != null) {
                    }
                    if (dataOutputStream2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (httpURLConnection != null) {
                }
                if (dataOutputStream2 != null) {
                }
                throw th;
            }
            try {
                dataOutputStream.write(aVarArr[0].a().toString().getBytes(Charset.forName("UTF-8")));
                if (httpURLConnection.getResponseCode() != 200) {
                    if (httpURLConnection != null) {
                        try {
                            httpURLConnection.disconnect();
                        } catch (Exception unused) {
                        }
                    }
                    dataOutputStream.flush();
                    dataOutputStream.close();
                    return null;
                }
                j jVar = new j(httpURLConnection.getHeaderFields(), a(httpURLConnection.getInputStream()));
                if (httpURLConnection != null) {
                    try {
                        httpURLConnection.disconnect();
                    } catch (Exception unused2) {
                    }
                }
                dataOutputStream.flush();
                dataOutputStream.close();
                return jVar;
            } catch (Exception e2) {
                e = e2;
                Log.e("AppodealX", "", e);
                if (httpURLConnection != null) {
                }
                if (dataOutputStream != null) {
                }
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            httpURLConnection = null;
            dataOutputStream = null;
            Log.e("AppodealX", "", e);
            if (httpURLConnection != null) {
                try {
                    httpURLConnection.disconnect();
                } catch (Exception unused3) {
                    return null;
                }
            }
            if (dataOutputStream != null) {
                dataOutputStream.flush();
                dataOutputStream.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            httpURLConnection = null;
            if (httpURLConnection != null) {
                try {
                    httpURLConnection.disconnect();
                } catch (Exception unused4) {
                    throw th;
                }
            }
            if (dataOutputStream2 != null) {
                dataOutputStream2.flush();
                dataOutputStream2.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(j jVar) {
        super.onPostExecute(jVar);
        this.b.a(jVar);
    }
}
