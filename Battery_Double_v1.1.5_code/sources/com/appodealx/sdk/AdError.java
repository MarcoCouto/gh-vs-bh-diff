package com.appodealx.sdk;

public class AdError {
    public static final AdError AdaptersNotFound = new AdError("Adapters not found");
    public static final AdError InternalError = new AdError("Internal error");
    public static final AdError InvalidAdSize = new AdError("Invalid ad size");
    public static final AdError NetworkError = new AdError("Network error");
    public static final AdError NoFill = new AdError("No fill");
    public static final AdError RendererNotReady = new AdError("Renderer not ready");
    public static final AdError SDKNotInitialized = new AdError("SDK not initialized");
    public static final AdError TimeoutError = new AdError("Timeout error");

    /* renamed from: a reason: collision with root package name */
    private final String f1755a;

    private AdError(String str) {
        this.f1755a = str;
    }

    public String toString() {
        return String.format("AdError: %s", new Object[]{this.f1755a});
    }
}
