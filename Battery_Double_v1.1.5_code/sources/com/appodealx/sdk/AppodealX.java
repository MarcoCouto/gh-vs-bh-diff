package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class AppodealX {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static final Map<String, String> f1756a = new HashMap<String, String>() {
        {
            put("adcolony", "com.appodealx.adcolony.AdColonyAdapter");
            put("applovin", "com.appodealx.applovin.Applovin");
            put("facebook", "com.appodealx.facebook.Facebook");
            put("tapjoy", "com.appodealx.tapjoy.Tapjoy");
            put("vast", "com.appodealx.vast.Vast");
            put("my_target", "com.appodealx.mytarget.MyTarget");
            put(CampaignEx.JSON_KEY_MRAID, "com.appodealx.mraid.Mraid");
            put("nast", "com.appodealx.nast.Nast");
            put("inner-active", "com.appodealx.s2s.Adapter");
            put("smaato", "com.appodealx.s2s.Adapter");
            put("openx", "com.appodealx.s2s.Adapter");
            put("pubnative", "com.appodealx.s2s.Adapter");
        }
    };
    private static boolean b;
    private static Map<String, InternalAdapterInterface> c = new HashMap();

    private static InternalAdapterInterface a(String str) throws Exception {
        String str2 = (String) f1756a.get(str);
        if (str2 != null) {
            Class cls = Class.forName(str2);
            if (InternalAdapterInterface.class.isAssignableFrom(cls)) {
                return (InternalAdapterInterface) cls.newInstance();
            }
        }
        return null;
    }

    static Map<String, InternalAdapterInterface> a() {
        return c;
    }

    private static void a(Activity activity, String str, JSONObject jSONObject) throws Throwable {
        String str2;
        String str3;
        Object[] objArr;
        if (!c.containsKey(str)) {
            InternalAdapterInterface a2 = a(str);
            if (a2 != null) {
                a2.initialize(activity, jSONObject);
                c.put(str, a2);
                str2 = "AppodealX";
                str3 = "Register adapter: %s";
                objArr = new Object[]{str};
            } else {
                str2 = "AppodealX";
                str3 = "AppodealX adapter %s not found";
                objArr = new Object[]{str};
            }
            Log.d(str2, String.format(str3, objArr));
        }
    }

    @NonNull
    public static Set<String> getSupportedAdaptersNames() {
        return f1756a.keySet();
    }

    public static String getVersion() {
        return "1.0.0";
    }

    public static void initialize(@NonNull Activity activity, @NonNull List<JSONObject> list) {
        for (JSONObject jSONObject : list) {
            try {
                a(activity, jSONObject.getString("status"), jSONObject);
            } catch (Throwable unused) {
                Log.e("AppodealX", String.format("AppodealX adapter %s not found", new Object[]{jSONObject.toString()}));
            }
        }
    }

    public static boolean isLoggingEnabled() {
        return b;
    }

    public static void setLogging(boolean z) {
        b = z;
    }

    public static void updateConsent(Activity activity, boolean z, boolean z2) {
        for (InternalAdapterInterface updateConsent : c.values()) {
            updateConsent.updateConsent(activity, z, z2);
        }
    }

    public static void updateCoppa(boolean z) {
        for (InternalAdapterInterface updateCoppa : c.values()) {
            updateCoppa.updateCoppa(z);
        }
    }
}
