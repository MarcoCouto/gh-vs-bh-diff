package com.appodealx.sdk;

import android.support.annotation.NonNull;

public interface NativeListener {
    void onNativeClicked();

    void onNativeExpired();

    void onNativeFailedToLoad(@NonNull AdError adError);

    void onNativeLoaded(NativeAdObject nativeAdObject);
}
