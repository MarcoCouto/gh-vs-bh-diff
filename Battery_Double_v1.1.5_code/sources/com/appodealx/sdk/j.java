package com.appodealx.sdk;

import android.text.TextUtils;
import android.util.Log;
import com.github.mikephil.charting.utils.Utils;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class j {

    /* renamed from: a reason: collision with root package name */
    private final String f1773a;
    private final String b;
    private double c;
    private final String d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;
    private final String i;
    private final JSONObject j;

    j(Map<String, List<String>> map, JSONObject jSONObject) {
        this.f1773a = a(map, "X-Appodeal-Displaymanager");
        this.b = a(map, "X-Appodeal-Demand-Source");
        this.d = a(map, "X-Appodeal-Url-Click");
        this.e = a(map, "X-Appodeal-Url-Impression");
        this.f = a(map, "X-Appodeal-Url-Fill");
        this.g = a(map, "X-Appodeal-Url-Finish");
        this.h = a(map, "X-Appodeal-Url-Error");
        this.i = a(map, "X-Appodeal-Identifier");
        String a2 = a(map, "X-Appodeal-Price");
        if (!TextUtils.isEmpty(a2)) {
            try {
                this.c = Double.valueOf(a2).doubleValue();
            } catch (NumberFormatException e2) {
                this.c = Utils.DOUBLE_EPSILON;
                Log.e("AppodealX-Response", "", e2);
            }
        } else {
            this.c = Utils.DOUBLE_EPSILON;
        }
        String a3 = a(map, "X-Appodeal-Close-Time");
        long j2 = 0;
        if (!TextUtils.isEmpty(a3)) {
            try {
                j2 = Long.valueOf(a3).longValue();
            } catch (NumberFormatException e3) {
                Log.e("AppodealX-Response", "", e3);
            }
        }
        this.j = jSONObject;
        if (this.j != null) {
            try {
                this.j.put("close_time", j2);
            } catch (JSONException e4) {
                Log.e("AppodealX-Response", "", e4);
            }
        }
    }

    private String a(Map<String, List<String>> map, String str) {
        if (map.containsKey(str)) {
            List list = (List) map.get(str);
            if (list != null && list.size() > 0) {
                return (String) list.get(0);
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.j != null;
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.f1773a;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public double d() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public String f() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public String g() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public String h() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public String i() {
        return this.h;
    }

    public String j() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject k() {
        return this.j;
    }
}
