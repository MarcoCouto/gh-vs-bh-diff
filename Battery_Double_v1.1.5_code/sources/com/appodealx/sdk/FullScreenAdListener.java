package com.appodealx.sdk;

import android.support.annotation.NonNull;

public interface FullScreenAdListener extends d {
    void onFullScreenAdClicked();

    void onFullScreenAdClosed(boolean z);

    void onFullScreenAdCompleted();

    void onFullScreenAdExpired();

    void onFullScreenAdFailedToLoad(@NonNull AdError adError);

    void onFullScreenAdFailedToShow(@NonNull AdError adError);

    void onFullScreenAdLoaded(FullScreenAdObject fullScreenAdObject);

    void onFullScreenAdShown();
}
