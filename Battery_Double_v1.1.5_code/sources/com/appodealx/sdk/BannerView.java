package com.appodealx.sdk;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;
import java.util.List;
import org.json.JSONObject;

public class BannerView extends FrameLayout {

    /* renamed from: a reason: collision with root package name */
    private String f1757a;
    private String b;
    private String c;
    private int d;
    private c e;
    private Runnable f;
    private double g;

    public BannerView(@NonNull Context context) {
        super(context);
    }

    public BannerView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void destroy() {
        removeAllViews();
        if (this.f != null) {
            this.f.run();
        }
    }

    public String getAdId() {
        return this.f1757a;
    }

    public int getBannerHeight() {
        return this.d;
    }

    public String getDemandSource() {
        return this.c;
    }

    public double getEcpm() {
        return this.g;
    }

    public String getNetworkName() {
        return this.b;
    }

    public void loadAd(@NonNull String str, @NonNull List<JSONObject> list, long j, @NonNull BannerListener bannerListener) {
        if (getContext() instanceof Activity) {
            b bVar = new b((Activity) getContext(), this, j, list, bannerListener);
            bVar.a(str);
            return;
        }
        Log.e("AppodealX", "You should create BannerView with activity");
        bannerListener.onBannerFailedToLoad(AdError.InternalError);
    }

    /* access modifiers changed from: 0000 */
    public void setAdId(String str) {
        this.f1757a = str;
    }

    public void setBannerHeight(int i) {
        this.d = i;
    }

    public void setDemandSource(String str) {
        this.c = str;
    }

    public void setDestroyRunnable(Runnable runnable) {
        this.f = runnable;
    }

    public void setEcpm(double d2) {
        this.g = d2;
    }

    /* access modifiers changed from: 0000 */
    public void setEventTracker(c cVar) {
        this.e = cVar;
    }

    /* access modifiers changed from: 0000 */
    public void setNetworkName(String str) {
        this.b = str;
    }

    public void trackError(int i) {
        if (this.e != null) {
            this.e.a(String.valueOf(i));
        }
    }

    public void trackImpression(int i) {
        if (this.e != null) {
            this.e.a(i);
        }
    }
}
