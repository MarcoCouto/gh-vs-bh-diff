package com.appodealx.sdk;

import android.support.annotation.NonNull;
import com.google.android.gms.ads.formats.NativeContentAd;

public class InternalFullScreenAdListener implements FullScreenAdListener {

    /* renamed from: a reason: collision with root package name */
    private final FullScreenAdListener f1759a;
    private final c b;

    InternalFullScreenAdListener(@NonNull FullScreenAdListener fullScreenAdListener, @NonNull c cVar) {
        this.f1759a = fullScreenAdListener;
        this.b = cVar;
    }

    public int getPlacementId() {
        return this.f1759a.getPlacementId();
    }

    public void onFullScreenAdClicked() {
        this.b.b();
        this.f1759a.onFullScreenAdClicked();
    }

    public void onFullScreenAdClosed(boolean z) {
        this.f1759a.onFullScreenAdClosed(z);
    }

    public void onFullScreenAdCompleted() {
        this.b.c();
        this.f1759a.onFullScreenAdCompleted();
    }

    public void onFullScreenAdExpired() {
        this.f1759a.onFullScreenAdExpired();
    }

    public void onFullScreenAdFailedToLoad(@NonNull AdError adError) {
        this.b.a("1010");
        this.f1759a.onFullScreenAdFailedToLoad(adError);
    }

    public void onFullScreenAdFailedToShow(@NonNull AdError adError) {
        this.f1759a.onFullScreenAdFailedToShow(adError);
    }

    public void onFullScreenAdLoaded(FullScreenAdObject fullScreenAdObject) {
        this.b.a();
        fullScreenAdObject.a(this.b.d());
        fullScreenAdObject.setNetworkName(this.b.e());
        fullScreenAdObject.setDemandSource(this.b.f());
        fullScreenAdObject.setEcpm(this.b.g());
        this.f1759a.onFullScreenAdLoaded(fullScreenAdObject);
    }

    public void onFullScreenAdShown() {
        this.b.a(getPlacementId());
        this.f1759a.onFullScreenAdShown();
    }

    public void trackCloseTimeError() {
        this.b.a(NativeContentAd.ASSET_MEDIA_VIDEO);
    }
}
