package com.appodealx.sdk;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.appodealx.sdk";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 100100100;
    public static final String VERSION_NAME = "1.0.0";
}
