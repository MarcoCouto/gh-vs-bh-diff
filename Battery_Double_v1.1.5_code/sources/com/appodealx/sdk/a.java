package com.appodealx.sdk;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

abstract class a {

    /* renamed from: a reason: collision with root package name */
    protected long f1762a;
    /* access modifiers changed from: private */
    public final List<JSONObject> b;
    /* access modifiers changed from: private */
    public WeakReference<Activity> c;

    a(Activity activity, long j, List<JSONObject> list) {
        this.c = new WeakReference<>(activity);
        this.f1762a = j;
        this.b = list;
    }

    /* access modifiers changed from: private */
    public static JSONArray b(JSONArray... jSONArrayArr) {
        JSONArray jSONArray = new JSONArray();
        for (JSONArray jSONArray2 : jSONArrayArr) {
            for (int i = 0; i < jSONArray2.length(); i++) {
                try {
                    jSONArray.put(jSONArray2.get(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return jSONArray;
    }

    /* access modifiers changed from: 0000 */
    public abstract JSONArray a(@NonNull Activity activity, @NonNull InternalAdapterInterface internalAdapterInterface, @NonNull JSONObject jSONObject);

    /* access modifiers changed from: 0000 */
    public abstract void a(@NonNull Activity activity, @NonNull j jVar);

    /* access modifiers changed from: 0000 */
    public abstract void a(AdError adError);

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        final Map a2 = AppodealX.a();
        if (a2.values().size() == 0) {
            a(AdError.AdaptersNotFound);
            return;
        }
        AnonymousClass1 r1 = new a() {
            @NonNull
            public JSONArray a() {
                JSONArray jSONArray = new JSONArray();
                Activity activity = (Activity) a.this.c.get();
                if (activity != null) {
                    for (JSONObject jSONObject : a.this.b) {
                        String optString = jSONObject.optString("status");
                        if (!TextUtils.isEmpty(optString) && a2.containsKey(optString)) {
                            InternalAdapterInterface internalAdapterInterface = (InternalAdapterInterface) a2.get(optString);
                            if (internalAdapterInterface != null) {
                                jSONArray = a.b(jSONArray, a.this.a(activity, internalAdapterInterface, jSONObject));
                            }
                        }
                    }
                }
                return jSONArray;
            }
        };
        i iVar = new i(str, new b() {
            public void a(@Nullable j jVar) {
                Activity activity = (Activity) a.this.c.get();
                if (jVar == null || !jVar.a() || activity == null) {
                    a.this.a(AdError.NoFill);
                } else {
                    a.this.a(activity, jVar);
                }
            }
        });
        try {
            iVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new a[]{r1});
        } catch (Exception e) {
            Log.d("AppodealX", "", e);
            a(AdError.InternalError);
        }
    }
}
