package com.appodealx.sdk;

import android.support.annotation.NonNull;

class f implements NativeListener {

    /* renamed from: a reason: collision with root package name */
    private final NativeListener f1771a;
    private final c b;

    f(@NonNull NativeListener nativeListener, @NonNull c cVar) {
        this.f1771a = nativeListener;
        this.b = cVar;
    }

    public void onNativeClicked() {
        this.f1771a.onNativeClicked();
        this.b.b();
    }

    public void onNativeExpired() {
        this.f1771a.onNativeExpired();
    }

    public void onNativeFailedToLoad(@NonNull AdError adError) {
        this.b.a("1010");
        this.f1771a.onNativeFailedToLoad(adError);
    }

    public void onNativeLoaded(NativeAdObject nativeAdObject) {
        this.b.a();
        nativeAdObject.setEventTracker(this.b);
        nativeAdObject.a(this.b.d());
        nativeAdObject.setNetworkName(this.b.e());
        nativeAdObject.setDemandSource(this.b.f());
        nativeAdObject.setEcpm(this.b.g());
        this.f1771a.onNativeLoaded(nativeAdObject);
    }
}
