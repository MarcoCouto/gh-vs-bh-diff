package com.appodealx.sdk;

import android.support.annotation.NonNull;

class e implements BannerListener {

    /* renamed from: a reason: collision with root package name */
    private final BannerListener f1770a;
    private final c b;

    e(@NonNull BannerListener bannerListener, @NonNull c cVar) {
        this.f1770a = bannerListener;
        this.b = cVar;
    }

    public void onBannerClicked() {
        this.b.b();
        this.f1770a.onBannerClicked();
    }

    public void onBannerExpired() {
        this.f1770a.onBannerExpired();
    }

    public void onBannerFailedToLoad(@NonNull AdError adError) {
        this.b.a("1010");
        this.f1770a.onBannerFailedToLoad(adError);
    }

    public void onBannerLoaded(BannerView bannerView) {
        this.b.a();
        bannerView.setAdId(this.b.d());
        bannerView.setNetworkName(this.b.e());
        bannerView.setDemandSource(this.b.f());
        bannerView.setEcpm(this.b.g());
        this.f1770a.onBannerLoaded(bannerView);
    }
}
