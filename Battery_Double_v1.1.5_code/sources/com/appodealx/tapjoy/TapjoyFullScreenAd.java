package com.appodealx.tapjoy;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.tapjoy.TJPlacement;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

class TapjoyFullScreenAd extends FullScreenAdObject {
    private JSONObject adInfo;
    private FullScreenAdListener listener;
    private TJPlacement placement;

    TapjoyFullScreenAd(JSONObject jSONObject, FullScreenAdListener fullScreenAdListener) {
        this.adInfo = jSONObject;
        this.listener = fullScreenAdListener;
    }

    /* access modifiers changed from: 0000 */
    public void load(Activity activity) {
        try {
            TapjoyFullScreenAdListener tapjoyFullScreenAdListener = new TapjoyFullScreenAdListener(this, this.listener);
            String string = this.adInfo.getString("placement_name");
            Tapjoy.setActivity(activity);
            this.placement = Tapjoy.getLimitedPlacement(string, tapjoyFullScreenAdListener);
            this.placement.setMediationName("appodeal");
            this.placement.setAdapterVersion("0.0.1");
            this.placement.setVideoListener(tapjoyFullScreenAdListener);
            HashMap hashMap = new HashMap();
            JSONObject jSONObject = this.adInfo.getJSONObject("tapjoy_metadata");
            String string2 = jSONObject.getString("id");
            String string3 = jSONObject.getString(TapjoyAuctionFlags.AUCTION_DATA);
            hashMap.put("id", string2);
            hashMap.put(TapjoyAuctionFlags.AUCTION_DATA, string3);
            this.placement.setAuctionData(hashMap);
            this.placement.requestContent();
        } catch (JSONException e) {
            Log.e("Appodealx-Tapjoy", e.getMessage());
            this.listener.onFullScreenAdFailedToLoad(AdError.InternalError);
        }
    }

    public void show(@NonNull Activity activity) {
        if (this.placement == null || !this.placement.isContentReady()) {
            this.listener.onFullScreenAdFailedToShow(AdError.InternalError);
            return;
        }
        Tapjoy.setActivity(activity);
        this.placement.showContent();
    }

    public void destroy() {
        if (this.placement != null) {
            this.placement.setVideoListener(null);
            this.placement = null;
        }
    }
}
