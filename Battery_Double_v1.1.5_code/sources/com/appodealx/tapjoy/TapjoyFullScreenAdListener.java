package com.appodealx.tapjoy;

import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.tapjoy.TJActionRequest;
import com.tapjoy.TJError;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TJPlacementVideoListener;

public class TapjoyFullScreenAdListener implements TJPlacementListener, TJPlacementVideoListener {
    private boolean finished;
    private TapjoyFullScreenAd fullScreenAd;
    private FullScreenAdListener listener;

    public void onPurchaseRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str) {
    }

    public void onRewardRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str, int i) {
    }

    public void onVideoError(TJPlacement tJPlacement, String str) {
    }

    public void onVideoStart(TJPlacement tJPlacement) {
    }

    TapjoyFullScreenAdListener(TapjoyFullScreenAd tapjoyFullScreenAd, FullScreenAdListener fullScreenAdListener) {
        this.fullScreenAd = tapjoyFullScreenAd;
        this.listener = fullScreenAdListener;
    }

    public void onRequestSuccess(TJPlacement tJPlacement) {
        if (!tJPlacement.isContentAvailable()) {
            this.listener.onFullScreenAdFailedToLoad(AdError.NoFill);
        }
    }

    public void onRequestFailure(TJPlacement tJPlacement, TJError tJError) {
        this.listener.onFullScreenAdFailedToLoad(AdError.NoFill);
    }

    public void onContentReady(TJPlacement tJPlacement) {
        this.listener.onFullScreenAdLoaded(this.fullScreenAd);
    }

    public void onContentShow(TJPlacement tJPlacement) {
        this.listener.onFullScreenAdShown();
    }

    public void onClick(TJPlacement tJPlacement) {
        this.listener.onFullScreenAdClicked();
    }

    public void onContentDismiss(TJPlacement tJPlacement) {
        this.listener.onFullScreenAdClosed(this.finished);
    }

    public void onVideoComplete(TJPlacement tJPlacement) {
        this.finished = true;
        this.listener.onFullScreenAdCompleted();
    }
}
