package com.appodealx.applovin;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import java.util.Map;

class ApplovinFullScreenAdListener implements AppLovinAdLoadListener, AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdVideoPlaybackListener, AppLovinAdRewardListener {
    private boolean finished;
    private ApplovinFullScreenAd fullScreenAd;
    private FullScreenAdListener listener;

    public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
    }

    public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
    }

    public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
    }

    public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
    }

    public void validationRequestFailed(AppLovinAd appLovinAd, int i) {
    }

    public void videoPlaybackBegan(AppLovinAd appLovinAd) {
    }

    ApplovinFullScreenAdListener(ApplovinFullScreenAd applovinFullScreenAd, FullScreenAdListener fullScreenAdListener) {
        this.fullScreenAd = applovinFullScreenAd;
        this.listener = fullScreenAdListener;
    }

    public void adClicked(AppLovinAd appLovinAd) {
        this.listener.onFullScreenAdClicked();
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        this.listener.onFullScreenAdShown();
    }

    public void adHidden(AppLovinAd appLovinAd) {
        this.listener.onFullScreenAdClosed(this.finished);
    }

    public void adReceived(AppLovinAd appLovinAd) {
        this.fullScreenAd.appLovinAd = appLovinAd;
        this.listener.onFullScreenAdLoaded(this.fullScreenAd);
    }

    public void failedToReceiveAd(int i) {
        this.listener.onFullScreenAdFailedToLoad(AdError.NoFill);
    }

    public void videoPlaybackEnded(AppLovinAd appLovinAd, double d, boolean z) {
        this.finished = z;
        if (z) {
            this.listener.onFullScreenAdCompleted();
        }
    }
}
