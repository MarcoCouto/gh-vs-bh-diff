package com.appodealx.applovin;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.FrameLayout.LayoutParams;
import com.applovin.adview.AppLovinAdView;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;

class ApplovinBanner {
    private String adToken;
    /* access modifiers changed from: private */
    public AppLovinAdView appLovinAdView;
    /* access modifiers changed from: private */
    public AppLovinSdk appLovinSdk;
    /* access modifiers changed from: private */
    public BannerView bannerView;
    private Runnable destroyRunnable = new Runnable() {
        public void run() {
            if (ApplovinBanner.this.appLovinAdView != null) {
                ApplovinBanner.this.appLovinAdView.destroy();
                ApplovinBanner.this.appLovinAdView = null;
            }
            ApplovinBanner.this.bannerView = null;
        }
    };
    /* access modifiers changed from: private */
    public BannerListener listener;

    ApplovinBanner(@NonNull BannerView bannerView2, @NonNull String str, @NonNull AppLovinSdk appLovinSdk2, @NonNull BannerListener bannerListener) {
        this.bannerView = bannerView2;
        this.adToken = str;
        this.appLovinSdk = appLovinSdk2;
        this.listener = bannerListener;
    }

    /* access modifiers changed from: 0000 */
    public void load() {
        this.bannerView.setDestroyRunnable(this.destroyRunnable);
        this.appLovinSdk.getAdService().loadNextAdForAdToken(this.adToken, new ApplovinBannerListener(this, this.listener));
    }

    /* access modifiers changed from: 0000 */
    public void fillContainer(@NonNull final AppLovinAd appLovinAd, @NonNull final AppLovinAdClickListener appLovinAdClickListener) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (ApplovinBanner.this.bannerView != null) {
                    ApplovinBanner.this.bannerView.setBannerHeight(50);
                    ApplovinBanner.this.appLovinAdView = new AppLovinAdView(ApplovinBanner.this.appLovinSdk, AppLovinAdSize.BANNER, ApplovinBanner.this.bannerView.getContext());
                    ApplovinBanner.this.appLovinAdView.setAdClickListener(appLovinAdClickListener);
                    ApplovinBanner.this.appLovinAdView.renderAd(appLovinAd);
                    ApplovinBanner.this.bannerView.addView(ApplovinBanner.this.appLovinAdView, new LayoutParams(-1, -1));
                    ApplovinBanner.this.listener.onBannerLoaded(ApplovinBanner.this.bannerView);
                }
            }
        });
    }
}
