package com.appodealx.applovin;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinSdk;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;

public class ApplovinInterstitial extends ApplovinFullScreenAd {
    public /* bridge */ /* synthetic */ void destroy() {
        super.destroy();
    }

    public /* bridge */ /* synthetic */ void load() {
        super.load();
    }

    ApplovinInterstitial(@NonNull String str, @NonNull AppLovinSdk appLovinSdk, @NonNull FullScreenAdListener fullScreenAdListener) {
        super(str, appLovinSdk, fullScreenAdListener);
    }

    public void show(@NonNull Activity activity) {
        if (this.appLovinAd != null) {
            AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.appLovinSdk, activity);
            create.setAdDisplayListener(this.applovinFullScreenAdListener);
            create.setAdClickListener(this.applovinFullScreenAdListener);
            create.setAdVideoPlaybackListener(this.applovinFullScreenAdListener);
            create.showAndRender(this.appLovinAd);
            return;
        }
        this.listener.onFullScreenAdFailedToShow(AdError.RendererNotReady);
    }
}
