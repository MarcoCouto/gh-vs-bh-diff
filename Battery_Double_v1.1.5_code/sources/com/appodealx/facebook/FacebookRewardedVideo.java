package com.appodealx.facebook;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.facebook.ads.RewardedVideoAd;

public class FacebookRewardedVideo extends FullScreenAdObject {
    private FullScreenAdListener fullScreenAdListener;
    private RewardedVideoAd rewardedVideoAd;

    FacebookRewardedVideo(FullScreenAdListener fullScreenAdListener2) {
        this.fullScreenAdListener = fullScreenAdListener2;
    }

    public void load(Context context, String str, String str2) {
        if (VERSION.SDK_INT < 15) {
            this.fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
            return;
        }
        this.rewardedVideoAd = new RewardedVideoAd(context, str);
        this.rewardedVideoAd.setAdListener(new FacebookRewardedVideoListener(this, this.fullScreenAdListener));
        this.rewardedVideoAd.loadAdFromBid(str2);
    }

    public void show(@NonNull Activity activity) {
        if (this.rewardedVideoAd == null || !this.rewardedVideoAd.isAdLoaded() || this.rewardedVideoAd.isAdInvalidated()) {
            this.fullScreenAdListener.onFullScreenAdFailedToShow(AdError.InternalError);
        } else {
            this.rewardedVideoAd.show();
        }
    }

    public void destroy() {
        if (this.rewardedVideoAd != null) {
            this.rewardedVideoAd.setAdListener(null);
            this.rewardedVideoAd.destroy();
            this.rewardedVideoAd = null;
        }
    }
}
