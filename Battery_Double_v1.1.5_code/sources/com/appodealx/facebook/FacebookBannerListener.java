package com.appodealx.facebook;

import android.support.annotation.NonNull;
import com.appodealx.sdk.BannerListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;

public class FacebookBannerListener implements AdListener {
    private BannerListener bannerListener;
    private FacebookBanner facebookBanner;

    public void onLoggingImpression(Ad ad) {
    }

    FacebookBannerListener(@NonNull FacebookBanner facebookBanner2, BannerListener bannerListener2) {
        this.facebookBanner = facebookBanner2;
        this.bannerListener = bannerListener2;
    }

    public void onError(Ad ad, AdError adError) {
        if (ad != null) {
            ad.destroy();
        }
        this.bannerListener.onBannerFailedToLoad(com.appodealx.sdk.AdError.NoFill);
    }

    public void onAdLoaded(Ad ad) {
        this.facebookBanner.fillContainer();
    }

    public void onAdClicked(Ad ad) {
        this.bannerListener.onBannerClicked();
    }
}
