package com.appodealx.facebook;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.facebook.ads.InterstitialAd;

public class FacebookInterstitial extends FullScreenAdObject {
    private FullScreenAdListener fullScreenAdListener;
    private InterstitialAd interstitial;

    FacebookInterstitial(FullScreenAdListener fullScreenAdListener2) {
        this.fullScreenAdListener = fullScreenAdListener2;
    }

    public void load(Context context, String str, String str2) {
        if (VERSION.SDK_INT < 15) {
            this.fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
            return;
        }
        this.interstitial = new InterstitialAd(context, str);
        this.interstitial.setAdListener(new FacebookInterstitialListener(this, this.fullScreenAdListener));
        this.interstitial.loadAdFromBid(str2);
    }

    public void show(@NonNull Activity activity) {
        if (this.interstitial == null || !this.interstitial.isAdLoaded() || this.interstitial.isAdInvalidated()) {
            this.fullScreenAdListener.onFullScreenAdFailedToShow(AdError.InternalError);
        } else {
            this.interstitial.show();
        }
    }

    public void destroy() {
        if (this.interstitial != null) {
            this.interstitial.setAdListener(null);
            this.interstitial.destroy();
            this.interstitial = null;
        }
    }
}
