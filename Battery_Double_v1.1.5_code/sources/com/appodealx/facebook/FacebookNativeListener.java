package com.appodealx.facebook;

import com.appodealx.sdk.NativeAd;
import com.appodealx.sdk.NativeAdObject;
import com.appodealx.sdk.NativeListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAdBase;
import com.facebook.ads.NativeAdListener;

public class FacebookNativeListener implements NativeAdListener {
    private FacebookNative facebookNative;
    private NativeAd nativeAd;
    private NativeListener nativeListener;

    public void onLoggingImpression(Ad ad) {
    }

    public void onMediaDownloaded(Ad ad) {
    }

    FacebookNativeListener(FacebookNative facebookNative2, NativeAd nativeAd2, NativeListener nativeListener2) {
        this.facebookNative = facebookNative2;
        this.nativeAd = nativeAd2;
        this.nativeListener = nativeListener2;
    }

    public void onError(Ad ad, AdError adError) {
        if (ad != null) {
            ad.destroy();
        }
        this.nativeListener.onNativeFailedToLoad(com.appodealx.sdk.AdError.NoFill);
    }

    public void onAdLoaded(Ad ad) {
        try {
            NativeAdObject createNativeAdObject = this.facebookNative.createNativeAdObject((NativeAdBase) ad);
            if (this.nativeAd != null) {
                this.nativeAd.setAd(createNativeAdObject);
            }
            this.nativeListener.onNativeLoaded(createNativeAdObject);
        } catch (Exception unused) {
            this.nativeListener.onNativeFailedToLoad(com.appodealx.sdk.AdError.NoFill);
        }
    }

    public void onAdClicked(Ad ad) {
        this.nativeListener.onNativeClicked();
    }
}
