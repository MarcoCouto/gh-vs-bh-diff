package com.appodealx.facebook;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

public class FacebookBanner {
    /* access modifiers changed from: private */
    public AdView adView;
    /* access modifiers changed from: private */
    public BannerListener bannerListener;
    /* access modifiers changed from: private */
    public BannerView bannerView;
    private Runnable destroyRunnable = new Runnable() {
        public void run() {
            if (FacebookBanner.this.adView != null) {
                FacebookBanner.this.adView.destroy();
                FacebookBanner.this.adView = null;
            }
            FacebookBanner.this.bannerView = null;
        }
    };
    /* access modifiers changed from: private */
    public int height;

    FacebookBanner(BannerView bannerView2, BannerListener bannerListener2, int i) {
        this.bannerView = bannerView2;
        this.bannerListener = bannerListener2;
        this.height = i;
    }

    public void load(Context context, String str, String str2) {
        AdSize adSize;
        if (VERSION.SDK_INT < 15) {
            this.bannerListener.onBannerFailedToLoad(AdError.InternalError);
            return;
        }
        this.bannerView.setDestroyRunnable(this.destroyRunnable);
        if (this.height == 50) {
            adSize = AdSize.BANNER_HEIGHT_50;
        } else {
            adSize = AdSize.BANNER_HEIGHT_90;
        }
        this.adView = new AdView(context, str, adSize);
        this.adView.setAdListener(new FacebookBannerListener(this, this.bannerListener));
        this.adView.loadAdFromBid(str2);
    }

    /* access modifiers changed from: 0000 */
    public void fillContainer() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (FacebookBanner.this.bannerView != null) {
                    FacebookBanner.this.bannerView.setBannerHeight(FacebookBanner.this.height);
                    FacebookBanner.this.bannerView.addView(FacebookBanner.this.adView);
                    FacebookBanner.this.bannerListener.onBannerLoaded(FacebookBanner.this.bannerView);
                }
            }
        });
    }
}
