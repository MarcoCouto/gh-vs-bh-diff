package com.appodealx.mytarget;

import android.support.annotation.NonNull;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;

public class MyTargetFullScreenAdListener implements InterstitialAdListener {
    private boolean finished = false;
    private FullScreenAdObject fullScreenAd;
    private FullScreenAdListener fullScreenAdListener;

    MyTargetFullScreenAdListener(@NonNull FullScreenAdObject fullScreenAdObject, FullScreenAdListener fullScreenAdListener2) {
        this.fullScreenAd = fullScreenAdObject;
        this.fullScreenAdListener = fullScreenAdListener2;
    }

    public void onLoad(@NonNull InterstitialAd interstitialAd) {
        this.fullScreenAdListener.onFullScreenAdLoaded(this.fullScreenAd);
    }

    public void onNoAd(@NonNull String str, @NonNull InterstitialAd interstitialAd) {
        this.fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.NoFill);
    }

    public void onClick(@NonNull InterstitialAd interstitialAd) {
        this.fullScreenAdListener.onFullScreenAdClicked();
    }

    public void onDismiss(@NonNull InterstitialAd interstitialAd) {
        this.fullScreenAdListener.onFullScreenAdClosed(this.finished);
    }

    public void onVideoCompleted(@NonNull InterstitialAd interstitialAd) {
        this.finished = true;
        this.fullScreenAdListener.onFullScreenAdCompleted();
    }

    public void onDisplay(@NonNull InterstitialAd interstitialAd) {
        this.fullScreenAdListener.onFullScreenAdShown();
    }
}
