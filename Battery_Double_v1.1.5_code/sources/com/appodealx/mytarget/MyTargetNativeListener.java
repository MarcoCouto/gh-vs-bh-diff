package com.appodealx.mytarget;

import android.support.annotation.NonNull;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.NativeListener;
import com.my.target.nativeads.NativeAd;
import com.my.target.nativeads.NativeAd.NativeAdListener;
import com.my.target.nativeads.banners.NativePromoBanner;

public class MyTargetNativeListener implements NativeAdListener {
    private MyTargetNative myTargetNative;
    private NativeListener nativeListener;

    public void onShow(@NonNull NativeAd nativeAd) {
    }

    public void onVideoComplete(@NonNull NativeAd nativeAd) {
    }

    public void onVideoPause(@NonNull NativeAd nativeAd) {
    }

    public void onVideoPlay(@NonNull NativeAd nativeAd) {
    }

    MyTargetNativeListener(MyTargetNative myTargetNative2, NativeListener nativeListener2) {
        this.myTargetNative = myTargetNative2;
        this.nativeListener = nativeListener2;
    }

    public void onLoad(@NonNull NativePromoBanner nativePromoBanner, @NonNull NativeAd nativeAd) {
        this.nativeListener.onNativeLoaded(this.myTargetNative.createNativeAd(nativeAd));
    }

    public void onNoAd(@NonNull String str, @NonNull NativeAd nativeAd) {
        this.nativeListener.onNativeFailedToLoad(AdError.NoFill);
    }

    public void onClick(@NonNull NativeAd nativeAd) {
        this.nativeListener.onNativeClicked();
    }
}
