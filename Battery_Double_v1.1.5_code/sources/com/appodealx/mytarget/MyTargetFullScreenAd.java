package com.appodealx.mytarget;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.my.target.ads.InterstitialAd;

public class MyTargetFullScreenAd extends FullScreenAdObject {
    private FullScreenAdListener fullScreenAdListener;
    private InterstitialAd interstitialAd;

    MyTargetFullScreenAd(FullScreenAdListener fullScreenAdListener2) {
        this.fullScreenAdListener = fullScreenAdListener2;
    }

    public void load(Context context, int i, String str) {
        this.interstitialAd = new InterstitialAd(i, context);
        this.interstitialAd.setListener(new MyTargetFullScreenAdListener(this, this.fullScreenAdListener));
        this.interstitialAd.getCustomParams().setCustomParam("bid_id", str);
        this.interstitialAd.load();
    }

    public void show(@NonNull Activity activity) {
        if (this.interstitialAd != null) {
            this.interstitialAd.show();
        } else {
            this.fullScreenAdListener.onFullScreenAdFailedToShow(AdError.InternalError);
        }
    }

    public void destroy() {
        if (this.interstitialAd != null) {
            this.interstitialAd.destroy();
            this.interstitialAd = null;
        }
    }
}
