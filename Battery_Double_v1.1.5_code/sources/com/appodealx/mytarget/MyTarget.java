package com.appodealx.mytarget;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.NativeAd;
import com.appodealx.sdk.NativeListener;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.my.target.common.MyTargetPrivacy;
import com.my.target.common.MyTargetVersion;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyTarget extends InternalAdapterInterface {
    @NonNull
    public JSONArray getBannerRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfoFromSettings(jSONObject);
    }

    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfoFromSettings(jSONObject);
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfoFromSettings(jSONObject);
    }

    @NonNull
    public JSONArray getNativeRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfoFromSettings(jSONObject);
    }

    public void loadBanner(@NonNull Activity activity, @NonNull BannerView bannerView, JSONObject jSONObject, BannerListener bannerListener) {
        try {
            int i = jSONObject.getInt("slot_id");
            String string = jSONObject.getString("bid_id");
            if (!TextUtils.isEmpty(string)) {
                new MyTargetBanner(bannerView, bannerListener).load(activity, i, string);
            } else {
                bannerListener.onBannerFailedToLoad(AdError.InternalError);
            }
        } catch (Exception unused) {
            bannerListener.onBannerFailedToLoad(AdError.InternalError);
        }
    }

    public void loadInterstitial(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        loadFullscreenAd(activity, jSONObject, fullScreenAd, fullScreenAdListener);
    }

    public void loadRewardedVideo(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        loadFullscreenAd(activity, jSONObject, fullScreenAd, fullScreenAdListener);
    }

    public void loadNative(@NonNull Activity activity, JSONObject jSONObject, @NonNull Map<String, String> map, NativeAd nativeAd, NativeListener nativeListener) {
        try {
            int i = jSONObject.getInt("slot_id");
            String string = jSONObject.getString("bid_id");
            if (!TextUtils.isEmpty(string)) {
                MyTargetNative myTargetNative = new MyTargetNative(nativeListener);
                myTargetNative.load(activity, map, i, string);
                nativeAd.setAd(myTargetNative);
                return;
            }
            nativeListener.onNativeFailedToLoad(AdError.InternalError);
        } catch (Exception unused) {
            nativeListener.onNativeFailedToLoad(AdError.InternalError);
        }
    }

    public void updateConsent(Activity activity, boolean z, boolean z2) {
        MyTargetPrivacy.setUserConsent(z);
    }

    public void updateCoppa(boolean z) {
        MyTargetPrivacy.setUserAgeRestricted(z);
    }

    @NonNull
    private JSONArray getRequestInfoFromSettings(JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", jSONObject.getString("id"));
            jSONObject3.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM));
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("slot_id", jSONObject.getInt("mailru_slot_id"));
            JSONObject optJSONObject = jSONObject.optJSONObject(RequestInfoKeys.EXTRA_PARALLEL_BIDDING_INFO);
            if (optJSONObject != null) {
                Iterator keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    jSONObject4.put(str, optJSONObject.get(str));
                }
            }
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, "my_target");
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, MyTargetVersion.VERSION);
            jSONObject2.put("appodeal", jSONObject3);
            jSONObject2.put(RequestInfoKeys.EXT, jSONObject4);
        } catch (JSONException e) {
            Log.e("Appodealx-MyTarget", e.getMessage());
        }
        jSONArray.put(jSONObject2);
        return jSONArray;
    }

    private void loadFullscreenAd(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        try {
            int i = jSONObject.getInt("slot_id");
            String string = jSONObject.getString("bid_id");
            if (!TextUtils.isEmpty(string)) {
                MyTargetFullScreenAd myTargetFullScreenAd = new MyTargetFullScreenAd(fullScreenAdListener);
                myTargetFullScreenAd.load(activity, i, string);
                fullScreenAd.setAd(myTargetFullScreenAd);
                return;
            }
            fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
        } catch (Exception unused) {
            fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
        }
    }
}
