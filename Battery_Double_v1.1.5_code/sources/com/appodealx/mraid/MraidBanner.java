package com.appodealx.mraid;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.widget.FrameLayout.LayoutParams;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;
import com.explorestack.iab.mraid.MRAIDView;
import com.explorestack.iab.mraid.MRAIDView.builder;

class MraidBanner {
    /* access modifiers changed from: private */
    public BannerView bannerView;
    private String creative;
    private Runnable destroyRunnable = new Runnable() {
        public void run() {
            if (MraidBanner.this.mraidView != null) {
                MraidBanner.this.mraidView.destroy();
                MraidBanner.this.mraidView = null;
            }
            MraidBanner.this.bannerView = null;
        }
    };
    private int height;
    private BannerListener listener;
    @VisibleForTesting
    MRAIDView mraidView;
    private int width;

    MraidBanner(@NonNull BannerView bannerView2, String str, int i, int i2, BannerListener bannerListener) {
        this.bannerView = bannerView2;
        this.creative = str;
        this.width = i;
        this.height = i2;
        this.listener = bannerListener;
    }

    /* access modifiers changed from: 0000 */
    public void prepare(Activity activity) {
        this.bannerView.setDestroyRunnable(this.destroyRunnable);
        MraidBannerListener mraidBannerListener = new MraidBannerListener(this, this.listener);
        this.mraidView = new builder(activity, this.creative, this.width, this.height).setListener(mraidBannerListener).setNativeFeatureListener(mraidBannerListener).setPreload(true).build();
        this.mraidView.load();
    }

    /* access modifiers changed from: 0000 */
    public void fillContainer() {
        if (this.mraidView == null || this.mraidView.getParent() != null) {
            this.listener.onBannerFailedToLoad(AdError.InternalError);
            return;
        }
        this.mraidView.show();
        this.bannerView.addView(this.mraidView, new LayoutParams(-1, -1));
        this.listener.onBannerLoaded(this.bannerView);
    }

    /* access modifiers changed from: 0000 */
    public MRAIDView getMraidView() {
        return this.mraidView;
    }
}
