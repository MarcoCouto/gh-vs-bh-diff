package com.appodealx.mraid;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.webkit.WebView;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.explorestack.iab.mraid.MRAIDInterstitial;
import com.explorestack.iab.mraid.MRAIDInterstitialListener;
import com.explorestack.iab.mraid.MRAIDNativeFeatureListener;
import com.explorestack.iab.utils.Utils;

public class MraidInterstitialListener implements MRAIDInterstitialListener, MRAIDNativeFeatureListener {
    private Runnable afterLoadedRunnable;
    private FullScreenAdListener fullScreenAdListener;
    /* access modifiers changed from: private */
    public MraidInterstitial mraidInterstitialAd;

    public void mraidNativeFeatureCallTel(String str) {
    }

    public void mraidNativeFeatureCreateCalendarEvent(String str) {
    }

    public void mraidNativeFeaturePlayVideo(String str) {
    }

    public void mraidNativeFeatureSendSms(String str) {
    }

    public void mraidNativeFeatureStorePicture(String str) {
    }

    MraidInterstitialListener(@NonNull MraidInterstitial mraidInterstitial, @NonNull FullScreenAdListener fullScreenAdListener2, Runnable runnable) {
        this.mraidInterstitialAd = mraidInterstitial;
        this.fullScreenAdListener = fullScreenAdListener2;
        this.afterLoadedRunnable = runnable;
    }

    public void mraidInterstitialLoaded(MRAIDInterstitial mRAIDInterstitial) {
        this.fullScreenAdListener.onFullScreenAdLoaded(this.mraidInterstitialAd);
        if (this.afterLoadedRunnable != null) {
            this.afterLoadedRunnable.run();
        }
    }

    public void mraidInterstitialShow(MRAIDInterstitial mRAIDInterstitial) {
        this.fullScreenAdListener.onFullScreenAdShown();
    }

    public void mraidInterstitialHide(MRAIDInterstitial mRAIDInterstitial) {
        this.fullScreenAdListener.onFullScreenAdClosed(true);
        if (this.mraidInterstitialAd.getShowingActivity() != null) {
            this.mraidInterstitialAd.getShowingActivity().finish();
            this.mraidInterstitialAd.getShowingActivity().overridePendingTransition(0, 0);
            this.mraidInterstitialAd.setShowingActivity(null);
        }
    }

    public void mraidInterstitialNoFill(MRAIDInterstitial mRAIDInterstitial) {
        this.fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.NoFill);
    }

    public void mraidNativeFeatureOpenBrowser(String str, WebView webView) {
        this.fullScreenAdListener.onFullScreenAdClicked();
        MraidActivity showingActivity = this.mraidInterstitialAd.getShowingActivity();
        if (!TextUtils.isEmpty(str) && showingActivity != null) {
            showingActivity.showProgressBar();
            Utils.openBrowser(showingActivity.getApplicationContext(), str, new Runnable() {
                public void run() {
                    if (MraidInterstitialListener.this.mraidInterstitialAd.getShowingActivity() != null) {
                        MraidInterstitialListener.this.mraidInterstitialAd.getShowingActivity().hideProgressBar();
                    }
                }
            });
        }
    }
}
