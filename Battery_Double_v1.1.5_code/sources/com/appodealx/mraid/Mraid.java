package com.appodealx.mraid;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.explorestack.iab.mraid.internal.MRAIDLog;
import com.explorestack.iab.mraid.internal.MRAIDLog.LOG_LEVEL;
import com.explorestack.iab.vast.VideoType;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Mraid extends InternalAdapterInterface {
    public void setLogging(boolean z) {
        if (z) {
            MRAIDLog.setLoggingLevel(LOG_LEVEL.verbose);
        } else {
            MRAIDLog.setLoggingLevel(LOG_LEVEL.none);
        }
    }

    @NonNull
    public JSONArray getBannerRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    public void loadBanner(@NonNull Activity activity, @NonNull BannerView bannerView, JSONObject jSONObject, BannerListener bannerListener) {
        String optString = jSONObject.optString("creative");
        int optInt = jSONObject.optInt("width", ModuleDescriptor.MODULE_VERSION);
        int optInt2 = jSONObject.optInt("height", 50);
        bannerView.setBannerHeight(optInt2);
        MraidBanner mraidBanner = new MraidBanner(bannerView, optString, optInt, optInt2, bannerListener);
        mraidBanner.prepare(activity);
    }

    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    public void loadInterstitial(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        loadFullScreenAd(activity, jSONObject, fullScreenAd, fullScreenAdListener, VideoType.NonRewarded);
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    public void loadRewardedVideo(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        loadFullScreenAd(activity, jSONObject, fullScreenAd, fullScreenAdListener, VideoType.Rewarded);
    }

    private void loadFullScreenAd(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener, VideoType videoType) {
        JSONObject jSONObject2 = jSONObject;
        String optString = jSONObject.optString("creative");
        int optInt = jSONObject.optInt("width");
        int optInt2 = jSONObject.optInt("height");
        long optLong = jSONObject.optLong("close_time", 0);
        MraidInterstitial mraidInterstitial = new MraidInterstitial(optString, optInt, optInt2, optLong, fullScreenAdListener, videoType);
        if (optLong > 0) {
            fullScreenAdListener.onFullScreenAdLoaded(mraidInterstitial);
        } else {
            Activity activity2 = activity;
            mraidInterstitial.prepare(activity);
        }
        fullScreenAd.setAd(mraidInterstitial);
    }

    @Nullable
    private JSONObject getRequestInfoFromSettings(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, CampaignEx.JSON_KEY_MRAID);
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, MIntegralConstans.NATIVE_VIDEO_VERSION);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", jSONObject.getString("id"));
            jSONObject3.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM));
            jSONObject2.put("appodeal", jSONObject3);
            return jSONObject2;
        } catch (JSONException e) {
            Log.e("Appodealx-Mraid", e.getMessage());
            return null;
        }
    }
}
