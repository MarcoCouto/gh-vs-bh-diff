package com.adcolony.sdk;

import org.json.JSONObject;

class k implements ad {
    k() {
        a.a("CustomMessage.controller_send", (ad) this);
    }

    public void a(ab abVar) {
        JSONObject c = abVar.c();
        final String b = u.b(c, "type");
        final String b2 = u.b(c, "message");
        at.a((Runnable) new Runnable() {
            public void run() {
                new a().a("Received custom message ").a(b2).a(" of type ").a(b).a(w.d);
                try {
                    AdColonyCustomMessageListener adColonyCustomMessageListener = (AdColonyCustomMessageListener) a.a().A().get(b);
                    if (adColonyCustomMessageListener != null) {
                        adColonyCustomMessageListener.onAdColonyCustomMessage(new AdColonyCustomMessage(b, b2));
                    }
                } catch (RuntimeException unused) {
                }
            }
        });
    }
}
