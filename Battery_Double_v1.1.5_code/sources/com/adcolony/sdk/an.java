package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import java.util.HashMap;
import org.json.JSONObject;

@SuppressLint({"UseSparseArrays"})
class an {

    /* renamed from: a reason: collision with root package name */
    final String f746a;
    private final int b;
    private HashMap<Integer, Integer> c = new HashMap<>();
    private HashMap<Integer, Integer> d = new HashMap<>();
    private HashMap<Integer, Boolean> e = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, Integer> f = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, Integer> g = new HashMap<>();
    private HashMap<String, Integer> h = new HashMap<>();
    private SoundPool i;

    an(final String str, final int i2) {
        this.f746a = str;
        this.b = i2;
        this.i = new SoundPool(50, 3, 0);
        this.i.setOnLoadCompleteListener(new OnLoadCompleteListener() {
            public void onLoadComplete(SoundPool soundPool, int i, int i2) {
                JSONObject a2 = u.a();
                u.b(a2, "id", ((Integer) an.this.f.get(Integer.valueOf(i))).intValue());
                u.a(a2, "ad_session_id", str);
                if (i2 == 0) {
                    new ab("AudioPlayer.on_ready", i2, a2).b();
                    an.this.g.put(an.this.f.get(Integer.valueOf(i)), Integer.valueOf(i));
                    return;
                }
                new ab("AudioPlayer.on_error", i2, a2).b();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(ab abVar) {
        JSONObject c2 = abVar.c();
        int load = this.i.load(u.b(c2, "filepath"), 1);
        int i2 = u.d(c2, "repeats") ? -1 : 0;
        this.f.put(Integer.valueOf(load), Integer.valueOf(u.c(c2, "id")));
        new a().a("Load audio with id = ").a(load).a(w.d);
        this.d.put(Integer.valueOf(load), Integer.valueOf(i2));
        this.e.put(Integer.valueOf(load), Boolean.valueOf(false));
    }

    /* access modifiers changed from: 0000 */
    public void b(ab abVar) {
        this.i.unload(((Integer) this.g.get(Integer.valueOf(u.c(abVar.c(), "id")))).intValue());
    }

    /* access modifiers changed from: 0000 */
    public void c(ab abVar) {
        int intValue = ((Integer) this.g.get(Integer.valueOf(u.c(abVar.c(), "id")))).intValue();
        if (!((Boolean) this.e.get(Integer.valueOf(intValue))).booleanValue()) {
            int play = this.i.play(intValue, 1.0f, 1.0f, 0, ((Integer) this.d.get(Integer.valueOf(intValue))).intValue(), 1.0f);
            if (play != 0) {
                this.c.put(Integer.valueOf(intValue), Integer.valueOf(play));
                return;
            }
            JSONObject a2 = u.a();
            u.b(a2, "id", u.c(abVar.c(), "id"));
            u.a(a2, "ad_session_id", this.f746a);
            new ab("AudioPlayer.on_error", this.b, a2).b();
            return;
        }
        this.i.resume(((Integer) this.c.get(Integer.valueOf(intValue))).intValue());
    }

    /* access modifiers changed from: 0000 */
    public void d(ab abVar) {
        int intValue = ((Integer) this.g.get(Integer.valueOf(u.c(abVar.c(), "id")))).intValue();
        this.i.pause(((Integer) this.c.get(Integer.valueOf(intValue))).intValue());
        this.e.put(Integer.valueOf(intValue), Boolean.valueOf(true));
    }

    /* access modifiers changed from: 0000 */
    public void e(ab abVar) {
        this.i.stop(((Integer) this.c.get(this.g.get(Integer.valueOf(u.c(abVar.c(), "id"))))).intValue());
    }

    /* access modifiers changed from: 0000 */
    public SoundPool a() {
        return this.i;
    }
}
