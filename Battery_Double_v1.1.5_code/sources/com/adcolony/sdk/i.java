package com.adcolony.sdk;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings.System;
import com.google.android.exoplayer2.util.MimeTypes;
import org.json.JSONObject;

class i extends ContentObserver {

    /* renamed from: a reason: collision with root package name */
    private AudioManager f899a;
    private AdColonyInterstitial b;

    public boolean deliverSelfNotifications() {
        return false;
    }

    public i(Handler handler, AdColonyInterstitial adColonyInterstitial) {
        super(handler);
        Context c = a.c();
        if (c != null) {
            this.f899a = (AudioManager) c.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            this.b = adColonyInterstitial;
            c.getApplicationContext().getContentResolver().registerContentObserver(System.CONTENT_URI, true, this);
        }
    }

    public void onChange(boolean z) {
        if (this.f899a != null && this.b != null && this.b.d() != null) {
            double streamVolume = (double) ((((float) this.f899a.getStreamVolume(3)) / 15.0f) * 100.0f);
            JSONObject a2 = u.a();
            u.a(a2, "audio_percentage", streamVolume);
            u.a(a2, "ad_session_id", this.b.d().b());
            u.b(a2, "id", this.b.d().d());
            new ab("AdContainer.on_audio_change", this.b.d().c(), a2).b();
            new a().a("Volume changed to ").a(streamVolume).a(w.d);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        Context c = a.c();
        if (c != null) {
            c.getApplicationContext().getContentResolver().unregisterContentObserver(this);
        }
        this.b = null;
        this.f899a = null;
    }
}
