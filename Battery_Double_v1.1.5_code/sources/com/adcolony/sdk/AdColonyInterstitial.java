package com.adcolony.sdk;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import org.json.JSONObject;

public class AdColonyInterstitial {
    public static final int ADCOLONY_IAP_ENGAGEMENT_END_CARD = 0;
    public static final int ADCOLONY_IAP_ENGAGEMENT_OVERLAY = 1;

    /* renamed from: a reason: collision with root package name */
    private AdColonyInterstitialListener f720a;
    private c b;
    private AdColonyAdOptions c;
    private ag d;
    private int e;
    private String f;
    private String g;
    private String h;
    private int i;
    private String j;
    private boolean k;
    private boolean l;
    private boolean m;

    AdColonyInterstitial(String str, AdColonyInterstitialListener adColonyInterstitialListener, String str2) {
        this.f720a = adColonyInterstitialListener;
        this.j = str2;
        this.f = str;
    }

    public boolean show() {
        if (!a.b()) {
            return false;
        }
        j a2 = a.a();
        if (this.l) {
            new a().a("This ad object has already been shown. Please request a new ad ").a("via AdColony.requestInterstitial.").a(w.e);
            return false;
        } else if (this.k) {
            new a().a("This ad object has expired. Please request a new ad via AdColony").a(".requestInterstitial.").a(w.e);
            return false;
        } else if (a2.x()) {
            new a().a("Can not show ad while an interstitial is already active.").a(w.e);
            return false;
        } else if (a((AdColonyZone) a2.f().get(this.j))) {
            new a().a("Skipping show()").a(w.d);
            return false;
        } else {
            JSONObject a3 = u.a();
            u.a(a3, "zone_id", this.j);
            u.b(a3, "type", 0);
            u.a(a3, "id", this.f);
            if (this.c != null) {
                u.b(a3, "pre_popup", this.c.f710a);
                u.b(a3, "post_popup", this.c.b);
            }
            AdColonyZone adColonyZone = (AdColonyZone) a2.f().get(this.j);
            if (adColonyZone != null && adColonyZone.isRewarded() && a2.i() == null) {
                new a().a("Rewarded ad: show() called with no reward listener set.").a(w.e);
            }
            new ab("AdSession.launch_ad_unit", 1, a3).b();
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(AdColonyZone adColonyZone) {
        if (adColonyZone == null) {
            return true;
        }
        if (adColonyZone.getPlayFrequency() <= 1) {
            return false;
        }
        if (adColonyZone.b() == 0) {
            adColonyZone.a(adColonyZone.getPlayFrequency() - 1);
            return false;
        }
        adColonyZone.a(adColonyZone.b() - 1);
        return true;
    }

    public boolean cancel() {
        if (this.b != null) {
            Context c2 = a.c();
            if (c2 == null || (c2 instanceof AdColonyInterstitialActivity)) {
                JSONObject a2 = u.a();
                u.a(a2, "id", this.b.b());
                new ab("AdSession.on_request_close", this.b.c(), a2).b();
                return true;
            }
        }
        return false;
    }

    public AdColonyInterstitialListener getListener() {
        return this.f720a;
    }

    public void setListener(@NonNull AdColonyInterstitialListener adColonyInterstitialListener) {
        this.f720a = adColonyInterstitialListener;
    }

    public String getZoneID() {
        return this.j;
    }

    public boolean isExpired() {
        return this.k || this.l;
    }

    public boolean destroy() {
        a.a().l().c().remove(this.f);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        Context c2 = a.c();
        if (c2 == null || !a.b()) {
            return false;
        }
        a.a().c(true);
        a.a().a(this.b);
        a.a().a(this);
        new a().a("Launching fullscreen Activity via AdColonyInterstitial's launch ").a("method.").a(w.b);
        Intent intent = new Intent(c2, AdColonyInterstitialActivity.class);
        if (c2 instanceof Application) {
            intent.addFlags(268435456);
        }
        c2.startActivity(intent);
        this.l = true;
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        if (jSONObject.length() > 0) {
            this.d = new ag(jSONObject, this.f);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        this.b = cVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.k = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyAdOptions adColonyAdOptions) {
        this.c = adColonyAdOptions;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        this.i = i2;
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        this.e = i2;
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        if (this.g == null) {
            return "";
        }
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.g = str;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        if (this.h == null) {
            return "";
        }
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.h = str;
    }

    /* access modifiers changed from: 0000 */
    public c d() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public int e() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public String f() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean g() {
        return this.d != null;
    }

    /* access modifiers changed from: 0000 */
    public ag h() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public boolean i() {
        return this.m;
    }
}
