package com.adcolony.sdk;

import com.explorestack.iab.vast.VastError;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public class AdColonyAdSize {
    public static final AdColonyAdSize BANNER = new AdColonyAdSize(ModuleDescriptor.MODULE_VERSION, 50);
    public static final AdColonyAdSize LEADERBOARD = new AdColonyAdSize(728, 90);
    public static final AdColonyAdSize MEDIUM_RECTANGLE = new AdColonyAdSize(VastError.ERROR_CODE_GENERAL_WRAPPER, 250);
    public static final AdColonyAdSize SKYSCRAPER = new AdColonyAdSize(160, 600);

    /* renamed from: a reason: collision with root package name */
    int f711a;
    int b;

    public AdColonyAdSize(int i, int i2) {
        this.f711a = i;
        this.b = i2;
    }

    public int getWidth() {
        return this.f711a;
    }

    public int getHeight() {
        return this.b;
    }
}
