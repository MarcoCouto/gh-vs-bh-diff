package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.iab.omid.library.adcolony.Omid;
import com.iab.omid.library.adcolony.adsession.Partner;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.tapjoy.TapjoyConstants;
import io.realm.BuildConfig;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

class j implements a {
    private static volatile String H = "";

    /* renamed from: a reason: collision with root package name */
    static final String f900a = "026ae9c9824b3e483fa6c71fa88f57ae27816141";
    static final String b = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5";
    static String e = "https://adc3-launch.adcolony.com/v4/launch";
    private HashMap<String, AdColonyZone> A = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, av> B = new HashMap<>();
    private String C;
    private String D;
    private String E;
    private String F;
    private String G = "";
    private boolean I;
    /* access modifiers changed from: private */
    public boolean J;
    private boolean K;
    /* access modifiers changed from: private */
    public boolean L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    /* access modifiers changed from: private */
    public boolean Q;
    /* access modifiers changed from: private */
    public boolean R;
    /* access modifiers changed from: private */
    public boolean S;
    private int T;
    /* access modifiers changed from: private */
    public int U = 1;
    private final int V = 120;
    private ActivityLifecycleCallbacks W;
    /* access modifiers changed from: private */
    public Partner X = null;
    l c;
    af d;
    boolean f;
    private k g;
    /* access modifiers changed from: private */
    public ac h;
    /* access modifiers changed from: private */
    public o i;
    /* access modifiers changed from: private */
    public al j;
    private d k;
    /* access modifiers changed from: private */
    public m l;
    private r m;
    private aq n;
    /* access modifiers changed from: private */
    public ao o;
    private y p;
    private c q;
    private AdColonyAdView r;
    private AdColonyInterstitial s;
    /* access modifiers changed from: private */
    public AdColonyRewardListener t;
    private HashMap<String, AdColonyCustomMessageListener> u = new HashMap<>();
    /* access modifiers changed from: private */
    public AdColonyAppOptions v;
    /* access modifiers changed from: private */
    public ab w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public ab y;
    private JSONObject z;

    j() {
    }

    /* access modifiers changed from: 0000 */
    public Context a() {
        return a.c();
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0115  */
    public void a(AdColonyAppOptions adColonyAppOptions, boolean z2) {
        boolean z3;
        this.K = z2;
        this.v = adColonyAppOptions;
        this.h = new ac();
        this.g = new k();
        this.i = new o();
        this.i.a();
        this.j = new al();
        this.j.a();
        this.k = new d();
        this.k.a();
        this.l = new m();
        this.m = new r();
        this.m.a();
        this.p = new y();
        y yVar = this.p;
        y.c();
        this.o = new ao();
        this.o.a();
        this.n = new aq();
        this.n.a();
        this.c = new l();
        this.c.e();
        this.d = new af();
        this.C = this.d.c();
        AdColony.a(a.c(), adColonyAppOptions);
        boolean z4 = false;
        if (!z2) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.o.g());
            sb.append(f900a);
            this.N = new File(sb.toString()).exists();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.o.g());
            sb2.append(b);
            this.O = new File(sb2.toString()).exists();
            if (this.N && this.O) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.o.g());
                sb3.append(f900a);
                if (u.b(u.c(sb3.toString()), GeneralPropertiesWorker.SDK_VERSION).equals(this.c.F())) {
                    z3 = true;
                    this.M = z3;
                    if (this.N) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append(this.o.g());
                        sb4.append(f900a);
                        this.z = u.c(sb4.toString());
                        b(this.z);
                    }
                    e(this.M);
                    I();
                }
            }
            z3 = false;
            this.M = z3;
            if (this.N) {
            }
            e(this.M);
            I();
        }
        a.a("Module.load", (ad) new ad() {
            public void a(ab abVar) {
                j.this.a(abVar);
            }
        });
        a.a("Module.unload", (ad) new ad() {
            public void a(ab abVar) {
                j.this.f(abVar);
            }
        });
        a.a("AdColony.on_configured", (ad) new ad() {
            public void a(ab abVar) {
                j.this.L = true;
                if (j.this.Q) {
                    JSONObject a2 = u.a();
                    JSONObject a3 = u.a();
                    u.a(a3, TapjoyConstants.TJC_APP_VERSION_NAME, at.b());
                    u.a(a2, "app_bundle_info", a3);
                    new ab("AdColony.on_update", 1, a2).b();
                    j.this.Q = false;
                }
                if (j.this.R) {
                    new ab("AdColony.on_install", 1).b();
                }
                if (y.l != null) {
                    y.l.b(u.b(abVar.c(), "app_session_id"));
                }
                if (AdColonyEventTracker.b()) {
                    AdColonyEventTracker.a();
                }
                int a4 = u.a(abVar.c(), "concurrent_requests", 4);
                if (a4 != j.this.i.b()) {
                    j.this.i.a(a4);
                }
                j.this.E();
            }
        });
        a.a("AdColony.get_app_info", (ad) new ad() {
            public void a(ab abVar) {
                j.this.g(abVar);
            }
        });
        a.a("AdColony.v4vc_reward", (ad) new ad() {
            public void a(ab abVar) {
                j.this.d(abVar);
            }
        });
        a.a("AdColony.zone_info", (ad) new ad() {
            public void a(ab abVar) {
                j.this.e(abVar);
            }
        });
        a.a("AdColony.probe_launch_server", (ad) new ad() {
            public void a(ab abVar) {
                j.this.a(true, true);
            }
        });
        a.a("Crypto.sha1", (ad) new ad() {
            public void a(ab abVar) {
                JSONObject a2 = u.a();
                u.a(a2, "sha1", at.c(u.b(abVar.c(), "data")));
                abVar.a(a2).b();
            }
        });
        a.a("Crypto.crc32", (ad) new ad() {
            public void a(ab abVar) {
                JSONObject a2 = u.a();
                u.b(a2, "crc32", at.b(u.b(abVar.c(), "data")));
                abVar.a(a2).b();
            }
        });
        a.a("Crypto.uuid", (ad) new ad() {
            public void a(ab abVar) {
                int c = u.c(abVar.c(), "number");
                JSONObject a2 = u.a();
                u.a(a2, "uuids", at.a(c));
                abVar.a(a2).b();
            }
        });
        a.a("Device.query_advertiser_info", (ad) new ad() {
            public void a(final ab abVar) {
                final Context c = a.c();
                if (c != null) {
                    at.b.execute(new Runnable() {
                        public void run() {
                            j.this.a(c, abVar);
                        }
                    });
                }
            }
        });
        a.a("AdColony.controller_version", (ad) new ad() {
            public void a(ab abVar) {
                j.this.m().b(u.b(abVar.c(), "version"));
                if (y.l != null) {
                    y.l.a(j.this.m().h());
                }
                new a().a("Controller version: ").a(j.this.m().h()).a(w.d);
            }
        });
        int a2 = at.a(this.o);
        this.Q = a2 == 1;
        if (a2 == 2) {
            z4 = true;
        }
        this.R = z4;
        at.a((Runnable) new Runnable() {
            public void run() {
                Context c = a.c();
                if (!j.this.S && c != null) {
                    try {
                        j.this.S = Omid.activateWithOmidApiVersion(Omid.getVersion(), c.getApplicationContext());
                    } catch (IllegalArgumentException unused) {
                        new a().a("IllegalArgumentException when activating Omid").a(w.h);
                        j.this.S = false;
                    }
                }
                if (j.this.S && j.this.X == null) {
                    try {
                        j.this.X = Partner.createPartner("AdColony", BuildConfig.VERSION_NAME);
                    } catch (IllegalArgumentException unused2) {
                        new a().a("IllegalArgumentException when creating Omid Partner").a(w.h);
                        j.this.S = false;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void E() {
        JSONObject a2 = u.a();
        u.a(a2, "type", "AdColony.on_configuration_completed");
        JSONArray jSONArray = new JSONArray();
        for (String put : f().keySet()) {
            jSONArray.put(put);
        }
        JSONObject a3 = u.a();
        u.a(a3, "zone_ids", jSONArray);
        u.a(a2, "message", a3);
        new ab("CustomMessage.controller_send", 0, a2).b();
    }

    private boolean e(boolean z2) {
        return a(z2, false);
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.G;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject c() {
        return this.z;
    }

    /* access modifiers changed from: private */
    public boolean a(boolean z2, boolean z3) {
        if (!a.d()) {
            return false;
        }
        this.P = z3;
        this.M = z2;
        if (z2 && !z3 && !H()) {
            return false;
        }
        F();
        return true;
    }

    /* access modifiers changed from: private */
    public void F() {
        new Thread(new Runnable() {
            public void run() {
                JSONObject a2 = u.a();
                u.a(a2, "url", j.e);
                u.a(a2, Param.CONTENT_TYPE, "application/json");
                u.a(a2, "content", j.this.m().J().toString());
                new a().a("Launch: ").a(j.this.m().J().toString()).a(w.b);
                new a().a("Saving Launch to ").a(j.this.o.g()).a(j.f900a).a(w.d);
                j.this.i.a(new n(new ab("WebServices.post", 0, a2), j.this));
            }
        }).start();
    }

    private boolean a(JSONObject jSONObject) {
        if (!this.M) {
            new a().a("Non-standard launch. Downloading new controller.").a(w.f);
            return true;
        } else if (this.z != null && u.b(u.f(this.z, "controller"), "sha1").equals(u.b(u.f(jSONObject, "controller"), "sha1"))) {
            return false;
        } else {
            new a().a("Controller sha1 does not match, downloading new controller.").a(w.f);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void f(ab abVar) {
        a(u.c(abVar.c(), "id"));
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        this.K = z2;
    }

    /* access modifiers changed from: private */
    public void g(ab abVar) {
        JSONObject jSONObject = this.v.d;
        u.a(jSONObject, "app_id", this.v.f716a);
        u.a(jSONObject, "zone_ids", this.v.c);
        JSONObject a2 = u.a();
        u.a(a2, "options", jSONObject);
        abVar.a(a2).b();
    }

    /* access modifiers changed from: 0000 */
    public boolean a(final ab abVar) {
        final Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        try {
            int c3 = abVar.c().has("id") ? u.c(abVar.c(), "id") : 0;
            if (c3 <= 0) {
                c3 = this.h.d();
            }
            a(c3);
            final boolean d2 = u.d(abVar.c(), "is_display_module");
            at.a((Runnable) new Runnable() {
                public void run() {
                    av avVar = new av(c2.getApplicationContext(), j.this.h.d(), d2);
                    avVar.a(true, abVar);
                    j.this.B.put(Integer.valueOf(avVar.a()), avVar);
                }
            });
            return true;
        } catch (RuntimeException e2) {
            a aVar = new a();
            StringBuilder sb = new StringBuilder();
            sb.append(e2.toString());
            sb.append(": during WebView initialization.");
            aVar.a(sb.toString()).a(" Disabling AdColony.").a(w.g);
            AdColony.disable();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(ab abVar) {
        this.w = abVar;
    }

    /* access modifiers changed from: 0000 */
    public void c(ab abVar) {
        this.y = abVar;
    }

    private void G() {
        if (a.a().k().e()) {
            this.T++;
            int i2 = 120;
            if (this.U * this.T <= 120) {
                i2 = this.T * this.U;
            }
            this.U = i2;
            at.a((Runnable) new Runnable() {
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if (a.a().k().e()) {
                                j.this.F();
                            }
                        }
                    }, (long) (j.this.U * 1000));
                }
            });
            return;
        }
        new a().a("Max launch server download attempts hit, or AdColony is no longer").a(" active.").a(w.f);
    }

    public void a(n nVar, ab abVar, Map<String, List<String>> map) {
        if (nVar.f936a.equals(e)) {
            if (nVar.c) {
                new a().a("Launch: ").a(nVar.b).a(w.b);
                JSONObject a2 = u.a(nVar.b, "Parsing launch response");
                u.a(a2, GeneralPropertiesWorker.SDK_VERSION, m().F());
                StringBuilder sb = new StringBuilder();
                sb.append(this.o.g());
                sb.append(f900a);
                u.h(a2, sb.toString());
                if (!c(a2)) {
                    if (!this.M) {
                        new a().a("Incomplete or disabled launch server response. ").a("Disabling AdColony until next launch.").a(w.g);
                        a(true);
                    }
                    return;
                }
                if (a(a2)) {
                    new a().a("Controller missing or out of date. Downloading controller").a(w.d);
                    JSONObject a3 = u.a();
                    u.a(a3, "url", this.D);
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(this.o.g());
                    sb2.append(b);
                    u.a(a3, "filepath", sb2.toString());
                    this.i.a(new n(new ab("WebServices.download", 0, a3), this));
                }
                this.z = a2;
            } else {
                G();
            }
        } else if (nVar.f936a.equals(this.D)) {
            if (!b(this.E) && !av.f795a) {
                new a().a("Downloaded controller sha1 does not match, retrying.").a(w.e);
                G();
            } else if (!this.M && !this.P) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        boolean l = j.this.H();
                        a aVar = new a();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Loaded library. Success=");
                        sb.append(l);
                        aVar.a(sb.toString()).a(w.b);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean H() {
        this.h.a();
        return true;
    }

    private boolean b(String str) {
        Context c2 = a.c();
        if (c2 != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(c2.getFilesDir().getAbsolutePath());
            sb.append("/adc3/");
            sb.append(b);
            File file = new File(sb.toString());
            if (file.exists()) {
                return at.a(str, file);
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(Context context, ab abVar) {
        Info info;
        if (context == null) {
            return false;
        }
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(context);
        } catch (NoClassDefFoundError unused) {
            new a().a("Google Play Services ads dependencies are missing. Collecting ").a("Android ID instead of Advertising ID.").a(w.e);
            return false;
        } catch (NoSuchMethodError unused2) {
            new a().a("Google Play Services is out of date, please update to GPS 4.0+. ").a("Collecting Android ID instead of Advertising ID.").a(w.e);
            info = null;
        } catch (Exception e2) {
            e2.printStackTrace();
            if (Build.MANUFACTURER.equals("Amazon")) {
                return false;
            }
            new a().a("Advertising ID is not available. Collecting Android ID instead of").a(" Advertising ID.").a(w.e);
            return false;
        }
        if (info == null) {
            return false;
        }
        m().a(info.getId());
        y.l.g.put("advertisingId", m().c());
        m().b(info.isLimitAdTrackingEnabled());
        m().a(true);
        if (abVar != null) {
            JSONObject a2 = u.a();
            u.a(a2, "advertiser_id", m().c());
            u.b(a2, "limit_ad_tracking", m().g());
            abVar.a(a2).b();
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyAppOptions adColonyAppOptions) {
        synchronized (this.k.c()) {
            for (Entry value : this.k.c().entrySet()) {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) value.getValue();
                AdColonyInterstitialListener listener = adColonyInterstitial.getListener();
                adColonyInterstitial.a(true);
                if (listener != null) {
                    listener.onExpiring(adColonyInterstitial);
                }
            }
            this.k.c().clear();
        }
        this.L = false;
        a(1);
        this.A.clear();
        this.v = adColonyAppOptions;
        this.h.a();
        a(true, true);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i2) {
        ae a2 = this.h.a(i2);
        final av avVar = (av) this.B.remove(Integer.valueOf(i2));
        boolean z2 = false;
        if (a2 == null) {
            return false;
        }
        if (avVar != null && avVar.l()) {
            z2 = true;
        }
        AnonymousClass10 r2 = new Runnable() {
            public void run() {
                if (avVar != null && avVar.m()) {
                    avVar.loadUrl("about:blank");
                    avVar.clearCache(true);
                    avVar.removeAllViews();
                    avVar.a(true);
                    avVar.destroy();
                }
                if (j.this.y != null) {
                    j.this.y.b();
                    j.this.y = null;
                    j.this.x = false;
                }
            }
        };
        if (z2) {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            new Handler().postDelayed(r2, 1000);
        } else {
            r2.run();
        }
        return true;
    }

    private void b(JSONObject jSONObject) {
        if (!av.f795a) {
            JSONObject f2 = u.f(jSONObject, "logging");
            y.k = u.a(f2, "send_level", 1);
            y.f982a = u.d(f2, "log_private");
            y.i = u.a(f2, "print_level", 3);
            this.p.a(u.g(f2, "modules"));
        }
        m().a(u.f(jSONObject, TtmlNode.TAG_METADATA));
        this.G = u.b(u.f(jSONObject, "controller"), "version");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append(r3.o.g());
        r1.append(f900a);
        new java.io.File(r1.toString()).delete();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0047 */
    private boolean c(JSONObject jSONObject) {
        if (jSONObject == null) {
            new a().a("Launch response verification failed - response is null or unknown").a(w.d);
            return false;
        }
        JSONObject f2 = u.f(jSONObject, "controller");
        this.D = u.b(f2, "url");
        this.E = u.b(f2, "sha1");
        this.F = u.b(jSONObject, "status");
        H = u.b(jSONObject, "pie");
        if (AdColonyEventTracker.b()) {
            AdColonyEventTracker.a();
        }
        b(jSONObject);
        if (this.F.equals("disable") && !av.f795a) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(this.o.g());
                sb.append(b);
                new File(sb.toString()).delete();
            } catch (Exception unused) {
            }
            new a().a("Launch server response with disabled status. Disabling AdColony ").a("until next launch.").a(w.f);
            AdColony.disable();
            return false;
        } else if ((!this.D.equals("") && !this.F.equals("")) || av.f795a) {
            return true;
        } else {
            new a().a("Missing controller status or URL. Disabling AdColony until next ").a("launch.").a(w.g);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean d(final ab abVar) {
        if (this.t == null) {
            return false;
        }
        at.a((Runnable) new Runnable() {
            public void run() {
                j.this.t.onReward(new AdColonyReward(abVar));
            }
        });
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void e(ab abVar) {
        AdColonyZone adColonyZone;
        if (this.K) {
            new a().a("AdColony is disabled. Ignoring zone_info message.").a(w.f);
            return;
        }
        String b2 = u.b(abVar.c(), "zone_id");
        if (this.A.containsKey(b2)) {
            adColonyZone = (AdColonyZone) this.A.get(b2);
        } else {
            AdColonyZone adColonyZone2 = new AdColonyZone(b2);
            this.A.put(b2, adColonyZone2);
            adColonyZone = adColonyZone2;
        }
        adColonyZone.a(abVar);
    }

    private void I() {
        Application application;
        Context c2 = a.c();
        if (c2 != null && this.W == null && VERSION.SDK_INT > 14) {
            this.W = new ActivityLifecycleCallbacks() {
                public void onActivityDestroyed(Activity activity) {
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                }

                public void onActivityStarted(Activity activity) {
                }

                public void onActivityStopped(Activity activity) {
                }

                public void onActivityResumed(Activity activity) {
                    a.b = true;
                    a.a((Context) activity);
                    Context c = a.c();
                    if (c == null || !j.this.j.c() || !(c instanceof b) || ((b) c).g) {
                        new a().a("onActivityResumed() Activity Lifecycle Callback").a(w.d);
                        a.a((Context) activity);
                        if (j.this.w != null) {
                            j.this.w.a(j.this.w.c()).b();
                            j.this.w = null;
                        }
                        j.this.J = false;
                        j.this.j.d(true);
                        j.this.j.e(true);
                        j.this.j.f(false);
                        if (j.this.f && !j.this.j.e()) {
                            j.this.j.a(true);
                        }
                        j.this.l.a();
                        if (y.l == null || y.l.d == null || y.l.d.isShutdown() || y.l.d.isTerminated()) {
                            AdColony.a((Context) activity, a.a().v);
                        }
                        return;
                    }
                    new a().a("Ignoring onActivityResumed").a(w.d);
                }

                public void onActivityPaused(Activity activity) {
                    a.b = false;
                    j.this.j.d(false);
                    j.this.j.e(true);
                    a.a().m().I();
                }

                public void onActivityCreated(Activity activity, Bundle bundle) {
                    if (!j.this.j.e()) {
                        j.this.j.a(true);
                    }
                    a.a((Context) activity);
                }
            };
            if (c2 instanceof Application) {
                application = (Application) c2;
            } else {
                application = ((Activity) c2).getApplication();
            }
            application.registerActivityLifecycleCallbacks(this.W);
        }
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAppOptions d() {
        if (this.v == null) {
            this.v = new AdColonyAppOptions();
        }
        return this.v;
    }

    /* access modifiers changed from: 0000 */
    public boolean e() {
        return this.v != null;
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull AdColonyAppOptions adColonyAppOptions) {
        this.v = adColonyAppOptions;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, AdColonyZone> f() {
        return this.A;
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z2) {
        this.J = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean g() {
        return this.J;
    }

    /* access modifiers changed from: 0000 */
    public boolean h() {
        return this.K;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyRewardListener i() {
        return this.t;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyRewardListener adColonyRewardListener) {
        this.t = adColonyRewardListener;
    }

    /* access modifiers changed from: 0000 */
    public r j() {
        if (this.m == null) {
            this.m = new r();
            this.m.a();
        }
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public al k() {
        if (this.j == null) {
            this.j = new al();
            this.j.a();
        }
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public d l() {
        if (this.k == null) {
            this.k = new d();
            this.k.a();
        }
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public l m() {
        if (this.c == null) {
            this.c = new l();
            this.c.e();
        }
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public aq n() {
        if (this.n == null) {
            this.n = new aq();
            this.n.a();
        }
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    public ao o() {
        if (this.o == null) {
            this.o = new ao();
            this.o.a();
        }
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    public af p() {
        if (this.d == null) {
            this.d = new af();
        }
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public ac q() {
        if (this.h == null) {
            this.h = new ac();
            this.h.a();
        }
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public m r() {
        if (this.l == null) {
            this.l = new m();
        }
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public o s() {
        if (this.i == null) {
            this.i = new o();
        }
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public c t() {
        return this.q;
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        this.q = cVar;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAdView u() {
        return this.r;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyAdView adColonyAdView) {
        this.r = adColonyAdView;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyInterstitial v() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyInterstitial adColonyInterstitial) {
        this.s = adColonyInterstitial;
    }

    /* access modifiers changed from: 0000 */
    public String w() {
        return this.C;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.C = str;
    }

    /* access modifiers changed from: 0000 */
    public boolean x() {
        return this.I;
    }

    /* access modifiers changed from: 0000 */
    public void c(boolean z2) {
        this.I = z2;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, av> y() {
        return this.B;
    }

    /* access modifiers changed from: 0000 */
    public boolean z() {
        return this.x;
    }

    /* access modifiers changed from: 0000 */
    public void d(boolean z2) {
        this.x = z2;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, AdColonyCustomMessageListener> A() {
        return this.u;
    }

    /* access modifiers changed from: 0000 */
    public boolean B() {
        return this.L;
    }

    static String C() {
        return H;
    }

    /* access modifiers changed from: 0000 */
    public Partner D() {
        return this.X;
    }
}
