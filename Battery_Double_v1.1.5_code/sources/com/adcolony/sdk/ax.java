package com.adcolony.sdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

class ax {

    /* renamed from: a reason: collision with root package name */
    private static int f811a;
    private static int b;
    private static int c;
    private static int d;

    private static class a {
        private a() {
        }

        static int a(WindowManager windowManager) {
            Point point = new Point();
            windowManager.getDefaultDisplay().getSize(point);
            return point.y;
        }
    }

    private static class b {
        private b() {
        }

        static int a(WindowManager windowManager) {
            Point point = new Point();
            windowManager.getDefaultDisplay().getSize(point);
            return point.x;
        }
    }

    ax() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:79:0x0138 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0139  */
    static float a(View view, Context context, boolean z, boolean z2, boolean z3, boolean z4) {
        float f;
        float f2;
        if (view == null || context == null || view.getVisibility() != 0 || a(view) == 0.0f || !z) {
            return 0.0f;
        }
        if (z3 && (context instanceof Activity) && !((Activity) context).hasWindowFocus() && !z4) {
            return 0.0f;
        }
        if (view.getHeight() > 0 && view.getWidth() > 0) {
            float height = (float) (view.getHeight() * view.getWidth());
            Rect rect = new Rect();
            boolean globalVisibleRect = view.getGlobalVisibleRect(rect);
            int[] iArr = {-1, -1};
            view.getLocationInWindow(iArr);
            int[] iArr2 = {-1, -1};
            iArr2[0] = iArr[0] + view.getWidth();
            iArr2[1] = iArr[1] + view.getHeight();
            int a2 = a(context);
            int b2 = b(context);
            if (iArr2[0] >= 0 && iArr2[1] >= 0 && iArr[0] <= b2 && iArr[1] <= a2 && ((rect.top != 0 || iArr[1] <= a2 / 2) && globalVisibleRect)) {
                float height2 = (float) (rect.height() * rect.width());
                if (height2 > 0.0f) {
                    if (z2) {
                        try {
                            float a3 = a(view, rect, height2, false);
                            if (a3 > 0.0f && a3 <= height2) {
                                height2 -= a3;
                            }
                        } catch (Exception unused) {
                        }
                    }
                    float f3 = (height2 / height) * 100.0f;
                    if (f3 < 0.0f) {
                        return 0.0f;
                    }
                    if (f3 > 100.0f) {
                        return 100.0f;
                    }
                    return f3;
                }
            }
        } else if (view.getLayoutParams().height == -2) {
            int[] iArr3 = {-1, -1};
            view.getLocationInWindow(iArr3);
            int[] iArr4 = {-1, -1};
            iArr4[0] = iArr3[0] + view.getWidth();
            iArr4[1] = iArr3[1] + 1;
            Rect rect2 = new Rect(iArr3[0], iArr3[1], iArr4[0], iArr4[1]);
            int a4 = a(context);
            int b3 = b(context);
            if (iArr4[0] < 0 || iArr4[1] < 0 || iArr3[0] > b3 || iArr3[1] > a4 || (rect2.top == 0 && iArr3[1] > a4 / 2)) {
                return 0.0f;
            }
            float height3 = (float) (rect2.height() * rect2.width());
            if (z2) {
                try {
                    float a5 = a(view, rect2, height3, true);
                    if (a5 > 0.0f && a5 <= height3) {
                        f = height3 - a5;
                        f2 = (f / height3) * 100.0f;
                        if (f2 >= 0.0f) {
                            return 0.0f;
                        }
                        if (f2 > 100.0f) {
                            return 100.0f;
                        }
                        return f2;
                    }
                } catch (Exception unused2) {
                }
            }
            f = height3;
            f2 = (f / height3) * 100.0f;
            if (f2 >= 0.0f) {
            }
        }
        return 0.0f;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0063, code lost:
        return r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0131, code lost:
        r11 = false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0071 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0072  */
    private static float a(View view, Rect rect, float f, boolean z) {
        View view2;
        boolean z2;
        LinkedList linkedList = new LinkedList();
        LinkedList linkedList2 = new LinkedList();
        LinkedList linkedList3 = new LinkedList();
        LinkedList linkedList4 = new LinkedList();
        ArrayList arrayList = new ArrayList();
        linkedList3.add(rect);
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        View rootView = view.getRootView();
        try {
            view2 = ((Activity) view.getContext()).findViewById(16908290);
        } catch (Exception unused) {
            view2 = null;
        }
        while (true) {
            float f2 = 0.0f;
            if (viewGroup == null || viewGroup.getParent() == rootView) {
                if (viewGroup != null) {
                    return f;
                }
                Iterator it = linkedList.iterator();
                while (it.hasNext()) {
                    View view3 = (View) it.next();
                    ViewGroup viewGroup2 = (ViewGroup) view3.getParent();
                    if (viewGroup2 == null) {
                        return f;
                    }
                    if (!"viewpager".equalsIgnoreCase(viewGroup2.getClass().getSimpleName())) {
                        int indexOfChild = viewGroup2.indexOfChild(view3);
                        if (indexOfChild < viewGroup2.getChildCount() - 1) {
                            while (true) {
                                indexOfChild++;
                                if (indexOfChild >= viewGroup2.getChildCount()) {
                                    break;
                                }
                                View childAt = viewGroup2.getChildAt(indexOfChild);
                                if (c(childAt)) {
                                    ArrayList b2 = b(childAt);
                                    if (b2 != null) {
                                        linkedList4.addAll(0, b2);
                                    }
                                } else if (childAt.getVisibility() == 0 && a(childAt) != 0.0f) {
                                    linkedList4.addFirst(childAt);
                                }
                            }
                        }
                    }
                }
                Iterator it2 = linkedList4.iterator();
                float f3 = 0.0f;
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    View view4 = (View) it2.next();
                    if (f3 >= f) {
                        break;
                    }
                    if (view4 != null) {
                        try {
                            if (view4.getTag() != null && ((String) view4.getTag()).contains("BTN_CLOSE")) {
                            }
                        } catch (Exception unused2) {
                        }
                    }
                    Rect rect2 = new Rect();
                    if (view4.getGlobalVisibleRect(rect2)) {
                        if (z) {
                            rect2.top++;
                        }
                        if (rect2.intersect(rect)) {
                            linkedList2.add(rect2);
                            f3 = (float) (rect2.width() * rect2.height());
                            if (f3 >= f) {
                                z2 = true;
                                break;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }
                if (z2) {
                    return f;
                }
                if (linkedList2.isEmpty() || linkedList2.size() == 1) {
                    return f3;
                }
                Iterator it3 = linkedList2.iterator();
                while (it3.hasNext()) {
                    Rect rect3 = (Rect) it3.next();
                    arrayList.clear();
                    arrayList.addAll(linkedList3);
                    for (int i = 0; i < arrayList.size(); i++) {
                        Rect rect4 = (Rect) arrayList.get(i);
                        if (rect3.intersect(rect4)) {
                            linkedList3.remove(arrayList.get(i));
                            for (int i2 = 1; i2 < 9; i2++) {
                                Rect a2 = a(rect4, rect3, i2);
                                if (a2.height() > 0 && a2.width() > 0) {
                                    linkedList3.add(a2);
                                }
                            }
                        }
                    }
                }
                if (!linkedList3.isEmpty()) {
                    Iterator it4 = linkedList3.iterator();
                    while (it4.hasNext()) {
                        Rect rect5 = (Rect) it4.next();
                        f2 += (float) (rect5.width() * rect5.height());
                    }
                    if (f2 < f) {
                        return f - f2;
                    }
                }
                return f3;
            } else if (viewGroup.getVisibility() != 0 || a((View) viewGroup) == 0.0f) {
                return f;
            } else {
                if (view2 == null || !z || viewGroup == view2 || !(viewGroup.getLayoutParams().height == 0 || viewGroup.getLayoutParams().width == 0)) {
                    linkedList.addFirst(viewGroup);
                    viewGroup = (ViewGroup) viewGroup.getParent();
                }
            }
        }
        if (viewGroup != null) {
        }
    }

    private static Rect a(Rect rect, Rect rect2, int i) {
        Rect rect3 = new Rect();
        switch (i) {
            case 1:
                rect3.set(rect.left, rect.top, rect2.left, rect2.top);
                break;
            case 2:
                rect3.set(rect2.left, rect.top, rect2.right, rect2.top);
                break;
            case 3:
                rect3.set(rect2.right, rect.top, rect.right, rect2.top);
                break;
            case 4:
                rect3.set(rect2.right, rect2.top, rect.right, rect2.bottom);
                break;
            case 5:
                rect3.set(rect2.right, rect2.bottom, rect.right, rect.bottom);
                break;
            case 6:
                rect3.set(rect2.left, rect2.bottom, rect2.right, rect.bottom);
                break;
            case 7:
                rect3.set(rect.left, rect2.bottom, rect2.left, rect.bottom);
                break;
            case 8:
                rect3.set(rect.left, rect2.top, rect2.left, rect2.bottom);
                break;
        }
        return rect3;
    }

    private static float a(View view) {
        if (view == null) {
            return 0.0f;
        }
        if (a() < 11) {
            return 1.0f;
        }
        return view.getAlpha();
    }

    private static ArrayList<View> b(View view) {
        if (!(view instanceof ViewGroup) || view.getVisibility() != 0 || a(view) == 0.0f) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        ArrayList<View> arrayList = new ArrayList<>();
        linkedList.add((ViewGroup) view);
        ListIterator listIterator = linkedList.listIterator();
        while (listIterator.hasNext()) {
            ViewGroup viewGroup = (ViewGroup) listIterator.next();
            listIterator.remove();
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt.getVisibility() == 0 && a(childAt) != 0.0f) {
                    if (childAt instanceof ViewGroup) {
                        if (c(childAt)) {
                            listIterator.add((ViewGroup) childAt);
                            listIterator.previous();
                        } else {
                            arrayList.add(childAt);
                        }
                    } else if (!c(childAt)) {
                        arrayList.add(childAt);
                    }
                }
            }
        }
        if (!arrayList.isEmpty()) {
            return arrayList;
        }
        return null;
    }

    private static boolean c(View view) {
        boolean z = false;
        if (view == null) {
            return false;
        }
        if (view.getBackground() == null || (a() > 18 && view.getBackground().getAlpha() == 0)) {
            z = true;
        }
        return z;
    }

    private static int a(Context context) {
        int i;
        int i2 = context != null ? context.getResources().getConfiguration().orientation : -1;
        if (i2 == 2 && f811a > 0) {
            return f811a;
        }
        if (i2 == 1 && c > 0) {
            return c;
        }
        try {
            WindowManager windowManager = (WindowManager) context.getApplicationContext().getSystemService("window");
            if (a() >= 13) {
                i = a.a(windowManager);
            } else {
                i = windowManager.getDefaultDisplay().getHeight();
            }
            if (i2 == 2) {
                f811a = i;
            } else if (i2 == 1) {
                c = i;
            }
            return i;
        } catch (Exception unused) {
            return 0;
        }
    }

    private static int b(Context context) {
        int i;
        int i2 = context != null ? context.getResources().getConfiguration().orientation : -1;
        if (i2 == 2 && b > 0) {
            return b;
        }
        if (i2 == 1 && d > 0) {
            return d;
        }
        try {
            WindowManager windowManager = (WindowManager) context.getApplicationContext().getSystemService("window");
            if (a() >= 13) {
                i = b.a(windowManager);
            } else {
                i = windowManager.getDefaultDisplay().getWidth();
            }
            if (i2 == 2) {
                b = i;
            } else if (i2 == 1) {
                d = i;
            }
            return i;
        } catch (Exception unused) {
            return 0;
        }
    }

    private static int a() {
        return VERSION.SDK_INT;
    }
}
