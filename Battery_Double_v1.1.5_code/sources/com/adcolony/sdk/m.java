package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Build.VERSION;
import org.json.JSONObject;

class m {

    /* renamed from: a reason: collision with root package name */
    static AlertDialog f930a;
    /* access modifiers changed from: private */
    public ab b;
    /* access modifiers changed from: private */
    public AlertDialog c;
    /* access modifiers changed from: private */
    public boolean d;

    m() {
        a.a("Alert.show", (ad) new ad() {
            public void a(ab abVar) {
                if (!a.d() || !(a.c() instanceof Activity)) {
                    new a().a("Missing Activity reference, can't build AlertDialog.").a(w.g);
                    return;
                }
                if (u.d(abVar.c(), "on_resume")) {
                    m.this.b = abVar;
                } else {
                    m.this.a(abVar);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"InlinedApi"})
    public void a(final ab abVar) {
        Context c2 = a.c();
        if (c2 != null) {
            final Builder builder = VERSION.SDK_INT >= 21 ? new Builder(c2, 16974374) : new Builder(c2, 16974126);
            JSONObject c3 = abVar.c();
            String b2 = u.b(c3, "message");
            String b3 = u.b(c3, "title");
            String b4 = u.b(c3, "positive");
            String b5 = u.b(c3, "negative");
            builder.setMessage(b2);
            builder.setTitle(b3);
            builder.setPositiveButton(b4, new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    m.this.c = null;
                    dialogInterface.dismiss();
                    JSONObject a2 = u.a();
                    u.b(a2, "positive", true);
                    m.this.d = false;
                    abVar.a(a2).b();
                }
            });
            if (!b5.equals("")) {
                builder.setNegativeButton(b5, new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        m.this.c = null;
                        dialogInterface.dismiss();
                        JSONObject a2 = u.a();
                        u.b(a2, "positive", false);
                        m.this.d = false;
                        abVar.a(a2).b();
                    }
                });
            }
            builder.setOnCancelListener(new OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    m.this.c = null;
                    m.this.d = false;
                    JSONObject a2 = u.a();
                    u.b(a2, "positive", false);
                    abVar.a(a2).b();
                }
            });
            at.a((Runnable) new Runnable() {
                public void run() {
                    m.this.d = true;
                    m.this.c = builder.show();
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.b != null) {
            a(this.b);
            this.b = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public AlertDialog b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void a(AlertDialog alertDialog) {
        this.c = alertDialog;
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return this.d;
    }
}
