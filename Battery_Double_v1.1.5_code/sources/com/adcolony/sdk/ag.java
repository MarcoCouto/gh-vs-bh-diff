package com.adcolony.sdk;

import android.webkit.WebView;
import com.iab.omid.library.adcolony.adsession.AdEvents;
import com.iab.omid.library.adcolony.adsession.AdSession;
import com.iab.omid.library.adcolony.adsession.AdSessionConfiguration;
import com.iab.omid.library.adcolony.adsession.AdSessionContext;
import com.iab.omid.library.adcolony.adsession.Owner;
import com.iab.omid.library.adcolony.adsession.VerificationScriptResource;
import com.iab.omid.library.adcolony.adsession.video.InteractionType;
import com.iab.omid.library.adcolony.adsession.video.Position;
import com.iab.omid.library.adcolony.adsession.video.VastProperties;
import com.iab.omid.library.adcolony.adsession.video.VideoEvents;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

class ag {

    /* renamed from: a reason: collision with root package name */
    private AdSessionContext f732a;
    private AdSessionConfiguration b;
    private AdSession c;
    private AdEvents d;
    private VideoEvents e;
    private AdColonyCustomMessageListener f;
    private List<VerificationScriptResource> g = new ArrayList();
    /* access modifiers changed from: private */
    public int h = -1;
    /* access modifiers changed from: private */
    public String i = "";
    private int j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    private int p;
    private int q;
    private String r = "";
    /* access modifiers changed from: private */
    public String s = "";

    ag(JSONObject jSONObject, String str) {
        VerificationScriptResource verificationScriptResource;
        this.h = a(jSONObject);
        this.n = u.d(jSONObject, "skippable");
        this.p = u.c(jSONObject, "skip_offset");
        this.q = u.c(jSONObject, "video_duration");
        JSONArray g2 = u.g(jSONObject, "js_resources");
        JSONArray g3 = u.g(jSONObject, "verification_params");
        JSONArray g4 = u.g(jSONObject, "vendor_keys");
        this.s = str;
        for (int i2 = 0; i2 < g2.length(); i2++) {
            try {
                String c2 = u.c(g3, i2);
                String c3 = u.c(g4, i2);
                URL url = new URL(u.c(g2, i2));
                if (!c2.equals("") && !c3.equals("")) {
                    verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithParameters(c3, url, c2);
                } else if (!c3.equals("")) {
                    verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithoutParameters(c3, url);
                } else {
                    verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithoutParameters(url);
                }
                this.g.add(verificationScriptResource);
            } catch (MalformedURLException unused) {
                new a().a("Invalid js resource url passed to Omid").a(w.h);
            }
        }
        try {
            this.r = a.a().j().a(u.b(jSONObject, "filepath"), true).toString();
        } catch (IOException unused2) {
            new a().a("Error loading IAB JS Client").a(w.h);
        }
    }

    private int a(JSONObject jSONObject) {
        if (this.h == -1) {
            this.j = u.c(jSONObject, "ad_unit_type");
            String b2 = u.b(jSONObject, "ad_type");
            if (this.j == 0) {
                return 0;
            }
            if (this.j == 1) {
                if (b2.equals("video")) {
                    return 0;
                }
                if (b2.equals("display")) {
                    return 1;
                }
                if (b2.equals("banner_display") || b2.equals("interstitial_display")) {
                    return 2;
                }
            }
        }
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        VideoEvents videoEvents;
        VastProperties vastProperties;
        if (!this.m && this.h >= 0 && this.c != null) {
            b(cVar);
            f();
            if (this.h != 0) {
                videoEvents = null;
            } else {
                videoEvents = VideoEvents.createVideoEvents(this.c);
            }
            this.e = videoEvents;
            this.c.start();
            this.d = AdEvents.createAdEvents(this.c);
            b("start_session");
            if (this.e != null) {
                Position position = Position.PREROLL;
                if (this.n) {
                    vastProperties = VastProperties.createVastPropertiesForSkippableVideo((float) this.p, true, position);
                } else {
                    vastProperties = VastProperties.createVastPropertiesForNonSkippableVideo(true, position);
                }
                this.e.loaded(vastProperties);
            }
            this.m = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        AdColony.removeCustomMessageListener("viewability_ad_event");
        this.c.finish();
        b("end_session");
        this.c = null;
    }

    /* access modifiers changed from: 0000 */
    public void b() throws IllegalArgumentException {
        a((WebView) null);
    }

    /* access modifiers changed from: 0000 */
    public void a(WebView webView) throws IllegalArgumentException {
        if (this.h >= 0 && this.r != null && !this.r.equals("") && this.g != null) {
            if (!this.g.isEmpty() || c() == 2) {
                j a2 = a.a();
                Owner owner = Owner.NATIVE;
                switch (c()) {
                    case 0:
                        this.f732a = AdSessionContext.createNativeAdSessionContext(a2.D(), this.r, this.g, null);
                        this.b = AdSessionConfiguration.createAdSessionConfiguration(owner, owner, false);
                        this.c = AdSession.createAdSession(this.b, this.f732a);
                        this.i = this.c.getAdSessionId();
                        b("inject_javascript");
                        return;
                    case 1:
                        this.f732a = AdSessionContext.createNativeAdSessionContext(a2.D(), this.r, this.g, null);
                        this.b = AdSessionConfiguration.createAdSessionConfiguration(owner, null, false);
                        this.c = AdSession.createAdSession(this.b, this.f732a);
                        this.i = this.c.getAdSessionId();
                        b("inject_javascript");
                        return;
                    case 2:
                        this.f732a = AdSessionContext.createHtmlAdSessionContext(a2.D(), webView, "");
                        this.b = AdSessionConfiguration.createAdSessionConfiguration(owner, null, false);
                        this.c = AdSession.createAdSession(this.b, this.f732a);
                        this.i = this.c.getAdSessionId();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    private void b(final String str) {
        at.b.execute(new Runnable() {
            public void run() {
                JSONObject a2 = u.a();
                JSONObject a3 = u.a();
                u.b(a3, "session_type", ag.this.h);
                u.a(a3, "session_id", ag.this.i);
                u.a(a3, "event", str);
                u.a(a2, "type", "iab_hook");
                u.a(a2, "message", a3.toString());
                new ab("CustomMessage.controller_send", 0, a2).b();
            }
        });
    }

    private void b(c cVar) {
        b("register_ad_view");
        av avVar = (av) a.a().y().get(Integer.valueOf(cVar.c()));
        if (avVar == null && !cVar.g().isEmpty()) {
            avVar = (av) ((Entry) cVar.g().entrySet().iterator().next()).getValue();
        }
        if (this.c != null && avVar != null) {
            this.c.registerAdView(avVar);
            avVar.i();
        } else if (this.c != null) {
            this.c.registerAdView(cVar);
            cVar.a(this.c);
            b("register_obstructions");
        }
    }

    private void f() {
        this.f = new AdColonyCustomMessageListener() {
            public void onAdColonyCustomMessage(AdColonyCustomMessage adColonyCustomMessage) {
                JSONObject a2 = u.a(adColonyCustomMessage.getMessage());
                final String b = u.b(a2, "event_type");
                final float floatValue = BigDecimal.valueOf(u.e(a2, IronSourceConstants.EVENTS_DURATION)).floatValue();
                boolean d = u.d(a2, "replay");
                boolean equals = u.b(a2, "skip_type").equals("dec");
                final String b2 = u.b(a2, "asi");
                if (!b.equals("skip") || !equals) {
                    if (!d || (!b.equals("start") && !b.equals(CampaignEx.JSON_NATIVE_VIDEO_FIRST_QUARTILE) && !b.equals("midpoint") && !b.equals(CampaignEx.JSON_NATIVE_VIDEO_THIRD_QUARTILE) && !b.equals("complete"))) {
                        at.a((Runnable) new Runnable() {
                            public void run() {
                                if (b2.equals(ag.this.s)) {
                                    ag.this.a(b, floatValue);
                                    return;
                                }
                                AdColonyAdView adColonyAdView = (AdColonyAdView) a.a().l().e().get(b2);
                                ag omidManager = adColonyAdView != null ? adColonyAdView.getOmidManager() : null;
                                if (omidManager != null) {
                                    omidManager.a(b, floatValue);
                                }
                            }
                        });
                    }
                    return;
                }
                ag.this.o = true;
            }
        };
        AdColony.addCustomMessageListener(this.f, "viewability_ad_event");
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        a(str, 0.0f);
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, float f2) {
        if (a.d() && this.c != null) {
            if (this.e != null || str.equals("start") || str.equals("skip") || str.equals("continue") || str.equals("cancel")) {
                char c2 = 65535;
                try {
                    switch (str.hashCode()) {
                        case -1941887438:
                            if (str.equals(CampaignEx.JSON_NATIVE_VIDEO_FIRST_QUARTILE)) {
                                c2 = 1;
                                break;
                            }
                            break;
                        case -1710060637:
                            if (str.equals("buffer_start")) {
                                c2 = 12;
                                break;
                            }
                            break;
                        case -1638835128:
                            if (str.equals("midpoint")) {
                                c2 = 2;
                                break;
                            }
                            break;
                        case -1367724422:
                            if (str.equals("cancel")) {
                                c2 = 7;
                                break;
                            }
                            break;
                        case -934426579:
                            if (str.equals(CampaignEx.JSON_NATIVE_VIDEO_RESUME)) {
                                c2 = 11;
                                break;
                            }
                            break;
                        case -651914917:
                            if (str.equals(CampaignEx.JSON_NATIVE_VIDEO_THIRD_QUARTILE)) {
                                c2 = 3;
                                break;
                            }
                            break;
                        case -599445191:
                            if (str.equals("complete")) {
                                c2 = 4;
                                break;
                            }
                            break;
                        case -567202649:
                            if (str.equals("continue")) {
                                c2 = 5;
                                break;
                            }
                            break;
                        case -342650039:
                            if (str.equals("sound_mute")) {
                                c2 = 8;
                                break;
                            }
                            break;
                        case 3532159:
                            if (str.equals("skip")) {
                                c2 = 6;
                                break;
                            }
                            break;
                        case 106440182:
                            if (str.equals(CampaignEx.JSON_NATIVE_VIDEO_PAUSE)) {
                                c2 = 10;
                                break;
                            }
                            break;
                        case 109757538:
                            if (str.equals("start")) {
                                c2 = 0;
                                break;
                            }
                            break;
                        case 583742045:
                            if (str.equals("in_video_engagement")) {
                                c2 = 14;
                                break;
                            }
                            break;
                        case 823102269:
                            if (str.equals("html5_interaction")) {
                                c2 = 15;
                                break;
                            }
                            break;
                        case 1648173410:
                            if (str.equals("sound_unmute")) {
                                c2 = 9;
                                break;
                            }
                            break;
                        case 1906584668:
                            if (str.equals("buffer_end")) {
                                c2 = 13;
                                break;
                            }
                            break;
                    }
                    switch (c2) {
                        case 0:
                            this.d.impressionOccurred();
                            if (this.e != null) {
                                VideoEvents videoEvents = this.e;
                                if (f2 <= 0.0f) {
                                    f2 = (float) this.q;
                                }
                                videoEvents.start(f2, 1.0f);
                            }
                            b(str);
                            break;
                        case 1:
                            this.e.firstQuartile();
                            b(str);
                            break;
                        case 2:
                            this.e.midpoint();
                            b(str);
                            break;
                        case 3:
                            this.e.thirdQuartile();
                            b(str);
                            break;
                        case 4:
                            this.o = true;
                            this.e.complete();
                            b(str);
                            break;
                        case 5:
                            b(str);
                            a();
                            break;
                        case 6:
                        case 7:
                            if (this.e != null) {
                                this.e.skipped();
                            }
                            b(str);
                            a();
                            break;
                        case 8:
                            this.e.volumeChange(0.0f);
                            b(str);
                            break;
                        case 9:
                            this.e.volumeChange(1.0f);
                            b(str);
                            break;
                        case 10:
                            if (!this.k && !this.l && !this.o) {
                                this.e.pause();
                                b(str);
                                this.k = true;
                                this.l = false;
                                break;
                            }
                        case 11:
                            if (this.k && !this.o) {
                                this.e.resume();
                                b(str);
                                this.k = false;
                                break;
                            }
                        case 12:
                            this.e.bufferStart();
                            b(str);
                            break;
                        case 13:
                            this.e.bufferFinish();
                            b(str);
                            break;
                        case 14:
                        case 15:
                            this.e.adUserInteraction(InteractionType.CLICK);
                            b(str);
                            if (this.l && !this.k && !this.o) {
                                this.e.pause();
                                b(CampaignEx.JSON_NATIVE_VIDEO_PAUSE);
                                this.k = true;
                                this.l = false;
                                break;
                            }
                    }
                } catch (IllegalArgumentException | IllegalStateException e2) {
                    a a2 = new a().a("Recording IAB event for ").a(str);
                    StringBuilder sb = new StringBuilder();
                    sb.append(" caused ");
                    sb.append(e2.getClass());
                    a2.a(sb.toString()).a(w.f);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        this.l = true;
    }

    /* access modifiers changed from: 0000 */
    public AdSession e() {
        return this.c;
    }
}
