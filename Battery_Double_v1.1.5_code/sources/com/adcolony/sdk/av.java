package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.RequiresApi;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.ConsoleMessage;
import android.webkit.ConsoleMessage.MessageLevel;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.iab.omid.library.adcolony.ScriptInjector;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;

@SuppressLint({"SetJavaScriptEnabled"})
class av extends WebView implements ae {

    /* renamed from: a reason: collision with root package name */
    static boolean f795a = false;
    private boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    private boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private boolean F;
    private boolean G;
    /* access modifiers changed from: private */
    public JSONArray H = u.b();
    /* access modifiers changed from: private */
    public JSONObject I = u.a();
    private JSONObject J = u.a();
    /* access modifiers changed from: private */
    public c K;
    /* access modifiers changed from: private */
    public ab L;
    private ImageView M;
    /* access modifiers changed from: private */
    public final Object N = new Object();
    /* access modifiers changed from: private */
    public String b;
    private String c;
    private String d = "";
    private String e = "";
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g = "";
    private String h = "";
    private String i = "";
    /* access modifiers changed from: private */
    public String j = "";
    private String k = "";
    /* access modifiers changed from: private */
    public int l;
    private int m;
    private int n;
    private int o;
    private int p;
    /* access modifiers changed from: private */
    public int q;
    private int r;
    /* access modifiers changed from: private */
    public int s;
    private int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;
    private int w;
    private int x;
    /* access modifiers changed from: private */
    public boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    private class a extends WebViewClient {
        private a() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!av.this.B) {
                return false;
            }
            at.a(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            aq n = a.a().n();
            n.b(av.this.f);
            n.a(av.this.f);
            JSONObject a2 = u.a();
            u.a(a2, "url", str);
            u.a(a2, "ad_session_id", av.this.f);
            new ab("WebView.redirect_detected", av.this.K.c(), a2).b();
            return true;
        }

        public void onLoadResource(WebView webView, String str) {
            if (str.equals(av.this.b)) {
                av.this.a("if (typeof(CN) != 'undefined' && CN.div) {\n  if (typeof(cn_dispatch_on_touch_begin) != 'undefined') CN.div.removeEventListener('mousedown',  cn_dispatch_on_touch_begin, true);\n  if (typeof(cn_dispatch_on_touch_end) != 'undefined')   CN.div.removeEventListener('mouseup',  cn_dispatch_on_touch_end, true);\n  if (typeof(cn_dispatch_on_touch_move) != 'undefined')  CN.div.removeEventListener('mousemove',  cn_dispatch_on_touch_move, true);\n}\n");
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            av.this.B = false;
            new a().a("onPageStarted with URL = ").a(str).a(w.d);
        }

        public void onPageFinished(WebView webView, String str) {
            JSONObject a2 = u.a();
            u.b(a2, "id", av.this.l);
            u.a(a2, "url", str);
            new a().a("onPageFinished called with URL = ").a(str).a(w.b);
            if (av.this.K == null) {
                new ab("WebView.on_load", av.this.u, a2).b();
            } else {
                u.a(a2, "ad_session_id", av.this.f);
                u.b(a2, "container_id", av.this.K.d());
                new ab("WebView.on_load", av.this.K.c(), a2).b();
            }
            if ((av.this.y || av.this.z) && !av.this.B) {
                int m = av.this.v > 0 ? av.this.v : av.this.u;
                if (av.this.v > 0) {
                    float p = a.a().m().p();
                    u.b(av.this.I, "app_orientation", at.j(at.h()));
                    u.b(av.this.I, AvidJSONUtil.KEY_X, at.a((View) av.this));
                    u.b(av.this.I, AvidJSONUtil.KEY_Y, at.b((View) av.this));
                    u.b(av.this.I, "width", (int) (((float) av.this.q) / p));
                    u.b(av.this.I, "height", (int) (((float) av.this.s) / p));
                    u.a(av.this.I, "ad_session_id", av.this.f);
                }
                av avVar = av.this;
                StringBuilder sb = new StringBuilder();
                sb.append("ADC3_init(");
                sb.append(m);
                sb.append(",");
                sb.append(av.this.I.toString());
                sb.append(");");
                avVar.a(sb.toString());
                av.this.B = true;
            }
            if (!av.this.z) {
                return;
            }
            if (av.this.u != 1 || av.this.v > 0) {
                JSONObject a3 = u.a();
                u.b(a3, "success", true);
                u.b(a3, "id", av.this.u);
                av.this.L.a(a3).b();
            }
        }

        @TargetApi(11)
        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            if (VERSION.SDK_INT >= 21 || !str.endsWith("mraid.js")) {
                return null;
            }
            try {
                return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(av.this.g.getBytes("UTF-8")));
            } catch (UnsupportedEncodingException unused) {
                new a().a("UTF-8 not supported.").a(w.h);
                return null;
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            if (VERSION.SDK_INT < 23) {
                av.this.a(i, str, str2);
            }
        }

        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            if (VERSION.SDK_INT < 26) {
                return super.onRenderProcessGone(webView, renderProcessGoneDetail);
            }
            if (renderProcessGoneDetail.didCrash()) {
                av.this.a(u.a(), "An error occurred while rendering the ad. Ad closing.");
            }
            return true;
        }
    }

    public void c() {
    }

    private av(Context context) {
        super(context);
    }

    av(Context context, ab abVar, int i2, int i3, c cVar) {
        super(context);
        this.L = abVar;
        a(abVar, i2, i3, cVar);
        e();
    }

    av(Context context, int i2, boolean z2) {
        super(context);
        this.u = i2;
        this.A = z2;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        if (this.C) {
            new a().a("Ignoring call to execute_js as WebView has been destroyed.").a(w.b);
            return;
        }
        if (VERSION.SDK_INT >= 19) {
            evaluateJavascript(str, null);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("javascript:");
            sb.append(str);
            loadUrl(sb.toString());
        }
    }

    public int a() {
        return this.u;
    }

    public int b() {
        return this.v;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ab abVar) {
        JSONObject c2 = abVar.c();
        return u.c(c2, "id") == this.l && u.c(c2, "container_id") == this.K.d() && u.b(c2, "ad_session_id").equals(this.K.b());
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        AdColonyAdView adColonyAdView;
        if (motionEvent.getAction() == 1) {
            if (this.f == null) {
                adColonyAdView = null;
            } else {
                adColonyAdView = (AdColonyAdView) a.a().l().e().get(this.f);
            }
            if (adColonyAdView != null && !adColonyAdView.getUserInteraction()) {
                JSONObject a2 = u.a();
                u.a(a2, "ad_session_id", this.f);
                new ab("WebView.on_first_click", 1, a2).b();
                adColonyAdView.setUserInteraction(true);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        a(false, (ab) null);
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"AddJavascriptInterface"})
    public void a(boolean z2, ab abVar) {
        String str;
        String str2;
        if (this.L == null) {
            this.L = abVar;
        }
        final JSONObject c2 = this.L.c();
        this.z = z2;
        this.A = u.d(c2, "is_display_module");
        if (z2) {
            String b2 = u.b(c2, "filepath");
            this.k = u.b(c2, "interstitial_html");
            this.h = u.b(c2, "mraid_filepath");
            this.e = u.b(c2, "base_url");
            this.c = b2;
            this.J = u.f(c2, "iab");
            if (f795a && this.u == 1) {
                this.c = "android_asset/ADCController.js";
            }
            if (this.k.equals("")) {
                StringBuilder sb = new StringBuilder();
                sb.append("file:///");
                sb.append(this.c);
                str2 = sb.toString();
            } else {
                str2 = "";
            }
            this.b = str2;
            this.I = u.f(c2, String.VIDEO_INFO);
            this.f = u.b(c2, "ad_session_id");
            this.y = true;
        }
        setFocusable(true);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        if (VERSION.SDK_INT >= 19) {
            setWebContentsDebuggingEnabled(true);
        }
        setWebChromeClient(new WebChromeClient() {
            public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                new a().a("JS Alert: ").a(str2).a(w.d);
                jsResult.confirm();
                return true;
            }

            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                String str;
                MessageLevel messageLevel = consoleMessage.messageLevel();
                String message = consoleMessage.message();
                boolean z = false;
                boolean z2 = messageLevel == MessageLevel.ERROR;
                String str2 = "Viewport target-densitydpi is not supported.";
                if (consoleMessage.message().contains("Viewport argument key \"shrink-to-fit\" not recognized and ignored") || consoleMessage.message().contains(str2)) {
                    z = true;
                }
                if (message.contains("ADC3_update is not defined") || message.contains("NativeLayer.dispatch_messages is not a function")) {
                    av.this.a(c2, "Unable to communicate with AdColony. Please ensure that you have added an exception for our Javascript interface in your ProGuard configuration and that you do not have a faulty proxy enabled on your device.");
                }
                if (!z && (messageLevel == MessageLevel.WARNING || z2)) {
                    AdColonyInterstitial adColonyInterstitial = null;
                    if (av.this.f != null) {
                        adColonyInterstitial = (AdColonyInterstitial) a.a().l().c().get(av.this.f);
                    }
                    if (adColonyInterstitial == null) {
                        str = "unknown";
                    } else {
                        str = adColonyInterstitial.b();
                    }
                    new a().a("onConsoleMessage: ").a(consoleMessage.message()).a(" with ad id: ").a(str).a(z2 ? w.h : w.f);
                }
                return true;
            }
        });
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setGeolocationEnabled(true);
        settings.setUseWideViewPort(true);
        if (VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        if (VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
            settings.setAllowFileAccess(true);
        }
        WebViewClient webViewClient = VERSION.SDK_INT >= 23 ? new a() {
            @RequiresApi(api = 24)
            public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
                if (!av.this.B || !webResourceRequest.isForMainFrame()) {
                    return false;
                }
                Uri url = webResourceRequest.getUrl();
                at.a(new Intent("android.intent.action.VIEW", url));
                JSONObject a2 = u.a();
                u.a(a2, "url", url.toString());
                u.a(a2, "ad_session_id", av.this.f);
                new ab("WebView.redirect_detected", av.this.K.c(), a2).b();
                aq n = a.a().n();
                n.b(av.this.f);
                n.a(av.this.f);
                return true;
            }

            @RequiresApi(api = 23)
            public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                av.this.a(webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
            }

            @RequiresApi(api = 23)
            public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                if (!webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                    return null;
                }
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(av.this.g.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new a().a("UTF-8 not supported.").a(w.h);
                    return null;
                }
            }
        } : VERSION.SDK_INT >= 21 ? new a() {
            @RequiresApi(api = 21)
            public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                if (!webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                    return null;
                }
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(av.this.g.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new a().a("UTF-8 not supported.").a(w.h);
                    return null;
                }
            }
        } : new a();
        addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void push_messages(String str) {
                av.this.b(str);
            }

            @JavascriptInterface
            public void dispatch_messages(String str) {
                av.this.b(str);
            }

            @JavascriptInterface
            public String pull_messages() {
                String str = "[]";
                synchronized (av.this.N) {
                    if (av.this.H.length() > 0) {
                        if (av.this.y) {
                            str = av.this.H.toString();
                        }
                        av.this.H = u.b();
                    }
                }
                return str;
            }

            @JavascriptInterface
            public void enable_reverse_messaging() {
                av.this.D = true;
            }
        }, "NativeLayer");
        setWebViewClient(webViewClient);
        if (this.A) {
            try {
                if (this.k.equals("")) {
                    FileInputStream fileInputStream = new FileInputStream(this.c);
                    StringBuilder sb2 = new StringBuilder(fileInputStream.available());
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr, 0, 1024);
                        if (read < 0) {
                            break;
                        }
                        sb2.append(new String(bArr, 0, read));
                    }
                    if (this.c.contains(".html")) {
                        str = sb2.toString();
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("<html><script>");
                        sb3.append(sb2.toString());
                        sb3.append("</script></html>");
                        str = sb3.toString();
                    }
                } else {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("script src=\"file://");
                    sb4.append(this.h);
                    sb4.append("\"");
                    str = this.k.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", sb4.toString());
                }
                String b3 = u.b(u.f(c2, String.VIDEO_INFO), TtmlNode.TAG_METADATA);
                StringBuilder sb5 = new StringBuilder();
                sb5.append("var ADC_DEVICE_INFO = ");
                sb5.append(b3);
                sb5.append(";");
                loadDataWithBaseURL(this.b.equals("") ? this.e : this.b, a(str, u.b(u.a(b3), "iab_filepath")).replaceFirst("var\\s*ADC_DEVICE_INFO\\s*=\\s*null\\s*;", Matcher.quoteReplacement(sb5.toString())), WebRequest.CONTENT_TYPE_HTML, null, null);
            } catch (IOException e2) {
                a((Exception) e2);
                return;
            } catch (IllegalArgumentException e3) {
                a((Exception) e3);
                return;
            } catch (IndexOutOfBoundsException e4) {
                a((Exception) e4);
                return;
            }
        } else if (!this.b.startsWith("http") && !this.b.startsWith(ParametersKeys.FILE)) {
            loadDataWithBaseURL(this.e, this.b, WebRequest.CONTENT_TYPE_HTML, null, null);
        } else if (this.b.contains(".html") || !this.b.startsWith(ParametersKeys.FILE)) {
            loadUrl(this.b);
        } else {
            String str3 = this.b;
            StringBuilder sb6 = new StringBuilder();
            sb6.append("<html><script src=\"");
            sb6.append(this.b);
            sb6.append("\"></script></html>");
            loadDataWithBaseURL(str3, sb6.toString(), WebRequest.CONTENT_TYPE_HTML, null, null);
        }
        if (!z2) {
            f();
            g();
        }
        if (z2 || this.y) {
            a.a().q().a((ae) this);
        }
        if (!this.d.equals("")) {
            a(this.d);
        }
    }

    private String a(String str, String str2) {
        ag agVar;
        d l2 = a.a().l();
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) l2.c().get(this.f);
        AdColonyAdViewListener adColonyAdViewListener = (AdColonyAdViewListener) l2.d().get(this.f);
        if (adColonyInterstitial != null && this.J.length() > 0 && !u.b(this.J, "ad_type").equals("video")) {
            adColonyInterstitial.a(this.J);
        } else if (adColonyAdViewListener != null && this.J.length() > 0) {
            adColonyAdViewListener.a(new ag(this.J, this.f));
        }
        if (adColonyInterstitial == null) {
            agVar = null;
        } else {
            agVar = adColonyInterstitial.h();
        }
        if (agVar == null && adColonyAdViewListener != null) {
            agVar = adColonyAdViewListener.c();
        }
        if (agVar != null && agVar.c() == 2) {
            this.E = true;
            if (!str2.equals("")) {
                try {
                    return ScriptInjector.injectScriptContentIntoHtml(a.a().j().a(str2, false).toString(), str);
                } catch (IOException e2) {
                    a((Exception) e2);
                }
            }
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        JSONArray b2 = u.b(str);
        for (int i2 = 0; i2 < b2.length(); i2++) {
            a.a().q().a(u.d(b2, i2));
        }
    }

    private boolean a(Exception exc) {
        new a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(u.b(this.I, TtmlNode.TAG_METADATA)).a(w.h);
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) a.a().l().c().remove(u.b(this.I, "ad_session_id"));
        if (adColonyInterstitial == null) {
            return false;
        }
        AdColonyInterstitialListener listener = adColonyInterstitial.getListener();
        if (listener == null) {
            return false;
        }
        listener.onExpiring(adColonyInterstitial);
        adColonyInterstitial.a(true);
        return true;
    }

    private void b(Exception exc) {
        new a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(u.b(this.I, TtmlNode.TAG_METADATA)).a(w.h);
        JSONObject a2 = u.a();
        u.a(a2, "id", this.f);
        new ab("AdSession.on_error", this.K.c(), a2).b();
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        this.K.m().add(a.a("WebView.set_visible", (ad) new ad() {
            public void a(final ab abVar) {
                if (av.this.a(abVar)) {
                    at.a((Runnable) new Runnable() {
                        public void run() {
                            av.this.c(abVar);
                        }
                    });
                }
            }
        }, true));
        this.K.m().add(a.a("WebView.set_bounds", (ad) new ad() {
            public void a(final ab abVar) {
                if (av.this.a(abVar)) {
                    at.a((Runnable) new Runnable() {
                        public void run() {
                            av.this.b(abVar);
                        }
                    });
                }
            }
        }, true));
        this.K.m().add(a.a("WebView.execute_js", (ad) new ad() {
            public void a(final ab abVar) {
                if (av.this.a(abVar)) {
                    at.a((Runnable) new Runnable() {
                        public void run() {
                            av.this.a(u.b(abVar.c(), "custom_js"));
                        }
                    });
                }
            }
        }, true));
        this.K.m().add(a.a("WebView.set_transparent", (ad) new ad() {
            public void a(final ab abVar) {
                if (av.this.a(abVar)) {
                    at.a((Runnable) new Runnable() {
                        public void run() {
                            av.this.b(u.d(abVar.c(), "transparent"));
                        }
                    });
                }
            }
        }, true));
        this.K.n().add("WebView.set_visible");
        this.K.n().add("WebView.set_bounds");
        this.K.n().add("WebView.execute_js");
        this.K.n().add("WebView.set_transparent");
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        setBackgroundColor(z2 ? 0 : -1);
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        setVisibility(4);
        LayoutParams layoutParams = new LayoutParams(this.q, this.s);
        layoutParams.setMargins(this.m, this.o, 0, 0);
        layoutParams.gravity = 0;
        this.K.addView(this, layoutParams);
        if (!this.i.equals("") && !this.j.equals("")) {
            w();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(ab abVar, int i2, c cVar) {
        a(abVar, i2, -1, cVar);
        g();
    }

    /* access modifiers changed from: 0000 */
    public void a(ab abVar, int i2, int i3, c cVar) {
        JSONObject c2 = abVar.c();
        this.b = u.b(c2, "url");
        if (this.b.equals("")) {
            this.b = u.b(c2, "data");
        }
        this.e = u.b(c2, "base_url");
        this.d = u.b(c2, "custom_js");
        this.f = u.b(c2, "ad_session_id");
        this.I = u.f(c2, String.VIDEO_INFO);
        this.h = u.b(c2, "mraid_filepath");
        this.v = u.d(c2, "use_mraid_module") ? a.a().q().d() : this.v;
        this.i = u.b(c2, "ad_choices_filepath");
        this.j = u.b(c2, "ad_choices_url");
        this.F = u.d(c2, "disable_ad_choices");
        this.G = u.d(c2, "ad_choices_snap_to_webview");
        this.w = u.c(c2, "ad_choices_width");
        this.x = u.c(c2, "ad_choices_height");
        if (this.J.length() == 0) {
            this.J = u.f(c2, "iab");
        }
        boolean z2 = false;
        if (!this.A && !this.h.equals("")) {
            if (this.v > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("script src=\"file://");
                sb.append(this.h);
                sb.append("\"");
                this.b = a(this.b.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", sb.toString()), u.b(u.f(this.I, DeviceRequestsHelper.DEVICE_INFO_PARAM), "iab_filepath"));
            } else {
                try {
                    this.g = a.a().j().a(this.h, false).toString();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("bridge.os_name = \"\";\nvar ADC_DEVICE_INFO = ");
                    sb2.append(this.I.toString());
                    sb2.append(";\n");
                    this.g = this.g.replaceFirst("bridge.os_name\\s*=\\s*\"\"\\s*;", sb2.toString());
                } catch (IOException e2) {
                    b((Exception) e2);
                } catch (IllegalArgumentException e3) {
                    b((Exception) e3);
                } catch (IndexOutOfBoundsException e4) {
                    b((Exception) e4);
                }
            }
        }
        this.l = i2;
        this.K = cVar;
        if (i3 >= 0) {
            this.u = i3;
        } else {
            f();
        }
        this.q = u.c(c2, "width");
        this.s = u.c(c2, "height");
        this.m = u.c(c2, AvidJSONUtil.KEY_X);
        this.o = u.c(c2, AvidJSONUtil.KEY_Y);
        this.r = this.q;
        this.t = this.s;
        this.p = this.o;
        this.n = this.m;
        if (u.d(c2, "enable_messages") || this.z) {
            z2 = true;
        }
        this.y = z2;
        k();
    }

    /* access modifiers changed from: 0000 */
    public void b(ab abVar) {
        JSONObject c2 = abVar.c();
        this.m = u.c(c2, AvidJSONUtil.KEY_X);
        this.o = u.c(c2, AvidJSONUtil.KEY_Y);
        this.q = u.c(c2, "width");
        this.s = u.c(c2, "height");
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.m, this.o, 0, 0);
        layoutParams.width = this.q;
        layoutParams.height = this.s;
        setLayoutParams(layoutParams);
        if (this.z) {
            JSONObject a2 = u.a();
            u.b(a2, "success", true);
            u.b(a2, "id", this.u);
            abVar.a(a2).b();
        }
        h();
    }

    /* access modifiers changed from: 0000 */
    public void h() {
        if (this.M != null) {
            int q2 = a.a().m().q();
            int r2 = a.a().m().r();
            if (this.G) {
                q2 = this.m + this.q;
            }
            if (this.G) {
                r2 = this.o + this.s;
            }
            float p2 = a.a().m().p();
            int i2 = (int) (((float) this.w) * p2);
            int i3 = (int) (((float) this.x) * p2);
            this.M.setLayoutParams(new AbsoluteLayout.LayoutParams(i2, i3, q2 - i2, r2 - i3));
        }
    }

    private void w() {
        Context c2 = a.c();
        if (c2 != null && this.K != null && !this.F) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(-1);
            gradientDrawable.setShape(1);
            this.M = new ImageView(c2);
            this.M.setImageURI(Uri.fromFile(new File(this.i)));
            this.M.setBackground(gradientDrawable);
            this.M.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    at.a(new Intent("android.intent.action.VIEW", Uri.parse(av.this.j)));
                    a.a().n().a(av.this.f);
                }
            });
            h();
            addView(this.M);
        }
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        if (this.M != null) {
            this.K.a((View) this.M);
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(ab abVar) {
        if (u.d(abVar.c(), String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
        if (this.z) {
            JSONObject a2 = u.a();
            u.b(a2, "success", true);
            u.b(a2, "id", this.u);
            abVar.a(a2).b();
        }
    }

    public void a(JSONObject jSONObject) {
        synchronized (this.N) {
            this.H.put(jSONObject);
        }
    }

    public void d() {
        if (a.d() && this.B && !this.D) {
            j();
        }
    }

    /* access modifiers changed from: 0000 */
    public void j() {
        at.a((Runnable) new Runnable() {
            public void run() {
                String str = "";
                synchronized (av.this.N) {
                    if (av.this.H.length() > 0) {
                        if (av.this.y) {
                            str = av.this.H.toString();
                        }
                        av.this.H = u.b();
                    }
                }
                if (av.this.y) {
                    av avVar = av.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("NativeLayer.dispatch_messages(ADC3_update(");
                    sb.append(str);
                    sb.append("));");
                    avVar.a(sb.toString());
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject, String str) {
        Context c2 = a.c();
        if (c2 != null && (c2 instanceof b)) {
            ab abVar = new ab("AdSession.finish_fullscreen_ad", 0);
            u.b(jSONObject, "status", 1);
            new a().a(str).a(w.g);
            ((b) c2).a(abVar);
        } else if (this.u == 1) {
            new a().a("Unable to communicate with controller, disabling AdColony.").a(w.g);
            AdColony.disable();
        } else if (this.v > 0) {
            this.y = false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void k() {
        at.a((Runnable) new Runnable() {
            public void run() {
                ag agVar;
                int i;
                try {
                    d l = a.a().l();
                    AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) l.c().get(av.this.f);
                    AdColonyAdView adColonyAdView = (AdColonyAdView) l.e().get(av.this.f);
                    if (adColonyInterstitial == null) {
                        agVar = null;
                    } else {
                        agVar = adColonyInterstitial.h();
                    }
                    if (agVar == null && adColonyAdView != null) {
                        agVar = adColonyAdView.getOmidManager();
                    }
                    if (agVar == null) {
                        i = -1;
                    } else {
                        i = agVar.c();
                    }
                    if (agVar != null && i == 2) {
                        agVar.a((WebView) av.this);
                        agVar.a(av.this.K);
                    }
                } catch (IllegalArgumentException unused) {
                    new a().a("IllegalArgumentException when creating omid session").a(w.h);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str, String str2) {
        if (this.K != null) {
            JSONObject a2 = u.a();
            u.b(a2, "id", this.l);
            u.a(a2, "ad_session_id", this.f);
            u.b(a2, "container_id", this.K.d());
            u.b(a2, "code", i2);
            u.a(a2, "error", str);
            u.a(a2, "url", str2);
            new ab("WebView.on_error", this.K.c(), a2).b();
        }
        new a().a("onReceivedError: ").a(str).a(w.h);
    }

    /* access modifiers changed from: 0000 */
    public boolean l() {
        return this.E;
    }

    /* access modifiers changed from: 0000 */
    public boolean m() {
        return this.A;
    }

    /* access modifiers changed from: 0000 */
    public boolean n() {
        return this.C;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        this.C = z2;
    }

    /* access modifiers changed from: 0000 */
    public int o() {
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    public int p() {
        return this.p;
    }

    /* access modifiers changed from: 0000 */
    public int q() {
        return this.r;
    }

    /* access modifiers changed from: 0000 */
    public int r() {
        return this.t;
    }

    /* access modifiers changed from: 0000 */
    public int s() {
        return this.q;
    }

    /* access modifiers changed from: 0000 */
    public int t() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    public int u() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public int v() {
        return this.o;
    }
}
