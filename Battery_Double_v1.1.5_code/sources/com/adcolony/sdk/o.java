package com.adcolony.sdk;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

class o implements a {

    /* renamed from: a reason: collision with root package name */
    private BlockingQueue<Runnable> f937a = new LinkedBlockingQueue();
    private ThreadPoolExecutor b;
    private LinkedList<n> c;
    private String d;

    o() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4, 16, 60, TimeUnit.SECONDS, this.f937a);
        this.b = threadPoolExecutor;
        this.c = new LinkedList<>();
        this.d = a.a().m().G();
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        a.a("WebServices.download", (ad) new ad() {
            public void a(ab abVar) {
                o.this.a(new n(abVar, o.this));
            }
        });
        a.a("WebServices.get", (ad) new ad() {
            public void a(ab abVar) {
                o.this.a(new n(abVar, o.this));
            }
        });
        a.a("WebServices.post", (ad) new ad() {
            public void a(ab abVar) {
                o.this.a(new n(abVar, o.this));
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(n nVar) {
        if (this.d.equals("")) {
            this.c.push(nVar);
            return;
        }
        try {
            this.b.execute(nVar);
        } catch (RejectedExecutionException unused) {
            a a2 = new a().a("RejectedExecutionException: ThreadPoolExecutor unable to ");
            StringBuilder sb = new StringBuilder();
            sb.append("execute download for url ");
            sb.append(nVar.f936a);
            a2.a(sb.toString()).a(w.h);
            a(nVar, nVar.a(), null);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.d = str;
        while (!this.c.isEmpty()) {
            a((n) this.c.removeLast());
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        this.b.setCorePoolSize(i);
    }

    /* access modifiers changed from: 0000 */
    public int b() {
        return this.b.getCorePoolSize();
    }

    public void a(n nVar, ab abVar, Map<String, List<String>> map) {
        JSONObject a2 = u.a();
        u.a(a2, "url", nVar.f936a);
        u.b(a2, "success", nVar.c);
        u.b(a2, "status", nVar.e);
        u.a(a2, TtmlNode.TAG_BODY, nVar.b);
        u.b(a2, "size", nVar.d);
        if (map != null) {
            JSONObject a3 = u.a();
            for (Entry entry : map.entrySet()) {
                String obj = ((List) entry.getValue()).toString();
                String substring = obj.substring(1, obj.length() - 1);
                if (entry.getKey() != null) {
                    u.a(a3, (String) entry.getKey(), substring);
                }
            }
            u.a(a2, "headers", a3);
        }
        abVar.a(a2).b();
    }
}
