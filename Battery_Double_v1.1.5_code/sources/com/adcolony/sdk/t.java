package com.adcolony.sdk;

class t {

    /* renamed from: a reason: collision with root package name */
    private String f977a;
    private String b;
    private String c;
    private String d = "%s_%s_%s";

    public t(String str, String str2, String str3) {
        this.f977a = str;
        this.b = str2;
        this.c = str3;
    }

    public String a() {
        return String.format(this.d, new Object[]{b(), c(), d()});
    }

    public String b() {
        return this.f977a;
    }

    public String c() {
        return this.b;
    }

    public String d() {
        return this.c;
    }
}
