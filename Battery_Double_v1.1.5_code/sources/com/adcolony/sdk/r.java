package com.adcolony.sdk;

import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class r {

    /* renamed from: a reason: collision with root package name */
    private LinkedList<Runnable> f954a = new LinkedList<>();
    private boolean b;

    r() {
    }

    /* access modifiers changed from: private */
    public boolean f(ab abVar) {
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            int c2 = u.c(c, "offset");
            int c3 = u.c(c, "size");
            boolean d = u.d(c, "gunzip");
            String b3 = u.b(c, "output_filepath");
            InputStream apVar = new ap(new FileInputStream(b2), c2, c3);
            InputStream gZIPInputStream = d ? new GZIPInputStream(apVar, 1024) : apVar;
            if (b3.equals("")) {
                StringBuilder sb = new StringBuilder(gZIPInputStream.available());
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = gZIPInputStream.read(bArr, 0, 1024);
                    if (read < 0) {
                        break;
                    }
                    sb.append(new String(bArr, 0, read, "ISO-8859-1"));
                }
                u.b(a2, "size", sb.length());
                u.a(a2, "data", sb.toString());
            } else {
                FileOutputStream fileOutputStream = new FileOutputStream(b3);
                byte[] bArr2 = new byte[1024];
                int i = 0;
                while (true) {
                    int read2 = gZIPInputStream.read(bArr2, 0, 1024);
                    if (read2 < 0) {
                        break;
                    }
                    fileOutputStream.write(bArr2, 0, read2);
                    i += read2;
                }
                fileOutputStream.close();
                u.b(a2, "size", i);
            }
            gZIPInputStream.close();
            u.b(a2, "success", true);
            abVar.a(a2).b();
            return true;
        } catch (IOException unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        } catch (OutOfMemoryError unused2) {
            new a().a("Out of memory error - disabling AdColony.").a(w.g);
            a.a().a(true);
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00f8, code lost:
        new com.adcolony.sdk.w.a().a("Out of memory error - disabling AdColony.").a(com.adcolony.sdk.w.g);
        com.adcolony.sdk.a.a().a(true);
        com.adcolony.sdk.u.b(r4, "success", false);
        r0.a(r4).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x011d, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[ExcHandler: OutOfMemoryError (unused java.lang.OutOfMemoryError), SYNTHETIC, Splitter:B:1:0x0027] */
    public boolean g(ab abVar) {
        boolean z;
        ab abVar2 = abVar;
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        String b3 = u.b(c, "bundle_path");
        JSONArray g = u.g(c, "bundle_filenames");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            File file = new File(b3);
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, CampaignEx.JSON_KEY_AD_R);
            byte[] bArr = new byte[4];
            byte[] bArr2 = new byte[32];
            randomAccessFile.readInt();
            int readInt = randomAccessFile.readInt();
            JSONArray jSONArray = new JSONArray();
            byte[] bArr3 = new byte[1024];
            int i = 0;
            while (i < readInt) {
                randomAccessFile.seek((long) (8 + (i * 44)));
                randomAccessFile.read(bArr2);
                new String(bArr2);
                randomAccessFile.readInt();
                int readInt2 = randomAccessFile.readInt();
                int readInt3 = randomAccessFile.readInt();
                jSONArray.put(readInt3);
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(b2);
                    sb.append(g.get(i));
                    String sb2 = sb.toString();
                    JSONArray jSONArray2 = g;
                    String str = b2;
                    randomAccessFile.seek((long) readInt2);
                    FileOutputStream fileOutputStream = new FileOutputStream(sb2);
                    int i2 = readInt3 / 1024;
                    int i3 = readInt3 % 1024;
                    for (int i4 = 0; i4 < i2; i4++) {
                        randomAccessFile.read(bArr3, 0, 1024);
                        fileOutputStream.write(bArr3, 0, 1024);
                    }
                    randomAccessFile.read(bArr3, 0, i3);
                    fileOutputStream.write(bArr3, 0, i3);
                    fileOutputStream.close();
                    i++;
                    b2 = str;
                    g = jSONArray2;
                } catch (JSONException unused) {
                    new a().a("Could extract file name at index ").a(i).a(" unpacking ad unit bundle at ").a(b3).a(w.g);
                    z = false;
                    u.b(a2, "success", false);
                    abVar2.a(a2).b();
                    return false;
                }
            }
            randomAccessFile.close();
            file.delete();
            u.b(a2, "success", true);
            u.a(a2, "file_sizes", jSONArray);
            abVar2.a(a2).b();
            return true;
        } catch (IOException unused2) {
            z = false;
            new a().a("Failed to find or open ad unit bundle at path: ").a(b3).a(w.h);
            u.b(a2, "success", z);
            abVar2.a(a2).b();
            return z;
        } catch (OutOfMemoryError unused3) {
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        a.a("FileSystem.save", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.a(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.delete", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.a(abVar, new File(u.b(abVar.c(), "filepath")));
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.listing", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.b(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.load", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.c(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.rename", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.d(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.exists", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.e(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.extract", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.f(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.unpack_bundle", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.g(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.create_directory", (ad) new ad() {
            public void a(final ab abVar) {
                r.this.a((Runnable) new Runnable() {
                    public void run() {
                        r.this.h(abVar);
                        r.this.b();
                    }
                });
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ab abVar) {
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        String b3 = u.b(c, "data");
        String b4 = u.b(c, "encoding");
        boolean z = b4 != null && b4.equals("utf8");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            a(b2, b3, z);
            u.b(a2, "success", true);
            abVar.a(a2).b();
            return true;
        } catch (IOException unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, String str2, boolean z) throws IOException {
        BufferedWriter bufferedWriter = z ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(str), "UTF-8")) : new BufferedWriter(new OutputStreamWriter(new FileOutputStream(str)));
        bufferedWriter.write(str2);
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ab abVar, File file) {
        a.a().o().b();
        JSONObject a2 = u.a();
        if (a(file)) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            return true;
        }
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(File file) {
        try {
            if (!file.isDirectory()) {
                return file.delete();
            }
            if (file.list().length == 0) {
                return file.delete();
            }
            String[] list = file.list();
            if (list.length > 0) {
                return a(new File(file, list[0]));
            }
            if (file.list().length == 0) {
                return file.delete();
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b(ab abVar) {
        String b2 = u.b(abVar.c(), "filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        String[] list = new File(b2).list();
        if (list != null) {
            JSONArray b3 = u.b();
            for (String str : list) {
                JSONObject a3 = u.a();
                u.a(a3, "filename", str);
                StringBuilder sb = new StringBuilder();
                sb.append(b2);
                sb.append(str);
                if (new File(sb.toString()).isDirectory()) {
                    u.b(a3, "is_folder", true);
                } else {
                    u.b(a3, "is_folder", false);
                }
                u.a(b3, (Object) a3);
            }
            u.b(a2, "success", true);
            u.a(a2, "entries", b3);
            abVar.a(a2).b();
            return true;
        }
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public String c(ab abVar) {
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        String b3 = u.b(c, "encoding");
        boolean z = b3 != null && b3.equals("utf8");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            StringBuilder a3 = a(b2, z);
            u.b(a2, "success", true);
            u.a(a2, "data", a3.toString());
            abVar.a(a2).b();
            return a3.toString();
        } catch (IOException unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return "";
        }
    }

    /* access modifiers changed from: 0000 */
    public StringBuilder a(String str, boolean z) throws IOException {
        BufferedReader bufferedReader;
        File file = new File(str);
        StringBuilder sb = new StringBuilder((int) file.length());
        if (z) {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath())));
        }
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
                sb.append("\n");
            } else {
                bufferedReader.close();
                return sb;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public List<String> b(String str, boolean z) throws IOException {
        BufferedReader bufferedReader;
        File file = new File(str);
        file.length();
        ArrayList arrayList = new ArrayList();
        if (z) {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath())));
        }
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                arrayList.add(readLine);
            } else {
                bufferedReader.close();
                return arrayList;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean d(ab abVar) {
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        String b3 = u.b(c, "new_filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            if (new File(b2).renameTo(new File(b3))) {
                u.b(a2, "success", true);
                abVar.a(a2).b();
                return true;
            }
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        } catch (Exception unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean e(ab abVar) {
        String b2 = u.b(abVar.c(), "filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            boolean a3 = a(b2);
            u.b(a2, IronSourceConstants.EVENTS_RESULT, a3);
            u.b(a2, "success", true);
            abVar.a(a2).b();
            return a3;
        } catch (Exception e) {
            u.b(a2, IronSourceConstants.EVENTS_RESULT, false);
            u.b(a2, "success", false);
            abVar.a(a2).b();
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(String str) throws Exception {
        return new File(str).exists();
    }

    /* access modifiers changed from: private */
    public boolean h(ab abVar) {
        String b2 = u.b(abVar.c(), "filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            if (new File(b2).mkdir()) {
                u.b(a2, "success", true);
                abVar.a(a2).b();
                return true;
            }
            u.b(a2, "success", false);
            return false;
        } catch (Exception unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Runnable runnable) {
        if (!this.f954a.isEmpty() || this.b) {
            this.f954a.push(runnable);
            return;
        }
        this.b = true;
        runnable.run();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.b = false;
        if (!this.f954a.isEmpty()) {
            this.b = true;
            ((Runnable) this.f954a.removeLast()).run();
        }
    }
}
