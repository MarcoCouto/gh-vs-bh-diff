package com.adcolony.sdk;

import android.util.Log;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

class y {

    /* renamed from: a reason: collision with root package name */
    static boolean f982a = false;
    static final int b = 4000;
    static final int c = 4;
    static final int d = 3;
    static final int e = 2;
    static final int f = 1;
    static final int g = 0;
    static final int h = -1;
    static int i = 3;
    static JSONObject j = u.a();
    static int k = 1;
    static ai l;
    private static ExecutorService m = null;
    private static final Queue<Runnable> n = new ConcurrentLinkedQueue();

    y() {
    }

    static void a(int i2, String str, boolean z) {
        a(0, i2, str, z);
    }

    static void a(int i2, int i3, String str, boolean z) {
        if (!a(b(i2, i3, str, z))) {
            synchronized (n) {
                n.add(b(i2, i3, str, z));
            }
        }
    }

    private static Runnable b(final int i2, final int i3, final String str, final boolean z) {
        return new Runnable() {
            public void run() {
                y.a(i2, str, i3);
                int i = 0;
                while (i <= str.length() / y.b) {
                    int i2 = i * y.b;
                    i++;
                    int i3 = i * y.b;
                    if (i3 > str.length()) {
                        i3 = str.length();
                    }
                    if (i3 == 3 && y.a(u.f(y.j, Integer.toString(i2)), 3, z)) {
                        Log.d("AdColony [TRACE]", str.substring(i2, i3));
                    } else if (i3 == 2 && y.a(u.f(y.j, Integer.toString(i2)), 2, z)) {
                        Log.i("AdColony [INFO]", str.substring(i2, i3));
                    } else if (i3 == 1 && y.a(u.f(y.j, Integer.toString(i2)), 1, z)) {
                        Log.w("AdColony [WARNING]", str.substring(i2, i3));
                    } else if (i3 == 0 && y.a(u.f(y.j, Integer.toString(i2)), 0, z)) {
                        Log.e("AdColony [ERROR]", str.substring(i2, i3));
                    } else if (i3 == -1 && y.i >= -1) {
                        Log.e("AdColony [FATAL]", str.substring(i2, i3));
                    }
                }
            }
        };
    }

    static boolean a(JSONObject jSONObject, int i2, boolean z) {
        int c2 = u.c(jSONObject, "print_level");
        boolean d2 = u.d(jSONObject, "log_private");
        if (jSONObject.length() == 0) {
            c2 = i;
            d2 = f982a;
        }
        return (!z || d2) && c2 != 4 && c2 >= i2;
    }

    static void a() {
        if (m == null || m.isShutdown() || m.isTerminated()) {
            m = Executors.newSingleThreadExecutor();
        }
        synchronized (n) {
            while (!n.isEmpty()) {
                a((Runnable) n.poll());
            }
        }
    }

    static void b() {
        if (m != null) {
            m.shutdown();
            try {
                if (!m.awaitTermination(1, TimeUnit.SECONDS)) {
                    m.shutdownNow();
                    if (!m.awaitTermination(1, TimeUnit.SECONDS)) {
                        System.err.println("ADCLogManager: ScheduledExecutorService did not terminate");
                    }
                }
            } catch (InterruptedException unused) {
                m.shutdownNow();
                Thread.currentThread().interrupt();
            }
        }
    }

    static boolean a(JSONObject jSONObject, int i2) {
        int c2 = u.c(jSONObject, "send_level");
        if (jSONObject.length() == 0) {
            c2 = k;
        }
        return c2 >= i2 && c2 != 4;
    }

    static void a(HashMap<String, Object> hashMap) {
        try {
            l = new ai(new v(new URL("https://wd.adcolony.com/logs")), Executors.newSingleThreadScheduledExecutor(), hashMap);
            l.a(5, TimeUnit.SECONDS);
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
    }

    static void a(int i2, String str, int i3) {
        if (l != null) {
            if (i3 == 3 && a(u.f(j, Integer.toString(i2)), 3)) {
                l.c(str);
            } else if (i3 == 2 && a(u.f(j, Integer.toString(i2)), 2)) {
                l.d(str);
            } else if (i3 == 1 && a(u.f(j, Integer.toString(i2)), 1)) {
                l.e(str);
            } else if (i3 == 0 && a(u.f(j, Integer.toString(i2)), 0)) {
                l.f(str);
            }
        }
    }

    static void a(q qVar) {
        if (l != null && k != 4) {
            l.a(qVar);
        }
    }

    static void c() {
        a.a("Log.set_log_level", (ad) new ad() {
            public void a(ab abVar) {
                y.i = u.c(abVar.c(), "level");
            }
        });
        a.a("Log.public.trace", (ad) new ad() {
            public void a(ab abVar) {
                y.a(u.c(abVar.c(), "module"), 3, u.b(abVar.c(), "message"), false);
            }
        });
        a.a("Log.private.trace", (ad) new ad() {
            public void a(ab abVar) {
                y.a(u.c(abVar.c(), "module"), 3, u.b(abVar.c(), "message"), true);
            }
        });
        a.a("Log.public.info", (ad) new ad() {
            public void a(ab abVar) {
                y.a(u.c(abVar.c(), "module"), 2, u.b(abVar.c(), "message"), false);
            }
        });
        a.a("Log.private.info", (ad) new ad() {
            public void a(ab abVar) {
                y.a(u.c(abVar.c(), "module"), 2, u.b(abVar.c(), "message"), true);
            }
        });
        a.a("Log.public.warning", (ad) new ad() {
            public void a(ab abVar) {
                y.a(u.c(abVar.c(), "module"), 1, u.b(abVar.c(), "message"), false);
            }
        });
        a.a("Log.private.warning", (ad) new ad() {
            public void a(ab abVar) {
                y.a(u.c(abVar.c(), "module"), 1, u.b(abVar.c(), "message"), true);
            }
        });
        a.a("Log.public.error", (ad) new ad() {
            public void a(ab abVar) {
                y.a(u.c(abVar.c(), "module"), 0, u.b(abVar.c(), "message"), false);
            }
        });
        a.a("Log.private.error", (ad) new ad() {
            public void a(ab abVar) {
                y.a(u.c(abVar.c(), "module"), 0, u.b(abVar.c(), "message"), true);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONArray jSONArray) {
        j = b(jSONArray);
    }

    /* access modifiers changed from: 0000 */
    public JSONObject b(JSONArray jSONArray) {
        JSONObject a2 = u.a();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject d2 = u.d(jSONArray, i2);
            u.a(a2, Integer.toString(u.c(d2, "id")), d2);
        }
        return a2;
    }

    private static boolean a(Runnable runnable) {
        try {
            if (m != null && !m.isShutdown() && !m.isTerminated()) {
                m.submit(runnable);
                return true;
            }
        } catch (RejectedExecutionException unused) {
            Log.e("ADCLogError", "Internal error when submitting log to executor service.");
        }
        return false;
    }
}
