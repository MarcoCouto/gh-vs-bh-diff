package com.adcolony.sdk;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class d {

    /* renamed from: a reason: collision with root package name */
    private HashMap<String, c> f829a;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AdColonyInterstitial> b;
    private HashMap<String, AdColonyAdViewListener> c;
    /* access modifiers changed from: private */
    public HashMap<String, AdColonyAdView> d;
    private HashMap<String, e> e;

    d() {
    }

    /* access modifiers changed from: private */
    public boolean d(ab abVar) {
        final String b2 = u.b(abVar.c(), "id");
        final AdColonyAdViewListener adColonyAdViewListener = (AdColonyAdViewListener) this.c.remove(b2);
        if (adColonyAdViewListener == null) {
            a(abVar.d(), b2);
            return false;
        }
        final Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        final ab abVar2 = abVar;
        AnonymousClass1 r2 = new Runnable() {
            public void run() {
                AdColonyAdView adColonyAdView = new AdColonyAdView(c2, abVar2, adColonyAdViewListener);
                d.this.d.put(b2, adColonyAdView);
                adColonyAdView.setOmidManager(adColonyAdViewListener.c());
                adColonyAdView.a();
                adColonyAdViewListener.a((ag) null);
                adColonyAdViewListener.onRequestFilled(adColonyAdView);
            }
        };
        at.a((Runnable) r2);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean e(ab abVar) {
        String b2 = u.b(abVar.c(), "id");
        final AdColonyAdViewListener adColonyAdViewListener = (AdColonyAdViewListener) this.c.remove(b2);
        if (adColonyAdViewListener == null) {
            a(abVar.d(), b2);
            return false;
        }
        at.a((Runnable) new Runnable() {
            public void run() {
                String a2 = adColonyAdViewListener.a();
                AdColonyZone adColonyZone = (AdColonyZone) a.a().f().get(a2);
                if (adColonyZone == null) {
                    adColonyZone = new AdColonyZone(a2);
                    adColonyZone.b(6);
                }
                adColonyAdViewListener.onRequestNotFilled(adColonyZone);
            }
        });
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.f829a = new HashMap<>();
        this.b = new ConcurrentHashMap<>();
        this.c = new HashMap<>();
        this.d = new HashMap<>();
        this.e = new HashMap<>();
        a.a("AdContainer.create", (ad) new ad() {
            public void a(final ab abVar) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        d.this.j(abVar);
                    }
                });
            }
        });
        a.a("AdContainer.destroy", (ad) new ad() {
            public void a(final ab abVar) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        d.this.k(abVar);
                    }
                });
            }
        });
        a.a("AdContainer.move_view_to_index", (ad) new ad() {
            public void a(ab abVar) {
                d.this.l(abVar);
            }
        });
        a.a("AdContainer.move_view_to_front", (ad) new ad() {
            public void a(ab abVar) {
                d.this.m(abVar);
            }
        });
        a.a("AdSession.finish_fullscreen_ad", (ad) new ad() {
            public void a(ab abVar) {
                d.this.i(abVar);
            }
        });
        a.a("AdSession.start_fullscreen_ad", (ad) new ad() {
            public void a(ab abVar) {
                d.this.h(abVar);
            }
        });
        a.a("AdSession.ad_view_available", (ad) new ad() {
            public void a(ab abVar) {
                d.this.d(abVar);
            }
        });
        a.a("AdSession.ad_view_unavailable", (ad) new ad() {
            public void a(ab abVar) {
                d.this.e(abVar);
            }
        });
        a.a("AdSession.expiring", (ad) new ad() {
            public void a(ab abVar) {
                d.this.a(abVar);
            }
        });
        a.a("AdSession.audio_stopped", (ad) new ad() {
            public void a(final ab abVar) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) d.this.b.get(u.b(abVar.c(), "id"));
                        if (adColonyInterstitial != null && adColonyInterstitial.getListener() != null) {
                            adColonyInterstitial.getListener().onAudioStopped(adColonyInterstitial);
                        }
                    }
                });
            }
        });
        a.a("AdSession.audio_started", (ad) new ad() {
            public void a(final ab abVar) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) d.this.b.get(u.b(abVar.c(), "id"));
                        if (adColonyInterstitial != null && adColonyInterstitial.getListener() != null) {
                            adColonyInterstitial.getListener().onAudioStarted(adColonyInterstitial);
                        }
                    }
                });
            }
        });
        a.a("AudioPlayer.create", (ad) new ad() {
            public void a(ab abVar) {
                d.this.n(abVar);
            }
        });
        a.a("AudioPlayer.destroy", (ad) new ad() {
            public void a(ab abVar) {
                if (d.this.c(abVar)) {
                    d.this.o(abVar);
                }
            }
        });
        a.a("AudioPlayer.play", (ad) new ad() {
            public void a(ab abVar) {
                if (d.this.c(abVar)) {
                    d.this.p(abVar);
                }
            }
        });
        a.a("AudioPlayer.pause", (ad) new ad() {
            public void a(ab abVar) {
                if (d.this.c(abVar)) {
                    d.this.q(abVar);
                }
            }
        });
        a.a("AudioPlayer.stop", (ad) new ad() {
            public void a(ab abVar) {
                if (d.this.c(abVar)) {
                    d.this.r(abVar);
                }
            }
        });
        a.a("AdSession.interstitial_available", (ad) new ad() {
            public void a(ab abVar) {
                d.this.g(abVar);
            }
        });
        a.a("AdSession.interstitial_unavailable", (ad) new ad() {
            public void a(ab abVar) {
                d.this.b(abVar);
            }
        });
        a.a("AdSession.has_audio", (ad) new ad() {
            public void a(ab abVar) {
                d.this.f(abVar);
            }
        });
        a.a("WebView.prepare", (ad) new ad() {
            public void a(ab abVar) {
                JSONObject a2 = u.a();
                u.b(a2, "success", true);
                abVar.a(a2).b();
            }
        });
        a.a("AdSession.expanded", (ad) new ad() {
            public void a(final ab abVar) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        abVar.a(abVar.c()).b();
                    }
                });
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ab abVar) {
        final AdColonyInterstitialListener adColonyInterstitialListener;
        JSONObject c2 = abVar.c();
        String b2 = u.b(c2, "id");
        if (u.c(c2, "type") == 0) {
            final AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.remove(b2);
            if (adColonyInterstitial == null) {
                adColonyInterstitialListener = null;
            } else {
                adColonyInterstitialListener = adColonyInterstitial.getListener();
            }
            if (adColonyInterstitialListener == null) {
                a(abVar.d(), b2);
                return false;
            } else if (!a.d()) {
                return false;
            } else {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        adColonyInterstitial.a(true);
                        adColonyInterstitialListener.onExpiring(adColonyInterstitial);
                        m r = a.a().r();
                        if (r.b() != null) {
                            r.b().dismiss();
                            r.a((AlertDialog) null);
                        }
                    }
                });
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean f(ab abVar) {
        String b2 = u.b(abVar.c(), "id");
        JSONObject a2 = u.a();
        u.a(a2, "id", b2);
        Context c2 = a.c();
        if (c2 == null) {
            u.b(a2, "has_audio", false);
            abVar.a(a2).b();
            return false;
        }
        boolean a3 = at.a(at.a(c2));
        double b3 = at.b(at.a(c2));
        u.b(a2, "has_audio", a3);
        u.a(a2, AvidVideoPlaybackListenerImpl.VOLUME, b3);
        abVar.a(a2).b();
        return a3;
    }

    /* access modifiers changed from: private */
    public boolean g(final ab abVar) {
        final AdColonyInterstitialListener adColonyInterstitialListener;
        String b2 = u.b(abVar.c(), "id");
        final AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.get(b2);
        if (adColonyInterstitial == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = adColonyInterstitial.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(abVar.d(), b2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            at.a((Runnable) new Runnable() {
                public void run() {
                    if (adColonyInterstitial.h() == null) {
                        adColonyInterstitial.a(u.f(abVar.c(), "iab"));
                    }
                    adColonyInterstitial.a(u.b(abVar.c(), "ad_id"));
                    adColonyInterstitial.b(u.b(abVar.c(), "creative_id"));
                    ag h = adColonyInterstitial.h();
                    if (!(h == null || h.c() == 2)) {
                        try {
                            h.b();
                        } catch (IllegalArgumentException unused) {
                            new a().a("IllegalArgumentException when creating omid session").a(w.h);
                        }
                    }
                    adColonyInterstitialListener.onRequestFilled(adColonyInterstitial);
                }
            });
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b(ab abVar) {
        final AdColonyInterstitialListener adColonyInterstitialListener;
        String b2 = u.b(abVar.c(), "id");
        final AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.remove(b2);
        if (adColonyInterstitial == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = adColonyInterstitial.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(abVar.d(), b2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            at.a((Runnable) new Runnable() {
                public void run() {
                    AdColonyZone adColonyZone = (AdColonyZone) a.a().f().get(adColonyInterstitial.getZoneID());
                    if (adColonyZone == null) {
                        adColonyZone = new AdColonyZone(adColonyInterstitial.getZoneID());
                        adColonyZone.b(6);
                    }
                    adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
                }
            });
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean c(ab abVar) {
        String b2 = u.b(abVar.c(), "ad_session_id");
        c cVar = (c) this.f829a.get(b2);
        e eVar = (e) this.e.get(b2);
        if (cVar != null && eVar != null) {
            return true;
        }
        new a().a("Invalid AudioPlayer message!").a(w.g);
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, AdColonyAdViewListener adColonyAdViewListener, AdColonyAdSize adColonyAdSize, AdColonyAdOptions adColonyAdOptions) {
        String e2 = at.e();
        JSONObject a2 = u.a();
        float p = a.a().m().p();
        u.a(a2, "zone_id", str);
        u.b(a2, "type", 1);
        u.b(a2, "width_pixels", (int) (((float) adColonyAdSize.getWidth()) * p));
        u.b(a2, "height_pixels", (int) (((float) adColonyAdSize.getHeight()) * p));
        u.b(a2, "width", adColonyAdSize.getWidth());
        u.b(a2, "height", adColonyAdSize.getHeight());
        u.a(a2, "id", e2);
        adColonyAdViewListener.a(str);
        adColonyAdViewListener.a(adColonyAdSize);
        if (!(adColonyAdOptions == null || adColonyAdOptions.d == null)) {
            u.a(a2, "options", adColonyAdOptions.d);
        }
        this.c.put(e2, adColonyAdViewListener);
        new ab("AdSession.on_request", 1, a2).b();
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, AdColonyInterstitialListener adColonyInterstitialListener, AdColonyAdOptions adColonyAdOptions) {
        String e2 = at.e();
        j a2 = a.a();
        JSONObject a3 = u.a();
        u.a(a3, "zone_id", str);
        u.b(a3, Events.CREATIVE_FULLSCREEN, true);
        u.b(a3, "width", a2.m().q());
        u.b(a3, "height", a2.m().r());
        u.b(a3, "type", 0);
        u.a(a3, "id", e2);
        new a().a("AdSession request with id = ").a(e2).a(w.b);
        AdColonyInterstitial adColonyInterstitial = new AdColonyInterstitial(e2, adColonyInterstitialListener, str);
        this.b.put(e2, adColonyInterstitial);
        if (!(adColonyAdOptions == null || adColonyAdOptions.d == null)) {
            adColonyInterstitial.a(adColonyAdOptions);
            u.a(a3, "options", adColonyAdOptions.d);
        }
        new a().a("Requesting AdColony interstitial advertisement.").a(w.f979a);
        new ab("AdSession.on_request", 1, a3).b();
    }

    /* access modifiers changed from: private */
    public boolean h(ab abVar) {
        JSONObject c2 = abVar.c();
        String b2 = u.b(c2, "id");
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.get(b2);
        AdColonyAdView adColonyAdView = (AdColonyAdView) this.d.get(b2);
        int a2 = u.a(c2, "orientation", -1);
        boolean z = adColonyAdView != null;
        if (adColonyInterstitial != null || z) {
            JSONObject a3 = u.a();
            u.a(a3, "id", b2);
            if (adColonyInterstitial != null) {
                adColonyInterstitial.a(u.c(a3, "module_id"));
                if (a2 == 0 || a2 == 1) {
                    adColonyInterstitial.b(a2);
                }
                adColonyInterstitial.a();
            }
            return true;
        }
        a(abVar.d(), b2);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean i(ab abVar) {
        final AdColonyInterstitialListener adColonyInterstitialListener;
        JSONObject c2 = abVar.c();
        int c3 = u.c(c2, "status");
        if (c3 == 5 || c3 == 1 || c3 == 0 || c3 == 6) {
            return false;
        }
        String b2 = u.b(c2, "id");
        final AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.remove(b2);
        if (adColonyInterstitial == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = adColonyInterstitial.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(abVar.d(), b2);
            return false;
        }
        at.a((Runnable) new Runnable() {
            public void run() {
                a.a().c(false);
                adColonyInterstitialListener.onClosed(adColonyInterstitial);
            }
        });
        adColonyInterstitial.a((c) null);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean j(ab abVar) {
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        JSONObject c3 = abVar.c();
        String b2 = u.b(c3, "ad_session_id");
        c cVar = new c(c2.getApplicationContext(), b2);
        cVar.b(abVar);
        this.f829a.put(b2, cVar);
        if (u.c(c3, "width") != 0) {
            cVar.a(false);
        } else if (((AdColonyInterstitial) this.b.get(b2)) == null) {
            a(abVar.d(), b2);
            return false;
        } else {
            ((AdColonyInterstitial) this.b.get(b2)).a(cVar);
        }
        JSONObject a2 = u.a();
        u.b(a2, "success", true);
        abVar.a(a2).b();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean k(ab abVar) {
        String b2 = u.b(abVar.c(), "ad_session_id");
        c cVar = (c) this.f829a.get(b2);
        if (cVar == null) {
            a(abVar.d(), b2);
            return false;
        }
        a(cVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a(final c cVar) {
        at.a((Runnable) new Runnable() {
            public void run() {
                for (int i = 0; i < cVar.m().size(); i++) {
                    a.b((String) cVar.n().get(i), (ad) cVar.m().get(i));
                }
                cVar.n().clear();
                cVar.m().clear();
                cVar.removeAllViews();
                cVar.d = null;
                cVar.c = null;
                new a().a("Destroying container tied to ad_session_id = ").a(cVar.b()).a(w.d);
                for (av avVar : cVar.g().values()) {
                    if (!avVar.m()) {
                        int b2 = avVar.b();
                        if (b2 <= 0) {
                            b2 = avVar.a();
                        }
                        a.a().a(b2);
                        avVar.loadUrl("about:blank");
                        avVar.clearCache(true);
                        avVar.removeAllViews();
                        avVar.a(true);
                    }
                }
                new a().a("Stopping and releasing all media players associated with ").a("VideoViews tied to ad_session_id = ").a(cVar.b()).a(w.d);
                for (au auVar : cVar.e().values()) {
                    auVar.d();
                    auVar.g();
                }
                cVar.e().clear();
                cVar.f().clear();
                cVar.g().clear();
                cVar.j().clear();
                cVar.l().clear();
                cVar.i().clear();
                cVar.k().clear();
                cVar.f814a = true;
            }
        });
        AdColonyAdView adColonyAdView = (AdColonyAdView) this.d.get(cVar.b());
        if (adColonyAdView == null || adColonyAdView.d()) {
            new a().a("Removing ad 4").a(w.b);
            this.f829a.remove(cVar.b());
            cVar.c = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, String str2) {
        new a().a("Message '").a(str).a("' sent with invalid id: ").a(str2).a(w.g);
    }

    /* access modifiers changed from: private */
    public boolean l(ab abVar) {
        JSONObject c2 = abVar.c();
        String d2 = abVar.d();
        String b2 = u.b(c2, "ad_session_id");
        int c3 = u.c(c2, "view_id");
        c cVar = (c) this.f829a.get(b2);
        View view = (View) cVar.l().get(Integer.valueOf(c3));
        if (cVar == null) {
            a(d2, b2);
            return false;
        } else if (view == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(c3);
            a(d2, sb.toString());
            return false;
        } else {
            view.bringToFront();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean m(ab abVar) {
        JSONObject c2 = abVar.c();
        String d2 = abVar.d();
        String b2 = u.b(c2, "ad_session_id");
        int c3 = u.c(c2, "view_id");
        c cVar = (c) this.f829a.get(b2);
        if (cVar == null) {
            a(d2, b2);
            return false;
        }
        View view = (View) cVar.l().get(Integer.valueOf(c3));
        if (view == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(c3);
            a(d2, sb.toString());
            return false;
        }
        cVar.removeView(view);
        cVar.addView(view, view.getLayoutParams());
        return true;
    }

    /* access modifiers changed from: private */
    public boolean n(ab abVar) {
        String b2 = u.b(abVar.c(), "ad_session_id");
        c cVar = (c) this.f829a.get(b2);
        if (cVar == null) {
            a(abVar.d(), b2);
            return false;
        }
        e eVar = (e) this.e.get(b2);
        if (eVar == null) {
            eVar = new e(b2, cVar.c());
            this.e.put(b2, eVar);
        }
        eVar.a(abVar);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean o(ab abVar) {
        String b2 = u.b(abVar.c(), "ad_session_id");
        e eVar = (e) this.e.get(b2);
        if (eVar == null) {
            a(abVar.d(), b2);
            return false;
        }
        eVar.d(abVar);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean p(ab abVar) {
        String b2 = u.b(abVar.c(), "ad_session_id");
        e eVar = (e) this.e.get(b2);
        if (eVar == null) {
            a(abVar.d(), b2);
            return false;
        }
        eVar.c(abVar);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean q(ab abVar) {
        String b2 = u.b(abVar.c(), "ad_session_id");
        e eVar = (e) this.e.get(b2);
        if (eVar == null) {
            a(abVar.d(), b2);
            return false;
        }
        eVar.b(abVar);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean r(ab abVar) {
        String b2 = u.b(abVar.c(), "ad_session_id");
        e eVar = (e) this.e.get(b2);
        if (eVar == null) {
            a(abVar.d(), b2);
            return false;
        }
        eVar.e(abVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, c> b() {
        return this.f829a;
    }

    /* access modifiers changed from: 0000 */
    public ConcurrentHashMap<String, AdColonyInterstitial> c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, AdColonyAdViewListener> d() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, AdColonyAdView> e() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, e> f() {
        return this.e;
    }
}
