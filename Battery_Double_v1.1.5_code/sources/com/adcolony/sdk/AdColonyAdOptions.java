package com.adcolony.sdk;

import android.support.annotation.NonNull;
import org.json.JSONObject;

public class AdColonyAdOptions {

    /* renamed from: a reason: collision with root package name */
    boolean f710a;
    boolean b;
    AdColonyUserMetadata c;
    JSONObject d = u.a();

    public AdColonyAdOptions enableConfirmationDialog(boolean z) {
        this.f710a = z;
        u.b(this.d, "confirmation_enabled", true);
        return this;
    }

    public AdColonyAdOptions enableResultsDialog(boolean z) {
        this.b = z;
        u.b(this.d, "results_enabled", true);
        return this;
    }

    public AdColonyAdOptions setOption(@NonNull String str, boolean z) {
        if (at.d(str)) {
            u.b(this.d, str, z);
        }
        return this;
    }

    public Object getOption(@NonNull String str) {
        return u.a(this.d, str);
    }

    public AdColonyAdOptions setOption(@NonNull String str, double d2) {
        if (at.d(str)) {
            u.a(this.d, str, d2);
        }
        return this;
    }

    public AdColonyAdOptions setOption(@NonNull String str, @NonNull String str2) {
        if (str != null && at.d(str) && at.d(str2)) {
            u.a(this.d, str, str2);
        }
        return this;
    }

    public AdColonyAdOptions setUserMetadata(@NonNull AdColonyUserMetadata adColonyUserMetadata) {
        this.c = adColonyUserMetadata;
        u.a(this.d, "user_metadata", adColonyUserMetadata.c);
        return this;
    }

    public AdColonyUserMetadata getUserMetadata() {
        return this.c;
    }
}
