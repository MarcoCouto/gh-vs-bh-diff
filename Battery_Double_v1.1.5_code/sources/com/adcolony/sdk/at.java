package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.Toast;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.CRC32;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class at {

    /* renamed from: a reason: collision with root package name */
    static final int f781a = 128;
    static ExecutorService b = Executors.newSingleThreadExecutor();
    static Handler c;

    static class a {

        /* renamed from: a reason: collision with root package name */
        double f783a;
        double b = ((double) System.currentTimeMillis());

        a(double d) {
            a(d);
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            a(this.f783a);
        }

        /* access modifiers changed from: 0000 */
        public void a(double d) {
            this.f783a = d;
            double currentTimeMillis = (double) System.currentTimeMillis();
            Double.isNaN(currentTimeMillis);
            this.b = (currentTimeMillis / 1000.0d) + this.f783a;
        }

        /* access modifiers changed from: 0000 */
        public boolean b() {
            return c() == Utils.DOUBLE_EPSILON;
        }

        /* access modifiers changed from: 0000 */
        public double c() {
            double currentTimeMillis = (double) System.currentTimeMillis();
            Double.isNaN(currentTimeMillis);
            double d = this.b - (currentTimeMillis / 1000.0d);
            return d <= Utils.DOUBLE_EPSILON ? Utils.DOUBLE_EPSILON : d;
        }

        public String toString() {
            return at.a(c(), 2);
        }
    }

    at() {
    }

    static boolean a(String str) {
        Application application;
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        try {
            if (c2 instanceof Application) {
                application = (Application) c2;
            } else {
                application = ((Activity) c2).getApplication();
            }
            application.getPackageManager().getApplicationInfo(str, 0);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    static boolean a() {
        boolean z = false;
        try {
            j a2 = a.a();
            StringBuilder sb = new StringBuilder();
            sb.append(a2.o().g());
            sb.append("026ae9c9824b3e483fa6c71fa88f57ae27816141");
            File file = new File(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(a2.o().g());
            sb2.append("7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5");
            File file2 = new File(sb2.toString());
            boolean a3 = a2.j().a(file);
            boolean a4 = a2.j().a(file2);
            if (a3 && a4) {
                z = true;
            }
            return z;
        } catch (Exception unused) {
            new a().a("Unable to delete controller or launch response.").a(w.h);
            return false;
        }
    }

    static String b() {
        Context c2 = a.c();
        if (c2 == null) {
            return "1.0";
        }
        try {
            return c2.getPackageManager().getPackageInfo(c2.getPackageName(), 0).versionName;
        } catch (Exception unused) {
            new a().a("Failed to retrieve package info.").a(w.h);
            return "1.0";
        }
    }

    static int c() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0;
        }
        try {
            return c2.getPackageManager().getPackageInfo(c2.getPackageName(), 0).versionCode;
        } catch (Exception unused) {
            new a().a("Failed to retrieve package info.").a(w.h);
            return 0;
        }
    }

    static String d() {
        Application application;
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        if (c2 instanceof Application) {
            application = (Application) c2;
        } else {
            application = ((Activity) c2).getApplication();
        }
        PackageManager packageManager = application.getPackageManager();
        try {
            CharSequence applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(c2.getPackageName(), 0));
            return applicationLabel == null ? "" : applicationLabel.toString();
        } catch (Exception unused) {
            new a().a("Failed to retrieve application label.").a(w.h);
            return "";
        }
    }

    static int b(String str) {
        CRC32 crc32 = new CRC32();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            crc32.update(str.charAt(i));
        }
        return (int) crc32.getValue();
    }

    static String c(String str) {
        try {
            return aw.a(str);
        } catch (Exception unused) {
            return null;
        }
    }

    static String e() {
        return UUID.randomUUID().toString();
    }

    static JSONArray a(int i) {
        JSONArray b2 = u.b();
        for (int i2 = 0; i2 < i; i2++) {
            u.a(b2, e());
        }
        return b2;
    }

    static boolean a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length != strArr2.length) {
            return false;
        }
        Arrays.sort(strArr);
        Arrays.sort(strArr2);
        return Arrays.equals(strArr, strArr2);
    }

    static boolean a(Runnable runnable) {
        Looper mainLooper = Looper.getMainLooper();
        if (mainLooper == null) {
            return false;
        }
        if (c == null) {
            c = new Handler(mainLooper);
        }
        if (mainLooper == Looper.myLooper()) {
            runnable.run();
        } else {
            c.post(runnable);
        }
        return true;
    }

    static double f() {
        double currentTimeMillis = (double) System.currentTimeMillis();
        Double.isNaN(currentTimeMillis);
        return currentTimeMillis / 1000.0d;
    }

    static boolean d(String str) {
        if (str != null && str.length() <= 128) {
            return true;
        }
        new a().a("String must be non-null and the max length is 128 characters.").a(w.e);
        return false;
    }

    static boolean a(AudioManager audioManager) {
        boolean z = false;
        if (audioManager == null) {
            new a().a("isAudioEnabled() called with a null AudioManager").a(w.h);
            return false;
        }
        try {
            if (audioManager.getStreamVolume(3) > 0) {
                z = true;
            }
            return z;
        } catch (Exception e) {
            new a().a("Exception occurred when accessing AudioManager.getStreamVolume: ").a(e.toString()).a(w.h);
            return false;
        }
    }

    static double b(AudioManager audioManager) {
        if (audioManager == null) {
            new a().a("getAudioVolume() called with a null AudioManager").a(w.h);
            return Utils.DOUBLE_EPSILON;
        }
        try {
            double streamVolume = (double) audioManager.getStreamVolume(3);
            double streamMaxVolume = (double) audioManager.getStreamMaxVolume(3);
            if (streamMaxVolume == Utils.DOUBLE_EPSILON) {
                return Utils.DOUBLE_EPSILON;
            }
            Double.isNaN(streamVolume);
            Double.isNaN(streamMaxVolume);
            return streamVolume / streamMaxVolume;
        } catch (Exception e) {
            new a().a("Exception occurred when accessing AudioManager: ").a(e.toString()).a(w.h);
            return Utils.DOUBLE_EPSILON;
        }
    }

    static AudioManager a(Context context) {
        if (context != null) {
            return (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        }
        new a().a("getAudioManager called with a null Context").a(w.h);
        return null;
    }

    static void e(String str) {
        File[] listFiles = new File(str).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    new a().a(">").a(file.getAbsolutePath()).a(w.b);
                    e(file.getAbsolutePath());
                } else {
                    new a().a(file.getAbsolutePath()).a(w.b);
                }
            }
        }
    }

    static String a(double d, int i) {
        StringBuilder sb = new StringBuilder();
        a(d, i, sb);
        return sb.toString();
    }

    static void a(double d, int i, StringBuilder sb) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            sb.append(d);
            return;
        }
        if (d < Utils.DOUBLE_EPSILON) {
            d = -d;
            sb.append('-');
        }
        if (i == 0) {
            sb.append(Math.round(d));
            return;
        }
        long pow = (long) Math.pow(10.0d, (double) i);
        double d2 = (double) pow;
        Double.isNaN(d2);
        long round = Math.round(d * d2);
        sb.append(round / pow);
        sb.append('.');
        long j = round % pow;
        if (j == 0) {
            for (int i2 = 0; i2 < i; i2++) {
                sb.append('0');
            }
            return;
        }
        for (long j2 = j * 10; j2 < pow; j2 *= 10) {
            sb.append('0');
        }
        sb.append(j);
    }

    static String f(String str) {
        return str == null ? "" : URLDecoder.decode(str);
    }

    static String b(@NonNull Context context) {
        try {
            return context.getPackageName();
        } catch (Exception unused) {
            return "unknown";
        }
    }

    static String a(Exception exc) {
        StringWriter stringWriter = new StringWriter();
        exc.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    static int g(String str) {
        try {
            return (int) Long.parseLong(str, 16);
        } catch (NumberFormatException unused) {
            new a().a("Unable to parse '").a(str).a("' as a color.").a(w.f);
            return ViewCompat.MEASURED_STATE_MASK;
        }
    }

    static int c(Context context) {
        if (context == null) {
            return 0;
        }
        int identifier = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return context.getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    static boolean g() {
        Context c2 = a.c();
        return c2 != null && VERSION.SDK_INT >= 24 && (c2 instanceof Activity) && ((Activity) c2).isInMultiWindowMode();
    }

    static boolean a(String str, File file) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[8192];
                while (true) {
                    try {
                        int read = fileInputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        instance.update(bArr, 0, read);
                    } catch (IOException e) {
                        throw new RuntimeException("Unable to process file for MD5", e);
                    } catch (Throwable th) {
                        try {
                            fileInputStream.close();
                        } catch (IOException unused) {
                            new a().a("Exception on closing MD5 input stream").a(w.h);
                        }
                        throw th;
                    }
                }
                boolean equals = str.equals(String.format("%40s", new Object[]{new BigInteger(1, instance.digest()).toString(16)}).replace(' ', '0'));
                try {
                    fileInputStream.close();
                } catch (IOException unused2) {
                    new a().a("Exception on closing MD5 input stream").a(w.h);
                }
                return equals;
            } catch (FileNotFoundException unused3) {
                new a().a("Exception while getting FileInputStream").a(w.h);
                return false;
            }
        } catch (NoSuchAlgorithmException unused4) {
            new a().a("Exception while getting Digest").a(w.h);
            return false;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:4|5|6) */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0024, code lost:
        return new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", java.util.Locale.US).parse(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        return new java.text.SimpleDateFormat("yyyy-MM-dd", java.util.Locale.US).parse(r5);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0020 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0025 */
    static Date h(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ", Locale.US);
        return simpleDateFormat.parse(str);
    }

    static String a(JSONArray jSONArray) throws JSONException {
        String str = "";
        for (int i = 0; i < jSONArray.length(); i++) {
            if (i > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(",");
                str = sb.toString();
            }
            switch (jSONArray.getInt(i)) {
                case 1:
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append("MO");
                    str = sb2.toString();
                    break;
                case 2:
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append("TU");
                    str = sb3.toString();
                    break;
                case 3:
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str);
                    sb4.append("WE");
                    str = sb4.toString();
                    break;
                case 4:
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(str);
                    sb5.append("TH");
                    str = sb5.toString();
                    break;
                case 5:
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append(str);
                    sb6.append("FR");
                    str = sb6.toString();
                    break;
                case 6:
                    StringBuilder sb7 = new StringBuilder();
                    sb7.append(str);
                    sb7.append("SA");
                    str = sb7.toString();
                    break;
                case 7:
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(str);
                    sb8.append("SU");
                    str = sb8.toString();
                    break;
            }
        }
        return str;
    }

    static String b(JSONArray jSONArray) throws JSONException {
        String str = "";
        for (int i = 0; i < jSONArray.length(); i++) {
            if (i > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(",");
                str = sb.toString();
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(jSONArray.getInt(i));
            str = sb2.toString();
        }
        return str;
    }

    static boolean a(Intent intent, boolean z) {
        try {
            Context c2 = a.c();
            if (c2 == null) {
                return false;
            }
            AdColonyInterstitial v = a.a().v();
            if (v != null && v.g()) {
                v.h().d();
            }
            if (z) {
                c2.startActivity(Intent.createChooser(intent, "Handle this via..."));
            } else {
                c2.startActivity(intent);
            }
            return true;
        } catch (Exception e) {
            new a().a(e.toString()).a(w.f);
            return false;
        }
    }

    static boolean a(Intent intent) {
        return a(intent, false);
    }

    static boolean a(final String str, final int i) {
        final Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        a((Runnable) new Runnable() {
            public void run() {
                Toast.makeText(c2, str, i).show();
            }
        });
        return true;
    }

    private static void k(String str) {
        Context c2 = a.c();
        if (c2 != null) {
            try {
                InputStream open = c2.getAssets().open(str);
                StringBuilder sb = new StringBuilder();
                sb.append(a.a().o().d());
                sb.append(str);
                FileOutputStream fileOutputStream = new FileOutputStream(sb.toString());
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = open.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                open.close();
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (Exception e) {
                new a().a("Failed copy hardcoded ad unit file named: ").a(str).a(" with error: ").a(e.getMessage()).a(w.h);
            }
        }
    }

    public static void i(String str) {
        Context c2 = a.c();
        if (c2 != null) {
            try {
                String[] list = c2.getAssets().list(str);
                if (list.length == 0) {
                    k(str);
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(a.a().o().d());
                    sb.append(str);
                    File file = new File(sb.toString());
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    for (String append : list) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append("/");
                        sb2.append(append);
                        i(sb2.toString());
                    }
                }
            } catch (IOException e) {
                new a().a("Failed copy hardcoded ad unit with error: ").a(e.getMessage()).a(w.h);
            }
        }
    }

    static int a(ao aoVar) {
        int i = 0;
        try {
            Context c2 = a.c();
            if (c2 != null) {
                int i2 = (int) (c2.getPackageManager().getPackageInfo(c2.getPackageName(), 0).lastUpdateTime / 1000);
                StringBuilder sb = new StringBuilder();
                sb.append(aoVar.g());
                sb.append("AppVersion");
                boolean exists = new File(sb.toString()).exists();
                boolean z = true;
                if (exists) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(aoVar.g());
                    sb2.append("AppVersion");
                    if (u.c(u.c(sb2.toString()), "last_update") != i2) {
                        i = 1;
                    } else {
                        z = false;
                    }
                } else {
                    i = 2;
                }
                if (z) {
                    JSONObject a2 = u.a();
                    u.b(a2, "last_update", i2);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(aoVar.g());
                    sb3.append("AppVersion");
                    u.h(a2, sb3.toString());
                }
                return i;
            }
        } catch (Exception unused) {
        }
        return 0;
    }

    static JSONArray d(Context context) {
        JSONArray b2 = u.b();
        if (context == null) {
            return b2;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
            if (packageInfo.requestedPermissions == null) {
                return b2;
            }
            JSONArray b3 = u.b();
            int i = 0;
            while (i < packageInfo.requestedPermissions.length) {
                try {
                    b3.put(packageInfo.requestedPermissions[i]);
                    i++;
                } catch (Exception unused) {
                }
            }
            return b3;
        } catch (Exception unused2) {
            return b2;
        }
    }

    static String h() {
        String str = "landscape";
        Context c2 = a.c();
        return (!(c2 instanceof Activity) || c2.getResources().getConfiguration().orientation != 1) ? str : "portrait";
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    static int j(String str) {
        char c2;
        int hashCode = str.hashCode();
        if (hashCode == 729267099) {
            if (str.equals("portrait")) {
                c2 = 0;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }
        } else if (hashCode == 1430647483 && str.equals("landscape")) {
            c2 = 1;
            switch (c2) {
                case 0:
                    return 0;
                case 1:
                    return 1;
                default:
                    return -1;
            }
        }
        c2 = 65535;
        switch (c2) {
            case 0:
                break;
            case 1:
                break;
        }
    }

    static int a(View view) {
        if (view == null) {
            return 0;
        }
        int[] iArr = {0, 0};
        view.getLocationOnScreen(iArr);
        return (int) (((float) iArr[0]) / a.a().m().p());
    }

    static int b(View view) {
        if (view == null) {
            return 0;
        }
        int[] iArr = {0, 0};
        view.getLocationOnScreen(iArr);
        return (int) (((float) iArr[1]) / a.a().m().p());
    }
}
