package com.adcolony.sdk;

import io.realm.BuildConfig;
import java.util.Date;
import org.json.JSONObject;

class q extends x {

    /* renamed from: a reason: collision with root package name */
    static final t f952a = new t("adcolony_fatal_reports", BuildConfig.VERSION_NAME, "Production");
    static final String b = "sourceFile";
    static final String c = "lineNumber";
    static final String d = "methodName";
    static final String e = "stackTrace";
    static final String f = "isAdActive";
    static final String g = "activeAdId";
    static final String h = "active_creative_ad_id";
    static final String i = "listOfCachedAds";
    static final String j = "listOfCreativeAdIds";
    static final String k = "adCacheSize";
    /* access modifiers changed from: private */
    public JSONObject p;

    private class a extends a {
        a() {
            this.b = new q();
        }

        /* access modifiers changed from: 0000 */
        public a a(JSONObject jSONObject) {
            ((q) this.b).p = jSONObject;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(Date date) {
            u.a(((q) this.b).p, "timestamp", x.l.format(date));
            return super.a(date);
        }
    }

    q() {
    }

    /* access modifiers changed from: 0000 */
    public q a(JSONObject jSONObject) {
        a aVar = new a();
        aVar.a(jSONObject);
        aVar.a(u.b(jSONObject, "message"));
        try {
            aVar.a(new Date(Long.parseLong(u.b(jSONObject, "timestamp"))));
        } catch (NumberFormatException unused) {
        }
        aVar.a(f952a);
        aVar.a(-1);
        return (q) aVar.a();
    }

    /* access modifiers changed from: 0000 */
    public JSONObject a() {
        return this.p;
    }
}
