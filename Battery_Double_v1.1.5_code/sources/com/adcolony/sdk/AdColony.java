package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

public class AdColony {

    /* renamed from: a reason: collision with root package name */
    static ExecutorService f698a = Executors.newSingleThreadExecutor();

    public static boolean disable() {
        if (!a.e()) {
            return false;
        }
        Context c = a.c();
        if (c != null && (c instanceof b)) {
            ((Activity) c).finish();
        }
        final j a2 = a.a();
        for (final AdColonyInterstitial adColonyInterstitial : a2.l().c().values()) {
            at.a((Runnable) new Runnable() {
                public void run() {
                    AdColonyInterstitialListener listener = adColonyInterstitial.getListener();
                    adColonyInterstitial.a(true);
                    if (listener != null) {
                        listener.onExpiring(adColonyInterstitial);
                    }
                }
            });
        }
        at.a((Runnable) new Runnable() {
            public void run() {
                ArrayList arrayList = new ArrayList();
                Iterator it = a2.q().c().iterator();
                while (it.hasNext()) {
                    arrayList.add((ae) it.next());
                }
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    ae aeVar = (ae) it2.next();
                    a2.a(aeVar.a());
                    if (aeVar instanceof av) {
                        av avVar = (av) aeVar;
                        if (!avVar.m()) {
                            avVar.loadUrl("about:blank");
                            avVar.clearCache(true);
                            avVar.removeAllViews();
                            avVar.a(true);
                        }
                    }
                }
            }
        });
        a.a().a(true);
        return true;
    }

    public static boolean configure(Activity activity, @NonNull String str, @NonNull String... strArr) {
        return a(activity, null, str, strArr);
    }

    public static boolean configure(Activity activity, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        return a(activity, adColonyAppOptions, str, strArr);
    }

    public static boolean configure(Application application, @NonNull String str, @NonNull String... strArr) {
        return configure(application, (AdColonyAppOptions) null, str, strArr);
    }

    public static boolean configure(Application application, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        return a(application, adColonyAppOptions, str, strArr);
    }

    private static boolean a(Context context, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        if (ah.a(0, null)) {
            new a().a("Cannot configure AdColony; configuration mechanism requires 5 ").a("seconds between attempts.").a(w.e);
            return false;
        }
        if (context == null) {
            context = a.c();
        }
        if (context == null) {
            new a().a("Ignoring call to AdColony.configure() as the provided Activity or ").a("Application context is null and we do not currently hold a ").a("reference to either for our use.").a(w.e);
            return false;
        }
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        if (adColonyAppOptions == null) {
            adColonyAppOptions = new AdColonyAppOptions();
        }
        if (a.b() && !u.d(a.a().d().d(), "reconfigurable")) {
            j a2 = a.a();
            if (!a2.d().a().equals(str)) {
                new a().a("Ignoring call to AdColony.configure() as the app id does not ").a("match what was used during the initial configuration.").a(w.e);
                return false;
            } else if (at.a(strArr, a2.d().b())) {
                new a().a("Ignoring call to AdColony.configure() as the same zone ids ").a("were used during the previous configuration.").a(w.e);
                return true;
            }
        }
        adColonyAppOptions.a(str);
        adColonyAppOptions.a(strArr);
        adColonyAppOptions.f();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss:SSS", Locale.US);
        long currentTimeMillis = System.currentTimeMillis();
        String format = simpleDateFormat.format(new Date(currentTimeMillis));
        boolean z = true;
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] != null && !strArr[i].equals("")) {
                z = false;
            }
        }
        if (str.equals("") || z) {
            new a().a("AdColony.configure() called with an empty app or zone id String.").a(w.g);
            return false;
        }
        a.f724a = true;
        if (VERSION.SDK_INT < 14) {
            new a().a("The minimum API level for the AdColony SDK is 14.").a(w.e);
            a.a(context, adColonyAppOptions, true);
        } else {
            a.a(context, adColonyAppOptions, false);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(a.a().o().c());
        sb.append("/adc3/AppInfo");
        String sb2 = sb.toString();
        JSONObject a3 = u.a();
        if (new File(sb2).exists()) {
            a3 = u.c(sb2);
        }
        JSONObject a4 = u.a();
        if (u.b(a3, "appId").equals(str)) {
            u.a(a4, "zoneIds", u.a(u.g(a3, "zoneIds"), strArr, true));
            u.a(a4, "appId", str);
        } else {
            u.a(a4, "zoneIds", u.a(strArr));
            u.a(a4, "appId", str);
        }
        u.h(a4, sb2);
        a a5 = new a().a("Configure: Total Time (ms): ");
        StringBuilder sb3 = new StringBuilder();
        sb3.append("");
        sb3.append(System.currentTimeMillis() - currentTimeMillis);
        a a6 = a5.a(sb3.toString());
        StringBuilder sb4 = new StringBuilder();
        sb4.append(" and started at ");
        sb4.append(format);
        a6.a(sb4.toString()).a(w.f);
        return true;
    }

    public static AdColonyZone getZone(@NonNull String str) {
        if (!a.e()) {
            new a().a("Ignoring call to AdColony.getZone() as AdColony has not yet been ").a("configured.").a(w.e);
            return null;
        }
        HashMap f = a.a().f();
        if (f.containsKey(str)) {
            return (AdColonyZone) f.get(str);
        }
        AdColonyZone adColonyZone = new AdColonyZone(str);
        a.a().f().put(str, adColonyZone);
        return adColonyZone;
    }

    public static boolean notifyIAPComplete(@NonNull String str, @NonNull String str2) {
        return notifyIAPComplete(str, str2, null, Utils.DOUBLE_EPSILON);
    }

    public static boolean notifyIAPComplete(@NonNull String str, @NonNull String str2, String str3, @FloatRange(from = 0.0d) double d) {
        if (!a.e()) {
            new a().a("Ignoring call to notifyIAPComplete as AdColony has not yet been ").a("configured.").a(w.e);
            return false;
        } else if (!at.d(str) || !at.d(str2)) {
            new a().a("Ignoring call to notifyIAPComplete as one of the passed Strings ").a("is greater than ").a(128).a(" characters.").a(w.e);
            return false;
        } else {
            if (str3 != null && str3.length() > 3) {
                new a().a("You are trying to report an IAP event with a currency String ").a("containing more than 3 characters.").a(w.e);
            }
            final double d2 = d;
            final String str4 = str3;
            final String str5 = str;
            final String str6 = str2;
            AnonymousClass5 r2 = new Runnable() {
                public void run() {
                    AdColony.a();
                    JSONObject a2 = u.a();
                    if (d2 >= Utils.DOUBLE_EPSILON) {
                        u.a(a2, "price", d2);
                    }
                    if (str4 != null && str4.length() <= 3) {
                        u.a(a2, "currency_code", str4);
                    }
                    u.a(a2, "product_id", str5);
                    u.a(a2, "transaction_id", str6);
                    new ab("AdColony.on_iap_report", 1, a2).b();
                }
            };
            f698a.execute(r2);
            return true;
        }
    }

    public static boolean requestAdView(@NonNull String str, @NonNull AdColonyAdViewListener adColonyAdViewListener, @NonNull AdColonyAdSize adColonyAdSize) {
        return requestAdView(str, adColonyAdViewListener, adColonyAdSize, null);
    }

    public static boolean requestAdView(@NonNull final String str, @NonNull final AdColonyAdViewListener adColonyAdViewListener, @NonNull final AdColonyAdSize adColonyAdSize, final AdColonyAdOptions adColonyAdOptions) {
        if (!a.e()) {
            new a().a("Ignoring call to requestAdView as AdColony has not yet been").a(" configured.").a(w.e);
            a(adColonyAdViewListener, str);
            return false;
        } else if (adColonyAdSize.getHeight() <= 0 || adColonyAdSize.getWidth() <= 0) {
            new a().a("Ignoring call to requestAdView as you've provided an AdColonyAdSize").a(" object with an invalid width or height.").a(w.e);
            return false;
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("zone_id", str);
            if (ah.a(1, bundle)) {
                a(adColonyAdViewListener, str);
                return false;
            }
            try {
                f698a.execute(new Runnable() {
                    public void run() {
                        j a2 = a.a();
                        if (a2.g() || a2.h()) {
                            AdColony.b();
                            AdColony.a(adColonyAdViewListener, str);
                        }
                        if (!AdColony.a() && a.d()) {
                            AdColony.a(adColonyAdViewListener, str);
                        }
                        if (((AdColonyZone) a2.f().get(str)) == null) {
                            new AdColonyZone(str);
                            new a().a("Zone info for ").a(str).a(" doesn't exist in hashmap").a(w.b);
                        }
                        a2.l().a(str, adColonyAdViewListener, adColonyAdSize, adColonyAdOptions);
                    }
                });
                return true;
            } catch (RejectedExecutionException unused) {
                a(adColonyAdViewListener, str);
                return false;
            }
        }
    }

    public static boolean setAppOptions(@NonNull final AdColonyAppOptions adColonyAppOptions) {
        if (!a.e()) {
            new a().a("Ignoring call to AdColony.setAppOptions() as AdColony has not yet").a(" been configured.").a(w.e);
            return false;
        }
        a.a().b(adColonyAppOptions);
        adColonyAppOptions.f();
        try {
            f698a.execute(new Runnable() {
                public void run() {
                    AdColony.a();
                    JSONObject a2 = u.a();
                    u.a(a2, "options", adColonyAppOptions.d());
                    new ab("Options.set_options", 1, a2).b();
                }
            });
            return true;
        } catch (RejectedExecutionException unused) {
            return false;
        }
    }

    public static AdColonyAppOptions getAppOptions() {
        if (!a.e()) {
            return null;
        }
        return a.a().d();
    }

    public static boolean setRewardListener(@NonNull AdColonyRewardListener adColonyRewardListener) {
        if (!a.e()) {
            new a().a("Ignoring call to AdColony.setRewardListener() as AdColony has not").a(" yet been configured.").a(w.e);
            return false;
        }
        a.a().a(adColonyRewardListener);
        return true;
    }

    public static boolean removeRewardListener() {
        if (!a.e()) {
            new a().a("Ignoring call to AdColony.removeRewardListener() as AdColony has ").a("not yet been configured.").a(w.e);
            return false;
        }
        a.a().a((AdColonyRewardListener) null);
        return true;
    }

    public static String getSDKVersion() {
        if (!a.e()) {
            return "";
        }
        return a.a().m().F();
    }

    public static AdColonyRewardListener getRewardListener() {
        if (!a.e()) {
            return null;
        }
        return a.a().i();
    }

    public static boolean addCustomMessageListener(@NonNull AdColonyCustomMessageListener adColonyCustomMessageListener, final String str) {
        if (!a.e()) {
            new a().a("Ignoring call to AdColony.addCustomMessageListener as AdColony ").a("has not yet been configured.").a(w.e);
            return false;
        } else if (!at.d(str)) {
            new a().a("Ignoring call to AdColony.addCustomMessageListener.").a(w.e);
            return false;
        } else {
            try {
                a.a().A().put(str, adColonyCustomMessageListener);
                f698a.execute(new Runnable() {
                    public void run() {
                        AdColony.a();
                        JSONObject a2 = u.a();
                        u.a(a2, "type", str);
                        new ab("CustomMessage.register", 1, a2).b();
                    }
                });
                return true;
            } catch (RejectedExecutionException unused) {
                return false;
            }
        }
    }

    public static AdColonyCustomMessageListener getCustomMessageListener(@NonNull String str) {
        if (!a.e()) {
            return null;
        }
        return (AdColonyCustomMessageListener) a.a().A().get(str);
    }

    public static boolean removeCustomMessageListener(@NonNull final String str) {
        if (!a.e()) {
            new a().a("Ignoring call to AdColony.removeCustomMessageListener as AdColony").a(" has not yet been configured.").a(w.e);
            return false;
        }
        a.a().A().remove(str);
        f698a.execute(new Runnable() {
            public void run() {
                AdColony.a();
                JSONObject a2 = u.a();
                u.a(a2, "type", str);
                new ab("CustomMessage.unregister", 1, a2).b();
            }
        });
        return true;
    }

    public static boolean clearCustomMessageListeners() {
        if (!a.e()) {
            new a().a("Ignoring call to AdColony.clearCustomMessageListeners as AdColony").a(" has not yet been configured.").a(w.e);
            return false;
        }
        a.a().A().clear();
        f698a.execute(new Runnable() {
            public void run() {
                AdColony.a();
                for (String str : a.a().A().keySet()) {
                    JSONObject a2 = u.a();
                    u.a(a2, "type", str);
                    new ab("CustomMessage.unregister", 1, a2).b();
                }
            }
        });
        return true;
    }

    public static boolean requestInterstitial(@NonNull String str, @NonNull AdColonyInterstitialListener adColonyInterstitialListener) {
        return requestInterstitial(str, adColonyInterstitialListener, null);
    }

    public static boolean requestInterstitial(@NonNull final String str, @NonNull final AdColonyInterstitialListener adColonyInterstitialListener, final AdColonyAdOptions adColonyAdOptions) {
        if (!a.e()) {
            new a().a("Ignoring call to AdColony.requestInterstitial as AdColony has not").a(" yet been configured.").a(w.e);
            adColonyInterstitialListener.onRequestNotFilled(new AdColonyZone(str));
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("zone_id", str);
        if (ah.a(1, bundle)) {
            AdColonyZone adColonyZone = (AdColonyZone) a.a().f().get(str);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(str);
                a a2 = new a().a("Zone info for ");
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(" doesn't exist in hashmap");
                a2.a(sb.toString()).a(w.b);
            }
            adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
            return false;
        }
        try {
            f698a.execute(new Runnable() {
                public void run() {
                    j a2 = a.a();
                    if (a2.g() || a2.h()) {
                        AdColony.b();
                        AdColony.a(adColonyInterstitialListener, str);
                    } else if (AdColony.a() || !a.d()) {
                        final AdColonyZone adColonyZone = (AdColonyZone) a2.f().get(str);
                        if (adColonyZone == null) {
                            adColonyZone = new AdColonyZone(str);
                            a a3 = new a().a("Zone info for ");
                            StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append(" doesn't exist in hashmap");
                            a3.a(sb.toString()).a(w.b);
                        }
                        if (adColonyZone.getZoneType() == 2 || adColonyZone.getZoneType() == 1) {
                            at.a((Runnable) new Runnable() {
                                public void run() {
                                    adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
                                }
                            });
                        } else {
                            a2.l().a(str, adColonyInterstitialListener, adColonyAdOptions);
                        }
                    } else {
                        AdColony.a(adColonyInterstitialListener, str);
                    }
                }
            });
            return true;
        } catch (RejectedExecutionException unused) {
            a(adColonyInterstitialListener, str);
            return false;
        }
    }

    static boolean a() {
        a aVar = new a(15.0d);
        j a2 = a.a();
        while (!a2.B() && !aVar.b()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException unused) {
            }
        }
        return a2.B();
    }

    static boolean a(final AdColonyInterstitialListener adColonyInterstitialListener, final String str) {
        if (adColonyInterstitialListener != null && a.d()) {
            at.a((Runnable) new Runnable() {
                public void run() {
                    AdColonyZone adColonyZone = (AdColonyZone) a.a().f().get(str);
                    if (adColonyZone == null) {
                        adColonyZone = new AdColonyZone(str);
                    }
                    adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
                }
            });
        }
        return false;
    }

    static boolean a(final AdColonyAdViewListener adColonyAdViewListener, final String str) {
        if (adColonyAdViewListener != null && a.d()) {
            at.a((Runnable) new Runnable() {
                public void run() {
                    AdColonyZone adColonyZone;
                    if (!a.b()) {
                        adColonyZone = null;
                    } else {
                        adColonyZone = (AdColonyZone) a.a().f().get(str);
                    }
                    if (adColonyZone == null) {
                        adColonyZone = new AdColonyZone(str);
                    }
                    adColonyAdViewListener.onRequestNotFilled(adColonyZone);
                }
            });
        }
        return false;
    }

    static void a(Context context, AdColonyAppOptions adColonyAppOptions) {
        if (adColonyAppOptions != null && context != null) {
            String b = at.b(context);
            String b2 = at.b();
            int c = at.c();
            String i = a.a().m().i();
            String str = "none";
            if (a.a().p().a()) {
                str = "wifi";
            } else if (a.a().p().b()) {
                str = TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE;
            }
            HashMap hashMap = new HashMap();
            hashMap.put("sessionId", "unknown");
            hashMap.put("advertiserId", "unknown");
            StringBuilder sb = new StringBuilder();
            sb.append(Locale.getDefault().getDisplayLanguage());
            sb.append(" (");
            sb.append(Locale.getDefault().getDisplayCountry());
            sb.append(")");
            hashMap.put("countryLocale", sb.toString());
            hashMap.put("countryLocalShort", a.a().m().w());
            hashMap.put("manufacturer", a.a().m().z());
            hashMap.put("model", a.a().m().A());
            hashMap.put("osVersion", a.a().m().B());
            hashMap.put("carrierName", i);
            hashMap.put("networkType", str);
            hashMap.put(TapjoyConstants.TJC_PLATFORM, "android");
            hashMap.put("appName", b);
            hashMap.put(RequestParameters.APPLICATION_VERSION_NAME, b2);
            hashMap.put("appBuildNumber", Integer.valueOf(c));
            StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(adColonyAppOptions.a());
            hashMap.put("appId", sb2.toString());
            hashMap.put("apiLevel", Integer.valueOf(VERSION.SDK_INT));
            hashMap.put(GeneralPropertiesWorker.SDK_VERSION, a.a().m().F());
            hashMap.put("controllerVersion", "unknown");
            hashMap.put("zoneIds", adColonyAppOptions.c());
            JSONObject mediationInfo = adColonyAppOptions.getMediationInfo();
            JSONObject pluginInfo = adColonyAppOptions.getPluginInfo();
            if (!u.b(mediationInfo, "mediation_network").equals("")) {
                hashMap.put("mediationNetwork", u.b(mediationInfo, "mediation_network"));
                hashMap.put("mediationNetworkVersion", u.b(mediationInfo, "mediation_network_version"));
            }
            if (!u.b(pluginInfo, TapjoyConstants.TJC_PLUGIN).equals("")) {
                hashMap.put(TapjoyConstants.TJC_PLUGIN, u.b(pluginInfo, TapjoyConstants.TJC_PLUGIN));
                hashMap.put("pluginVersion", u.b(pluginInfo, "plugin_version"));
            }
            y.a(hashMap);
        }
    }

    static void b() {
        new a().a("The AdColony API is not available while AdColony is disabled.").a(w.g);
    }
}
