package com.adcolony.sdk;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.util.Log;
import com.tapjoy.TapjoyConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyAppOptions {
    public static final String ADMARVEL = "AdMarvel";
    public static final String ADMOB = "AdMob";
    public static final String ADOBEAIR = "Adobe AIR";
    public static final String AERSERVE = "AerServe";
    public static final int ALL = 2;
    public static final String APPODEAL = "Appodeal";
    public static final String COCOS2DX = "Cocos2d-x";
    public static final String CORONA = "Corona";
    public static final String FUSEPOWERED = "Fuse Powered";
    public static final String FYBER = "Fyber";
    public static final String IRONSOURCE = "ironSource";
    public static final int LANDSCAPE = 1;
    public static final String MOPUB = "MoPub";
    public static final int PORTRAIT = 0;
    @Deprecated
    public static final int SENSOR = 2;
    public static final String UNITY = "Unity";

    /* renamed from: a reason: collision with root package name */
    String f716a = "";
    String[] b;
    JSONArray c = u.b();
    JSONObject d = u.a();
    AdColonyUserMetadata e;

    public AdColonyAppOptions() {
        setOriginStore("google");
        if (a.b()) {
            j a2 = a.a();
            if (a2.e()) {
                a(a2.d().f716a);
                a(a2.d().b);
            }
        }
    }

    public AdColonyAppOptions setGDPRRequired(boolean z) {
        setOption("gdpr_required", z);
        return this;
    }

    public boolean getGDPRRequired() {
        return u.d(this.d, "gdpr_required");
    }

    public AdColonyAppOptions setGDPRConsentString(@NonNull String str) {
        u.a(this.d, "consent_string", str);
        return this;
    }

    public String getGDPRConsentString() {
        return u.b(this.d, "consent_string");
    }

    public AdColonyAppOptions setAppVersion(@NonNull String str) {
        if (at.d(str)) {
            setOption(TapjoyConstants.TJC_APP_VERSION_NAME, str);
        }
        return this;
    }

    public String getAppVersion() {
        return u.b(this.d, TapjoyConstants.TJC_APP_VERSION_NAME);
    }

    public AdColonyAppOptions setUserID(@NonNull String str) {
        if (at.d(str)) {
            setOption("user_id", str);
        }
        return this;
    }

    public String getUserID() {
        return u.b(this.d, "user_id");
    }

    public AdColonyAppOptions setOption(@NonNull String str, boolean z) {
        if (at.d(str)) {
            u.b(this.d, str, z);
        }
        return this;
    }

    public Object getOption(@NonNull String str) {
        return u.a(this.d, str);
    }

    public AdColonyAppOptions setOption(@NonNull String str, double d2) {
        if (at.d(str)) {
            u.a(this.d, str, d2);
        }
        return this;
    }

    public AdColonyAppOptions setOption(@NonNull String str, @NonNull String str2) {
        if (str != null && at.d(str) && at.d(str2)) {
            u.a(this.d, str, str2);
        }
        return this;
    }

    public AdColonyAppOptions setOriginStore(@NonNull String str) {
        if (at.d(str)) {
            setOption("origin_store", str);
        }
        return this;
    }

    public String getOriginStore() {
        return u.b(this.d, "origin_store");
    }

    public AdColonyAppOptions setRequestedAdOrientation(@IntRange(from = 0, to = 2) int i) {
        setOption("orientation", (double) i);
        return this;
    }

    public int getRequestedAdOrientation() {
        return u.a(this.d, "orientation", -1);
    }

    public AdColonyAppOptions setAppOrientation(@IntRange(from = 0, to = 2) int i) {
        setOption("app_orientation", (double) i);
        return this;
    }

    public int getAppOrientation() {
        return u.a(this.d, "app_orientation", -1);
    }

    public AdColonyAppOptions setUserMetadata(@NonNull AdColonyUserMetadata adColonyUserMetadata) {
        this.e = adColonyUserMetadata;
        u.a(this.d, "user_metadata", adColonyUserMetadata.c);
        return this;
    }

    public AdColonyAppOptions setTestModeEnabled(boolean z) {
        u.b(this.d, "test_mode", z);
        return this;
    }

    public boolean getTestModeEnabled() {
        return u.d(this.d, "test_mode");
    }

    public AdColonyAppOptions setMultiWindowEnabled(boolean z) {
        u.b(this.d, "multi_window_enabled", z);
        return this;
    }

    public boolean getMultiWindowEnabled() {
        return u.d(this.d, "multi_window_enabled");
    }

    public AdColonyUserMetadata getUserMetadata() {
        return this.e;
    }

    public AdColonyAppOptions setMediationNetwork(@NonNull String str, @NonNull String str2) {
        if (at.d(str) && at.d(str2)) {
            u.a(this.d, "mediation_network", str);
            u.a(this.d, "mediation_network_version", str2);
        }
        return this;
    }

    public JSONObject getMediationInfo() {
        JSONObject a2 = u.a();
        u.a(a2, "name", u.b(this.d, "mediation_network"));
        u.a(a2, "version", u.b(this.d, "mediation_network_version"));
        return a2;
    }

    public AdColonyAppOptions setPlugin(@NonNull String str, @NonNull String str2) {
        if (at.d(str) && at.d(str2)) {
            u.a(this.d, TapjoyConstants.TJC_PLUGIN, str);
            u.a(this.d, "plugin_version", str2);
        }
        return this;
    }

    public JSONObject getPluginInfo() {
        JSONObject a2 = u.a();
        u.a(a2, "name", u.b(this.d, TapjoyConstants.TJC_PLUGIN));
        u.a(a2, "version", u.b(this.d, "plugin_version"));
        return a2;
    }

    public AdColonyAppOptions setKeepScreenOn(boolean z) {
        u.b(this.d, "keep_screen_on", z);
        return this;
    }

    public boolean getKeepScreenOn() {
        return u.d(this.d, "keep_screen_on");
    }

    public static AdColonyAppOptions getMoPubAppOptions(@NonNull String str) {
        String str2 = "AdColonyMoPub";
        AdColonyAppOptions mediationNetwork = new AdColonyAppOptions().setMediationNetwork(MOPUB, "1.0");
        if (str == null || str.isEmpty()) {
            return mediationNetwork;
        }
        String[] split = str.split(",");
        int length = split.length;
        int i = 0;
        while (i < length) {
            String[] split2 = split[i].split(":");
            if (split2.length == 2) {
                String str3 = split2[0];
                char c2 = 65535;
                int hashCode = str3.hashCode();
                if (hashCode != 109770977) {
                    if (hashCode == 351608024 && str3.equals("version")) {
                        c2 = 1;
                    }
                } else if (str3.equals("store")) {
                    c2 = 0;
                }
                switch (c2) {
                    case 0:
                        mediationNetwork.setOriginStore(split2[1]);
                        break;
                    case 1:
                        mediationNetwork.setAppVersion(split2[1]);
                        break;
                    default:
                        Log.e(str2, "AdColony client options in wrong format - please check your MoPub dashboard");
                        return mediationNetwork;
                }
                i++;
            } else {
                Log.e(str2, "AdColony client options not recognized - please check your MoPub dashboard");
                return null;
            }
        }
        return mediationNetwork;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAppOptions a(String str) {
        if (str == null) {
            return this;
        }
        this.f716a = str;
        u.a(this.d, "app_id", str);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAppOptions a(String... strArr) {
        if (strArr == null) {
            return this;
        }
        this.b = strArr;
        this.c = u.b();
        for (String a2 : strArr) {
            u.a(this.c, a2);
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.f716a;
    }

    /* access modifiers changed from: 0000 */
    public String[] b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public JSONArray c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject d() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        setOption("bundle_id", a.a().m().H());
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        if (u.i(this.d, "use_forced_controller")) {
            av.f795a = u.d(this.d, "use_forced_controller");
        }
        if (u.i(this.d, "use_staging_launch_server") && u.d(this.d, "use_staging_launch_server")) {
            j.e = "https://adc3-launch-staging.adcolony.com/v4/launch";
        }
    }
}
