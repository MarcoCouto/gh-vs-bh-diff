package com.adcolony.sdk;

import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.tapjoy.TapjoyConstants;
import io.realm.BuildConfig;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ai {

    /* renamed from: a reason: collision with root package name */
    static final String f737a = "adcolony_android";
    static final String b = "adcolony_fatal_reports";
    v c;
    ScheduledExecutorService d;
    List<x> e = new ArrayList();
    List<x> f = new ArrayList();
    HashMap<String, Object> g;
    private t h = new t(f737a, BuildConfig.VERSION_NAME, "Production");
    private t i = new t(b, BuildConfig.VERSION_NAME, "Production");

    ai(v vVar, ScheduledExecutorService scheduledExecutorService, HashMap<String, Object> hashMap) {
        this.c = vVar;
        this.d = scheduledExecutorService;
        this.g = hashMap;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(String str) {
        this.g.put("controllerVersion", str);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void b(String str) {
        this.g.put("sessionId", str);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(long j, TimeUnit timeUnit) {
        try {
            if (!this.d.isShutdown() && !this.d.isTerminated()) {
                this.d.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                        ai.this.b();
                    }
                }, j, j, timeUnit);
            }
        } catch (RuntimeException unused) {
            Log.e("ADCLogError", "Internal error when submitting remote log to executor service");
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:9|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r4.d.shutdownNow();
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0040 */
    public synchronized void a() {
        this.d.shutdown();
        if (!this.d.awaitTermination(1, TimeUnit.SECONDS)) {
            this.d.shutdownNow();
            if (!this.d.awaitTermination(1, TimeUnit.SECONDS)) {
                PrintStream printStream = System.err;
                StringBuilder sb = new StringBuilder();
                sb.append(getClass().getSimpleName());
                sb.append(": ScheduledExecutorService did not terminate");
                printStream.println(sb.toString());
            }
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0039 */
    public synchronized void b() {
        synchronized (this) {
            try {
                if (this.e.size() > 0) {
                    this.c.a(a(this.h, this.e));
                    this.e.clear();
                }
                if (this.f.size() > 0) {
                    this.c.a(a(this.i, this.f));
                    this.f.clear();
                }
            } catch (IOException unused) {
                this.e.clear();
            } catch (JSONException ) {
                this.e.clear();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void c(String str) {
        a(new a().a(3).a(this.h).a(str).a());
    }

    /* access modifiers changed from: 0000 */
    public synchronized void d(String str) {
        a(new a().a(2).a(this.h).a(str).a());
    }

    /* access modifiers changed from: 0000 */
    public synchronized void e(String str) {
        a(new a().a(1).a(this.h).a(str).a());
    }

    /* access modifiers changed from: 0000 */
    public synchronized void f(String str) {
        a(new a().a(0).a(this.h).a(str).a());
    }

    /* access modifiers changed from: 0000 */
    public void a(q qVar) {
        qVar.a(this.i);
        qVar.a(-1);
        b((x) qVar);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(final x xVar) {
        try {
            if (!this.d.isShutdown() && !this.d.isTerminated()) {
                this.d.submit(new Runnable() {
                    public void run() {
                        ai.this.e.add(xVar);
                    }
                });
            }
        } catch (RejectedExecutionException unused) {
            Log.e("ADCLogError", "Internal error when submitting remote log to executor service");
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void b(x xVar) {
        this.f.add(xVar);
    }

    /* access modifiers changed from: 0000 */
    public String a(t tVar, List<x> list) throws IOException, JSONException {
        String c2 = a.a().m().c();
        String str = this.g.get("advertiserId") != null ? (String) this.g.get("advertiserId") : "unknown";
        if (c2 != null && c2.length() > 0 && !c2.equals(str)) {
            this.g.put("advertiserId", c2);
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(Param.INDEX, tVar.b());
        jSONObject.put("environment", tVar.d());
        jSONObject.put("version", tVar.c());
        JSONArray jSONArray = new JSONArray();
        for (x c3 : list) {
            jSONArray.put(c(c3));
        }
        jSONObject.put("logs", jSONArray);
        return jSONObject.toString();
    }

    private synchronized JSONObject c(x xVar) throws JSONException {
        JSONObject jSONObject;
        jSONObject = new JSONObject(this.g);
        jSONObject.put("environment", xVar.f().d());
        jSONObject.put("level", xVar.b());
        jSONObject.put("message", xVar.d());
        jSONObject.put("clientTimestamp", xVar.e());
        JSONObject mediationInfo = a.a().d().getMediationInfo();
        JSONObject pluginInfo = a.a().d().getPluginInfo();
        double t = a.a().m().t();
        jSONObject.put("mediation_network", u.b(mediationInfo, "name"));
        jSONObject.put("mediation_network_version", u.b(mediationInfo, "version"));
        jSONObject.put(TapjoyConstants.TJC_PLUGIN, u.b(pluginInfo, "name"));
        jSONObject.put("plugin_version", u.b(pluginInfo, "version"));
        jSONObject.put("batteryInfo", t);
        if (xVar instanceof q) {
            jSONObject = u.a(jSONObject, ((q) xVar).a());
            jSONObject.put(TapjoyConstants.TJC_PLATFORM, "android");
        }
        return jSONObject;
    }
}
