package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Environment;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class aq {
    aq() {
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        a.a("System.open_store", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.b(abVar);
            }
        });
        a.a("System.save_screenshot", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.c(abVar);
            }
        });
        a.a("System.telephone", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.d(abVar);
            }
        });
        a.a("System.sms", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.e(abVar);
            }
        });
        a.a("System.vibrate", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.f(abVar);
            }
        });
        a.a("System.open_browser", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.g(abVar);
            }
        });
        a.a("System.mail", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.h(abVar);
            }
        });
        a.a("System.launch_app", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.i(abVar);
            }
        });
        a.a("System.create_calendar_event", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.j(abVar);
            }
        });
        a.a("System.check_app_presence", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.k(abVar);
            }
        });
        a.a("System.check_social_presence", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.l(abVar);
            }
        });
        a.a("System.social_post", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.m(abVar);
            }
        });
        a.a("System.make_in_app_purchase", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.q(abVar);
            }
        });
        a.a("System.close", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.p(abVar);
            }
        });
        a.a("System.expand", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.a(abVar);
            }
        });
        a.a("System.use_custom_close", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.o(abVar);
            }
        });
        a.a("System.set_orientation_properties", (ad) new ad() {
            public void a(ab abVar) {
                aq.this.n(abVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean n(ab abVar) {
        JSONObject c = abVar.c();
        String b = u.b(c, "ad_session_id");
        int c2 = u.c(c, "orientation");
        d l = a.a().l();
        AdColonyAdView adColonyAdView = (AdColonyAdView) l.e().get(b);
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) l.c().get(b);
        Context c3 = a.c();
        if (adColonyAdView != null) {
            adColonyAdView.setOrientation(c2);
        } else if (adColonyInterstitial != null) {
            adColonyInterstitial.b(c2);
        }
        if (c3 instanceof b) {
            ((b) c3).a(adColonyAdView == null ? adColonyInterstitial.e() : adColonyAdView.getOrientation());
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ab abVar) {
        JSONObject c = abVar.c();
        Context c2 = a.c();
        if (c2 == null || !a.b()) {
            return false;
        }
        String b = u.b(c, "ad_session_id");
        j a2 = a.a();
        AdColonyAdView adColonyAdView = (AdColonyAdView) a2.l().e().get(b);
        if (adColonyAdView == null || ((!adColonyAdView.getTrustedDemandSource() && !adColonyAdView.getUserInteraction()) || a2.u() == adColonyAdView)) {
            return false;
        }
        adColonyAdView.setExpandMessage(abVar);
        adColonyAdView.setExpandedWidth(u.c(c, "width"));
        adColonyAdView.setExpandedHeight(u.c(c, "height"));
        adColonyAdView.setOrientation(u.a(c, "orientation", -1));
        adColonyAdView.setNoCloseButton(u.d(c, "use_custom_close"));
        a2.a(adColonyAdView);
        a2.a(adColonyAdView.getContainer());
        Intent intent = new Intent(c2, AdColonyAdViewActivity.class);
        if (c2 instanceof Application) {
            intent.addFlags(268435456);
        }
        c(b);
        b(b);
        c2.startActivity(intent);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean o(ab abVar) {
        AdColonyAdView adColonyAdView = (AdColonyAdView) a.a().l().e().get(u.b(abVar.c(), "ad_session_id"));
        if (adColonyAdView == null) {
            return false;
        }
        adColonyAdView.setNoCloseButton(u.d(abVar.c(), "use_custom_close"));
        return true;
    }

    /* access modifiers changed from: private */
    public boolean p(ab abVar) {
        String b = u.b(abVar.c(), "ad_session_id");
        Activity activity = a.c() instanceof Activity ? (Activity) a.c() : null;
        boolean z = activity instanceof AdColonyAdViewActivity;
        if (!(activity instanceof b)) {
            return false;
        }
        if (z) {
            ((AdColonyAdViewActivity) activity).b();
        } else {
            JSONObject a2 = u.a();
            u.a(a2, "id", b);
            new ab("AdSession.on_request_close", ((b) activity).f, a2).b();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean q(ab abVar) {
        JSONObject c = abVar.c();
        d l = a.a().l();
        String b = u.b(c, "ad_session_id");
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) l.c().get(b);
        AdColonyAdView adColonyAdView = (AdColonyAdView) l.e().get(b);
        if ((adColonyInterstitial == null || adColonyInterstitial.getListener() == null || adColonyInterstitial.d() == null) && (adColonyAdView == null || adColonyAdView.getListener() == null)) {
            return false;
        }
        if (adColonyAdView == null) {
            new ab("AdUnit.make_in_app_purchase", adColonyInterstitial.d().c()).b();
        }
        b(b);
        c(b);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean b(ab abVar) {
        JSONObject a2 = u.a();
        JSONObject c = abVar.c();
        String b = u.b(c, "product_id");
        String b2 = u.b(c, "ad_session_id");
        if (b.equals("")) {
            b = u.b(c, "handle");
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(b));
        d(b);
        if (at.a(intent)) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            a(b2);
            b(b2);
            c(b2);
            return true;
        }
        at.a("Unable to open.", 0);
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|9|10|11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        com.adcolony.sdk.at.a("Error saving screenshot.", 0);
        com.adcolony.sdk.u.b(r2, "success", false);
        r11.a(r2).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00e1, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e2, code lost:
        com.adcolony.sdk.at.a("Error saving screenshot.", 0);
        com.adcolony.sdk.u.b(r2, "success", false);
        r11.a(r2).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00f3, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x00ab */
    public boolean c(final ab abVar) {
        Context c = a.c();
        if (c == null || !(c instanceof Activity)) {
            return false;
        }
        try {
            if (ActivityCompat.checkSelfPermission(c, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
                b(u.b(abVar.c(), "ad_session_id"));
                final JSONObject a2 = u.a();
                StringBuilder sb = new StringBuilder();
                sb.append(Environment.getExternalStorageDirectory().toString());
                sb.append("/Pictures/AdColony_Screenshots/AdColony_Screenshot_");
                sb.append(System.currentTimeMillis());
                sb.append(".jpg");
                String sb2 = sb.toString();
                View rootView = ((Activity) c).getWindow().getDecorView().getRootView();
                rootView.setDrawingCacheEnabled(true);
                Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                rootView.setDrawingCacheEnabled(false);
                StringBuilder sb3 = new StringBuilder();
                sb3.append(Environment.getExternalStorageDirectory().getPath());
                sb3.append("/Pictures");
                File file = new File(sb3.toString());
                StringBuilder sb4 = new StringBuilder();
                sb4.append(Environment.getExternalStorageDirectory().getPath());
                sb4.append("/Pictures/AdColony_Screenshots");
                File file2 = new File(sb4.toString());
                file.mkdirs();
                file2.mkdirs();
                FileOutputStream fileOutputStream = new FileOutputStream(new File(sb2));
                createBitmap.compress(CompressFormat.JPEG, 90, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
                MediaScannerConnection.scanFile(c, new String[]{sb2}, null, new OnScanCompletedListener() {
                    public void onScanCompleted(String str, Uri uri) {
                        at.a("Screenshot saved to Gallery!", 0);
                        u.b(a2, "success", true);
                        abVar.a(a2).b();
                    }
                });
                return true;
            }
            at.a("Error saving screenshot.", 0);
            JSONObject c2 = abVar.c();
            u.b(c2, "success", false);
            abVar.a(c2).b();
            return false;
        } catch (NoClassDefFoundError unused) {
            at.a("Error saving screenshot.", 0);
            JSONObject c3 = abVar.c();
            u.b(c3, "success", false);
            abVar.a(c3).b();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean d(ab abVar) {
        JSONObject a2 = u.a();
        JSONObject c = abVar.c();
        Intent intent = new Intent("android.intent.action.DIAL");
        StringBuilder sb = new StringBuilder();
        sb.append("tel:");
        sb.append(u.b(c, "phone_number"));
        Intent data = intent.setData(Uri.parse(sb.toString()));
        String b = u.b(c, "ad_session_id");
        if (at.a(data)) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        at.a("Failed to dial number.", 0);
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean e(ab abVar) {
        JSONObject c = abVar.c();
        JSONObject a2 = u.a();
        String b = u.b(c, "ad_session_id");
        JSONArray g = u.g(c, "recipients");
        String str = "";
        for (int i = 0; i < g.length(); i++) {
            if (i != 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(";");
                str = sb.toString();
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(u.c(g, i));
            str = sb2.toString();
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("smsto:");
        sb3.append(str);
        if (at.a(new Intent("android.intent.action.VIEW", Uri.parse(sb3.toString())).putExtra("sms_body", u.b(c, TtmlNode.TAG_BODY)))) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        at.a("Failed to create sms.", 0);
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean f(ab abVar) {
        Context c = a.c();
        if (c == null) {
            return false;
        }
        int a2 = u.a(abVar.c(), "length_ms", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
        JSONObject a3 = u.a();
        JSONArray d = at.d(c);
        boolean z = false;
        for (int i = 0; i < d.length(); i++) {
            if (u.c(d, i).equals("android.permission.VIBRATE")) {
                z = true;
            }
        }
        if (!z) {
            new a().a("No vibrate permission detected.").a(w.e);
            u.b(a3, "success", false);
            abVar.a(a3).b();
            return false;
        }
        try {
            ((Vibrator) c.getSystemService("vibrator")).vibrate((long) a2);
            u.b(a3, "success", false);
            abVar.a(a3).b();
            return true;
        } catch (Exception unused) {
            new a().a("Vibrate command failed.").a(w.e);
            u.b(a3, "success", false);
            abVar.a(a3).b();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean g(ab abVar) {
        JSONObject a2 = u.a();
        JSONObject c = abVar.c();
        String b = u.b(c, "url");
        String b2 = u.b(c, "ad_session_id");
        AdColonyAdView adColonyAdView = (AdColonyAdView) a.a().l().e().get(b2);
        if (adColonyAdView != null && !adColonyAdView.getTrustedDemandSource() && !adColonyAdView.getUserInteraction()) {
            return false;
        }
        if (b.startsWith("browser")) {
            b = b.replaceFirst("browser", "http");
        }
        if (b.startsWith("safari")) {
            b = b.replaceFirst("safari", "http");
        }
        d(b);
        if (at.a(new Intent("android.intent.action.VIEW", Uri.parse(b)))) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            a(b2);
            b(b2);
            c(b2);
            return true;
        }
        at.a("Failed to launch browser.", 0);
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean h(ab abVar) {
        JSONObject a2 = u.a();
        JSONObject c = abVar.c();
        JSONArray g = u.g(c, "recipients");
        boolean d = u.d(c, String.HTML);
        String b = u.b(c, "subject");
        String b2 = u.b(c, TtmlNode.TAG_BODY);
        String b3 = u.b(c, "ad_session_id");
        String[] strArr = new String[g.length()];
        for (int i = 0; i < g.length(); i++) {
            strArr[i] = u.c(g, i);
        }
        Intent intent = new Intent("android.intent.action.SEND");
        if (!d) {
            intent.setType("plain/text");
        }
        intent.putExtra("android.intent.extra.SUBJECT", b).putExtra("android.intent.extra.TEXT", b2).putExtra("android.intent.extra.EMAIL", strArr);
        if (at.a(intent)) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            a(b3);
            b(b3);
            c(b3);
            return true;
        }
        at.a("Failed to send email.", 0);
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean i(ab abVar) {
        JSONObject a2 = u.a();
        JSONObject c = abVar.c();
        String b = u.b(c, "ad_session_id");
        if (u.d(c, CampaignEx.JSON_KEY_DEEP_LINK_URL)) {
            return b(abVar);
        }
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        if (at.a(c2.getPackageManager().getLaunchIntentForPackage(u.b(c, "handle")))) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        at.a("Failed to launch external application.", 0);
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean j(ab abVar) {
        Intent intent;
        ab abVar2 = abVar;
        JSONObject a2 = u.a();
        JSONObject c = abVar.c();
        String str = "";
        String str2 = "";
        String b = u.b(c, "ad_session_id");
        JSONObject f = u.f(c, "params");
        JSONObject f2 = u.f(f, "recurrence");
        JSONArray b2 = u.b();
        JSONArray b3 = u.b();
        JSONArray b4 = u.b();
        String b5 = u.b(f, "description");
        u.b(f, "location");
        String b6 = u.b(f, "start");
        String b7 = u.b(f, TtmlNode.END);
        String b8 = u.b(f, "summary");
        if (f2 != null && f2.length() > 0) {
            str2 = u.b(f2, "expires");
            str = u.b(f2, "frequency").toUpperCase(Locale.getDefault());
            b2 = u.g(f2, "daysInWeek");
            b3 = u.g(f2, "daysInMonth");
            b4 = u.g(f2, "daysInYear");
        }
        if (b8.equals("")) {
            b8 = b5;
        }
        Date h = at.h(b6);
        Date h2 = at.h(b7);
        Date h3 = at.h(str2);
        if (h == null || h2 == null) {
            ab abVar3 = abVar2;
            at.a("Unable to create Calendar Event", 0);
            u.b(a2, "success", false);
            abVar3.a(a2).b();
            return false;
        }
        long time = h.getTime();
        long time2 = h2.getTime();
        long j = 0;
        long time3 = h3 != null ? (h3.getTime() - h.getTime()) / 1000 : 0;
        if (str.equals("DAILY")) {
            j = (time3 / 86400) + 1;
        } else if (str.equals("WEEKLY")) {
            j = (time3 / 604800) + 1;
        } else if (str.equals("MONTHLY")) {
            j = (time3 / 2629800) + 1;
        } else if (str.equals("YEARLY")) {
            j = (time3 / 31557600) + 1;
        }
        long j2 = j;
        if (f2 == null || f2.length() <= 0) {
            intent = new Intent("android.intent.action.EDIT").setType("vnd.android.cursor.item/event").putExtra("title", b8).putExtra("description", b5).putExtra("beginTime", time).putExtra("endTime", time2);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("FREQ=");
            sb.append(str);
            sb.append(";COUNT=");
            sb.append(j2);
            String sb2 = sb.toString();
            try {
                if (b2.length() != 0) {
                    String a3 = at.a(b2);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(sb2);
                    sb3.append(";BYDAY=");
                    sb3.append(a3);
                    sb2 = sb3.toString();
                }
                if (b3.length() != 0) {
                    String b9 = at.b(b3);
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(sb2);
                    sb4.append(";BYMONTHDAY=");
                    sb4.append(b9);
                    sb2 = sb4.toString();
                }
                if (b4.length() != 0) {
                    String b10 = at.b(b4);
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(sb2);
                    sb5.append(";BYYEARDAY=");
                    sb5.append(b10);
                    sb2 = sb5.toString();
                }
            } catch (JSONException unused) {
            }
            intent = new Intent("android.intent.action.EDIT").setType("vnd.android.cursor.item/event").putExtra("title", b8).putExtra("description", b5).putExtra("beginTime", time).putExtra("endTime", time2).putExtra("rrule", sb2);
        }
        if (at.a(intent)) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        ab abVar4 = abVar;
        at.a("Unable to create Calendar Event.", 0);
        u.b(a2, "success", false);
        abVar4.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean k(ab abVar) {
        JSONObject a2 = u.a();
        String b = u.b(abVar.c(), "name");
        boolean a3 = at.a(b);
        u.b(a2, "success", true);
        u.b(a2, IronSourceConstants.EVENTS_RESULT, a3);
        u.a(a2, "name", b);
        u.a(a2, NotificationCompat.CATEGORY_SERVICE, b);
        abVar.a(a2).b();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean l(ab abVar) {
        return k(abVar);
    }

    /* access modifiers changed from: 0000 */
    public boolean m(ab abVar) {
        JSONObject a2 = u.a();
        JSONObject c = abVar.c();
        StringBuilder sb = new StringBuilder();
        sb.append(u.b(c, MimeTypes.BASE_TYPE_TEXT));
        sb.append(" ");
        sb.append(u.b(c, "url"));
        Intent putExtra = new Intent("android.intent.action.SEND").setType(WebRequest.CONTENT_TYPE_PLAIN_TEXT).putExtra("android.intent.extra.TEXT", sb.toString());
        String b = u.b(c, "ad_session_id");
        if (at.a(putExtra, true)) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        at.a("Unable to create social post.", 0);
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        d l = a.a().l();
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) l.c().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.getListener() == null) {
            AdColonyAdView adColonyAdView = (AdColonyAdView) l.e().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (!(adColonyAdView == null || listener == null)) {
                listener.onLeftApplication(adColonyAdView);
            }
            return;
        }
        adColonyInterstitial.getListener().onLeftApplication(adColonyInterstitial);
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        d l = a.a().l();
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) l.c().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.getListener() == null) {
            AdColonyAdView adColonyAdView = (AdColonyAdView) l.e().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (!(adColonyAdView == null || listener == null)) {
                listener.onClicked(adColonyAdView);
            }
            return;
        }
        adColonyInterstitial.getListener().onClicked(adColonyInterstitial);
    }

    private boolean c(@NonNull String str) {
        if (((AdColonyAdView) a.a().l().e().get(str)) == null) {
            return false;
        }
        JSONObject a2 = u.a();
        u.a(a2, "ad_session_id", str);
        new ab("MRAID.on_event", 1, a2).b();
        return true;
    }

    private void d(final String str) {
        at.b.execute(new Runnable() {
            public void run() {
                JSONObject a2 = u.a();
                u.a(a2, "type", "open_hook");
                u.a(a2, "message", str);
                new ab("CustomMessage.controller_send", 0, a2).b();
            }
        });
    }
}
