package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.exoplayer2.util.MimeTypes;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

@SuppressLint({"AppCompatCustomView"})
class ar extends Button {
    private c A;
    private ab B;

    /* renamed from: a reason: collision with root package name */
    private final int f769a = 0;
    private final int b = 1;
    private final int c = 2;
    private final int d = 3;
    private final int e = 1;
    private final int f = 2;
    private final int g = 3;
    private final int h = 0;
    private final int i = 1;
    private final int j = 2;
    private final int k = 1;
    private final int l = 2;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    private String z;

    /* access modifiers changed from: 0000 */
    public int a(boolean z2, int i2) {
        switch (i2) {
            case 0:
                return z2 ? 1 : 16;
            case 1:
                if (z2) {
                    return GravityCompat.START;
                }
                return 48;
            case 2:
                if (z2) {
                    return GravityCompat.END;
                }
                return 80;
            default:
                return 17;
        }
    }

    private ar(Context context) {
        super(context);
    }

    ar(Context context, ab abVar, int i2, c cVar) {
        super(context);
        this.m = i2;
        this.B = abVar;
        this.A = cVar;
    }

    ar(Context context, int i2, ab abVar, int i3, c cVar) {
        super(context, null, i2);
        this.m = i3;
        this.B = abVar;
        this.A = cVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(ab abVar) {
        JSONObject c2 = abVar.c();
        this.u = u.c(c2, AvidJSONUtil.KEY_X);
        this.v = u.c(c2, AvidJSONUtil.KEY_Y);
        setGravity(a(true, this.u) | a(false, this.v));
    }

    /* access modifiers changed from: 0000 */
    public void b(ab abVar) {
        JSONObject a2 = u.a();
        u.a(a2, MimeTypes.BASE_TYPE_TEXT, getText().toString());
        abVar.a(a2).b();
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        j a2 = a.a();
        d l2 = a2.l();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject a3 = u.a();
        u.b(a3, "view_id", this.m);
        u.a(a3, "ad_session_id", this.z);
        u.b(a3, "container_x", this.n + x2);
        u.b(a3, "container_y", this.o + y2);
        u.b(a3, "view_x", x2);
        u.b(a3, "view_y", y2);
        u.b(a3, "id", this.A.getId());
        switch (action) {
            case 0:
                new ab("AdContainer.on_touch_began", this.A.c(), a3).b();
                break;
            case 1:
                if (!this.A.q()) {
                    a2.a((AdColonyAdView) l2.e().get(this.z));
                }
                if (x2 > 0 && x2 < getWidth() && y2 > 0 && y2 < getHeight()) {
                    new ab("AdContainer.on_touch_ended", this.A.c(), a3).b();
                    break;
                } else {
                    new ab("AdContainer.on_touch_cancelled", this.A.c(), a3).b();
                    break;
                }
                break;
            case 2:
                new ab("AdContainer.on_touch_moved", this.A.c(), a3).b();
                break;
            case 3:
                new ab("AdContainer.on_touch_cancelled", this.A.c(), a3).b();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                u.b(a3, "container_x", ((int) motionEvent.getX(action2)) + this.n);
                u.b(a3, "container_y", ((int) motionEvent.getY(action2)) + this.o);
                u.b(a3, "view_x", (int) motionEvent.getX(action2));
                u.b(a3, "view_y", (int) motionEvent.getY(action2));
                new ab("AdContainer.on_touch_began", this.A.c(), a3).b();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                int x3 = (int) motionEvent.getX(action3);
                int y3 = (int) motionEvent.getY(action3);
                u.b(a3, "container_x", ((int) motionEvent.getX(action3)) + this.n);
                u.b(a3, "container_y", ((int) motionEvent.getY(action3)) + this.o);
                u.b(a3, "view_x", (int) motionEvent.getX(action3));
                u.b(a3, "view_y", (int) motionEvent.getY(action3));
                if (!this.A.q()) {
                    a2.a((AdColonyAdView) l2.e().get(this.z));
                }
                if (x3 > 0 && x3 < getWidth() && y3 > 0 && y3 < getHeight()) {
                    new ab("AdContainer.on_touch_ended", this.A.c(), a3).b();
                    break;
                } else {
                    new ab("AdContainer.on_touch_cancelled", this.A.c(), a3).b();
                    break;
                }
                break;
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean c(ab abVar) {
        JSONObject c2 = abVar.c();
        return u.c(c2, "id") == this.m && u.c(c2, "container_id") == this.A.d() && u.b(c2, "ad_session_id").equals(this.A.b());
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        int i2;
        int i3;
        JSONObject c2 = this.B.c();
        this.z = u.b(c2, "ad_session_id");
        this.n = u.c(c2, AvidJSONUtil.KEY_X);
        this.o = u.c(c2, AvidJSONUtil.KEY_Y);
        this.p = u.c(c2, "width");
        this.q = u.c(c2, "height");
        this.s = u.c(c2, "font_family");
        this.r = u.c(c2, "font_style");
        this.t = u.c(c2, "font_size");
        this.w = u.b(c2, "background_color");
        this.x = u.b(c2, "font_color");
        this.y = u.b(c2, MimeTypes.BASE_TYPE_TEXT);
        this.u = u.c(c2, "align_x");
        this.v = u.c(c2, "align_y");
        j a2 = a.a();
        if (this.y.equals("")) {
            this.y = "Learn More";
        }
        setVisibility(4);
        LayoutParams layoutParams = u.d(c2, "wrap_content") ? new LayoutParams(-2, -2) : new LayoutParams(this.p, this.q);
        layoutParams.gravity = 0;
        setText(this.y);
        setTextSize((float) this.t);
        if (u.d(c2, "overlay")) {
            this.n = 0;
            this.o = 0;
            i3 = (int) (a2.m().p() * 6.0f);
            i2 = (int) (a2.m().p() * 6.0f);
            int p2 = (int) (a2.m().p() * 4.0f);
            setPadding(p2, p2, p2, p2);
            layoutParams.gravity = 8388693;
        } else {
            i3 = 0;
            i2 = 0;
        }
        layoutParams.setMargins(this.n, this.o, i3, i2);
        this.A.addView(this, layoutParams);
        switch (this.s) {
            case 0:
                setTypeface(Typeface.DEFAULT);
                break;
            case 1:
                setTypeface(Typeface.SERIF);
                break;
            case 2:
                setTypeface(Typeface.SANS_SERIF);
                break;
            case 3:
                setTypeface(Typeface.MONOSPACE);
                break;
        }
        switch (this.r) {
            case 0:
                setTypeface(getTypeface(), 0);
                break;
            case 1:
                setTypeface(getTypeface(), 1);
                break;
            case 2:
                setTypeface(getTypeface(), 2);
                break;
            case 3:
                setTypeface(getTypeface(), 3);
                break;
        }
        setGravity(a(true, this.u) | a(false, this.v));
        if (!this.w.equals("")) {
            setBackgroundColor(at.g(this.w));
        }
        if (!this.x.equals("")) {
            setTextColor(at.g(this.x));
        }
        this.A.m().add(a.a("TextView.set_visible", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.k(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_bounds", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.d(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_font_color", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.f(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_background_color", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.e(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_typeface", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.j(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_font_size", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.g(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_font_style", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.h(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.get_text", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.b(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_text", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.i(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.align", (ad) new ad() {
            public void a(ab abVar) {
                if (ar.this.c(abVar)) {
                    ar.this.a(abVar);
                }
            }
        }, true));
        this.A.n().add("TextView.set_visible");
        this.A.n().add("TextView.set_bounds");
        this.A.n().add("TextView.set_font_color");
        this.A.n().add("TextView.set_background_color");
        this.A.n().add("TextView.set_typeface");
        this.A.n().add("TextView.set_font_size");
        this.A.n().add("TextView.set_font_style");
        this.A.n().add("TextView.get_text");
        this.A.n().add("TextView.set_text");
        this.A.n().add("TextView.align");
        new a().a("TextView added to layout").a(w.d);
    }

    /* access modifiers changed from: 0000 */
    public void d(ab abVar) {
        JSONObject c2 = abVar.c();
        this.n = u.c(c2, AvidJSONUtil.KEY_X);
        this.o = u.c(c2, AvidJSONUtil.KEY_Y);
        this.p = u.c(c2, "width");
        this.q = u.c(c2, "height");
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.n, this.o, 0, 0);
        layoutParams.width = this.p;
        layoutParams.height = this.q;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public void e(ab abVar) {
        this.w = u.b(abVar.c(), "background_color");
        setBackgroundColor(at.g(this.w));
    }

    /* access modifiers changed from: 0000 */
    public void f(ab abVar) {
        this.x = u.b(abVar.c(), "font_color");
        setTextColor(at.g(this.x));
    }

    /* access modifiers changed from: 0000 */
    public void g(ab abVar) {
        this.t = u.c(abVar.c(), "font_size");
        setTextSize((float) this.t);
    }

    /* access modifiers changed from: 0000 */
    public void h(ab abVar) {
        int c2 = u.c(abVar.c(), "font_style");
        this.r = c2;
        switch (c2) {
            case 0:
                setTypeface(getTypeface(), 0);
                return;
            case 1:
                setTypeface(getTypeface(), 1);
                return;
            case 2:
                setTypeface(getTypeface(), 2);
                return;
            case 3:
                setTypeface(getTypeface(), 3);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: 0000 */
    public void i(ab abVar) {
        this.y = u.b(abVar.c(), MimeTypes.BASE_TYPE_TEXT);
        setText(this.y);
    }

    /* access modifiers changed from: 0000 */
    public void j(ab abVar) {
        int c2 = u.c(abVar.c(), "font_family");
        this.s = c2;
        switch (c2) {
            case 0:
                setTypeface(Typeface.DEFAULT);
                return;
            case 1:
                setTypeface(Typeface.SERIF);
                return;
            case 2:
                setTypeface(Typeface.SANS_SERIF);
                return;
            case 3:
                setTypeface(Typeface.MONOSPACE);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: 0000 */
    public void k(ab abVar) {
        if (u.d(abVar.c(), String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }
}
