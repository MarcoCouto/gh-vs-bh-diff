package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.File;
import org.json.JSONObject;

@SuppressLint({"AppCompatCustomView"})
class s extends ImageView {

    /* renamed from: a reason: collision with root package name */
    private int f973a;
    private int b;
    private int c;
    private int d;
    private int e;
    private boolean f;
    private boolean g;
    private boolean h;
    private String i;
    private String j;
    private ab k;
    private c l;

    private s(Context context) {
        super(context);
    }

    s(Context context, ab abVar, int i2, c cVar) {
        super(context);
        this.f973a = i2;
        this.k = abVar;
        this.l = cVar;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        j a2 = a.a();
        d l2 = a2.l();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        JSONObject a3 = u.a();
        u.b(a3, "view_id", this.f973a);
        u.a(a3, "ad_session_id", this.j);
        u.b(a3, "container_x", this.b + x);
        u.b(a3, "container_y", this.c + y);
        u.b(a3, "view_x", x);
        u.b(a3, "view_y", y);
        u.b(a3, "id", this.l.getId());
        switch (action) {
            case 0:
                new ab("AdContainer.on_touch_began", this.l.c(), a3).b();
                break;
            case 1:
                if (!this.l.q()) {
                    a2.a((AdColonyAdView) l2.e().get(this.j));
                }
                if (x > 0 && x < this.d && y > 0 && y < this.e) {
                    new ab("AdContainer.on_touch_ended", this.l.c(), a3).b();
                    break;
                } else {
                    new ab("AdContainer.on_touch_cancelled", this.l.c(), a3).b();
                    break;
                }
                break;
            case 2:
                new ab("AdContainer.on_touch_moved", this.l.c(), a3).b();
                break;
            case 3:
                new ab("AdContainer.on_touch_cancelled", this.l.c(), a3).b();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                u.b(a3, "container_x", ((int) motionEvent.getX(action2)) + this.b);
                u.b(a3, "container_y", ((int) motionEvent.getY(action2)) + this.c);
                u.b(a3, "view_x", (int) motionEvent.getX(action2));
                u.b(a3, "view_y", (int) motionEvent.getY(action2));
                new ab("AdContainer.on_touch_began", this.l.c(), a3).b();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                int x2 = (int) motionEvent.getX(action3);
                int y2 = (int) motionEvent.getY(action3);
                u.b(a3, "container_x", ((int) motionEvent.getX(action3)) + this.b);
                u.b(a3, "container_y", ((int) motionEvent.getY(action3)) + this.c);
                u.b(a3, "view_x", (int) motionEvent.getX(action3));
                u.b(a3, "view_y", (int) motionEvent.getY(action3));
                if (!this.l.q()) {
                    a2.a((AdColonyAdView) l2.e().get(this.j));
                }
                if (x2 > 0 && x2 < this.d && y2 > 0 && y2 < this.e) {
                    new ab("AdContainer.on_touch_ended", this.l.c(), a3).b();
                    break;
                } else {
                    new ab("AdContainer.on_touch_cancelled", this.l.c(), a3).b();
                    break;
                }
                break;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean a(ab abVar) {
        JSONObject c2 = abVar.c();
        return u.c(c2, "id") == this.f973a && u.c(c2, "container_id") == this.l.d() && u.b(c2, "ad_session_id").equals(this.l.b());
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        JSONObject c2 = this.k.c();
        this.j = u.b(c2, "ad_session_id");
        this.b = u.c(c2, AvidJSONUtil.KEY_X);
        this.c = u.c(c2, AvidJSONUtil.KEY_Y);
        this.d = u.c(c2, "width");
        this.e = u.c(c2, "height");
        this.i = u.b(c2, "filepath");
        this.f = u.d(c2, "dpi");
        this.g = u.d(c2, "invert_y");
        this.h = u.d(c2, "wrap_content");
        setImageURI(Uri.fromFile(new File(this.i)));
        if (this.f) {
            float p = (((float) this.e) * a.a().m().p()) / ((float) getDrawable().getIntrinsicHeight());
            this.e = (int) (((float) getDrawable().getIntrinsicHeight()) * p);
            this.d = (int) (((float) getDrawable().getIntrinsicWidth()) * p);
            this.b -= this.d;
            this.c = this.g ? this.c + this.e : this.c - this.e;
        }
        setVisibility(4);
        LayoutParams layoutParams = this.h ? new LayoutParams(-2, -2) : new LayoutParams(this.d, this.e);
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.gravity = 0;
        this.l.addView(this, layoutParams);
        this.l.m().add(a.a("ImageView.set_visible", (ad) new ad() {
            public void a(ab abVar) {
                if (s.this.a(abVar)) {
                    s.this.d(abVar);
                }
            }
        }, true));
        this.l.m().add(a.a("ImageView.set_bounds", (ad) new ad() {
            public void a(ab abVar) {
                if (s.this.a(abVar)) {
                    s.this.b(abVar);
                }
            }
        }, true));
        this.l.m().add(a.a("ImageView.set_image", (ad) new ad() {
            public void a(ab abVar) {
                if (s.this.a(abVar)) {
                    s.this.c(abVar);
                }
            }
        }, true));
        this.l.n().add("ImageView.set_visible");
        this.l.n().add("ImageView.set_bounds");
        this.l.n().add("ImageView.set_image");
    }

    /* access modifiers changed from: private */
    public void b(ab abVar) {
        JSONObject c2 = abVar.c();
        this.b = u.c(c2, AvidJSONUtil.KEY_X);
        this.c = u.c(c2, AvidJSONUtil.KEY_Y);
        this.d = u.c(c2, "width");
        this.e = u.c(c2, "height");
        if (this.f) {
            float p = (((float) this.e) * a.a().m().p()) / ((float) getDrawable().getIntrinsicHeight());
            this.e = (int) (((float) getDrawable().getIntrinsicHeight()) * p);
            this.d = (int) (((float) getDrawable().getIntrinsicWidth()) * p);
            this.b -= this.d;
            this.c -= this.e;
        }
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.width = this.d;
        layoutParams.height = this.e;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public void c(ab abVar) {
        this.i = u.b(abVar.c(), "filepath");
        setImageURI(Uri.fromFile(new File(this.i)));
    }

    /* access modifiers changed from: private */
    public void d(ab abVar) {
        if (u.d(abVar.c(), String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }

    /* access modifiers changed from: 0000 */
    public int[] b() {
        return new int[]{this.b, this.c, this.d, this.e};
    }
}
