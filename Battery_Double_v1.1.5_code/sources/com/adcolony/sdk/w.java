package com.adcolony.sdk;

class w {

    /* renamed from: a reason: collision with root package name */
    static w f979a = new w(3, false);
    static w b = new w(3, true);
    static w c = new w(2, false);
    static w d = new w(2, true);
    static w e = new w(1, false);
    static w f = new w(1, true);
    static w g = new w(0, false);
    static w h = new w(0, true);
    private int i;
    private boolean j;

    static class a {

        /* renamed from: a reason: collision with root package name */
        StringBuilder f980a = new StringBuilder();

        a() {
        }

        /* access modifiers changed from: 0000 */
        public a a(char c) {
            if (c != 10) {
                this.f980a.append(c);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(String str) {
            this.f980a.append(str);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(Object obj) {
            if (obj != null) {
                this.f980a.append(obj.toString());
            } else {
                this.f980a.append("null");
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(double d) {
            at.a(d, 2, this.f980a);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(int i) {
            this.f980a.append(i);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(boolean z) {
            this.f980a.append(z);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public void a(w wVar) {
            wVar.a(this.f980a.toString());
        }
    }

    w(int i2, boolean z) {
        this.i = i2;
        this.j = z;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        y.a(this.i, str, this.j);
    }
}
