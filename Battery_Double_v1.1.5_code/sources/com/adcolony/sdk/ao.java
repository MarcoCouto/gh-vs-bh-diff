package com.adcolony.sdk;

import android.content.Context;
import android.os.StatFs;
import com.github.mikephil.charting.utils.Utils;
import java.io.File;

class ao {

    /* renamed from: a reason: collision with root package name */
    private String f748a;
    private String b;
    private String c;
    private String d;
    private File e;
    private File f;
    private File g;

    ao() {
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        new a().a("Configuring storage").a(w.d);
        j a2 = a.a();
        StringBuilder sb = new StringBuilder();
        sb.append(c());
        sb.append("/adc3/");
        this.f748a = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(this.f748a);
        sb2.append("media/");
        this.b = sb2.toString();
        this.e = new File(this.b);
        if (!this.e.isDirectory()) {
            this.e.delete();
            this.e.mkdirs();
        }
        if (!this.e.isDirectory()) {
            a2.a(true);
            return false;
        } else if (a(this.b) < 2.097152E7d) {
            new a().a("Not enough memory available at media path, disabling AdColony.").a(w.e);
            a2.a(true);
            return false;
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(c());
            sb3.append("/adc3/data/");
            this.c = sb3.toString();
            this.f = new File(this.c);
            if (!this.f.isDirectory()) {
                this.f.delete();
            }
            this.f.mkdirs();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(this.f748a);
            sb4.append("tmp/");
            this.d = sb4.toString();
            this.g = new File(this.d);
            if (!this.g.isDirectory()) {
                this.g.delete();
                this.g.mkdirs();
            }
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        if (this.e == null || this.f == null || this.g == null) {
            return false;
        }
        if (!this.e.isDirectory()) {
            this.e.delete();
        }
        if (!this.f.isDirectory()) {
            this.f.delete();
        }
        if (!this.g.isDirectory()) {
            this.g.delete();
        }
        this.e.mkdirs();
        this.f.mkdirs();
        this.g.mkdirs();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        return c2.getFilesDir().getAbsolutePath();
    }

    /* access modifiers changed from: 0000 */
    public double a(String str) {
        try {
            StatFs statFs = new StatFs(str);
            return (double) (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()));
        } catch (RuntimeException unused) {
            return Utils.DOUBLE_EPSILON;
        }
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public String f() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public String g() {
        return this.f748a;
    }
}
