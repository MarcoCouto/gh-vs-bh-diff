package com.adcolony.sdk;

import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyInterstitialActivity extends b {
    AdColonyInterstitial n;
    private i o;

    public AdColonyInterstitialActivity() {
        AdColonyInterstitial adColonyInterstitial;
        if (!a.b()) {
            adColonyInterstitial = null;
        } else {
            adColonyInterstitial = a.a().v();
        }
        this.n = adColonyInterstitial;
    }

    public /* bridge */ /* synthetic */ void onBackPressed() {
        super.onBackPressed();
    }

    public /* bridge */ /* synthetic */ void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public /* bridge */ /* synthetic */ void onDestroy() {
        super.onDestroy();
    }

    public /* bridge */ /* synthetic */ void onPause() {
        super.onPause();
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    public /* bridge */ /* synthetic */ void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public void onCreate(Bundle bundle) {
        this.d = this.n == null ? 0 : this.n.e();
        super.onCreate(bundle);
        if (a.b() && this.n != null) {
            ag h = this.n.h();
            if (h != null) {
                h.a(this.n.d());
            }
            this.o = new i(new Handler(Looper.getMainLooper()), this.n);
            if (this.n.getListener() != null) {
                this.n.getListener().onOpened(this.n);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(ab abVar) {
        super.a(abVar);
        d l = a.a().l();
        e eVar = (e) l.f().remove(this.e);
        if (eVar != null) {
            for (MediaPlayer mediaPlayer : eVar.c().c().values()) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                mediaPlayer.release();
            }
            eVar.d().a().autoPause();
            eVar.d().a().release();
        }
        JSONObject f = u.f(abVar.c(), "v4iap");
        JSONArray g = u.g(f, "product_ids");
        if (!(f == null || this.n == null || this.n.getListener() == null || g.length() <= 0)) {
            this.n.getListener().onIAPEvent(this.n, u.c(g, 0), u.c(f, "engagement_type"));
        }
        l.a(this.c);
        if (this.n != null) {
            l.c().remove(this.n.f());
        }
        if (!(this.n == null || this.n.getListener() == null)) {
            this.n.getListener().onClosed(this.n);
            this.n.a((c) null);
            this.n.setListener(null);
            this.n = null;
        }
        if (this.o != null) {
            this.o.a();
            this.o = null;
        }
        new a().a("finish_ad call finished").a(w.d);
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyInterstitial adColonyInterstitial) {
        this.n = adColonyInterstitial;
    }
}
