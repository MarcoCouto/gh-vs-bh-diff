package com.adcolony.sdk;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewParent;

public class AdColonyAdViewActivity extends b {
    AdColonyAdView n;

    public AdColonyAdViewActivity() {
        AdColonyAdView adColonyAdView;
        if (!a.b()) {
            adColonyAdView = null;
        } else {
            adColonyAdView = a.a().u();
        }
        this.n = adColonyAdView;
    }

    public /* bridge */ /* synthetic */ void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public /* bridge */ /* synthetic */ void onDestroy() {
        super.onDestroy();
    }

    public /* bridge */ /* synthetic */ void onPause() {
        super.onPause();
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    public /* bridge */ /* synthetic */ void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public void onCreate(Bundle bundle) {
        if (!a.b() || this.n == null) {
            a.a().a((AdColonyAdView) null);
            finish();
            return;
        }
        this.d = this.n.getOrientation();
        super.onCreate(bundle);
        this.n.b();
        AdColonyAdViewListener listener = this.n.getListener();
        if (listener != null) {
            listener.onOpened(this.n);
        }
    }

    public void onBackPressed() {
        b();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        ViewParent parent = this.c.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.c);
        }
        this.n.c();
        a.a().a((AdColonyAdView) null);
        finish();
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        this.n.b();
    }
}
