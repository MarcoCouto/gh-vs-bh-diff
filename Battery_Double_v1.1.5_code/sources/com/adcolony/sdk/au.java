package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import com.github.mikephil.charting.utils.Utils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

@TargetApi(14)
class au extends TextureView implements OnCompletionListener, OnErrorListener, OnPreparedListener, OnSeekCompleteListener, SurfaceTextureListener {
    /* access modifiers changed from: private */
    public boolean A;
    private boolean B;
    /* access modifiers changed from: private */
    public boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private String F;
    /* access modifiers changed from: private */
    public String G;
    private FileInputStream H;
    private ab I;
    /* access modifiers changed from: private */
    public c J;
    private Surface K;
    private SurfaceTexture L;
    /* access modifiers changed from: private */
    public RectF M = new RectF();
    /* access modifiers changed from: private */
    public a N;
    private ProgressBar O;
    /* access modifiers changed from: private */
    public MediaPlayer P;
    /* access modifiers changed from: private */
    public JSONObject Q = u.a();
    private ExecutorService R = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public ab S;

    /* renamed from: a reason: collision with root package name */
    private float f784a;
    private float b;
    /* access modifiers changed from: private */
    public float c;
    /* access modifiers changed from: private */
    public float d;
    private float e;
    private float f;
    /* access modifiers changed from: private */
    public int g;
    private boolean h = true;
    /* access modifiers changed from: private */
    public Paint i = new Paint();
    /* access modifiers changed from: private */
    public Paint j = new Paint(1);
    private int k;
    private int l;
    private int m;
    private int n;
    /* access modifiers changed from: private */
    public int o;
    private int p;
    private int q;
    private int r;
    /* access modifiers changed from: private */
    public double s;
    /* access modifiers changed from: private */
    public double t;
    /* access modifiers changed from: private */
    public long u;
    /* access modifiers changed from: private */
    public boolean v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public boolean y;
    private boolean z;

    private class a extends View {
        a(Context context) {
            super(context);
            setWillNotDraw(false);
            try {
                getClass().getMethod("setLayerType", new Class[]{Integer.TYPE, Paint.class}).invoke(this, new Object[]{Integer.valueOf(1), null});
            } catch (Exception unused) {
            }
        }

        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawArc(au.this.M, 270.0f, au.this.d, false, au.this.i);
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(au.this.g);
            String sb2 = sb.toString();
            float centerX = au.this.M.centerX();
            double centerY = (double) au.this.M.centerY();
            double d = (double) au.this.j.getFontMetrics().bottom;
            Double.isNaN(d);
            double d2 = d * 1.35d;
            Double.isNaN(centerY);
            canvas.drawText(sb2, centerX, (float) (centerY + d2), au.this.j);
            invalidate();
        }
    }

    private au(Context context) {
        super(context);
    }

    au(Context context, ab abVar, int i2, c cVar) {
        super(context);
        this.J = cVar;
        this.I = abVar;
        this.o = i2;
        setSurfaceTextureListener(this);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.L != null) {
            this.A = true;
        }
        this.R.shutdown();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        JSONObject c2 = this.I.c();
        this.G = u.b(c2, "ad_session_id");
        this.k = u.c(c2, AvidJSONUtil.KEY_X);
        this.l = u.c(c2, AvidJSONUtil.KEY_Y);
        this.m = u.c(c2, "width");
        this.n = u.c(c2, "height");
        this.C = u.d(c2, "enable_timer");
        this.E = u.d(c2, "enable_progress");
        this.F = u.b(c2, "filepath");
        this.p = u.c(c2, AdvertisementColumns.COLUMN_VIDEO_WIDTH);
        this.q = u.c(c2, AdvertisementColumns.COLUMN_VIDEO_HEIGHT);
        this.f = a.a().m().p();
        new a().a("Original video dimensions = ").a(this.p).a(AvidJSONUtil.KEY_X).a(this.q).a(w.b);
        setVisibility(4);
        LayoutParams layoutParams = new LayoutParams(this.m, this.n);
        layoutParams.setMargins(this.k, this.l, 0, 0);
        layoutParams.gravity = 0;
        this.J.addView(this, layoutParams);
        if (this.E) {
            Context c3 = a.c();
            if (c3 != null) {
                this.O = new ProgressBar(c3);
                this.J.addView(this.O, new LayoutParams((int) (this.f * 100.0f), (int) (this.f * 100.0f), 17));
            }
        }
        this.P = new MediaPlayer();
        this.z = false;
        try {
            if (!this.F.startsWith("http")) {
                this.H = new FileInputStream(this.F);
                this.P.setDataSource(this.H.getFD());
            } else {
                this.B = true;
                this.P.setDataSource(this.F);
            }
            this.P.setOnErrorListener(this);
            this.P.setOnPreparedListener(this);
            this.P.setOnCompletionListener(this);
            this.P.prepareAsync();
        } catch (IOException e2) {
            new a().a("Failed to create/prepare MediaPlayer: ").a(e2.toString()).a(w.g);
            l();
        }
        this.J.m().add(a.a("VideoView.play", (ad) new ad() {
            public void a(ab abVar) {
                if (au.this.a(abVar)) {
                    au.this.e();
                }
            }
        }, true));
        this.J.m().add(a.a("VideoView.set_bounds", (ad) new ad() {
            public void a(ab abVar) {
                if (au.this.a(abVar)) {
                    au.this.b(abVar);
                }
            }
        }, true));
        this.J.m().add(a.a("VideoView.set_visible", (ad) new ad() {
            public void a(ab abVar) {
                if (au.this.a(abVar)) {
                    au.this.c(abVar);
                }
            }
        }, true));
        this.J.m().add(a.a("VideoView.pause", (ad) new ad() {
            public void a(ab abVar) {
                if (au.this.a(abVar)) {
                    au.this.f();
                }
            }
        }, true));
        this.J.m().add(a.a("VideoView.seek_to_time", (ad) new ad() {
            public void a(ab abVar) {
                if (au.this.a(abVar)) {
                    au.this.e(abVar);
                }
            }
        }, true));
        this.J.m().add(a.a("VideoView.set_volume", (ad) new ad() {
            public void a(ab abVar) {
                if (au.this.a(abVar)) {
                    au.this.d(abVar);
                }
            }
        }, true));
        this.J.n().add("VideoView.play");
        this.J.n().add("VideoView.set_bounds");
        this.J.n().add("VideoView.set_visible");
        this.J.n().add("VideoView.pause");
        this.J.n().add("VideoView.seek_to_time");
        this.J.n().add("VideoView.set_volume");
    }

    /* access modifiers changed from: private */
    public boolean a(ab abVar) {
        JSONObject c2 = abVar.c();
        return u.c(c2, "id") == this.o && u.c(c2, "container_id") == this.J.d() && u.b(c2, "ad_session_id").equals(this.J.b());
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        if (surfaceTexture == null || this.A) {
            new a().a("Null texture provided by system's onSurfaceTextureAvailable or ").a("MediaPlayer has been destroyed.").a(w.h);
            return;
        }
        this.K = new Surface(surfaceTexture);
        try {
            this.P.setSurface(this.K);
        } catch (IllegalStateException unused) {
            new a().a("IllegalStateException thrown when calling MediaPlayer.setSurface()").a(w.g);
            l();
        }
        this.L = surfaceTexture;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.L = surfaceTexture;
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.L = surfaceTexture;
        if (!this.A) {
            return false;
        }
        surfaceTexture.release();
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        this.L = surfaceTexture;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        j a2 = a.a();
        d l2 = a2.l();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject a3 = u.a();
        u.b(a3, "view_id", this.o);
        u.a(a3, "ad_session_id", this.G);
        u.b(a3, "container_x", this.k + x2);
        u.b(a3, "container_y", this.l + y2);
        u.b(a3, "view_x", x2);
        u.b(a3, "view_y", y2);
        u.b(a3, "id", this.J.d());
        switch (action) {
            case 0:
                new ab("AdContainer.on_touch_began", this.J.c(), a3).b();
                break;
            case 1:
                if (!this.J.q()) {
                    a2.a((AdColonyAdView) l2.e().get(this.G));
                }
                new ab("AdContainer.on_touch_ended", this.J.c(), a3).b();
                break;
            case 2:
                new ab("AdContainer.on_touch_moved", this.J.c(), a3).b();
                break;
            case 3:
                new ab("AdContainer.on_touch_cancelled", this.J.c(), a3).b();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                u.b(a3, "container_x", ((int) motionEvent.getX(action2)) + this.k);
                u.b(a3, "container_y", ((int) motionEvent.getY(action2)) + this.l);
                u.b(a3, "view_x", (int) motionEvent.getX(action2));
                u.b(a3, "view_y", (int) motionEvent.getY(action2));
                new ab("AdContainer.on_touch_began", this.J.c(), a3).b();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                u.b(a3, "container_x", ((int) motionEvent.getX(action3)) + this.k);
                u.b(a3, "container_y", ((int) motionEvent.getY(action3)) + this.l);
                u.b(a3, "view_x", (int) motionEvent.getX(action3));
                u.b(a3, "view_y", (int) motionEvent.getY(action3));
                if (!this.J.q()) {
                    a2.a((AdColonyAdView) l2.e().get(this.G));
                }
                new ab("AdContainer.on_touch_ended", this.J.c(), a3).b();
                break;
        }
        return true;
    }

    private void k() {
        double d2 = (double) this.m;
        double d3 = (double) this.p;
        Double.isNaN(d2);
        Double.isNaN(d3);
        double d4 = d2 / d3;
        double d5 = (double) this.n;
        double d6 = (double) this.q;
        Double.isNaN(d5);
        Double.isNaN(d6);
        double d7 = d5 / d6;
        if (d4 > d7) {
            d4 = d7;
        }
        double d8 = (double) this.p;
        Double.isNaN(d8);
        int i2 = (int) (d8 * d4);
        double d9 = (double) this.q;
        Double.isNaN(d9);
        int i3 = (int) (d9 * d4);
        new a().a("setMeasuredDimension to ").a(i2).a(" by ").a(i3).a(w.d);
        setMeasuredDimension(i2, i3);
        if (this.B) {
            LayoutParams layoutParams = (LayoutParams) getLayoutParams();
            layoutParams.width = i2;
            layoutParams.height = i3;
            layoutParams.gravity = 17;
            layoutParams.setMargins(0, 0, 0, 0);
            setLayoutParams(layoutParams);
        }
    }

    public void onMeasure(int i2, int i3) {
        k();
    }

    /* access modifiers changed from: private */
    public void l() {
        JSONObject a2 = u.a();
        u.a(a2, "id", this.G);
        new ab("AdSession.on_error", this.J.c(), a2).b();
        this.v = true;
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        l();
        a aVar = new a();
        StringBuilder sb = new StringBuilder();
        sb.append("MediaPlayer error: ");
        sb.append(i2);
        sb.append(",");
        sb.append(i3);
        aVar.a(sb.toString()).a(w.g);
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.z = true;
        if (this.E) {
            this.J.removeView(this.O);
        }
        if (this.B) {
            this.p = mediaPlayer.getVideoWidth();
            this.q = mediaPlayer.getVideoHeight();
            k();
            new a().a("MediaPlayer getVideoWidth = ").a(mediaPlayer.getVideoWidth()).a(w.d);
            new a().a("MediaPlayer getVideoHeight = ").a(mediaPlayer.getVideoHeight()).a(w.d);
        }
        JSONObject a2 = u.a();
        u.b(a2, "id", this.o);
        u.b(a2, "container_id", this.J.d());
        u.a(a2, "ad_session_id", this.G);
        new a().a("ADCVideoView is prepared").a(w.b);
        new ab("VideoView.on_ready", this.J.c(), a2).b();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.v = true;
        this.s = this.t;
        u.b(this.Q, "id", this.o);
        u.b(this.Q, "container_id", this.J.d());
        u.a(this.Q, "ad_session_id", this.G);
        u.a(this.Q, "elapsed", this.s);
        u.a(this.Q, IronSourceConstants.EVENTS_DURATION, this.t);
        new ab("VideoView.on_progress", this.J.c(), this.Q).b();
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        if (this.R != null && !this.R.isShutdown()) {
            try {
                this.R.submit(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(150);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (au.this.S != null) {
                            JSONObject a2 = u.a();
                            u.b(a2, "id", au.this.o);
                            u.a(a2, "ad_session_id", au.this.G);
                            u.b(a2, "success", true);
                            au.this.S.a(a2).b();
                            au.this.S = null;
                        }
                    }
                });
            } catch (RejectedExecutionException unused) {
                l();
            }
        }
    }

    private void m() {
        try {
            this.R.submit(new Runnable() {
                public void run() {
                    au.this.u = 0;
                    while (!au.this.v && !au.this.y && a.d()) {
                        Context c = a.c();
                        if (!au.this.v && !au.this.A && c != null && (c instanceof Activity)) {
                            if (au.this.P.isPlaying()) {
                                if (au.this.u == 0 && a.b) {
                                    au.this.u = System.currentTimeMillis();
                                }
                                au.this.x = true;
                                au auVar = au.this;
                                double currentPosition = (double) au.this.P.getCurrentPosition();
                                Double.isNaN(currentPosition);
                                auVar.s = currentPosition / 1000.0d;
                                au auVar2 = au.this;
                                double duration = (double) au.this.P.getDuration();
                                Double.isNaN(duration);
                                auVar2.t = duration / 1000.0d;
                                if (System.currentTimeMillis() - au.this.u > 1000 && !au.this.D && a.b) {
                                    if (au.this.s == Utils.DOUBLE_EPSILON) {
                                        new a().a("getCurrentPosition() not working, firing ").a("AdSession.on_error").a(w.h);
                                        au.this.l();
                                    } else {
                                        au.this.D = true;
                                    }
                                }
                                if (au.this.C) {
                                    au.this.c();
                                }
                            }
                            if (au.this.x && !au.this.v && !au.this.y) {
                                u.b(au.this.Q, "id", au.this.o);
                                u.b(au.this.Q, "container_id", au.this.J.d());
                                u.a(au.this.Q, "ad_session_id", au.this.G);
                                u.a(au.this.Q, "elapsed", au.this.s);
                                u.a(au.this.Q, IronSourceConstants.EVENTS_DURATION, au.this.t);
                                new ab("VideoView.on_progress", au.this.J.c(), au.this.Q).b();
                            }
                            if (au.this.w || ((Activity) c).isFinishing()) {
                                au.this.w = false;
                                au.this.d();
                                return;
                            }
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException unused) {
                                au.this.l();
                                new a().a("InterruptedException in ADCVideoView's update thread.").a(w.g);
                            }
                        } else {
                            return;
                        }
                    }
                    if (au.this.w) {
                        au.this.d();
                    }
                }
            });
        } catch (RejectedExecutionException unused) {
            l();
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (this.h) {
            this.e = (float) (360.0d / this.t);
            this.j.setColor(-3355444);
            this.j.setShadowLayer((float) ((int) (this.f * 2.0f)), 0.0f, 0.0f, ViewCompat.MEASURED_STATE_MASK);
            this.j.setTextAlign(Align.CENTER);
            this.j.setLinearText(true);
            this.j.setTextSize(this.f * 12.0f);
            this.i.setStyle(Style.STROKE);
            float f2 = 6.0f;
            if (this.f * 2.0f <= 6.0f) {
                f2 = this.f * 2.0f;
            }
            float f3 = 4.0f;
            if (f2 >= 4.0f) {
                f3 = f2;
            }
            this.i.setStrokeWidth(f3);
            this.i.setShadowLayer((float) ((int) (this.f * 3.0f)), 0.0f, 0.0f, ViewCompat.MEASURED_STATE_MASK);
            this.i.setColor(-3355444);
            Rect rect = new Rect();
            this.j.getTextBounds("0123456789", 0, 9, rect);
            this.c = (float) rect.height();
            final Context c2 = a.c();
            if (c2 != null) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        au.this.N = new a(c2);
                        LayoutParams layoutParams = new LayoutParams((int) (au.this.c * 4.0f), (int) (au.this.c * 4.0f));
                        layoutParams.setMargins(0, au.this.J.o() - ((int) (au.this.c * 4.0f)), 0, 0);
                        layoutParams.gravity = 0;
                        au.this.J.addView(au.this.N, layoutParams);
                    }
                });
            }
            this.h = false;
        }
        this.g = (int) (this.t - this.s);
        this.f784a = (float) ((int) this.c);
        this.b = (float) ((int) (this.c * 3.0f));
        this.M.set(this.f784a - (this.c / 2.0f), this.b - (this.c * 2.0f), this.f784a + (this.c * 2.0f), this.b + (this.c / 2.0f));
        double d2 = (double) this.e;
        double d3 = this.t - this.s;
        Double.isNaN(d2);
        this.d = (float) (d2 * d3);
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        new a().a("MediaPlayer stopped and released.").a(w.d);
        try {
            if (!this.v && this.z && this.P.isPlaying()) {
                this.P.stop();
            }
        } catch (IllegalStateException unused) {
            new a().a("Caught IllegalStateException when calling stop on MediaPlayer").a(w.f);
        }
        if (this.O != null) {
            this.J.removeView(this.O);
        }
        this.v = true;
        this.z = false;
        this.P.release();
    }

    /* access modifiers changed from: private */
    public void b(ab abVar) {
        JSONObject c2 = abVar.c();
        this.k = u.c(c2, AvidJSONUtil.KEY_X);
        this.l = u.c(c2, AvidJSONUtil.KEY_Y);
        this.m = u.c(c2, "width");
        this.n = u.c(c2, "height");
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.k, this.l, 0, 0);
        layoutParams.width = this.m;
        layoutParams.height = this.n;
        setLayoutParams(layoutParams);
        if (this.C && this.N != null) {
            LayoutParams layoutParams2 = new LayoutParams((int) (this.c * 4.0f), (int) (this.c * 4.0f));
            layoutParams2.setMargins(0, this.J.o() - ((int) (this.c * 4.0f)), 0, 0);
            layoutParams2.gravity = 0;
            this.N.setLayoutParams(layoutParams2);
        }
    }

    /* access modifiers changed from: private */
    public void c(ab abVar) {
        if (u.d(abVar.c(), String.VISIBLE)) {
            setVisibility(0);
            if (this.C && this.N != null) {
                this.N.setVisibility(0);
                return;
            }
            return;
        }
        setVisibility(4);
        if (this.C && this.N != null) {
            this.N.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public boolean d(ab abVar) {
        boolean z2 = false;
        if (!this.z) {
            return false;
        }
        float e2 = (float) u.e(abVar.c(), AvidVideoPlaybackListenerImpl.VOLUME);
        AdColonyInterstitial v2 = a.a().v();
        if (v2 != null) {
            if (((double) e2) <= Utils.DOUBLE_EPSILON) {
                z2 = true;
            }
            v2.b(z2);
        }
        this.P.setVolume(e2, e2);
        JSONObject a2 = u.a();
        u.b(a2, "success", true);
        abVar.a(a2).b();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean e() {
        if (!this.z) {
            return false;
        }
        if (!this.y && a.b) {
            this.P.start();
            m();
            new a().a("MediaPlayer is prepared - ADCVideoView play() called.").a(w.b);
        } else if (!this.v && a.b) {
            this.P.start();
            this.y = false;
            if (!this.R.isShutdown()) {
                m();
            }
            if (this.N != null) {
                this.N.invalidate();
            }
        }
        setWillNotDraw(false);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean f() {
        if (!this.z) {
            new a().a("ADCVideoView pause() called while MediaPlayer is not prepared.").a(w.f);
            return false;
        } else if (!this.x) {
            new a().a("Ignoring ADCVideoView pause due to invalid MediaPlayer state.").a(w.d);
            return false;
        } else {
            this.r = this.P.getCurrentPosition();
            this.t = (double) this.P.getDuration();
            this.P.pause();
            this.y = true;
            new a().a("Video view paused").a(w.b);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean e(ab abVar) {
        if (!this.z) {
            return false;
        }
        if (this.v) {
            this.v = false;
        }
        this.S = abVar;
        int c2 = u.c(abVar.c(), LocationConst.TIME);
        int i2 = c2 * 1000;
        int duration = this.P.getDuration() / 1000;
        this.P.setOnSeekCompleteListener(this);
        this.P.seekTo(i2);
        if (duration == c2) {
            this.v = true;
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        this.w = true;
    }

    /* access modifiers changed from: 0000 */
    public boolean h() {
        return this.P != null;
    }

    /* access modifiers changed from: 0000 */
    public MediaPlayer i() {
        return this.P;
    }

    /* access modifiers changed from: 0000 */
    public boolean j() {
        return this.v;
    }
}
