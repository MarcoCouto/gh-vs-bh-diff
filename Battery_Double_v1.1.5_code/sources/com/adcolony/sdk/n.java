package com.adcolony.sdk;

import android.content.Context;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.vungle.warren.model.Advertisement;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

class n implements Runnable {

    /* renamed from: a reason: collision with root package name */
    String f936a = "";
    String b = "";
    boolean c;
    int d;
    int e;
    private HttpURLConnection f;
    private InputStream g;
    private ab h;
    private a i;
    private final int j = 4096;
    private String k;
    private int l = 0;
    private boolean m = false;
    private Map<String, List<String>> n;
    private String o = "";
    private String p = "";

    interface a {
        void a(n nVar, ab abVar, Map<String, List<String>> map);
    }

    n(ab abVar, a aVar) {
        this.h = abVar;
        this.i = aVar;
    }

    public void run() {
        boolean z = false;
        this.c = false;
        try {
            if (b()) {
                this.c = c();
                if (this.h.d().equals("WebServices.post") && this.e != 200) {
                    this.c = false;
                }
            }
        } catch (MalformedURLException e2) {
            new a().a("MalformedURLException: ").a(e2.toString()).a(w.h);
            this.c = true;
        } catch (OutOfMemoryError unused) {
            a a2 = new a().a("Out of memory error - disabling AdColony. (").a(this.d).a("/").a(this.l);
            StringBuilder sb = new StringBuilder();
            sb.append("): ");
            sb.append(this.f936a);
            a2.a(sb.toString()).a(w.g);
            a.a().a(true);
        } catch (IOException e3) {
            new a().a("Download of ").a(this.f936a).a(" failed: ").a(e3.toString()).a(w.f);
            this.e = this.e == 0 ? 504 : this.e;
        } catch (IllegalStateException e4) {
            new a().a("okhttp error: ").a(e4.toString()).a(w.g);
            e4.printStackTrace();
        } catch (Exception e5) {
            new a().a("Exception: ").a(e5.toString()).a(w.g);
            e5.printStackTrace();
        }
        z = true;
        if (this.c) {
            new a().a("Downloaded ").a(this.f936a).a(w.d);
        }
        if (z) {
            if (this.h.d().equals("WebServices.download")) {
                a(this.p, this.o);
            }
            this.i.a(this, this.h, this.n);
        }
    }

    private boolean b() throws IOException {
        JSONObject c2 = this.h.c();
        String b2 = u.b(c2, Param.CONTENT_TYPE);
        String b3 = u.b(c2, "content");
        boolean d2 = u.d(c2, "no_redirect");
        this.f936a = u.b(c2, "url");
        this.o = u.b(c2, "filepath");
        StringBuilder sb = new StringBuilder();
        sb.append(a.a().o().f());
        sb.append(this.o.substring(this.o.lastIndexOf("/") + 1));
        this.p = sb.toString();
        this.k = u.b(c2, "encoding");
        this.l = u.a(c2, "max_size", 0);
        this.m = this.l != 0;
        this.d = 0;
        this.g = null;
        this.f = null;
        this.n = null;
        String str = Advertisement.FILE_SCHEME;
        if (this.f936a.startsWith(str)) {
            String str2 = "file:///android_asset/";
            if (this.f936a.startsWith(str2)) {
                Context c3 = a.c();
                if (c3 != null) {
                    this.g = c3.getAssets().open(this.f936a.substring(str2.length()));
                }
            } else {
                this.g = new FileInputStream(this.f936a.substring(str.length()));
            }
        } else {
            this.f = (HttpURLConnection) new URL(this.f936a).openConnection();
            this.f.setInstanceFollowRedirects(!d2);
            this.f.setRequestProperty(HttpRequest.HEADER_ACCEPT_CHARSET, "UTF-8");
            this.f.setRequestProperty("User-Agent", a.a().m().G());
            if (!b2.equals("")) {
                this.f.setRequestProperty("Content-Type", b2);
            }
            if (this.h.d().equals("WebServices.post")) {
                this.f.setDoOutput(true);
                this.f.setFixedLengthStreamingMode(b3.getBytes("UTF-8").length);
                new PrintStream(this.f.getOutputStream()).print(b3);
            }
        }
        if (this.f == null && this.g == null) {
            return false;
        }
        return true;
    }

    private boolean c() throws Exception {
        OutputStream outputStream;
        String d2 = this.h.d();
        if (this.g != null) {
            outputStream = this.o.length() == 0 ? new ByteArrayOutputStream(4096) : new FileOutputStream(new File(this.o).getAbsolutePath());
        } else if (d2.equals("WebServices.download")) {
            this.g = this.f.getInputStream();
            outputStream = new FileOutputStream(this.p);
        } else if (d2.equals("WebServices.get")) {
            this.g = this.f.getInputStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else if (d2.equals("WebServices.post")) {
            this.f.connect();
            this.g = this.f.getResponseCode() == 200 ? this.f.getInputStream() : this.f.getErrorStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else {
            outputStream = null;
        }
        if (this.f != null) {
            this.e = this.f.getResponseCode();
            this.n = this.f.getHeaderFields();
        }
        return a(this.g, outputStream);
    }

    private boolean a(InputStream inputStream, OutputStream outputStream) throws Exception {
        BufferedInputStream bufferedInputStream;
        Throwable e2;
        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = bufferedInputStream.read(bArr, 0, 4096);
                    if (read != -1) {
                        this.d += read;
                        if (this.m) {
                            if (this.d > this.l) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Data exceeds expected maximum (");
                                sb.append(this.d);
                                sb.append("/");
                                sb.append(this.l);
                                sb.append("): ");
                                sb.append(this.f.getURL().toString());
                                throw new Exception(sb.toString());
                            }
                        }
                        outputStream.write(bArr, 0, read);
                    } else {
                        String str = "UTF-8";
                        if (this.k != null && !this.k.isEmpty()) {
                            str = this.k;
                        }
                        if (outputStream instanceof ByteArrayOutputStream) {
                            this.b = ((ByteArrayOutputStream) outputStream).toString(str);
                        }
                        if (outputStream != null) {
                            outputStream.close();
                        }
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        bufferedInputStream.close();
                        return true;
                    }
                }
            } catch (Exception e3) {
                e2 = e3;
                try {
                    throw e2;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (Exception e4) {
            Throwable th2 = e4;
            bufferedInputStream = null;
            e2 = th2;
            throw e2;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            bufferedInputStream = null;
            th = th4;
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (bufferedInputStream != null) {
                bufferedInputStream.close();
            }
            throw th;
        }
    }

    private void a(String str, String str2) {
        try {
            String substring = str2.substring(0, str2.lastIndexOf("/") + 1);
            if (str2 != null && !"".equals(str2) && !substring.equals(a.a().o().f()) && !new File(str).renameTo(new File(str2))) {
                a a2 = new a().a("Moving of ");
                if (str == null) {
                    str = "temp folder's asset file";
                }
                a2.a(str).a(" failed.").a(w.f);
            }
        } catch (Exception e2) {
            new a().a("Exception: ").a(e2.toString()).a(w.g);
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: 0000 */
    public ab a() {
        return this.h;
    }
}
