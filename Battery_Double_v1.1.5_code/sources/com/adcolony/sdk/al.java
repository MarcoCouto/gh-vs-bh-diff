package com.adcolony.sdk;

import android.content.Context;
import com.facebook.login.widget.ToolTipPopup;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executors;
import org.json.JSONObject;

class al implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final long f743a = 30000;
    private final int b = 17;
    private final int c = 15000;
    private final int d = 1000;
    private long e;
    private long f;
    private long g;
    private long h;
    private long i;
    private long j;
    private long k;
    private long l;
    private boolean m = true;
    private boolean n = true;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    /* access modifiers changed from: private */
    public boolean s;
    private boolean t;

    al() {
    }

    public void a() {
        a.a("SessionInfo.stopped", (ad) new ad() {
            public void a(ab abVar) {
                al.this.s = true;
            }
        });
    }

    public void run() {
        long j2;
        while (!this.r) {
            this.h = System.currentTimeMillis();
            a.f();
            if (this.f >= 30000) {
                break;
            }
            if (!this.m) {
                if (this.o && !this.n) {
                    this.o = false;
                    f();
                }
                long j3 = this.f;
                if (this.l == 0) {
                    j2 = 0;
                } else {
                    j2 = System.currentTimeMillis() - this.l;
                }
                this.f = j3 + j2;
                this.l = System.currentTimeMillis();
            } else {
                if (this.o && this.n) {
                    this.o = false;
                    g();
                }
                this.f = 0;
                this.l = 0;
            }
            this.g = 17;
            a(this.g);
            this.i = System.currentTimeMillis() - this.h;
            if (this.i > 0 && this.i < ToolTipPopup.DEFAULT_POPUP_DISPLAY_TIME) {
                this.e += this.i;
            }
            j a2 = a.a();
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.k > MTGInterstitialActivity.WEB_LOAD_TIME) {
                this.k = currentTimeMillis;
            }
            if (a.d() && currentTimeMillis - this.j > 1000) {
                this.j = currentTimeMillis;
                String c2 = a2.d.c();
                if (!c2.equals(a2.w())) {
                    a2.a(c2);
                    JSONObject a3 = u.a();
                    u.a(a3, "network_type", a2.w());
                    new ab("Network.on_status_change", 1, a3).b();
                }
            }
        }
        new a().a("AdColony session ending, releasing Context.").a(w.c);
        a.a().b(true);
        a.a((Context) null);
        this.q = true;
        this.t = true;
        b();
        a aVar = new a(10.0d);
        while (!this.s && !aVar.b() && this.t) {
            a.f();
            a(100);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        if (!this.p) {
            if (this.q) {
                a.a().b(false);
                this.q = false;
            }
            this.e = 0;
            this.f = 0;
            this.p = true;
            this.m = true;
            this.s = false;
            new Thread(this).start();
            if (z) {
                JSONObject a2 = u.a();
                u.a(a2, "id", at.e());
                new ab("SessionInfo.on_start", 1, a2).b();
                av avVar = (av) a.a().q().e().get(Integer.valueOf(1));
                if (avVar != null) {
                    avVar.j();
                }
            }
            if (AdColony.f698a.isShutdown()) {
                AdColony.f698a = Executors.newSingleThreadExecutor();
            }
            y.a();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.p = false;
        this.m = false;
        if (y.l != null) {
            y.l.a();
        }
        JSONObject a2 = u.a();
        double d2 = (double) this.e;
        Double.isNaN(d2);
        u.a(a2, "session_length", d2 / 1000.0d);
        new ab("SessionInfo.on_stop", 1, a2).b();
        a.f();
        AdColony.f698a.shutdown();
    }

    private void f() {
        b(false);
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z) {
        ArrayList c2 = a.a().q().c();
        synchronized (c2) {
            Iterator it = c2.iterator();
            while (it.hasNext()) {
                ae aeVar = (ae) it.next();
                JSONObject a2 = u.a();
                u.b(a2, "from_window_focus", z);
                new ab("SessionInfo.on_pause", aeVar.a(), a2).b();
            }
        }
        this.n = true;
        a.f();
    }

    private void g() {
        c(false);
    }

    /* access modifiers changed from: 0000 */
    public void c(boolean z) {
        ArrayList c2 = a.a().q().c();
        synchronized (c2) {
            Iterator it = c2.iterator();
            while (it.hasNext()) {
                ae aeVar = (ae) it.next();
                JSONObject a2 = u.a();
                u.b(a2, "from_window_focus", z);
                new ab("SessionInfo.on_resume", aeVar.a(), a2).b();
            }
        }
        y.a();
        this.n = false;
    }

    /* access modifiers changed from: 0000 */
    public void d(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: 0000 */
    public void e(boolean z) {
        this.o = z;
    }

    /* access modifiers changed from: 0000 */
    public void f(boolean z) {
        this.t = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    public boolean e() {
        return this.p;
    }

    private void a(long j2) {
        try {
            Thread.sleep(j2);
        } catch (InterruptedException unused) {
        }
    }
}
