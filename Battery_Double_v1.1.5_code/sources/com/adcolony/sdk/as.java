package com.adcolony.sdk;

import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;

class as {

    /* renamed from: a reason: collision with root package name */
    ak f780a;
    z b = new z().b();
    float[] c = new float[16];
    z d = new z().b();
    z e = new z().b();
    z f = new z().b();
    ArrayList<z> g = new ArrayList<>();
    ArrayList<z> h = new ArrayList<>();
    boolean i;
    boolean j;
    boolean k = true;

    as(ak akVar) {
        this.f780a = akVar;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.f780a.d();
        this.e.b();
        this.i = true;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a();
        do {
        } while (d());
    }

    /* access modifiers changed from: 0000 */
    public z c() {
        int size = this.h.size();
        if (size == 0) {
            return new z();
        }
        return (z) this.h.remove(size - 1);
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        int size = this.g.size();
        if (size == 0) {
            return false;
        }
        this.f780a.d();
        this.j = true;
        this.h.add(this.g.remove(size - 1));
        this.e.b();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        this.f780a.d();
        this.g.add(c().b(this.e));
        this.e.b();
        this.j = true;
        this.i = true;
    }

    /* access modifiers changed from: 0000 */
    public void a(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10) {
        double d11;
        double d12;
        boolean z;
        double d13;
        double d14 = d4;
        double d15 = d5;
        this.f780a.d();
        double d16 = d14 / d9;
        double d17 = d15 / d10;
        boolean z2 = true;
        double d18 = 1.0d;
        if (d16 >= Utils.DOUBLE_EPSILON) {
            d12 = d16;
            d11 = 1.0d;
            z = false;
        } else {
            d12 = -d16;
            d11 = -1.0d;
            z = true;
        }
        if (d17 >= Utils.DOUBLE_EPSILON) {
            d13 = d17;
            z2 = false;
        } else {
            d13 = -d17;
            d18 = -1.0d;
        }
        double d19 = d6 * d14;
        double d20 = d7 * d15;
        if (z || z2) {
            d19 -= d14 / 2.0d;
            d20 -= d15 / 2.0d;
            b((-d14) / 2.0d, (-d15) / 2.0d);
        }
        double cos = Math.cos(d8);
        double sin = Math.sin(d8);
        double d21 = d19 * d12;
        double d22 = (d2 - (d21 * cos)) + (sin * d13 * d20);
        double d23 = d12 * sin * d11;
        double d24 = cos * d13;
        this.e.a(cos * d12 * d11, d23, Utils.DOUBLE_EPSILON, Utils.DOUBLE_EPSILON, (-sin) * d13 * d18, d24 * d18, Utils.DOUBLE_EPSILON, Utils.DOUBLE_EPSILON, Utils.DOUBLE_EPSILON, Utils.DOUBLE_EPSILON, 1.0d, Utils.DOUBLE_EPSILON, d22, (d3 - (d21 * sin)) - (d24 * d20), Utils.DOUBLE_EPSILON, 1.0d);
    }

    /* access modifiers changed from: 0000 */
    public void a(double d2) {
        this.f780a.d();
        this.e.b(d2);
    }

    /* access modifiers changed from: 0000 */
    public void b(double d2) {
        this.f780a.d();
        this.e.a(d2);
    }

    /* access modifiers changed from: 0000 */
    public void a(double d2, double d3) {
        this.f780a.d();
        this.e.a(d2, d3, 1.0d);
    }

    /* access modifiers changed from: 0000 */
    public void a(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12, double d13, double d14, double d15, double d16, double d17) {
        double d18 = d2;
        double d19 = d3;
        double d20 = d4;
        double d21 = d5;
        double d22 = d6;
        double d23 = d7;
        double d24 = d8;
        double d25 = d9;
        double d26 = d10;
        double d27 = d11;
        double d28 = d12;
        double d29 = d13;
        double d30 = d14;
        double d31 = d15;
        double d32 = d16;
        double d33 = d17;
        this.f780a.d();
        this.e.b(d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33);
        this.i = true;
    }

    /* access modifiers changed from: 0000 */
    public void a(z zVar) {
        this.f780a.d();
        this.d.b(zVar);
        this.k = true;
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        b();
    }

    /* access modifiers changed from: 0000 */
    public void b(double d2, double d3) {
        this.e.b(d2, d3, Utils.DOUBLE_EPSILON);
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        if (this.j || this.k) {
            this.k = false;
            if (this.j) {
                this.j = false;
                this.f.b();
                for (int size = this.g.size() - 1; size >= 0; size--) {
                    this.f.a((z) this.g.get(size));
                }
            }
            this.b.b();
            this.b.a(this.e);
            this.b.a(this.f);
            this.b.a(this.d);
            this.b.a(this.c);
        }
    }
}
