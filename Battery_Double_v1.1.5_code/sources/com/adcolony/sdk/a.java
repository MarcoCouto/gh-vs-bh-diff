package com.adcolony.sdk;

import android.content.Context;
import java.lang.ref.WeakReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a {

    /* renamed from: a reason: collision with root package name */
    static boolean f724a;
    static boolean b;
    private static WeakReference<Context> c;
    /* access modifiers changed from: private */
    public static j d;

    a() {
    }

    static void a(final Context context, AdColonyAppOptions adColonyAppOptions, boolean z) {
        a(context);
        b = true;
        if (d == null) {
            d = new j();
            d.a(adColonyAppOptions, z);
        } else {
            d.a(adColonyAppOptions);
        }
        at.b.execute(new Runnable() {
            public void run() {
                a.d.a(context, (ab) null);
            }
        });
        new a().a("Configuring AdColony").a(w.c);
        d.b(false);
        d.k().d(true);
        d.k().e(true);
        d.k().f(false);
        d.f = true;
        d.k().a(false);
    }

    static j a() {
        if (!b()) {
            Context c2 = c();
            if (c2 == null) {
                return new j();
            }
            d = new j();
            StringBuilder sb = new StringBuilder();
            sb.append(c2.getFilesDir().getAbsolutePath());
            sb.append("/adc3/AppInfo");
            JSONObject c3 = u.c(sb.toString());
            JSONArray g = u.g(c3, "zoneIds");
            d.a(new AdColonyAppOptions().a(u.b(c3, "appId")).a(u.a(g)), false);
        }
        return d;
    }

    static boolean b() {
        return d != null;
    }

    static void a(Context context) {
        if (context == null) {
            c.clear();
        } else {
            c = new WeakReference<>(context);
        }
    }

    static Context c() {
        if (c == null) {
            return null;
        }
        return (Context) c.get();
    }

    static boolean d() {
        return (c == null || c.get() == null) ? false : true;
    }

    static boolean e() {
        return f724a;
    }

    static void a(String str, ad adVar) {
        a().q().a(str, adVar);
    }

    static ad a(String str, ad adVar, boolean z) {
        a().q().a(str, adVar);
        return adVar;
    }

    static void b(String str, ad adVar) {
        a().q().b(str, adVar);
    }

    static void f() {
        a().q().b();
    }

    static void a(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = u.a();
        }
        u.a(jSONObject, "m_type", str);
        a().q().a(jSONObject);
    }

    static void a(String str) {
        try {
            ab abVar = new ab("CustomMessage.send", 0);
            abVar.c().put("message", str);
            abVar.b();
        } catch (JSONException e) {
            new a().a("JSON error from ADC.java's send_custom_message(): ").a(e.toString()).a(w.h);
        }
    }
}
