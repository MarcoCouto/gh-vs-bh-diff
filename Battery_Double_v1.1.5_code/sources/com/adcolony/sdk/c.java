package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.FloatRange;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
import com.github.mikephil.charting.utils.Utils;
import com.iab.omid.library.adcolony.adsession.AdSession;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

class c extends FrameLayout {
    private boolean A;
    private AdSession B;

    /* renamed from: a reason: collision with root package name */
    boolean f814a;
    boolean b;
    Context c;
    VideoView d;
    private HashMap<Integer, au> e;
    private HashMap<Integer, ar> f;
    private HashMap<Integer, av> g;
    private HashMap<Integer, f> h;
    private HashMap<Integer, p> i;
    private HashMap<Integer, s> j;
    private HashMap<Integer, Boolean> k;
    private HashMap<Integer, View> l;
    private int m;
    private int n;
    private int o;
    private int p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public float r = 0.0f;
    /* access modifiers changed from: private */
    public double s = Utils.DOUBLE_EPSILON;
    /* access modifiers changed from: private */
    public long t = 0;
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public int v = 0;
    private ArrayList<ad> w;
    private ArrayList<String> x;
    private boolean y;
    private boolean z;

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    private c(Context context) {
        super(context);
    }

    c(Context context, String str) {
        super(context);
        this.c = context;
        this.q = str;
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ab abVar) {
        JSONObject c2 = abVar.c();
        return u.c(c2, "container_id") == this.o && u.b(c2, "ad_session_id").equals(this.q);
    }

    /* access modifiers changed from: 0000 */
    public void b(ab abVar) {
        int i2;
        this.e = new HashMap<>();
        this.f = new HashMap<>();
        this.g = new HashMap<>();
        this.h = new HashMap<>();
        this.i = new HashMap<>();
        this.j = new HashMap<>();
        this.k = new HashMap<>();
        this.l = new HashMap<>();
        this.w = new ArrayList<>();
        this.x = new ArrayList<>();
        JSONObject c2 = abVar.c();
        if (u.d(c2, "transparent")) {
            setBackgroundColor(0);
        }
        this.o = u.c(c2, "id");
        this.m = u.c(c2, "width");
        this.n = u.c(c2, "height");
        this.p = u.c(c2, "module_id");
        this.b = u.d(c2, "viewability_enabled");
        this.y = this.o == 1;
        j a2 = a.a();
        if (this.m == 0 && this.n == 0) {
            this.m = a2.m().q();
            if (a2.d().getMultiWindowEnabled()) {
                i2 = a2.m().r() - at.c(a.c());
            } else {
                i2 = a2.m().r();
            }
            this.n = i2;
        } else {
            setLayoutParams(new LayoutParams(this.m, this.n));
        }
        this.w.add(a.a("VideoView.create", (ad) new ad() {
            public void a(ab abVar) {
                if (c.this.a(abVar)) {
                    c.this.a((View) c.this.g(abVar));
                }
            }
        }, true));
        this.w.add(a.a("VideoView.destroy", (ad) new ad() {
            public void a(ab abVar) {
                if (c.this.a(abVar)) {
                    c.this.h(abVar);
                }
            }
        }, true));
        this.w.add(a.a("WebView.create", (ad) new ad() {
            public void a(final ab abVar) {
                if (c.this.a(abVar)) {
                    at.a((Runnable) new Runnable() {
                        public void run() {
                            c.this.a((View) c.this.i(abVar));
                        }
                    });
                }
            }
        }, true));
        this.w.add(a.a("WebView.destroy", (ad) new ad() {
            public void a(final ab abVar) {
                if (c.this.a(abVar)) {
                    at.a((Runnable) new Runnable() {
                        public void run() {
                            c.this.j(abVar);
                        }
                    });
                }
            }
        }, true));
        this.w.add(a.a("TextView.create", (ad) new ad() {
            public void a(ab abVar) {
                if (c.this.a(abVar)) {
                    c.this.a(c.this.k(abVar));
                }
            }
        }, true));
        this.w.add(a.a("TextView.destroy", (ad) new ad() {
            public void a(ab abVar) {
                if (c.this.a(abVar)) {
                    c.this.l(abVar);
                }
            }
        }, true));
        this.w.add(a.a("ImageView.create", (ad) new ad() {
            public void a(ab abVar) {
                if (c.this.a(abVar)) {
                    c.this.a((View) c.this.e(abVar));
                }
            }
        }, true));
        this.w.add(a.a("ImageView.destroy", (ad) new ad() {
            public void a(ab abVar) {
                if (c.this.a(abVar)) {
                    c.this.f(abVar);
                }
            }
        }, true));
        this.w.add(a.a("ColorView.create", (ad) new ad() {
            public void a(ab abVar) {
                if (c.this.a(abVar)) {
                    c.this.a((View) c.this.c(abVar));
                }
            }
        }, true));
        this.w.add(a.a("ColorView.destroy", (ad) new ad() {
            public void a(ab abVar) {
                if (c.this.a(abVar)) {
                    c.this.d(abVar);
                }
            }
        }, true));
        this.x.add("VideoView.create");
        this.x.add("VideoView.destroy");
        this.x.add("WebView.create");
        this.x.add("WebView.destroy");
        this.x.add("TextView.create");
        this.x.add("TextView.destroy");
        this.x.add("ImageView.create");
        this.x.add("ImageView.destroy");
        this.x.add("ColorView.create");
        this.x.add("ColorView.destroy");
        this.d = new VideoView(this.c);
        this.d.setVisibility(8);
        addView(this.d);
        setClipToPadding(false);
        if (this.b) {
            d(u.d(abVar.c(), "advanced_viewability"));
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        j a2 = a.a();
        d l2 = a2.l();
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject a3 = u.a();
        u.b(a3, "view_id", -1);
        u.a(a3, "ad_session_id", this.q);
        u.b(a3, "container_x", x2);
        u.b(a3, "container_y", y2);
        u.b(a3, "view_x", x2);
        u.b(a3, "view_y", y2);
        u.b(a3, "id", this.o);
        switch (action) {
            case 0:
                new ab("AdContainer.on_touch_began", this.p, a3).b();
                break;
            case 1:
                if (!this.y) {
                    a2.a((AdColonyAdView) l2.e().get(this.q));
                }
                new ab("AdContainer.on_touch_ended", this.p, a3).b();
                break;
            case 2:
                new ab("AdContainer.on_touch_moved", this.p, a3).b();
                break;
            case 3:
                new ab("AdContainer.on_touch_cancelled", this.p, a3).b();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                u.b(a3, "container_x", (int) motionEvent.getX(action2));
                u.b(a3, "container_y", (int) motionEvent.getY(action2));
                u.b(a3, "view_x", (int) motionEvent.getX(action2));
                u.b(a3, "view_y", (int) motionEvent.getY(action2));
                new ab("AdContainer.on_touch_began", this.p, a3).b();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                u.b(a3, "container_x", (int) motionEvent.getX(action3));
                u.b(a3, "container_y", (int) motionEvent.getY(action3));
                u.b(a3, "view_x", (int) motionEvent.getX(action3));
                u.b(a3, "view_y", (int) motionEvent.getY(action3));
                u.b(a3, AvidJSONUtil.KEY_X, (int) motionEvent.getX(action3));
                u.b(a3, AvidJSONUtil.KEY_Y, (int) motionEvent.getY(action3));
                if (!this.y) {
                    a2.a((AdColonyAdView) l2.e().get(this.q));
                }
                new ab("AdContainer.on_touch_ended", this.p, a3).b();
                break;
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public f c(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        f fVar = new f(this.c, abVar, c2, this);
        fVar.a();
        this.h.put(Integer.valueOf(c2), fVar);
        this.l.put(Integer.valueOf(c2), fVar);
        return fVar;
    }

    /* access modifiers changed from: 0000 */
    public boolean d(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        f fVar = (f) this.h.remove(Integer.valueOf(c2));
        if (((View) this.l.remove(Integer.valueOf(c2))) == null || fVar == null) {
            d l2 = a.a().l();
            String d2 = abVar.d();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(c2);
            l2.a(d2, sb.toString());
            return false;
        }
        removeView(fVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public s e(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        s sVar = new s(this.c, abVar, c2, this);
        sVar.a();
        this.j.put(Integer.valueOf(c2), sVar);
        this.l.put(Integer.valueOf(c2), sVar);
        return sVar;
    }

    /* access modifiers changed from: 0000 */
    public boolean f(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        s sVar = (s) this.j.remove(Integer.valueOf(c2));
        if (((View) this.l.remove(Integer.valueOf(c2))) == null || sVar == null) {
            d l2 = a.a().l();
            String d2 = abVar.d();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(c2);
            l2.a(d2, sb.toString());
            return false;
        }
        removeView(sVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public au g(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        au auVar = new au(this.c, abVar, c2, this);
        auVar.b();
        this.e.put(Integer.valueOf(c2), auVar);
        this.l.put(Integer.valueOf(c2), auVar);
        return auVar;
    }

    /* access modifiers changed from: 0000 */
    public boolean h(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        au auVar = (au) this.e.remove(Integer.valueOf(c2));
        if (((View) this.l.remove(Integer.valueOf(c2))) == null || auVar == null) {
            d l2 = a.a().l();
            String d2 = abVar.d();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(c2);
            l2.a(d2, sb.toString());
            return false;
        }
        if (auVar.h()) {
            auVar.d();
        }
        auVar.a();
        removeView(auVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public av i(ab abVar) {
        av avVar;
        JSONObject c2 = abVar.c();
        int c3 = u.c(c2, "id");
        boolean d2 = u.d(c2, "is_module");
        j a2 = a.a();
        if (d2) {
            avVar = (av) a2.y().get(Integer.valueOf(u.c(c2, "module_id")));
            if (avVar == null) {
                new a().a("Module WebView created with invalid id").a(w.g);
                return null;
            }
            avVar.a(abVar, c3, this);
        } else {
            try {
                avVar = new av(this.c, abVar, c3, a2.q().d(), this);
            } catch (RuntimeException e2) {
                a aVar = new a();
                StringBuilder sb = new StringBuilder();
                sb.append(e2.toString());
                sb.append(": during WebView initialization.");
                aVar.a(sb.toString()).a(" Disabling AdColony.").a(w.g);
                AdColony.disable();
                return null;
            }
        }
        this.g.put(Integer.valueOf(c3), avVar);
        this.l.put(Integer.valueOf(c3), avVar);
        JSONObject a3 = u.a();
        u.b(a3, "module_id", avVar.a());
        u.b(a3, "mraid_module_id", avVar.b());
        abVar.a(a3).b();
        return avVar;
    }

    /* access modifiers changed from: 0000 */
    public boolean j(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        j a2 = a.a();
        View view = (View) this.l.remove(Integer.valueOf(c2));
        av avVar = (av) this.g.remove(Integer.valueOf(c2));
        if (avVar == null || view == null) {
            d l2 = a2.l();
            String d2 = abVar.d();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(c2);
            l2.a(d2, sb.toString());
            return false;
        }
        a2.q().a(avVar.a());
        removeView(avVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"InlinedApi"})
    public View k(ab abVar) {
        JSONObject c2 = abVar.c();
        int c3 = u.c(c2, "id");
        if (u.d(c2, "editable")) {
            p pVar = new p(this.c, abVar, c3, this);
            pVar.a();
            this.i.put(Integer.valueOf(c3), pVar);
            this.l.put(Integer.valueOf(c3), pVar);
            this.k.put(Integer.valueOf(c3), Boolean.valueOf(true));
            return pVar;
        } else if (!u.d(c2, "button")) {
            ar arVar = new ar(this.c, abVar, c3, this);
            arVar.a();
            this.f.put(Integer.valueOf(c3), arVar);
            this.l.put(Integer.valueOf(c3), arVar);
            this.k.put(Integer.valueOf(c3), Boolean.valueOf(false));
            return arVar;
        } else {
            ar arVar2 = new ar(this.c, 16974145, abVar, c3, this);
            arVar2.a();
            this.f.put(Integer.valueOf(c3), arVar2);
            this.l.put(Integer.valueOf(c3), arVar2);
            this.k.put(Integer.valueOf(c3), Boolean.valueOf(false));
            return arVar2;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean l(ab abVar) {
        TextView textView;
        int c2 = u.c(abVar.c(), "id");
        View view = (View) this.l.remove(Integer.valueOf(c2));
        if (((Boolean) this.k.remove(Integer.valueOf(this.o))).booleanValue()) {
            textView = (TextView) this.i.remove(Integer.valueOf(c2));
        } else {
            textView = (TextView) this.f.remove(Integer.valueOf(c2));
        }
        if (view == null || textView == null) {
            d l2 = a.a().l();
            String d2 = abVar.d();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(c2);
            l2.a(d2, sb.toString());
            return false;
        }
        removeView(textView);
        return true;
    }

    private void d(final boolean z2) {
        final AnonymousClass3 r0 = new Runnable() {
            public void run() {
                av webView;
                double d;
                if (c.this.t == 0) {
                    c.this.t = System.currentTimeMillis();
                }
                View view = (View) c.this.getParent();
                AdColonyAdView adColonyAdView = (AdColonyAdView) a.a().l().e().get(c.this.q);
                if (adColonyAdView == null) {
                    webView = null;
                } else {
                    webView = adColonyAdView.getWebView();
                }
                av avVar = webView;
                Context c = a.c();
                boolean z = false;
                float a2 = ax.a(view, c, true, z2, true, adColonyAdView != null);
                if (c == null) {
                    d = Utils.DOUBLE_EPSILON;
                } else {
                    d = at.b(at.a(c));
                }
                int a3 = at.a((View) avVar);
                int b2 = at.b((View) avVar);
                if (!(a3 == c.this.u && b2 == c.this.v)) {
                    z = true;
                }
                if (z) {
                    c.this.u = a3;
                    c.this.v = b2;
                    c.this.a(a3, b2, avVar);
                }
                long currentTimeMillis = System.currentTimeMillis();
                if (c.this.t + 200 < currentTimeMillis) {
                    c.this.t = currentTimeMillis;
                    if (!(c.this.r == a2 && c.this.s == d && !z)) {
                        c.this.a(a2, d);
                    }
                    c.this.r = a2;
                    c.this.s = d;
                }
            }
        };
        new Thread(new Runnable() {
            public void run() {
                while (!c.this.f814a) {
                    at.a(r0);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException unused) {
                    }
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, av avVar) {
        float p2 = a.a().m().p();
        if (avVar != null) {
            JSONObject a2 = u.a();
            u.b(a2, "app_orientation", at.j(at.h()));
            u.b(a2, "width", (int) (((float) avVar.s()) / p2));
            u.b(a2, "height", (int) (((float) avVar.t()) / p2));
            u.b(a2, AvidJSONUtil.KEY_X, i2);
            u.b(a2, AvidJSONUtil.KEY_Y, i3);
            u.a(a2, "ad_session_id", this.q);
            new ab("MRAID.on_size_change", this.p, a2).b();
        }
    }

    /* access modifiers changed from: private */
    public void a(@FloatRange(from = 0.0d, to = 100.0d) float f2, @FloatRange(from = 0.0d, to = 1.0d) double d2) {
        JSONObject a2 = u.a();
        u.b(a2, "id", this.o);
        u.a(a2, "ad_session_id", this.q);
        u.a(a2, "exposure", (double) f2);
        u.a(a2, AvidVideoPlaybackListenerImpl.VOLUME, d2);
        new ab("AdContainer.on_exposure_change", this.p, a2).b();
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        JSONObject a2 = u.a();
        u.a(a2, "id", this.q);
        new ab("AdSession.on_error", this.p, a2).b();
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.q;
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        return this.p;
    }

    /* access modifiers changed from: 0000 */
    public int d() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, au> e() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, ar> f() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, av> g() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, f> h() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, p> i() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, s> j() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, Boolean> k() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, View> l() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public ArrayList<ad> m() {
        return this.w;
    }

    /* access modifiers changed from: 0000 */
    public ArrayList<String> n() {
        return this.x;
    }

    /* access modifiers changed from: 0000 */
    public int o() {
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        this.n = i2;
    }

    /* access modifiers changed from: 0000 */
    public int p() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        this.m = i2;
    }

    /* access modifiers changed from: 0000 */
    public boolean q() {
        return this.y;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        this.y = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean r() {
        return this.A;
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z2) {
        this.A = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean s() {
        return this.z;
    }

    /* access modifiers changed from: 0000 */
    public void c(boolean z2) {
        this.z = z2;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdSession adSession) {
        this.B = adSession;
        a((Map) this.l);
    }

    /* access modifiers changed from: 0000 */
    public void a(Map map) {
        if (this.B != null && map != null) {
            for (Entry value : map.entrySet()) {
                this.B.addFriendlyObstruction((View) value.getValue());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(View view) {
        if (this.B != null && view != null) {
            this.B.addFriendlyObstruction(view);
        }
    }
}
