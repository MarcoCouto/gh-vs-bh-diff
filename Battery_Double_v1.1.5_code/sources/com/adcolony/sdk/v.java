package com.adcolony.sdk;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.zip.GZIPOutputStream;

class v {

    /* renamed from: a reason: collision with root package name */
    URL f978a;

    public v(URL url) {
        this.f978a = url;
    }

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r3v0, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r1v1, types: [java.io.DataOutputStream] */
    /* JADX WARNING: type inference failed for: r5v0 */
    /* JADX WARNING: type inference failed for: r3v1 */
    /* JADX WARNING: type inference failed for: r1v2 */
    /* JADX WARNING: type inference failed for: r5v1 */
    /* JADX WARNING: type inference failed for: r1v3 */
    /* JADX WARNING: type inference failed for: r3v2 */
    /* JADX WARNING: type inference failed for: r5v2 */
    /* JADX WARNING: type inference failed for: r1v4 */
    /* JADX WARNING: type inference failed for: r5v3 */
    /* JADX WARNING: type inference failed for: r3v3 */
    /* JADX WARNING: type inference failed for: r3v6, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r5v4 */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: type inference failed for: r5v5 */
    /* JADX WARNING: type inference failed for: r5v9 */
    /* JADX WARNING: type inference failed for: r5v10, types: [java.io.DataOutputStream] */
    /* JADX WARNING: type inference failed for: r1v8 */
    /* JADX WARNING: type inference failed for: r1v9 */
    /* JADX WARNING: type inference failed for: r1v10 */
    /* JADX WARNING: type inference failed for: r1v11 */
    /* JADX WARNING: type inference failed for: r1v12 */
    /* JADX WARNING: type inference failed for: r1v13 */
    /* JADX WARNING: type inference failed for: r3v7 */
    /* JADX WARNING: type inference failed for: r5v11 */
    /* JADX WARNING: type inference failed for: r5v12 */
    /* JADX WARNING: type inference failed for: r1v14 */
    /* JADX WARNING: type inference failed for: r3v8 */
    /* JADX WARNING: type inference failed for: r3v9 */
    /* JADX WARNING: type inference failed for: r3v10 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: type inference failed for: r5v13 */
    /* JADX WARNING: type inference failed for: r5v14 */
    /* JADX WARNING: type inference failed for: r5v15 */
    /* JADX WARNING: type inference failed for: r5v16 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v1
  assigns: []
  uses: []
  mth insns count: 89
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x008c  */
    /* JADX WARNING: Unknown variable types count: 12 */
    public int a(String str) throws IOException {
        GZIPOutputStream gZIPOutputStream;
        ? r3;
        ? r1;
        ? r5;
        ? r32;
        ? r52;
        ? r12;
        ? r53;
        ? r13;
        ? r54;
        ? dataOutputStream;
        boolean z = true;
        ? r14 = 0;
        boolean z2 = false;
        try {
            ? r33 = (HttpURLConnection) this.f978a.openConnection();
            try {
                r33.setRequestMethod(HttpRequest.METHOD_POST);
                r33.setRequestProperty(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
                r33.setRequestProperty("Content-Type", "application/json");
                r33.setDoInput(true);
                gZIPOutputStream = new GZIPOutputStream(r33.getOutputStream());
            } catch (IOException e) {
                e = e;
                gZIPOutputStream = null;
                r54 = 0;
                r13 = r33;
                r53 = r54;
                z = false;
                r52 = r53;
                r12 = r13;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    z2 = z;
                    r32 = r12;
                    r5 = r52;
                }
            } catch (Throwable th2) {
                th = th2;
                gZIPOutputStream = null;
                r1 = r14;
                r3 = r33;
                r1.close();
                if (gZIPOutputStream != null) {
                }
                if (r3 != 0) {
                }
                throw th;
            }
            try {
                dataOutputStream = new DataOutputStream(gZIPOutputStream);
            } catch (IOException e2) {
                e = e2;
                r54 = 0;
                r13 = r33;
                r53 = r54;
                z = false;
                r52 = r53;
                r12 = r13;
                throw e;
            } catch (Throwable th3) {
                th = th3;
                r1 = r14;
                r3 = r33;
                if (r1 != 0 && !z2) {
                    r1.close();
                }
                if (gZIPOutputStream != null) {
                    gZIPOutputStream.close();
                }
                if (r3 != 0) {
                    if (r3.getInputStream() != null) {
                        r3.getInputStream().close();
                    }
                    r3.disconnect();
                }
                throw th;
            }
            try {
                dataOutputStream.write(str.getBytes(Charset.forName("UTF-8")));
                dataOutputStream.close();
                try {
                    int responseCode = r33.getResponseCode();
                    gZIPOutputStream.close();
                    if (r33 != 0) {
                        if (r33.getInputStream() != null) {
                            r33.getInputStream().close();
                        }
                        r33.disconnect();
                    }
                    return responseCode;
                } catch (IOException e3) {
                    e = e3;
                    r12 = r33;
                    r52 = dataOutputStream;
                    throw e;
                } catch (Throwable th4) {
                    th = th4;
                    r1 = dataOutputStream;
                    z2 = true;
                    r3 = r33;
                    r1.close();
                    if (gZIPOutputStream != null) {
                    }
                    if (r3 != 0) {
                    }
                    throw th;
                }
            } catch (IOException e4) {
                e = e4;
                r54 = dataOutputStream;
                r13 = r33;
                r53 = r54;
                z = false;
                r52 = r53;
                r12 = r13;
                throw e;
            } catch (Throwable th5) {
                th = th5;
                r32 = r33;
                r5 = dataOutputStream;
                r1 = r5;
                r3 = r32;
                r1.close();
                if (gZIPOutputStream != null) {
                }
                if (r3 != 0) {
                }
                throw th;
            }
        } catch (IOException e5) {
            e = e5;
            gZIPOutputStream = null;
            r53 = 0;
            r13 = r14;
            z = false;
            r52 = r53;
            r12 = r13;
            throw e;
        } catch (Throwable th6) {
            th = th6;
            r3 = 0;
            gZIPOutputStream = null;
            r1 = r14;
            r1.close();
            if (gZIPOutputStream != null) {
            }
            if (r3 != 0) {
            }
            throw th;
        }
    }
}
