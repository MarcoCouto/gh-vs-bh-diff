package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;

@SuppressLint({"UseSparseArrays"})
class aa {

    /* renamed from: a reason: collision with root package name */
    final String f726a;
    /* access modifiers changed from: private */
    public final int b;
    private HashMap<Integer, MediaPlayer> c = new HashMap<>();
    private HashMap<Integer, a> d = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, Boolean> e = new HashMap<>();
    private HashMap<Integer, Boolean> f = new HashMap<>();
    private ArrayList<MediaPlayer> g = new ArrayList<>();

    private class a implements OnErrorListener, OnPreparedListener {

        /* renamed from: a reason: collision with root package name */
        int f727a;
        boolean b;

        a(int i, boolean z) {
            this.f727a = i;
            this.b = z;
        }

        public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            JSONObject a2 = u.a();
            u.b(a2, "id", this.f727a);
            u.a(a2, "ad_session_id", aa.this.f726a);
            new ab("AudioPlayer.on_error", aa.this.b, a2).b();
            return true;
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            mediaPlayer.setLooping(this.b);
            aa.this.e.put(Integer.valueOf(this.f727a), Boolean.valueOf(true));
            JSONObject a2 = u.a();
            u.b(a2, "id", this.f727a);
            u.a(a2, "ad_session_id", aa.this.f726a);
            new ab("AudioPlayer.on_ready", aa.this.b, a2).b();
        }
    }

    aa(String str, int i) {
        this.f726a = str;
        this.b = i;
    }

    /* access modifiers changed from: 0000 */
    public void a(ab abVar) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        JSONObject c2 = abVar.c();
        int c3 = u.c(c2, "id");
        a aVar = new a(c3, u.d(c2, "repeats"));
        this.c.put(Integer.valueOf(c3), mediaPlayer);
        this.d.put(Integer.valueOf(c3), aVar);
        this.e.put(Integer.valueOf(c3), Boolean.valueOf(false));
        this.f.put(Integer.valueOf(c3), Boolean.valueOf(false));
        mediaPlayer.setOnErrorListener(aVar);
        mediaPlayer.setOnPreparedListener(aVar);
        try {
            mediaPlayer.setDataSource(u.b(c2, "filepath"));
        } catch (Exception unused) {
            JSONObject a2 = u.a();
            u.b(a2, "id", c3);
            u.a(a2, "ad_session_id", this.f726a);
            new ab("AudioPlayer.on_error", this.b, a2).b();
        }
        mediaPlayer.prepareAsync();
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.g.clear();
        for (MediaPlayer mediaPlayer : this.c.values()) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                this.g.add(mediaPlayer);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        Iterator it = this.g.iterator();
        while (it.hasNext()) {
            ((MediaPlayer) it.next()).start();
        }
        this.g.clear();
    }

    /* access modifiers changed from: 0000 */
    public void b(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        if (((Boolean) this.f.get(Integer.valueOf(c2))).booleanValue()) {
            ((MediaPlayer) this.c.get(Integer.valueOf(c2))).pause();
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        if (((Boolean) this.e.get(Integer.valueOf(c2))).booleanValue()) {
            ((MediaPlayer) this.c.get(Integer.valueOf(c2))).start();
            this.f.put(Integer.valueOf(c2), Boolean.valueOf(true));
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(ab abVar) {
        ((MediaPlayer) this.c.remove(Integer.valueOf(u.c(abVar.c(), "id")))).release();
    }

    /* access modifiers changed from: 0000 */
    public void e(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        if (((Boolean) this.f.get(Integer.valueOf(c2))).booleanValue()) {
            MediaPlayer mediaPlayer = (MediaPlayer) this.c.get(Integer.valueOf(c2));
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
        }
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, MediaPlayer> c() {
        return this.c;
    }
}
