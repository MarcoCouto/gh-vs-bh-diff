package com.adcolony.sdk;

import com.appodeal.ads.utils.LogConstants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class x {
    static final SimpleDateFormat l = new SimpleDateFormat("yyyyMMdd'T'HHmmss.SSSZ", Locale.US);
    static final String m = "message";
    static final String n = "timestamp";
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public Date f981a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public t c;
    protected String o;

    static class a {
        protected x b = new x();

        a() {
        }

        /* access modifiers changed from: 0000 */
        public a a(int i) {
            this.b.b = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(t tVar) {
            this.b.c = tVar;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(String str) {
            this.b.o = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(Date date) {
            this.b.f981a = date;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public x a() {
            if (this.b.f981a == null) {
                this.b.f981a = new Date(System.currentTimeMillis());
            }
            return this.b;
        }
    }

    x() {
    }

    /* access modifiers changed from: 0000 */
    public void a(t tVar) {
        this.c = tVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        this.b = i;
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        switch (this.b) {
            case -1:
                return "Fatal";
            case 0:
                return "Error";
            case 1:
                return "Warn";
            case 2:
                return LogConstants.EVENT_INFO;
            case 3:
                return "Debug";
            default:
                return "UNKNOWN LOG LEVEL";
        }
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return l.format(this.f981a);
    }

    /* access modifiers changed from: 0000 */
    public t f() {
        return this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(e());
        sb.append(" ");
        sb.append(b());
        sb.append("/");
        sb.append(f().d());
        sb.append(": ");
        sb.append(d());
        return sb.toString();
    }
}
