package com.adcolony.sdk;

import com.mintegral.msdk.base.entity.CampaignEx;
import org.json.JSONObject;

public class AdColonyReward {

    /* renamed from: a reason: collision with root package name */
    private int f721a;
    private String b;
    private String c;
    private boolean d;

    AdColonyReward(ab abVar) {
        JSONObject c2 = abVar.c();
        this.f721a = u.c(c2, CampaignEx.JSON_KEY_REWARD_AMOUNT);
        this.b = u.b(c2, CampaignEx.JSON_KEY_REWARD_NAME);
        this.d = u.d(c2, "success");
        this.c = u.b(c2, "zone_id");
    }

    public int getRewardAmount() {
        return this.f721a;
    }

    public String getRewardName() {
        return this.b;
    }

    public String getZoneID() {
        return this.c;
    }

    public boolean success() {
        return this.d;
    }
}
