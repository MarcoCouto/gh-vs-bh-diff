package com.adcolony.sdk;

import org.json.JSONException;
import org.json.JSONObject;

class ab {

    /* renamed from: a reason: collision with root package name */
    private String f728a;
    private JSONObject b;

    ab(JSONObject jSONObject) {
        try {
            this.b = jSONObject;
            this.f728a = jSONObject.getString("m_type");
        } catch (JSONException e) {
            new a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(w.h);
        }
    }

    ab(String str, int i) {
        try {
            this.f728a = str;
            this.b = new JSONObject();
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(w.h);
        }
    }

    ab(String str, int i, String str2) {
        try {
            this.f728a = str;
            this.b = u.a(str2);
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(w.h);
        }
    }

    ab(String str, int i, JSONObject jSONObject) {
        try {
            this.f728a = str;
            if (jSONObject == null) {
                jSONObject = new JSONObject();
            }
            this.b = jSONObject;
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(w.h);
        }
    }

    /* access modifiers changed from: 0000 */
    public ab a() {
        return a((JSONObject) null);
    }

    /* access modifiers changed from: 0000 */
    public ab a(String str) {
        return a(u.a(str));
    }

    /* access modifiers changed from: 0000 */
    public ab a(JSONObject jSONObject) {
        try {
            ab abVar = new ab("reply", this.b.getInt("m_origin"), jSONObject);
            abVar.b.put("m_id", this.b.getInt("m_id"));
            return abVar;
        } catch (JSONException e) {
            new a().a("JSON error in ADCMessage's createReply(): ").a(e.toString()).a(w.h);
            return new ab("JSONException", 0);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a.a(this.f728a, this.b);
    }

    /* access modifiers changed from: 0000 */
    public JSONObject c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void b(JSONObject jSONObject) {
        this.b = jSONObject;
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return this.f728a;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.f728a = str;
    }
}
