package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.security.NetworkSecurityPolicy;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.webkit.WebView;
import com.facebook.appevents.UserDataStore;
import com.facebook.internal.NativeProtocol;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TapjoyConstants;
import io.realm.BuildConfig;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

class l {

    /* renamed from: a reason: collision with root package name */
    static final String f924a = "Production";
    private static int g = 2;
    private String b = "";
    /* access modifiers changed from: private */
    public String c = "";
    private boolean d;
    private boolean e;
    private JSONObject f = u.a();
    private String h = "android";
    private String i = "android_native";
    private String j = "";

    private static class a extends AsyncTask<Void, Void, JSONObject> {

        /* renamed from: a reason: collision with root package name */
        private ab f929a;
        private boolean b;

        a(ab abVar, boolean z) {
            this.f929a = abVar;
            this.b = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public JSONObject doInBackground(Void... voidArr) {
            if (VERSION.SDK_INT < 14) {
                return null;
            }
            return a.a().m().J();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(JSONObject jSONObject) {
            if (this.b) {
                new ab("Device.update_info", 1, jSONObject).b();
            } else {
                this.f929a.a(jSONObject).b();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public String F() {
        return BuildConfig.VERSION_NAME;
    }

    /* access modifiers changed from: 0000 */
    public String x() {
        return "";
    }

    l() {
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        this.f = jSONObject;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject a() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"HardwareIds"})
    public String b() {
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        return Secure.getString(c2.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return System.getProperty("os.arch").toLowerCase(Locale.ENGLISH);
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        this.d = false;
        a.a("Device.get_info", (ad) new ad() {
            public void a(final ab abVar) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        try {
                            if (l.this.s() < 14) {
                                new a(abVar, false).execute(new Void[0]);
                            } else {
                                new a(abVar, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                            }
                        } catch (RuntimeException unused) {
                            new a().a("Error retrieving device info, disabling AdColony.").a(w.h);
                            AdColony.disable();
                        }
                    }
                });
            }
        });
        a.a("Device.application_exists", (ad) new ad() {
            public void a(ab abVar) {
                JSONObject a2 = u.a();
                u.b(a2, IronSourceConstants.EVENTS_RESULT, at.a(u.b(abVar.c(), "name")));
                u.b(a2, "success", true);
                abVar.a(a2).b();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public String f() {
        return u() ? "tablet" : PlaceFields.PHONE;
    }

    /* access modifiers changed from: 0000 */
    public boolean g() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z) {
        this.e = z;
    }

    /* access modifiers changed from: 0000 */
    public String h() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.j = str;
    }

    /* access modifiers changed from: 0000 */
    public String i() {
        String str;
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        TelephonyManager telephonyManager = (TelephonyManager) c2.getSystemService(PlaceFields.PHONE);
        if (telephonyManager == null) {
            str = "";
        } else {
            str = telephonyManager.getNetworkOperatorName();
        }
        if (str.length() == 0) {
            str = "unknown";
        }
        return str;
    }

    /* access modifiers changed from: 0000 */
    public int j() {
        Context c2 = a.c();
        int i2 = 0;
        if (c2 == null) {
            return 0;
        }
        ActivityManager activityManager = (ActivityManager) c2.getSystemService("activity");
        if (activityManager != null) {
            i2 = activityManager.getMemoryClass();
        }
        return i2;
    }

    /* access modifiers changed from: 0000 */
    public String k() {
        String str;
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        TelephonyManager telephonyManager = (TelephonyManager) c2.getSystemService(PlaceFields.PHONE);
        if (telephonyManager == null) {
            str = "";
        } else {
            str = telephonyManager.getSimCountryIso();
        }
        return str;
    }

    /* access modifiers changed from: 0000 */
    public String l() {
        return TimeZone.getDefault().getID();
    }

    /* access modifiers changed from: 0000 */
    public int m() {
        return TimeZone.getDefault().getOffset(15) / 60000;
    }

    /* access modifiers changed from: 0000 */
    public int n() {
        TimeZone timeZone = TimeZone.getDefault();
        if (!timeZone.inDaylightTime(new Date())) {
            return 0;
        }
        return timeZone.getDSTSavings() / 60000;
    }

    /* access modifiers changed from: 0000 */
    public long o() {
        Runtime runtime = Runtime.getRuntime();
        return (runtime.totalMemory() - runtime.freeMemory()) / ((long) 1048576);
    }

    /* access modifiers changed from: 0000 */
    public float p() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0.0f;
        }
        return c2.getResources().getDisplayMetrics().density;
    }

    /* access modifiers changed from: 0000 */
    public int q() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) c2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.widthPixels;
    }

    /* access modifiers changed from: 0000 */
    public int r() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) c2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.heightPixels;
    }

    /* access modifiers changed from: 0000 */
    public int s() {
        return VERSION.SDK_INT;
    }

    /* access modifiers changed from: 0000 */
    public double t() {
        Context c2 = a.c();
        if (c2 == null) {
            return Utils.DOUBLE_EPSILON;
        }
        Intent registerReceiver = c2.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return Utils.DOUBLE_EPSILON;
        }
        int intExtra = registerReceiver.getIntExtra("level", -1);
        int intExtra2 = registerReceiver.getIntExtra("scale", -1);
        if (intExtra < 0 || intExtra2 < 0) {
            return Utils.DOUBLE_EPSILON;
        }
        double d2 = (double) intExtra;
        double d3 = (double) intExtra2;
        Double.isNaN(d2);
        Double.isNaN(d3);
        return d2 / d3;
    }

    /* access modifiers changed from: 0000 */
    public boolean u() {
        Context c2 = a.c();
        boolean z = false;
        if (c2 == null) {
            return false;
        }
        DisplayMetrics displayMetrics = c2.getResources().getDisplayMetrics();
        float f2 = ((float) displayMetrics.widthPixels) / displayMetrics.xdpi;
        float f3 = ((float) displayMetrics.heightPixels) / displayMetrics.ydpi;
        if (Math.sqrt((double) ((f2 * f2) + (f3 * f3))) >= 6.0d) {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public String v() {
        return Locale.getDefault().getLanguage();
    }

    /* access modifiers changed from: 0000 */
    public String w() {
        return Locale.getDefault().getCountry();
    }

    /* access modifiers changed from: 0000 */
    public boolean y() {
        Context c2 = a.c();
        boolean z = false;
        if (c2 == null || VERSION.SDK_INT < 29) {
            return false;
        }
        int i2 = c2.getResources().getConfiguration().uiMode & 48;
        if (i2 != 16 && i2 == 32) {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public String z() {
        return Build.MANUFACTURER;
    }

    /* access modifiers changed from: 0000 */
    public String A() {
        return Build.MODEL;
    }

    /* access modifiers changed from: 0000 */
    public String B() {
        return VERSION.RELEASE;
    }

    /* access modifiers changed from: 0000 */
    public boolean C() {
        return VERSION.SDK_INT < 23 || NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
    }

    /* access modifiers changed from: 0000 */
    public int D() {
        Context c2 = a.c();
        if (c2 == null) {
            return 2;
        }
        switch (c2.getResources().getConfiguration().orientation) {
            case 1:
                return 0;
            case 2:
                return 1;
            default:
                return 2;
        }
    }

    /* access modifiers changed from: 0000 */
    public int E() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) c2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.densityDpi;
    }

    /* access modifiers changed from: 0000 */
    public String G() {
        final Context c2 = a.c();
        if (this.c.equals("") && c2 != null) {
            at.a((Runnable) new Runnable() {
                public void run() {
                    try {
                        l.this.c = new WebView(c2).getSettings().getUserAgentString();
                    } catch (RuntimeException e) {
                        a aVar = new a();
                        StringBuilder sb = new StringBuilder();
                        sb.append(e.toString());
                        sb.append(": during WebView initialization.");
                        aVar.a(sb.toString()).a(" Disabling AdColony.").a(w.g);
                        AdColony.disable();
                    }
                    a.a().s().a(l.this.c);
                }
            });
        }
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public String H() {
        Context c2 = a.c();
        return c2 == null ? "unknown" : c2.getPackageName();
    }

    /* access modifiers changed from: 0000 */
    public boolean I() {
        if (!a.d()) {
            return false;
        }
        int D = D();
        switch (D) {
            case 0:
                if (g == 1) {
                    new a().a("Sending device info update").a(w.d);
                    g = D;
                    if (s() < 14) {
                        new a(null, true).execute(new Void[0]);
                    } else {
                        new a(null, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                    }
                    return true;
                }
                break;
            case 1:
                if (g == 0) {
                    new a().a("Sending device info update").a(w.d);
                    g = D;
                    if (s() < 14) {
                        new a(null, true).execute(new Void[0]);
                    } else {
                        new a(null, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                    }
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject J() {
        JSONObject a2 = u.a();
        j a3 = a.a();
        u.a(a2, TapjoyConstants.TJC_CARRIER_NAME, i());
        u.a(a2, "data_path", a.a().o().e());
        u.b(a2, "device_api", s());
        u.b(a2, "display_width", q());
        u.b(a2, "display_height", r());
        u.b(a2, "screen_width", q());
        u.b(a2, "screen_height", r());
        u.b(a2, "display_dpi", E());
        u.a(a2, TapjoyConstants.TJC_DEVICE_TYPE_NAME, f());
        u.a(a2, "locale_language_code", v());
        u.a(a2, UserDataStore.LAST_NAME, v());
        u.a(a2, "locale_country_code", w());
        u.a(a2, "locale", w());
        u.a(a2, TapjoyConstants.TJC_DEVICE_MAC_ADDRESS, x());
        u.a(a2, "manufacturer", z());
        u.a(a2, "device_brand", z());
        u.a(a2, "media_path", a.a().o().d());
        u.a(a2, "temp_storage_path", a.a().o().f());
        u.b(a2, "memory_class", j());
        u.b(a2, "network_speed", 20);
        u.a(a2, "memory_used_mb", o());
        u.a(a2, "model", A());
        u.a(a2, "device_model", A());
        u.a(a2, TapjoyConstants.TJC_SDK_TYPE, this.i);
        u.a(a2, "sdk_version", F());
        u.a(a2, "network_type", a3.d.c());
        u.a(a2, TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, B());
        u.a(a2, "os_name", this.h);
        u.a(a2, TapjoyConstants.TJC_PLATFORM, this.h);
        u.a(a2, "arch", d());
        u.a(a2, "user_id", u.b(a3.d().d, "user_id"));
        u.a(a2, "app_id", a3.d().f716a);
        u.a(a2, "app_bundle_name", at.d());
        u.a(a2, "app_bundle_version", at.b());
        u.a(a2, "battery_level", t());
        u.a(a2, "cell_service_country_code", k());
        u.a(a2, "timezone_ietf", l());
        u.b(a2, "timezone_gmt_m", m());
        u.b(a2, "timezone_dst_m", n());
        u.a(a2, "launch_metadata", a());
        u.a(a2, "controller_version", a3.b());
        g = D();
        u.b(a2, "current_orientation", g);
        u.b(a2, "cleartext_permitted", C());
        u.a(a2, "density", (double) p());
        u.b(a2, "dark_mode", y());
        JSONArray b2 = u.b();
        if (at.a("com.android.vending")) {
            b2.put("google");
        }
        if (at.a("com.amazon.venezia")) {
            b2.put("amazon");
        }
        u.a(a2, "available_stores", b2);
        u.a(a2, NativeProtocol.RESULT_ARGS_PERMISSIONS, at.d(a.c()));
        int i2 = 40;
        while (!this.d && i2 > 0) {
            try {
                Thread.sleep(50);
                i2--;
            } catch (Exception unused) {
            }
        }
        u.a(a2, "advertiser_id", c());
        u.b(a2, "limit_tracking", g());
        if (c() == null || c().equals("")) {
            u.a(a2, "android_id_sha1", at.c(b()));
        }
        return a2;
    }
}
