package com.adcolony.sdk;

public abstract class AdColonyAdViewListener {

    /* renamed from: a reason: collision with root package name */
    String f715a = "";
    AdColonyAdSize b;
    ag c;

    public void onClicked(AdColonyAdView adColonyAdView) {
    }

    public void onClosed(AdColonyAdView adColonyAdView) {
    }

    public void onLeftApplication(AdColonyAdView adColonyAdView) {
    }

    public void onOpened(AdColonyAdView adColonyAdView) {
    }

    public abstract void onRequestFilled(AdColonyAdView adColonyAdView);

    public void onRequestNotFilled(AdColonyZone adColonyZone) {
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.f715a;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAdSize b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public ag c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void a(ag agVar) {
        this.c = agVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.f715a = str;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyAdSize adColonyAdSize) {
        this.b = adColonyAdSize;
    }
}
