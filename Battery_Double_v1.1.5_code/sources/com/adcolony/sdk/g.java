package com.adcolony.sdk;

class g {

    /* renamed from: a reason: collision with root package name */
    static final int f868a = 0;
    static final int b = 1;
    static final int c = 0;
    static final int d = 1;
    static final int e = 2;
    static final int f = 3;
    static final int g = 4;
    static final int h = 5;
    static final int i = 6;
    static final int j = 7;
    static final boolean k = false;
    static final int l = 1;
    static final int m = 2;

    static final class a {

        /* renamed from: a reason: collision with root package name */
        static final String f869a = "AdColony.get_app_info";
        static final String b = "AdColony.probe_launch_server";
        static final String c = "AdColony.send_custom_message";
        static final String d = "AdColony.v4vc_reward";
        static final String e = "AdColony.zone_info";
        static final String f = "AdColony.controller_version";
        static final String g = "AdColony.on_custom_message";
        static final String h = "AdColony.on_configured";
        static final String i = "AdColony.on_update";
        static final String j = "AdColony.on_install";
        static final String k = "AdColony.on_iap_report";
        static final String l = "AdColony.on_configuration_completed";

        a() {
        }
    }

    static final class aa {

        /* renamed from: a reason: collision with root package name */
        static final String f870a = "VideoView.create";
        static final String b = "VideoView.destroy";
        static final String c = "VideoView.play";
        static final String d = "VideoView.pause";
        static final String e = "VideoView.seek_to_time";
        static final String f = "VideoView.set_bounds";
        static final String g = "VideoView.set_visible";
        static final String h = "VideoView.set_volume";
        static final String i = "VideoView.on_progress";
        static final String j = "VideoView.on_error";
        static final String k = "VideoView.on_ready";

        aa() {
        }
    }

    static final class ab {

        /* renamed from: a reason: collision with root package name */
        static final String f871a = "WebServices.download";
        static final String b = "WebServices.get";
        static final String c = "WebServices.post";

        ab() {
        }
    }

    static final class ac {

        /* renamed from: a reason: collision with root package name */
        static final String f872a = "WebView.create";
        static final String b = "WebView.execute_js";
        static final String c = "WebView.destroy";
        static final String d = "WebView.prepare";
        static final String e = "WebView.set_bounds";
        static final String f = "WebView.set_visible";
        static final String g = "WebView.set_transparent";
        static final String h = "WebView.on_error";
        static final String i = "WebView.on_load";
        static final String j = "WebView.on_mraid";
        static final String k = "WebView.redirect_detected";
        static final String l = "WebView.on_first_click";

        ac() {
        }
    }

    static final class b {

        /* renamed from: a reason: collision with root package name */
        static final String f873a = "AdContainer.create";
        static final String b = "AdContainer.destroy";
        static final String c = "AdContainer.move_view_to_index";
        static final String d = "AdContainer.move_view_to_front";
        static final String e = "AdContainer.on_orientation_change";
        static final String f = "AdContainer.on_audio_change";
        static final String g = "AdContainer.on_touch_began";
        static final String h = "AdContainer.on_touch_moved";
        static final String i = "AdContainer.on_touch_ended";
        static final String j = "AdContainer.on_touch_cancelled";

        b() {
        }
    }

    static final class c {
        static final String A = "AdSession.on_ad_view_set_volume";
        static final String B = "AdSession.on_ad_view_destroyed";
        static final String C = "AdSession.on_native_ad_view_destroyed";
        static final String D = "AdSession.on_native_ad_view_set_volume";
        static final String E = "AdSession.on_native_ad_view_visible";
        static final String F = "AdSession.on_native_ad_view_hidden";
        static final String G = "AdSession.on_manual_pause";
        static final String H = "AdSession.on_manual_resume";
        static final String I = "AdSession.iap_event";

        /* renamed from: a reason: collision with root package name */
        static final String f874a = "AdSession.start_fullscreen_ad";
        static final String b = "AdSession.finish_fullscreen_ad";
        static final String c = "AdSession.ad_view_available";
        static final String d = "AdSession.ad_view_unavailable";
        static final String e = "AdSession.interstitial_available";
        static final String f = "AdSession.interstitial_unavailable";
        static final String g = "AdSession.expiring";
        static final String h = "AdSession.has_audio";
        static final String i = "AdSession.audio_stopped";
        static final String j = "AdSession.audio_started";
        static final String k = "AdSession.native_ad_view_available";
        static final String l = "AdSession.native_ad_view_unavailable";
        static final String m = "AdSession.native_ad_view_finished";
        static final String n = "AdSession.native_ad_view_started";
        static final String o = "AdSession.native_ad_muted";
        static final String p = "AdSession.destroy_native_ad_view";
        static final String q = "AdSession.expanded";
        static final String r = "AdSession.change_orientation";
        static final String s = "AdSession.on_back_button";
        static final String t = "AdSession.on_error";
        static final String u = "AdSession.on_close";
        static final String v = "AdSession.on_fullscreen_ad_started";
        static final String w = "AdSession.on_request";
        static final String x = "AdSession.on_request_close";
        static final String y = "AdSession.on_ad_view_visible";
        static final String z = "AdSession.on_ad_view_hidden";

        c() {
        }
    }

    static final class d {

        /* renamed from: a reason: collision with root package name */
        static final String f875a = "start";
        static final String b = "first_quartile";
        static final String c = "midpoint";
        static final String d = "third_quartile";
        static final String e = "complete";
        static final String f = "continue";
        static final String g = "in_video_engagement";
        static final String h = "html5_interaction";
        static final String i = "skip";
        static final String j = "cancel";
        static final String k = "sound_mute";
        static final String l = "sound_unmute";
        static final String m = "pause";
        static final String n = "resume";
        static final String o = "volume_change";
        static final String p = "buffer_start";
        static final String q = "buffer_end";

        d() {
        }
    }

    static final class e {

        /* renamed from: a reason: collision with root package name */
        static final String f876a = "Alert.show";

        e() {
        }
    }

    static final class f {

        /* renamed from: a reason: collision with root package name */
        static final String f877a = "AudioPlayer.create";
        static final String b = "AudioPlayer.destroy";
        static final String c = "AudioPlayer.pause";
        static final String d = "AudioPlayer.play";
        static final String e = "AudioPlayer.stop";
        static final String f = "AudioPlayer.on_error";
        static final String g = "AudioPlayer.on_interrupted";
        static final String h = "AudioPlayer.on_ready";
        static final String i = "AudioPlayer.on_ready_to_resume";

        f() {
        }
    }

    /* renamed from: com.adcolony.sdk.g$g reason: collision with other inner class name */
    static final class C0004g {

        /* renamed from: a reason: collision with root package name */
        static final String f878a = "ColorView.create";
        static final String b = "ColorView.destroy";
        static final String c = "ColorView.set_bounds";
        static final String d = "ColorView.set_visible";
        static final String e = "ColorView.set_color";

        C0004g() {
        }
    }

    static final class h {

        /* renamed from: a reason: collision with root package name */
        static final String f879a = "Crypto.crc32";
        static final String b = "Crypto.sha1";
        static final String c = "Crypto.uuid";

        h() {
        }
    }

    static final class i {

        /* renamed from: a reason: collision with root package name */
        static final String f880a = "CustomMessage.send";
        static final String b = "CustomMessage.native_send";
        static final String c = "CustomMessage.register";
        static final String d = "CustomMessage.unregister";
        static final String e = "CustomMessage.controller_send";
        static final String f = "iab_hook";
        static final String g = "open_hook";

        i() {
        }
    }

    static final class j {

        /* renamed from: a reason: collision with root package name */
        static final String f881a = "Device.get_info";
        static final String b = "Device.update_info";
        static final String c = "Device.query_advertiser_info";
        static final String d = "Device.application_exists";
        static final String e = "Device.on_battery_level_change";
        static final String f = "Device.on_battery_state_change";

        j() {
        }
    }

    static final class k {

        /* renamed from: a reason: collision with root package name */
        static final String f882a = "FileSystem.crc32";
        static final String b = "FileSystem.delete";
        static final String c = "FileSystem.exists";
        static final String d = "FileSystem.extract";
        static final String e = "FileSystem.listing";
        static final String f = "FileSystem.load";
        static final String g = "FileSystem.rename";
        static final String h = "FileSystem.save";
        static final String i = "FileSystem.unpack_bundle";
        static final String j = "422de421e0f4e019426b9abfd780746bc40740eb";
        static final String k = "FileSystem.create_directory";

        k() {
        }
    }

    static final class l {

        /* renamed from: a reason: collision with root package name */
        static final String f883a = "register_ad_view";
        static final String b = "end_session";
        static final String c = "record_ready";
        static final String d = "start_session";
        static final String e = "register_obstructions";
        static final String f = "inject_javascript";
        static final String g = "viewability_ad_event";
        static final String h = "verification_params";
        static final String i = "vendor_keys";

        l() {
        }
    }

    static final class m {

        /* renamed from: a reason: collision with root package name */
        static final String f884a = "ImageView.create";
        static final String b = "ImageView.destroy";
        static final String c = "ImageView.set_visible";
        static final String d = "ImageView.set_bounds";
        static final String e = "ImageView.set_image";

        m() {
        }
    }

    static final class n {

        /* renamed from: a reason: collision with root package name */
        static final int f885a = 0;
        static final int b = 1;
        static final int c = 0;
        static final int d = 1;
        static final int e = 2;
        static final String f = "ad_type";
        static final String g = "ad_unit_type";
        static final String h = "js_resources";
        static final String i = "video";
        static final String j = "display";
        static final String k = "banner_display";
        static final String l = "interstitial_display";
        static final String m = "skippable";
        static final String n = "skip_offset";
        static final String o = "video_duration";

        n() {
        }
    }

    static final class o {

        /* renamed from: a reason: collision with root package name */
        static final String f886a = "https://adc3-launch.adcolony.com/v4/launch";
        static final String b = "https://adc3-launch-staging.adcolony.com/v4/launch";

        o() {
        }
    }

    static final class p {

        /* renamed from: a reason: collision with root package name */
        static final String f887a = "Log.set_log_level";
        static final String b = "Log.public.info";
        static final String c = "Log.public.warning";
        static final String d = "Log.public.error";
        static final String e = "Log.public.trace";
        static final String f = "Log.private.info";
        static final String g = "Log.private.warning";
        static final String h = "Log.private.error";
        static final String i = "Log.private.trace";
        static final String j = "send_level";
        static final String k = "print_level";
        static final String l = "log_private";
        static final String m = "ADCLogError";

        p() {
        }
    }

    static final class q {

        /* renamed from: a reason: collision with root package name */
        static final String f888a = "MediaPool.cache";

        q() {
        }
    }

    static final class r {

        /* renamed from: a reason: collision with root package name */
        static final String f889a = "Module.load";
        static final String b = "Module.unload";

        r() {
        }
    }

    static final class s {

        /* renamed from: a reason: collision with root package name */
        static final String f890a = "MRAID.on_size_change";
        static final String b = "MRAID.on_close";
        static final String c = "MRAID.on_event";

        s() {
        }
    }

    static final class t {

        /* renamed from: a reason: collision with root package name */
        static final String f891a = "Network.on_status_change";

        t() {
        }
    }

    static final class u {

        /* renamed from: a reason: collision with root package name */
        static final String f892a = "Options.set_options";
        static final String b = "use_forced_controller";
        static final String c = "use_staging_launch_server";
        static final String d = "test_mode";
        static final String e = "mediation_network";
        static final String f = "mediation_network_version";
        static final String g = "plugin";
        static final String h = "plugin_version";
        static final String i = "keep_screen_on";

        u() {
        }
    }

    static final class v {

        /* renamed from: a reason: collision with root package name */
        static final String f893a = "RenderView.create";
        static final String b = "RenderView.destroy";
        static final String c = "RenderView.load_texture";
        static final String d = "RenderView.set_bounds";
        static final String e = "RenderView.set_visible";
        static final String f = "RenderView.create_image";

        v() {
        }
    }

    static final class w {

        /* renamed from: a reason: collision with root package name */
        static final String f894a = "SessionInfo.stopped";
        static final String b = "SessionInfo.on_start";
        static final String c = "SessionInfo.on_stop";
        static final String d = "SessionInfo.on_pause";
        static final String e = "SessionInfo.on_resume";
        static final String f = "from_window_focus";

        w() {
        }
    }

    static final class x {

        /* renamed from: a reason: collision with root package name */
        static final String f895a = "System.open_store";
        static final String b = "System.save_screenshot";
        static final String c = "System.telephone";
        static final String d = "System.sms";
        static final String e = "System.vibrate";
        static final String f = "System.open_browser";
        static final String g = "System.mail";
        static final String h = "System.launch_app";
        static final String i = "System.create_calendar_event";
        static final String j = "System.check_social_presence";
        static final String k = "System.social_post";
        static final String l = "System.check_app_presence";
        static final String m = "System.make_in_app_purchase";
        static final String n = "System.close";
        static final String o = "System.expand";
        static final String p = "System.use_custom_close";
        static final String q = "System.set_orientation_properties";

        x() {
        }
    }

    static final class y {

        /* renamed from: a reason: collision with root package name */
        static final String f896a = "TextView.align";
        static final String b = "TextView.create";
        static final String c = "TextView.destroy";
        static final String d = "TextView.set_text";
        static final String e = "TextView.get_text";
        static final String f = "TextView.set_bounds";
        static final String g = "TextView.set_visible";
        static final String h = "TextView.set_font_color";
        static final String i = "TextView.set_font_style";
        static final String j = "TextView.set_font_family";
        static final String k = "TextView.set_font_size";
        static final String l = "TextView.set_editable";
        static final String m = "TextView.set_background_color";
        static final String n = "TextView.set_typeface";

        y() {
        }
    }

    static final class z {

        /* renamed from: a reason: collision with root package name */
        static final int f897a = 4;
        static final int b = 16;
        static final int c = 60;

        z() {
        }
    }

    g() {
    }
}
