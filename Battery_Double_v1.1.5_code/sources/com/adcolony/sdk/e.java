package com.adcolony.sdk;

import java.util.HashMap;

class e {

    /* renamed from: a reason: collision with root package name */
    String f863a;
    private HashMap<Integer, Boolean> b = new HashMap<>();
    private an c;
    private aa d;
    private int e;

    e(String str, int i) {
        this.f863a = str;
        this.e = i;
    }

    /* access modifiers changed from: 0000 */
    public void a(ab abVar) {
        if (this.c == null) {
            this.c = new an(this.f863a, this.e);
            this.d = new aa(this.f863a, this.e);
        }
        int c2 = u.c(abVar.c(), "id");
        if (u.d(abVar.c(), "use_sound_pool")) {
            this.b.put(Integer.valueOf(c2), Boolean.valueOf(true));
            this.c.a(abVar);
            return;
        }
        this.b.put(Integer.valueOf(c2), Boolean.valueOf(false));
        this.d.a(abVar);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.c.a().autoPause();
        this.d.a();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.c.a().autoResume();
        this.d.b();
    }

    /* access modifiers changed from: 0000 */
    public void b(ab abVar) {
        if (((Boolean) this.b.get(Integer.valueOf(u.c(abVar.c(), "id")))).booleanValue()) {
            this.c.d(abVar);
        } else {
            this.d.b(abVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(ab abVar) {
        if (((Boolean) this.b.get(Integer.valueOf(u.c(abVar.c(), "id")))).booleanValue()) {
            this.c.c(abVar);
        } else {
            this.d.c(abVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(ab abVar) {
        if (((Boolean) this.b.get(Integer.valueOf(u.c(abVar.c(), "id")))).booleanValue()) {
            this.c.b(abVar);
        } else {
            this.d.d(abVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(ab abVar) {
        if (((Boolean) this.b.get(Integer.valueOf(u.c(abVar.c(), "id")))).booleanValue()) {
            this.c.e(abVar);
        } else {
            this.d.e(abVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public aa c() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public an d() {
        return this.c;
    }
}
