package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

class af {
    af() {
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"MissingPermission"})
    public boolean a() {
        NetworkInfo networkInfo;
        Context c = a.c();
        boolean z = false;
        if (c == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) c.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null) {
                networkInfo = null;
            } else {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo == null) {
                return false;
            }
            if (networkInfo.getType() == 1) {
                z = true;
            }
            return z;
        } catch (SecurityException e) {
            new a().a("SecurityException - please ensure you added the ").a("ACCESS_NETWORK_STATE permission: ").a(e.toString()).a(w.g);
            return false;
        } catch (Exception e2) {
            new a().a("Exception occurred when retrieving activeNetworkInfo in ").a("ADCNetwork.using_wifi(): ").a(e2.toString()).a(w.h);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"MissingPermission"})
    public boolean b() {
        NetworkInfo networkInfo;
        Context c = a.c();
        boolean z = false;
        if (c == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) c.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null) {
                networkInfo = null;
            } else {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo == null) {
                return false;
            }
            int type = networkInfo.getType();
            if (type == 0 || type >= 2) {
                z = true;
            }
            return z;
        } catch (SecurityException e) {
            new a().a("SecurityException - please ensure you added the ").a("ACCESS_NETWORK_STATE permission: ").a(e.toString()).a(w.g);
            return false;
        } catch (Exception e2) {
            new a().a("Exception occurred when retrieving activeNetworkInfo in ").a("ADCNetwork.using_mobile(): ").a(e2.toString()).a(w.h);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        if (a()) {
            return "wifi";
        }
        return b() ? "cell" : "none";
    }
}
