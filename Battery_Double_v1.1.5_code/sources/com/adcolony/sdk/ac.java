package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"UseSparseArrays"})
class ac {

    /* renamed from: a reason: collision with root package name */
    private ArrayList<ae> f729a = new ArrayList<>();
    private HashMap<Integer, ae> b = new HashMap<>();
    private int c = 2;
    private HashMap<String, ArrayList<ad>> d = new HashMap<>();
    private JSONArray e = u.b();
    /* access modifiers changed from: private */
    public int f = 1;

    ac() {
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, ad adVar) {
        ArrayList arrayList = (ArrayList) this.d.get(str);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.d.put(str, arrayList);
        }
        arrayList.add(adVar);
    }

    /* access modifiers changed from: 0000 */
    public void b(String str, ad adVar) {
        synchronized (this.d) {
            ArrayList arrayList = (ArrayList) this.d.get(str);
            if (arrayList != null) {
                arrayList.remove(adVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        j a2 = a.a();
        if (!a2.g() && !a2.h()) {
            final Context c2 = a.c();
            if (c2 != null) {
                at.a((Runnable) new Runnable() {
                    public void run() {
                        AdColonyAppOptions d = a.a().d();
                        d.e();
                        JSONObject d2 = d.d();
                        JSONObject a2 = u.a();
                        u.a(d2, "os_name", "android");
                        StringBuilder sb = new StringBuilder();
                        sb.append(a.a().o().g());
                        sb.append("7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5");
                        u.a(a2, "filepath", sb.toString());
                        u.a(a2, String.VIDEO_INFO, d2);
                        u.b(a2, "m_origin", 0);
                        u.b(a2, "m_id", ac.this.f = ac.this.f + 1);
                        u.a(a2, "m_type", "Controller.create");
                        try {
                            new av(c2, 1, false).a(true, new ab(a2));
                        } catch (RuntimeException e) {
                            a aVar = new a();
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(e.toString());
                            sb2.append(": during WebView initialization.");
                            aVar.a(sb2.toString()).a(" Disabling AdColony.").a(w.g);
                            AdColony.disable();
                        }
                    }
                });
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public ae a(ae aeVar) {
        synchronized (this.f729a) {
            int b2 = aeVar.b();
            if (b2 <= 0) {
                b2 = aeVar.a();
            }
            this.f729a.add(aeVar);
            this.b.put(Integer.valueOf(b2), aeVar);
        }
        return aeVar;
    }

    /* access modifiers changed from: 0000 */
    public ae a(int i) {
        synchronized (this.f729a) {
            ae aeVar = (ae) this.b.get(Integer.valueOf(i));
            if (aeVar == null) {
                return null;
            }
            this.f729a.remove(aeVar);
            this.b.remove(Integer.valueOf(i));
            aeVar.c();
            return aeVar;
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void b() {
        synchronized (this.f729a) {
            for (int size = this.f729a.size() - 1; size >= 0; size--) {
                ((ae) this.f729a.get(size)).d();
            }
        }
        JSONArray jSONArray = null;
        if (this.e.length() > 0) {
            jSONArray = this.e;
            this.e = u.b();
        }
        if (jSONArray != null) {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    final JSONObject jSONObject = jSONArray.getJSONObject(i);
                    final String string = jSONObject.getString("m_type");
                    if (jSONObject.getInt("m_origin") >= 2) {
                        at.a((Runnable) new Runnable() {
                            public void run() {
                                ac.this.a(string, jSONObject);
                            }
                        });
                    } else {
                        a(string, jSONObject);
                    }
                } catch (JSONException e2) {
                    new a().a("JSON error from message dispatcher's updateModules(): ").a(e2.toString()).a(w.h);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, JSONObject jSONObject) {
        synchronized (this.d) {
            ArrayList arrayList = (ArrayList) this.d.get(str);
            if (arrayList != null) {
                ab abVar = new ab(jSONObject);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    try {
                        ((ad) it.next()).a(abVar);
                    } catch (RuntimeException e2) {
                        new a().a((Object) e2).a(w.h);
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        try {
            if (!jSONObject.has("m_id")) {
                int i = this.f;
                this.f = i + 1;
                jSONObject.put("m_id", i);
            }
            if (!jSONObject.has("m_origin")) {
                jSONObject.put("m_origin", 0);
            }
            int i2 = jSONObject.getInt("m_target");
            if (i2 == 0) {
                synchronized (this) {
                    this.e.put(jSONObject);
                }
                return;
            }
            ae aeVar = (ae) this.b.get(Integer.valueOf(i2));
            if (aeVar != null) {
                aeVar.a(jSONObject);
            }
        } catch (JSONException e2) {
            new a().a("JSON error in ADCMessageDispatcher's sendMessage(): ").a(e2.toString()).a(w.h);
        }
    }

    /* access modifiers changed from: 0000 */
    public ArrayList<ae> c() {
        return this.f729a;
    }

    /* access modifiers changed from: 0000 */
    public int d() {
        int i = this.c;
        this.c = i + 1;
        return i;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, ae> e() {
        return this.b;
    }
}
