package com.ironsource.environment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings.System;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.WindowManager;
import com.facebook.places.model.PlaceFields;
import com.google.android.exoplayer2.util.MimeTypes;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import org.json.JSONObject;

public class DeviceStatus {
    private static final String CACHED_UUID_KEY = "cachedUUID";
    private static final String DEVICE_OS = "android";
    private static final String GOOGLE_PLAY_SERVICES_CLASS_NAME = "com.google.android.gms.ads.identifier.AdvertisingIdClient";
    private static final String GOOGLE_PLAY_SERVICES_GET_AID_INFO_METHOD_NAME = "getAdvertisingIdInfo";
    private static final String GOOGLE_PLAY_SERVICES_GET_AID_METHOD_NAME = "getId";
    private static final String GOOGLE_PLAY_SERVICES_IS_LIMITED_AD_TRACKING_METHOD_NAME = "isLimitAdTrackingEnabled";
    private static final String MEDIATION_SHARED_PREFS = "Mediation_Shared_Preferences";
    public static final String UUID_ENABLED = "uuidEnabled";
    private static String uniqueId;

    public static String getDeviceOs() {
        return "android";
    }

    public static long getDeviceLocalTime() {
        return Calendar.getInstance(TimeZone.getDefault()).getTime().getTime();
    }

    public static int getDeviceTimeZoneOffsetInMinutes() {
        return -(TimeZone.getDefault().getOffset(getDeviceLocalTime()) / 60000);
    }

    public static String[] getAdvertisingIdInfo(Context context) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class cls = Class.forName(GOOGLE_PLAY_SERVICES_CLASS_NAME);
        Object invoke = cls.getMethod(GOOGLE_PLAY_SERVICES_GET_AID_INFO_METHOD_NAME, new Class[]{Context.class}).invoke(cls, new Object[]{context});
        Method method = invoke.getClass().getMethod(GOOGLE_PLAY_SERVICES_GET_AID_METHOD_NAME, new Class[0]);
        Method method2 = invoke.getClass().getMethod("isLimitAdTrackingEnabled", new Class[0]);
        String obj = method.invoke(invoke, new Object[0]).toString();
        boolean booleanValue = ((Boolean) method2.invoke(invoke, new Object[0])).booleanValue();
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(booleanValue);
        return new String[]{obj, sb.toString()};
    }

    public static String getDeviceLanguage(Context context) throws Exception {
        return context.getResources().getConfiguration().locale.getLanguage();
    }

    private static long getFreeStorageInBytes(File file) {
        long j;
        StatFs statFs = new StatFs(file.getPath());
        if (VERSION.SDK_INT < 19) {
            j = ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } else {
            j = statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong();
        }
        return j / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    public static boolean isExternalMemoryAvailableWritable() {
        return "mounted".equals(Environment.getExternalStorageState()) && Environment.isExternalStorageRemovable();
    }

    public static String getMobileCarrier(Context context) {
        return ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getNetworkOperatorName();
    }

    public static String getAndroidOsVersion() {
        return VERSION.RELEASE;
    }

    public static int getAndroidAPIVersion() {
        return VERSION.SDK_INT;
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }

    public static String getDeviceOEM() {
        return Build.MANUFACTURER;
    }

    public static boolean isRootedDevice() {
        return findBinary("su");
    }

    private static boolean findBinary(String str) {
        String[] strArr;
        try {
            for (String str2 : new String[]{"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"}) {
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                sb.append(str);
                if (new File(sb.toString()).exists()) {
                    return true;
                }
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean isRTL(Context context) {
        return VERSION.SDK_INT >= 17 && context.getResources().getConfiguration().getLayoutDirection() == 1;
    }

    public static int getApplicationRotation(Context context) {
        return ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRotation();
    }

    public static float getSystemVolumePercent(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        return ((float) audioManager.getStreamVolume(3)) / ((float) audioManager.getStreamMaxVolume(3));
    }

    public static int getDisplayWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getDisplayHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int getDeviceWidth() {
        return getDisplayWidth();
    }

    public static int getDeviceHeight() {
        return getDisplayHeight();
    }

    public static int getActivityRequestedOrientation(Context context) {
        if (context instanceof Activity) {
            return ((Activity) context).getRequestedOrientation();
        }
        return -1;
    }

    public static int getDeviceDefaultOrientation(Context context) {
        int applicationRotation = getApplicationRotation(context);
        int deviceOrientation = getDeviceOrientation(context);
        return (((applicationRotation == 0 || applicationRotation == 2) && deviceOrientation == 2) || ((applicationRotation == 1 || applicationRotation == 3) && deviceOrientation == 1)) ? 2 : 1;
    }

    public static int getDeviceOrientation(Context context) {
        return context.getResources().getConfiguration().orientation;
    }

    public static float getDeviceDensity() {
        return Resources.getSystem().getDisplayMetrics().density;
    }

    public static List<ApplicationInfo> getInstalledApplications(Context context) {
        return context.getPackageManager().getInstalledApplications(0);
    }

    public static boolean isDeviceOrientationLocked(Context context) {
        return System.getInt(context.getContentResolver(), "accelerometer_rotation", 0) != 1;
    }

    public static File getExternalCacheDir(Context context) {
        return context.getExternalCacheDir();
    }

    public static String getInternalCacheDirPath(Context context) {
        File cacheDir = context.getCacheDir();
        if (cacheDir != null) {
            return cacheDir.getPath();
        }
        return null;
    }

    public static long getAvailableInternalMemorySizeInMegaBytes() {
        return getFreeStorageInBytes(Environment.getDataDirectory());
    }

    public static long getAvailableMemorySizeInMegaBytes(String str) {
        return getFreeStorageInBytes(new File(str));
    }

    public static long getAvailableExternalMemorySizeInMegaBytes() {
        if (isExternalMemoryAvailableWritable()) {
            return getFreeStorageInBytes(Environment.getExternalStorageDirectory());
        }
        return 0;
    }

    @TargetApi(19)
    public static boolean isImmersiveSupported(Activity activity) {
        int systemUiVisibility = activity.getWindow().getDecorView().getSystemUiVisibility();
        return (systemUiVisibility | 4096) == systemUiVisibility || (systemUiVisibility | 2048) == systemUiVisibility;
    }

    public static int getBatteryLevel(Context context) {
        try {
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = 0;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : 0;
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("scale", -1);
            }
            if (intExtra == -1 || i == -1) {
                return -1;
            }
            return (int) ((((float) intExtra) / ((float) i)) * 100.0f);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static synchronized String getOrGenerateOnceUniqueIdentifier(Context context) {
        synchronized (DeviceStatus.class) {
            if (!TextUtils.isEmpty(uniqueId)) {
                String str = uniqueId;
                return str;
            }
            try {
                SharedPreferences sharedPreferences = context.getSharedPreferences(MEDIATION_SHARED_PREFS, 0);
                if (sharedPreferences.getBoolean(UUID_ENABLED, true)) {
                    String string = sharedPreferences.getString(CACHED_UUID_KEY, "");
                    if (TextUtils.isEmpty(string)) {
                        uniqueId = UUID.randomUUID().toString();
                        Editor edit = sharedPreferences.edit();
                        edit.putString(CACHED_UUID_KEY, uniqueId);
                        edit.apply();
                    } else {
                        uniqueId = string;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            String str2 = uniqueId;
            return str2;
        }
    }

    private static boolean isSystemPackage(ResolveInfo resolveInfo) {
        return (resolveInfo.activityInfo.applicationInfo.flags & 1) != 0;
    }

    public static JSONObject getAppsInstallTime(Context context, boolean z) {
        Intent intent = new Intent("android.intent.action.MAIN", null);
        intent.addCategory("android.intent.category.LAUNCHER");
        List queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        JSONObject jSONObject = new JSONObject();
        PackageManager packageManager = context.getPackageManager();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        for (int i = 0; i < queryIntentActivities.size(); i++) {
            ResolveInfo resolveInfo = (ResolveInfo) queryIntentActivities.get(i);
            if (!z) {
                try {
                    if (isSystemPackage(resolveInfo)) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            String format = simpleDateFormat.format(new Date(packageManager.getPackageInfo(resolveInfo.activityInfo.packageName, 4096).firstInstallTime));
            jSONObject.put(format, jSONObject.optInt(format, 0) + 1);
        }
        return jSONObject;
    }
}
