package com.ironsource.environment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkStateReceiver extends BroadcastReceiver {
    private boolean mConnected;
    private NetworkStateReceiverListener mListener;
    private ConnectivityManager mManager;

    public interface NetworkStateReceiverListener {
        void onNetworkAvailabilityChanged(boolean z);
    }

    public NetworkStateReceiver(Context context, NetworkStateReceiverListener networkStateReceiverListener) {
        this.mListener = networkStateReceiverListener;
        this.mManager = (ConnectivityManager) context.getSystemService("connectivity");
        checkAndSetState();
    }

    public void onReceive(Context context, Intent intent) {
        if (!(intent == null || intent.getExtras() == null || !checkAndSetState())) {
            notifyState();
        }
    }

    private boolean checkAndSetState() {
        boolean z = this.mConnected;
        NetworkInfo activeNetworkInfo = this.mManager.getActiveNetworkInfo();
        this.mConnected = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        if (z != this.mConnected) {
            return true;
        }
        return false;
    }

    private void notifyState() {
        if (this.mListener == null) {
            return;
        }
        if (this.mConnected) {
            this.mListener.onNetworkAvailabilityChanged(true);
        } else {
            this.mListener.onNetworkAvailabilityChanged(false);
        }
    }
}
