package com.ironsource.sdk.precache;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.ironsource.sdk.data.SSAFile;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.Callable;

public class DownloadManager {
    public static final String CAMPAIGNS = "campaigns";
    public static final String FILE_ALREADY_EXIST = "file_already_exist";
    protected static final String FILE_NOT_FOUND_EXCEPTION = "file not found exception";
    public static final String GLOBAL_ASSETS = "globalAssets";
    protected static final String HTTP_EMPTY_RESPONSE = "http empty response";
    protected static final String HTTP_ERROR_CODE = "http error code";
    protected static final String HTTP_NOT_FOUND = "http not found";
    protected static final String HTTP_OK = "http ok";
    protected static final String IO_EXCEPTION = "io exception";
    protected static final String MALFORMED_URL_EXCEPTION = "malformed url exception";
    static final int MESSAGE_EMPTY_URL = 1007;
    static final int MESSAGE_FILE_DOWNLOAD_FAIL = 1017;
    static final int MESSAGE_FILE_DOWNLOAD_SUCCESS = 1016;
    static final int MESSAGE_FILE_NOT_FOUND_EXCEPTION = 1018;
    static final int MESSAGE_GENERAL_HTTP_ERROR_CODE = 1011;
    static final int MESSAGE_HTTP_EMPTY_RESPONSE = 1006;
    static final int MESSAGE_HTTP_NOT_FOUND = 1005;
    static final int MESSAGE_INIT_BC_FAIL = 1014;
    static final int MESSAGE_IO_EXCEPTION = 1009;
    static final int MESSAGE_MALFORMED_URL_EXCEPTION = 1004;
    static final int MESSAGE_NUM_OF_BANNERS_TO_CACHE = 1013;
    static final int MESSAGE_NUM_OF_BANNERS_TO_INIT_SUCCESS = 1012;
    static final int MESSAGE_OUT_OF_MEMORY_EXCEPTION = 1019;
    static final int MESSAGE_SOCKET_TIMEOUT_EXCEPTION = 1008;
    static final int MESSAGE_TMP_FILE_RENAME_FAILED = 1020;
    static final int MESSAGE_URI_SYNTAX_EXCEPTION = 1010;
    static final int MESSAGE_ZERO_CAMPAIGNS_TO_INIT_SUCCESS = 1015;
    public static final String NO_DISK_SPACE = "no_disk_space";
    public static final String NO_NETWORK_CONNECTION = "no_network_connection";
    public static final int OPERATION_TIMEOUT = 5000;
    protected static final String OUT_OF_MEMORY_EXCEPTION = "out of memory exception";
    public static final String SETTINGS = "settings";
    protected static final String SOCKET_TIMEOUT_EXCEPTION = "socket timeout exception";
    public static final String STORAGE_UNAVAILABLE = "sotrage_unavailable";
    private static final String TAG = "DownloadManager";
    private static final String TEMP_DIR_FOR_FILES = "temp";
    private static final String TEMP_PREFIX_FOR_FILES = "tmp_";
    private static final String UNABLE_TO_CREATE_FOLDER = "unable_to_create_folder";
    protected static final String URI_SYNTAX_EXCEPTION = "uri syntax exception";
    public static final String UTF8_CHARSET = "UTF-8";
    private static DownloadManager mDownloadManager;
    private String mCacheRootDirectory;
    private DownloadHandler mDownloadHandler = getDownloadHandler();
    private Thread mMobileControllerThread;

    static class DownloadHandler extends Handler {
        OnPreCacheCompletion mListener;

        DownloadHandler() {
        }

        /* access modifiers changed from: 0000 */
        public void setOnPreCacheCompletion(OnPreCacheCompletion onPreCacheCompletion) {
            if (onPreCacheCompletion != null) {
                this.mListener = onPreCacheCompletion;
                return;
            }
            throw new IllegalArgumentException();
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1016:
                    this.mListener.onFileDownloadSuccess((SSAFile) message.obj);
                    return;
                case 1017:
                    this.mListener.onFileDownloadFail((SSAFile) message.obj);
                    return;
                default:
                    return;
            }
        }

        public void release() {
            this.mListener = null;
        }
    }

    static class FileWorkerThread implements Callable<Result> {
        private long mConnectionRetries;
        private String mDirectory;
        private String mFileName;
        private String mFileUrl;
        private String mTmpFilesDirectory;

        public FileWorkerThread(String str, String str2, String str3, long j, String str4) {
            this.mFileUrl = str;
            this.mDirectory = str2;
            this.mFileName = str3;
            this.mConnectionRetries = j;
            this.mTmpFilesDirectory = str4;
        }

        /* access modifiers changed from: 0000 */
        public int saveFile(byte[] bArr, String str) throws Exception {
            return IronSourceStorageUtils.saveFile(bArr, str);
        }

        /* access modifiers changed from: 0000 */
        public boolean renameFile(String str, String str2) throws Exception {
            return IronSourceStorageUtils.renameFile(str, str2);
        }

        /* access modifiers changed from: 0000 */
        public byte[] getBytes(InputStream inputStream) throws IOException {
            return DownloadManager.getBytes(inputStream);
        }

        public Result call() {
            if (this.mConnectionRetries == 0) {
                this.mConnectionRetries = 1;
            }
            Result result = null;
            for (int i = 0; ((long) i) < this.mConnectionRetries; i++) {
                result = downloadContent(this.mFileUrl, i);
                int i2 = result.responseCode;
                if (i2 != 1008 && i2 != 1009) {
                    break;
                }
            }
            if (!(result == null || result.body == null)) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.mDirectory);
                sb.append(File.separator);
                sb.append(this.mFileName);
                String sb2 = sb.toString();
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.mTmpFilesDirectory);
                sb3.append(File.separator);
                sb3.append(DownloadManager.TEMP_PREFIX_FOR_FILES);
                sb3.append(this.mFileName);
                String sb4 = sb3.toString();
                try {
                    if (saveFile(result.body, sb4) == 0) {
                        result.responseCode = 1006;
                    } else if (!renameFile(sb4, sb2)) {
                        result.responseCode = 1020;
                    }
                } catch (FileNotFoundException unused) {
                    result.responseCode = 1018;
                } catch (Exception e) {
                    if (!TextUtils.isEmpty(e.getMessage())) {
                        Logger.i(DownloadManager.TAG, e.getMessage());
                    }
                    result.responseCode = 1009;
                } catch (Error e2) {
                    if (!TextUtils.isEmpty(e2.getMessage())) {
                        Logger.i(DownloadManager.TAG, e2.getMessage());
                    }
                    result.responseCode = 1019;
                }
            }
            return result;
        }

        /* access modifiers changed from: 0000 */
        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0122, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x0125, code lost:
            if (r3 == null) goto L_0x015a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:108:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x0131, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x0132, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x0135, code lost:
            if (r3 == null) goto L_0x015a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x0141, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x0142, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:121:0x0145, code lost:
            if (r3 == null) goto L_0x015a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x0151, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:130:0x0152, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:131:0x0155, code lost:
            if (r3 == null) goto L_0x015a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x0157, code lost:
            r3.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x015a, code lost:
            r0.url = r8;
            r0.responseCode = r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0065, code lost:
            r9 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0066, code lost:
            r2 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0069, code lost:
            r9 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x006a, code lost:
            r2 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ac, code lost:
            r9 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x00c1, code lost:
            com.ironsource.sdk.utils.Logger.i(com.ironsource.sdk.precache.DownloadManager.TAG, r9.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x00d0, code lost:
            r9 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x00d1, code lost:
            r9.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x00d6, code lost:
            r3.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x00eb, code lost:
            com.ironsource.sdk.utils.Logger.i(com.ironsource.sdk.precache.DownloadManager.TAG, r9.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x00fc, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x00fd, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x0100, code lost:
            if (r3 != null) goto L_0x0157;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x0121, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:105:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), PHI: r1 
  PHI: (r1v18 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:107:0x012d A[SYNTHETIC, Splitter:B:107:0x012d] */
        /* JADX WARNING: Removed duplicated region for block: B:115:? A[ExcHandler: URISyntaxException (unused java.net.URISyntaxException), PHI: r1 
  PHI: (r1v17 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:117:0x013d A[SYNTHETIC, Splitter:B:117:0x013d] */
        /* JADX WARNING: Removed duplicated region for block: B:125:? A[ExcHandler: MalformedURLException (unused java.net.MalformedURLException), PHI: r1 
  PHI: (r1v16 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:127:0x014d A[SYNTHETIC, Splitter:B:127:0x014d] */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x00ac A[ExcHandler: Error (e java.lang.Error), PHI: r1 
  PHI: (r1v20 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x00c1 A[Catch:{ all -> 0x0103 }] */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x00cc A[SYNTHETIC, Splitter:B:62:0x00cc] */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x00d6  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x00eb A[Catch:{ all -> 0x0103 }] */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x00f8 A[SYNTHETIC, Splitter:B:77:0x00f8] */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x0106 A[SYNTHETIC, Splitter:B:84:0x0106] */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0110  */
        /* JADX WARNING: Removed duplicated region for block: B:95:? A[ExcHandler: FileNotFoundException (unused java.io.FileNotFoundException), PHI: r1 
  PHI: (r1v19 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v21 java.io.InputStream), (r1v21 java.io.InputStream), (r1v1 java.io.InputStream) binds: [B:8:0x0024, B:40:0x0073, B:41:?, B:14:0x003f] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:8:0x0024] */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x011d A[SYNTHETIC, Splitter:B:97:0x011d] */
        public Result downloadContent(String str, int i) {
            int i2;
            HttpURLConnection httpURLConnection;
            Result result = new Result();
            if (TextUtils.isEmpty(str)) {
                result.url = str;
                result.responseCode = 1007;
                return result;
            }
            InputStream inputStream = null;
            int i3 = 0;
            try {
                URL url = new URL(str);
                url.toURI();
                httpURLConnection = (HttpURLConnection) url.openConnection();
                try {
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                    httpURLConnection.setConnectTimeout(5000);
                    httpURLConnection.setReadTimeout(5000);
                    httpURLConnection.connect();
                    int responseCode = httpURLConnection.getResponseCode();
                    if (responseCode < 200 || responseCode >= 400) {
                        responseCode = 1011;
                    } else {
                        InputStream inputStream2 = httpURLConnection.getInputStream();
                        try {
                            result.body = getBytes(inputStream2);
                            inputStream = inputStream2;
                        } catch (MalformedURLException unused) {
                            inputStream = inputStream2;
                            i2 = 1004;
                            if (inputStream != null) {
                            }
                        } catch (URISyntaxException unused2) {
                            inputStream = inputStream2;
                            i2 = 1010;
                            if (inputStream != null) {
                            }
                        } catch (SocketTimeoutException unused3) {
                            inputStream = inputStream2;
                            i2 = 1008;
                            if (inputStream != null) {
                            }
                        } catch (FileNotFoundException unused4) {
                            inputStream = inputStream2;
                            i2 = 1018;
                            if (inputStream != null) {
                            }
                        } catch (Exception e) {
                            e = e;
                            i3 = responseCode;
                            inputStream = inputStream2;
                            if (!TextUtils.isEmpty(e.getMessage())) {
                            }
                            i2 = 1009;
                            if (inputStream != null) {
                            }
                        } catch (Error e2) {
                            e = e2;
                            inputStream = inputStream2;
                            i3 = 1019;
                            try {
                                if (!TextUtils.isEmpty(e.getMessage())) {
                                }
                                if (inputStream != null) {
                                }
                                if (httpURLConnection != null) {
                                }
                                result.url = str;
                                result.responseCode = 1019;
                                return result;
                            } catch (Throwable th) {
                                th = th;
                                if (inputStream != null) {
                                    try {
                                        inputStream.close();
                                    } catch (IOException e3) {
                                        e3.printStackTrace();
                                    }
                                }
                                if (httpURLConnection != null) {
                                    httpURLConnection.disconnect();
                                }
                                result.url = str;
                                result.responseCode = i3;
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            i3 = responseCode;
                            inputStream = inputStream2;
                            if (inputStream != null) {
                            }
                            if (httpURLConnection != null) {
                            }
                            result.url = str;
                            result.responseCode = i3;
                            throw th;
                        }
                    }
                    if (responseCode != 200) {
                        String str2 = DownloadManager.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append(" RESPONSE CODE: ");
                        sb.append(responseCode);
                        sb.append(" URL: ");
                        sb.append(str);
                        sb.append(" ATTEMPT: ");
                        sb.append(i);
                        Logger.i(str2, sb.toString());
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                    }
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    result.url = str;
                    result.responseCode = responseCode;
                } catch (MalformedURLException unused5) {
                } catch (URISyntaxException unused6) {
                } catch (SocketTimeoutException unused7) {
                } catch (FileNotFoundException unused8) {
                } catch (Exception e5) {
                    e = e5;
                    if (!TextUtils.isEmpty(e.getMessage())) {
                    }
                    i2 = 1009;
                    if (inputStream != null) {
                    }
                } catch (Error e6) {
                }
            } catch (MalformedURLException unused9) {
                httpURLConnection = null;
                i2 = 1004;
                if (inputStream != null) {
                }
            } catch (URISyntaxException unused10) {
                httpURLConnection = null;
                i2 = 1010;
                if (inputStream != null) {
                }
            } catch (SocketTimeoutException unused11) {
                httpURLConnection = null;
                i2 = 1008;
                if (inputStream != null) {
                }
            } catch (FileNotFoundException unused12) {
                httpURLConnection = null;
                i2 = 1018;
                if (inputStream != null) {
                }
            } catch (Exception e7) {
                e = e7;
                httpURLConnection = null;
                if (!TextUtils.isEmpty(e.getMessage())) {
                }
                i2 = 1009;
                if (inputStream != null) {
                }
            } catch (Error e8) {
                e = e8;
                httpURLConnection = null;
                i3 = 1019;
                if (!TextUtils.isEmpty(e.getMessage())) {
                }
                if (inputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                result.url = str;
                result.responseCode = 1019;
                return result;
            } catch (Throwable th3) {
                th = th3;
                httpURLConnection = null;
                if (inputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                result.url = str;
                result.responseCode = i3;
                throw th;
            }
            return result;
        }
    }

    public interface OnPreCacheCompletion {
        void onFileDownloadFail(SSAFile sSAFile);

        void onFileDownloadSuccess(SSAFile sSAFile);
    }

    static class Result {
        byte[] body;
        int responseCode;
        public String url;

        Result() {
        }
    }

    static class SingleFileWorkerThread implements Runnable {
        private String mCacheRootDirectory;
        private long mConnectionRetries = getConnectionRetries();
        Handler mDownloadHandler;
        private String mFile;
        private String mFileName = guessFileName(this.mFile);
        private String mPath;
        private final String mTempFilesDirectory;

        SingleFileWorkerThread(SSAFile sSAFile, Handler handler, String str, String str2) {
            this.mFile = sSAFile.getFile();
            this.mPath = sSAFile.getPath();
            this.mCacheRootDirectory = str;
            this.mDownloadHandler = handler;
            this.mTempFilesDirectory = str2;
        }

        /* access modifiers changed from: 0000 */
        public String guessFileName(String str) {
            return SDKUtils.getFileName(this.mFile);
        }

        /* access modifiers changed from: 0000 */
        public FileWorkerThread getFileWorkerThread(String str, String str2, String str3, long j, String str4) {
            FileWorkerThread fileWorkerThread = new FileWorkerThread(str, str2, str3, j, str4);
            return fileWorkerThread;
        }

        /* access modifiers changed from: 0000 */
        public Message getMessage() {
            return new Message();
        }

        /* access modifiers changed from: 0000 */
        public String makeDir(String str, String str2) {
            return IronSourceStorageUtils.makeDir(str, str2);
        }

        public void run() {
            SSAFile sSAFile = new SSAFile(this.mFileName, this.mPath);
            Message message = getMessage();
            message.obj = sSAFile;
            String makeDir = makeDir(this.mCacheRootDirectory, this.mPath);
            if (makeDir == null) {
                message.what = 1017;
                sSAFile.setErrMsg(DownloadManager.UNABLE_TO_CREATE_FOLDER);
                this.mDownloadHandler.sendMessage(message);
                return;
            }
            int i = getFileWorkerThread(this.mFile, makeDir, sSAFile.getFile(), this.mConnectionRetries, this.mTempFilesDirectory).call().responseCode;
            if (i != 200) {
                if (i != 404) {
                    switch (i) {
                        case 1004:
                        case 1005:
                        case 1006:
                            break;
                        default:
                            switch (i) {
                                case 1008:
                                case 1009:
                                case 1010:
                                case 1011:
                                    break;
                                default:
                                    switch (i) {
                                        case 1018:
                                        case 1019:
                                            break;
                                        default:
                                            return;
                                    }
                            }
                    }
                }
                String convertErrorCodeToMessage = convertErrorCodeToMessage(i);
                message.what = 1017;
                sSAFile.setErrMsg(convertErrorCodeToMessage);
                this.mDownloadHandler.sendMessage(message);
                return;
            }
            message.what = 1016;
            this.mDownloadHandler.sendMessage(message);
        }

        /* access modifiers changed from: 0000 */
        public String convertErrorCodeToMessage(int i) {
            StringBuilder sb = new StringBuilder();
            sb.append("not defined message for ");
            sb.append(i);
            String sb2 = sb.toString();
            if (i != 404) {
                switch (i) {
                    case 1004:
                        return DownloadManager.MALFORMED_URL_EXCEPTION;
                    case 1005:
                        break;
                    case 1006:
                        return DownloadManager.HTTP_EMPTY_RESPONSE;
                    default:
                        switch (i) {
                            case 1008:
                                return DownloadManager.SOCKET_TIMEOUT_EXCEPTION;
                            case 1009:
                                return DownloadManager.IO_EXCEPTION;
                            case 1010:
                                return DownloadManager.URI_SYNTAX_EXCEPTION;
                            case 1011:
                                return DownloadManager.HTTP_ERROR_CODE;
                            default:
                                switch (i) {
                                    case 1018:
                                        return DownloadManager.FILE_NOT_FOUND_EXCEPTION;
                                    case 1019:
                                        return DownloadManager.OUT_OF_MEMORY_EXCEPTION;
                                    default:
                                        return sb2;
                                }
                        }
                }
            }
            return DownloadManager.HTTP_NOT_FOUND;
        }

        public long getConnectionRetries() {
            return Long.parseLong(IronSourceSharedPrefHelper.getSupersonicPrefHelper().getConnectionRetries());
        }
    }

    private DownloadManager(String str) {
        this.mCacheRootDirectory = str;
        IronSourceStorageUtils.deleteFolder(this.mCacheRootDirectory, TEMP_DIR_FOR_FILES);
        IronSourceStorageUtils.makeDir(this.mCacheRootDirectory, TEMP_DIR_FOR_FILES);
    }

    public static synchronized DownloadManager getInstance(String str) {
        DownloadManager downloadManager;
        synchronized (DownloadManager.class) {
            if (mDownloadManager == null) {
                mDownloadManager = new DownloadManager(str);
            }
            downloadManager = mDownloadManager;
        }
        return downloadManager;
    }

    /* access modifiers changed from: 0000 */
    public DownloadHandler getDownloadHandler() {
        return new DownloadHandler();
    }

    public void setOnPreCacheCompletion(OnPreCacheCompletion onPreCacheCompletion) {
        this.mDownloadHandler.setOnPreCacheCompletion(onPreCacheCompletion);
    }

    public void release() {
        mDownloadManager = null;
        this.mDownloadHandler.release();
        this.mDownloadHandler = null;
    }

    public void downloadFile(SSAFile sSAFile) {
        new Thread(new SingleFileWorkerThread(sSAFile, this.mDownloadHandler, this.mCacheRootDirectory, getTempFilesDirectory())).start();
    }

    public void downloadMobileControllerFile(SSAFile sSAFile) {
        this.mMobileControllerThread = new Thread(new SingleFileWorkerThread(sSAFile, this.mDownloadHandler, this.mCacheRootDirectory, getTempFilesDirectory()));
        this.mMobileControllerThread.start();
    }

    public boolean isMobileControllerThreadLive() {
        return this.mMobileControllerThread != null && this.mMobileControllerThread.isAlive();
    }

    /* access modifiers changed from: 0000 */
    public String getTempFilesDirectory() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.mCacheRootDirectory);
        sb.append(File.separator);
        sb.append(TEMP_DIR_FOR_FILES);
        return sb.toString();
    }

    static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[8192];
        while (true) {
            int read = inputStream.read(bArr, 0, bArr.length);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }
}
