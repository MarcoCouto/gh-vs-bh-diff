package com.ironsource.sdk.utils;

import android.os.AsyncTask;
import android.support.graphics.drawable.PathInterpolatorCompat;
import java.net.HttpURLConnection;
import java.net.URL;

public class IronSourceAsyncHttpRequestTask extends AsyncTask<String, Integer, Integer> {
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a  */
    public Integer doInBackground(String... strArr) {
        HttpURLConnection httpURLConnection = null;
        try {
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(strArr[0]).openConnection();
            try {
                httpURLConnection2.setConnectTimeout(PathInterpolatorCompat.MAX_NUM_POINTS);
                httpURLConnection2.getInputStream();
                if (httpURLConnection2 != null) {
                    httpURLConnection2.disconnect();
                }
            } catch (Exception e) {
                Exception exc = e;
                httpURLConnection = httpURLConnection2;
                e = exc;
                try {
                    e.printStackTrace();
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    return Integer.valueOf(1);
                } catch (Throwable th) {
                    th = th;
                    if (httpURLConnection != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                httpURLConnection = httpURLConnection2;
                th = th3;
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (httpURLConnection != null) {
            }
            return Integer.valueOf(1);
        }
        return Integer.valueOf(1);
    }
}
