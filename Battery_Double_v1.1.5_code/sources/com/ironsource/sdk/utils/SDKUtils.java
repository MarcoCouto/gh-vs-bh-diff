package com.ironsource.sdk.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import com.applovin.sdk.AppLovinEventParameters;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.data.SSAEnums.ProductType;
import com.ironsource.sdk.data.SSAObj;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.unity3d.ads.metadata.InAppPurchaseMetaData;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPInputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SDKUtils {
    private static final String TAG = "SDKUtils";
    private static String mAdvertisingId = null;
    private static String mControllerConfig = null;
    private static String mControllerUrl = null;
    private static int mDebugMode = 0;
    private static Map<String, String> mInitSDKParams = null;
    private static boolean mIsLimitedTrackingEnabled = true;
    private static String mUserGroup = "";
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    public static String getSDKVersion() {
        return Constants.SDK_VERSION;
    }

    public static String translateOrientation(int i) {
        String str = "none";
        switch (i) {
            case 1:
                return "portrait";
            case 2:
                return "landscape";
            default:
                return str;
        }
    }

    public static String translateRequestedOrientation(int i) {
        String str = "none";
        switch (i) {
            case 0:
            case 6:
            case 8:
            case 11:
                return "landscape";
            case 1:
            case 7:
            case 9:
            case 12:
                return "portrait";
            default:
                return str;
        }
    }

    public static String getFileName(String str) {
        String[] split = str.split(File.separator);
        try {
            return URLEncoder.encode(split[split.length - 1].split("\\?")[0], "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int pxToDp(long j) {
        return (int) ((((float) j) / Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    public static int dpToPx(long j) {
        return (int) ((((float) j) * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    public static int convertPxToDp(int i) {
        return (int) TypedValue.applyDimension(1, (float) i, Resources.getSystem().getDisplayMetrics());
    }

    public static int convertDpToPx(int i) {
        return (int) TypedValue.applyDimension(0, (float) i, Resources.getSystem().getDisplayMetrics());
    }

    public static JSONObject getOrientation(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("orientation", translateOrientation(DeviceStatus.getDeviceOrientation(context)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    public static String createErrorMessage(String str, String str2) {
        return String.format("%s Failure occurred during initiation at: %s", new Object[]{str, str2});
    }

    public static Long getCurrentTimeMillis() {
        return Long.valueOf(System.currentTimeMillis());
    }

    public static boolean isApplicationVisible(Context context) {
        String packageName = context.getPackageName();
        for (RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (runningAppProcessInfo.processName.equalsIgnoreCase(packageName) && runningAppProcessInfo.importance == 100) {
                return true;
            }
        }
        return false;
    }

    public static void showNoInternetDialog(Context context) {
        new Builder(context).setMessage("No Internet Connection").setPositiveButton("Ok", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[RETURN] */
    public static byte[] encrypt(String str) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
            try {
                messageDigest.reset();
                messageDigest.update(str.getBytes("UTF-8"));
            } catch (NoSuchAlgorithmException e) {
                e = e;
            } catch (UnsupportedEncodingException e2) {
                e = e2;
                e.printStackTrace();
                if (messageDigest == null) {
                }
            }
        } catch (NoSuchAlgorithmException e3) {
            e = e3;
            messageDigest = null;
            e.printStackTrace();
            if (messageDigest == null) {
            }
        } catch (UnsupportedEncodingException e4) {
            e = e4;
            messageDigest = null;
            e.printStackTrace();
            if (messageDigest == null) {
            }
        }
        if (messageDigest == null) {
            return messageDigest.digest();
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0063  */
    public static String convertStreamToString(InputStream inputStream, boolean z, String str, String str2) throws IOException {
        BufferedReader bufferedReader;
        Throwable th;
        InputStream gZIPInputStream = z ? new GZIPInputStream(inputStream) : inputStream;
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(str, str2)));
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(gZIPInputStream, "UTF-8"));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    sb.append("\n");
                }
                bufferedWriter.write(sb.toString());
                String sb2 = sb.toString();
                bufferedReader.close();
                gZIPInputStream.close();
                if (z) {
                    inputStream.close();
                }
                bufferedWriter.close();
                return sb2;
            } catch (Throwable th2) {
                th = th2;
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                gZIPInputStream.close();
                if (z) {
                    inputStream.close();
                }
                bufferedWriter.close();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            bufferedReader = null;
            if (bufferedReader != null) {
            }
            gZIPInputStream.close();
            if (z) {
            }
            bufferedWriter.close();
            throw th;
        }
    }

    public static String encodeString(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException unused) {
            return "";
        }
    }

    public static String decodeString(String str) {
        String str2 = "";
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            String str3 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed decoding string ");
            sb.append(e.getMessage());
            Logger.d(str3, sb.toString());
            return str2;
        }
    }

    public static String getMD5(String str) {
        try {
            String bigInteger = new BigInteger(1, MessageDigest.getInstance(CommonMD5.TAG).digest(str.getBytes())).toString(16);
            while (bigInteger.length() < 32) {
                StringBuilder sb = new StringBuilder();
                sb.append("0");
                sb.append(bigInteger);
                bigInteger = sb.toString();
            }
            return bigInteger;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static void loadGoogleAdvertiserInfo(Context context) {
        try {
            String[] advertisingIdInfo = DeviceStatus.getAdvertisingIdInfo(context);
            mAdvertisingId = advertisingIdInfo[0];
            mIsLimitedTrackingEnabled = Boolean.valueOf(advertisingIdInfo[1]).booleanValue();
        } catch (Exception e) {
            if (e.getMessage() != null) {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getMessage());
                Logger.i(str, sb.toString());
            }
            if (e.getCause() != null) {
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(e.getClass().getSimpleName());
                sb2.append(": ");
                sb2.append(e.getCause());
                Logger.i(str2, sb2.toString());
            }
        }
    }

    public static String getAdvertiserId() {
        return mAdvertisingId;
    }

    public static boolean isLimitAdTrackingEnabled() {
        return mIsLimitedTrackingEnabled;
    }

    public static Object getIInAppBillingServiceClass(IBinder iBinder) {
        StringBuilder sb;
        String str;
        try {
            return Class.forName("com.android.vending.billing.IInAppBillingService$Stub").getMethod("asInterface", new Class[]{IBinder.class}).invoke(null, new Object[]{iBinder});
        } catch (ClassNotFoundException e) {
            e = e;
            if (e.getMessage() != null) {
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(e.getClass().getSimpleName());
                sb2.append(": ");
                sb2.append(e.getMessage());
                Logger.i(str2, sb2.toString());
            }
            if (e.getCause() != null) {
                str = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str, sb.toString());
            }
            return null;
        } catch (NoSuchMethodException e2) {
            e = e2;
            if (e.getMessage() != null) {
                String str3 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(e.getClass().getSimpleName());
                sb3.append(": ");
                sb3.append(e.getMessage());
                Logger.i(str3, sb3.toString());
            }
            if (e.getCause() != null) {
                str = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str, sb.toString());
            }
            return null;
        } catch (IllegalAccessException e3) {
            e = e3;
            if (e.getMessage() != null) {
                String str4 = TAG;
                StringBuilder sb4 = new StringBuilder();
                sb4.append(e.getClass().getSimpleName());
                sb4.append(": ");
                sb4.append(e.getMessage());
                Logger.i(str4, sb4.toString());
            }
            if (e.getCause() != null) {
                str = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str, sb.toString());
            }
            return null;
        } catch (IllegalArgumentException e4) {
            e = e4;
            if (e.getMessage() != null) {
                String str5 = TAG;
                StringBuilder sb5 = new StringBuilder();
                sb5.append(e.getClass().getSimpleName());
                sb5.append(": ");
                sb5.append(e.getMessage());
                Logger.i(str5, sb5.toString());
            }
            if (e.getCause() != null) {
                str = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str, sb.toString());
            }
            return null;
        } catch (InvocationTargetException e5) {
            e = e5;
            if (e.getMessage() != null) {
                String str6 = TAG;
                StringBuilder sb6 = new StringBuilder();
                sb6.append(e.getClass().getSimpleName());
                sb6.append(": ");
                sb6.append(e.getMessage());
                Logger.i(str6, sb6.toString());
            }
            if (e.getCause() != null) {
                str = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str, sb.toString());
            }
            return null;
        }
    }

    public static String queryingPurchasedItems(Object obj, String str) {
        String str2;
        StringBuilder sb;
        JSONArray jSONArray = new JSONArray();
        try {
            Object invoke = obj.getClass().getMethod("getPurchases", new Class[]{Integer.TYPE, String.class, String.class, String.class}).invoke(obj, new Object[]{Integer.valueOf(3), str, "inapp", null});
            Method method = invoke.getClass().getMethod("getInt", new Class[]{String.class});
            Method method2 = invoke.getClass().getMethod("getStringArrayList", new Class[]{String.class});
            Method method3 = invoke.getClass().getMethod("getString", new Class[]{String.class});
            if (((Integer) method.invoke(invoke, new Object[]{"RESPONSE_CODE"})).intValue() == 0) {
                ArrayList arrayList = (ArrayList) method2.invoke(invoke, new Object[]{"INAPP_PURCHASE_ITEM_LIST"});
                ArrayList arrayList2 = (ArrayList) method2.invoke(invoke, new Object[]{"INAPP_PURCHASE_DATA_LIST"});
                ArrayList arrayList3 = (ArrayList) method2.invoke(invoke, new Object[]{"INAPP_DATA_SIGNATURE_LIST"});
                String str3 = (String) method3.invoke(invoke, new Object[]{"INAPP_CONTINUATION_TOKEN"});
                for (int i = 0; i < arrayList2.size(); i++) {
                    String str4 = (String) arrayList2.get(i);
                    String str5 = (String) arrayList3.get(i);
                    String str6 = (String) arrayList.get(i);
                    Logger.i(TAG, str4);
                    Logger.i(TAG, str5);
                    Logger.i(TAG, str6);
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put("purchaseData", str4);
                        jSONObject.put(InAppPurchaseMetaData.KEY_SIGNATURE, str4);
                        jSONObject.put(AppLovinEventParameters.PRODUCT_IDENTIFIER, str4);
                        jSONArray.put(jSONObject);
                    } catch (JSONException unused) {
                    }
                }
            }
        } catch (NoSuchMethodException e) {
            e = e;
            if (e.getMessage() != null) {
                String str7 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(e.getClass().getSimpleName());
                sb2.append(": ");
                sb2.append(e.getMessage());
                Logger.i(str7, sb2.toString());
            }
            if (e.getCause() != null) {
                str2 = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str2, sb.toString());
            }
        } catch (IllegalAccessException e2) {
            e = e2;
            if (e.getMessage() != null) {
                String str8 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(e.getClass().getSimpleName());
                sb3.append(": ");
                sb3.append(e.getMessage());
                Logger.i(str8, sb3.toString());
            }
            if (e.getCause() != null) {
                str2 = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str2, sb.toString());
            }
        } catch (IllegalArgumentException e3) {
            e = e3;
            if (e.getMessage() != null) {
                String str9 = TAG;
                StringBuilder sb4 = new StringBuilder();
                sb4.append(e.getClass().getSimpleName());
                sb4.append(": ");
                sb4.append(e.getMessage());
                Logger.i(str9, sb4.toString());
            }
            if (e.getCause() != null) {
                str2 = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str2, sb.toString());
            }
        } catch (InvocationTargetException e4) {
            e = e4;
            if (e.getMessage() != null) {
                String str10 = TAG;
                StringBuilder sb5 = new StringBuilder();
                sb5.append(e.getClass().getSimpleName());
                sb5.append(": ");
                sb5.append(e.getMessage());
                Logger.i(str10, sb5.toString());
            }
            if (e.getCause() != null) {
                str2 = TAG;
                sb = new StringBuilder();
                sb.append(e.getClass().getSimpleName());
                sb.append(": ");
                sb.append(e.getCause());
                Logger.i(str2, sb.toString());
            }
        }
        return jSONArray.toString();
    }

    public static String getControllerUrl() {
        return !TextUtils.isEmpty(mControllerUrl) ? mControllerUrl : "";
    }

    public static void setControllerUrl(String str) {
        mControllerUrl = str;
    }

    public static String getControllerConfig() {
        return mControllerConfig;
    }

    public static void setControllerConfig(String str) {
        mControllerConfig = str;
    }

    public static Map<String, String> getInitSDKParams() {
        return mInitSDKParams;
    }

    public static void setInitSDKParams(Map<String, String> map) {
        mInitSDKParams = map;
    }

    public static int getDebugMode() {
        return mDebugMode;
    }

    public static void setDebugMode(int i) {
        mDebugMode = i;
    }

    public static String getValueFromJsonObject(String str, String str2) {
        try {
            return new JSONObject(str).getString(str2);
        } catch (Exception unused) {
            return null;
        }
    }

    public static boolean isExternalStorageAvailable() {
        String externalStorageState = Environment.getExternalStorageState();
        return "mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState);
    }

    public static int getActivityUIFlags(boolean z) {
        int i = VERSION.SDK_INT >= 14 ? 2 : 0;
        if (VERSION.SDK_INT >= 16) {
            i |= 1796;
        }
        return (VERSION.SDK_INT < 19 || !z) ? i : i | 4096;
    }

    private static int generateViewIdForOldOS() {
        int i;
        int i2;
        do {
            i = sNextGeneratedId.get();
            i2 = i + 1;
            if (i2 > 16777215) {
                i2 = 1;
            }
        } while (!sNextGeneratedId.compareAndSet(i, i2));
        return i;
    }

    public static int generateViewId() {
        if (VERSION.SDK_INT < 17) {
            return generateViewIdForOldOS();
        }
        return View.generateViewId();
    }

    public static JSONObject getControllerConfigAsJSONObject() {
        try {
            return new JSONObject(getControllerConfig());
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }

    public static ProductType getProductType(String str) {
        if (str.equalsIgnoreCase(ProductType.RewardedVideo.toString())) {
            return ProductType.RewardedVideo;
        }
        if (str.equalsIgnoreCase(ProductType.Interstitial.toString())) {
            return ProductType.Interstitial;
        }
        if (str.equalsIgnoreCase(ProductType.OfferWall.toString())) {
            return ProductType.OfferWall;
        }
        return null;
    }

    public static void setTesterParameters(String str) {
        mUserGroup = str;
    }

    public static String getTesterParameters() {
        return mUserGroup;
    }

    public static JSONObject mergeJSONObjects(JSONObject jSONObject, JSONObject jSONObject2) throws Exception {
        JSONObject jSONObject3 = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        if (jSONObject != null) {
            jSONObject3 = new JSONObject(jSONObject.toString());
        }
        if (jSONObject2 != null) {
            jSONArray = jSONObject2.names();
        }
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                String string = jSONArray.getString(i);
                jSONObject3.putOpt(string, jSONObject2.opt(string));
            }
        }
        return jSONObject3;
    }

    public static String flatMapToJsonAsString(Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        if (map != null) {
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                Entry entry = (Entry) it.next();
                try {
                    jSONObject.putOpt((String) entry.getKey(), encodeString((String) entry.getValue()));
                } catch (JSONException e) {
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("flatMapToJsonAsStringfailed ");
                    sb.append(e.toString());
                    Logger.i(str, sb.toString());
                }
                it.remove();
            }
        }
        return jSONObject.toString();
    }

    public static Map<String, String> mergeHashMaps(Map<String, String>[] mapArr) {
        HashMap hashMap = new HashMap();
        if (mapArr == null) {
            return hashMap;
        }
        for (Map<String, String> map : mapArr) {
            if (map != null) {
                hashMap.putAll(map);
            }
        }
        return hashMap;
    }

    public static String fetchDemandSourceId(SSAObj sSAObj) {
        return fetchDemandSourceId(sSAObj.getJsonObject());
    }

    public static String fetchDemandSourceId(JSONObject jSONObject) {
        String optString = jSONObject.optString("demandSourceId");
        return !TextUtils.isEmpty(optString) ? optString : jSONObject.optString("demandSourceName");
    }

    public static <T> T requireNonNull(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    public static String requireNonEmptyOrNull(String str, String str2) {
        if (str != null) {
            return str;
        }
        throw new NullPointerException(str2);
    }
}
