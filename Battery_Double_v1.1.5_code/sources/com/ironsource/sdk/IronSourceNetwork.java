package com.ironsource.sdk;

import android.app.Activity;
import android.content.Context;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.agent.IronSourceAdsPublisherAgent;
import com.ironsource.sdk.listeners.OnBannerListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.Map;
import org.json.JSONObject;

public class IronSourceNetwork {
    /* access modifiers changed from: private */
    public static IronSourceNetworkAPI ironSourceNetwork;
    /* access modifiers changed from: private */
    public static JSONObject mConsentParams;

    public static synchronized void initSDK(final Activity activity, final String str, final String str2, final Map<String, String> map) {
        synchronized (IronSourceNetwork.class) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (IronSourceNetwork.ironSourceNetwork == null) {
                        SDKUtils.setInitSDKParams(map);
                        IronSourceNetwork.ironSourceNetwork = IronSourceAdsPublisherAgent.createInstance(activity, str, str2);
                        IronSourceNetwork.applyConsentInfo(IronSourceNetwork.mConsentParams);
                    }
                }
            });
        }
    }

    private static synchronized void validateInitSDK() throws Exception {
        synchronized (IronSourceNetwork.class) {
            if (ironSourceNetwork == null) {
                throw new NullPointerException("Call initSDK first");
            }
        }
    }

    public static synchronized void loadAd(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) throws Exception {
        synchronized (IronSourceNetwork.class) {
            validateInitSDK();
            ironSourceNetwork.loadAd(ironSourceAdInstance, map);
        }
    }

    public static String getVersion() {
        return SDKUtils.getSDKVersion();
    }

    public static synchronized void loadAd(IronSourceAdInstance ironSourceAdInstance) throws Exception {
        synchronized (IronSourceNetwork.class) {
            loadAd(ironSourceAdInstance, null);
        }
    }

    public static synchronized void showAd(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) throws Exception {
        synchronized (IronSourceNetwork.class) {
            validateInitSDK();
            ironSourceNetwork.showAd(ironSourceAdInstance, map);
        }
    }

    public static synchronized void showAd(IronSourceAdInstance ironSourceAdInstance) throws Exception {
        synchronized (IronSourceNetwork.class) {
            showAd(ironSourceAdInstance, null);
        }
    }

    public static synchronized boolean isAdAvailableForInstance(IronSourceAdInstance ironSourceAdInstance) {
        synchronized (IronSourceNetwork.class) {
            if (ironSourceNetwork == null) {
                return false;
            }
            boolean isAdAvailable = ironSourceNetwork.isAdAvailable(ironSourceAdInstance);
            return isAdAvailable;
        }
    }

    public static synchronized void onPause(Activity activity) {
        synchronized (IronSourceNetwork.class) {
            if (ironSourceNetwork != null) {
                ironSourceNetwork.onPause(activity);
            }
        }
    }

    public static synchronized void onResume(Activity activity) {
        synchronized (IronSourceNetwork.class) {
            if (ironSourceNetwork != null) {
                ironSourceNetwork.onResume(activity);
            }
        }
    }

    public static synchronized void updateConsentInfo(JSONObject jSONObject) {
        synchronized (IronSourceNetwork.class) {
            mConsentParams = jSONObject;
            applyConsentInfo(jSONObject);
        }
    }

    public static synchronized void applyConsentInfo(JSONObject jSONObject) {
        synchronized (IronSourceNetwork.class) {
            if (ironSourceNetwork != null) {
                if (jSONObject != null) {
                    ironSourceNetwork.updateConsentInfo(jSONObject);
                }
            }
        }
    }

    public static synchronized void release(Activity activity) {
        synchronized (IronSourceNetwork.class) {
            if (ironSourceNetwork != null) {
                ironSourceNetwork.release(activity);
            }
        }
    }

    public static synchronized String getToken(Context context) {
        synchronized (IronSourceNetwork.class) {
            if (ironSourceNetwork == null) {
                return null;
            }
            String token = ironSourceNetwork.getToken(context);
            return token;
        }
    }

    public static synchronized void initOfferWall(Map<String, String> map, OnOfferWallListener onOfferWallListener) throws Exception {
        synchronized (IronSourceNetwork.class) {
            validateInitSDK();
            ironSourceNetwork.initOfferWall(map, onOfferWallListener);
        }
    }

    public static synchronized void showOfferWall(Map<String, String> map) throws Exception {
        synchronized (IronSourceNetwork.class) {
            validateInitSDK();
            ironSourceNetwork.showOfferWall(map);
        }
    }

    public static synchronized void getOfferWallCredits(OnOfferWallListener onOfferWallListener) throws Exception {
        synchronized (IronSourceNetwork.class) {
            validateInitSDK();
            ironSourceNetwork.getOfferWallCredits(onOfferWallListener);
        }
    }

    public static synchronized void initBanner(String str, Map<String, String> map, OnBannerListener onBannerListener) throws Exception {
        synchronized (IronSourceNetwork.class) {
            validateInitSDK();
            ironSourceNetwork.initBanner(str, map, onBannerListener);
        }
    }

    public static synchronized void loadBanner(JSONObject jSONObject) throws Exception {
        synchronized (IronSourceNetwork.class) {
            validateInitSDK();
            ironSourceNetwork.loadBanner(jSONObject);
        }
    }

    public static synchronized ISNAdView createBanner(Activity activity, ISAdSize iSAdSize) throws Exception {
        ISNAdView createBanner;
        synchronized (IronSourceNetwork.class) {
            validateInitSDK();
            createBanner = ironSourceNetwork.createBanner(activity, iSAdSize);
        }
        return createBanner;
    }
}
