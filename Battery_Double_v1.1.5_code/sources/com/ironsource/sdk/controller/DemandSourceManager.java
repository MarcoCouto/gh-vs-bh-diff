package com.ironsource.sdk.controller;

import android.text.TextUtils;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums.ProductType;
import com.ironsource.sdk.listeners.OnAdProductListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class DemandSourceManager {
    private Map<String, DemandSource> mBannerDemandSourceMap = new LinkedHashMap();
    private Map<String, DemandSource> mInterstitialDemandSourceMap = new LinkedHashMap();
    private Map<String, DemandSource> mRewardedVideoDemandSourceMap = new LinkedHashMap();

    private Map<String, DemandSource> getMapByProductType(ProductType productType) {
        if (productType.name().equalsIgnoreCase(ProductType.RewardedVideo.name())) {
            return this.mRewardedVideoDemandSourceMap;
        }
        if (productType.name().equalsIgnoreCase(ProductType.Interstitial.name())) {
            return this.mInterstitialDemandSourceMap;
        }
        if (productType.name().equalsIgnoreCase(ProductType.Banner.name())) {
            return this.mBannerDemandSourceMap;
        }
        return null;
    }

    public Collection<DemandSource> getDemandSources(ProductType productType) {
        Map mapByProductType = getMapByProductType(productType);
        if (mapByProductType != null) {
            return mapByProductType.values();
        }
        return new ArrayList();
    }

    public DemandSource getDemandSourceById(ProductType productType, String str) {
        if (!TextUtils.isEmpty(str)) {
            Map mapByProductType = getMapByProductType(productType);
            if (mapByProductType != null) {
                return (DemandSource) mapByProductType.get(str);
            }
        }
        return null;
    }

    private void addDemandSourceToDemandSources(ProductType productType, String str, DemandSource demandSource) {
        if (!TextUtils.isEmpty(str) && demandSource != null) {
            Map mapByProductType = getMapByProductType(productType);
            if (mapByProductType != null) {
                mapByProductType.put(str, demandSource);
            }
        }
    }

    public DemandSource createDemandSource(ProductType productType, IronSourceAdInstance ironSourceAdInstance) {
        String id = ironSourceAdInstance.getId();
        DemandSource demandSource = new DemandSource(id, ironSourceAdInstance.getName(), ironSourceAdInstance.convertToMap(), ironSourceAdInstance.getAdListener());
        addDemandSourceToDemandSources(productType, id, demandSource);
        return demandSource;
    }

    public DemandSource createDemandSource(ProductType productType, String str, Map<String, String> map, OnAdProductListener onAdProductListener) {
        DemandSource demandSource = new DemandSource(str, str, map, onAdProductListener);
        addDemandSourceToDemandSources(productType, str, demandSource);
        return demandSource;
    }
}
