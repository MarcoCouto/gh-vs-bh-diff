package com.ironsource.sdk.controller;

import android.app.Application;
import android.webkit.WebView;
import com.ironsource.sdk.analytics.moat.MOATManager;
import com.ironsource.sdk.analytics.moat.MOATManager.EventsListener;
import org.json.JSONException;
import org.json.JSONObject;

public class MOATJSAdapter {
    private static final String createAdTracker = "createAdTracker";
    private static final String fail = "fail";
    private static final String initWithOptions = "initWithOptions";
    private static final String moatFunction = "moatFunction";
    private static final String moatParams = "moatParams";
    private static final String startTracking = "startTracking";
    private static final String stopTracking = "stopTracking";
    private static final String success = "success";
    private Application mApplication;

    private static class FunctionCall {
        String failCallback;
        String name;
        JSONObject params;
        String successCallback;

        private FunctionCall() {
        }
    }

    public MOATJSAdapter(Application application) {
        this.mApplication = application;
    }

    /* access modifiers changed from: 0000 */
    public void call(String str, JSCallbackTask jSCallbackTask, WebView webView) throws Exception {
        FunctionCall fetchFunctionCall = fetchFunctionCall(str);
        if (initWithOptions.equals(fetchFunctionCall.name)) {
            MOATManager.initWithOptions(fetchFunctionCall.params, this.mApplication);
        } else if (createAdTracker.equals(fetchFunctionCall.name) && webView != null) {
            MOATManager.createAdTracker(webView);
        } else if (startTracking.equals(fetchFunctionCall.name)) {
            MOATManager.setEventListener(createEventListener(jSCallbackTask, fetchFunctionCall.successCallback, fetchFunctionCall.failCallback));
            MOATManager.startTracking();
        } else if (stopTracking.equals(fetchFunctionCall.name)) {
            MOATManager.setEventListener(createEventListener(jSCallbackTask, fetchFunctionCall.successCallback, fetchFunctionCall.failCallback));
            MOATManager.stopTracking();
        }
    }

    private EventsListener createEventListener(final JSCallbackTask jSCallbackTask, final String str, final String str2) {
        return new EventsListener() {
            public void onTrackingStarted(String str) {
                if (jSCallbackTask != null) {
                    jSCallbackTask.sendMessage(true, str, str);
                }
            }

            public void onTrackingFailedToStart(String str) {
                if (jSCallbackTask != null) {
                    jSCallbackTask.sendMessage(false, str2, str);
                }
            }

            public void onTrackingStopped(String str) {
                if (jSCallbackTask != null) {
                    jSCallbackTask.sendMessage(true, str, str);
                }
            }
        };
    }

    private FunctionCall fetchFunctionCall(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        FunctionCall functionCall = new FunctionCall();
        functionCall.name = jSONObject.optString(moatFunction);
        functionCall.params = jSONObject.optJSONObject(moatParams);
        functionCall.successCallback = jSONObject.optString("success");
        functionCall.failCallback = jSONObject.optString("fail");
        return functionCall;
    }
}
