package com.ironsource.sdk.controller;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.InputDeviceCompat;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.ironsource.sdk.agent.IronSourceAdsPublisherAgent;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.ErrorCodes;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.List;

public class OpenUrlActivity extends Activity {
    private static final int PROGRESS_BAR_VIEW_ID = SDKUtils.generateViewId();
    private static final String TAG = "OpenUrlActivity";
    private static final int WEB_VIEW_VIEW_ID = SDKUtils.generateViewId();
    /* access modifiers changed from: private */
    public final Runnable decorViewSettings = new Runnable() {
        public void run() {
            OpenUrlActivity.this.getWindow().getDecorView().setSystemUiVisibility(SDKUtils.getActivityUIFlags(OpenUrlActivity.this.mIsImmersive));
        }
    };
    boolean isSecondaryWebview;
    /* access modifiers changed from: private */
    public boolean mIsImmersive = false;
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;
    /* access modifiers changed from: private */
    public Handler mUiThreadHandler = new Handler();
    private String mUrl;
    /* access modifiers changed from: private */
    public IronSourceWebView mWebViewController;
    private RelativeLayout mainLayout;
    private WebView webView = null;

    private class Client extends WebViewClient {
        private Client() {
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            OpenUrlActivity.this.mProgressBar.setVisibility(0);
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            OpenUrlActivity.this.mProgressBar.setVisibility(4);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            List<String> searchKeys = IronSourceSharedPrefHelper.getSupersonicPrefHelper().getSearchKeys();
            if (searchKeys != null && !searchKeys.isEmpty()) {
                for (String contains : searchKeys) {
                    if (str.contains(contains)) {
                        try {
                            OpenUrlActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                            OpenUrlActivity.this.mWebViewController.interceptedUrlToStore();
                        } catch (Exception e) {
                            StringBuilder sb = new StringBuilder();
                            if (e instanceof ActivityNotFoundException) {
                                sb.append(ErrorCodes.STORE_ACTIVITY_FAILED_REASON_NO_ACTIVITY);
                            } else {
                                sb.append(ErrorCodes.STORE_ACTIVITY_FAILED_REASON_UNSPECIFIED);
                            }
                            OpenUrlActivity.this.mWebViewController.failedToStartStoreActivity(sb.toString(), str);
                        }
                        OpenUrlActivity.this.finish();
                        return true;
                    }
                }
            }
            return super.shouldOverrideUrlLoading(webView, str);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Logger.i(TAG, "onCreate()");
        try {
            this.mWebViewController = IronSourceAdsPublisherAgent.getInstance(this).getWebViewController();
            hideActivityTitle();
            hideActivtiyStatusBar();
            Bundle extras = getIntent().getExtras();
            this.mUrl = extras.getString(IronSourceWebView.EXTERNAL_URL);
            this.isSecondaryWebview = extras.getBoolean(IronSourceWebView.SECONDARY_WEB_VIEW);
            this.mIsImmersive = getIntent().getBooleanExtra(ParametersKeys.IMMERSIVE, false);
            if (this.mIsImmersive) {
                getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener() {
                    public void onSystemUiVisibilityChange(int i) {
                        if ((i & InputDeviceCompat.SOURCE_TOUCHSCREEN) == 0) {
                            OpenUrlActivity.this.mUiThreadHandler.removeCallbacks(OpenUrlActivity.this.decorViewSettings);
                            OpenUrlActivity.this.mUiThreadHandler.postDelayed(OpenUrlActivity.this.decorViewSettings, 500);
                        }
                    }
                });
                runOnUiThread(this.decorViewSettings);
            }
            this.mainLayout = new RelativeLayout(this);
            setContentView(this.mainLayout, new LayoutParams(-1, -1));
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        createWebView();
    }

    private void createWebView() {
        if (this.webView == null) {
            this.webView = new WebView(getApplicationContext());
            this.webView.setId(WEB_VIEW_VIEW_ID);
            this.webView.getSettings().setJavaScriptEnabled(true);
            this.webView.setWebViewClient(new Client());
            loadUrl(this.mUrl);
        }
        if (findViewById(WEB_VIEW_VIEW_ID) == null) {
            this.mainLayout.addView(this.webView, new RelativeLayout.LayoutParams(-1, -1));
        }
        createProgressBarForWebView();
        if (this.mWebViewController != null) {
            this.mWebViewController.viewableChange(true, ParametersKeys.SECONDARY);
        }
    }

    private void createProgressBarForWebView() {
        if (this.mProgressBar == null) {
            if (VERSION.SDK_INT >= 11) {
                this.mProgressBar = new ProgressBar(new ContextThemeWrapper(this, 16973939));
            } else {
                this.mProgressBar = new ProgressBar(this);
            }
            this.mProgressBar.setId(PROGRESS_BAR_VIEW_ID);
        }
        if (findViewById(PROGRESS_BAR_VIEW_ID) == null) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            this.mProgressBar.setLayoutParams(layoutParams);
            this.mProgressBar.setVisibility(4);
            this.mainLayout.addView(this.mProgressBar);
        }
    }

    private void removeWebViewFromLayout() {
        if (this.mWebViewController != null) {
            this.mWebViewController.viewableChange(false, ParametersKeys.SECONDARY);
            if (this.mainLayout != null) {
                ViewGroup viewGroup = (ViewGroup) this.webView.getParent();
                if (viewGroup != null) {
                    if (viewGroup.findViewById(WEB_VIEW_VIEW_ID) != null) {
                        viewGroup.removeView(this.webView);
                    }
                    if (viewGroup.findViewById(PROGRESS_BAR_VIEW_ID) != null) {
                        viewGroup.removeView(this.mProgressBar);
                    }
                }
            }
        }
    }

    private void destroyWebView() {
        if (this.webView != null) {
            this.webView.destroy();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        removeWebViewFromLayout();
    }

    public void loadUrl(String str) {
        this.webView.stopLoading();
        this.webView.clearHistory();
        try {
            this.webView.loadUrl(str);
        } catch (Throwable th) {
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("OpenUrlActivity:: loadUrl: ");
            sb.append(th.toString());
            Logger.e(str2, sb.toString());
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(Constants.NATIVE_EXCEPTION_BASE_URL);
            sb2.append(th.getStackTrace()[0].getMethodName());
            ironSourceAsyncHttpRequestTask.execute(new String[]{sb2.toString()});
        }
    }

    private void hideActivityTitle() {
        requestWindowFeature(1);
    }

    private void hideActivtiyStatusBar() {
        getWindow().setFlags(1024, 1024);
    }

    private void disableTouch() {
        getWindow().addFlags(16);
    }

    public void onBackPressed() {
        if (this.webView.canGoBack()) {
            this.webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        destroyWebView();
    }

    public void finish() {
        if (this.isSecondaryWebview) {
            this.mWebViewController.engageEnd(ParametersKeys.SECONDARY_CLOSE);
        }
        super.finish();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.mIsImmersive && z) {
            runOnUiThread(this.decorViewSettings);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.mIsImmersive && (i == 25 || i == 24)) {
            this.mUiThreadHandler.postDelayed(this.decorViewSettings, 500);
        }
        return super.onKeyDown(i, keyEvent);
    }
}
