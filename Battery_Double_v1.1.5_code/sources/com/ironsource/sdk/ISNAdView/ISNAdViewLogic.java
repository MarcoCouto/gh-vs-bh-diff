package com.ironsource.sdk.ISNAdView;

import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import org.json.JSONException;
import org.json.JSONObject;

class ISNAdViewLogic {
    private static Handler mUIThreadHandler;
    /* access modifiers changed from: private */
    public String TAG = ISNAdViewLogic.class.getSimpleName();
    private String[] commandsToHandleInAdView = {ISNAdViewConstants.HANDLE_GET_VIEW_VISIBILITY};
    /* access modifiers changed from: private */
    public JSONObject mAdViewConfiguration = null;
    /* access modifiers changed from: private */
    public ViewVisibilityParameters mAdViewVisibilityParameters = new ViewVisibilityParameters();
    /* access modifiers changed from: private */
    public ISNAdViewDelegate mDelegate;
    private WebView mWebView;
    private final String[] supportedCommandsFromController = {ISNAdViewConstants.LOAD_WITH_URL, ISNAdViewConstants.UPDATE_AD, ISNAdViewConstants.IS_EXTERNAL_AD_VIEW_INITIATED, ISNAdViewConstants.HANDLE_GET_VIEW_VISIBILITY, ISNAdViewConstants.SEND_MESSAGE};

    ISNAdViewLogic() {
    }

    /* access modifiers changed from: 0000 */
    public void setControllerDelegate(ISNAdViewDelegate iSNAdViewDelegate) {
        this.mDelegate = iSNAdViewDelegate;
    }

    private Handler getUIThreadHandler() {
        try {
            if (mUIThreadHandler == null) {
                mUIThreadHandler = new Handler(Looper.getMainLooper());
            }
        } catch (Exception e) {
            Log.e(this.TAG, "Error while trying execute method getUIThreadHandler");
            e.printStackTrace();
        }
        return mUIThreadHandler;
    }

    /* access modifiers changed from: 0000 */
    public void setAdViewWebView(WebView webView) {
        this.mWebView = webView;
    }

    /* access modifiers changed from: 0000 */
    public void destroy() {
        this.mAdViewConfiguration = null;
        this.mDelegate = null;
        this.mAdViewVisibilityParameters = null;
        mUIThreadHandler = null;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject buildDataForLoadingAd(JSONObject jSONObject, String str) throws Exception {
        try {
            boolean isInReload = isInReload();
            if (this.mAdViewConfiguration == null) {
                this.mAdViewConfiguration = new JSONObject(jSONObject.toString());
            }
            this.mAdViewConfiguration.put(ISNAdViewConstants.EXTERNAL_AD_VIEW_ID, str);
            this.mAdViewConfiguration.put(ISNAdViewConstants.IS_IN_RELOAD, isInReload);
            return this.mAdViewConfiguration;
        } catch (Exception unused) {
            throw new Exception("ISNAdViewLogic | buildDataForLoadingAd | Could not build load parameters");
        }
    }

    private boolean isInReload() {
        return this.mAdViewConfiguration != null;
    }

    private void sendMessageToController(String str, JSONObject jSONObject) {
        if (this.mDelegate != null) {
            this.mDelegate.sendMessageToController(str, jSONObject);
        }
    }

    /* access modifiers changed from: 0000 */
    public void sendErrorMessageToController(String str, String str2) {
        if (this.mDelegate != null) {
            this.mDelegate.sendErrorMessageToController(str, str2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void handleMessageFromController(String str, JSONObject jSONObject, String str2, String str3) {
        Handler uIThreadHandler = getUIThreadHandler();
        final String str4 = str;
        final String str5 = str3;
        final String str6 = str2;
        final JSONObject jSONObject2 = jSONObject;
        AnonymousClass1 r1 = new Runnable() {
            public void run() {
                try {
                    if (!ISNAdViewLogic.this.canHandleCommandFromController(str4)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("ISNAdViewLogic | handleMessageFromController | cannot handle command: ");
                        sb.append(str4);
                        String sb2 = sb.toString();
                        Log.e(ISNAdViewLogic.this.TAG, sb2);
                        ISNAdViewLogic.this.mDelegate.sendErrorMessageToController(str5, sb2);
                        return;
                    }
                    if (str4.equalsIgnoreCase(ISNAdViewConstants.IS_EXTERNAL_AD_VIEW_INITIATED)) {
                        ISNAdViewLogic.this.sendIsExternalAdViewInitiated(str6);
                    } else if (str4.equalsIgnoreCase(ISNAdViewConstants.HANDLE_GET_VIEW_VISIBILITY)) {
                        ISNAdViewLogic.this.sendHandleGetViewVisibilityParams(str6);
                    } else {
                        if (!str4.equalsIgnoreCase(ISNAdViewConstants.SEND_MESSAGE)) {
                            if (!str4.equalsIgnoreCase(ISNAdViewConstants.UPDATE_AD)) {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("ISNAdViewLogic | handleMessageFromController | unhandled API request ");
                                sb3.append(str4);
                                sb3.append(" ");
                                sb3.append(jSONObject2.toString());
                                String sb4 = sb3.toString();
                                Log.e(ISNAdViewLogic.this.TAG, sb4);
                                ISNAdViewLogic.this.mDelegate.sendErrorMessageToController(str5, sb4);
                            }
                        }
                        ISNAdViewLogic.this.sendMessageToAdunit(jSONObject2.getString("params"), str6, str5);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("ISNAdViewLogic | handleMessageFromController | Error while trying handle message: ");
                    sb5.append(str4);
                    String sb6 = sb5.toString();
                    Log.e(ISNAdViewLogic.this.TAG, sb6);
                    ISNAdViewLogic.this.mDelegate.sendErrorMessageToController(str5, sb6);
                }
            }
        };
        uIThreadHandler.post(r1);
    }

    /* access modifiers changed from: private */
    public boolean canHandleCommandFromController(String str) {
        boolean z = false;
        for (int i = 0; i < this.supportedCommandsFromController.length && !z; i++) {
            if (this.supportedCommandsFromController[i].equalsIgnoreCase(str)) {
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public void updateViewVisibilityParameters(String str, int i, boolean z) {
        this.mAdViewVisibilityParameters.updateViewVisibilityParameters(str, i, z);
        if (shouldReportVisibilityToController(str)) {
            reportAdContainerIsVisible();
        }
    }

    private boolean shouldReportVisibilityToController(String str) {
        if (VERSION.SDK_INT <= 22) {
            return str.equalsIgnoreCase(ISNAdViewConstants.IS_WINDOW_VISIBLE_KEY);
        }
        return str.equalsIgnoreCase(ISNAdViewConstants.IS_VISIBLE_KEY);
    }

    private void reportAdContainerIsVisible() {
        if (this.mDelegate != null && this.mAdViewVisibilityParameters != null) {
            sendMessageToController(ISNAdViewConstants.CONTAINER_IMPRESSION_MESSAGE, buildParamsObjectForAdViewVisibility());
        }
    }

    /* access modifiers changed from: 0000 */
    public void reportAdContainerWasRemoved() {
        if (this.mDelegate != null && this.mAdViewVisibilityParameters != null) {
            sendMessageToController(ISNAdViewConstants.CONTAINER_DESTRUCTION_MESSAGE, buildParamsObjectForAdViewVisibility());
        }
    }

    private JSONObject buildParamsObjectForAdViewVisibility() {
        return new JSONObject() {
            {
                try {
                    put(ISNAdViewConstants.CONFIGS, ISNAdViewLogic.this.extendConfigurationWithVisibilityParams(ISNAdViewLogic.this.mAdViewConfiguration, ISNAdViewLogic.this.mAdViewVisibilityParameters.collectVisibilityParameters()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public JSONObject extendConfigurationWithVisibilityParams(JSONObject jSONObject, JSONObject jSONObject2) {
        try {
            JSONObject jSONObject3 = new JSONObject(jSONObject.toString());
            jSONObject3.put(ISNAdViewConstants.VISIBILITY_PARAMS_KEY, jSONObject2);
            return jSONObject3;
        } catch (JSONException e) {
            e.printStackTrace();
            return jSONObject;
        }
    }

    /* access modifiers changed from: private */
    public void sendIsExternalAdViewInitiated(String str) {
        try {
            boolean z = (this.mWebView == null || this.mWebView.getUrl() == null) ? false : true;
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(ISNAdViewConstants.IS_EXTERNAL_AD_VIEW_INITIATED, z);
            sendMessageToController(str, jSONObject);
        } catch (Exception e) {
            Log.e(this.TAG, "Error while trying execute method sendIsExternalAdViewInitiated");
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void sendHandleGetViewVisibilityParams(String str) {
        sendMessageToController(str, this.mAdViewVisibilityParameters.collectVisibilityParameters());
    }

    /* access modifiers changed from: private */
    public void sendMessageToAdunit(String str, String str2, String str3) {
        if (this.mWebView == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("No external adunit attached to ISNAdView while trying to send message: ");
            sb.append(str);
            String sb2 = sb.toString();
            Log.e(this.TAG, sb2);
            this.mDelegate.sendErrorMessageToController(str3, sb2);
            return;
        }
        try {
            new JSONObject(str);
        } catch (JSONException unused) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("\"");
            sb3.append(str);
            sb3.append("\"");
            str = sb3.toString();
        }
        final String buildCommandForWebView = buildCommandForWebView(str);
        getUIThreadHandler().post(new Runnable() {
            public void run() {
                ISNAdViewLogic.this.injectJavaScriptIntoWebView(buildCommandForWebView);
            }
        });
    }

    private String buildCommandForWebView(String str) {
        return String.format(ISNAdViewConstants.ADUNIT_MESSAGE_FORMAT, new Object[]{str});
    }

    /* access modifiers changed from: private */
    public void injectJavaScriptIntoWebView(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("javascript:try{");
            sb.append(str);
            sb.append("}catch(e){console.log(\"JS exception: \" + JSON.stringify(e));}");
            String sb2 = sb.toString();
            if (VERSION.SDK_INT >= 19) {
                this.mWebView.evaluateJavascript(sb2, null);
            } else {
                this.mWebView.loadUrl(sb2);
            }
        } catch (Throwable th) {
            String str2 = this.TAG;
            StringBuilder sb3 = new StringBuilder();
            sb3.append("injectJavaScriptIntoWebView | Error while trying inject JS into external adunit: ");
            sb3.append(str);
            sb3.append("Android API level: ");
            sb3.append(VERSION.SDK_INT);
            Log.e(str2, sb3.toString());
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: 0000 */
    public void handleMessageFromWebView(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("method");
            if (TextUtils.isEmpty(optString) || !shouldHandleMessageInContainer(optString)) {
                sendMessageToController(ISNAdViewConstants.CONTAINER_SEND_MESSAGE, jSONObject);
            } else if (optString.equalsIgnoreCase(ISNAdViewConstants.HANDLE_GET_VIEW_VISIBILITY)) {
                sendHandleGetViewVisibilityParamsForWebView(jSONObject);
            }
        } catch (JSONException e) {
            String str2 = this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("ISNAdViewLogic | receiveMessageFromExternal | Error while trying handle message: ");
            sb.append(str);
            Log.e(str2, sb.toString());
            e.printStackTrace();
        }
    }

    private boolean shouldHandleMessageInContainer(String str) {
        for (String equalsIgnoreCase : this.commandsToHandleInAdView) {
            if (equalsIgnoreCase.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    private void sendHandleGetViewVisibilityParamsForWebView(JSONObject jSONObject) {
        sendMessageToAdunit(buildVisibilityMessageForAdunit(jSONObject).toString(), null, null);
    }

    private JSONObject buildVisibilityMessageForAdunit(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("id", jSONObject.getString("id"));
            jSONObject2.put("data", this.mAdViewVisibilityParameters.collectVisibilityParameters());
        } catch (Exception e) {
            String str = this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Error while trying execute method buildVisibilityMessageForAdunit | params: ");
            sb.append(jSONObject);
            Log.e(str, sb.toString());
            e.printStackTrace();
        }
        return jSONObject2;
    }
}
