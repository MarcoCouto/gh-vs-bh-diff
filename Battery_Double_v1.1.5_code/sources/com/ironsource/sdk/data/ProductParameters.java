package com.ironsource.sdk.data;

public class ProductParameters {
    public final String appKey;
    public final String userId;

    public ProductParameters(String str, String str2) {
        this.appKey = str;
        this.userId = str2;
    }
}
