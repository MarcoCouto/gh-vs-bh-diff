package com.ironsource.sdk.data;

public class SSABCParameters extends SSAObj {
    private String CONNECTION_RETRIES = "connectionRetries";
    private String mConnectionRetries;

    public SSABCParameters() {
    }

    public SSABCParameters(String str) {
        super(str);
        if (containsKey(this.CONNECTION_RETRIES)) {
            setConnectionRetries(getString(this.CONNECTION_RETRIES));
        }
    }

    public String getConnectionRetries() {
        return this.mConnectionRetries;
    }

    public void setConnectionRetries(String str) {
        this.mConnectionRetries = str;
    }
}
