package com.ironsource.sdk.service;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.utils.DeviceProperties;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import org.json.JSONObject;

public class DeviceData {
    private static final String TAG = "DeviceData";

    public static JSONObject fetchAdvertiserIdData(Context context) {
        SDKUtils.loadGoogleAdvertiserInfo(context);
        String advertiserId = SDKUtils.getAdvertiserId();
        Boolean valueOf = Boolean.valueOf(SDKUtils.isLimitAdTrackingEnabled());
        JSONObject jSONObject = new JSONObject();
        if (!TextUtils.isEmpty(advertiserId)) {
            try {
                Logger.i(TAG, "add AID and LAT");
                jSONObject.put(RequestParameters.isLAT, valueOf);
                StringBuilder sb = new StringBuilder();
                sb.append(RequestParameters.DEVICE_IDS);
                sb.append(RequestParameters.LEFT_BRACKETS);
                sb.append(RequestParameters.AID);
                sb.append(RequestParameters.RIGHT_BRACKETS);
                jSONObject.put(sb.toString(), SDKUtils.encodeString(advertiserId));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jSONObject;
    }

    public static JSONObject fetchMutableData(Context context) {
        JSONObject jSONObject = new JSONObject();
        updateWithDisplaySize(jSONObject);
        updateWithConnectionType(context, jSONObject);
        updateWithFreeDiskSize(context, jSONObject);
        updateWithBatteryLevel(context, jSONObject);
        updateWithDeviceVolume(context, jSONObject);
        return jSONObject;
    }

    private static void updateObjectWithKeyValue(JSONObject jSONObject, String str, String str2) {
        try {
            if (!TextUtils.isEmpty(str2)) {
                jSONObject.put(str, SDKUtils.encodeString(str2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void updateWithDisplaySize(JSONObject jSONObject) {
        try {
            updateObjectWithKeyValue(jSONObject, RequestParameters.DISPLAY_SIZE_WIDTH, String.valueOf(DeviceStatus.getDisplayWidth()));
            updateObjectWithKeyValue(jSONObject, RequestParameters.DISPLAY_SIZE_HEIGHT, String.valueOf(DeviceStatus.getDisplayHeight()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void updateWithConnectionType(Context context, JSONObject jSONObject) {
        try {
            String connectionType = ConnectivityService.getConnectionType(context);
            if (!TextUtils.isEmpty(connectionType)) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.CONNECTION_TYPE), SDKUtils.encodeString(connectionType));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void updateWithBatteryLevel(Context context, JSONObject jSONObject) {
        try {
            jSONObject.put(SDKUtils.encodeString(RequestParameters.BATTERY_LEVEL), DeviceStatus.getBatteryLevel(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void updateWithDeviceVolume(Context context, JSONObject jSONObject) {
        try {
            jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_VOLUME), (double) DeviceProperties.getInstance(context).getDeviceVolume(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void updateWithFreeDiskSize(Context context, JSONObject jSONObject) {
        try {
            jSONObject.put(SDKUtils.encodeString(RequestParameters.DISK_FREE_SIZE), SDKUtils.encodeString(String.valueOf(DeviceStatus.getAvailableMemorySizeInMegaBytes(IronSourceStorageUtils.getDiskCacheDirPath(context)))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject fetchPermanentData(Context context) {
        DeviceProperties instance = DeviceProperties.getInstance(context);
        JSONObject jSONObject = new JSONObject();
        try {
            String deviceOem = instance.getDeviceOem();
            if (deviceOem != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_OEM), SDKUtils.encodeString(deviceOem));
            }
            String deviceModel = instance.getDeviceModel();
            if (deviceModel != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_MODEL), SDKUtils.encodeString(deviceModel));
            }
            String deviceOsType = instance.getDeviceOsType();
            if (deviceOsType != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_OS), SDKUtils.encodeString(deviceOsType));
            }
            String deviceOsVersion = instance.getDeviceOsVersion();
            if (deviceOsVersion != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_OS_VERSION), deviceOsVersion.replaceAll("[^0-9/.]", ""));
            }
            String deviceOsVersion2 = instance.getDeviceOsVersion();
            if (deviceOsVersion2 != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_OS_VERSION_FULL), SDKUtils.encodeString(deviceOsVersion2));
            }
            jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_API_LEVEL), String.valueOf(instance.getDeviceApiLevel()));
            String supersonicSdkVersion = DeviceProperties.getSupersonicSdkVersion();
            if (supersonicSdkVersion != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.SDK_VERSION), SDKUtils.encodeString(supersonicSdkVersion));
            }
            if (instance.getDeviceCarrier() != null && instance.getDeviceCarrier().length() > 0) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.MOBILE_CARRIER), SDKUtils.encodeString(instance.getDeviceCarrier()));
            }
            String language = context.getResources().getConfiguration().locale.getLanguage();
            if (!TextUtils.isEmpty(language)) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_LANGUAGE), SDKUtils.encodeString(language.toUpperCase()));
            }
            String packageName = ApplicationContext.getPackageName(context);
            if (!TextUtils.isEmpty(packageName)) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.PACKAGE_NAME), SDKUtils.encodeString(packageName));
            }
            String valueOf = String.valueOf(DeviceStatus.getDeviceDensity());
            if (!TextUtils.isEmpty(valueOf)) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_SCREEN_SCALE), SDKUtils.encodeString(valueOf));
            }
            String valueOf2 = String.valueOf(DeviceStatus.isRootedDevice());
            if (!TextUtils.isEmpty(valueOf2)) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.IS_ROOT_DEVICE), SDKUtils.encodeString(valueOf2));
            }
            jSONObject.put(SDKUtils.encodeString(RequestParameters.NETWORK_MCC), ConnectivityService.getNetworkMCC(context));
            jSONObject.put(SDKUtils.encodeString(RequestParameters.NETWORK_MNC), ConnectivityService.getNetworkMNC(context));
            jSONObject.put(SDKUtils.encodeString(RequestParameters.PHONE_TYPE), ConnectivityService.getPhoneType(context));
            jSONObject.put(SDKUtils.encodeString(RequestParameters.SIM_OPERATOR), SDKUtils.encodeString(ConnectivityService.getSimOperator(context)));
            jSONObject.put(SDKUtils.encodeString("lastUpdateTime"), ApplicationContext.getLastUpdateTime(context));
            jSONObject.put(SDKUtils.encodeString(RequestParameters.FIRST_INSTALL_TIME), ApplicationContext.getFirstInstallTime(context));
            jSONObject.put(SDKUtils.encodeString(RequestParameters.APPLICATION_VERSION_NAME), SDKUtils.encodeString(ApplicationContext.getApplicationVersionName(context)));
            String installerPackageName = ApplicationContext.getInstallerPackageName(context);
            if (!TextUtils.isEmpty(installerPackageName)) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.INSTALLER_PACKAGE_NAME), SDKUtils.encodeString(installerPackageName));
            }
            jSONObject.put(RequestParameters.LOCAL_TIME, SDKUtils.encodeString(String.valueOf(DeviceStatus.getDeviceLocalTime())));
            jSONObject.put(RequestParameters.TIMEZONE_OFFSET, SDKUtils.encodeString(String.valueOf(DeviceStatus.getDeviceTimeZoneOffsetInMinutes())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jSONObject;
    }
}
