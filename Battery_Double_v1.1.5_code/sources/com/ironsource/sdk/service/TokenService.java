package com.ironsource.sdk.service;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class TokenService {
    private JSONObject tokeData = new JSONObject();

    /* access modifiers changed from: 0000 */
    public synchronized void put(String str, Object obj) {
        try {
            this.tokeData.put(str, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return;
    }

    public void collectApplicationUserId(String str) {
        if (str != null) {
            put(RequestParameters.APPLICATION_USER_ID, SDKUtils.encodeString(str));
        }
    }

    public void collectApplicationKey(String str) {
        if (str != null) {
            put(RequestParameters.APPLICATION_KEY, SDKUtils.encodeString(str));
        }
    }

    public void collectDataFromActivity(Activity activity) {
        if (activity != null) {
            if (VERSION.SDK_INT >= 19) {
                put(SDKUtils.encodeString(RequestParameters.IMMERSIVE), Boolean.valueOf(DeviceStatus.isImmersiveSupported(activity)));
            }
            put(RequestParameters.APP_ORIENTATION, SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(activity)));
        }
    }

    public void collectAdvertisingID(final Context context) {
        try {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        TokenService.this.updateData(DeviceData.fetchAdvertiserIdData(context));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void collectDataFromDevice(Context context) {
        updateData(DeviceData.fetchPermanentData(context));
        updateData(DeviceData.fetchMutableData(context));
    }

    public void collectDataFromExternalParams(Map<String, String> map) {
        if (map == null) {
            Log.d("TokenService", "collectDataFromExternalParams params=null");
            return;
        }
        for (String str : map.keySet()) {
            put(str, SDKUtils.encodeString((String) map.get(str)));
        }
    }

    public void collectDataFromControllerConfig(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                put("chinaCDN", new JSONObject(str).opt("chinaCDN"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateData(JSONObject jSONObject) {
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            put(str, jSONObject.opt(str));
        }
    }

    public String getToken() {
        return Gibberish.encode(this.tokeData.toString());
    }
}
