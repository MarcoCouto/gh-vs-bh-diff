package com.ironsource.sdk.agent;

import android.app.Activity;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.ISAdSize;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.IronSourceNetworkAPI;
import com.ironsource.sdk.SSAPublisher;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.controller.BannerJSAdapter;
import com.ironsource.sdk.controller.CommandExecutor;
import com.ironsource.sdk.controller.DemandSourceManager;
import com.ironsource.sdk.controller.IronSourceWebView;
import com.ironsource.sdk.controller.MOATJSAdapter;
import com.ironsource.sdk.controller.PermissionsJSAdapter;
import com.ironsource.sdk.controller.TokenJSAdapter;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums.ProductType;
import com.ironsource.sdk.data.SSASession;
import com.ironsource.sdk.data.SSASession.SessionType;
import com.ironsource.sdk.listeners.OnBannerListener;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnRewardedVideoListener;
import com.ironsource.sdk.listeners.internals.DSAdProductListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import com.ironsource.sdk.service.TokenService;
import com.ironsource.sdk.utils.DeviceProperties;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import com.ironsource.sdk.utils.IronSourceQaProperties;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class IronSourceAdsPublisherAgent implements SSAPublisher, DSRewardedVideoListener, DSInterstitialListener, DSAdProductListener, DSBannerListener, IronSourceNetworkAPI {
    private static final String TAG = "IronSourceAdsPublisherAgent";
    /* access modifiers changed from: private */
    public static MutableContextWrapper mutableContextWrapper;
    private static IronSourceAdsPublisherAgent sInstance;
    private final String SUPERSONIC_ADS = IronSourceConstants.SUPERSONIC_CONFIG_NAME;
    private long adViewContainerCounter;
    /* access modifiers changed from: private */
    public String mApplicationKey;
    /* access modifiers changed from: private */
    public BannerJSAdapter mBannerJSAdapter;
    /* access modifiers changed from: private */
    public CommandExecutor mCommandExecutor;
    /* access modifiers changed from: private */
    public DemandSourceManager mDemandSourceManager;
    /* access modifiers changed from: private */
    public TokenService mTokenService;
    /* access modifiers changed from: private */
    public String mUserId;
    private SSASession session;
    /* access modifiers changed from: private */
    public IronSourceWebView wvc;

    private IronSourceAdsPublisherAgent(Activity activity, int i) {
        initPublisherAgent(activity);
    }

    IronSourceAdsPublisherAgent(String str, String str2, Activity activity) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        initPublisherAgent(activity);
    }

    private void initPublisherAgent(Activity activity) {
        this.mTokenService = createToken(activity);
        this.mCommandExecutor = new CommandExecutor();
        IronSourceSharedPrefHelper.getSupersonicPrefHelper(activity);
        this.mDemandSourceManager = new DemandSourceManager();
        Logger.enableLogging(SDKUtils.getDebugMode());
        Logger.i(TAG, "C'tor");
        mutableContextWrapper = new MutableContextWrapper(activity);
        this.adViewContainerCounter = 0;
        createWebView(activity);
        startSession(activity);
    }

    private TokenService createToken(Activity activity) {
        TokenService tokenService = new TokenService();
        tokenService.collectApplicationUserId(this.mUserId);
        tokenService.collectApplicationKey(this.mApplicationKey);
        tokenService.collectAdvertisingID(activity);
        tokenService.collectDataFromActivity(activity);
        tokenService.collectDataFromDevice(activity);
        tokenService.collectDataFromControllerConfig(SDKUtils.getControllerConfig());
        tokenService.collectDataFromExternalParams(SDKUtils.getInitSDKParams());
        collectQaParameters(tokenService);
        return tokenService;
    }

    private void collectQaParameters(TokenService tokenService) {
        if (IronSourceQaProperties.isInitialized()) {
            tokenService.collectDataFromExternalParams(IronSourceQaProperties.getInstance().getParameters());
        }
    }

    private void createWebView(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc = new IronSourceWebView(IronSourceAdsPublisherAgent.mutableContextWrapper, IronSourceAdsPublisherAgent.this.mDemandSourceManager);
                IronSourceAdsPublisherAgent.this.wvc.addTokenJSInterface(new TokenJSAdapter(IronSourceAdsPublisherAgent.this.mTokenService));
                IronSourceAdsPublisherAgent.this.wvc.addMoatJSInterface(new MOATJSAdapter(activity.getApplication()));
                IronSourceAdsPublisherAgent.this.wvc.addPermissionsJSInterface(new PermissionsJSAdapter(activity.getApplicationContext()));
                IronSourceAdsPublisherAgent.this.mBannerJSAdapter = new BannerJSAdapter();
                IronSourceAdsPublisherAgent.this.mBannerJSAdapter.setCommunicationWithController(IronSourceAdsPublisherAgent.this.wvc.getControllerDelegate());
                IronSourceAdsPublisherAgent.this.wvc.addBannerJSInterface(IronSourceAdsPublisherAgent.this.mBannerJSAdapter);
                IronSourceAdsPublisherAgent.this.wvc.registerConnectionReceiver(activity);
                IronSourceAdsPublisherAgent.this.wvc.setDebugMode(SDKUtils.getDebugMode());
                IronSourceAdsPublisherAgent.this.wvc.downloadController();
                IronSourceAdsPublisherAgent.this.mCommandExecutor.setReady();
                IronSourceAdsPublisherAgent.this.mCommandExecutor.purgeDelayedCommands();
            }
        });
    }

    public static IronSourceNetworkAPI createInstance(Activity activity, String str, String str2) {
        return getInstance(str, str2, activity);
    }

    public static synchronized IronSourceNetworkAPI getInstance(String str, String str2, Activity activity) {
        IronSourceAdsPublisherAgent ironSourceAdsPublisherAgent;
        synchronized (IronSourceAdsPublisherAgent.class) {
            if (sInstance == null) {
                sInstance = new IronSourceAdsPublisherAgent(str, str2, activity);
            } else {
                mutableContextWrapper.setBaseContext(activity);
                sInstance.updateApplicationKey(str);
                sInstance.updateUserId(str2);
            }
            ironSourceAdsPublisherAgent = sInstance;
        }
        return ironSourceAdsPublisherAgent;
    }

    private void updateApplicationKey(String str) {
        this.mTokenService.collectApplicationKey(str);
    }

    private void updateUserId(String str) {
        this.mTokenService.collectApplicationUserId(str);
    }

    public static synchronized IronSourceAdsPublisherAgent getInstance(Activity activity) throws Exception {
        IronSourceAdsPublisherAgent instance;
        synchronized (IronSourceAdsPublisherAgent.class) {
            instance = getInstance(activity, 0);
        }
        return instance;
    }

    public static synchronized IronSourceAdsPublisherAgent getInstance(Activity activity, int i) throws Exception {
        IronSourceAdsPublisherAgent ironSourceAdsPublisherAgent;
        synchronized (IronSourceAdsPublisherAgent.class) {
            Logger.i(TAG, "getInstance()");
            if (sInstance == null) {
                sInstance = new IronSourceAdsPublisherAgent(activity, i);
            } else {
                mutableContextWrapper.setBaseContext(activity);
            }
            ironSourceAdsPublisherAgent = sInstance;
        }
        return ironSourceAdsPublisherAgent;
    }

    public IronSourceWebView getWebViewController() {
        return this.wvc;
    }

    private OnRewardedVideoListener getAdProductListenerAsRVListener(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnRewardedVideoListener) demandSource.getListener();
    }

    private OnInterstitialListener getAdProductListenerAsISListener(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnInterstitialListener) demandSource.getListener();
    }

    private OnBannerListener getAdProductListenerAsBNListener(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnBannerListener) demandSource.getListener();
    }

    private void startSession(Context context) {
        this.session = new SSASession(context, SessionType.launched);
    }

    public void resumeSession(Context context) {
        this.session = new SSASession(context, SessionType.backFromBG);
    }

    private void endSession() {
        if (this.session != null) {
            this.session.endSession();
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().addSession(this.session);
            this.session = null;
        }
    }

    public void initRewardedVideo(String str, String str2, String str3, Map<String, String> map, OnRewardedVideoListener onRewardedVideoListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.wvc.initRewardedVideo(str, str2, this.mDemandSourceManager.createDemandSource(ProductType.RewardedVideo, str3, map, onRewardedVideoListener), this);
    }

    public void showRewardedVideo(JSONObject jSONObject) {
        this.wvc.showRewardedVideo(jSONObject);
    }

    public void initOfferWall(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.wvc.initOfferWall(str, str2, map, onOfferWallListener);
    }

    public void initOfferWall(Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        this.wvc.initOfferWall(this.mApplicationKey, this.mUserId, map, onOfferWallListener);
    }

    public void showOfferWall(Map<String, String> map) {
        this.wvc.showOfferWall(map);
    }

    public void getOfferWallCredits(String str, String str2, OnOfferWallListener onOfferWallListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.wvc.getOfferWallCredits(str, str2, onOfferWallListener);
    }

    public void getOfferWallCredits(OnOfferWallListener onOfferWallListener) {
        this.wvc.getOfferWallCredits(this.mApplicationKey, this.mUserId, onOfferWallListener);
    }

    public void initInterstitial(String str, String str2, String str3, Map<String, String> map, OnInterstitialListener onInterstitialListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.wvc.initInterstitial(str, str2, this.mDemandSourceManager.createDemandSource(ProductType.Interstitial, str3, map, onInterstitialListener), this);
    }

    public void loadInterstitial(JSONObject jSONObject) {
        if (jSONObject != null) {
            String optString = jSONObject.optString("demandSourceName");
            if (!TextUtils.isEmpty(optString)) {
                this.wvc.loadInterstitial(optString);
            }
        }
    }

    public boolean isInterstitialAdAvailable(String str) {
        return this.wvc.isInterstitialAdAvailable(str);
    }

    public void showInterstitial(JSONObject jSONObject) {
        this.wvc.showInterstitial(jSONObject);
    }

    public void initBanner(String str, String str2, String str3, Map<String, String> map, OnBannerListener onBannerListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.wvc.initBanner(str, str2, this.mDemandSourceManager.createDemandSource(ProductType.Banner, str3, map, onBannerListener), this);
    }

    public void initBanner(String str, Map<String, String> map, OnBannerListener onBannerListener) {
        this.wvc.initBanner(this.mApplicationKey, this.mUserId, this.mDemandSourceManager.createDemandSource(ProductType.Banner, str, map, onBannerListener), this);
    }

    public void loadBanner(JSONObject jSONObject) {
        if (jSONObject != null) {
            this.wvc.loadBanner(jSONObject);
        }
    }

    public void onResume(Activity activity) {
        mutableContextWrapper.setBaseContext(activity);
        this.wvc.enterForeground();
        this.wvc.registerConnectionReceiver(activity);
        if (this.session == null) {
            resumeSession(activity);
        }
    }

    public void onPause(Activity activity) {
        try {
            this.wvc.enterBackground();
            this.wvc.unregisterConnectionReceiver(activity);
            endSession();
        } catch (Exception e) {
            e.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            StringBuilder sb = new StringBuilder();
            sb.append(Constants.NATIVE_EXCEPTION_BASE_URL);
            sb.append(e.getStackTrace()[0].getMethodName());
            ironSourceAsyncHttpRequestTask.execute(new String[]{sb.toString()});
        }
    }

    public void release(Activity activity) {
        try {
            Logger.i(TAG, "release()");
            DeviceProperties.release();
            this.wvc.unregisterConnectionReceiver(activity);
            if (Looper.getMainLooper().equals(Looper.myLooper())) {
                this.wvc.destroy();
                this.wvc = null;
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        IronSourceAdsPublisherAgent.this.wvc.destroy();
                        IronSourceAdsPublisherAgent.this.wvc = null;
                    }
                });
            }
        } catch (Exception unused) {
        }
        sInstance = null;
        endSession();
    }

    public void onAdProductInitSuccess(ProductType productType, String str, AdUnitsReady adUnitsReady) {
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName != null) {
            demandSourceByName.setDemandSourceInitState(2);
            if (productType == ProductType.RewardedVideo) {
                OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
                if (adProductListenerAsRVListener != null) {
                    adProductListenerAsRVListener.onRVInitSuccess(adUnitsReady);
                }
            } else if (productType == ProductType.Interstitial) {
                OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
                if (adProductListenerAsISListener != null) {
                    adProductListenerAsISListener.onInterstitialInitSuccess();
                }
            } else if (productType == ProductType.Banner) {
                OnBannerListener adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName);
                if (adProductListenerAsBNListener != null) {
                    adProductListenerAsBNListener.onBannerInitSuccess();
                }
            }
        }
    }

    public void onAdProductInitFailed(ProductType productType, String str, String str2) {
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName != null) {
            demandSourceByName.setDemandSourceInitState(3);
            if (productType == ProductType.RewardedVideo) {
                OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
                if (adProductListenerAsRVListener != null) {
                    adProductListenerAsRVListener.onRVInitFail(str2);
                }
            } else if (productType == ProductType.Interstitial) {
                OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
                if (adProductListenerAsISListener != null) {
                    adProductListenerAsISListener.onInterstitialInitFailed(str2);
                }
            } else if (productType == ProductType.Banner) {
                OnBannerListener adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName);
                if (adProductListenerAsBNListener != null) {
                    adProductListenerAsBNListener.onBannerInitFailed(str2);
                }
            }
        }
    }

    public void onRVNoMoreOffers(String str) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.RewardedVideo, str);
        if (demandSourceByName != null) {
            OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
            if (adProductListenerAsRVListener != null) {
                adProductListenerAsRVListener.onRVNoMoreOffers();
            }
        }
    }

    public void onRVAdCredited(String str, int i) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.RewardedVideo, str);
        if (demandSourceByName != null) {
            OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
            if (adProductListenerAsRVListener != null) {
                adProductListenerAsRVListener.onRVAdCredited(i);
            }
        }
    }

    public void onAdProductClose(ProductType productType, String str) {
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName == null) {
            return;
        }
        if (productType == ProductType.RewardedVideo) {
            OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
            if (adProductListenerAsRVListener != null) {
                adProductListenerAsRVListener.onRVAdClosed();
            }
        } else if (productType == ProductType.Interstitial) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialClose();
            }
        }
    }

    public void onRVShowFail(String str, String str2) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.RewardedVideo, str);
        if (demandSourceByName != null) {
            OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
            if (adProductListenerAsRVListener != null) {
                adProductListenerAsRVListener.onRVShowFail(str2);
            }
        }
    }

    public void onAdProductClick(ProductType productType, String str) {
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName == null) {
            return;
        }
        if (productType == ProductType.RewardedVideo) {
            OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
            if (adProductListenerAsRVListener != null) {
                adProductListenerAsRVListener.onRVAdClicked();
            }
        } else if (productType == ProductType.Interstitial) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialClick();
            }
        } else if (productType == ProductType.Banner) {
            OnBannerListener adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName);
            if (adProductListenerAsBNListener != null) {
                adProductListenerAsBNListener.onBannerClick();
            }
        }
    }

    public void onAdProductEventNotificationReceived(ProductType productType, String str, String str2, JSONObject jSONObject) {
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName != null) {
            try {
                if (productType == ProductType.Interstitial) {
                    OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
                    if (adProductListenerAsISListener != null) {
                        jSONObject.put("demandSourceName", str);
                        adProductListenerAsISListener.onInterstitialEventNotificationReceived(str2, jSONObject);
                    }
                } else if (productType == ProductType.RewardedVideo) {
                    OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
                    if (adProductListenerAsRVListener != null) {
                        jSONObject.put("demandSourceName", str);
                        adProductListenerAsRVListener.onRVEventNotificationReceived(str2, jSONObject);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onAdProductOpen(ProductType productType, String str) {
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName == null) {
            return;
        }
        if (productType == ProductType.Interstitial) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialOpen();
            }
        } else if (productType == ProductType.RewardedVideo) {
            OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
            if (adProductListenerAsRVListener != null) {
                adProductListenerAsRVListener.onRVAdOpened();
            }
        }
    }

    public void onInterstitialLoadSuccess(String str) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.Interstitial, str);
        if (demandSourceByName != null) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialLoadSuccess();
            }
        }
    }

    public void onInterstitialLoadFailed(String str, String str2) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.Interstitial, str);
        if (demandSourceByName != null) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialLoadFailed(str2);
            }
        }
    }

    public void onInterstitialShowSuccess(String str) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.Interstitial, str);
        if (demandSourceByName != null) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialShowSuccess();
            }
        }
    }

    public void onInterstitialShowFailed(String str, String str2) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.Interstitial, str);
        if (demandSourceByName != null) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialShowFailed(str2);
            }
        }
    }

    public void onInterstitialAdRewarded(String str, int i) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.Interstitial, str);
        OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
        if (demandSourceByName != null && adProductListenerAsISListener != null) {
            adProductListenerAsISListener.onInterstitialAdRewarded(str, i);
        }
    }

    private DemandSource getDemandSourceByName(ProductType productType, String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return this.mDemandSourceManager.getDemandSourceById(productType, str);
    }

    public void setMediationState(String str, String str2, int i) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            ProductType productType = SDKUtils.getProductType(str);
            if (productType != null) {
                DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(productType, str2);
                if (demandSourceById != null) {
                    demandSourceById.setMediationState(i);
                }
            }
        }
    }

    public void updateConsentInfo(final JSONObject jSONObject) {
        updateConsentInToken(jSONObject);
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.updateConsentInfo(jSONObject);
            }
        });
    }

    private void updateConsentInToken(JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.has(RequestParameters.GDPR_CONSENT_STATUS)) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put(RequestParameters.CONSENT, Boolean.valueOf(jSONObject.getString(RequestParameters.GDPR_CONSENT_STATUS)).booleanValue());
                this.mTokenService.updateData(jSONObject2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ISNAdView createBanner(Activity activity, ISAdSize iSAdSize) {
        StringBuilder sb = new StringBuilder();
        sb.append("SupersonicAds_");
        sb.append(this.adViewContainerCounter);
        String sb2 = sb.toString();
        this.adViewContainerCounter++;
        ISNAdView iSNAdView = new ISNAdView(activity, sb2, iSAdSize);
        this.mBannerJSAdapter.setCommunicationWithAdView(iSNAdView);
        return iSNAdView;
    }

    public void onBannerLoadSuccess(String str) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.Banner, str);
        if (demandSourceByName != null) {
            OnBannerListener adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName);
            if (adProductListenerAsBNListener != null) {
                adProductListenerAsBNListener.onBannerLoadSuccess();
            }
        }
    }

    public void onBannerLoadFail(String str, String str2) {
        DemandSource demandSourceByName = getDemandSourceByName(ProductType.Banner, str);
        if (demandSourceByName != null) {
            OnBannerListener adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName);
            if (adProductListenerAsBNListener != null) {
                adProductListenerAsBNListener.onBannerLoadFail(str2);
            }
        }
    }

    public void loadAd(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("loadAd ");
        sb.append(ironSourceAdInstance.getId());
        Logger.d(str, sb.toString());
        if (ironSourceAdInstance.isInAppBidding()) {
            loadInAppBiddingAd(ironSourceAdInstance, map);
        } else {
            loadInstance(ironSourceAdInstance, map);
        }
    }

    private void loadInAppBiddingAd(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        try {
            map = decodeADM(map);
        } catch (Exception e) {
            e.printStackTrace();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("loadInAppBiddingAd failed decoding ADM ");
            sb.append(e.getMessage());
            Logger.d(str, sb.toString());
        }
        loadInstance(ironSourceAdInstance, map);
    }

    private void loadInstance(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        if (ironSourceAdInstance.isInitialized()) {
            loadInitializedInstance(ironSourceAdInstance, map);
        } else {
            loadUninitializedInstance(ironSourceAdInstance, map);
        }
    }

    private Map<String, String> decodeADM(Map<String, String> map) {
        map.put(ParametersKeys.ADM, SDKUtils.decodeString((String) map.get(ParametersKeys.ADM)));
        return map;
    }

    private void loadInitializedInstance(final IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("loadOnInitializedInstance ");
        sb.append(ironSourceAdInstance.getId());
        Logger.d(str, sb.toString());
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                DemandSource demandSourceById = IronSourceAdsPublisherAgent.this.mDemandSourceManager.getDemandSourceById(ProductType.Interstitial, ironSourceAdInstance.getId());
                if (demandSourceById != null) {
                    IronSourceAdsPublisherAgent.this.wvc.loadInterstitial(demandSourceById, map);
                }
            }
        });
    }

    private void loadUninitializedInstance(final IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("loadOnNewInstance ");
        sb.append(ironSourceAdInstance.getId());
        Logger.d(str, sb.toString());
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                DemandSource createDemandSource = IronSourceAdsPublisherAgent.this.mDemandSourceManager.createDemandSource(ProductType.Interstitial, ironSourceAdInstance);
                IronSourceAdsPublisherAgent.this.wvc.initInterstitial(IronSourceAdsPublisherAgent.this.mApplicationKey, IronSourceAdsPublisherAgent.this.mUserId, createDemandSource, IronSourceAdsPublisherAgent.this);
                ironSourceAdInstance.setInitialized(true);
                IronSourceAdsPublisherAgent.this.wvc.loadInterstitial(createDemandSource, map);
            }
        });
    }

    public void showAd(IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("showAd ");
        sb.append(ironSourceAdInstance.getId());
        Logger.i(str, sb.toString());
        final DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(ProductType.Interstitial, ironSourceAdInstance.getId());
        if (demandSourceById != null) {
            this.mCommandExecutor.executeCommand(new Runnable() {
                public void run() {
                    IronSourceAdsPublisherAgent.this.wvc.showInterstitial(demandSourceById, map);
                }
            });
        }
    }

    public boolean isAdAvailable(IronSourceAdInstance ironSourceAdInstance) {
        if (this.wvc == null) {
            return false;
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("isAdAvailable ");
        sb.append(ironSourceAdInstance.getId());
        Logger.d(str, sb.toString());
        DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(ProductType.Interstitial, ironSourceAdInstance.getId());
        if (demandSourceById == null) {
            return false;
        }
        return demandSourceById.getAvailabilityState();
    }

    public String getToken(Context context) {
        if (this.mTokenService == null) {
            return null;
        }
        this.mTokenService.collectDataFromDevice(context);
        return this.mTokenService.getToken();
    }
}
