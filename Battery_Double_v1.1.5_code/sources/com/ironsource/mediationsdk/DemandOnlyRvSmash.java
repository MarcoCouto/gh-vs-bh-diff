package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyRvManagerListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class DemandOnlyRvSmash extends DemandOnlySmash implements RewardedVideoSmashListener {
    /* access modifiers changed from: private */
    public DemandOnlyRvManagerListener mListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;
    private int mLoadTimeoutSecs;
    /* access modifiers changed from: private */
    public SMASH_STATE mState;
    private Timer mTimer;

    public void onRewardedVideoAdEnded() {
    }

    public void onRewardedVideoAdStarted() {
    }

    public void onRewardedVideoAvailabilityChanged(boolean z) {
    }

    public void onRewardedVideoInitFailed(IronSourceError ironSourceError) {
    }

    public void onRewardedVideoInitSuccess() {
    }

    DemandOnlyRvSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, DemandOnlyRvManagerListener demandOnlyRvManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getInterstitialSettings()), abstractAdapter);
        this.mAdapterConfig = new AdapterConfig(providerSettings, providerSettings.getRewardedVideoSettings());
        this.mAdUnitSettings = this.mAdapterConfig.getAdUnitSetings();
        this.mAdapter = abstractAdapter;
        this.mListener = demandOnlyRvManagerListener;
        this.mTimer = null;
        this.mLoadTimeoutSecs = i;
        this.mState = SMASH_STATE.NOT_LOADED;
        this.mAdapter.initRvForDemandOnly(activity, str, str2, this.mAdUnitSettings, this);
    }

    public void loadRewardedVideo() {
        StringBuilder sb = new StringBuilder();
        sb.append("loadRewardedVideo state=");
        sb.append(this.mState.name());
        logInternal(sb.toString());
        if (this.mState == SMASH_STATE.NOT_LOADED || this.mState == SMASH_STATE.LOADED) {
            this.mState = SMASH_STATE.LOAD_IN_PROGRESS;
            startTimer();
            this.mLoadStartTime = new Date().getTime();
            this.mAdapter.loadVideoForDemandOnly(this.mAdUnitSettings, this);
        } else if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_ALREADY_IN_PROGRESS, "load already in progress"), this, 0);
        } else {
            this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_ALREADY_IN_PROGRESS, "cannot load because show is in progress"), this, 0);
        }
    }

    public void showRewardedVideo() {
        StringBuilder sb = new StringBuilder();
        sb.append("showRewardedVideo state=");
        sb.append(this.mState.name());
        logInternal(sb.toString());
        if (this.mState == SMASH_STATE.LOADED) {
            this.mState = SMASH_STATE.SHOW_IN_PROGRESS;
            this.mAdapter.showRewardedVideo(this.mAdUnitSettings, this);
            return;
        }
        this.mListener.onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_CALL_LOAD_BEFORE_SHOW, "load must be called before show"), this);
    }

    public boolean isRewardedVideoAvailable() {
        return this.mAdapter.isRewardedVideoAvailable(this.mAdUnitSettings);
    }

    private void stopTimer() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }

    private void startTimer() {
        logInternal("start timer");
        stopTimer();
        this.mTimer = new Timer();
        this.mTimer.schedule(new TimerTask() {
            public void run() {
                DemandOnlyRvSmash demandOnlyRvSmash = DemandOnlyRvSmash.this;
                StringBuilder sb = new StringBuilder();
                sb.append("load timed out state=");
                sb.append(DemandOnlyRvSmash.this.mState.toString());
                demandOnlyRvSmash.logInternal(sb.toString());
                if (DemandOnlyRvSmash.this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
                    DemandOnlyRvSmash.this.mState = SMASH_STATE.NOT_LOADED;
                    DemandOnlyRvSmash.this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_TIMED_OUT, "load timed out"), DemandOnlyRvSmash.this, new Date().getTime() - DemandOnlyRvSmash.this.mLoadStartTime);
                }
            }
        }, (long) (this.mLoadTimeoutSecs * 1000));
    }

    public void onRewardedVideoLoadSuccess() {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoLoadSuccess state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mState = SMASH_STATE.LOADED;
            this.mListener.onRewardedVideoLoadSuccess(this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onRewardedVideoLoadFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoLoadFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        sb.append(" state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mState = SMASH_STATE.NOT_LOADED;
            this.mListener.onRewardedVideoAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onRewardedVideoAdOpened() {
        logAdapterCallback("onRewardedVideoAdOpened");
        this.mListener.onRewardedVideoAdOpened(this);
    }

    public void onRewardedVideoAdClosed() {
        this.mState = SMASH_STATE.NOT_LOADED;
        logAdapterCallback("onRewardedVideoAdClosed");
        this.mListener.onRewardedVideoAdClosed(this);
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
        this.mState = SMASH_STATE.NOT_LOADED;
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdClosed error=");
        sb.append(ironSourceError);
        logAdapterCallback(sb.toString());
        this.mListener.onRewardedVideoAdShowFailed(ironSourceError, this);
    }

    public void onRewardedVideoAdClicked() {
        logAdapterCallback("onRewardedVideoAdClicked");
        this.mListener.onRewardedVideoAdClicked(this);
    }

    public void onRewardedVideoAdVisible() {
        logAdapterCallback("onRewardedVideoAdVisible");
        this.mListener.onRewardedVideoAdVisible(this);
    }

    public void onRewardedVideoAdRewarded() {
        logAdapterCallback("onRewardedVideoAdRewarded");
        this.mListener.onRewardedVideoAdRewarded(this);
    }

    private void logAdapterCallback(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyRewardedVideoSmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.ADAPTER_CALLBACK, sb.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyRewardedVideoSmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }
}
