package com.ironsource.mediationsdk;

import android.app.Activity;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.RewardedVideoManagerListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashApi;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class RewardedVideoSmash extends AbstractSmash implements RewardedVideoSmashListener, RewardedVideoSmashApi {
    private final String REQUEST_URL_KEY = IronSourceConstants.REQUEST_URL;
    /* access modifiers changed from: private */
    public long mLoadStartTime;
    private String mRequestUrl;
    private JSONObject mRewardedVideoAdapterConfigs;
    /* access modifiers changed from: private */
    public RewardedVideoManagerListener mRewardedVideoManagerListener;
    /* access modifiers changed from: private */
    public AtomicBoolean mShouldSendLoadEvents;
    private int mTimeout;

    /* access modifiers changed from: protected */
    public String getAdUnitString() {
        return "rewardedvideo";
    }

    public void onRewardedVideoInitFailed(IronSourceError ironSourceError) {
    }

    public void onRewardedVideoInitSuccess() {
    }

    /* access modifiers changed from: 0000 */
    public void startLoadTimer() {
    }

    RewardedVideoSmash(ProviderSettings providerSettings, int i) {
        super(providerSettings);
        this.mRewardedVideoAdapterConfigs = providerSettings.getRewardedVideoSettings();
        this.mMaxAdsPerIteration = this.mRewardedVideoAdapterConfigs.optInt("maxAdsPerIteration", 99);
        this.mMaxAdsPerSession = this.mRewardedVideoAdapterConfigs.optInt("maxAdsPerSession", 99);
        this.mMaxAdsPerDay = this.mRewardedVideoAdapterConfigs.optInt("maxAdsPerDay", 99);
        this.mRequestUrl = this.mRewardedVideoAdapterConfigs.optString(IronSourceConstants.REQUEST_URL);
        this.mShouldSendLoadEvents = new AtomicBoolean(false);
        this.mTimeout = i;
    }

    /* access modifiers changed from: 0000 */
    public void completeIteration() {
        this.mIterationShowCounter = 0;
        setMediationState(isRewardedVideoAvailable() ? MEDIATION_STATE.AVAILABLE : MEDIATION_STATE.NOT_AVAILABLE);
    }

    /* access modifiers changed from: 0000 */
    public void startInitTimer() {
        try {
            stopInitTimer();
            this.mInitTimer = new Timer();
            this.mInitTimer.schedule(new TimerTask() {
                public void run() {
                    synchronized (RewardedVideoSmash.this) {
                        cancel();
                        if (RewardedVideoSmash.this.mRewardedVideoManagerListener != null) {
                            IronSourceLoggerManager ironSourceLoggerManager = RewardedVideoSmash.this.mLoggerManager;
                            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Timeout for ");
                            sb.append(RewardedVideoSmash.this.getInstanceName());
                            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
                            RewardedVideoSmash.this.setMediationState(MEDIATION_STATE.NOT_AVAILABLE);
                            if (RewardedVideoSmash.this.mShouldSendLoadEvents.compareAndSet(true, false)) {
                                long time = new Date().getTime() - RewardedVideoSmash.this.mLoadStartTime;
                                RewardedVideoSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1025)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
                            } else {
                                RewardedVideoSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE);
                            }
                            RewardedVideoSmash.this.mRewardedVideoManagerListener.onRewardedVideoAvailabilityChanged(false, RewardedVideoSmash.this);
                        }
                    }
                }
            }, (long) (this.mTimeout * 1000));
        } catch (Exception e) {
            logException("startInitTimer", e.getLocalizedMessage());
        }
    }

    public void initRewardedVideo(Activity activity, String str, String str2) {
        startInitTimer();
        if (this.mAdapter != null) {
            this.mShouldSendLoadEvents.set(true);
            this.mLoadStartTime = new Date().getTime();
            this.mAdapter.addRewardedVideoListener(this);
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getInstanceName());
            sb.append(":initRewardedVideo()");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            this.mAdapter.initRewardedVideo(activity, str, str2, this.mRewardedVideoAdapterConfigs, this);
        }
    }

    public void fetchRewardedVideo() {
        if (this.mAdapter != null) {
            if (!(getMediationState() == MEDIATION_STATE.CAPPED_PER_DAY || getMediationState() == MEDIATION_STATE.CAPPED_PER_SESSION)) {
                this.mShouldSendLoadEvents.set(true);
                this.mLoadStartTime = new Date().getTime();
            }
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getInstanceName());
            sb.append(":fetchRewardedVideo()");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            this.mAdapter.fetchRewardedVideo(this.mRewardedVideoAdapterConfigs);
        }
    }

    public void showRewardedVideo() {
        if (this.mAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getInstanceName());
            sb.append(":showRewardedVideo()");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            preShow();
            this.mAdapter.showRewardedVideo(this.mRewardedVideoAdapterConfigs, this);
        }
    }

    public boolean isRewardedVideoAvailable() {
        if (this.mAdapter == null) {
            return false;
        }
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
        StringBuilder sb = new StringBuilder();
        sb.append(getInstanceName());
        sb.append(":isRewardedVideoAvailable()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        return this.mAdapter.isRewardedVideoAvailable(this.mRewardedVideoAdapterConfigs);
    }

    public void setRewardedVideoManagerListener(RewardedVideoManagerListener rewardedVideoManagerListener) {
        this.mRewardedVideoManagerListener = rewardedVideoManagerListener;
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
        if (this.mRewardedVideoManagerListener != null) {
            this.mRewardedVideoManagerListener.onRewardedVideoAdShowFailed(ironSourceError, this);
        }
    }

    public void onRewardedVideoAdOpened() {
        if (this.mRewardedVideoManagerListener != null) {
            this.mRewardedVideoManagerListener.onRewardedVideoAdOpened(this);
        }
    }

    public void onRewardedVideoAdClosed() {
        if (this.mRewardedVideoManagerListener != null) {
            this.mRewardedVideoManagerListener.onRewardedVideoAdClosed(this);
        }
        fetchRewardedVideo();
    }

    public synchronized void onRewardedVideoAvailabilityChanged(boolean z) {
        stopInitTimer();
        if (this.mShouldSendLoadEvents.compareAndSet(true, false)) {
            sendProviderEvent(z ? 1002 : IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(new Date().getTime() - this.mLoadStartTime)}});
        } else {
            sendProviderEvent(z ? IronSourceConstants.RV_INSTANCE_AVAILABILITY_TRUE : IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE);
        }
        if (isMediationAvailable() && ((z && this.mMediationState != MEDIATION_STATE.AVAILABLE) || (!z && this.mMediationState != MEDIATION_STATE.NOT_AVAILABLE))) {
            setMediationState(z ? MEDIATION_STATE.AVAILABLE : MEDIATION_STATE.NOT_AVAILABLE);
            if (this.mRewardedVideoManagerListener != null) {
                this.mRewardedVideoManagerListener.onRewardedVideoAvailabilityChanged(z, this);
            }
        }
    }

    public void onRewardedVideoAdStarted() {
        if (this.mRewardedVideoManagerListener != null) {
            this.mRewardedVideoManagerListener.onRewardedVideoAdStarted(this);
        }
    }

    public void onRewardedVideoAdEnded() {
        if (this.mRewardedVideoManagerListener != null) {
            this.mRewardedVideoManagerListener.onRewardedVideoAdEnded(this);
        }
    }

    public void onRewardedVideoAdRewarded() {
        if (this.mRewardedVideoManagerListener != null) {
            this.mRewardedVideoManagerListener.onRewardedVideoAdRewarded(this);
        }
    }

    public void onRewardedVideoAdClicked() {
        if (this.mRewardedVideoManagerListener != null) {
            this.mRewardedVideoManagerListener.onRewardedVideoAdClicked(this);
        }
    }

    public void onRewardedVideoAdVisible() {
        if (this.mRewardedVideoManagerListener != null) {
            this.mRewardedVideoManagerListener.onRewardedVideoAdVisible(this);
        }
    }

    public void onRewardedVideoLoadSuccess() {
        sendProviderEvent(1002, null);
    }

    public void onRewardedVideoLoadFailed(IronSourceError ironSourceError) {
        if (this.mShouldSendLoadEvents.compareAndSet(false, true)) {
            long time = new Date().getTime() - this.mLoadStartTime;
            sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
            return;
        }
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE);
    }

    /* access modifiers changed from: private */
    public void sendProviderEvent(int i) {
        sendProviderEvent(i, null);
    }

    /* access modifiers changed from: private */
    public void sendProviderEvent(int i, Object[][] objArr) {
        JSONObject providerAdditionalData = IronSourceUtils.getProviderAdditionalData((AbstractSmash) this);
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerAdditionalData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("RewardedVideoSmash logProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, providerAdditionalData));
    }

    /* access modifiers changed from: 0000 */
    public String getRequestUrl() {
        return this.mRequestUrl;
    }
}
