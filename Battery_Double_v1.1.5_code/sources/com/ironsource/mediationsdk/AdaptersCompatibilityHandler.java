package com.ironsource.mediationsdk;

import com.applovin.sdk.AppLovinMediationProvider;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.smaato.sdk.core.api.VideoType;
import io.realm.BuildConfig;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AdaptersCompatibilityHandler {
    private static final AdaptersCompatibilityHandler instance = new AdaptersCompatibilityHandler();
    private ConcurrentHashMap<String, String> mAdapterNameToMinIsVersion = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, String> mAdapterNameToMinRvVersion;

    public static synchronized AdaptersCompatibilityHandler getInstance() {
        AdaptersCompatibilityHandler adaptersCompatibilityHandler;
        synchronized (AdaptersCompatibilityHandler.class) {
            adaptersCompatibilityHandler = instance;
        }
        return adaptersCompatibilityHandler;
    }

    private AdaptersCompatibilityHandler() {
        this.mAdapterNameToMinIsVersion.put("adcolony", "4.1.6");
        this.mAdapterNameToMinIsVersion.put("vungle", "4.1.5");
        this.mAdapterNameToMinIsVersion.put("applovin", "4.3.3");
        this.mAdapterNameToMinIsVersion.put(AppLovinMediationProvider.ADMOB, "4.3.2");
        this.mAdapterNameToMinRvVersion = new ConcurrentHashMap<>();
        this.mAdapterNameToMinRvVersion.put("adcolony", "4.1.6");
        this.mAdapterNameToMinRvVersion.put(AppLovinMediationProvider.ADMOB, "4.3.2");
        this.mAdapterNameToMinRvVersion.put("applovin", "4.3.3");
        this.mAdapterNameToMinRvVersion.put("chartboost", "4.1.9");
        this.mAdapterNameToMinRvVersion.put(AppLovinMediationProvider.FYBER, BuildConfig.VERSION_NAME);
        this.mAdapterNameToMinRvVersion.put("hyprmx", "4.1.2");
        this.mAdapterNameToMinRvVersion.put(com.integralads.avid.library.inmobi.BuildConfig.SDK_NAME, "4.3.1");
        this.mAdapterNameToMinRvVersion.put("maio", "4.1.3");
        this.mAdapterNameToMinRvVersion.put("mediabrix", "4.1.1");
        this.mAdapterNameToMinRvVersion.put(AppLovinMediationProvider.MOPUB, "3.2.0");
        this.mAdapterNameToMinRvVersion.put("tapjoy", "4.0.0");
        this.mAdapterNameToMinRvVersion.put("unityads", "4.1.4");
        this.mAdapterNameToMinRvVersion.put("vungle", "4.1.5");
    }

    public synchronized boolean isAdapterVersionRVCompatible(AbstractAdapter abstractAdapter) {
        return isAdapterVersionCompatible(abstractAdapter, this.mAdapterNameToMinRvVersion, "rewarded video");
    }

    public synchronized boolean isAdapterVersionISCompatible(AbstractAdapter abstractAdapter) {
        return isAdapterVersionCompatible(abstractAdapter, this.mAdapterNameToMinIsVersion, VideoType.INTERSTITIAL);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0063, code lost:
        return r4;
     */
    private synchronized boolean isAdapterVersionCompatible(AbstractAdapter abstractAdapter, Map<String, String> map, String str) {
        if (abstractAdapter == null) {
            return false;
        }
        String lowerCase = abstractAdapter.getProviderName().toLowerCase();
        if (!map.containsKey(lowerCase)) {
            return true;
        }
        String str2 = (String) map.get(lowerCase);
        String version = abstractAdapter.getVersion();
        boolean isVersionGreaterOrEqual = isVersionGreaterOrEqual(str2, version);
        if (!isVersionGreaterOrEqual) {
            StringBuilder sb = new StringBuilder();
            sb.append(abstractAdapter.getProviderName());
            sb.append(" adapter ");
            sb.append(version);
            sb.append(" is incompatible with SDK version ");
            sb.append(IronSourceUtils.getSDKVersion());
            sb.append(" for ");
            sb.append(str);
            sb.append(" ad unit, please update your adapter to the latest version");
            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, sb.toString(), 3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0046, code lost:
        return r1;
     */
    public synchronized boolean isBannerAdapterCompatible(AbstractAdapter abstractAdapter) {
        if (abstractAdapter == null) {
            return false;
        }
        String version = abstractAdapter.getVersion();
        boolean isVersionGreaterOrEqual = isVersionGreaterOrEqual("4.3.0", version);
        if (!isVersionGreaterOrEqual) {
            StringBuilder sb = new StringBuilder();
            sb.append(abstractAdapter.getProviderName());
            sb.append(" adapter ");
            sb.append(version);
            sb.append(" is incompatible with SDK version ");
            sb.append(IronSourceUtils.getSDKVersion());
            sb.append(", please update your adapter to the latest version");
            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, sb.toString(), 3);
        }
    }

    private boolean isVersionGreaterOrEqual(String str, String str2) {
        if (str.equalsIgnoreCase(str2)) {
            return true;
        }
        String[] split = str.split("\\.");
        String[] split2 = str2.split("\\.");
        for (int i = 0; i < 3; i++) {
            int parseInt = Integer.parseInt(split[i]);
            int parseInt2 = Integer.parseInt(split2[i]);
            if (parseInt2 < parseInt) {
                return false;
            }
            if (parseInt2 > parseInt) {
                return true;
            }
        }
        return true;
    }
}
