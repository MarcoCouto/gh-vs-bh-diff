package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.InterstitialManagerListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashApi;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialManagerListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialSmashApi;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.smaato.sdk.core.api.VideoType;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class InterstitialSmash extends AbstractSmash implements InterstitialSmashListener, RewardedInterstitialListener, InterstitialSmashApi, RewardedInterstitialSmashApi {
    private JSONObject mInterstitialAdapterConfigs;
    /* access modifiers changed from: private */
    public InterstitialManagerListener mInterstitialManagerListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;
    private RewardedInterstitialManagerListener mRewardedInterstitialManagerListener;
    private int mTimeout;

    /* access modifiers changed from: protected */
    public String getAdUnitString() {
        return VideoType.INTERSTITIAL;
    }

    InterstitialSmash(ProviderSettings providerSettings, int i) {
        super(providerSettings);
        this.mInterstitialAdapterConfigs = providerSettings.getInterstitialSettings();
        this.mMaxAdsPerIteration = this.mInterstitialAdapterConfigs.optInt("maxAdsPerIteration", 99);
        this.mMaxAdsPerSession = this.mInterstitialAdapterConfigs.optInt("maxAdsPerSession", 99);
        this.mMaxAdsPerDay = this.mInterstitialAdapterConfigs.optInt("maxAdsPerDay", 99);
        this.mIsMultipleInstances = providerSettings.isMultipleInstances();
        this.mSpId = providerSettings.getSubProviderId();
        this.mTimeout = i;
    }

    /* access modifiers changed from: 0000 */
    public void completeIteration() {
        this.mIterationShowCounter = 0;
        setMediationState(MEDIATION_STATE.INITIATED);
    }

    /* access modifiers changed from: 0000 */
    public void startInitTimer() {
        try {
            stopInitTimer();
            this.mInitTimer = new Timer();
            this.mInitTimer.schedule(new TimerTask() {
                public void run() {
                    cancel();
                    if (InterstitialSmash.this.mMediationState == MEDIATION_STATE.INIT_PENDING && InterstitialSmash.this.mInterstitialManagerListener != null) {
                        InterstitialSmash.this.setMediationState(MEDIATION_STATE.INIT_FAILED);
                        InterstitialSmash.this.mInterstitialManagerListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("Timeout", "Interstitial"), InterstitialSmash.this);
                    }
                }
            }, (long) (this.mTimeout * 1000));
        } catch (Exception e) {
            logException("startInitTimer", e.getLocalizedMessage());
        }
    }

    /* access modifiers changed from: 0000 */
    public void startLoadTimer() {
        try {
            stopLoadTimer();
            this.mLoadTimer = new Timer();
            this.mLoadTimer.schedule(new TimerTask() {
                public void run() {
                    cancel();
                    if (InterstitialSmash.this.mMediationState == MEDIATION_STATE.LOAD_PENDING && InterstitialSmash.this.mInterstitialManagerListener != null) {
                        InterstitialSmash.this.setMediationState(MEDIATION_STATE.NOT_AVAILABLE);
                        InterstitialSmash.this.mInterstitialManagerListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("Timeout"), InterstitialSmash.this, new Date().getTime() - InterstitialSmash.this.mLoadStartTime);
                    }
                }
            }, (long) (this.mTimeout * 1000));
        } catch (Exception e) {
            logException("startLoadTimer", e.getLocalizedMessage());
        }
    }

    public void initInterstitial(Activity activity, String str, String str2) {
        startInitTimer();
        if (this.mAdapter != null) {
            this.mAdapter.addInterstitialListener(this);
            if (this.mRewardedInterstitialManagerListener != null) {
                this.mAdapter.setRewardedInterstitialListener(this);
            }
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getInstanceName());
            sb.append(":initInterstitial()");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            this.mAdapter.initInterstitial(activity, str, str2, this.mInterstitialAdapterConfigs, this);
        }
    }

    public void loadInterstitial() {
        startLoadTimer();
        if (this.mAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getInstanceName());
            sb.append(":loadInterstitial()");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            this.mLoadStartTime = new Date().getTime();
            this.mAdapter.loadInterstitial(this.mInterstitialAdapterConfigs, this);
        }
    }

    public void showInterstitial() {
        if (this.mAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getInstanceName());
            sb.append(":showInterstitial()");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            preShow();
            this.mAdapter.showInterstitial(this.mInterstitialAdapterConfigs, this);
        }
    }

    public boolean isInterstitialReady() {
        if (this.mAdapter == null) {
            return false;
        }
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
        StringBuilder sb = new StringBuilder();
        sb.append(getInstanceName());
        sb.append(":isInterstitialReady()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        return this.mAdapter.isInterstitialReady(this.mInterstitialAdapterConfigs);
    }

    public void setInterstitialManagerListener(InterstitialManagerListener interstitialManagerListener) {
        this.mInterstitialManagerListener = interstitialManagerListener;
    }

    public void setRewardedInterstitialManagerListener(RewardedInterstitialManagerListener rewardedInterstitialManagerListener) {
        this.mRewardedInterstitialManagerListener = rewardedInterstitialManagerListener;
    }

    public void onInterstitialInitSuccess() {
        stopInitTimer();
        if (this.mMediationState == MEDIATION_STATE.INIT_PENDING) {
            setMediationState(MEDIATION_STATE.INITIATED);
            if (this.mInterstitialManagerListener != null) {
                this.mInterstitialManagerListener.onInterstitialInitSuccess(this);
            }
        }
    }

    public void onInterstitialInitFailed(IronSourceError ironSourceError) {
        stopInitTimer();
        if (this.mMediationState == MEDIATION_STATE.INIT_PENDING) {
            setMediationState(MEDIATION_STATE.INIT_FAILED);
            if (this.mInterstitialManagerListener != null) {
                this.mInterstitialManagerListener.onInterstitialInitFailed(ironSourceError, this);
            }
        }
    }

    public void onInterstitialAdReady() {
        stopLoadTimer();
        if (this.mMediationState == MEDIATION_STATE.LOAD_PENDING && this.mInterstitialManagerListener != null) {
            this.mInterstitialManagerListener.onInterstitialAdReady(this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onInterstitialAdLoadFailed(IronSourceError ironSourceError) {
        stopLoadTimer();
        if (this.mMediationState == MEDIATION_STATE.LOAD_PENDING && this.mInterstitialManagerListener != null) {
            this.mInterstitialManagerListener.onInterstitialAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onInterstitialAdOpened() {
        if (this.mInterstitialManagerListener != null) {
            this.mInterstitialManagerListener.onInterstitialAdOpened(this);
        }
    }

    public void onInterstitialAdClosed() {
        if (this.mInterstitialManagerListener != null) {
            this.mInterstitialManagerListener.onInterstitialAdClosed(this);
        }
    }

    public void onInterstitialAdShowSucceeded() {
        if (this.mInterstitialManagerListener != null) {
            this.mInterstitialManagerListener.onInterstitialAdShowSucceeded(this);
        }
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError) {
        if (this.mInterstitialManagerListener != null) {
            this.mInterstitialManagerListener.onInterstitialAdShowFailed(ironSourceError, this);
        }
    }

    public void onInterstitialAdClicked() {
        if (this.mInterstitialManagerListener != null) {
            this.mInterstitialManagerListener.onInterstitialAdClicked(this);
        }
    }

    public void onInterstitialAdVisible() {
        if (this.mInterstitialManagerListener != null) {
            this.mInterstitialManagerListener.onInterstitialAdVisible(this);
        }
    }

    public void onInterstitialAdRewarded() {
        if (this.mRewardedInterstitialManagerListener != null) {
            this.mRewardedInterstitialManagerListener.onInterstitialAdRewarded(this);
        }
    }
}
