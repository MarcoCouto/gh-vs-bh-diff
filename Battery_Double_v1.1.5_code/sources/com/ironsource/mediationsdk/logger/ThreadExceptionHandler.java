package com.ironsource.mediationsdk.logger;

import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import java.lang.Thread.UncaughtExceptionHandler;

public class ThreadExceptionHandler implements UncaughtExceptionHandler {
    public void uncaughtException(Thread thread, Throwable th) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
        StringBuilder sb = new StringBuilder();
        sb.append("Thread name =");
        sb.append(thread.getName());
        logger.logException(ironSourceTag, sb.toString(), th);
    }
}
