package com.ironsource.mediationsdk.logger;

import android.util.Log;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TJAdUnitConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ServerLogger extends IronSourceLogger {
    public static final String NAME = "server";
    private final int SERVER_LOGS_SIZE_LIMIT = 1000;
    private ArrayList<ServerLogEntry> mLogs = new ArrayList<>();

    private class SendingCalc {
        private int DEFAULT_DEBUG_LEVEL = 3;
        private int DEFAULT_SIZE = 1;
        private int DEFAULT_TIME = 1;

        private boolean error(int i) {
            return i == 3;
        }

        private void initDefaults() {
        }

        private boolean size() {
            return false;
        }

        private boolean time() {
            return false;
        }

        public SendingCalc() {
            initDefaults();
        }

        public void notifyEvent(int i) {
            if (calc(i)) {
                ServerLogger.this.send();
            }
        }

        private boolean calc(int i) {
            if (!error(i) && !size() && !time()) {
                return false;
            }
            return true;
        }
    }

    public ServerLogger() {
        super(NAME);
    }

    public ServerLogger(int i) {
        super(NAME, i);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r2.mLogs = new java.util.ArrayList<>();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0038 */
    private synchronized void addLogEntry(ServerLogEntry serverLogEntry) {
        this.mLogs.add(serverLogEntry);
        if (shouldSendLogs()) {
            send();
        } else if (this.mLogs.size() > 1000) {
            ArrayList<ServerLogEntry> arrayList = new ArrayList<>();
            for (int i = TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL; i < this.mLogs.size(); i++) {
                arrayList.add(this.mLogs.get(i));
            }
            this.mLogs = arrayList;
        }
        return;
    }

    private boolean shouldSendLogs() {
        return ((ServerLogEntry) this.mLogs.get(this.mLogs.size() - 1)).getLogLevel() == 3;
    }

    /* access modifiers changed from: private */
    public void send() {
        IronSourceUtils.createAndStartWorker(new LogsSender(this.mLogs), "LogsSender");
        this.mLogs = new ArrayList<>();
    }

    private String getTimestamp() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
    }

    public synchronized void log(IronSourceTag ironSourceTag, String str, int i) {
        addLogEntry(new ServerLogEntry(ironSourceTag, getTimestamp(), str, i));
    }

    public synchronized void logException(IronSourceTag ironSourceTag, String str, Throwable th) {
        StringBuilder sb = new StringBuilder(str);
        if (th != null) {
            sb.append(":stacktrace[");
            sb.append(Log.getStackTraceString(th));
            sb.append(RequestParameters.RIGHT_BRACKETS);
        }
        addLogEntry(new ServerLogEntry(ironSourceTag, getTimestamp(), sb.toString(), 3));
    }
}
