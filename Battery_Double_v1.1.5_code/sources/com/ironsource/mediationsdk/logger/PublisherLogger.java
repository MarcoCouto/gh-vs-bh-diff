package com.ironsource.mediationsdk.logger;

import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;

public class PublisherLogger extends IronSourceLogger {
    private static final String NAME = "publisher";
    private LogListener mLogListener;

    private PublisherLogger() {
        super(NAME);
    }

    public PublisherLogger(LogListener logListener, int i) {
        super(NAME, i);
        this.mLogListener = logListener;
    }

    public synchronized void log(IronSourceTag ironSourceTag, String str, int i) {
        if (!(this.mLogListener == null || str == null)) {
            this.mLogListener.onLog(ironSourceTag, str, i);
        }
    }

    public void logException(IronSourceTag ironSourceTag, String str, Throwable th) {
        if (th != null) {
            log(ironSourceTag, th.getMessage(), 3);
        }
    }

    public void setLogListener(LogListener logListener) {
        this.mLogListener = logListener;
    }
}
