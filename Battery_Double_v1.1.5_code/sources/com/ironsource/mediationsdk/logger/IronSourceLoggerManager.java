package com.ironsource.mediationsdk.logger;

import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import java.util.ArrayList;
import java.util.Iterator;

public class IronSourceLoggerManager extends IronSourceLogger implements LogListener {
    private static IronSourceLoggerManager mInstance;
    private boolean mIsDebugEnabled = false;
    private ArrayList<IronSourceLogger> mLoggers = new ArrayList<>();

    private IronSourceLoggerManager(String str) {
        super(str);
        initSubLoggers();
    }

    private IronSourceLoggerManager(String str, int i) {
        super(str, i);
        initSubLoggers();
    }

    private void initSubLoggers() {
        this.mLoggers.add(new ConsoleLogger(1));
    }

    public static synchronized IronSourceLoggerManager getLogger() {
        IronSourceLoggerManager ironSourceLoggerManager;
        synchronized (IronSourceLoggerManager.class) {
            if (mInstance == null) {
                mInstance = new IronSourceLoggerManager(IronSourceLoggerManager.class.getSimpleName());
            }
            ironSourceLoggerManager = mInstance;
        }
        return ironSourceLoggerManager;
    }

    public static synchronized IronSourceLoggerManager getLogger(int i) {
        IronSourceLoggerManager ironSourceLoggerManager;
        synchronized (IronSourceLoggerManager.class) {
            if (mInstance == null) {
                mInstance = new IronSourceLoggerManager(IronSourceLoggerManager.class.getSimpleName());
            } else {
                mInstance.mDebugLevel = i;
            }
            ironSourceLoggerManager = mInstance;
        }
        return ironSourceLoggerManager;
    }

    public void addLogger(IronSourceLogger ironSourceLogger) {
        this.mLoggers.add(ironSourceLogger);
    }

    public synchronized void log(IronSourceTag ironSourceTag, String str, int i) {
        if (i >= this.mDebugLevel) {
            Iterator it = this.mLoggers.iterator();
            while (it.hasNext()) {
                IronSourceLogger ironSourceLogger = (IronSourceLogger) it.next();
                if (ironSourceLogger.getDebugLevel() <= i) {
                    ironSourceLogger.log(ironSourceTag, str, i);
                }
            }
        }
    }

    public synchronized void onLog(IronSourceTag ironSourceTag, String str, int i) {
        log(ironSourceTag, str, i);
    }

    public synchronized void logException(IronSourceTag ironSourceTag, String str, Throwable th) {
        if (th == null) {
            try {
                Iterator it = this.mLoggers.iterator();
                while (it.hasNext()) {
                    ((IronSourceLogger) it.next()).log(ironSourceTag, str, 3);
                }
            } finally {
            }
        } else {
            Iterator it2 = this.mLoggers.iterator();
            while (it2.hasNext()) {
                ((IronSourceLogger) it2.next()).logException(ironSourceTag, str, th);
            }
        }
    }

    private IronSourceLogger findLoggerByName(String str) {
        Iterator it = this.mLoggers.iterator();
        while (it.hasNext()) {
            IronSourceLogger ironSourceLogger = (IronSourceLogger) it.next();
            if (ironSourceLogger.getLoggerName().equals(str)) {
                return ironSourceLogger;
            }
        }
        return null;
    }

    public void setLoggerDebugLevel(String str, int i) {
        if (str != null) {
            IronSourceLogger findLoggerByName = findLoggerByName(str);
            if (findLoggerByName == null) {
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to find logger:setLoggerDebugLevel(loggerName:");
                sb.append(str);
                sb.append(" ,debugLevel:");
                sb.append(i);
                sb.append(")");
                log(ironSourceTag, sb.toString(), 0);
            } else if (i < 0 || i > 3) {
                this.mLoggers.remove(findLoggerByName);
            } else {
                IronSourceTag ironSourceTag2 = IronSourceTag.NATIVE;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("setLoggerDebugLevel(loggerName:");
                sb2.append(str);
                sb2.append(" ,debugLevel:");
                sb2.append(i);
                sb2.append(")");
                log(ironSourceTag2, sb2.toString(), 0);
                findLoggerByName.setDebugLevel(i);
            }
        }
    }

    public boolean isDebugEnabled() {
        return this.mIsDebugEnabled;
    }

    public void setAdaptersDebug(boolean z) {
        this.mIsDebugEnabled = z;
    }
}
