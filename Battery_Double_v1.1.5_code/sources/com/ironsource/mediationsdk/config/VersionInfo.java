package com.ironsource.mediationsdk.config;

public class VersionInfo {
    public static final String BUILD_DATE = "2019-07-31T10:56:41Z";
    public static final long BUILD_UNIX_TIME = 1564570601027L;
    public static final String GIT_DATE = "2019-07-31T10:51:38Z";
    public static final int GIT_REVISION = 4758;
    public static final String GIT_SHA = "710b355bc59f5267c089a2a6be84ff49600218a1";
    public static final String MAVEN_GROUP = "";
    public static final String MAVEN_NAME = "Android-SSP-unit-testing-develop-release-master-branches";
    public static final String VERSION = "6.9.1";
}
