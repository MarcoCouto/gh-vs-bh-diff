package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.IronSource.AD_UNIT;
import com.ironsource.mediationsdk.config.ConfigValidationResult;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.events.SuperLooper;
import com.ironsource.mediationsdk.logger.ConsoleLogger;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.logger.LogListener;
import com.ironsource.mediationsdk.logger.PublisherLogger;
import com.ironsource.mediationsdk.model.ApplicationEvents;
import com.ironsource.mediationsdk.model.BannerConfigurations;
import com.ironsource.mediationsdk.model.BannerPlacement;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.OfferwallPlacement;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyRewardedVideoListener;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.ironsource.mediationsdk.sdk.IronSourceInterface;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.OfferwallListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;
import com.ironsource.mediationsdk.sdk.SegmentListener;
import com.ironsource.mediationsdk.server.HttpFunctions;
import com.ironsource.mediationsdk.server.ServerURL;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceObject implements IronSourceInterface, OnMediationInitializationListener {
    private static IronSourceObject sInstance;
    private final String IRONSOURCE_VERSION_STRING = "!SDK-VERSION-STRING!:com.ironsource:mediationsdk:​6.9.1";
    private final String KEY_INIT_COUNTER = RequestParameters.SESSION_DEPTH;
    private final String TAG = getClass().getName();
    private Activity mActivity;
    private Set<AD_UNIT> mAdUnitsToInitialize;
    private String mAppKey = null;
    private AtomicBoolean mAtomicIsFirstInit;
    private ArrayList<AbstractAdapter> mBannerAdaptersList;
    private BannerManager mBannerManager;
    private IronSourceBannerLayout mBnLayoutToLoad;
    private String mBnPlacementToLoad;
    private Boolean mConsent = null;
    private ServerResponseWrapper mCurrentServerResponse = null;
    private CopyOnWriteArraySet<String> mDemandOnlyIsLoadBeforeInitCompleted;
    private DemandOnlyIsManager mDemandOnlyIsManager;
    private CopyOnWriteArraySet<String> mDemandOnlyRvLoadBeforeInitCompleted;
    private DemandOnlyRvManager mDemandOnlyRvManager;
    private boolean mDidInitBanner;
    private boolean mDidInitInterstitial;
    private boolean mDidInitRewardedVideo;
    private String mDynamicUserId = null;
    private AtomicBoolean mEventManagersInit;
    private int mInitCounter;
    private boolean mInitSucceeded = false;
    private List<AD_UNIT> mInitiatedAdUnits;
    private ArrayList<AbstractAdapter> mInterstitialAdaptersList;
    private InterstitialManager mInterstitialManager;
    private IronSourceSegment mIronSegment;
    private Boolean mIsBnLoadBeforeInitCompleted;
    private boolean mIsDemandOnlyIs;
    private boolean mIsDemandOnlyRv;
    private boolean mIsIsLoadBeforeInitCompleted;
    private boolean mIsIsProgrammatic;
    private boolean mIsRvProgrammatic;
    private ListenersWrapper mListenersWrapper;
    private IronSourceLoggerManager mLoggerManager;
    private String mMediationType = null;
    private AbstractAdapter mOfferwallAdapter;
    private OfferwallManager mOfferwallManager;
    private ProgIsManager mProgIsManager;
    private ProgRvManager mProgRvManager;
    private PublisherLogger mPublisherLogger;
    private Set<AD_UNIT> mRequestedAdUnits;
    private ArrayList<AbstractAdapter> mRewardedVideoAdaptersList;
    private RewardedVideoManager mRewardedVideoManager;
    private Map<String, String> mRvServerParams = null;
    private String mSegment = null;
    private final Object mServerResponseLocker = new Object();
    private String mSessionId = null;
    private boolean mShouldSendGetInstanceEvent = true;
    private Integer mUserAge = null;
    private String mUserGender = null;
    private String mUserId = null;

    public interface IResponseListener {
        void onUnrecoverableError(String str);
    }

    public void initInterstitial(Activity activity, String str, String str2) {
    }

    public void initOfferwall(Activity activity, String str, String str2) {
    }

    public void initRewardedVideo(Activity activity, String str, String str2) {
    }

    public static synchronized IronSourceObject getInstance() {
        IronSourceObject ironSourceObject;
        synchronized (IronSourceObject.class) {
            if (sInstance == null) {
                sInstance = new IronSourceObject();
            }
            ironSourceObject = sInstance;
        }
        return ironSourceObject;
    }

    private IronSourceObject() {
        initializeManagers();
        this.mEventManagersInit = new AtomicBoolean();
        this.mRewardedVideoAdaptersList = new ArrayList<>();
        this.mInterstitialAdaptersList = new ArrayList<>();
        this.mBannerAdaptersList = new ArrayList<>();
        this.mAdUnitsToInitialize = new HashSet();
        this.mRequestedAdUnits = new HashSet();
        this.mIsDemandOnlyIs = false;
        this.mIsDemandOnlyRv = false;
        this.mAtomicIsFirstInit = new AtomicBoolean(true);
        this.mInitCounter = 0;
        this.mDidInitRewardedVideo = false;
        this.mDidInitInterstitial = false;
        this.mDidInitBanner = false;
        this.mSessionId = UUID.randomUUID().toString();
        this.mIsBnLoadBeforeInitCompleted = Boolean.valueOf(false);
        this.mIsIsLoadBeforeInitCompleted = false;
        this.mBnPlacementToLoad = null;
        this.mProgRvManager = null;
        this.mProgIsManager = null;
        this.mIsRvProgrammatic = false;
        this.mIsIsProgrammatic = false;
        this.mDemandOnlyIsLoadBeforeInitCompleted = new CopyOnWriteArraySet<>();
        this.mDemandOnlyRvLoadBeforeInitCompleted = new CopyOnWriteArraySet<>();
        this.mDemandOnlyIsManager = null;
        this.mDemandOnlyRvManager = null;
        this.mBannerManager = null;
    }

    public void sendInitCompletedEvent(long j) {
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(this.mIsDemandOnlyRv || this.mIsDemandOnlyIs);
        try {
            mediationAdditionalData.put(IronSourceConstants.EVENTS_DURATION, j);
            mediationAdditionalData.put(RequestParameters.SESSION_DEPTH, this.mInitCounter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(IronSourceConstants.INIT_COMPLETE, mediationAdditionalData));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0145, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007d A[Catch:{ Exception -> 0x00b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0089 A[SYNTHETIC, Splitter:B:31:0x0089] */
    public synchronized void init(Activity activity, String str, boolean z, AD_UNIT... ad_unitArr) {
        if (this.mAtomicIsFirstInit != null && this.mAtomicIsFirstInit.compareAndSet(true, false)) {
            if (ad_unitArr != null) {
                if (ad_unitArr.length != 0) {
                    for (AD_UNIT ad_unit : ad_unitArr) {
                        this.mAdUnitsToInitialize.add(ad_unit);
                        this.mRequestedAdUnits.add(ad_unit);
                        if (ad_unit.equals(AD_UNIT.INTERSTITIAL)) {
                            this.mDidInitInterstitial = true;
                        }
                        if (ad_unit.equals(AD_UNIT.BANNER)) {
                            this.mDidInitBanner = true;
                        }
                        if (ad_unit.equals(AD_UNIT.REWARDED_VIDEO)) {
                            this.mDidInitRewardedVideo = true;
                        }
                    }
                    IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                    IronSourceTag ironSourceTag = IronSourceTag.API;
                    StringBuilder sb = new StringBuilder();
                    sb.append("init(appKey:");
                    sb.append(str);
                    sb.append(")");
                    ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
                    if (activity != null) {
                        this.mLoggerManager.log(IronSourceTag.API, "Init Fail - provided activity is null", 2);
                        return;
                    }
                    this.mActivity = activity;
                    prepareEventManagers(activity);
                    ConfigValidationResult validateAppKey = validateAppKey(str);
                    if (validateAppKey.isValid()) {
                        this.mAppKey = str;
                        if (this.mShouldSendGetInstanceEvent) {
                            JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(z);
                            if (ad_unitArr != null) {
                                try {
                                    for (AD_UNIT ad_unit2 : ad_unitArr) {
                                        mediationAdditionalData.put(ad_unit2.toString(), true);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            String str2 = RequestParameters.SESSION_DEPTH;
                            int i = this.mInitCounter + 1;
                            this.mInitCounter = i;
                            mediationAdditionalData.put(str2, i);
                            RewardedVideoEventsManager.getInstance().log(new EventData(14, mediationAdditionalData));
                            this.mShouldSendGetInstanceEvent = false;
                        }
                        if (this.mAdUnitsToInitialize.contains(AD_UNIT.INTERSTITIAL)) {
                            MediationInitializer.getInstance().addMediationInitializationListener(this.mInterstitialManager);
                        }
                        MediationInitializer.getInstance().addMediationInitializationListener(this);
                        MediationInitializer.getInstance().init(activity, str, this.mUserId, ad_unitArr);
                    } else {
                        MediationInitializer.getInstance().setInitStatusFailed();
                        if (this.mAdUnitsToInitialize.contains(AD_UNIT.REWARDED_VIDEO)) {
                            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(false);
                        }
                        if (this.mAdUnitsToInitialize.contains(AD_UNIT.OFFERWALL)) {
                            this.mListenersWrapper.onOfferwallAvailable(false, validateAppKey.getIronSourceError());
                        }
                        IronSourceLoggerManager.getLogger().log(IronSourceTag.API, validateAppKey.getIronSourceError().toString(), 1);
                        return;
                    }
                }
            }
            for (AD_UNIT add : AD_UNIT.values()) {
                this.mAdUnitsToInitialize.add(add);
            }
            this.mDidInitRewardedVideo = true;
            this.mDidInitInterstitial = true;
            this.mDidInitBanner = true;
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceTag ironSourceTag2 = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("init(appKey:");
            sb2.append(str);
            sb2.append(")");
            ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 1);
            if (activity != null) {
            }
        } else if (ad_unitArr != null) {
            attachAdUnits(z, ad_unitArr);
        } else {
            this.mLoggerManager.log(IronSourceTag.API, "Multiple calls to init without ad units are not allowed", 3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d1, code lost:
        return;
     */
    public synchronized void initISDemandOnly(Activity activity, String str, AD_UNIT... ad_unitArr) {
        ArrayList arrayList = new ArrayList();
        if (ad_unitArr == null) {
            this.mLoggerManager.log(IronSourceTag.API, "Cannot initialized demand only mode: No ad units selected", 3);
        } else if (ad_unitArr.length <= 0) {
            this.mLoggerManager.log(IronSourceTag.API, "Cannot initialized demand only mode: No ad units selected", 3);
        } else {
            for (AD_UNIT ad_unit : ad_unitArr) {
                if (!ad_unit.equals(AD_UNIT.BANNER)) {
                    if (!ad_unit.equals(AD_UNIT.OFFERWALL)) {
                        if (ad_unit.equals(AD_UNIT.INTERSTITIAL)) {
                            if (this.mDidInitInterstitial) {
                                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                                IronSourceTag ironSourceTag = IronSourceTag.API;
                                StringBuilder sb = new StringBuilder();
                                sb.append(ad_unit);
                                sb.append(" ad unit has already been initialized");
                                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
                            } else {
                                this.mDidInitInterstitial = true;
                                this.mIsDemandOnlyIs = true;
                                if (!arrayList.contains(ad_unit)) {
                                    arrayList.add(ad_unit);
                                }
                            }
                        }
                        if (ad_unit.equals(AD_UNIT.REWARDED_VIDEO)) {
                            if (this.mDidInitRewardedVideo) {
                                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                                IronSourceTag ironSourceTag2 = IronSourceTag.API;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(ad_unit);
                                sb2.append(" ad unit has already been initialized");
                                ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 3);
                            } else {
                                this.mDidInitRewardedVideo = true;
                                this.mIsDemandOnlyRv = true;
                                if (!arrayList.contains(ad_unit)) {
                                    arrayList.add(ad_unit);
                                }
                            }
                        }
                    }
                }
                IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
                IronSourceTag ironSourceTag3 = IronSourceTag.API;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(ad_unit);
                sb3.append(" ad unit cannot be initialized in demand only mode");
                ironSourceLoggerManager3.log(ironSourceTag3, sb3.toString(), 3);
            }
            if (arrayList.size() > 0) {
                init(activity, str, true, (AD_UNIT[]) arrayList.toArray(new AD_UNIT[arrayList.size()]));
            }
        }
    }

    private synchronized void attachAdUnits(boolean z, AD_UNIT... ad_unitArr) {
        int i = 0;
        for (AD_UNIT ad_unit : ad_unitArr) {
            if (ad_unit.equals(AD_UNIT.INTERSTITIAL)) {
                this.mDidInitInterstitial = true;
            } else if (ad_unit.equals(AD_UNIT.BANNER)) {
                this.mDidInitBanner = true;
            }
        }
        if (MediationInitializer.getInstance().getCurrentInitStatus() == EInitStatus.INIT_FAILED) {
            try {
                if (this.mListenersWrapper != null) {
                    int length = ad_unitArr.length;
                    while (i < length) {
                        AD_UNIT ad_unit2 = ad_unitArr[i];
                        if (!this.mAdUnitsToInitialize.contains(ad_unit2)) {
                            notifyPublisherAboutInitFailed(ad_unit2, true);
                        }
                        i++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!this.mInitSucceeded) {
            JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(z);
            int length2 = ad_unitArr.length;
            boolean z2 = false;
            while (i < length2) {
                AD_UNIT ad_unit3 = ad_unitArr[i];
                if (!this.mAdUnitsToInitialize.contains(ad_unit3)) {
                    this.mAdUnitsToInitialize.add(ad_unit3);
                    this.mRequestedAdUnits.add(ad_unit3);
                    try {
                        mediationAdditionalData.put(ad_unit3.toString(), true);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    z2 = true;
                } else {
                    IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                    IronSourceTag ironSourceTag = IronSourceTag.API;
                    StringBuilder sb = new StringBuilder();
                    sb.append(ad_unit3);
                    sb.append(" ad unit has started initializing.");
                    ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
                }
                i++;
            }
            if (z2) {
                String str = RequestParameters.SESSION_DEPTH;
                try {
                    int i2 = this.mInitCounter + 1;
                    this.mInitCounter = i2;
                    mediationAdditionalData.put(str, i2);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                RewardedVideoEventsManager.getInstance().log(new EventData(14, mediationAdditionalData));
            }
        } else if (this.mInitiatedAdUnits != null) {
            JSONObject mediationAdditionalData2 = IronSourceUtils.getMediationAdditionalData(z);
            boolean z3 = false;
            for (AD_UNIT ad_unit4 : ad_unitArr) {
                if (!this.mAdUnitsToInitialize.contains(ad_unit4)) {
                    this.mAdUnitsToInitialize.add(ad_unit4);
                    this.mRequestedAdUnits.add(ad_unit4);
                    try {
                        mediationAdditionalData2.put(ad_unit4.toString(), true);
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                    if (this.mInitiatedAdUnits == null || !this.mInitiatedAdUnits.contains(ad_unit4)) {
                        notifyPublisherAboutInitFailed(ad_unit4, false);
                    } else {
                        startAdUnit(ad_unit4);
                    }
                    z3 = true;
                } else {
                    IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                    IronSourceTag ironSourceTag2 = IronSourceTag.API;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(ad_unit4);
                    sb2.append(" ad unit has already been initialized");
                    ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 3);
                }
            }
            if (z3) {
                String str2 = RequestParameters.SESSION_DEPTH;
                try {
                    int i3 = this.mInitCounter + 1;
                    this.mInitCounter = i3;
                    mediationAdditionalData2.put(str2, i3);
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                RewardedVideoEventsManager.getInstance().log(new EventData(14, mediationAdditionalData2));
            }
        }
    }

    public void onInitSuccess(List<AD_UNIT> list, boolean z) {
        AD_UNIT[] values;
        try {
            this.mInitiatedAdUnits = list;
            this.mInitSucceeded = true;
            this.mLoggerManager.log(IronSourceTag.API, "onInitSuccess()", 1);
            IronSourceUtils.sendAutomationLog("init success");
            if (z) {
                JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
                try {
                    mediationAdditionalData.put("revived", true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                RewardedVideoEventsManager.getInstance().log(new EventData(114, mediationAdditionalData));
            }
            InterstitialEventsManager.getInstance().triggerEventsSend();
            RewardedVideoEventsManager.getInstance().triggerEventsSend();
            for (AD_UNIT ad_unit : AD_UNIT.values()) {
                if (this.mAdUnitsToInitialize.contains(ad_unit)) {
                    if (list.contains(ad_unit)) {
                        startAdUnit(ad_unit);
                    } else {
                        notifyPublisherAboutInitFailed(ad_unit, false);
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void startAdUnit(AD_UNIT ad_unit) {
        switch (ad_unit) {
            case REWARDED_VIDEO:
                startRewardedVideo();
                return;
            case INTERSTITIAL:
                startInterstitial();
                return;
            case OFFERWALL:
                this.mOfferwallManager.initOfferwall(this.mActivity, getIronSourceAppKey(), getIronSourceUserId());
                return;
            case BANNER:
                startBanner();
                return;
            default:
                return;
        }
    }

    private void startProgrammaticRv() {
        this.mLoggerManager.log(IronSourceTag.INTERNAL, "Rewarded Video started in programmatic mode", 0);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().size(); i++) {
            String str = (String) this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().get(i);
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str));
            }
        }
        if (arrayList.size() > 0) {
            ProgRvManager progRvManager = new ProgRvManager(this.mActivity, arrayList, this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations(), getIronSourceAppKey(), getIronSourceUserId());
            this.mProgRvManager = progRvManager;
            if (this.mConsent != null) {
                this.mProgRvManager.setConsent(this.mConsent.booleanValue());
                return;
            }
            return;
        }
        notifyPublisherAboutInitFailed(AD_UNIT.REWARDED_VIDEO, false);
    }

    private void startDemandOnlyRv() {
        synchronized (this.mDemandOnlyRvLoadBeforeInitCompleted) {
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "Rewarded Video started in demand only mode", 0);
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().size(); i++) {
                String str = (String) this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().get(i);
                if (!TextUtils.isEmpty(str)) {
                    arrayList.add(this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str));
                }
            }
            if (arrayList.size() > 0) {
                DemandOnlyRvManager demandOnlyRvManager = new DemandOnlyRvManager(this.mActivity, arrayList, this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations(), getIronSourceAppKey(), getIronSourceUserId());
                this.mDemandOnlyRvManager = demandOnlyRvManager;
                if (this.mConsent != null) {
                    this.mDemandOnlyRvManager.setConsent(this.mConsent.booleanValue());
                }
                Iterator it = this.mDemandOnlyRvLoadBeforeInitCompleted.iterator();
                while (it.hasNext()) {
                    this.mDemandOnlyRvManager.loadRewardedVideo((String) it.next());
                }
                this.mDemandOnlyRvLoadBeforeInitCompleted.clear();
            } else {
                notifyPublisherAboutInitFailed(AD_UNIT.REWARDED_VIDEO, false);
            }
        }
    }

    private void startRewardedVideo() {
        if (this.mIsDemandOnlyRv) {
            startDemandOnlyRv();
            return;
        }
        this.mIsRvProgrammatic = this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoAuctionSettings().getIsProgrammatic();
        if (this.mIsRvProgrammatic) {
            startProgrammaticRv();
            return;
        }
        int rewardedVideoAdaptersSmartLoadTimeout = this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoAdaptersSmartLoadTimeout();
        for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().size(); i++) {
            String str = (String) this.mCurrentServerResponse.getProviderOrder().getRewardedVideoProviderOrder().get(i);
            if (!TextUtils.isEmpty(str)) {
                ProviderSettings providerSettings = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str);
                if (providerSettings != null) {
                    RewardedVideoSmash rewardedVideoSmash = new RewardedVideoSmash(providerSettings, rewardedVideoAdaptersSmartLoadTimeout);
                    if (validateSmash(rewardedVideoSmash)) {
                        rewardedVideoSmash.setRewardedVideoManagerListener(this.mRewardedVideoManager);
                        rewardedVideoSmash.setProviderPriority(i + 1);
                        this.mRewardedVideoManager.addSmashToArray(rewardedVideoSmash);
                    }
                }
            }
        }
        if (this.mRewardedVideoManager.mSmashArray.size() > 0) {
            this.mRewardedVideoManager.setIsUltraEventsEnabled(this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().isUltraEventsEnabled());
            this.mRewardedVideoManager.setSmartLoadAmount(this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoAdaptersSmartLoadAmount());
            this.mRewardedVideoManager.setManualLoadInterval(this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations().getManualLoadIntervalInSeconds());
            String rVBackFillProvider = this.mCurrentServerResponse.getRVBackFillProvider();
            if (!TextUtils.isEmpty(rVBackFillProvider)) {
                ProviderSettings providerSettings2 = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(rVBackFillProvider);
                if (providerSettings2 != null) {
                    RewardedVideoSmash rewardedVideoSmash2 = new RewardedVideoSmash(providerSettings2, rewardedVideoAdaptersSmartLoadTimeout);
                    if (validateSmash(rewardedVideoSmash2)) {
                        rewardedVideoSmash2.setRewardedVideoManagerListener(this.mRewardedVideoManager);
                        this.mRewardedVideoManager.setBackfillSmash(rewardedVideoSmash2);
                    }
                }
            }
            String rVPremiumProvider = this.mCurrentServerResponse.getRVPremiumProvider();
            if (!TextUtils.isEmpty(rVPremiumProvider)) {
                ProviderSettings providerSettings3 = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(rVPremiumProvider);
                if (providerSettings3 != null) {
                    RewardedVideoSmash rewardedVideoSmash3 = new RewardedVideoSmash(providerSettings3, rewardedVideoAdaptersSmartLoadTimeout);
                    if (validateSmash(rewardedVideoSmash3)) {
                        rewardedVideoSmash3.setRewardedVideoManagerListener(this.mRewardedVideoManager);
                        this.mRewardedVideoManager.setPremiumSmash(rewardedVideoSmash3);
                    }
                }
            }
            this.mRewardedVideoManager.initRewardedVideo(this.mActivity, getIronSourceAppKey(), getIronSourceUserId());
        } else {
            notifyPublisherAboutInitFailed(AD_UNIT.REWARDED_VIDEO, false);
        }
    }

    private void startProgrammaticIs() {
        this.mLoggerManager.log(IronSourceTag.INTERNAL, "Interstitial started in programmatic mode", 0);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().size(); i++) {
            String str = (String) this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().get(i);
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str));
            }
        }
        if (arrayList.size() > 0) {
            ProgIsManager progIsManager = new ProgIsManager(this.mActivity, arrayList, this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations(), getIronSourceAppKey(), getIronSourceUserId(), this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getISDelayLoadFailure());
            this.mProgIsManager = progIsManager;
            if (this.mConsent != null) {
                this.mProgIsManager.setConsent(this.mConsent.booleanValue());
            }
            if (this.mIsIsLoadBeforeInitCompleted) {
                this.mIsIsLoadBeforeInitCompleted = false;
                this.mProgIsManager.loadInterstitial();
                return;
            }
            return;
        }
        notifyPublisherAboutInitFailed(AD_UNIT.INTERSTITIAL, false);
    }

    private void startDemandOnlyIs() {
        synchronized (this.mDemandOnlyIsLoadBeforeInitCompleted) {
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "Interstitial started in demand only mode", 0);
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().size(); i++) {
                String str = (String) this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().get(i);
                if (!TextUtils.isEmpty(str)) {
                    arrayList.add(this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str));
                }
            }
            if (arrayList.size() > 0) {
                DemandOnlyIsManager demandOnlyIsManager = new DemandOnlyIsManager(this.mActivity, arrayList, this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations(), getIronSourceAppKey(), getIronSourceUserId());
                this.mDemandOnlyIsManager = demandOnlyIsManager;
                if (this.mConsent != null) {
                    this.mDemandOnlyIsManager.setConsent(this.mConsent.booleanValue());
                }
                Iterator it = this.mDemandOnlyIsLoadBeforeInitCompleted.iterator();
                while (it.hasNext()) {
                    this.mDemandOnlyIsManager.loadInterstitial((String) it.next());
                }
                this.mDemandOnlyIsLoadBeforeInitCompleted.clear();
            } else {
                notifyPublisherAboutInitFailed(AD_UNIT.INTERSTITIAL, false);
            }
        }
    }

    private void startInterstitial() {
        if (this.mIsDemandOnlyIs) {
            startDemandOnlyIs();
            return;
        }
        this.mIsIsProgrammatic = this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getInterstitialAuctionSettings().getIsProgrammatic();
        if (this.mIsIsProgrammatic) {
            startProgrammaticIs();
            return;
        }
        int interstitialAdaptersSmartLoadTimeout = this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getInterstitialAdaptersSmartLoadTimeout();
        this.mInterstitialManager.setDelayLoadFailureNotificationInSeconds(this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getISDelayLoadFailure());
        for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().size(); i++) {
            String str = (String) this.mCurrentServerResponse.getProviderOrder().getInterstitialProviderOrder().get(i);
            if (!TextUtils.isEmpty(str)) {
                ProviderSettings providerSettings = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str);
                if (providerSettings != null) {
                    InterstitialSmash interstitialSmash = new InterstitialSmash(providerSettings, interstitialAdaptersSmartLoadTimeout);
                    if (validateSmash(interstitialSmash)) {
                        interstitialSmash.setInterstitialManagerListener(this.mInterstitialManager);
                        interstitialSmash.setProviderPriority(i + 1);
                        this.mInterstitialManager.addSmashToArray(interstitialSmash);
                    }
                }
            }
        }
        if (this.mInterstitialManager.mSmashArray.size() > 0) {
            this.mInterstitialManager.setSmartLoadAmount(this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations().getInterstitialAdaptersSmartLoadAmount());
            this.mInterstitialManager.initInterstitial(this.mActivity, getIronSourceAppKey(), getIronSourceUserId());
            if (this.mIsIsLoadBeforeInitCompleted) {
                this.mIsIsLoadBeforeInitCompleted = false;
                this.mInterstitialManager.loadInterstitial();
            }
        } else {
            notifyPublisherAboutInitFailed(AD_UNIT.INTERSTITIAL, false);
        }
    }

    private void startBanner() {
        synchronized (this.mIsBnLoadBeforeInitCompleted) {
            long bannerAdaptersSmartLoadTimeout = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations().getBannerAdaptersSmartLoadTimeout();
            int bannerRefreshInterval = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations().getBannerRefreshInterval();
            int bannerDelayLoadFailure = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations().getBannerDelayLoadFailure();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < this.mCurrentServerResponse.getProviderOrder().getBannerProviderOrder().size(); i++) {
                String str = (String) this.mCurrentServerResponse.getProviderOrder().getBannerProviderOrder().get(i);
                if (!TextUtils.isEmpty(str)) {
                    ProviderSettings providerSettings = this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettings(str);
                    if (providerSettings != null) {
                        arrayList.add(providerSettings);
                    }
                }
            }
            BannerManager bannerManager = new BannerManager(arrayList, this.mActivity, getIronSourceAppKey(), getIronSourceUserId(), bannerAdaptersSmartLoadTimeout, bannerRefreshInterval, bannerDelayLoadFailure);
            this.mBannerManager = bannerManager;
            if (this.mIsBnLoadBeforeInitCompleted.booleanValue()) {
                this.mIsBnLoadBeforeInitCompleted = Boolean.valueOf(false);
                loadBanner(this.mBnLayoutToLoad, this.mBnPlacementToLoad);
                this.mBnLayoutToLoad = null;
                this.mBnPlacementToLoad = null;
            }
        }
    }

    private boolean validateSmash(AbstractSmash abstractSmash) {
        return abstractSmash.getMaxAdsPerIteration() >= 1 && abstractSmash.getMaxAdsPerSession() >= 1;
    }

    public void onInitFailed(String str) {
        try {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.API;
            StringBuilder sb = new StringBuilder();
            sb.append("onInitFailed(reason:");
            sb.append(str);
            sb.append(")");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            if (this.mListenersWrapper != null) {
                for (AD_UNIT notifyPublisherAboutInitFailed : this.mAdUnitsToInitialize) {
                    notifyPublisherAboutInitFailed(notifyPublisherAboutInitFailed, true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onStillInProgressAfter15Secs() {
        synchronized (this.mIsBnLoadBeforeInitCompleted) {
            if (this.mIsBnLoadBeforeInitCompleted.booleanValue()) {
                this.mIsBnLoadBeforeInitCompleted = Boolean.valueOf(false);
                BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(this.mBnLayoutToLoad, new IronSourceError(IronSourceError.ERROR_BN_LOAD_WHILE_LONG_INITIATION, "init had failed"));
                this.mBnLayoutToLoad = null;
                this.mBnPlacementToLoad = null;
            }
        }
        if (this.mIsIsLoadBeforeInitCompleted) {
            this.mIsIsLoadBeforeInitCompleted = false;
            CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
        }
        synchronized (this.mDemandOnlyIsLoadBeforeInitCompleted) {
            Iterator it = this.mDemandOnlyIsLoadBeforeInitCompleted.iterator();
            while (it.hasNext()) {
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed((String) it.next(), ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
            }
            this.mDemandOnlyIsLoadBeforeInitCompleted.clear();
        }
        synchronized (this.mDemandOnlyRvLoadBeforeInitCompleted) {
            Iterator it2 = this.mDemandOnlyRvLoadBeforeInitCompleted.iterator();
            while (it2.hasNext()) {
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed((String) it2.next(), ErrorBuilder.buildInitFailedError("init() had failed", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            }
            this.mDemandOnlyRvLoadBeforeInitCompleted.clear();
        }
    }

    private void notifyPublisherAboutInitFailed(AD_UNIT ad_unit, boolean z) {
        switch (ad_unit) {
            case REWARDED_VIDEO:
                if (this.mIsDemandOnlyRv) {
                    Iterator it = this.mDemandOnlyRvLoadBeforeInitCompleted.iterator();
                    while (it.hasNext()) {
                        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed((String) it.next(), ErrorBuilder.buildInitFailedError("initISDemandOnly() had failed", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                    }
                    this.mDemandOnlyRvLoadBeforeInitCompleted.clear();
                    return;
                } else if (z || isRewardedVideoConfigurationsReady() || this.mRequestedAdUnits.contains(ad_unit)) {
                    this.mListenersWrapper.onRewardedVideoAvailabilityChanged(false);
                    return;
                } else {
                    return;
                }
            case INTERSTITIAL:
                if (this.mIsDemandOnlyIs) {
                    Iterator it2 = this.mDemandOnlyIsLoadBeforeInitCompleted.iterator();
                    while (it2.hasNext()) {
                        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed((String) it2.next(), ErrorBuilder.buildInitFailedError("initISDemandOnly() had failed", "Interstitial"));
                    }
                    this.mDemandOnlyIsLoadBeforeInitCompleted.clear();
                    return;
                } else if (this.mIsIsLoadBeforeInitCompleted) {
                    this.mIsIsLoadBeforeInitCompleted = false;
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                    return;
                } else {
                    return;
                }
            case OFFERWALL:
                if (z || isOfferwallConfigurationsReady() || this.mRequestedAdUnits.contains(ad_unit)) {
                    this.mListenersWrapper.onOfferwallAvailable(false);
                    return;
                }
                return;
            case BANNER:
                synchronized (this.mIsBnLoadBeforeInitCompleted) {
                    if (this.mIsBnLoadBeforeInitCompleted.booleanValue()) {
                        this.mIsBnLoadBeforeInitCompleted = Boolean.valueOf(false);
                        BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(this.mBnLayoutToLoad, new IronSourceError(IronSourceError.ERROR_BN_INIT_FAILED_AFTER_LOAD, "Init had failed"));
                        this.mBnLayoutToLoad = null;
                        this.mBnPlacementToLoad = null;
                    }
                }
                return;
            default:
                return;
        }
    }

    private void prepareEventManagers(Activity activity) {
        if (this.mEventManagersInit != null && this.mEventManagersInit.compareAndSet(false, true)) {
            SuperLooper.getLooper().post(new GeneralPropertiesWorker(activity.getApplicationContext()));
            InterstitialEventsManager.getInstance().start(activity.getApplicationContext(), this.mIronSegment);
            RewardedVideoEventsManager.getInstance().start(activity.getApplicationContext(), this.mIronSegment);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void addToRVAdaptersList(AbstractAdapter abstractAdapter) {
        if (!(this.mRewardedVideoAdaptersList == null || abstractAdapter == null || this.mRewardedVideoAdaptersList.contains(abstractAdapter))) {
            this.mRewardedVideoAdaptersList.add(abstractAdapter);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void addToISAdaptersList(AbstractAdapter abstractAdapter) {
        if (!(this.mInterstitialAdaptersList == null || abstractAdapter == null || this.mInterstitialAdaptersList.contains(abstractAdapter))) {
            this.mInterstitialAdaptersList.add(abstractAdapter);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void addToBannerAdaptersList(AbstractAdapter abstractAdapter) {
        if (!(this.mBannerAdaptersList == null || abstractAdapter == null || this.mBannerAdaptersList.contains(abstractAdapter))) {
            this.mBannerAdaptersList.add(abstractAdapter);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void addOWAdapter(AbstractAdapter abstractAdapter) {
        this.mOfferwallAdapter = abstractAdapter;
    }

    /* access modifiers changed from: 0000 */
    public synchronized AbstractAdapter getExistingAdapter(String str) {
        if (this.mRewardedVideoAdaptersList != null) {
            Iterator it = this.mRewardedVideoAdaptersList.iterator();
            while (it.hasNext()) {
                AbstractAdapter abstractAdapter = (AbstractAdapter) it.next();
                if (abstractAdapter.getProviderName().equals(str)) {
                    return abstractAdapter;
                }
            }
        }
        try {
            if (this.mInterstitialAdaptersList != null) {
                Iterator it2 = this.mInterstitialAdaptersList.iterator();
                while (it2.hasNext()) {
                    AbstractAdapter abstractAdapter2 = (AbstractAdapter) it2.next();
                    if (abstractAdapter2.getProviderName().equals(str)) {
                        return abstractAdapter2;
                    }
                }
            }
            if (this.mBannerAdaptersList != null) {
                Iterator it3 = this.mBannerAdaptersList.iterator();
                while (it3.hasNext()) {
                    AbstractAdapter abstractAdapter3 = (AbstractAdapter) it3.next();
                    if (abstractAdapter3.getProviderName().equals(str)) {
                        return abstractAdapter3;
                    }
                }
            }
            if (this.mOfferwallAdapter != null && this.mOfferwallAdapter.getProviderName().equals(str)) {
                return this.mOfferwallAdapter;
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("getExistingAdapter exception: ");
            sb.append(e);
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        }
        return null;
    }

    private void initializeManagers() {
        this.mLoggerManager = IronSourceLoggerManager.getLogger(0);
        this.mPublisherLogger = new PublisherLogger(null, 1);
        this.mLoggerManager.addLogger(this.mPublisherLogger);
        this.mListenersWrapper = new ListenersWrapper();
        this.mRewardedVideoManager = new RewardedVideoManager();
        this.mRewardedVideoManager.setRewardedVideoListener(this.mListenersWrapper);
        this.mInterstitialManager = new InterstitialManager();
        this.mInterstitialManager.setInterstitialListener(this.mListenersWrapper);
        this.mInterstitialManager.setRewardedInterstitialListener(this.mListenersWrapper);
        this.mOfferwallManager = new OfferwallManager();
        this.mOfferwallManager.setInternalOfferwallListener(this.mListenersWrapper);
    }

    public void onResume(Activity activity) {
        String str = "onResume()";
        try {
            this.mActivity = activity;
            this.mLoggerManager.log(IronSourceTag.API, str, 1);
            if (this.mRewardedVideoManager != null) {
                this.mRewardedVideoManager.onResume(activity);
            }
            if (this.mInterstitialManager != null) {
                this.mInterstitialManager.onResume(activity);
            }
            if (this.mBannerManager != null) {
                this.mBannerManager.onResume(activity);
            }
            if (this.mProgRvManager != null) {
                this.mProgRvManager.onResume(activity);
            }
            if (this.mProgIsManager != null) {
                this.mProgIsManager.onResume(activity);
            }
            if (this.mDemandOnlyIsManager != null) {
                this.mDemandOnlyIsManager.onResume(activity);
            }
            if (this.mDemandOnlyRvManager != null) {
                this.mDemandOnlyRvManager.onResume(activity);
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceTag.API, str, th);
        }
    }

    public void onPause(Activity activity) {
        String str = "onPause()";
        try {
            this.mLoggerManager.log(IronSourceTag.API, str, 1);
            if (this.mRewardedVideoManager != null) {
                this.mRewardedVideoManager.onPause(activity);
            }
            if (this.mInterstitialManager != null) {
                this.mInterstitialManager.onPause(activity);
            }
            if (this.mBannerManager != null) {
                this.mBannerManager.onPause(activity);
            }
            if (this.mProgRvManager != null) {
                this.mProgRvManager.onPause(activity);
            }
            if (this.mProgIsManager != null) {
                this.mProgIsManager.onPause(activity);
            }
            if (this.mDemandOnlyIsManager != null) {
                this.mDemandOnlyIsManager.onPause(activity);
            }
            if (this.mDemandOnlyRvManager != null) {
                this.mDemandOnlyRvManager.onPause(activity);
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceTag.API, str, th);
        }
    }

    public synchronized void setAge(int i) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(this.TAG);
            sb.append(":setAge(age:");
            sb.append(i);
            sb.append(")");
            this.mLoggerManager.log(IronSourceTag.API, sb.toString(), 1);
            ConfigValidationResult configValidationResult = new ConfigValidationResult();
            validateAge(i, configValidationResult);
            if (configValidationResult.isValid()) {
                this.mUserAge = Integer.valueOf(i);
            } else {
                IronSourceLoggerManager.getLogger().log(IronSourceTag.API, configValidationResult.getIronSourceError().toString(), 2);
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":setAge(age:");
            sb2.append(i);
            sb2.append(")");
            ironSourceLoggerManager.logException(ironSourceTag, sb2.toString(), e);
        }
        return;
    }

    public synchronized void setGender(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(this.TAG);
            sb.append(":setGender(gender:");
            sb.append(str);
            sb.append(")");
            this.mLoggerManager.log(IronSourceTag.API, sb.toString(), 1);
            ConfigValidationResult configValidationResult = new ConfigValidationResult();
            validateGender(str, configValidationResult);
            if (configValidationResult.isValid()) {
                this.mUserGender = str;
            } else {
                IronSourceLoggerManager.getLogger().log(IronSourceTag.API, configValidationResult.getIronSourceError().toString(), 2);
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":setGender(gender:");
            sb2.append(str);
            sb2.append(")");
            ironSourceLoggerManager.logException(ironSourceTag, sb2.toString(), e);
        }
        return;
    }

    public void setMediationSegment(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(this.TAG);
            sb.append(":setMediationSegment(segment:");
            sb.append(str);
            sb.append(")");
            this.mLoggerManager.log(IronSourceTag.API, sb.toString(), 1);
            ConfigValidationResult configValidationResult = new ConfigValidationResult();
            validateSegment(str, configValidationResult);
            if (configValidationResult.isValid()) {
                this.mSegment = str;
            } else {
                IronSourceLoggerManager.getLogger().log(IronSourceTag.API, configValidationResult.getIronSourceError().toString(), 2);
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":setMediationSegment(segment:");
            sb2.append(str);
            sb2.append(")");
            ironSourceLoggerManager.logException(ironSourceTag, sb2.toString(), e);
        }
    }

    public void setSegment(IronSourceSegment ironSourceSegment) {
        if (MediationInitializer.getInstance().getCurrentInitStatus() == EInitStatus.INIT_IN_PROGRESS || MediationInitializer.getInstance().getCurrentInitStatus() == EInitStatus.INITIATED) {
            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, "Segments must be set prior to Init. Setting a segment after the init will be ignored", 0);
        } else {
            this.mIronSegment = ironSourceSegment;
        }
    }

    public boolean setDynamicUserId(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(this.TAG);
            sb.append(":setDynamicUserId(dynamicUserId:");
            sb.append(str);
            sb.append(")");
            this.mLoggerManager.log(IronSourceTag.API, sb.toString(), 1);
            ConfigValidationResult configValidationResult = new ConfigValidationResult();
            validateDynamicUserId(str, configValidationResult);
            if (configValidationResult.isValid()) {
                this.mDynamicUserId = str;
                return true;
            }
            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, configValidationResult.getIronSourceError().toString(), 2);
            return false;
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":setDynamicUserId(dynamicUserId:");
            sb2.append(str);
            sb2.append(")");
            ironSourceLoggerManager.logException(ironSourceTag, sb2.toString(), e);
            return false;
        }
    }

    public void setAdaptersDebug(boolean z) {
        IronSourceLoggerManager.getLogger().setAdaptersDebug(z);
    }

    public void setMediationType(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(this.TAG);
            sb.append(":setMediationType(mediationType:");
            sb.append(str);
            sb.append(")");
            this.mLoggerManager.log(IronSourceTag.INTERNAL, sb.toString(), 1);
            if (!validateLength(str, 1, 64) || !validateAlphanumeric(str)) {
                this.mLoggerManager.log(IronSourceTag.INTERNAL, " mediationType value is invalid - should be alphanumeric and 1-64 chars in length", 1);
                return;
            }
            this.mMediationType = str;
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":setMediationType(mediationType:");
            sb2.append(str);
            sb2.append(")");
            ironSourceLoggerManager.logException(ironSourceTag, sb2.toString(), e);
        }
    }

    public synchronized Integer getAge() {
        return this.mUserAge;
    }

    public synchronized String getGender() {
        return this.mUserGender;
    }

    /* access modifiers changed from: 0000 */
    public synchronized String getMediationSegment() {
        return this.mSegment;
    }

    /* access modifiers changed from: 0000 */
    public synchronized String getDynamicUserId() {
        return this.mDynamicUserId;
    }

    /* access modifiers changed from: 0000 */
    public synchronized Map<String, String> getRvServerParams() {
        return this.mRvServerParams;
    }

    public synchronized String getMediationType() {
        return this.mMediationType;
    }

    public void showRewardedVideo() {
        Placement defaultRewardedVideoPlacement = getDefaultRewardedVideoPlacement();
        if (defaultRewardedVideoPlacement == null) {
            String str = "showRewardedVideo error: empty default placement in response";
            this.mLoggerManager.log(IronSourceTag.INTERNAL, str, 3);
            this.mListenersWrapper.onRewardedVideoAdShowFailed(new IronSourceError(1021, str));
            return;
        }
        showRewardedVideo(defaultRewardedVideoPlacement.getPlacementName());
    }

    private Placement getRewardedVideoPlacement(String str) {
        RewardedVideoConfigurations rewardedVideoConfigurations = this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations();
        if (rewardedVideoConfigurations != null) {
            return rewardedVideoConfigurations.getRewardedVideoPlacement(str);
        }
        return null;
    }

    private Placement getDefaultRewardedVideoPlacement() {
        RewardedVideoConfigurations rewardedVideoConfigurations = this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations();
        if (rewardedVideoConfigurations != null) {
            return rewardedVideoConfigurations.getDefaultRewardedVideoPlacement();
        }
        return null;
    }

    private void showProgrammaticRewardedVideo(String str) {
        Placement rewardedVideoPlacement = getRewardedVideoPlacement(str);
        if (rewardedVideoPlacement == null) {
            rewardedVideoPlacement = getDefaultRewardedVideoPlacement();
        }
        if (rewardedVideoPlacement == null) {
            String str2 = "showProgrammaticRewardedVideo error: empty default placement in response";
            this.mLoggerManager.log(IronSourceTag.INTERNAL, str2, 3);
            this.mListenersWrapper.onRewardedVideoAdShowFailed(new IronSourceError(1021, str2));
            return;
        }
        this.mProgRvManager.showRewardedVideo(rewardedVideoPlacement);
    }

    public void showRewardedVideo(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("showRewardedVideo(");
        sb.append(str);
        sb.append(")");
        String sb2 = sb.toString();
        this.mLoggerManager.log(IronSourceTag.API, sb2, 1);
        try {
            if (this.mIsDemandOnlyRv) {
                String str2 = "Rewarded Video was initialized in demand only mode. Use showISDemandOnlyRewardedVideo instead";
                this.mLoggerManager.log(IronSourceTag.API, str2, 3);
                this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildInitFailedError(str2, IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            } else if (!isRewardedVideoConfigurationsReady()) {
                this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildInitFailedError("showRewardedVideo can't be called before the Rewarded Video ad unit initialization completed successfully", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            } else if (!this.mIsRvProgrammatic || this.mProgRvManager == null) {
                Placement placementToShowWithEvent = getPlacementToShowWithEvent(str);
                if (placementToShowWithEvent != null) {
                    this.mRewardedVideoManager.setCurrentPlacement(placementToShowWithEvent);
                    this.mRewardedVideoManager.showRewardedVideo(placementToShowWithEvent.getPlacementName());
                }
            } else {
                showProgrammaticRewardedVideo(str);
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceTag.API, sb2, e);
            this.mListenersWrapper.onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, e.getMessage()));
        }
    }

    public boolean isRewardedVideoAvailable() {
        Throwable th;
        boolean z;
        boolean z2 = false;
        try {
            if (this.mIsDemandOnlyRv) {
                this.mLoggerManager.log(IronSourceTag.API, "Rewarded Video was initialized in demand only mode. Use isISDemandOnlyRewardedVideoAvailable instead", 3);
                return false;
            }
            z = this.mIsRvProgrammatic ? this.mProgRvManager != null && this.mProgRvManager.isRewardedVideoAvailable() : this.mRewardedVideoManager.isRewardedVideoAvailable();
            try {
                JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
                if (this.mIsRvProgrammatic) {
                    addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{IronSourceConstants.EVENTS_PROGRAMMATIC, Integer.valueOf(1)}});
                }
                RewardedVideoEventsManager.getInstance().log(new EventData(z ? IronSourceConstants.RV_API_HAS_AVAILABILITY_TRUE : IronSourceConstants.RV_API_HAS_AVAILABILITY_FALSE, mediationAdditionalData));
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.API;
                StringBuilder sb = new StringBuilder();
                sb.append("isRewardedVideoAvailable():");
                sb.append(z);
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
                z2 = z;
            } catch (Throwable th2) {
                th = th2;
                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                IronSourceTag ironSourceTag2 = IronSourceTag.API;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("isRewardedVideoAvailable():");
                sb2.append(z);
                ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 1);
                this.mLoggerManager.logException(IronSourceTag.API, "isRewardedVideoAvailable()", th);
                return z2;
            }
            return z2;
        } catch (Throwable th3) {
            th = th3;
            z = false;
            IronSourceLoggerManager ironSourceLoggerManager22 = this.mLoggerManager;
            IronSourceTag ironSourceTag22 = IronSourceTag.API;
            StringBuilder sb22 = new StringBuilder();
            sb22.append("isRewardedVideoAvailable():");
            sb22.append(z);
            ironSourceLoggerManager22.log(ironSourceTag22, sb22.toString(), 1);
            this.mLoggerManager.logException(IronSourceTag.API, "isRewardedVideoAvailable()", th);
            return z2;
        }
    }

    public void setRewardedVideoListener(RewardedVideoListener rewardedVideoListener) {
        if (rewardedVideoListener == null) {
            this.mLoggerManager.log(IronSourceTag.API, "setRewardedVideoListener(RVListener:null)", 1);
        } else {
            this.mLoggerManager.log(IronSourceTag.API, "setRewardedVideoListener(RVListener)", 1);
        }
        this.mListenersWrapper.setRewardedVideoListener(rewardedVideoListener);
        RVListenerWrapper.getInstance().setListener(rewardedVideoListener);
    }

    public void setRewardedVideoServerParameters(Map<String, String> map) {
        if (map != null) {
            try {
                if (map.size() != 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.TAG);
                    sb.append(":setRewardedVideoServerParameters(params:");
                    sb.append(map.toString());
                    sb.append(")");
                    this.mLoggerManager.log(IronSourceTag.API, sb.toString(), 1);
                    this.mRvServerParams = new HashMap(map);
                }
            } catch (Exception e) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.API;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.TAG);
                sb2.append(":setRewardedVideoServerParameters(params:");
                sb2.append(map.toString());
                sb2.append(")");
                ironSourceLoggerManager.logException(ironSourceTag, sb2.toString(), e);
            }
        }
    }

    public void clearRewardedVideoServerParameters() {
        this.mRvServerParams = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b9, code lost:
        if (r5.mCurrentServerResponse == null) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c1, code lost:
        if (r5.mCurrentServerResponse.getConfigurations() == null) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00cd, code lost:
        if (r5.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations() != null) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d0, code lost:
        r5.mDemandOnlyRvManager.loadRewardedVideo(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d6, code lost:
        r5.mLoggerManager.log(com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API, "No rewarded video configurations found", 3);
        com.ironsource.mediationsdk.RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(r6, com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError("the server response does not contain rewarded video data", com.ironsource.mediationsdk.utils.IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ef, code lost:
        return;
     */
    public synchronized void loadDemandOnlyRewardedVideo(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append("loadISDemandOnlyRewardedVideo() instanceId=");
        sb.append(str);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        try {
            if (!this.mDidInitRewardedVideo) {
                String str2 = "initISDemandOnly() must be called before loadISDemandOnlyRewardedVideo()";
                this.mLoggerManager.log(IronSourceTag.API, str2, 3);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, str2));
                return;
            } else if (!this.mIsDemandOnlyRv) {
                String str3 = "Rewarded video was initialized in mediation mode";
                this.mLoggerManager.log(IronSourceTag.API, str3, 3);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, str3));
                return;
            } else {
                EInitStatus currentInitStatus = MediationInitializer.getInstance().getCurrentInitStatus();
                if (currentInitStatus == EInitStatus.INIT_FAILED) {
                    this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                    RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, ErrorBuilder.buildInitFailedError("init() had failed", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                    return;
                } else if (currentInitStatus != EInitStatus.INIT_IN_PROGRESS) {
                    synchronized (this.mDemandOnlyRvLoadBeforeInitCompleted) {
                        if (this.mDemandOnlyRvManager == null) {
                            this.mDemandOnlyRvLoadBeforeInitCompleted.add(str);
                            return;
                        }
                    }
                } else if (MediationInitializer.getInstance().isInProgressMoreThan15Secs()) {
                    this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                    RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, ErrorBuilder.buildInitFailedError("init() had failed", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                } else {
                    this.mDemandOnlyRvLoadBeforeInitCompleted.add(str);
                }
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceTag.API, "loadISDemandOnlyRewardedVideo", th);
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, th.getMessage()));
        }
    }

    public synchronized void showDemandOnlyRewardedVideo(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append("showISDemandOnlyRewardedVideo() instanceId=");
        sb.append(str);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        try {
            if (!this.mIsDemandOnlyRv) {
                String str2 = "Rewarded video was initialized in mediation mode. Use showRewardedVideo instead";
                this.mLoggerManager.log(IronSourceTag.API, str2, 3);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, str2));
                return;
            } else if (this.mDemandOnlyRvManager == null) {
                String str3 = "Rewarded video was not initiated";
                this.mLoggerManager.log(IronSourceTag.API, str3, 3);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, str3));
                return;
            } else {
                this.mDemandOnlyRvManager.showRewardedVideo(str);
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceTag.API, "showISDemandOnlyRewardedVideo", e);
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, e.getMessage()));
        }
        return;
    }

    public synchronized boolean isDemandOnlyRewardedVideoAvailable(String str) {
        return this.mDemandOnlyRvManager != null && this.mDemandOnlyRvManager.isRewardedVideoAvailable(str);
    }

    /* access modifiers changed from: 0000 */
    public void setISDemandOnlyRewardedVideoListener(ISDemandOnlyRewardedVideoListener iSDemandOnlyRewardedVideoListener) {
        RVDemandOnlyListenerWrapper.getInstance().setListener(iSDemandOnlyRewardedVideoListener);
    }

    private boolean isRewardedVideoConfigurationsReady() {
        return (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations() == null) ? false : true;
    }

    private Placement getPlacementToShowWithEvent(String str) {
        Placement rewardedVideoPlacement = getRewardedVideoPlacement(str);
        if (rewardedVideoPlacement == null) {
            this.mLoggerManager.log(IronSourceTag.API, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
            rewardedVideoPlacement = getDefaultRewardedVideoPlacement();
            if (rewardedVideoPlacement == null) {
                this.mLoggerManager.log(IronSourceTag.API, "Default placement was not found, please make sure you are using the right placements.", 3);
                return null;
            }
        }
        String cappingMessage = getCappingMessage(rewardedVideoPlacement.getPlacementName(), CappingManager.isPlacementCapped((Context) this.mActivity, rewardedVideoPlacement));
        if (TextUtils.isEmpty(cappingMessage)) {
            return rewardedVideoPlacement;
        }
        this.mLoggerManager.log(IronSourceTag.API, cappingMessage, 1);
        this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildCappedPerPlacementError(cappingMessage));
        return null;
    }

    public void loadInterstitial() {
        String str = "loadInterstitial()";
        this.mLoggerManager.log(IronSourceTag.API, str, 1);
        try {
            if (this.mIsDemandOnlyIs) {
                String str2 = "Interstitial was initialized in demand only mode. Use loadISDemandOnlyInterstitial instead";
                this.mLoggerManager.log(IronSourceTag.API, str2, 3);
                CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError(str2, "Interstitial"));
            } else if (!this.mDidInitInterstitial) {
                String str3 = "init() must be called before loadInterstitial()";
                this.mLoggerManager.log(IronSourceTag.API, str3, 3);
                CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError(str3, "Interstitial"));
            } else {
                EInitStatus currentInitStatus = MediationInitializer.getInstance().getCurrentInitStatus();
                if (currentInitStatus == EInitStatus.INIT_FAILED) {
                    this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                } else if (currentInitStatus == EInitStatus.INIT_IN_PROGRESS) {
                    if (MediationInitializer.getInstance().isInProgressMoreThan15Secs()) {
                        this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                        CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                    } else {
                        this.mIsIsLoadBeforeInitCompleted = true;
                    }
                } else {
                    if (!(this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null)) {
                        if (this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations() != null) {
                            if (this.mIsIsProgrammatic) {
                                this.mProgIsManager.loadInterstitial();
                            } else {
                                this.mInterstitialManager.loadInterstitial();
                            }
                            return;
                        }
                    }
                    this.mLoggerManager.log(IronSourceTag.API, "No interstitial configurations found", 3);
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("the server response does not contain interstitial data", "Interstitial"));
                }
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceTag.API, str, th);
            CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, th.getMessage()));
        }
    }

    public void showInterstitial() {
        String str = "showInterstitial()";
        this.mLoggerManager.log(IronSourceTag.API, str, 1);
        try {
            if (this.mIsDemandOnlyIs) {
                String str2 = "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead";
                this.mLoggerManager.log(IronSourceTag.API, str2, 3);
                this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, str2));
            } else if (!isInterstitialConfigurationsReady()) {
                this.mListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildInitFailedError("showInterstitial can't be called before the Interstitial ad unit initialization completed successfully", "Interstitial"));
            } else {
                InterstitialPlacement defaultInterstitialPlacement = getDefaultInterstitialPlacement();
                if (defaultInterstitialPlacement != null) {
                    showInterstitial(defaultInterstitialPlacement.getPlacementName());
                } else {
                    this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(1020, "showInterstitial error: empty default placement in response"));
                }
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceTag.API, str, e);
            this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, e.getMessage()));
        }
    }

    public void showInterstitial(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("showInterstitial(");
        sb.append(str);
        sb.append(")");
        String sb2 = sb.toString();
        this.mLoggerManager.log(IronSourceTag.API, sb2, 1);
        try {
            if (this.mIsDemandOnlyIs) {
                String str2 = "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead";
                this.mLoggerManager.log(IronSourceTag.API, str2, 3);
                this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, str2));
            } else if (!isInterstitialConfigurationsReady()) {
                this.mListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildInitFailedError("showInterstitial can't be called before the Interstitial ad unit initialization completed successfully", "Interstitial"));
            } else if (this.mIsIsProgrammatic) {
                showProgrammaticInterstitial(str);
            } else {
                InterstitialPlacement interstitialPlacementToShowWithEvent = getInterstitialPlacementToShowWithEvent(str);
                JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
                if (interstitialPlacementToShowWithEvent != null) {
                    try {
                        mediationAdditionalData.put("placement", interstitialPlacementToShowWithEvent.getPlacementName());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!TextUtils.isEmpty(str)) {
                    mediationAdditionalData.put("placement", str);
                }
                InterstitialEventsManager.getInstance().log(new EventData(2100, mediationAdditionalData));
                if (interstitialPlacementToShowWithEvent != null) {
                    this.mInterstitialManager.setCurrentPlacement(interstitialPlacementToShowWithEvent);
                    this.mInterstitialManager.showInterstitial(interstitialPlacementToShowWithEvent.getPlacementName());
                }
            }
        } catch (Exception e2) {
            this.mLoggerManager.logException(IronSourceTag.API, sb2, e2);
            this.mListenersWrapper.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, e2.getMessage()));
        }
    }

    private InterstitialPlacement getInterstitialPlacement(String str) {
        InterstitialConfigurations interstitialConfigurations = this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations();
        if (interstitialConfigurations != null) {
            return interstitialConfigurations.getInterstitialPlacement(str);
        }
        return null;
    }

    private InterstitialPlacement getDefaultInterstitialPlacement() {
        InterstitialConfigurations interstitialConfigurations = this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations();
        if (interstitialConfigurations != null) {
            return interstitialConfigurations.getDefaultInterstitialPlacement();
        }
        return null;
    }

    private void showProgrammaticInterstitial(String str) {
        String str2 = null;
        try {
            InterstitialPlacement interstitialPlacement = getInterstitialPlacement(str);
            if (interstitialPlacement == null) {
                interstitialPlacement = getDefaultInterstitialPlacement();
            }
            if (interstitialPlacement != null) {
                str2 = interstitialPlacement.getPlacementName();
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceTag.API, "showProgrammaticInterstitial()", e);
        }
        this.mProgIsManager.showInterstitial(str2);
    }

    public boolean isInterstitialReady() {
        Throwable th;
        boolean z;
        boolean z2 = false;
        try {
            if (this.mIsDemandOnlyIs) {
                this.mLoggerManager.log(IronSourceTag.API, "Interstitial was initialized in demand only mode. Use isISDemandOnlyInterstitialReady instead", 3);
                return false;
            }
            z = !this.mIsIsProgrammatic ? !(this.mInterstitialManager == null || !this.mInterstitialManager.isInterstitialReady()) : !(this.mProgIsManager == null || !this.mProgIsManager.isInterstitialReady());
            try {
                InterstitialEventsManager.getInstance().log(new EventData(z ? IronSourceConstants.IS_CHECK_READY_TRUE : IronSourceConstants.IS_CHECK_READY_FALSE, IronSourceUtils.getMediationAdditionalData(false, this.mIsIsProgrammatic)));
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.API;
                StringBuilder sb = new StringBuilder();
                sb.append("isInterstitialReady():");
                sb.append(z);
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
                z2 = z;
            } catch (Throwable th2) {
                th = th2;
                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                IronSourceTag ironSourceTag2 = IronSourceTag.API;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("isInterstitialReady():");
                sb2.append(z);
                ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 1);
                this.mLoggerManager.logException(IronSourceTag.API, "isInterstitialReady()", th);
                return z2;
            }
            return z2;
        } catch (Throwable th3) {
            th = th3;
            z = false;
            IronSourceLoggerManager ironSourceLoggerManager22 = this.mLoggerManager;
            IronSourceTag ironSourceTag22 = IronSourceTag.API;
            StringBuilder sb22 = new StringBuilder();
            sb22.append("isInterstitialReady():");
            sb22.append(z);
            ironSourceLoggerManager22.log(ironSourceTag22, sb22.toString(), 1);
            this.mLoggerManager.logException(IronSourceTag.API, "isInterstitialReady()", th);
            return z2;
        }
    }

    public void setInterstitialListener(InterstitialListener interstitialListener) {
        if (interstitialListener == null) {
            this.mLoggerManager.log(IronSourceTag.API, "setInterstitialListener(ISListener:null)", 1);
        } else {
            this.mLoggerManager.log(IronSourceTag.API, "setInterstitialListener(ISListener)", 1);
        }
        this.mListenersWrapper.setInterstitialListener(interstitialListener);
        ISListenerWrapper.getInstance().setListener(interstitialListener);
        CallbackThrottler.getInstance().setInterstitialListener(interstitialListener);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b9, code lost:
        if (r5.mCurrentServerResponse == null) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c1, code lost:
        if (r5.mCurrentServerResponse.getConfigurations() == null) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00cd, code lost:
        if (r5.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations() != null) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d0, code lost:
        r5.mDemandOnlyIsManager.loadInterstitial(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00d6, code lost:
        r5.mLoggerManager.log(com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API, "No interstitial configurations found", 3);
        com.ironsource.mediationsdk.ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(r6, com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError("the server response does not contain interstitial data", "Interstitial"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ef, code lost:
        return;
     */
    public synchronized void loadDemandOnlyInterstitial(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append("loadISDemandOnlyInterstitial() instanceId=");
        sb.append(str);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        try {
            if (!this.mDidInitInterstitial) {
                String str2 = "initISDemandOnly() must be called before loadISDemandOnlyInterstitial()";
                this.mLoggerManager.log(IronSourceTag.API, str2, 3);
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, str2));
                return;
            } else if (!this.mIsDemandOnlyIs) {
                String str3 = "Interstitial was initialized in mediation mode. Use loadInterstitial instead";
                this.mLoggerManager.log(IronSourceTag.API, str3, 3);
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, str3));
                return;
            } else {
                EInitStatus currentInitStatus = MediationInitializer.getInstance().getCurrentInitStatus();
                if (currentInitStatus == EInitStatus.INIT_FAILED) {
                    this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                    ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                    return;
                } else if (currentInitStatus != EInitStatus.INIT_IN_PROGRESS) {
                    synchronized (this.mDemandOnlyIsLoadBeforeInitCompleted) {
                        if (this.mDemandOnlyIsManager == null) {
                            this.mDemandOnlyIsLoadBeforeInitCompleted.add(str);
                            return;
                        }
                    }
                } else if (MediationInitializer.getInstance().isInProgressMoreThan15Secs()) {
                    this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                    ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                } else {
                    this.mDemandOnlyIsLoadBeforeInitCompleted.add(str);
                }
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceTag.API, "loadDemandOnlyInterstitial", th);
            ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, th.getMessage()));
        }
        return;
    }

    public void showDemandOnlyInterstitial(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append("showISDemandOnlyInterstitial() instanceId=");
        sb.append(str);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        try {
            if (!this.mIsDemandOnlyIs) {
                this.mLoggerManager.log(IronSourceTag.API, "Interstitial was initialized in mediation mode. Use showInterstitial instead", 3);
            } else if (this.mDemandOnlyIsManager == null) {
                String str2 = "Interstitial video was not initiated";
                this.mLoggerManager.log(IronSourceTag.API, str2, 3);
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdShowFailed(str, new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, str2));
            } else {
                this.mDemandOnlyIsManager.showInterstitial(str);
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceTag.API, "showISDemandOnlyInterstitial", e);
            ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdShowFailed(str, ErrorBuilder.buildInitFailedError("showISDemandOnlyInterstitial can't be called before the Interstitial ad unit initialization completed successfully", "Interstitial"));
        }
    }

    public synchronized boolean isDemandOnlyInterstitialReady(String str) {
        return this.mDemandOnlyIsManager != null && this.mDemandOnlyIsManager.isInterstitialReady(str);
    }

    public void setISDemandOnlyInterstitialListener(ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener) {
        ISDemandOnlyListenerWrapper.getInstance().setListener(iSDemandOnlyInterstitialListener);
    }

    private boolean isInterstitialConfigurationsReady() {
        return (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations() == null) ? false : true;
    }

    private InterstitialPlacement getInterstitialPlacementToShowWithEvent(String str) {
        InterstitialPlacement interstitialPlacement = getInterstitialPlacement(str);
        if (interstitialPlacement == null) {
            this.mLoggerManager.log(IronSourceTag.API, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
            interstitialPlacement = getDefaultInterstitialPlacement();
            if (interstitialPlacement == null) {
                this.mLoggerManager.log(IronSourceTag.API, "Default placement was not found, please make sure you are using the right placements.", 3);
                return null;
            }
        }
        String cappingMessage = getCappingMessage(interstitialPlacement.getPlacementName(), getInterstitialCappingStatus(interstitialPlacement.getPlacementName()));
        if (TextUtils.isEmpty(cappingMessage)) {
            return interstitialPlacement;
        }
        this.mLoggerManager.log(IronSourceTag.API, cappingMessage, 1);
        this.mListenersWrapper.setInterstitialPlacement(interstitialPlacement);
        this.mListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildCappedPerPlacementError(cappingMessage));
        return null;
    }

    private boolean isOfferwallConfigurationsReady() {
        return (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getOfferwallConfigurations() == null) ? false : true;
    }

    public void showOfferwall() {
        String str = "showOfferwall()";
        try {
            this.mLoggerManager.log(IronSourceTag.API, str, 1);
            if (!isOfferwallConfigurationsReady()) {
                this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildInitFailedError("showOfferwall can't be called before the Offerwall ad unit initialization completed successfully", IronSourceConstants.OFFERWALL_AD_UNIT));
                return;
            }
            OfferwallPlacement defaultOfferwallPlacement = this.mCurrentServerResponse.getConfigurations().getOfferwallConfigurations().getDefaultOfferwallPlacement();
            if (defaultOfferwallPlacement != null) {
                showOfferwall(defaultOfferwallPlacement.getPlacementName());
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceTag.API, str, e);
            this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildInitFailedError("showOfferwall can't be called before the Offerwall ad unit initialization completed successfully", IronSourceConstants.OFFERWALL_AD_UNIT));
        }
    }

    public void showOfferwall(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("showOfferwall(");
        sb.append(str);
        sb.append(")");
        String sb2 = sb.toString();
        this.mLoggerManager.log(IronSourceTag.API, sb2, 1);
        try {
            if (!isOfferwallConfigurationsReady()) {
                this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildInitFailedError("showOfferwall can't be called before the Offerwall ad unit initialization completed successfully", IronSourceConstants.OFFERWALL_AD_UNIT));
                return;
            }
            OfferwallPlacement offerwallPlacement = this.mCurrentServerResponse.getConfigurations().getOfferwallConfigurations().getOfferwallPlacement(str);
            if (offerwallPlacement == null) {
                this.mLoggerManager.log(IronSourceTag.API, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
                offerwallPlacement = this.mCurrentServerResponse.getConfigurations().getOfferwallConfigurations().getDefaultOfferwallPlacement();
                if (offerwallPlacement == null) {
                    this.mLoggerManager.log(IronSourceTag.API, "Default placement was not found, please make sure you are using the right placements.", 3);
                    return;
                }
            }
            this.mOfferwallManager.showOfferwall(offerwallPlacement.getPlacementName());
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceTag.API, sb2, e);
            this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildInitFailedError("showOfferwall can't be called before the Offerwall ad unit initialization completed successfully", IronSourceConstants.OFFERWALL_AD_UNIT));
        }
    }

    public boolean isOfferwallAvailable() {
        try {
            if (this.mOfferwallManager != null) {
                return this.mOfferwallManager.isOfferwallAvailable();
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public void getOfferwallCredits() {
        String str = "getOfferwallCredits()";
        this.mLoggerManager.log(IronSourceTag.API, str, 1);
        try {
            this.mOfferwallManager.getOfferwallCredits();
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceTag.API, str, th);
        }
    }

    public void setOfferwallListener(OfferwallListener offerwallListener) {
        if (offerwallListener == null) {
            this.mLoggerManager.log(IronSourceTag.API, "setOfferwallListener(OWListener:null)", 1);
        } else {
            this.mLoggerManager.log(IronSourceTag.API, "setOfferwallListener(OWListener)", 1);
        }
        this.mListenersWrapper.setOfferwallListener(offerwallListener);
    }

    public void setLogListener(LogListener logListener) {
        if (logListener == null) {
            this.mLoggerManager.log(IronSourceTag.API, "setLogListener(LogListener:null)", 1);
            return;
        }
        this.mPublisherLogger.setLogListener(logListener);
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append("setLogListener(LogListener:");
        sb.append(logListener.getClass().getSimpleName());
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
    }

    public void setRewardedInterstitialListener(RewardedInterstitialListener rewardedInterstitialListener) {
        this.mListenersWrapper.setRewardedInterstitialListener(rewardedInterstitialListener);
    }

    private boolean isBannerConfigurationsReady() {
        return (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getBannerConfigurations() == null) ? false : true;
    }

    public IronSourceBannerLayout createBanner(Activity activity, ISBannerSize iSBannerSize) {
        this.mLoggerManager.log(IronSourceTag.API, "createBanner()", 1);
        if (activity != null) {
            return new IronSourceBannerLayout(activity, iSBannerSize);
        }
        this.mLoggerManager.log(IronSourceTag.API, "createBanner() : Activity cannot be null", 3);
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00df, code lost:
        if (r4.mCurrentServerResponse == null) goto L_0x0100;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e7, code lost:
        if (r4.mCurrentServerResponse.getConfigurations() == null) goto L_0x0100;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f3, code lost:
        if (r4.mCurrentServerResponse.getConfigurations().getBannerConfigurations() != null) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f6, code lost:
        r4.mBannerManager.loadBanner(r5, getBannerPlacement(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ff, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0100, code lost:
        r4.mLoggerManager.log(com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API, "No banner configurations found", 3);
        com.ironsource.mediationsdk.BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(r5, new com.ironsource.mediationsdk.logger.IronSourceError(com.ironsource.mediationsdk.logger.IronSourceError.ERROR_BN_LOAD_NO_CONFIG, "No banner configurations found"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0119, code lost:
        return;
     */
    public void loadBanner(IronSourceBannerLayout ironSourceBannerLayout, String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append("loadBanner(");
        sb.append(str);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (ironSourceBannerLayout == null) {
            this.mLoggerManager.log(IronSourceTag.API, "loadBanner can't be called with a null parameter", 1);
        } else if (!this.mDidInitBanner) {
            this.mLoggerManager.log(IronSourceTag.API, "init() must be called before loadBanner()", 3);
        } else if (!ironSourceBannerLayout.getSize().getDescription().equals("CUSTOM") || (ironSourceBannerLayout.getSize().getWidth() > 0 && ironSourceBannerLayout.getSize().getHeight() > 0)) {
            EInitStatus currentInitStatus = MediationInitializer.getInstance().getCurrentInitStatus();
            if (currentInitStatus == EInitStatus.INIT_FAILED) {
                this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(ironSourceBannerLayout, new IronSourceError(600, "Init() had failed"));
            } else if (currentInitStatus == EInitStatus.INIT_IN_PROGRESS) {
                if (MediationInitializer.getInstance().isInProgressMoreThan15Secs()) {
                    this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                    BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(ironSourceBannerLayout, new IronSourceError(IronSourceError.ERROR_BN_LOAD_AFTER_LONG_INITIATION, "Init had failed"));
                } else {
                    this.mBnLayoutToLoad = ironSourceBannerLayout;
                    this.mIsBnLoadBeforeInitCompleted = Boolean.valueOf(true);
                    this.mBnPlacementToLoad = str;
                }
            } else {
                synchronized (this.mIsBnLoadBeforeInitCompleted) {
                    if (this.mBannerManager == null) {
                        this.mIsBnLoadBeforeInitCompleted = Boolean.valueOf(true);
                    }
                }
            }
        } else {
            this.mLoggerManager.log(IronSourceTag.API, "loadBanner: Unsupported banner size. Height and width must be bigger than 0", 3);
            BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(ironSourceBannerLayout, ErrorBuilder.unsupportedBannerSize(""));
        }
    }

    public void loadBanner(IronSourceBannerLayout ironSourceBannerLayout) {
        loadBanner(ironSourceBannerLayout, "");
    }

    public void destroyBanner(IronSourceBannerLayout ironSourceBannerLayout) {
        this.mLoggerManager.log(IronSourceTag.API, "destroyBanner()", 1);
        try {
            if (this.mBannerManager != null) {
                this.mBannerManager.destroyBanner(ironSourceBannerLayout);
            }
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceTag.API, "destroyBanner()", th);
        }
    }

    /* access modifiers changed from: 0000 */
    public ServerResponseWrapper getServerResponse(Context context, String str, IResponseListener iResponseListener) {
        synchronized (this.mServerResponseLocker) {
            if (this.mCurrentServerResponse != null) {
                ServerResponseWrapper serverResponseWrapper = new ServerResponseWrapper(this.mCurrentServerResponse);
                return serverResponseWrapper;
            }
            ServerResponseWrapper connectAndGetServerResponse = connectAndGetServerResponse(context, str, iResponseListener);
            if (connectAndGetServerResponse == null || !connectAndGetServerResponse.isValidResponse()) {
                IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, "Null or invalid response. Trying to get cached response", 0);
                connectAndGetServerResponse = getCachedResponse(context, str);
            }
            if (connectAndGetServerResponse != null) {
                this.mCurrentServerResponse = connectAndGetServerResponse;
                IronSourceUtils.saveLastResponse(context, connectAndGetServerResponse.toString());
                initializeSettingsFromServerResponse(this.mCurrentServerResponse, context);
            }
            InterstitialEventsManager.getInstance().setHasServerResponse(true);
            RewardedVideoEventsManager.getInstance().setHasServerResponse(true);
            return connectAndGetServerResponse;
        }
    }

    private ServerResponseWrapper getCachedResponse(Context context, String str) {
        JSONObject jSONObject;
        try {
            jSONObject = new JSONObject(IronSourceUtils.getLastResponse(context));
        } catch (JSONException unused) {
            jSONObject = new JSONObject();
        }
        String optString = jSONObject.optString(ServerResponseWrapper.APP_KEY_FIELD);
        String optString2 = jSONObject.optString("userId");
        String optString3 = jSONObject.optString(ServerResponseWrapper.RESPONSE_FIELD);
        if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2) || TextUtils.isEmpty(optString3) || getIronSourceAppKey() == null || !optString.equals(getIronSourceAppKey()) || !optString2.equals(str)) {
            return null;
        }
        ServerResponseWrapper serverResponseWrapper = new ServerResponseWrapper(context, optString, optString2, optString3);
        IronSourceError buildUsingCachedConfigurationError = ErrorBuilder.buildUsingCachedConfigurationError(optString, optString2);
        this.mLoggerManager.log(IronSourceTag.INTERNAL, buildUsingCachedConfigurationError.toString(), 1);
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(buildUsingCachedConfigurationError.toString());
        sb.append(": ");
        sb.append(serverResponseWrapper.toString());
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        RewardedVideoEventsManager.getInstance().log(new EventData(IronSourceConstants.USING_CACHE_FOR_INIT_EVENT, IronSourceUtils.getMediationAdditionalData(false)));
        return serverResponseWrapper;
    }

    private ServerResponseWrapper connectAndGetServerResponse(Context context, String str, IResponseListener iResponseListener) {
        ServerResponseWrapper serverResponseWrapper;
        if (!IronSourceUtils.isNetworkConnected(context)) {
            return null;
        }
        try {
            String advertiserId = getAdvertiserId(context);
            if (TextUtils.isEmpty(advertiserId)) {
                advertiserId = DeviceStatus.getOrGenerateOnceUniqueIdentifier(context);
                IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, "using custom identifier", 1);
            }
            String stringFromURL = HttpFunctions.getStringFromURL(ServerURL.getCPVProvidersURL(context, getIronSourceAppKey(), str, advertiserId, getMediationType(), this.mIronSegment != null ? this.mIronSegment.getSegmentData() : null), iResponseListener);
            if (stringFromURL == null) {
                return null;
            }
            if (IronSourceUtils.getSerr() == 1) {
                String optString = new JSONObject(stringFromURL).optString(ServerResponseWrapper.RESPONSE_FIELD, null);
                if (TextUtils.isEmpty(optString)) {
                    return null;
                }
                stringFromURL = IronSourceAES.decode(IronSourceUtils.KEY, optString);
            }
            serverResponseWrapper = new ServerResponseWrapper(context, getIronSourceAppKey(), str, stringFromURL);
            try {
                if (!serverResponseWrapper.isValidResponse()) {
                    return null;
                }
                return serverResponseWrapper;
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return serverResponseWrapper;
            }
        } catch (Exception e2) {
            e = e2;
            serverResponseWrapper = null;
            e.printStackTrace();
            return serverResponseWrapper;
        }
    }

    private void initializeSettingsFromServerResponse(ServerResponseWrapper serverResponseWrapper, Context context) {
        initializeLoggerManager(serverResponseWrapper);
        initializeEventsSettings(serverResponseWrapper, context);
    }

    private void initializeEventsSettings(ServerResponseWrapper serverResponseWrapper, Context context) {
        boolean isEventsEnabled = isRewardedVideoConfigurationsReady() ? serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().isEventsEnabled() : false;
        boolean isEventsEnabled2 = isInterstitialConfigurationsReady() ? serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().isEventsEnabled() : false;
        boolean isEventsEnabled3 = isBannerConfigurationsReady() ? serverResponseWrapper.getConfigurations().getBannerConfigurations().getBannerEventsConfigurations().isEventsEnabled() : false;
        boolean isEventsEnabled4 = isOfferwallConfigurationsReady() ? serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().isEventsEnabled() : false;
        if (isEventsEnabled) {
            RewardedVideoEventsManager.getInstance().setFormatterType(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getEventsType(), context);
            RewardedVideoEventsManager.getInstance().setEventsUrl(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getEventsURL(), context);
            RewardedVideoEventsManager.getInstance().setMaxNumberOfEvents(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getMaxNumberOfEvents());
            RewardedVideoEventsManager.getInstance().setMaxEventsPerBatch(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getMaxEventsPerBatch());
            RewardedVideoEventsManager.getInstance().setBackupThreshold(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getEventsBackupThreshold());
            RewardedVideoEventsManager.getInstance().setOptOutEvents(serverResponseWrapper.getConfigurations().getRewardedVideoConfigurations().getRewardedVideoEventsConfigurations().getOptOutEvents(), context);
            RewardedVideoEventsManager.getInstance().setServerSegmentData(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData());
        } else if (isEventsEnabled4) {
            RewardedVideoEventsManager.getInstance().setFormatterType(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getEventsType(), context);
            RewardedVideoEventsManager.getInstance().setEventsUrl(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getEventsURL(), context);
            RewardedVideoEventsManager.getInstance().setMaxNumberOfEvents(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getMaxNumberOfEvents());
            RewardedVideoEventsManager.getInstance().setMaxEventsPerBatch(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getMaxEventsPerBatch());
            RewardedVideoEventsManager.getInstance().setBackupThreshold(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getEventsBackupThreshold());
            RewardedVideoEventsManager.getInstance().setOptOutEvents(serverResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferWallEventsConfigurations().getOptOutEvents(), context);
            RewardedVideoEventsManager.getInstance().setServerSegmentData(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData());
        } else {
            RewardedVideoEventsManager.getInstance().setIsEventsEnabled(false);
        }
        if (isEventsEnabled2) {
            InterstitialEventsManager.getInstance().setFormatterType(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getEventsType(), context);
            InterstitialEventsManager.getInstance().setEventsUrl(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getEventsURL(), context);
            InterstitialEventsManager.getInstance().setMaxNumberOfEvents(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getMaxNumberOfEvents());
            InterstitialEventsManager.getInstance().setMaxEventsPerBatch(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getMaxEventsPerBatch());
            InterstitialEventsManager.getInstance().setBackupThreshold(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getEventsBackupThreshold());
            InterstitialEventsManager.getInstance().setOptOutEvents(serverResponseWrapper.getConfigurations().getInterstitialConfigurations().getInterstitialEventsConfigurations().getOptOutEvents(), context);
            InterstitialEventsManager.getInstance().setServerSegmentData(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData());
        } else if (isEventsEnabled3) {
            ApplicationEvents bannerEventsConfigurations = serverResponseWrapper.getConfigurations().getBannerConfigurations().getBannerEventsConfigurations();
            InterstitialEventsManager.getInstance().setFormatterType(bannerEventsConfigurations.getEventsType(), context);
            InterstitialEventsManager.getInstance().setEventsUrl(bannerEventsConfigurations.getEventsURL(), context);
            InterstitialEventsManager.getInstance().setMaxNumberOfEvents(bannerEventsConfigurations.getMaxNumberOfEvents());
            InterstitialEventsManager.getInstance().setMaxEventsPerBatch(bannerEventsConfigurations.getMaxEventsPerBatch());
            InterstitialEventsManager.getInstance().setBackupThreshold(bannerEventsConfigurations.getEventsBackupThreshold());
            InterstitialEventsManager.getInstance().setOptOutEvents(bannerEventsConfigurations.getOptOutEvents(), context);
            InterstitialEventsManager.getInstance().setServerSegmentData(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData());
        } else {
            InterstitialEventsManager.getInstance().setIsEventsEnabled(false);
        }
    }

    private void initializeLoggerManager(ServerResponseWrapper serverResponseWrapper) {
        this.mPublisherLogger.setDebugLevel(serverResponseWrapper.getConfigurations().getApplicationConfigurations().getLoggerConfigurations().getPublisherLoggerLevel());
        this.mLoggerManager.setLoggerDebugLevel(ConsoleLogger.NAME, serverResponseWrapper.getConfigurations().getApplicationConfigurations().getLoggerConfigurations().getConsoleLoggerLevel());
    }

    public void removeRewardedVideoListener() {
        this.mLoggerManager.log(IronSourceTag.API, "removeRewardedVideoListener()", 1);
        this.mListenersWrapper.setRewardedVideoListener(null);
    }

    public void removeInterstitialListener() {
        this.mLoggerManager.log(IronSourceTag.API, "removeInterstitialListener()", 1);
        this.mListenersWrapper.setInterstitialListener(null);
    }

    public void removeOfferwallListener() {
        this.mLoggerManager.log(IronSourceTag.API, "removeOfferwallListener()", 1);
        this.mListenersWrapper.setOfferwallListener(null);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void setIronSourceUserId(String str) {
        this.mUserId = str;
    }

    public synchronized String getIronSourceAppKey() {
        return this.mAppKey;
    }

    public synchronized String getIronSourceUserId() {
        return this.mUserId;
    }

    private ConfigValidationResult validateAppKey(String str) {
        ConfigValidationResult configValidationResult = new ConfigValidationResult();
        if (str == null) {
            configValidationResult.setInvalid(new IronSourceError(IronSourceError.ERROR_CODE_INVALID_KEY_VALUE, "Init Fail - appKey is missing"));
        } else if (!validateLength(str, 5, 10)) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidCredentialsError(ServerResponseWrapper.APP_KEY_FIELD, str, "length should be between 5-10 characters"));
        } else if (!validateAlphanumeric(str)) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidCredentialsError(ServerResponseWrapper.APP_KEY_FIELD, str, "should contain only english characters and numbers"));
        }
        return configValidationResult;
    }

    private void validateGender(String str, ConfigValidationResult configValidationResult) {
        if (str != null) {
            try {
                String trim = str.toLowerCase().trim();
                if (!"male".equals(trim) && !"female".equals(trim) && !"unknown".equals(trim)) {
                    configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError("gender", IronSourceConstants.SUPERSONIC_CONFIG_NAME, "gender value should be one of male/female/unknown."));
                }
            } catch (Exception unused) {
                configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError("gender", IronSourceConstants.SUPERSONIC_CONFIG_NAME, "gender value should be one of male/female/unknown."));
            }
        }
    }

    private void validateAge(int i, ConfigValidationResult configValidationResult) {
        if (i < 5 || i > 120) {
            try {
                configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError(IronSourceSegment.AGE, IronSourceConstants.SUPERSONIC_CONFIG_NAME, "age value should be between 5-120"));
            } catch (NumberFormatException unused) {
                configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError(IronSourceSegment.AGE, IronSourceConstants.SUPERSONIC_CONFIG_NAME, "age value should be between 5-120"));
            }
        }
    }

    private void validateSegment(String str, ConfigValidationResult configValidationResult) {
        if (str != null) {
            try {
                if (str.length() > 64) {
                    configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError("segment", IronSourceConstants.SUPERSONIC_CONFIG_NAME, "segment value should not exceed 64 characters."));
                }
            } catch (Exception unused) {
                configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError("segment", IronSourceConstants.SUPERSONIC_CONFIG_NAME, "segment value should not exceed 64 characters."));
            }
        }
    }

    private void validateDynamicUserId(String str, ConfigValidationResult configValidationResult) {
        if (!validateLength(str, 1, 128)) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidKeyValueError(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceConstants.SUPERSONIC_CONFIG_NAME, "dynamicUserId is invalid, should be between 1-128 chars in length."));
        }
    }

    private boolean validateLength(String str, int i, int i2) {
        return str != null && str.length() >= i && str.length() <= i2;
    }

    private boolean validateAlphanumeric(String str) {
        if (str == null) {
            return false;
        }
        return str.matches("^[a-zA-Z0-9]*$");
    }

    public InterstitialPlacement getInterstitialPlacementInfo(String str) {
        try {
            InterstitialPlacement interstitialPlacement = getInterstitialPlacement(str);
            try {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.API;
                StringBuilder sb = new StringBuilder();
                sb.append("getPlacementInfo(placement: ");
                sb.append(str);
                sb.append("):");
                sb.append(interstitialPlacement);
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
                return interstitialPlacement;
            } catch (Exception unused) {
                return interstitialPlacement;
            }
        } catch (Exception unused2) {
            return null;
        }
    }

    public Placement getRewardedVideoPlacementInfo(String str) {
        try {
            Placement rewardedVideoPlacement = getRewardedVideoPlacement(str);
            try {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.API;
                StringBuilder sb = new StringBuilder();
                sb.append("getPlacementInfo(placement: ");
                sb.append(str);
                sb.append("):");
                sb.append(rewardedVideoPlacement);
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
                return rewardedVideoPlacement;
            } catch (Exception unused) {
                return rewardedVideoPlacement;
            }
        } catch (Exception unused2) {
            return null;
        }
    }

    public String getAdvertiserId(Context context) {
        try {
            String[] advertisingIdInfo = DeviceStatus.getAdvertisingIdInfo(context);
            return (advertisingIdInfo.length <= 0 || advertisingIdInfo[0] == null) ? "" : advertisingIdInfo[0];
        } catch (Exception unused) {
            return "";
        }
    }

    public void shouldTrackNetworkState(Context context, boolean z) {
        if (this.mRewardedVideoManager != null) {
            this.mRewardedVideoManager.shouldTrackNetworkState(context, z);
        }
        if (this.mInterstitialManager != null) {
            this.mInterstitialManager.shouldTrackNetworkState(context, z);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isInterstitialPlacementCapped(String str) {
        boolean z = false;
        if (this.mIsDemandOnlyIs) {
            return false;
        }
        if (getInterstitialCappingStatus(str) != ECappingStatus.NOT_CAPPED) {
            z = true;
        }
        if (z) {
            JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(this.mIsDemandOnlyIs, this.mIsIsProgrammatic);
            try {
                mediationAdditionalData.put("placement", str);
                if (this.mIsIsProgrammatic) {
                    mediationAdditionalData.put(IronSourceConstants.EVENTS_PROGRAMMATIC, 1);
                }
            } catch (Exception unused) {
            }
            InterstitialEventsManager.getInstance().log(new EventData(IronSourceConstants.IS_CHECK_CAPPED_TRUE, mediationAdditionalData));
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public boolean isRewardedVideoPlacementCapped(String str) {
        boolean z;
        ECappingStatus rewardedVideoCappingStatus = getRewardedVideoCappingStatus(str);
        if (rewardedVideoCappingStatus != null) {
            switch (rewardedVideoCappingStatus) {
                case CAPPED_PER_DELIVERY:
                case CAPPED_PER_COUNT:
                case CAPPED_PER_PACE:
                    z = true;
                    break;
            }
        }
        z = false;
        sendIsCappedEvent(z, str);
        return z;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0052 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0053  */
    public boolean isBannerPlacementCapped(String str) {
        BannerPlacement bannerPlacement;
        if (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getBannerConfigurations() == null) {
            return false;
        }
        BannerPlacement bannerPlacement2 = null;
        try {
            bannerPlacement = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations().getBannerPlacement(str);
            if (bannerPlacement == null) {
                try {
                    bannerPlacement2 = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations().getDefaultBannerPlacement();
                    if (bannerPlacement2 == null) {
                        this.mLoggerManager.log(IronSourceTag.API, "Banner default placement was not found", 3);
                        return false;
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    bannerPlacement2 = bannerPlacement;
                    e = exc;
                    e.printStackTrace();
                    bannerPlacement = bannerPlacement2;
                    if (bannerPlacement == null) {
                    }
                }
                bannerPlacement = bannerPlacement2;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            bannerPlacement = bannerPlacement2;
            if (bannerPlacement == null) {
            }
        }
        if (bannerPlacement == null) {
            return false;
        }
        return CappingManager.isBnPlacementCapped(this.mActivity, bannerPlacement.getPlacementName());
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0040  */
    private ECappingStatus getInterstitialCappingStatus(String str) {
        InterstitialPlacement interstitialPlacement;
        if (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getInterstitialConfigurations() == null) {
            return ECappingStatus.NOT_CAPPED;
        }
        InterstitialPlacement interstitialPlacement2 = null;
        try {
            interstitialPlacement = getInterstitialPlacement(str);
            if (interstitialPlacement == null) {
                try {
                    interstitialPlacement2 = getDefaultInterstitialPlacement();
                    if (interstitialPlacement2 == null) {
                        this.mLoggerManager.log(IronSourceTag.API, "Default placement was not found", 3);
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    interstitialPlacement2 = interstitialPlacement;
                    e = exc;
                    e.printStackTrace();
                    interstitialPlacement = interstitialPlacement2;
                    if (interstitialPlacement != null) {
                    }
                }
                interstitialPlacement = interstitialPlacement2;
            }
        } catch (Exception e2) {
            e = e2;
        }
        if (interstitialPlacement != null) {
            return ECappingStatus.NOT_CAPPED;
        }
        return CappingManager.isPlacementCapped((Context) this.mActivity, interstitialPlacement);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0040  */
    private ECappingStatus getRewardedVideoCappingStatus(String str) {
        Placement placement;
        if (this.mCurrentServerResponse == null || this.mCurrentServerResponse.getConfigurations() == null || this.mCurrentServerResponse.getConfigurations().getRewardedVideoConfigurations() == null) {
            return ECappingStatus.NOT_CAPPED;
        }
        Placement placement2 = null;
        try {
            placement = getRewardedVideoPlacement(str);
            if (placement == null) {
                try {
                    placement2 = getDefaultRewardedVideoPlacement();
                    if (placement2 == null) {
                        this.mLoggerManager.log(IronSourceTag.API, "Default placement was not found", 3);
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    placement2 = placement;
                    e = exc;
                    e.printStackTrace();
                    placement = placement2;
                    if (placement != null) {
                    }
                }
                placement = placement2;
            }
        } catch (Exception e2) {
            e = e2;
        }
        if (placement != null) {
            return ECappingStatus.NOT_CAPPED;
        }
        return CappingManager.isPlacementCapped((Context) this.mActivity, placement);
    }

    private void sendIsCappedEvent(boolean z, String str) {
        if (z) {
            JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(this.mIsDemandOnlyRv);
            if (str != null) {
                addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{"placement", str}});
            }
            if (this.mIsRvProgrammatic) {
                addToDictionary(mediationAdditionalData, new Object[][]{new Object[]{IronSourceConstants.EVENTS_PROGRAMMATIC, Integer.valueOf(1)}});
            }
            RewardedVideoEventsManager.getInstance().log(new EventData(IronSourceConstants.RV_API_IS_CAPPED_TRUE, mediationAdditionalData));
        }
    }

    /* access modifiers changed from: 0000 */
    public String getCappingMessage(String str, ECappingStatus eCappingStatus) {
        if (eCappingStatus == null) {
            return null;
        }
        switch (eCappingStatus) {
            case CAPPED_PER_DELIVERY:
            case CAPPED_PER_COUNT:
            case CAPPED_PER_PACE:
                StringBuilder sb = new StringBuilder();
                sb.append("placement ");
                sb.append(str);
                sb.append(" is capped");
                return sb.toString();
            default:
                return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public ServerResponseWrapper getCurrentServerResponse() {
        return this.mCurrentServerResponse;
    }

    /* access modifiers changed from: 0000 */
    public void setSegmentListener(SegmentListener segmentListener) {
        if (this.mListenersWrapper != null) {
            this.mListenersWrapper.setSegmentListener(segmentListener);
            MediationInitializer.getInstance().setSegmentListener(this.mListenersWrapper);
        }
    }

    /* access modifiers changed from: 0000 */
    public HashSet<String> getAllSettingsForProvider(String str, String str2) {
        if (this.mCurrentServerResponse == null) {
            return new HashSet<>();
        }
        return this.mCurrentServerResponse.getProviderSettingsHolder().getProviderSettingsByReflectionName(str, str2);
    }

    private BannerPlacement getBannerPlacement(String str) {
        BannerConfigurations bannerConfigurations = this.mCurrentServerResponse.getConfigurations().getBannerConfigurations();
        if (bannerConfigurations == null) {
            return null;
        }
        if (TextUtils.isEmpty(str)) {
            return bannerConfigurations.getDefaultBannerPlacement();
        }
        BannerPlacement bannerPlacement = bannerConfigurations.getBannerPlacement(str);
        if (bannerPlacement != null) {
            return bannerPlacement;
        }
        return bannerConfigurations.getDefaultBannerPlacement();
    }

    public synchronized String getSessionId() {
        return this.mSessionId;
    }

    public void setConsent(boolean z) {
        this.mConsent = Boolean.valueOf(z);
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append("setConsent : ");
        sb.append(z);
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mRewardedVideoManager != null) {
            this.mRewardedVideoManager.setConsent(z);
        }
        if (this.mInterstitialManager != null) {
            this.mInterstitialManager.setConsent(z);
        }
        if (this.mBannerManager != null) {
            this.mBannerManager.setConsent(z);
        }
        if (this.mOfferwallAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Offerwall | setConsent(consent:");
            sb2.append(z);
            sb2.append(")");
            ironSourceLoggerManager.log(ironSourceTag2, sb2.toString(), 1);
            this.mOfferwallAdapter.setConsent(z);
        }
        if (this.mProgRvManager != null) {
            this.mProgRvManager.setConsent(z);
        }
        if (this.mProgIsManager != null) {
            this.mProgIsManager.setConsent(z);
        }
        if (this.mDemandOnlyIsManager != null) {
            this.mDemandOnlyIsManager.setConsent(z);
        }
        if (this.mDemandOnlyRvManager != null) {
            this.mDemandOnlyRvManager.setConsent(z);
        }
        int i = 40;
        if (!z) {
            i = 41;
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, IronSourceUtils.getMediationAdditionalData(false)));
    }

    /* access modifiers changed from: 0000 */
    public Boolean getConsent() {
        return this.mConsent;
    }

    private void addToDictionary(JSONObject jSONObject, Object[][] objArr) {
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    jSONObject.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("IronSourceObject addToDictionary: ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
    }
}
