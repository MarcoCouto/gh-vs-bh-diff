package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyIsManagerListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.sdk.constants.Constants.JSMethods;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class DemandOnlyIsSmash extends DemandOnlySmash implements InterstitialSmashListener {
    /* access modifiers changed from: private */
    public DemandOnlyIsManagerListener mListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;
    private int mLoadTimeoutSecs;
    /* access modifiers changed from: private */
    public SMASH_STATE mState;
    private Timer mTimer = null;

    public void onInterstitialInitFailed(IronSourceError ironSourceError) {
    }

    public void onInterstitialInitSuccess() {
    }

    public DemandOnlyIsSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, DemandOnlyIsManagerListener demandOnlyIsManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getInterstitialSettings()), abstractAdapter);
        this.mListener = demandOnlyIsManagerListener;
        this.mLoadTimeoutSecs = i;
        this.mState = SMASH_STATE.NOT_LOADED;
        this.mAdapter.initInterstitial(activity, str, str2, this.mAdUnitSettings, this);
    }

    public synchronized void loadInterstitial() {
        StringBuilder sb = new StringBuilder();
        sb.append("loadInterstitial state=");
        sb.append(this.mState.name());
        logInternal(sb.toString());
        if (this.mState != SMASH_STATE.NOT_LOADED) {
            if (this.mState != SMASH_STATE.LOADED) {
                if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
                    this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_ALREADY_IN_PROGRESS, "load already in progress"), this, 0);
                } else {
                    this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_ALREADY_IN_PROGRESS, "cannot load because show is in progress"), this, 0);
                }
            }
        }
        this.mState = SMASH_STATE.LOAD_IN_PROGRESS;
        startTimer();
        this.mLoadStartTime = new Date().getTime();
        this.mAdapter.loadInterstitial(this.mAdUnitSettings, this);
    }

    public synchronized void showInterstitial() {
        StringBuilder sb = new StringBuilder();
        sb.append("showInterstitial state=");
        sb.append(this.mState.name());
        logInternal(sb.toString());
        if (this.mState == SMASH_STATE.LOADED) {
            this.mState = SMASH_STATE.SHOW_IN_PROGRESS;
            this.mAdapter.showInterstitial(this.mAdUnitSettings, this);
        } else {
            this.mListener.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_CALL_LOAD_BEFORE_SHOW, "load must be called before show"), this);
        }
    }

    public synchronized boolean isInterstitialReady() {
        return this.mAdapter.isInterstitialReady(this.mAdUnitSettings);
    }

    private void stopTimer() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }

    private void startTimer() {
        logInternal("start timer");
        stopTimer();
        this.mTimer = new Timer();
        this.mTimer.schedule(new TimerTask() {
            public void run() {
                DemandOnlyIsSmash demandOnlyIsSmash = DemandOnlyIsSmash.this;
                StringBuilder sb = new StringBuilder();
                sb.append("load timed out state=");
                sb.append(DemandOnlyIsSmash.this.mState.toString());
                demandOnlyIsSmash.logInternal(sb.toString());
                if (DemandOnlyIsSmash.this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
                    DemandOnlyIsSmash.this.mState = SMASH_STATE.NOT_LOADED;
                    DemandOnlyIsSmash.this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_TIMED_OUT, "load timed out"), DemandOnlyIsSmash.this, new Date().getTime() - DemandOnlyIsSmash.this.mLoadStartTime);
                }
            }
        }, (long) (this.mLoadTimeoutSecs * 1000));
    }

    public synchronized void onInterstitialAdReady() {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdReady state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mState = SMASH_STATE.LOADED;
            this.mListener.onInterstitialAdReady(this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public synchronized void onInterstitialAdLoadFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdLoadFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        sb.append(" state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mState = SMASH_STATE.NOT_LOADED;
            this.mListener.onInterstitialAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public synchronized void onInterstitialAdOpened() {
        logAdapterCallback("onInterstitialAdOpened");
        this.mListener.onInterstitialAdOpened(this);
    }

    public synchronized void onInterstitialAdClosed() {
        this.mState = SMASH_STATE.NOT_LOADED;
        logAdapterCallback("onInterstitialAdClosed");
        this.mListener.onInterstitialAdClosed(this);
    }

    public synchronized void onInterstitialAdShowSucceeded() {
    }

    public synchronized void onInterstitialAdShowFailed(IronSourceError ironSourceError) {
        this.mState = SMASH_STATE.NOT_LOADED;
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdShowFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        logAdapterCallback(sb.toString());
        this.mListener.onInterstitialAdShowFailed(ironSourceError, this);
    }

    public synchronized void onInterstitialAdClicked() {
        logAdapterCallback(JSMethods.ON_INTERSTITIAL_AD_CLICKED);
        this.mListener.onInterstitialAdClicked(this);
    }

    public synchronized void onInterstitialAdVisible() {
        logAdapterCallback("onInterstitialAdVisible");
        this.mListener.onInterstitialAdVisible(this);
    }

    private void logAdapterCallback(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyInterstitialSmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.ADAPTER_CALLBACK, sb.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyInterstitialSmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }
}
