package com.ironsource.mediationsdk.utils;

import com.ironsource.mediationsdk.ProgSmash;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SessionCappingManager {
    private Map<String, Integer> mShowCountMap = new HashMap();

    public SessionCappingManager(List<ProgSmash> list) {
        for (ProgSmash instanceName : list) {
            this.mShowCountMap.put(instanceName.getInstanceName(), Integer.valueOf(0));
        }
    }

    public void increaseShowCounter(ProgSmash progSmash) {
        synchronized (this) {
            String instanceName = progSmash.getInstanceName();
            if (this.mShowCountMap.containsKey(instanceName)) {
                this.mShowCountMap.put(instanceName, Integer.valueOf(((Integer) this.mShowCountMap.get(instanceName)).intValue() + 1));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        return r2;
     */
    public boolean isCapped(ProgSmash progSmash) {
        synchronized (this) {
            String instanceName = progSmash.getInstanceName();
            boolean z = false;
            if (!this.mShowCountMap.containsKey(instanceName)) {
                return false;
            }
            if (((Integer) this.mShowCountMap.get(instanceName)).intValue() >= progSmash.getMaxAdsPerSession()) {
                z = true;
            }
        }
    }
}
