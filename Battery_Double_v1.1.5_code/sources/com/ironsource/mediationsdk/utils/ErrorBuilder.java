package com.ironsource.mediationsdk.utils;

import android.text.TextUtils;
import com.ironsource.mediationsdk.logger.IronSourceError;

public class ErrorBuilder {
    public static IronSourceError buildNoConfigurationAvailableError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(str);
        sb.append(" Init Fail - Unable to retrieve configurations from the server");
        return new IronSourceError(IronSourceError.ERROR_CODE_NO_CONFIGURATION_AVAILABLE, sb.toString());
    }

    public static IronSourceError buildInvalidConfigurationError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(str);
        sb.append(" Init Fail - Configurations from the server are not valid");
        return new IronSourceError(IronSourceError.ERROR_CODE_NO_CONFIGURATION_AVAILABLE, sb.toString());
    }

    public static IronSourceError buildUsingCachedConfigurationError(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("Mediation - Unable to retrieve configurations from IronSource server, using cached configurations with appKey:");
        sb.append(str);
        sb.append(" and userId:");
        sb.append(str2);
        return new IronSourceError(IronSourceError.ERROR_CODE_USING_CACHED_CONFIGURATION, sb.toString());
    }

    public static IronSourceError buildKeyNotSetError(String str, String str2, String str3) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return getGenericErrorForMissingParams();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str3);
        sb.append(" Mediation - ");
        sb.append(str);
        sb.append(" is not set for ");
        sb.append(str2);
        return new IronSourceError(IronSourceError.ERROR_CODE_KEY_NOT_SET, sb.toString());
    }

    public static IronSourceError buildInvalidKeyValueError(String str, String str2, String str3) {
        String str4;
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return getGenericErrorForMissingParams();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Mediation - ");
        sb.append(str);
        sb.append(" value is not valid for ");
        sb.append(str2);
        if (!TextUtils.isEmpty(str3)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(" - ");
            sb2.append(str3);
            str4 = sb2.toString();
        } else {
            str4 = "";
        }
        sb.append(str4);
        return new IronSourceError(IronSourceError.ERROR_CODE_INVALID_KEY_VALUE, sb.toString());
    }

    public static IronSourceError buildInvalidCredentialsError(String str, String str2, String str3) {
        String str4;
        StringBuilder sb = new StringBuilder();
        sb.append("Init Fail - ");
        sb.append(str);
        sb.append(" value ");
        sb.append(str2);
        sb.append(" is not valid");
        if (!TextUtils.isEmpty(str3)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(" - ");
            sb2.append(str3);
            str4 = sb2.toString();
        } else {
            str4 = "";
        }
        sb.append(str4);
        return new IronSourceError(IronSourceError.ERROR_CODE_INVALID_KEY_VALUE, sb.toString());
    }

    public static IronSourceError buildInitFailedError(String str, String str2) {
        String str3;
        if (TextUtils.isEmpty(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(" init failed due to an unknown error");
            str3 = sb.toString();
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append(" - ");
            sb2.append(str);
            str3 = sb2.toString();
        }
        return new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, str3);
    }

    public static IronSourceError buildNoAdsToShowError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" Show Fail - No ads to show");
        return new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, sb.toString());
    }

    public static IronSourceError buildShowFailedError(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" Show Fail - ");
        sb.append(str2);
        return new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, sb.toString());
    }

    public static IronSourceError buildLoadFailedError(String str, String str2, String str3) {
        String str4;
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(str);
        sb.append(" Load Fail");
        if (!TextUtils.isEmpty(str2)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(" ");
            sb2.append(str2);
            str4 = sb2.toString();
        } else {
            str4 = "";
        }
        sb.append(str4);
        sb.append(" - ");
        String sb3 = sb.toString();
        if (TextUtils.isEmpty(str3)) {
            str3 = "unknown error";
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append(sb3);
        sb4.append(str3);
        return new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, sb4.toString());
    }

    public static IronSourceError buildGenericError(String str) {
        if (TextUtils.isEmpty(str)) {
            str = "An error occurred";
        }
        return new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, str);
    }

    public static IronSourceError buildNoInternetConnectionInitFailError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(str);
        sb.append(" Init Fail - No Internet connection");
        return new IronSourceError(IronSourceError.ERROR_NO_INTERNET_CONNECTION, sb.toString());
    }

    public static IronSourceError buildNoInternetConnectionLoadFailError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(str);
        sb.append(" Load Fail - No Internet connection");
        return new IronSourceError(IronSourceError.ERROR_NO_INTERNET_CONNECTION, sb.toString());
    }

    public static IronSourceError buildNoInternetConnectionShowFailError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(str);
        sb.append(" Show Fail - No Internet connection");
        return new IronSourceError(IronSourceError.ERROR_NO_INTERNET_CONNECTION, sb.toString());
    }

    public static IronSourceError buildCappedPerPlacementError(String str) {
        return new IronSourceError(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT, str);
    }

    public static IronSourceError buildCappedPerSessionError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" Show Fail - Networks have reached their cap per session");
        return new IronSourceError(IronSourceError.ERROR_CAPPED_PER_SESSION, sb.toString());
    }

    public static IronSourceError buildNonExistentInstanceError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" The requested instance does not exist");
        return new IronSourceError(IronSourceError.ERROR_NON_EXISTENT_INSTANCE, sb.toString());
    }

    private static IronSourceError getGenericErrorForMissingParams() {
        return buildGenericError("Mediation - wrong configuration");
    }

    public static IronSourceError buildLoadFailedError(String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            str2 = "Load failed due to an unknown error";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Load failed - ");
            sb.append(str);
            str2 = sb.toString();
        }
        return new IronSourceError(IronSourceError.ERROR_CODE_GENERIC, str2);
    }

    public static IronSourceError unsupportedBannerSize(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" unsupported banner size");
        return new IronSourceError(IronSourceError.ERROR_BN_UNSUPPORTED_SIZE, sb.toString());
    }
}
