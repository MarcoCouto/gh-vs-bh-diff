package com.ironsource.mediationsdk.utils;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Base64;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.BannerSmash;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.logger.ThreadExceptionHandler;
import com.mintegral.msdk.base.utils.CommonMD5;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceUtils {
    private static final String ADAPTER_VERSION_KEY = "providerAdapterVersion";
    private static final String DEFAULT_IS_EVENTS_FORMATTER_TYPE = "default_is_events_formatter_type";
    private static final String DEFAULT_IS_EVENTS_URL = "default_is_events_url";
    private static final String DEFAULT_IS_OPT_OUT_EVENTS = "default_is_opt_out_events";
    private static final String DEFAULT_RV_EVENTS_FORMATTER_TYPE = "default_rv_events_formatter_type";
    private static final String DEFAULT_RV_EVENTS_URL = "default_rv_events_url";
    private static final String DEFAULT_RV_OPT_OUT_EVENTS = "default_rv_opt_out_events";
    private static final String GENERAL_PROPERTIES = "general_properties";
    public static final String KEY = "C38FB23A402222A0C17D34A92F971D1F";
    private static final String LAST_RESPONSE = "last_response";
    private static final String PROVIDER_KEY = "provider";
    private static final String PROVIDER_PRIORITY = "providerPriority";
    private static final String SDK_VERSION = "6.9.1";
    private static final String SDK_VERSION_KEY = "providerSDKVersion";
    private static final String SHARED_PREFERENCES_NAME = "Mediation_Shared_Preferences";
    private static final String SUB_PROVIDER_ID_KEY = "spId";
    private static int serr = 1;

    public static String getSDKVersion() {
        return "6.9.1";
    }

    private static void setSerr(int i) {
        serr = i;
    }

    public static int getSerr() {
        return serr;
    }

    public static String getMD5(String str) {
        try {
            String bigInteger = new BigInteger(1, MessageDigest.getInstance(CommonMD5.TAG).digest(str.getBytes())).toString(16);
            while (bigInteger.length() < 32) {
                StringBuilder sb = new StringBuilder();
                sb.append("0");
                sb.append(bigInteger);
                bigInteger = sb.toString();
            }
            return bigInteger;
        } catch (Throwable th) {
            if (str == null) {
                IronSourceLoggerManager.getLogger().logException(IronSourceTag.NATIVE, "getMD5(input:null)", th);
            } else {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("getMD5(input:");
                sb2.append(str);
                sb2.append(")");
                logger.logException(ironSourceTag, sb2.toString(), th);
            }
            return "";
        }
    }

    private static String getSHA256(String str) {
        try {
            return String.format("%064x", new Object[]{new BigInteger(1, MessageDigest.getInstance(CommonUtils.SHA256_INSTANCE).digest(str.getBytes()))});
        } catch (NoSuchAlgorithmException e) {
            if (str == null) {
                IronSourceLoggerManager.getLogger().logException(IronSourceTag.NATIVE, "getSHA256(input:null)", e);
            } else {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb = new StringBuilder();
                sb.append("getSHA256(input:");
                sb.append(str);
                sb.append(")");
                logger.logException(ironSourceTag, sb.toString(), e);
            }
            return "";
        }
    }

    public static String getTransId(String str) {
        return getSHA256(str);
    }

    public static int getCurrentTimestamp() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public static String getConnectionType(Context context) {
        if (context == null) {
            return "none";
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return "none";
        }
        if (VERSION.SDK_INT >= 23) {
            NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
            if (networkCapabilities == null) {
                return "none";
            }
            if (networkCapabilities.hasTransport(1)) {
                return "wifi";
            }
            return networkCapabilities.hasTransport(0) ? "cellular" : "none";
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return "none";
        }
        if (activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) {
            return "wifi";
        }
        return activeNetworkInfo.getTypeName().equalsIgnoreCase("MOBILE") ? "cellular" : "none";
    }

    public static void createAndStartWorker(Runnable runnable, String str) {
        Thread thread = new Thread(runnable, str);
        thread.setUncaughtExceptionHandler(new ThreadExceptionHandler());
        thread.start();
    }

    public static String getBase64Auth(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Basic ");
        sb3.append(Base64.encodeToString(sb2.getBytes(), 10));
        return sb3.toString();
    }

    private static String getDefaultEventsUrlByEventType(String str) {
        if (IronSourceConstants.INTERSTITIAL_EVENT_TYPE.equals(str)) {
            return DEFAULT_IS_EVENTS_URL;
        }
        return IronSourceConstants.REWARDED_VIDEO_EVENT_TYPE.equals(str) ? DEFAULT_RV_EVENTS_URL : "";
    }

    private static String getDefaultOptOutEventsByEventType(String str) {
        if (IronSourceConstants.INTERSTITIAL_EVENT_TYPE.equals(str)) {
            return DEFAULT_IS_OPT_OUT_EVENTS;
        }
        return IronSourceConstants.REWARDED_VIDEO_EVENT_TYPE.equals(str) ? DEFAULT_RV_OPT_OUT_EVENTS : "";
    }

    private static String getDefaultFormatterTypeByEventType(String str) {
        if (IronSourceConstants.INTERSTITIAL_EVENT_TYPE.equals(str)) {
            return DEFAULT_IS_EVENTS_FORMATTER_TYPE;
        }
        return IronSourceConstants.REWARDED_VIDEO_EVENT_TYPE.equals(str) ? DEFAULT_RV_EVENTS_FORMATTER_TYPE : "";
    }

    public static synchronized void saveDefaultEventsURL(Context context, String str, String str2) {
        synchronized (IronSourceUtils.class) {
            try {
                Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
                edit.putString(getDefaultEventsUrlByEventType(str), str2);
                edit.commit();
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb = new StringBuilder();
                sb.append("IronSourceUtils:saveDefaultEventsURL(eventType: ");
                sb.append(str);
                sb.append(", eventsUrl:");
                sb.append(str2);
                sb.append(")");
                logger.logException(ironSourceTag, sb.toString(), e);
            }
        }
        return;
    }

    public static synchronized void saveDefaultOptOutEvents(Context context, String str, int[] iArr) {
        synchronized (IronSourceUtils.class) {
            try {
                Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
                String str2 = null;
                if (iArr != null) {
                    StringBuilder sb = new StringBuilder();
                    for (int append : iArr) {
                        sb.append(append);
                        sb.append(",");
                    }
                    str2 = sb.toString();
                }
                edit.putString(getDefaultOptOutEventsByEventType(str), str2);
                edit.commit();
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("IronSourceUtils:saveDefaultOptOutEvents(eventType: ");
                sb2.append(str);
                sb2.append(", optOutEvents:");
                sb2.append(iArr);
                sb2.append(")");
                logger.logException(ironSourceTag, sb2.toString(), e);
            }
        }
        return;
    }

    public static synchronized void saveDefaultEventsFormatterType(Context context, String str, String str2) {
        synchronized (IronSourceUtils.class) {
            try {
                Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
                edit.putString(getDefaultFormatterTypeByEventType(str), str2);
                edit.commit();
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb = new StringBuilder();
                sb.append("IronSourceUtils:saveDefaultEventsFormatterType(eventType: ");
                sb.append(str);
                sb.append(", formatterType:");
                sb.append(str2);
                sb.append(")");
                logger.logException(ironSourceTag, sb.toString(), e);
            }
        }
        return;
    }

    public static synchronized String getDefaultEventsFormatterType(Context context, String str, String str2) {
        String str3;
        synchronized (IronSourceUtils.class) {
            try {
                str3 = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getString(getDefaultFormatterTypeByEventType(str), str2);
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb = new StringBuilder();
                sb.append("IronSourceUtils:getDefaultEventsFormatterType(eventType: ");
                sb.append(str);
                sb.append(", defaultFormatterType:");
                sb.append(str2);
                sb.append(")");
                logger.logException(ironSourceTag, sb.toString(), e);
                str3 = str2;
            }
        }
        return str3;
    }

    public static synchronized String getDefaultEventsURL(Context context, String str, String str2) {
        String str3;
        synchronized (IronSourceUtils.class) {
            try {
                str3 = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getString(getDefaultEventsUrlByEventType(str), str2);
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb = new StringBuilder();
                sb.append("IronSourceUtils:getDefaultEventsURL(eventType: ");
                sb.append(str);
                sb.append(", defaultEventsURL:");
                sb.append(str2);
                sb.append(")");
                logger.logException(ironSourceTag, sb.toString(), e);
                str3 = str2;
            }
        }
        return str3;
    }

    public static synchronized int[] getDefaultOptOutEvents(Context context, String str) {
        int[] iArr;
        synchronized (IronSourceUtils.class) {
            iArr = null;
            int i = 0;
            try {
                String string = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getString(getDefaultOptOutEventsByEventType(str), null);
                if (!TextUtils.isEmpty(string)) {
                    StringTokenizer stringTokenizer = new StringTokenizer(string, ",");
                    ArrayList arrayList = new ArrayList();
                    while (stringTokenizer.hasMoreTokens()) {
                        arrayList.add(Integer.valueOf(Integer.parseInt(stringTokenizer.nextToken())));
                    }
                    int[] iArr2 = new int[arrayList.size()];
                    while (i < iArr2.length) {
                        try {
                            iArr2[i] = ((Integer) arrayList.get(i)).intValue();
                            i++;
                        } catch (Exception e) {
                            e = e;
                            iArr = iArr2;
                            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                            StringBuilder sb = new StringBuilder();
                            sb.append("IronSourceUtils:getDefaultOptOutEvents(eventType: ");
                            sb.append(str);
                            sb.append(")");
                            logger.logException(ironSourceTag, sb.toString(), e);
                            return iArr;
                        }
                    }
                    iArr = iArr2;
                }
            } catch (Exception e2) {
                e = e2;
                IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag2 = IronSourceTag.NATIVE;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("IronSourceUtils:getDefaultOptOutEvents(eventType: ");
                sb2.append(str);
                sb2.append(")");
                logger2.logException(ironSourceTag2, sb2.toString(), e);
                return iArr;
            }
        }
        return iArr;
    }

    public static synchronized void saveLastResponse(Context context, String str) {
        synchronized (IronSourceUtils.class) {
            Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
            edit.putString(LAST_RESPONSE, str);
            edit.apply();
        }
    }

    public static String getLastResponse(Context context) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getString(LAST_RESPONSE, "");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0025, code lost:
        return;
     */
    static synchronized void saveGeneralProperties(Context context, JSONObject jSONObject) {
        synchronized (IronSourceUtils.class) {
            if (context != null && jSONObject != null) {
                Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
                edit.putString(GENERAL_PROPERTIES, jSONObject.toString());
                edit.apply();
            }
        }
    }

    public static synchronized JSONObject getGeneralProperties(Context context) {
        JSONObject jSONObject;
        synchronized (IronSourceUtils.class) {
            jSONObject = new JSONObject();
            if (context == null) {
                return jSONObject;
            }
            try {
                jSONObject = new JSONObject(context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getString(GENERAL_PROPERTIES, jSONObject.toString()));
            } catch (JSONException unused) {
            }
        }
        return jSONObject;
    }

    public static boolean isNetworkConnected(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return activeNetworkInfo.isConnected();
    }

    public static long getTimeStamp() {
        return System.currentTimeMillis();
    }

    public static JSONObject getProviderAdditionalData(AbstractSmash abstractSmash) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(PROVIDER_PRIORITY, abstractSmash.getProviderPriority());
            jSONObject.put(SUB_PROVIDER_ID_KEY, abstractSmash.getSubProviderId());
            jSONObject.put("provider", abstractSmash.getAdSourceNameForEvents());
            jSONObject.put(SDK_VERSION_KEY, abstractSmash.getAdapter().getCoreSDKVersion());
            jSONObject.put(ADAPTER_VERSION_KEY, abstractSmash.getAdapter().getVersion());
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
            StringBuilder sb = new StringBuilder();
            sb.append("IronSourceUtils:getProviderAdditionalData(adapter: ");
            sb.append(abstractSmash.getName());
            sb.append(")");
            logger.logException(ironSourceTag, sb.toString(), e);
        }
        return jSONObject;
    }

    public static JSONObject getProviderAdditionalData(BannerSmash bannerSmash) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(SUB_PROVIDER_ID_KEY, bannerSmash.getSubProviderId());
            jSONObject.put("provider", bannerSmash.getAdSourceNameForEvents());
            jSONObject.put(SDK_VERSION_KEY, bannerSmash.getAdapter().getCoreSDKVersion());
            jSONObject.put(ADAPTER_VERSION_KEY, bannerSmash.getAdapter().getVersion());
            jSONObject.put(PROVIDER_PRIORITY, bannerSmash.getProviderPriority());
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
            StringBuilder sb = new StringBuilder();
            sb.append("IronSourceUtils:getProviderAdditionalData(adapter: ");
            sb.append(bannerSmash.getName());
            sb.append(")");
            logger.logException(ironSourceTag, sb.toString(), e);
        }
        return jSONObject;
    }

    public static JSONObject getMediationAdditionalData(boolean z) {
        return getMediationAdditionalData(z, false);
    }

    public static JSONObject getMediationAdditionalData(boolean z, boolean z2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("provider", "Mediation");
            if (z) {
                jSONObject.put(IronSourceConstants.EVENTS_DEMAND_ONLY, 1);
            }
            if (z2) {
                jSONObject.put(IronSourceConstants.EVENTS_PROGRAMMATIC, 1);
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    static void saveStringToSharedPrefs(Context context, String str, String str2) {
        Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
        edit.putString(str, str2);
        edit.apply();
    }

    static String getStringFromSharedPrefs(Context context, String str, String str2) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getString(str, str2);
    }

    static void saveBooleanToSharedPrefs(Context context, String str, boolean z) {
        Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
        edit.putBoolean(str, z);
        edit.apply();
    }

    static boolean getBooleanFromSharedPrefs(Context context, String str, boolean z) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getBoolean(str, z);
    }

    static void saveIntToSharedPrefs(Context context, String str, int i) {
        Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
        edit.putInt(str, i);
        edit.apply();
    }

    static int getIntFromSharedPrefs(Context context, String str, int i) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getInt(str, i);
    }

    static void saveLongToSharedPrefs(Context context, String str, long j) {
        Editor edit = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
        edit.putLong(str, j);
        edit.apply();
    }

    static long getLongFromSharedPrefs(Context context, String str, long j) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getLong(str, j);
    }

    public static JSONObject mergeJsons(JSONObject jSONObject, JSONObject jSONObject2) {
        if (jSONObject == null && jSONObject2 == null) {
            try {
                return new JSONObject();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (jSONObject == null) {
            return jSONObject2;
        } else {
            if (jSONObject2 == null) {
                return jSONObject;
            }
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                if (!jSONObject.has(str)) {
                    jSONObject.put(str, jSONObject2.get(str));
                }
            }
        }
        return jSONObject;
    }

    public static synchronized void sendAutomationLog(String str) {
        synchronized (IronSourceUtils.class) {
            StringBuilder sb = new StringBuilder();
            sb.append("automation_log:");
            sb.append(Long.toString(System.currentTimeMillis()));
            sb.append("text:");
            sb.append(str);
            IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 1);
        }
    }

    static Map<String, String> parseJsonToStringMap(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        try {
            if (jSONObject != JSONObject.NULL) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if (!jSONObject.get(str).toString().isEmpty()) {
                        hashMap.put(str, jSONObject.get(str).toString());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }
}
