package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.environment.NetworkStateReceiver;
import com.ironsource.environment.NetworkStateReceiver.NetworkStateReceiverListener;
import com.ironsource.mediationsdk.IronSource.AD_UNIT;
import com.ironsource.mediationsdk.IronSourceObject.IResponseListener;
import com.ironsource.mediationsdk.config.ConfigValidationResult;
import com.ironsource.mediationsdk.integration.IntegrationHelper;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ServerSegmetData;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.mediationsdk.sdk.SegmentListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

class MediationInitializer implements NetworkStateReceiverListener {
    private static MediationInitializer sInstance;
    private final String GENERAL_PROPERTIES_APP_KEY;
    private final String GENERAL_PROPERTIES_USER_ID;
    private final String TAG;
    private InitRunnable initRunnable;
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public String mAppKey;
    private AtomicBoolean mAtomicShouldPerformInit;
    /* access modifiers changed from: private */
    public CountDownTimer mCountDownTimer;
    /* access modifiers changed from: private */
    public boolean mDidReportInitialAvailability;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private HandlerThread mHandlerThread;
    /* access modifiers changed from: private */
    public long mInitStartTime;
    private EInitStatus mInitStatus;
    /* access modifiers changed from: private */
    public boolean mIsInProgressMoreThan15Secs;
    /* access modifiers changed from: private */
    public boolean mIsRevived;
    private boolean mListenForInit;
    private NetworkStateReceiver mNetworkStateReceiver;
    /* access modifiers changed from: private */
    public List<OnMediationInitializationListener> mOnMediationInitializationListeners;
    /* access modifiers changed from: private */
    public int mRetryAvailabilityLimit;
    /* access modifiers changed from: private */
    public int mRetryCounter;
    /* access modifiers changed from: private */
    public int mRetryDelay;
    /* access modifiers changed from: private */
    public int mRetryGrowLimit;
    /* access modifiers changed from: private */
    public int mRetryLimit;
    /* access modifiers changed from: private */
    public SegmentListener mSegmentListener;
    /* access modifiers changed from: private */
    public ServerResponseWrapper mServerResponseWrapper;
    /* access modifiers changed from: private */
    public String mUserId;
    /* access modifiers changed from: private */
    public String mUserIdType;

    enum EInitStatus {
        NOT_INIT,
        INIT_IN_PROGRESS,
        INIT_FAILED,
        INITIATED
    }

    abstract class InitRunnable implements Runnable {
        boolean isRecoverable = true;
        protected IResponseListener listener = new IResponseListener() {
            public void onUnrecoverableError(String str) {
                InitRunnable.this.isRecoverable = false;
                InitRunnable.this.reason = str;
            }
        };
        String reason;

        InitRunnable() {
        }
    }

    interface OnMediationInitializationListener {
        void onInitFailed(String str);

        void onInitSuccess(List<AD_UNIT> list, boolean z);

        void onStillInProgressAfter15Secs();
    }

    public static synchronized MediationInitializer getInstance() {
        MediationInitializer mediationInitializer;
        synchronized (MediationInitializer.class) {
            if (sInstance == null) {
                sInstance = new MediationInitializer();
            }
            mediationInitializer = sInstance;
        }
        return mediationInitializer;
    }

    private MediationInitializer() {
        this.GENERAL_PROPERTIES_USER_ID = "userId";
        this.GENERAL_PROPERTIES_APP_KEY = ServerResponseWrapper.APP_KEY_FIELD;
        this.TAG = getClass().getSimpleName();
        this.mDidReportInitialAvailability = false;
        this.mHandlerThread = null;
        this.mListenForInit = false;
        this.mOnMediationInitializationListeners = new ArrayList();
        this.initRunnable = new InitRunnable() {
            public void run() {
                try {
                    IronSourceObject instance = IronSourceObject.getInstance();
                    if (MediationInitializer.this.validateUserId(MediationInitializer.this.mUserId).isValid()) {
                        MediationInitializer.this.mUserIdType = IronSourceConstants.TYPE_USER_GENERATED;
                    } else {
                        MediationInitializer.this.mUserId = instance.getAdvertiserId(MediationInitializer.this.mActivity);
                        if (!TextUtils.isEmpty(MediationInitializer.this.mUserId)) {
                            MediationInitializer.this.mUserIdType = IronSourceConstants.TYPE_GAID;
                        } else {
                            MediationInitializer.this.mUserId = DeviceStatus.getOrGenerateOnceUniqueIdentifier(MediationInitializer.this.mActivity);
                            if (!TextUtils.isEmpty(MediationInitializer.this.mUserId)) {
                                MediationInitializer.this.mUserIdType = IronSourceConstants.TYPE_UUID;
                            } else {
                                MediationInitializer.this.mUserId = "";
                            }
                        }
                        instance.setIronSourceUserId(MediationInitializer.this.mUserId);
                    }
                    GeneralProperties.getProperties().putKey(GeneralProperties.USER_ID_TYPE, MediationInitializer.this.mUserIdType);
                    if (!TextUtils.isEmpty(MediationInitializer.this.mUserId)) {
                        GeneralProperties.getProperties().putKey("userId", MediationInitializer.this.mUserId);
                    }
                    if (!TextUtils.isEmpty(MediationInitializer.this.mAppKey)) {
                        GeneralProperties.getProperties().putKey(ServerResponseWrapper.APP_KEY_FIELD, MediationInitializer.this.mAppKey);
                    }
                    MediationInitializer.this.mInitStartTime = new Date().getTime();
                    MediationInitializer.this.mServerResponseWrapper = instance.getServerResponse(MediationInitializer.this.mActivity, MediationInitializer.this.mUserId, this.listener);
                    if (MediationInitializer.this.mServerResponseWrapper != null) {
                        MediationInitializer.this.mHandler.removeCallbacks(this);
                        if (MediationInitializer.this.mServerResponseWrapper.isValidResponse()) {
                            MediationInitializer.this.setInitStatus(EInitStatus.INITIATED);
                            instance.sendInitCompletedEvent(new Date().getTime() - MediationInitializer.this.mInitStartTime);
                            if (MediationInitializer.this.mServerResponseWrapper.getConfigurations().getApplicationConfigurations().getIntegration()) {
                                IntegrationHelper.validateIntegration(MediationInitializer.this.mActivity);
                            }
                            List initiatedAdUnits = MediationInitializer.this.mServerResponseWrapper.getInitiatedAdUnits();
                            for (OnMediationInitializationListener onInitSuccess : MediationInitializer.this.mOnMediationInitializationListeners) {
                                onInitSuccess.onInitSuccess(initiatedAdUnits, MediationInitializer.this.wasInitRevived());
                            }
                            if (MediationInitializer.this.mSegmentListener != null) {
                                ServerSegmetData segmetData = MediationInitializer.this.mServerResponseWrapper.getConfigurations().getApplicationConfigurations().getSegmetData();
                                if (segmetData != null && !TextUtils.isEmpty(segmetData.getSegmentName())) {
                                    MediationInitializer.this.mSegmentListener.onSegmentReceived(segmetData.getSegmentName());
                                }
                            }
                        } else if (!MediationInitializer.this.mDidReportInitialAvailability) {
                            MediationInitializer.this.setInitStatus(EInitStatus.INIT_FAILED);
                            MediationInitializer.this.mDidReportInitialAvailability = true;
                            for (OnMediationInitializationListener onInitFailed : MediationInitializer.this.mOnMediationInitializationListeners) {
                                onInitFailed.onInitFailed(IronSourceConstants.FALSE_AVAILABILITY_REASON_SERVER_RESPONSE_IS_NOT_VALID);
                            }
                        }
                    } else {
                        if (MediationInitializer.this.mRetryCounter == 3) {
                            MediationInitializer.this.mIsInProgressMoreThan15Secs = true;
                            for (OnMediationInitializationListener onStillInProgressAfter15Secs : MediationInitializer.this.mOnMediationInitializationListeners) {
                                onStillInProgressAfter15Secs.onStillInProgressAfter15Secs();
                            }
                        }
                        if (this.isRecoverable && MediationInitializer.this.mRetryCounter < MediationInitializer.this.mRetryLimit) {
                            MediationInitializer.this.mIsRevived = true;
                            MediationInitializer.this.mHandler.postDelayed(this, (long) (MediationInitializer.this.mRetryDelay * 1000));
                            if (MediationInitializer.this.mRetryCounter < MediationInitializer.this.mRetryGrowLimit) {
                                MediationInitializer.this.mRetryDelay = MediationInitializer.this.mRetryDelay * 2;
                            }
                        }
                        if ((!this.isRecoverable || MediationInitializer.this.mRetryCounter == MediationInitializer.this.mRetryAvailabilityLimit) && !MediationInitializer.this.mDidReportInitialAvailability) {
                            MediationInitializer.this.mDidReportInitialAvailability = true;
                            if (TextUtils.isEmpty(this.reason)) {
                                this.reason = IronSourceConstants.FALSE_AVAILABILITY_REASON_NO_SERVER_RESPONSE;
                            }
                            for (OnMediationInitializationListener onInitFailed2 : MediationInitializer.this.mOnMediationInitializationListeners) {
                                onInitFailed2.onInitFailed(this.reason);
                            }
                            MediationInitializer.this.setInitStatus(EInitStatus.INIT_FAILED);
                            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, "Mediation availability false reason: No server response", 1);
                        }
                        MediationInitializer.this.mRetryCounter = MediationInitializer.this.mRetryCounter + 1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        this.mInitStatus = EInitStatus.NOT_INIT;
        this.mHandlerThread = new HandlerThread("IronSourceInitiatorHandler");
        this.mHandlerThread.start();
        this.mHandler = new Handler(this.mHandlerThread.getLooper());
        this.mRetryDelay = 1;
        this.mRetryCounter = 0;
        this.mRetryLimit = 62;
        this.mRetryGrowLimit = 12;
        this.mRetryAvailabilityLimit = 5;
        this.mAtomicShouldPerformInit = new AtomicBoolean(true);
        this.mIsRevived = false;
        this.mIsInProgressMoreThan15Secs = false;
    }

    /* access modifiers changed from: private */
    public synchronized void setInitStatus(EInitStatus eInitStatus) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("setInitStatus(old status: ");
        sb.append(this.mInitStatus);
        sb.append(", new status: ");
        sb.append(eInitStatus);
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 0);
        this.mInitStatus = eInitStatus;
    }

    public synchronized void init(Activity activity, String str, String str2, AD_UNIT... ad_unitArr) {
        try {
            if (this.mAtomicShouldPerformInit == null || !this.mAtomicShouldPerformInit.compareAndSet(true, false)) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.API;
                StringBuilder sb = new StringBuilder();
                sb.append(this.TAG);
                sb.append(": Multiple calls to init are not allowed");
                logger.log(ironSourceTag, sb.toString(), 2);
            } else {
                setInitStatus(EInitStatus.INIT_IN_PROGRESS);
                this.mActivity = activity;
                this.mUserId = str2;
                this.mAppKey = str;
                if (IronSourceUtils.isNetworkConnected(activity)) {
                    this.mHandler.post(this.initRunnable);
                } else {
                    this.mListenForInit = true;
                    if (this.mNetworkStateReceiver == null) {
                        this.mNetworkStateReceiver = new NetworkStateReceiver(activity, this);
                    }
                    activity.getApplicationContext().registerReceiver(this.mNetworkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            MediationInitializer mediationInitializer = MediationInitializer.this;
                            AnonymousClass1 r1 = new CountDownTimer(ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS, MTGInterstitialActivity.WEB_LOAD_TIME) {
                                public void onTick(long j) {
                                    if (j <= 45000) {
                                        MediationInitializer.this.mIsInProgressMoreThan15Secs = true;
                                        for (OnMediationInitializationListener onStillInProgressAfter15Secs : MediationInitializer.this.mOnMediationInitializationListeners) {
                                            onStillInProgressAfter15Secs.onStillInProgressAfter15Secs();
                                        }
                                    }
                                }

                                public void onFinish() {
                                    if (!MediationInitializer.this.mDidReportInitialAvailability) {
                                        MediationInitializer.this.mDidReportInitialAvailability = true;
                                        for (OnMediationInitializationListener onInitFailed : MediationInitializer.this.mOnMediationInitializationListeners) {
                                            onInitFailed.onInitFailed(IronSourceConstants.FALSE_AVAILABILITY_REASON_NO_INTERNET);
                                        }
                                        IronSourceLoggerManager.getLogger().log(IronSourceTag.API, "Mediation availability false reason: No internet connection", 1);
                                    }
                                }
                            };
                            mediationInitializer.mCountDownTimer = r1.start();
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public void onNetworkAvailabilityChanged(boolean z) {
        if (this.mListenForInit && z) {
            if (this.mCountDownTimer != null) {
                this.mCountDownTimer.cancel();
            }
            this.mListenForInit = false;
            this.mIsRevived = true;
            this.mHandler.post(this.initRunnable);
        }
    }

    /* access modifiers changed from: private */
    public boolean wasInitRevived() {
        return this.mIsRevived;
    }

    public synchronized EInitStatus getCurrentInitStatus() {
        return this.mInitStatus;
    }

    /* access modifiers changed from: 0000 */
    public void setInitStatusFailed() {
        setInitStatus(EInitStatus.INIT_FAILED);
    }

    public synchronized boolean isInProgressMoreThan15Secs() {
        return this.mIsInProgressMoreThan15Secs;
    }

    public void addMediationInitializationListener(OnMediationInitializationListener onMediationInitializationListener) {
        if (onMediationInitializationListener != null) {
            this.mOnMediationInitializationListeners.add(onMediationInitializationListener);
        }
    }

    public void removeMediationInitializationListener(OnMediationInitializationListener onMediationInitializationListener) {
        if (onMediationInitializationListener != null && this.mOnMediationInitializationListeners.size() != 0) {
            this.mOnMediationInitializationListeners.remove(onMediationInitializationListener);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setSegmentListener(SegmentListener segmentListener) {
        this.mSegmentListener = segmentListener;
    }

    /* access modifiers changed from: private */
    public ConfigValidationResult validateUserId(String str) {
        ConfigValidationResult configValidationResult = new ConfigValidationResult();
        if (str == null) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidCredentialsError("userId", str, "it's missing"));
        } else if (!validateLength(str, 1, 64)) {
            configValidationResult.setInvalid(ErrorBuilder.buildInvalidCredentialsError("userId", str, null));
        }
        return configValidationResult;
    }

    private boolean validateLength(String str, int i, int i2) {
        boolean z = false;
        if (str == null) {
            return false;
        }
        if (str.length() >= i && str.length() <= i2) {
            z = true;
        }
        return z;
    }
}
