package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;

public class ISDemandOnlyListenerWrapper {
    private static final ISDemandOnlyListenerWrapper sInstance = new ISDemandOnlyListenerWrapper();
    /* access modifiers changed from: private */
    public ISDemandOnlyInterstitialListener mListener = null;

    public static synchronized ISDemandOnlyListenerWrapper getInstance() {
        ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper;
        synchronized (ISDemandOnlyListenerWrapper.class) {
            iSDemandOnlyListenerWrapper = sInstance;
        }
        return iSDemandOnlyListenerWrapper;
    }

    private ISDemandOnlyListenerWrapper() {
    }

    public synchronized void setListener(ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener) {
        this.mListener = iSDemandOnlyInterstitialListener;
    }

    public synchronized ISDemandOnlyInterstitialListener getListener() {
        return this.mListener;
    }

    public synchronized void onInterstitialAdReady(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdReady(str);
                        ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdReady() instanceId=");
                        sb.append(str);
                        iSDemandOnlyListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdLoadFailed(final String str, final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdLoadFailed(str, ironSourceError);
                        ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdLoadFailed() instanceId=");
                        sb.append(str);
                        sb.append(" error=");
                        sb.append(ironSourceError.getErrorMessage());
                        iSDemandOnlyListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdOpened(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdOpened(str);
                        ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdOpened() instanceId=");
                        sb.append(str);
                        iSDemandOnlyListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdClosed(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdClosed(str);
                        ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdClosed() instanceId=");
                        sb.append(str);
                        iSDemandOnlyListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdShowFailed(final String str, final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdShowFailed(str, ironSourceError);
                        ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdShowFailed() instanceId=");
                        sb.append(str);
                        sb.append(" error=");
                        sb.append(ironSourceError.getErrorMessage());
                        iSDemandOnlyListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdClicked(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdClicked(str);
                        ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdClicked() instanceId=");
                        sb.append(str);
                        iSDemandOnlyListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, str, 1);
    }
}
