package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class ProgRvSmash extends ProgSmash implements RewardedVideoSmashListener {
    private Activity mActivity;
    private String mAppKey;
    private String mAuctionIdToLoad;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId;
    private Placement mCurrentPlacement;
    private boolean mIsShowCandidate;
    /* access modifiers changed from: private */
    public ProgRvManagerListener mListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;
    private int mLoadTimeoutSecs;
    private String mServerDataToLoad;
    private boolean mShouldLoadAfterClose;
    private boolean mShouldLoadAfterLoad;
    /* access modifiers changed from: private */
    public SMASH_STATE mState = SMASH_STATE.NO_INIT;
    private Timer mTimer;
    private String mUserId;

    protected enum SMASH_STATE {
        NO_INIT,
        INIT_IN_PROGRESS,
        NOT_LOADED,
        LOAD_IN_PROGRESS,
        LOADED,
        SHOW_IN_PROGRESS
    }

    public void onRewardedVideoLoadFailed(IronSourceError ironSourceError) {
    }

    public void onRewardedVideoLoadSuccess() {
    }

    public ProgRvSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, ProgRvManagerListener progRvManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getRewardedVideoSettings()), abstractAdapter);
        this.mActivity = activity;
        this.mAppKey = str;
        this.mUserId = str2;
        this.mListener = progRvManagerListener;
        this.mTimer = null;
        this.mLoadTimeoutSecs = i;
        this.mAdapter.addRewardedVideoListener(this);
        this.mShouldLoadAfterClose = false;
        this.mShouldLoadAfterLoad = false;
        this.mIsShowCandidate = false;
        this.mCurrentPlacement = null;
        this.mServerDataToLoad = "";
        this.mCurrentAuctionId = "";
        this.mAuctionIdToLoad = "";
    }

    public synchronized boolean isReadyToBid() {
        return (this.mState == SMASH_STATE.NO_INIT || this.mState == SMASH_STATE.INIT_IN_PROGRESS) ? false : true;
    }

    public synchronized boolean isLoadingInProgress() {
        return this.mState == SMASH_STATE.INIT_IN_PROGRESS || this.mState == SMASH_STATE.LOAD_IN_PROGRESS;
    }

    public synchronized Map<String, Object> getBiddingData() {
        return isBidder() ? this.mAdapter.getRvBiddingData(this.mAdUnitSettings) : null;
    }

    public synchronized void initForBidding() {
        logInternal("initForBidding()");
        setState(SMASH_STATE.INIT_IN_PROGRESS);
        setCustomParams();
        this.mAdapter.initRvForBidding(this.mActivity, this.mAppKey, this.mUserId, this.mAdUnitSettings, this);
    }

    public synchronized void unloadVideo() {
        if (isBidder()) {
            this.mIsShowCandidate = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        return r2.mIsShowCandidate && r2.mState == com.ironsource.mediationsdk.ProgRvSmash.SMASH_STATE.LOADED && r2.mAdapter.isRewardedVideoAvailable(r2.mAdUnitSettings);
     */
    public synchronized boolean isReadyToShow() {
        if (!isBidder()) {
            return this.mAdapter.isRewardedVideoAvailable(this.mAdUnitSettings);
        }
    }

    public synchronized void loadVideo(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("loadVideo() auctionId: ");
        sb.append(str2);
        sb.append(" state: ");
        sb.append(this.mState);
        logInternal(sb.toString());
        setIsLoadCandidate(false);
        this.mIsShowCandidate = true;
        if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mShouldLoadAfterLoad = true;
            this.mAuctionIdToLoad = str2;
            this.mServerDataToLoad = str;
            this.mListener.onLoadError(this, str2);
        } else if (this.mState == SMASH_STATE.SHOW_IN_PROGRESS) {
            this.mShouldLoadAfterClose = true;
            this.mAuctionIdToLoad = str2;
            this.mServerDataToLoad = str;
        } else {
            this.mCurrentAuctionId = str2;
            startLoadTimer();
            this.mLoadStartTime = new Date().getTime();
            sendProviderEvent(1001);
            if (isBidder()) {
                setState(SMASH_STATE.LOAD_IN_PROGRESS);
                this.mAdapter.loadVideo(this.mAdUnitSettings, this, str);
            } else if (this.mState == SMASH_STATE.NO_INIT) {
                setCustomParams();
                setState(SMASH_STATE.LOAD_IN_PROGRESS);
                this.mAdapter.initRewardedVideo(this.mActivity, this.mAppKey, this.mUserId, this.mAdUnitSettings, this);
            } else {
                setState(SMASH_STATE.LOAD_IN_PROGRESS);
                this.mAdapter.fetchRewardedVideo(this.mAdUnitSettings);
            }
        }
    }

    public synchronized void reportShowChance(boolean z) {
        Object[][] objArr = new Object[1][];
        Object[] objArr2 = new Object[2];
        objArr2[0] = "status";
        objArr2[1] = z ? "true" : "false";
        objArr[0] = objArr2;
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW_CHANCE, objArr);
    }

    public synchronized void showVideo(Placement placement) {
        stopLoadTimer();
        logInternal("showVideo()");
        this.mCurrentPlacement = placement;
        setState(SMASH_STATE.SHOW_IN_PROGRESS);
        this.mAdapter.showRewardedVideo(this.mAdUnitSettings, this);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW);
    }

    public synchronized void setCappedPerSession() {
        this.mAdapter.setMediationState(MEDIATION_STATE.CAPPED_PER_SESSION, "rewardedvideo");
        sendProviderEvent(IronSourceConstants.RV_CAP_SESSION);
    }

    /* access modifiers changed from: private */
    public void setState(SMASH_STATE smash_state) {
        StringBuilder sb = new StringBuilder();
        sb.append("current state=");
        sb.append(this.mState);
        sb.append(", new state=");
        sb.append(smash_state);
        logInternal(sb.toString());
        this.mState = smash_state;
    }

    private void setCustomParams() {
        try {
            Integer age = IronSourceObject.getInstance().getAge();
            if (age != null) {
                this.mAdapter.setAge(age.intValue());
            }
            String gender = IronSourceObject.getInstance().getGender();
            if (!TextUtils.isEmpty(gender)) {
                this.mAdapter.setGender(gender);
            }
            String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
            if (!TextUtils.isEmpty(mediationSegment)) {
                this.mAdapter.setMediationSegment(mediationSegment);
            }
            String pluginType = ConfigFile.getConfigFile().getPluginType();
            if (!TextUtils.isEmpty(pluginType)) {
                this.mAdapter.setPluginData(pluginType, ConfigFile.getConfigFile().getPluginFrameworkVersion());
            }
            Boolean consent = IronSourceObject.getInstance().getConsent();
            if (consent != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("setConsent(");
                sb.append(consent);
                sb.append(")");
                logInternal(sb.toString());
                this.mAdapter.setConsent(consent.booleanValue());
            }
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("setCustomParams() ");
            sb2.append(e.getMessage());
            logInternal(sb2.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a6, code lost:
        return;
     */
    public synchronized void onRewardedVideoAvailabilityChanged(boolean z) {
        stopLoadTimer();
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAvailabilityChanged available=");
        sb.append(z);
        sb.append(" state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        if (this.mState != SMASH_STATE.LOAD_IN_PROGRESS) {
            sendProviderEvent(z ? IronSourceConstants.RV_INSTANCE_AVAILABILITY_TRUE : IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE);
            return;
        }
        setState(z ? SMASH_STATE.LOADED : SMASH_STATE.NOT_LOADED);
        sendProviderEvent(z ? 1002 : IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(new Date().getTime() - this.mLoadStartTime)}});
        if (this.mShouldLoadAfterLoad) {
            this.mShouldLoadAfterLoad = false;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("onRewardedVideoAvailabilityChanged to ");
            sb2.append(z);
            sb2.append("and mShouldLoadAfterLoad is true - calling loadVideo");
            logInternal(sb2.toString());
            loadVideo(this.mServerDataToLoad, this.mAuctionIdToLoad);
        } else if (z) {
            this.mListener.onLoadSuccess(this, this.mCurrentAuctionId);
        } else {
            this.mListener.onLoadError(this, this.mCurrentAuctionId);
        }
    }

    public synchronized void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdShowFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        logAdapterCallback(sb.toString());
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}});
        if (this.mState == SMASH_STATE.SHOW_IN_PROGRESS) {
            setState(SMASH_STATE.NOT_LOADED);
            this.mListener.onRewardedVideoAdShowFailed(ironSourceError, this);
        }
    }

    public synchronized void onRewardedVideoInitSuccess() {
        logAdapterCallback("onRewardedVideoInitSuccess");
        if (this.mState == SMASH_STATE.INIT_IN_PROGRESS) {
            setState(SMASH_STATE.NOT_LOADED);
        }
    }

    public synchronized void onRewardedVideoInitFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoInitFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        logAdapterCallback(sb.toString());
        stopLoadTimer();
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_RV_LOAD_FAIL_DUE_TO_INIT)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(new Date().getTime() - this.mLoadStartTime)}});
        if (this.mState == SMASH_STATE.INIT_IN_PROGRESS) {
            setState(SMASH_STATE.NO_INIT);
            this.mListener.onLoadError(this, this.mCurrentAuctionId);
        }
    }

    public synchronized void onRewardedVideoAdOpened() {
        logAdapterCallback("onRewardedVideoAdOpened");
        this.mListener.onRewardedVideoAdOpened(this);
        sendProviderEventWithPlacement(1005);
    }

    public synchronized void onRewardedVideoAdStarted() {
        logAdapterCallback("onRewardedVideoAdStarted");
        this.mListener.onRewardedVideoAdStarted(this);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_STARTED);
    }

    public synchronized void onRewardedVideoAdVisible() {
        logAdapterCallback("onRewardedVideoAdVisible");
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_VISIBLE);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0039, code lost:
        return;
     */
    public synchronized void onRewardedVideoAdClosed() {
        logAdapterCallback("onRewardedVideoAdClosed");
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_CLOSED);
        if (this.mState == SMASH_STATE.SHOW_IN_PROGRESS) {
            setState(SMASH_STATE.NOT_LOADED);
            this.mListener.onRewardedVideoAdClosed(this);
            if (this.mShouldLoadAfterClose) {
                logInternal("onRewardedVideoAdClosed and mShouldLoadAfterClose is true - calling loadVideo");
                this.mShouldLoadAfterClose = false;
                loadVideo(this.mServerDataToLoad, this.mAuctionIdToLoad);
                this.mServerDataToLoad = "";
                this.mAuctionIdToLoad = "";
            }
        }
    }

    public synchronized void onRewardedVideoAdEnded() {
        logAdapterCallback("onRewardedVideoAdEnded");
        this.mListener.onRewardedVideoAdEnded(this);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_ENDED);
    }

    public synchronized void onRewardedVideoAdRewarded() {
        logAdapterCallback("onRewardedVideoAdRewarded");
        this.mListener.onRewardedVideoAdRewarded(this, this.mCurrentPlacement);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Object[]{"placement", this.mCurrentPlacement.getPlacementName()});
        arrayList.add(new Object[]{IronSourceConstants.EVENTS_REWARD_NAME, this.mCurrentPlacement.getRewardName()});
        arrayList.add(new Object[]{IronSourceConstants.EVENTS_REWARD_AMOUNT, Integer.valueOf(this.mCurrentPlacement.getRewardAmount())});
        StringBuilder sb = new StringBuilder();
        sb.append(Long.toString(new Date().getTime()));
        sb.append(this.mAppKey);
        sb.append(getInstanceName());
        arrayList.add(new Object[]{IronSourceConstants.EVENTS_TRANS_ID, IronSourceUtils.getTransId(sb.toString())});
        if (!TextUtils.isEmpty(IronSourceObject.getInstance().getDynamicUserId())) {
            arrayList.add(new Object[]{IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceObject.getInstance().getDynamicUserId()});
        }
        if (IronSourceObject.getInstance().getRvServerParams() != null) {
            for (String str : IronSourceObject.getInstance().getRvServerParams().keySet()) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("custom_");
                sb2.append(str);
                arrayList.add(new Object[]{sb2.toString(), IronSourceObject.getInstance().getRvServerParams().get(str)});
            }
        }
        sendProviderEvent(1010, (Object[][]) arrayList.toArray((Object[][]) Array.newInstance(Object.class, new int[]{2, arrayList.size()})));
    }

    public synchronized void onRewardedVideoAdClicked() {
        logAdapterCallback("onRewardedVideoAdClicked");
        this.mListener.onRewardedVideoAdClicked(this, this.mCurrentPlacement);
        sendProviderEventWithPlacement(1006);
    }

    private void stopLoadTimer() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }

    private void startLoadTimer() {
        stopLoadTimer();
        this.mTimer = new Timer();
        this.mTimer.schedule(new TimerTask() {
            public void run() {
                ProgRvSmash.this.logInternal("Rewarded Video - load instance time out");
                long time = new Date().getTime() - ProgRvSmash.this.mLoadStartTime;
                if (ProgRvSmash.this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
                    ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1025)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
                    ProgRvSmash.this.setState(SMASH_STATE.NOT_LOADED);
                    ProgRvSmash.this.mListener.onLoadError(ProgRvSmash.this, ProgRvSmash.this.mCurrentAuctionId);
                } else if (ProgRvSmash.this.mState == SMASH_STATE.INIT_IN_PROGRESS) {
                    ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_RV_INIT_FAILED_TIMEOUT)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
                    ProgRvSmash.this.setState(SMASH_STATE.NOT_LOADED);
                    ProgRvSmash.this.mListener.onLoadError(ProgRvSmash.this, ProgRvSmash.this.mCurrentAuctionId);
                } else {
                    ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1025)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
                }
            }
        }, (long) (this.mLoadTimeoutSecs * 1000));
    }

    private void logAdapterCallback(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(getInstanceName());
        sb.append(" smash: ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.ADAPTER_CALLBACK, sb.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(getInstanceName());
        sb.append(" smash: ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }

    private void sendProviderEventWithPlacement(int i) {
        sendProviderEventWithPlacement(i, null);
    }

    private void sendProviderEventWithPlacement(int i, Object[][] objArr) {
        sendProviderEvent(i, objArr, true);
    }

    private void sendProviderEvent(int i) {
        sendProviderEvent(i, null, false);
    }

    /* access modifiers changed from: private */
    public void sendProviderEvent(int i, Object[][] objArr) {
        sendProviderEvent(i, objArr, false);
    }

    private void sendProviderEvent(int i, Object[][] objArr, boolean z) {
        Map providerEventData = getProviderEventData();
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            providerEventData.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && this.mCurrentPlacement != null && !TextUtils.isEmpty(this.mCurrentPlacement.getPlacementName())) {
            providerEventData.put("placement", this.mCurrentPlacement.getPlacementName());
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append(getInstanceName());
                sb.append(" smash: RV sendMediationEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }
}
