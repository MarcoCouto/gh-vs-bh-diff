package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.sdk.DemandOnlyRvManagerListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class DemandOnlyRvManager implements DemandOnlyRvManagerListener {
    private String mAppKey;
    private ConcurrentHashMap<String, DemandOnlyRvSmash> mSmashes = new ConcurrentHashMap<>();

    DemandOnlyRvManager(Activity activity, List<ProviderSettings> list, RewardedVideoConfigurations rewardedVideoConfigurations, String str, String str2) {
        this.mAppKey = str;
        for (ProviderSettings providerSettings : list) {
            if (providerSettings.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME) || providerSettings.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.IRONSOURCE_CONFIG_NAME)) {
                AbstractAdapter loadAdapter = loadAdapter(providerSettings.getProviderInstanceName());
                if (loadAdapter != null) {
                    DemandOnlyRvSmash demandOnlyRvSmash = new DemandOnlyRvSmash(activity, str, str2, providerSettings, this, rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadTimeout(), loadAdapter);
                    this.mSmashes.put(providerSettings.getSubProviderId(), demandOnlyRvSmash);
                }
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("cannot load ");
                sb.append(providerSettings.getProviderTypeForReflection());
                logInternal(sb.toString());
            }
        }
    }

    public synchronized void onResume(Activity activity) {
        if (activity != null) {
            for (DemandOnlyRvSmash onResume : this.mSmashes.values()) {
                onResume.onResume(activity);
            }
        }
    }

    public synchronized void onPause(Activity activity) {
        if (activity != null) {
            for (DemandOnlyRvSmash onPause : this.mSmashes.values()) {
                onPause.onPause(activity);
            }
        }
    }

    public synchronized void setConsent(boolean z) {
        for (DemandOnlyRvSmash consent : this.mSmashes.values()) {
            consent.setConsent(z);
        }
    }

    public synchronized void loadRewardedVideo(String str) {
        try {
            if (!this.mSmashes.containsKey(str)) {
                sendMediationEvent(1500, str);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, ErrorBuilder.buildNonExistentInstanceError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                return;
            }
            DemandOnlyRvSmash demandOnlyRvSmash = (DemandOnlyRvSmash) this.mSmashes.get(str);
            sendProviderEvent(1001, demandOnlyRvSmash);
            demandOnlyRvSmash.loadRewardedVideo();
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadRewardedVideo exception ");
            sb.append(e.getMessage());
            logInternal(sb.toString());
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, ErrorBuilder.buildLoadFailedError("loadRewardedVideo exception"));
        }
        return;
    }

    public synchronized void showRewardedVideo(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(1500, str);
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, ErrorBuilder.buildNonExistentInstanceError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            return;
        }
        DemandOnlyRvSmash demandOnlyRvSmash = (DemandOnlyRvSmash) this.mSmashes.get(str);
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW, demandOnlyRvSmash);
        demandOnlyRvSmash.showRewardedVideo();
    }

    public synchronized boolean isRewardedVideoAvailable(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(1500, str);
            return false;
        }
        DemandOnlyRvSmash demandOnlyRvSmash = (DemandOnlyRvSmash) this.mSmashes.get(str);
        if (demandOnlyRvSmash.isRewardedVideoAvailable()) {
            sendProviderEvent(IronSourceConstants.RV_INSTANCE_READY_TRUE, demandOnlyRvSmash);
            return true;
        }
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_READY_FALSE, demandOnlyRvSmash);
        return false;
    }

    public void onRewardedVideoLoadSuccess(DemandOnlyRvSmash demandOnlyRvSmash, long j) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoLoadSuccess");
        sendProviderEvent(1002, demandOnlyRvSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoLoadSuccess(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdLoadFailed(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdLoadFailed error=");
        sb.append(ironSourceError);
        logSmashCallback(demandOnlyRvSmash, sb.toString());
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, demandOnlyRvSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(demandOnlyRvSmash.getSubProviderId(), ironSourceError);
    }

    public void onRewardedVideoAdOpened(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdOpened");
        sendProviderEvent(1005, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdOpened(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdClosed(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdClosed");
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_CLOSED, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdClosed(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdShowFailed error=");
        sb.append(ironSourceError);
        logSmashCallback(demandOnlyRvSmash, sb.toString());
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_FAILED, demandOnlyRvSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(demandOnlyRvSmash.getSubProviderId(), ironSourceError);
    }

    public void onRewardedVideoAdClicked(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdClicked");
        sendProviderEvent(1006, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdClicked(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdVisible(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdVisible");
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_VISIBLE, demandOnlyRvSmash);
    }

    public void onRewardedVideoAdRewarded(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdRewarded");
        Map providerEventData = demandOnlyRvSmash.getProviderEventData();
        StringBuilder sb = new StringBuilder();
        sb.append(Long.toString(new Date().getTime()));
        sb.append(this.mAppKey);
        sb.append(demandOnlyRvSmash.getInstanceName());
        providerEventData.put(IronSourceConstants.EVENTS_TRANS_ID, IronSourceUtils.getTransId(sb.toString()));
        if (!TextUtils.isEmpty(IronSourceObject.getInstance().getDynamicUserId())) {
            providerEventData.put(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceObject.getInstance().getDynamicUserId());
        }
        if (IronSourceObject.getInstance().getRvServerParams() != null) {
            for (String str : IronSourceObject.getInstance().getRvServerParams().keySet()) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("custom_");
                sb2.append(str);
                providerEventData.put(sb2.toString(), IronSourceObject.getInstance().getRvServerParams().get(str));
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(1010, new JSONObject(providerEventData)));
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdRewarded(demandOnlyRvSmash.getSubProviderId());
    }

    private void sendProviderEvent(int i, DemandOnlyRvSmash demandOnlyRvSmash) {
        sendProviderEvent(i, demandOnlyRvSmash, null);
    }

    private void sendProviderEvent(int i, DemandOnlyRvSmash demandOnlyRvSmash, Object[][] objArr) {
        Map providerEventData = demandOnlyRvSmash.getProviderEventData();
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("RV sendProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }

    private void sendMediationEvent(int i, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_DEMAND_ONLY, Integer.valueOf(1));
        String str2 = "spId";
        if (str == null) {
            str = "";
        }
        hashMap.put(str2, str);
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    private AbstractAdapter loadAdapter(String str) {
        try {
            Class cls = Class.forName("com.ironsource.adapters.ironsource.IronSourceAdapter");
            return (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, new Class[]{String.class}).invoke(cls, new Object[]{str});
        } catch (Exception unused) {
            return null;
        }
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyRvManager ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void logSmashCallback(DemandOnlyRvSmash demandOnlyRvSmash, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyRvManager ");
        sb.append(demandOnlyRvSmash.getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }
}
