package com.ironsource.mediationsdk.model;

import com.mintegral.msdk.base.b.d;

public enum PlacementCappingType {
    PER_DAY(d.b),
    PER_HOUR("h");
    
    public String value;

    private PlacementCappingType(String str) {
        this.value = str;
    }

    public String toString() {
        return this.value;
    }
}
