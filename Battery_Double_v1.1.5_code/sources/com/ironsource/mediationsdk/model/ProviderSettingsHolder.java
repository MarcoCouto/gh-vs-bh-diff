package com.ironsource.mediationsdk.model;

import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class ProviderSettingsHolder {
    private static ProviderSettingsHolder mInstance;
    private ArrayList<ProviderSettings> mProviderSettingsArrayList = new ArrayList<>();

    public static synchronized ProviderSettingsHolder getProviderSettingsHolder() {
        ProviderSettingsHolder providerSettingsHolder;
        synchronized (ProviderSettingsHolder.class) {
            if (mInstance == null) {
                mInstance = new ProviderSettingsHolder();
            }
            providerSettingsHolder = mInstance;
        }
        return providerSettingsHolder;
    }

    private ProviderSettingsHolder() {
    }

    public void addProviderSettings(ProviderSettings providerSettings) {
        if (providerSettings != null) {
            this.mProviderSettingsArrayList.add(providerSettings);
        }
    }

    public ProviderSettings getProviderSettings(String str) {
        Iterator it = this.mProviderSettingsArrayList.iterator();
        while (it.hasNext()) {
            ProviderSettings providerSettings = (ProviderSettings) it.next();
            if (providerSettings.getProviderName().equals(str)) {
                return providerSettings;
            }
        }
        ProviderSettings providerSettings2 = new ProviderSettings(str);
        addProviderSettings(providerSettings2);
        return providerSettings2;
    }

    public HashSet<String> getProviderSettingsByReflectionName(String str, String str2) {
        HashSet<String> hashSet = new HashSet<>();
        try {
            Iterator it = this.mProviderSettingsArrayList.iterator();
            while (it.hasNext()) {
                ProviderSettings providerSettings = (ProviderSettings) it.next();
                if (providerSettings.getProviderTypeForReflection().equals(str)) {
                    if (providerSettings.getRewardedVideoSettings() != null && providerSettings.getRewardedVideoSettings().length() > 0 && !TextUtils.isEmpty(providerSettings.getRewardedVideoSettings().optString(str2))) {
                        hashSet.add(providerSettings.getRewardedVideoSettings().optString(str2));
                    }
                    if (providerSettings.getInterstitialSettings() != null && providerSettings.getInterstitialSettings().length() > 0 && !TextUtils.isEmpty(providerSettings.getInterstitialSettings().optString(str2))) {
                        hashSet.add(providerSettings.getInterstitialSettings().optString(str2));
                    }
                    if (providerSettings.getBannerSettings() != null && providerSettings.getBannerSettings().length() > 0 && !TextUtils.isEmpty(providerSettings.getBannerSettings().optString(str2))) {
                        hashSet.add(providerSettings.getBannerSettings().optString(str2));
                    }
                }
            }
        } catch (Exception unused) {
        }
        return hashSet;
    }

    public boolean containsProviderSettings(String str) {
        Iterator it = this.mProviderSettingsArrayList.iterator();
        while (it.hasNext()) {
            if (((ProviderSettings) it.next()).getProviderName().equals(str)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<ProviderSettings> getProviderSettingsArrayList() {
        return this.mProviderSettingsArrayList;
    }

    public void fillSubProvidersDetails() {
        Iterator it = this.mProviderSettingsArrayList.iterator();
        while (it.hasNext()) {
            ProviderSettings providerSettings = (ProviderSettings) it.next();
            if (providerSettings.isMultipleInstances() && !TextUtils.isEmpty(providerSettings.getProviderTypeForReflection())) {
                ProviderSettings providerSettings2 = getProviderSettings(providerSettings.getProviderTypeForReflection());
                providerSettings.setInterstitialSettings(IronSourceUtils.mergeJsons(providerSettings.getInterstitialSettings(), providerSettings2.getInterstitialSettings()));
                providerSettings.setRewardedVideoSettings(IronSourceUtils.mergeJsons(providerSettings.getRewardedVideoSettings(), providerSettings2.getRewardedVideoSettings()));
                providerSettings.setBannerSettings(IronSourceUtils.mergeJsons(providerSettings.getBannerSettings(), providerSettings2.getBannerSettings()));
            }
        }
    }
}
