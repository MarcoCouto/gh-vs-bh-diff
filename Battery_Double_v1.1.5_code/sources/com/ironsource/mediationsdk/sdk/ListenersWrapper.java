package com.ironsource.mediationsdk.sdk;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class ListenersWrapper implements RewardedVideoListener, InterstitialListener, InternalOfferwallListener, RewardedInterstitialListener, SegmentListener {
    private CallbackHandlerThread mCallbackHandlerThread = new CallbackHandlerThread();
    /* access modifiers changed from: private */
    public InterstitialListener mInterstitialListener;
    private InterstitialPlacement mInterstitialPlacement = null;
    private long mLastChangedAvailabilityTime;
    /* access modifiers changed from: private */
    public OfferwallListener mOfferwallListener;
    /* access modifiers changed from: private */
    public RewardedInterstitialListener mRewardedInterstitialListener;
    /* access modifiers changed from: private */
    public RewardedVideoListener mRewardedVideoListener;
    private String mRvPlacementName = null;
    /* access modifiers changed from: private */
    public SegmentListener mSegementListener;

    private class CallbackHandlerThread extends Thread {
        private Handler mCallbackHandler;

        private CallbackHandlerThread() {
        }

        public void run() {
            Looper.prepare();
            this.mCallbackHandler = new Handler();
            Looper.loop();
        }

        public Handler getCallbackHandler() {
            return this.mCallbackHandler;
        }
    }

    public ListenersWrapper() {
        this.mCallbackHandlerThread.start();
        this.mLastChangedAvailabilityTime = new Date().getTime();
    }

    public void setInterstitialPlacement(InterstitialPlacement interstitialPlacement) {
        this.mInterstitialPlacement = interstitialPlacement;
    }

    public void setRvPlacement(String str) {
        this.mRvPlacementName = str;
    }

    private boolean canSendCallback(Object obj) {
        return (obj == null || this.mCallbackHandlerThread == null) ? false : true;
    }

    private void sendCallback(Runnable runnable) {
        if (this.mCallbackHandlerThread != null) {
            Handler callbackHandler = this.mCallbackHandlerThread.getCallbackHandler();
            if (callbackHandler != null) {
                callbackHandler.post(runnable);
            }
        }
    }

    public void setRewardedVideoListener(RewardedVideoListener rewardedVideoListener) {
        this.mRewardedVideoListener = rewardedVideoListener;
    }

    public void setInterstitialListener(InterstitialListener interstitialListener) {
        this.mInterstitialListener = interstitialListener;
    }

    public void setOfferwallListener(OfferwallListener offerwallListener) {
        this.mOfferwallListener = offerwallListener;
    }

    public void setRewardedInterstitialListener(RewardedInterstitialListener rewardedInterstitialListener) {
        this.mRewardedInterstitialListener = rewardedInterstitialListener;
    }

    public void setSegmentListener(SegmentListener segmentListener) {
        this.mSegementListener = segmentListener;
    }

    public void onSegmentReceived(final String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onSegmentReceived(");
        sb.append(str);
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (canSendCallback(this.mSegementListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    if (!TextUtils.isEmpty(str)) {
                        ListenersWrapper.this.mSegementListener.onSegmentReceived(str);
                    }
                }
            });
        }
    }

    public void onRewardedVideoAdOpened() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onRewardedVideoAdOpened()", 1);
        if (canSendCallback(this.mRewardedVideoListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedVideoListener.onRewardedVideoAdOpened();
                }
            });
        }
    }

    public void onRewardedVideoAdClosed() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onRewardedVideoAdClosed()", 1);
        if (canSendCallback(this.mRewardedVideoListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedVideoListener.onRewardedVideoAdClosed();
                }
            });
        }
    }

    public void onRewardedVideoAvailabilityChanged(final boolean z) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAvailabilityChanged(available:");
        sb.append(z);
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        long time = new Date().getTime() - this.mLastChangedAvailabilityTime;
        this.mLastChangedAvailabilityTime = new Date().getTime();
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        try {
            mediationAdditionalData.put(IronSourceConstants.EVENTS_DURATION, time);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(z ? IronSourceConstants.RV_CALLBACK_AVAILABILITY_TRUE : IronSourceConstants.RV_CALLBACK_AVAILABILITY_FALSE, mediationAdditionalData));
        if (canSendCallback(this.mRewardedVideoListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedVideoListener.onRewardedVideoAvailabilityChanged(z);
                }
            });
        }
    }

    public void onRewardedVideoAdStarted() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onRewardedVideoAdStarted()", 1);
        if (canSendCallback(this.mRewardedVideoListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedVideoListener.onRewardedVideoAdStarted();
                }
            });
        }
    }

    public void onRewardedVideoAdEnded() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onRewardedVideoAdEnded()", 1);
        if (canSendCallback(this.mRewardedVideoListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedVideoListener.onRewardedVideoAdEnded();
                }
            });
        }
    }

    public void onRewardedVideoAdRewarded(final Placement placement) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdRewarded(");
        sb.append(placement.toString());
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (canSendCallback(this.mRewardedVideoListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedVideoListener.onRewardedVideoAdRewarded(placement);
                }
            });
        }
    }

    public void onRewardedVideoAdClicked(final Placement placement) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdClicked(");
        sb.append(placement.getPlacementName());
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (canSendCallback(this.mRewardedVideoListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedVideoListener.onRewardedVideoAdClicked(placement);
                }
            });
        }
    }

    public void onRewardedVideoAdShowFailed(final IronSourceError ironSourceError) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdShowFailed(");
        sb.append(ironSourceError.toString());
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        try {
            String substring = ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39));
            mediationAdditionalData.put(IronSourceConstants.EVENTS_ERROR_CODE, ironSourceError.getErrorCode());
            mediationAdditionalData.put(IronSourceConstants.EVENTS_ERROR_REASON, substring);
            if (!TextUtils.isEmpty(this.mRvPlacementName)) {
                mediationAdditionalData.put("placement", this.mRvPlacementName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, mediationAdditionalData));
        if (canSendCallback(this.mRewardedVideoListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedVideoListener.onRewardedVideoAdShowFailed(ironSourceError);
                }
            });
        }
    }

    public void onInterstitialAdReady() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onInterstitialAdReady()", 1);
        if (canSendCallback(this.mInterstitialListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mInterstitialListener.onInterstitialAdReady();
                }
            });
        }
    }

    public void onInterstitialAdLoadFailed(final IronSourceError ironSourceError) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdLoadFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (canSendCallback(this.mInterstitialListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mInterstitialListener.onInterstitialAdLoadFailed(ironSourceError);
                }
            });
        }
    }

    public void onInterstitialAdOpened() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onInterstitialAdOpened()", 1);
        if (canSendCallback(this.mInterstitialListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mInterstitialListener.onInterstitialAdOpened();
                }
            });
        }
    }

    public void onInterstitialAdShowSucceeded() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onInterstitialAdShowSucceeded()", 1);
        if (canSendCallback(this.mInterstitialListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mInterstitialListener.onInterstitialAdShowSucceeded();
                }
            });
        }
    }

    public void onInterstitialAdShowFailed(final IronSourceError ironSourceError) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdShowFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        try {
            mediationAdditionalData.put(IronSourceConstants.EVENTS_ERROR_CODE, ironSourceError.getErrorCode());
            if (this.mInterstitialPlacement != null && !TextUtils.isEmpty(this.mInterstitialPlacement.getPlacementName())) {
                mediationAdditionalData.put("placement", this.mInterstitialPlacement.getPlacementName());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InterstitialEventsManager.getInstance().log(new EventData(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, mediationAdditionalData));
        if (canSendCallback(this.mInterstitialListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mInterstitialListener.onInterstitialAdShowFailed(ironSourceError);
                }
            });
        }
    }

    public void onInterstitialAdClicked() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onInterstitialAdClicked()", 1);
        if (canSendCallback(this.mInterstitialListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mInterstitialListener.onInterstitialAdClicked();
                }
            });
        }
    }

    public void onInterstitialAdClosed() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onInterstitialAdClosed()", 1);
        if (canSendCallback(this.mInterstitialListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mInterstitialListener.onInterstitialAdClosed();
                }
            });
        }
    }

    public void onInterstitialAdRewarded() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onInterstitialAdRewarded()", 1);
        if (canSendCallback(this.mRewardedInterstitialListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mRewardedInterstitialListener.onInterstitialAdRewarded();
                }
            });
        }
    }

    public void onOfferwallOpened() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onOfferwallOpened()", 1);
        if (canSendCallback(this.mOfferwallListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mOfferwallListener.onOfferwallOpened();
                }
            });
        }
    }

    public void onOfferwallShowFailed(final IronSourceError ironSourceError) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onOfferwallShowFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (canSendCallback(this.mOfferwallListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mOfferwallListener.onOfferwallShowFailed(ironSourceError);
                }
            });
        }
    }

    public boolean onOfferwallAdCredited(int i, int i2, boolean z) {
        boolean onOfferwallAdCredited = this.mOfferwallListener != null ? this.mOfferwallListener.onOfferwallAdCredited(i, i2, z) : false;
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onOfferwallAdCredited(credits:");
        sb.append(i);
        sb.append(", ");
        sb.append("totalCredits:");
        sb.append(i2);
        sb.append(", ");
        sb.append("totalCreditsFlag:");
        sb.append(z);
        sb.append("):");
        sb.append(onOfferwallAdCredited);
        logger.log(ironSourceTag, sb.toString(), 1);
        return onOfferwallAdCredited;
    }

    public void onGetOfferwallCreditsFailed(final IronSourceError ironSourceError) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onGetOfferwallCreditsFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (canSendCallback(this.mOfferwallListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mOfferwallListener.onGetOfferwallCreditsFailed(ironSourceError);
                }
            });
        }
    }

    public void onOfferwallClosed() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onOfferwallClosed()", 1);
        if (canSendCallback(this.mOfferwallListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mOfferwallListener.onOfferwallClosed();
                }
            });
        }
    }

    public void onOfferwallAvailable(boolean z) {
        onOfferwallAvailable(z, null);
    }

    public void onOfferwallAvailable(final boolean z, IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onOfferwallAvailable(isAvailable: ");
        sb.append(z);
        sb.append(")");
        String sb2 = sb.toString();
        if (ironSourceError != null) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb2);
            sb3.append(", error: ");
            sb3.append(ironSourceError.getErrorMessage());
            sb2 = sb3.toString();
        }
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, sb2, 1);
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        try {
            mediationAdditionalData.put("status", String.valueOf(z));
            if (ironSourceError != null) {
                mediationAdditionalData.put(IronSourceConstants.EVENTS_ERROR_CODE, ironSourceError.getErrorCode());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(302, mediationAdditionalData));
        if (canSendCallback(this.mOfferwallListener)) {
            sendCallback(new Runnable() {
                public void run() {
                    ListenersWrapper.this.mOfferwallListener.onOfferwallAvailable(z);
                }
            });
        }
    }
}
