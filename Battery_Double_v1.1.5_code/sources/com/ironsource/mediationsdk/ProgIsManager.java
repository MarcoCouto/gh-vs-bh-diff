package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionCappingManager;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.constants.Constants.JSMethods;
import com.smaato.sdk.core.api.VideoType;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

class ProgIsManager implements ProgIsManagerListener {
    private Context mAppContext;
    private AuctionHandler mAuctionHandler;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId = "";
    private String mCurrentPlacement = "";
    private long mInitMangerTime;
    private long mLoadStartTime;
    private int mMaxSmashesToLoad;
    private SessionCappingManager mSessionCappingManager;
    private final ConcurrentHashMap<String, ProgIsSmash> mSmashes = new ConcurrentHashMap<>();
    private MEDIATION_STATE mState;
    private long mTimeToWaitBeforeFirstAction;
    private CopyOnWriteArrayList<ProgIsSmash> mWaterfall = new CopyOnWriteArrayList<>();
    private ConcurrentHashMap<String, AuctionResponseItem> mWaterfallServerData = new ConcurrentHashMap<>();

    enum MEDIATION_STATE {
        STATE_NOT_INITIALIZED,
        STATE_READY_TO_LOAD,
        STATE_AUCTION,
        STATE_LOADING_SMASHES,
        STATE_READY_TO_SHOW,
        STATE_SHOWING
    }

    public ProgIsManager(Activity activity, List<ProviderSettings> list, InterstitialConfigurations interstitialConfigurations, String str, String str2, int i) {
        setState(MEDIATION_STATE.STATE_NOT_INITIALIZED);
        this.mAppContext = activity.getApplicationContext();
        this.mMaxSmashesToLoad = interstitialConfigurations.getInterstitialAdaptersSmartLoadAmount();
        CallbackThrottler.getInstance().setDelayLoadFailureNotificationInSeconds(i);
        AuctionSettings interstitialAuctionSettings = interstitialConfigurations.getInterstitialAuctionSettings();
        this.mTimeToWaitBeforeFirstAction = interstitialAuctionSettings.getTimeToWaitBeforeFirstAuctionMs();
        this.mAuctionHandler = new AuctionHandler(this.mAppContext, VideoType.INTERSTITIAL, interstitialAuctionSettings.getBlob(), interstitialAuctionSettings.getUrl());
        HashSet hashSet = new HashSet();
        for (ProviderSettings providerSettings : list) {
            AbstractAdapter loadAdapter = ProgUtils.loadAdapter(providerSettings);
            if (loadAdapter != null && AdaptersCompatibilityHandler.getInstance().isAdapterVersionISCompatible(loadAdapter)) {
                IronSourceObject.getInstance().addToISAdaptersList(loadAdapter);
                ProgIsSmash progIsSmash = new ProgIsSmash(activity, str, str2, providerSettings, this, interstitialConfigurations.getInterstitialAdaptersSmartLoadTimeout(), loadAdapter);
                this.mSmashes.put(progIsSmash.getInstanceName(), progIsSmash);
                hashSet.add(progIsSmash.getNameForReflection());
            }
        }
        this.mSessionCappingManager = new SessionCappingManager(new ArrayList(this.mSmashes.values()));
        for (ProgIsSmash progIsSmash2 : this.mSmashes.values()) {
            if (progIsSmash2.isBidder()) {
                progIsSmash2.initForBidding();
            } else if (hashSet.contains(progIsSmash2.getNameForReflection())) {
                hashSet.remove(progIsSmash2.getNameForReflection());
                progIsSmash2.preInitInterstitial();
            }
        }
        this.mInitMangerTime = new Date().getTime();
        setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
    }

    public void onResume(Activity activity) {
        synchronized (this.mSmashes) {
            for (ProgIsSmash onResume : this.mSmashes.values()) {
                onResume.onResume(activity);
            }
        }
    }

    public void onPause(Activity activity) {
        synchronized (this.mSmashes) {
            for (ProgIsSmash onPause : this.mSmashes.values()) {
                onPause.onPause(activity);
            }
        }
    }

    public void setConsent(boolean z) {
        synchronized (this.mSmashes) {
            for (ProgIsSmash consent : this.mSmashes.values()) {
                consent.setConsent(z);
            }
        }
    }

    /* access modifiers changed from: private */
    public void makeAuction() {
        setState(MEDIATION_STATE.STATE_AUCTION);
        this.mCurrentAuctionId = "";
        StringBuilder sb = new StringBuilder();
        long time = this.mTimeToWaitBeforeFirstAction - (new Date().getTime() - this.mInitMangerTime);
        if (time > 0) {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    ProgIsManager.this.makeAuction();
                }
            }, time);
            return;
        }
        sendMediationEvent(2000, null);
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        synchronized (this.mSmashes) {
            for (ProgIsSmash progIsSmash : this.mSmashes.values()) {
                if (!this.mSessionCappingManager.isCapped(progIsSmash)) {
                    if (progIsSmash.isBidder() && progIsSmash.isReadyToBid()) {
                        Map biddingData = progIsSmash.getBiddingData();
                        if (biddingData != null) {
                            hashMap.put(progIsSmash.getInstanceName(), biddingData);
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("2");
                            sb2.append(progIsSmash.getInstanceName());
                            sb2.append(",");
                            sb.append(sb2.toString());
                        }
                    } else if (!progIsSmash.isBidder()) {
                        arrayList.add(progIsSmash.getInstanceName());
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("1");
                        sb3.append(progIsSmash.getInstanceName());
                        sb3.append(",");
                        sb.append(sb3.toString());
                    }
                }
            }
        }
        if (hashMap.size() == 0 && arrayList.size() == 0) {
            sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1005)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Integer.valueOf(0)}});
            CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(1005, "No candidates available for auctioning"));
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1005)}});
            setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
            return;
        }
        if (sb.length() > 256) {
            sb.setLength(256);
        } else {
            sb.deleteCharAt(sb.length() - 1);
        }
        sendMediationEvent(IronSourceConstants.IS_AUCTION_REQUEST_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
        this.mAuctionHandler.executeAuction(hashMap, arrayList, SessionDepthManager.getInstance().getSessionDepth(2), new AuctionHandlerCallback() {
            public void callback(boolean z, List<AuctionResponseItem> list, String str, int i, String str2, long j) {
                if (z) {
                    ProgIsManager.this.mCurrentAuctionId = str;
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_AUCTION_SUCCESS, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
                    ProgIsManager.this.updateWaterfall(list);
                    ProgIsManager.this.loadSmashes();
                    return;
                }
                if (TextUtils.isEmpty(str2)) {
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(i, "Auction failed"));
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}});
                } else {
                    String substring = str2.substring(0, Math.min(str2.length(), 39));
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, substring}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(i, str2));
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, substring}});
                }
                ProgIsManager.this.setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
            }
        });
    }

    private String getAsString(AuctionResponseItem auctionResponseItem) {
        String str = TextUtils.isEmpty(auctionResponseItem.getServerData()) ? "1" : "2";
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(auctionResponseItem.getInstanceName());
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public void updateWaterfall(List<AuctionResponseItem> list) {
        synchronized (this.mSmashes) {
            this.mWaterfall.clear();
            this.mWaterfallServerData.clear();
            StringBuilder sb = new StringBuilder();
            for (AuctionResponseItem auctionResponseItem : list) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(getAsString(auctionResponseItem));
                sb2.append(",");
                sb.append(sb2.toString());
                ProgIsSmash progIsSmash = (ProgIsSmash) this.mSmashes.get(auctionResponseItem.getInstanceName());
                if (progIsSmash != null) {
                    progIsSmash.setIsLoadCandidate(true);
                    this.mWaterfall.add(progIsSmash);
                    this.mWaterfallServerData.put(progIsSmash.getInstanceName(), auctionResponseItem);
                }
            }
            if (sb.length() > 256) {
                sb.setLength(256);
            } else {
                sb.deleteCharAt(sb.length() - 1);
            }
            sendMediationEvent(IronSourceConstants.IS_AUCTION_RESPONSE_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
        }
    }

    /* access modifiers changed from: private */
    public void loadSmashes() {
        synchronized (this.mSmashes) {
            setState(MEDIATION_STATE.STATE_LOADING_SMASHES);
            for (int i = 0; i < Math.min(this.mMaxSmashesToLoad, this.mWaterfall.size()); i++) {
                ProgIsSmash progIsSmash = (ProgIsSmash) this.mWaterfall.get(i);
                String serverData = ((AuctionResponseItem) this.mWaterfallServerData.get(progIsSmash.getInstanceName())).getServerData();
                sendProviderEvent(2002, progIsSmash);
                progIsSmash.loadInterstitial(serverData);
            }
        }
    }

    public synchronized void loadInterstitial() {
        if (this.mState == MEDIATION_STATE.STATE_SHOWING) {
            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, "loadInterstitial() cannot be invoked while showing", 3);
        } else if ((this.mState == MEDIATION_STATE.STATE_READY_TO_LOAD || this.mState == MEDIATION_STATE.STATE_READY_TO_SHOW) && !CallbackThrottler.getInstance().hasPendingInvocation()) {
            this.mCurrentAuctionId = "";
            this.mCurrentPlacement = "";
            sendMediationEvent(2001);
            this.mLoadStartTime = new Date().getTime();
            makeAuction();
        } else {
            logInternal("loadInterstitial() already in progress");
        }
    }

    public synchronized void showInterstitial(String str) {
        if (this.mState != MEDIATION_STATE.STATE_READY_TO_SHOW) {
            StringBuilder sb = new StringBuilder();
            sb.append("showInterstitial() error state=");
            sb.append(this.mState.toString());
            logInternal(sb.toString());
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, "No ads to show"));
        } else if (str == null) {
            String str2 = "showInterstitial error: empty default placement in response";
            logInternal(str2);
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(1020, str2));
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1020)}});
        } else {
            this.mCurrentPlacement = str;
            sendMediationEventWithPlacement(2100);
            if (CappingManager.isInterstitialPlacementCapped(this.mAppContext, this.mCurrentPlacement)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("placement ");
                sb2.append(this.mCurrentPlacement);
                sb2.append(" is capped");
                String sb3 = sb2.toString();
                logInternal(sb3);
                ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT, sb3));
                sendMediationEventWithPlacement(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT)}});
                return;
            }
            synchronized (this.mSmashes) {
                Iterator it = this.mWaterfall.iterator();
                while (it.hasNext()) {
                    ProgIsSmash progIsSmash = (ProgIsSmash) it.next();
                    if (progIsSmash.isReadyToShow()) {
                        showInterstitial(progIsSmash, this.mCurrentPlacement);
                        return;
                    }
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("showInterstitial ");
                    sb4.append(progIsSmash.getInstanceName());
                    sb4.append(" isReadyToShow() == false");
                    logInternal(sb4.toString());
                }
                ISListenerWrapper.getInstance().onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
                sendMediationEventWithPlacement(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
            }
        }
    }

    private void showInterstitial(ProgIsSmash progIsSmash, String str) {
        setState(MEDIATION_STATE.STATE_SHOWING);
        progIsSmash.showInterstitial();
        sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW, progIsSmash);
        this.mSessionCappingManager.increaseShowCounter(progIsSmash);
        if (this.mSessionCappingManager.isCapped(progIsSmash)) {
            progIsSmash.setCappedPerSession();
            sendProviderEvent(IronSourceConstants.IS_CAP_SESSION, progIsSmash);
            StringBuilder sb = new StringBuilder();
            sb.append(progIsSmash.getInstanceName());
            sb.append(" was session capped");
            logInternal(sb.toString());
        }
        CappingManager.incrementIsShowCounter(this.mAppContext, str);
        if (CappingManager.isInterstitialPlacementCapped(this.mAppContext, str)) {
            sendMediationEventWithPlacement(IronSourceConstants.IS_CAP_PLACEMENT);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0037, code lost:
        return false;
     */
    public synchronized boolean isInterstitialReady() {
        if (IronSourceUtils.isNetworkConnected(this.mAppContext)) {
            if (this.mState == MEDIATION_STATE.STATE_READY_TO_SHOW) {
                synchronized (this.mSmashes) {
                    Iterator it = this.mWaterfall.iterator();
                    while (it.hasNext()) {
                        if (((ProgIsSmash) it.next()).isReadyToShow()) {
                            return true;
                        }
                    }
                    return false;
                }
            }
        }
    }

    public void onInterstitialAdReady(ProgIsSmash progIsSmash, long j) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdReady");
            sendProviderEvent(2003, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
            if (this.mState == MEDIATION_STATE.STATE_LOADING_SMASHES) {
                setState(MEDIATION_STATE.STATE_READY_TO_SHOW);
                ISListenerWrapper.getInstance().onInterstitialAdReady();
                sendMediationEvent(2004, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(new Date().getTime() - this.mLoadStartTime)}});
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00f3, code lost:
        return;
     */
    public void onInterstitialAdLoadFailed(IronSourceError ironSourceError, ProgIsSmash progIsSmash, long j) {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("onInterstitialAdLoadFailed error=");
            sb.append(ironSourceError.getErrorMessage());
            sb.append(" state=");
            sb.append(this.mState.name());
            logSmashCallback(progIsSmash, sb.toString());
            sendProviderEvent(IronSourceConstants.IS_INSTANCE_LOAD_FAILED, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
            if (this.mState == MEDIATION_STATE.STATE_LOADING_SMASHES || this.mState == MEDIATION_STATE.STATE_READY_TO_SHOW) {
                synchronized (this.mSmashes) {
                    Iterator it = this.mWaterfall.iterator();
                    boolean z = false;
                    while (it.hasNext()) {
                        ProgIsSmash progIsSmash2 = (ProgIsSmash) it.next();
                        if (progIsSmash2.getIsLoadCandidate()) {
                            String serverData = ((AuctionResponseItem) this.mWaterfallServerData.get(progIsSmash2.getInstanceName())).getServerData();
                            sendProviderEvent(2002, progIsSmash2);
                            progIsSmash2.loadInterstitial(serverData);
                            return;
                        } else if (progIsSmash2.isLoadingInProgress()) {
                            z = true;
                        }
                    }
                    if (this.mState == MEDIATION_STATE.STATE_LOADING_SMASHES && !z) {
                        CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, "No ads to show"));
                        sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
                        setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
                    }
                }
            }
        }
    }

    public void onInterstitialAdOpened(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdOpened");
            ISListenerWrapper.getInstance().onInterstitialAdOpened();
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_OPENED, progIsSmash);
            if (this.mWaterfallServerData.containsKey(progIsSmash.getInstanceName())) {
                this.mAuctionHandler.reportImpression((AuctionResponseItem) this.mWaterfallServerData.get(progIsSmash.getInstanceName()));
            }
        }
    }

    public void onInterstitialAdClosed(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdClosed");
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_CLOSED, progIsSmash);
            ISListenerWrapper.getInstance().onInterstitialAdClosed();
            setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
        }
    }

    public void onInterstitialAdShowSucceeded(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdShowSucceeded");
            ISListenerWrapper.getInstance().onInterstitialAdShowSucceeded();
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_SUCCESS, progIsSmash);
        }
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError, ProgIsSmash progIsSmash) {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("onInterstitialAdShowFailed error=");
            sb.append(ironSourceError.getErrorMessage());
            logSmashCallback(progIsSmash, sb.toString());
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(ironSourceError);
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_FAILED, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}});
            setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
        }
    }

    public void onInterstitialAdClicked(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, JSMethods.ON_INTERSTITIAL_AD_CLICKED);
            ISListenerWrapper.getInstance().onInterstitialAdClicked();
            sendProviderEventWithPlacement(2006, progIsSmash);
        }
    }

    public void onInterstitialAdVisible(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdVisible");
        }
    }

    public void onInterstitialInitFailed(IronSourceError ironSourceError, ProgIsSmash progIsSmash) {
        synchronized (this) {
            sendProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_FAILED, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}});
        }
    }

    public void onInterstitialInitSuccess(ProgIsSmash progIsSmash) {
        synchronized (this) {
            sendProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_SUCCESS, progIsSmash);
        }
    }

    /* access modifiers changed from: private */
    public void setState(MEDIATION_STATE mediation_state) {
        this.mState = mediation_state;
        StringBuilder sb = new StringBuilder();
        sb.append("state=");
        sb.append(mediation_state);
        logInternal(sb.toString());
    }

    private void logSmashCallback(ProgIsSmash progIsSmash, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("ProgIsManager ");
        sb.append(progIsSmash.getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("ProgIsManager ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void sendMediationEvent(int i) {
        sendMediationEvent(i, null, false);
    }

    /* access modifiers changed from: private */
    public void sendMediationEvent(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false);
    }

    private void sendMediationEventWithPlacement(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, true);
    }

    private void sendMediationEventWithPlacement(int i) {
        sendMediationEvent(i, null, true);
    }

    private void sendMediationEvent(int i, Object[][] objArr, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_PROGRAMMATIC, Integer.valueOf(1));
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            hashMap.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            hashMap.put("placement", this.mCurrentPlacement);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    hashMap.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("sendMediationEvent ");
                sb.append(e.getMessage());
                logInternal(sb.toString());
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash) {
        sendProviderEvent(i, progIsSmash, null, false);
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash, Object[][] objArr) {
        sendProviderEvent(i, progIsSmash, objArr, false);
    }

    private void sendProviderEventWithPlacement(int i, ProgIsSmash progIsSmash, Object[][] objArr) {
        sendProviderEvent(i, progIsSmash, objArr, true);
    }

    private void sendProviderEventWithPlacement(int i, ProgIsSmash progIsSmash) {
        sendProviderEvent(i, progIsSmash, null, true);
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash, Object[][] objArr, boolean z) {
        Map providerEventData = progIsSmash.getProviderEventData();
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            providerEventData.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            providerEventData.put("placement", this.mCurrentPlacement);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("IS sendProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }
}
