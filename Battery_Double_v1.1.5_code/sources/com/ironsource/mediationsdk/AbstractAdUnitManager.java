package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.BaseApi;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class AbstractAdUnitManager implements BaseApi {
    final String KEY_PLACEMENT = "placement";
    final String KEY_PROVIDER_PRIORITY = "providerPriority";
    final String KEY_REASON = IronSourceConstants.EVENTS_ERROR_REASON;
    final String KEY_REWARD_AMOUNT = IronSourceConstants.EVENTS_REWARD_AMOUNT;
    final String KEY_REWARD_NAME = IronSourceConstants.EVENTS_REWARD_NAME;
    final String KEY_STATUS = "status";
    Activity mActivity;
    String mAppKey;
    boolean mBackFillInitStarted;
    private AbstractSmash mBackfillSmash;
    boolean mCanShowPremium = true;
    DailyCappingManager mDailyCappingManager = null;
    AtomicBoolean mDidImplementOnPause = new AtomicBoolean();
    AtomicBoolean mDidImplementOnResume = new AtomicBoolean();
    Boolean mLastMediationAvailabilityState;
    IronSourceLoggerManager mLoggerManager = IronSourceLoggerManager.getLogger();
    private AbstractSmash mPremiumSmash;
    boolean mShouldTrackNetworkState = false;
    int mSmartLoadAmount;
    final CopyOnWriteArrayList<AbstractSmash> mSmashArray = new CopyOnWriteArrayList<>();
    String mUserId;

    public void setAge(int i) {
    }

    public void setGender(String str) {
    }

    public void setMediationSegment(String str) {
    }

    /* access modifiers changed from: 0000 */
    public abstract void shouldTrackNetworkState(Context context, boolean z);

    AbstractAdUnitManager() {
    }

    public void onResume(Activity activity) {
        this.mDidImplementOnResume.set(true);
        if (activity != null) {
            this.mActivity = activity;
        }
        synchronized (this.mSmashArray) {
            if (this.mSmashArray != null) {
                Iterator it = this.mSmashArray.iterator();
                while (it.hasNext()) {
                    ((AbstractSmash) it.next()).onResume(activity);
                }
            }
        }
    }

    public void onPause(Activity activity) {
        this.mDidImplementOnPause.set(true);
        synchronized (this.mSmashArray) {
            if (this.mSmashArray != null) {
                Iterator it = this.mSmashArray.iterator();
                while (it.hasNext()) {
                    ((AbstractSmash) it.next()).onPause(activity);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void setSmartLoadAmount(int i) {
        this.mSmartLoadAmount = i;
    }

    /* access modifiers changed from: 0000 */
    public void addSmashToArray(AbstractSmash abstractSmash) {
        this.mSmashArray.add(abstractSmash);
        if (this.mDailyCappingManager != null) {
            this.mDailyCappingManager.addSmash(abstractSmash);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setBackfillSmash(AbstractSmash abstractSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(abstractSmash.getInstanceName());
        sb.append(" is set as backfill");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
        this.mBackfillSmash = abstractSmash;
    }

    /* access modifiers changed from: 0000 */
    public void setPremiumSmash(AbstractSmash abstractSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(abstractSmash.getInstanceName());
        sb.append(" is set as premium");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
        this.mPremiumSmash = abstractSmash;
    }

    /* access modifiers changed from: 0000 */
    public AbstractSmash getBackfillSmash() {
        return this.mBackfillSmash;
    }

    /* access modifiers changed from: 0000 */
    public AbstractSmash getPremiumSmash() {
        return this.mPremiumSmash;
    }

    /* access modifiers changed from: 0000 */
    public void setCustomParams(AbstractSmash abstractSmash) {
        try {
            Integer age = IronSourceObject.getInstance().getAge();
            if (age != null) {
                abstractSmash.setAge(age.intValue());
            }
            String gender = IronSourceObject.getInstance().getGender();
            if (!TextUtils.isEmpty(gender)) {
                abstractSmash.setGender(gender);
            }
            String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
            if (!TextUtils.isEmpty(mediationSegment)) {
                abstractSmash.setMediationSegment(mediationSegment);
            }
            String pluginType = ConfigFile.getConfigFile().getPluginType();
            if (!TextUtils.isEmpty(pluginType)) {
                abstractSmash.setPluginData(pluginType, ConfigFile.getConfigFile().getPluginFrameworkVersion());
            }
            Boolean consent = IronSourceObject.getInstance().getConsent();
            if (consent != null) {
                abstractSmash.setConsent(consent.booleanValue());
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append(":setCustomParams():");
            sb.append(e.toString());
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean canShowPremium() {
        return this.mCanShowPremium;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void disablePremiumForCurrentSession() {
        this.mCanShowPremium = false;
    }

    /* access modifiers changed from: 0000 */
    public synchronized AbstractAdapter getLoadedAdapterOrFetchByReflection(AbstractSmash abstractSmash) {
        AbstractAdapter existingAdapter;
        try {
            existingAdapter = IronSourceObject.getInstance().getExistingAdapter(abstractSmash.getName());
            if (existingAdapter == null) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("loading ");
                sb.append(abstractSmash.getName());
                sb.append(" with reflection");
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
                StringBuilder sb2 = new StringBuilder();
                sb2.append("com.ironsource.adapters.");
                sb2.append(abstractSmash.getNameForReflection().toLowerCase());
                sb2.append(".");
                sb2.append(abstractSmash.getNameForReflection());
                sb2.append("Adapter");
                Class cls = Class.forName(sb2.toString());
                existingAdapter = (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, new Class[]{String.class}).invoke(cls, new Object[]{abstractSmash.getName()});
            } else {
                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                IronSourceTag ironSourceTag2 = IronSourceTag.INTERNAL;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("using previously loaded ");
                sb3.append(abstractSmash.getName());
                ironSourceLoggerManager2.log(ironSourceTag2, sb3.toString(), 0);
            }
        } catch (Exception unused) {
            return null;
        }
        return existingAdapter;
    }

    /* access modifiers changed from: 0000 */
    public void setConsent(boolean z) {
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash != null) {
                abstractSmash.setConsent(z);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void verifyOnPauseOnResume() {
        if (!this.mDidImplementOnPause.get()) {
            this.mLoggerManager.log(IronSourceTag.NATIVE, "IronSource.onPause() wasn't overridden in your activity lifecycle!", 3);
        }
        if (!this.mDidImplementOnResume.get()) {
            this.mLoggerManager.log(IronSourceTag.NATIVE, "IronSource.onResume() wasn't overridden in your activity lifecycle!", 3);
        }
    }
}
