package com.ironsource.mediationsdk;

/* compiled from: RvAuctionTrigger */
interface RvAuctionTriggerCallback {
    void onAuctionTriggered();
}
