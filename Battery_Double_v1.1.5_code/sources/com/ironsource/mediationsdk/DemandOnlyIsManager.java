package com.ironsource.mediationsdk;

import android.app.Activity;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyIsManagerListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.JSMethods;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class DemandOnlyIsManager implements DemandOnlyIsManagerListener {
    private ConcurrentHashMap<String, DemandOnlyIsSmash> mSmashes = new ConcurrentHashMap<>();

    DemandOnlyIsManager(Activity activity, List<ProviderSettings> list, InterstitialConfigurations interstitialConfigurations, String str, String str2) {
        for (ProviderSettings providerSettings : list) {
            if (providerSettings.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME) || providerSettings.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.IRONSOURCE_CONFIG_NAME)) {
                AbstractAdapter loadAdapter = loadAdapter(providerSettings.getProviderInstanceName());
                if (loadAdapter != null) {
                    DemandOnlyIsSmash demandOnlyIsSmash = new DemandOnlyIsSmash(activity, str, str2, providerSettings, this, interstitialConfigurations.getInterstitialAdaptersSmartLoadTimeout(), loadAdapter);
                    this.mSmashes.put(providerSettings.getSubProviderId(), demandOnlyIsSmash);
                }
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("cannot load ");
                sb.append(providerSettings.getProviderTypeForReflection());
                logInternal(sb.toString());
            }
        }
    }

    public synchronized void onResume(Activity activity) {
        if (activity != null) {
            for (DemandOnlyIsSmash onResume : this.mSmashes.values()) {
                onResume.onResume(activity);
            }
        }
    }

    public synchronized void onPause(Activity activity) {
        if (activity != null) {
            for (DemandOnlyIsSmash onPause : this.mSmashes.values()) {
                onPause.onPause(activity);
            }
        }
    }

    public synchronized void setConsent(boolean z) {
        for (DemandOnlyIsSmash consent : this.mSmashes.values()) {
            consent.setConsent(z);
        }
    }

    public synchronized void loadInterstitial(String str) {
        try {
            if (!this.mSmashes.containsKey(str)) {
                sendMediationEvent(2500, str);
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, ErrorBuilder.buildNonExistentInstanceError("Interstitial"));
                return;
            }
            DemandOnlyIsSmash demandOnlyIsSmash = (DemandOnlyIsSmash) this.mSmashes.get(str);
            sendProviderEvent(2002, demandOnlyIsSmash);
            demandOnlyIsSmash.loadInterstitial();
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadInterstitial exception ");
            sb.append(e.getMessage());
            logInternal(sb.toString());
            ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, ErrorBuilder.buildLoadFailedError("loadInterstitial exception"));
        }
        return;
    }

    public synchronized void showInterstitial(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(2500, str);
            ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdShowFailed(str, ErrorBuilder.buildNonExistentInstanceError("Interstitial"));
            return;
        }
        DemandOnlyIsSmash demandOnlyIsSmash = (DemandOnlyIsSmash) this.mSmashes.get(str);
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_SHOW, demandOnlyIsSmash);
        demandOnlyIsSmash.showInterstitial();
    }

    public synchronized boolean isInterstitialReady(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(2500, str);
            return false;
        }
        DemandOnlyIsSmash demandOnlyIsSmash = (DemandOnlyIsSmash) this.mSmashes.get(str);
        if (demandOnlyIsSmash.isInterstitialReady()) {
            sendProviderEvent(IronSourceConstants.IS_INSTANCE_READY_TRUE, demandOnlyIsSmash);
            return true;
        }
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_READY_FALSE, demandOnlyIsSmash);
        return false;
    }

    public synchronized void onInterstitialAdReady(DemandOnlyIsSmash demandOnlyIsSmash, long j) {
        logSmashCallback(demandOnlyIsSmash, "onInterstitialAdReady");
        sendProviderEvent(2003, demandOnlyIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdReady(demandOnlyIsSmash.getSubProviderId());
    }

    public synchronized void onInterstitialAdLoadFailed(IronSourceError ironSourceError, DemandOnlyIsSmash demandOnlyIsSmash, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdLoadFailed error=");
        sb.append(ironSourceError.toString());
        logSmashCallback(demandOnlyIsSmash, sb.toString());
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_LOAD_FAILED, demandOnlyIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(demandOnlyIsSmash.getSubProviderId(), ironSourceError);
    }

    public synchronized void onInterstitialAdOpened(DemandOnlyIsSmash demandOnlyIsSmash) {
        logSmashCallback(demandOnlyIsSmash, "onInterstitialAdOpened");
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_OPENED, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdOpened(demandOnlyIsSmash.getSubProviderId());
    }

    public synchronized void onInterstitialAdClosed(DemandOnlyIsSmash demandOnlyIsSmash) {
        logSmashCallback(demandOnlyIsSmash, "onInterstitialAdClosed");
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_CLOSED, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdClosed(demandOnlyIsSmash.getSubProviderId());
    }

    public synchronized void onInterstitialAdShowFailed(IronSourceError ironSourceError, DemandOnlyIsSmash demandOnlyIsSmash) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdShowFailed error=");
        sb.append(ironSourceError.toString());
        logSmashCallback(demandOnlyIsSmash, sb.toString());
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_SHOW_FAILED, demandOnlyIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}});
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdShowFailed(demandOnlyIsSmash.getSubProviderId(), ironSourceError);
    }

    public synchronized void onInterstitialAdClicked(DemandOnlyIsSmash demandOnlyIsSmash) {
        logSmashCallback(demandOnlyIsSmash, JSMethods.ON_INTERSTITIAL_AD_CLICKED);
        sendProviderEvent(2006, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdClicked(demandOnlyIsSmash.getSubProviderId());
    }

    public synchronized void onInterstitialAdVisible(DemandOnlyIsSmash demandOnlyIsSmash) {
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_VISIBLE, demandOnlyIsSmash);
        logSmashCallback(demandOnlyIsSmash, "onInterstitialAdVisible");
    }

    private void sendProviderEvent(int i, DemandOnlyIsSmash demandOnlyIsSmash) {
        sendProviderEvent(i, demandOnlyIsSmash, null);
    }

    private void sendProviderEvent(int i, DemandOnlyIsSmash demandOnlyIsSmash, Object[][] objArr) {
        Map providerEventData = demandOnlyIsSmash.getProviderEventData();
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("IS sendProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }

    private void sendMediationEvent(int i, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_DEMAND_ONLY, Integer.valueOf(1));
        String str2 = "spId";
        if (str == null) {
            str = "";
        }
        hashMap.put(str2, str);
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    private AbstractAdapter loadAdapter(String str) {
        try {
            Class cls = Class.forName("com.ironsource.adapters.ironsource.IronSourceAdapter");
            return (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, new Class[]{String.class}).invoke(cls, new Object[]{str});
        } catch (Exception unused) {
            return null;
        }
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyIsManager ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void logSmashCallback(DemandOnlyIsSmash demandOnlyIsSmash, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyIsManager ");
        sb.append(demandOnlyIsSmash.getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }
}
