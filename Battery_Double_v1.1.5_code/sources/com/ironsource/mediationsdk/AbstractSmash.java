package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.BaseApi;
import java.util.HashSet;
import java.util.Timer;

public abstract class AbstractSmash implements BaseApi {
    public static final int MAX_ADS_PER_DAY_DEFAULT_VALUE = 99;
    final String MAX_ADS_PER_DAY_KEY = "maxAdsPerDay";
    final String MAX_ADS_PER_ITERATION_KEY = "maxAdsPerIteration";
    final String MAX_ADS_PER_SESSION_KEY = "maxAdsPerSession";
    String mAdSourceNameForEvents;
    AbstractAdapter mAdapter;
    ProviderSettings mAdapterConfigs;
    Timer mInitTimer;
    String mInstanceName;
    boolean mIsInForeground;
    boolean mIsMultipleInstances;
    int mIterationShowCounter;
    Timer mLoadTimer;
    IronSourceLoggerManager mLoggerManager;
    int mMaxAdsPerDay;
    int mMaxAdsPerIteration;
    int mMaxAdsPerSession;
    MEDIATION_STATE mMediationState;
    String mNameForReflection;
    int mProviderPriority;
    int mSessionShowCounter;
    String mSpId;

    public enum MEDIATION_STATE {
        NOT_INITIATED(0),
        INIT_FAILED(1),
        INITIATED(2),
        AVAILABLE(3),
        NOT_AVAILABLE(4),
        EXHAUSTED(5),
        CAPPED_PER_SESSION(6),
        INIT_PENDING(7),
        LOAD_PENDING(8),
        CAPPED_PER_DAY(9);
        
        private int mValue;

        private MEDIATION_STATE(int i) {
            this.mValue = i;
        }

        public int getValue() {
            return this.mValue;
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void completeIteration();

    /* access modifiers changed from: protected */
    public abstract String getAdUnitString();

    /* access modifiers changed from: 0000 */
    public abstract void startInitTimer();

    /* access modifiers changed from: 0000 */
    public abstract void startLoadTimer();

    AbstractSmash(ProviderSettings providerSettings) {
        this.mNameForReflection = providerSettings.getProviderTypeForReflection();
        this.mInstanceName = providerSettings.getProviderInstanceName();
        this.mIsMultipleInstances = providerSettings.isMultipleInstances();
        this.mAdapterConfigs = providerSettings;
        this.mSpId = providerSettings.getSubProviderId();
        this.mAdSourceNameForEvents = providerSettings.getAdSourceNameForEvents();
        this.mIterationShowCounter = 0;
        this.mSessionShowCounter = 0;
        this.mMediationState = MEDIATION_STATE.NOT_INITIATED;
        this.mLoggerManager = IronSourceLoggerManager.getLogger();
        this.mIsInForeground = true;
    }

    /* access modifiers changed from: 0000 */
    public void setAdapterForSmash(AbstractAdapter abstractAdapter) {
        this.mAdapter = abstractAdapter;
    }

    /* access modifiers changed from: 0000 */
    public boolean isExhausted() {
        return this.mIterationShowCounter >= this.mMaxAdsPerIteration;
    }

    /* access modifiers changed from: 0000 */
    public boolean isCappedPerSession() {
        return this.mSessionShowCounter >= this.mMaxAdsPerSession;
    }

    /* access modifiers changed from: 0000 */
    public boolean isCappedPerDay() {
        return this.mMediationState == MEDIATION_STATE.CAPPED_PER_DAY;
    }

    /* access modifiers changed from: 0000 */
    public boolean isMediationAvailable() {
        return !isExhausted() && !isCappedPerSession() && !isCappedPerDay();
    }

    /* access modifiers changed from: 0000 */
    public void preShow() {
        this.mIterationShowCounter++;
        this.mSessionShowCounter++;
        if (isCappedPerSession()) {
            setMediationState(MEDIATION_STATE.CAPPED_PER_SESSION);
        } else if (isExhausted()) {
            setMediationState(MEDIATION_STATE.EXHAUSTED);
        }
    }

    /* access modifiers changed from: 0000 */
    public void stopInitTimer() {
        try {
            if (this.mInitTimer != null) {
                this.mInitTimer.cancel();
            }
        } catch (Exception e) {
            logException("stopInitTimer", e.getLocalizedMessage());
        } catch (Throwable th) {
            this.mInitTimer = null;
            throw th;
        }
        this.mInitTimer = null;
    }

    /* access modifiers changed from: 0000 */
    public void stopLoadTimer() {
        try {
            if (this.mLoadTimer != null) {
                this.mLoadTimer.cancel();
            }
        } catch (Exception e) {
            logException("stopLoadTimer", e.getLocalizedMessage());
        } catch (Throwable th) {
            this.mLoadTimer = null;
            throw th;
        }
        this.mLoadTimer = null;
    }

    /* access modifiers changed from: 0000 */
    public void setPluginData(String str, String str2) {
        if (this.mAdapter != null) {
            this.mAdapter.setPluginData(str, str2);
        }
    }

    /* access modifiers changed from: 0000 */
    public MEDIATION_STATE getMediationState() {
        return this.mMediationState;
    }

    /* access modifiers changed from: 0000 */
    public String getNameForReflection() {
        return this.mNameForReflection;
    }

    /* access modifiers changed from: 0000 */
    public String getInstanceName() {
        return this.mInstanceName;
    }

    public String getName() {
        if (this.mIsMultipleInstances) {
            return this.mNameForReflection;
        }
        return this.mInstanceName;
    }

    public String getSubProviderId() {
        return this.mSpId;
    }

    public String getAdSourceNameForEvents() {
        if (!TextUtils.isEmpty(this.mAdSourceNameForEvents)) {
            return this.mAdSourceNameForEvents;
        }
        return getName();
    }

    /* access modifiers changed from: 0000 */
    public int getMaxAdsPerSession() {
        return this.mMaxAdsPerSession;
    }

    /* access modifiers changed from: 0000 */
    public int getMaxAdsPerIteration() {
        return this.mMaxAdsPerIteration;
    }

    public int getMaxAdsPerDay() {
        return this.mMaxAdsPerDay;
    }

    public AbstractAdapter getAdapter() {
        return this.mAdapter;
    }

    public int getProviderPriority() {
        return this.mProviderPriority;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0048, code lost:
        return;
     */
    public synchronized void setMediationState(MEDIATION_STATE mediation_state) {
        if (this.mMediationState != mediation_state) {
            this.mMediationState = mediation_state;
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("Smart Loading - ");
            sb.append(getInstanceName());
            sb.append(" state changed to ");
            sb.append(mediation_state.toString());
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
            if (this.mAdapter != null && (mediation_state == MEDIATION_STATE.CAPPED_PER_SESSION || mediation_state == MEDIATION_STATE.CAPPED_PER_DAY)) {
                this.mAdapter.setMediationState(mediation_state, getAdUnitString());
            }
        }
    }

    public void onResume(Activity activity) {
        if (this.mAdapter != null) {
            this.mAdapter.onResume(activity);
        }
        this.mIsInForeground = true;
    }

    public void onPause(Activity activity) {
        if (this.mAdapter != null) {
            this.mAdapter.onPause(activity);
        }
        this.mIsInForeground = false;
    }

    public void setAge(int i) {
        if (this.mAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getName());
            sb.append(":setAge(age:");
            sb.append(i);
            sb.append(")");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            this.mAdapter.setAge(i);
        }
    }

    public void setGender(String str) {
        if (this.mAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getName());
            sb.append(":setGender(gender:");
            sb.append(str);
            sb.append(")");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            this.mAdapter.setGender(str);
        }
    }

    public void setMediationSegment(String str) {
        if (this.mAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getName());
            sb.append(":setMediationSegment(segment:");
            sb.append(str);
            sb.append(")");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            this.mAdapter.setMediationSegment(str);
        }
    }

    public HashSet<String> getAllSettingsForProvider(String str) {
        return IronSourceObject.getInstance().getAllSettingsForProvider(this.mNameForReflection, str);
    }

    /* access modifiers changed from: 0000 */
    public void setProviderPriority(int i) {
        this.mProviderPriority = i;
    }

    /* access modifiers changed from: 0000 */
    public void setConsent(boolean z) {
        if (this.mAdapter != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getName());
            sb.append(" | ");
            sb.append(getAdUnitString());
            sb.append("| setConsent(consent:");
            sb.append(z);
            sb.append(")");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            this.mAdapter.setConsent(z);
        }
    }

    /* access modifiers changed from: 0000 */
    public void logException(String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" exception: ");
        sb.append(getInstanceName());
        sb.append(" | ");
        sb.append(str2);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
    }
}
