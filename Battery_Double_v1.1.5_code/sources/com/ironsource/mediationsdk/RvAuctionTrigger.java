package com.ironsource.mediationsdk;

import com.ironsource.mediationsdk.utils.AuctionSettings;
import java.util.Timer;
import java.util.TimerTask;

public class RvAuctionTrigger {
    private AuctionSettings mAuctionSettings;
    /* access modifiers changed from: private */
    public RvAuctionTriggerCallback mListener;
    private Timer mTimer = null;

    public RvAuctionTrigger(AuctionSettings auctionSettings, RvAuctionTriggerCallback rvAuctionTriggerCallback) {
        this.mAuctionSettings = auctionSettings;
        this.mListener = rvAuctionTriggerCallback;
    }

    public synchronized void showStart() {
        if (this.mAuctionSettings.getIsAuctionOnShowStart()) {
            stopTimer();
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    RvAuctionTrigger.this.mListener.onAuctionTriggered();
                }
            }, this.mAuctionSettings.getTimeToWaitBeforeAuctionMs());
        }
    }

    public synchronized void showEnd() {
        if (!this.mAuctionSettings.getIsAuctionOnShowStart()) {
            stopTimer();
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    RvAuctionTrigger.this.mListener.onAuctionTriggered();
                }
            }, this.mAuctionSettings.getTimeToWaitBeforeAuctionMs());
        }
    }

    public synchronized void showError() {
        stopTimer();
        this.mListener.onAuctionTriggered();
    }

    public synchronized void loadError() {
        stopTimer();
        this.mTimer = new Timer();
        this.mTimer.schedule(new TimerTask() {
            public void run() {
                RvAuctionTrigger.this.mListener.onAuctionTriggered();
            }
        }, this.mAuctionSettings.getAuctionRetryInterval());
    }

    private void stopTimer() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }
}
