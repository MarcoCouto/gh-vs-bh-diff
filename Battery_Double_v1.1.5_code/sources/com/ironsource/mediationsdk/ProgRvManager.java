package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.SessionCappingManager;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

class ProgRvManager implements ProgRvManagerListener, RvAuctionTriggerCallback {
    private Context mAppContext;
    private AuctionHandler mAuctionHandler;
    private long mAuctionStartTime;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId;
    private String mCurrentPlacement;
    private boolean mIsShowingVideo;
    private long mLastChangedAvailabilityTime;
    private Boolean mLastReportedAvailabilityState = null;
    private int mMaxSmashesToLoad;
    /* access modifiers changed from: private */
    public RvAuctionTrigger mRvAuctionTrigger;
    private SessionCappingManager mSessionCappingManager;
    private final ConcurrentHashMap<String, ProgRvSmash> mSmashes;
    private RV_MEDIATION_STATE mState;
    private CopyOnWriteArrayList<ProgRvSmash> mWaterfall;
    private ConcurrentHashMap<String, AuctionResponseItem> mWaterfallServerData;

    private enum RV_MEDIATION_STATE {
        RV_STATE_INITIATING,
        RV_STATE_AUCTION_IN_PROGRESS,
        RV_STATE_NOT_LOADED,
        RV_STATE_LOADING_SMASHES,
        RV_STATE_READY_TO_SHOW
    }

    public ProgRvManager(Activity activity, List<ProviderSettings> list, RewardedVideoConfigurations rewardedVideoConfigurations, String str, String str2) {
        setState(RV_MEDIATION_STATE.RV_STATE_INITIATING);
        this.mAppContext = activity.getApplicationContext();
        this.mMaxSmashesToLoad = rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadAmount();
        this.mCurrentPlacement = "";
        AuctionSettings rewardedVideoAuctionSettings = rewardedVideoConfigurations.getRewardedVideoAuctionSettings();
        this.mIsShowingVideo = false;
        this.mWaterfall = new CopyOnWriteArrayList<>();
        this.mWaterfallServerData = new ConcurrentHashMap<>();
        this.mLastChangedAvailabilityTime = new Date().getTime();
        this.mAuctionHandler = new AuctionHandler(this.mAppContext, "rewardedVideo", rewardedVideoAuctionSettings.getBlob(), rewardedVideoAuctionSettings.getUrl());
        this.mRvAuctionTrigger = new RvAuctionTrigger(rewardedVideoAuctionSettings, this);
        this.mSmashes = new ConcurrentHashMap<>();
        for (ProviderSettings providerSettings : list) {
            AbstractAdapter loadAdapter = ProgUtils.loadAdapter(providerSettings);
            if (loadAdapter != null && AdaptersCompatibilityHandler.getInstance().isAdapterVersionRVCompatible(loadAdapter)) {
                IronSourceObject.getInstance().addToRVAdaptersList(loadAdapter);
                ProgRvSmash progRvSmash = new ProgRvSmash(activity, str, str2, providerSettings, this, rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadTimeout(), loadAdapter);
                this.mSmashes.put(progRvSmash.getInstanceName(), progRvSmash);
            }
        }
        this.mSessionCappingManager = new SessionCappingManager(new ArrayList(this.mSmashes.values()));
        for (ProgRvSmash progRvSmash2 : this.mSmashes.values()) {
            if (progRvSmash2.isBidder()) {
                progRvSmash2.initForBidding();
            }
        }
        new Timer().schedule(new TimerTask() {
            public void run() {
                ProgRvManager.this.makeAuction();
            }
        }, rewardedVideoAuctionSettings.getTimeToWaitBeforeFirstAuctionMs());
    }

    public synchronized void showRewardedVideo(Placement placement) {
        if (placement == null) {
            String str = "showRewardedVideo error: empty default placement in response";
            logInternal(str);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(1021, str));
            sendMediationEvent(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1021)}}, false, true);
            return;
        }
        this.mCurrentPlacement = placement.getPlacementName();
        StringBuilder sb = new StringBuilder();
        sb.append("showRewardedVideo() placement=");
        sb.append(this.mCurrentPlacement);
        logApi(sb.toString());
        sendMediationEventWithPlacement(IronSourceConstants.RV_API_SHOW_CALLED);
        if (this.mIsShowingVideo) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("showRewardedVideo error: can't show ad while an ad is already showing. State: ");
            sb2.append(this.mState);
            String sb3 = sb2.toString();
            logInternal(sb3);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_RV_SHOW_CALLED_DURING_SHOW, sb3));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_RV_SHOW_CALLED_DURING_SHOW)}});
        } else if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
            String str2 = "showRewardedVideo error: show called while no ads are available";
            logInternal(str2);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_RV_SHOW_CALLED_WRONG_STATE, str2));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_RV_SHOW_CALLED_WRONG_STATE)}});
        } else if (CappingManager.isRvPlacementCapped(this.mAppContext, this.mCurrentPlacement)) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("placement ");
            sb4.append(this.mCurrentPlacement);
            sb4.append(" is capped");
            String sb5 = sb4.toString();
            logInternal(sb5);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT, sb5));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT)}});
        } else {
            synchronized (this.mSmashes) {
                Iterator it = this.mWaterfall.iterator();
                while (it.hasNext()) {
                    ProgRvSmash progRvSmash = (ProgRvSmash) it.next();
                    if (progRvSmash.isReadyToShow()) {
                        this.mIsShowingVideo = true;
                        progRvSmash.reportShowChance(true);
                        showVideo(progRvSmash, placement);
                        setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
                        this.mRvAuctionTrigger.showStart();
                        return;
                    }
                    progRvSmash.reportShowChance(false);
                }
                logApi("showRewardedVideo(): No ads to show ");
                RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
                this.mRvAuctionTrigger.showError();
            }
        }
    }

    private void showVideo(ProgRvSmash progRvSmash, Placement placement) {
        logInternal("showVideo()");
        this.mSessionCappingManager.increaseShowCounter(progRvSmash);
        if (this.mSessionCappingManager.isCapped(progRvSmash)) {
            progRvSmash.setCappedPerSession();
            StringBuilder sb = new StringBuilder();
            sb.append(progRvSmash.getInstanceName());
            sb.append(" rewarded video is now session capped");
            logInternal(sb.toString());
        }
        CappingManager.incrementRvShowCounter(this.mAppContext, placement.getPlacementName());
        if (CappingManager.isRvPlacementCapped(this.mAppContext, placement.getPlacementName())) {
            sendMediationEventWithPlacement(IronSourceConstants.RV_CAP_PLACEMENT);
        }
        progRvSmash.showVideo(placement);
    }

    public synchronized boolean isRewardedVideoAvailable() {
        return isRewardedVideoAvailableInternal();
    }

    private boolean isRewardedVideoAvailableInternal() {
        if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW || this.mIsShowingVideo) {
            return false;
        }
        synchronized (this.mSmashes) {
            Iterator it = this.mWaterfall.iterator();
            while (it.hasNext()) {
                if (((ProgRvSmash) it.next()).isReadyToShow()) {
                    return true;
                }
            }
            return false;
        }
    }

    public void onResume(Activity activity) {
        synchronized (this.mSmashes) {
            for (ProgRvSmash onResume : this.mSmashes.values()) {
                onResume.onResume(activity);
            }
        }
    }

    public void onPause(Activity activity) {
        synchronized (this.mSmashes) {
            for (ProgRvSmash onPause : this.mSmashes.values()) {
                onPause.onPause(activity);
            }
        }
    }

    public void setConsent(boolean z) {
        synchronized (this.mSmashes) {
            for (ProgRvSmash consent : this.mSmashes.values()) {
                consent.setConsent(z);
            }
        }
    }

    /* access modifiers changed from: private */
    public void makeAuction() {
        logInternal("makeAuction()");
        setState(RV_MEDIATION_STATE.RV_STATE_AUCTION_IN_PROGRESS);
        this.mCurrentAuctionId = "";
        this.mAuctionStartTime = new Date().getTime();
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        StringBuilder sb = new StringBuilder();
        synchronized (this.mSmashes) {
            for (ProgRvSmash progRvSmash : this.mSmashes.values()) {
                progRvSmash.unloadVideo();
                if (!this.mSessionCappingManager.isCapped(progRvSmash)) {
                    if (progRvSmash.isBidder() && progRvSmash.isReadyToBid()) {
                        Map biddingData = progRvSmash.getBiddingData();
                        if (biddingData != null) {
                            hashMap.put(progRvSmash.getInstanceName(), biddingData);
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("2");
                            sb2.append(progRvSmash.getInstanceName());
                            sb2.append(",");
                            sb.append(sb2.toString());
                        }
                    } else if (!progRvSmash.isBidder()) {
                        arrayList.add(progRvSmash.getInstanceName());
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("1");
                        sb3.append(progRvSmash.getInstanceName());
                        sb3.append(",");
                        sb.append(sb3.toString());
                    }
                }
            }
        }
        if (hashMap.keySet().size() == 0 && arrayList.size() == 0) {
            logInternal("makeAuction() failed - request waterfall is empty");
            reportAvailabilityIfNeeded(false);
            setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
            this.mRvAuctionTrigger.loadError();
            return;
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append("makeAuction() - request waterfall is: ");
        sb4.append(sb);
        logInternal(sb4.toString());
        if (sb.length() > 256) {
            sb.setLength(256);
        } else {
            sb.deleteCharAt(sb.length() - 1);
        }
        sendMediationEventWithoutAuctionId(1000);
        sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_REQUEST);
        sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_REQUEST_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
        this.mAuctionHandler.executeAuction(hashMap, arrayList, SessionDepthManager.getInstance().getSessionDepth(1), new AuctionHandlerCallback() {
            public void callback(boolean z, List<AuctionResponseItem> list, String str, int i, String str2, long j) {
                if (z) {
                    ProgRvManager.this.logInternal("makeAuction(): success");
                    ProgRvManager.this.mCurrentAuctionId = str;
                    ProgRvManager.this.sendMediationEvent(IronSourceConstants.RV_AUCTION_SUCCESS, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
                    ProgRvManager.this.updateWaterfall(list);
                    ProgRvManager.this.loadSmashes();
                    return;
                }
                ProgRvManager.this.logInternal("makeAuction(): failed");
                if (TextUtils.isEmpty(str2)) {
                    ProgRvManager.this.sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
                } else {
                    String substring = str2.substring(0, Math.min(str2.length(), 39));
                    ProgRvManager.this.sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, substring}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
                }
                ProgRvManager.this.reportAvailabilityIfNeeded(false);
                ProgRvManager.this.setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
                ProgRvManager.this.mRvAuctionTrigger.loadError();
            }
        });
    }

    private String getAsString(AuctionResponseItem auctionResponseItem) {
        String str = TextUtils.isEmpty(auctionResponseItem.getServerData()) ? "1" : "2";
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(auctionResponseItem.getInstanceName());
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public void updateWaterfall(List<AuctionResponseItem> list) {
        synchronized (this.mSmashes) {
            this.mWaterfall.clear();
            this.mWaterfallServerData.clear();
            StringBuilder sb = new StringBuilder();
            for (AuctionResponseItem auctionResponseItem : list) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(getAsString(auctionResponseItem));
                sb2.append(",");
                sb.append(sb2.toString());
                ProgRvSmash progRvSmash = (ProgRvSmash) this.mSmashes.get(auctionResponseItem.getInstanceName());
                if (progRvSmash != null) {
                    progRvSmash.setIsLoadCandidate(true);
                    this.mWaterfall.add(progRvSmash);
                    this.mWaterfallServerData.put(progRvSmash.getInstanceName(), auctionResponseItem);
                }
            }
            if (sb.length() > 256) {
                sb.setLength(256);
            } else {
                sb.deleteCharAt(sb.length() - 1);
            }
            sendMediationEvent(IronSourceConstants.RV_AUCTION_RESPONSE_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
        }
    }

    /* access modifiers changed from: private */
    public void loadSmashes() {
        synchronized (this.mSmashes) {
            setState(RV_MEDIATION_STATE.RV_STATE_LOADING_SMASHES);
            int i = 0;
            for (int i2 = 0; i2 < this.mWaterfall.size() && i < this.mMaxSmashesToLoad; i2++) {
                ProgRvSmash progRvSmash = (ProgRvSmash) this.mWaterfall.get(i2);
                if (progRvSmash.getIsLoadCandidate()) {
                    progRvSmash.loadVideo(((AuctionResponseItem) this.mWaterfallServerData.get(progRvSmash.getInstanceName())).getServerData(), this.mCurrentAuctionId);
                    i++;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void setState(RV_MEDIATION_STATE rv_mediation_state) {
        StringBuilder sb = new StringBuilder();
        sb.append("current state=");
        sb.append(this.mState);
        sb.append(", new state=");
        sb.append(rv_mediation_state);
        logInternal(sb.toString());
        this.mState = rv_mediation_state;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0086, code lost:
        return;
     */
    public synchronized void onLoadSuccess(ProgRvSmash progRvSmash, String str) {
        logSmashCallback(progRvSmash, "onLoadSuccess ");
        if (this.mState != RV_MEDIATION_STATE.RV_STATE_LOADING_SMASHES && this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
            StringBuilder sb = new StringBuilder();
            sb.append("onLoadSuccess was invoked at the wrong manager state: ");
            sb.append(this.mState);
            logErrorInternal(sb.toString());
        } else if (!str.equalsIgnoreCase(this.mCurrentAuctionId)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("onLoadSuccess was invoked with auctionId: ");
            sb2.append(str);
            sb2.append(" and the current id is ");
            sb2.append(this.mCurrentAuctionId);
            logInternal(sb2.toString());
        } else {
            reportAvailabilityIfNeeded(true);
            if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                setState(RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW);
                sendMediationEvent(1003, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(new Date().getTime() - this.mAuctionStartTime)}});
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bd, code lost:
        return;
     */
    public synchronized void onLoadError(ProgRvSmash progRvSmash, String str) {
        logSmashCallback(progRvSmash, "onLoadError ");
        if (this.mState != RV_MEDIATION_STATE.RV_STATE_LOADING_SMASHES && this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
            StringBuilder sb = new StringBuilder();
            sb.append("onLoadError was invoked at the wrong manager state: ");
            sb.append(this.mState);
            logErrorInternal(sb.toString());
        } else if (!str.equalsIgnoreCase(this.mCurrentAuctionId)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("onLoadError was invoked with auctionId:");
            sb2.append(str);
            sb2.append(" and the current id is ");
            sb2.append(this.mCurrentAuctionId);
            logInternal(sb2.toString());
        } else {
            synchronized (this.mSmashes) {
                Iterator it = this.mWaterfall.iterator();
                boolean z = false;
                boolean z2 = false;
                while (it.hasNext()) {
                    ProgRvSmash progRvSmash2 = (ProgRvSmash) it.next();
                    if (progRvSmash2.getIsLoadCandidate()) {
                        if (this.mWaterfallServerData.get(progRvSmash2.getInstanceName()) != null) {
                            progRvSmash2.loadVideo(((AuctionResponseItem) this.mWaterfallServerData.get(progRvSmash2.getInstanceName())).getServerData(), this.mCurrentAuctionId);
                            return;
                        }
                    } else if (progRvSmash2.isLoadingInProgress()) {
                        z2 = true;
                    } else if (progRvSmash2.isReadyToShow()) {
                        z = true;
                    }
                }
                if (!z && !z2) {
                    logInternal("onLoadError(): No other available smashes");
                    reportAvailabilityIfNeeded(false);
                    setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
                    this.mRvAuctionTrigger.loadError();
                }
            }
        }
    }

    public void onRewardedVideoAdOpened(ProgRvSmash progRvSmash) {
        synchronized (this) {
            logSmashCallback(progRvSmash, "onRewardedVideoAdOpened");
            RVListenerWrapper.getInstance().onRewardedVideoAdOpened();
            this.mAuctionHandler.reportImpression((AuctionResponseItem) this.mWaterfallServerData.get(progRvSmash.getInstanceName()));
        }
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError, ProgRvSmash progRvSmash) {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("onRewardedVideoAdShowFailed error=");
            sb.append(ironSourceError.getErrorMessage());
            logSmashCallback(progRvSmash, sb.toString());
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}});
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(ironSourceError);
            this.mIsShowingVideo = false;
            if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                reportAvailabilityIfNeeded(false);
            }
            this.mRvAuctionTrigger.showError();
        }
    }

    public void onRewardedVideoAdClosed(ProgRvSmash progRvSmash) {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("onRewardedVideoAdClosed, mediation state: ");
            sb.append(this.mState.name());
            logSmashCallback(progRvSmash, sb.toString());
            RVListenerWrapper.getInstance().onRewardedVideoAdClosed();
            this.mIsShowingVideo = false;
            if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                reportAvailabilityIfNeeded(false);
            }
            this.mRvAuctionTrigger.showEnd();
        }
    }

    public void onRewardedVideoAdStarted(ProgRvSmash progRvSmash) {
        synchronized (this) {
            logSmashCallback(progRvSmash, "onRewardedVideoAdStarted");
            RVListenerWrapper.getInstance().onRewardedVideoAdStarted();
        }
    }

    public void onRewardedVideoAdEnded(ProgRvSmash progRvSmash) {
        synchronized (this) {
            logSmashCallback(progRvSmash, "onRewardedVideoAdEnded");
            RVListenerWrapper.getInstance().onRewardedVideoAdEnded();
        }
    }

    public void onRewardedVideoAdRewarded(ProgRvSmash progRvSmash, Placement placement) {
        synchronized (this) {
            logSmashCallback(progRvSmash, "onRewardedVideoAdRewarded");
            RVListenerWrapper.getInstance().onRewardedVideoAdRewarded(placement);
        }
    }

    public void onRewardedVideoAdClicked(ProgRvSmash progRvSmash, Placement placement) {
        synchronized (this) {
            logSmashCallback(progRvSmash, "onRewardedVideoAdClicked");
            RVListenerWrapper.getInstance().onRewardedVideoAdClicked(placement);
        }
    }

    /* access modifiers changed from: private */
    public void reportAvailabilityIfNeeded(boolean z) {
        if (this.mLastReportedAvailabilityState == null || this.mLastReportedAvailabilityState.booleanValue() != z) {
            this.mLastReportedAvailabilityState = Boolean.valueOf(z);
            long time = new Date().getTime() - this.mLastChangedAvailabilityTime;
            this.mLastChangedAvailabilityTime = new Date().getTime();
            if (z) {
                sendMediationEvent(IronSourceConstants.RV_CALLBACK_AVAILABILITY_TRUE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
            } else {
                sendMediationEvent(IronSourceConstants.RV_CALLBACK_AVAILABILITY_FALSE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
            }
            RVListenerWrapper.getInstance().onRewardedVideoAvailabilityChanged(z);
        }
    }

    private void logSmashCallback(ProgRvSmash progRvSmash, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(progRvSmash.getInstanceName());
        sb.append(" : ");
        sb.append(str);
        String sb2 = sb.toString();
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("ProgRvManager: ");
        sb3.append(sb2);
        logger.log(ironSourceTag, sb3.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("ProgRvManager: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void logErrorInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("ProgRvManager: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 3);
    }

    private void logApi(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.API, str, 1);
    }

    /* access modifiers changed from: private */
    public void sendMediationEventWithoutAuctionId(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false, false);
    }

    private void sendMediationEventWithoutAuctionId(int i) {
        sendMediationEvent(i, null, false, false);
    }

    /* access modifiers changed from: private */
    public void sendMediationEvent(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false, true);
    }

    private void sendMediationEventWithPlacement(int i) {
        sendMediationEvent(i, null, true, true);
    }

    private void sendMediationEventWithPlacement(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, true, true);
    }

    private void sendMediationEvent(int i, Object[][] objArr, boolean z, boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_PROGRAMMATIC, Integer.valueOf(1));
        if (z2 && !TextUtils.isEmpty(this.mCurrentAuctionId)) {
            hashMap.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            hashMap.put("placement", this.mCurrentPlacement);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    hashMap.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("ProgRvManager: RV sendMediationEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    public synchronized void onAuctionTriggered() {
        StringBuilder sb = new StringBuilder();
        sb.append("onAuctionTriggered: auction was triggered in ");
        sb.append(this.mState);
        sb.append(" state");
        logInternal(sb.toString());
        makeAuction();
    }
}
