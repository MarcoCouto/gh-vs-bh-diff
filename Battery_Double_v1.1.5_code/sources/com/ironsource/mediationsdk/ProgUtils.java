package com.ironsource.mediationsdk;

import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.utils.IronSourceConstants;

public class ProgUtils {
    private static final Object lock = new Object();

    public static AbstractAdapter loadAdapter(ProviderSettings providerSettings) {
        synchronized (lock) {
            String providerTypeForReflection = providerSettings.isMultipleInstances() ? providerSettings.getProviderTypeForReflection() : providerSettings.getProviderName();
            try {
                AbstractAdapter loadedAdapterOrFetchByReflection = getLoadedAdapterOrFetchByReflection(providerTypeForReflection, providerSettings.getProviderTypeForReflection());
                if (loadedAdapterOrFetchByReflection == null) {
                    return null;
                }
                loadedAdapterOrFetchByReflection.setLogListener(IronSourceLoggerManager.getLogger());
                return loadedAdapterOrFetchByReflection;
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("loadAdapter(");
                sb.append(providerTypeForReflection);
                sb.append(") ");
                sb.append(th.getMessage());
                logErrorInternal(sb.toString());
                return null;
            }
        }
    }

    private static AbstractAdapter getLoadedAdapterOrFetchByReflection(String str, String str2) {
        try {
            AbstractAdapter existingAdapter = IronSourceObject.getInstance().getExistingAdapter(str);
            if (existingAdapter != null) {
                return existingAdapter;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("com.ironsource.adapters.");
            sb.append(str2.toLowerCase());
            sb.append(".");
            sb.append(str2);
            sb.append("Adapter");
            Class cls = Class.forName(sb.toString());
            return (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, new Class[]{String.class}).invoke(cls, new Object[]{str});
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("getLoadedAdapterOrFetchByReflection ");
            sb2.append(e.getMessage());
            logErrorInternal(sb2.toString());
            return null;
        }
    }

    private static void logErrorInternal(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, str, 3);
    }
}
