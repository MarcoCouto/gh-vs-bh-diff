package com.ironsource.mediationsdk.integration;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.adcolony.sdk.AdColonyAppOptions;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.ArrayList;
import java.util.Iterator;

public class IntegrationHelper {
    private static final String BANNER_COMPATIBILITY_VERSION = "4.3";
    private static final String SDK_COMPATIBILITY_VERSION = "4.1";
    private static final String TAG = "IntegrationHelper";

    public static void validateIntegration(Activity activity) {
        String[] strArr;
        Log.i(TAG, "Verifying Integration:");
        validatePermissions(activity);
        for (String str : new String[]{"AdColony", AdColonyAppOptions.ADMOB, "Amazon", "AppLovin", "Chartboost", "Facebook", AdColonyAppOptions.FYBER, "HyprMX", "InMobi", "Maio", "MediaBrix", AdColonyAppOptions.MOPUB, IronSourceConstants.SUPERSONIC_CONFIG_NAME, "Tapjoy", "UnityAds", "Vungle"}) {
            if (isAdapterValid(activity, str)) {
                if (str.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME)) {
                    Log.i(TAG, ">>>> IronSource - VERIFIED");
                } else {
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append(">>>> ");
                    sb.append(str);
                    sb.append(" - VERIFIED");
                    Log.i(str2, sb.toString());
                }
            } else if (str.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME)) {
                Log.e(TAG, ">>>> IronSource - NOT VERIFIED");
            } else {
                String str3 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(">>>> ");
                sb2.append(str);
                sb2.append(" - NOT VERIFIED");
                Log.e(str3, sb2.toString());
            }
        }
        validateGooglePlayServices(activity);
    }

    private static boolean isAdapterValid(Activity activity, String str) {
        boolean z = false;
        try {
            if (str.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME)) {
                Log.i(TAG, "--------------- IronSource  --------------");
            } else {
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("--------------- ");
                sb.append(str);
                sb.append(" --------------");
                Log.i(str2, sb.toString());
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("com.ironsource.adapters.");
            sb2.append(str.toLowerCase());
            sb2.append(".");
            sb2.append(str);
            sb2.append("Adapter");
            String sb3 = sb2.toString();
            IntegrationData integrationData = getIntegrationData(activity, sb3);
            if (integrationData == null || !verifyBannerAdapterVersion(integrationData)) {
                return false;
            }
            if (!str.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME) && !isAdapterVersionValid(integrationData)) {
                return false;
            }
            if (!str.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME)) {
                validateSDKVersion(sb3);
            }
            boolean isActivitiesValid = isActivitiesValid(activity, integrationData.activities);
            if (!isExternalLibsValid(integrationData.externalLibs)) {
                isActivitiesValid = false;
            }
            if (!isServicesValid(activity, integrationData.services)) {
                isActivitiesValid = false;
            }
            if (integrationData.validateWriteExternalStorage && VERSION.SDK_INT <= 18) {
                if (activity.getPackageManager().checkPermission("android.permission.WRITE_EXTERNAL_STORAGE", activity.getPackageName()) == 0) {
                    Log.i(TAG, "android.permission.WRITE_EXTERNAL_STORAGE - VERIFIED");
                } else {
                    Log.e(TAG, "android.permission.WRITE_EXTERNAL_STORAGE - MISSING");
                    return z;
                }
            }
            z = isActivitiesValid;
            return z;
        } catch (Exception e) {
            String str3 = TAG;
            StringBuilder sb4 = new StringBuilder();
            sb4.append("isAdapterValid ");
            sb4.append(str);
            Log.e(str3, sb4.toString(), e);
            return false;
        }
    }

    private static boolean isServicesValid(Activity activity, String[] strArr) {
        if (strArr == null) {
            return true;
        }
        PackageManager packageManager = activity.getPackageManager();
        Log.i(TAG, "*** Services ***");
        int length = strArr.length;
        int i = 0;
        boolean z = true;
        while (i < length) {
            String str = strArr[i];
            try {
                if (packageManager.queryIntentServices(new Intent(activity, Class.forName(str)), 65536).size() > 0) {
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(" - VERIFIED");
                    Log.i(str2, sb.toString());
                    i++;
                } else {
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(" - MISSING");
                    Log.e(str3, sb2.toString());
                    z = false;
                    i++;
                }
            } catch (ClassNotFoundException unused) {
                String str4 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(" - MISSING");
                Log.e(str4, sb3.toString());
            }
        }
        return z;
    }

    private static boolean isExternalLibsValid(ArrayList<Pair<String, String>> arrayList) {
        boolean z = true;
        if (arrayList == null) {
            return true;
        }
        Log.i(TAG, "*** External Libraries ***");
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            try {
                Class.forName((String) pair.first);
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append((String) pair.second);
                sb.append(" - VERIFIED");
                Log.i(str, sb.toString());
            } catch (ClassNotFoundException unused) {
                z = false;
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append((String) pair.second);
                sb2.append(" - MISSING");
                Log.e(str2, sb2.toString());
            }
        }
        return z;
    }

    private static boolean isActivitiesValid(Activity activity, String[] strArr) {
        if (strArr == null) {
            return true;
        }
        Log.i(TAG, "*** Activities ***");
        int length = strArr.length;
        int i = 0;
        boolean z = true;
        while (i < length) {
            String str = strArr[i];
            try {
                if (activity.getPackageManager().queryIntentActivities(new Intent(activity, Class.forName(str)), 65536).size() > 0) {
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(" - VERIFIED");
                    Log.i(str2, sb.toString());
                    i++;
                } else {
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(" - MISSING");
                    Log.e(str3, sb2.toString());
                    z = false;
                    i++;
                }
            } catch (ClassNotFoundException unused) {
                String str4 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(" - MISSING");
                Log.e(str4, sb3.toString());
            }
        }
        return z;
    }

    private static void validatePermissions(Activity activity) {
        Log.i(TAG, "*** Permissions ***");
        PackageManager packageManager = activity.getPackageManager();
        if (packageManager.checkPermission("android.permission.INTERNET", activity.getPackageName()) == 0) {
            Log.i(TAG, "android.permission.INTERNET - VERIFIED");
        } else {
            Log.e(TAG, "android.permission.INTERNET - MISSING");
        }
        if (packageManager.checkPermission("android.permission.ACCESS_NETWORK_STATE", activity.getPackageName()) == 0) {
            Log.i(TAG, "android.permission.ACCESS_NETWORK_STATE - VERIFIED");
        } else {
            Log.e(TAG, "android.permission.ACCESS_NETWORK_STATE - MISSING");
        }
    }

    private static boolean verifyBannerAdapterVersion(IntegrationData integrationData) {
        if ((!integrationData.name.equalsIgnoreCase("AppLovin") && !integrationData.name.equalsIgnoreCase(AdColonyAppOptions.ADMOB) && !integrationData.name.equalsIgnoreCase("Facebook") && !integrationData.name.equalsIgnoreCase("Amazon") && !integrationData.name.equalsIgnoreCase("InMobi") && !integrationData.name.equalsIgnoreCase("Amazon") && !integrationData.name.equalsIgnoreCase(AdColonyAppOptions.FYBER)) || integrationData.version.startsWith(BANNER_COMPATIBILITY_VERSION)) {
            return true;
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append(integrationData.name);
        sb.append(" adapter ");
        sb.append(integrationData.version);
        sb.append(" is incompatible for showing banners with SDK version ");
        sb.append(IronSourceUtils.getSDKVersion());
        sb.append(", please update your adapter to version ");
        sb.append(BANNER_COMPATIBILITY_VERSION);
        sb.append(".*");
        Log.e(str, sb.toString());
        return false;
    }

    private static boolean isAdapterVersionValid(IntegrationData integrationData) {
        if (integrationData.version.startsWith(SDK_COMPATIBILITY_VERSION) || integrationData.version.startsWith(BANNER_COMPATIBILITY_VERSION)) {
            Log.i(TAG, "Adapter - VERIFIED");
            return true;
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append(integrationData.name);
        sb.append(" adapter ");
        sb.append(integrationData.version);
        sb.append(" is incompatible with SDK version ");
        sb.append(IronSourceUtils.getSDKVersion());
        sb.append(", please update your adapter to version ");
        sb.append(SDK_COMPATIBILITY_VERSION);
        sb.append(".*");
        Log.e(str, sb.toString());
        return false;
    }

    private static IntegrationData getIntegrationData(Activity activity, String str) {
        try {
            IntegrationData integrationData = (IntegrationData) Class.forName(str).getMethod("getIntegrationData", new Class[]{Activity.class}).invoke(null, new Object[]{activity});
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Adapter ");
            sb.append(integrationData.version);
            sb.append(" - VERIFIED");
            Log.i(str2, sb.toString());
            return integrationData;
        } catch (ClassNotFoundException unused) {
            Log.e(TAG, "Adapter - MISSING");
            return null;
        } catch (Exception unused2) {
            Log.e(TAG, "Adapter version - NOT VERIFIED");
            return null;
        }
    }

    private static void validateGooglePlayServices(final Activity activity) {
        new Thread() {
            public void run() {
                try {
                    Log.w(IntegrationHelper.TAG, "--------------- Google Play Services --------------");
                    if (activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 128).metaData.containsKey("com.google.android.gms.version")) {
                        Log.i(IntegrationHelper.TAG, "Google Play Services - VERIFIED");
                        String advertiserId = IronSourceObject.getInstance().getAdvertiserId(activity);
                        if (!TextUtils.isEmpty(advertiserId)) {
                            String str = IntegrationHelper.TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("GAID is: ");
                            sb.append(advertiserId);
                            sb.append(" (use this for test devices)");
                            Log.i(str, sb.toString());
                            return;
                        }
                        return;
                    }
                    Log.e(IntegrationHelper.TAG, "Google Play Services - MISSING");
                } catch (Exception unused) {
                    Log.e(IntegrationHelper.TAG, "Google Play Services - MISSING");
                }
            }
        }.start();
    }

    private static void validateSDKVersion(String str) {
        try {
            String str2 = (String) Class.forName(str).getMethod("getAdapterSDKVersion", new Class[0]).invoke(null, new Object[0]);
            String str3 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("SDK Version - ");
            sb.append(str2);
            Log.i(str3, sb.toString());
        } catch (Exception unused) {
            Log.w("validateSDKVersion", "Unable to get SDK version");
        }
    }
}
