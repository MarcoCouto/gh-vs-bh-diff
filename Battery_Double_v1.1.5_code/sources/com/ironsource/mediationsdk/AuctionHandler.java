package com.ironsource.mediationsdk;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.facebook.places.model.PlaceFields;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AuctionHandler {
    private static final int SERVER_REQUEST_TIMEOUT = 15000;
    private String mAdUnit;
    private String mBlob;
    private Context mContext;
    private String mSessionId = IronSourceObject.getInstance().getSessionId();
    private String mUrl;

    static class AuctionHttpRequestTask extends AsyncTask<Object, Void, Boolean> {
        private String mAuctionId;
        private AuctionHandlerCallback mCallback;
        private int mErrorCode;
        private String mErrorMessage;
        private long mRequestStartTime = new Date().getTime();
        private List<AuctionResponseItem> mWaterfall;

        AuctionHttpRequestTask(AuctionHandlerCallback auctionHandlerCallback) {
            this.mCallback = auctionHandlerCallback;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x0175, code lost:
            r10.mErrorCode = 1002;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x017b, code lost:
            return java.lang.Boolean.valueOf(false);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x01ab, code lost:
            r10.mErrorCode = 1006;
            r10.mErrorMessage = "Connection timed out";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x01b7, code lost:
            return java.lang.Boolean.valueOf(false);
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0056 A[Catch:{ SocketTimeoutException -> 0x01ab, Exception -> 0x019b }] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0059 A[Catch:{ SocketTimeoutException -> 0x01ab, Exception -> 0x019b }] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00bf A[Catch:{ SocketTimeoutException -> 0x01ab, Exception -> 0x019b }] */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x017c A[Catch:{ SocketTimeoutException -> 0x01ab, Exception -> 0x019b }] */
        /* JADX WARNING: Removed duplicated region for block: B:60:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:1:0x0001] */
        public Boolean doInBackground(Object... objArr) {
            boolean z;
            int responseCode;
            try {
                Context context = objArr[0];
                URL url = new URL(objArr[1]);
                JSONObject jSONObject = objArr[2];
                Boolean bool = objArr[3];
                JSONObject jSONObject2 = (JSONObject) jSONObject.get("clientParams");
                String orGenerateOnceUniqueIdentifier = DeviceStatus.getOrGenerateOnceUniqueIdentifier(context);
                String str = IronSourceConstants.TYPE_UUID;
                String[] advertisingIdInfo = DeviceStatus.getAdvertisingIdInfo(context);
                if (advertisingIdInfo.length == 2) {
                    z = Boolean.valueOf(advertisingIdInfo[1]).booleanValue();
                    if (!TextUtils.isEmpty(advertisingIdInfo[0])) {
                        String str2 = advertisingIdInfo[0];
                        str = IronSourceConstants.TYPE_GAID;
                        orGenerateOnceUniqueIdentifier = str2;
                    }
                    jSONObject2.put("advId", orGenerateOnceUniqueIdentifier);
                    jSONObject2.put("advIdType", str);
                    jSONObject2.put(RequestParameters.isLAT, !z ? "true" : "false");
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                    httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                    httpURLConnection.setConnectTimeout(15000);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setDoOutput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String encode = IronSourceAES.encode(IronSourceUtils.KEY, jSONObject.toString());
                    StringBuilder sb = new StringBuilder();
                    sb.append("{\"request\" : \"");
                    sb.append(encode);
                    sb.append("\"}");
                    bufferedWriter.write(sb.toString());
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    responseCode = httpURLConnection.getResponseCode();
                    if (responseCode != 200) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                        StringBuilder sb2 = new StringBuilder();
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                break;
                            }
                            sb2.append(readLine);
                        }
                        String sb3 = sb2.toString();
                        if (TextUtils.isEmpty(sb3)) {
                            this.mErrorCode = 1002;
                            this.mErrorMessage = "empty response";
                            return Boolean.valueOf(false);
                        }
                        JSONObject jSONObject3 = new JSONObject(sb3);
                        if (bool.booleanValue()) {
                            jSONObject3 = new JSONObject(IronSourceAES.decode(IronSourceUtils.KEY, jSONObject3.getString(ServerResponseWrapper.RESPONSE_FIELD)));
                        }
                        this.mAuctionId = jSONObject3.getString(IronSourceConstants.EVENTS_AUCTION_ID);
                        this.mWaterfall = new ArrayList();
                        JSONArray jSONArray = jSONObject3.getJSONArray("waterfall");
                        for (int i = 0; i < jSONArray.length(); i++) {
                            AuctionResponseItem auctionResponseItem = new AuctionResponseItem(jSONArray.getJSONObject(i));
                            if (!auctionResponseItem.isValid()) {
                                this.mErrorCode = 1002;
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("waterfall ");
                                sb4.append(i);
                                this.mErrorMessage = sb4.toString();
                                return Boolean.valueOf(false);
                            }
                            this.mWaterfall.add(auctionResponseItem);
                        }
                        if (this.mWaterfall.size() == 0) {
                            this.mErrorCode = 1004;
                            return Boolean.valueOf(false);
                        }
                        httpURLConnection.disconnect();
                        return Boolean.valueOf(true);
                    }
                    this.mErrorCode = 1001;
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(responseCode);
                    sb5.append("");
                    this.mErrorMessage = sb5.toString();
                    httpURLConnection.disconnect();
                    return Boolean.valueOf(false);
                }
                z = false;
                jSONObject2.put("advId", orGenerateOnceUniqueIdentifier);
                jSONObject2.put("advIdType", str);
                jSONObject2.put(RequestParameters.isLAT, !z ? "true" : "false");
                HttpURLConnection httpURLConnection2 = (HttpURLConnection) url.openConnection();
                httpURLConnection2.setRequestMethod(HttpRequest.METHOD_POST);
                httpURLConnection2.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                httpURLConnection2.setConnectTimeout(15000);
                httpURLConnection2.setDoInput(true);
                httpURLConnection2.setDoOutput(true);
                OutputStream outputStream2 = httpURLConnection2.getOutputStream();
                BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(outputStream2, "UTF-8"));
                String encode2 = IronSourceAES.encode(IronSourceUtils.KEY, jSONObject.toString());
                StringBuilder sb6 = new StringBuilder();
                sb6.append("{\"request\" : \"");
                sb6.append(encode2);
                sb6.append("\"}");
                bufferedWriter2.write(sb6.toString());
                bufferedWriter2.flush();
                bufferedWriter2.close();
                outputStream2.close();
                responseCode = httpURLConnection2.getResponseCode();
                if (responseCode != 200) {
                }
            } catch (SocketTimeoutException unused) {
            } catch (Exception e) {
                this.mErrorCode = 1000;
                this.mErrorMessage = e.getMessage();
                return Boolean.valueOf(false);
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            long time = new Date().getTime() - this.mRequestStartTime;
            if (bool.booleanValue()) {
                this.mCallback.callback(true, this.mWaterfall, this.mAuctionId, 0, null, time);
            } else {
                this.mCallback.callback(false, null, null, this.mErrorCode, this.mErrorMessage, time);
            }
        }
    }

    static class ImpressionHttpTask extends AsyncTask<String, Void, Boolean> {
        ImpressionHttpTask() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(strArr[0]).openConnection();
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.setReadTimeout(15000);
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                httpURLConnection.disconnect();
                return Boolean.valueOf(responseCode == 200);
            } catch (Exception unused) {
                return Boolean.valueOf(false);
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
        }
    }

    public AuctionHandler(Context context, String str, String str2, String str3) {
        this.mContext = context;
        this.mAdUnit = str;
        this.mBlob = str2;
        this.mUrl = str3;
    }

    public void executeAuction(Map<String, Object> map, List<String> list, int i, AuctionHandlerCallback auctionHandlerCallback) {
        try {
            boolean z = IronSourceUtils.getSerr() == 1;
            new AuctionHttpRequestTask(auctionHandlerCallback).execute(new Object[]{this.mContext, this.mUrl, generateRequest(this.mContext, map, list, i, z), Boolean.valueOf(z)});
        } catch (Exception e) {
            auctionHandlerCallback.callback(false, null, null, 1000, e.getMessage(), 0);
        }
    }

    public void reportImpression(AuctionResponseItem auctionResponseItem) {
        for (String str : auctionResponseItem.getWinUrls()) {
            new ImpressionHttpTask().execute(new String[]{str});
        }
    }

    private JSONObject generateRequest(Context context, Map<String, Object> map, List<String> list, int i, boolean z) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        for (String str : map.keySet()) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, 2);
            jSONObject2.put("biddingAdditionalData", new JSONObject((Map) map.get(str)));
            jSONObject.put(str, jSONObject2);
        }
        for (String str2 : list) {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, 1);
            jSONObject.put(str2, jSONObject3);
        }
        JSONObject jSONObject4 = new JSONObject();
        jSONObject4.put(RequestParameters.APPLICATION_USER_ID, IronSourceObject.getInstance().getIronSourceUserId());
        String gender = IronSourceObject.getInstance().getGender();
        if (TextUtils.isEmpty(gender)) {
            gender = "unknown";
        }
        jSONObject4.put("applicationUserGender", gender);
        Integer age = IronSourceObject.getInstance().getAge();
        if (age == null) {
            age = Integer.valueOf(-1);
        }
        jSONObject4.put("applicationUserAge", age);
        Boolean consent = IronSourceObject.getInstance().getConsent();
        if (consent != null) {
            jSONObject4.put(RequestParameters.CONSENT, consent.booleanValue() ? 1 : 0);
        }
        jSONObject4.put(RequestParameters.MOBILE_CARRIER, getMobileCarrier(context));
        jSONObject4.put(RequestParameters.CONNECTION_TYPE, IronSourceUtils.getConnectionType(context));
        jSONObject4.put("deviceOS", "android");
        jSONObject4.put("deviceWidth", context.getResources().getConfiguration().screenWidthDp);
        jSONObject4.put("deviceHeight", context.getResources().getConfiguration().screenHeightDp);
        String str3 = RequestParameters.DEVICE_OS_VERSION;
        StringBuilder sb = new StringBuilder();
        sb.append(VERSION.SDK_INT);
        sb.append("(");
        sb.append(VERSION.RELEASE);
        sb.append(")");
        jSONObject4.put(str3, sb.toString());
        jSONObject4.put(RequestParameters.DEVICE_MODEL, Build.MODEL);
        jSONObject4.put("deviceMake", Build.MANUFACTURER);
        jSONObject4.put(RequestParameters.PACKAGE_NAME, context.getPackageName());
        jSONObject4.put(RequestParameters.APPLICATION_VERSION_NAME, ApplicationContext.getPublisherApplicationVersion(context, context.getPackageName()));
        jSONObject4.put("clientTimestamp", new Date().getTime());
        JSONObject jSONObject5 = new JSONObject();
        jSONObject5.put("adUnit", this.mAdUnit);
        jSONObject5.put("auctionData", this.mBlob);
        jSONObject5.put(RequestParameters.APPLICATION_KEY, IronSourceObject.getInstance().getIronSourceAppKey());
        jSONObject5.put(RequestParameters.SDK_VERSION, IronSourceUtils.getSDKVersion());
        jSONObject5.put("clientParams", jSONObject4);
        jSONObject5.put(RequestParameters.SESSION_DEPTH, i);
        jSONObject5.put("sessionId", this.mSessionId);
        jSONObject5.put("doNotEncryptResponse", z ? "false" : "true");
        jSONObject5.put("instances", jSONObject);
        return jSONObject5;
    }

    private String getMobileCarrier(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                return telephonyManager.getNetworkOperatorName();
            }
        } catch (Exception unused) {
        }
        return "";
    }
}
