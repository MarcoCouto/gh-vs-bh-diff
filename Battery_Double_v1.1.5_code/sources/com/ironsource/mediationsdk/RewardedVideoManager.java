package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.environment.NetworkStateReceiver;
import com.ironsource.environment.NetworkStateReceiver.NetworkStateReceiverListener;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.RewardedVideoManagerListener;
import com.ironsource.mediationsdk.server.Server;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.DailyCappingListener;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

class RewardedVideoManager extends AbstractAdUnitManager implements RewardedVideoManagerListener, NetworkStateReceiverListener, DailyCappingListener {
    private final String TAG;
    private Placement mCurrentPlacement;
    private boolean mIsUltraEventsEnabled;
    private ListenersWrapper mListenersWrapper;
    private long mLoadStartTime;
    private int mManualLoadInterval;
    private NetworkStateReceiver mNetworkStateReceiver;
    private boolean mPauseSmartLoadDueToNetworkUnavailability;
    private boolean mShouldSendMediationLoadSuccessEvent;
    private List<MEDIATION_STATE> mStatesToIgnore;
    private Timer mTimer;

    RewardedVideoManager() {
        this.TAG = getClass().getSimpleName();
        this.mTimer = null;
        this.mPauseSmartLoadDueToNetworkUnavailability = false;
        this.mIsUltraEventsEnabled = false;
        this.mShouldSendMediationLoadSuccessEvent = false;
        this.mLoadStartTime = new Date().getTime();
        this.mStatesToIgnore = Arrays.asList(new MEDIATION_STATE[]{MEDIATION_STATE.INIT_FAILED, MEDIATION_STATE.CAPPED_PER_SESSION, MEDIATION_STATE.EXHAUSTED, MEDIATION_STATE.CAPPED_PER_DAY});
        this.mDailyCappingManager = new DailyCappingManager("rewarded_video", this);
    }

    public void setRewardedVideoListener(ListenersWrapper listenersWrapper) {
        this.mListenersWrapper = listenersWrapper;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00b7, code lost:
        return;
     */
    public synchronized void initRewardedVideo(Activity activity, String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(":initRewardedVideo(appKey: ");
        sb.append(str);
        sb.append(", userId: ");
        sb.append(str2);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        this.mAppKey = str;
        this.mUserId = str2;
        this.mActivity = activity;
        this.mDailyCappingManager.setContext(this.mActivity);
        Iterator it = this.mSmashArray.iterator();
        int i = 0;
        int i2 = 0;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (this.mDailyCappingManager.shouldSendCapReleasedEvent(abstractSmash)) {
                logProviderEvent(IronSourceConstants.REWARDED_VIDEO_DAILY_CAPPED, abstractSmash, new Object[][]{new Object[]{"status", "false"}});
            }
            if (this.mDailyCappingManager.isCapped(abstractSmash)) {
                abstractSmash.setMediationState(MEDIATION_STATE.CAPPED_PER_DAY);
                i2++;
            }
        }
        if (i2 == this.mSmashArray.size()) {
            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(false);
            return;
        }
        logMediationEvent(1000);
        this.mListenersWrapper.setRvPlacement(null);
        this.mShouldSendMediationLoadSuccessEvent = true;
        this.mLoadStartTime = new Date().getTime();
        while (i < this.mSmartLoadAmount && i < this.mSmashArray.size() && loadNextAdapter() != null) {
            i++;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f9, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x016f, code lost:
        return;
     */
    public synchronized void showRewardedVideo(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(":showRewardedVideo(placementName: ");
        sb.append(str);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        this.mListenersWrapper.setRvPlacement(str);
        logMediationEvent(IronSourceConstants.RV_API_SHOW_CALLED, new Object[][]{new Object[]{"placement", str}});
        if (!IronSourceUtils.isNetworkConnected(this.mActivity)) {
            this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildNoInternetConnectionShowFailError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            return;
        }
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < this.mSmashArray.size(); i3++) {
            AbstractSmash abstractSmash = (AbstractSmash) this.mSmashArray.get(i3);
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceTag ironSourceTag2 = IronSourceTag.INTERNAL;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("showRewardedVideo, iterating on: ");
            sb2.append(abstractSmash.getInstanceName());
            sb2.append(", Status: ");
            sb2.append(abstractSmash.getMediationState());
            ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 0);
            if (abstractSmash.getMediationState() != MEDIATION_STATE.AVAILABLE) {
                if (abstractSmash.getMediationState() != MEDIATION_STATE.CAPPED_PER_SESSION) {
                    if (abstractSmash.getMediationState() != MEDIATION_STATE.CAPPED_PER_DAY) {
                        if (abstractSmash.getMediationState() == MEDIATION_STATE.NOT_AVAILABLE) {
                            i2++;
                        }
                    }
                }
                i++;
            } else if (((RewardedVideoSmash) abstractSmash).isRewardedVideoAvailable()) {
                showAdapter(abstractSmash, i3);
                if (this.mCanShowPremium && !abstractSmash.equals(getPremiumSmash())) {
                    disablePremiumForCurrentSession();
                }
                if (abstractSmash.isCappedPerSession()) {
                    abstractSmash.setMediationState(MEDIATION_STATE.CAPPED_PER_SESSION);
                    logProviderEvent(IronSourceConstants.RV_CAP_SESSION, abstractSmash, null);
                    completeAdapterCap();
                } else if (this.mDailyCappingManager.isCapped(abstractSmash)) {
                    abstractSmash.setMediationState(MEDIATION_STATE.CAPPED_PER_DAY);
                    logProviderEvent(IronSourceConstants.REWARDED_VIDEO_DAILY_CAPPED, abstractSmash, new Object[][]{new Object[]{"status", "true"}});
                    completeAdapterCap();
                } else if (abstractSmash.isExhausted()) {
                    loadNextAdapter();
                    completeIterationRound();
                }
            } else {
                onRewardedVideoAvailabilityChanged(false, (RewardedVideoSmash) abstractSmash);
                Exception exc = new Exception("FailedToShowVideoException");
                IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
                IronSourceTag ironSourceTag3 = IronSourceTag.INTERNAL;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(abstractSmash.getInstanceName());
                sb3.append(" Failed to show video");
                ironSourceLoggerManager3.logException(ironSourceTag3, sb3.toString(), exc);
            }
        }
        if (isBackFillAvailable()) {
            showAdapter(getBackfillSmash(), this.mSmashArray.size());
        } else if (i + i2 == this.mSmashArray.size()) {
            this.mListenersWrapper.onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
        }
    }

    public synchronized boolean isRewardedVideoAvailable() {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.API;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(":isRewardedVideoAvailable()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (this.mPauseSmartLoadDueToNetworkUnavailability) {
            return false;
        }
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.isMediationAvailable() && ((RewardedVideoSmash) abstractSmash).isRewardedVideoAvailable()) {
                return true;
            }
        }
        return false;
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError, RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(":onRewardedVideoAdShowFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_FAILED, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}, new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}});
        sendMediationLoadEvents();
        this.mListenersWrapper.onRewardedVideoAdShowFailed(ironSourceError);
    }

    public void onRewardedVideoAdOpened(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(":onRewardedVideoAdOpened()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEvent(1005, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        this.mListenersWrapper.onRewardedVideoAdOpened();
    }

    public void onRewardedVideoAdClosed(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(":onRewardedVideoAdClosed()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        verifyOnPauseOnResume();
        logProviderEvent(IronSourceConstants.RV_INSTANCE_CLOSED, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        if (!rewardedVideoSmash.isCappedPerSession() && !this.mDailyCappingManager.isCapped(rewardedVideoSmash)) {
            logProviderEvent(1001, rewardedVideoSmash, null);
        }
        sendMediationLoadEvents();
        this.mListenersWrapper.onRewardedVideoAdClosed();
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceTag ironSourceTag2 = IronSourceTag.INTERNAL;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Fetch on ad closed, iterating on: ");
            sb2.append(abstractSmash.getInstanceName());
            sb2.append(", Status: ");
            sb2.append(abstractSmash.getMediationState());
            ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 0);
            if (abstractSmash.getMediationState() == MEDIATION_STATE.NOT_AVAILABLE) {
                try {
                    if (!abstractSmash.getInstanceName().equals(rewardedVideoSmash.getInstanceName())) {
                        IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
                        IronSourceTag ironSourceTag3 = IronSourceTag.INTERNAL;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(abstractSmash.getInstanceName());
                        sb3.append(":reload smash");
                        ironSourceLoggerManager3.log(ironSourceTag3, sb3.toString(), 1);
                        ((RewardedVideoSmash) abstractSmash).fetchRewardedVideo();
                        logProviderEvent(1001, abstractSmash, null);
                    }
                } catch (Throwable th) {
                    IronSourceLoggerManager ironSourceLoggerManager4 = this.mLoggerManager;
                    IronSourceTag ironSourceTag4 = IronSourceTag.NATIVE;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(abstractSmash.getInstanceName());
                    sb4.append(" Failed to call fetchVideo(), ");
                    sb4.append(th.getLocalizedMessage());
                    ironSourceLoggerManager4.log(ironSourceTag4, sb4.toString(), 1);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0074, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00bf, code lost:
        return;
     */
    public synchronized void onRewardedVideoAvailabilityChanged(boolean z, RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(": onRewardedVideoAvailabilityChanged(available:");
        sb.append(z);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (!this.mPauseSmartLoadDueToNetworkUnavailability) {
            if (z) {
                if (this.mShouldSendMediationLoadSuccessEvent) {
                    this.mShouldSendMediationLoadSuccessEvent = false;
                    logMediationEvent(1003, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(new Date().getTime() - this.mLoadStartTime)}});
                }
            }
            try {
                if (!rewardedVideoSmash.equals(getBackfillSmash())) {
                    if (rewardedVideoSmash.equals(getPremiumSmash())) {
                        IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                        IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_CALLBACK;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(rewardedVideoSmash.getInstanceName());
                        sb2.append(" is a premium adapter, canShowPremium: ");
                        sb2.append(canShowPremium());
                        ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 1);
                        if (!canShowPremium()) {
                            rewardedVideoSmash.setMediationState(MEDIATION_STATE.CAPPED_PER_SESSION);
                            if (shouldNotifyAvailabilityChanged(false)) {
                                this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
                            }
                        }
                    }
                    if (rewardedVideoSmash.isMediationAvailable() && !this.mDailyCappingManager.isCapped(rewardedVideoSmash)) {
                        if (!z) {
                            if (shouldNotifyAvailabilityChanged(false)) {
                                notifyAvailabilityChange();
                            }
                            loadNextAdapter();
                            completeIterationRound();
                        } else if (shouldNotifyAvailabilityChanged(true)) {
                            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
                        }
                    }
                } else if (shouldNotifyAvailabilityChanged(z)) {
                    this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
                }
            } catch (Throwable th) {
                IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
                IronSourceTag ironSourceTag3 = IronSourceTag.ADAPTER_CALLBACK;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("onRewardedVideoAvailabilityChanged(available:");
                sb3.append(z);
                sb3.append(", ");
                sb3.append("provider:");
                sb3.append(rewardedVideoSmash.getName());
                sb3.append(")");
                ironSourceLoggerManager3.logException(ironSourceTag3, sb3.toString(), th);
            }
        } else {
            return;
        }
        return;
    }

    public void onRewardedVideoAdStarted(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(":onRewardedVideoAdStarted()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEvent(IronSourceConstants.RV_INSTANCE_STARTED, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        this.mListenersWrapper.onRewardedVideoAdStarted();
    }

    public void onRewardedVideoAdEnded(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(":onRewardedVideoAdEnded()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEvent(IronSourceConstants.RV_INSTANCE_ENDED, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        this.mListenersWrapper.onRewardedVideoAdEnded();
    }

    public void onRewardedVideoAdRewarded(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(":onRewardedVideoAdRewarded()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (this.mCurrentPlacement == null) {
            this.mCurrentPlacement = IronSourceObject.getInstance().getCurrentServerResponse().getConfigurations().getRewardedVideoConfigurations().getDefaultRewardedVideoPlacement();
        }
        JSONObject providerAdditionalData = IronSourceUtils.getProviderAdditionalData((AbstractSmash) rewardedVideoSmash);
        try {
            if (this.mCurrentPlacement != null) {
                providerAdditionalData.put("placement", this.mCurrentPlacement.getPlacementName());
                providerAdditionalData.put(IronSourceConstants.EVENTS_REWARD_NAME, this.mCurrentPlacement.getRewardName());
                providerAdditionalData.put(IronSourceConstants.EVENTS_REWARD_AMOUNT, this.mCurrentPlacement.getRewardAmount());
            } else {
                this.mLoggerManager.log(IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        EventData eventData = new EventData(1010, providerAdditionalData);
        if (!TextUtils.isEmpty(this.mAppKey)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(Long.toString(eventData.getTimeStamp()));
            sb2.append(this.mAppKey);
            sb2.append(rewardedVideoSmash.getName());
            eventData.addToAdditionalData(IronSourceConstants.EVENTS_TRANS_ID, IronSourceUtils.getTransId(sb2.toString()));
            if (!TextUtils.isEmpty(IronSourceObject.getInstance().getDynamicUserId())) {
                eventData.addToAdditionalData(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceObject.getInstance().getDynamicUserId());
            }
            Map rvServerParams = IronSourceObject.getInstance().getRvServerParams();
            if (rvServerParams != null) {
                for (String str : rvServerParams.keySet()) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("custom_");
                    sb3.append(str);
                    eventData.addToAdditionalData(sb3.toString(), rvServerParams.get(str));
                }
            }
        }
        RewardedVideoEventsManager.getInstance().log(eventData);
        if (this.mCurrentPlacement != null) {
            this.mListenersWrapper.onRewardedVideoAdRewarded(this.mCurrentPlacement);
        } else {
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
        }
    }

    public void onRewardedVideoAdClicked(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(":onRewardedVideoAdClicked()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (this.mCurrentPlacement == null) {
            this.mCurrentPlacement = IronSourceObject.getInstance().getCurrentServerResponse().getConfigurations().getRewardedVideoConfigurations().getDefaultRewardedVideoPlacement();
        }
        if (this.mCurrentPlacement == null) {
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
            return;
        }
        logProviderEvent(1006, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        this.mListenersWrapper.onRewardedVideoAdClicked(this.mCurrentPlacement);
    }

    public void onRewardedVideoAdVisible(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(":onRewardedVideoAdVisible()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (this.mCurrentPlacement != null) {
            logProviderEvent(IronSourceConstants.RV_INSTANCE_VISIBLE, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
            return;
        }
        this.mLoggerManager.log(IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
    }

    public void onNetworkAvailabilityChanged(boolean z) {
        if (this.mShouldTrackNetworkState) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("Network Availability Changed To: ");
            sb.append(z);
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
            if (shouldNotifyNetworkAvailabilityChanged(z)) {
                this.mPauseSmartLoadDueToNetworkUnavailability = !z;
                this.mListenersWrapper.onRewardedVideoAvailabilityChanged(z);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void shouldTrackNetworkState(Context context, boolean z) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(" Should Track Network State: ");
        sb.append(z);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
        this.mShouldTrackNetworkState = z;
        if (this.mShouldTrackNetworkState) {
            if (this.mNetworkStateReceiver == null) {
                this.mNetworkStateReceiver = new NetworkStateReceiver(context, this);
            }
            context.getApplicationContext().registerReceiver(this.mNetworkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } else if (this.mNetworkStateReceiver != null) {
            context.getApplicationContext().unregisterReceiver(this.mNetworkStateReceiver);
        }
    }

    private boolean shouldNotifyNetworkAvailabilityChanged(boolean z) {
        if (this.mLastMediationAvailabilityState == null) {
            return false;
        }
        boolean z2 = true;
        if (z && !this.mLastMediationAvailabilityState.booleanValue() && hasAvailableSmash()) {
            this.mLastMediationAvailabilityState = Boolean.valueOf(true);
        } else if (z || !this.mLastMediationAvailabilityState.booleanValue()) {
            z2 = false;
        } else {
            this.mLastMediationAvailabilityState = Boolean.valueOf(false);
        }
        return z2;
    }

    /* access modifiers changed from: 0000 */
    public void setIsUltraEventsEnabled(boolean z) {
        this.mIsUltraEventsEnabled = z;
    }

    private void reportFalseImpressionsOnHigherPriority(int i, int i2) {
        int i3 = 0;
        while (i3 < i && i3 < this.mSmashArray.size()) {
            if (!this.mStatesToIgnore.contains(((AbstractSmash) this.mSmashArray.get(i3)).getMediationState())) {
                reportImpression(((RewardedVideoSmash) this.mSmashArray.get(i3)).getRequestUrl(), false, i2);
            }
            i3++;
        }
    }

    private synchronized void reportImpression(String str, boolean z, int i) {
        Throwable th;
        String str2;
        String str3 = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(str3);
            sb.append(str);
            str2 = sb.toString();
            try {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str2);
                sb2.append("&sdkVersion=");
                sb2.append(IronSourceUtils.getSDKVersion());
                str3 = sb2.toString();
                Server.callAsyncRequestURL(str3, z, i);
            } catch (Throwable th2) {
                th = th2;
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.NETWORK;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("reportImpression:(providerURL:");
                sb3.append(str2);
                sb3.append(", ");
                sb3.append("hit:");
                sb3.append(z);
                sb3.append(")");
                ironSourceLoggerManager.logException(ironSourceTag, sb3.toString(), th);
            }
        } catch (Throwable th3) {
            th = th3;
            str2 = str3;
        }
    }

    /* access modifiers changed from: 0000 */
    public void setCurrentPlacement(Placement placement) {
        this.mCurrentPlacement = placement;
        this.mListenersWrapper.setRvPlacement(placement.getPlacementName());
    }

    /* access modifiers changed from: protected */
    public synchronized void disablePremiumForCurrentSession() {
        super.disablePremiumForCurrentSession();
        Iterator it = this.mSmashArray.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.equals(getPremiumSmash())) {
                abstractSmash.setMediationState(MEDIATION_STATE.CAPPED_PER_SESSION);
                loadNextAdapter();
                break;
            }
        }
    }

    private synchronized AbstractAdapter startAdapter(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(":startAdapter(");
        sb.append(rewardedVideoSmash.getInstanceName());
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        try {
            AbstractAdapter loadedAdapterOrFetchByReflection = getLoadedAdapterOrFetchByReflection(rewardedVideoSmash);
            if (loadedAdapterOrFetchByReflection == null) {
                return null;
            }
            IronSourceObject.getInstance().addToRVAdaptersList(loadedAdapterOrFetchByReflection);
            loadedAdapterOrFetchByReflection.setLogListener(this.mLoggerManager);
            rewardedVideoSmash.setAdapterForSmash(loadedAdapterOrFetchByReflection);
            rewardedVideoSmash.setMediationState(MEDIATION_STATE.INITIATED);
            setCustomParams(rewardedVideoSmash);
            logProviderEvent(1001, rewardedVideoSmash, null);
            rewardedVideoSmash.initRewardedVideo(this.mActivity, this.mAppKey, this.mUserId);
            return loadedAdapterOrFetchByReflection;
        } catch (Throwable th) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceTag ironSourceTag2 = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":startAdapter(");
            sb2.append(rewardedVideoSmash.getName());
            sb2.append(")");
            ironSourceLoggerManager2.logException(ironSourceTag2, sb2.toString(), th);
            rewardedVideoSmash.setMediationState(MEDIATION_STATE.INIT_FAILED);
            if (shouldNotifyAvailabilityChanged(false)) {
                this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append(rewardedVideoSmash.getName());
            sb3.append(" initialization failed - please verify that required dependencies are in you build path.");
            this.mLoggerManager.log(IronSourceTag.API, ErrorBuilder.buildInitFailedError(sb3.toString(), IronSourceConstants.REWARDED_VIDEO_AD_UNIT).toString(), 2);
            return null;
        }
    }

    private AbstractAdapter loadNextAdapter() {
        AbstractAdapter abstractAdapter = null;
        int i = 0;
        for (int i2 = 0; i2 < this.mSmashArray.size() && abstractAdapter == null; i2++) {
            if (((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == MEDIATION_STATE.AVAILABLE || ((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == MEDIATION_STATE.INITIATED) {
                i++;
                if (i >= this.mSmartLoadAmount) {
                    break;
                }
            } else if (((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == MEDIATION_STATE.NOT_INITIATED) {
                abstractAdapter = startAdapter((RewardedVideoSmash) this.mSmashArray.get(i2));
                if (abstractAdapter == null) {
                    ((AbstractSmash) this.mSmashArray.get(i2)).setMediationState(MEDIATION_STATE.INIT_FAILED);
                }
            }
        }
        return abstractAdapter;
    }

    private synchronized void showAdapter(AbstractSmash abstractSmash, int i) {
        Object[][] objArr;
        CappingManager.incrementShowCounter((Context) this.mActivity, this.mCurrentPlacement);
        if (CappingManager.isRvPlacementCapped(this.mActivity, this.mCurrentPlacement.getPlacementName())) {
            logMediationEvent(IronSourceConstants.RV_CAP_PLACEMENT, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        }
        this.mDailyCappingManager.increaseShowCounter(abstractSmash);
        if (this.mCurrentPlacement != null) {
            if (this.mIsUltraEventsEnabled) {
                reportImpression(((RewardedVideoSmash) abstractSmash).getRequestUrl(), true, this.mCurrentPlacement.getPlacementId());
                reportFalseImpressionsOnHigherPriority(i, this.mCurrentPlacement.getPlacementId());
            }
            sendShowChanceEvents(abstractSmash, i, this.mCurrentPlacement.getPlacementName());
        } else {
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
        }
        if (this.mCurrentPlacement != null) {
            objArr = new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}};
        } else {
            objArr = null;
        }
        logProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW, abstractSmash, objArr);
        ((RewardedVideoSmash) abstractSmash).showRewardedVideo();
    }

    /* access modifiers changed from: 0000 */
    public void setManualLoadInterval(int i) {
        this.mManualLoadInterval = i;
    }

    /* access modifiers changed from: private */
    public void scheduleFetchTimer() {
        if (this.mManualLoadInterval <= 0) {
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "load interval is not set, ignoring", 1);
            return;
        }
        if (this.mTimer != null) {
            this.mTimer.cancel();
        }
        this.mTimer = new Timer();
        this.mTimer.schedule(new TimerTask() {
            public void run() {
                cancel();
                RewardedVideoManager.this.loadRewardedVideo();
                RewardedVideoManager.this.scheduleFetchTimer();
            }
        }, (long) (this.mManualLoadInterval * 1000));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0094, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0096, code lost:
        return;
     */
    public synchronized void loadRewardedVideo() {
        if (IronSourceUtils.isNetworkConnected(this.mActivity)) {
            if (this.mLastMediationAvailabilityState != null) {
                if (!this.mLastMediationAvailabilityState.booleanValue()) {
                    logMediationEvent(102);
                    logMediationEvent(1000);
                    this.mShouldSendMediationLoadSuccessEvent = true;
                    Iterator it = this.mSmashArray.iterator();
                    while (it.hasNext()) {
                        AbstractSmash abstractSmash = (AbstractSmash) it.next();
                        if (abstractSmash.getMediationState() == MEDIATION_STATE.NOT_AVAILABLE) {
                            try {
                                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Fetch from timer: ");
                                sb.append(abstractSmash.getInstanceName());
                                sb.append(":reload smash");
                                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
                                logProviderEvent(1001, abstractSmash, null);
                                ((RewardedVideoSmash) abstractSmash).fetchRewardedVideo();
                            } catch (Throwable th) {
                                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                                IronSourceTag ironSourceTag2 = IronSourceTag.NATIVE;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(abstractSmash.getInstanceName());
                                sb2.append(" Failed to call fetchVideo(), ");
                                sb2.append(th.getLocalizedMessage());
                                ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 1);
                            }
                        }
                    }
                }
            }
        }
    }

    private synchronized boolean shouldNotifyAvailabilityChanged(boolean z) {
        boolean z2;
        z2 = true;
        if (this.mLastMediationAvailabilityState == null) {
            scheduleFetchTimer();
            if (z) {
                this.mLastMediationAvailabilityState = Boolean.valueOf(true);
            } else if (!isBackFillAvailable() && isAllAdaptersInactive()) {
                this.mLastMediationAvailabilityState = Boolean.valueOf(false);
            }
        } else if (z && !this.mLastMediationAvailabilityState.booleanValue()) {
            this.mLastMediationAvailabilityState = Boolean.valueOf(true);
        } else if (!z && this.mLastMediationAvailabilityState.booleanValue() && !hasAvailableSmash() && !isBackFillAvailable()) {
            this.mLastMediationAvailabilityState = Boolean.valueOf(false);
        }
        z2 = false;
        return z2;
    }

    private synchronized boolean isAllAdaptersInactive() {
        boolean z;
        Iterator it = this.mSmashArray.iterator();
        z = false;
        int i = 0;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == MEDIATION_STATE.INIT_FAILED || abstractSmash.getMediationState() == MEDIATION_STATE.CAPPED_PER_DAY || abstractSmash.getMediationState() == MEDIATION_STATE.CAPPED_PER_SESSION || abstractSmash.getMediationState() == MEDIATION_STATE.NOT_AVAILABLE || abstractSmash.getMediationState() == MEDIATION_STATE.EXHAUSTED) {
                i++;
            }
        }
        if (this.mSmashArray.size() == i) {
            z = true;
        }
        return z;
    }

    private synchronized boolean isAvailableAdaptersToLoad() {
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == MEDIATION_STATE.NOT_AVAILABLE || abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE || abstractSmash.getMediationState() == MEDIATION_STATE.INITIATED || abstractSmash.getMediationState() == MEDIATION_STATE.INIT_PENDING) {
                return true;
            }
            if (abstractSmash.getMediationState() == MEDIATION_STATE.LOAD_PENDING) {
                return true;
            }
        }
        return false;
    }

    private synchronized boolean hasAvailableSmash() {
        boolean z;
        z = false;
        Iterator it = this.mSmashArray.iterator();
        while (true) {
            if (it.hasNext()) {
                if (((AbstractSmash) it.next()).getMediationState() == MEDIATION_STATE.AVAILABLE) {
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        return z;
    }

    private synchronized boolean isBackFillAvailable() {
        if (getBackfillSmash() == null) {
            return false;
        }
        return ((RewardedVideoSmash) getBackfillSmash()).isRewardedVideoAvailable();
    }

    private void sendShowChanceEvents(AbstractSmash abstractSmash, int i, String str) {
        logProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_CHANCE, abstractSmash, new Object[][]{new Object[]{"placement", str}, new Object[]{"status", "true"}});
        int i2 = 0;
        while (i2 < this.mSmashArray.size() && i2 < i) {
            AbstractSmash abstractSmash2 = (AbstractSmash) this.mSmashArray.get(i2);
            if (abstractSmash2.getMediationState() == MEDIATION_STATE.NOT_AVAILABLE) {
                logProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_CHANCE, abstractSmash2, new Object[][]{new Object[]{"placement", str}, new Object[]{"status", "false"}});
            }
            i2++;
        }
    }

    private synchronized void notifyAvailabilityChange() {
        if (getBackfillSmash() != null && !this.mBackFillInitStarted) {
            this.mBackFillInitStarted = true;
            if (startAdapter((RewardedVideoSmash) getBackfillSmash()) == null) {
                this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
            }
        } else if (!isBackFillAvailable()) {
            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
        } else if (shouldNotifyAvailabilityChanged(true)) {
            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        return;
     */
    private synchronized void completeAdapterCap() {
        if (loadNextAdapter() == null) {
            if (smashesCount(MEDIATION_STATE.NOT_AVAILABLE, MEDIATION_STATE.CAPPED_PER_SESSION, MEDIATION_STATE.CAPPED_PER_DAY) < this.mSmashArray.size()) {
                completeIterationRound();
            } else if (shouldNotifyAvailabilityChanged(false)) {
                notifyAvailabilityChange();
            }
        }
    }

    private synchronized void completeIterationRound() {
        if (isIterationRoundComplete()) {
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "Reset Iteration", 0);
            Iterator it = this.mSmashArray.iterator();
            boolean z = false;
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                if (abstractSmash.getMediationState() == MEDIATION_STATE.EXHAUSTED) {
                    abstractSmash.completeIteration();
                }
                if (abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE) {
                    z = true;
                }
            }
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "End of Reset Iteration", 0);
            if (shouldNotifyAvailabilityChanged(z)) {
                this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
            }
        }
    }

    private synchronized boolean isIterationRoundComplete() {
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == MEDIATION_STATE.NOT_INITIATED || abstractSmash.getMediationState() == MEDIATION_STATE.INITIATED) {
                return false;
            }
            if (abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE) {
                return false;
            }
        }
        return true;
    }

    private void logMediationEvent(int i) {
        logMediationEvent(i, null);
    }

    private void logMediationEvent(int i, Object[][] objArr) {
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    mediationAdditionalData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("RewardedVideoManager logMediationEvent ");
                sb.append(Log.getStackTraceString(e));
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, mediationAdditionalData));
    }

    private void logProviderEvent(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        JSONObject providerAdditionalData = IronSourceUtils.getProviderAdditionalData(abstractSmash);
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerAdditionalData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("RewardedVideoManager logProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, providerAdditionalData));
    }

    private int smashesCount(MEDIATION_STATE... mediation_stateArr) {
        int i;
        synchronized (this.mSmashArray) {
            Iterator it = this.mSmashArray.iterator();
            i = 0;
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                int i2 = i;
                for (MEDIATION_STATE mediation_state : mediation_stateArr) {
                    if (abstractSmash.getMediationState() == mediation_state) {
                        i2++;
                    }
                }
                i = i2;
            }
        }
        return i;
    }

    private void sendMediationLoadEvents() {
        if (isRewardedVideoAvailable()) {
            logMediationEvent(1000);
            logMediationEvent(1003, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Integer.valueOf(0)}});
            this.mShouldSendMediationLoadSuccessEvent = false;
            return;
        }
        if (isAvailableAdaptersToLoad()) {
            logMediationEvent(1000);
            this.mShouldSendMediationLoadSuccessEvent = true;
            this.mLoadStartTime = new Date().getTime();
        }
    }

    public void onDailyCapReleased() {
        Iterator it = this.mSmashArray.iterator();
        boolean z = false;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == MEDIATION_STATE.CAPPED_PER_DAY) {
                logProviderEvent(IronSourceConstants.REWARDED_VIDEO_DAILY_CAPPED, abstractSmash, new Object[][]{new Object[]{"status", "false"}});
                abstractSmash.setMediationState(MEDIATION_STATE.NOT_AVAILABLE);
                if (((RewardedVideoSmash) abstractSmash).isRewardedVideoAvailable() && abstractSmash.isMediationAvailable()) {
                    abstractSmash.setMediationState(MEDIATION_STATE.AVAILABLE);
                    z = true;
                }
            }
        }
        if (z && shouldNotifyAvailabilityChanged(true)) {
            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(true);
        }
    }
}
