package com.ironsource.mediationsdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.ironsource.mediationsdk.IronSource.AD_UNIT;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IronSourceInitializer {
    private boolean isInitializationInProgress = false;
    private boolean isInitialized = false;
    /* access modifiers changed from: private */
    public ArrayList<IronSourceInitializationListener> listeners;

    public interface IronSourceInitializationListener {
        void onInitializationFailed();

        void onInitialized();
    }

    public void init(@NonNull final Activity activity, @NonNull String str, @NonNull IronSourceInitializationListener ironSourceInitializationListener) throws Exception {
        synchronized (IronSourceInitializer.class) {
            if (this.isInitialized) {
                ironSourceInitializationListener.onInitialized();
            } else {
                if (this.listeners == null) {
                    this.listeners = new ArrayList<>();
                }
                this.listeners.add(ironSourceInitializationListener);
            }
        }
        if (!this.isInitializationInProgress) {
            this.isInitializationInProgress = true;
            IronSource.initISDemandOnly(activity, str, AD_UNIT.INTERSTITIAL, AD_UNIT.REWARDED_VIDEO);
            MediationInitializer.getInstance().addMediationInitializationListener(new OnMediationInitializationListener() {
                public void onInitSuccess(List<AD_UNIT> list, boolean z) {
                    IronSourceInitializer.this.processFinish(activity, true);
                }

                public void onInitFailed(String str) {
                    IronSourceInitializer.this.processFinish(activity, false);
                }

                public void onStillInProgressAfter15Secs() {
                    IronSourceInitializer.this.processFinish(activity, false);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void processFinish(@NonNull Activity activity, final boolean z) {
        final ArrayList arrayList;
        this.isInitialized = z;
        this.isInitializationInProgress = false;
        if (this.listeners != null) {
            synchronized (IronSourceInitializer.class) {
                arrayList = new ArrayList(this.listeners);
            }
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        IronSourceInitializationListener ironSourceInitializationListener = (IronSourceInitializationListener) it.next();
                        if (z) {
                            ironSourceInitializationListener.onInitialized();
                        } else {
                            ironSourceInitializationListener.onInitializationFailed();
                        }
                    }
                    synchronized (IronSourceInitializer.class) {
                        IronSourceInitializer.this.listeners.removeAll(arrayList);
                    }
                }
            });
        }
    }
}
