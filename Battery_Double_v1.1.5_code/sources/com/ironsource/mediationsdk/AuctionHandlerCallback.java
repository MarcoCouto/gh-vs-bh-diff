package com.ironsource.mediationsdk;

import java.util.List;

/* compiled from: AuctionHandler */
interface AuctionHandlerCallback {
    void callback(boolean z, List<AuctionResponseItem> list, String str, int i, String str2, long j);
}
