package com.ironsource.mediationsdk.events;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.eventsmodule.DataBaseEventsStorage;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.eventsmodule.EventsSender;
import com.ironsource.eventsmodule.IEventsManager;
import com.ironsource.eventsmodule.IEventsSenderResultListener;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ServerSegmetData;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseEventsManager implements IEventsManager {
    private static final String KEY_SESSION_DEPTH = "sessionDepth";
    final String DATABASE_NAME = "supersonic_sdk.db";
    final int DATABASE_VERSION = 5;
    final int DEFAULT_BACKUP_THRESHOLD = 1;
    final int DEFAULT_MAX_EVENTS_PER_BATCH = 5000;
    final int DEFAULT_MAX_NUMBER_OF_EVENTS = 100;
    final String KEY_PLACEMENT = "placement";
    final String KEY_PROVIDER = "provider";
    private final String MEDIATION_ABT = "abt";
    final int NO_CONNECTIVITY_EVENT_ID_ADDITION = 90000;
    private String mAbt = "";
    int mAdUnitType;
    private int mBackupThreshold = 1;
    private Map<String, String> mBatchParams = new HashMap();
    Set<Integer> mConnectivitySensitiveEventsSet;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public DataBaseEventsStorage mDbStorage;
    /* access modifiers changed from: private */
    public EventThread mEventThread;
    String mEventType;
    private AbstractEventsFormatter mFormatter;
    String mFormatterType;
    private Map<String, String> mGenericEventParams = new HashMap();
    /* access modifiers changed from: private */
    public boolean mHadTopPriorityEvent = false;
    private boolean mHasServerResponse;
    /* access modifiers changed from: private */
    public boolean mIsEventsEnabled = true;
    /* access modifiers changed from: private */
    public ArrayList<EventData> mLocalEvents;
    /* access modifiers changed from: private */
    public IronSourceLoggerManager mLoggerManager;
    private int mMaxEventsPerBatch = 5000;
    private int mMaxNumberOfEvents = 100;
    private int[] mOptOutEvents;
    private IronSourceSegment mSegment;
    private ServerSegmetData mServerSegmentData;
    /* access modifiers changed from: private */
    public String mSessionId;
    /* access modifiers changed from: private */
    public int mTotalEvents;

    private class EventThread extends HandlerThread {
        private Handler mHandler;

        EventThread(String str) {
            super(str);
        }

        /* access modifiers changed from: 0000 */
        public void postTask(Runnable runnable) {
            this.mHandler.post(runnable);
        }

        /* access modifiers changed from: 0000 */
        public void prepareHandler() {
            this.mHandler = new Handler(getLooper());
        }
    }

    /* access modifiers changed from: protected */
    public abstract String getCurrentPlacement(int i);

    /* access modifiers changed from: protected */
    public abstract int getSessionDepth(EventData eventData);

    /* access modifiers changed from: protected */
    public abstract boolean increaseSessionDepthIfNeeded(EventData eventData);

    /* access modifiers changed from: protected */
    public void initConnectivitySensitiveEventsSet() {
    }

    /* access modifiers changed from: protected */
    public abstract boolean isTopPriorityEvent(EventData eventData);

    /* access modifiers changed from: protected */
    public abstract void setCurrentPlacement(EventData eventData);

    /* access modifiers changed from: protected */
    public abstract boolean shouldExtractCurrentPlacement(EventData eventData);

    /* access modifiers changed from: protected */
    public abstract boolean shouldIncludeCurrentPlacement(EventData eventData);

    /* access modifiers changed from: 0000 */
    public void initState() {
        this.mLocalEvents = new ArrayList<>();
        this.mTotalEvents = 0;
        this.mFormatter = EventsFormatterFactory.getFormatter(this.mFormatterType, this.mAdUnitType);
        StringBuilder sb = new StringBuilder();
        sb.append(this.mEventType);
        sb.append("EventThread");
        this.mEventThread = new EventThread(sb.toString());
        this.mEventThread.start();
        this.mEventThread.prepareHandler();
        this.mLoggerManager = IronSourceLoggerManager.getLogger();
        this.mSessionId = IronSourceObject.getInstance().getSessionId();
        this.mConnectivitySensitiveEventsSet = new HashSet();
        initConnectivitySensitiveEventsSet();
    }

    public synchronized void start(Context context, IronSourceSegment ironSourceSegment) {
        this.mFormatterType = IronSourceUtils.getDefaultEventsFormatterType(context, this.mEventType, this.mFormatterType);
        verifyCurrentFormatter(this.mFormatterType);
        this.mFormatter.setEventsServerUrl(IronSourceUtils.getDefaultEventsURL(context, this.mEventType, null));
        this.mDbStorage = DataBaseEventsStorage.getInstance(context, "supersonic_sdk.db", 5);
        backupEventsToDb();
        this.mOptOutEvents = IronSourceUtils.getDefaultOptOutEvents(context, this.mEventType);
        this.mSegment = ironSourceSegment;
        this.mContext = context;
    }

    public synchronized void setServerSegmentData(ServerSegmetData serverSegmetData) {
        this.mServerSegmentData = serverSegmetData;
    }

    /* access modifiers changed from: private */
    public synchronized boolean isNoConnectivitydEvent(String str, EventData eventData) {
        return str.equalsIgnoreCase("none") && this.mConnectivitySensitiveEventsSet.contains(Integer.valueOf(eventData.getEventId()));
    }

    /* access modifiers changed from: private */
    public synchronized int convertEventtoNotConnected(EventData eventData) {
        return eventData.getEventId() + 90000;
    }

    public synchronized void log(final EventData eventData) {
        this.mEventThread.postTask(new Runnable() {
            public void run() {
                if (eventData != null && BaseEventsManager.this.mIsEventsEnabled) {
                    eventData.addToAdditionalData("eventSessionId", BaseEventsManager.this.mSessionId);
                    String connectionType = IronSourceUtils.getConnectionType(BaseEventsManager.this.mContext);
                    if (!(eventData.getEventId() == 40 || eventData.getEventId() == 41)) {
                        eventData.addToAdditionalData(RequestParameters.CONNECTION_TYPE, connectionType);
                    }
                    if (BaseEventsManager.this.isNoConnectivitydEvent(connectionType, eventData)) {
                        eventData.setEventId(BaseEventsManager.this.convertEventtoNotConnected(eventData));
                    }
                    if (!BaseEventsManager.this.getGenericEventParams().isEmpty()) {
                        for (Entry entry : BaseEventsManager.this.getGenericEventParams().entrySet()) {
                            if (!(eventData.getAdditionalDataJSON().has((String) entry.getKey()) || entry.getKey() == "eventId" || entry.getKey() == "timestamp")) {
                                eventData.addToAdditionalData((String) entry.getKey(), entry.getValue());
                            }
                        }
                    }
                    try {
                        StringBuilder sb = new StringBuilder();
                        sb.append("{\"eventId\":");
                        sb.append(eventData.getEventId());
                        sb.append(",\"timestamp\":");
                        sb.append(eventData.getTimeStamp());
                        sb.append(",");
                        sb.append(eventData.getAdditionalData().substring(1));
                        BaseEventsManager.this.mLoggerManager.log(IronSourceTag.EVENT, sb.toString().replace(",", "\n"), 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (BaseEventsManager.this.shouldEventBeLogged(eventData)) {
                        if (BaseEventsManager.this.shouldAddSessionDepth(eventData)) {
                            int sessionDepth = BaseEventsManager.this.getSessionDepth(eventData);
                            if (BaseEventsManager.this.increaseSessionDepthIfNeeded(eventData)) {
                                sessionDepth = BaseEventsManager.this.getSessionDepth(eventData);
                            }
                            eventData.addToAdditionalData("sessionDepth", Integer.valueOf(sessionDepth));
                        }
                        if (BaseEventsManager.this.shouldExtractCurrentPlacement(eventData)) {
                            BaseEventsManager.this.setCurrentPlacement(eventData);
                        } else if (!TextUtils.isEmpty(BaseEventsManager.this.getCurrentPlacement(eventData.getEventId())) && BaseEventsManager.this.shouldIncludeCurrentPlacement(eventData)) {
                            eventData.addToAdditionalData("placement", BaseEventsManager.this.getCurrentPlacement(eventData.getEventId()));
                        }
                        BaseEventsManager.this.mLocalEvents.add(eventData);
                        BaseEventsManager.this.mTotalEvents = BaseEventsManager.this.mTotalEvents + 1;
                    }
                    boolean isTopPriorityEvent = BaseEventsManager.this.isTopPriorityEvent(eventData);
                    if (!BaseEventsManager.this.mHadTopPriorityEvent && isTopPriorityEvent) {
                        BaseEventsManager.this.mHadTopPriorityEvent = true;
                    }
                    if (BaseEventsManager.this.mDbStorage != null) {
                        if (BaseEventsManager.this.shouldSendEvents()) {
                            BaseEventsManager.this.sendEvents();
                        } else if (BaseEventsManager.this.shouldBackupEventsToDb(BaseEventsManager.this.mLocalEvents) || isTopPriorityEvent) {
                            BaseEventsManager.this.backupEventsToDb();
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void sendEvents() {
        this.mHadTopPriorityEvent = false;
        ArrayList initCombinedEventList = initCombinedEventList(this.mLocalEvents, this.mDbStorage.loadEvents(this.mEventType), this.mMaxEventsPerBatch);
        this.mLocalEvents.clear();
        this.mDbStorage.clearEvents(this.mEventType);
        this.mTotalEvents = 0;
        if (initCombinedEventList.size() > 0) {
            JSONObject json = GeneralProperties.getProperties().toJSON();
            try {
                updateSegmentsData(json);
                String abt = getAbt();
                if (!TextUtils.isEmpty(abt)) {
                    json.put("abt", abt);
                }
                Map batchParams = getBatchParams();
                if (!batchParams.isEmpty()) {
                    for (Entry entry : batchParams.entrySet()) {
                        if (!json.has((String) entry.getKey())) {
                            json.put((String) entry.getKey(), entry.getValue());
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String format = this.mFormatter.format(initCombinedEventList, json);
            new EventsSender(new IEventsSenderResultListener() {
                public synchronized void onEventsSenderResult(final ArrayList<EventData> arrayList, final boolean z) {
                    BaseEventsManager.this.mEventThread.postTask(new Runnable() {
                        public void run() {
                            if (z) {
                                BaseEventsManager.this.mTotalEvents = BaseEventsManager.this.mDbStorage.loadEvents(BaseEventsManager.this.mEventType).size() + BaseEventsManager.this.mLocalEvents.size();
                            } else if (arrayList != null) {
                                BaseEventsManager.this.mDbStorage.saveEvents(arrayList, BaseEventsManager.this.mEventType);
                                BaseEventsManager.this.mTotalEvents = BaseEventsManager.this.mDbStorage.loadEvents(BaseEventsManager.this.mEventType).size() + BaseEventsManager.this.mLocalEvents.size();
                            }
                        }
                    });
                }
            }).execute(new Object[]{format, this.mFormatter.getEventsServerUrl(), initCombinedEventList});
        }
    }

    private ArrayList<EventData> initCombinedEventList(ArrayList<EventData> arrayList, ArrayList<EventData> arrayList2, int i) {
        ArrayList arrayList3 = new ArrayList();
        arrayList3.addAll(arrayList);
        arrayList3.addAll(arrayList2);
        Collections.sort(arrayList3, new Comparator<EventData>() {
            public int compare(EventData eventData, EventData eventData2) {
                return eventData.getTimeStamp() >= eventData2.getTimeStamp() ? 1 : -1;
            }
        });
        if (arrayList3.size() <= i) {
            return new ArrayList<>(arrayList3);
        }
        ArrayList<EventData> arrayList4 = new ArrayList<>(arrayList3.subList(0, i));
        this.mDbStorage.saveEvents(arrayList3.subList(i, arrayList3.size()), this.mEventType);
        return arrayList4;
    }

    private void verifyCurrentFormatter(String str) {
        if (this.mFormatter == null || !this.mFormatter.getFormatterType().equals(str)) {
            this.mFormatter = EventsFormatterFactory.getFormatter(str, this.mAdUnitType);
        }
    }

    public void setBackupThreshold(int i) {
        if (i > 0) {
            this.mBackupThreshold = i;
        }
    }

    public void setMaxNumberOfEvents(int i) {
        if (i > 0) {
            this.mMaxNumberOfEvents = i;
        }
    }

    public void setMaxEventsPerBatch(int i) {
        if (i > 0) {
            this.mMaxEventsPerBatch = i;
        }
    }

    public void setOptOutEvents(int[] iArr, Context context) {
        this.mOptOutEvents = iArr;
        IronSourceUtils.saveDefaultOptOutEvents(context, this.mEventType, iArr);
    }

    public void setEventsUrl(String str, Context context) {
        if (!TextUtils.isEmpty(str)) {
            if (this.mFormatter != null) {
                this.mFormatter.setEventsServerUrl(str);
            }
            IronSourceUtils.saveDefaultEventsURL(context, this.mEventType, str);
        }
    }

    public void setFormatterType(String str, Context context) {
        if (!TextUtils.isEmpty(str)) {
            this.mFormatterType = str;
            IronSourceUtils.saveDefaultEventsFormatterType(context, this.mEventType, str);
            verifyCurrentFormatter(str);
        }
    }

    public void setIsEventsEnabled(boolean z) {
        this.mIsEventsEnabled = z;
    }

    /* access modifiers changed from: private */
    public void backupEventsToDb() {
        this.mDbStorage.saveEvents(this.mLocalEvents, this.mEventType);
        this.mLocalEvents.clear();
    }

    /* access modifiers changed from: private */
    public boolean shouldSendEvents() {
        return (this.mTotalEvents >= this.mMaxNumberOfEvents || this.mHadTopPriorityEvent) && this.mHasServerResponse;
    }

    /* access modifiers changed from: private */
    public boolean shouldBackupEventsToDb(ArrayList<EventData> arrayList) {
        return arrayList != null && arrayList.size() >= this.mBackupThreshold;
    }

    /* access modifiers changed from: private */
    public boolean shouldEventBeLogged(EventData eventData) {
        if (!(eventData == null || this.mOptOutEvents == null || this.mOptOutEvents.length <= 0)) {
            int eventId = eventData.getEventId();
            for (int i : this.mOptOutEvents) {
                if (eventId == i) {
                    return false;
                }
            }
        }
        return true;
    }

    public void setHasServerResponse(boolean z) {
        this.mHasServerResponse = z;
    }

    /* access modifiers changed from: 0000 */
    public String getProviderNameForEvent(EventData eventData) {
        try {
            return new JSONObject(eventData.getAdditionalData()).optString("provider", "");
        } catch (JSONException unused) {
            return "";
        }
    }

    public void triggerEventsSend() {
        sendEvents();
    }

    public void sendEventToUrl(EventData eventData, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(eventData);
            String format = this.mFormatter.format(arrayList, GeneralProperties.getProperties().toJSON());
            new EventsSender().execute(new Object[]{format, str, null});
        } catch (Exception unused) {
        }
    }

    private void updateSegmentsData(JSONObject jSONObject) {
        try {
            if (this.mSegment != null) {
                if (this.mSegment.getAge() > 0) {
                    jSONObject.put(IronSourceSegment.AGE, this.mSegment.getAge());
                }
                if (!TextUtils.isEmpty(this.mSegment.getGender())) {
                    jSONObject.put(IronSourceSegment.GENDER, this.mSegment.getGender());
                }
                if (this.mSegment.getLevel() > 0) {
                    jSONObject.put(IronSourceSegment.LEVEL, this.mSegment.getLevel());
                }
                if (this.mSegment.getIsPaying() != null) {
                    jSONObject.put(IronSourceSegment.PAYING, this.mSegment.getIsPaying().get());
                }
                if (this.mSegment.getIapt() > Utils.DOUBLE_EPSILON) {
                    jSONObject.put(IronSourceSegment.IAPT, this.mSegment.getIapt());
                }
                if (this.mSegment.getUcd() > 0) {
                    jSONObject.put(IronSourceSegment.USER_CREATION_DATE, this.mSegment.getUcd());
                }
            }
            if (this.mServerSegmentData != null) {
                String segmentId = this.mServerSegmentData.getSegmentId();
                if (!TextUtils.isEmpty(segmentId)) {
                    jSONObject.put("segmentId", segmentId);
                }
                JSONObject customSegments = this.mServerSegmentData.getCustomSegments();
                Iterator keys = customSegments.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    jSONObject.put(str, customSegments.get(str));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setABT(String str) {
        this.mAbt = str;
    }

    public String getAbt() {
        return this.mAbt;
    }

    public void setBatchParams(Map<String, String> map) {
        this.mBatchParams.putAll(map);
    }

    public Map<String, String> getBatchParams() {
        return this.mBatchParams;
    }

    public void setEventGenericParams(Map<String, String> map) {
        this.mGenericEventParams.putAll(map);
    }

    public Map<String, String> getGenericEventParams() {
        return this.mGenericEventParams;
    }

    /* access modifiers changed from: private */
    public boolean shouldAddSessionDepth(EventData eventData) {
        return (eventData.getEventId() == 14 || eventData.getEventId() == 514 || eventData.getEventId() == 140 || eventData.getEventId() == 40 || eventData.getEventId() == 41) ? false : true;
    }
}
