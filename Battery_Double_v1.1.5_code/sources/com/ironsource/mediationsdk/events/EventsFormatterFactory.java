package com.ironsource.mediationsdk.events;

import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;

class EventsFormatterFactory {
    static final int AD_UNIT_INTERSTITIAL = 2;
    static final int AD_UNIT_REWARDED_VIDEO = 3;
    static final String TYPE_IRONBEAST = "ironbeast";
    static final String TYPE_OUTCOME = "outcome";

    EventsFormatterFactory() {
    }

    static AbstractEventsFormatter getFormatter(String str, int i) {
        if (TYPE_IRONBEAST.equals(str)) {
            return new IronbeastEventsFormatter(i);
        }
        if (TYPE_OUTCOME.equals(str)) {
            return new OutcomeEventsFormatter(i);
        }
        if (i == 2) {
            return new IronbeastEventsFormatter(i);
        }
        if (i == 3) {
            return new OutcomeEventsFormatter(i);
        }
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
        StringBuilder sb = new StringBuilder();
        sb.append("EventsFormatterFactory failed to instantiate a formatter (type: ");
        sb.append(str);
        sb.append(", adUnit: ");
        sb.append(i);
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 2);
        return null;
    }
}
