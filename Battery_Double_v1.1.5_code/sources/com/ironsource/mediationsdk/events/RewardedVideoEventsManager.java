package com.ironsource.mediationsdk.events;

import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.SessionDepthManager;

public class RewardedVideoEventsManager extends BaseEventsManager {
    private static RewardedVideoEventsManager sInstance;
    private String mCurrentOWPlacement;

    /* access modifiers changed from: protected */
    public boolean shouldExtractCurrentPlacement(EventData eventData) {
        return false;
    }

    private RewardedVideoEventsManager() {
        this.mFormatterType = "outcome";
        this.mAdUnitType = 3;
        this.mEventType = IronSourceConstants.REWARDED_VIDEO_EVENT_TYPE;
        this.mCurrentOWPlacement = "";
    }

    public static synchronized RewardedVideoEventsManager getInstance() {
        RewardedVideoEventsManager rewardedVideoEventsManager;
        synchronized (RewardedVideoEventsManager.class) {
            if (sInstance == null) {
                sInstance = new RewardedVideoEventsManager();
                sInstance.initState();
            }
            rewardedVideoEventsManager = sInstance;
        }
        return rewardedVideoEventsManager;
    }

    /* access modifiers changed from: protected */
    public boolean shouldIncludeCurrentPlacement(EventData eventData) {
        return eventData.getEventId() == 305;
    }

    /* access modifiers changed from: protected */
    public boolean isTopPriorityEvent(EventData eventData) {
        int eventId = eventData.getEventId();
        return eventId == 14 || eventId == 514 || eventId == 305 || eventId == 1005 || eventId == 1203 || eventId == 1010 || eventId == 1301 || eventId == 1302;
    }

    /* access modifiers changed from: protected */
    public int getSessionDepth(EventData eventData) {
        if (eventData.getEventId() == 15 || (eventData.getEventId() >= 300 && eventData.getEventId() < 400)) {
            return SessionDepthManager.getInstance().getSessionDepth(0);
        }
        return SessionDepthManager.getInstance().getSessionDepth(1);
    }

    /* access modifiers changed from: protected */
    public void setCurrentPlacement(EventData eventData) {
        if (eventData.getEventId() == 15 || (eventData.getEventId() >= 300 && eventData.getEventId() < 400)) {
            this.mCurrentOWPlacement = eventData.getAdditionalDataJSON().optString("placement");
        }
    }

    /* access modifiers changed from: protected */
    public String getCurrentPlacement(int i) {
        return (i == 15 || (i >= 300 && i < 400)) ? this.mCurrentOWPlacement : "";
    }

    /* access modifiers changed from: protected */
    public boolean increaseSessionDepthIfNeeded(EventData eventData) {
        if (eventData.getEventId() == 1203) {
            SessionDepthManager.getInstance().increaseSessionDepth(1);
        } else if (eventData.getEventId() == 305) {
            SessionDepthManager.getInstance().increaseSessionDepth(0);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void initConnectivitySensitiveEventsSet() {
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(1001));
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(IronSourceConstants.RV_INSTANCE_SHOW_CHANCE));
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(IronSourceConstants.RV_INSTANCE_READY_TRUE));
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(IronSourceConstants.RV_INSTANCE_READY_FALSE));
    }
}
