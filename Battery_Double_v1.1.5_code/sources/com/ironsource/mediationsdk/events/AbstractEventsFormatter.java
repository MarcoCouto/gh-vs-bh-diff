package com.ironsource.mediationsdk.events;

import android.text.TextUtils;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

abstract class AbstractEventsFormatter {
    private final String EVENTS_KEY_DEFAULT = EventEntry.TABLE_NAME;
    private final String EVENTS_KEY_IS = "InterstitialEvents";
    private final String EVENTS_KEY_RV = EventEntry.TABLE_NAME;
    private final String KEY_AD_UNIT = "adUnit";
    private final String KEY_EVENT_ID = "eventId";
    private final String KEY_TIMESTAMP = "timestamp";
    int mAdUnit;
    JSONObject mGeneralProperties;
    private String mServerUrl;

    private String getEventsKey(int i) {
        switch (i) {
            case 2:
                return "InterstitialEvents";
            case 3:
                return EventEntry.TABLE_NAME;
            default:
                return EventEntry.TABLE_NAME;
        }
    }

    public abstract String format(ArrayList<EventData> arrayList, JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public abstract String getDefaultEventsUrl();

    public abstract String getFormatterType();

    AbstractEventsFormatter() {
    }

    /* access modifiers changed from: 0000 */
    public JSONObject createJSONForEvent(EventData eventData) {
        try {
            JSONObject jSONObject = new JSONObject(eventData.getAdditionalData());
            jSONObject.put("eventId", eventData.getEventId());
            jSONObject.put("timestamp", eventData.getTimeStamp());
            return jSONObject;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public String createDataToSend(JSONArray jSONArray) {
        String str = "";
        try {
            if (this.mGeneralProperties == null) {
                return str;
            }
            JSONObject jSONObject = new JSONObject(this.mGeneralProperties.toString());
            jSONObject.put("timestamp", IronSourceUtils.getTimeStamp());
            jSONObject.put("adUnit", this.mAdUnit);
            jSONObject.put(getEventsKey(this.mAdUnit), jSONArray);
            return jSONObject.toString();
        } catch (Exception unused) {
            return str;
        }
    }

    /* access modifiers changed from: 0000 */
    public String getEventsServerUrl() {
        return TextUtils.isEmpty(this.mServerUrl) ? getDefaultEventsUrl() : this.mServerUrl;
    }

    /* access modifiers changed from: 0000 */
    public void setEventsServerUrl(String str) {
        this.mServerUrl = str;
    }
}
