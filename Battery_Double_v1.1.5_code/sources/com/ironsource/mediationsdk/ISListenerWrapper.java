package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialListener;

public class ISListenerWrapper {
    private static final ISListenerWrapper sInstance = new ISListenerWrapper();
    /* access modifiers changed from: private */
    public InterstitialListener mListener = null;

    public static synchronized ISListenerWrapper getInstance() {
        ISListenerWrapper iSListenerWrapper;
        synchronized (ISListenerWrapper.class) {
            iSListenerWrapper = sInstance;
        }
        return iSListenerWrapper;
    }

    private ISListenerWrapper() {
    }

    public synchronized void setListener(InterstitialListener interstitialListener) {
        this.mListener = interstitialListener;
    }

    public synchronized InterstitialListener getListener() {
        return this.mListener;
    }

    public synchronized void onInterstitialAdReady() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.mListener.onInterstitialAdReady();
                        ISListenerWrapper.this.log("onInterstitialAdReady()");
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdLoadFailed(final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.mListener.onInterstitialAdLoadFailed(ironSourceError);
                        ISListenerWrapper iSListenerWrapper = ISListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdLoadFailed() error=");
                        sb.append(ironSourceError.getErrorMessage());
                        iSListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdOpened() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.mListener.onInterstitialAdOpened();
                        ISListenerWrapper.this.log("onInterstitialAdOpened()");
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdClosed() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.mListener.onInterstitialAdClosed();
                        ISListenerWrapper.this.log("onInterstitialAdClosed()");
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdShowSucceeded() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.mListener.onInterstitialAdShowSucceeded();
                        ISListenerWrapper.this.log("onInterstitialAdShowSucceeded()");
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdShowFailed(final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.mListener.onInterstitialAdShowFailed(ironSourceError);
                        ISListenerWrapper iSListenerWrapper = ISListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdShowFailed() error=");
                        sb.append(ironSourceError.getErrorMessage());
                        iSListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onInterstitialAdClicked() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.mListener.onInterstitialAdClicked();
                        ISListenerWrapper.this.log("onInterstitialAdClicked()");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, str, 1);
    }
}
