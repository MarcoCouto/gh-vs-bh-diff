package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class DemandOnlySmash {
    protected JSONObject mAdUnitSettings;
    protected AbstractAdapter mAdapter;
    protected AdapterConfig mAdapterConfig;

    protected enum SMASH_STATE {
        NOT_LOADED,
        LOAD_IN_PROGRESS,
        LOADED,
        SHOW_IN_PROGRESS
    }

    public DemandOnlySmash(AdapterConfig adapterConfig, AbstractAdapter abstractAdapter) {
        this.mAdapterConfig = adapterConfig;
        this.mAdapter = abstractAdapter;
        this.mAdUnitSettings = adapterConfig.getAdUnitSetings();
    }

    public synchronized void onResume(Activity activity) {
        this.mAdapter.onResume(activity);
    }

    public synchronized void onPause(Activity activity) {
        this.mAdapter.onPause(activity);
    }

    public synchronized void setConsent(boolean z) {
        this.mAdapter.setConsent(z);
    }

    public synchronized String getInstanceName() {
        return this.mAdapterConfig.getProviderName();
    }

    public synchronized String getSubProviderId() {
        return this.mAdapterConfig.getSubProviderId();
    }

    public Map<String, Object> getProviderEventData() {
        HashMap hashMap = new HashMap();
        try {
            hashMap.put("providerAdapterVersion", this.mAdapter != null ? this.mAdapter.getVersion() : "");
            hashMap.put("providerSDKVersion", this.mAdapter != null ? this.mAdapter.getCoreSDKVersion() : "");
            hashMap.put("spId", this.mAdapterConfig.getSubProviderId());
            hashMap.put("provider", this.mAdapterConfig.getAdSourceNameForEvents());
            hashMap.put(IronSourceConstants.EVENTS_DEMAND_ONLY, Integer.valueOf(1));
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
            StringBuilder sb = new StringBuilder();
            sb.append("getProviderEventData ");
            sb.append(getInstanceName());
            sb.append(")");
            logger.logException(ironSourceTag, sb.toString(), e);
        }
        return hashMap;
    }
}
