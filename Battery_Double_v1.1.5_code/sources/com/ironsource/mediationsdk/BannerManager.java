package com.ironsource.mediationsdk;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.BannerPlacement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.BannerManagerListener;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.startapp.android.publish.common.model.AdPreferences;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class BannerManager implements BannerManagerListener {
    private BannerSmash mActiveSmash;
    private Activity mActivity;
    private String mAppKey;
    private BannerPlacement mCurrentPlacement;
    AtomicBoolean mDidImplementOnPause = new AtomicBoolean();
    AtomicBoolean mDidImplementOnResume = new AtomicBoolean();
    private IronSourceBannerLayout mIronsourceBanner;
    private Boolean mIsInForeground = Boolean.valueOf(true);
    private IronSourceLoggerManager mLoggerManager = IronSourceLoggerManager.getLogger();
    private long mReloadInterval;
    private Timer mReloadTimer;
    private final CopyOnWriteArrayList<BannerSmash> mSmashArray = new CopyOnWriteArrayList<>();
    private BANNER_STATE mState = BANNER_STATE.NOT_INITIATED;
    private String mUserId;

    private enum BANNER_STATE {
        NOT_INITIATED,
        READY_TO_LOAD,
        FIRST_LOAD_IN_PROGRESS,
        LOAD_IN_PROGRESS,
        RELOAD_IN_PROGRESS
    }

    public BannerManager(List<ProviderSettings> list, Activity activity, String str, String str2, long j, int i, int i2) {
        this.mAppKey = str;
        this.mUserId = str2;
        this.mActivity = activity;
        this.mReloadInterval = (long) i;
        BannerCallbackThrottler.getInstance().setDelayLoadFailureNotificationInSeconds(i2);
        for (int i3 = 0; i3 < list.size(); i3++) {
            ProviderSettings providerSettings = (ProviderSettings) list.get(i3);
            AbstractAdapter loadAdapter = loadAdapter(providerSettings);
            if (loadAdapter == null || !AdaptersCompatibilityHandler.getInstance().isBannerAdapterCompatible(loadAdapter)) {
                StringBuilder sb = new StringBuilder();
                sb.append(providerSettings.getProviderInstanceName());
                sb.append(" can't load adapter or wrong version");
                debugLog(sb.toString());
            } else {
                BannerSmash bannerSmash = new BannerSmash(this, providerSettings, loadAdapter, j, i3 + 1);
                this.mSmashArray.add(bannerSmash);
            }
        }
        this.mCurrentPlacement = null;
        setState(BANNER_STATE.READY_TO_LOAD);
    }

    public synchronized void setConsent(boolean z) {
        synchronized (this.mSmashArray) {
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                ((BannerSmash) it.next()).setConsent(z);
            }
        }
    }

    public synchronized void loadBanner(IronSourceBannerLayout ironSourceBannerLayout, BannerPlacement bannerPlacement) {
        try {
            if (this.mState == BANNER_STATE.READY_TO_LOAD) {
                if (!BannerCallbackThrottler.getInstance().hasPendingInvocation()) {
                    setState(BANNER_STATE.FIRST_LOAD_IN_PROGRESS);
                    this.mIronsourceBanner = ironSourceBannerLayout;
                    this.mCurrentPlacement = bannerPlacement;
                    sendMediationEvent(3001);
                    if (CappingManager.isBnPlacementCapped(this.mActivity, bannerPlacement.getPlacementName())) {
                        BannerCallbackThrottler instance = BannerCallbackThrottler.getInstance();
                        StringBuilder sb = new StringBuilder();
                        sb.append("placement ");
                        sb.append(bannerPlacement.getPlacementName());
                        sb.append(" is capped");
                        instance.sendBannerAdLoadFailed(ironSourceBannerLayout, new IronSourceError(IronSourceError.ERROR_BN_LOAD_PLACEMENT_CAPPED, sb.toString()));
                        sendMediationEvent(IronSourceConstants.BN_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_BN_LOAD_PLACEMENT_CAPPED)}});
                        setState(BANNER_STATE.READY_TO_LOAD);
                        return;
                    }
                    synchronized (this.mSmashArray) {
                        Iterator it = this.mSmashArray.iterator();
                        while (it.hasNext()) {
                            ((BannerSmash) it.next()).setReadyToLoad(true);
                        }
                        BannerSmash bannerSmash = (BannerSmash) this.mSmashArray.get(0);
                        sendProviderEvent(IronSourceConstants.BN_INSTANCE_LOAD, bannerSmash);
                        bannerSmash.loadBanner(ironSourceBannerLayout, this.mActivity, this.mAppKey, this.mUserId);
                    }
                }
            }
            this.mLoggerManager.log(IronSourceTag.API, "A banner is already loaded", 3);
            return;
        } catch (Exception e) {
            BannerCallbackThrottler instance2 = BannerCallbackThrottler.getInstance();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("loadBanner() failed ");
            sb2.append(e.getMessage());
            instance2.sendBannerAdLoadFailed(ironSourceBannerLayout, new IronSourceError(IronSourceError.ERROR_BN_LOAD_EXCEPTION, sb2.toString()));
            String message = e.getMessage();
            sendMediationEvent(IronSourceConstants.BN_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_BN_LOAD_EXCEPTION)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, message.substring(0, Math.min(message.length(), 39))}});
            setState(BANNER_STATE.READY_TO_LOAD);
        }
    }

    public synchronized void destroyBanner(IronSourceBannerLayout ironSourceBannerLayout) {
        if (ironSourceBannerLayout == null) {
            this.mLoggerManager.log(IronSourceTag.API, "destroyBanner banner cannot be null", 3);
        } else if (ironSourceBannerLayout.isDestroyed()) {
            this.mLoggerManager.log(IronSourceTag.API, "Banner is already destroyed and can't be used anymore. Please create a new one using IronSource.createBanner API", 3);
        } else {
            sendMediationEvent(IronSourceConstants.BN_DESTROY);
            stopReloadTimer();
            ironSourceBannerLayout.destroyBanner();
            this.mIronsourceBanner = null;
            this.mCurrentPlacement = null;
            if (this.mActiveSmash != null) {
                sendProviderEvent(IronSourceConstants.BN_INSTANCE_DESTROY, this.mActiveSmash);
                this.mActiveSmash.destroyBanner();
                this.mActiveSmash = null;
            }
            setState(BANNER_STATE.READY_TO_LOAD);
        }
    }

    private void errorLog(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("BannerManager ");
        sb.append(str);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
    }

    private void debugLog(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("BannerManager ");
        sb.append(str);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
    }

    private void setState(BANNER_STATE banner_state) {
        this.mState = banner_state;
        StringBuilder sb = new StringBuilder();
        sb.append("state=");
        sb.append(banner_state.name());
        debugLog(sb.toString());
    }

    private void callbackLog(String str, BannerSmash bannerSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("BannerManager ");
        sb.append(str);
        sb.append(" ");
        sb.append(bannerSmash.getName());
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
    }

    private void bindView(BannerSmash bannerSmash, View view, LayoutParams layoutParams) {
        this.mActiveSmash = bannerSmash;
        this.mIronsourceBanner.addViewWithFrameLayoutParams(view, layoutParams);
    }

    public void onBannerAdLoaded(BannerSmash bannerSmash, View view, LayoutParams layoutParams) {
        callbackLog("onBannerAdLoaded", bannerSmash);
        if (this.mState == BANNER_STATE.FIRST_LOAD_IN_PROGRESS) {
            sendProviderEvent(IronSourceConstants.BN_INSTANCE_LOAD_SUCCESS, bannerSmash);
            bindView(bannerSmash, view, layoutParams);
            CappingManager.incrementBnShowCounter(this.mActivity, this.mCurrentPlacement.getPlacementName());
            if (CappingManager.isBnPlacementCapped(this.mActivity, this.mCurrentPlacement.getPlacementName())) {
                sendMediationEvent(IronSourceConstants.BN_PLACEMENT_CAPPED);
            }
            this.mIronsourceBanner.sendBannerAdLoaded(bannerSmash);
            sendMediationEvent(IronSourceConstants.BN_CALLBACK_LOAD_SUCCESS);
            setState(BANNER_STATE.RELOAD_IN_PROGRESS);
            startReloadTimer();
        } else if (this.mState == BANNER_STATE.LOAD_IN_PROGRESS) {
            sendProviderEvent(IronSourceConstants.BN_INSTANCE_RELOAD_SUCCESS, bannerSmash);
            bindView(bannerSmash, view, layoutParams);
            setState(BANNER_STATE.RELOAD_IN_PROGRESS);
            startReloadTimer();
        }
    }

    public void onBannerAdLoadFailed(IronSourceError ironSourceError, BannerSmash bannerSmash, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("onBannerAdLoadFailed ");
        sb.append(ironSourceError.getErrorMessage());
        callbackLog(sb.toString(), bannerSmash);
        if (this.mState == BANNER_STATE.FIRST_LOAD_IN_PROGRESS || this.mState == BANNER_STATE.LOAD_IN_PROGRESS) {
            if (z) {
                sendProviderEvent(IronSourceConstants.BN_INSTANCE_LOAD_NO_FILL, bannerSmash);
            } else {
                sendProviderEvent(IronSourceConstants.BN_INSTANCE_LOAD_ERROR, bannerSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}});
            }
            if (!loadNextSmash()) {
                if (this.mState == BANNER_STATE.FIRST_LOAD_IN_PROGRESS) {
                    BannerCallbackThrottler.getInstance().sendBannerAdLoadFailed(this.mIronsourceBanner, new IronSourceError(IronSourceError.ERROR_BN_LOAD_NO_FILL, "No ads to show"));
                    sendMediationEvent(IronSourceConstants.BN_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_BN_LOAD_NO_FILL)}});
                    setState(BANNER_STATE.READY_TO_LOAD);
                } else {
                    sendMediationEvent(IronSourceConstants.BN_RELOAD_FAILED);
                    setState(BANNER_STATE.RELOAD_IN_PROGRESS);
                    startReloadTimer();
                }
                return;
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("onBannerAdLoadFailed ");
        sb2.append(bannerSmash.getName());
        sb2.append(" wrong state=");
        sb2.append(this.mState.name());
        debugLog(sb2.toString());
    }

    public void onBannerAdReloaded(BannerSmash bannerSmash) {
        callbackLog("onBannerAdReloaded", bannerSmash);
        if (this.mState != BANNER_STATE.RELOAD_IN_PROGRESS) {
            StringBuilder sb = new StringBuilder();
            sb.append("onBannerAdReloaded ");
            sb.append(bannerSmash.getName());
            sb.append(" wrong state=");
            sb.append(this.mState.name());
            debugLog(sb.toString());
            return;
        }
        IronSourceUtils.sendAutomationLog("bannerReloadSucceeded");
        sendProviderEvent(IronSourceConstants.BN_INSTANCE_RELOAD_SUCCESS, bannerSmash);
        startReloadTimer();
    }

    public void onBannerAdReloadFailed(IronSourceError ironSourceError, BannerSmash bannerSmash, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("onBannerAdReloadFailed ");
        sb.append(ironSourceError.getErrorMessage());
        callbackLog(sb.toString(), bannerSmash);
        if (this.mState != BANNER_STATE.RELOAD_IN_PROGRESS) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("onBannerAdReloadFailed ");
            sb2.append(bannerSmash.getName());
            sb2.append(" wrong state=");
            sb2.append(this.mState.name());
            debugLog(sb2.toString());
            return;
        }
        if (z) {
            sendProviderEvent(IronSourceConstants.BN_INSTANCE_RELOAD_NO_FILL, bannerSmash);
        } else {
            sendProviderEvent(IronSourceConstants.BN_INSTANCE_RELOAD_ERROR, bannerSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}});
        }
        synchronized (this.mSmashArray) {
            if (this.mSmashArray.size() == 1) {
                sendMediationEvent(IronSourceConstants.BN_RELOAD_FAILED);
                startReloadTimer();
                return;
            }
            setState(BANNER_STATE.LOAD_IN_PROGRESS);
            resetIteration();
            loadNextSmash();
        }
    }

    public void onBannerAdClicked(BannerSmash bannerSmash) {
        callbackLog("onBannerAdClicked", bannerSmash);
        sendMediationEvent(IronSourceConstants.BN_CALLBACK_CLICK);
        this.mIronsourceBanner.sendBannerAdClicked();
        sendProviderEvent(IronSourceConstants.BN_INSTANCE_CLICK, bannerSmash);
    }

    public void onBannerAdScreenDismissed(BannerSmash bannerSmash) {
        callbackLog("onBannerAdScreenDismissed", bannerSmash);
        sendMediationEvent(IronSourceConstants.BN_CALLBACK_DISMISS_SCREEN);
        this.mIronsourceBanner.sendBannerAdScreenDismissed();
        sendProviderEvent(IronSourceConstants.BN_INSTANCE_DISMISS_SCREEN, bannerSmash);
    }

    public void onBannerAdScreenPresented(BannerSmash bannerSmash) {
        callbackLog("onBannerAdScreenPresented", bannerSmash);
        sendMediationEvent(IronSourceConstants.BN_CALLBACK_PRESENT_SCREEN);
        this.mIronsourceBanner.sendBannerAdScreenPresented();
        sendProviderEvent(IronSourceConstants.BN_INSTANCE_PRESENT_SCREEN, bannerSmash);
    }

    public void onBannerAdLeftApplication(BannerSmash bannerSmash) {
        callbackLog("onBannerAdLeftApplication", bannerSmash);
        Object[][] objArr = null;
        sendMediationEvent(IronSourceConstants.BN_CALLBACK_LEAVE_APP, objArr);
        this.mIronsourceBanner.sendBannerAdLeftApplication();
        sendProviderEvent(IronSourceConstants.BN_INSTANCE_LEAVE_APP, bannerSmash, objArr);
    }

    private void sendMediationEvent(int i) {
        sendMediationEvent(i, null);
    }

    private void sendMediationEvent(int i, Object[][] objArr) {
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        try {
            if (this.mIronsourceBanner != null) {
                addEventSizeFields(mediationAdditionalData, this.mIronsourceBanner.getSize());
            }
            if (this.mCurrentPlacement != null) {
                mediationAdditionalData.put("placement", this.mCurrentPlacement.getPlacementName());
            }
            if (objArr != null) {
                for (Object[] objArr2 : objArr) {
                    mediationAdditionalData.put(objArr2[0].toString(), objArr2[1]);
                }
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("sendMediationEvent ");
            sb.append(Log.getStackTraceString(e));
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, mediationAdditionalData));
    }

    private void sendProviderEvent(int i, BannerSmash bannerSmash) {
        sendProviderEvent(i, bannerSmash, null);
    }

    private void addEventSizeFields(JSONObject jSONObject, ISBannerSize iSBannerSize) {
        char c;
        try {
            String description = iSBannerSize.getDescription();
            switch (description.hashCode()) {
                case -387072689:
                    if (description.equals("RECTANGLE")) {
                        c = 2;
                        break;
                    }
                case 72205083:
                    if (description.equals("LARGE")) {
                        c = 1;
                        break;
                    }
                case 79011241:
                    if (description.equals("SMART")) {
                        c = 3;
                        break;
                    }
                case 1951953708:
                    if (description.equals(AdPreferences.TYPE_BANNER)) {
                        c = 0;
                        break;
                    }
                case 1999208305:
                    if (description.equals("CUSTOM")) {
                        c = 4;
                        break;
                    }
            }
            c = 65535;
            switch (c) {
                case 0:
                    jSONObject.put("bannerAdSize", 1);
                    return;
                case 1:
                    jSONObject.put("bannerAdSize", 2);
                    return;
                case 2:
                    jSONObject.put("bannerAdSize", 3);
                    return;
                case 3:
                    jSONObject.put("bannerAdSize", 5);
                    return;
                case 4:
                    jSONObject.put("bannerAdSize", 6);
                    StringBuilder sb = new StringBuilder();
                    sb.append(iSBannerSize.getWidth());
                    sb.append(AvidJSONUtil.KEY_X);
                    sb.append(iSBannerSize.getHeight());
                    jSONObject.put("custom_banner_size", sb.toString());
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("sendProviderEvent ");
            sb2.append(Log.getStackTraceString(e));
            ironSourceLoggerManager.log(ironSourceTag, sb2.toString(), 3);
        }
    }

    private void sendProviderEvent(int i, BannerSmash bannerSmash, Object[][] objArr) {
        JSONObject providerAdditionalData = IronSourceUtils.getProviderAdditionalData(bannerSmash);
        try {
            if (this.mIronsourceBanner != null) {
                addEventSizeFields(providerAdditionalData, this.mIronsourceBanner.getSize());
            }
            if (this.mCurrentPlacement != null) {
                providerAdditionalData.put("placement", this.mCurrentPlacement.getPlacementName());
            }
            if (objArr != null) {
                for (Object[] objArr2 : objArr) {
                    providerAdditionalData.put(objArr2[0].toString(), objArr2[1]);
                }
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("sendProviderEvent ");
            sb.append(Log.getStackTraceString(e));
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, providerAdditionalData));
    }

    private void resetIteration() {
        synchronized (this.mSmashArray) {
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                ((BannerSmash) it.next()).setReadyToLoad(true);
            }
        }
    }

    private boolean loadNextSmash() {
        synchronized (this.mSmashArray) {
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                BannerSmash bannerSmash = (BannerSmash) it.next();
                if (bannerSmash.isReadyToLoad() && this.mActiveSmash != bannerSmash) {
                    if (this.mState == BANNER_STATE.FIRST_LOAD_IN_PROGRESS) {
                        sendProviderEvent(IronSourceConstants.BN_INSTANCE_LOAD, bannerSmash);
                    } else {
                        sendProviderEvent(IronSourceConstants.BN_INSTANCE_RELOAD, bannerSmash);
                    }
                    bannerSmash.loadBanner(this.mIronsourceBanner, this.mActivity, this.mAppKey, this.mUserId);
                    return true;
                }
            }
            return false;
        }
    }

    public void onPause(Activity activity) {
        synchronized (this.mSmashArray) {
            this.mIsInForeground = Boolean.valueOf(false);
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                ((BannerSmash) it.next()).onPause(activity);
            }
        }
    }

    public void onResume(Activity activity) {
        synchronized (this.mSmashArray) {
            this.mIsInForeground = Boolean.valueOf(true);
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                ((BannerSmash) it.next()).onResume(activity);
            }
        }
    }

    private void startReloadTimer() {
        try {
            stopReloadTimer();
            this.mReloadTimer = new Timer();
            this.mReloadTimer.schedule(new TimerTask() {
                public void run() {
                    BannerManager.this.onReloadTimer();
                }
            }, this.mReloadInterval * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopReloadTimer() {
        if (this.mReloadTimer != null) {
            this.mReloadTimer.cancel();
            this.mReloadTimer = null;
        }
    }

    /* access modifiers changed from: private */
    public void onReloadTimer() {
        if (this.mState != BANNER_STATE.RELOAD_IN_PROGRESS) {
            StringBuilder sb = new StringBuilder();
            sb.append("onReloadTimer wrong state=");
            sb.append(this.mState.name());
            debugLog(sb.toString());
            return;
        }
        if (this.mIsInForeground.booleanValue()) {
            sendMediationEvent(IronSourceConstants.BN_RELOAD);
            sendProviderEvent(IronSourceConstants.BN_INSTANCE_RELOAD, this.mActiveSmash);
            this.mActiveSmash.reloadBanner();
        } else {
            sendMediationEvent(3200, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_BN_RELOAD_SKIP_BACKGROUND)}});
            startReloadTimer();
        }
    }

    private AbstractAdapter getLoadedAdapterOrFetchByReflection(String str, String str2) {
        try {
            AbstractAdapter existingAdapter = IronSourceObject.getInstance().getExistingAdapter(str);
            if (existingAdapter != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("using previously loaded ");
                sb.append(str);
                debugLog(sb.toString());
                return existingAdapter;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("loading ");
            sb2.append(str);
            sb2.append(" with reflection");
            debugLog(sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("com.ironsource.adapters.");
            sb3.append(str2.toLowerCase());
            sb3.append(".");
            sb3.append(str2);
            sb3.append("Adapter");
            Class cls = Class.forName(sb3.toString());
            return (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, new Class[]{String.class}).invoke(cls, new Object[]{str});
        } catch (Exception e) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("getLoadedAdapterOrFetchByReflection ");
            sb4.append(e.getMessage());
            errorLog(sb4.toString());
            return null;
        }
    }

    private AbstractAdapter loadAdapter(ProviderSettings providerSettings) {
        String providerTypeForReflection = providerSettings.isMultipleInstances() ? providerSettings.getProviderTypeForReflection() : providerSettings.getProviderName();
        String providerTypeForReflection2 = providerSettings.getProviderTypeForReflection();
        StringBuilder sb = new StringBuilder();
        sb.append("loadAdapter(");
        sb.append(providerTypeForReflection);
        sb.append(")");
        debugLog(sb.toString());
        try {
            AbstractAdapter loadedAdapterOrFetchByReflection = getLoadedAdapterOrFetchByReflection(providerTypeForReflection, providerTypeForReflection2);
            if (loadedAdapterOrFetchByReflection == null) {
                return null;
            }
            IronSourceObject.getInstance().addToBannerAdaptersList(loadedAdapterOrFetchByReflection);
            loadedAdapterOrFetchByReflection.setLogListener(this.mLoggerManager);
            return loadedAdapterOrFetchByReflection;
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("loadAdapter(");
            sb2.append(providerTypeForReflection);
            sb2.append(") ");
            sb2.append(th.getMessage());
            errorLog(sb2.toString());
            return null;
        }
    }
}
