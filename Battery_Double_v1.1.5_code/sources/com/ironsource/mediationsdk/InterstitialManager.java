package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.IronSource.AD_UNIT;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.sdk.InterstitialManagerListener;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialApi;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialManagerListener;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus;
import com.ironsource.mediationsdk.utils.DailyCappingListener;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.smaato.sdk.core.api.VideoType;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONObject;

class InterstitialManager extends AbstractAdUnitManager implements InterstitialManagerListener, OnMediationInitializationListener, RewardedInterstitialManagerListener, RewardedInterstitialApi, DailyCappingListener {
    private final String TAG;
    private CallbackThrottler mCallbackThrottler;
    private InterstitialPlacement mCurrentPlacement;
    private boolean mDidCallLoadInterstitial;
    private boolean mDidFinishToInitInterstitial;
    private Map<String, InterstitialSmash> mInstanceIdToSmashMap;
    private CopyOnWriteArraySet<String> mInstancesToLoad;
    private ListenersWrapper mInterstitialListenersWrapper;
    private boolean mIsCurrentlyShowing;
    private boolean mIsLoadInterstitialInProgress;
    private long mLoadStartTime;
    private RewardedInterstitialListener mRewardedInterstitialListenerWrapper;
    private boolean mShouldSendAdReadyEvent;

    public void onInitSuccess(List<AD_UNIT> list, boolean z) {
    }

    InterstitialManager() {
        this.TAG = getClass().getName();
        this.mInstancesToLoad = new CopyOnWriteArraySet<>();
        this.mInstanceIdToSmashMap = new ConcurrentHashMap();
        this.mCallbackThrottler = CallbackThrottler.getInstance();
        this.mShouldSendAdReadyEvent = false;
        this.mIsLoadInterstitialInProgress = false;
        this.mDidCallLoadInterstitial = false;
        this.mDailyCappingManager = new DailyCappingManager(VideoType.INTERSTITIAL, this);
        this.mIsCurrentlyShowing = false;
    }

    public void setInterstitialListener(ListenersWrapper listenersWrapper) {
        this.mInterstitialListenersWrapper = listenersWrapper;
        this.mCallbackThrottler.setInterstitialListener(listenersWrapper);
    }

    public void setRewardedInterstitialListener(RewardedInterstitialListener rewardedInterstitialListener) {
        this.mRewardedInterstitialListenerWrapper = rewardedInterstitialListener;
    }

    public synchronized void initInterstitial(Activity activity, String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(":initInterstitial(appKey: ");
        sb.append(str);
        sb.append(", userId: ");
        sb.append(str2);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        this.mAppKey = str;
        this.mUserId = str2;
        this.mActivity = activity;
        this.mDailyCappingManager.setContext(this.mActivity);
        Iterator it = this.mSmashArray.iterator();
        int i = 0;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (this.mDailyCappingManager.shouldSendCapReleasedEvent(abstractSmash)) {
                logProviderEvent(250, abstractSmash, new Object[][]{new Object[]{"status", "false"}});
            }
            if (this.mDailyCappingManager.isCapped(abstractSmash)) {
                abstractSmash.setMediationState(MEDIATION_STATE.CAPPED_PER_DAY);
                i++;
            }
        }
        if (i == this.mSmashArray.size()) {
            this.mDidFinishToInitInterstitial = true;
        }
        for (int i2 = 0; i2 < this.mSmartLoadAmount && startNextAdapter() != null; i2++) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01bf, code lost:
        return;
     */
    public synchronized void loadInterstitial() {
        try {
            if (this.mIsCurrentlyShowing) {
                this.mLoggerManager.log(IronSourceTag.API, "loadInterstitial() cannot be invoked while showing", 3);
                return;
            }
            this.mCurrentPlacement = null;
            this.mInterstitialListenersWrapper.setInterstitialPlacement(null);
            if (!this.mIsLoadInterstitialInProgress) {
                if (!this.mCallbackThrottler.hasPendingInvocation()) {
                    EInitStatus currentInitStatus = MediationInitializer.getInstance().getCurrentInitStatus();
                    if (currentInitStatus == EInitStatus.NOT_INIT) {
                        this.mLoggerManager.log(IronSourceTag.API, "init() must be called before loadInterstitial()", 3);
                        return;
                    } else if (currentInitStatus == EInitStatus.INIT_IN_PROGRESS) {
                        if (MediationInitializer.getInstance().isInProgressMoreThan15Secs()) {
                            this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                            this.mCallbackThrottler.onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                        } else {
                            this.mLoadStartTime = new Date().getTime();
                            logMediationEvent(2001, null);
                            this.mDidCallLoadInterstitial = true;
                            this.mShouldSendAdReadyEvent = true;
                        }
                    } else if (currentInitStatus == EInitStatus.INIT_FAILED) {
                        this.mLoggerManager.log(IronSourceTag.API, "init() had failed", 3);
                        this.mCallbackThrottler.onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
                        return;
                    } else if (this.mSmashArray.size() == 0) {
                        this.mLoggerManager.log(IronSourceTag.API, "the server response does not contain interstitial data", 3);
                        this.mCallbackThrottler.onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("the server response does not contain interstitial data", "Interstitial"));
                        return;
                    } else {
                        this.mLoadStartTime = new Date().getTime();
                        logMediationEvent(2001, null);
                        this.mShouldSendAdReadyEvent = true;
                        changeStateToInitiated();
                        if (smashesCount(MEDIATION_STATE.INITIATED) != 0) {
                            this.mDidCallLoadInterstitial = true;
                            this.mIsLoadInterstitialInProgress = true;
                            Iterator it = this.mSmashArray.iterator();
                            int i = 0;
                            while (it.hasNext()) {
                                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                                if (abstractSmash.getMediationState() == MEDIATION_STATE.INITIATED) {
                                    abstractSmash.setMediationState(MEDIATION_STATE.LOAD_PENDING);
                                    loadAdapterAndSendEvent((InterstitialSmash) abstractSmash);
                                    i++;
                                    if (i >= this.mSmartLoadAmount) {
                                        return;
                                    }
                                }
                            }
                        } else if (!this.mDidFinishToInitInterstitial) {
                            this.mDidCallLoadInterstitial = true;
                            return;
                        } else {
                            IronSourceError buildGenericError = ErrorBuilder.buildGenericError("no ads to load");
                            this.mLoggerManager.log(IronSourceTag.API, buildGenericError.getErrorMessage(), 1);
                            this.mCallbackThrottler.onInterstitialAdLoadFailed(buildGenericError);
                            logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(buildGenericError.getErrorCode())}});
                            this.mShouldSendAdReadyEvent = false;
                            return;
                        }
                    }
                }
            }
            this.mLoggerManager.log(IronSourceTag.API, "Load Interstitial is already in progress", 3);
        } catch (Exception e) {
            e.printStackTrace();
            StringBuilder sb = new StringBuilder();
            sb.append("loadInterstitial exception ");
            sb.append(e.getMessage());
            IronSourceError buildLoadFailedError = ErrorBuilder.buildLoadFailedError(sb.toString());
            this.mLoggerManager.log(IronSourceTag.API, buildLoadFailedError.getErrorMessage(), 3);
            this.mCallbackThrottler.onInterstitialAdLoadFailed(buildLoadFailedError);
            if (this.mShouldSendAdReadyEvent) {
                this.mShouldSendAdReadyEvent = false;
                logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(buildLoadFailedError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, e.getMessage().substring(0, Math.min(e.getMessage().length(), 39))}});
            }
        }
    }

    public void showInterstitial(String str) {
        if (this.mIsCurrentlyShowing) {
            this.mLoggerManager.log(IronSourceTag.API, "showInterstitial() cannot be invoked while showing", 3);
        } else if (this.mShouldTrackNetworkState && this.mActivity != null && !IronSourceUtils.isNetworkConnected(this.mActivity)) {
            this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildNoInternetConnectionShowFailError("Interstitial"));
        } else if (!this.mDidCallLoadInterstitial) {
            this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", "showInterstitial failed - You need to load interstitial before showing it"));
        } else {
            for (int i = 0; i < this.mSmashArray.size(); i++) {
                AbstractSmash abstractSmash = (AbstractSmash) this.mSmashArray.get(i);
                if (abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE) {
                    CappingManager.incrementShowCounter((Context) this.mActivity, this.mCurrentPlacement);
                    if (CappingManager.isPlacementCapped((Context) this.mActivity, this.mCurrentPlacement) != ECappingStatus.NOT_CAPPED) {
                        logMediationEventWithPlacement(IronSourceConstants.IS_CAP_PLACEMENT, null);
                    }
                    logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW, abstractSmash, null);
                    this.mIsCurrentlyShowing = true;
                    ((InterstitialSmash) abstractSmash).showInterstitial();
                    if (abstractSmash.isCappedPerSession()) {
                        logProviderEvent(IronSourceConstants.IS_CAP_SESSION, abstractSmash);
                    }
                    this.mDailyCappingManager.increaseShowCounter(abstractSmash);
                    if (this.mDailyCappingManager.isCapped(abstractSmash)) {
                        abstractSmash.setMediationState(MEDIATION_STATE.CAPPED_PER_DAY);
                        logProviderEvent(250, abstractSmash, new Object[][]{new Object[]{"status", "true"}});
                    }
                    this.mDidCallLoadInterstitial = false;
                    if (!abstractSmash.isMediationAvailable()) {
                        startNextAdapter();
                    }
                    return;
                }
            }
            this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", "showInterstitial failed - No adapters ready to show"));
        }
    }

    public synchronized boolean isInterstitialReady() {
        if (this.mShouldTrackNetworkState && this.mActivity != null && !IronSourceUtils.isNetworkConnected(this.mActivity)) {
            return false;
        }
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE && ((InterstitialSmash) abstractSmash).isInterstitialReady()) {
                return true;
            }
        }
        return false;
    }

    public synchronized void onInterstitialInitSuccess(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(" :onInterstitialInitSuccess()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_SUCCESS, interstitialSmash);
        this.mDidFinishToInitInterstitial = true;
        if (this.mDidCallLoadInterstitial) {
            if (smashesCount(MEDIATION_STATE.AVAILABLE, MEDIATION_STATE.LOAD_PENDING) < this.mSmartLoadAmount) {
                interstitialSmash.setMediationState(MEDIATION_STATE.LOAD_PENDING);
                loadAdapterAndSendEvent(interstitialSmash);
            }
        }
    }

    public synchronized void onInterstitialInitFailed(IronSourceError ironSourceError, InterstitialSmash interstitialSmash) {
        try {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(interstitialSmash.getInstanceName());
            sb.append(":onInterstitialInitFailed(");
            sb.append(ironSourceError);
            sb.append(")");
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
            logProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_FAILED, interstitialSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}});
            if (smashesCount(MEDIATION_STATE.INIT_FAILED) >= this.mSmashArray.size()) {
                IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
                IronSourceTag ironSourceTag2 = IronSourceTag.NATIVE;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Smart Loading - initialization failed - no adapters are initiated and no more left to init, error: ");
                sb2.append(ironSourceError.getErrorMessage());
                ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 2);
                if (this.mDidCallLoadInterstitial) {
                    this.mCallbackThrottler.onInterstitialAdLoadFailed(ErrorBuilder.buildGenericError("no ads to show"));
                    logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_GENERIC)}});
                    this.mShouldSendAdReadyEvent = false;
                }
                this.mDidFinishToInitInterstitial = true;
            } else {
                if (startNextAdapter() == null && this.mDidCallLoadInterstitial) {
                    if (smashesCount(MEDIATION_STATE.INIT_FAILED, MEDIATION_STATE.NOT_AVAILABLE, MEDIATION_STATE.CAPPED_PER_SESSION, MEDIATION_STATE.CAPPED_PER_DAY, MEDIATION_STATE.EXHAUSTED) >= this.mSmashArray.size()) {
                        this.mCallbackThrottler.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, "No ads to show"));
                        logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
                        this.mShouldSendAdReadyEvent = false;
                    }
                }
                completeIterationRound();
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
            IronSourceTag ironSourceTag3 = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb3 = new StringBuilder();
            sb3.append("onInterstitialInitFailed(error:");
            sb3.append(ironSourceError);
            sb3.append(", ");
            sb3.append("provider:");
            sb3.append(interstitialSmash.getName());
            sb3.append(")");
            ironSourceLoggerManager3.logException(ironSourceTag3, sb3.toString(), e);
        }
        return;
    }

    public synchronized void onInterstitialAdReady(InterstitialSmash interstitialSmash, long j) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(":onInterstitialAdReady()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEvent(2003, interstitialSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
        long time = new Date().getTime() - this.mLoadStartTime;
        interstitialSmash.setMediationState(MEDIATION_STATE.AVAILABLE);
        this.mIsLoadInterstitialInProgress = false;
        if (this.mShouldSendAdReadyEvent) {
            this.mShouldSendAdReadyEvent = false;
            this.mInterstitialListenersWrapper.onInterstitialAdReady();
            logMediationEvent(2004, new Object[][]{new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(time)}});
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00f1, code lost:
        return;
     */
    public synchronized void onInterstitialAdLoadFailed(IronSourceError ironSourceError, InterstitialSmash interstitialSmash, long j) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(":onInterstitialAdLoadFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEvent(IronSourceConstants.IS_INSTANCE_LOAD_FAILED, interstitialSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage().substring(0, Math.min(ironSourceError.getErrorMessage().length(), 39))}, new Object[]{IronSourceConstants.EVENTS_DURATION, Long.valueOf(j)}});
        interstitialSmash.setMediationState(MEDIATION_STATE.NOT_AVAILABLE);
        int smashesCount = smashesCount(MEDIATION_STATE.AVAILABLE, MEDIATION_STATE.LOAD_PENDING);
        if (smashesCount < this.mSmartLoadAmount) {
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                if (abstractSmash.getMediationState() == MEDIATION_STATE.INITIATED) {
                    abstractSmash.setMediationState(MEDIATION_STATE.LOAD_PENDING);
                    loadAdapterAndSendEvent((InterstitialSmash) abstractSmash);
                    return;
                }
            }
            if (startNextAdapter() == null) {
                if (this.mDidCallLoadInterstitial) {
                    if (smashesCount + smashesCount(MEDIATION_STATE.INIT_PENDING) == 0) {
                        completeIterationRound();
                        this.mIsLoadInterstitialInProgress = false;
                        this.mCallbackThrottler.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, "No ads to show"));
                        logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
                    }
                }
            }
        }
    }

    public void onInterstitialAdOpened(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(":onInterstitialAdOpened()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_OPENED, interstitialSmash, null);
        this.mInterstitialListenersWrapper.onInterstitialAdOpened();
    }

    public void onInterstitialAdClosed(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(":onInterstitialAdClosed()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        verifyOnPauseOnResume();
        logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_CLOSED, interstitialSmash, null);
        this.mInterstitialListenersWrapper.onInterstitialAdClosed();
        this.mIsCurrentlyShowing = false;
    }

    public void onInterstitialAdShowSucceeded(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(":onInterstitialAdShowSucceeded()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_SUCCESS, interstitialSmash, null);
        Iterator it = this.mSmashArray.iterator();
        boolean z = false;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE) {
                completeAdapterShow(abstractSmash);
                z = true;
            }
        }
        if (!z && (interstitialSmash.getMediationState() == MEDIATION_STATE.CAPPED_PER_SESSION || interstitialSmash.getMediationState() == MEDIATION_STATE.EXHAUSTED || interstitialSmash.getMediationState() == MEDIATION_STATE.CAPPED_PER_DAY)) {
            completeIterationRound();
        }
        changeStateToInitiated();
        this.mInterstitialListenersWrapper.onInterstitialAdShowSucceeded();
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError, InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(":onInterstitialAdShowFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_FAILED, interstitialSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}});
        completeAdapterShow(interstitialSmash);
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            if (((AbstractSmash) it.next()).getMediationState() == MEDIATION_STATE.AVAILABLE) {
                this.mDidCallLoadInterstitial = true;
                showInterstitial(this.mCurrentPlacement.getPlacementName());
                return;
            }
        }
        this.mInterstitialListenersWrapper.onInterstitialAdShowFailed(ironSourceError);
        this.mIsCurrentlyShowing = false;
    }

    public void onInterstitialAdClicked(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(":onInterstitialAdClicked()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        logProviderEventWithPlacement(2006, interstitialSmash, null);
        this.mInterstitialListenersWrapper.onInterstitialAdClicked();
    }

    public void onInterstitialAdVisible(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(interstitialSmash.getInstanceName());
        sb.append(":onInterstitialAdVisible()");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
    }

    public void onInterstitialAdRewarded(InterstitialSmash interstitialSmash) {
        logProviderEvent(IronSourceConstants.INTERSTITIAL_AD_REWARDED, interstitialSmash, null);
        if (this.mRewardedInterstitialListenerWrapper != null) {
            this.mRewardedInterstitialListenerWrapper.onInterstitialAdRewarded();
        }
    }

    /* access modifiers changed from: 0000 */
    public void shouldTrackNetworkState(Context context, boolean z) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(" Should Track Network State: ");
        sb.append(z);
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
        this.mShouldTrackNetworkState = z;
    }

    public void onInitFailed(String str) {
        if (this.mDidCallLoadInterstitial) {
            this.mCallbackThrottler.onInterstitialAdLoadFailed(ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial"));
            this.mDidCallLoadInterstitial = false;
            this.mIsLoadInterstitialInProgress = false;
        }
    }

    public void onStillInProgressAfter15Secs() {
        if (this.mDidCallLoadInterstitial) {
            IronSourceError buildInitFailedError = ErrorBuilder.buildInitFailedError("init() had failed", "Interstitial");
            this.mCallbackThrottler.onInterstitialAdLoadFailed(buildInitFailedError);
            this.mDidCallLoadInterstitial = false;
            this.mIsLoadInterstitialInProgress = false;
            if (this.mShouldSendAdReadyEvent) {
                logMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(buildInitFailedError.getErrorCode())}});
                this.mShouldSendAdReadyEvent = false;
            }
        }
    }

    private boolean isIterationRoundComplete() {
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == MEDIATION_STATE.NOT_INITIATED || abstractSmash.getMediationState() == MEDIATION_STATE.INIT_PENDING || abstractSmash.getMediationState() == MEDIATION_STATE.INITIATED || abstractSmash.getMediationState() == MEDIATION_STATE.LOAD_PENDING) {
                return false;
            }
            if (abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE) {
                return false;
            }
        }
        return true;
    }

    private void completeIterationRound() {
        if (isIterationRoundComplete()) {
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "Reset Iteration", 0);
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                if (abstractSmash.getMediationState() == MEDIATION_STATE.EXHAUSTED) {
                    abstractSmash.completeIteration();
                }
            }
            this.mLoggerManager.log(IronSourceTag.INTERNAL, "End of Reset Iteration", 0);
        }
    }

    private void completeAdapterShow(AbstractSmash abstractSmash) {
        if (!abstractSmash.isMediationAvailable()) {
            startNextAdapter();
            completeIterationRound();
            return;
        }
        abstractSmash.setMediationState(MEDIATION_STATE.INITIATED);
    }

    private AbstractAdapter startNextAdapter() {
        AbstractAdapter abstractAdapter = null;
        int i = 0;
        for (int i2 = 0; i2 < this.mSmashArray.size() && abstractAdapter == null; i2++) {
            if (((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == MEDIATION_STATE.AVAILABLE || ((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == MEDIATION_STATE.INITIATED || ((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == MEDIATION_STATE.INIT_PENDING || ((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == MEDIATION_STATE.LOAD_PENDING) {
                i++;
                if (i >= this.mSmartLoadAmount) {
                    break;
                }
            } else if (((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == MEDIATION_STATE.NOT_INITIATED) {
                abstractAdapter = startAdapter((InterstitialSmash) this.mSmashArray.get(i2));
                if (abstractAdapter == null) {
                    ((AbstractSmash) this.mSmashArray.get(i2)).setMediationState(MEDIATION_STATE.INIT_FAILED);
                }
            }
        }
        return abstractAdapter;
    }

    private synchronized AbstractAdapter startAdapter(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(":startAdapter(");
        sb.append(interstitialSmash.getName());
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        try {
            AbstractAdapter loadedAdapterOrFetchByReflection = getLoadedAdapterOrFetchByReflection(interstitialSmash);
            if (loadedAdapterOrFetchByReflection == null) {
                return null;
            }
            IronSourceObject.getInstance().addToISAdaptersList(loadedAdapterOrFetchByReflection);
            loadedAdapterOrFetchByReflection.setLogListener(this.mLoggerManager);
            interstitialSmash.setAdapterForSmash(loadedAdapterOrFetchByReflection);
            interstitialSmash.setMediationState(MEDIATION_STATE.INIT_PENDING);
            if (this.mRewardedInterstitialListenerWrapper != null) {
                interstitialSmash.setRewardedInterstitialManagerListener(this);
            }
            setCustomParams(interstitialSmash);
            interstitialSmash.initInterstitial(this.mActivity, this.mAppKey, this.mUserId);
            return loadedAdapterOrFetchByReflection;
        } catch (Throwable th) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceTag ironSourceTag2 = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":startAdapter(");
            sb2.append(interstitialSmash.getName());
            sb2.append(")");
            ironSourceLoggerManager2.logException(ironSourceTag2, sb2.toString(), th);
            interstitialSmash.setMediationState(MEDIATION_STATE.INIT_FAILED);
            StringBuilder sb3 = new StringBuilder();
            sb3.append(interstitialSmash.getName());
            sb3.append(" initialization failed - please verify that required dependencies are in you build path.");
            this.mLoggerManager.log(IronSourceTag.API, ErrorBuilder.buildInitFailedError(sb3.toString(), "Interstitial").toString(), 2);
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void setCurrentPlacement(InterstitialPlacement interstitialPlacement) {
        this.mCurrentPlacement = interstitialPlacement;
        this.mInterstitialListenersWrapper.setInterstitialPlacement(interstitialPlacement);
    }

    private synchronized void loadAdapterAndSendEvent(InterstitialSmash interstitialSmash) {
        logProviderEvent(2002, interstitialSmash, null);
        interstitialSmash.loadInterstitial();
    }

    private synchronized void changeStateToInitiatedForInstanceId(String str) {
        AbstractSmash abstractSmash;
        Iterator it = this.mSmashArray.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            abstractSmash = (AbstractSmash) it.next();
            if (!abstractSmash.getSubProviderId().equals(str) || !(abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE || abstractSmash.getMediationState() == MEDIATION_STATE.LOAD_PENDING || abstractSmash.getMediationState() == MEDIATION_STATE.NOT_AVAILABLE)) {
            }
        }
        abstractSmash.setMediationState(MEDIATION_STATE.INITIATED);
    }

    private synchronized void changeStateToInitiated() {
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == MEDIATION_STATE.AVAILABLE || abstractSmash.getMediationState() == MEDIATION_STATE.LOAD_PENDING || abstractSmash.getMediationState() == MEDIATION_STATE.NOT_AVAILABLE) {
                abstractSmash.setMediationState(MEDIATION_STATE.INITIATED);
            }
        }
    }

    private void logMediationEvent(int i) {
        logMediationEvent(i, null);
    }

    private void logMediationEvent(int i, Object[][] objArr) {
        logMediationEvent(i, objArr, false);
    }

    private void logMediationEventWithPlacement(int i, Object[][] objArr) {
        logMediationEvent(i, objArr, true);
    }

    private void logMediationEvent(int i, Object[][] objArr, boolean z) {
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        if (z) {
            try {
                if (this.mCurrentPlacement != null && !TextUtils.isEmpty(this.mCurrentPlacement.getPlacementName())) {
                    mediationAdditionalData.put("placement", this.mCurrentPlacement.getPlacementName());
                }
            } catch (Exception e) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("InterstitialManager logMediationEvent ");
                sb.append(Log.getStackTraceString(e));
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
            }
        }
        if (objArr != null) {
            for (Object[] objArr2 : objArr) {
                mediationAdditionalData.put(objArr2[0].toString(), objArr2[1]);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, mediationAdditionalData));
    }

    private void logProviderEvent(int i, AbstractSmash abstractSmash) {
        logProviderEvent(i, abstractSmash, null);
    }

    private void logProviderEvent(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        logProviderEvent(i, abstractSmash, objArr, false);
    }

    private void logProviderEventWithPlacement(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        logProviderEvent(i, abstractSmash, objArr, true);
    }

    private void logProviderEvent(int i, AbstractSmash abstractSmash, Object[][] objArr, boolean z) {
        JSONObject providerAdditionalData = IronSourceUtils.getProviderAdditionalData(abstractSmash);
        if (z) {
            try {
                if (this.mCurrentPlacement != null && !TextUtils.isEmpty(this.mCurrentPlacement.getPlacementName())) {
                    providerAdditionalData.put("placement", this.mCurrentPlacement.getPlacementName());
                }
            } catch (Exception e) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("InterstitialManager logProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
            }
        }
        if (objArr != null) {
            for (Object[] objArr2 : objArr) {
                providerAdditionalData.put(objArr2[0].toString(), objArr2[1]);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, providerAdditionalData));
    }

    private int smashesCount(MEDIATION_STATE... mediation_stateArr) {
        int i;
        synchronized (this.mSmashArray) {
            Iterator it = this.mSmashArray.iterator();
            i = 0;
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                int i2 = i;
                for (MEDIATION_STATE mediation_state : mediation_stateArr) {
                    if (abstractSmash.getMediationState() == mediation_state) {
                        i2++;
                    }
                }
                i = i2;
            }
        }
        return i;
    }

    public void onDailyCapReleased() {
        if (this.mSmashArray != null) {
            Iterator it = this.mSmashArray.iterator();
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                if (abstractSmash.getMediationState() == MEDIATION_STATE.CAPPED_PER_DAY) {
                    logProviderEvent(250, abstractSmash, new Object[][]{new Object[]{"status", "false"}});
                    if (abstractSmash.isCappedPerSession()) {
                        abstractSmash.setMediationState(MEDIATION_STATE.CAPPED_PER_SESSION);
                    } else if (abstractSmash.isExhausted()) {
                        abstractSmash.setMediationState(MEDIATION_STATE.EXHAUSTED);
                    } else {
                        abstractSmash.setMediationState(MEDIATION_STATE.INITIATED);
                    }
                }
            }
        }
    }

    public void setDelayLoadFailureNotificationInSeconds(int i) {
        this.mCallbackThrottler.setDelayLoadFailureNotificationInSeconds(i);
    }
}
