package com.ironsource.adapters.ris;

import android.app.Activity;
import android.text.TextUtils;
import com.appodeal.ads.utils.LogConstants;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.SSAFactory;
import com.ironsource.sdk.SSAPublisher;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.listeners.OnRewardedVideoListener;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class RISAdapter extends AbstractAdapter implements OnRewardedVideoListener {
    private final String AD_VISIBLE_EVENT_NAME = "impressions";
    private final String DYNAMIC_CONTROLLER_CONFIG = RequestParameters.CONTROLLER_CONFIG;
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = "debugMode";
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private boolean hasAdAvailable = false;
    /* access modifiers changed from: private */
    public boolean mConsent;
    private boolean mDidReportInitStatus = false;
    /* access modifiers changed from: private */
    public boolean mDidSetConsent;
    /* access modifiers changed from: private */
    public SSAPublisher mSSAPublisher;

    public void fetchRewardedVideo(JSONObject jSONObject) {
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        return false;
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
    }

    public static RISAdapter startAdapter(String str) {
        return new RISAdapter(str);
    }

    private RISAdapter(String str) {
        super(str);
    }

    public String getVersion() {
        return IronSourceUtils.getSDKVersion();
    }

    public String getCoreSDKVersion() {
        return SDKUtils.getSDKVersion();
    }

    public void onResume(Activity activity) {
        if (this.mSSAPublisher != null) {
            this.mSSAPublisher.onResume(activity);
        }
    }

    public void onPause(Activity activity) {
        if (this.mSSAPublisher != null) {
            this.mSSAPublisher.onPause(activity);
        }
    }

    public void initInterstitial(final Activity activity, final String str, final String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        SDKUtils.setControllerUrl(jSONObject.optString("controllerUrl"));
        if (isAdaptersDebugEnabled()) {
            SDKUtils.setDebugMode(3);
        } else {
            SDKUtils.setDebugMode(jSONObject.optInt("debugMode", 0));
        }
        SDKUtils.setControllerConfig(jSONObject.optString(RequestParameters.CONTROLLER_CONFIG, ""));
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    RISAdapter.this.mSSAPublisher = SSAFactory.getPublisherInstance(activity);
                    if (RISAdapter.this.mDidSetConsent) {
                        RISAdapter.this.applyConsent(RISAdapter.this.mConsent);
                    }
                    SSAFactory.getPublisherInstance(activity).initRewardedVideo(str, str2, RISAdapter.this.getProviderName(), new HashMap(), RISAdapter.this);
                } catch (Exception e) {
                    RISAdapter.this.onRVInitFail(e.getMessage());
                }
            }
        });
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        if (this.hasAdAvailable) {
            Iterator it = this.mAllInterstitialSmashes.iterator();
            while (it.hasNext()) {
                InterstitialSmashListener interstitialSmashListener2 = (InterstitialSmashListener) it.next();
                if (interstitialSmashListener2 != null) {
                    interstitialSmashListener2.onInterstitialAdReady();
                }
            }
            return;
        }
        Iterator it2 = this.mAllInterstitialSmashes.iterator();
        while (it2.hasNext()) {
            InterstitialSmashListener interstitialSmashListener3 = (InterstitialSmashListener) it2.next();
            if (interstitialSmashListener3 != null) {
                interstitialSmashListener3.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("No Ads to Load"));
            }
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        this.mActiveInterstitialSmash = interstitialSmashListener;
        if (this.mSSAPublisher != null) {
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(2);
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("demandSourceName", getProviderName());
                jSONObject2.put(RequestParameters.SESSION_DEPTH, sessionDepth);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.showRewardedVideo(jSONObject2);
        } else if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, "Please call init before calling showRewardedVideo"));
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        return this.hasAdAvailable;
    }

    public void onRVInitSuccess(AdUnitsReady adUnitsReady) {
        int i;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(":onRVInitSuccess()");
        log(ironSourceTag, sb.toString(), 1);
        boolean z = false;
        try {
            i = Integer.parseInt(adUnitsReady.getNumOfAdUnits());
        } catch (NumberFormatException e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceTag.NATIVE, "onRVInitSuccess:parseInt()", e);
            i = 0;
        }
        if (i > 0) {
            z = true;
        }
        this.hasAdAvailable = z;
        if (!this.mDidReportInitStatus) {
            this.mDidReportInitStatus = true;
            Iterator it = this.mAllInterstitialSmashes.iterator();
            while (it.hasNext()) {
                InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) it.next();
                if (interstitialSmashListener != null) {
                    interstitialSmashListener.onInterstitialInitSuccess();
                }
            }
        }
    }

    public void onRVInitFail(String str) {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(":onRVInitFail()");
        log(ironSourceTag, sb.toString(), 1);
        this.hasAdAvailable = false;
        if (!this.mDidReportInitStatus) {
            this.mDidReportInitStatus = true;
            Iterator it = this.mAllInterstitialSmashes.iterator();
            while (it.hasNext()) {
                InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) it.next();
                if (interstitialSmashListener != null) {
                    interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError(str, "Interstitial"));
                }
            }
        }
    }

    public void onRVNoMoreOffers() {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(":onRVNoMoreOffers()");
        log(ironSourceTag, sb.toString(), 1);
        if (!this.mDidReportInitStatus) {
            this.mDidReportInitStatus = true;
            Iterator it = this.mAllInterstitialSmashes.iterator();
            while (it.hasNext()) {
                InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) it.next();
                if (interstitialSmashListener != null) {
                    interstitialSmashListener.onInterstitialInitSuccess();
                }
            }
        }
    }

    public void onRVAdCredited(int i) {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(":onRVAdCredited()");
        log(ironSourceTag, sb.toString(), 1);
        if (this.mRewardedInterstitial != null) {
            this.mRewardedInterstitial.onInterstitialAdRewarded();
        }
    }

    public void onRVAdClosed() {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(":onRVAdClosed()");
        log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdClosed();
        }
    }

    public void onRVAdOpened() {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(":onRVAdOpened()");
        log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdShowSucceeded();
            this.mActiveInterstitialSmash.onInterstitialAdOpened();
        }
    }

    public void onRVShowFail(String str) {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(":onRVShowFail()");
        log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, LogConstants.EVENT_SHOW_FAILED));
        }
    }

    public void onRVAdClicked() {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(":onRVAdClicked()");
        log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdClicked();
        }
    }

    public void onRVEventNotificationReceived(String str, JSONObject jSONObject) {
        if (jSONObject != null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" :onRISEventNotificationReceived: ");
            sb.append(str);
            sb.append(" extData: ");
            sb.append(jSONObject.toString());
            logger.log(ironSourceTag, sb.toString(), 1);
            if (!TextUtils.isEmpty(str) && "impressions".equals(str) && this.mActiveInterstitialSmash != null) {
                this.mActiveInterstitialSmash.onInterstitialAdVisible();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setMediationState(MEDIATION_STATE mediation_state, String str) {
        if (this.mSSAPublisher != null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" :setMediationState(RIS:(rewardedvideo)):");
            sb.append(str);
            sb.append(" , ");
            sb.append(getProviderName());
            sb.append(" , ");
            sb.append(mediation_state.getValue());
            sb.append(")");
            logger.log(ironSourceTag, sb.toString(), 1);
            this.mSSAPublisher.setMediationState("rewardedvideo", getProviderName(), mediation_state.getValue());
        }
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        this.mConsent = z;
        this.mDidSetConsent = true;
        applyConsent(z);
    }

    /* access modifiers changed from: private */
    public void applyConsent(boolean z) {
        if (this.mSSAPublisher != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(RequestParameters.GDPR_CONSENT_STATUS, String.valueOf(z));
                jSONObject.put("demandSourceName", getProviderName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.updateConsentInfo(jSONObject);
        }
    }
}
