package com.ironsource.adapters.ironsource;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.IronSourceAdInstanceBuilder;
import com.ironsource.sdk.IronSourceNetwork;
import com.ironsource.sdk.constants.Constants.JSMethods;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.utils.SDKUtils;
import com.mansoon.BatteryDouble.Config;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceAdapter extends AbstractAdapter {
    private static final int IS_LOAD_EXCEPTION = 1000;
    private static final int IS_SHOW_EXCEPTION = 1001;
    private static final int RV_LOAD_EXCEPTION = 1002;
    private static final int RV_SHOW_EXCEPTION = 1003;
    private static final String VERSION = "6.9.1";
    private final String ADM_KEY = ParametersKeys.ADM;
    private final String APPLICATION_USER_AGE_GROUP = "applicationUserAgeGroup";
    private final String APPLICATION_USER_GENDER = "applicationUserGender";
    private final String CUSTOM_SEGMENT = "custom_Segment";
    private final String DEMAND_SOURCE_NAME = "demandSourceName";
    private final String DYNAMIC_CONTROLLER_CONFIG = RequestParameters.CONTROLLER_CONFIG;
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = "debugMode";
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private final String SDK_PLUGIN_TYPE = "SDKPluginType";
    private Context mContext;
    private ConcurrentHashMap<String, IronSourceAdInstance> mDemandSourceToISAd;
    private ConcurrentHashMap<String, IronSourceAdInstance> mDemandSourceToRvAd;
    private boolean mDidInitSdk = false;
    private String mMediationSegment;
    private String mUserAgeGroup;
    private String mUserGender;

    private class IronSourceInterstitialListener implements OnInterstitialListener {
        private String mDemandSourceName;
        private InterstitialSmashListener mListener;

        IronSourceInterstitialListener(InterstitialSmashListener interstitialSmashListener, String str) {
            this.mDemandSourceName = str;
            this.mListener = interstitialSmashListener;
        }

        public void onInterstitialInitSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialInitSuccess");
            ironSourceAdapter.log(sb.toString());
        }

        public void onInterstitialInitFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialInitFailed");
            ironSourceAdapter.log(sb.toString());
        }

        public void onInterstitialLoadSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialLoadSuccess");
            ironSourceAdapter.log(sb.toString());
            this.mListener.onInterstitialAdReady();
        }

        public void onInterstitialLoadFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialLoadFailed ");
            sb.append(str);
            ironSourceAdapter.log(sb.toString());
            this.mListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(str));
        }

        public void onInterstitialOpen() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialOpen");
            ironSourceAdapter.log(sb.toString());
            this.mListener.onInterstitialAdOpened();
        }

        public void onInterstitialAdRewarded(String str, int i) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialAdRewarded demandSourceId=");
            sb.append(str);
            sb.append(" amount=");
            sb.append(i);
            ironSourceAdapter.log(sb.toString());
        }

        public void onInterstitialClose() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialClose");
            ironSourceAdapter.log(sb.toString());
            this.mListener.onInterstitialAdClosed();
        }

        public void onInterstitialShowSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialShowSuccess");
            ironSourceAdapter.log(sb.toString());
            this.mListener.onInterstitialAdShowSucceeded();
        }

        public void onInterstitialShowFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialShowFailed ");
            sb.append(str);
            ironSourceAdapter.log(sb.toString());
            this.mListener.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", str));
        }

        public void onInterstitialClick() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialClick");
            ironSourceAdapter.log(sb.toString());
            this.mListener.onInterstitialAdClicked();
        }

        public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialEventNotificationReceived eventName=");
            sb.append(str);
            ironSourceAdapter.log(sb.toString());
            this.mListener.onInterstitialAdVisible();
        }
    }

    private class IronSourceRewardedVideoListener implements OnInterstitialListener {
        private String mDemandSourceName;
        boolean mIsRvDemandOnly;
        RewardedVideoSmashListener mListener;

        IronSourceRewardedVideoListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
            this.mDemandSourceName = str;
            this.mListener = rewardedVideoSmashListener;
            this.mIsRvDemandOnly = false;
        }

        IronSourceRewardedVideoListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str, boolean z) {
            this.mDemandSourceName = str;
            this.mListener = rewardedVideoSmashListener;
            this.mIsRvDemandOnly = z;
        }

        public void onInterstitialInitSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialInitSuccess");
            ironSourceAdapter.log(sb.toString());
        }

        public void onInterstitialInitFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialInitFailed");
            ironSourceAdapter.log(sb.toString());
        }

        public void onInterstitialLoadSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialLoadSuccess");
            ironSourceAdapter.log(sb.toString());
            if (this.mIsRvDemandOnly) {
                this.mListener.onRewardedVideoLoadSuccess();
            } else {
                this.mListener.onRewardedVideoAvailabilityChanged(true);
            }
        }

        public void onInterstitialLoadFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialLoadFailed ");
            sb.append(str);
            ironSourceAdapter.log(sb.toString());
            if (this.mIsRvDemandOnly) {
                this.mListener.onRewardedVideoLoadFailed(ErrorBuilder.buildLoadFailedError(str));
            } else {
                this.mListener.onRewardedVideoAvailabilityChanged(false);
            }
        }

        public void onInterstitialOpen() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialOpen");
            ironSourceAdapter.log(sb.toString());
            this.mListener.onRewardedVideoAdOpened();
        }

        public void onInterstitialAdRewarded(String str, int i) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialAdRewarded demandSourceId=");
            sb.append(str);
            sb.append(" amount=");
            sb.append(i);
            ironSourceAdapter.log(sb.toString());
            this.mListener.onRewardedVideoAdRewarded();
        }

        public void onInterstitialClose() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialClose");
            ironSourceAdapter.log(sb.toString());
            this.mListener.onRewardedVideoAdClosed();
        }

        public void onInterstitialShowSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialShowSuccess");
            ironSourceAdapter.log(sb.toString());
        }

        public void onInterstitialShowFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append("rewardedVideoListener onInterstitialShowSuccess ");
            sb.append(str);
            ironSourceAdapter.log(sb.toString());
            this.mListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildShowFailedError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT, str));
        }

        public void onInterstitialClick() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialClick");
            ironSourceAdapter.log(sb.toString());
            this.mListener.onRewardedVideoAdClicked();
        }

        public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialEventNotificationReceived eventName=");
            sb.append(str);
            ironSourceAdapter.log(sb.toString());
            this.mListener.onRewardedVideoAdVisible();
        }
    }

    public String getVersion() {
        return "6.9.1";
    }

    public static IronSourceAdapter startAdapter(String str) {
        return new IronSourceAdapter(str);
    }

    private IronSourceAdapter(String str) {
        super(str);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(": new instance");
        log(sb.toString());
        this.mDemandSourceToISAd = new ConcurrentHashMap<>();
        this.mDemandSourceToRvAd = new ConcurrentHashMap<>();
        this.mUserAgeGroup = null;
        this.mUserGender = null;
        this.mMediationSegment = null;
    }

    public String getCoreSDKVersion() {
        return SDKUtils.getSDKVersion();
    }

    public void onPause(Activity activity) {
        IronSourceNetwork.onPause(activity);
    }

    public void onResume(Activity activity) {
        IronSourceNetwork.onResume(activity);
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(RequestParameters.GDPR_CONSENT_STATUS, String.valueOf(z));
            IronSourceNetwork.updateConsentInfo(jSONObject);
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("setConsent exception ");
            sb.append(e.getMessage());
            logError(sb.toString());
        }
    }

    public void setAge(int i) {
        if (i >= 13 && i <= 17) {
            this.mUserAgeGroup = "1";
        } else if (i >= 18 && i <= 20) {
            this.mUserAgeGroup = "2";
        } else if (i >= 21 && i <= 24) {
            this.mUserAgeGroup = "3";
        } else if (i >= 25 && i <= 34) {
            this.mUserAgeGroup = Config.DATA_HISTORY_DEFAULT;
        } else if (i >= 35 && i <= 44) {
            this.mUserAgeGroup = "5";
        } else if (i >= 45 && i <= 54) {
            this.mUserAgeGroup = "6";
        } else if (i >= 55 && i <= 64) {
            this.mUserAgeGroup = "7";
        } else if (i <= 65 || i > 120) {
            this.mUserAgeGroup = "0";
        } else {
            this.mUserAgeGroup = "8";
        }
    }

    public void setGender(String str) {
        this.mUserGender = str;
    }

    public void setMediationSegment(String str) {
        this.mMediationSegment = str;
    }

    private HashMap<String, String> getInitParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(this.mUserAgeGroup)) {
            hashMap.put("applicationUserAgeGroup", this.mUserAgeGroup);
        }
        if (!TextUtils.isEmpty(this.mUserGender)) {
            hashMap.put("applicationUserGender", this.mUserGender);
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("SDKPluginType", pluginType);
        }
        if (!TextUtils.isEmpty(this.mMediationSegment)) {
            hashMap.put("custom_Segment", this.mMediationSegment);
        }
        return hashMap;
    }

    public void preInitInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, "preInitInterstitial");
        initSDK(activity, str, str2, jSONObject);
    }

    public synchronized Map<String, Object> getIsBiddingData(JSONObject jSONObject) {
        HashMap hashMap;
        hashMap = new HashMap();
        String token = IronSourceNetwork.getToken(this.mContext);
        if (token != null) {
            hashMap.put("token", token);
        } else {
            logError("IS bidding token is null");
            hashMap.put("token", "");
        }
        return hashMap;
    }

    public void initInterstitialForBidding(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, "initInterstitialForBidding");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToISAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceInterstitialListener(interstitialSmashListener, demandSourceName)).setInAppBidding().build());
        interstitialSmashListener.onInterstitialInitSuccess();
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, JSMethods.INIT_INTERSTITIAL);
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToISAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceInterstitialListener(interstitialSmashListener, demandSourceName)).build());
        interstitialSmashListener.onInterstitialInitSuccess();
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener, String str) {
        log(jSONObject, JSMethods.LOAD_INTERSTITIAL);
        try {
            HashMap hashMap = new HashMap();
            hashMap.put(ParametersKeys.ADM, str);
            IronSourceNetwork.loadAd((IronSourceAdInstance) this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadInterstitial for bidding exception ");
            sb.append(e.getMessage());
            logError(sb.toString());
            interstitialSmashListener.onInterstitialAdLoadFailed(new IronSourceError(1000, e.getMessage()));
        }
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, JSMethods.LOAD_INTERSTITIAL);
        try {
            IronSourceNetwork.loadAd((IronSourceAdInstance) this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadInterstitial exception ");
            sb.append(e.getMessage());
            logError(sb.toString());
            interstitialSmashListener.onInterstitialAdLoadFailed(new IronSourceError(1000, e.getMessage()));
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, JSMethods.SHOW_INTERSTITIAL);
        try {
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(2);
            HashMap hashMap = new HashMap();
            hashMap.put(RequestParameters.SESSION_DEPTH, String.valueOf(sessionDepth));
            IronSourceNetwork.showAd((IronSourceAdInstance) this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("showInterstitial exception ");
            sb.append(e.getMessage());
            logError(sb.toString());
            interstitialSmashListener.onInterstitialAdShowFailed(new IronSourceError(1001, e.getMessage()));
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        IronSourceAdInstance ironSourceAdInstance = (IronSourceAdInstance) this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject));
        return ironSourceAdInstance != null && IronSourceNetwork.isAdAvailableForInstance(ironSourceAdInstance);
    }

    public synchronized Map<String, Object> getRvBiddingData(JSONObject jSONObject) {
        HashMap hashMap;
        hashMap = new HashMap();
        String token = IronSourceNetwork.getToken(this.mContext);
        if (token != null) {
            hashMap.put("token", token);
        } else {
            logError("RV bidding token is null");
            hashMap.put("token", "");
        }
        return hashMap;
    }

    public void initRvForBidding(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, "initRvForBidding");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToRvAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName)).setInAppBidding().setRewarded().build());
    }

    public void initRvForDemandOnly(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, "initRvForDemandOnly");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToRvAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName, true)).setRewarded().build());
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, JSMethods.INIT_REWARDED_VIDEO);
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToRvAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName)).setRewarded().build());
        fetchRewardedVideo(jSONObject);
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        log(jSONObject, "fetchRewardedVideo");
        IronSourceAdInstance ironSourceAdInstance = (IronSourceAdInstance) this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject));
        if (ironSourceAdInstance == null) {
            logError("fetchRewardedVideo exception: null adInstance ");
            return;
        }
        try {
            IronSourceNetwork.loadAd(ironSourceAdInstance);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("fetchRewardedVideo exception ");
            sb.append(e.getMessage());
            logError(sb.toString());
            OnInterstitialListener adListener = ironSourceAdInstance.getAdListener();
            if (adListener != null) {
                adListener.onInterstitialLoadFailed("fetch exception");
            }
        }
    }

    public void loadVideoForDemandOnly(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, "loadVideoForDemandOnly");
        try {
            IronSourceNetwork.loadAd((IronSourceAdInstance) this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadVideoForDemandOnly exception ");
            sb.append(e.getMessage());
            logError(sb.toString());
            rewardedVideoSmashListener.onRewardedVideoLoadFailed(new IronSourceError(1002, e.getMessage()));
        }
    }

    public void loadVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
        log(jSONObject, "loadVideo (RV in bidding mode)");
        try {
            IronSourceNetwork.loadAd((IronSourceAdInstance) this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadVideo exception ");
            sb.append(e.getMessage());
            logError(sb.toString());
            rewardedVideoSmashListener.onRewardedVideoLoadFailed(new IronSourceError(1002, e.getMessage()));
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, JSMethods.SHOW_REWARDED_VIDEO);
        try {
            IronSourceAdInstance ironSourceAdInstance = (IronSourceAdInstance) this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject));
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(1);
            HashMap hashMap = new HashMap();
            hashMap.put(RequestParameters.SESSION_DEPTH, String.valueOf(sessionDepth));
            IronSourceNetwork.showAd(ironSourceAdInstance, hashMap);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("showRewardedVideo exception ");
            sb.append(e.getMessage());
            logError(sb.toString());
            rewardedVideoSmashListener.onRewardedVideoAdShowFailed(new IronSourceError(1003, e.getMessage()));
        }
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        IronSourceAdInstance ironSourceAdInstance = (IronSourceAdInstance) this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject));
        return ironSourceAdInstance != null && IronSourceNetwork.isAdAvailableForInstance(ironSourceAdInstance);
    }

    private synchronized void initSDK(Activity activity, String str, String str2, JSONObject jSONObject) {
        if (!this.mDidInitSdk) {
            this.mDidInitSdk = true;
            if (isAdaptersDebugEnabled()) {
                SDKUtils.setDebugMode(3);
            } else {
                SDKUtils.setDebugMode(jSONObject.optInt("debugMode", 0));
            }
            SDKUtils.setControllerUrl(jSONObject.optString("controllerUrl"));
            SDKUtils.setControllerConfig(jSONObject.optString(RequestParameters.CONTROLLER_CONFIG, ""));
            this.mContext = activity.getApplicationContext();
            IronSourceNetwork.initSDK(activity, str, str2, getInitParams());
        }
    }

    private String getDemandSourceName(JSONObject jSONObject) {
        if (!TextUtils.isEmpty(jSONObject.optString("demandSourceName"))) {
            return jSONObject.optString("demandSourceName");
        }
        return getProviderName();
    }

    private void logError(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("IronSourceAdapter: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 3);
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("IronSourceAdapter: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void log(JSONObject jSONObject, String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("IronSourceAdapter ");
        sb.append(getDemandSourceName(jSONObject));
        sb.append(": ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }
}
