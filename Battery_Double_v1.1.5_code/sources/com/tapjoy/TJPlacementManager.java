package com.tapjoy;

import android.content.Context;
import com.tapjoy.internal.az;
import com.tapjoy.internal.gw;
import com.tapjoy.internal.hf;
import com.tapjoy.internal.jr;

public class TJPlacementManager {

    /* renamed from: a reason: collision with root package name */
    private static final az f4262a = az.a();
    private static int b = 0;
    private static int c = 0;
    private static int d = 3;
    private static int e = 3;

    public static TJPlacement createPlacement(Context context, String str, boolean z, TJPlacementListener tJPlacementListener) {
        TJCorePlacement a2 = a(str, null, null, z, false);
        a2.j = z;
        a2.c.setPlacementType("sdk");
        a2.setContext(context);
        return new TJPlacement(a2, tJPlacementListener);
    }

    public static TJPlacement a(String str, String str2, String str3, TJPlacementListener tJPlacementListener) {
        TJPlacement tJPlacement;
        synchronized (f4262a) {
            tJPlacement = new TJPlacement(a(str, str2, str3, false, false), tJPlacementListener);
        }
        return tJPlacement;
    }

    public static TJPlacement b(String str, String str2, String str3, TJPlacementListener tJPlacementListener) {
        TJPlacement tJPlacement;
        synchronized (f4262a) {
            tJPlacement = new TJPlacement(a(str, str2, str3, false, true), tJPlacementListener);
        }
        return tJPlacement;
    }

    static TJCorePlacement a(String str) {
        TJCorePlacement tJCorePlacement;
        synchronized (f4262a) {
            tJCorePlacement = (TJCorePlacement) f4262a.get(str);
        }
        return tJCorePlacement;
    }

    public static void setCachedPlacementLimit(int i) {
        d = i;
    }

    public static void setPreRenderedPlacementLimit(int i) {
        e = i;
    }

    public static int getCachedPlacementLimit() {
        return d;
    }

    public static int getPreRenderedPlacementLimit() {
        return e;
    }

    public static int getCachedPlacementCount() {
        return b;
    }

    public static int getPreRenderedPlacementCount() {
        return c;
    }

    public static boolean canCachePlacement() {
        return getCachedPlacementCount() < getCachedPlacementLimit();
    }

    public static boolean canPreRenderPlacement() {
        return getPreRenderedPlacementCount() < getPreRenderedPlacementLimit();
    }

    public static void incrementPlacementCacheCount() {
        int i = b + 1;
        b = i;
        if (i > d) {
            b = d;
        }
        printPlacementCacheInformation();
    }

    public static void decrementPlacementCacheCount() {
        int i = b - 1;
        b = i;
        if (i < 0) {
            b = 0;
        }
        printPlacementCacheInformation();
    }

    public static void incrementPlacementPreRenderCount() {
        int i = c + 1;
        c = i;
        if (i > e) {
            c = e;
        }
    }

    public static void decrementPlacementPreRenderCount() {
        int i = c - 1;
        c = i;
        if (i < 0) {
            c = 0;
        }
    }

    public static void printPlacementCacheInformation() {
        StringBuilder sb = new StringBuilder("Space available in placement cache: ");
        sb.append(b);
        sb.append(" out of ");
        sb.append(d);
        TapjoyLog.i("TJPlacementManager", sb.toString());
    }

    public static void printPlacementPreRenderInformation() {
        StringBuilder sb = new StringBuilder("Space available for placement pre-render: ");
        sb.append(c);
        sb.append(" out of ");
        sb.append(e);
        TapjoyLog.i("TJPlacementManager", sb.toString());
    }

    public static void dismissContentShowing(boolean z) {
        if (z) {
            TJAdUnitActivity.a();
        }
        hf.a();
        gw.a();
    }

    static TJCorePlacement a(String str, String str2, String str3, boolean z, boolean z2) {
        TJCorePlacement a2;
        StringBuilder sb = new StringBuilder();
        sb.append(z ? "!SYSTEM!" : "");
        sb.append(!jr.c(str) ? str : "");
        if (jr.c(str2)) {
            str2 = "";
        }
        sb.append(str2);
        if (jr.c(str3)) {
            str3 = "";
        }
        sb.append(str3);
        sb.append(Boolean.toString(z2));
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder("TJCorePlacement key=");
        sb3.append(sb2);
        TapjoyLog.d("TJPlacementManager", sb3.toString());
        synchronized (f4262a) {
            a2 = a(sb2);
            if (a2 == null) {
                a2 = new TJCorePlacement(str, sb2, z2);
                f4262a.put(sb2, a2);
                StringBuilder sb4 = new StringBuilder("Created TJCorePlacement with GUID: ");
                sb4.append(a2.d);
                TapjoyLog.d("TJPlacementManager", sb4.toString());
            }
        }
        return a2;
    }
}
