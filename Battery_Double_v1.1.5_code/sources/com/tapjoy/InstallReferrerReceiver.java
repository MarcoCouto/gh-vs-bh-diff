package com.tapjoy;

import android.content.Context;
import android.content.Intent;
import com.tapjoy.internal.ha;
import com.tapjoy.internal.j;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class InstallReferrerReceiver extends j {
    public void onReceive(Context context, Intent intent) {
        String a2 = ha.a(context, intent);
        int a3 = a(context, intent);
        if (intent.getBooleanExtra("fiverocks:verify", false) && isOrderedBroadcast()) {
            setResultCode(a3 + 1);
            if (a2 != null) {
                try {
                    StringBuilder sb = new StringBuilder("http://play.google.com/store/apps/details?id=");
                    sb.append(context.getPackageName());
                    sb.append("&referrer=");
                    sb.append(URLEncoder.encode(a2, "UTF-8"));
                    setResultData(sb.toString());
                } catch (UnsupportedEncodingException unused) {
                }
            }
        }
    }
}
