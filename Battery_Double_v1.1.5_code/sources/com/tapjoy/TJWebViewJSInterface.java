package com.tapjoy;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.json.JSONArray;
import org.json.JSONObject;

public class TJWebViewJSInterface {

    /* renamed from: a reason: collision with root package name */
    WebView f4275a;
    TJWebViewJSInterfaceListener b;
    private final ConcurrentLinkedQueue c = new ConcurrentLinkedQueue();
    private boolean d;

    @TargetApi(19)
    class a extends AsyncTask {

        /* renamed from: a reason: collision with root package name */
        WebView f4276a;

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
            return ((String[]) objArr)[0];
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            String str = (String) obj;
            if (this.f4276a != null) {
                if (!str.startsWith("javascript:") || VERSION.SDK_INT < 19) {
                    try {
                        this.f4276a.loadUrl(str);
                    } catch (Exception e) {
                        ErrorType errorType = ErrorType.INTERNAL_ERROR;
                        StringBuilder sb = new StringBuilder("Exception in loadUrl. Device not supported. ");
                        sb.append(e.toString());
                        TapjoyLog.e("TJWebViewJSInterface", new TapjoyErrorMessage(errorType, sb.toString()));
                    }
                } else {
                    try {
                        this.f4276a.evaluateJavascript(str.replaceFirst("javascript:", ""), null);
                    } catch (Exception e2) {
                        ErrorType errorType2 = ErrorType.INTERNAL_ERROR;
                        StringBuilder sb2 = new StringBuilder("Exception in evaluateJavascript. Device not supported. ");
                        sb2.append(e2.toString());
                        TapjoyLog.e("TJWebViewJSInterface", new TapjoyErrorMessage(errorType2, sb2.toString()));
                    }
                }
            }
        }

        public a(WebView webView) {
            this.f4276a = webView;
        }
    }

    public TJWebViewJSInterface(WebView webView, TJWebViewJSInterfaceListener tJWebViewJSInterfaceListener) {
        this.f4275a = webView;
        this.b = tJWebViewJSInterfaceListener;
    }

    @JavascriptInterface
    public void dispatchMethod(String str) {
        StringBuilder sb = new StringBuilder("dispatchMethod params: ");
        sb.append(str);
        TapjoyLog.d("TJWebViewJSInterface", sb.toString());
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getJSONObject("data").getString("method");
            StringBuilder sb2 = new StringBuilder("method: ");
            sb2.append(string);
            TapjoyLog.d("TJWebViewJSInterface", sb2.toString());
            if (this.b != null) {
                this.b.onDispatchMethod(string, jSONObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callback(ArrayList arrayList, String str, String str2) {
        try {
            callbackToJavaScript(new JSONArray(arrayList), str, str2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callback(Map map, String str, String str2) {
        try {
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(new JSONObject(map));
            callbackToJavaScript(jSONArray, str, str2);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("Exception in callback to JS: ");
            sb.append(e.toString());
            TapjoyLog.e("TJWebViewJSInterface", sb.toString());
            e.printStackTrace();
        }
    }

    public void callbackToJavaScript(Object obj, String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(String.ARGUMENTS, obj);
            if (str != null && str.length() > 0) {
                jSONObject.put("method", str);
            }
            JSONObject jSONObject2 = new JSONObject();
            if (str2 != null && str2.length() > 0) {
                jSONObject2.put(String.CALLBACK_ID, str2);
            }
            jSONObject2.put("data", jSONObject);
            StringBuilder sb = new StringBuilder("javascript:if(window.AndroidWebViewJavascriptBridge) AndroidWebViewJavascriptBridge._handleMessageFromAndroid('");
            sb.append(jSONObject2);
            sb.append("');");
            String sb2 = sb.toString();
            if (!this.d) {
                this.c.add(sb2);
                return;
            }
            new a(this.f4275a).execute(new String[]{sb2});
        } catch (Exception e) {
            StringBuilder sb3 = new StringBuilder("Exception in callback to JS: ");
            sb3.append(e.toString());
            TapjoyLog.e("TJWebViewJSInterface", sb3.toString());
            e.printStackTrace();
        }
    }

    public void flushMessageQueue() {
        if (!this.d) {
            while (true) {
                String str = (String) this.c.poll();
                if (str != null) {
                    new a(this.f4275a).execute(new String[]{str});
                } else {
                    this.d = true;
                    return;
                }
            }
        }
    }
}
