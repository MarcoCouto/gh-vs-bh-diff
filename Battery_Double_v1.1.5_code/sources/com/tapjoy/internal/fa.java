package com.tapjoy.internal;

public final class fa extends ei {
    public static final ek c = new b();
    public static final Long d = Long.valueOf(0);
    public final String e;
    public final Long f;

    public static final class a extends com.tapjoy.internal.ei.a {
        public String c;
        public Long d;

        public final fa b() {
            if (this.c != null && this.d != null) {
                return new fa(this.c, this.d, super.a());
            }
            throw ep.a(this.c, "name", this.d, "value");
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            fa faVar = (fa) obj;
            return ek.p.a(1, (Object) faVar.e) + ek.i.a(2, (Object) faVar.f) + faVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            fa faVar = (fa) obj;
            ek.p.a(emVar, 1, faVar.e);
            ek.i.a(emVar, 2, faVar.f);
            emVar.a(faVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, fa.class);
        }

        public final /* synthetic */ Object a(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            aVar.c = (String) ek.p.a(elVar);
                            break;
                        case 2:
                            aVar.d = (Long) ek.i.a(elVar);
                            break;
                        default:
                            eh c = elVar.c();
                            aVar.a(b, c, c.a().a(elVar));
                            break;
                    }
                } else {
                    elVar.a(a2);
                    return aVar.b();
                }
            }
        }
    }

    public fa(String str, Long l) {
        this(str, l, iv.b);
    }

    public fa(String str, Long l, iv ivVar) {
        super(c, ivVar);
        this.e = str;
        this.f = l;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fa)) {
            return false;
        }
        fa faVar = (fa) obj;
        return a().equals(faVar.a()) && this.e.equals(faVar.e) && this.f.equals(faVar.f);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int hashCode = (((a().hashCode() * 37) + this.e.hashCode()) * 37) + this.f.hashCode();
        this.b = hashCode;
        return hashCode;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(", name=");
        sb.append(this.e);
        sb.append(", value=");
        sb.append(this.f);
        StringBuilder replace = sb.replace(0, 2, "EventValue{");
        replace.append('}');
        return replace.toString();
    }
}
