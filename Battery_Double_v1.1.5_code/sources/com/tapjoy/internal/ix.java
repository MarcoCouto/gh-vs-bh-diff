package com.tapjoy.internal;

final class ix implements it {

    /* renamed from: a reason: collision with root package name */
    public final is f4510a = new is();
    public final jc b;
    boolean c;

    ix(jc jcVar) {
        if (jcVar != null) {
            this.b = jcVar;
            return;
        }
        throw new IllegalArgumentException("sink == null");
    }

    public final void a(is isVar, long j) {
        if (!this.c) {
            this.f4510a.a(isVar, j);
            b();
            return;
        }
        throw new IllegalStateException("closed");
    }

    public final it b(iv ivVar) {
        if (!this.c) {
            this.f4510a.b(ivVar);
            return b();
        }
        throw new IllegalStateException("closed");
    }

    public final it b(String str) {
        if (!this.c) {
            this.f4510a.b(str);
            return b();
        }
        throw new IllegalStateException("closed");
    }

    public final it e(int i) {
        if (!this.c) {
            this.f4510a.e(i);
            return b();
        }
        throw new IllegalStateException("closed");
    }

    public final it d(int i) {
        if (!this.c) {
            this.f4510a.d(i);
            return b();
        }
        throw new IllegalStateException("closed");
    }

    public final it f(long j) {
        if (!this.c) {
            this.f4510a.f(j);
            return b();
        }
        throw new IllegalStateException("closed");
    }

    private it b() {
        if (!this.c) {
            is isVar = this.f4510a;
            long j = isVar.b;
            if (j == 0) {
                j = 0;
            } else {
                iz izVar = isVar.f4505a.g;
                if (izVar.c < 8192 && izVar.e) {
                    j -= (long) (izVar.c - izVar.b);
                }
            }
            if (j > 0) {
                this.b.a(this.f4510a, j);
            }
            return this;
        }
        throw new IllegalStateException("closed");
    }

    public final it a() {
        if (!this.c) {
            long j = this.f4510a.b;
            if (j > 0) {
                this.b.a(this.f4510a, j);
            }
            return this;
        }
        throw new IllegalStateException("closed");
    }

    public final void flush() {
        if (!this.c) {
            if (this.f4510a.b > 0) {
                this.b.a(this.f4510a, this.f4510a.b);
            }
            this.b.flush();
            return;
        }
        throw new IllegalStateException("closed");
    }

    public final void close() {
        if (!this.c) {
            Throwable th = null;
            try {
                if (this.f4510a.b > 0) {
                    this.b.a(this.f4510a, this.f4510a.b);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.b.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.c = true;
            if (th != null) {
                jf.a(th);
            }
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("buffer(");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }
}
