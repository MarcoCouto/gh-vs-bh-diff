package com.tapjoy.internal;

public final class fc extends ei {
    public static final ek c = new b();
    public final fb d;
    public final ev e;
    public final fi f;

    public static final class a extends com.tapjoy.internal.ei.a {
        public fb c;
        public ev d;
        public fi e;

        public final fc b() {
            return new fc(this.c, this.d, this.e, super.a());
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            fc fcVar = (fc) obj;
            int i = 0;
            int a2 = (fcVar.d != null ? fb.c.a(1, (Object) fcVar.d) : 0) + (fcVar.e != null ? ev.c.a(2, (Object) fcVar.e) : 0);
            if (fcVar.f != null) {
                i = fi.c.a(3, (Object) fcVar.f);
            }
            return a2 + i + fcVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            fc fcVar = (fc) obj;
            if (fcVar.d != null) {
                fb.c.a(emVar, 1, fcVar.d);
            }
            if (fcVar.e != null) {
                ev.c.a(emVar, 2, fcVar.e);
            }
            if (fcVar.f != null) {
                fi.c.a(emVar, 3, fcVar.f);
            }
            emVar.a(fcVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, fc.class);
        }

        public final /* synthetic */ Object a(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            aVar.c = (fb) fb.c.a(elVar);
                            break;
                        case 2:
                            aVar.d = (ev) ev.c.a(elVar);
                            break;
                        case 3:
                            aVar.e = (fi) fi.c.a(elVar);
                            break;
                        default:
                            eh c = elVar.c();
                            aVar.a(b, c, c.a().a(elVar));
                            break;
                    }
                } else {
                    elVar.a(a2);
                    return aVar.b();
                }
            }
        }
    }

    public fc(fb fbVar, ev evVar, fi fiVar) {
        this(fbVar, evVar, fiVar, iv.b);
    }

    public fc(fb fbVar, ev evVar, fi fiVar, iv ivVar) {
        super(c, ivVar);
        this.d = fbVar;
        this.e = evVar;
        this.f = fiVar;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fc)) {
            return false;
        }
        fc fcVar = (fc) obj;
        return a().equals(fcVar.a()) && ep.a((Object) this.d, (Object) fcVar.d) && ep.a((Object) this.e, (Object) fcVar.e) && ep.a((Object) this.f, (Object) fcVar.f);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        int hashCode = ((((a().hashCode() * 37) + (this.d != null ? this.d.hashCode() : 0)) * 37) + (this.e != null ? this.e.hashCode() : 0)) * 37;
        if (this.f != null) {
            i2 = this.f.hashCode();
        }
        int i3 = hashCode + i2;
        this.b = i3;
        return i3;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.d != null) {
            sb.append(", info=");
            sb.append(this.d);
        }
        if (this.e != null) {
            sb.append(", app=");
            sb.append(this.e);
        }
        if (this.f != null) {
            sb.append(", user=");
            sb.append(this.f);
        }
        StringBuilder replace = sb.replace(0, 2, "InfoSet{");
        replace.append('}');
        return replace.toString();
    }
}
