package com.tapjoy.internal;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;

public final class bh implements bl {

    /* renamed from: a reason: collision with root package name */
    private final StringWriter f4321a = new StringWriter();
    private final bt b = new bt(this.f4321a);

    public final String toString() {
        try {
            this.b.f4332a.flush();
            return this.f4321a.toString();
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final void a(Writer writer) {
        try {
            this.b.f4332a.flush();
            writer.write(this.f4321a.toString());
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh a() {
        try {
            this.b.a();
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh b() {
        try {
            this.b.b();
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh c() {
        try {
            this.b.c();
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh d() {
        try {
            this.b.d();
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh a(String str) {
        try {
            this.b.a(str);
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh a(bl blVar) {
        try {
            this.b.a(blVar);
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh b(String str) {
        try {
            this.b.b(str);
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh a(long j) {
        try {
            this.b.a(j);
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh a(Number number) {
        try {
            this.b.a(number);
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    private bh b(Object obj) {
        try {
            this.b.a(obj);
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh a(Collection collection) {
        try {
            this.b.a(collection);
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public final bh a(Map map) {
        try {
            this.b.a(map);
            return this;
        } catch (IOException e) {
            throw js.a(e);
        }
    }

    public static String a(Object obj) {
        return new bh().b(obj).toString();
    }
}
