package com.tapjoy.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public final class ip extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    private hs f4500a;
    /* access modifiers changed from: private */
    public a b;
    private aa c = aa.UNSPECIFIED;
    private int d = 0;
    private int e = 0;
    private ib f = null;
    private ArrayList g = null;
    private ArrayList h = null;

    public interface a {
        void a();

        void a(ia iaVar);
    }

    public ip(Context context, hs hsVar, a aVar) {
        super(context);
        this.f4500a = hsVar;
        this.b = aVar;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.b.a();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0091  */
    public final void onMeasure(int i, int i2) {
        float f2;
        float f3;
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        if (size >= size2) {
            if (this.c != aa.LANDSCAPE) {
                this.c = aa.LANDSCAPE;
                a();
            }
        } else if (this.c != aa.PORTRAIT) {
            this.c = aa.PORTRAIT;
            a();
        }
        if (!(this.d == size && this.e == size2)) {
            this.d = size;
            this.e = size2;
            float f4 = (float) size;
            float f5 = (float) size2;
            float f6 = 0.0f;
            if (!(this.f == null || this.f.b == null)) {
                float f7 = ((this.f.b.y * f4) / this.f.b.x) / f5;
                if (f7 < 1.0f) {
                    f2 = (this.f.b.y * f4) / this.f.b.x;
                    f3 = (f5 - f2) / 2.0f;
                    for (View view : ac.a(this)) {
                        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                        ia iaVar = (ia) view.getTag();
                        float a2 = iaVar.f4487a.a(f4, f2);
                        float a3 = iaVar.b.a(f4, f2);
                        float a4 = iaVar.c.a(f4, f2);
                        float a5 = iaVar.d.a(f4, f2);
                        int i3 = iaVar.e;
                        int i4 = iaVar.f;
                        if (i3 == 14) {
                            a2 += (f4 - a4) / 2.0f;
                            i3 = 9;
                        }
                        if (i4 == 15) {
                            a3 += (f2 - a5) / 2.0f;
                            i4 = 10;
                        }
                        layoutParams.addRule(i3, -1);
                        layoutParams.addRule(i4, -1);
                        layoutParams.width = Math.round(a4);
                        layoutParams.height = Math.round(a5);
                        if (i3 == 9) {
                            layoutParams.leftMargin = Math.round(a2 + f6);
                        } else if (i3 == 11) {
                            layoutParams.rightMargin = Math.round(a2 + f6);
                        }
                        if (i4 == 10) {
                            layoutParams.topMargin = Math.round(a3 + f3);
                        } else if (i4 == 12) {
                            layoutParams.bottomMargin = Math.round(a3 + f3);
                        }
                    }
                } else if (f7 > 1.0f) {
                    float f8 = (this.f.b.x * f5) / this.f.b.y;
                    f6 = (f4 - f8) / 2.0f;
                    f4 = f8;
                }
            }
            f2 = f5;
            f3 = 0.0f;
            for (View view2 : ac.a(this)) {
            }
        }
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public final void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (i == 0) {
            if (this.h != null) {
                Iterator it = this.h.iterator();
                while (it.hasNext()) {
                    ih ihVar = (ih) ((WeakReference) it.next()).get();
                    if (ihVar != null) {
                        ihVar.setVisibility(4);
                        ihVar.b();
                    }
                }
            }
            if (this.g != null) {
                Iterator it2 = this.g.iterator();
                while (it2.hasNext()) {
                    ih ihVar2 = (ih) ((WeakReference) it2.next()).get();
                    if (ihVar2 != null) {
                        ihVar2.setVisibility(0);
                        ihVar2.a();
                    }
                }
            }
        } else {
            if (this.g != null) {
                Iterator it3 = this.g.iterator();
                while (it3.hasNext()) {
                    ih ihVar3 = (ih) ((WeakReference) it3.next()).get();
                    if (ihVar3 != null) {
                        ihVar3.b();
                    }
                }
            }
            if (this.h != null) {
                Iterator it4 = this.h.iterator();
                while (it4.hasNext()) {
                    ih ihVar4 = (ih) ((WeakReference) it4.next()).get();
                    if (ihVar4 != null) {
                        ihVar4.b();
                    }
                }
            }
        }
    }

    private void a() {
        ih ihVar;
        ih ihVar2;
        Bitmap bitmap;
        Iterator it = this.f4500a.f4476a.iterator();
        ib ibVar = null;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ib ibVar2 = (ib) it.next();
            if (ibVar2.f4488a == this.c) {
                ibVar = ibVar2;
                break;
            } else if (ibVar2.f4488a == aa.UNSPECIFIED) {
                ibVar = ibVar2;
            }
        }
        removeAllViews();
        if (this.g != null) {
            Iterator it2 = this.g.iterator();
            while (it2.hasNext()) {
                ih ihVar3 = (ih) ((WeakReference) it2.next()).get();
                if (ihVar3 != null) {
                    ihVar3.c();
                }
            }
            this.g.clear();
        }
        if (this.h != null) {
            Iterator it3 = this.h.iterator();
            while (it3.hasNext()) {
                ih ihVar4 = (ih) ((WeakReference) it3.next()).get();
                if (ihVar4 != null) {
                    ihVar4.c();
                }
            }
            this.h.clear();
        }
        if (ibVar != null) {
            this.f = ibVar;
            Context context = getContext();
            Iterator it4 = ibVar.c.iterator();
            while (it4.hasNext()) {
                ia iaVar = (ia) it4.next();
                RelativeLayout relativeLayout = new RelativeLayout(context);
                if (iaVar.l.c != null) {
                    ih ihVar5 = new ih(context);
                    ihVar5.setScaleType(ScaleType.FIT_XY);
                    ihVar5.a(iaVar.l.d, iaVar.l.c);
                    if (this.g == null) {
                        this.g = new ArrayList();
                    }
                    this.g.add(new WeakReference(ihVar5));
                    ihVar = ihVar5;
                } else {
                    ihVar = null;
                }
                if (iaVar.m == null || iaVar.m.c == null) {
                    ihVar2 = null;
                } else {
                    ih ihVar6 = new ih(context);
                    ihVar6.setScaleType(ScaleType.FIT_XY);
                    ihVar6.a(iaVar.m.d, iaVar.m.c);
                    if (this.h == null) {
                        this.h = new ArrayList();
                    }
                    this.h.add(new WeakReference(ihVar6));
                    ihVar2 = ihVar6;
                }
                LayoutParams layoutParams = new LayoutParams(0, 0);
                LayoutParams layoutParams2 = new LayoutParams(-1, -1);
                Bitmap bitmap2 = iaVar.l.b;
                if (iaVar.m != null) {
                    bitmap = iaVar.m.b;
                } else {
                    bitmap = null;
                }
                final BitmapDrawable bitmapDrawable = bitmap2 != null ? new BitmapDrawable(null, bitmap2) : null;
                final BitmapDrawable bitmapDrawable2 = bitmap != null ? new BitmapDrawable(null, bitmap) : null;
                if (bitmapDrawable != null) {
                    ab.a(relativeLayout, bitmapDrawable);
                }
                if (ihVar != null) {
                    relativeLayout.addView(ihVar, layoutParams2);
                    ihVar.a();
                }
                if (ihVar2 != null) {
                    relativeLayout.addView(ihVar2, layoutParams2);
                    ihVar2.setVisibility(4);
                }
                final ih ihVar7 = ihVar2;
                final ih ihVar8 = ihVar;
                AnonymousClass1 r0 = new OnTouchListener() {
                    public final boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == 0) {
                            if (!(ihVar7 == null && bitmapDrawable2 == null)) {
                                if (ihVar8 != null) {
                                    ihVar8.b();
                                    ihVar8.setVisibility(4);
                                }
                                ab.a(view, null);
                            }
                            if (bitmapDrawable2 != null) {
                                ab.a(view, bitmapDrawable2);
                            } else if (ihVar7 != null) {
                                ihVar7.setVisibility(0);
                                ihVar7.a();
                            }
                        } else {
                            boolean z = true;
                            if (motionEvent.getAction() == 1) {
                                float x = motionEvent.getX();
                                float y = motionEvent.getY();
                                if (x >= 0.0f && x < ((float) view.getWidth()) && y >= 0.0f && y < ((float) view.getHeight())) {
                                    z = false;
                                }
                                if (z) {
                                    if (bitmapDrawable != null) {
                                        ab.a(view, bitmapDrawable);
                                    } else if (bitmapDrawable2 != null) {
                                        ab.a(view, null);
                                    }
                                }
                                if (ihVar7 != null) {
                                    ihVar7.b();
                                    ihVar7.setVisibility(4);
                                }
                                if (!((ihVar7 == null && bitmapDrawable2 == null) || ihVar8 == null || !z)) {
                                    ihVar8.setVisibility(0);
                                    ihVar8.a();
                                }
                            }
                        }
                        return false;
                    }
                };
                relativeLayout.setOnTouchListener(r0);
                final RelativeLayout relativeLayout2 = relativeLayout;
                final ia iaVar2 = iaVar;
                AnonymousClass2 r02 = new OnClickListener() {
                    public final void onClick(View view) {
                        if (ihVar7 != null) {
                            ihVar7.b();
                            relativeLayout2.removeView(ihVar7);
                        }
                        if (ihVar8 != null) {
                            ihVar8.b();
                            relativeLayout2.removeView(ihVar8);
                        }
                        ip.this.b.a(iaVar2);
                    }
                };
                relativeLayout.setOnClickListener(r02);
                relativeLayout.setTag(iaVar);
                addView(relativeLayout, layoutParams);
            }
        }
    }
}
