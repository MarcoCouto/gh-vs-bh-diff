package com.tapjoy.internal;

import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.tapjoy.TapjoyLog;
import com.tapjoy.internal.gg.a;
import java.util.HashMap;
import java.util.Map;

public abstract class fr {
    private static final String c = "fr";

    /* renamed from: a reason: collision with root package name */
    public final Map f4410a = new HashMap();
    public final Map b = new HashMap();

    protected fr(String str, String str2, String str3) {
        this.f4410a.put("placement", str);
        this.f4410a.put("placement_type", str2);
        this.f4410a.put(Param.CONTENT_TYPE, str3);
    }

    /* access modifiers changed from: protected */
    public final a a(String str, Map map, Map map2) {
        a b2 = gg.e(str).a().a(this.f4410a).a(map).b(map2);
        this.b.put(str, b2);
        return b2;
    }

    public final void a(String str, Object obj) {
        this.f4410a.put(str, obj);
    }

    /* access modifiers changed from: protected */
    public final a b(String str, Map map, Map map2) {
        a aVar = !al.a(str) ? (a) this.b.remove(str) : null;
        if (aVar == null) {
            String str2 = c;
            StringBuilder sb = new StringBuilder("Error when calling endTrackingEvent -- ");
            sb.append(str);
            sb.append(" tracking has not been started.");
            TapjoyLog.e(str2, sb.toString());
        } else {
            aVar.a(this.f4410a).a(map).b(map2).b().c();
        }
        return aVar;
    }

    public final a a() {
        return a("Content.rendered", null, null);
    }

    public final a b() {
        return b("Content.rendered", null, null);
    }
}
