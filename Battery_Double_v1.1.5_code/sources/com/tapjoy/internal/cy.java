package com.tapjoy.internal;

import java.net.URL;

public final class cy {

    /* renamed from: a reason: collision with root package name */
    public final String f4352a;
    public final URL b;
    public final String c;

    private cy(String str, URL url, String str2) {
        this.f4352a = str;
        this.b = url;
        this.c = str2;
    }

    public static cy a(String str, URL url, String str2) {
        dp.a(str, "VendorKey is null or empty");
        dp.a((Object) url, "ResourceURL is null");
        dp.a(str2, "VerificationParameters is null or empty");
        return new cy(str, url, str2);
    }

    public static cy a(URL url) {
        dp.a((Object) url, "ResourceURL is null");
        return new cy(null, url, null);
    }
}
