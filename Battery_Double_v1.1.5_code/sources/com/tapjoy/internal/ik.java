package com.tapjoy.internal;

import android.content.Context;
import com.smaato.sdk.core.api.VideoType;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.List;
import java.util.Map;

public final class ik extends ij {
    public final String c;
    public boolean d = false;
    private final ha e;
    private final fb f;
    private final ev g;
    private final fi h;
    private Context i;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public hh f4497a;
        public final List b;

        public a(hh hhVar, List list) {
            this.f4497a = hhVar;
            this.b = list;
        }
    }

    public final String c() {
        return "placement";
    }

    public ik(ha haVar, fb fbVar, ev evVar, fi fiVar, String str, Context context) {
        this.e = haVar;
        this.f = fbVar;
        this.g = evVar;
        this.h = fiVar;
        this.c = str;
        this.i = context;
    }

    public final Map e() {
        Map e2 = super.e();
        e2.put(String.VIDEO_INFO, new bm(hq.a(this.f)));
        e2.put("app", new bm(hq.a(this.g)));
        e2.put("user", new bm(hq.a(this.h)));
        e2.put("placement", this.c);
        return e2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(bn bnVar) {
        bnVar.h();
        hv hvVar = null;
        hs hsVar = null;
        List list = null;
        while (bnVar.j()) {
            String l = bnVar.l();
            if (VideoType.INTERSTITIAL.equals(l)) {
                hvVar = (hv) bnVar.a(hv.n);
            } else if ("contextual_button".equals(l)) {
                hsVar = (hs) bnVar.a(hs.d);
            } else if ("enabled_placements".equals(l)) {
                list = bnVar.c();
            } else {
                bnVar.s();
            }
        }
        bnVar.i();
        if (hvVar != null && (hvVar.a() || hvVar.b())) {
            return new a(new hf(this.e, this.c, hvVar, this.i), list);
        }
        if (hsVar != null) {
            return new a(new gw(this.e, this.c, hsVar, this.i), list);
        }
        return new a(new hg(), list);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object f() {
        a aVar = (a) super.f();
        if (!(aVar.f4497a instanceof hg)) {
            aVar.f4497a.b();
            if (!aVar.f4497a.c()) {
                new Object[1][0] = this.c;
                aVar.f4497a = new hg();
            }
        }
        return aVar;
    }
}
