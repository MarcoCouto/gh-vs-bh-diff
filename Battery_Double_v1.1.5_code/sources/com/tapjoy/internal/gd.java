package com.tapjoy.internal;

import android.content.Context;
import com.tapjoy.TJPlacementListener;

abstract class gd {

    /* renamed from: a reason: collision with root package name */
    private static final gd f4429a;
    private static gd b;

    public abstract Object a(Context context, String str, TJPlacementListener tJPlacementListener);

    gd() {
    }

    static {
        ge geVar = new ge();
        f4429a = geVar;
        b = geVar;
    }

    static gd a() {
        return b;
    }
}
