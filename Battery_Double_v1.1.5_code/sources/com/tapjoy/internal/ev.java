package com.tapjoy.internal;

public final class ev extends ei {
    public static final ek c = new b();
    public static final Integer d = Integer.valueOf(0);
    public final String e;
    public final Integer f;
    public final String g;
    public final String h;
    public final String i;

    public static final class a extends com.tapjoy.internal.ei.a {
        public String c;
        public Integer d;
        public String e;
        public String f;
        public String g;

        public final ev b() {
            ev evVar = new ev(this.c, this.d, this.e, this.f, this.g, super.a());
            return evVar;
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            ev evVar = (ev) obj;
            int i = 0;
            int a2 = (evVar.e != null ? ek.p.a(1, (Object) evVar.e) : 0) + (evVar.f != null ? ek.d.a(2, (Object) evVar.f) : 0) + (evVar.g != null ? ek.p.a(3, (Object) evVar.g) : 0) + (evVar.h != null ? ek.p.a(4, (Object) evVar.h) : 0);
            if (evVar.i != null) {
                i = ek.p.a(5, (Object) evVar.i);
            }
            return a2 + i + evVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            ev evVar = (ev) obj;
            if (evVar.e != null) {
                ek.p.a(emVar, 1, evVar.e);
            }
            if (evVar.f != null) {
                ek.d.a(emVar, 2, evVar.f);
            }
            if (evVar.g != null) {
                ek.p.a(emVar, 3, evVar.g);
            }
            if (evVar.h != null) {
                ek.p.a(emVar, 4, evVar.h);
            }
            if (evVar.i != null) {
                ek.p.a(emVar, 5, evVar.i);
            }
            emVar.a(evVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, ev.class);
        }

        public final /* synthetic */ Object a(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            aVar.c = (String) ek.p.a(elVar);
                            break;
                        case 2:
                            aVar.d = (Integer) ek.d.a(elVar);
                            break;
                        case 3:
                            aVar.e = (String) ek.p.a(elVar);
                            break;
                        case 4:
                            aVar.f = (String) ek.p.a(elVar);
                            break;
                        case 5:
                            aVar.g = (String) ek.p.a(elVar);
                            break;
                        default:
                            eh c = elVar.c();
                            aVar.a(b, c, c.a().a(elVar));
                            break;
                    }
                } else {
                    elVar.a(a2);
                    return aVar.b();
                }
            }
        }
    }

    public ev(String str, Integer num, String str2, String str3, String str4, iv ivVar) {
        super(c, ivVar);
        this.e = str;
        this.f = num;
        this.g = str2;
        this.h = str3;
        this.i = str4;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ev)) {
            return false;
        }
        ev evVar = (ev) obj;
        return a().equals(evVar.a()) && ep.a((Object) this.e, (Object) evVar.e) && ep.a((Object) this.f, (Object) evVar.f) && ep.a((Object) this.g, (Object) evVar.g) && ep.a((Object) this.h, (Object) evVar.h) && ep.a((Object) this.i, (Object) evVar.i);
    }

    public final int hashCode() {
        int i2 = this.b;
        if (i2 != 0) {
            return i2;
        }
        int i3 = 0;
        int hashCode = ((((((((a().hashCode() * 37) + (this.e != null ? this.e.hashCode() : 0)) * 37) + (this.f != null ? this.f.hashCode() : 0)) * 37) + (this.g != null ? this.g.hashCode() : 0)) * 37) + (this.h != null ? this.h.hashCode() : 0)) * 37;
        if (this.i != null) {
            i3 = this.i.hashCode();
        }
        int i4 = hashCode + i3;
        this.b = i4;
        return i4;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.e != null) {
            sb.append(", pkgVer=");
            sb.append(this.e);
        }
        if (this.f != null) {
            sb.append(", pkgRev=");
            sb.append(this.f);
        }
        if (this.g != null) {
            sb.append(", dataVer=");
            sb.append(this.g);
        }
        if (this.h != null) {
            sb.append(", installer=");
            sb.append(this.h);
        }
        if (this.i != null) {
            sb.append(", store=");
            sb.append(this.i);
        }
        StringBuilder replace = sb.replace(0, 2, "App{");
        replace.append('}');
        return replace.toString();
    }
}
