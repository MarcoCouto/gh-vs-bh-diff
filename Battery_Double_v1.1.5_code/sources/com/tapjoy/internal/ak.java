package com.tapjoy.internal;

import com.google.android.exoplayer2.C;
import java.nio.charset.Charset;

public final class ak {

    /* renamed from: a reason: collision with root package name */
    public static final Charset f4311a = Charset.forName(C.ASCII_NAME);
    public static final Charset b = Charset.forName("ISO-8859-1");
    public static final Charset c = Charset.forName("UTF-8");
    public static final Charset d = Charset.forName("UTF-16BE");
    public static final Charset e = Charset.forName("UTF-16LE");
    public static final Charset f = Charset.forName("UTF-16");
}
