package com.tapjoy.internal;

import com.tapjoy.TJAdUnitConstants.String;
import java.util.Map;

public final class im extends ij {
    private final fb c;
    private final ev d;
    private final fi e;
    private final String f;

    public final String c() {
        return "api/v1/tokens";
    }

    private im(fb fbVar, ev evVar, fi fiVar, String str) {
        this.c = fbVar;
        this.d = evVar;
        this.e = fiVar;
        this.f = str;
    }

    public im(fc fcVar, String str) {
        this(fcVar.d, fcVar.e, fcVar.f, str);
    }

    public final Map e() {
        Map e2 = super.e();
        e2.put(String.VIDEO_INFO, new bm(hq.a(this.c)));
        e2.put("app", new bm(hq.a(this.d)));
        e2.put("user", new bm(hq.a(this.e)));
        if (!al.a(this.f)) {
            e2.put("push_token", this.f);
        }
        return e2;
    }
}
