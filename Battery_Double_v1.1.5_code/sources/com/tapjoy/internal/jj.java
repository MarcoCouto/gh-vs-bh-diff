package com.tapjoy.internal;

import android.app.Notification;
import android.app.Notification.BigTextStyle;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.annotation.RestrictTo.Scope;
import android.support.v4.app.NotificationCompatExtras;
import android.util.SparseArray;
import android.widget.RemoteViews;
import java.util.ArrayList;

public final class jj {

    public static class a {

        /* renamed from: a reason: collision with root package name */
        final Bundle f4517a;
        final jm[] b;
        final jm[] c;
        boolean d;
        public int e;
        public CharSequence f;
        public PendingIntent g;
    }

    public static class b extends d {
        private CharSequence e;

        public final b a(CharSequence charSequence) {
            this.b = c.d(charSequence);
            return this;
        }

        public final b b(CharSequence charSequence) {
            this.e = c.d(charSequence);
            return this;
        }

        @RestrictTo({Scope.LIBRARY_GROUP})
        public final void a(ji jiVar) {
            if (VERSION.SDK_INT >= 16) {
                BigTextStyle bigText = new BigTextStyle(jiVar.a()).setBigContentTitle(this.b).bigText(this.e);
                if (this.d) {
                    bigText.setSummaryText(this.c);
                }
            }
        }
    }

    public static class c {
        Bundle A;
        int B = 0;
        int C = 0;
        Notification D;
        RemoteViews E;
        RemoteViews F;
        RemoteViews G;
        String H;
        int I = 0;
        String J;
        long K;
        int L = 0;
        Notification M = new Notification();
        @Deprecated
        public ArrayList N;
        @RestrictTo({Scope.LIBRARY_GROUP})

        /* renamed from: a reason: collision with root package name */
        public Context f4518a;
        @RestrictTo({Scope.LIBRARY_GROUP})
        public ArrayList b = new ArrayList();
        CharSequence c;
        CharSequence d;
        PendingIntent e;
        PendingIntent f;
        RemoteViews g;
        Bitmap h;
        CharSequence i;
        int j;
        int k;
        boolean l = true;
        boolean m;
        d n;
        CharSequence o;
        CharSequence[] p;
        int q;
        int r;
        boolean s;
        String t;
        boolean u;
        String v;
        boolean w = false;
        boolean x;
        boolean y;
        String z;

        public c(@NonNull Context context, @NonNull String str) {
            this.f4518a = context;
            this.H = str;
            this.M.when = System.currentTimeMillis();
            this.M.audioStreamType = -1;
            this.k = 0;
            this.N = new ArrayList();
        }

        public final c a(int i2) {
            this.M.icon = i2;
            return this;
        }

        public final c a(CharSequence charSequence) {
            this.c = d(charSequence);
            return this;
        }

        public final c b(CharSequence charSequence) {
            this.d = d(charSequence);
            return this;
        }

        public final c a(PendingIntent pendingIntent) {
            this.e = pendingIntent;
            return this;
        }

        public final c c(CharSequence charSequence) {
            this.M.tickerText = d(charSequence);
            return this;
        }

        public final c a(Bitmap bitmap) {
            this.h = bitmap;
            return this;
        }

        public final c b() {
            this.M.defaults = 1;
            return this;
        }

        public final c c() {
            this.k = 0;
            return this;
        }

        public final c a(d dVar) {
            if (this.n != dVar) {
                this.n = dVar;
                if (this.n != null) {
                    this.n.a(this);
                }
            }
            return this;
        }

        public final Notification d() {
            Notification notification;
            jk jkVar = new jk(this);
            d dVar = jkVar.b.n;
            if (dVar != null) {
                dVar.a((ji) jkVar);
            }
            if (VERSION.SDK_INT >= 26) {
                notification = jkVar.f4520a.build();
            } else if (VERSION.SDK_INT >= 24) {
                notification = jkVar.f4520a.build();
                if (jkVar.g != 0) {
                    if (!(notification.getGroup() == null || (notification.flags & 512) == 0 || jkVar.g != 2)) {
                        jk.a(notification);
                    }
                    if (notification.getGroup() != null && (notification.flags & 512) == 0 && jkVar.g == 1) {
                        jk.a(notification);
                    }
                }
            } else if (VERSION.SDK_INT >= 21) {
                jkVar.f4520a.setExtras(jkVar.f);
                notification = jkVar.f4520a.build();
                if (jkVar.c != null) {
                    notification.contentView = jkVar.c;
                }
                if (jkVar.d != null) {
                    notification.bigContentView = jkVar.d;
                }
                if (jkVar.h != null) {
                    notification.headsUpContentView = jkVar.h;
                }
                if (jkVar.g != 0) {
                    if (!(notification.getGroup() == null || (notification.flags & 512) == 0 || jkVar.g != 2)) {
                        jk.a(notification);
                    }
                    if (notification.getGroup() != null && (notification.flags & 512) == 0 && jkVar.g == 1) {
                        jk.a(notification);
                    }
                }
            } else if (VERSION.SDK_INT >= 20) {
                jkVar.f4520a.setExtras(jkVar.f);
                notification = jkVar.f4520a.build();
                if (jkVar.c != null) {
                    notification.contentView = jkVar.c;
                }
                if (jkVar.d != null) {
                    notification.bigContentView = jkVar.d;
                }
                if (jkVar.g != 0) {
                    if (!(notification.getGroup() == null || (notification.flags & 512) == 0 || jkVar.g != 2)) {
                        jk.a(notification);
                    }
                    if (notification.getGroup() != null && (notification.flags & 512) == 0 && jkVar.g == 1) {
                        jk.a(notification);
                    }
                }
            } else if (VERSION.SDK_INT >= 19) {
                SparseArray a2 = jl.a(jkVar.e);
                if (a2 != null) {
                    jkVar.f.putSparseParcelableArray(NotificationCompatExtras.EXTRA_ACTION_EXTRAS, a2);
                }
                jkVar.f4520a.setExtras(jkVar.f);
                notification = jkVar.f4520a.build();
                if (jkVar.c != null) {
                    notification.contentView = jkVar.c;
                }
                if (jkVar.d != null) {
                    notification.bigContentView = jkVar.d;
                }
            } else if (VERSION.SDK_INT >= 16) {
                notification = jkVar.f4520a.build();
                Bundle a3 = jj.a(notification);
                Bundle bundle = new Bundle(jkVar.f);
                for (String str : jkVar.f.keySet()) {
                    if (a3.containsKey(str)) {
                        bundle.remove(str);
                    }
                }
                a3.putAll(bundle);
                SparseArray a4 = jl.a(jkVar.e);
                if (a4 != null) {
                    jj.a(notification).putSparseParcelableArray(NotificationCompatExtras.EXTRA_ACTION_EXTRAS, a4);
                }
                if (jkVar.c != null) {
                    notification.contentView = jkVar.c;
                }
                if (jkVar.d != null) {
                    notification.bigContentView = jkVar.d;
                }
            } else {
                notification = jkVar.f4520a.getNotification();
            }
            if (jkVar.b.E != null) {
                notification.contentView = jkVar.b.E;
            }
            int i2 = VERSION.SDK_INT;
            int i3 = VERSION.SDK_INT;
            if (VERSION.SDK_INT >= 16 && dVar != null) {
                jj.a(notification);
            }
            return notification;
        }

        protected static CharSequence d(CharSequence charSequence) {
            if (charSequence == null) {
                return charSequence;
            }
            if (charSequence.length() > 5120) {
                charSequence = charSequence.subSequence(0, 5120);
            }
            return charSequence;
        }

        public final c a() {
            this.M.flags |= 16;
            return this;
        }
    }

    public static abstract class d {
        @RestrictTo({Scope.LIBRARY_GROUP})

        /* renamed from: a reason: collision with root package name */
        protected c f4519a;
        CharSequence b;
        CharSequence c;
        boolean d = false;

        @RestrictTo({Scope.LIBRARY_GROUP})
        public void a(ji jiVar) {
        }

        public final void a(c cVar) {
            if (this.f4519a != cVar) {
                this.f4519a = cVar;
                if (this.f4519a != null) {
                    this.f4519a.a(this);
                }
            }
        }
    }

    public static Bundle a(Notification notification) {
        if (VERSION.SDK_INT >= 19) {
            return notification.extras;
        }
        if (VERSION.SDK_INT >= 16) {
            return jl.a(notification);
        }
        return null;
    }
}
