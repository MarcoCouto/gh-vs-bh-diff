package com.tapjoy.internal;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build.VERSION;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.mansoon.BatteryDouble.Config;
import com.tapjoy.FiveRocksIntegration;
import com.tapjoy.TJAdUnit;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TJAwardCurrencyListener;
import com.tapjoy.TJConnectListener;
import com.tapjoy.TJCurrency;
import com.tapjoy.TJEarnedCurrencyListener;
import com.tapjoy.TJEventOptimizer;
import com.tapjoy.TJGetCurrencyBalanceListener;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TJPlacementManager;
import com.tapjoy.TJSetUserIDListener;
import com.tapjoy.TJSpendCurrencyListener;
import com.tapjoy.TJVideoListener;
import com.tapjoy.TapjoyAppSettings;
import com.tapjoy.TapjoyCache;
import com.tapjoy.TapjoyConnectCore;
import com.tapjoy.TapjoyConnectFlag;
import com.tapjoy.TapjoyErrorMessage;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.TapjoyException;
import com.tapjoy.TapjoyIntegrationException;
import com.tapjoy.TapjoyLog;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class et extends es {
    private boolean c = false;
    private String d = "";
    /* access modifiers changed from: private */
    public TJCurrency e = null;
    /* access modifiers changed from: private */
    public TapjoyCache f = null;

    public final String b() {
        return "12.3.3";
    }

    et() {
    }

    public final void a(boolean z) {
        TapjoyLog.setDebugEnabled(z);
    }

    public final boolean a(Context context, String str) {
        return a(context, str, (Hashtable) null, (TJConnectListener) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0038, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0053, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0098, code lost:
        return true;
     */
    public synchronized boolean a(final Context context, String str, Hashtable hashtable, final TJConnectListener tJConnectListener) {
        if (hashtable != null) {
            try {
                Object obj = hashtable.get(TapjoyConnectFlag.ENABLE_LOGGING);
                if (obj != null) {
                    TapjoyLog.setDebugEnabled("true".equals(obj.toString()));
                }
            } catch (TapjoyIntegrationException e2) {
                TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, e2.getMessage()));
                if (tJConnectListener != null) {
                    tJConnectListener.onConnectFailure();
                }
                return false;
            } catch (TapjoyException e3) {
                TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.SDK_ERROR, e3.getMessage()));
                if (tJConnectListener != null) {
                    tJConnectListener.onConnectFailure();
                }
                return false;
            } catch (Throwable th) {
                throw th;
            }
        }
        TapjoyConnectCore.setSDKType("event");
        boolean z = false;
        if (context == null) {
            TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, "The application context is NULL"));
            if (tJConnectListener != null) {
                tJConnectListener.onConnectFailure();
            }
        } else if (jr.c(str)) {
            TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, "The SDK key is NULL. A valid SDK key is required to connect successfully to Tapjoy"));
            if (tJConnectListener != null) {
                tJConnectListener.onConnectFailure();
            }
        } else {
            FiveRocksIntegration.a();
            TapjoyAppSettings.init(context);
            TapjoyConnectCore.requestTapjoyConnect(context, str, hashtable, new TJConnectListener() {
                public final void onConnectSuccess() {
                    et.this.e = new TJCurrency(context);
                    et.this.f = new TapjoyCache(context);
                    try {
                        TJEventOptimizer.init(context);
                        et.this.f4394a = true;
                        if (tJConnectListener != null) {
                            tJConnectListener.onConnectSuccess();
                        }
                    } catch (InterruptedException unused) {
                        onConnectFailure();
                    } catch (RuntimeException e) {
                        TapjoyLog.w("TapjoyAPI", e.getMessage());
                        onConnectFailure();
                    }
                }

                public final void onConnectFailure() {
                    if (tJConnectListener != null) {
                        tJConnectListener.onConnectFailure();
                    }
                }
            });
            this.c = true;
            if (VERSION.SDK_INT < 14) {
                TapjoyLog.i("TapjoyAPI", "Automatic session tracking is not available on this device.");
            } else {
                if (hashtable != null) {
                    String valueOf = String.valueOf(hashtable.get(TapjoyConnectFlag.DISABLE_AUTOMATIC_SESSION_TRACKING));
                    if (valueOf != null && valueOf.equalsIgnoreCase("true")) {
                        z = true;
                    }
                }
                if (!z) {
                    fm.a(context);
                } else {
                    TapjoyLog.i("TapjoyAPI", "Automatic session tracking is disabled.");
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        return false;
     */
    public final synchronized boolean a(final Context context, String str, final TJConnectListener tJConnectListener) {
        TapjoyConnectCore.setSDKType("event");
        if (context == null) {
            TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, "The application context is NULL"));
            if (tJConnectListener != null) {
                tJConnectListener.onConnectFailure();
            }
        } else if (jr.c(str)) {
            TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, "The limited SDK key is NULL. A valid limited SDK key is required to connect successfully to Tapjoy"));
            if (tJConnectListener != null) {
                tJConnectListener.onConnectFailure();
            }
        } else {
            try {
                TapjoyConnectCore.requestLimitedTapjoyConnect(context, str, new TJConnectListener() {
                    public final void onConnectSuccess() {
                        et.this.f = new TapjoyCache(context);
                        et.this.b = true;
                        if (tJConnectListener != null) {
                            tJConnectListener.onConnectSuccess();
                        }
                    }

                    public final void onConnectFailure() {
                        if (tJConnectListener != null) {
                            tJConnectListener.onConnectFailure();
                        }
                    }
                });
                return true;
            } catch (TapjoyIntegrationException e2) {
                TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, e2.getMessage()));
                if (tJConnectListener != null) {
                    tJConnectListener.onConnectFailure();
                }
                return false;
            } catch (TapjoyException e3) {
                TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.SDK_ERROR, e3.getMessage()));
                if (tJConnectListener != null) {
                    tJConnectListener.onConnectFailure();
                }
                return false;
            }
        }
    }

    public final TJPlacement a(String str, TJPlacementListener tJPlacementListener) {
        return TJPlacementManager.a(str, "", "", tJPlacementListener);
    }

    public final TJPlacement b(String str, TJPlacementListener tJPlacementListener) {
        return TJPlacementManager.b(str, "", "", tJPlacementListener);
    }

    public final void a(Activity activity) {
        if (activity != null) {
            b.a(activity);
        } else {
            TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, "Cannot set activity to NULL"));
        }
    }

    public final void a(float f2) {
        if (k("setCurrencyMultiplier")) {
            TapjoyConnectCore.getInstance().setCurrencyMultiplier(f2);
        }
    }

    public final float c() {
        if (k("getCurrencyMultiplier")) {
            return TapjoyConnectCore.getInstance().getCurrencyMultiplier();
        }
        return 1.0f;
    }

    public final void e(String str) {
        if (j("actionComplete")) {
            TapjoyConnectCore.getInstance().actionComplete(str);
        }
    }

    public final void a(TJGetCurrencyBalanceListener tJGetCurrencyBalanceListener) {
        if (this.e != null && j("getCurrencyBalance")) {
            this.e.getCurrencyBalance(tJGetCurrencyBalanceListener);
        }
    }

    public final void a(int i, TJSpendCurrencyListener tJSpendCurrencyListener) {
        if (this.e != null && j("spendCurrency")) {
            this.e.spendCurrency(i, tJSpendCurrencyListener);
        }
    }

    public final void a(int i, TJAwardCurrencyListener tJAwardCurrencyListener) {
        if (this.e != null && j("awardCurrency")) {
            this.e.awardCurrency(i, tJAwardCurrencyListener);
        }
    }

    public final void a(TJEarnedCurrencyListener tJEarnedCurrencyListener) {
        if (this.e != null && j("setEarnedCurrencyListener")) {
            this.e.setEarnedCurrencyListener(tJEarnedCurrencyListener);
        }
    }

    public final void a(TJVideoListener tJVideoListener) {
        if (k("setVideoListener")) {
            TJAdUnit.f4205a = tJVideoListener;
        }
    }

    public final void a(String str, String str2, String str3, String str4) {
        go.a(str, str2, str3, str4);
    }

    public final void a(String str, String str2) {
        go.a(str, null, null, str2);
    }

    public final void a(String str) {
        go.a(null, str, null, null, 0);
    }

    public final void a(String str, long j) {
        go.a(null, str, null, null, j);
    }

    public final void a(String str, String str2, long j) {
        go.a(str, str2, null, null, j);
    }

    public final void b(String str, String str2, String str3, String str4) {
        go.a(str, str2, str3, str4, 0);
    }

    public final void a(String str, String str2, String str3, String str4, long j) {
        go.a(str, str2, str3, str4, j);
    }

    public final void a(String str, String str2, String str3, String str4, String str5, long j) {
        go.a(str, str2, str3, str4, str5, j, null, 0, null, 0);
    }

    public final void a(String str, String str2, String str3, String str4, String str5, long j, String str6, long j2) {
        go.a(str, str2, str3, str4, str5, j, str6, j2, null, 0);
    }

    public final void a(String str, String str2, String str3, String str4, String str5, long j, String str6, long j2, String str7, long j3) {
        go.a(str, str2, str3, str4, str5, j, str6, j2, str7, j3);
    }

    public final void d() {
        if (k("startSession")) {
            if (VERSION.SDK_INT >= 14) {
                fm.a();
            }
            TapjoyConnectCore.getInstance().appResume();
            go.a();
        }
    }

    public final void e() {
        if (k("endSession")) {
            if (VERSION.SDK_INT >= 14) {
                fm.a();
            }
            ha.a().n = false;
            TapjoyConnectCore.getInstance().appPause();
            go.b();
        }
    }

    public final void b(Activity activity) {
        if (VERSION.SDK_INT >= 14) {
            fm.a();
        }
        ha.a().n = true;
        go.a(activity);
    }

    public final void c(Activity activity) {
        if (VERSION.SDK_INT >= 14) {
            fm.a();
        }
        go.b(activity);
    }

    public final void a(String str, TJSetUserIDListener tJSetUserIDListener) {
        if (k("setUserID")) {
            TapjoyConnectCore.setUserID(str, tJSetUserIDListener);
            ha a2 = ha.a();
            if (a2.d("setUserId")) {
                a2.f.b(gv.a(str));
            }
            return;
        }
        if (tJSetUserIDListener != null) {
            tJSetUserIDListener.onSetUserIDFailure(this.d);
        }
    }

    public final Set f() {
        return ha.a().c();
    }

    public final void a(Set set) {
        ha.a().a(set);
    }

    public final void g() {
        ha.a().a((Set) null);
    }

    public final void c(String str) {
        if (!jr.c(str)) {
            ha a2 = ha.a();
            Set c2 = a2.c();
            if (c2.add(str)) {
                a2.a(c2);
            }
        }
    }

    public final void d(String str) {
        if (!jr.c(str)) {
            ha a2 = ha.a();
            Set c2 = a2.c();
            if (c2.remove(str)) {
                a2.a(c2);
            }
        }
    }

    public final boolean h() {
        ha a2 = ha.a();
        if (!a2.d("isPushNotificationDisabled")) {
            return false;
        }
        boolean f2 = a2.f.f();
        gx.a("isPushNotificationDisabled = {}", Boolean.valueOf(f2));
        return f2;
    }

    public final void b(boolean z) {
        Object obj;
        Object[] objArr;
        String str;
        String str2;
        ha a2 = ha.a();
        if (a2.d("setPushNotificationDisabled")) {
            boolean a3 = a2.f.a(z);
            char c2 = 1;
            if (a3) {
                str = "setPushNotificationDisabled({}) called";
                Object[] objArr2 = new Object[1];
                obj = Boolean.valueOf(z);
                objArr = objArr2;
                c2 = 0;
            } else {
                str = "setPushNotificationDisabled({}) called, but it is already {}";
                objArr = new Object[2];
                objArr[0] = Boolean.valueOf(z);
                obj = z ? Config.IMPORTANCE_DISABLED : String.ENABLED;
            }
            objArr[c2] = obj;
            gx.a(str, objArr);
            if (a3 && a2.k && !jr.c(a2.d)) {
                if (a2.o != null) {
                    str2 = null;
                } else {
                    hc b = hc.b(a2.e);
                    str2 = jr.b(b.b.b(b.f4542a));
                }
                a2.a(str2);
            }
        }
    }

    public final boolean i() {
        return this.f4394a;
    }

    public final boolean j() {
        return this.b;
    }

    public final String g(String str) {
        if (j("getSupportURL")) {
            return TapjoyConnectCore.getSupportURL(str);
        }
        return null;
    }

    public final String k() {
        return TapjoyConnectCore.getUserToken();
    }

    public final void i(String str) {
        ha.a().a(str);
    }

    public final void a(Context context, Map map) {
        ha a2 = ha.a();
        if (a2.e == null) {
            a2.b(context);
        }
        hc.b(a2.e);
        Context context2 = a2.e;
        boolean z = true;
        new Object[1][0] = map;
        String str = (String) map.get("fiverocks");
        if (str != null) {
            if (hd.a(context2).f()) {
                ha.a(context2).b(str);
                return;
            }
            String str2 = (String) map.get("title");
            String str3 = (String) map.get("message");
            if (str3 != null) {
                String str4 = (String) map.get("rich");
                String str5 = (String) map.get("sound");
                String str6 = (String) map.get(MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
                String str7 = (String) map.get("always");
                boolean z2 = "true".equals(str7) || Boolean.TRUE.equals(str7);
                String str8 = (String) map.get("repeatable");
                if (!"true".equals(str8) && !Boolean.TRUE.equals(str8)) {
                    z = false;
                }
                String str9 = (String) map.get("placement");
                int b = hc.b(map.get("nid"));
                String str10 = (String) map.get("channel_id");
                if (z2 || !ha.a(context2).d()) {
                    Notification a3 = hc.a(context2, str, jr.a(str2), str3, hc.a((Object) str4), hc.a((Object) str5), str6, str9, b, str10);
                    if (ha.a(context2).a(context2, str, z)) {
                        hc.a(context2, b, a3);
                    }
                }
            }
        }
    }

    private boolean j(String str) {
        if (this.f4394a) {
            return true;
        }
        StringBuilder sb = new StringBuilder("Can not call ");
        sb.append(str);
        sb.append(" because Tapjoy SDK has not successfully connected.");
        TapjoyLog.w("TapjoyAPI", sb.toString());
        return false;
    }

    private boolean k(String str) {
        if (this.c) {
            return true;
        }
        StringBuilder sb = new StringBuilder("Can not call ");
        sb.append(str);
        sb.append(" because Tapjoy SDK is not initialized.");
        this.d = sb.toString();
        TapjoyLog.e("TapjoyAPI", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, this.d));
        return false;
    }

    public final void c(boolean z) {
        gf a2 = gf.a();
        a2.f4430a = Boolean.valueOf(z);
        if (!a2.b()) {
            a2.d = true;
        }
    }

    public final void h(String str) {
        gf a2 = gf.a();
        if (!al.a(str)) {
            a2.b = str;
            if (!a2.c()) {
                a2.d = true;
            }
        }
    }

    public final void d(boolean z) {
        gf a2 = gf.a();
        a2.c = Boolean.valueOf(z);
        if (!a2.d()) {
            a2.d = true;
        }
    }

    public final void a(String str, String str2, double d2, String str3) {
        ha a2 = ha.a();
        if (a2.c("trackPurchase")) {
            String str4 = str;
            String a3 = gv.a(str, "trackPurchase", "productId");
            if (a3 != null) {
                String str5 = str2;
                String a4 = gv.a(str2, "trackPurchase", "currencyCode");
                if (a4 != null) {
                    if (a4.length() != 3) {
                        gx.a("trackPurchase", "currencyCode", "invalid currency code");
                        return;
                    }
                    a2.g.a(a3, a4.toUpperCase(Locale.US), d2, (String) null, (String) null, gv.b(str3));
                    gx.a("trackPurchase called");
                }
            }
        }
    }

    public final void a(String str, String str2, String str3, String str4, Map map) {
        ha a2 = ha.a();
        if (a2.c("trackEvent") && !jr.c(str2)) {
            LinkedHashMap b = jv.b();
            if (map != null && map.size() > 0) {
                Iterator it = map.entrySet().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Entry entry = (Entry) it.next();
                    Object key = entry.getKey();
                    if (key == null) {
                        String str5 = "trackEvent";
                        String str6 = "key in values map";
                        if (gx.f4443a) {
                            x.a("Tapjoy", "{}: {} must not be null", str5, str6);
                        }
                        return;
                    } else if (key instanceof String) {
                        String a3 = gv.a((String) key, "trackEvent", "key in values map");
                        if (a3 == null) {
                            break;
                        }
                        Object value = entry.getValue();
                        if (value instanceof Number) {
                            b.put(a3, Long.valueOf(((Number) value).longValue()));
                        } else {
                            gx.a("trackEvent", "value in values map", "must be a long");
                            return;
                        }
                    }
                }
            }
            a2.g.a(str, str2, str3, str4, b);
            gx.a("trackEvent category:{}, name:{}, p1:{}, p2:{}, values:{} called", str, str2, str3, str4, b);
        }
    }

    public final void a(int i) {
        ha a2 = ha.a();
        if (a2.d("setUserLevel")) {
            gx.a("setUserLevel({}) called", Integer.valueOf(i));
            a2.f.a(i >= 0 ? Integer.valueOf(i) : null);
        }
    }

    public final void b(int i) {
        ha a2 = ha.a();
        if (a2.d("setUserFriendCount")) {
            gx.a("setUserFriendCount({}) called", Integer.valueOf(i));
            a2.f.b(i >= 0 ? Integer.valueOf(i) : null);
        }
    }

    public final void b(String str) {
        ha a2 = ha.a();
        if (a2.d("setAppDataVersion")) {
            a2.f.a(gv.a(str));
        }
    }

    public final void a(int i, String str) {
        ha a2 = ha.a();
        if (a2.d("setUserCohortVariable")) {
            boolean z = i > 0 && i <= 5;
            String str2 = "setCohortVariable: variableIndex is out of range";
            if (gx.f4443a && !z) {
                gx.b(str2);
            }
            if (z) {
                gx.a("setUserCohortVariable({}, {}) called", Integer.valueOf(i), str);
                a2.f.a(i, gv.a(str));
            }
        }
    }

    public final void f(String str) {
        ha a2 = ha.a();
        gx.a("setGcmSender({}) called", str);
        a2.d = jr.a(str);
        a2.b();
    }

    public final void a(GLSurfaceView gLSurfaceView) {
        ha.a();
        ha.a(gLSurfaceView);
    }
}
