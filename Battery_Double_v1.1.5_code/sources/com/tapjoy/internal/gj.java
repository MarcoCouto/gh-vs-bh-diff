package com.tapjoy.internal;

import com.github.mikephil.charting.utils.Utils;

public final class gj {

    /* renamed from: a reason: collision with root package name */
    public static final gj f4434a;
    public final long b;
    public final long c;
    public final double d;
    public long e;
    private final long f;

    static {
        gj gjVar = new gj(0, 0, 0, Utils.DOUBLE_EPSILON);
        f4434a = gjVar;
    }

    public gj(long j, long j2, long j3, double d2) {
        this.f = j;
        this.b = j2;
        this.c = j3;
        this.d = d2;
        this.e = j;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        gj gjVar = (gj) obj;
        return this.f == gjVar.f && this.b == gjVar.b && this.c == gjVar.c && this.d == gjVar.d && this.e == gjVar.e;
    }
}
