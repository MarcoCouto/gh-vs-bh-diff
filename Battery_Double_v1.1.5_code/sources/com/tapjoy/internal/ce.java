package com.tapjoy.internal;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public final class ce implements cd {

    /* renamed from: a reason: collision with root package name */
    private final String f4340a;
    private final URL b;

    public ce(String str, URL url) {
        this.f4340a = str;
        this.b = url;
    }

    public final Object a(ca caVar) {
        URI uri;
        URL url = new URL(this.b, caVar.c());
        String b2 = caVar.b();
        if (HttpRequest.METHOD_GET.equals(b2) || HttpRequest.METHOD_DELETE.equals(b2)) {
            Map e = caVar.e();
            if (!e.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append(url.getPath());
                sb.append("?");
                sb.append(fl.a(e));
                url = new URL(url, sb.toString());
            }
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) fk.a(url);
        httpURLConnection.setRequestMethod(b2);
        httpURLConnection.setRequestProperty("User-Agent", this.f4340a);
        for (Entry entry : caVar.a().entrySet()) {
            httpURLConnection.setRequestProperty((String) entry.getKey(), entry.getValue().toString());
        }
        if (!HttpRequest.METHOD_GET.equals(b2) && !HttpRequest.METHOD_DELETE.equals(b2)) {
            if (HttpRequest.METHOD_POST.equals(b2) || HttpRequest.METHOD_PUT.equals(b2)) {
                String d = caVar.d();
                if (d == null) {
                    fl.a(httpURLConnection, HttpRequest.CONTENT_TYPE_FORM, fl.a(caVar.e()), jn.c);
                } else if ("application/json".equals(d)) {
                    fl.a(httpURLConnection, "application/json; charset=utf-8", bh.a((Object) caVar.e()), jn.c);
                } else {
                    StringBuilder sb2 = new StringBuilder("Unknown content type: ");
                    sb2.append(d);
                    throw new IllegalArgumentException(sb2.toString());
                }
            } else {
                StringBuilder sb3 = new StringBuilder("Unknown method: ");
                sb3.append(b2);
                throw new IllegalArgumentException(sb3.toString());
            }
        }
        httpURLConnection.connect();
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode != 409) {
            switch (responseCode) {
                case Callback.DEFAULT_DRAG_ANIMATION_DURATION /*200*/:
                case 201:
                    break;
                default:
                    StringBuilder sb4 = new StringBuilder("Unexpected status code: ");
                    sb4.append(httpURLConnection.getResponseCode());
                    throw new IOException(sb4.toString());
            }
        }
        InputStream inputStream = httpURLConnection.getInputStream();
        try {
            uri = httpURLConnection.getURL().toURI();
        } catch (URISyntaxException unused) {
            uri = null;
        }
        try {
            return caVar.a(uri, inputStream);
        } finally {
            inputStream.close();
        }
    }
}
