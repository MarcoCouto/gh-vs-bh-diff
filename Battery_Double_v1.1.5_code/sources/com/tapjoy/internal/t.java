package com.tapjoy.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import java.io.InputStream;
import java.io.OutputStream;

public final class t implements bd {

    /* renamed from: a reason: collision with root package name */
    public static final t f4544a = new t();

    public final /* synthetic */ void a(OutputStream outputStream, Object obj) {
        if (!((Bitmap) obj).compress(CompressFormat.PNG, 100, outputStream)) {
            throw new RuntimeException();
        }
    }

    private t() {
    }

    /* renamed from: a */
    public final Bitmap b(final InputStream inputStream) {
        try {
            return (Bitmap) y.a(new bb() {
                public final /* synthetic */ Object call() {
                    if (inputStream instanceof bc) {
                        return BitmapFactory.decodeStream(inputStream);
                    }
                    return BitmapFactory.decodeStream(new bc(inputStream));
                }
            });
        } catch (OutOfMemoryError unused) {
            return null;
        }
    }
}
