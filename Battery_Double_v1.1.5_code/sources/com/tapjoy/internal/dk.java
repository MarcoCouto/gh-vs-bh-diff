package com.tapjoy.internal;

import android.view.View;
import com.tapjoy.internal.di.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import org.json.JSONObject;

public final class dk implements di {

    /* renamed from: a reason: collision with root package name */
    private final di f4365a;

    public dk(di diVar) {
        this.f4365a = diVar;
    }

    public final JSONObject a(View view) {
        return dm.a(0, 0, 0, 0);
    }

    public final void a(View view, JSONObject jSONObject, a aVar, boolean z) {
        ArrayList arrayList = new ArrayList();
        dd a2 = dd.a();
        if (a2 != null) {
            Collection<cz> unmodifiableCollection = Collections.unmodifiableCollection(a2.b);
            IdentityHashMap identityHashMap = new IdentityHashMap((unmodifiableCollection.size() * 2) + 3);
            for (cz c : unmodifiableCollection) {
                View c2 = c.c();
                if (c2 != null && dq.b(c2)) {
                    View rootView = c2.getRootView();
                    if (rootView != null && !identityHashMap.containsKey(rootView)) {
                        identityHashMap.put(rootView, rootView);
                        float a3 = dq.a(rootView);
                        int size = arrayList.size();
                        while (size > 0 && dq.a((View) arrayList.get(size - 1)) > a3) {
                            size--;
                        }
                        arrayList.add(size, rootView);
                    }
                }
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            aVar.a((View) it.next(), this.f4365a, jSONObject);
        }
    }
}
