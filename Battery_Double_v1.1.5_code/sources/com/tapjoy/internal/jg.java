package com.tapjoy.internal;

public final class jg {

    /* renamed from: a reason: collision with root package name */
    public static jg f4516a = new jg(null);
    public String b;
    public Throwable c;
    private Object[] d;

    public jg(String str) {
        this(str, null, null);
    }

    public jg(String str, Object[] objArr, Throwable th) {
        this.b = str;
        this.c = th;
        if (th == null) {
            this.d = objArr;
        } else if (objArr == null || objArr.length == 0) {
            throw new IllegalStateException("non-sensical empty or null argument array");
        } else {
            int length = objArr.length - 1;
            Object[] objArr2 = new Object[length];
            System.arraycopy(objArr, 0, objArr2, 0, length);
            this.d = objArr2;
        }
    }
}
