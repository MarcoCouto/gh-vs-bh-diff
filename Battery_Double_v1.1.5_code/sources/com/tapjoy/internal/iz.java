package com.tapjoy.internal;

final class iz {

    /* renamed from: a reason: collision with root package name */
    final byte[] f4512a;
    int b;
    int c;
    boolean d;
    boolean e;
    iz f;
    iz g;

    iz() {
        this.f4512a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    iz(iz izVar) {
        this(izVar.f4512a, izVar.b, izVar.c);
        izVar.d = true;
    }

    iz(byte[] bArr, int i, int i2) {
        this.f4512a = bArr;
        this.b = i;
        this.c = i2;
        this.e = false;
        this.d = true;
    }

    public final iz a() {
        iz izVar = this.f != this ? this.f : null;
        this.g.f = this.f;
        this.f.g = this.g;
        this.f = null;
        this.g = null;
        return izVar;
    }

    public final iz a(iz izVar) {
        izVar.g = this;
        izVar.f = this.f;
        this.f.g = izVar;
        this.f = izVar;
        return izVar;
    }

    public final void a(iz izVar, int i) {
        if (izVar.e) {
            if (izVar.c + i > 8192) {
                if (izVar.d) {
                    throw new IllegalArgumentException();
                } else if ((izVar.c + i) - izVar.b <= 8192) {
                    System.arraycopy(izVar.f4512a, izVar.b, izVar.f4512a, 0, izVar.c - izVar.b);
                    izVar.c -= izVar.b;
                    izVar.b = 0;
                } else {
                    throw new IllegalArgumentException();
                }
            }
            System.arraycopy(this.f4512a, this.b, izVar.f4512a, izVar.c, i);
            izVar.c += i;
            this.b += i;
            return;
        }
        throw new IllegalArgumentException();
    }
}
