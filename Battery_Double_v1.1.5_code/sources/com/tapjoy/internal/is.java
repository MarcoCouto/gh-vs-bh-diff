package com.tapjoy.internal;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.io.EOFException;
import java.nio.charset.Charset;

public final class is implements it, iu, Cloneable {
    private static final byte[] c = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: a reason: collision with root package name */
    iz f4505a;
    long b;

    public final it a() {
        return this;
    }

    public final void close() {
    }

    public final void flush() {
    }

    public final boolean b() {
        return this.b == 0;
    }

    public final void a(long j) {
        if (this.b < j) {
            throw new EOFException();
        }
    }

    public final byte c() {
        if (this.b != 0) {
            iz izVar = this.f4505a;
            int i = izVar.b;
            int i2 = izVar.c;
            int i3 = i + 1;
            byte b2 = izVar.f4512a[i];
            this.b--;
            if (i3 == i2) {
                this.f4505a = izVar.a();
                ja.a(izVar);
            } else {
                izVar.b = i3;
            }
            return b2;
        }
        throw new IllegalStateException("size == 0");
    }

    public final int d() {
        if (this.b >= 4) {
            iz izVar = this.f4505a;
            int i = izVar.b;
            int i2 = izVar.c;
            if (i2 - i < 4) {
                return ((c() & 255) << 24) | ((c() & 255) << 16) | ((c() & 255) << 8) | (c() & 255);
            }
            byte[] bArr = izVar.f4512a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
            int i5 = i4 + 1;
            byte b3 = b2 | ((bArr[i4] & 255) << 8);
            int i6 = i5 + 1;
            byte b4 = b3 | (bArr[i5] & 255);
            this.b -= 4;
            if (i6 == i2) {
                this.f4505a = izVar.a();
                ja.a(izVar);
            } else {
                izVar.b = i6;
            }
            return b4;
        }
        StringBuilder sb = new StringBuilder("size < 4: ");
        sb.append(this.b);
        throw new IllegalStateException(sb.toString());
    }

    public final int e() {
        return jf.a(d());
    }

    public final iv b(long j) {
        return new iv(g(j));
    }

    public final String c(long j) {
        Charset charset = jf.f4515a;
        jf.a(this.b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            StringBuilder sb = new StringBuilder("byteCount > Integer.MAX_VALUE: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (j == 0) {
            return "";
        } else {
            iz izVar = this.f4505a;
            if (((long) izVar.b) + j > ((long) izVar.c)) {
                return new String(g(j), charset);
            }
            String str = new String(izVar.f4512a, izVar.b, (int) j, charset);
            izVar.b = (int) (((long) izVar.b) + j);
            this.b -= j;
            if (izVar.b == izVar.c) {
                this.f4505a = izVar.a();
                ja.a(izVar);
            }
            return str;
        }
    }

    public final byte[] g() {
        try {
            return g(this.b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    private byte[] g(long j) {
        jf.a(this.b, 0, j);
        if (j <= 2147483647L) {
            byte[] bArr = new byte[((int) j)];
            a(bArr);
            return bArr;
        }
        StringBuilder sb = new StringBuilder("byteCount > Integer.MAX_VALUE: ");
        sb.append(j);
        throw new IllegalArgumentException(sb.toString());
    }

    private void a(byte[] bArr) {
        int i;
        int i2 = 0;
        while (i2 < bArr.length) {
            int length = bArr.length - i2;
            jf.a((long) bArr.length, (long) i2, (long) length);
            iz izVar = this.f4505a;
            if (izVar == null) {
                i = -1;
            } else {
                i = Math.min(length, izVar.c - izVar.b);
                System.arraycopy(izVar.f4512a, izVar.b, bArr, i2, i);
                izVar.b += i;
                this.b -= (long) i;
                if (izVar.b == izVar.c) {
                    this.f4505a = izVar.a();
                    ja.a(izVar);
                }
            }
            if (i != -1) {
                i2 += i;
            } else {
                throw new EOFException();
            }
        }
    }

    public final void d(long j) {
        while (j > 0) {
            if (this.f4505a != null) {
                int min = (int) Math.min(j, (long) (this.f4505a.c - this.f4505a.b));
                long j2 = (long) min;
                this.b -= j2;
                j -= j2;
                this.f4505a.b += min;
                if (this.f4505a.b == this.f4505a.c) {
                    iz izVar = this.f4505a;
                    this.f4505a = izVar.a();
                    ja.a(izVar);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    /* renamed from: a */
    public final is b(iv ivVar) {
        if (ivVar != null) {
            ivVar.a(this);
            return this;
        }
        throw new IllegalArgumentException("byteString == null");
    }

    /* renamed from: a */
    public final is b(String str) {
        char c2;
        int length = str.length();
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (length < 0) {
            StringBuilder sb = new StringBuilder("endIndex < beginIndex: ");
            sb.append(length);
            sb.append(" < 0");
            throw new IllegalArgumentException(sb.toString());
        } else if (length <= str.length()) {
            int i = 0;
            while (i < length) {
                char charAt = str.charAt(i);
                if (charAt < 128) {
                    iz c3 = c(1);
                    byte[] bArr = c3.f4512a;
                    int i2 = c3.c - i;
                    int min = Math.min(length, 8192 - i2);
                    int i3 = i + 1;
                    bArr[i + i2] = (byte) charAt;
                    while (true) {
                        i = i3;
                        if (i >= min) {
                            break;
                        }
                        char charAt2 = str.charAt(i);
                        if (charAt2 >= 128) {
                            break;
                        }
                        i3 = i + 1;
                        bArr[i + i2] = (byte) charAt2;
                    }
                    int i4 = (i2 + i) - c3.c;
                    c3.c += i4;
                    this.b += (long) i4;
                } else if (charAt < 2048) {
                    e((charAt >> 6) | PsExtractor.AUDIO_STREAM);
                    e((int) (charAt & '?') | 128);
                    i++;
                } else if (charAt < 55296 || charAt > 57343) {
                    e((charAt >> 12) | 224);
                    e(((charAt >> 6) & 63) | 128);
                    e((int) (charAt & '?') | 128);
                    i++;
                } else {
                    int i5 = i + 1;
                    if (i5 < length) {
                        c2 = str.charAt(i5);
                    } else {
                        c2 = 0;
                    }
                    if (charAt > 56319 || c2 < 56320 || c2 > 57343) {
                        e(63);
                        i = i5;
                    } else {
                        int i6 = (((charAt & 10239) << 10) | (9215 & c2)) + 0;
                        e((i6 >> 18) | PsExtractor.VIDEO_STREAM_MASK);
                        e(((i6 >> 12) & 63) | 128);
                        e(((i6 >> 6) & 63) | 128);
                        e((i6 & 63) | 128);
                        i += 2;
                    }
                }
            }
            return this;
        } else {
            StringBuilder sb2 = new StringBuilder("endIndex > string.length: ");
            sb2.append(length);
            sb2.append(" > ");
            sb2.append(str.length());
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    public final is a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            long j = (long) i2;
            jf.a((long) bArr.length, 0, j);
            int i3 = i2 + 0;
            while (i < i3) {
                iz c2 = c(1);
                int min = Math.min(i3 - i, 8192 - c2.c);
                System.arraycopy(bArr, i, c2.f4512a, c2.c, min);
                i += min;
                c2.c += min;
            }
            this.b += j;
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: a */
    public final is e(int i) {
        iz c2 = c(1);
        byte[] bArr = c2.f4512a;
        int i2 = c2.c;
        c2.c = i2 + 1;
        bArr[i2] = (byte) i;
        this.b++;
        return this;
    }

    /* renamed from: b */
    public final is d(int i) {
        int a2 = jf.a(i);
        iz c2 = c(4);
        byte[] bArr = c2.f4512a;
        int i2 = c2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((a2 >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((a2 >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((a2 >>> 8) & 255);
        int i6 = i5 + 1;
        bArr[i5] = (byte) (a2 & 255);
        c2.c = i6;
        this.b += 4;
        return this;
    }

    /* renamed from: e */
    public final is f(long j) {
        long a2 = jf.a(j);
        iz c2 = c(8);
        byte[] bArr = c2.f4512a;
        int i = c2.c;
        int i2 = i + 1;
        bArr[i] = (byte) ((int) ((a2 >>> 56) & 255));
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((int) ((a2 >>> 48) & 255));
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((int) ((a2 >>> 40) & 255));
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((int) ((a2 >>> 32) & 255));
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((int) ((a2 >>> 24) & 255));
        int i7 = i6 + 1;
        bArr[i6] = (byte) ((int) ((a2 >>> 16) & 255));
        int i8 = i7 + 1;
        bArr[i7] = (byte) ((int) ((a2 >>> 8) & 255));
        int i9 = i8 + 1;
        bArr[i8] = (byte) ((int) (a2 & 255));
        c2.c = i9;
        this.b += 8;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public final iz c(int i) {
        if (i <= 0 || i > 8192) {
            throw new IllegalArgumentException();
        } else if (this.f4505a == null) {
            this.f4505a = ja.a();
            iz izVar = this.f4505a;
            iz izVar2 = this.f4505a;
            iz izVar3 = this.f4505a;
            izVar2.g = izVar3;
            izVar.f = izVar3;
            return izVar3;
        } else {
            iz izVar4 = this.f4505a.g;
            if (izVar4.c + i > 8192 || !izVar4.e) {
                izVar4 = izVar4.a(ja.a());
            }
            return izVar4;
        }
    }

    public final void a(is isVar, long j) {
        iz izVar;
        if (isVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (isVar != this) {
            jf.a(isVar.b, 0, j);
            while (j > 0) {
                int i = 0;
                if (j < ((long) (isVar.f4505a.c - isVar.f4505a.b))) {
                    iz izVar2 = this.f4505a != null ? this.f4505a.g : null;
                    if (izVar2 != null && izVar2.e) {
                        if ((((long) izVar2.c) + j) - ((long) (izVar2.d ? 0 : izVar2.b)) <= PlaybackStateCompat.ACTION_PLAY_FROM_URI) {
                            isVar.f4505a.a(izVar2, (int) j);
                            isVar.b -= j;
                            this.b += j;
                            return;
                        }
                    }
                    iz izVar3 = isVar.f4505a;
                    int i2 = (int) j;
                    if (i2 <= 0 || i2 > izVar3.c - izVar3.b) {
                        throw new IllegalArgumentException();
                    }
                    if (i2 >= 1024) {
                        izVar = new iz(izVar3);
                    } else {
                        izVar = ja.a();
                        System.arraycopy(izVar3.f4512a, izVar3.b, izVar.f4512a, 0, i2);
                    }
                    izVar.c = izVar.b + i2;
                    izVar3.b += i2;
                    izVar3.g.a(izVar);
                    isVar.f4505a = izVar;
                }
                iz izVar4 = isVar.f4505a;
                long j2 = (long) (izVar4.c - izVar4.b);
                isVar.f4505a = izVar4.a();
                if (this.f4505a == null) {
                    this.f4505a = izVar4;
                    iz izVar5 = this.f4505a;
                    iz izVar6 = this.f4505a;
                    iz izVar7 = this.f4505a;
                    izVar6.g = izVar7;
                    izVar5.f = izVar7;
                } else {
                    iz a2 = this.f4505a.g.a(izVar4);
                    if (a2.g == a2) {
                        throw new IllegalStateException();
                    } else if (a2.g.e) {
                        int i3 = a2.c - a2.b;
                        int i4 = 8192 - a2.g.c;
                        if (!a2.g.d) {
                            i = a2.g.b;
                        }
                        if (i3 <= i4 + i) {
                            a2.a(a2.g, i3);
                            a2.a();
                            ja.a(a2);
                        }
                    }
                }
                isVar.b -= j2;
                this.b += j2;
                j -= j2;
            }
        } else {
            throw new IllegalArgumentException("source == this");
        }
    }

    public final long b(is isVar, long j) {
        if (isVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            StringBuilder sb = new StringBuilder("byteCount < 0: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (this.b == 0) {
            return -1;
        } else {
            if (j > this.b) {
                j = this.b;
            }
            isVar.a(this, j);
            return j;
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof is)) {
            return false;
        }
        is isVar = (is) obj;
        if (this.b != isVar.b) {
            return false;
        }
        long j = 0;
        if (this.b == 0) {
            return true;
        }
        iz izVar = this.f4505a;
        iz izVar2 = isVar.f4505a;
        int i = izVar.b;
        int i2 = izVar2.b;
        while (j < this.b) {
            long min = (long) Math.min(izVar.c - i, izVar2.c - i2);
            int i3 = i2;
            int i4 = i;
            int i5 = 0;
            while (((long) i5) < min) {
                int i6 = i4 + 1;
                int i7 = i3 + 1;
                if (izVar.f4512a[i4] != izVar2.f4512a[i3]) {
                    return false;
                }
                i5++;
                i4 = i6;
                i3 = i7;
            }
            if (i4 == izVar.c) {
                izVar = izVar.f;
                i = izVar.b;
            } else {
                i = i4;
            }
            if (i3 == izVar2.c) {
                izVar2 = izVar2.f;
                i2 = izVar2.b;
            } else {
                i2 = i3;
            }
            j += min;
        }
        return true;
    }

    public final int hashCode() {
        iz izVar = this.f4505a;
        if (izVar == null) {
            return 0;
        }
        int i = 1;
        do {
            for (int i2 = izVar.b; i2 < izVar.c; i2++) {
                i = (i * 31) + izVar.f4512a[i2];
            }
            izVar = izVar.f;
        } while (izVar != this.f4505a);
        return i;
    }

    /* renamed from: h */
    public final is clone() {
        is isVar = new is();
        if (this.b == 0) {
            return isVar;
        }
        isVar.f4505a = new iz(this.f4505a);
        iz izVar = isVar.f4505a;
        iz izVar2 = isVar.f4505a;
        iz izVar3 = isVar.f4505a;
        izVar2.g = izVar3;
        izVar.f = izVar3;
        iz izVar4 = this.f4505a;
        while (true) {
            izVar4 = izVar4.f;
            if (izVar4 != this.f4505a) {
                isVar.f4505a.g.a(new iz(izVar4));
            } else {
                isVar.b = this.b;
                return isVar;
            }
        }
    }

    public final long f() {
        long j;
        if (this.b >= 8) {
            iz izVar = this.f4505a;
            int i = izVar.b;
            int i2 = izVar.c;
            if (i2 - i < 8) {
                j = ((((long) d()) & 4294967295L) << 32) | (4294967295L & ((long) d()));
            } else {
                byte[] bArr = izVar.f4512a;
                int i3 = i + 1;
                int i4 = i3 + 1;
                int i5 = i4 + 1;
                int i6 = i5 + 1;
                int i7 = i6 + 1;
                int i8 = i7 + 1;
                long j2 = ((((long) bArr[i]) & 255) << 56) | ((((long) bArr[i3]) & 255) << 48) | ((((long) bArr[i4]) & 255) << 40) | ((((long) bArr[i5]) & 255) << 32) | ((((long) bArr[i6]) & 255) << 24) | ((((long) bArr[i7]) & 255) << 16);
                int i9 = i8 + 1;
                long j3 = ((((long) bArr[i8]) & 255) << 8) | j2;
                int i10 = i9 + 1;
                long j4 = (((long) bArr[i9]) & 255) | j3;
                this.b -= 8;
                if (i10 == i2) {
                    this.f4505a = izVar.a();
                    ja.a(izVar);
                } else {
                    izVar.b = i10;
                }
                j = j4;
            }
            return jf.a(j);
        }
        StringBuilder sb = new StringBuilder("size < 8: ");
        sb.append(this.b);
        throw new IllegalStateException(sb.toString());
    }

    public final String toString() {
        iv ivVar;
        if (this.b <= 2147483647L) {
            int i = (int) this.b;
            if (i == 0) {
                ivVar = iv.b;
            } else {
                ivVar = new jb(this, i);
            }
            return ivVar.toString();
        }
        StringBuilder sb = new StringBuilder("size > Integer.MAX_VALUE: ");
        sb.append(this.b);
        throw new IllegalArgumentException(sb.toString());
    }
}
