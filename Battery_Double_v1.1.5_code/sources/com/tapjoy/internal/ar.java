package com.tapjoy.internal;

import java.util.Iterator;
import java.util.LinkedHashMap;

public final class ar extends aq {

    /* renamed from: a reason: collision with root package name */
    private final LinkedHashMap f4314a = new LinkedHashMap(0, 0.75f, true);
    private int b = 10;

    private void a() {
        int size = this.f4314a.size() - this.b;
        if (size > 0) {
            Iterator it = this.f4314a.entrySet().iterator();
            while (size > 0 && it.hasNext()) {
                size--;
                it.next();
                it.remove();
            }
        }
    }

    public final void a(Object obj, Object obj2) {
        super.a(obj, obj2);
        a();
    }

    /* access modifiers changed from: protected */
    public final ao a(Object obj, boolean z) {
        am amVar = (am) this.f4314a.get(obj);
        if (amVar != null || !z) {
            return amVar;
        }
        am amVar2 = new am(obj);
        this.f4314a.put(obj, amVar2);
        a();
        return amVar2;
    }
}
