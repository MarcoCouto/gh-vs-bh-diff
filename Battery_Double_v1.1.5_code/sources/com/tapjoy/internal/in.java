package com.tapjoy.internal;

import android.os.SystemClock;

public abstract class in implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final long f4498a = 300;

    public abstract boolean a();

    public void run() {
        long elapsedRealtime = SystemClock.elapsedRealtime() + this.f4498a;
        while (!a() && elapsedRealtime - SystemClock.elapsedRealtime() > 0) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException unused) {
                return;
            }
        }
    }
}
