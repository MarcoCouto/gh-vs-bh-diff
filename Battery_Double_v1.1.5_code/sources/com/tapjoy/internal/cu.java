package com.tapjoy.internal;

import android.webkit.WebView;
import java.util.ArrayList;
import java.util.List;

public final class cu {

    /* renamed from: a reason: collision with root package name */
    public final cx f4348a;
    final WebView b;
    public final List c = new ArrayList();
    final String d;
    public final String e;
    public final cv f;

    private cu(cx cxVar, String str, List list, String str2) {
        cv cvVar;
        this.f4348a = cxVar;
        this.b = null;
        this.d = str;
        if (list != null) {
            this.c.addAll(list);
            cvVar = cv.NATIVE;
        } else {
            cvVar = cv.HTML;
        }
        this.f = cvVar;
        this.e = str2;
    }

    public static cu a(cx cxVar, String str, List list, String str2) {
        dp.a((Object) cxVar, "Partner is null");
        dp.a((Object) str, "OMID JS script content is null");
        dp.a((Object) list, "VerificationScriptResources is null");
        if (str2.length() <= 256) {
            return new cu(cxVar, str, list, str2);
        }
        throw new IllegalArgumentException("CustomReferenceData is greater than 256 characters");
    }
}
