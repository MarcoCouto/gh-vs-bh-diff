package com.tapjoy.internal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import javax.annotation.Nullable;

public final class he {

    /* renamed from: a reason: collision with root package name */
    private final File f4457a;

    public he(File file) {
        this.f4457a = file;
    }

    public final synchronized boolean a() {
        FileOutputStream fileOutputStream;
        if (b() != null) {
            return false;
        }
        try {
            File file = this.f4457a;
            String uuid = UUID.randomUUID().toString();
            fileOutputStream = new FileOutputStream(file);
            bg.a((OutputStream) fileOutputStream, uuid);
            ka.a(fileOutputStream);
            if (b() != null) {
                return true;
            }
            return false;
        } catch (IOException e) {
            try {
                this.f4457a.delete();
                throw e;
            } catch (IOException unused) {
                return false;
            }
        } catch (Throwable th) {
            ka.a(fileOutputStream);
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String b() {
        if (this.f4457a.exists()) {
            try {
                String a2 = bg.a(this.f4457a, ak.c);
                if (a2.length() > 0) {
                    return a2;
                }
            } catch (IOException unused) {
            }
        }
        return null;
    }
}
