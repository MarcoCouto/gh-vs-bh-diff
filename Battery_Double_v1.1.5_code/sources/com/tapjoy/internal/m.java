package com.tapjoy.internal;

import android.content.SharedPreferences;

public abstract class m {

    /* renamed from: a reason: collision with root package name */
    protected SharedPreferences f4541a;
    protected String b;

    public m(SharedPreferences sharedPreferences, String str) {
        this.f4541a = sharedPreferences;
        this.b = str;
    }

    public final void c() {
        this.f4541a.edit().remove(this.b).apply();
    }
}
