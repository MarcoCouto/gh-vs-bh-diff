package com.tapjoy.internal;

import java.util.AbstractQueue;
import java.util.Iterator;

public abstract class at extends AbstractQueue implements ax {
    public Iterator iterator() {
        return new Iterator() {
            private int b = 0;

            public final boolean hasNext() {
                return this.b < at.this.size();
            }

            public final Object next() {
                at atVar = at.this;
                int i = this.b;
                this.b = i + 1;
                return atVar.a(i);
            }

            public final void remove() {
                if (this.b == 1) {
                    at.this.b(1);
                    this.b = 0;
                    return;
                }
                throw new UnsupportedOperationException("For the first element only");
            }
        };
    }
}
