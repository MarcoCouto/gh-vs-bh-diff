package com.tapjoy.internal;

import com.tapjoy.internal.dz.a;
import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ea implements a {

    /* renamed from: a reason: collision with root package name */
    private final BlockingQueue f4377a = new LinkedBlockingQueue();
    private final ThreadPoolExecutor b;
    private final ArrayDeque c = new ArrayDeque();
    private dz d = null;

    public ea() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, this.f4377a);
        this.b = threadPoolExecutor;
    }

    public final void a() {
        this.d = null;
        b();
    }

    public final void a(dz dzVar) {
        dzVar.d = this;
        this.c.add(dzVar);
        if (this.d == null) {
            b();
        }
    }

    private void b() {
        this.d = (dz) this.c.poll();
        if (this.d != null) {
            this.d.executeOnExecutor(this.b, new Object[0]);
        }
    }
}
