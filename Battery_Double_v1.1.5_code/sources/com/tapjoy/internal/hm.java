package com.tapjoy.internal;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import javax.annotation.Nullable;

public final class hm implements Flushable {

    /* renamed from: a reason: collision with root package name */
    final Object f4468a = this;
    ax b;
    private final File c;

    public hm(File file) {
        this.c = file;
        try {
            this.b = au.a((ax) new g(file, new bd() {
                public final /* bridge */ /* synthetic */ void a(OutputStream outputStream, Object obj) {
                    ew.c.a(outputStream, (Object) (ew) obj);
                }

                public final /* synthetic */ Object b(InputStream inputStream) {
                    return (ew) ew.c.a(inputStream);
                }
            }));
        } catch (Exception unused) {
            a();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.c.delete();
        if (this.b instanceof Closeable) {
            try {
                ((Closeable) this.b).close();
            } catch (Exception unused) {
            }
        }
        this.b = new av(new LinkedList());
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:5|6|7|8) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0011 */
    public final void flush() {
        synchronized (this.f4468a) {
            if (this.b instanceof Flushable) {
                ((Flushable) this.b).flush();
                a();
            }
        }
    }

    public final int b() {
        int size;
        synchronized (this.f4468a) {
            try {
                size = this.b.size();
            } catch (Exception unused) {
                a();
                return 0;
            } catch (Throwable th) {
                throw th;
            }
        }
        return size;
    }

    public final boolean c() {
        boolean isEmpty;
        synchronized (this.f4468a) {
            try {
                isEmpty = this.b.isEmpty();
            } catch (Exception unused) {
                a();
                return true;
            } catch (Throwable th) {
                throw th;
            }
        }
        return isEmpty;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x000b */
    public final void a(int i) {
        synchronized (this.f4468a) {
            this.b.b(i);
            a();
        }
    }

    @Nullable
    public final ew b(int i) {
        ew ewVar;
        synchronized (this.f4468a) {
            try {
                ewVar = (ew) this.b.a(i);
            } catch (Exception unused) {
                a();
                return null;
            } catch (Throwable th) {
                throw th;
            }
        }
        return ewVar;
    }
}
