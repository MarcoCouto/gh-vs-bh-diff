package com.tapjoy.internal;

import java.util.List;

public final class ex extends ei {
    public static final ek c = new b();
    public final List d;

    public static final class a extends com.tapjoy.internal.ei.a {
        public List c = ep.a();

        public final ex b() {
            return new ex(this.c, super.a());
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            ex exVar = (ex) obj;
            return ew.c.a().a(1, (Object) exVar.d) + exVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            ex exVar = (ex) obj;
            ew.c.a().a(emVar, 1, exVar.d);
            emVar.a(exVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, ex.class);
        }

        public final /* synthetic */ Object a(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b == -1) {
                    elVar.a(a2);
                    return aVar.b();
                } else if (b != 1) {
                    eh c = elVar.c();
                    aVar.a(b, c, c.a().a(elVar));
                } else {
                    aVar.c.add(ew.c.a(elVar));
                }
            }
        }
    }

    public ex(List list, iv ivVar) {
        super(c, ivVar);
        this.d = ep.a(EventEntry.TABLE_NAME, list);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ex)) {
            return false;
        }
        ex exVar = (ex) obj;
        return a().equals(exVar.a()) && this.d.equals(exVar.d);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int hashCode = (a().hashCode() * 37) + this.d.hashCode();
        this.b = hashCode;
        return hashCode;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (!this.d.isEmpty()) {
            sb.append(", events=");
            sb.append(this.d);
        }
        StringBuilder replace = sb.replace(0, 2, "EventBatch{");
        replace.append('}');
        return replace.toString();
    }
}
