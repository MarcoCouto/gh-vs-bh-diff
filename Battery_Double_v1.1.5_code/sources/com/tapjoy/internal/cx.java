package com.tapjoy.internal;

public final class cx {

    /* renamed from: a reason: collision with root package name */
    public final String f4351a;
    public final String b;

    private cx(String str, String str2) {
        this.f4351a = str;
        this.b = str2;
    }

    public static cx a(String str, String str2) {
        dp.a(str, "Name is null or empty");
        dp.a(str2, "Version is null or empty");
        return new cx(str, str2);
    }
}
