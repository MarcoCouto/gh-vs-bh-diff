package com.tapjoy.internal;

import io.fabric.sdk.android.services.common.CommonUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class ch {
    public static byte[] a(byte[] bArr) {
        try {
            return MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE).digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
