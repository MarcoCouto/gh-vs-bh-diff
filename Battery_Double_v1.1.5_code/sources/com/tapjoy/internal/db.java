package com.tapjoy.internal;

import com.ironsource.sdk.constants.Constants.ParametersKeys;
import org.json.JSONException;
import org.json.JSONObject;

public final class db {

    /* renamed from: a reason: collision with root package name */
    private final boolean f4355a = false;
    private final Float b = null;
    private final boolean c = false;
    private final da d;

    private db(da daVar) {
        this.d = daVar;
    }

    public static db a(da daVar) {
        dp.a((Object) daVar, "Position is null");
        return new db(daVar);
    }

    /* access modifiers changed from: 0000 */
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("skippable", this.f4355a);
            if (this.f4355a) {
                jSONObject.put("skipOffset", this.b);
            }
            jSONObject.put("autoPlay", this.c);
            jSONObject.put(ParametersKeys.POSITION, this.d);
        } catch (JSONException e) {
            dn.a("VastProperties: JSON error", e);
        }
        return jSONObject;
    }
}
