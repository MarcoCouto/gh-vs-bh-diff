package com.tapjoy.internal;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

public final class bw extends WeakReference {

    /* renamed from: a reason: collision with root package name */
    public final Object f4334a;

    public bw(Object obj, Object obj2, ReferenceQueue referenceQueue) {
        super(obj2, referenceQueue);
        this.f4334a = obj;
    }
}
