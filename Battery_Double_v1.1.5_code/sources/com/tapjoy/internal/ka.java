package com.tapjoy.internal;

import java.io.Closeable;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;

public final class ka {

    /* renamed from: a reason: collision with root package name */
    static final Logger f4529a = Logger.getLogger(ka.class.getName());

    private ka() {
    }

    public static void a(@Nullable Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                try {
                    f4529a.log(Level.WARNING, "IOException thrown while closing Closeable.", e);
                } catch (IOException e2) {
                    f4529a.log(Level.SEVERE, "IOException should not have been thrown.", e2);
                }
            }
        }
    }
}
