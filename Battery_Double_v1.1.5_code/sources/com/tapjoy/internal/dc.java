package com.tapjoy.internal;

import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public final class dc {

    /* renamed from: a reason: collision with root package name */
    private final cz f4356a;

    private dc(cz czVar) {
        this.f4356a = czVar;
    }

    private static void b(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Invalid Video volume");
        }
    }

    public static dc a(cs csVar) {
        cz czVar = (cz) csVar;
        dp.a((Object) csVar, "AdSession is null");
        if (!(cw.NATIVE == czVar.f4353a.b)) {
            throw new IllegalStateException("Cannot create VideoEvents for JavaScript AdSession");
        } else if (!czVar.d) {
            dp.a(czVar);
            if (czVar.c.c == null) {
                dc dcVar = new dc(czVar);
                czVar.c.c = dcVar;
                return dcVar;
            }
            throw new IllegalStateException("VideoEvents already exists for AdSession");
        } else {
            throw new IllegalStateException("AdSession is started");
        }
    }

    public final void a(db dbVar) {
        dp.a((Object) dbVar, "VastProperties is null");
        dp.a(this.f4356a);
        this.f4356a.c.a(ParametersKeys.LOADED, dbVar.a());
    }

    public final void a(float f, float f2) {
        if (f > 0.0f) {
            b(f2);
            dp.b(this.f4356a);
            JSONObject jSONObject = new JSONObject();
            dm.a(jSONObject, IronSourceConstants.EVENTS_DURATION, Float.valueOf(f));
            dm.a(jSONObject, "videoPlayerVolume", Float.valueOf(f2));
            dm.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(dh.a().f4363a));
            this.f4356a.c.a("start", jSONObject);
            return;
        }
        throw new IllegalArgumentException("Invalid Video duration");
    }

    public final void a() {
        dp.b(this.f4356a);
        this.f4356a.c.a("firstQuartile");
    }

    public final void b() {
        dp.b(this.f4356a);
        this.f4356a.c.a("midpoint");
    }

    public final void c() {
        dp.b(this.f4356a);
        this.f4356a.c.a("thirdQuartile");
    }

    public final void d() {
        dp.b(this.f4356a);
        this.f4356a.c.a("complete");
    }

    public final void e() {
        dp.b(this.f4356a);
        this.f4356a.c.a(CampaignEx.JSON_NATIVE_VIDEO_PAUSE);
    }

    public final void f() {
        dp.b(this.f4356a);
        this.f4356a.c.a(CampaignEx.JSON_NATIVE_VIDEO_RESUME);
    }

    public final void g() {
        dp.b(this.f4356a);
        this.f4356a.c.a(String.VIDEO_BUFFER_START);
    }

    public final void h() {
        dp.b(this.f4356a);
        this.f4356a.c.a("bufferFinish");
    }

    public final void i() {
        dp.b(this.f4356a);
        this.f4356a.c.a(String.VIDEO_SKIPPED);
    }

    public final void a(float f) {
        b(f);
        dp.b(this.f4356a);
        JSONObject jSONObject = new JSONObject();
        dm.a(jSONObject, "videoPlayerVolume", Float.valueOf(f));
        dm.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(dh.a().f4363a));
        this.f4356a.c.a("volumeChange", jSONObject);
    }
}
