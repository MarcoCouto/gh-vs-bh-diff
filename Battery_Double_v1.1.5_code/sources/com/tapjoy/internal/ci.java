package com.tapjoy.internal;

import java.math.BigInteger;

public final class ci extends Number {

    /* renamed from: a reason: collision with root package name */
    private final String f4341a;

    public ci(String str) {
        this.f4341a = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Number)) {
            return false;
        }
        Number number = (Number) obj;
        if (number instanceof Integer) {
            return intValue() == number.intValue();
        }
        if (number instanceof Long) {
            return longValue() == number.longValue();
        }
        if (number instanceof Float) {
            return floatValue() == number.floatValue();
        }
        if (number instanceof Double) {
            return doubleValue() == number.doubleValue();
        }
        return this.f4341a.equals(number.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        return (int) java.lang.Long.parseLong(r2.f4341a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        return new java.math.BigInteger(r2.f4341a).intValue();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0007 */
    public final int intValue() {
        return Integer.parseInt(this.f4341a);
    }

    public final long longValue() {
        try {
            return Long.parseLong(this.f4341a);
        } catch (NumberFormatException unused) {
            return new BigInteger(this.f4341a).longValue();
        }
    }

    public final float floatValue() {
        return Float.parseFloat(this.f4341a);
    }

    public final double doubleValue() {
        return Double.parseDouble(this.f4341a);
    }

    public final String toString() {
        return this.f4341a;
    }
}
