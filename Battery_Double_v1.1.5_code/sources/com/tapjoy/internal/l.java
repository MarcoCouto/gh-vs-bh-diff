package com.tapjoy.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class l extends m {
    private final long c = 0;

    public l(SharedPreferences sharedPreferences, String str) {
        super(sharedPreferences, str);
    }

    public final long a() {
        return this.f4541a.getLong(this.b, this.c);
    }

    public final void a(long j) {
        this.f4541a.edit().putLong(this.b, j).apply();
    }

    public final Editor a(Editor editor) {
        return editor.remove(this.b);
    }

    public final Editor a(Editor editor, long j) {
        return editor.putLong(this.b, j);
    }
}
