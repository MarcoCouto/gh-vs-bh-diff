package com.tapjoy.internal;

import android.os.Handler;
import android.os.Looper;

public class hb implements gp {

    /* renamed from: a reason: collision with root package name */
    private static final hb f4449a = new hb() {
        public final void a(String str) {
        }

        public final void a(String str, gm gmVar) {
        }

        public final void a(String str, String str2, gm gmVar) {
        }

        public final void b(String str) {
        }

        public final void c(String str) {
        }

        public final void d(String str) {
        }
    };
    /* access modifiers changed from: private */
    public final gp b;
    private final ba c;

    /* synthetic */ hb(byte b2) {
        this();
    }

    public static hb a(gp gpVar) {
        if (!(!(gpVar instanceof hb))) {
            throw new IllegalArgumentException();
        } else if (gpVar != null) {
            return new hb(gpVar);
        } else {
            return f4449a;
        }
    }

    private hb() {
        this.b = null;
        this.c = null;
    }

    private hb(gp gpVar) {
        Handler handler;
        this.b = gpVar;
        Looper myLooper = Looper.myLooper();
        if (myLooper != null) {
            jq.a(myLooper);
            handler = myLooper == Looper.getMainLooper() ? u.a() : new Handler(myLooper);
        } else {
            handler = null;
        }
        if (handler != null) {
            this.c = u.a(handler);
            new Object[1][0] = handler.getLooper();
        } else if (Thread.currentThread() == gs.b()) {
            this.c = gs.f4437a;
        } else {
            this.c = u.a(u.a());
        }
    }

    public void a(final String str) {
        this.c.a(new Runnable() {
            public final void run() {
                hb.this.b.a(str);
            }
        });
    }

    public void b(final String str) {
        this.c.a(new Runnable() {
            public final void run() {
                hb.this.b.b(str);
            }
        });
    }

    public void c(final String str) {
        this.c.a(new Runnable() {
            public final void run() {
                hb.this.b.c(str);
            }
        });
    }

    public void d(final String str) {
        this.c.a(new Runnable() {
            public final void run() {
                hb.this.b.d(str);
            }
        });
    }

    public void a(final String str, final gm gmVar) {
        this.c.a(new Runnable() {
            public final void run() {
                hb.this.b.a(str, gmVar);
            }
        });
    }

    public void a(final String str, final String str2, final gm gmVar) {
        this.c.a(new Runnable() {
            public final void run() {
                hb.this.b.a(str, str2, gmVar);
            }
        });
    }
}
