package com.tapjoy.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.tapjoy.TapjoyConnectCore;
import com.tapjoy.TapjoyConstants;

public final class gf {
    private static final gf e;
    private static gf f;

    /* renamed from: a reason: collision with root package name */
    public Boolean f4430a = null;
    public String b = null;
    public Boolean c = null;
    public boolean d = false;
    private Context g;

    static {
        gf gfVar = new gf();
        e = gfVar;
        f = gfVar;
    }

    public static gf a() {
        return f;
    }

    public final synchronized void a(Context context) {
        if (context != null) {
            try {
                if (this.g == null) {
                    this.g = context;
                }
            } finally {
            }
        }
        gf gfVar = f;
        if (gfVar.g != null) {
            SharedPreferences sharedPreferences = gfVar.g.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0);
            if (gfVar.f4430a == null && sharedPreferences.contains("gdpr")) {
                gfVar.f4430a = Boolean.valueOf(sharedPreferences.getBoolean("gdpr", false));
            }
            if (gfVar.b == null) {
                gfVar.b = sharedPreferences.getString("cgdpr", "");
            }
            if (gfVar.c == null && sharedPreferences.contains(TapjoyConstants.PREF_BELOW_CONSENT_AGE)) {
                gfVar.c = Boolean.valueOf(sharedPreferences.getBoolean(TapjoyConstants.PREF_BELOW_CONSENT_AGE, false));
            }
        }
        if (this.d) {
            this.d = false;
            gf gfVar2 = f;
            if (gfVar2.g != null) {
                if (gfVar2.f4430a != null) {
                    gfVar2.b();
                }
                if (gfVar2.b != null) {
                    gfVar2.c();
                }
                if (gfVar2.c != null) {
                    gfVar2.d();
                }
            }
        }
    }

    public final boolean b() {
        if (this.g == null) {
            return false;
        }
        Editor edit = this.g.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0).edit();
        edit.putBoolean("gdpr", this.f4430a.booleanValue());
        edit.apply();
        return true;
    }

    public final boolean c() {
        if (this.g == null) {
            return false;
        }
        Editor edit = this.g.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0).edit();
        edit.putString("cgdpr", this.b);
        edit.apply();
        return true;
    }

    public final boolean d() {
        if (this.g == null) {
            return false;
        }
        Editor edit = this.g.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0).edit();
        edit.putBoolean(TapjoyConstants.PREF_BELOW_CONSENT_AGE, this.c.booleanValue());
        edit.apply();
        TapjoyConnectCore.setAdTrackingEnabled();
        return true;
    }
}
