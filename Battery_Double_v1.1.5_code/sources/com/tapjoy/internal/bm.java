package com.tapjoy.internal;

import java.io.Writer;

public final class bm implements bl {

    /* renamed from: a reason: collision with root package name */
    public final String f4324a;

    public bm(String str) {
        this.f4324a = str;
    }

    public final void a(Writer writer) {
        writer.write(this.f4324a);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bm)) {
            return false;
        }
        return this.f4324a.equals(((bm) obj).f4324a);
    }

    public final int hashCode() {
        return this.f4324a.hashCode();
    }

    public final String toString() {
        return this.f4324a;
    }
}
