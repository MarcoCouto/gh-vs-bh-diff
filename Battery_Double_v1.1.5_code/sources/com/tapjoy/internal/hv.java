package com.tapjoy.internal;

import android.graphics.Point;
import android.os.SystemClock;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

public final class hv extends hr {
    public static final bi n = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            return new hv(bnVar);
        }
    };
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public hy f4482a;
    @Nullable
    public hy b;
    public hy c;
    @Nullable
    public Point d;
    @Nullable
    public hy e;
    @Nullable
    public hy f;
    public String g;
    @Nullable
    public gm h;
    public ArrayList i = new ArrayList();
    public ArrayList j = new ArrayList();
    public Map k;
    public long l;
    @Nullable
    public hw m;

    public hv() {
    }

    hv(bn bnVar) {
        bnVar.h();
        String str = null;
        String str2 = null;
        while (bnVar.j()) {
            String l2 = bnVar.l();
            if ("frame".equals(l2)) {
                bnVar.h();
                while (bnVar.j()) {
                    String l3 = bnVar.l();
                    if ("portrait".equals(l3)) {
                        this.f4482a = (hy) hy.e.a(bnVar);
                    } else if ("landscape".equals(l3)) {
                        this.b = (hy) hy.e.a(bnVar);
                    } else if ("close_button".equals(l3)) {
                        this.c = (hy) hy.e.a(bnVar);
                    } else if ("close_button_offset".equals(l3)) {
                        this.d = (Point) bj.f4322a.a(bnVar);
                    } else {
                        bnVar.s();
                    }
                }
                bnVar.i();
            } else if ("creative".equals(l2)) {
                bnVar.h();
                while (bnVar.j()) {
                    String l4 = bnVar.l();
                    if ("portrait".equals(l4)) {
                        this.e = (hy) hy.e.a(bnVar);
                    } else if ("landscape".equals(l4)) {
                        this.f = (hy) hy.e.a(bnVar);
                    } else {
                        bnVar.s();
                    }
                }
                bnVar.i();
            } else if ("url".equals(l2)) {
                this.g = bnVar.b();
            } else if (hp.a(l2)) {
                this.h = hp.a(l2, bnVar);
            } else if ("mappings".equals(l2)) {
                bnVar.h();
                while (bnVar.j()) {
                    String l5 = bnVar.l();
                    if ("portrait".equals(l5)) {
                        bnVar.a((List) this.i, ht.h);
                    } else if ("landscape".equals(l5)) {
                        bnVar.a((List) this.j, ht.h);
                    } else {
                        bnVar.s();
                    }
                }
                bnVar.i();
            } else if ("meta".equals(l2)) {
                this.k = bnVar.d();
            } else if ("ttl".equals(l2)) {
                this.l = SystemClock.elapsedRealtime() + ((long) (bnVar.p() * 1000.0d));
            } else if ("no_more_today".equals(l2)) {
                this.m = (hw) hw.d.a(bnVar);
            } else if ("ad_content".equals(l2)) {
                str2 = bnVar.b();
            } else if (TapjoyConstants.TJC_REDIRECT_URL.equals(l2)) {
                str = bnVar.b();
            } else {
                bnVar.s();
            }
        }
        bnVar.i();
        if (this.g == null) {
            this.g = "";
        }
        if (this.i != null) {
            Iterator it = this.i.iterator();
            while (it.hasNext()) {
                ht htVar = (ht) it.next();
                if (htVar.f == null) {
                    htVar.f = str2;
                }
                if (htVar.e == null) {
                    htVar.e = str;
                }
            }
        }
        if (this.j != null) {
            Iterator it2 = this.j.iterator();
            while (it2.hasNext()) {
                ht htVar2 = (ht) it2.next();
                if (htVar2.f == null) {
                    htVar2.f = str2;
                }
                if (htVar2.e == null) {
                    htVar2.e = str;
                }
            }
        }
    }

    public final boolean a() {
        return (this.c == null || this.f4482a == null || this.e == null) ? false : true;
    }

    public final boolean b() {
        return (this.c == null || this.b == null || this.f == null) ? false : true;
    }
}
