package com.tapjoy.internal;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.WindowManager;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class dm {

    /* renamed from: a reason: collision with root package name */
    static float f4367a = Resources.getSystem().getDisplayMetrics().density;
    private static WindowManager b;
    private static String[] c = {AvidJSONUtil.KEY_X, AvidJSONUtil.KEY_Y, "width", "height"};

    static class a {

        /* renamed from: a reason: collision with root package name */
        final float f4368a;
        final float b;

        a(float f, float f2) {
            this.f4368a = f;
            this.b = f2;
        }
    }

    public static void a(Context context) {
        if (context != null) {
            f4367a = context.getResources().getDisplayMetrics().density;
            b = (WindowManager) context.getSystemService("window");
        }
    }

    public static void a(JSONObject jSONObject, String str) {
        try {
            jSONObject.put("adSessionId", str);
        } catch (JSONException e) {
            dn.a("Error with setting ad session id", e);
        }
    }

    public static void a(JSONObject jSONObject, String str, Object obj) {
        try {
            jSONObject.put(str, obj);
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder("JSONException during JSONObject.put for name [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            dn.a(sb.toString(), e);
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r3v0, types: [java.util.List, java.util.List<java.lang.String>] */
    public static void a(JSONObject jSONObject, List<String> list) {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put(put);
        }
        try {
            jSONObject.put(AvidJSONUtil.KEY_IS_FRIENDLY_OBSTRUCTION_FOR, jSONArray);
        } catch (JSONException e) {
            dn.a("Error with setting friendly obstruction", e);
        }
    }

    public static void a(JSONObject jSONObject, JSONObject jSONObject2) {
        try {
            JSONArray optJSONArray = jSONObject.optJSONArray(AvidJSONUtil.KEY_CHILD_VIEWS);
            if (optJSONArray == null) {
                optJSONArray = new JSONArray();
                jSONObject.put(AvidJSONUtil.KEY_CHILD_VIEWS, optJSONArray);
            }
            optJSONArray.put(jSONObject2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static boolean a(JSONArray jSONArray, JSONArray jSONArray2) {
        if (jSONArray == null && jSONArray2 == null) {
            return true;
        }
        return (jSONArray == null || jSONArray2 == null || jSONArray.length() != jSONArray2.length()) ? false : true;
    }

    public static JSONObject a(int i, int i2, int i3, int i4) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(AvidJSONUtil.KEY_X, (double) (((float) i) / f4367a));
            jSONObject.put(AvidJSONUtil.KEY_Y, (double) (((float) i2) / f4367a));
            jSONObject.put("width", (double) (((float) i3) / f4367a));
            jSONObject.put("height", (double) (((float) i4) / f4367a));
        } catch (JSONException e) {
            dn.a("Error with creating viewStateObject", e);
        }
        return jSONObject;
    }

    public static void a(JSONObject jSONObject) {
        float f;
        float f2 = 0.0f;
        if (VERSION.SDK_INT < 17) {
            JSONArray optJSONArray = jSONObject.optJSONArray(AvidJSONUtil.KEY_CHILD_VIEWS);
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                float f3 = 0.0f;
                for (int i = 0; i < length; i++) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        double optDouble = optJSONObject.optDouble(AvidJSONUtil.KEY_X);
                        double optDouble2 = optJSONObject.optDouble(AvidJSONUtil.KEY_Y);
                        double optDouble3 = optJSONObject.optDouble("width");
                        double optDouble4 = optJSONObject.optDouble("height");
                        f2 = Math.max(f2, (float) (optDouble + optDouble3));
                        f3 = Math.max(f3, (float) (optDouble2 + optDouble4));
                    }
                }
                f = f3;
                a aVar = new a(f2, f);
                jSONObject.put("width", (double) aVar.f4368a);
                jSONObject.put("height", (double) aVar.b);
            }
        } else if (b != null) {
            Point point = new Point(0, 0);
            b.getDefaultDisplay().getRealSize(point);
            f2 = ((float) point.x) / f4367a;
            f = ((float) point.y) / f4367a;
            a aVar2 = new a(f2, f);
            jSONObject.put("width", (double) aVar2.f4368a);
            jSONObject.put("height", (double) aVar2.b);
        }
        f = 0.0f;
        a aVar22 = new a(f2, f);
        try {
            jSONObject.put("width", (double) aVar22.f4368a);
            jSONObject.put("height", (double) aVar22.b);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ab A[RETURN] */
    public static boolean b(@NonNull JSONObject jSONObject, @Nullable JSONObject jSONObject2) {
        boolean z;
        boolean z2;
        boolean z3;
        if (jSONObject == null && jSONObject2 == null) {
            return true;
        }
        if (jSONObject == null || jSONObject2 == null) {
            return false;
        }
        String[] strArr = c;
        int length = strArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = true;
                break;
            }
            String str = strArr[i];
            if (jSONObject.optDouble(str) != jSONObject2.optDouble(str)) {
                z = false;
                break;
            }
            i++;
        }
        if (z && jSONObject.optString("adSessionId", "").equals(jSONObject2.optString("adSessionId", ""))) {
            JSONArray optJSONArray = jSONObject.optJSONArray(AvidJSONUtil.KEY_IS_FRIENDLY_OBSTRUCTION_FOR);
            JSONArray optJSONArray2 = jSONObject2.optJSONArray(AvidJSONUtil.KEY_IS_FRIENDLY_OBSTRUCTION_FOR);
            if (optJSONArray != null || optJSONArray2 != null) {
                if (a(optJSONArray, optJSONArray2)) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= optJSONArray.length()) {
                            break;
                        } else if (!optJSONArray.optString(i2, "").equals(optJSONArray2.optString(i2, ""))) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
                z2 = false;
                if (z2) {
                    JSONArray optJSONArray3 = jSONObject.optJSONArray(AvidJSONUtil.KEY_CHILD_VIEWS);
                    JSONArray optJSONArray4 = jSONObject2.optJSONArray(AvidJSONUtil.KEY_CHILD_VIEWS);
                    if (optJSONArray3 != null || optJSONArray4 != null) {
                        if (a(optJSONArray3, optJSONArray4)) {
                            int i3 = 0;
                            while (true) {
                                if (i3 >= optJSONArray3.length()) {
                                    break;
                                } else if (!b(optJSONArray3.optJSONObject(i3), optJSONArray4.optJSONObject(i3))) {
                                    break;
                                } else {
                                    i3++;
                                }
                            }
                        }
                        z3 = false;
                        if (!z3) {
                            return true;
                        }
                    }
                    z3 = true;
                    if (!z3) {
                        return false;
                    }
                }
            }
            z2 = true;
            if (z2) {
            }
        }
        return false;
    }
}
