package com.tapjoy.internal;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import javax.annotation.Nullable;

public abstract class kc implements kf {

    /* renamed from: a reason: collision with root package name */
    private final a f4534a = new a();
    private final ke b = new ke();

    static final class a extends AbstractQueuedSynchronizer {

        /* renamed from: a reason: collision with root package name */
        private Object f4535a;
        private Throwable b;

        a() {
        }

        /* access modifiers changed from: protected */
        public final int tryAcquireShared(int i) {
            return b() ? 1 : -1;
        }

        /* access modifiers changed from: protected */
        public final boolean tryReleaseShared(int i) {
            setState(i);
            return true;
        }

        /* access modifiers changed from: 0000 */
        public final Object a() {
            int state = getState();
            if (state != 2) {
                if (state != 4) {
                    StringBuilder sb = new StringBuilder("Error, synchronizer in invalid state: ");
                    sb.append(state);
                    throw new IllegalStateException(sb.toString());
                }
                throw new CancellationException("Task was cancelled.");
            } else if (this.b == null) {
                return this.f4535a;
            } else {
                throw new ExecutionException(this.b);
            }
        }

        /* access modifiers changed from: 0000 */
        public final boolean b() {
            return (getState() & 6) != 0;
        }

        /* access modifiers changed from: 0000 */
        public final boolean c() {
            return getState() == 4;
        }

        /* access modifiers changed from: 0000 */
        public final boolean a(@Nullable Object obj, @Nullable Throwable th, int i) {
            boolean compareAndSetState = compareAndSetState(0, 1);
            if (compareAndSetState) {
                this.f4535a = obj;
                this.b = th;
                releaseShared(i);
            } else if (getState() == 1) {
                acquireShared(-1);
            }
            return compareAndSetState;
        }
    }

    public Object get(long j, TimeUnit timeUnit) {
        a aVar = this.f4534a;
        if (aVar.tryAcquireSharedNanos(-1, timeUnit.toNanos(j))) {
            return aVar.a();
        }
        throw new TimeoutException("Timeout waiting for task.");
    }

    public Object get() {
        a aVar = this.f4534a;
        aVar.acquireSharedInterruptibly(-1);
        return aVar.a();
    }

    public boolean isDone() {
        return this.f4534a.b();
    }

    public boolean isCancelled() {
        return this.f4534a.c();
    }

    public boolean cancel(boolean z) {
        if (!this.f4534a.a(null, null, 4)) {
            return false;
        }
        this.b.a();
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean a(@Nullable Object obj) {
        boolean a2 = this.f4534a.a(obj, null, 2);
        if (a2) {
            this.b.a();
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final boolean a(Throwable th) {
        boolean a2 = this.f4534a.a(null, (Throwable) jq.a(th), 2);
        if (a2) {
            this.b.a();
        }
        if (!(th instanceof Error)) {
            return a2;
        }
        throw ((Error) th);
    }
}
