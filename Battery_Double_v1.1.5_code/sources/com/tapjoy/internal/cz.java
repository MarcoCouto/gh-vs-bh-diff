package com.tapjoy.internal;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings.System;
import android.view.View;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.json.JSONObject;

public final class cz extends cs {

    /* renamed from: a reason: collision with root package name */
    public final ct f4353a;
    public final List b = new ArrayList();
    public dt c;
    public boolean d = false;
    public boolean e = false;
    public String f;
    boolean g;
    private final cu h;
    private dr i;

    private void b(View view) {
        this.i = new dr(view);
    }

    public final View c() {
        return (View) this.i.get();
    }

    public final boolean d() {
        return this.d && !this.e;
    }

    cz(ct ctVar, cu cuVar) {
        this.f4353a = ctVar;
        this.h = cuVar;
        this.f = UUID.randomUUID().toString();
        b(null);
        if (cuVar.f == cv.HTML) {
            this.c = new du(cuVar.b);
        } else {
            this.c = new dv(Collections.unmodifiableList(cuVar.c), cuVar.d);
        }
        this.c.a();
        dd.a().f4357a.add(this);
        dt dtVar = this.c;
        dg a2 = dg.a();
        WebView c2 = dtVar.c();
        JSONObject jSONObject = new JSONObject();
        dm.a(jSONObject, "impressionOwner", ctVar.f4347a);
        dm.a(jSONObject, "videoEventsOwner", ctVar.b);
        dm.a(jSONObject, "isolateVerificationScripts", Boolean.valueOf(ctVar.c));
        a2.a(c2, "init", jSONObject);
    }

    public final void a() {
        if (!this.d) {
            this.d = true;
            dd a2 = dd.a();
            boolean b2 = a2.b();
            a2.b.add(this);
            if (!b2) {
                dh a3 = dh.a();
                de.a().e = a3;
                de a4 = de.a();
                a4.b = new BroadcastReceiver() {
                    public final void onReceive(Context context, Intent intent) {
                        if (intent != null) {
                            if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                                de.a(de.this, true);
                            } else if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                                de.a(de.this, false);
                            } else {
                                if ("android.intent.action.SCREEN_ON".equals(intent.getAction())) {
                                    KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
                                    if (keyguardManager != null && !keyguardManager.inKeyguardRestrictedInputMode()) {
                                        de.a(de.this, false);
                                    }
                                }
                            }
                        }
                    }
                };
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                intentFilter.addAction("android.intent.action.SCREEN_ON");
                intentFilter.addAction("android.intent.action.USER_PRESENT");
                a4.f4358a.registerReceiver(a4.b, intentFilter);
                a4.c = true;
                a4.c();
                if (de.a().b()) {
                    dw.a();
                    dw.b();
                }
                cp cpVar = a3.b;
                cpVar.b = cpVar.a();
                cpVar.b();
                cpVar.f4345a.getContentResolver().registerContentObserver(System.CONTENT_URI, true, cpVar);
            }
            this.c.a(dh.a().f4363a);
            this.c.a(this, this.h);
        }
    }

    public final void a(View view) {
        if (!this.e) {
            dp.a((Object) view, "AdView is null");
            if (c() != view) {
                b(view);
                this.c.d();
                Collection<cz> unmodifiableCollection = Collections.unmodifiableCollection(dd.a().f4357a);
                if (unmodifiableCollection != null && unmodifiableCollection.size() > 0) {
                    for (cz czVar : unmodifiableCollection) {
                        if (czVar != this && czVar.c() == view) {
                            czVar.i.clear();
                        }
                    }
                }
            }
        }
    }

    public final void b() {
        if (!this.e) {
            this.i.clear();
            if (!this.e) {
                this.b.clear();
            }
            this.e = true;
            dg.a().a(this.c.c(), "finishSession", new Object[0]);
            dd a2 = dd.a();
            boolean b2 = a2.b();
            a2.f4357a.remove(this);
            a2.b.remove(this);
            if (b2 && !a2.b()) {
                dh a3 = dh.a();
                dw a4 = dw.a();
                dw.c();
                a4.b.clear();
                dw.f4372a.post(new Runnable() {
                    public final void run() {
                        dw.this.h.b();
                    }
                });
                de a5 = de.a();
                if (!(a5.f4358a == null || a5.b == null)) {
                    a5.f4358a.unregisterReceiver(a5.b);
                    a5.b = null;
                }
                a5.c = false;
                a5.d = false;
                a5.e = null;
                cp cpVar = a3.b;
                cpVar.f4345a.getContentResolver().unregisterContentObserver(cpVar);
            }
            this.c.b();
            this.c = null;
        }
    }
}
