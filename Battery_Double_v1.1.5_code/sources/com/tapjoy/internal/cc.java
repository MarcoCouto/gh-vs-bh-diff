package com.tapjoy.internal;

import javax.annotation.Nullable;

public final class cc implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ca f4339a;
    @Nullable
    private final cf b;

    protected cc(ca caVar, @Nullable cf cfVar) {
        this.f4339a = caVar;
        this.b = cfVar;
    }

    public final void run() {
        try {
            Object f = this.f4339a.f();
            if (this.b != null && !(this.b instanceof cg)) {
                this.b.a(this.f4339a, f);
            }
        } catch (Throwable unused) {
            if (this.b != null && !(this.b instanceof cg)) {
                this.b.a(this.f4339a);
            }
        }
    }
}
