package com.tapjoy.internal;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.internal.kg.a;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

public abstract class kb implements kg {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Logger f4530a = Logger.getLogger(kb.class.getName());
    private final kg b = new kd() {
        /* access modifiers changed from: protected */
        public final void a() {
            new Executor() {
                public final void execute(Runnable runnable) {
                    new Thread(runnable, kb.this.getClass().getSimpleName()).start();
                }
            }.execute(new Runnable() {
                public final void run() {
                    try {
                        kb.this.b();
                        AnonymousClass1.this.c();
                        if (AnonymousClass1.this.f() == a.RUNNING) {
                            kb.this.d();
                        }
                        kb.this.c();
                        AnonymousClass1.this.d();
                    } catch (Throwable th) {
                        AnonymousClass1.this.a(th);
                        throw js.a(th);
                    }
                }
            });
        }

        /* access modifiers changed from: protected */
        public final void b() {
            kb.this.a();
        }
    };

    public void a() {
    }

    public void b() {
    }

    public void c() {
    }

    public abstract void d();

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(f());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public final kf e() {
        return this.b.e();
    }

    public final a f() {
        return this.b.f();
    }
}
