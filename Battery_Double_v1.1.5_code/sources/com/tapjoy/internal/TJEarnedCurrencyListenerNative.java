package com.tapjoy.internal;

import com.tapjoy.TJEarnedCurrencyListener;

public class TJEarnedCurrencyListenerNative implements TJEarnedCurrencyListener {

    /* renamed from: a reason: collision with root package name */
    private final long f4296a;

    private static native void onEarnedCurrencyNative(long j, String str, int i);

    private TJEarnedCurrencyListenerNative(long j) {
        if (j != 0) {
            this.f4296a = j;
            return;
        }
        throw new IllegalArgumentException();
    }

    public void onEarnedCurrency(String str, int i) {
        onEarnedCurrencyNative(this.f4296a, str, i);
    }

    @fu
    static Object create(long j) {
        return new TJEarnedCurrencyListenerNative(j);
    }
}
