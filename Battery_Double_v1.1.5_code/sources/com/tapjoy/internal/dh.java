package com.tapjoy.internal;

import com.tapjoy.internal.de.a;
import java.util.Collections;

public final class dh implements co, a {
    private static dh c;

    /* renamed from: a reason: collision with root package name */
    public float f4363a = 0.0f;
    public cp b;
    private final cq d;
    private final cn e;
    private dd f;

    private dh(cq cqVar, cn cnVar) {
        this.d = cqVar;
        this.e = cnVar;
    }

    public static dh a() {
        if (c == null) {
            c = new dh(new cq(), new cn());
        }
        return c;
    }

    public final void a(boolean z) {
        if (z) {
            dw.a();
            dw.b();
            return;
        }
        dw.a();
        dw.c();
    }

    public final void a(float f2) {
        this.f4363a = f2;
        if (this.f == null) {
            this.f = dd.a();
        }
        for (cz czVar : Collections.unmodifiableCollection(this.f.b)) {
            czVar.c.a(f2);
        }
    }
}
