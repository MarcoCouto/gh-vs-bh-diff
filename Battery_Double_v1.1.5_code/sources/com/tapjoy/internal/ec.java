package com.tapjoy.internal;

import com.tapjoy.internal.dt.a;
import com.tapjoy.internal.dz.b;
import java.util.Collections;
import java.util.HashSet;
import org.json.JSONObject;

public final class ec extends dy {
    public ec(b bVar, HashSet hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void onPostExecute(String str) {
        dd a2 = dd.a();
        if (a2 != null) {
            for (cz czVar : Collections.unmodifiableCollection(a2.f4357a)) {
                if (this.f4375a.contains(czVar.f)) {
                    dt dtVar = czVar.c;
                    if (this.c > dtVar.e && dtVar.d != a.c) {
                        dtVar.d = a.c;
                        dg.a().b(dtVar.c(), str);
                    }
                }
            }
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        return this.b.toString();
    }
}
