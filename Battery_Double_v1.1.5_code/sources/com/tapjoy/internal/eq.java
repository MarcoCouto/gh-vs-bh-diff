package com.tapjoy.internal;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

final class eq extends AbstractList implements Serializable, RandomAccess {

    /* renamed from: a reason: collision with root package name */
    List f4389a;
    private final List b;

    eq(List list) {
        this.b = list;
        this.f4389a = list;
    }

    public final Object get(int i) {
        return this.f4389a.get(i);
    }

    public final int size() {
        return this.f4389a.size();
    }

    public final Object set(int i, Object obj) {
        if (this.f4389a == this.b) {
            this.f4389a = new ArrayList(this.b);
        }
        return this.f4389a.set(i, obj);
    }

    public final void add(int i, Object obj) {
        if (this.f4389a == this.b) {
            this.f4389a = new ArrayList(this.b);
        }
        this.f4389a.add(i, obj);
    }

    public final Object remove(int i) {
        if (this.f4389a == this.b) {
            this.f4389a = new ArrayList(this.b);
        }
        return this.f4389a.remove(i);
    }
}
