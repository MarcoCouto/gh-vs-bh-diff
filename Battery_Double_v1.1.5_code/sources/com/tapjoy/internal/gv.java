package com.tapjoy.internal;

public final class gv {
    public static String a(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        String trim = str.trim();
        if (trim == null || trim.length() == 0) {
            return null;
        }
        return trim;
    }

    public static String b(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return null;
        }
        return trim;
    }

    public static String a(String str, String str2, String str3) {
        if (str == null) {
            gx.a(str2, str3, "must not be null");
            return null;
        } else if (str.length() == 0) {
            gx.a(str2, str3, "must not be empty");
            return null;
        } else {
            String trim = str.trim();
            if (trim.length() != 0) {
                return trim;
            }
            gx.a(str2, str3, "must not be blank");
            return null;
        }
    }
}
