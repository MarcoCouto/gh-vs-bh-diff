package com.tapjoy.internal;

import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class hs extends hr {
    public static final bi d = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            return new hs(bnVar);
        }
    };

    /* renamed from: a reason: collision with root package name */
    public ArrayList f4476a = new ArrayList();
    public Map b;
    public float c;

    public hs(bn bnVar) {
        bnVar.h();
        String str = null;
        String str2 = null;
        while (bnVar.j()) {
            String l = bnVar.l();
            if ("layouts".equals(l)) {
                bnVar.a((List) this.f4476a, ib.d);
            } else if ("meta".equals(l)) {
                this.b = bnVar.d();
            } else if ("max_show_time".equals(l)) {
                this.c = (float) bnVar.p();
            } else if ("ad_content".equals(l)) {
                str = bnVar.b();
            } else if (TapjoyConstants.TJC_REDIRECT_URL.equals(l)) {
                str2 = bnVar.b();
            } else {
                bnVar.s();
            }
        }
        bnVar.i();
        if (this.f4476a != null) {
            Iterator it = this.f4476a.iterator();
            while (it.hasNext()) {
                ib ibVar = (ib) it.next();
                if (ibVar.c != null) {
                    Iterator it2 = ibVar.c.iterator();
                    while (it2.hasNext()) {
                        ia iaVar = (ia) it2.next();
                        if (iaVar.i == null) {
                            iaVar.i = str;
                        }
                        if (iaVar.h == null) {
                            iaVar.h = str2;
                        }
                    }
                }
            }
        }
    }
}
