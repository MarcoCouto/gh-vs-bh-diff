package com.tapjoy.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;

public abstract class ek {
    public static final ek c = new ek(eh.VARINT, Boolean.class) {
        public final /* bridge */ /* synthetic */ int a(Object obj) {
            return 1;
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.c(((Boolean) obj).booleanValue() ? 1 : 0);
        }

        public final /* synthetic */ Object a(el elVar) {
            int d = elVar.d();
            if (d == 0) {
                return Boolean.FALSE;
            }
            if (d == 1) {
                return Boolean.TRUE;
            }
            throw new IOException(String.format("Invalid boolean value 0x%02x", new Object[]{Integer.valueOf(d)}));
        }
    };
    public static final ek d = new ek(eh.VARINT, Integer.class) {
        public final /* synthetic */ int a(Object obj) {
            int intValue = ((Integer) obj).intValue();
            if (intValue >= 0) {
                return em.a(intValue);
            }
            return 10;
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            int intValue = ((Integer) obj).intValue();
            if (intValue >= 0) {
                emVar.c(intValue);
            } else {
                emVar.c((long) intValue);
            }
        }

        public final /* synthetic */ Object a(el elVar) {
            return Integer.valueOf(elVar.d());
        }
    };
    public static final ek e = new ek(eh.VARINT, Integer.class) {
        public final /* synthetic */ int a(Object obj) {
            return em.a(((Integer) obj).intValue());
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.c(((Integer) obj).intValue());
        }

        public final /* synthetic */ Object a(el elVar) {
            return Integer.valueOf(elVar.d());
        }
    };
    public static final ek f = new ek(eh.VARINT, Integer.class) {
        public final /* synthetic */ int a(Object obj) {
            return em.a(em.b(((Integer) obj).intValue()));
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.c(em.b(((Integer) obj).intValue()));
        }

        public final /* synthetic */ Object a(el elVar) {
            int d = elVar.d();
            return Integer.valueOf((-(d & 1)) ^ (d >>> 1));
        }
    };
    public static final ek g;
    public static final ek h;
    public static final ek i = new ek(eh.VARINT, Long.class) {
        public final /* synthetic */ int a(Object obj) {
            return em.a(((Long) obj).longValue());
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.c(((Long) obj).longValue());
        }

        public final /* synthetic */ Object a(el elVar) {
            return Long.valueOf(elVar.e());
        }
    };
    public static final ek j = new ek(eh.VARINT, Long.class) {
        public final /* synthetic */ int a(Object obj) {
            return em.a(((Long) obj).longValue());
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.c(((Long) obj).longValue());
        }

        public final /* synthetic */ Object a(el elVar) {
            return Long.valueOf(elVar.e());
        }
    };
    public static final ek k = new ek(eh.VARINT, Long.class) {
        public final /* synthetic */ int a(Object obj) {
            return em.a(em.b(((Long) obj).longValue()));
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.c(em.b(((Long) obj).longValue()));
        }

        public final /* synthetic */ Object a(el elVar) {
            long e = elVar.e();
            return Long.valueOf((-(e & 1)) ^ (e >>> 1));
        }
    };
    public static final ek l;
    public static final ek m;
    public static final ek n = new ek(eh.FIXED32, Float.class) {
        public final /* bridge */ /* synthetic */ int a(Object obj) {
            return 4;
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.d(Float.floatToIntBits(((Float) obj).floatValue()));
        }

        public final /* synthetic */ Object a(el elVar) {
            return Float.valueOf(Float.intBitsToFloat(elVar.f()));
        }
    };
    public static final ek o = new ek(eh.FIXED64, Double.class) {
        public final /* bridge */ /* synthetic */ int a(Object obj) {
            return 8;
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.d(Double.doubleToLongBits(((Double) obj).doubleValue()));
        }

        public final /* synthetic */ Object a(el elVar) {
            return Double.valueOf(Double.longBitsToDouble(elVar.g()));
        }
    };
    public static final ek p = new ek(eh.LENGTH_DELIMITED, String.class) {
        public final /* synthetic */ int a(Object obj) {
            String str = (String) obj;
            int length = str.length();
            int i = 0;
            int i2 = 0;
            while (i < length) {
                char charAt = str.charAt(i);
                if (charAt >= 128) {
                    if (charAt < 2048) {
                        i2 += 2;
                    } else if (charAt < 55296 || charAt > 57343) {
                        i2 += 3;
                    } else if (charAt <= 56319) {
                        int i3 = i + 1;
                        if (i3 < length && str.charAt(i3) >= 56320 && str.charAt(i3) <= 57343) {
                            i2 += 4;
                            i = i3;
                        }
                    }
                    i++;
                }
                i2++;
                i++;
            }
            return i2;
        }

        public final /* synthetic */ void a(em emVar, Object obj) {
            emVar.f4387a.b((String) obj);
        }

        public final /* synthetic */ Object a(el elVar) {
            return elVar.f4386a.c(elVar.h());
        }
    };
    public static final ek q = new ek(eh.LENGTH_DELIMITED, iv.class) {
        public final /* synthetic */ int a(Object obj) {
            return ((iv) obj).c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            emVar.a((iv) obj);
        }

        public final /* synthetic */ Object a(el elVar) {
            return elVar.f4386a.b(elVar.h());
        }
    };

    /* renamed from: a reason: collision with root package name */
    final Class f4384a;
    ek b;
    private final eh r;

    public static final class a extends IllegalArgumentException {

        /* renamed from: a reason: collision with root package name */
        public final int f4385a;

        a(int i, Class cls) {
            StringBuilder sb = new StringBuilder("Unknown enum tag ");
            sb.append(i);
            sb.append(" for ");
            sb.append(cls.getCanonicalName());
            super(sb.toString());
            this.f4385a = i;
        }
    }

    public abstract int a(Object obj);

    public abstract Object a(el elVar);

    public abstract void a(em emVar, Object obj);

    public ek(eh ehVar, Class cls) {
        this.r = ehVar;
        this.f4384a = cls;
    }

    public int a(int i2, Object obj) {
        int a2 = a(obj);
        if (this.r == eh.LENGTH_DELIMITED) {
            a2 += em.a(a2);
        }
        return a2 + em.a(em.a(i2, eh.VARINT));
    }

    public void a(em emVar, int i2, Object obj) {
        emVar.c(em.a(i2, this.r));
        if (this.r == eh.LENGTH_DELIMITED) {
            emVar.c(a(obj));
        }
        a(emVar, obj);
    }

    private void a(it itVar, Object obj) {
        ej.a(obj, "value == null");
        ej.a(itVar, "sink == null");
        a(new em(itVar), obj);
    }

    public final byte[] b(Object obj) {
        ej.a(obj, "value == null");
        is isVar = new is();
        try {
            a((it) isVar, obj);
            return isVar.g();
        } catch (IOException e2) {
            throw new AssertionError(e2);
        }
    }

    public final void a(OutputStream outputStream, Object obj) {
        ej.a(obj, "value == null");
        ej.a(outputStream, "stream == null");
        it a2 = iw.a(iw.a(outputStream));
        a(a2, obj);
        a2.a();
    }

    public final Object a(byte[] bArr) {
        ej.a(bArr, "bytes == null");
        is isVar = new is();
        if (bArr != null) {
            return a((iu) isVar.a(bArr, 0, bArr.length));
        }
        throw new IllegalArgumentException("source == null");
    }

    public final Object a(InputStream inputStream) {
        ej.a(inputStream, "stream == null");
        return a(iw.a(iw.a(inputStream)));
    }

    private Object a(iu iuVar) {
        ej.a(iuVar, "source == null");
        return a(new el(iuVar));
    }

    public static String c(Object obj) {
        return obj.toString();
    }

    static {
        AnonymousClass10 r0 = new ek(eh.FIXED32, Integer.class) {
            public final /* bridge */ /* synthetic */ int a(Object obj) {
                return 4;
            }

            public final /* synthetic */ void a(em emVar, Object obj) {
                emVar.d(((Integer) obj).intValue());
            }

            public final /* synthetic */ Object a(el elVar) {
                return Integer.valueOf(elVar.f());
            }
        };
        g = r0;
        h = r0;
        AnonymousClass14 r02 = new ek(eh.FIXED64, Long.class) {
            public final /* bridge */ /* synthetic */ int a(Object obj) {
                return 8;
            }

            public final /* synthetic */ void a(em emVar, Object obj) {
                emVar.d(((Long) obj).longValue());
            }

            public final /* synthetic */ Object a(el elVar) {
                return Long.valueOf(elVar.g());
            }
        };
        l = r02;
        m = r02;
    }

    public final ek a() {
        ek ekVar = this.b;
        if (ekVar != null) {
            return ekVar;
        }
        AnonymousClass6 r0 = new ek(this.r, List.class) {
            public final /* synthetic */ int a(int i, Object obj) {
                List list = (List) obj;
                int i2 = 0;
                for (int i3 = 0; i3 < list.size(); i3++) {
                    i2 += ek.this.a(i, list.get(i3));
                }
                return i2;
            }

            public final /* synthetic */ void a(em emVar, int i, Object obj) {
                List list = (List) obj;
                int size = list.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ek.this.a(emVar, i, list.get(i2));
                }
            }

            public final /* synthetic */ Object a(el elVar) {
                return Collections.singletonList(ek.this.a(elVar));
            }

            public final /* synthetic */ void a(em emVar, Object obj) {
                throw new UnsupportedOperationException("Repeated values can only be encoded with a tag.");
            }

            public final /* synthetic */ int a(Object obj) {
                throw new UnsupportedOperationException("Repeated values can only be sized with a tag.");
            }
        };
        this.b = r0;
        return r0;
    }
}
