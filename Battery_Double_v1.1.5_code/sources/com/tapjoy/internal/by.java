package com.tapjoy.internal;

import java.lang.ref.WeakReference;
import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public final class by {

    /* renamed from: a reason: collision with root package name */
    public WeakReference f4335a;

    public final Object a() {
        WeakReference weakReference = this.f4335a;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    public final void a(Object obj) {
        this.f4335a = new WeakReference(obj);
    }
}
