package com.tapjoy.internal;

import android.app.RemoteInput;
import android.app.RemoteInput.Builder;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import java.util.Set;

public final class jm {

    /* renamed from: a reason: collision with root package name */
    final String f4522a;
    final CharSequence b;
    final CharSequence[] c;
    final boolean d;
    final Bundle e;
    final Set f;

    @RequiresApi(20)
    static RemoteInput[] a(jm[] jmVarArr) {
        if (jmVarArr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[jmVarArr.length];
        for (int i = 0; i < jmVarArr.length; i++) {
            jm jmVar = jmVarArr[i];
            remoteInputArr[i] = new Builder(jmVar.f4522a).setLabel(jmVar.b).setChoices(jmVar.c).setAllowFreeFormInput(jmVar.d).addExtras(jmVar.e).build();
        }
        return remoteInputArr;
    }
}
