package com.tapjoy.internal;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public final class gs {

    /* renamed from: a reason: collision with root package name */
    public static final ba f4437a = new ba() {
        public final boolean a(Runnable runnable) {
            GLSurfaceView gLSurfaceView = (GLSurfaceView) gs.c.a();
            if (gLSurfaceView == null) {
                return false;
            }
            gLSurfaceView.queueEvent(runnable);
            return true;
        }
    };
    private static Activity b;
    /* access modifiers changed from: private */
    public static final by c = new by();
    /* access modifiers changed from: private */
    public static final by d = new by();

    static void a(GLSurfaceView gLSurfaceView) {
        new Object[1][0] = gLSurfaceView;
        c.a(gLSurfaceView);
        gLSurfaceView.queueEvent(new Runnable() {
            public final void run() {
                Thread currentThread = Thread.currentThread();
                new Object[1][0] = currentThread;
                gs.d.a(currentThread);
            }
        });
    }

    public static Activity a() {
        Activity activity = b;
        return activity == null ? b.a() : activity;
    }

    public static Thread b() {
        return (Thread) d.a();
    }
}
