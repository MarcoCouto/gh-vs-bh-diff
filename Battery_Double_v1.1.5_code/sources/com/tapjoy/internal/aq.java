package com.tapjoy.internal;

import javax.annotation.Nullable;

public abstract class aq implements an {
    /* access modifiers changed from: protected */
    @Nullable
    public abstract ao a(Object obj, boolean z);

    public final Object a(Object obj) {
        ao a2 = a(obj, false);
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    public void a(Object obj, Object obj2) {
        a(obj, true).a(obj2);
    }
}
