package com.tapjoy.internal;

import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public final class dx {

    /* renamed from: a reason: collision with root package name */
    final HashMap f4374a = new HashMap();
    final HashMap b = new HashMap();
    final HashSet c = new HashSet();
    final HashSet d = new HashSet();
    final HashSet e = new HashSet();
    boolean f;

    /* access modifiers changed from: 0000 */
    public final void a(cz czVar) {
        for (dr drVar : czVar.b) {
            View view = (View) drVar.get();
            if (view != null) {
                ArrayList arrayList = (ArrayList) this.b.get(view);
                if (arrayList == null) {
                    arrayList = new ArrayList();
                    this.b.put(view, arrayList);
                }
                arrayList.add(czVar.f);
            }
        }
    }
}
