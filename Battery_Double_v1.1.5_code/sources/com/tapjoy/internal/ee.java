package com.tapjoy.internal;

import android.support.annotation.VisibleForTesting;
import com.tapjoy.internal.dz.b;
import org.json.JSONObject;

public final class ee implements b {

    /* renamed from: a reason: collision with root package name */
    final ea f4378a;
    private JSONObject b;

    public ee(ea eaVar) {
        this.f4378a = eaVar;
    }

    @VisibleForTesting
    public final JSONObject a() {
        return this.b;
    }

    @VisibleForTesting
    public final void a(JSONObject jSONObject) {
        this.b = jSONObject;
    }

    public final void b() {
        this.f4378a.a(new eb(this));
    }
}
