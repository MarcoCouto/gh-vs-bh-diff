package com.tapjoy.internal;

import android.content.SharedPreferences.Editor;
import android.os.SystemClock;
import com.tapjoy.internal.ew.a;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Nullable;

public final class hn {

    /* renamed from: a reason: collision with root package name */
    final gz f4470a;
    final AtomicBoolean b = new AtomicBoolean();
    @Nullable
    ScheduledFuture c;
    private final Runnable d = new Runnable() {
        public final void run() {
            if (hn.this.b.compareAndSet(true, false)) {
                gx.a("The session ended");
                gz gzVar = hn.this.f4470a;
                long elapsedRealtime = SystemClock.elapsedRealtime() - gzVar.c;
                hd hdVar = gzVar.f4446a;
                synchronized (hdVar) {
                    long a2 = hdVar.c.i.a() + elapsedRealtime;
                    hdVar.c.i.a(a2);
                    hdVar.b.i = Long.valueOf(a2);
                }
                a a3 = gzVar.a(ez.APP, SettingsJsonConstants.SESSION_KEY);
                a3.i = Long.valueOf(elapsedRealtime);
                gzVar.a(a3);
                gzVar.c = 0;
                hd hdVar2 = gzVar.f4446a;
                long longValue = a3.e.longValue();
                synchronized (hdVar2) {
                    Editor a4 = hdVar2.c.a();
                    hdVar2.c.j.a(a4, longValue);
                    hdVar2.c.k.a(a4, elapsedRealtime);
                    a4.apply();
                    hdVar2.b.j = Long.valueOf(longValue);
                    hdVar2.b.k = Long.valueOf(elapsedRealtime);
                }
                gy gyVar = gzVar.b;
                if (gyVar.b != null) {
                    gyVar.a();
                    new in() {
                        /* access modifiers changed from: protected */
                        public final boolean a() {
                            return !gy.this.f4444a.c();
                        }
                    }.run();
                }
                gyVar.f4444a.flush();
                ft.d.notifyObservers();
            }
        }
    };
    private final Runnable e = new Runnable() {
        public final void run() {
        }
    };

    hn(gz gzVar) {
        this.f4470a = gzVar;
    }

    public final void a() {
        if (this.b.get()) {
            if (Boolean.FALSE.booleanValue()) {
                if (this.c == null || this.c.cancel(false)) {
                    this.c = ho.f4473a.schedule(this.d, 3000, TimeUnit.MILLISECONDS);
                }
                return;
            }
            this.d.run();
        }
    }
}
