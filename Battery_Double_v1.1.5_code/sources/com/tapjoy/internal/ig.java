package com.tapjoy.internal;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public final class ig {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f4493a = new byte[256];
    private ByteBuffer b;
    private Cif c;
    private int d = 0;

    public final ig a(byte[] bArr) {
        if (bArr != null) {
            ByteBuffer wrap = ByteBuffer.wrap(bArr);
            this.b = null;
            Arrays.fill(this.f4493a, 0);
            this.c = new Cif();
            this.d = 0;
            this.b = wrap.asReadOnlyBuffer();
            this.b.position(0);
            this.b.order(ByteOrder.LITTLE_ENDIAN);
        } else {
            this.b = null;
            this.c.b = 2;
        }
        return this;
    }

    public final Cif a() {
        if (this.b == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (h()) {
            return this.c;
        } else {
            d();
            if (!h()) {
                b();
                if (this.c.c < 0) {
                    this.c.b = 1;
                }
            }
            return this.c;
        }
    }

    private void b() {
        boolean z = false;
        while (!z && !h() && this.c.c <= Integer.MAX_VALUE) {
            int g = g();
            if (g == 33) {
                int g2 = g();
                if (g2 == 1) {
                    e();
                } else if (g2 != 249) {
                    switch (g2) {
                        case 254:
                            e();
                            break;
                        case 255:
                            f();
                            String str = "";
                            for (int i = 0; i < 11; i++) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(str);
                                sb.append((char) this.f4493a[i]);
                                str = sb.toString();
                            }
                            if (!str.equals("NETSCAPE2.0")) {
                                e();
                                break;
                            } else {
                                c();
                                break;
                            }
                        default:
                            e();
                            break;
                    }
                } else {
                    this.c.d = new ie();
                    g();
                    int g3 = g();
                    this.c.d.g = (g3 & 28) >> 2;
                    if (this.c.d.g == 0) {
                        this.c.d.g = 1;
                    }
                    this.c.d.f = (g3 & 1) != 0;
                    short s = this.b.getShort();
                    if (s < 2) {
                        s = 10;
                    }
                    this.c.d.i = s * 10;
                    this.c.d.h = g();
                    g();
                }
            } else if (g == 44) {
                if (this.c.d == null) {
                    this.c.d = new ie();
                }
                this.c.d.f4491a = this.b.getShort();
                this.c.d.b = this.b.getShort();
                this.c.d.c = this.b.getShort();
                this.c.d.d = this.b.getShort();
                int g4 = g();
                boolean z2 = (g4 & 128) != 0;
                int pow = (int) Math.pow(2.0d, (double) ((g4 & 7) + 1));
                this.c.d.e = (g4 & 64) != 0;
                if (z2) {
                    this.c.d.k = a(pow);
                } else {
                    this.c.d.k = null;
                }
                this.c.d.j = this.b.position();
                g();
                e();
                if (!h()) {
                    this.c.c++;
                    this.c.e.add(this.c.d);
                }
            } else if (g != 59) {
                this.c.b = 1;
            } else {
                z = true;
            }
        }
    }

    private void c() {
        do {
            f();
            if (this.f4493a[0] == 1) {
                this.c.m = (this.f4493a[1] & 255) | ((this.f4493a[2] & 255) << 8);
                if (this.c.m == 0) {
                    this.c.m = -1;
                }
            }
            if (this.d <= 0) {
                return;
            }
        } while (!h());
    }

    private void d() {
        boolean z = false;
        String str = "";
        for (int i = 0; i < 6; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append((char) g());
            str = sb.toString();
        }
        if (!str.startsWith("GIF")) {
            this.c.b = 1;
            return;
        }
        this.c.f = this.b.getShort();
        this.c.g = this.b.getShort();
        int g = g();
        Cif ifVar = this.c;
        if ((g & 128) != 0) {
            z = true;
        }
        ifVar.h = z;
        this.c.i = 2 << (g & 7);
        this.c.j = g();
        this.c.k = g();
        if (this.c.h && !h()) {
            this.c.f4492a = a(this.c.i);
            this.c.l = this.c.f4492a[this.c.j];
        }
    }

    private int[] a(int i) {
        int[] iArr;
        byte[] bArr = new byte[(i * 3)];
        try {
            this.b.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            int i3 = 0;
            while (i2 < i) {
                int i4 = i3 + 1;
                try {
                    int i5 = i4 + 1;
                    int i6 = i5 + 1;
                    int i7 = i2 + 1;
                    iArr[i2] = ((bArr[i3] & 255) << 16) | -16777216 | ((bArr[i4] & 255) << 8) | (bArr[i5] & 255);
                    i3 = i6;
                    i2 = i7;
                } catch (BufferUnderflowException e) {
                    e = e;
                    new Object[1][0] = e;
                    this.c.b = 1;
                    return iArr;
                }
            }
        } catch (BufferUnderflowException e2) {
            e = e2;
            iArr = null;
            new Object[1][0] = e;
            this.c.b = 1;
            return iArr;
        }
        return iArr;
    }

    private void e() {
        int g;
        do {
            try {
                g = g();
                this.b.position(this.b.position() + g);
            } catch (IllegalArgumentException unused) {
                return;
            }
        } while (g > 0);
    }

    private int f() {
        this.d = g();
        if (this.d <= 0) {
            return 0;
        }
        int i = 0;
        int i2 = 0;
        while (i < this.d) {
            try {
                i2 = this.d - i;
                this.b.get(this.f4493a, i, i2);
                i += i2;
            } catch (Exception e) {
                Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(this.d), e};
                this.c.b = 1;
                return i;
            }
        }
        return i;
    }

    private int g() {
        try {
            return this.b.get() & 255;
        } catch (Exception unused) {
            this.c.b = 1;
            return 0;
        }
    }

    private boolean h() {
        return this.c.b != 0;
    }
}
