package com.tapjoy.internal;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import java.util.Collections;

public final class de {
    @SuppressLint({"StaticFieldLeak"})
    private static de f = new de();

    /* renamed from: a reason: collision with root package name */
    public Context f4358a;
    public BroadcastReceiver b;
    public boolean c;
    public boolean d;
    public a e;

    public interface a {
        void a(boolean z);
    }

    private de() {
    }

    public static de a() {
        return f;
    }

    public final boolean b() {
        return !this.d;
    }

    public final void c() {
        boolean z = !this.d;
        for (cz czVar : Collections.unmodifiableCollection(dd.a().f4357a)) {
            dt dtVar = czVar.c;
            if (dtVar.f4369a.get() != null) {
                dg.a().a(dtVar.c(), "setState", z ? "foregrounded" : "backgrounded");
            }
        }
    }

    static /* synthetic */ void a(de deVar, boolean z) {
        if (deVar.d != z) {
            deVar.d = z;
            if (deVar.c) {
                deVar.c();
                if (deVar.e != null) {
                    deVar.e.a(deVar.b());
                }
            }
        }
    }
}
