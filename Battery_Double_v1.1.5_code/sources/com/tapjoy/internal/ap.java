package com.tapjoy.internal;

public final class ap {

    public static final class a implements as {

        /* renamed from: a reason: collision with root package name */
        private final aq f4313a;

        public a(aq aqVar) {
            this.f4313a = aqVar;
        }

        public final Object a(Object obj) {
            ao a2;
            Object a3;
            synchronized (this.f4313a) {
                a2 = this.f4313a.a(obj, false);
            }
            if (a2 == null) {
                return null;
            }
            synchronized (a2) {
                a3 = a2.a();
            }
            return a3;
        }

        public final void a(Object obj, Object obj2) {
            ao a2;
            synchronized (this.f4313a) {
                a2 = this.f4313a.a(obj, true);
            }
            synchronized (a2) {
                a2.a(obj2);
            }
        }
    }
}
