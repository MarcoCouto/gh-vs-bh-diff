package com.tapjoy.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import java.nio.ByteBuffer;

public final class ih extends ImageView implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public id f4494a;
    /* access modifiers changed from: private */
    public Bitmap b;
    private final Handler c = new Handler(Looper.getMainLooper());
    private boolean d;
    private boolean e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public Thread g;
    private b h = null;
    private long i = -1;
    private a j = null;
    private final Runnable k = new Runnable() {
        public final void run() {
            if (ih.this.b != null && !ih.this.b.isRecycled()) {
                ih.this.setImageBitmap(ih.this.b);
            }
        }
    };
    private final Runnable l = new Runnable() {
        public final void run() {
            ih.this.b = null;
            ih.this.f4494a = null;
            ih.this.g = null;
            ih.this.f = false;
        }
    };

    public interface a {
    }

    public interface b {
        Bitmap a();
    }

    public ih(Context context) {
        super(context);
    }

    public final void a(Cif ifVar, byte[] bArr) {
        try {
            this.f4494a = new id(new ii(), ifVar, ByteBuffer.wrap(bArr));
            if (this.d) {
                e();
            } else {
                d();
            }
        } catch (Exception e2) {
            this.f4494a = null;
            new Object[1][0] = e2;
        }
    }

    public final void setBytes(byte[] bArr) {
        this.f4494a = new id();
        try {
            this.f4494a.a(bArr);
            if (this.d) {
                e();
            } else {
                d();
            }
        } catch (Exception e2) {
            this.f4494a = null;
            new Object[1][0] = e2;
        }
    }

    public final long getFramesDisplayDuration() {
        return this.i;
    }

    public final void setFramesDisplayDuration(long j2) {
        this.i = j2;
    }

    public final void a() {
        this.d = true;
        e();
    }

    public final void b() {
        this.d = false;
        if (this.g != null) {
            this.g.interrupt();
            this.g = null;
        }
    }

    private void d() {
        boolean z;
        if (this.f4494a.f4490a != 0) {
            id idVar = this.f4494a;
            if (-1 >= idVar.c.c) {
                z = false;
            } else {
                idVar.f4490a = -1;
                z = true;
            }
            if (z && !this.d) {
                this.e = true;
                e();
            }
        }
    }

    public final void c() {
        this.d = false;
        this.e = false;
        this.f = true;
        b();
        this.c.post(this.l);
    }

    public final int getGifWidth() {
        return this.f4494a.c.f;
    }

    public final int getGifHeight() {
        return this.f4494a.c.g;
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f A[Catch:{ ArrayIndexOutOfBoundsException -> 0x0075, IllegalArgumentException -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0081 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00c1 A[ADDED_TO_REGION, EDGE_INSN: B:65:0x00c1->B:56:0x00c1 ?: BREAK  , SYNTHETIC] */
    public final void run() {
        boolean z;
        long j2;
        while (true) {
            if (!this.d && !this.e) {
                break;
            }
            id idVar = this.f4494a;
            int i2 = 0;
            if (idVar.c.c > 0) {
                if (idVar.f4490a == idVar.c.c - 1) {
                    idVar.b++;
                }
                if (idVar.c.m == -1 || idVar.b <= idVar.c.m) {
                    idVar.f4490a = (idVar.f4490a + 1) % idVar.c.c;
                    z = true;
                    long nanoTime = System.nanoTime();
                    this.b = this.f4494a.a();
                    if (this.h != null) {
                        this.b = this.h.a();
                    }
                    j2 = (System.nanoTime() - nanoTime) / 1000000;
                    this.c.post(this.k);
                    this.e = false;
                    if (!this.d && z) {
                        try {
                            id idVar2 = this.f4494a;
                            if (idVar2.c.c > 0) {
                                if (idVar2.f4490a >= 0) {
                                    int i3 = idVar2.f4490a;
                                    i2 = (i3 < 0 || i3 >= idVar2.c.c) ? -1 : ((ie) idVar2.c.e.get(i3)).i;
                                }
                            }
                            int i4 = (int) (((long) i2) - j2);
                            if (i4 > 0) {
                                Thread.sleep(this.i > 0 ? this.i : (long) i4);
                            }
                        } catch (InterruptedException unused) {
                        }
                        if (!this.d) {
                            break;
                        }
                    } else {
                        this.d = false;
                    }
                }
            }
            z = false;
            try {
                long nanoTime2 = System.nanoTime();
                this.b = this.f4494a.a();
                if (this.h != null) {
                }
                j2 = (System.nanoTime() - nanoTime2) / 1000000;
                try {
                    this.c.post(this.k);
                } catch (ArrayIndexOutOfBoundsException e2) {
                    e = e2;
                } catch (IllegalArgumentException e3) {
                    e = e3;
                    new Object[1][0] = e;
                    this.e = false;
                    if (!this.d) {
                    }
                    this.d = false;
                    if (this.f) {
                    }
                    this.g = null;
                }
            } catch (ArrayIndexOutOfBoundsException e4) {
                e = e4;
                j2 = 0;
                new Object[1][0] = e;
                this.e = false;
                if (!this.d) {
                }
                this.d = false;
                if (this.f) {
                }
                this.g = null;
            } catch (IllegalArgumentException e5) {
                e = e5;
                j2 = 0;
                new Object[1][0] = e;
                this.e = false;
                if (!this.d) {
                }
                this.d = false;
                if (this.f) {
                }
                this.g = null;
            }
            this.e = false;
            if (!this.d) {
                break;
            }
            break;
        }
        this.d = false;
        if (this.f) {
            this.c.post(this.l);
        }
        this.g = null;
    }

    public final b getOnFrameAvailable() {
        return this.h;
    }

    public final void setOnFrameAvailable(b bVar) {
        this.h = bVar;
    }

    public final a getOnAnimationStop() {
        return this.j;
    }

    public final void setOnAnimationStop(a aVar) {
        this.j = aVar;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        c();
    }

    private void e() {
        if ((this.d || this.e) && this.f4494a != null && this.g == null) {
            this.g = new Thread(this);
            this.g.start();
        }
    }
}
