package com.tapjoy.internal;

import android.os.Handler;
import android.os.Looper;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public final class fo {

    static class a implements InvocationHandler {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final Object f4405a;
        private final Thread b;
        private final Looper c;

        public a(Object obj, Thread thread, Looper looper) {
            this.f4405a = obj;
            this.b = thread;
            this.c = looper;
        }

        public final Object invoke(Object obj, final Method method, final Object[] objArr) {
            if (this.b == Thread.currentThread()) {
                return method.invoke(this.f4405a, objArr);
            }
            if (method.getReturnType().equals(Void.TYPE)) {
                AnonymousClass1 r4 = new Runnable() {
                    public final void run() {
                        try {
                            method.invoke(a.this.f4405a, objArr);
                        } catch (IllegalArgumentException e) {
                            throw js.a(e);
                        } catch (IllegalAccessException e2) {
                            throw js.a(e2);
                        } catch (InvocationTargetException e3) {
                            throw js.a(e3);
                        }
                    }
                };
                if (this.c != null && new Handler(this.c).post(r4)) {
                    return null;
                }
                if (this.b == gs.b() && gs.f4437a.a(r4)) {
                    return null;
                }
                Looper mainLooper = Looper.getMainLooper();
                if (mainLooper == null || !new Handler(mainLooper).post(r4)) {
                    return method.invoke(this.f4405a, objArr);
                }
                return null;
            }
            StringBuilder sb = new StringBuilder("method not return void: ");
            sb.append(method.getName());
            throw new UnsupportedOperationException(sb.toString());
        }
    }

    public static Object a(Object obj, Class cls) {
        return Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new a(obj, Thread.currentThread(), Looper.myLooper()));
    }
}
