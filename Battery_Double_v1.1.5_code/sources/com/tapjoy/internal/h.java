package com.tapjoy.internal;

import android.content.SharedPreferences;

public final class h extends m {
    private final boolean c = false;

    public h(SharedPreferences sharedPreferences, String str) {
        super(sharedPreferences, str);
    }

    public final void a(boolean z) {
        this.f4541a.edit().putBoolean(this.b, z).apply();
    }

    public final Boolean a() {
        return Boolean.valueOf(this.f4541a.getBoolean(this.b, this.c));
    }
}
