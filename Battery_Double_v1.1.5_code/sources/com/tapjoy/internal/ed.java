package com.tapjoy.internal;

import android.text.TextUtils;
import com.tapjoy.internal.dt.a;
import com.tapjoy.internal.dz.b;
import java.util.Collections;
import java.util.HashSet;
import org.json.JSONObject;

public final class ed extends dy {
    public ed(b bVar, HashSet hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void onPostExecute(String str) {
        if (!TextUtils.isEmpty(str)) {
            dd a2 = dd.a();
            if (a2 != null) {
                for (cz czVar : Collections.unmodifiableCollection(a2.f4357a)) {
                    if (this.f4375a.contains(czVar.f)) {
                        dt dtVar = czVar.c;
                        if (this.c > dtVar.e) {
                            dtVar.d = a.b;
                            dg.a().b(dtVar.c(), str);
                        }
                    }
                }
            }
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        if (dm.b(this.b, this.e.a())) {
            return null;
        }
        this.e.a(this.b);
        return this.b.toString();
    }
}
