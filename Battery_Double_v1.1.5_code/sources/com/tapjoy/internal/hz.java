package com.tapjoy.internal;

import com.google.firebase.analytics.FirebaseAnalytics.Param;

final class hz extends hp implements gr {

    /* renamed from: a reason: collision with root package name */
    public static final bi f4486a = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            bnVar.h();
            String str = null;
            String str2 = null;
            String str3 = null;
            int i = 1;
            while (bnVar.j()) {
                String l = bnVar.l();
                if ("id".equals(l)) {
                    str = bnVar.m();
                } else if ("name".equals(l)) {
                    str2 = bnVar.m();
                } else if (Param.QUANTITY.equals(l)) {
                    i = bnVar.r();
                } else if ("token".equals(l)) {
                    str3 = bnVar.m();
                } else {
                    bnVar.s();
                }
            }
            bnVar.i();
            return new hz(str, str2, i, str3);
        }
    };
    private final String b;
    private final String c;
    private final int d;
    private final String e;

    hz(String str, String str2, int i, String str3) {
        this.b = str;
        this.c = str2;
        this.d = i;
        this.e = str3;
    }

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    public final String d() {
        return this.e;
    }
}
