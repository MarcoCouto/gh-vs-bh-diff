package com.tapjoy.internal;

import android.app.Activity;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;

public final class go {
    public static void a(Activity activity) {
        ha a2 = ha.a();
        if (gx.a((Object) activity, "onActivityStart: The given activity was null")) {
            gx.c("onActivityStart");
            b.a(activity.getApplication());
            b.b(activity);
            if (a2.c("onActivityStart") && a2.e()) {
                gu.b(activity);
            }
        }
    }

    public static void b(Activity activity) {
        ha a2 = ha.a();
        if (gx.a((Object) activity, "onActivityStop: The given activity was null")) {
            gx.c("onActivityStop");
            b.c(activity);
            if (a2.c("onActivityStop") && !b.b()) {
                a2.h.a();
            }
        }
    }

    public static void a() {
        ha a2 = ha.a();
        if (a2.c("startSession") && a2.e()) {
            gu.b(null);
        }
    }

    public static void b() {
        ha a2 = ha.a();
        if (a2.c("endSession")) {
            a2.h.a();
        }
    }

    public static void a(String str, String str2, String str3, String str4, long j) {
        ha a2 = ha.a();
        if (a2.d("trackEvent") && gx.a((Object) str2, "trackEvent: name was null")) {
            LinkedHashMap linkedHashMap = null;
            if (j != 0) {
                linkedHashMap = jv.b();
                linkedHashMap.put("value", Long.valueOf(j));
            }
            a2.g.a(str, str2, str3, str4, linkedHashMap);
            gx.a("trackEvent category:{}, name:{}, p1:{}, p2:{}, values:{} called", str, str2, str3, str4, linkedHashMap);
        }
    }

    public static void a(String str, String str2, String str3, String str4, String str5, long j, String str6, long j2, String str7, long j3) {
        String str8 = str2;
        String str9 = str5;
        String str10 = str6;
        String str11 = str7;
        ha a2 = ha.a();
        if (a2.d("trackEvent") && gx.a((Object) str2, "trackEvent: name was null")) {
            LinkedHashMap b = jv.b();
            if (!(str9 == null || j == 0)) {
                b.put(str5, Long.valueOf(j));
            }
            if (!(str10 == null || j2 == 0)) {
                b.put(str10, Long.valueOf(j2));
            }
            if (!(str11 == null || j3 == 0)) {
                b.put(str11, Long.valueOf(j3));
            }
            if (b.isEmpty()) {
                b = null;
            }
            a2.g.a(str, str2, str3, str4, b);
            gx.a("trackEvent category:{}, name:{}, p1:{}, p2:{}, values:{} called", str, str8, str3, str4, b);
        }
    }

    public static void a(String str, String str2, String str3, String str4) {
        ha a2 = ha.a();
        if (a2.c("trackPurchase")) {
            try {
                e eVar = new e(str);
                String b = gv.b(eVar.f4376a);
                String b2 = gv.b(eVar.f);
                if (b == null || b2 == null) {
                    gx.a("trackPurchase", "skuDetails", "insufficient fields");
                } else if (b2.length() != 3) {
                    gx.a("trackPurchase", "skuDetails", "invalid currency code");
                } else {
                    String b3 = gv.b(str2);
                    String b4 = gv.b(str3);
                    if (b3 != null) {
                        if (b4 != null) {
                            try {
                                f fVar = new f(b3);
                                if (jr.c(fVar.f4399a) || jr.c(fVar.b) || jr.c(fVar.c) || fVar.d == 0) {
                                    gx.a("trackPurchase", "purchaseData", "insufficient fields");
                                }
                            } catch (IOException unused) {
                                gx.a("trackPurchase", "purchaseData", "invalid PurchaseData JSON");
                            }
                        } else {
                            gx.a("trackPurchase", "dataSignature", "is null, skipping purchase validation");
                        }
                    } else if (b4 != null) {
                        gx.a("trackPurchase", "purchaseData", "is null. skipping purchase validation");
                    }
                    String upperCase = b2.toUpperCase(Locale.US);
                    String b5 = gv.b(str4);
                    gz gzVar = a2.g;
                    double d = (double) eVar.g;
                    Double.isNaN(d);
                    gzVar.a(b, upperCase, d / 1000000.0d, b3, b4, b5);
                    if (b3 == null || b4 == null) {
                        gx.a("trackPurchase without purchaseData called");
                    } else {
                        gx.a("trackPurchase with purchaseData called");
                    }
                }
            } catch (IOException unused2) {
                gx.a("trackPurchase", "skuDetails", "invalid SkuDetails JSON");
            }
        }
    }
}
