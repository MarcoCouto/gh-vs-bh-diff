package com.tapjoy.internal;

import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TJAdUnitJSBridge;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyCache;
import com.tapjoy.TapjoyCachedAssetData;
import com.tapjoy.TapjoyLog;
import com.tapjoy.TapjoyUtil;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class er {
    public static String b = "";

    /* renamed from: a reason: collision with root package name */
    public TJAdUnitJSBridge f4390a;
    public boolean c = false;
    /* access modifiers changed from: private */
    public cr d;
    /* access modifiers changed from: private */
    public cs e;
    /* access modifiers changed from: private */
    public cx f = cx.a("Tapjoy", Tapjoy.getVersion());
    /* access modifiers changed from: private */
    public dc g;

    public er(TJAdUnitJSBridge tJAdUnitJSBridge) {
        this.f4390a = tJAdUnitJSBridge;
    }

    public final boolean a(JSONObject jSONObject) {
        if (this.f4390a.b == null) {
            TapjoyLog.d("TJOMViewabilityAgent", "Can not init -- WebView is null");
            return false;
        } else if (this.f4390a.f4222a == null) {
            TapjoyLog.d("TJOMViewabilityAgent", "Can not init -- TJAdUnit is null");
            return false;
        } else if (this.f4390a.f4222a.getVideoView() == null) {
            TapjoyLog.d("TJOMViewabilityAgent", "Can not init -- VideoView is null");
            return false;
        } else if (jSONObject == null) {
            TapjoyLog.d("TJOMViewabilityAgent", "Can not init -- json parameter is null");
            return false;
        } else if (!jSONObject.has(String.OM_JAVASCRIPT_URL)) {
            TapjoyLog.d("TJOMViewabilityAgent", "Can not init -- unable to parse om javascript url from json");
            return false;
        } else {
            try {
                jSONObject.getJSONArray(String.VENDORS);
                return true;
            } catch (JSONException unused) {
                TapjoyLog.d("TJOMViewabilityAgent", "Can not init -- unable to parse vendors from json");
                return false;
            }
        }
    }

    /* access modifiers changed from: private */
    public static List b(JSONArray jSONArray) {
        cy cyVar;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                String optString = jSONObject.optString(String.VENDOR_JS_URL, null);
                if (optString == null) {
                    TapjoyLog.d("TJOMViewabilityAgent", "Vendor JS URL not found. Skipping.");
                } else {
                    try {
                        URL url = new URL(optString);
                        String optString2 = jSONObject.optString(String.VENDOR_NAME, null);
                        String optString3 = jSONObject.optString(String.VENDOR_PARAMETERS, null);
                        if (optString3 == null || optString2 == null) {
                            cyVar = cy.a(url);
                        } else {
                            cyVar = cy.a(optString2, url, optString3);
                        }
                        arrayList.add(cyVar);
                    } catch (Exception unused) {
                        StringBuilder sb = new StringBuilder("Malformed vendor JS URL. Skipping ");
                        sb.append(optString);
                        TapjoyLog.d("TJOMViewabilityAgent", sb.toString());
                    }
                }
            } catch (JSONException unused2) {
                TapjoyLog.d("TJOMViewabilityAgent", "Malformed vendor object. Skipping.");
            }
        }
        return arrayList;
    }

    public static void b(JSONObject jSONObject) {
        String str;
        if (al.a(b)) {
            String optString = jSONObject.optString(String.OM_JAVASCRIPT_URL, null);
            if (optString == null) {
                TapjoyLog.d("TJOMViewabilityAgent", "Open Mediation JavaScript name not found in json.");
                return;
            }
            try {
                TapjoyCachedAssetData cachedDataForURL = TapjoyCache.getInstance().getCachedDataForURL(optString);
                if (cachedDataForURL == null) {
                    TapjoyCache.getInstance().cacheAssetFromURL(optString, "", 30).get();
                    cachedDataForURL = TapjoyCache.getInstance().getCachedDataForURL(optString);
                }
                if (cachedDataForURL == null) {
                    str = "";
                } else {
                    str = TapjoyUtil.getFileContents(new File(cachedDataForURL.getLocalFilePath()));
                }
                b = str;
            } catch (Exception unused) {
                TapjoyLog.d("TJOMViewabilityAgent", "Failed downloading Open Mediation JavaScript");
            }
        }
    }
}
