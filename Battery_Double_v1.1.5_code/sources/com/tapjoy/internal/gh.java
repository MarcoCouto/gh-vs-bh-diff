package com.tapjoy.internal;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.applovin.sdk.AppLovinMediationProvider;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.annotation.Nullable;

class gh extends gg {
    private final File b;
    private final gz c;
    private volatile SQLiteDatabase d;
    private long e;
    private long f;
    private long g;

    public gh(File file, gz gzVar) {
        this.b = file;
        this.c = gzVar;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.d != null) {
            ka.a(this.d);
            this.d = null;
        }
        super.finalize();
    }

    /* access modifiers changed from: protected */
    /* JADX INFO: finally extract failed */
    public void a(long j) {
        if (this.d == null) {
            this.d = SQLiteDatabase.openOrCreateDatabase(this.b, null);
            int version = this.d.getVersion();
            switch (version) {
                case 0:
                    this.d.beginTransaction();
                    try {
                        this.d.execSQL("CREATE TABLE IF NOT EXISTS UsageStats(name TEXT,dimensions TEXT,count INTEGER,first_time INTEGER,last_time INTEGER,PRIMARY KEY(name, dimensions))");
                        this.d.execSQL("CREATE TABLE IF NOT EXISTS UsageStatValues(stat_id LONG,name TEXT,count INTEGER,avg REAL,max INTEGER,PRIMARY KEY(stat_id, name))");
                        this.d.setVersion(1);
                        this.d.setTransactionSuccessful();
                        break;
                    } finally {
                        this.d.endTransaction();
                    }
                case 1:
                    break;
                default:
                    StringBuilder sb = new StringBuilder("Unknown database version: ");
                    sb.append(version);
                    throw new SQLException(sb.toString());
            }
            Cursor rawQuery = this.d.rawQuery("SELECT MIN(first_time), MAX(last_time) FROM UsageStats", null);
            try {
                if (rawQuery.moveToNext()) {
                    this.f = rawQuery.getLong(0);
                    this.g = rawQuery.getLong(1);
                }
                rawQuery.close();
                if (this.f > 0 && this.f + 86400000 <= j) {
                    b();
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.d != null) {
            ka.a(this.d);
            this.d = null;
        }
        this.b.delete();
        this.g = 0;
        this.f = 0;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01d1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01d5, code lost:
        throw r0;
     */
    public void a(long j, String str, @Nullable String str2, @Nullable Map map) {
        long j2;
        long j3;
        Object obj;
        long j4 = j;
        String str3 = str;
        if (this.d != null) {
            if (this.e == 0) {
                this.g = j4;
                this.e = j4;
            } else if (j4 < this.e || j4 >= this.e + 86400000) {
                if (j4 >= this.e || this.g - j4 >= 86400000) {
                    b();
                    this.g = j4;
                    this.e = j4;
                } else {
                    this.e = j4;
                }
            } else if (j4 > this.g) {
                this.g = j4;
            }
            String str4 = str2 == null ? "" : str2;
            int i = 2;
            int i2 = 0;
            int i3 = 1;
            Cursor rawQuery = this.d.rawQuery("SELECT ROWID,count,first_time,last_time FROM UsageStats WHERE name = ? AND dimensions = ?", new String[]{str3, str4});
            try {
                ContentValues contentValues = new ContentValues();
                if (rawQuery.moveToNext()) {
                    j2 = rawQuery.getLong(0);
                    int i4 = rawQuery.getInt(1);
                    long j5 = rawQuery.getLong(2);
                    long j6 = rawQuery.getLong(3);
                    contentValues.put("count", Integer.valueOf(i4 + 1));
                    if (j4 < j5) {
                        contentValues.put("first_time", Long.valueOf(j));
                    }
                    if (j4 > j6) {
                        contentValues.put("last_time", Long.valueOf(j));
                    }
                    StringBuilder sb = new StringBuilder("ROWID = ");
                    sb.append(j2);
                    this.d.update("UsageStats", contentValues, sb.toString(), null);
                } else {
                    contentValues.put("name", str3);
                    contentValues.put(String.USAGE_TRACKER_DIMENSIONS, str4);
                    contentValues.put("count", Integer.valueOf(1));
                    contentValues.put("first_time", Long.valueOf(j));
                    contentValues.put("last_time", Long.valueOf(j));
                    j2 = this.d.insert("UsageStats", null, contentValues);
                }
                if (map != null && !map.isEmpty()) {
                    for (Entry entry : map.entrySet()) {
                        if (entry.getValue() != null) {
                            String str5 = (String) entry.getKey();
                            long longValue = ((Long) entry.getValue()).longValue();
                            String[] strArr = new String[i];
                            strArr[i2] = Long.toString(j2);
                            strArr[i3] = str5;
                            rawQuery = this.d.rawQuery("SELECT ROWID, * FROM UsageStatValues WHERE stat_id = ? AND name = ?", strArr);
                            if (rawQuery.moveToNext()) {
                                long j7 = rawQuery.getLong(i2);
                                int i5 = rawQuery.getInt(3);
                                double d2 = rawQuery.getDouble(4);
                                long j8 = rawQuery.getLong(5);
                                contentValues.clear();
                                int i6 = i5 + i3;
                                contentValues.put("count", Integer.valueOf(i6));
                                String str6 = "avg";
                                double d3 = (double) longValue;
                                Double.isNaN(d3);
                                double d4 = d3 - d2;
                                j3 = j2;
                                double d5 = (double) i6;
                                Double.isNaN(d5);
                                contentValues.put(str6, Double.valueOf(d2 + (d4 / d5)));
                                if (longValue > j8) {
                                    contentValues.put(AppLovinMediationProvider.MAX, Long.valueOf(longValue));
                                }
                                StringBuilder sb2 = new StringBuilder("ROWID = ");
                                sb2.append(j7);
                                this.d.update("UsageStatValues", contentValues, sb2.toString(), null);
                                obj = null;
                            } else {
                                j3 = j2;
                                contentValues.clear();
                                contentValues.put("stat_id", Long.valueOf(j3));
                                contentValues.put("name", str5);
                                contentValues.put("count", Integer.valueOf(1));
                                contentValues.put("avg", Long.valueOf(longValue));
                                contentValues.put(AppLovinMediationProvider.MAX, Long.valueOf(longValue));
                                obj = null;
                                this.d.insert("UsageStatValues", null, contentValues);
                            }
                            rawQuery.close();
                            Object obj2 = obj;
                            j2 = j3;
                            i = 2;
                            i2 = 0;
                            i3 = 1;
                        }
                    }
                }
            } finally {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c7, code lost:
        throw r0;
     */
    private void b() {
        HashMap hashMap;
        Set set = gg.f4431a;
        String str = null;
        Cursor rawQuery = this.d.rawQuery("SELECT ROWID, * FROM UsageStats ORDER BY ROWID ASC", null);
        try {
            rawQuery = this.d.rawQuery("SELECT * FROM UsageStatValues ORDER BY stat_id ASC", null);
            rawQuery.moveToNext();
            while (rawQuery.moveToNext()) {
                int i = 0;
                long j = rawQuery.getLong(0);
                int i2 = 1;
                String string = rawQuery.getString(1);
                String string2 = rawQuery.getString(2);
                String str2 = string2.isEmpty() ? str : string2;
                int i3 = rawQuery.getInt(3);
                long j2 = rawQuery.getLong(4);
                long j3 = rawQuery.getLong(5);
                if (!rawQuery.isAfterLast()) {
                    hashMap = null;
                    while (true) {
                        if (rawQuery.getLong(i) != j) {
                            break;
                        }
                        if (hashMap == null) {
                            hashMap = new HashMap();
                        }
                        String string3 = rawQuery.getString(i2);
                        long j4 = rawQuery.getLong(3);
                        long j5 = rawQuery.getLong(4);
                        hashMap.put(string3, Long.valueOf(j4));
                        StringBuilder sb = new StringBuilder();
                        sb.append(string3);
                        sb.append("_max");
                        hashMap.put(sb.toString(), Long.valueOf(j5));
                        if (!rawQuery.moveToNext()) {
                            break;
                        }
                        i = 0;
                        i2 = 1;
                    }
                } else {
                    hashMap = null;
                }
                if (set != null) {
                    if (set.contains(string)) {
                        str = null;
                    }
                }
                this.c.a(string, str2, i3, j2, j3, (Map) hashMap);
                str = null;
            }
            rawQuery.close();
            this.d.execSQL("DELETE FROM UsageStats");
            this.d.execSQL("DELETE FROM UsageStatValues");
            this.g = 0;
            this.f = 0;
        } finally {
        }
    }
}
