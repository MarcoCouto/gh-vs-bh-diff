package com.tapjoy.internal;

import java.util.Iterator;

public abstract class jx implements Iterator {
    protected jx() {
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
