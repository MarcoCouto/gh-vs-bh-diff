package com.tapjoy.internal;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public final class av implements ax {

    /* renamed from: a reason: collision with root package name */
    private final List f4317a;

    public av(List list) {
        this.f4317a = list;
    }

    public final boolean add(Object obj) {
        return this.f4317a.add(obj);
    }

    public final boolean addAll(Collection collection) {
        return this.f4317a.addAll(collection);
    }

    public final void clear() {
        this.f4317a.clear();
    }

    public final boolean contains(Object obj) {
        return this.f4317a.contains(obj);
    }

    public final boolean containsAll(Collection collection) {
        return this.f4317a.containsAll(collection);
    }

    public final boolean equals(Object obj) {
        return this.f4317a.equals(obj);
    }

    public final Object a(int i) {
        return this.f4317a.get(i);
    }

    public final int hashCode() {
        return this.f4317a.hashCode();
    }

    public final boolean isEmpty() {
        return this.f4317a.isEmpty();
    }

    public final Iterator iterator() {
        return this.f4317a.iterator();
    }

    public final boolean remove(Object obj) {
        return this.f4317a.remove(obj);
    }

    public final boolean removeAll(Collection collection) {
        return this.f4317a.removeAll(collection);
    }

    public final boolean retainAll(Collection collection) {
        return this.f4317a.retainAll(collection);
    }

    public final int size() {
        return this.f4317a.size();
    }

    public final Object[] toArray() {
        return this.f4317a.toArray();
    }

    public final Object[] toArray(Object[] objArr) {
        return this.f4317a.toArray(objArr);
    }

    public final boolean offer(Object obj) {
        return this.f4317a.add(obj);
    }

    public final Object remove() {
        Object poll = poll();
        if (poll != null) {
            return poll;
        }
        throw new NoSuchElementException();
    }

    public final Object poll() {
        if (this.f4317a.isEmpty()) {
            return null;
        }
        return this.f4317a.remove(0);
    }

    public final Object element() {
        Object peek = peek();
        if (peek != null) {
            return peek;
        }
        throw new NoSuchElementException();
    }

    public final Object peek() {
        if (this.f4317a.isEmpty()) {
            return null;
        }
        return this.f4317a.get(0);
    }

    public final void b(int i) {
        aw.a(this.f4317a, i);
    }
}
