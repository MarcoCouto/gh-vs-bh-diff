package com.tapjoy.internal;

public final class ic {

    /* renamed from: a reason: collision with root package name */
    public float f4489a;
    public int b;

    public static ic a(String str) {
        if (jr.c(str)) {
            return null;
        }
        try {
            ic icVar = new ic();
            int length = str.length() - 1;
            char charAt = str.charAt(length);
            if (charAt == 'w') {
                icVar.f4489a = Float.valueOf(str.substring(0, length)).floatValue();
                icVar.b = 1;
            } else if (charAt == 'h') {
                icVar.f4489a = Float.valueOf(str.substring(0, length)).floatValue();
                icVar.b = 2;
            } else {
                icVar.f4489a = Float.valueOf(str).floatValue();
                icVar.b = 0;
            }
            return icVar;
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    public final float a(float f, float f2) {
        if (this.b == 1) {
            return (this.f4489a * f) / 100.0f;
        }
        if (this.b == 2) {
            return (this.f4489a * f2) / 100.0f;
        }
        return this.f4489a;
    }
}
