package com.tapjoy.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

public final class iw {

    /* renamed from: a reason: collision with root package name */
    static final Logger f4507a = Logger.getLogger(iw.class.getName());

    private iw() {
    }

    public static iu a(jd jdVar) {
        if (jdVar != null) {
            return new iy(jdVar);
        }
        throw new IllegalArgumentException("source == null");
    }

    public static it a(jc jcVar) {
        if (jcVar != null) {
            return new ix(jcVar);
        }
        throw new IllegalArgumentException("sink == null");
    }

    public static jc a(final OutputStream outputStream) {
        final je jeVar = new je();
        if (outputStream != null) {
            return new jc() {
                public final void a(is isVar, long j) {
                    jf.a(isVar.b, 0, j);
                    while (j > 0) {
                        jeVar.a();
                        iz izVar = isVar.f4505a;
                        int min = (int) Math.min(j, (long) (izVar.c - izVar.b));
                        outputStream.write(izVar.f4512a, izVar.b, min);
                        izVar.b += min;
                        long j2 = (long) min;
                        j -= j2;
                        isVar.b -= j2;
                        if (izVar.b == izVar.c) {
                            isVar.f4505a = izVar.a();
                            ja.a(izVar);
                        }
                    }
                }

                public final void flush() {
                    outputStream.flush();
                }

                public final void close() {
                    outputStream.close();
                }

                public final String toString() {
                    StringBuilder sb = new StringBuilder("sink(");
                    sb.append(outputStream);
                    sb.append(")");
                    return sb.toString();
                }
            };
        }
        throw new IllegalArgumentException("out == null");
    }

    public static jd a(final InputStream inputStream) {
        final je jeVar = new je();
        if (inputStream != null) {
            return new jd() {
                public final long b(is isVar, long j) {
                    if (j < 0) {
                        StringBuilder sb = new StringBuilder("byteCount < 0: ");
                        sb.append(j);
                        throw new IllegalArgumentException(sb.toString());
                    } else if (j == 0) {
                        return 0;
                    } else {
                        try {
                            jeVar.a();
                            iz c = isVar.c(1);
                            int read = inputStream.read(c.f4512a, c.c, (int) Math.min(j, (long) (8192 - c.c)));
                            if (read == -1) {
                                return -1;
                            }
                            c.c += read;
                            long j2 = (long) read;
                            isVar.b += j2;
                            return j2;
                        } catch (AssertionError e) {
                            if (iw.a(e)) {
                                throw new IOException(e);
                            }
                            throw e;
                        }
                    }
                }

                public final void close() {
                    inputStream.close();
                }

                public final String toString() {
                    StringBuilder sb = new StringBuilder("source(");
                    sb.append(inputStream);
                    sb.append(")");
                    return sb.toString();
                }
            };
        }
        throw new IllegalArgumentException("in == null");
    }

    static boolean a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }
}
