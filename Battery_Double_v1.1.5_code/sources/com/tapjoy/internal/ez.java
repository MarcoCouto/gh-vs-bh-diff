package com.tapjoy.internal;

public enum ez implements en {
    APP(0),
    CAMPAIGN(1),
    CUSTOM(2),
    USAGES(3);
    
    public static final ek ADAPTER = null;

    /* renamed from: a reason: collision with root package name */
    private final int f4398a;

    static final class a extends eg {
        a() {
            super(ez.class);
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ en a(int i) {
            return ez.a(i);
        }
    }

    static {
        ADAPTER = new a();
    }

    private ez(int i) {
        this.f4398a = i;
    }

    public static ez a(int i) {
        switch (i) {
            case 0:
                return APP;
            case 1:
                return CAMPAIGN;
            case 2:
                return CUSTOM;
            case 3:
                return USAGES;
            default:
                return null;
        }
    }

    public final int a() {
        return this.f4398a;
    }
}
