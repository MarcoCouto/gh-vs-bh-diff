package com.tapjoy.internal;

import com.tapjoy.internal.ex.a;
import java.util.Map;

public final class il extends ij {
    private final a c = new a();
    private ez d = null;

    public final String c() {
        return this.d == ez.USAGES ? "api/v1/usages" : "api/v1/cevs";
    }

    public final boolean a(ew ewVar) {
        if (this.d == null) {
            this.d = ewVar.n;
        } else if (ewVar.n != this.d) {
            return false;
        }
        this.c.c.add(ewVar);
        return true;
    }

    public final int g() {
        return this.c.c.size();
    }

    public final Map e() {
        Map e = super.e();
        e.put(EventEntry.TABLE_NAME, new bm(hq.a(this.c.b())));
        return e;
    }
}
