package com.tapjoy.internal;

import android.os.SystemClock;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;

public abstract class hl {

    /* renamed from: a reason: collision with root package name */
    protected static a f4466a;
    private static hl b;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public final String f4467a;
        public final String b;
        public final long c = SystemClock.elapsedRealtime();
        public final fj d = new fj(ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);

        public a(String str, String str2) {
            this.f4467a = str;
            this.b = str2;
        }
    }

    public abstract void a(a aVar);

    public abstract boolean b();

    protected static void a(hl hlVar) {
        synchronized (hl.class) {
            b = hlVar;
            a aVar = f4466a;
            if (aVar != null) {
                f4466a = null;
                hlVar.a(aVar);
            }
        }
    }

    public static void a(String str, String str2) {
        synchronized (hl.class) {
            a aVar = new a(str, str2);
            if (b != null) {
                f4466a = null;
                b.a(aVar);
            } else {
                f4466a = aVar;
            }
        }
    }

    public static boolean c() {
        if (b != null && b.b()) {
            return true;
        }
        a aVar = f4466a;
        if (aVar == null || aVar.d.a()) {
            return false;
        }
        return true;
    }
}
