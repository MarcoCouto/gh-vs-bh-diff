package com.tapjoy.internal;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.tapjoy.TJConnectListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public abstract class gc {

    /* renamed from: a reason: collision with root package name */
    private final ReentrantLock f4420a = new ReentrantLock();
    volatile int b = c.f4428a;
    b c;
    long d = 1000;
    a e;
    private final Condition f = this.f4420a.newCondition();
    private final LinkedList g = new LinkedList();
    private a h;

    /* renamed from: com.tapjoy.internal.gc$3 reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f4423a = new int[c.a().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0019 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0021 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0029 */
        static {
            f4423a[c.e - 1] = 1;
            f4423a[c.f4428a - 1] = 2;
            f4423a[c.b - 1] = 3;
            f4423a[c.c - 1] = 4;
            f4423a[c.d - 1] = 5;
        }
    }

    class a {

        /* renamed from: a reason: collision with root package name */
        public final Context f4424a;
        public final String b;
        public final Hashtable c;

        public a(Context context, String str, Hashtable hashtable) {
            Context context2;
            if (context == null) {
                context2 = null;
            } else if (context instanceof Application) {
                context2 = context;
            } else {
                context2 = context.getApplicationContext();
            }
            if (context2 == null) {
                context2 = context;
            }
            this.f4424a = context2;
            this.b = str;
            this.c = hashtable;
        }
    }

    class b extends kb {
        private boolean b;
        /* access modifiers changed from: private */
        public boolean c;
        private Context d;
        private BroadcastReceiver e;

        private b() {
            this.e = new BroadcastReceiver() {
                public final void onReceive(Context context, Intent intent) {
                    gc.this.b();
                }
            };
        }

        /* synthetic */ b(gc gcVar, byte b2) {
            this();
        }

        /* access modifiers changed from: protected */
        public final void a() {
            this.b = true;
            gc.this.b();
        }

        /* access modifiers changed from: protected */
        public final void b() {
            gc gcVar = gc.this;
            int i = c.c;
            int i2 = c.b;
            gcVar.a(i);
        }

        /* access modifiers changed from: protected */
        public final void c() {
            if (gc.this.c == this) {
                gc.this.c = null;
            }
            if (gc.this.b == c.c) {
                gc gcVar = gc.this;
                int i = c.f4428a;
                int i2 = c.c;
                gcVar.a(i);
            }
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x004e */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0064 A[SYNTHETIC, Splitter:B:17:0x0064] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0052 A[SYNTHETIC] */
        public final void d() {
            this.d = gc.this.a().f4424a;
            this.d.registerReceiver(this.e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            while (!this.b) {
                try {
                    final CountDownLatch countDownLatch = new CountDownLatch(1);
                    ft.b.addObserver(new Observer() {
                        public final void update(Observable observable, Object obj) {
                            ft.b.deleteObserver(this);
                            b.this.c = Boolean.TRUE.equals(obj);
                            countDownLatch.countDown();
                        }
                    });
                    a a2 = gc.this.a();
                    if (!gc.this.a(a2.f4424a, a2.b, a2.c, null)) {
                        gc.this.a(false);
                        h();
                        return;
                    }
                    countDownLatch.await();
                    if (!this.c) {
                        gc gcVar = gc.this;
                        int i = c.e;
                        int i2 = c.c;
                        gcVar.a(i);
                        gc.this.a(true);
                        return;
                    }
                    gc.this.a(false);
                    long max = Math.max(gc.this.d, 1000);
                    gc.this.d = Math.min(max << 2, 3600000);
                    gc.this.a(max);
                } finally {
                    h();
                }
            }
            h();
        }

        private void h() {
            this.d.unregisterReceiver(this.e);
        }
    }

    enum c {
        ;
        

        /* renamed from: a reason: collision with root package name */
        public static final int f4428a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;

        static {
            f = new int[]{f4428a, b, c, d, e};
        }

        public static int[] a() {
            return (int[]) f.clone();
        }
    }

    public abstract boolean a(Context context, String str, Hashtable hashtable, TJConnectListener tJConnectListener);

    public final boolean b(Context context, String str, Hashtable hashtable, TJConnectListener tJConnectListener) {
        this.f4420a.lock();
        if (tJConnectListener != null) {
            try {
                this.g.addLast(fo.a(tJConnectListener, TJConnectListener.class));
            } catch (Throwable th) {
                this.f4420a.unlock();
                throw th;
            }
        }
        a aVar = new a(context, str, hashtable);
        switch (AnonymousClass3.f4423a[this.b - 1]) {
            case 1:
                a(true);
                this.f4420a.unlock();
                return true;
            case 2:
                this.e = aVar;
                ft.b.addObserver(new Observer() {
                    public final void update(Observable observable, Object obj) {
                        ft.b.deleteObserver(this);
                        if (!Boolean.valueOf(Boolean.TRUE.equals(obj)).booleanValue() && gc.this.e != null && gc.this.e.f4424a != null) {
                            gc.this.c = new b(gc.this, 0);
                            gc.this.c.e();
                        }
                    }
                });
                if (a(aVar.f4424a, aVar.b, aVar.c, new TJConnectListener() {
                    public final void onConnectSuccess() {
                        gc gcVar = gc.this;
                        int i = c.e;
                        int i2 = c.b;
                        gcVar.a(i);
                        gc.this.a(true);
                    }

                    public final void onConnectFailure() {
                        gc.this.a(false);
                    }
                })) {
                    int i = c.b;
                    int i2 = c.f4428a;
                    a(i);
                    this.f4420a.unlock();
                    return true;
                }
                this.g.clear();
                this.f4420a.unlock();
                return false;
            case 3:
            case 4:
                this.h = aVar;
                this.f4420a.unlock();
                return true;
            case 5:
                this.h = aVar;
                b();
                this.f4420a.unlock();
                return true;
            default:
                a(c.f4428a);
                this.f4420a.unlock();
                return false;
        }
        this.f4420a.unlock();
        throw th;
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i) {
        this.f4420a.lock();
        try {
            int i2 = this.b;
            this.b = i;
        } finally {
            this.f4420a.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public final a a() {
        this.f4420a.lock();
        try {
            if (this.h != null) {
                this.e = this.h;
                this.h = null;
            }
            return this.e;
        } finally {
            this.f4420a.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        this.f4420a.lock();
        try {
            if (this.g.size() != 0) {
                ArrayList arrayList = new ArrayList(this.g);
                this.g.clear();
                this.f4420a.unlock();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    TJConnectListener tJConnectListener = (TJConnectListener) it.next();
                    if (z) {
                        tJConnectListener.onConnectSuccess();
                    } else {
                        tJConnectListener.onConnectFailure();
                    }
                }
            }
        } finally {
            this.f4420a.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.f4420a.lock();
        try {
            this.d = 1000;
            this.f.signal();
        } finally {
            this.f4420a.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(long j) {
        this.f4420a.lock();
        try {
            int i = c.d;
            int i2 = c.c;
            a(i);
            if (this.f.await(j, TimeUnit.MILLISECONDS)) {
                this.d = 1000;
            }
            return false;
        } catch (InterruptedException unused) {
            return false;
        } finally {
            int i3 = c.c;
            int i4 = c.d;
            a(i3);
            this.f4420a.unlock();
        }
    }
}
