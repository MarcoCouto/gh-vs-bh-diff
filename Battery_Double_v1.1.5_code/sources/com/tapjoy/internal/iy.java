package com.tapjoy.internal;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.EOFException;

final class iy implements iu {

    /* renamed from: a reason: collision with root package name */
    public final is f4511a = new is();
    public final jd b;
    boolean c;

    iy(jd jdVar) {
        if (jdVar != null) {
            this.b = jdVar;
            return;
        }
        throw new IllegalArgumentException("source == null");
    }

    public final long b(is isVar, long j) {
        if (isVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            StringBuilder sb = new StringBuilder("byteCount < 0: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (this.c) {
            throw new IllegalStateException("closed");
        } else if (this.f4511a.b == 0 && this.b.b(this.f4511a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
            return -1;
        } else {
            return this.f4511a.b(isVar, Math.min(j, this.f4511a.b));
        }
    }

    public final boolean b() {
        if (!this.c) {
            return this.f4511a.b() && this.b.b(this.f4511a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1;
        }
        throw new IllegalStateException("closed");
    }

    public final byte c() {
        a(1);
        return this.f4511a.c();
    }

    public final iv b(long j) {
        a(j);
        return this.f4511a.b(j);
    }

    public final String c(long j) {
        a(j);
        return this.f4511a.c(j);
    }

    public final int e() {
        a(4);
        return jf.a(this.f4511a.d());
    }

    public final long f() {
        a(8);
        return this.f4511a.f();
    }

    public final void d(long j) {
        if (!this.c) {
            while (j > 0) {
                if (this.f4511a.b == 0 && this.b.b(this.f4511a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j, this.f4511a.b);
                this.f4511a.d(min);
                j -= min;
            }
            return;
        }
        throw new IllegalStateException("closed");
    }

    public final void close() {
        if (!this.c) {
            this.c = true;
            this.b.close();
            is isVar = this.f4511a;
            try {
                isVar.d(isVar.b);
            } catch (EOFException e) {
                throw new AssertionError(e);
            }
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("buffer(");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }

    public final void a(long j) {
        boolean z;
        if (j < 0) {
            StringBuilder sb = new StringBuilder("byteCount < 0: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (!this.c) {
            while (true) {
                if (this.f4511a.b < j) {
                    if (this.b.b(this.f4511a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                        z = false;
                        break;
                    }
                } else {
                    z = true;
                    break;
                }
            }
            if (!z) {
                throw new EOFException();
            }
        } else {
            throw new IllegalStateException("closed");
        }
    }
}
