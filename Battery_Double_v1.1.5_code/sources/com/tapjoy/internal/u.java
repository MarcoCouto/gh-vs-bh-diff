package com.tapjoy.internal;

import android.os.Handler;
import android.os.Looper;

public final class u {

    /* renamed from: a reason: collision with root package name */
    private static Handler f4546a;

    public static synchronized Handler a() {
        Handler handler;
        synchronized (u.class) {
            if (f4546a == null) {
                f4546a = new Handler(Looper.getMainLooper());
            }
            handler = f4546a;
        }
        return handler;
    }

    public static ba a(final Handler handler) {
        return new ba() {
            public final boolean a(Runnable runnable) {
                return handler.post(runnable);
            }
        };
    }
}
