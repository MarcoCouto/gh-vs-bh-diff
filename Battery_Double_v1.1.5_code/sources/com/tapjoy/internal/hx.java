package com.tapjoy.internal;

final class hx extends hp implements gq {

    /* renamed from: a reason: collision with root package name */
    public static final bi f4484a = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            String str = "";
            String str2 = "";
            bnVar.h();
            while (bnVar.j()) {
                String l = bnVar.l();
                if ("campaign_id".equals(l)) {
                    str = bnVar.c("");
                } else if ("product_id".equals(l)) {
                    str2 = bnVar.c("");
                } else {
                    bnVar.s();
                }
            }
            bnVar.i();
            return new hx(str, str2);
        }
    };
    private final String b;
    private final String c;

    hx(String str, String str2) {
        this.b = str;
        this.c = str2;
    }

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }
}
