package com.tapjoy.internal;

import android.graphics.Rect;
import com.tapjoy.TapjoyConstants;

public final class ht {
    public static final bi h = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            bnVar.h();
            String str = "";
            Rect rect = null;
            String str2 = null;
            String str3 = null;
            String str4 = null;
            gm gmVar = null;
            boolean z = false;
            while (bnVar.j()) {
                String l = bnVar.l();
                if (TtmlNode.TAG_REGION.equals(l)) {
                    rect = (Rect) bj.b.a(bnVar);
                } else if ("value".equals(l)) {
                    str2 = bnVar.m();
                } else if (TapjoyConstants.TJC_FULLSCREEN_AD_DISMISS_URL.equals(l)) {
                    z = bnVar.n();
                } else if ("url".equals(l)) {
                    str = bnVar.m();
                } else if (TapjoyConstants.TJC_REDIRECT_URL.equals(l)) {
                    str3 = bnVar.b();
                } else if ("ad_content".equals(l)) {
                    str4 = bnVar.b();
                } else if (hp.a(l)) {
                    gmVar = hp.a(l, bnVar);
                } else {
                    bnVar.s();
                }
            }
            bnVar.i();
            ht htVar = new ht(rect, str2, z, str, str3, str4, gmVar);
            return htVar;
        }
    };

    /* renamed from: a reason: collision with root package name */
    public final Rect f4477a;
    public final String b;
    public final boolean c;
    public final String d;
    public String e;
    public String f;
    public final gm g;

    ht(Rect rect, String str, boolean z, String str2, String str3, String str4, gm gmVar) {
        this.f4477a = rect;
        this.b = str;
        this.c = z;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = gmVar;
    }
}
