package com.tapjoy.internal;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;

public final class ia {
    public static final bi n = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            return new ia(bnVar);
        }
    };

    /* renamed from: a reason: collision with root package name */
    public ic f4487a;
    public ic b;
    public ic c;
    public ic d;
    public int e = 9;
    public int f = 10;
    public String g;
    public String h;
    public String i;
    public boolean j = false;
    public String k;
    public hy l;
    public hy m;

    public ia(bn bnVar) {
        bnVar.h();
        while (bnVar.j()) {
            String l2 = bnVar.l();
            if (AvidJSONUtil.KEY_X.equals(l2)) {
                this.f4487a = ic.a(bnVar.m());
            } else if (AvidJSONUtil.KEY_Y.equals(l2)) {
                this.b = ic.a(bnVar.m());
            } else if ("width".equals(l2)) {
                this.c = ic.a(bnVar.m());
            } else if ("height".equals(l2)) {
                this.d = ic.a(bnVar.m());
            } else if ("url".equals(l2)) {
                this.g = bnVar.m();
            } else if (TapjoyConstants.TJC_REDIRECT_URL.equals(l2)) {
                this.h = bnVar.m();
            } else if ("ad_content".equals(l2)) {
                this.i = bnVar.m();
            } else if (TapjoyConstants.TJC_FULLSCREEN_AD_DISMISS_URL.equals(l2)) {
                this.j = bnVar.n();
            } else if ("value".equals(l2)) {
                this.k = bnVar.m();
            } else if (MessengerShareContentUtility.MEDIA_IMAGE.equals(l2)) {
                this.l = (hy) hy.e.a(bnVar);
            } else if ("image_clicked".equals(l2)) {
                this.m = (hy) hy.e.a(bnVar);
            } else if ("align".equals(l2)) {
                String m2 = bnVar.m();
                if ("left".equals(m2)) {
                    this.e = 9;
                } else if ("right".equals(m2)) {
                    this.e = 11;
                } else if (TtmlNode.CENTER.equals(m2)) {
                    this.e = 14;
                } else {
                    bnVar.s();
                }
            } else if ("valign".equals(l2)) {
                String m3 = bnVar.m();
                if (String.TOP.equals(m3)) {
                    this.f = 10;
                } else if ("middle".equals(m3)) {
                    this.f = 15;
                } else if (String.BOTTOM.equals(m3)) {
                    this.f = 12;
                } else {
                    bnVar.s();
                }
            } else {
                bnVar.s();
            }
        }
        bnVar.i();
    }
}
