package com.tapjoy.internal;

import java.util.List;

public final class fg extends ei {
    public static final ek c = new b();
    public final List d;

    public static final class a extends com.tapjoy.internal.ei.a {
        public List c = ep.a();

        public final fg b() {
            return new fg(this.c, super.a());
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            fg fgVar = (fg) obj;
            return ff.c.a().a(1, (Object) fgVar.d) + fgVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            fg fgVar = (fg) obj;
            ff.c.a().a(emVar, 1, fgVar.d);
            emVar.a(fgVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, fg.class);
        }

        public final /* synthetic */ Object a(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b == -1) {
                    elVar.a(a2);
                    return aVar.b();
                } else if (b != 1) {
                    eh c = elVar.c();
                    aVar.a(b, c, c.a().a(elVar));
                } else {
                    aVar.c.add(ff.c.a(elVar));
                }
            }
        }
    }

    public fg(List list) {
        this(list, iv.b);
    }

    public fg(List list, iv ivVar) {
        super(c, ivVar);
        this.d = ep.a("pushes", list);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fg)) {
            return false;
        }
        fg fgVar = (fg) obj;
        return a().equals(fgVar.a()) && this.d.equals(fgVar.d);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int hashCode = (a().hashCode() * 37) + this.d.hashCode();
        this.b = hashCode;
        return hashCode;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (!this.d.isEmpty()) {
            sb.append(", pushes=");
            sb.append(this.d);
        }
        StringBuilder replace = sb.replace(0, 2, "PushList{");
        replace.append('}');
        return replace.toString();
    }
}
