package com.tapjoy.internal;

import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

public final class ay extends AbstractSet implements Serializable, Set {

    /* renamed from: a reason: collision with root package name */
    transient WeakHashMap f4318a;

    public ay() {
        this(new WeakHashMap());
    }

    private ay(WeakHashMap weakHashMap) {
        this.f4318a = weakHashMap;
    }

    public final boolean add(Object obj) {
        return this.f4318a.put(obj, this) == null;
    }

    public final void clear() {
        this.f4318a.clear();
    }

    public final Object clone() {
        try {
            return (ay) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean contains(Object obj) {
        return this.f4318a.containsKey(obj);
    }

    public final boolean isEmpty() {
        return this.f4318a.isEmpty();
    }

    public final Iterator iterator() {
        return this.f4318a.keySet().iterator();
    }

    public final boolean remove(Object obj) {
        return this.f4318a.remove(obj) != null;
    }

    public final int size() {
        return this.f4318a.size();
    }
}
