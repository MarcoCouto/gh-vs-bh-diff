package com.tapjoy.internal;

public final class ff extends ei {
    public static final ek c = new b();
    public static final Long d = Long.valueOf(0);
    public static final Long e = Long.valueOf(0);
    public final String f;
    public final Long g;
    public final Long h;

    public static final class a extends com.tapjoy.internal.ei.a {
        public String c;
        public Long d;
        public Long e;

        public final ff b() {
            if (this.c != null && this.d != null) {
                return new ff(this.c, this.d, this.e, super.a());
            }
            throw ep.a(this.c, "id", this.d, "received");
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            ff ffVar = (ff) obj;
            return ek.p.a(1, (Object) ffVar.f) + ek.i.a(2, (Object) ffVar.g) + (ffVar.h != null ? ek.i.a(3, (Object) ffVar.h) : 0) + ffVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            ff ffVar = (ff) obj;
            ek.p.a(emVar, 1, ffVar.f);
            ek.i.a(emVar, 2, ffVar.g);
            if (ffVar.h != null) {
                ek.i.a(emVar, 3, ffVar.h);
            }
            emVar.a(ffVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, ff.class);
        }

        public final /* synthetic */ Object a(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            aVar.c = (String) ek.p.a(elVar);
                            break;
                        case 2:
                            aVar.d = (Long) ek.i.a(elVar);
                            break;
                        case 3:
                            aVar.e = (Long) ek.i.a(elVar);
                            break;
                        default:
                            eh c = elVar.c();
                            aVar.a(b, c, c.a().a(elVar));
                            break;
                    }
                } else {
                    elVar.a(a2);
                    return aVar.b();
                }
            }
        }
    }

    public ff(String str, Long l) {
        this(str, l, null, iv.b);
    }

    public ff(String str, Long l, Long l2, iv ivVar) {
        super(c, ivVar);
        this.f = str;
        this.g = l;
        this.h = l2;
    }

    public final a b() {
        a aVar = new a();
        aVar.c = this.f;
        aVar.d = this.g;
        aVar.e = this.h;
        aVar.a(a());
        return aVar;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ff)) {
            return false;
        }
        ff ffVar = (ff) obj;
        return a().equals(ffVar.a()) && this.f.equals(ffVar.f) && this.g.equals(ffVar.g) && ep.a((Object) this.h, (Object) ffVar.h);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int hashCode = (((((a().hashCode() * 37) + this.f.hashCode()) * 37) + this.g.hashCode()) * 37) + (this.h != null ? this.h.hashCode() : 0);
        this.b = hashCode;
        return hashCode;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(", id=");
        sb.append(this.f);
        sb.append(", received=");
        sb.append(this.g);
        if (this.h != null) {
            sb.append(", clicked=");
            sb.append(this.h);
        }
        StringBuilder replace = sb.replace(0, 2, "Push{");
        replace.append('}');
        return replace.toString();
    }
}
