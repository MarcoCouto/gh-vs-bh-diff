package com.tapjoy.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.ArrayList;
import java.util.Iterator;

public final class ir extends RelativeLayout implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private boolean f4504a;
    private float b = 1.0f;
    private View c;
    private View d;
    private FrameLayout e;
    private ImageView f;
    private io g;
    private hv h;
    private a i;

    public interface a {
        void a();

        void a(ht htVar);

        void b();
    }

    public ir(Context context, hv hvVar, a aVar) {
        super(context);
        this.h = hvVar;
        this.i = aVar;
        Context context2 = getContext();
        this.c = new View(context2);
        boolean z = true;
        this.c.setId(1);
        LayoutParams layoutParams = new LayoutParams(0, 0);
        layoutParams.addRule(13);
        addView(this.c, layoutParams);
        this.d = new View(context2);
        LayoutParams layoutParams2 = new LayoutParams(0, 0);
        layoutParams2.addRule(13);
        addView(this.d, layoutParams2);
        this.e = new FrameLayout(context2);
        LayoutParams layoutParams3 = new LayoutParams(0, 0);
        layoutParams3.addRule(13);
        addView(this.e, layoutParams3);
        this.f = new ImageView(context2);
        this.f.setOnClickListener(this);
        LayoutParams layoutParams4 = new LayoutParams(0, 0);
        layoutParams4.addRule(7, this.c.getId());
        layoutParams4.addRule(6, this.c.getId());
        addView(this.f, layoutParams4);
        if (this.h.m != null) {
            hw hwVar = this.h.m;
            if (hwVar.f4483a == null || (hwVar.b == null && hwVar.c == null)) {
                z = false;
            }
            if (z) {
                this.g = new io(context2);
                this.g.setOnClickListener(this);
                LayoutParams layoutParams5 = new LayoutParams(0, 0);
                layoutParams5.addRule(5, this.d.getId());
                layoutParams5.addRule(8, this.d.getId());
                addView(this.g, layoutParams5);
            }
        }
        this.f.setImageBitmap(hvVar.c.b);
        if (this.g != null && hvVar.m != null && hvVar.m.f4483a != null) {
            this.g.setImageBitmap(hvVar.m.f4483a.b);
        }
    }

    public final void setLandscape(boolean z) {
        Bitmap bitmap;
        ArrayList arrayList;
        Bitmap bitmap2;
        this.f4504a = z;
        if (z) {
            bitmap = this.h.b.b;
            bitmap2 = this.h.f.b;
            arrayList = this.h.j;
        } else {
            bitmap = this.h.f4482a.b;
            bitmap2 = this.h.e.b;
            arrayList = this.h.i;
        }
        ab.a(this.c, new BitmapDrawable(null, bitmap));
        ab.a(this.d, new BitmapDrawable(null, bitmap2));
        if (this.e.getChildCount() > 0) {
            this.e.removeAllViews();
        }
        Context context = getContext();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ht htVar = (ht) it.next();
            View view = new View(context);
            view.setTag(htVar);
            view.setOnClickListener(this);
            this.e.addView(view, new FrameLayout.LayoutParams(0, 0, 51));
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        int i4;
        Point point;
        int size = MeasureSpec.getSize(i2);
        int size2 = MeasureSpec.getSize(i3);
        if (this.f4504a) {
            this.b = Math.min(((float) size) / 480.0f, ((float) size2) / 320.0f);
        } else {
            this.b = Math.min(((float) size) / 320.0f, ((float) size2) / 480.0f);
        }
        LayoutParams layoutParams = (LayoutParams) this.c.getLayoutParams();
        boolean z = this.f4504a;
        int i5 = ModuleDescriptor.MODULE_VERSION;
        layoutParams.width = a(z ? 480 : ModuleDescriptor.MODULE_VERSION);
        if (!this.f4504a) {
            i5 = 480;
        }
        layoutParams.height = a(i5);
        LayoutParams layoutParams2 = (LayoutParams) this.d.getLayoutParams();
        boolean z2 = this.f4504a;
        int i6 = IronSourceConstants.INTERSTITIAL_AD_REWARDED;
        layoutParams2.width = a(z2 ? 448 : IronSourceConstants.INTERSTITIAL_AD_REWARDED);
        if (!this.f4504a) {
            i6 = 448;
        }
        layoutParams2.height = a(i6);
        LayoutParams layoutParams3 = (LayoutParams) this.e.getLayoutParams();
        layoutParams3.width = layoutParams2.width;
        layoutParams3.height = layoutParams2.height;
        for (View view : ac.a(this.e)) {
            FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) view.getLayoutParams();
            Rect rect = ((ht) view.getTag()).f4477a;
            layoutParams4.width = a(rect.width());
            layoutParams4.height = a(rect.height());
            layoutParams4.leftMargin = a(rect.left);
            layoutParams4.topMargin = a(rect.top);
        }
        int i7 = 0;
        int a2 = a(0);
        this.f.setPadding(a2, a2, a2, a2);
        LayoutParams layoutParams5 = (LayoutParams) this.f.getLayoutParams();
        layoutParams5.width = a(30);
        layoutParams5.height = layoutParams5.width;
        int i8 = -a2;
        layoutParams5.rightMargin = a(this.h.d.x) + i8;
        layoutParams5.topMargin = i8 + a(this.h.d.y);
        if (this.g != null) {
            int i9 = 15;
            int a3 = a(this.f4504a ? 16 : 15);
            if (!this.f4504a) {
                i9 = 16;
            }
            int a4 = a(i9);
            this.g.setPadding(a2, a2, a2, a2);
            LayoutParams layoutParams6 = (LayoutParams) this.g.getLayoutParams();
            layoutParams6.width = a(26);
            layoutParams6.height = layoutParams6.width;
            if (this.h.m != null) {
                if (this.f4504a) {
                    hw hwVar = this.h.m;
                    if (hwVar.b == null) {
                        point = hwVar.c;
                    } else {
                        point = hwVar.b;
                    }
                } else {
                    hw hwVar2 = this.h.m;
                    if (hwVar2.c == null) {
                        point = hwVar2.b;
                    } else {
                        point = hwVar2.c;
                    }
                }
                if (point != null) {
                    i7 = point.x;
                    i4 = point.y;
                    layoutParams6.leftMargin = a3 + a(i7);
                    layoutParams6.topMargin = a4 + a(i4);
                }
            }
            i4 = 0;
            layoutParams6.leftMargin = a3 + a(i7);
            layoutParams6.topMargin = a4 + a(i4);
        }
        super.onMeasure(i2, i3);
    }

    private int a(int i2) {
        return (int) (((float) i2) * this.b);
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
    }

    public final void onClick(View view) {
        if (view == this.f) {
            this.i.a();
        } else if (view == null || view != this.g) {
            if (view.getTag() instanceof ht) {
                this.i.a((ht) view.getTag());
            }
        } else {
            io ioVar = this.g;
            ioVar.f4499a = !ioVar.f4499a;
            ioVar.a();
            ioVar.invalidate();
            this.i.b();
        }
    }
}
