package com.tapjoy.internal;

import com.tapjoy.internal.ik.a;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class hi implements cf {

    /* renamed from: a reason: collision with root package name */
    public final ha f4463a;
    Set b = null;
    private final Map c = Collections.synchronizedMap(new HashMap());
    private final Map d = jv.a();

    public hi(ha haVar) {
        this.f4463a = haVar;
    }

    /* access modifiers changed from: private */
    public void a(ca caVar, a aVar) {
        if (caVar instanceof ik) {
            if (aVar.b != null) {
                List list = aVar.b;
                synchronized (this) {
                    this.b = list instanceof Collection ? new HashSet(jt.a(list)) : jw.a(list.iterator());
                }
            }
            ik ikVar = (ik) caVar;
            String str = ikVar.c;
            boolean z = ikVar.d;
            this.d.remove(str);
            if (!z) {
                this.c.put(str, aVar.f4497a);
            }
            hh hhVar = aVar.f4497a;
            hb hbVar = this.f4463a.p;
            if (hhVar instanceof hg) {
                gx.a("No content for \"{}\"", str);
                hbVar.a(str);
                return;
            }
            gx.a("New content for \"{}\" is ready", str);
            if (z) {
                hhVar.a(hbVar, new fx());
            } else {
                hbVar.b(str);
            }
        } else {
            throw new IllegalStateException(caVar.getClass().getName());
        }
    }

    public final void a(ca caVar) {
        a(caVar, new a(new hg(), null));
    }
}
