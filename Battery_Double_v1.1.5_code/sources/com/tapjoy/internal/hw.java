package com.tapjoy.internal;

import android.graphics.Point;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import java.net.URL;

public final class hw {
    public static final bi d = new bi() {
        private static Point b(bn bnVar) {
            bnVar.h();
            Point point = null;
            while (bnVar.j()) {
                if ("offset".equals(bnVar.l())) {
                    bnVar.h();
                    int i = 0;
                    int i2 = 0;
                    while (bnVar.j()) {
                        String l = bnVar.l();
                        if (AvidJSONUtil.KEY_X.equals(l)) {
                            i = bnVar.r();
                        } else if (AvidJSONUtil.KEY_Y.equals(l)) {
                            i2 = bnVar.r();
                        } else {
                            bnVar.s();
                        }
                    }
                    bnVar.i();
                    point = new Point(i, i2);
                } else {
                    bnVar.s();
                }
            }
            bnVar.i();
            return point;
        }

        public final /* synthetic */ Object a(bn bnVar) {
            bnVar.h();
            hy hyVar = null;
            Point point = null;
            Point point2 = null;
            while (bnVar.j()) {
                String l = bnVar.l();
                if (MessengerShareContentUtility.MEDIA_IMAGE.equals(l)) {
                    String m = bnVar.m();
                    if (!jr.c(m)) {
                        hyVar = new hy(new URL(m));
                    }
                } else if ("landscape".equals(l)) {
                    point = b(bnVar);
                } else if ("portrait".equals(l)) {
                    point2 = b(bnVar);
                } else {
                    bnVar.s();
                }
            }
            bnVar.i();
            return new hw(hyVar, point, point2);
        }
    };

    /* renamed from: a reason: collision with root package name */
    public final hy f4483a;
    public final Point b;
    public final Point c;

    public hw(hy hyVar, Point point, Point point2) {
        this.f4483a = hyVar;
        this.b = point;
        this.c = point2;
    }
}
