package com.tapjoy.internal;

import java.io.Closeable;
import java.io.Flushable;
import java.util.LinkedList;

public final class au extends at implements ax, Closeable, Flushable {

    /* renamed from: a reason: collision with root package name */
    private final ax f4316a;
    private final LinkedList b = new LinkedList();
    private final LinkedList c = new LinkedList();
    private int d;
    private boolean e;

    public static au a(ax axVar) {
        return new au(axVar);
    }

    private au(ax axVar) {
        this.f4316a = axVar;
        this.d = axVar.size();
        this.e = this.d == 0;
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        close();
        super.finalize();
    }

    public final void close() {
        try {
            flush();
        } finally {
            if (this.f4316a instanceof Closeable) {
                ((Closeable) this.f4316a).close();
            }
        }
    }

    public final void flush() {
        if (!this.c.isEmpty()) {
            this.f4316a.addAll(this.c);
            if (this.e) {
                this.b.addAll(this.c);
            }
            this.c.clear();
        }
    }

    public final int size() {
        return this.d;
    }

    public final boolean offer(Object obj) {
        this.c.add(obj);
        this.d++;
        return true;
    }

    public final Object poll() {
        Object obj;
        if (this.d <= 0) {
            return null;
        }
        if (!this.b.isEmpty()) {
            obj = this.b.remove();
            this.f4316a.b(1);
        } else if (this.e) {
            obj = this.c.remove();
        } else {
            obj = this.f4316a.remove();
            if (this.d == this.c.size() + 1) {
                this.e = true;
            }
        }
        this.d--;
        return obj;
    }

    public final Object peek() {
        if (this.d <= 0) {
            return null;
        }
        if (!this.b.isEmpty()) {
            return this.b.element();
        }
        if (this.e) {
            return this.c.element();
        }
        Object peek = this.f4316a.peek();
        this.b.add(peek);
        if (this.d == this.b.size() + this.c.size()) {
            this.e = true;
        }
        return peek;
    }

    public final Object a(int i) {
        if (i < 0 || i >= this.d) {
            throw new IndexOutOfBoundsException();
        }
        int size = this.b.size();
        if (i < size) {
            return this.b.get(i);
        }
        if (this.e) {
            return this.c.get(i - size);
        }
        if (i >= this.f4316a.size()) {
            return this.c.get(i - this.f4316a.size());
        }
        Object obj = null;
        while (size <= i) {
            obj = this.f4316a.a(size);
            this.b.add(obj);
            size++;
        }
        if (i + 1 + this.c.size() == this.d) {
            this.e = true;
        }
        return obj;
    }

    public final void b(int i) {
        if (i <= 0 || i > this.d) {
            throw new IndexOutOfBoundsException();
        }
        if (i <= this.b.size()) {
            aw.a(this.b, i);
            this.f4316a.b(i);
        } else {
            this.b.clear();
            int size = (this.c.size() + i) - this.d;
            if (size < 0) {
                this.f4316a.b(i);
            } else {
                this.f4316a.clear();
                this.e = true;
                if (size > 0) {
                    aw.a(this.c, size);
                }
            }
        }
        this.d -= i;
    }
}
