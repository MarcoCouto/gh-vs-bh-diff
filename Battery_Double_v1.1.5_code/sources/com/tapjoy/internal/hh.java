package com.tapjoy.internal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import javax.annotation.Nullable;

public abstract class hh {
    long c;
    boolean d;
    public gt e;
    public String f;
    fr g;

    public abstract void a(hb hbVar, fx fxVar);

    public abstract void b();

    public boolean c() {
        return true;
    }

    static void a(Context context, @Nullable String str) {
        if (!jr.c(str)) {
            try {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            } catch (Exception unused) {
            }
        }
    }
}
