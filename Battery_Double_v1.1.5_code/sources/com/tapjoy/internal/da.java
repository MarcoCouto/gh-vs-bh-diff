package com.tapjoy.internal;

import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;

public enum da {
    PREROLL(BreakId.PREROLL),
    MIDROLL(BreakId.MIDROLL),
    POSTROLL("postroll"),
    STANDALONE("standalone");
    
    private final String e;

    private da(String str) {
        this.e = str;
    }

    public final String toString() {
        return this.e;
    }
}
