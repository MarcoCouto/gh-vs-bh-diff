package com.tapjoy.internal;

import android.view.animation.Animation;

public class ad {

    /* renamed from: a reason: collision with root package name */
    protected final Animation f4304a;

    /* renamed from: com.tapjoy.internal.ad$1 reason: invalid class name */
    public static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a reason: collision with root package name */
        public static final /* synthetic */ int[] f4305a = new int[a.a().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0019 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0021 */
        static {
            f4305a[a.f4306a - 1] = 1;
            f4305a[a.b - 1] = 2;
            f4305a[a.c - 1] = 3;
            try {
                f4305a[a.d - 1] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public enum a {
        ;
        

        /* renamed from: a reason: collision with root package name */
        public static final int f4306a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;

        static {
            e = new int[]{f4306a, b, c, d};
        }

        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    public ad(Animation animation) {
        this.f4304a = animation;
        animation.setDuration(400);
    }

    public Animation a() {
        return this.f4304a;
    }

    public final ad b() {
        this.f4304a.setDuration(600);
        return this;
    }
}
