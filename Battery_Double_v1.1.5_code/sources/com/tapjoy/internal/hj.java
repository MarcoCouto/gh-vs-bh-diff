package com.tapjoy.internal;

import android.content.Context;
import android.content.SharedPreferences;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class hj {

    /* renamed from: a reason: collision with root package name */
    final o f4464a = new o(this.c, "noMoreToday.date");
    public final o b = new o(this.c, "noMoreToday.actionIds");
    private final SharedPreferences c;

    public hj(Context context) {
        this.c = context.getApplicationContext().getSharedPreferences("fiverocks", 0);
        b();
    }

    static String a() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
    }

    public final void b() {
        String a2 = this.f4464a.a();
        if (a2 != null && !a().equals(a2)) {
            this.f4464a.a(null);
            this.b.a(null);
        }
    }
}
