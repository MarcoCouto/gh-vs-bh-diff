package com.tapjoy.internal;

import android.content.Context;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Base64;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TapjoyConstants;
import com.tapjoy.internal.ew.a;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import javax.annotation.Nullable;

public final class ha {
    private static final ha q;
    private static ha r;
    private static Handler w;
    private static File x;

    /* renamed from: a reason: collision with root package name */
    public final hi f4447a = new hi(this);
    public hj b;
    public boolean c = false;
    public String d = null;
    public Context e;
    public hd f;
    public gz g;
    public hn h;
    public gy i;
    public String j;
    public boolean k;
    public String l;
    public String m;
    public boolean n = false;
    public String o;
    public hb p = hb.a((gp) null);
    private boolean s = false;
    private boolean t = false;
    private String u;
    private String v;

    static {
        ha haVar = new ha();
        q = haVar;
        r = haVar;
    }

    public static ha a() {
        return r;
    }

    private ha() {
    }

    public final synchronized void b(Context context) {
        if (this.e == null) {
            Context applicationContext = context.getApplicationContext();
            this.e = applicationContext;
            gb.a().a(applicationContext);
            this.f = hd.a(applicationContext);
            File file = new File(c(applicationContext), "events2");
            if (this.i == null) {
                this.i = new gy(file);
            }
            this.g = new gz(this.f, this.i);
            this.h = new hn(this.g);
            this.b = new hj(applicationContext);
            gg.a(new gi(new File(c(applicationContext), "usages"), this.g));
            hu huVar = hu.f4478a;
            huVar.b = applicationContext.getApplicationContext();
            huVar.c = applicationContext.getSharedPreferences("tapjoyCacheDataMMF2E", 0);
            huVar.d = applicationContext.getSharedPreferences("tapjoyCacheDataMMF2U", 0);
            huVar.a();
        }
    }

    public final fc a(boolean z) {
        if (z) {
            this.f.a();
        }
        return this.f.b();
    }

    public final synchronized void b() {
        if (this.k) {
            hc.b(this.e).e(this.d);
            a((String) null);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        return;
     */
    public final synchronized void a(@Nullable final String str) {
        if (this.k) {
            if (str == null && this.o != null) {
                str = this.o;
            }
            this.o = null;
            if (str != null) {
                fc b2 = this.f.b();
                gx.a("GCM registration id of device {} updated for sender {}: {}", b2.d.h, this.d, str);
                new im(b2, str).a((cf) new cf() {
                    public final void a(ca caVar) {
                    }

                    public final /* synthetic */ void a(ca caVar, Object obj) {
                        hc b2 = hc.b(ha.this.e);
                        String str = str;
                        if (!str.equals(b2.b.b(b2.f4542a))) {
                            new Object[1][0] = str;
                            return;
                        }
                        b2.b.b(b2.f4542a, true);
                        b2.b.a(b2.f4542a, 0);
                    }
                }, ca.f4337a);
            }
        } else if (str != null) {
            this.o = str;
        }
    }

    public final void b(String str) {
        gz gzVar = this.g;
        a a2 = gzVar.a(ez.APP, "push_ignore");
        a2.s = new fd(null, null, str);
        gzVar.a(a2);
    }

    public final boolean a(Context context, String str, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        b(context);
        if (!this.f.a(str, currentTimeMillis, z)) {
            return false;
        }
        gz gzVar = this.g;
        a a2 = gzVar.a(ez.APP, "push_show");
        a2.s = new fd(null, null, str);
        gzVar.a(a2);
        return true;
    }

    public static void a(GLSurfaceView gLSurfaceView) {
        if (gx.a((Object) gLSurfaceView, "setGLSurfaceView: The given GLSurfaceView was null")) {
            gs.a(gLSurfaceView);
        }
    }

    public final Set c() {
        if (!d("getUserTags")) {
            return new HashSet();
        }
        return this.f.e();
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Set, code=java.util.Set<java.lang.String>, for r5v0, types: [java.util.Set, java.util.Set<java.lang.String>] */
    public final void a(Set<String> set) {
        if (d("setUserTags")) {
            if (set != null && !set.isEmpty()) {
                Set hashSet = new HashSet();
                for (String str : set) {
                    if (str != null) {
                        String trim = str.trim();
                        if (!trim.isEmpty() && trim.length() <= 200) {
                            hashSet.add(trim);
                            if (hashSet.size() >= 200) {
                                break;
                            }
                        }
                    }
                }
                set = hashSet;
            }
            hd hdVar = this.f;
            synchronized (hdVar) {
                if (set != null) {
                    try {
                        if (!set.isEmpty()) {
                            hdVar.c.z.a(Base64.encodeToString(fh.c.b(new fh(new ArrayList(set))), 2));
                            hdVar.b.A.clear();
                            hdVar.b.A.addAll(set);
                        }
                    } finally {
                    }
                }
                hdVar.c.z.c();
                hdVar.b.A.clear();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003d A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x005f A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0061 A[SYNTHETIC, Splitter:B:39:0x0061] */
    public final synchronized void a(Context context, String str, String str2, String str3, String str4, String str5) {
        boolean z;
        boolean z2;
        if (!this.k) {
            b(context);
            boolean z3 = true;
            if (gx.a(this.e != null, "The given context was null")) {
                if (str4 != null) {
                    if (str4.length() == 24 && str4.matches("[0-9a-f]{24}")) {
                        z = true;
                        if (!z) {
                            if (str5 != null) {
                                if (str5.length() == 20 && str5.matches("[0-9A-Za-z\\-_]{20}")) {
                                    z2 = true;
                                    if (!z2) {
                                        this.l = str;
                                        this.m = str2;
                                        this.u = str4;
                                        this.v = str5;
                                        try {
                                            URL url = new URL(str3);
                                            String str6 = "TapjoySDK";
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(str6);
                                            sb.append(" ");
                                            sb.append(str2);
                                            sb.append(" (");
                                            sb.append(Build.MODEL);
                                            sb.append("; Android ");
                                            sb.append(VERSION.RELEASE);
                                            sb.append("; ");
                                            sb.append(Locale.getDefault());
                                            sb.append(")");
                                            ce ceVar = new ce(sb.toString(), url);
                                            ca.b = ceVar;
                                            ca.f4337a = Executors.newCachedThreadPool();
                                            gy gyVar = this.i;
                                            gyVar.b = ceVar;
                                            gyVar.a();
                                            new Object[1][0] = str3;
                                            this.k = true;
                                            he heVar = new he(d(this.e));
                                            if (heVar.b() == null) {
                                                z3 = false;
                                            }
                                            if (!z3 && heVar.a()) {
                                                gz gzVar = this.g;
                                                gzVar.a(gzVar.a(ez.APP, "install"));
                                            }
                                            hd hdVar = this.f;
                                            if (!jr.c(str4) && !str4.equals(hdVar.c.D.a())) {
                                                hdVar.c.D.a(str4);
                                                hdVar.c.a(false);
                                            }
                                            b();
                                            return;
                                        } catch (MalformedURLException e2) {
                                            throw new IllegalArgumentException(e2);
                                        }
                                    } else {
                                        return;
                                    }
                                }
                            }
                            gx.b("Invalid App Key: {}", str5);
                            z2 = false;
                            if (!z2) {
                            }
                        } else {
                            return;
                        }
                    }
                }
                gx.b("Invalid App ID: {}", str4);
                z = false;
                if (!z) {
                }
            }
        }
    }

    public final boolean c(String str) {
        if ((this.k || this.j != null) && this.e != null) {
            return true;
        }
        if (gx.f4443a) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(": Should be called after initializing the SDK");
            gx.b(sb.toString());
        }
        return false;
    }

    public final boolean d(String str) {
        if (this.e != null) {
            return true;
        }
        if (gx.f4443a) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(": Should be called after initializing the SDK");
            gx.b(sb.toString());
        }
        return false;
    }

    public final boolean d() {
        return this.h != null && this.h.b.get();
    }

    public final boolean e() {
        boolean z;
        hn hnVar = this.h;
        if (hnVar.c != null) {
            hnVar.c.cancel(false);
            hnVar.c = null;
        }
        if (hnVar.b.compareAndSet(false, true)) {
            gx.a("New session started");
            gz gzVar = hnVar.f4470a;
            fd d2 = gzVar.f4446a.d();
            hd hdVar = gzVar.f4446a;
            synchronized (hdVar) {
                int b2 = hdVar.c.h.b() + 1;
                hdVar.c.h.a(b2);
                hdVar.b.h = Integer.valueOf(b2);
            }
            a a2 = gzVar.a(ez.APP, "bootup");
            gzVar.c = SystemClock.elapsedRealtime();
            if (d2 != null) {
                a2.s = d2;
            }
            gzVar.a(a2);
            ft.c.notifyObservers();
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            return false;
        }
        hi hiVar = this.f4447a;
        synchronized (hiVar) {
            hiVar.b = null;
        }
        hu.f4478a.a();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final void a(Map map) {
        gz gzVar = this.g;
        a a2 = gzVar.a(ez.CAMPAIGN, "impression");
        if (map != null) {
            a2.r = bh.a((Object) map);
        }
        gzVar.a(a2);
    }

    /* access modifiers changed from: 0000 */
    public final void a(Map map, long j2) {
        gz gzVar = this.g;
        a a2 = gzVar.a(ez.CAMPAIGN, ParametersKeys.VIEW);
        a2.i = Long.valueOf(j2);
        if (map != null) {
            a2.r = bh.a((Object) map);
        }
        gzVar.a(a2);
    }

    /* access modifiers changed from: 0000 */
    public final void a(Map map, String str) {
        gz gzVar = this.g;
        a a2 = gzVar.a(ez.CAMPAIGN, "click");
        LinkedHashMap linkedHashMap = new LinkedHashMap(map);
        linkedHashMap.put(TtmlNode.TAG_REGION, str);
        a2.r = bh.a((Object) linkedHashMap);
        gzVar.a(a2);
    }

    public static synchronized void a(Runnable runnable) {
        synchronized (ha.class) {
            if (w == null) {
                w = new Handler(Looper.getMainLooper());
            }
            w.post(runnable);
        }
    }

    public static synchronized File c(Context context) {
        File file;
        synchronized (ha.class) {
            if (x == null) {
                x = context.getDir("fiverocks", 0);
            }
            file = x;
        }
        return file;
    }

    static File d(Context context) {
        return new File(c(context), "install");
    }

    public static String a(Context context, Intent intent) {
        String a2 = d.a(intent);
        if (a2 != null) {
            ha haVar = r;
            haVar.b(context);
            if (jr.c(haVar.f.c()) || intent.getBooleanExtra("fiverocks:force", false)) {
                hd hdVar = haVar.f;
                synchronized (hdVar) {
                    hdVar.c.d.a(a2);
                    hdVar.b.d = a2;
                }
                if (a2.length() > 0) {
                    gz gzVar = haVar.g;
                    gzVar.a(gzVar.a(ez.APP, TapjoyConstants.TJC_REFERRER));
                }
            }
        }
        return a2;
    }

    public static ha a(Context context) {
        ha haVar = r;
        haVar.b(context);
        return haVar;
    }
}
