package com.tapjoy.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.ViewGroup;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager.BadTokenException;
import android.widget.FrameLayout.LayoutParams;
import com.tapjoy.TJContentActivity;
import com.tapjoy.TapjoyErrorMessage;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.TapjoyLog;
import com.tapjoy.internal.ip.a;
import java.util.Iterator;

public class gw extends hh {
    private static final String h = "gw";
    private static gw i;

    /* renamed from: a reason: collision with root package name */
    final String f4439a;
    final hs b;
    /* access modifiers changed from: private */
    public final ha j;
    private boolean k;
    private boolean l;
    private long m;
    private Context n;
    private ip o;
    private Activity p;
    private hb q;
    private Handler r;
    private Runnable s;

    public static void a() {
        gw gwVar = i;
        if (gwVar != null) {
            AnonymousClass1 r1 = new Runnable(gwVar) {

                /* renamed from: a reason: collision with root package name */
                final /* synthetic */ gw f4440a;

                {
                    this.f4440a = r1;
                }

                public final void run() {
                    gw.a(this.f4440a);
                }
            };
            Looper mainLooper = Looper.getMainLooper();
            if (mainLooper != null && mainLooper.getThread() == Thread.currentThread()) {
                r1.run();
                return;
            }
            u.a().post(r1);
        }
    }

    public gw(ha haVar, String str, hs hsVar, Context context) {
        this.j = haVar;
        this.f4439a = str;
        this.b = hsVar;
        this.n = context;
    }

    public final void b() {
        Iterator it = this.b.f4476a.iterator();
        while (it.hasNext()) {
            Iterator it2 = ((ib) it.next()).c.iterator();
            while (it2.hasNext()) {
                ia iaVar = (ia) it2.next();
                if (iaVar.l != null) {
                    iaVar.l.b();
                }
                if (iaVar.m != null) {
                    iaVar.m.b();
                }
            }
        }
    }

    public final boolean c() {
        Iterator it = this.b.f4476a.iterator();
        boolean z = true;
        while (it.hasNext()) {
            Iterator it2 = ((ib) it.next()).c.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = true;
                    continue;
                    break;
                }
                ia iaVar = (ia) it2.next();
                if ((iaVar.l == null || iaVar.l.a()) && (iaVar.m == null || iaVar.m.a())) {
                }
            }
            z = false;
            continue;
            if (!z) {
                return false;
            }
        }
        return z;
    }

    public final void a(hb hbVar, fx fxVar) {
        this.q = hbVar;
        this.p = gs.a();
        if (this.p != null && !this.p.isFinishing()) {
            try {
                a(this.p, hbVar, fxVar);
                new Object[1][0] = this.f4439a;
                return;
            } catch (BadTokenException unused) {
            }
        }
        this.p = a.a(this.n);
        if (this.p != null && !this.p.isFinishing()) {
            try {
                a(this.p, hbVar, fxVar);
                new Object[1][0] = this.f4439a;
                return;
            } catch (BadTokenException unused2) {
            }
        }
        gx.b("Failed to show the content for \"{}\". No usable activity found.", this.f4439a);
        hbVar.a(this.f4439a, this.f, null);
    }

    private void a(final Activity activity, final hb hbVar, fx fxVar) {
        if (this.k) {
            TapjoyLog.e(h, new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, "Content is already displayed"));
            return;
        }
        this.k = true;
        this.l = true;
        i = this;
        this.g = fxVar.f4413a;
        this.o = new ip(activity, this.b, new a() {
            public final void a(ia iaVar) {
                if (gw.this.g instanceof fw) {
                    fw fwVar = (fw) gw.this.g;
                    if (!(fwVar == null || fwVar.c == null)) {
                        fwVar.c.a();
                    }
                }
                gw.this.j.a(gw.this.b.b, iaVar.k);
                if (!jr.c(iaVar.h)) {
                    gw.this.e.a(activity, iaVar.h, jr.b(iaVar.i));
                    gw.this.d = true;
                } else if (!jr.c(iaVar.g)) {
                    hh.a((Context) activity, iaVar.g);
                }
                hbVar.a(gw.this.f4439a, null);
                if (iaVar.j) {
                    gw.a(gw.this);
                }
            }

            public final void a() {
                gw.a(gw.this);
            }
        });
        Window window = activity.getWindow();
        ip ipVar = this.o;
        LayoutParams layoutParams = new LayoutParams(-1, -1, 17);
        Callback callback = window.getCallback();
        window.setCallback(null);
        window.addContentView(ipVar, layoutParams);
        window.setCallback(callback);
        this.m = SystemClock.elapsedRealtime();
        this.j.a(this.b.b);
        fxVar.a();
        fr frVar = this.g;
        if (frVar != null) {
            frVar.b();
        }
        hbVar.c(this.f4439a);
        if (this.b.c > 0.0f) {
            this.r = new Handler(Looper.getMainLooper());
            this.s = new Runnable() {
                public final void run() {
                    gw.a(gw.this);
                }
            };
            this.r.postDelayed(this.s, (long) (this.b.c * 1000.0f));
        }
    }

    static /* synthetic */ void a(gw gwVar) {
        if (gwVar.l) {
            gwVar.l = false;
            if (gwVar.r != null) {
                gwVar.r.removeCallbacks(gwVar.s);
                gwVar.s = null;
                gwVar.r = null;
            }
            if (i == gwVar) {
                i = null;
            }
            gwVar.j.a(gwVar.b.b, SystemClock.elapsedRealtime() - gwVar.m);
            if (!gwVar.d && gwVar.q != null) {
                gwVar.q.a(gwVar.f4439a, gwVar.f, null);
                gwVar.q = null;
            }
            ViewGroup viewGroup = (ViewGroup) gwVar.o.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(gwVar.o);
            }
            gwVar.o = null;
            if (gwVar.p instanceof TJContentActivity) {
                gwVar.p.finish();
            }
            gwVar.p = null;
        }
    }
}
