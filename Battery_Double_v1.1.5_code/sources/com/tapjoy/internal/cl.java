package com.tapjoy.internal;

public final class cl {

    /* renamed from: a reason: collision with root package name */
    boolean f4344a;

    static int a(String str) {
        b(str);
        return Integer.parseInt(str.split("\\.", 2)[0]);
    }

    static void b(String str) {
        dp.a((Object) str, "Version cannot be null");
        if (!str.matches("[0-9]+\\.[0-9]+\\.[0-9]+.*")) {
            StringBuilder sb = new StringBuilder("Invalid version format : ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
    }
}
