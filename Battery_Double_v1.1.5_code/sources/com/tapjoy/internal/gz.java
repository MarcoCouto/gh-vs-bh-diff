package com.tapjoy.internal;

import android.content.SharedPreferences.Editor;
import android.os.SystemClock;
import com.tapjoy.internal.ey.a;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nullable;

public final class gz {

    /* renamed from: a reason: collision with root package name */
    final hd f4446a;
    final gy b;
    long c;
    private int d = 1;
    private final a e = new a();

    gz(hd hdVar, gy gyVar) {
        this.f4446a = hdVar;
        this.b = gyVar;
    }

    public final void a(String str, String str2, double d2, @Nullable String str3, @Nullable String str4, @Nullable String str5) {
        double d3;
        hd hdVar = this.f4446a;
        synchronized (hdVar) {
            Editor a2 = hdVar.c.a();
            int i = 1;
            if (str2.equals(hdVar.c.l.a())) {
                i = 1 + hdVar.c.m.b();
                hdVar.c.m.a(a2, i);
                d3 = hdVar.c.n.a() + d2;
                hdVar.c.n.a(a2, d3);
                a2.apply();
            } else {
                hdVar.c.l.a(a2, str2);
                hdVar.c.m.a(a2, 1);
                hdVar.c.n.a(a2, d2);
                hdVar.c.o.a(a2);
                hdVar.c.p.a(a2);
                a2.apply();
                hdVar.b.l = str2;
                hdVar.b.o = null;
                hdVar.b.p = null;
                d3 = d2;
            }
            hdVar.b.m = Integer.valueOf(i);
            hdVar.b.n = Double.valueOf(d3);
        }
        ew.a a3 = a(ez.APP, "purchase");
        fe.a aVar = new fe.a();
        aVar.c = str;
        if (str2 != null) {
            aVar.f = str2;
        }
        aVar.e = Double.valueOf(d2);
        if (str5 != null) {
            aVar.m = str5;
        }
        if (str3 != null) {
            aVar.o = str3;
        }
        if (str4 != null) {
            aVar.p = str4;
        }
        a3.p = aVar.b();
        a(a3);
        hd hdVar2 = this.f4446a;
        long longValue = a3.e.longValue();
        synchronized (hdVar2) {
            Editor a4 = hdVar2.c.a();
            hdVar2.c.o.a(a4, longValue);
            hdVar2.c.p.a(a4, d2);
            a4.apply();
            hdVar2.b.o = Long.valueOf(longValue);
            hdVar2.b.p = Double.valueOf(d2);
        }
    }

    public final void a(String str, String str2, String str3, String str4, Map map) {
        ew.a a2 = a(ez.CUSTOM, str2);
        a2.t = str;
        a2.u = str3;
        a2.v = str4;
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                a2.w.add(new fa((String) entry.getKey(), (Long) entry.getValue()));
            }
        }
        a(a2);
    }

    public final void a(String str, String str2, int i, long j, long j2, Map map) {
        ew.a a2 = a(ez.USAGES, str);
        a2.x = str2;
        a2.y = Integer.valueOf(i);
        a2.z = Long.valueOf(j);
        a2.A = Long.valueOf(j2);
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                a2.w.add(new fa((String) entry.getKey(), (Long) entry.getValue()));
            }
        }
        a(a2);
    }

    public final ew.a a(ez ezVar, String str) {
        fc b2 = this.f4446a.b();
        ew.a aVar = new ew.a();
        aVar.g = hd.f4456a;
        aVar.c = ezVar;
        aVar.d = str;
        if (v.c()) {
            aVar.e = Long.valueOf(v.b());
            aVar.f = Long.valueOf(System.currentTimeMillis());
        } else {
            aVar.e = Long.valueOf(System.currentTimeMillis());
            aVar.h = Long.valueOf(SystemClock.elapsedRealtime());
        }
        aVar.j = b2.d;
        aVar.k = b2.e;
        aVar.l = b2.f;
        return aVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:12|13|15|16|17|18|19|20) */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0046 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004e */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006a A[SYNTHETIC, Splitter:B:37:0x006a] */
    public final synchronized void a(ew.a aVar) {
        if (aVar.c != ez.USAGES) {
            int i = this.d;
            this.d = i + 1;
            aVar.n = Integer.valueOf(i);
            if (this.e.c != null) {
                aVar.o = this.e.b();
            }
            this.e.c = aVar.c;
            this.e.d = aVar.d;
            this.e.e = aVar.t;
        }
        gy gyVar = this.b;
        ew b2 = aVar.b();
        try {
            hm hmVar = gyVar.f4444a;
            synchronized (hmVar.f4468a) {
                hmVar.b.add(b2);
                hmVar.a();
                hmVar.b.add(b2);
            }
            if (gyVar.b == null) {
                if (!gx.f4443a) {
                    if (b2.n == ez.CUSTOM) {
                        gyVar.a(false);
                        return;
                    }
                }
                gyVar.a(true);
                return;
            }
            gyVar.f4444a.flush();
        } catch (Exception unused) {
        }
    }
}
