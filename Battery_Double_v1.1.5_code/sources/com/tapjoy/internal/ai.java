package com.tapjoy.internal;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;

public class ai extends aj {

    /* renamed from: a reason: collision with root package name */
    private int f4310a = 0;
    private final Matrix b = new Matrix();
    private final float[] c = new float[2];

    public ai(Context context) {
        super(context);
    }

    public int getRotationCount() {
        return this.f4310a;
    }

    public void setRotationCount(int i) {
        this.f4310a = i & 3;
    }

    public void onMeasure(int i, int i2) {
        if (this.f4310a % 2 == 0) {
            super.onMeasure(i, i2);
            return;
        }
        super.onMeasure(i2, i);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(14:4|5|6|7|8|(1:12)|13|14|15|17|23|24|30|31) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x002c */
    public void dispatchDraw(Canvas canvas) {
        if (this.f4310a == 0) {
            super.dispatchDraw(canvas);
            return;
        }
        canvas.save();
        int width = getWidth();
        int height = getHeight();
        canvas.clipRect(0, 0, width, height);
        try {
            ViewGroup viewGroup = (ViewGroup) getParent();
            ViewGroup viewGroup2 = (ViewGroup) viewGroup.getParent();
            if ((viewGroup2 instanceof ScrollView) || (viewGroup2 instanceof HorizontalScrollView)) {
                viewGroup = viewGroup2;
            }
            int left = getLeft() - viewGroup.getScrollX();
            int top = getTop() - viewGroup.getScrollY();
            canvas.clipRect(0 - left, 0 - top, viewGroup.getWidth() - left, viewGroup.getHeight() - top);
        } catch (Exception unused) {
        }
        canvas.rotate((float) (this.f4310a * 90));
        switch (this.f4310a) {
            case 1:
                canvas.translate(0.0f, (float) (-width));
                break;
            case 2:
                canvas.translate((float) (-width), (float) (-height));
                break;
            case 3:
                canvas.translate((float) (-height), 0.0f);
                break;
            default:
                throw new IllegalStateException();
        }
        this.b.setRotate((float) (this.f4310a * -90));
        switch (this.f4310a) {
            case 1:
                this.b.postTranslate(0.0f, (float) (width - 1));
                break;
            case 2:
                this.b.postTranslate((float) (width - 1), (float) (height - 1));
                break;
            case 3:
                this.b.postTranslate((float) (height - 1), 0.0f);
                break;
            default:
                throw new IllegalStateException();
        }
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.f4310a == 0) {
            return super.dispatchTouchEvent(motionEvent);
        }
        float[] fArr = this.c;
        fArr[0] = motionEvent.getX();
        fArr[1] = motionEvent.getY();
        this.b.mapPoints(fArr);
        motionEvent.setLocation(fArr[0], fArr[1]);
        return super.dispatchTouchEvent(motionEvent);
    }
}
