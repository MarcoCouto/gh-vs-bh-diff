package com.tapjoy.internal;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class gi extends gh {
    private final ThreadPoolExecutor b;

    class a implements Runnable {
        private int b;
        private long c;
        private String d;
        private String e;
        private Map f;

        a(int i, long j, String str, String str2, Map map) {
            this.b = i;
            this.c = j;
            this.d = str;
            this.e = str2;
            this.f = map;
        }

        public final void run() {
            try {
                switch (this.b) {
                    case 1:
                        gi.super.a(this.c);
                        return;
                    case 2:
                        gi.super.a();
                        return;
                    case 3:
                        gi.super.a(this.c, this.d, this.e, this.f);
                        break;
                }
            } catch (Throwable unused) {
                gi.super.a();
            }
        }
    }

    public gi(File file, gz gzVar) {
        super(file, gzVar);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, 1, TimeUnit.SECONDS, new LinkedBlockingQueue());
        this.b = threadPoolExecutor;
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        try {
            this.b.shutdown();
            this.b.awaitTermination(1, TimeUnit.SECONDS);
        } finally {
            super.finalize();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(long j) {
        try {
            ThreadPoolExecutor threadPoolExecutor = this.b;
            a aVar = new a(1, j, null, null, null);
            threadPoolExecutor.execute(aVar);
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        try {
            ThreadPoolExecutor threadPoolExecutor = this.b;
            a aVar = new a(2, 0, null, null, null);
            threadPoolExecutor.execute(aVar);
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: protected */
    public final void a(long j, String str, String str2, Map map) {
        try {
            ThreadPoolExecutor threadPoolExecutor = this.b;
            a aVar = new a(3, j, str, str2, map != null ? new HashMap(map) : null);
            threadPoolExecutor.execute(aVar);
        } catch (Throwable unused) {
        }
    }
}
