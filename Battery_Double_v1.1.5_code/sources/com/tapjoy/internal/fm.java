package com.tapjoy.internal;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import com.tapjoy.TapjoyLog;
import com.tapjoy.TapjoyUtil;
import java.util.HashSet;
import java.util.concurrent.CountDownLatch;

public final class fm {

    /* renamed from: a reason: collision with root package name */
    private static final fm f4402a = new fm();
    /* access modifiers changed from: private */
    public Application b;
    private ActivityLifecycleCallbacks c;
    private final HashSet d = new HashSet();

    public static void a(Context context) {
        if (VERSION.SDK_INT >= 14 && context != null) {
            fm fmVar = f4402a;
            Context applicationContext = context.getApplicationContext();
            if (fmVar.b == null) {
                try {
                    if (applicationContext instanceof Application) {
                        fmVar.b = (Application) applicationContext;
                    } else {
                        final CountDownLatch countDownLatch = new CountDownLatch(1);
                        TapjoyUtil.runOnMainThread(new Runnable() {
                            public final void run() {
                                try {
                                    fm.this.b = fm.b();
                                } catch (Exception e) {
                                    TapjoyLog.w("Tapjoy.ActivityTracker", Log.getStackTraceString(e));
                                } catch (Throwable th) {
                                    countDownLatch.countDown();
                                    throw th;
                                }
                                countDownLatch.countDown();
                            }
                        });
                        countDownLatch.await();
                    }
                } catch (Exception e) {
                    TapjoyLog.w("Tapjoy.ActivityTracker", Log.getStackTraceString(e));
                }
                if (fmVar.b == null) {
                    return;
                }
            }
            synchronized (fmVar) {
                if (fmVar.c == null) {
                    Activity c2 = b.c();
                    if (c2 != null) {
                        fmVar.d.add(b(c2));
                    }
                    final HashSet hashSet = fmVar.d;
                    fmVar.c = new ActivityLifecycleCallbacks() {
                        public final void onActivityCreated(Activity activity, Bundle bundle) {
                        }

                        public final void onActivityDestroyed(Activity activity) {
                        }

                        public final void onActivityPaused(Activity activity) {
                        }

                        public final void onActivityResumed(Activity activity) {
                        }

                        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                        }

                        public final void onActivityStarted(Activity activity) {
                            hashSet.add(fm.b(activity));
                            if (hashSet.size() == 1) {
                                go.a();
                            }
                            b.a(activity);
                        }

                        public final void onActivityStopped(Activity activity) {
                            hashSet.remove(fm.b(activity));
                            if (hashSet.size() <= 0) {
                                go.b();
                            }
                        }
                    };
                    fmVar.b.registerActivityLifecycleCallbacks(fmVar.c);
                    go.a();
                }
            }
        }
    }

    public static void a() {
        if (VERSION.SDK_INT >= 14) {
            fm fmVar = f4402a;
            if (fmVar.b != null) {
                synchronized (fmVar) {
                    if (fmVar.c != null) {
                        fmVar.b.unregisterActivityLifecycleCallbacks(fmVar.c);
                        fmVar.c = null;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static String b(Activity activity) {
        StringBuilder sb = new StringBuilder();
        sb.append(activity.getClass().getName());
        sb.append("@");
        sb.append(System.identityHashCode(activity));
        return sb.toString();
    }

    static /* synthetic */ Application b() {
        return (Application) Class.forName("android.app.ActivityThread").getMethod("currentApplication", new Class[0]).invoke(null, null);
    }
}
