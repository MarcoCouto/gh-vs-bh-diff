package com.tapjoy.internal;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewParent;
import com.tapjoy.internal.di.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class dw implements a {

    /* renamed from: a reason: collision with root package name */
    public static Handler f4372a = new Handler(Looper.getMainLooper());
    private static dw c = new dw();
    /* access modifiers changed from: private */
    public static Handler d = null;
    /* access modifiers changed from: private */
    public static final Runnable j = new Runnable() {
        public final void run() {
            dw.b(dw.a());
        }
    };
    /* access modifiers changed from: private */
    public static final Runnable k = new Runnable() {
        public final void run() {
            if (dw.d != null) {
                dw.d.post(dw.j);
                dw.d.postDelayed(dw.k, 200);
            }
        }
    };
    public List b = new ArrayList();
    private int e;
    private dj f = new dj();
    private dx g = new dx();
    /* access modifiers changed from: private */
    public ee h = new ee(new ea());
    private double i;

    dw() {
    }

    public static dw a() {
        return c;
    }

    private void a(View view, di diVar, JSONObject jSONObject, int i2) {
        diVar.a(view, jSONObject, this, i2 == ef.f4379a);
    }

    private void g() {
        if (this.b.size() > 0) {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }

    public static void b() {
        if (d == null) {
            Handler handler = new Handler(Looper.getMainLooper());
            d = handler;
            handler.post(j);
            d.postDelayed(k, 200);
        }
    }

    public static void c() {
        if (d != null) {
            d.removeCallbacks(k);
            d = null;
        }
    }

    public final void a(View view, di diVar, JSONObject jSONObject) {
        String str;
        boolean z;
        if (dq.c(view)) {
            dx dxVar = this.g;
            int i2 = dxVar.c.contains(view) ? ef.f4379a : dxVar.f ? ef.b : ef.c;
            if (i2 != ef.c) {
                JSONObject a2 = diVar.a(view);
                dm.a(jSONObject, a2);
                dx dxVar2 = this.g;
                ArrayList arrayList = null;
                if (dxVar2.f4374a.size() == 0) {
                    str = null;
                } else {
                    str = (String) dxVar2.f4374a.get(view);
                    if (str != null) {
                        dxVar2.f4374a.remove(view);
                    }
                }
                if (str != null) {
                    dm.a(a2, str);
                    this.g.f = true;
                    z = true;
                } else {
                    z = false;
                }
                if (!z) {
                    dx dxVar3 = this.g;
                    if (dxVar3.b.size() != 0) {
                        arrayList = (ArrayList) dxVar3.b.get(view);
                        if (arrayList != null) {
                            dxVar3.b.remove(view);
                            Collections.sort(arrayList);
                        }
                    }
                    if (arrayList != null) {
                        dm.a(a2, (List) arrayList);
                    }
                    a(view, diVar, a2, i2);
                }
                this.e++;
            }
        }
    }

    static /* synthetic */ void b(dw dwVar) {
        boolean z;
        dwVar.e = 0;
        dwVar.i = Cdo.a();
        dx dxVar = dwVar.g;
        dd a2 = dd.a();
        if (a2 != null) {
            for (cz czVar : Collections.unmodifiableCollection(a2.b)) {
                View c2 = czVar.c();
                if (czVar.d()) {
                    if (c2 != null) {
                        if (c2.hasWindowFocus()) {
                            HashSet hashSet = new HashSet();
                            View view = c2;
                            while (true) {
                                if (view != null) {
                                    if (!dq.c(view)) {
                                        break;
                                    }
                                    hashSet.add(view);
                                    ViewParent parent = view.getParent();
                                    view = parent instanceof View ? (View) parent : null;
                                } else {
                                    dxVar.c.addAll(hashSet);
                                    z = true;
                                    break;
                                }
                            }
                        }
                        z = false;
                        if (z) {
                            dxVar.d.add(czVar.f);
                            dxVar.f4374a.put(c2, czVar.f);
                            dxVar.a(czVar);
                        }
                    }
                    dxVar.e.add(czVar.f);
                }
            }
        }
        double a3 = Cdo.a();
        dk dkVar = dwVar.f.f4364a;
        if (dwVar.g.e.size() > 0) {
            JSONObject a4 = dkVar.a(null);
            ee eeVar = dwVar.h;
            HashSet hashSet2 = dwVar.g.e;
            ea eaVar = eeVar.f4378a;
            ec ecVar = new ec(eeVar, hashSet2, a4, a3);
            eaVar.a(ecVar);
        }
        if (dwVar.g.d.size() > 0) {
            JSONObject a5 = dkVar.a(null);
            dwVar.a(null, dkVar, a5, ef.f4379a);
            dm.a(a5);
            ee eeVar2 = dwVar.h;
            HashSet hashSet3 = dwVar.g.d;
            ea eaVar2 = eeVar2.f4378a;
            ed edVar = new ed(eeVar2, hashSet3, a5, a3);
            eaVar2.a(edVar);
        } else {
            dwVar.h.b();
        }
        dx dxVar2 = dwVar.g;
        dxVar2.f4374a.clear();
        dxVar2.b.clear();
        dxVar2.c.clear();
        dxVar2.d.clear();
        dxVar2.e.clear();
        dxVar2.f = false;
        Cdo.a();
        dwVar.g();
    }
}
