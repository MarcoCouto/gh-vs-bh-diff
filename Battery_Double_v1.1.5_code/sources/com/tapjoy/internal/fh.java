package com.tapjoy.internal;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.List;

public final class fh extends ei {
    public static final ek c = new b();
    public final List d;

    public static final class a extends com.tapjoy.internal.ei.a {
        public List c = ep.a();

        public final fh b() {
            return new fh(this.c, super.a());
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            fh fhVar = (fh) obj;
            return ek.p.a().a(1, (Object) fhVar.d) + fhVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            fh fhVar = (fh) obj;
            ek.p.a().a(emVar, 1, fhVar.d);
            emVar.a(fhVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, fh.class);
        }

        public final /* synthetic */ Object a(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b == -1) {
                    elVar.a(a2);
                    return aVar.b();
                } else if (b != 1) {
                    eh c = elVar.c();
                    aVar.a(b, c, c.a().a(elVar));
                } else {
                    aVar.c.add(ek.p.a(elVar));
                }
            }
        }
    }

    public fh(List list) {
        this(list, iv.b);
    }

    public fh(List list, iv ivVar) {
        super(c, ivVar);
        this.d = ep.a(MessengerShareContentUtility.ELEMENTS, list);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fh)) {
            return false;
        }
        fh fhVar = (fh) obj;
        return a().equals(fhVar.a()) && this.d.equals(fhVar.d);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int hashCode = (a().hashCode() * 37) + this.d.hashCode();
        this.b = hashCode;
        return hashCode;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (!this.d.isEmpty()) {
            sb.append(", elements=");
            sb.append(this.d);
        }
        StringBuilder replace = sb.replace(0, 2, "StringList{");
        replace.append('}');
        return replace.toString();
    }
}
