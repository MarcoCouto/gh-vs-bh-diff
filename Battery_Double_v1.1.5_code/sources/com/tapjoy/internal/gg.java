package com.tapjoy.internal;

import android.os.SystemClock;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.Nullable;

public abstract class gg {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    static Set f4431a = null;
    private static final ThreadLocal b = new ThreadLocal() {
        /* access modifiers changed from: protected */
        public final /* synthetic */ Object initialValue() {
            return new HashMap();
        }
    };
    private static gg c;
    private static volatile boolean d = false;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        final String f4432a;
        private final TreeMap b = new TreeMap();
        private final Map c = new HashMap();
        private volatile long d;

        a(String str) {
            this.f4432a = str;
        }

        public final a a() {
            try {
                this.d = SystemClock.elapsedRealtime();
            } catch (NullPointerException unused) {
                this.d = -1;
            }
            return this;
        }

        public final a b() {
            long j = this.d;
            if (j != -1) {
                try {
                    a("spent_time", SystemClock.elapsedRealtime() - j);
                } catch (NullPointerException unused) {
                }
            }
            return this;
        }

        public final a a(String str, Object obj) {
            this.b.put(str, obj);
            return this;
        }

        public final a a(Map map) {
            if (map != null) {
                this.b.putAll(map);
            }
            return this;
        }

        public final a a(String str) {
            this.b.put("failure", str);
            return this;
        }

        public final a b(String str) {
            this.b.put("misuse", str);
            return this;
        }

        public final a a(String str, long j) {
            this.c.put(str, Long.valueOf(j));
            return this;
        }

        public final a b(Map map) {
            if (map != null) {
                this.c.putAll(map);
            }
            return this;
        }

        public final void c() {
            String str = this.f4432a;
            Map map = null;
            String a2 = this.b.size() > 0 ? bh.a((Object) this.b) : null;
            if (this.c.size() > 0) {
                map = this.c;
            }
            gg.b(str, a2, map);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void a(long j);

    /* access modifiers changed from: protected */
    public abstract void a(long j, String str, @Nullable String str2, @Nullable Map map);

    public static void a(gi giVar) {
        if (c == null) {
            c = giVar;
            if (d) {
                giVar.a(v.b());
            }
        }
    }

    public static void a(boolean z) {
        if (d != z) {
            d = z;
            if (c != null) {
                if (z) {
                    c.a(v.b());
                    return;
                }
                c.a();
            }
        }
    }

    public static void a(Collection collection) {
        if (collection == null || collection.isEmpty()) {
            f4431a = null;
        } else {
            f4431a = new HashSet(collection);
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, @Nullable String str2, @Nullable Map map) {
        Set set = f4431a;
        if ((set == null || !set.contains(str)) && d && c != null) {
            c.a(v.b(), str, str2, map);
        }
    }

    public static void a(String str, @Nullable TreeMap treeMap, @Nullable Map map) {
        b(str, treeMap != null ? bh.a((Object) treeMap) : null, map);
    }

    public static a a(String str) {
        a a2 = new a(str).a();
        ((Map) b.get()).put(str, a2);
        return a2;
    }

    public static a b(String str) {
        a aVar = (a) ((Map) b.get()).remove(str);
        return aVar != null ? aVar.b() : new a(str);
    }

    public static a c(String str) {
        return (a) ((Map) b.get()).get(str);
    }

    public static a d(String str) {
        return (a) ((Map) b.get()).remove(str);
    }

    public static void a(String str, a aVar) {
        if (aVar == null) {
            new Object[1][0] = str;
        } else if (str.equals(aVar.f4432a)) {
            ((Map) b.get()).put(str, aVar);
        } else {
            Object[] objArr = {str, aVar.f4432a};
        }
    }

    public static a e(String str) {
        return new a(str);
    }
}
