package com.tapjoy.internal;

import android.app.Dialog;
import android.content.Context;

public final class c extends Dialog {

    /* renamed from: a reason: collision with root package name */
    private boolean f4336a = false;

    public c(Context context) {
        super(context, 16973835);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawableResource(17170445);
    }

    public final void show() {
        this.f4336a = false;
        super.show();
    }

    public final void cancel() {
        this.f4336a = true;
        super.cancel();
    }
}
