package com.tapjoy.internal;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.Html;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyReceiver;
import com.tapjoy.internal.jj.b;
import com.tapjoy.internal.jj.c;
import com.tapjoy.internal.jj.d;
import javax.annotation.Nullable;

public final class hc extends p {
    private static hc c;

    public static synchronized hc b(Context context) {
        hc hcVar;
        synchronized (hc.class) {
            if (c == null) {
                c = new hc(context);
            }
            hcVar = c;
        }
        return hcVar;
    }

    private hc(Context context) {
        super(context, new r() {
            public final String a(Context context) {
                return hk.a(context).b.getString("gcm.senderIds", "");
            }

            public final void a(Context context, String str) {
                n.a(hk.a(context).b, "gcm.senderIds", str);
            }

            public final String b(Context context) {
                return hk.a(context).b.getString("gcm.regId", "");
            }

            public final void b(Context context, String str) {
                n.a(hk.a(context).b, "gcm.regId", str);
            }

            public final boolean c(Context context) {
                return hk.a(context).b.getBoolean("gcm.stale", true);
            }

            public final void a(Context context, boolean z) {
                n.a(hk.a(context).b, "gcm.stale", z);
            }

            public final int d(Context context) {
                return hk.a(context).b.getInt("gcm.appVersion", Integer.MIN_VALUE);
            }

            public final void a(Context context, int i) {
                n.a(hk.a(context).b, "gcm.appVersion", i);
            }

            public final boolean e(Context context) {
                return hk.a(context).b.getBoolean("gcm.onServer", false);
            }

            public final void b(Context context, boolean z) {
                hk.a(context).a(z);
            }

            public final long f(Context context) {
                return hk.a(context).b.getLong("gcm.onServerExpirationTime", 0);
            }

            public final void a(Context context, long j) {
                Editor edit = hk.a(context).b.edit();
                edit.putLong("gcm.onServerExpirationTime", j);
                edit.apply();
            }

            public final int g(Context context) {
                return hk.a(context).b.getInt("gcm.backoff", 0);
            }

            public final void b(Context context, int i) {
                n.a(hk.a(context).b, "gcm.backoff", i);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void e(String str) {
        if (str != null && str.length() > 0) {
            String[] strArr = {str};
            super.a(this.f4542a);
            super.a(strArr[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, String str) {
        new Object[1][0] = str;
        ha.a(context).a(str);
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        new Object[1][0] = str;
        a();
    }

    /* access modifiers changed from: protected */
    public final boolean a(Context context, Intent intent) {
        Context context2 = context;
        Intent intent2 = intent;
        Object[] objArr = {intent2, intent.getExtras()};
        String stringExtra = intent2.getStringExtra("fiverocks");
        if (stringExtra == null) {
            return false;
        }
        if (hd.a(context).f()) {
            ha.a(context).b(stringExtra);
            return true;
        }
        String stringExtra2 = intent2.getStringExtra("title");
        String stringExtra3 = intent2.getStringExtra("message");
        if (stringExtra3 != null) {
            Bundle extras = intent.getExtras();
            Object obj = extras.get("rich");
            Object obj2 = extras.get("sound");
            String string = extras.getString(MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
            Object obj3 = extras.get("always");
            boolean z = "true".equals(obj3) || Boolean.TRUE.equals(obj3);
            Object obj4 = extras.get("repeatable");
            boolean z2 = "true".equals(obj4) || Boolean.TRUE.equals(obj4);
            String string2 = extras.getString("placement");
            int b = b(extras.get("nid"));
            String string3 = extras.getString("channel_id");
            if (z || !ha.a(context).d()) {
                Notification a2 = a(context, stringExtra, jr.a(stringExtra2), stringExtra3, a(obj), a(obj2), string, string2, b, string3);
                if (ha.a(context).a(context2, stringExtra, z2)) {
                    a(context2, b, a2);
                }
            }
        }
        return true;
    }

    public static boolean a(Object obj) {
        return Boolean.TRUE.equals(obj) || "true".equals(obj);
    }

    public static int b(Object obj) {
        if (obj instanceof Number) {
            return ((Number) obj).intValue();
        }
        if (obj instanceof String) {
            try {
                return Integer.parseInt((String) obj);
            } catch (NumberFormatException unused) {
            }
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        new Object[1][0] = Integer.valueOf(i);
    }

    /* access modifiers changed from: protected */
    public final boolean c(String str) {
        new Object[1][0] = str;
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean d(String str) {
        new Object[1][0] = str;
        return false;
    }

    public static void a(Context context, int i, Notification notification) {
        ((NotificationManager) context.getSystemService("notification")).notify(i, notification);
    }

    private static int a(Bundle bundle, String str, Context context) {
        if (bundle != null) {
            Object obj = bundle.get(str);
            if (obj instanceof Integer) {
                int intValue = ((Integer) obj).intValue();
                try {
                    if ("drawable".equals(context.getResources().getResourceTypeName(intValue))) {
                        return intValue;
                    }
                } catch (NotFoundException unused) {
                }
            }
            if (obj != null) {
                String str2 = "meta-data of {} invalid";
                Object[] objArr = {str};
                if (gx.f4443a) {
                    x.a(4, "Tapjoy", str2, objArr);
                }
            }
        }
        return 0;
    }

    /* JADX WARNING: type inference failed for: r6v2, types: [android.text.Spanned] */
    /* JADX WARNING: type inference failed for: r5v2, types: [android.text.Spanned] */
    /* JADX WARNING: type inference failed for: r5v3, types: [java.lang.CharSequence] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 3 */
    @Nullable
    public static Notification a(Context context, String str, String str2, String str3, boolean z, boolean z2, String str4, String str5, int i, String str6) {
        Intent intent = new Intent(context.getApplicationContext(), TapjoyReceiver.class);
        intent.setAction("com.tapjoy.PUSH_CLICK");
        intent.putExtra("com.tapjoy.PUSH_ID", str);
        if (str4 != null) {
            intent.putExtra(Tapjoy.INTENT_EXTRA_PUSH_PAYLOAD, str4);
        }
        if (str5 != null) {
            intent.putExtra("com.tapjoy.PUSH_PLACEMENT", str5);
        }
        int i2 = 134217728;
        if (VERSION.SDK_INT == 19) {
            i2 = 268435456;
        }
        PendingIntent broadcast = PendingIntent.getBroadcast(context.getApplicationContext(), i, intent, i2);
        String str7 = null;
        if (broadcast == null) {
            return null;
        }
        PackageManager packageManager = context.getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
            if (str2.length() == 0) {
                str2 = packageManager.getApplicationLabel(applicationInfo);
            } else if (z) {
                str2 = Html.fromHtml(str2);
            }
            if (z) {
                str3 = Html.fromHtml(str3);
            }
            int a2 = a(applicationInfo.metaData, "com.tapjoy.notification.icon", context);
            if (a2 == 0) {
                a2 = applicationInfo.icon != 0 ? applicationInfo.icon : 17301651;
            }
            int a3 = a(applicationInfo.metaData, "com.tapjoy.notification.icon.large", context);
            Bitmap decodeResource = a3 != 0 ? BitmapFactory.decodeResource(context.getResources(), a3) : null;
            if (str6 == null && applicationInfo.metaData != null) {
                str6 = applicationInfo.metaData.getString("com.tapjoy.notification.default_channel_id", "tapjoy");
            }
            if (VERSION.SDK_INT >= 26) {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                if (str6 == null || notificationManager.getNotificationChannel(str6) == null) {
                    notificationManager.createNotificationChannel(new NotificationChannel("tapjoy", "Tapjoy", 3));
                    str7 = "tapjoy";
                } else {
                    str7 = str6;
                }
            }
            c a4 = new c(context, str7).a(a2).c(str2).a((CharSequence) str2).b(str3).a(broadcast).a().c().a((d) new b().a((CharSequence) str2).b(str3));
            if (z2) {
                a4.b();
            }
            if (decodeResource != null) {
                a4.a(decodeResource);
            }
            return a4.d();
        } catch (NameNotFoundException unused) {
            return null;
        }
    }
}
