package com.tapjoy.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class k extends m {
    private final int c;

    public k(SharedPreferences sharedPreferences, String str, int i) {
        super(sharedPreferences, str);
        this.c = i;
    }

    public final Integer a() {
        return Integer.valueOf(b());
    }

    public final int b() {
        return this.f4541a.getInt(this.b, this.c);
    }

    public final void a(Integer num) {
        if (num != null) {
            a(num.intValue());
        } else {
            c();
        }
    }

    public final void a(int i) {
        this.f4541a.edit().putInt(this.b, i).apply();
    }

    public final Editor a(Editor editor, int i) {
        return editor.putInt(this.b, i);
    }
}
