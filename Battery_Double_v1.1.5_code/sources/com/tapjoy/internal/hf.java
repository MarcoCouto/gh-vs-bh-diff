package com.tapjoy.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Window;
import android.view.WindowManager.BadTokenException;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.tapjoy.TJContentActivity;
import com.tapjoy.TJContentActivity.AbstractContentProducer;
import com.tapjoy.TapjoyErrorMessage;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.TapjoyLog;
import com.tapjoy.internal.ir.a;

public class hf extends hh {
    private static final String h = "hf";
    /* access modifiers changed from: private */
    public static hf i;

    /* renamed from: a reason: collision with root package name */
    final String f4458a;
    final hv b;
    /* access modifiers changed from: private */
    public final ha j;
    /* access modifiers changed from: private */
    public c k;
    private boolean l;
    /* access modifiers changed from: private */
    public long m;
    private Context n;
    /* access modifiers changed from: private */
    public boolean o = false;

    public static void a() {
        hf hfVar = i;
        if (hfVar != null) {
            hfVar.e();
        }
    }

    public hf(ha haVar, String str, hv hvVar, Context context) {
        this.j = haVar;
        this.f4458a = str;
        this.b = hvVar;
        this.n = context;
    }

    public final void b() {
        hv hvVar = this.b;
        if (hvVar.f4482a != null) {
            hvVar.f4482a.b();
        }
        if (hvVar.b != null) {
            hvVar.b.b();
        }
        hvVar.c.b();
        if (hvVar.e != null) {
            hvVar.e.b();
        }
        if (hvVar.f != null) {
            hvVar.f.b();
        }
        if (hvVar.m != null && hvVar.m.f4483a != null) {
            hvVar.m.f4483a.b();
        }
    }

    public final boolean c() {
        hv hvVar = this.b;
        return (hvVar.c == null || hvVar.c.b == null || (hvVar.m != null && hvVar.m.f4483a != null && hvVar.m.f4483a.b == null) || ((hvVar.b == null || hvVar.f == null || hvVar.b.b == null || hvVar.f.b == null) && (hvVar.f4482a == null || hvVar.e == null || hvVar.f4482a.b == null || hvVar.e.b == null))) ? false : true;
    }

    public final void a(final hb hbVar, final fx fxVar) {
        Activity a2 = a.a(this.n);
        if (a2 != null && !a2.isFinishing()) {
            try {
                a(a2, hbVar, fxVar);
                new Object[1][0] = this.f4458a;
                return;
            } catch (BadTokenException unused) {
            }
        }
        Activity a3 = gs.a();
        try {
            TJContentActivity.start(ha.a().e, new AbstractContentProducer() {
                public final void show(Activity activity) {
                    try {
                        hf.this.a(activity, hbVar, fxVar);
                    } catch (BadTokenException unused) {
                        gx.b("Failed to show the content for \"{}\" caused by invalid activity", hf.this.f4458a);
                        hbVar.a(hf.this.f4458a, hf.this.f, null);
                    }
                }

                public final void dismiss(Activity activity) {
                    hf.this.e();
                }
            }, (a3 == null || (a3.getWindow().getAttributes().flags & 1024) == 0) ? false : true);
            new Object[1][0] = this.f4458a;
        } catch (ActivityNotFoundException unused2) {
            if (a3 != null && !a3.isFinishing()) {
                try {
                    a(a3, hbVar, fxVar);
                    new Object[1][0] = this.f4458a;
                    return;
                } catch (BadTokenException unused3) {
                    gx.b("Failed to show the content for \"{}\" caused by no registration of TJContentActivity", this.f4458a);
                    hbVar.a(this.f4458a, this.f, null);
                }
            }
            gx.b("Failed to show the content for \"{}\" caused by no registration of TJContentActivity", this.f4458a);
            hbVar.a(this.f4458a, this.f, null);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x009d  */
    public void a(final Activity activity, final hb hbVar, fx fxVar) {
        boolean z;
        if (this.l) {
            TapjoyLog.e(h, new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, "Content is already displayed"));
            return;
        }
        this.l = true;
        i = this;
        this.g = fxVar.f4413a;
        this.k = new c(activity);
        this.k.setOnCancelListener(new OnCancelListener() {
            public final void onCancel(DialogInterface dialogInterface) {
                hbVar.d(hf.this.f4458a);
            }
        });
        this.k.setOnDismissListener(new OnDismissListener() {
            public final void onDismiss(DialogInterface dialogInterface) {
                String str;
                hf.i = null;
                hh.a((Context) activity, hf.this.b.g);
                hf.this.j.a(hf.this.b.k, SystemClock.elapsedRealtime() - hf.this.m);
                if (!hf.this.d) {
                    hbVar.a(hf.this.f4458a, hf.this.f, hf.this.b.h);
                }
                if (hf.this.o && hf.this.b.k != null && hf.this.b.k.containsKey("action_id")) {
                    String obj = hf.this.b.k.get("action_id").toString();
                    if (obj != null && obj.length() > 0) {
                        ha c2 = hf.this.j;
                        if (c2.b != null) {
                            hj hjVar = c2.b;
                            String a2 = hj.a();
                            String a3 = hjVar.b.a();
                            String a4 = hjVar.f4464a.a();
                            if (a4 == null || !a2.equals(a4)) {
                                hjVar.f4464a.a(a2);
                                str = "";
                            } else {
                                str = a3;
                            }
                            if (!(str.length() == 0)) {
                                if (!str.contains(obj)) {
                                    StringBuilder sb = new StringBuilder(",");
                                    sb.append(obj);
                                    obj = str.concat(sb.toString());
                                } else {
                                    obj = str;
                                }
                            }
                            hjVar.b.a(obj);
                        }
                    }
                }
                if (activity instanceof TJContentActivity) {
                    activity.finish();
                }
            }
        });
        this.k.setCanceledOnTouchOutside(false);
        iq iqVar = new iq(activity, this.b, new ir(activity, this.b, new a() {
            public final void a() {
                hf.this.k.cancel();
            }

            public final void a(ht htVar) {
                if (hf.this.g instanceof fv) {
                    fv fvVar = (fv) hf.this.g;
                    if (!(fvVar == null || fvVar.c == null)) {
                        fvVar.c.a();
                    }
                }
                hf.this.j.a(hf.this.b.k, htVar.b);
                hh.a((Context) activity, htVar.d);
                if (!jr.c(htVar.e)) {
                    hf.this.e.a(activity, htVar.e, jr.b(htVar.f));
                    hf.this.d = true;
                }
                hbVar.a(hf.this.f4458a, htVar.g);
                if (htVar.c) {
                    hf.this.k.dismiss();
                }
            }

            public final void b() {
                hf.this.o = !hf.this.o;
            }
        }));
        FrameLayout frameLayout = new FrameLayout(activity);
        frameLayout.addView(iqVar, new LayoutParams(-2, -2, 17));
        this.k.setContentView(frameLayout);
        if (Boolean.FALSE.booleanValue()) {
            Window window = this.k.getWindow();
            if (VERSION.SDK_INT == 16 && "4.1.2".equals(VERSION.RELEASE)) {
                if (Boolean.FALSE.equals(a(window.getContext()))) {
                    z = false;
                    if (z) {
                        int i2 = ad.a.b;
                        ae aeVar = new ae();
                        switch (com.tapjoy.internal.ad.AnonymousClass1.f4305a[i2 - 1]) {
                            case 1:
                                ag agVar = new ag();
                                agVar.f4308a = false;
                                agVar.b = 60.0f;
                                aeVar.a(agVar.a()).a(new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f)).a(new ah().a(1.0f).b(0.3f).a());
                                break;
                            case 2:
                                ag agVar2 = new ag();
                                agVar2.f4308a = false;
                                agVar2.b = -60.0f;
                                aeVar.a(agVar2.a()).a(new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f)).a(new ah().a(-0.4f).b(0.3f).a());
                                break;
                            case 3:
                                ag agVar3 = new ag();
                                agVar3.f4308a = true;
                                agVar3.b = -60.0f;
                                aeVar.a(agVar3.a()).a(new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f)).a(new ah().a(0.3f).b(1.0f).a());
                                break;
                            case 4:
                                ag agVar4 = new ag();
                                agVar4.f4308a = true;
                                agVar4.b = 60.0f;
                                aeVar.a(agVar4.a()).a(new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f)).a(new ah().a(0.3f).b(-0.4f).a());
                                break;
                        }
                        iqVar.startAnimation(aeVar.b().a());
                    }
                } else {
                    window.setFlags(16777216, 16777216);
                }
            }
            z = true;
            if (z) {
            }
        }
        try {
            this.k.show();
            this.k.getWindow().setLayout(-1, -1);
            if ((activity.getWindow().getAttributes().flags & 1024) != 0) {
                this.k.getWindow().setFlags(1024, 1024);
            }
            this.m = SystemClock.elapsedRealtime();
            this.j.a(this.b.k);
            fxVar.a();
            fr frVar = this.g;
            if (frVar != null) {
                frVar.b();
            }
            hbVar.c(this.f4458a);
        } catch (BadTokenException e) {
            throw e;
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.k != null) {
            this.k.dismiss();
        }
    }

    private static Boolean a(Context context) {
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle != null) {
                Object obj = bundle.get("tapjoy:hardwareAccelerated");
                if (obj instanceof Boolean) {
                    return (Boolean) obj;
                }
            }
        } catch (NameNotFoundException unused) {
        }
        return null;
    }
}
