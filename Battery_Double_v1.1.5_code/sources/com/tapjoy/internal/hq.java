package com.tapjoy.internal;

import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import com.unity3d.ads.metadata.InAppPurchaseMetaData;
import com.unity3d.services.purchasing.core.TransactionDetailsUtilities;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public final class hq {
    public static String a(fb fbVar) {
        bh b = new bh().c().a("sdk").b(fbVar.t).a("os_name").b(fbVar.k).a("os_ver").b(fbVar.l).a("device_id").b(fbVar.h).a("device_maker").b(fbVar.i).a("device_model").b(fbVar.j).a(TapjoyConstants.TJC_PACKAGE_ID).b(fbVar.r).a(TapjoyConstants.TJC_PACKAGE_SIGN).b(fbVar.s).a("locale").b(fbVar.p).a(TapjoyConstants.TJC_DEVICE_TIMEZONE).b(fbVar.q);
        if (fbVar.m != null) {
            b.a(TapjoyConstants.TJC_DEVICE_DISPLAY_DENSITY).a((Number) fbVar.m);
        }
        if (fbVar.n != null) {
            b.a(TapjoyConstants.TJC_DEVICE_DISPLAY_WIDTH).a((Number) fbVar.n);
        }
        if (fbVar.o != null) {
            b.a(TapjoyConstants.TJC_DEVICE_DISPLAY_HEIGHT).a((Number) fbVar.o);
        }
        if (fbVar.g != null) {
            b.a("mac").b(fbVar.g);
        }
        if (fbVar.u != null) {
            b.a(TapjoyConstants.TJC_DEVICE_COUNTRY_SIM).b(fbVar.u);
        }
        if (fbVar.v != null) {
            b.a("country_net").b(fbVar.v);
        }
        if (fbVar.w != null) {
            b.a("imei").b(fbVar.w);
        }
        if (fbVar.x != null) {
            b.a(TapjoyConstants.TJC_ANDROID_ID).b(fbVar.x);
        }
        return b.d().toString();
    }

    public static String a(ev evVar) {
        bh c = new bh().c();
        if (evVar.e != null) {
            c.a(TapjoyConstants.TJC_PACKAGE_VERSION).b(evVar.e);
        }
        if (evVar.f != null) {
            c.a(TapjoyConstants.TJC_PACKAGE_REVISION).a((Number) evVar.f);
        }
        if (evVar.g != null) {
            c.a("data_ver").b(evVar.g);
        }
        if (evVar.h != null) {
            c.a(TapjoyConstants.TJC_INSTALLER).b(evVar.h);
        }
        if (evVar.i != null) {
            c.a("store").b(evVar.i);
        }
        return c.d().toString();
    }

    public static String a(fi fiVar) {
        return a(fiVar, null);
    }

    private static String a(fi fiVar, ew ewVar) {
        bh c = new bh().c();
        if (fiVar.s != null) {
            c.a("installed").a((Number) fiVar.s);
        }
        if (fiVar.t != null) {
            c.a(TapjoyConstants.TJC_REFERRER).b(fiVar.t);
        }
        if (fiVar.G != null) {
            c.a("idfa").b(fiVar.G);
            if (fiVar.H != null && fiVar.H.booleanValue()) {
                c.a("idfa_optout").a(1);
            }
        } else if (!(ewVar == null || ewVar.r == null || !hd.f4456a.equals(ewVar.r))) {
            String b = ho.b();
            if (b != null) {
                c.a("idfa").b(b);
                if (ho.c()) {
                    c.a("idfa_optout").a(1);
                }
            }
        }
        if (fiVar.u != null) {
            c.a(TapjoyConstants.TJC_USER_WEEKLY_FREQUENCY).a((long) Math.max(fiVar.u.intValue(), 1));
        }
        if (fiVar.v != null) {
            c.a(TapjoyConstants.TJC_USER_MONTHLY_FREQUENCY).a((long) Math.max(fiVar.v.intValue(), 1));
        }
        if (fiVar.w.size() > 0) {
            ArrayList arrayList = new ArrayList(fiVar.w.size());
            for (ff ffVar : fiVar.w) {
                if (ffVar.h != null) {
                    arrayList.add(ffVar.f);
                }
            }
            if (!arrayList.isEmpty()) {
                c.a("push").a();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    c.b((String) it.next());
                }
                c.b();
            }
        }
        c.a(SettingsJsonConstants.SESSION_KEY).c();
        if (fiVar.x != null) {
            c.a("total_count").a((Number) fiVar.x);
        }
        if (fiVar.y != null) {
            c.a("total_length").a((Number) fiVar.y);
        }
        if (fiVar.z != null) {
            c.a("last_at").a((Number) fiVar.z);
        }
        if (fiVar.A != null) {
            c.a("last_length").a((Number) fiVar.A);
        }
        c.d();
        c.a("purchase").c();
        if (fiVar.B != null) {
            c.a("currency").b(fiVar.B);
        }
        if (fiVar.C != null) {
            c.a("total_count").a((Number) fiVar.C);
        }
        if (fiVar.D != null) {
            c.a("total_price").a((Number) fiVar.D);
        }
        if (fiVar.E != null) {
            c.a("last_at").a((Number) fiVar.E);
        }
        if (fiVar.F != null) {
            c.a("last_price").a((Number) fiVar.F);
        }
        c.d();
        if (fiVar.I != null) {
            c.a("user_id").b(fiVar.I);
        }
        if (fiVar.J != null) {
            c.a(TapjoyConstants.TJC_USER_LEVEL).a((Number) fiVar.J);
        }
        if (fiVar.K != null) {
            c.a(TapjoyConstants.TJC_USER_FRIEND_COUNT).a((Number) fiVar.K);
        }
        if (fiVar.L != null) {
            c.a(TapjoyConstants.TJC_USER_VARIABLE_1).b(fiVar.L);
        }
        if (fiVar.M != null) {
            c.a(TapjoyConstants.TJC_USER_VARIABLE_2).b(fiVar.M);
        }
        if (fiVar.N != null) {
            c.a(TapjoyConstants.TJC_USER_VARIABLE_3).b(fiVar.N);
        }
        if (fiVar.O != null) {
            c.a(TapjoyConstants.TJC_USER_VARIABLE_4).b(fiVar.O);
        }
        if (fiVar.P != null) {
            c.a(TapjoyConstants.TJC_USER_VARIABLE_5).b(fiVar.P);
        }
        if (fiVar.Q.size() > 0) {
            c.a("tags").a((Collection) fiVar.Q);
        }
        if (Boolean.TRUE.equals(fiVar.R)) {
            c.a("push_optout").a(1);
        }
        return c.d().toString();
    }

    private static String a(ew ewVar, boolean z, boolean z2, boolean z3) {
        bh b = new bh().c().a("type").b(a(ewVar.n)).a("name").b(ewVar.o);
        b.a(LocationConst.TIME);
        if (ewVar.q != null) {
            b.a((Number) ewVar.p);
            b.a("systime").a((Number) ewVar.q);
        } else if (!v.c() || ewVar.r == null || ewVar.s == null || !hd.f4456a.equals(ewVar.r)) {
            b.a((Number) ewVar.p);
        } else {
            b.a(v.a(ewVar.s.longValue()));
            b.a("systime").a((Number) ewVar.p);
        }
        if (ewVar.t != null) {
            b.a(IronSourceConstants.EVENTS_DURATION).a((Number) ewVar.t);
        }
        if (!z && ewVar.u != null) {
            b.a(String.VIDEO_INFO).a((bl) new bm(a(ewVar.u)));
        }
        if (!z2 && ewVar.v != null) {
            b.a("app").a((bl) new bm(a(ewVar.v)));
        }
        if (!z3 && ewVar.w != null) {
            b.a("user").a((bl) new bm(a(ewVar.w, ewVar)));
        }
        if (ewVar.y != null) {
            b.a("event_seq").a((Number) ewVar.y);
        }
        if (ewVar.z != null) {
            bh a2 = b.a("event_prev");
            ey eyVar = ewVar.z;
            bh b2 = new bh().c().a("type").b(a(eyVar.e)).a("name").b(eyVar.f);
            if (eyVar.g != null) {
                b2.a("category").b(eyVar.g);
            }
            a2.a((bl) new bm(b2.d().toString()));
        }
        if (ewVar.A != null) {
            bh a3 = b.a("purchase");
            fe feVar = ewVar.A;
            bh b3 = new bh().c().a("product_id").b(feVar.h);
            if (feVar.i != null) {
                b3.a("product_quantity").a((Number) feVar.i);
            }
            if (feVar.j != null) {
                b3.a("product_price").a((Number) feVar.j);
            }
            if (feVar.k != null) {
                b3.a("product_price_currency").b(feVar.k);
            }
            if (feVar.s != null) {
                b3.a("currency_price").b(feVar.s);
            }
            if (feVar.l != null) {
                b3.a("product_type").b(feVar.l);
            }
            if (feVar.m != null) {
                b3.a("product_title").b(feVar.m);
            }
            if (feVar.n != null) {
                b3.a("product_description").b(feVar.n);
            }
            if (feVar.o != null) {
                b3.a("transaction_id").b(feVar.o);
            }
            if (feVar.p != null) {
                b3.a("transaction_state").a((Number) feVar.p);
            }
            if (feVar.q != null) {
                b3.a("transaction_date").a((Number) feVar.q);
            }
            if (feVar.r != null) {
                b3.a("campaign_id").b(feVar.r);
            }
            if (feVar.t != null) {
                b3.a(TransactionDetailsUtilities.RECEIPT).b(feVar.t);
            }
            if (feVar.u != null) {
                b3.a(InAppPurchaseMetaData.KEY_SIGNATURE).b(feVar.u);
            }
            a3.a((bl) new bm(b3.d().toString()));
        }
        if (ewVar.B != null) {
            b.a("exception").b(ewVar.B);
        }
        try {
            if (ewVar.D != null) {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                if (ewVar.C != null) {
                    bn.b(ewVar.C).a((Map) linkedHashMap);
                }
                fd fdVar = ewVar.D;
                if (fdVar.d != null) {
                    linkedHashMap.put("fq7_change", fdVar.d);
                }
                if (fdVar.e != null) {
                    linkedHashMap.put("fq30_change", fdVar.e);
                }
                if (fdVar.f != null) {
                    linkedHashMap.put(TJAdUnitConstants.PARAM_PUSH_ID, fdVar.f);
                }
                b.a("meta").a((Map) linkedHashMap);
            } else if (ewVar.C != null) {
                b.a("meta").a((bl) new bm(ewVar.C));
            }
        } catch (IOException unused) {
        }
        if (ewVar.I != null) {
            b.a(String.USAGE_TRACKER_DIMENSIONS).a((bl) new bm(ewVar.I));
        }
        if (ewVar.J != null) {
            b.a("count").a((Number) ewVar.J);
        }
        if (ewVar.K != null) {
            b.a("first_time").a((Number) ewVar.K);
        }
        if (ewVar.L != null) {
            b.a("last_time").a((Number) ewVar.L);
        }
        if (ewVar.E != null) {
            b.a("category").b(ewVar.E);
        }
        if (ewVar.F != null) {
            b.a("p1").b(ewVar.F);
        }
        if (ewVar.G != null) {
            b.a("p2").b(ewVar.G);
        }
        if (ewVar.H.size() > 0) {
            b.a(String.USAGE_TRACKER_VALUES).c();
            for (fa faVar : ewVar.H) {
                b.a(faVar.e).a((Number) faVar.f);
            }
            b.d();
        }
        return b.d().toString();
    }

    public static String a(ex exVar) {
        fb fbVar;
        boolean z;
        ev evVar;
        boolean z2;
        bh a2 = new bh().a();
        fb fbVar2 = null;
        ev evVar2 = null;
        fi fiVar = null;
        for (ew ewVar : exVar.d) {
            boolean z3 = true;
            if (fbVar2 == null || !fbVar2.equals(ewVar.u)) {
                fbVar = ewVar.u;
                z = false;
            } else {
                fbVar = fbVar2;
                z = true;
            }
            if (evVar2 == null || !evVar2.equals(ewVar.v)) {
                evVar = ewVar.v;
                z2 = false;
            } else {
                evVar = evVar2;
                z2 = true;
            }
            if (fiVar == null || !fiVar.equals(ewVar.w)) {
                fiVar = ewVar.w;
                z3 = false;
            }
            a2.a((bl) new bm(a(ewVar, z, z2, z3)));
            fbVar2 = fbVar;
            evVar2 = evVar;
        }
        return a2.b().toString();
    }

    private static String a(ez ezVar) {
        switch (ezVar) {
            case APP:
                return "app";
            case CAMPAIGN:
                return "campaign";
            case CUSTOM:
                return "custom";
            case USAGES:
                return "usages";
            default:
                throw new RuntimeException();
        }
    }
}
