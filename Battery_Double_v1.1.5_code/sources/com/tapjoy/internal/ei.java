package com.tapjoy.internal;

import java.io.IOException;
import java.io.Serializable;

public abstract class ei implements Serializable {

    /* renamed from: a reason: collision with root package name */
    transient int f4382a = 0;
    protected transient int b = 0;
    private final transient ek c;
    private final transient iv d;

    public static abstract class a {

        /* renamed from: a reason: collision with root package name */
        is f4383a;
        em b;

        protected a() {
        }

        public final a a(iv ivVar) {
            if (ivVar.c() > 0) {
                if (this.b == null) {
                    this.f4383a = new is();
                    this.b = new em(this.f4383a);
                }
                try {
                    this.b.a(ivVar);
                } catch (IOException unused) {
                    throw new AssertionError();
                }
            }
            return this;
        }

        public final a a(int i, eh ehVar, Object obj) {
            if (this.b == null) {
                this.f4383a = new is();
                this.b = new em(this.f4383a);
            }
            try {
                ehVar.a().a(this.b, i, obj);
                return this;
            } catch (IOException unused) {
                throw new AssertionError();
            }
        }

        public final iv a() {
            return this.f4383a != null ? new iv(this.f4383a.clone().g()) : iv.b;
        }
    }

    protected ei(ek ekVar, iv ivVar) {
        if (ekVar == null) {
            throw new NullPointerException("adapter == null");
        } else if (ivVar != null) {
            this.c = ekVar;
            this.d = ivVar;
        } else {
            throw new NullPointerException("unknownFields == null");
        }
    }

    public final iv a() {
        iv ivVar = this.d;
        if (ivVar != null) {
            return ivVar;
        }
        return iv.b;
    }

    public String toString() {
        return ek.c(this);
    }
}
