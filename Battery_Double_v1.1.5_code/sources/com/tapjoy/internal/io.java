package com.tapjoy.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

public final class io extends View {

    /* renamed from: a reason: collision with root package name */
    public boolean f4499a = false;
    private Bitmap b = null;
    private Rect c = null;
    private Rect d = null;
    private Rect e = null;
    private Rect f = new Rect();

    public io(Context context) {
        super(context);
    }

    public final void setImageBitmap(Bitmap bitmap) {
        this.b = bitmap;
        int width = this.b.getWidth();
        int height = this.b.getHeight();
        int i = width / 2;
        this.d = new Rect(0, 0, i, height);
        this.c = new Rect(i, 0, width, height);
        a();
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        if (this.f4499a) {
            this.e = this.c;
        } else {
            this.e = this.d;
        }
    }

    public final void onDraw(Canvas canvas) {
        if (this.e != null && this.b != null) {
            getDrawingRect(this.f);
            canvas.drawBitmap(this.b, this.e, this.f, null);
        }
    }
}
