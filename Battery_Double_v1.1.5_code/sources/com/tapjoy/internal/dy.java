package com.tapjoy.internal;

import com.tapjoy.internal.dz.b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class dy extends dz {

    /* renamed from: a reason: collision with root package name */
    protected final HashSet f4375a;
    protected final JSONObject b;
    protected final double c;

    public dy(b bVar, HashSet hashSet, JSONObject jSONObject, double d) {
        super(bVar);
        this.f4375a = new HashSet(hashSet);
        this.b = jSONObject;
        this.c = d;
    }
}
