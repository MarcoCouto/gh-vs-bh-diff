package com.tapjoy.internal;

import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import java.util.Arrays;
import javax.annotation.Nullable;

abstract class hp implements gm {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f4474a;

    hp() {
    }

    static {
        String[] strArr = {MTGRewardVideoActivity.INTENT_REWARD, "purchase", "custom_action"};
        f4474a = strArr;
        Arrays.sort(strArr);
    }

    public final void a(gn gnVar) {
        if (this instanceof gq) {
            gq gqVar = (gq) this;
            gnVar.a(gqVar.a(), gqVar.b());
            return;
        }
        if (this instanceof gr) {
            gr grVar = (gr) this;
            gnVar.a(grVar.a(), grVar.b(), grVar.c(), grVar.d());
        }
    }

    public static boolean a(String str) {
        return Arrays.binarySearch(f4474a, str) >= 0;
    }

    @Nullable
    public static hp a(String str, bn bnVar) {
        if (MTGRewardVideoActivity.INTENT_REWARD.equals(str)) {
            return (hp) bnVar.a(hz.f4486a);
        }
        if ("purchase".equals(str)) {
            return (hp) bnVar.a(hx.f4484a);
        }
        return null;
    }
}
