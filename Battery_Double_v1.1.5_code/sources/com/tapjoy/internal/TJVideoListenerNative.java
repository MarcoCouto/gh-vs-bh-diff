package com.tapjoy.internal;

import com.tapjoy.TJVideoListener;

public class TJVideoListenerNative implements TJVideoListener {

    /* renamed from: a reason: collision with root package name */
    private final long f4300a;

    private static native void onVideoCompleteNative(long j);

    private static native void onVideoErrorNative(long j, int i);

    private static native void onVideoStartNative(long j);

    private TJVideoListenerNative(long j) {
        if (j != 0) {
            this.f4300a = j;
            return;
        }
        throw new IllegalArgumentException();
    }

    public void onVideoStart() {
        onVideoStartNative(this.f4300a);
    }

    public void onVideoError(int i) {
        onVideoErrorNative(this.f4300a, i);
    }

    public void onVideoComplete() {
        onVideoCompleteNative(this.f4300a);
    }

    @fu
    static Object create(long j) {
        return new TJVideoListenerNative(j);
    }
}
