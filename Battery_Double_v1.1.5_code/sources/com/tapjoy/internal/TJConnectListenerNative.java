package com.tapjoy.internal;

import com.tapjoy.TJConnectListener;

public class TJConnectListenerNative implements TJConnectListener {

    /* renamed from: a reason: collision with root package name */
    private final long f4295a;

    private static native void onConnectFailureNative(long j);

    private static native void onConnectSuccessNative(long j);

    private TJConnectListenerNative(long j) {
        if (j != 0) {
            this.f4295a = j;
            return;
        }
        throw new IllegalArgumentException();
    }

    public void onConnectSuccess() {
        onConnectSuccessNative(this.f4295a);
    }

    public void onConnectFailure() {
        onConnectFailureNative(this.f4295a);
    }

    @fu
    static Object create(long j) {
        return new TJConnectListenerNative(j);
    }
}
