package com.tapjoy.internal;

import android.support.v4.media.session.PlaybackStateCompat;

final class ja {

    /* renamed from: a reason: collision with root package name */
    static iz f4513a;
    static long b;

    private ja() {
    }

    static iz a() {
        synchronized (ja.class) {
            if (f4513a == null) {
                return new iz();
            }
            iz izVar = f4513a;
            f4513a = izVar.f;
            izVar.f = null;
            b -= PlaybackStateCompat.ACTION_PLAY_FROM_URI;
            return izVar;
        }
    }

    static void a(iz izVar) {
        if (izVar.f != null || izVar.g != null) {
            throw new IllegalArgumentException();
        } else if (!izVar.d) {
            synchronized (ja.class) {
                if (b + PlaybackStateCompat.ACTION_PLAY_FROM_URI <= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                    b += PlaybackStateCompat.ACTION_PLAY_FROM_URI;
                    izVar.f = f4513a;
                    izVar.c = 0;
                    izVar.b = 0;
                    f4513a = izVar;
                }
            }
        }
    }
}
