package com.tapjoy.internal;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

public abstract class kd implements kg {

    /* renamed from: a reason: collision with root package name */
    private final ReentrantLock f4536a = new ReentrantLock();
    private final a b = new a(this, 0);
    private final a c = new a(this, 0);
    private com.tapjoy.internal.kg.a d = com.tapjoy.internal.kg.a.NEW;
    private boolean e = false;

    class a extends kc {
        private a() {
        }

        /* synthetic */ a(kd kdVar, byte b) {
            this();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public com.tapjoy.internal.kg.a get(long j, TimeUnit timeUnit) {
            try {
                return (com.tapjoy.internal.kg.a) super.get(j, timeUnit);
            } catch (TimeoutException unused) {
                throw new TimeoutException(kd.this.toString());
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void b();

    public final kf e() {
        this.f4536a.lock();
        try {
            if (this.d == com.tapjoy.internal.kg.a.NEW) {
                this.d = com.tapjoy.internal.kg.a.STARTING;
                a();
            }
        } catch (Throwable th) {
            this.f4536a.unlock();
            throw th;
        }
        this.f4536a.unlock();
        return this.b;
    }

    private kf g() {
        this.f4536a.lock();
        try {
            if (this.d == com.tapjoy.internal.kg.a.NEW) {
                this.d = com.tapjoy.internal.kg.a.TERMINATED;
                this.b.a((Object) com.tapjoy.internal.kg.a.TERMINATED);
                this.c.a((Object) com.tapjoy.internal.kg.a.TERMINATED);
            } else if (this.d == com.tapjoy.internal.kg.a.STARTING) {
                this.e = true;
                this.b.a((Object) com.tapjoy.internal.kg.a.STOPPING);
            } else if (this.d == com.tapjoy.internal.kg.a.RUNNING) {
                this.d = com.tapjoy.internal.kg.a.STOPPING;
                b();
            }
        } catch (Throwable th) {
            this.f4536a.unlock();
            throw th;
        }
        this.f4536a.unlock();
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.f4536a.lock();
        try {
            if (this.d == com.tapjoy.internal.kg.a.STARTING) {
                this.d = com.tapjoy.internal.kg.a.RUNNING;
                if (this.e) {
                    g();
                } else {
                    this.b.a((Object) com.tapjoy.internal.kg.a.RUNNING);
                }
                return;
            }
            StringBuilder sb = new StringBuilder("Cannot notifyStarted() when the service is ");
            sb.append(this.d);
            IllegalStateException illegalStateException = new IllegalStateException(sb.toString());
            a(illegalStateException);
            throw illegalStateException;
        } finally {
            this.f4536a.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.f4536a.lock();
        try {
            if (this.d != com.tapjoy.internal.kg.a.STOPPING) {
                if (this.d != com.tapjoy.internal.kg.a.RUNNING) {
                    StringBuilder sb = new StringBuilder("Cannot notifyStopped() when the service is ");
                    sb.append(this.d);
                    IllegalStateException illegalStateException = new IllegalStateException(sb.toString());
                    a(illegalStateException);
                    throw illegalStateException;
                }
            }
            this.d = com.tapjoy.internal.kg.a.TERMINATED;
            this.c.a((Object) com.tapjoy.internal.kg.a.TERMINATED);
        } finally {
            this.f4536a.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Throwable th) {
        jq.a(th);
        this.f4536a.lock();
        try {
            if (this.d == com.tapjoy.internal.kg.a.STARTING) {
                this.b.a(th);
                this.c.a((Throwable) new Exception("Service failed to start.", th));
            } else if (this.d == com.tapjoy.internal.kg.a.STOPPING) {
                this.c.a(th);
            } else if (this.d == com.tapjoy.internal.kg.a.RUNNING) {
                this.c.a((Throwable) new Exception("Service failed while running", th));
            } else if (this.d == com.tapjoy.internal.kg.a.NEW || this.d == com.tapjoy.internal.kg.a.TERMINATED) {
                StringBuilder sb = new StringBuilder("Failed while in state:");
                sb.append(this.d);
                throw new IllegalStateException(sb.toString(), th);
            }
            this.d = com.tapjoy.internal.kg.a.FAILED;
        } finally {
            this.f4536a.unlock();
        }
    }

    public final com.tapjoy.internal.kg.a f() {
        com.tapjoy.internal.kg.a aVar;
        this.f4536a.lock();
        try {
            if (!this.e || this.d != com.tapjoy.internal.kg.a.STARTING) {
                aVar = this.d;
            } else {
                aVar = com.tapjoy.internal.kg.a.STOPPING;
            }
            return aVar;
        } finally {
            this.f4536a.unlock();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(f());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
