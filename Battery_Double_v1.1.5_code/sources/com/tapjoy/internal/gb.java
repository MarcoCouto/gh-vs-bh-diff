package com.tapjoy.internal;

import android.content.Context;
import android.content.SharedPreferences;
import com.tapjoy.TapjoyConstants;
import com.tapjoy.internal.gk.a;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public final class gb {
    private static final gb b;
    private static gb c;

    /* renamed from: a reason: collision with root package name */
    public final fz f4418a = new fz();
    private Context d;

    static {
        gb gbVar = new gb();
        b = gbVar;
        c = gbVar;
    }

    public static gb a() {
        return c;
    }

    public static fz b() {
        return c.f4418a;
    }

    gb() {
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0030 */
    public final synchronized void a(Context context) {
        SharedPreferences c2;
        if (context != null) {
            if (this.d == null) {
                this.d = context;
                c2 = c();
                String string = c().getString(TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS, null);
                if (string != null) {
                    bn b2 = bn.b(string);
                    try {
                        Map d2 = b2.d();
                        b2.close();
                        this.f4418a.a(d2);
                    } catch (Exception ) {
                        c2.edit().remove(TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS).apply();
                    } catch (Throwable th) {
                        b2.close();
                        throw th;
                    }
                }
                AnonymousClass1 r4 = new Observer() {
                    public final void update(Observable observable, Object obj) {
                        Object obj2;
                        gg.a(gb.this.f4418a.a("usage_tracking_enabled", false));
                        String str = "usage_tracking_exclude";
                        Class<List> cls = List.class;
                        Iterator it = gb.this.f4418a.b.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                obj2 = null;
                                break;
                            }
                            Object a2 = ((a) it.next()).a(str);
                            if (a2 != null && cls.isInstance(a2)) {
                                obj2 = cls.cast(a2);
                                break;
                            }
                        }
                        gg.a((Collection) obj2);
                    }
                };
                this.f4418a.addObserver(r4);
                r4.update(this.f4418a, null);
            }
        }
    }

    public final SharedPreferences c() {
        return this.d.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0);
    }
}
