package com.tapjoy.internal;

public final class cr {

    /* renamed from: a reason: collision with root package name */
    private final cz f4346a;

    private cr(cz czVar) {
        this.f4346a = czVar;
    }

    public static cr a(cs csVar) {
        cz czVar = (cz) csVar;
        dp.a((Object) csVar, "AdSession is null");
        if (czVar.c.b == null) {
            dp.a(czVar);
            cr crVar = new cr(czVar);
            czVar.c.b = crVar;
            return crVar;
        }
        throw new IllegalStateException("AdEvents already exists for AdSession");
    }

    public final void a() {
        dp.a(this.f4346a);
        if (cw.f4350a == this.f4346a.f4353a.f4347a) {
            if (!this.f4346a.d()) {
                try {
                    this.f4346a.a();
                } catch (Exception unused) {
                }
            }
            if (this.f4346a.d()) {
                cz czVar = this.f4346a;
                if (!czVar.g) {
                    dg.a().a(czVar.c.c(), "publishImpressionEvent", new Object[0]);
                    czVar.g = true;
                    return;
                }
                throw new IllegalStateException("Impression event can only be sent once");
            }
            return;
        }
        throw new IllegalStateException("Impression event is not expected from the Native AdSession");
    }
}
