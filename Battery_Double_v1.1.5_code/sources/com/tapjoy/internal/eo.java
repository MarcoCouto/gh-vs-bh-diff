package com.tapjoy.internal;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

final class eo extends AbstractList implements Serializable, RandomAccess {

    /* renamed from: a reason: collision with root package name */
    private final ArrayList f4388a;

    eo(List list) {
        this.f4388a = new ArrayList(list);
    }

    public final int size() {
        return this.f4388a.size();
    }

    public final Object get(int i) {
        return this.f4388a.get(i);
    }

    public final Object[] toArray() {
        return this.f4388a.toArray();
    }
}
