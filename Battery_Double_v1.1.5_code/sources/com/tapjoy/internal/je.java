package com.tapjoy.internal;

import java.io.InterruptedIOException;

public class je {

    /* renamed from: a reason: collision with root package name */
    public static final je f4514a = new je() {
        public final void a() {
        }
    };
    private boolean b;
    private long c;

    public void a() {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.b && this.c - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }
}
