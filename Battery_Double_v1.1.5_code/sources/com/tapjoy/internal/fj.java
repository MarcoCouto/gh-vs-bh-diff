package com.tapjoy.internal;

import android.os.SystemClock;

public final class fj {

    /* renamed from: a reason: collision with root package name */
    public static final fj f4400a = new fj(-1);
    public final long b;
    public long c;

    public fj(long j) {
        this.b = j;
        this.c = SystemClock.elapsedRealtime();
    }

    public fj() {
        this.b = 3600000;
        try {
            this.c = SystemClock.elapsedRealtime() - 3600000;
        } catch (NullPointerException unused) {
            this.c = -1;
        }
    }

    public final boolean a() {
        try {
            return SystemClock.elapsedRealtime() - this.c > this.b;
        } catch (NullPointerException unused) {
            return true;
        }
    }

    public final boolean a(long j) {
        try {
            return (SystemClock.elapsedRealtime() - this.c) + j > this.b;
        } catch (NullPointerException unused) {
            return true;
        }
    }
}
