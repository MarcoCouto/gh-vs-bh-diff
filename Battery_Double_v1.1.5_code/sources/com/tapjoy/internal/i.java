package com.tapjoy.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.github.mikephil.charting.utils.Utils;

public final class i extends m {
    private final double c = Utils.DOUBLE_EPSILON;

    public i(SharedPreferences sharedPreferences, String str) {
        super(sharedPreferences, str);
    }

    public final double a() {
        String string = this.f4541a.getString(this.b, null);
        if (string != null) {
            try {
                return Double.parseDouble(string);
            } catch (NumberFormatException unused) {
            }
        }
        return this.c;
    }

    public final Editor a(Editor editor) {
        return editor.remove(this.b);
    }

    public final Editor a(Editor editor, double d) {
        return editor.putString(this.b, Double.toString(d));
    }
}
