package com.tapjoy.internal;

import android.content.Context;
import android.os.Handler;

public final class ck {

    /* renamed from: a reason: collision with root package name */
    private static cl f4343a = new cl();

    public static String a() {
        return "1.1.0-tapjoy";
    }

    public static boolean b() {
        return f4343a.f4344a;
    }

    public static boolean a(String str, Context context) {
        cl clVar = f4343a;
        Context applicationContext = context.getApplicationContext();
        cl.b(str);
        dp.a((Object) applicationContext, "Application Context cannot be null");
        if (!(cl.a("1.1.0-tapjoy") == cl.a(str))) {
            return false;
        }
        if (!clVar.f4344a) {
            clVar.f4344a = true;
            dh a2 = dh.a();
            a2.b = new cp(new Handler(), applicationContext, new cm(), a2);
            de.a().f4358a = applicationContext.getApplicationContext();
            dm.a(applicationContext);
            df.a().f4360a = applicationContext != null ? applicationContext.getApplicationContext() : null;
        }
        return true;
    }
}
