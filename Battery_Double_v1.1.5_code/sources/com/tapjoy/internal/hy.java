package com.tapjoy.internal;

import android.graphics.Bitmap;
import com.tapjoy.internal.ap.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutorService;

public final class hy {
    public static final bi e = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            return new hy(bnVar);
        }
    };
    private static final an f;

    /* renamed from: a reason: collision with root package name */
    public URL f4485a;
    public Bitmap b;
    public byte[] c;
    public Cif d;

    static {
        aq arVar = new ar();
        if (!(arVar instanceof as)) {
            arVar = new a(arVar);
        }
        f = arVar;
    }

    public hy(URL url) {
        this.f4485a = url;
    }

    public final boolean a() {
        return (this.b == null && this.c == null) ? false : true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0099, code lost:
        r12 = java.lang.Long.parseLong(r8.substring(8));
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004a  */
    public final void b() {
        boolean a2 = gb.b().a("mm_external_cache_enabled", true);
        boolean z = !a2;
        if (z) {
            this.b = (Bitmap) f.a(this.f4485a);
            if (this.b != null) {
                return;
            }
        }
        if (a2) {
            File a3 = hu.f4478a.a(this.f4485a);
            if (a3 != null) {
                FileInputStream fileInputStream = null;
                try {
                    FileInputStream fileInputStream2 = new FileInputStream(a3);
                    try {
                        a(fileInputStream2);
                        ka.a(fileInputStream2);
                    } catch (IOException unused) {
                        fileInputStream = fileInputStream2;
                        ka.a(fileInputStream);
                        if (this.b == null) {
                        }
                        f.a(this.f4485a, this.b);
                        return;
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream = fileInputStream2;
                        ka.a(fileInputStream);
                        throw th;
                    }
                } catch (IOException unused2) {
                    ka.a(fileInputStream);
                    if (this.b == null) {
                    }
                    f.a(this.f4485a, this.b);
                    return;
                } catch (Throwable th2) {
                    th = th2;
                    ka.a(fileInputStream);
                    throw th;
                }
                if (this.b == null || this.c != null) {
                    if (z && this.b != null) {
                        f.a(this.f4485a, this.b);
                    }
                    return;
                }
                a3.delete();
            }
        }
        URLConnection a4 = fk.a(this.f4485a);
        String headerField = a4.getHeaderField(HttpRequest.HEADER_CACHE_CONTROL);
        if (!jr.c(headerField)) {
            String[] split = headerField.split(",");
            int length = split.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                String trim = split[i].trim();
                if (trim.startsWith("max-age=")) {
                    break;
                }
                i++;
            }
        }
        long j = 0;
        InputStream inputStream = a4.getInputStream();
        ByteArrayInputStream a5 = a(inputStream);
        ka.a(inputStream);
        hu huVar = hu.f4478a;
        if (hu.a(j) && a2 && !(this.b == null && this.c == null)) {
            hu huVar2 = hu.f4478a;
            URL url = this.f4485a;
            if (huVar2.b != null) {
                ExecutorService executorService = huVar2.e;
                AnonymousClass2 r8 = new Runnable(url, a5, j) {

                    /* renamed from: a reason: collision with root package name */
                    final /* synthetic */ URL f4480a;
                    final /* synthetic */ InputStream b;
                    final /* synthetic */ long c;

                    {
                        this.f4480a = r2;
                        this.b = r3;
                        this.c = r4;
                    }

                    public final void run() {
                        try {
                            File createTempFile = File.createTempFile("tj_", null, hu.this.b());
                            if (createTempFile == null) {
                                new Object[1][0] = this.f4480a;
                                return;
                            }
                            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
                            try {
                                jy.a(this.b, fileOutputStream);
                                fileOutputStream.close();
                                long j = this.c;
                                if (j > 604800) {
                                    j = 604800;
                                }
                                long b2 = v.b() + (j * 1000);
                                synchronized (hu.this) {
                                    String b3 = hu.this.b(this.f4480a);
                                    if (createTempFile.renameTo(hu.this.a(b3))) {
                                        hu.this.c.edit().putLong(b3, b2).commit();
                                        Object[] objArr = {createTempFile, b3, this.f4480a};
                                    }
                                }
                            } catch (IOException unused) {
                                new Object[1][0] = this.f4480a;
                            }
                        } catch (FileNotFoundException unused2) {
                            new Object[1][0] = this.f4480a;
                        } catch (IOException unused3) {
                            new Object[1][0] = this.f4480a;
                        }
                    }
                };
                executorService.submit(r8);
            }
        }
        if (z && this.b != null) {
            f.a(this.f4485a, this.b);
        }
    }

    private ByteArrayInputStream a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        jy.a(inputStream, byteArrayOutputStream);
        byteArrayOutputStream.close();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);
        ig igVar = new ig();
        igVar.a(byteArray);
        Cif a2 = igVar.a();
        if (a2.b == 0) {
            this.c = byteArray;
            this.d = a2;
        } else {
            s sVar = s.f4543a;
            this.b = s.a((InputStream) byteArrayInputStream);
            byteArrayInputStream.reset();
        }
        return byteArrayInputStream;
    }

    hy(bn bnVar) {
        if (bnVar.k() == bs.STRING) {
            this.f4485a = bnVar.e();
            return;
        }
        bnVar.h();
        String l = bnVar.l();
        while (bnVar.j()) {
            if ("url".equals(l)) {
                this.f4485a = bnVar.e();
            } else {
                bnVar.s();
            }
        }
        bnVar.i();
    }
}
