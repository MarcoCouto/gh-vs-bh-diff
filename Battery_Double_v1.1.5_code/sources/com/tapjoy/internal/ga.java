package com.tapjoy.internal;

import android.content.Context;
import com.tapjoy.TJActionRequest;
import com.tapjoy.TJError;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TapjoyConnectCore;
import com.tapjoy.TapjoyLog;
import java.util.Observable;
import java.util.Observer;

abstract class ga {
    volatile a b;

    class a implements TJPlacementListener, Observer {
        private final Object b;
        private final fj c;
        private volatile boolean d;
        private TJPlacement e;

        public final void onClick(TJPlacement tJPlacement) {
        }

        public final void onContentDismiss(TJPlacement tJPlacement) {
        }

        public final void onContentShow(TJPlacement tJPlacement) {
        }

        public final void onPurchaseRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str) {
        }

        public final void onRequestSuccess(TJPlacement tJPlacement) {
        }

        public final void onRewardRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str, int i) {
        }

        a(ga gaVar, Object obj) {
            this(obj, new fj(10000));
        }

        a(Object obj, fj fjVar) {
            this.b = obj;
            this.c = fjVar;
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            synchronized (this) {
                if (!this.d) {
                    if (this.c.a()) {
                        a("Timed out");
                        return;
                    }
                    if (!TapjoyConnectCore.isConnected()) {
                        ft.f4412a.addObserver(this);
                        if (TapjoyConnectCore.isConnected()) {
                            ft.f4412a.deleteObserver(this);
                        } else {
                            return;
                        }
                    }
                    if (this.e == null) {
                        if (!ga.this.a()) {
                            a("Cannot request");
                            return;
                        }
                        this.e = ga.this.a(TapjoyConnectCore.getContext(), this, this.b);
                        this.e.requestContent();
                    } else if (this.e.isContentReady()) {
                        if (ga.this.a((Observer) this)) {
                            this.e.showContent();
                            a(null);
                        }
                    }
                }
            }
        }

        private void a(String str) {
            synchronized (this) {
                String a2 = ga.this.a(this.b);
                if (str == null) {
                    StringBuilder sb = new StringBuilder("Placement ");
                    sb.append(a2);
                    sb.append(" is presented now");
                    TapjoyLog.i("SystemPlacement", sb.toString());
                } else {
                    StringBuilder sb2 = new StringBuilder("Cannot show placement ");
                    sb2.append(a2);
                    sb2.append(" now (");
                    sb2.append(str);
                    sb2.append(")");
                    TapjoyLog.i("SystemPlacement", sb2.toString());
                }
                this.d = true;
                this.e = null;
                ft.f4412a.deleteObserver(this);
                ft.e.deleteObserver(this);
                ft.c.deleteObserver(this);
            }
            ga gaVar = ga.this;
            synchronized (gaVar) {
                if (gaVar.b == this) {
                    gaVar.b = null;
                }
            }
        }

        public final void update(Observable observable, Object obj) {
            a();
        }

        public final void onRequestFailure(TJPlacement tJPlacement, TJError tJError) {
            a(tJError.message);
        }

        public final void onContentReady(TJPlacement tJPlacement) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public abstract TJPlacement a(Context context, TJPlacementListener tJPlacementListener, Object obj);

    /* access modifiers changed from: protected */
    public abstract String a(Object obj);

    ga() {
    }

    public final boolean c(Object obj) {
        if (!a()) {
            return false;
        }
        a aVar = null;
        synchronized (this) {
            if (this.b == null) {
                aVar = b(obj);
                this.b = aVar;
            }
        }
        if (aVar == null) {
            return false;
        }
        aVar.a();
        return true;
    }

    /* access modifiers changed from: protected */
    public a b(Object obj) {
        return new a(this, obj);
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return !TapjoyConnectCore.isFullScreenViewOpen();
    }

    /* access modifiers changed from: protected */
    public boolean a(Observer observer) {
        if (TapjoyConnectCore.isFullScreenViewOpen()) {
            ft.e.addObserver(observer);
            if (TapjoyConnectCore.isFullScreenViewOpen()) {
                return false;
            }
            ft.e.deleteObserver(observer);
        }
        if (!ha.a().d()) {
            ft.c.addObserver(observer);
            if (!ha.a().d()) {
                return false;
            }
            ft.c.deleteObserver(observer);
        }
        return true;
    }
}
