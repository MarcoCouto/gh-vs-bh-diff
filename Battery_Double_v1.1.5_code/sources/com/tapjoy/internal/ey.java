package com.tapjoy.internal;

public final class ey extends ei {
    public static final ek c = new b();
    public static final ez d = ez.APP;
    public final ez e;
    public final String f;
    public final String g;

    public static final class a extends com.tapjoy.internal.ei.a {
        public ez c;
        public String d;
        public String e;

        public final ey b() {
            if (this.c != null && this.d != null) {
                return new ey(this.c, this.d, this.e, super.a());
            }
            throw ep.a(this.c, "type", this.d, "name");
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            ey eyVar = (ey) obj;
            return ez.ADAPTER.a(1, (Object) eyVar.e) + ek.p.a(2, (Object) eyVar.f) + (eyVar.g != null ? ek.p.a(3, (Object) eyVar.g) : 0) + eyVar.a().c();
        }

        public final /* synthetic */ Object a(el elVar) {
            return b(elVar);
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            ey eyVar = (ey) obj;
            ez.ADAPTER.a(emVar, 1, eyVar.e);
            ek.p.a(emVar, 2, eyVar.f);
            if (eyVar.g != null) {
                ek.p.a(emVar, 3, eyVar.g);
            }
            emVar.a(eyVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, ey.class);
        }

        private static ey b(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            try {
                                aVar.c = (ez) ez.ADAPTER.a(elVar);
                                break;
                            } catch (com.tapjoy.internal.ek.a e) {
                                aVar.a(b, eh.VARINT, Long.valueOf((long) e.f4385a));
                                break;
                            }
                        case 2:
                            aVar.d = (String) ek.p.a(elVar);
                            break;
                        case 3:
                            aVar.e = (String) ek.p.a(elVar);
                            break;
                        default:
                            eh c = elVar.c();
                            aVar.a(b, c, c.a().a(elVar));
                            break;
                    }
                } else {
                    elVar.a(a2);
                    return aVar.b();
                }
            }
        }
    }

    public ey(ez ezVar, String str, String str2, iv ivVar) {
        super(c, ivVar);
        this.e = ezVar;
        this.f = str;
        this.g = str2;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ey)) {
            return false;
        }
        ey eyVar = (ey) obj;
        return a().equals(eyVar.a()) && this.e.equals(eyVar.e) && this.f.equals(eyVar.f) && ep.a((Object) this.g, (Object) eyVar.g);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int hashCode = (((((a().hashCode() * 37) + this.e.hashCode()) * 37) + this.f.hashCode()) * 37) + (this.g != null ? this.g.hashCode() : 0);
        this.b = hashCode;
        return hashCode;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(", type=");
        sb.append(this.e);
        sb.append(", name=");
        sb.append(this.f);
        if (this.g != null) {
            sb.append(", category=");
            sb.append(this.g);
        }
        StringBuilder replace = sb.replace(0, 2, "EventGroup{");
        replace.append('}');
        return replace.toString();
    }
}
