package com.tapjoy.internal;

public final class fd extends ei {
    public static final ek c = new b();
    public final String d;
    public final String e;
    public final String f;

    public static final class a extends com.tapjoy.internal.ei.a {
        public String c;
        public String d;
        public String e;

        public final fd b() {
            return new fd(this.c, this.d, this.e, super.a());
        }
    }

    static final class b extends ek {
        public final /* synthetic */ int a(Object obj) {
            fd fdVar = (fd) obj;
            int i = 0;
            int a2 = (fdVar.d != null ? ek.p.a(1, (Object) fdVar.d) : 0) + (fdVar.e != null ? ek.p.a(2, (Object) fdVar.e) : 0);
            if (fdVar.f != null) {
                i = ek.p.a(3, (Object) fdVar.f);
            }
            return a2 + i + fdVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(em emVar, Object obj) {
            fd fdVar = (fd) obj;
            if (fdVar.d != null) {
                ek.p.a(emVar, 1, fdVar.d);
            }
            if (fdVar.e != null) {
                ek.p.a(emVar, 2, fdVar.e);
            }
            if (fdVar.f != null) {
                ek.p.a(emVar, 3, fdVar.f);
            }
            emVar.a(fdVar.a());
        }

        b() {
            super(eh.LENGTH_DELIMITED, fd.class);
        }

        public final /* synthetic */ Object a(el elVar) {
            a aVar = new a();
            long a2 = elVar.a();
            while (true) {
                int b = elVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            aVar.c = (String) ek.p.a(elVar);
                            break;
                        case 2:
                            aVar.d = (String) ek.p.a(elVar);
                            break;
                        case 3:
                            aVar.e = (String) ek.p.a(elVar);
                            break;
                        default:
                            eh c = elVar.c();
                            aVar.a(b, c, c.a().a(elVar));
                            break;
                    }
                } else {
                    elVar.a(a2);
                    return aVar.b();
                }
            }
        }
    }

    public fd(String str, String str2, String str3) {
        this(str, str2, str3, iv.b);
    }

    public fd(String str, String str2, String str3, iv ivVar) {
        super(c, ivVar);
        this.d = str;
        this.e = str2;
        this.f = str3;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fd)) {
            return false;
        }
        fd fdVar = (fd) obj;
        return a().equals(fdVar.a()) && ep.a((Object) this.d, (Object) fdVar.d) && ep.a((Object) this.e, (Object) fdVar.e) && ep.a((Object) this.f, (Object) fdVar.f);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        int hashCode = ((((a().hashCode() * 37) + (this.d != null ? this.d.hashCode() : 0)) * 37) + (this.e != null ? this.e.hashCode() : 0)) * 37;
        if (this.f != null) {
            i2 = this.f.hashCode();
        }
        int i3 = hashCode + i2;
        this.b = i3;
        return i3;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.d != null) {
            sb.append(", fq7Change=");
            sb.append(this.d);
        }
        if (this.e != null) {
            sb.append(", fq30Change=");
            sb.append(this.e);
        }
        if (this.f != null) {
            sb.append(", pushId=");
            sb.append(this.f);
        }
        StringBuilder replace = sb.replace(0, 2, "Meta{");
        replace.append('}');
        return replace.toString();
    }
}
