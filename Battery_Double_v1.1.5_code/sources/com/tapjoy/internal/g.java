package com.tapjoy.internal;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

public final class g extends at implements ax, Closeable {

    /* renamed from: a reason: collision with root package name */
    private SQLiteDatabase f4416a;
    private final bd b;
    private int c;

    public g(File file, bd bdVar) {
        this.f4416a = SQLiteDatabase.openOrCreateDatabase(file, null);
        this.b = bdVar;
        if (this.f4416a.getVersion() != 1) {
            this.f4416a.beginTransaction();
            try {
                this.f4416a.execSQL("CREATE TABLE IF NOT EXISTS List(value BLOB)");
                this.f4416a.setVersion(1);
                this.f4416a.setTransactionSuccessful();
            } finally {
                this.f4416a.endTransaction();
            }
        }
        this.c = a();
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        close();
        super.finalize();
    }

    public final void close() {
        if (this.f4416a != null) {
            this.f4416a.close();
            this.f4416a = null;
        }
    }

    private int a() {
        Cursor cursor = null;
        try {
            Cursor rawQuery = this.f4416a.rawQuery("SELECT COUNT(1) FROM List", null);
            try {
                if (rawQuery.moveToNext()) {
                    int i = rawQuery.getInt(0);
                    a(rawQuery);
                    return i;
                }
                a(rawQuery);
                return 0;
            } catch (Throwable th) {
                Cursor cursor2 = rawQuery;
                th = th;
                cursor = cursor2;
                a(cursor);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            a(cursor);
            throw th;
        }
    }

    public final int size() {
        return this.c;
    }

    public final void clear() {
        this.f4416a.delete("List", "1", null);
        this.c = 0;
    }

    public final boolean offer(Object obj) {
        jq.a(obj);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            this.b.a(byteArrayOutputStream, obj);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            ka.a(byteArrayOutputStream);
            ContentValues contentValues = new ContentValues();
            contentValues.put("value", byteArray);
            if (this.f4416a.insert("List", null, contentValues) == -1) {
                return false;
            }
            this.c++;
            return true;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } catch (Throwable th) {
            ka.a(byteArrayOutputStream);
            throw th;
        }
    }

    public final Object poll() {
        if (this.c <= 0) {
            return null;
        }
        Object peek = peek();
        b(1);
        return peek;
    }

    public final Object peek() {
        if (this.c > 0) {
            return a(0);
        }
        return null;
    }

    public final Object a(int i) {
        Cursor cursor;
        Throwable th;
        ByteArrayInputStream byteArrayInputStream;
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException();
        }
        try {
            SQLiteDatabase sQLiteDatabase = this.f4416a;
            StringBuilder sb = new StringBuilder("SELECT value FROM List ORDER BY rowid LIMIT ");
            sb.append(i);
            sb.append(",1");
            cursor = sQLiteDatabase.rawQuery(sb.toString(), null);
            try {
                if (cursor.moveToNext()) {
                    byteArrayInputStream = new ByteArrayInputStream(cursor.getBlob(0));
                    Object b2 = this.b.b(byteArrayInputStream);
                    ka.a(byteArrayInputStream);
                    a(cursor);
                    return b2;
                }
                throw new NoSuchElementException();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            } catch (Throwable th2) {
                th = th2;
                a(cursor);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            a(cursor);
            throw th;
        }
    }

    public final void b(int i) {
        if (i <= 0 || i > this.c) {
            throw new IndexOutOfBoundsException();
        } else if (i == this.c) {
            clear();
        } else {
            Cursor cursor = null;
            try {
                SQLiteDatabase sQLiteDatabase = this.f4416a;
                StringBuilder sb = new StringBuilder("SELECT rowid FROM List ORDER BY rowid LIMIT ");
                sb.append(i - 1);
                sb.append(",1");
                Cursor rawQuery = sQLiteDatabase.rawQuery(sb.toString(), null);
                try {
                    if (rawQuery.moveToNext()) {
                        long j = rawQuery.getLong(0);
                        rawQuery.close();
                        StringBuilder sb2 = new StringBuilder("rowid <= ");
                        sb2.append(j);
                        int delete = this.f4416a.delete("List", sb2.toString(), null);
                        this.c -= delete;
                        if (delete == i) {
                            a((Cursor) null);
                            return;
                        }
                        StringBuilder sb3 = new StringBuilder("Try to delete ");
                        sb3.append(i);
                        sb3.append(", but deleted ");
                        sb3.append(delete);
                        throw new IllegalStateException(sb3.toString());
                    }
                    throw new IllegalStateException();
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                    a(cursor);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                a(cursor);
                throw th;
            }
        }
    }

    private static Cursor a(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
        return null;
    }
}
