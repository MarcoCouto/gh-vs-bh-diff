package com.tapjoy.internal;

import android.view.animation.Animation;
import android.view.animation.AnimationSet;

public final class ae extends ad {
    private final AnimationSet b = ((AnimationSet) this.f4304a);

    public ae() {
        super(new AnimationSet(true));
    }

    public final ae a(Animation animation) {
        this.b.addAnimation(animation);
        return this;
    }

    public final /* bridge */ /* synthetic */ Animation a() {
        return this.b;
    }
}
