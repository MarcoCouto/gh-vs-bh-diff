package com.tapjoy.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class o extends m {
    private final String c = null;

    public o(SharedPreferences sharedPreferences, String str) {
        super(sharedPreferences, str);
    }

    public final String a() {
        return this.f4541a.getString(this.b, this.c);
    }

    public final void a(String str) {
        this.f4541a.edit().putString(this.b, str).apply();
    }

    public final Editor a(Editor editor, String str) {
        return str != null ? editor.putString(this.b, str) : editor.remove(this.b);
    }
}
