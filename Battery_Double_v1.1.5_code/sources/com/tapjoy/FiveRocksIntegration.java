package com.tapjoy;

import com.tapjoy.internal.az;
import com.tapjoy.internal.gm;
import com.tapjoy.internal.gn;
import com.tapjoy.internal.gp;
import com.tapjoy.internal.ha;
import com.tapjoy.internal.hb;

public class FiveRocksIntegration {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static az f4200a = new az();

    public static void addPlacementCallback(String str, TJPlacement tJPlacement) {
        synchronized (f4200a) {
            f4200a.put(str, tJPlacement);
        }
    }

    public static void a() {
        ha a2 = ha.a();
        if (!a2.c) {
            a2.c = true;
        }
        AnonymousClass1 r0 = new gp() {
            public final void a(String str) {
            }

            public final void d(String str) {
            }

            public final void b(String str) {
                TJPlacement tJPlacement;
                synchronized (FiveRocksIntegration.f4200a) {
                    tJPlacement = (TJPlacement) FiveRocksIntegration.f4200a.get(str);
                }
                if (tJPlacement != null && tJPlacement.f4260a != null) {
                    tJPlacement.f4260a.onContentReady(tJPlacement);
                }
            }

            public final void c(String str) {
                TJPlacement tJPlacement;
                synchronized (FiveRocksIntegration.f4200a) {
                    tJPlacement = (TJPlacement) FiveRocksIntegration.f4200a.get(str);
                }
                if (tJPlacement != null && tJPlacement.f4260a != null) {
                    tJPlacement.f4260a.onContentShow(tJPlacement);
                }
            }

            public final void a(String str, gm gmVar) {
                if (gmVar != null) {
                    gmVar.a(e(str));
                }
            }

            public final void a(String str, String str2, gm gmVar) {
                TJPlacement tJPlacement;
                if (gmVar != null) {
                    gmVar.a(e(str));
                }
                synchronized (FiveRocksIntegration.f4200a) {
                    tJPlacement = (TJPlacement) FiveRocksIntegration.f4200a.get(str);
                }
                if (tJPlacement != null) {
                    TapjoyConnectCore.viewDidClose(str2);
                    if (tJPlacement.f4260a != null) {
                        tJPlacement.f4260a.onContentDismiss(tJPlacement);
                    }
                }
            }

            private gn e(final String str) {
                return new gn() {
                    public final void a(final String str, String str2) {
                        TJPlacement tJPlacement;
                        synchronized (FiveRocksIntegration.f4200a) {
                            tJPlacement = (TJPlacement) FiveRocksIntegration.f4200a.get(str);
                        }
                        if (tJPlacement != null && tJPlacement.f4260a != null) {
                            tJPlacement.f4260a.onPurchaseRequest(tJPlacement, new TJActionRequest() {
                                public final void cancelled() {
                                }

                                public final void completed() {
                                }

                                public final String getToken() {
                                    return null;
                                }

                                public final String getRequestId() {
                                    return str;
                                }
                            }, str2);
                        }
                    }

                    public final void a(final String str, String str2, int i, final String str3) {
                        TJPlacement tJPlacement;
                        synchronized (FiveRocksIntegration.f4200a) {
                            tJPlacement = (TJPlacement) FiveRocksIntegration.f4200a.get(str);
                        }
                        if (tJPlacement != null && tJPlacement.f4260a != null) {
                            tJPlacement.f4260a.onRewardRequest(tJPlacement, new TJActionRequest() {
                                public final void cancelled() {
                                }

                                public final void completed() {
                                }

                                public final String getRequestId() {
                                    return str;
                                }

                                public final String getToken() {
                                    return str3;
                                }
                            }, str2, i);
                        }
                    }
                };
            }
        };
        ha.a().p = hb.a((gp) r0);
    }
}
