package com.tapjoy;

import android.content.Context;
import android.os.SystemClock;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.tapjoy.TJAdUnit.TJAdUnitVideoListener;
import com.tapjoy.TJAdUnit.TJAdUnitWebViewListener;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.internal.b;
import com.tapjoy.internal.cb;
import com.tapjoy.internal.fc;
import com.tapjoy.internal.fj;
import com.tapjoy.internal.fn;
import com.tapjoy.internal.fr;
import com.tapjoy.internal.fv;
import com.tapjoy.internal.fw;
import com.tapjoy.internal.fx;
import com.tapjoy.internal.gb;
import com.tapjoy.internal.gg;
import com.tapjoy.internal.gg.a;
import com.tapjoy.internal.gj;
import com.tapjoy.internal.gw;
import com.tapjoy.internal.gx;
import com.tapjoy.internal.ha;
import com.tapjoy.internal.hf;
import com.tapjoy.internal.hh;
import com.tapjoy.internal.hi;
import com.tapjoy.internal.hj;
import com.tapjoy.internal.ik;
import com.tapjoy.internal.jr;
import com.tapjoy.internal.v;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TJCorePlacement {
    /* access modifiers changed from: 0000 */

    /* renamed from: a reason: collision with root package name */
    public static final String f4243a = "TJCorePlacement";
    private TJAdUnitVideoListener A = new TJAdUnitVideoListener() {
        public final void onVideoStart() {
            TJPlacement a2 = TJCorePlacement.this.a("SHOW");
            if (a2 != null && a2.getVideoListener() != null) {
                a2.getVideoListener().onVideoStart(a2);
            }
        }

        public final void onVideoCompleted() {
            TJPlacement a2 = TJCorePlacement.this.a("SHOW");
            if (a2 != null && a2.getVideoListener() != null) {
                a2.getVideoListener().onVideoComplete(a2);
            }
        }

        public final void onVideoError(String str) {
            TJPlacement a2 = TJCorePlacement.this.a("SHOW");
            if (a2 != null && a2.getVideoListener() != null) {
                a2.getVideoListener().onVideoError(a2, str);
            }
        }
    };
    /* access modifiers changed from: 0000 */
    public Context b = b.c();
    /* access modifiers changed from: 0000 */
    public TJPlacementData c;
    String d;
    /* access modifiers changed from: 0000 */
    public long e;
    final fx f = new fx();
    TJAdUnit g;
    /* access modifiers changed from: 0000 */
    public boolean h = false;
    /* access modifiers changed from: 0000 */
    public hh i = null;
    boolean j;
    volatile boolean k = false;
    volatile boolean l = false;
    String m;
    String n;
    String o;
    String p;
    HashMap q;
    private Map r = new HashMap();
    /* access modifiers changed from: private */
    public Map s;
    /* access modifiers changed from: private */
    public fn t;
    /* access modifiers changed from: private */
    public boolean u = false;
    /* access modifiers changed from: private */
    public ik v = null;
    /* access modifiers changed from: private */
    public volatile boolean w = false;
    private volatile boolean x = false;
    private boolean y;
    private TJAdUnitWebViewListener z = new TJAdUnitWebViewListener() {
        public final void onContentReady() {
            TJCorePlacement.this.e();
        }

        public final void onClosed() {
            if (TJCorePlacement.this.h) {
                TJPlacementManager.decrementPlacementCacheCount();
                TJCorePlacement.this.h = false;
            }
            if (TJCorePlacement.this.u) {
                TJPlacementManager.decrementPlacementPreRenderCount();
                TJCorePlacement.this.u = false;
            }
        }

        public final void onClick() {
            TJCorePlacement.e(TJCorePlacement.this);
        }
    };

    TJCorePlacement(String str, String str2, boolean z2) {
        if (this.b == null) {
            TapjoyLog.d(f4243a, "getVisibleActivity() is NULL. Activity can be explicitly set via `Tapjoy.setActivity(Activity)`");
        }
        this.y = z2;
        this.c = new TJPlacementData(str2, getPlacementContentUrl());
        this.c.setPlacementName(str);
        this.d = UUID.randomUUID().toString();
        this.g = new TJAdUnit();
        this.g.setWebViewListener(this.z);
        this.g.setVideoListener(this.A);
    }

    /* access modifiers changed from: 0000 */
    public final void a(TJPlacement tJPlacement) {
        boolean z2 = false;
        if (tJPlacement == null) {
            a(ErrorType.SDK_ERROR, new TJError(0, "Cannot request content from a NULL placement"));
            return;
        }
        a("REQUEST", tJPlacement);
        if (this.e - SystemClock.elapsedRealtime() > 0) {
            String str = f4243a;
            StringBuilder sb = new StringBuilder("Content has not expired yet for ");
            sb.append(this.c.getPlacementName());
            TapjoyLog.d(str, sb.toString());
            if (this.k) {
                gg.b("TJPlacement.requestContent").a(Param.CONTENT_TYPE, (Object) a()).a("from", (Object) "cache").c();
                this.x = false;
                b(tJPlacement);
                e();
                return;
            }
            gg.b("TJPlacement.requestContent").a(Param.CONTENT_TYPE, (Object) "none").a("from", (Object) "cache").c();
            b(tJPlacement);
            return;
        }
        if (this.k) {
            gg.c("TJPlacement.requestContent").a("was_available", (Object) Boolean.valueOf(true));
        }
        if (this.l) {
            gg.c("TJPlacement.requestContent").a("was_ready", (Object) Boolean.valueOf(true));
        }
        if (!jr.c(this.o)) {
            HashMap hashMap = new HashMap();
            hashMap.put(TJAdUnitConstants.PARAM_PLACEMENT_MEDIATION_AGENT, this.o);
            hashMap.put(TJAdUnitConstants.PARAM_PLACEMENT_MEDIATION_ID, this.p);
            if (this.q != null && !this.q.isEmpty()) {
                z2 = true;
            }
            if (z2) {
                for (String str2 : this.q.keySet()) {
                    StringBuilder sb2 = new StringBuilder(TJAdUnitConstants.AUCTION_PARAM_PREFIX);
                    sb2.append(str2);
                    hashMap.put(sb2.toString(), this.q.get(str2));
                }
                a(this.c.getAuctionMediationURL(), (Map) hashMap);
                return;
            }
            a(this.c.getMediationURL(), (Map) hashMap);
            return;
        }
        d();
    }

    private synchronized void d() {
        String url = this.c.getUrl();
        if (jr.c(url)) {
            url = getPlacementContentUrl();
            if (jr.c(url)) {
                String str = "TJPlacement is missing APP_ID";
                gg.b("TJPlacement.requestContent").a(str).c();
                a(ErrorType.SDK_ERROR, new TJError(0, str));
                return;
            }
            this.c.updateUrl(url);
        }
        String str2 = f4243a;
        StringBuilder sb = new StringBuilder("sendContentRequest -- URL: ");
        sb.append(url);
        sb.append(" name: ");
        sb.append(this.c.getPlacementName());
        TapjoyLog.d(str2, sb.toString());
        a(url, (Map) null);
    }

    private synchronized void a(String str, Map map) {
        if (this.w) {
            String str2 = f4243a;
            StringBuilder sb = new StringBuilder("Placement ");
            sb.append(this.c.getPlacementName());
            sb.append(" is already requesting content");
            TapjoyLog.i(str2, sb.toString());
            gg.b("TJPlacement.requestContent").b("already doing").c();
            return;
        }
        this.c.resetPlacementRequestData();
        fx fxVar = this.f;
        String str3 = null;
        fxVar.b = null;
        fxVar.d = null;
        fxVar.f4413a = null;
        this.g.resetContentLoadState();
        this.w = false;
        this.x = false;
        this.k = false;
        this.l = false;
        this.i = null;
        this.v = null;
        this.w = true;
        final TJPlacement a2 = a("REQUEST");
        if (!this.y) {
            this.s = TapjoyConnectCore.getGenericURLParams();
            this.s.putAll(TapjoyConnectCore.getTimeStampAndVerifierParams());
        } else {
            this.s = TapjoyConnectCore.getLimitedGenericURLParams();
            this.s.putAll(TapjoyConnectCore.getLimitedTimeStampAndVerifierParams());
        }
        TapjoyUtil.safePut(this.s, "event_name", this.c.getPlacementName(), true);
        TapjoyUtil.safePut(this.s, TJAdUnitConstants.PARAM_PLACEMENT_PRELOAD, "true", true);
        TapjoyUtil.safePut(this.s, "debug", Boolean.toString(gx.f4443a), true);
        ha a3 = ha.a();
        Map map2 = this.s;
        String str4 = TJAdUnitConstants.PARAM_ACTION_ID_EXCLUSION;
        if (a3.b != null) {
            hj hjVar = a3.b;
            hjVar.b();
            str3 = hjVar.b.a();
        }
        TapjoyUtil.safePut(map2, str4, str3, true);
        TapjoyUtil.safePut(this.s, TJAdUnitConstants.PARAM_PLACEMENT_BY_SDK, String.valueOf(this.j), true);
        TapjoyUtil.safePut(this.s, TJAdUnitConstants.PARAM_PUSH_ID, a2.pushId, true);
        TapjoyUtil.safePut(this.s, TapjoyConstants.TJC_MEDIATION_SOURCE, this.m, true);
        TapjoyUtil.safePut(this.s, TapjoyConstants.TJC_ADAPTER_VERSION, this.n, true);
        if (map != null) {
            this.s.putAll(map);
        }
        final fj fjVar = new fj(gb.b().b("placement_request_content_retry_timeout"));
        final gj c2 = gb.b().c("placement_request_content_retry_backoff");
        final a d2 = gg.d("TJPlacement.requestContent");
        final String str5 = str;
        AnonymousClass3 r3 = new Thread() {
            public final void run() {
                gg.a("TJPlacement.requestContent", d2);
                int i = 0;
                while (!a()) {
                    i++;
                    TJCorePlacement.this.s.put(TapjoyConstants.TJC_RETRY, Integer.toString(i));
                    if (i == 1) {
                        d2.a("retry_timeout", (Object) Long.valueOf(fjVar.b));
                    }
                    d2.a("retry_count", (long) i);
                }
            }

            /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
            /* JADX WARNING: Missing exception handler attribute for start block: B:64:0x0318 */
            private boolean a() {
                String c2 = TJCorePlacement.f4243a;
                StringBuilder sb = new StringBuilder("Sending content request for placement ");
                sb.append(TJCorePlacement.this.c.getPlacementName());
                TapjoyLog.i(c2, sb.toString());
                TJCorePlacement tJCorePlacement = TJCorePlacement.this;
                ha a2 = ha.a();
                String g = TJCorePlacement.this.c.getPlacementName();
                Context h = TJCorePlacement.this.b;
                hi hiVar = a2.f4447a;
                fc a3 = hiVar.f4463a.a(false);
                ik ikVar = new ik(hiVar.f4463a, a3.d, a3.e, a3.f, g, h);
                tJCorePlacement.v = ikVar;
                TapjoyHttpURLResponse responseFromURL = new TapjoyURLConnection().getResponseFromURL(str5, (Map) null, (Map) null, TJCorePlacement.this.s);
                TJCorePlacement.this.c.setHttpStatusCode(responseFromURL.statusCode);
                TJCorePlacement.this.c.setHttpResponse(responseFromURL.response);
                if (!responseFromURL.getHeaderFieldAsString(TapjoyConstants.TAPJOY_PRERENDER_HEADER).equals("0")) {
                    TJCorePlacement.this.c.setPrerenderingRequested(true);
                }
                String headerFieldAsString = responseFromURL.getHeaderFieldAsString(TapjoyConstants.TAPJOY_DEBUG_HEADER);
                if (headerFieldAsString != null) {
                    String c3 = TJCorePlacement.f4243a;
                    StringBuilder sb2 = new StringBuilder("Tapjoy-Server-Debug: ");
                    sb2.append(headerFieldAsString);
                    TapjoyLog.v(c3, sb2.toString());
                }
                if (responseFromURL.expires > 0) {
                    long b2 = responseFromURL.expires - (responseFromURL.date > 0 ? responseFromURL.date : v.b());
                    if (b2 > 0) {
                        TJCorePlacement.this.e = SystemClock.elapsedRealtime() + b2;
                    }
                } else {
                    TJCorePlacement.this.e = 0;
                }
                if (!(responseFromURL == null || a2.getListener() == null)) {
                    int i = responseFromURL.statusCode;
                    if (i != 0) {
                        if (i != 200) {
                            gg.b("TJPlacement.requestContent").a(Param.CONTENT_TYPE, (Object) "none").a("code", (Object) Integer.valueOf(responseFromURL.statusCode)).c();
                            TJCorePlacement.this.b(a2);
                        } else {
                            TJCorePlacement.j(TJCorePlacement.this);
                            String headerFieldAsString2 = responseFromURL.getHeaderFieldAsString("Content-Type");
                            if (jr.c(headerFieldAsString2) || !headerFieldAsString2.contains("json")) {
                                gg.b("TJPlacement.requestContent").a(Param.CONTENT_TYPE, (Object) "ad").c();
                                TJCorePlacement.this.f.f4413a = TJCorePlacement.this.t;
                                TJCorePlacement.l(TJCorePlacement.this);
                                TJCorePlacement tJCorePlacement2 = TJCorePlacement.this;
                                AnonymousClass1 r3 = new TJCacheListener() {
                                    public final void onCachingComplete(int i) {
                                        TJCorePlacement.this.u = TJCorePlacement.this.getAdUnit().preload(TJCorePlacement.this.c, TJCorePlacement.this.b);
                                    }
                                };
                                String str = TJCorePlacement.f4243a;
                                StringBuilder sb3 = new StringBuilder("Checking if there is content to cache for placement ");
                                sb3.append(tJCorePlacement2.c.getPlacementName());
                                TapjoyLog.i(str, sb3.toString());
                                String headerFieldAsString3 = responseFromURL.getHeaderFieldAsString(TapjoyConstants.TAPJOY_CACHE_HEADER);
                                try {
                                    if (!TJPlacementManager.canCachePlacement()) {
                                        String str2 = TJCorePlacement.f4243a;
                                        StringBuilder sb4 = new StringBuilder("Placement caching limit reached. No content will be cached for placement ");
                                        sb4.append(tJCorePlacement2.c.getPlacementName());
                                        TapjoyLog.i(str2, sb4.toString());
                                        r3.onCachingComplete(2);
                                    } else {
                                        JSONArray jSONArray = new JSONArray(headerFieldAsString3);
                                        if (jSONArray.length() > 0) {
                                            String str3 = TJCorePlacement.f4243a;
                                            StringBuilder sb5 = new StringBuilder("Begin caching content for placement ");
                                            sb5.append(tJCorePlacement2.c.getPlacementName());
                                            TapjoyLog.i(str3, sb5.toString());
                                            TJPlacementManager.incrementPlacementCacheCount();
                                            tJCorePlacement2.h = true;
                                            TapjoyCache.getInstance().cacheAssetGroup(jSONArray, new TJCacheListener(r3) {

                                                /* renamed from: a reason: collision with root package name */
                                                final /* synthetic */ TJCacheListener f4250a;

                                                {
                                                    this.f4250a = r2;
                                                }

                                                public final void onCachingComplete(int i) {
                                                    this.f4250a.onCachingComplete(i);
                                                }
                                            });
                                        } else {
                                            r3.onCachingComplete(1);
                                        }
                                    }
                                } catch (Exception e2) {
                                    r3.onCachingComplete(2);
                                    String str4 = TJCorePlacement.f4243a;
                                    StringBuilder sb6 = new StringBuilder("Error while handling placement cache: ");
                                    sb6.append(e2.getMessage());
                                    TapjoyLog.d(str4, sb6.toString());
                                }
                            } else if (responseFromURL.getHeaderFieldAsString(TapjoyConstants.TAPJOY_DISABLE_PRELOAD_HEADER).equals("1")) {
                                try {
                                    TJCorePlacement.a(TJCorePlacement.this, responseFromURL.response);
                                    gg.b("TJPlacement.requestContent").a(Param.CONTENT_TYPE, (Object) "ad").c();
                                    TJCorePlacement.this.f.f4413a = TJCorePlacement.this.t;
                                    TJCorePlacement.l(TJCorePlacement.this);
                                    TJCorePlacement.this.e();
                                } catch (TapjoyException e3) {
                                    StringBuilder sb7 = new StringBuilder();
                                    sb7.append(e3.getMessage());
                                    sb7.append(" for placement ");
                                    sb7.append(TJCorePlacement.this.c.getPlacementName());
                                    String sb8 = sb7.toString();
                                    gg.b("TJPlacement.requestContent").a("server error").c();
                                    TJCorePlacement.this.a(a2, ErrorType.SERVER_ERROR, new TJError(responseFromURL.statusCode, sb8));
                                }
                            } else if (TJCorePlacement.this.b(responseFromURL.response)) {
                                gg.b("TJPlacement.requestContent").a(Param.CONTENT_TYPE, (Object) "mm").c();
                                TJCorePlacement.l(TJCorePlacement.this);
                                TJCorePlacement.this.e();
                            } else {
                                gg.b("TJPlacement.requestContent").a("asset error").c();
                                TJCorePlacement.this.a(a2, ErrorType.SERVER_ERROR, new TJError(responseFromURL.statusCode, responseFromURL.response));
                            }
                        }
                    } else if (fjVar.a(c2.e)) {
                        gg.b("TJPlacement.requestContent").a("network error").a("retry_timeout", (Object) Long.valueOf(fjVar.b)).c();
                        TJCorePlacement.this.a(a2, ErrorType.NETWORK_ERROR, new TJError(responseFromURL.statusCode, responseFromURL.response));
                    } else {
                        gj gjVar = c2;
                        long j = gjVar.e;
                        double d2 = (double) gjVar.e;
                        double d3 = gjVar.d;
                        Double.isNaN(d2);
                        long j2 = (long) (d2 * d3);
                        if (j2 < gjVar.b) {
                            j2 = gjVar.b;
                        } else if (j2 > gjVar.c) {
                            j2 = gjVar.c;
                        }
                        gjVar.e = j2;
                        if (j > 0) {
                            synchronized (gjVar) {
                                gjVar.wait(j);
                            }
                        }
                        return false;
                    }
                }
                TJCorePlacement.this.w = false;
                return true;
            }
        };
        r3.start();
    }

    /* access modifiers changed from: private */
    public boolean b(String str) {
        try {
            ik.a aVar = (ik.a) this.v.a(URI.create(this.c.getUrl()), new ByteArrayInputStream(str.getBytes()));
            this.i = aVar.f4497a;
            aVar.f4497a.b();
            if (!aVar.f4497a.c()) {
                TapjoyLog.e(f4243a, "Failed to load fiverocks placement");
                return false;
            }
            fr frVar = null;
            if (this.i instanceof hf) {
                frVar = new fv(this.c.getPlacementName(), this.c.getPlacementType(), this.t);
            } else if (this.i instanceof gw) {
                frVar = new fw(this.c.getPlacementName(), this.c.getPlacementType(), this.t);
            }
            this.f.f4413a = frVar;
            return true;
        } catch (IOException e2) {
            TapjoyLog.e(f4243a, e2.toString());
            e2.printStackTrace();
            return false;
        } catch (cb e3) {
            TapjoyLog.e(f4243a, e3.toString());
            e3.printStackTrace();
            return false;
        }
    }

    public Context getContext() {
        return this.b;
    }

    public void setContext(Context context) {
        this.b = context;
    }

    public TJAdUnit getAdUnit() {
        return this.g;
    }

    public TJPlacementData getPlacementData() {
        return this.c;
    }

    public boolean isContentReady() {
        return this.l;
    }

    public boolean isContentAvailable() {
        return this.k;
    }

    public String getPlacementContentUrl() {
        String b2 = b();
        if (!jr.c(b2)) {
            StringBuilder sb = new StringBuilder();
            sb.append(TapjoyConnectCore.getPlacementURL());
            sb.append("v1/apps/");
            sb.append(b2);
            sb.append("/content?");
            return sb.toString();
        }
        TapjoyLog.i(f4243a, "Placement content URL cannot be generated for null app ID");
        return "";
    }

    /* access modifiers changed from: 0000 */
    public final String a() {
        if (this.i != null) {
            return "mm";
        }
        return this.k ? "ad" : "none";
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str, TJPlacement tJPlacement) {
        synchronized (this.r) {
            this.r.put(str, tJPlacement);
            if (tJPlacement != null) {
                String str2 = f4243a;
                StringBuilder sb = new StringBuilder("Setting ");
                sb.append(str);
                sb.append(" placement: ");
                sb.append(tJPlacement.getGUID());
                TapjoyLog.d(str2, sb.toString());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final TJPlacement a(String str) {
        TJPlacement tJPlacement;
        synchronized (this.r) {
            tJPlacement = (TJPlacement) this.r.get(str);
            if (tJPlacement != null) {
                String str2 = f4243a;
                StringBuilder sb = new StringBuilder("Returning ");
                sb.append(str);
                sb.append(" placement: ");
                sb.append(tJPlacement.getGUID());
                TapjoyLog.d(str2, sb.toString());
            }
        }
        return tJPlacement;
    }

    /* access modifiers changed from: private */
    public void b(TJPlacement tJPlacement) {
        fx fxVar = this.f;
        String placementName = this.c.getPlacementName();
        String placementType = this.c.getPlacementType();
        String a2 = a();
        fxVar.c = 0;
        fxVar.b = gg.e("PlacementContent.funnel").a().a("placement", (Object) placementName).a("placement_type", (Object) placementType).a(Param.CONTENT_TYPE, (Object) a2).a("state", (Object) Integer.valueOf(fxVar.c));
        fxVar.b.c();
        if (!"none".equals(a2)) {
            fxVar.e = gg.e("PlacementContent.ready").a().a("placement", (Object) placementName).a("placement_type", (Object) placementType).a(Param.CONTENT_TYPE, (Object) a2);
        }
        if (tJPlacement != null && tJPlacement.getListener() != null) {
            String str = f4243a;
            StringBuilder sb = new StringBuilder("Content request delivered successfully for placement ");
            sb.append(this.c.getPlacementName());
            sb.append(", contentAvailable: ");
            sb.append(isContentAvailable());
            sb.append(", mediationAgent: ");
            sb.append(this.o);
            TapjoyLog.i(str, sb.toString());
            tJPlacement.getListener().onRequestSuccess(tJPlacement);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(ErrorType errorType, TJError tJError) {
        a(a("REQUEST"), errorType, tJError);
    }

    /* access modifiers changed from: 0000 */
    public final void a(TJPlacement tJPlacement, ErrorType errorType, TJError tJError) {
        String str = f4243a;
        StringBuilder sb = new StringBuilder("Content request failed for placement ");
        sb.append(this.c.getPlacementName());
        sb.append("; Reason= ");
        sb.append(tJError.message);
        TapjoyLog.e(str, new TapjoyErrorMessage(errorType, sb.toString()));
        if (tJPlacement != null && tJPlacement.getListener() != null) {
            tJPlacement.getListener().onRequestFailure(tJPlacement, tJError);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (!this.x) {
            this.l = true;
            String str = f4243a;
            StringBuilder sb = new StringBuilder("Content is ready for placement ");
            sb.append(this.c.getPlacementName());
            TapjoyLog.i(str, sb.toString());
            if (this.g.isPrerendered()) {
                fx fxVar = this.f;
                String str2 = SettingsJsonConstants.ICON_PRERENDERED_KEY;
                Boolean valueOf = Boolean.valueOf(true);
                a aVar = fxVar.b;
                if (aVar != null) {
                    aVar.a(str2, (Object) valueOf);
                }
                a aVar2 = fxVar.e;
                if (aVar2 != null) {
                    aVar2.a(str2, (Object) valueOf);
                }
            }
            fx fxVar2 = this.f;
            a aVar3 = fxVar2.e;
            if (aVar3 != null) {
                fxVar2.e = null;
                aVar3.b().c();
            }
            TJPlacement a2 = a("REQUEST");
            if (a2 != null && a2.getListener() != null) {
                a2.getListener().onContentReady(a2);
                this.x = true;
            }
        }
    }

    public boolean isLimited() {
        return this.y;
    }

    /* access modifiers changed from: 0000 */
    public final String b() {
        if (!this.y) {
            return TapjoyConnectCore.getAppID();
        }
        return TapjoyConnectCore.getLimitedAppID();
    }

    static /* synthetic */ void e(TJCorePlacement tJCorePlacement) {
        TJPlacement a2 = tJCorePlacement.a("SHOW");
        String str = f4243a;
        StringBuilder sb = new StringBuilder("Handle onClick for placement ");
        sb.append(tJCorePlacement.c.getPlacementName());
        TapjoyLog.i(str, sb.toString());
        if (a2 != null && a2.getListener() != null) {
            a2.getListener().onClick(a2);
        }
    }

    static /* synthetic */ void j(TJCorePlacement tJCorePlacement) {
        tJCorePlacement.t = new fn(tJCorePlacement.c.getPlacementName(), tJCorePlacement.c.getPlacementType());
        tJCorePlacement.g.setAdContentTracker(tJCorePlacement.t);
    }

    static /* synthetic */ void a(TJCorePlacement tJCorePlacement, String str) {
        if (str != null) {
            try {
                String str2 = f4243a;
                StringBuilder sb = new StringBuilder("Disable preload flag is set for placement ");
                sb.append(tJCorePlacement.c.getPlacementName());
                TapjoyLog.d(str2, sb.toString());
                tJCorePlacement.c.setRedirectURL(new JSONObject(str).getString(TapjoyConstants.TJC_REDIRECT_URL));
                tJCorePlacement.c.setPreloadDisabled(true);
                tJCorePlacement.c.setHasProgressSpinner(true);
                String str3 = f4243a;
                StringBuilder sb2 = new StringBuilder("redirect_url:");
                sb2.append(tJCorePlacement.c.getRedirectURL());
                TapjoyLog.d(str3, sb2.toString());
            } catch (JSONException unused) {
                throw new TapjoyException("TJPlacement request failed, malformed server response");
            }
        } else {
            throw new TapjoyException("TJPlacement request failed due to null response");
        }
    }

    static /* synthetic */ void l(TJCorePlacement tJCorePlacement) {
        tJCorePlacement.k = true;
        tJCorePlacement.b(tJCorePlacement.a("REQUEST"));
    }
}
