package com.tapjoy;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.text.TextUtils;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.internal.fk;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TapjoyCache {
    public static final String CACHE_DIRECTORY_NAME = "Tapjoy/Cache/";
    public static final int CACHE_LIMIT = -1;

    /* renamed from: a reason: collision with root package name */
    private static TapjoyCache f4279a = null;
    public static boolean unit_test_mode = false;
    private Context b;
    /* access modifiers changed from: private */
    public TapjoyCacheMap c;
    /* access modifiers changed from: private */
    public Vector d;
    private ExecutorService e;
    /* access modifiers changed from: private */
    public File f;

    public class CacheAssetThread implements Callable {
        private URL b;
        private String c;
        private long d;

        public CacheAssetThread(URL url, String str, long j) {
            this.b = url;
            this.c = str;
            this.d = j;
            if (this.d <= 0) {
                this.d = 86400;
            }
            TapjoyCache.this.d.add(TapjoyCache.b(this.b.toString()));
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(19:12|13|14|15|16|(2:18|(1:20)(2:21|22))|23|24|25|26|27|28|29|30|31|32|(1:35)|36|37) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x0121 */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0139  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x01ad A[SYNTHETIC, Splitter:B:58:0x01ad] */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x01b2 A[SYNTHETIC, Splitter:B:62:0x01b2] */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x01e8 A[SYNTHETIC, Splitter:B:72:0x01e8] */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x01ed A[SYNTHETIC, Splitter:B:76:0x01ed] */
        /* JADX WARNING: Removed duplicated region for block: B:82:0x01f4 A[SYNTHETIC, Splitter:B:82:0x01f4] */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x01f9 A[SYNTHETIC, Splitter:B:86:0x01f9] */
        public Boolean call() {
            BufferedOutputStream bufferedOutputStream;
            String a2 = TapjoyCache.b(this.b.toString());
            if (TapjoyCache.this.c.containsKey(a2)) {
                if (new File(((TapjoyCachedAssetData) TapjoyCache.this.c.get(a2)).getLocalFilePath()).exists()) {
                    if (this.d != 0) {
                        ((TapjoyCachedAssetData) TapjoyCache.this.c.get(a2)).resetTimeToLive(this.d);
                    } else {
                        ((TapjoyCachedAssetData) TapjoyCache.this.c.get(a2)).resetTimeToLive(86400);
                    }
                    StringBuilder sb = new StringBuilder("Reseting time to live for ");
                    sb.append(this.b.toString());
                    TapjoyLog.d("TapjoyCache", sb.toString());
                    TapjoyCache.this.d.remove(a2);
                    return Boolean.valueOf(true);
                }
                TapjoyCache.getInstance().removeAssetFromCache(a2);
            }
            System.currentTimeMillis();
            try {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(TapjoyCache.this.f);
                sb2.append("/");
                sb2.append(TapjoyUtil.SHA256(a2));
                File file = new File(sb2.toString());
                StringBuilder sb3 = new StringBuilder("Downloading and caching asset from: ");
                sb3.append(this.b);
                sb3.append(" to ");
                sb3.append(file);
                TapjoyLog.d("TapjoyCache", sb3.toString());
                BufferedInputStream bufferedInputStream = null;
                try {
                    URLConnection a3 = fk.a(this.b);
                    a3.setConnectTimeout(15000);
                    a3.setReadTimeout(30000);
                    a3.connect();
                    if (a3 instanceof HttpURLConnection) {
                        int responseCode = ((HttpURLConnection) a3).getResponseCode();
                        if (responseCode != 200) {
                            StringBuilder sb4 = new StringBuilder("Unexpected response code: ");
                            sb4.append(responseCode);
                            throw new IOException(sb4.toString());
                        }
                    }
                    BufferedInputStream bufferedInputStream2 = new BufferedInputStream(a3.getInputStream());
                    try {
                        bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
                        try {
                            TapjoyUtil.writeFileToDevice(bufferedInputStream2, bufferedOutputStream);
                            bufferedInputStream2.close();
                            bufferedOutputStream.close();
                            TapjoyCachedAssetData tapjoyCachedAssetData = new TapjoyCachedAssetData(this.b.toString(), file.getAbsolutePath(), this.d);
                            if (this.c != null) {
                                tapjoyCachedAssetData.setOfferID(this.c);
                            }
                            TapjoyCache.this.c.put(a2, tapjoyCachedAssetData);
                            TapjoyCache.this.d.remove(a2);
                            StringBuilder sb5 = new StringBuilder("----- Download complete -----");
                            sb5.append(tapjoyCachedAssetData.toString());
                            TapjoyLog.d("TapjoyCache", sb5.toString());
                            return Boolean.valueOf(true);
                        } catch (SocketTimeoutException e) {
                            e = e;
                            bufferedInputStream = bufferedInputStream2;
                            String str = "TapjoyCache";
                            ErrorType errorType = ErrorType.NETWORK_ERROR;
                            StringBuilder sb6 = new StringBuilder("Network timeout during caching: ");
                            sb6.append(e.toString());
                            TapjoyLog.e(str, new TapjoyErrorMessage(errorType, sb6.toString()));
                            TapjoyCache.this.d.remove(a2);
                            TapjoyUtil.deleteFileOrDirectory(file);
                            Boolean valueOf = Boolean.valueOf(false);
                            if (bufferedInputStream != null) {
                                try {
                                    bufferedInputStream.close();
                                } catch (IOException unused) {
                                }
                            }
                            if (bufferedOutputStream != null) {
                                try {
                                    bufferedOutputStream.close();
                                } catch (IOException unused2) {
                                }
                            }
                            return valueOf;
                        } catch (Exception e2) {
                            e = e2;
                            bufferedInputStream = bufferedInputStream2;
                            String str2 = "TapjoyCache";
                            try {
                                StringBuilder sb7 = new StringBuilder("Error caching asset: ");
                                sb7.append(e.toString());
                                TapjoyLog.e(str2, sb7.toString());
                                TapjoyCache.this.d.remove(a2);
                                TapjoyUtil.deleteFileOrDirectory(file);
                                Boolean valueOf2 = Boolean.valueOf(false);
                                if (bufferedInputStream != null) {
                                    try {
                                        bufferedInputStream.close();
                                    } catch (IOException unused3) {
                                    }
                                }
                                if (bufferedOutputStream != null) {
                                    try {
                                        bufferedOutputStream.close();
                                    } catch (IOException unused4) {
                                    }
                                }
                                return valueOf2;
                            } catch (Throwable th) {
                                th = th;
                                if (bufferedInputStream != null) {
                                }
                                if (bufferedOutputStream != null) {
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            bufferedInputStream = bufferedInputStream2;
                            if (bufferedInputStream != null) {
                                try {
                                    bufferedInputStream.close();
                                } catch (IOException unused5) {
                                }
                            }
                            if (bufferedOutputStream != null) {
                                try {
                                    bufferedOutputStream.close();
                                } catch (IOException unused6) {
                                }
                            }
                            throw th;
                        }
                    } catch (SocketTimeoutException e3) {
                        e = e3;
                        bufferedOutputStream = null;
                        bufferedInputStream = bufferedInputStream2;
                        String str3 = "TapjoyCache";
                        ErrorType errorType2 = ErrorType.NETWORK_ERROR;
                        StringBuilder sb62 = new StringBuilder("Network timeout during caching: ");
                        sb62.append(e.toString());
                        TapjoyLog.e(str3, new TapjoyErrorMessage(errorType2, sb62.toString()));
                        TapjoyCache.this.d.remove(a2);
                        TapjoyUtil.deleteFileOrDirectory(file);
                        Boolean valueOf3 = Boolean.valueOf(false);
                        if (bufferedInputStream != null) {
                        }
                        if (bufferedOutputStream != null) {
                        }
                        return valueOf3;
                    } catch (Exception e4) {
                        e = e4;
                        bufferedOutputStream = null;
                        bufferedInputStream = bufferedInputStream2;
                        String str22 = "TapjoyCache";
                        StringBuilder sb72 = new StringBuilder("Error caching asset: ");
                        sb72.append(e.toString());
                        TapjoyLog.e(str22, sb72.toString());
                        TapjoyCache.this.d.remove(a2);
                        TapjoyUtil.deleteFileOrDirectory(file);
                        Boolean valueOf22 = Boolean.valueOf(false);
                        if (bufferedInputStream != null) {
                        }
                        if (bufferedOutputStream != null) {
                        }
                        return valueOf22;
                    } catch (Throwable th3) {
                        th = th3;
                        bufferedOutputStream = null;
                        bufferedInputStream = bufferedInputStream2;
                        if (bufferedInputStream != null) {
                        }
                        if (bufferedOutputStream != null) {
                        }
                        throw th;
                    }
                } catch (SocketTimeoutException e5) {
                    e = e5;
                    bufferedOutputStream = null;
                    String str32 = "TapjoyCache";
                    ErrorType errorType22 = ErrorType.NETWORK_ERROR;
                    StringBuilder sb622 = new StringBuilder("Network timeout during caching: ");
                    sb622.append(e.toString());
                    TapjoyLog.e(str32, new TapjoyErrorMessage(errorType22, sb622.toString()));
                    TapjoyCache.this.d.remove(a2);
                    TapjoyUtil.deleteFileOrDirectory(file);
                    Boolean valueOf32 = Boolean.valueOf(false);
                    if (bufferedInputStream != null) {
                    }
                    if (bufferedOutputStream != null) {
                    }
                    return valueOf32;
                } catch (Exception e6) {
                    e = e6;
                    bufferedOutputStream = null;
                    String str222 = "TapjoyCache";
                    StringBuilder sb722 = new StringBuilder("Error caching asset: ");
                    sb722.append(e.toString());
                    TapjoyLog.e(str222, sb722.toString());
                    TapjoyCache.this.d.remove(a2);
                    TapjoyUtil.deleteFileOrDirectory(file);
                    Boolean valueOf222 = Boolean.valueOf(false);
                    if (bufferedInputStream != null) {
                    }
                    if (bufferedOutputStream != null) {
                    }
                    return valueOf222;
                } catch (Throwable th4) {
                    th = th4;
                    bufferedOutputStream = null;
                    if (bufferedInputStream != null) {
                    }
                    if (bufferedOutputStream != null) {
                    }
                    throw th;
                }
            } catch (Exception unused7) {
                TapjoyCache.this.d.remove(a2);
                return Boolean.valueOf(false);
            }
        }
    }

    public TapjoyCache(Context context) {
        if (f4279a == null || unit_test_mode) {
            f4279a = this;
            this.b = context;
            this.c = new TapjoyCacheMap(context, -1);
            this.d = new Vector();
            this.e = Executors.newFixedThreadPool(5);
            if (Environment.getExternalStorageDirectory() != null) {
                TapjoyUtil.deleteFileOrDirectory(new File(Environment.getExternalStorageDirectory(), "tapjoy"));
                TapjoyUtil.deleteFileOrDirectory(new File(Environment.getExternalStorageDirectory(), "tjcache/tmp/"));
            }
            StringBuilder sb = new StringBuilder();
            sb.append(this.b.getFilesDir());
            sb.append("/Tapjoy/Cache/");
            this.f = new File(sb.toString());
            if (!this.f.exists()) {
                if (this.f.mkdirs()) {
                    StringBuilder sb2 = new StringBuilder("Created directory at: ");
                    sb2.append(this.f.getPath());
                    TapjoyLog.d("TapjoyCache", sb2.toString());
                } else {
                    TapjoyLog.e("TapjoyCache", "Error initalizing cache");
                    f4279a = null;
                }
            }
            a();
        }
    }

    private void a() {
        SharedPreferences sharedPreferences = this.b.getSharedPreferences(TapjoyConstants.PREF_TAPJOY_CACHE, 0);
        Editor edit = sharedPreferences.edit();
        for (Entry entry : sharedPreferences.getAll().entrySet()) {
            File file = new File((String) entry.getKey());
            if (!file.exists() || !file.isFile()) {
                StringBuilder sb = new StringBuilder("Removing reference to missing asset: ");
                sb.append((String) entry.getKey());
                TapjoyLog.d("TapjoyCache", sb.toString());
                edit.remove((String) entry.getKey()).apply();
            } else {
                TapjoyCachedAssetData fromRawJSONString = TapjoyCachedAssetData.fromRawJSONString(entry.getValue().toString());
                if (fromRawJSONString != null) {
                    StringBuilder sb2 = new StringBuilder("Loaded Asset: ");
                    sb2.append(fromRawJSONString.getAssetURL());
                    TapjoyLog.d("TapjoyCache", sb2.toString());
                    String b2 = b(fromRawJSONString.getAssetURL());
                    if (b2 == null || "".equals(b2) || b2.length() <= 0) {
                        TapjoyLog.e("TapjoyCache", "Removing asset because deserialization failed.");
                        edit.remove((String) entry.getKey()).apply();
                    } else if (fromRawJSONString.getTimeOfDeathInSeconds() < System.currentTimeMillis() / 1000) {
                        StringBuilder sb3 = new StringBuilder("Asset expired, removing from cache: ");
                        sb3.append(fromRawJSONString.getAssetURL());
                        TapjoyLog.d("TapjoyCache", sb3.toString());
                        if (fromRawJSONString.getLocalFilePath() != null && fromRawJSONString.getLocalFilePath().length() > 0) {
                            TapjoyUtil.deleteFileOrDirectory(new File(fromRawJSONString.getLocalFilePath()));
                        }
                    } else {
                        this.c.put(b2, fromRawJSONString);
                    }
                } else {
                    TapjoyLog.e("TapjoyCache", "Removing asset because deserialization failed.");
                    edit.remove((String) entry.getKey()).apply();
                }
            }
        }
    }

    public void cacheAssetGroup(final JSONArray jSONArray, final TJCacheListener tJCacheListener) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            if (tJCacheListener != null) {
                tJCacheListener.onCachingComplete(1);
            }
            return;
        }
        new Thread() {
            public final void run() {
                StringBuilder sb = new StringBuilder("Starting to cache asset group size of ");
                sb.append(jSONArray.length());
                TapjoyLog.d("TapjoyCache", sb.toString());
                ArrayList<Future> arrayList = new ArrayList<>();
                for (int i = 0; i < jSONArray.length(); i++) {
                    try {
                        Future cacheAssetFromJSONObject = TapjoyCache.this.cacheAssetFromJSONObject(jSONArray.getJSONObject(i));
                        if (cacheAssetFromJSONObject != null) {
                            arrayList.add(cacheAssetFromJSONObject);
                        }
                    } catch (JSONException unused) {
                        TapjoyLog.e("TapjoyCache", "Failed to load JSON object from JSONArray");
                    }
                }
                int i2 = 1;
                for (Future future : arrayList) {
                    try {
                        if (((Boolean) future.get()).booleanValue()) {
                        }
                    } catch (InterruptedException e) {
                        StringBuilder sb2 = new StringBuilder("Caching thread failed: ");
                        sb2.append(e.toString());
                        TapjoyLog.e("TapjoyCache", sb2.toString());
                    } catch (ExecutionException e2) {
                        StringBuilder sb3 = new StringBuilder("Caching thread failed: ");
                        sb3.append(e2.toString());
                        TapjoyLog.e("TapjoyCache", sb3.toString());
                    }
                    i2 = 2;
                }
                TapjoyLog.d("TapjoyCache", "Finished caching group");
                if (tJCacheListener != null) {
                    tJCacheListener.onCachingComplete(i2);
                }
            }
        }.start();
    }

    public Future cacheAssetFromJSONObject(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("url");
            Long.valueOf(86400);
            return cacheAssetFromURL(string, jSONObject.optString(TapjoyConstants.TJC_PLACEMENT_OFFER_ID), Long.valueOf(jSONObject.optLong(TapjoyConstants.TJC_TIME_TO_LIVE)).longValue());
        } catch (JSONException unused) {
            TapjoyLog.e("TapjoyCache", "Required parameters to cache an asset from JSON is not present");
            return null;
        }
    }

    public Future cacheAssetFromURL(String str, String str2, long j) {
        try {
            URL url = new URL(str);
            if (!this.d.contains(b(str))) {
                return startCachingThread(url, str2, j);
            }
            StringBuilder sb = new StringBuilder("URL is already in the process of being cached: ");
            sb.append(str);
            TapjoyLog.d("TapjoyCache", sb.toString());
            return null;
        } catch (MalformedURLException unused) {
            TapjoyLog.d("TapjoyCache", "Invalid cache assetURL");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static String b(String str) {
        if (str.startsWith("//")) {
            StringBuilder sb = new StringBuilder("http:");
            sb.append(str);
            str = sb.toString();
        }
        try {
            return new URL(str).getFile();
        } catch (MalformedURLException unused) {
            StringBuilder sb2 = new StringBuilder("Invalid URL ");
            sb2.append(str);
            TapjoyLog.e("TapjoyCache", sb2.toString());
            return "";
        }
    }

    public Future startCachingThread(URL url, String str, long j) {
        if (url == null) {
            return null;
        }
        ExecutorService executorService = this.e;
        CacheAssetThread cacheAssetThread = new CacheAssetThread(url, str, j);
        return executorService.submit(cacheAssetThread);
    }

    public void clearTapjoyCache() {
        TapjoyLog.d("TapjoyCache", "Cleaning Tapjoy cache!");
        TapjoyUtil.deleteFileOrDirectory(this.f);
        if (this.f.mkdirs()) {
            StringBuilder sb = new StringBuilder("Created new cache directory at: ");
            sb.append(this.f.getPath());
            TapjoyLog.d("TapjoyCache", sb.toString());
        }
        this.c = new TapjoyCacheMap(this.b, -1);
    }

    public boolean removeAssetFromCache(String str) {
        String b2 = b(str);
        return (b2 == "" || this.c.remove((Object) b2) == null) ? false : true;
    }

    public boolean isURLDownloading(String str) {
        if (this.d == null) {
            return false;
        }
        String b2 = b(str);
        if (b2 == "" || !this.d.contains(b2)) {
            return false;
        }
        return true;
    }

    public boolean isURLCached(String str) {
        return this.c.get(b(str)) != null;
    }

    public TapjoyCachedAssetData getCachedDataForURL(String str) {
        String b2 = b(str);
        if (b2 != "") {
            return (TapjoyCachedAssetData) this.c.get(b2);
        }
        return null;
    }

    public TapjoyCacheMap getCachedData() {
        return this.c;
    }

    public String getPathOfCachedURL(String str) {
        String b2 = b(str);
        if (b2 == "" || !this.c.containsKey(b2)) {
            return str;
        }
        TapjoyCachedAssetData tapjoyCachedAssetData = (TapjoyCachedAssetData) this.c.get(b2);
        if (new File(tapjoyCachedAssetData.getLocalFilePath()).exists()) {
            return tapjoyCachedAssetData.getLocalURL();
        }
        getInstance().removeAssetFromCache(str);
        return str;
    }

    public String cachedAssetsToJSON() {
        JSONObject jSONObject = new JSONObject();
        for (Entry entry : this.c.entrySet()) {
            try {
                jSONObject.put(((String) entry.getKey()).toString(), ((TapjoyCachedAssetData) entry.getValue()).toRawJSONString());
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return jSONObject.toString();
    }

    public String getCachedOfferIDs() {
        String str = "";
        ArrayList arrayList = new ArrayList();
        if (this.c == null) {
            return str;
        }
        for (Entry value : this.c.entrySet()) {
            String offerId = ((TapjoyCachedAssetData) value.getValue()).getOfferId();
            if (!(offerId == null || offerId.length() == 0 || arrayList.contains(offerId))) {
                arrayList.add(offerId);
            }
        }
        return TextUtils.join(",", arrayList);
    }

    public void printCacheInformation() {
        TapjoyLog.d("TapjoyCache", "------------- Cache Data -------------");
        StringBuilder sb = new StringBuilder("Number of files in cache: ");
        sb.append(this.c.size());
        TapjoyLog.d("TapjoyCache", sb.toString());
        StringBuilder sb2 = new StringBuilder("Cache Size: ");
        sb2.append(TapjoyUtil.fileOrDirectorySize(this.f));
        TapjoyLog.d("TapjoyCache", sb2.toString());
        TapjoyLog.d("TapjoyCache", "--------------------------------------");
    }

    public static TapjoyCache getInstance() {
        return f4279a;
    }

    public static void setInstance(TapjoyCache tapjoyCache) {
        f4279a = tapjoyCache;
    }
}
