package com.tapjoy;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.internal.jr;
import java.util.Arrays;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;

public class TJSplitWebView extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public TJWebView f4263a = new TJWebView(this.k);
    @Nullable
    private a b;
    @Nullable
    private a c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    @Nullable
    public String f;
    /* access modifiers changed from: private */
    @Nullable
    public Uri g;
    /* access modifiers changed from: private */
    public HashSet h;
    private JSONObject i;
    private TJAdUnitJSBridge j;
    /* access modifiers changed from: private */
    public Context k;
    /* access modifiers changed from: private */
    public Boolean l;
    private RelativeLayout m;
    private FrameLayout n;
    /* access modifiers changed from: private */
    public ProgressBar o;
    /* access modifiers changed from: private */
    public TextView p;
    private TJImageButton q;
    private TJImageButton r;
    private String s;
    /* access modifiers changed from: private */
    public boolean t;
    private String u;
    private String v;
    private String w;

    static class a {

        /* renamed from: a reason: collision with root package name */
        final double f4272a;
        final double b;
        final double c;
        final double d;
        final float e;

        a(JSONObject jSONObject) {
            this.f4272a = jSONObject.optDouble("width", Utils.DOUBLE_EPSILON);
            this.b = jSONObject.optDouble("height", Utils.DOUBLE_EPSILON);
            this.c = jSONObject.optDouble("left", Utils.DOUBLE_EPSILON);
            this.d = jSONObject.optDouble(String.TOP, Utils.DOUBLE_EPSILON);
            this.e = (float) jSONObject.optDouble("cornerRadius", Utils.DOUBLE_EPSILON);
        }
    }

    class b extends WebViewClient {
        private b() {
        }

        /* synthetic */ b(TJSplitWebView tJSplitWebView, byte b) {
            this();
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            if (TJSplitWebView.this.l.booleanValue()) {
                TJSplitWebView.this.p.setText(TapjoyUrlFormatter.getDomain(str));
                TJSplitWebView.this.o.setVisibility(0);
            }
            StringBuilder sb = new StringBuilder("onPageStarted: ");
            sb.append(str);
            TapjoyLog.d("TJSplitWebView", sb.toString());
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            StringBuilder sb = new StringBuilder("shouldOverrideUrlLoading: ");
            sb.append(str);
            TapjoyLog.d("TJSplitWebView", sb.toString());
            Uri parse = Uri.parse(str);
            if (parse != null) {
                String host = parse.getHost();
                String scheme = parse.getScheme();
                if (!(scheme == null || host == null || ((!scheme.equals("http") && !scheme.equals("https")) || (TJSplitWebView.this.h != null && TJSplitWebView.this.h.contains(host))))) {
                    TJSplitWebView.this.e = str;
                    return false;
                }
            }
            try {
                TJSplitWebView.this.k.startActivity(new Intent("android.intent.action.VIEW", parse));
                if (!TJSplitWebView.this.l.booleanValue()) {
                    TJSplitWebView.this.a();
                }
            } catch (Exception e) {
                TapjoyLog.e("TJSplitWebView", e.getMessage());
            }
            return true;
        }

        @Nullable
        public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            String h = TJSplitWebView.this.f;
            Uri i = TJSplitWebView.this.g;
            if (!(h == null || i == null || str == null || !str.startsWith(h))) {
                TJSplitWebView.this.k.startActivity(new Intent("android.intent.action.VIEW", i));
                TJSplitWebView.this.a();
            }
            return super.shouldInterceptRequest(webView, str);
        }

        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            StringBuilder sb = new StringBuilder("onReceivedError: ");
            sb.append(str2);
            sb.append(" firstUrl:");
            sb.append(TJSplitWebView.this.d);
            TapjoyLog.d("TJSplitWebView", sb.toString());
            if (TJSplitWebView.this.t) {
                TJSplitWebView.this.showErrorDialog();
                return;
            }
            if (str2.equals(TJSplitWebView.this.d)) {
                TJSplitWebView.this.a();
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (TJSplitWebView.this.l.booleanValue()) {
                if (TJSplitWebView.this.o != null) {
                    TJSplitWebView.this.o.setProgress(0);
                    TJSplitWebView.this.o.setVisibility(8);
                }
                TJSplitWebView.this.isFirstOrLastPage();
            }
        }
    }

    @TargetApi(21)
    public TJSplitWebView(Context context, JSONObject jSONObject, TJAdUnitJSBridge tJAdUnitJSBridge) {
        super(context);
        this.j = tJAdUnitJSBridge;
        this.k = context;
        JSONObject optJSONObject = jSONObject.optJSONObject(String.SPLIT_VIEW_LAYOUT);
        JSONArray optJSONArray = jSONObject.optJSONArray(String.SPLIT_VIEW_EXIT_HOSTS);
        JSONObject optJSONObject2 = jSONObject.optJSONObject(String.SPLIT_VIEW_ERROR_DIALOG_STRINGS);
        this.s = jSONObject.optString(String.SPLIT_VIEW_URL_FOR_EXTERNAL_OPEN);
        this.i = jSONObject.optJSONObject(String.SPLIT_VIEW_ANIMATION);
        setLayoutOption(optJSONObject);
        setExitHosts(optJSONArray);
        setErrorDialog(optJSONObject2);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        this.f4263a.setId(TapjoyUtil.generateViewId());
        this.f4263a.setBackgroundColor(-1);
        WebSettings settings = this.f4263a.getSettings();
        if (settings != null) {
            settings.setUseWideViewPort(true);
        }
        this.f4263a.setWebViewClient(new b(this, 0));
        this.l = Boolean.valueOf(jSONObject.optBoolean(String.SPLIT_VIEW_SHOW_TOOLBAR));
        if (this.l.booleanValue()) {
            addToolbar();
            addLineBreak();
            addProgressBar();
            this.f4263a.setWebChromeClient(new WebChromeClient() {
                public final void onProgressChanged(WebView webView, int i) {
                    super.onProgressChanged(webView, i);
                    TJSplitWebView.this.o.setProgress(i);
                    TJSplitWebView.this.isFirstOrLastPage();
                }
            });
        }
        addView(this.f4263a, layoutParams);
    }

    @TargetApi(21)
    public void addToolbar() {
        this.m = new RelativeLayout(this.k);
        this.m.setId(TapjoyUtil.generateViewId());
        LayoutParams layoutParams = new LayoutParams(-1, (int) (new TapjoyDisplayMetricsUtil(getContext()).getScreenDensityScale() * 40.0f));
        layoutParams.addRule(6);
        this.m.setBackgroundColor(-1);
        this.m.setVisibility(0);
        setupToolbarUI();
        addView(this.m, layoutParams);
    }

    @TargetApi(21)
    public void addProgressBar() {
        this.o = new ProgressBar(this.k, null, 16842872);
        this.o.setMax(100);
        this.o.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#5d95ff")));
        this.o.setProgressBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#dddddd")));
        addView(this.o);
    }

    @TargetApi(21)
    public void addLineBreak() {
        this.n = new FrameLayout(getContext());
        this.n.setBackgroundColor(Color.parseColor("#dddddd"));
        addView(this.n);
    }

    @TargetApi(21)
    public void setupToolbarUI() {
        float screenDensityScale = new TapjoyDisplayMetricsUtil(getContext()).getScreenDensityScale();
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        this.q = new TJImageButton(this.k);
        this.q.setId(TapjoyUtil.generateViewId());
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(15);
        layoutParams.addRule(9);
        int i2 = (int) (10.0f * screenDensityScale);
        layoutParams.setMargins(i2, i2, i2, i2);
        int i3 = (int) (5.0f * screenDensityScale);
        this.q.setPadding(i3, i2, i2, i2);
        this.q.setEnabledImageBitmap(TapjoyIcons.getBackEnabledImage(screenDensityScale));
        this.q.setDisableImageBitmap(TapjoyIcons.getBackDisabledImage(screenDensityScale));
        this.q.setBackgroundColor(0);
        this.q.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                if (TJSplitWebView.this.f4263a.canGoBack()) {
                    TJSplitWebView.this.f4263a.goBack();
                }
            }
        });
        relativeLayout.addView(this.q, layoutParams);
        this.r = new TJImageButton(this.k);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(15);
        layoutParams2.addRule(1, this.q.getId());
        layoutParams2.setMargins(i2, i2, i2, i2);
        this.r.setPadding(i2, i2, i3, i2);
        this.r.setEnabledImageBitmap(TapjoyIcons.getForwardEnabledImage(screenDensityScale));
        this.r.setDisableImageBitmap(TapjoyIcons.getForwardDisabledImage(screenDensityScale));
        this.r.setBackgroundColor(0);
        this.r.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                TJSplitWebView.this.f4263a.goForward();
            }
        });
        relativeLayout.addView(this.r, layoutParams2);
        ImageButton imageButton = new ImageButton(this.k);
        LayoutParams layoutParams3 = new LayoutParams(-2, -2);
        layoutParams3.addRule(15);
        layoutParams3.addRule(11);
        layoutParams3.setMargins(i2, i2, i2, i2);
        imageButton.setPadding(i3, i3, i3, i3);
        imageButton.setImageBitmap(TapjoyIcons.getCloseImage(screenDensityScale));
        imageButton.setBackgroundColor(0);
        imageButton.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                TJSplitWebView.this.a();
            }
        });
        relativeLayout.addView(imageButton, layoutParams3);
        this.p = new TextView(this.k);
        this.p.setId(TapjoyUtil.generateViewId());
        LayoutParams layoutParams4 = new LayoutParams(-2, -2);
        layoutParams4.addRule(13);
        this.p.setMaxLines(1);
        this.p.setMaxEms(Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        this.p.setTextAlignment(4);
        this.p.setTextColor(Color.parseColor("#5d95ff"));
        this.p.setBackgroundColor(0);
        this.p.setEnabled(false);
        this.p.setTypeface(Typeface.create("sans-serif-medium", 0));
        relativeLayout.addView(this.p, layoutParams4);
        ImageButton imageButton2 = new ImageButton(this.k);
        imageButton2.setId(TapjoyUtil.generateViewId());
        LayoutParams layoutParams5 = new LayoutParams(-2, -2);
        layoutParams5.addRule(1, this.p.getId());
        layoutParams5.addRule(15);
        imageButton2.setPadding(i3, i3, i3, i3);
        imageButton2.setImageBitmap(TapjoyIcons.getOpenBrowserImage(screenDensityScale));
        imageButton2.setBackgroundColor(0);
        imageButton2.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                TJSplitWebView.this.openInExternalBrowser();
            }
        });
        relativeLayout.addView(imageButton2, layoutParams5);
        this.m.addView(relativeLayout, new LayoutParams(-1, -2));
    }

    public void openInExternalBrowser() {
        Uri uri;
        if (jr.c(this.s)) {
            uri = Uri.parse(this.f4263a.getUrl());
            if (uri == null) {
                uri = Uri.parse(getLastUrl());
            }
        } else {
            uri = Uri.parse(this.s);
        }
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(uri);
        intent.addFlags(268435456);
        if (this.f4263a.getContext() != null) {
            try {
                this.f4263a.getContext().startActivity(intent);
            } catch (Exception e2) {
                TapjoyLog.d("TJSplitWebView", e2.getMessage());
            }
        }
    }

    public boolean goBack() {
        if (!this.f4263a.canGoBack()) {
            return false;
        }
        this.f4263a.goBack();
        return true;
    }

    public void isFirstOrLastPage() {
        this.q.setEnabled(this.f4263a.canGoBack());
        this.r.setEnabled(this.f4263a.canGoForward());
    }

    @TargetApi(21)
    public void showErrorDialog() {
        new Builder(this.k, 16974394).setMessage(this.u).setPositiveButton(this.v, new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        }).setNegativeButton(this.w, new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                if (jr.c(TJSplitWebView.this.getLastUrl())) {
                    TJSplitWebView.this.loadUrl(TJSplitWebView.this.d);
                } else {
                    TJSplitWebView.this.loadUrl(TJSplitWebView.this.getLastUrl());
                }
                dialogInterface.cancel();
            }
        }).create().show();
    }

    public void applyLayoutOption(JSONObject jSONObject) {
        setLayoutOption(jSONObject);
        a(getWidth(), getHeight());
    }

    public void setExitHosts(JSONArray jSONArray) {
        if (jSONArray == null) {
            this.h = null;
            return;
        }
        this.h = new HashSet();
        for (int i2 = 0; i2 <= jSONArray.length(); i2++) {
            String optString = jSONArray.optString(i2);
            if (optString != null) {
                this.h.add(optString);
            }
        }
    }

    public void setErrorDialog(JSONObject jSONObject) {
        if (jSONObject != null) {
            this.t = true;
            this.u = jSONObject.optString("description");
            this.v = jSONObject.optString("close");
            this.w = jSONObject.optString("reload");
        }
    }

    public void setUserAgent(String str) {
        this.f4263a.getSettings().setUserAgentString(str);
    }

    public void setTrigger(@Nullable String str, @Nullable String str2) {
        this.f = jr.b(str);
        this.g = str2 != null ? Uri.parse(str2) : null;
    }

    public void loadUrl(String str) {
        if (this.f4263a != null) {
            this.d = str;
            this.e = str;
            this.f4263a.loadUrl(str);
        }
    }

    public String getLastUrl() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.j.dismissSplitView(null, null);
    }

    /* access modifiers changed from: protected */
    public void setLayoutOption(JSONObject jSONObject) {
        if (jSONObject != null) {
            JSONObject optJSONObject = jSONObject.optJSONObject("landscape");
            a aVar = null;
            this.c = optJSONObject != null ? new a(optJSONObject) : null;
            JSONObject optJSONObject2 = jSONObject.optJSONObject("portrait");
            if (optJSONObject2 != null) {
                aVar = new a(optJSONObject2);
            }
            this.b = aVar;
        }
    }

    private void a(int i2, int i3) {
        a aVar = i2 <= i3 ? this.b : this.c;
        if (aVar == null) {
            this.f4263a.setVisibility(4);
            return;
        }
        double d2 = (double) i2;
        double d3 = aVar.f4272a;
        Double.isNaN(d2);
        int i4 = (int) (d3 * d2);
        double d4 = (double) i3;
        double d5 = aVar.b;
        Double.isNaN(d4);
        int i5 = (int) (d5 * d4);
        if (i4 == 0 || i5 == 0) {
            this.f4263a.setVisibility(4);
            return;
        }
        double d6 = aVar.c;
        Double.isNaN(d2);
        int i6 = (int) (d2 * d6);
        double d7 = aVar.d;
        Double.isNaN(d4);
        int i7 = (int) (d4 * d7);
        int i8 = (i2 - i4) - i6;
        int i9 = (i3 - i5) - i7;
        LayoutParams layoutParams = (LayoutParams) this.f4263a.getLayoutParams();
        layoutParams.width = i4;
        layoutParams.height = i5;
        if (this.l == null || !this.l.booleanValue()) {
            layoutParams.setMargins(i6, i7, i8, i9);
        } else {
            float screenDensityScale = new TapjoyDisplayMetricsUtil(getContext()).getScreenDensityScale();
            int height = ((int) (40.0f * screenDensityScale)) + this.n.getHeight();
            LayoutParams layoutParams2 = (LayoutParams) this.m.getLayoutParams();
            layoutParams2.setMargins(i6, i7, i8, i9);
            this.m.setLayoutParams(layoutParams2);
            layoutParams.setMargins(i6, i7 + height, i8, i9);
            LayoutParams layoutParams3 = new LayoutParams(i4, (int) screenDensityScale);
            layoutParams3.setMargins(i6, layoutParams.topMargin - this.o.getHeight(), i8, i9);
            this.o.setLayoutParams(layoutParams3);
            this.n.setLayoutParams(layoutParams3);
        }
        this.f4263a.setLayoutParams(layoutParams);
        this.f4263a.setVisibility(0);
        if (VERSION.SDK_INT >= 21) {
            if (aVar.e > 0.0f) {
                float[] fArr = new float[8];
                final float f2 = aVar.e * getResources().getDisplayMetrics().density;
                if (this.l == null || !this.l.booleanValue()) {
                    Arrays.fill(fArr, f2);
                    ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(fArr, null, null));
                    shapeDrawable.getPaint().setColor(-1);
                    this.f4263a.setBackground(shapeDrawable);
                    this.f4263a.setClipToOutline(true);
                    return;
                }
                this.m.setOutlineProvider(new ViewOutlineProvider() {
                    public final void getOutline(View view, Outline outline) {
                        outline.setRoundRect(0, 0, view.getWidth(), (int) (((float) view.getHeight()) + f2), f2);
                    }
                });
                this.m.setClipToOutline(true);
                return;
            }
            this.f4263a.setBackground(null);
            this.f4263a.setClipToOutline(false);
            if (this.l != null && this.l.booleanValue()) {
                this.m.setClipToOutline(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        a(MeasureSpec.getSize(i2), MeasureSpec.getSize(i3));
        super.onMeasure(i2, i3);
    }

    public void animateOpen(ViewGroup viewGroup) {
        if (this.i != null && this.i.has(String.ANIMATION_EVENT_ON_OPEN) && this.i.optString(String.ANIMATION_EVENT_ON_OPEN).equalsIgnoreCase(String.ANIMATION_TYPE_SLIDE_UP)) {
            setY((float) viewGroup.getHeight());
            animate().translationY(0.0f);
        }
    }
}
