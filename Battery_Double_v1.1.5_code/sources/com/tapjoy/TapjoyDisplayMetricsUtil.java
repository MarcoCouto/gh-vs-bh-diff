package com.tapjoy;

import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class TapjoyDisplayMetricsUtil {

    /* renamed from: a reason: collision with root package name */
    private Context f4288a;
    private Configuration b;
    private DisplayMetrics c = new DisplayMetrics();

    public TapjoyDisplayMetricsUtil(Context context) {
        this.f4288a = context;
        ((WindowManager) this.f4288a.getSystemService("window")).getDefaultDisplay().getMetrics(this.c);
        this.b = this.f4288a.getResources().getConfiguration();
    }

    public int getScreenDensityDPI() {
        return this.c.densityDpi;
    }

    public float getScreenDensityScale() {
        return this.c.density;
    }

    public int getScreenLayoutSize() {
        return this.b.screenLayout & 15;
    }
}
