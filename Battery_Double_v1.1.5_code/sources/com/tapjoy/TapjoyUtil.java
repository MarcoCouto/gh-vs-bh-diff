package com.tapjoy;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TJAdUnitConstants.String;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import javax.xml.parsers.DocumentBuilderFactory;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TapjoyUtil {

    /* renamed from: a reason: collision with root package name */
    private static HashMap f4293a = new HashMap();
    private static final AtomicInteger b = new AtomicInteger(1);

    public static String SHA1(String str) {
        return a(CommonUtils.SHA1_INSTANCE, str);
    }

    public static String SHA256(String str) {
        return a(CommonUtils.SHA256_INSTANCE, str);
    }

    private static String a(String str, String str2) {
        MessageDigest instance = MessageDigest.getInstance(str);
        instance.update(str2.getBytes("iso-8859-1"), 0, str2.length());
        return convertToHex(instance.digest());
    }

    public static String convertToHex(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = (bArr[i] >>> 4) & 15;
            int i2 = 0;
            while (true) {
                if (b2 < 0 || b2 > 9) {
                    stringBuffer.append((char) ((b2 - 10) + 97));
                } else {
                    stringBuffer.append((char) (b2 + 48));
                }
                b2 = bArr[i] & 15;
                int i3 = i2 + 1;
                if (i2 > 0) {
                    break;
                }
                i2 = i3;
            }
        }
        return stringBuffer.toString();
    }

    public static Document buildDocument(String str) {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            return newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("buildDocument exception: ");
            sb.append(e.toString());
            TapjoyLog.e("TapjoyUtil", sb.toString());
            return null;
        }
    }

    public static String getNodeTrimValue(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        String str = "";
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            Node item = childNodes.item(i);
            if (item != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(item.getNodeValue());
                str = sb.toString();
            }
        }
        if (str == null || str.equals("")) {
            return null;
        }
        return str.trim();
    }

    public static void deleteFileOrDirectory(File file) {
        if (file != null) {
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (listFiles != null && listFiles.length > 0) {
                    for (File deleteFileOrDirectory : listFiles) {
                        deleteFileOrDirectory(deleteFileOrDirectory);
                    }
                }
            }
            TapjoyLog.d("TapjoyUtil", "****************************************");
            StringBuilder sb = new StringBuilder("deleteFileOrDirectory: ");
            sb.append(file.getAbsolutePath());
            TapjoyLog.d("TapjoyUtil", sb.toString());
            TapjoyLog.d("TapjoyUtil", "****************************************");
            file.delete();
        }
    }

    public static long fileOrDirectorySize(File file) {
        File[] listFiles;
        long j;
        long j2 = 0;
        for (File file2 : file.listFiles()) {
            if (file2.isFile()) {
                j = file2.length();
            } else {
                j = fileOrDirectorySize(file2);
            }
            j2 += j;
        }
        return j2;
    }

    public static void writeFileToDevice(BufferedInputStream bufferedInputStream, OutputStream outputStream) {
        byte[] bArr = new byte[1024];
        while (true) {
            int read = bufferedInputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static Bitmap createBitmapFromView(View view) {
        Bitmap bitmap = null;
        if (view == null || view.getLayoutParams().width <= 0 || view.getLayoutParams().height <= 0) {
            return null;
        }
        try {
            Bitmap createBitmap = Bitmap.createBitmap(view.getLayoutParams().width, view.getLayoutParams().height, Config.ARGB_8888);
            try {
                Canvas canvas = new Canvas(createBitmap);
                view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
                view.draw(canvas);
                return createBitmap;
            } catch (Exception e) {
                e = e;
                bitmap = createBitmap;
                StringBuilder sb = new StringBuilder("error creating bitmap: ");
                sb.append(e.toString());
                TapjoyLog.d("TapjoyUtil", sb.toString());
                return bitmap;
            }
        } catch (Exception e2) {
            e = e2;
            StringBuilder sb2 = new StringBuilder("error creating bitmap: ");
            sb2.append(e.toString());
            TapjoyLog.d("TapjoyUtil", sb2.toString());
            return bitmap;
        }
    }

    public static View scaleDisplayAd(View view, int i) {
        int i2 = view.getLayoutParams().width;
        int i3 = view.getLayoutParams().height;
        StringBuilder sb = new StringBuilder("wxh: ");
        sb.append(i2);
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(i3);
        TapjoyLog.d("TapjoyUtil", sb.toString());
        if (i2 > i) {
            int intValue = Double.valueOf(Double.valueOf(Double.valueOf((double) i).doubleValue() / Double.valueOf((double) i2).doubleValue()).doubleValue() * 100.0d).intValue();
            WebView webView = (WebView) view;
            webView.getSettings().setSupportZoom(true);
            webView.setPadding(0, 0, 0, 0);
            webView.setVerticalScrollBarEnabled(false);
            webView.setHorizontalScrollBarEnabled(false);
            webView.setInitialScale(intValue);
            view.setLayoutParams(new LayoutParams(i, (i3 * i) / i2));
        }
        return view;
    }

    public static void safePut(Map map, String str, String str2, boolean z) {
        if (str != null && str.length() > 0 && str2 != null && str2.length() > 0) {
            if (z) {
                map.put(Uri.encode(str), Uri.encode(str2));
                return;
            }
            map.put(str, str2);
        }
    }

    public static void safePut(Map map, String str, Number number) {
        if (str != null && str.length() > 0 && number != null) {
            map.put(str, number.toString());
        }
    }

    public static String convertURLParams(Map map, boolean z) {
        String str = "";
        for (Entry entry : map.entrySet()) {
            if (str.length() > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(RequestParameters.AMPERSAND);
                str = sb.toString();
            }
            if (z) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(Uri.encode((String) entry.getKey()));
                sb2.append(RequestParameters.EQUAL);
                sb2.append(Uri.encode((String) entry.getValue()));
                str = sb2.toString();
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append((String) entry.getKey());
                sb3.append(RequestParameters.EQUAL);
                sb3.append((String) entry.getValue());
                str = sb3.toString();
            }
        }
        return str;
    }

    public static Map convertURLParams(String str, boolean z) {
        HashMap hashMap = new HashMap();
        String str2 = "";
        String str3 = "";
        int i = 0;
        boolean z2 = false;
        while (i < str.length() && i != -1) {
            char charAt = str.charAt(i);
            if (!z2) {
                if (charAt == '=') {
                    if (z) {
                        str2 = Uri.decode(str2);
                    }
                    str3 = str2;
                    str2 = "";
                    z2 = true;
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str2);
                    sb.append(charAt);
                    str2 = sb.toString();
                }
            } else if (z2) {
                if (charAt == '&') {
                    if (z) {
                        str2 = Uri.decode(str2);
                    }
                    hashMap.put(str3, str2);
                    str2 = "";
                    z2 = false;
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append(charAt);
                    str2 = sb2.toString();
                }
            }
            i++;
        }
        if (z2 && str2.length() > 0) {
            if (z) {
                str2 = Uri.decode(str2);
            }
            hashMap.put(str3, str2);
        }
        return hashMap;
    }

    public static String copyTextFromJarIntoString(String str) {
        return copyTextFromJarIntoString(str, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ac A[SYNTHETIC, Splitter:B:45:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b4 A[SYNTHETIC, Splitter:B:52:0x00b4] */
    public static Bitmap loadBitmapFromJar(String str, Context context) {
        Bitmap bitmap;
        InputStream inputStream;
        InputStream inputStream2;
        Options options = new Options();
        options.inJustDecodeBounds = true;
        byte[] bArr = (byte[]) getResource(str);
        InputStream inputStream3 = null;
        if (bArr != null) {
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        } else {
            bitmap = null;
        }
        if (bitmap == null) {
            StringBuilder sb = new StringBuilder("com/tapjoy/res/");
            sb.append(str);
            String sb2 = sb.toString();
            try {
                URL resource = TapjoyUtil.class.getClassLoader().getResource(sb2);
                if (resource == null) {
                    AssetManager assets = context.getAssets();
                    InputStream open = assets.open(sb2);
                    try {
                        BitmapFactory.decodeStream(open, null, options);
                        inputStream2 = assets.open(sb2);
                    } catch (Exception e) {
                        e = e;
                        inputStream = open;
                        try {
                            e.printStackTrace();
                            if (inputStream != null) {
                            }
                            return null;
                        } catch (Throwable th) {
                            th = th;
                            inputStream3 = inputStream;
                            if (inputStream3 != null) {
                                try {
                                    inputStream3.close();
                                } catch (IOException unused) {
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        inputStream3 = open;
                        if (inputStream3 != null) {
                        }
                        throw th;
                    }
                } else {
                    String file = resource.getFile();
                    if (file.startsWith("jar:")) {
                        file = file.substring(4);
                    }
                    if (file.startsWith("file:")) {
                        file = file.substring(5);
                    }
                    int indexOf = file.indexOf("!");
                    if (indexOf > 0) {
                        file = file.substring(0, indexOf);
                    }
                    JarFile jarFile = new JarFile(file);
                    JarEntry jarEntry = jarFile.getJarEntry(sb2);
                    inputStream = jarFile.getInputStream(jarEntry);
                    try {
                        BitmapFactory.decodeStream(inputStream, null, options);
                        inputStream2 = jarFile.getInputStream(jarEntry);
                    } catch (Exception e2) {
                        e = e2;
                        e.printStackTrace();
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException unused2) {
                            }
                        }
                        return null;
                    }
                }
                try {
                    bitmap = BitmapFactory.decodeStream(inputStream2);
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException unused3) {
                        }
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    inputStream = inputStream2;
                    e = exc;
                    e.printStackTrace();
                    if (inputStream != null) {
                    }
                    return null;
                } catch (Throwable th3) {
                    inputStream3 = inputStream2;
                    th = th3;
                    if (inputStream3 != null) {
                    }
                    throw th;
                }
            } catch (Exception e4) {
                e = e4;
                inputStream = null;
                e.printStackTrace();
                if (inputStream != null) {
                }
                return null;
            } catch (Throwable th4) {
                th = th4;
                if (inputStream3 != null) {
                }
                throw th;
            }
        }
        float deviceScreenDensityScale = TapjoyConnectCore.getDeviceScreenDensityScale();
        if (bitmap != null) {
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) (((float) options.outWidth) * deviceScreenDensityScale), (int) (((float) options.outHeight) * deviceScreenDensityScale), true);
        }
        return bitmap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x009f A[SYNTHETIC, Splitter:B:36:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a7 A[SYNTHETIC, Splitter:B:43:0x00a7] */
    public static String copyTextFromJarIntoString(String str, Context context) {
        InputStream inputStream;
        InputStream inputStream2;
        Exception e;
        Throwable th;
        byte[] bArr = new byte[1024];
        StringBuffer stringBuffer = new StringBuffer();
        URL resource = TapjoyUtil.class.getClassLoader().getResource(str);
        InputStream inputStream3 = null;
        if (context == null || resource != null) {
            String file = resource.getFile();
            if (file.startsWith("jar:")) {
                file = file.substring(4);
            }
            if (file.startsWith("file:")) {
                file = file.substring(5);
            }
            int indexOf = file.indexOf("!");
            if (indexOf > 0) {
                file = file.substring(0, indexOf);
            }
            JarFile jarFile = new JarFile(file);
            inputStream = jarFile.getInputStream(jarFile.getJarEntry(str));
        } else {
            try {
                inputStream = context.getAssets().open(str);
            } catch (Exception e2) {
                e = e2;
                inputStream2 = null;
                String str2 = "TapjoyUtil";
                try {
                    StringBuilder sb = new StringBuilder("file exception: ");
                    sb.append(e.toString());
                    TapjoyLog.d(str2, sb.toString());
                    e.printStackTrace();
                    if (inputStream2 != null) {
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    inputStream3 = inputStream2;
                    if (inputStream3 != null) {
                        try {
                            inputStream3.close();
                        } catch (Exception unused) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                if (inputStream3 != null) {
                }
                throw th;
            }
        }
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                stringBuffer.append(new String(bArr).substring(0, read));
            } catch (Exception e3) {
                Exception exc = e3;
                inputStream2 = inputStream;
                e = exc;
                String str22 = "TapjoyUtil";
                StringBuilder sb2 = new StringBuilder("file exception: ");
                sb2.append(e.toString());
                TapjoyLog.d(str22, sb2.toString());
                e.printStackTrace();
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    } catch (Exception unused2) {
                    }
                }
                return null;
            } catch (Throwable th4) {
                inputStream3 = inputStream;
                th = th4;
                if (inputStream3 != null) {
                }
                throw th;
            }
        }
        String stringBuffer2 = stringBuffer.toString();
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception unused3) {
            }
        }
        return stringBuffer2;
    }

    public static void setResource(String str, Object obj) {
        f4293a.put(str, obj);
    }

    public static Object getResource(String str) {
        return f4293a.get(str);
    }

    public static String getRedirectDomain(String str) {
        return str != null ? str.substring(str.indexOf("//") + 2, str.lastIndexOf("/")) : "";
    }

    public static String determineMimeType(String str) {
        String str2 = "";
        if (str.endsWith(".")) {
            str = str.substring(0, str.length() - 1);
        }
        if (str.lastIndexOf(46) != -1) {
            str2 = str.substring(str.lastIndexOf(46) + 1);
        }
        if (str2.equals("css")) {
            return WebRequest.CONTENT_TYPE_CSS;
        }
        if (str2.equals("js")) {
            return "text/javascript";
        }
        return str2.equals(String.HTML) ? WebRequest.CONTENT_TYPE_HTML : "application/octet-stream";
    }

    public static Map jsonToStringMap(JSONObject jSONObject) {
        return jSONObject != JSONObject.NULL ? toStringMap(jSONObject) : new HashMap();
    }

    public static Map toStringMap(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            hashMap.put(str, jSONObject.get(str).toString());
        }
        return hashMap;
    }

    public static void runOnMainThread(Runnable runnable) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
        } else {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    public static String getFileContents(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        while (!z) {
            String readLine = bufferedReader.readLine();
            boolean z2 = readLine == null;
            if (readLine != null) {
                sb.append(readLine);
            }
            z = z2;
        }
        bufferedReader.close();
        fileInputStream.close();
        return sb.toString();
    }

    public static int generateViewId() {
        int i;
        int i2;
        do {
            i = b.get();
            i2 = i + 1;
            if (i2 > 16777215) {
                i2 = 1;
            }
        } while (!b.compareAndSet(i, i2));
        return i;
    }
}
