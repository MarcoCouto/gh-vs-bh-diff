package com.tapjoy;

import com.tapjoy.internal.fk;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

public class TapjoyURLConnection {
    public static final int TYPE_GET = 0;
    public static final int TYPE_POST = 1;

    public TapjoyHttpURLResponse getRedirectFromURL(String str) {
        return getResponseFromURL(str, "", 0, true, null, null, null);
    }

    public TapjoyHttpURLResponse getResponseFromURL(String str, Map map) {
        return getResponseFromURL(str, TapjoyUtil.convertURLParams(map, false), 0);
    }

    public TapjoyHttpURLResponse getResponseFromURL(String str, Map map, int i) {
        return getResponseFromURL(str, TapjoyUtil.convertURLParams(map, false), i);
    }

    public TapjoyHttpURLResponse getResponseFromURL(String str) {
        return getResponseFromURL(str, "", 0);
    }

    public TapjoyHttpURLResponse getResponseFromURL(String str, String str2) {
        return getResponseFromURL(str, str2, 0);
    }

    public TapjoyHttpURLResponse getResponseFromURL(String str, String str2, int i) {
        return getResponseFromURL(str, str2, i, false, null, null, null);
    }

    public TapjoyHttpURLResponse getResponseFromURL(String str, Map map, Map map2, Map map3) {
        return getResponseFromURL(str, map != null ? TapjoyUtil.convertURLParams(map, false) : "", 1, false, map2, HttpRequest.CONTENT_TYPE_FORM, TapjoyUtil.convertURLParams(map3, false));
    }

    public TapjoyHttpURLResponse getResponseFromURL(String str, Map map, Map map2, String str2) {
        return getResponseFromURL(str, map != null ? TapjoyUtil.convertURLParams(map, false) : "", 1, false, map2, "application/json;charset=utf-8", str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x017a A[SYNTHETIC, Splitter:B:49:0x017a] */
    public TapjoyHttpURLResponse getResponseFromURL(String str, String str2, int i, boolean z, Map map, String str3, String str4) {
        Exception e;
        HttpURLConnection httpURLConnection;
        TapjoyHttpURLResponse tapjoyHttpURLResponse = new TapjoyHttpURLResponse();
        BufferedReader bufferedReader = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            String sb2 = sb.toString();
            String str5 = "TapjoyURLConnection";
            StringBuilder sb3 = new StringBuilder("http ");
            sb3.append(i == 0 ? "get" : "post");
            sb3.append(": ");
            sb3.append(sb2);
            TapjoyLog.i(str5, sb3.toString());
            httpURLConnection = (HttpURLConnection) fk.a(new URL(sb2));
            if (z) {
                try {
                    httpURLConnection.setInstanceFollowRedirects(false);
                } catch (Exception e2) {
                    e = e2;
                    StringBuilder sb4 = new StringBuilder("Exception: ");
                    sb4.append(e.toString());
                    TapjoyLog.e("TapjoyURLConnection", sb4.toString());
                    tapjoyHttpURLResponse.statusCode = 0;
                    if (httpURLConnection != null) {
                    }
                    TapjoyLog.i("TapjoyURLConnection", "--------------------");
                    StringBuilder sb5 = new StringBuilder("response status: ");
                    sb5.append(tapjoyHttpURLResponse.statusCode);
                    TapjoyLog.i("TapjoyURLConnection", sb5.toString());
                    StringBuilder sb6 = new StringBuilder("response size: ");
                    sb6.append(tapjoyHttpURLResponse.contentLength);
                    TapjoyLog.i("TapjoyURLConnection", sb6.toString());
                    StringBuilder sb7 = new StringBuilder("redirectURL: ");
                    sb7.append(tapjoyHttpURLResponse.redirectURL);
                    TapjoyLog.i("TapjoyURLConnection", sb7.toString());
                    TapjoyLog.i("TapjoyURLConnection", "--------------------");
                    return tapjoyHttpURLResponse;
                }
            }
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setReadTimeout(30000);
            if (map != null) {
                for (Entry entry : map.entrySet()) {
                    httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }
            }
            if (i == 1) {
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                if (str4 != null) {
                    StringBuilder sb8 = new StringBuilder("Content-Type: ");
                    sb8.append(str3);
                    TapjoyLog.i("TapjoyURLConnection", sb8.toString());
                    TapjoyLog.i("TapjoyURLConnection", "Content:");
                    TapjoyLog.i("TapjoyURLConnection", str4);
                    httpURLConnection.setRequestProperty("Content-Type", str3);
                    httpURLConnection.setRequestProperty("Connection", "close");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setFixedLengthStreamingMode(str4.length());
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
                    outputStreamWriter.write(str4);
                    outputStreamWriter.close();
                }
            }
            httpURLConnection.connect();
            tapjoyHttpURLResponse.statusCode = httpURLConnection.getResponseCode();
            tapjoyHttpURLResponse.headerFields = httpURLConnection.getHeaderFields();
            tapjoyHttpURLResponse.date = httpURLConnection.getDate();
            tapjoyHttpURLResponse.expires = httpURLConnection.getExpiration();
            if (!z) {
                bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            }
            if (!z) {
                StringBuilder sb9 = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    StringBuilder sb10 = new StringBuilder();
                    sb10.append(readLine);
                    sb10.append(10);
                    sb9.append(sb10.toString());
                }
                tapjoyHttpURLResponse.response = sb9.toString();
            }
            if (tapjoyHttpURLResponse.statusCode == 302) {
                tapjoyHttpURLResponse.redirectURL = httpURLConnection.getHeaderField("Location");
            }
            String headerField = httpURLConnection.getHeaderField("content-length");
            if (headerField != null) {
                try {
                    tapjoyHttpURLResponse.contentLength = Integer.valueOf(headerField).intValue();
                } catch (Exception e3) {
                    String str6 = "TapjoyURLConnection";
                    StringBuilder sb11 = new StringBuilder("Exception: ");
                    sb11.append(e3.toString());
                    TapjoyLog.e(str6, sb11.toString());
                }
            }
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        } catch (Exception e4) {
            e = e4;
            httpURLConnection = null;
            StringBuilder sb42 = new StringBuilder("Exception: ");
            sb42.append(e.toString());
            TapjoyLog.e("TapjoyURLConnection", sb42.toString());
            tapjoyHttpURLResponse.statusCode = 0;
            if (httpURLConnection != null) {
                try {
                    if (tapjoyHttpURLResponse.response == null) {
                        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                        StringBuilder sb12 = new StringBuilder();
                        while (true) {
                            String readLine2 = bufferedReader2.readLine();
                            if (readLine2 == null) {
                                break;
                            }
                            StringBuilder sb13 = new StringBuilder();
                            sb13.append(readLine2);
                            sb13.append(10);
                            sb12.append(sb13.toString());
                        }
                        tapjoyHttpURLResponse.response = sb12.toString();
                    }
                } catch (Exception e5) {
                    StringBuilder sb14 = new StringBuilder("Exception trying to get error code/content: ");
                    sb14.append(e5.toString());
                    TapjoyLog.e("TapjoyURLConnection", sb14.toString());
                }
            }
            TapjoyLog.i("TapjoyURLConnection", "--------------------");
            StringBuilder sb52 = new StringBuilder("response status: ");
            sb52.append(tapjoyHttpURLResponse.statusCode);
            TapjoyLog.i("TapjoyURLConnection", sb52.toString());
            StringBuilder sb62 = new StringBuilder("response size: ");
            sb62.append(tapjoyHttpURLResponse.contentLength);
            TapjoyLog.i("TapjoyURLConnection", sb62.toString());
            StringBuilder sb72 = new StringBuilder("redirectURL: ");
            sb72.append(tapjoyHttpURLResponse.redirectURL);
            TapjoyLog.i("TapjoyURLConnection", sb72.toString());
            TapjoyLog.i("TapjoyURLConnection", "--------------------");
            return tapjoyHttpURLResponse;
        }
        TapjoyLog.i("TapjoyURLConnection", "--------------------");
        StringBuilder sb522 = new StringBuilder("response status: ");
        sb522.append(tapjoyHttpURLResponse.statusCode);
        TapjoyLog.i("TapjoyURLConnection", sb522.toString());
        StringBuilder sb622 = new StringBuilder("response size: ");
        sb622.append(tapjoyHttpURLResponse.contentLength);
        TapjoyLog.i("TapjoyURLConnection", sb622.toString());
        if (tapjoyHttpURLResponse.redirectURL != null && tapjoyHttpURLResponse.redirectURL.length() > 0) {
            StringBuilder sb722 = new StringBuilder("redirectURL: ");
            sb722.append(tapjoyHttpURLResponse.redirectURL);
            TapjoyLog.i("TapjoyURLConnection", sb722.toString());
        }
        TapjoyLog.i("TapjoyURLConnection", "--------------------");
        return tapjoyHttpURLResponse;
    }

    public String getContentLength(String str) {
        String str2;
        try {
            String replaceAll = str.replaceAll(" ", "%20");
            StringBuilder sb = new StringBuilder("requestURL: ");
            sb.append(replaceAll);
            TapjoyLog.d("TapjoyURLConnection", sb.toString());
            HttpURLConnection httpURLConnection = (HttpURLConnection) fk.a(new URL(replaceAll));
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setReadTimeout(30000);
            str2 = httpURLConnection.getHeaderField("content-length");
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder("Exception: ");
            sb2.append(e.toString());
            TapjoyLog.e("TapjoyURLConnection", sb2.toString());
            str2 = null;
        }
        StringBuilder sb3 = new StringBuilder("content-length: ");
        sb3.append(str2);
        TapjoyLog.d("TapjoyURLConnection", sb3.toString());
        return str2;
    }
}
