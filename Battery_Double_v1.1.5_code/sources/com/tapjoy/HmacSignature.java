package com.tapjoy;

import android.net.Uri;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HmacSignature {

    /* renamed from: a reason: collision with root package name */
    private String f4204a;
    private String b;

    public HmacSignature(String str, String str2) {
        this.f4204a = str;
        this.b = str2;
    }

    public String sign(String str, Map map) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(this.b.getBytes(), this.f4204a);
            Mac instance = Mac.getInstance(this.f4204a);
            instance.init(secretKeySpec);
            Uri parse = Uri.parse(str);
            StringBuilder sb = new StringBuilder();
            sb.append(parse.getScheme());
            sb.append("://");
            sb.append(parse.getHost());
            String sb2 = sb.toString();
            if (!((parse.getScheme().equals("http") && parse.getPort() == 80) || (parse.getScheme().equals("https") && parse.getPort() == 443)) && -1 != parse.getPort()) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(sb2);
                sb3.append(":");
                sb3.append(parse.getPort());
                sb2 = sb3.toString();
            }
            String lowerCase = sb2.toLowerCase();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(lowerCase);
            sb4.append(parse.getPath());
            String sb5 = sb4.toString();
            String a2 = a(map);
            StringBuilder sb6 = new StringBuilder("POST&");
            sb6.append(Uri.encode(sb5));
            sb6.append(RequestParameters.AMPERSAND);
            sb6.append(Uri.encode(a2));
            String sb7 = sb6.toString();
            StringBuilder sb8 = new StringBuilder("Base Url: ");
            sb8.append(sb7);
            TapjoyLog.v("HmacSignature", sb8.toString());
            byte[] doFinal = instance.doFinal(sb7.getBytes());
            StringBuilder sb9 = new StringBuilder();
            for (byte b2 : doFinal) {
                String hexString = Integer.toHexString(b2 & 255);
                if (hexString.length() == 1) {
                    sb9.append('0');
                }
                sb9.append(hexString);
            }
            return sb9.toString();
        } catch (Exception unused) {
            return null;
        }
    }

    public boolean matches(String str, Map map, String str2) {
        return sign(str, map).equals(str2);
    }

    private static String a(Map map) {
        TreeSet treeSet = new TreeSet(map.keySet());
        StringBuilder sb = new StringBuilder();
        Iterator it = treeSet.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            String str2 = (String) map.get(str);
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(RequestParameters.EQUAL);
            sb2.append(str2);
            sb2.append(RequestParameters.AMPERSAND);
            sb.append(sb2.toString());
        }
        sb.deleteCharAt(sb.lastIndexOf(RequestParameters.AMPERSAND));
        StringBuilder sb3 = new StringBuilder("Unhashed String: ");
        sb3.append(sb.toString());
        TapjoyLog.v("HmacSignature", sb3.toString());
        return sb.toString();
    }
}
