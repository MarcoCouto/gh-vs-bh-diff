package com.tapjoy;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.lang.reflect.Method;

public class TapjoyAdIdClient {

    /* renamed from: a reason: collision with root package name */
    private Context f4277a;
    private String b;
    private boolean c;

    public TapjoyAdIdClient(Context context) {
        this.f4277a = context;
    }

    public boolean setupAdIdInfo() {
        try {
            Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.f4277a);
            this.b = advertisingIdInfo.getId();
            this.c = !advertisingIdInfo.isLimitAdTrackingEnabled();
            return true;
        } catch (Exception unused) {
            return false;
        } catch (Error unused2) {
            return false;
        }
    }

    public boolean setupAdIdInfoReflection() {
        try {
            Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            Method method = cls.getMethod("getAdvertisingIdInfo", new Class[]{Context.class});
            StringBuilder sb = new StringBuilder("Found method: ");
            sb.append(method);
            TapjoyLog.d("TapjoyAdIdClient", sb.toString());
            Object invoke = method.invoke(cls, new Object[]{this.f4277a});
            Method method2 = invoke.getClass().getMethod(RequestParameters.isLAT, new Class[0]);
            Method method3 = invoke.getClass().getMethod("getId", new Class[0]);
            this.c = !((Boolean) method2.invoke(invoke, new Object[0])).booleanValue();
            this.b = (String) method3.invoke(invoke, new Object[0]);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public String getAdvertisingId() {
        return this.b;
    }

    public boolean isAdTrackingEnabled() {
        return this.c;
    }
}
