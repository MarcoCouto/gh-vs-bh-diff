package com.tapjoy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.internal.al;
import com.tapjoy.internal.bh;
import com.tapjoy.internal.bn;
import com.tapjoy.internal.ev;
import com.tapjoy.internal.fb;
import com.tapjoy.internal.fc;
import com.tapjoy.internal.fi;
import com.tapjoy.internal.fp;
import com.tapjoy.internal.fp.a;
import com.tapjoy.internal.ft;
import com.tapjoy.internal.gb;
import com.tapjoy.internal.gf;
import com.tapjoy.internal.ha;
import com.tapjoy.internal.hd;
import com.tapjoy.internal.ho;
import com.tapjoy.internal.jr;
import com.tapjoy.internal.ka;
import com.tapjoy.internal.v;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import org.w3c.dom.Document;

public class TapjoyConnectCore {
    private static float A = 1.0f;
    private static int B = 1;
    /* access modifiers changed from: private */
    public static String C = "";
    private static String D = "";
    public static final float DEFAULT_CURRENCY_MULTIPLIER = 1.0f;
    private static String E = "";
    private static String F = "";
    private static String G = "";
    private static String H = "";
    private static String I = "";
    private static String J = "";
    private static String K = "";
    private static String L = "";
    private static String M = "";
    private static String N = "native";
    private static String O = "";
    private static String P = "";
    private static float Q = 1.0f;
    private static boolean R = false;
    private static String S = "";
    private static String T = "";
    private static String U = "";
    private static String V = "";
    private static String W = null;
    private static long Z = 0;

    /* renamed from: a reason: collision with root package name */
    protected static int f4284a = 0;
    private static Integer aA;
    private static Long aB;
    private static Long aC;
    private static Long aD;
    private static String aE;
    private static Integer aF;
    private static Double aG;
    private static Double aH;
    private static Long aI;
    private static Integer aJ;
    private static Integer aK;
    private static Integer aL;
    private static String aM;
    private static String aN;
    private static String aO;
    private static String aP = "";
    private static String aQ = "";
    private static String aR = "";
    private static boolean aS = false;
    private static TJConnectListener aT = null;
    private static boolean aU = false;
    private static boolean ab;
    private static PackageManager ac;
    private static TapjoyGpsHelper ad;
    private static Hashtable ae = TapjoyConnectFlag.CONNECT_FLAG_DEFAULTS;
    private static String af = "";
    private static Map ag = new ConcurrentHashMap();
    private static String ah;
    private static String ai;
    private static String aj;
    private static String ak;
    private static Integer al;
    private static String am;
    private static String an;
    private static Long ao;
    private static String ap;
    private static Integer aq;
    private static Integer ar;
    private static String as;
    private static String at;
    private static String au;
    private static String av;
    private static String aw;
    private static Set ax;
    private static Integer ay;
    private static Integer az;
    protected static int b = 0;
    protected static String c = "";
    protected static boolean d;
    protected static String e = "";
    protected static String f = "";
    private static Context g;
    private static String h;
    private static TapjoyConnectCore i;
    /* access modifiers changed from: private */
    public static TapjoyURLConnection j;
    private static TJConnectListener k;
    private static TJSetUserIDListener l;
    private static Vector m = new Vector(Arrays.asList(TapjoyConstants.dependencyClassNames));
    private static String n = "";
    private static String o = "";
    private static String p = "";
    private static String q = "";
    private static String r = "";
    private static String s = "";
    private static String t = "";
    private static String u = "";
    private static String v = "";
    private static String w = "";
    private static String x = "";
    private static String y = "";
    private static int z = 1;
    private long X = 0;
    private boolean Y = false;
    private boolean aa = false;

    public class PPAThread implements Runnable {
        private Map b;

        public PPAThread(Map map) {
            this.b = map;
        }

        public void run() {
            TapjoyURLConnection c = TapjoyConnectCore.j;
            StringBuilder sb = new StringBuilder();
            sb.append(TapjoyConnectCore.getHostURL());
            sb.append(TapjoyConstants.TJC_CONNECT_URL_PATH);
            TapjoyHttpURLResponse responseFromURL = c.getResponseFromURL(sb.toString(), (Map) null, (Map) null, this.b);
            if (responseFromURL.response != null) {
                TapjoyConnectCore.d(responseFromURL.response);
            }
        }
    }

    public static String getConnectURL() {
        return TapjoyConfig.TJC_CONNECT_SERVICE_URL;
    }

    public static TapjoyConnectCore getInstance() {
        return i;
    }

    public static void requestTapjoyConnect(Context context, String str) {
        requestTapjoyConnect(context, str, null);
    }

    public static void requestTapjoyConnect(Context context, String str, Hashtable hashtable) {
        requestTapjoyConnect(context, str, hashtable, null);
    }

    public static void requestTapjoyConnect(Context context, String str, Hashtable hashtable, TJConnectListener tJConnectListener) {
        try {
            fp fpVar = new fp(str);
            if (fpVar.f4407a == a.SDK_ANDROID) {
                h = str;
                v = fpVar.b;
                L = fpVar.c;
                M = fpVar.d;
                if (hashtable != null) {
                    ae.putAll(hashtable);
                    gb.b().a(hashtable);
                }
                ha.a(context).j = str;
                k = tJConnectListener;
                if (i == null) {
                    i = new TapjoyConnectCore();
                }
                TapjoyConnectCore tapjoyConnectCore = i;
                try {
                    a(context);
                    new Thread(new Runnable() {
                        public final void run() {
                            TapjoyConnectCore.a();
                            TapjoyConnectCore.this.completeConnectCall();
                        }
                    }).start();
                    tapjoyConnectCore.aa = true;
                } catch (TapjoyIntegrationException e2) {
                    TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, e2.getMessage()));
                    d();
                    ft.b.notifyObservers(Boolean.FALSE);
                } catch (TapjoyException e3) {
                    TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.SDK_ERROR, e3.getMessage()));
                    d();
                    ft.b.notifyObservers(Boolean.FALSE);
                }
            } else {
                throw new IllegalArgumentException("The given API key was not for Android.");
            }
        } catch (IllegalArgumentException e4) {
            throw new TapjoyIntegrationException(e4.getMessage());
        }
    }

    public static void requestLimitedTapjoyConnect(Context context, String str, TJConnectListener tJConnectListener) {
        try {
            fp fpVar = new fp(str);
            if (fpVar.f4407a == a.SDK_ANDROID) {
                aP = fpVar.b;
                aQ = fpVar.c;
                if (i == null) {
                    i = new TapjoyConnectCore();
                }
                aT = tJConnectListener;
                TapjoyConnectCore tapjoyConnectCore = i;
                try {
                    a(context);
                    new Thread(new Runnable() {
                        public final void run() {
                            TapjoyConnectCore.a();
                            TapjoyConnectCore.this.completeLimitedConnectCall();
                        }
                    }).start();
                } catch (TapjoyIntegrationException e2) {
                    TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, e2.getMessage()));
                    e();
                    ft.b.notifyObservers(Boolean.FALSE);
                } catch (TapjoyException e3) {
                    TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.SDK_ERROR, e3.getMessage()));
                    e();
                    ft.b.notifyObservers(Boolean.FALSE);
                }
                TapjoyLog.d("TapjoyConnect", "requestTapjoyConnect function complete");
                return;
            }
            throw new IllegalArgumentException("The given API key was not for Android.");
        } catch (IllegalArgumentException e4) {
            TapjoyLog.d("TapjoyConnect", e4.getMessage());
            throw new TapjoyIntegrationException(e4.getMessage());
        }
    }

    private static void d() {
        if (!jr.c(M)) {
            ha.a().a(g, h, "12.3.3", TapjoyConfig.TJC_ANALYTICS_SERVICE_URL, M, L);
        }
        if (k != null) {
            k.onConnectFailure();
        }
    }

    private static void e() {
        if (aT != null) {
            aT.onConnectFailure();
        }
    }

    public void appPause() {
        this.Y = true;
    }

    public void appResume() {
        if (this.Y) {
            p();
            this.Y = false;
        }
    }

    public static Map getURLParams() {
        Map genericURLParams = getGenericURLParams();
        genericURLParams.putAll(getTimeStampAndVerifierParams());
        return genericURLParams;
    }

    public static Map getLimitedURLParams() {
        Map limitedGenericURLParams = getLimitedGenericURLParams();
        limitedGenericURLParams.putAll(getLimitedTimeStampAndVerifierParams());
        return limitedGenericURLParams;
    }

    public static Map getGenericURLParams() {
        Map f2 = f();
        TapjoyUtil.safePut(f2, "app_id", v, true);
        return f2;
    }

    public static Map getLimitedGenericURLParams() {
        Map f2 = f();
        TapjoyUtil.safePut(f2, "app_id", aP, true);
        TapjoyUtil.safePut(f2, TapjoyConstants.TJC_APP_GROUP_ID, aR, true);
        TapjoyUtil.safePut(f2, TapjoyConstants.TJC_LIMITED, "true", true);
        return f2;
    }

    private static Map f() {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        TapjoyUtil.safePut(hashMap3, TapjoyConstants.TJC_PLUGIN, N, true);
        TapjoyUtil.safePut(hashMap3, TapjoyConstants.TJC_SDK_TYPE, O, true);
        TapjoyUtil.safePut(hashMap3, "app_id", v, true);
        TapjoyUtil.safePut(hashMap3, TapjoyConstants.TJC_LIBRARY_VERSION, x, true);
        TapjoyUtil.safePut(hashMap3, TapjoyConstants.TJC_LIBRARY_REVISION, TapjoyRevision.GIT_REVISION, true);
        TapjoyUtil.safePut(hashMap3, TapjoyConstants.TJC_BRIDGE_VERSION, y, true);
        TapjoyUtil.safePut(hashMap3, TapjoyConstants.TJC_APP_VERSION_NAME, w, true);
        hashMap2.putAll(hashMap3);
        HashMap hashMap4 = new HashMap();
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_DEVICE_NAME, r, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_PLATFORM, D, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, u, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_DEVICE_MANUFACTURER, s, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_DEVICE_TYPE_NAME, t, true);
        String str = TapjoyConstants.TJC_DEVICE_SCREEN_LAYOUT_SIZE;
        StringBuilder sb = new StringBuilder();
        sb.append(B);
        TapjoyUtil.safePut(hashMap4, str, sb.toString(), true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_STORE_NAME, K, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_STORE_VIEW, String.valueOf(R), true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_CARRIER_NAME, E, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_CARRIER_COUNTRY_CODE, F, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_MOBILE_NETWORK_CODE, H, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_MOBILE_COUNTRY_CODE, G, true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, Locale.getDefault().getCountry(), true);
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_DEVICE_LANGUAGE, Locale.getDefault().getLanguage(), true);
        I = getConnectionType();
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_CONNECTION_TYPE, I, true);
        J = getConnectionSubType();
        TapjoyUtil.safePut(hashMap4, TapjoyConstants.TJC_CONNECTION_SUBTYPE, J, true);
        String str2 = TapjoyConstants.TJC_DEVICE_SCREEN_DENSITY;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(z);
        TapjoyUtil.safePut(hashMap4, str2, sb2.toString(), true);
        hashMap2.putAll(hashMap4);
        HashMap hashMap5 = new HashMap();
        if (m()) {
            TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_ADVERTISING_ID, c, true);
            TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_AD_TRACKING_ENABLED, String.valueOf(d), true);
        }
        if ((n() && !m()) || !o()) {
            TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_ANDROID_ID, n, true);
            TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_DEVICE_MAC_ADDRESS, p, true);
        }
        TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_INSTALL_ID, q, true);
        TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_USER_ID, C, true);
        TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_ADVERTISING_ID_CHECK_DISABLED, e, true);
        TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_LEGACY_ID_FALLBACK_ALLOWED, f, true);
        if (f4284a != 0) {
            TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_PACKAGED_GOOGLE_PLAY_SERVICES_VERSION, Integer.toString(f4284a), true);
        }
        if (b != 0) {
            TapjoyUtil.safePut(hashMap5, TapjoyConstants.TJC_DEVICE_GOOGLE_PLAY_SERVICES_VERSION, Integer.toString(b), true);
        }
        if (o == null || o.length() == 0 || System.currentTimeMillis() - Z > TapjoyConstants.SESSION_ID_INACTIVITY_TIME) {
            o = p();
        } else {
            Z = System.currentTimeMillis();
        }
        TapjoyUtil.safePut(hashMap5, "session_id", o, true);
        hashMap2.putAll(hashMap5);
        HashMap hashMap6 = new HashMap();
        TapjoyUtil.safePut(hashMap6, TapjoyConstants.TJC_APP_GROUP_ID, S, true);
        TapjoyUtil.safePut(hashMap6, "store", T, true);
        TapjoyUtil.safePut(hashMap6, TapjoyConstants.TJC_ANALYTICS_API_KEY, U, true);
        TapjoyUtil.safePut(hashMap6, TapjoyConstants.TJC_MANAGED_DEVICE_ID, V, true);
        hashMap2.putAll(hashMap6);
        gf a2 = gf.a();
        HashMap hashMap7 = new HashMap();
        if (a2.f4430a != null) {
            TapjoyUtil.safePut(hashMap7, "gdpr", a2.f4430a.booleanValue() ? "1" : "0", true);
        }
        if (!al.a(a2.b)) {
            TapjoyUtil.safePut(hashMap7, "cgdpr", a2.b, true);
        }
        hashMap2.putAll(hashMap7);
        if (!(TapjoyCache.getInstance() == null || TapjoyCache.getInstance().getCachedOfferIDs() == null || TapjoyCache.getInstance().getCachedOfferIDs().length() <= 0)) {
            TapjoyUtil.safePut(hashMap2, TapjoyConstants.TJC_CACHED_OFFERS, TapjoyCache.getInstance().getCachedOfferIDs(), true);
        }
        TapjoyUtil.safePut(hashMap2, TapjoyConstants.TJC_CURRENCY_MULTIPLIER, Float.toString(Q), true);
        hashMap.putAll(hashMap2);
        HashMap hashMap8 = new HashMap();
        h();
        HashMap hashMap9 = new HashMap();
        TapjoyUtil.safePut(hashMap9, TapjoyConstants.TJC_ANALYTICS_ID, ah, true);
        TapjoyUtil.safePut(hashMap9, TapjoyConstants.TJC_PACKAGE_ID, ai, true);
        TapjoyUtil.safePut(hashMap9, TapjoyConstants.TJC_PACKAGE_SIGN, aj, true);
        TapjoyUtil.safePut(hashMap9, TapjoyConstants.TJC_DEVICE_DISPLAY_DENSITY, aJ);
        TapjoyUtil.safePut(hashMap9, TapjoyConstants.TJC_DEVICE_DISPLAY_WIDTH, aK);
        TapjoyUtil.safePut(hashMap9, TapjoyConstants.TJC_DEVICE_DISPLAY_HEIGHT, aL);
        TapjoyUtil.safePut(hashMap9, TapjoyConstants.TJC_DEVICE_COUNTRY_SIM, aM, true);
        TapjoyUtil.safePut(hashMap9, TapjoyConstants.TJC_DEVICE_TIMEZONE, aN, true);
        hashMap8.putAll(hashMap9);
        HashMap hashMap10 = new HashMap();
        TapjoyUtil.safePut(hashMap10, TapjoyConstants.TJC_PACKAGE_VERSION, ak, true);
        TapjoyUtil.safePut(hashMap10, TapjoyConstants.TJC_PACKAGE_REVISION, al);
        TapjoyUtil.safePut(hashMap10, TapjoyConstants.TJC_PACKAGE_DATA_VERSION, am, true);
        TapjoyUtil.safePut(hashMap10, TapjoyConstants.TJC_INSTALLER, an, true);
        if (jr.c(K)) {
            TapjoyUtil.safePut(hashMap10, TapjoyConstants.TJC_STORE_NAME, aO, true);
        }
        hashMap8.putAll(hashMap10);
        hashMap8.putAll(g());
        hashMap.putAll(hashMap8);
        return hashMap;
    }

    public static Map getTimeStampAndVerifierParams() {
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        String a2 = a(currentTimeMillis);
        HashMap hashMap = new HashMap();
        TapjoyUtil.safePut(hashMap, "timestamp", String.valueOf(currentTimeMillis), true);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_VERIFIER, a2, true);
        return hashMap;
    }

    public static Map getLimitedTimeStampAndVerifierParams() {
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        String b2 = b(currentTimeMillis);
        HashMap hashMap = new HashMap();
        TapjoyUtil.safePut(hashMap, "timestamp", String.valueOf(currentTimeMillis), true);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_VERIFIER, b2, true);
        return hashMap;
    }

    private static Map g() {
        HashMap hashMap = new HashMap();
        TapjoyUtil.safePut(hashMap, "installed", ao);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_REFERRER, ap, true);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_LEVEL, aq);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_FRIEND_COUNT, ar);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_VARIABLE_1, as, true);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_VARIABLE_2, at, true);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_VARIABLE_3, au, true);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_VARIABLE_4, av, true);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_VARIABLE_5, aw, true);
        int i2 = 0;
        for (String str : ax) {
            StringBuilder sb = new StringBuilder("user_tags[");
            int i3 = i2 + 1;
            sb.append(i2);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            TapjoyUtil.safePut(hashMap, sb.toString(), str, true);
            i2 = i3;
        }
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_WEEKLY_FREQUENCY, ay);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_USER_MONTHLY_FREQUENCY, az);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_SESSION_TOTAL_COUNT, aA);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_SESSION_TOTAL_LENGTH, aB);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_SESSION_LAST_AT, aC);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_SESSION_LAST_LENGTH, aD);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_PURCHASE_CURRENCY, aE, true);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_PURCHASE_TOTAL_COUNT, aF);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_PURCHASE_TOTAL_PRICE, aG);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_PURCHASE_LAST_PRICE, aH);
        TapjoyUtil.safePut(hashMap, TapjoyConstants.TJC_PURCHASE_LAST_AT, aI);
        return hashMap;
    }

    private static boolean a(Context context) {
        g = context;
        ac = context.getPackageManager();
        gf.a().a(context);
        gb.a().a(context);
        ad = new TapjoyGpsHelper(g);
        if (j == null) {
            j = new TapjoyURLConnection();
        }
        if (ae == null) {
            ae = new Hashtable();
        }
        j();
        int identifier = g.getResources().getIdentifier("raw/tapjoy_config", null, g.getPackageName());
        Properties properties = new Properties();
        try {
            properties.load(g.getResources().openRawResource(identifier));
            a(properties);
        } catch (Exception unused) {
        }
        if (jr.c(getConnectFlagValue("unit_test_mode"))) {
            k();
        }
        String string = Secure.getString(g.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
        n = string;
        if (string != null) {
            n = n.toLowerCase();
        }
        try {
            boolean z2 = false;
            w = ac.getPackageInfo(g.getPackageName(), 0).versionName;
            t = "android";
            D = "android";
            r = Build.MODEL;
            s = Build.MANUFACTURER;
            u = VERSION.RELEASE;
            x = "12.3.3";
            y = TapjoyConstants.TJC_BRIDGE_VERSION_NUMBER;
            try {
                if (VERSION.SDK_INT > 3) {
                    TapjoyDisplayMetricsUtil tapjoyDisplayMetricsUtil = new TapjoyDisplayMetricsUtil(g);
                    z = tapjoyDisplayMetricsUtil.getScreenDensityDPI();
                    A = tapjoyDisplayMetricsUtil.getScreenDensityScale();
                    B = tapjoyDisplayMetricsUtil.getScreenLayoutSize();
                }
            } catch (Exception e2) {
                StringBuilder sb = new StringBuilder("Error getting screen density/dimensions/layout: ");
                sb.append(e2.toString());
                TapjoyLog.e("TapjoyConnect", sb.toString());
            }
            if (f("android.permission.ACCESS_WIFI_STATE")) {
                try {
                    WifiManager wifiManager = (WifiManager) g.getSystemService("wifi");
                    if (wifiManager != null) {
                        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                        if (connectionInfo != null) {
                            String macAddress = connectionInfo.getMacAddress();
                            p = macAddress;
                            if (macAddress != null) {
                                p = p.replace(":", "").toLowerCase();
                            }
                        }
                    }
                } catch (Exception e3) {
                    StringBuilder sb2 = new StringBuilder("Error getting device mac address: ");
                    sb2.append(e3.toString());
                    TapjoyLog.e("TapjoyConnect", sb2.toString());
                }
            } else {
                TapjoyLog.d("TapjoyConnect", "*** ignore macAddress");
            }
            TelephonyManager telephonyManager = (TelephonyManager) g.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                E = telephonyManager.getNetworkOperatorName();
                F = telephonyManager.getNetworkCountryIso();
                String networkOperator = telephonyManager.getNetworkOperator();
                if (networkOperator != null && (networkOperator.length() == 5 || networkOperator.length() == 6)) {
                    G = networkOperator.substring(0, 3);
                    H = networkOperator.substring(3);
                }
            }
            SharedPreferences sharedPreferences = g.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0);
            String string2 = sharedPreferences.getString(TapjoyConstants.PREF_INSTALL_ID, "");
            q = string2;
            if (string2 == null || q.length() == 0) {
                try {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(UUID.randomUUID().toString());
                    sb3.append(System.currentTimeMillis());
                    q = TapjoyUtil.SHA256(sb3.toString());
                    Editor edit = sharedPreferences.edit();
                    edit.putString(TapjoyConstants.PREF_INSTALL_ID, q);
                    edit.apply();
                } catch (Exception e4) {
                    StringBuilder sb4 = new StringBuilder("Error generating install id: ");
                    sb4.append(e4.toString());
                    TapjoyLog.e("TapjoyConnect", sb4.toString());
                }
            }
            if (getConnectFlagValue(TapjoyConnectFlag.STORE_NAME) != null && getConnectFlagValue(TapjoyConnectFlag.STORE_NAME).length() > 0) {
                K = getConnectFlagValue(TapjoyConnectFlag.STORE_NAME);
                if (!new ArrayList(Arrays.asList(TapjoyConnectFlag.STORE_ARRAY)).contains(K)) {
                    StringBuilder sb5 = new StringBuilder("Warning -- undefined STORE_NAME: ");
                    sb5.append(K);
                    TapjoyLog.w("TapjoyConnect", sb5.toString());
                }
            }
            try {
                String str = K;
                Intent intent = new Intent("android.intent.action.VIEW");
                if (str.length() <= 0) {
                    intent.setData(Uri.parse("market://details"));
                    if (ac.queryIntentActivities(intent, 0).size() > 0) {
                        z2 = true;
                    }
                } else if (str.equals(TapjoyConnectFlag.STORE_GFAN)) {
                    z2 = e("com.mappn.gfan");
                } else if (str.equals(TapjoyConnectFlag.STORE_SKT)) {
                    z2 = e("com.skt.skaf.TSCINSTALL");
                }
                R = z2;
            } catch (Exception e5) {
                StringBuilder sb6 = new StringBuilder("Error trying to detect store intent on devicee: ");
                sb6.append(e5.toString());
                TapjoyLog.e("TapjoyConnect", sb6.toString());
            }
            h();
            if (getConnectFlagValue(TapjoyConnectFlag.ALLOW_LEGACY_ID_FALLBACK) != null && getConnectFlagValue(TapjoyConnectFlag.ALLOW_LEGACY_ID_FALLBACK).length() > 0) {
                f = getConnectFlagValue(TapjoyConnectFlag.ALLOW_LEGACY_ID_FALLBACK);
            }
            if (getConnectFlagValue(TapjoyConnectFlag.DISABLE_ADVERTISING_ID_CHECK) != null && getConnectFlagValue(TapjoyConnectFlag.DISABLE_ADVERTISING_ID_CHECK).length() > 0) {
                e = getConnectFlagValue(TapjoyConnectFlag.DISABLE_ADVERTISING_ID_CHECK);
            }
            if (getConnectFlagValue(TapjoyConnectFlag.USER_ID) != null && getConnectFlagValue(TapjoyConnectFlag.USER_ID).length() > 0) {
                StringBuilder sb7 = new StringBuilder("Setting userID to: ");
                sb7.append(getConnectFlagValue(TapjoyConnectFlag.USER_ID));
                TapjoyLog.i("TapjoyConnect", sb7.toString());
                setUserID(getConnectFlagValue(TapjoyConnectFlag.USER_ID), null);
            }
            P = TapjoyUtil.getRedirectDomain(getConnectFlagValue(TapjoyConnectFlag.SERVICE_URL));
            if (ae != null) {
                i();
            }
            return true;
        } catch (NameNotFoundException e6) {
            throw new TapjoyException(e6.getMessage());
        }
    }

    private static void h() {
        fc a2 = ha.a(g).a(true);
        fb fbVar = a2.d;
        ah = fbVar.h;
        ai = fbVar.r;
        aj = fbVar.s;
        aJ = fbVar.m;
        aK = fbVar.n;
        aL = fbVar.o;
        aM = fbVar.u;
        aN = fbVar.q;
        ev evVar = a2.e;
        ak = evVar.e;
        al = evVar.f;
        am = evVar.g;
        an = evVar.h;
        aO = evVar.i;
        fi fiVar = a2.f;
        ao = fiVar.s;
        ap = fiVar.t;
        aq = fiVar.J;
        ar = fiVar.K;
        as = fiVar.L;
        at = fiVar.M;
        au = fiVar.N;
        av = fiVar.O;
        aw = fiVar.P;
        ax = new HashSet(fiVar.Q);
        ay = fiVar.u;
        az = fiVar.v;
        aA = fiVar.x;
        aB = fiVar.y;
        aC = fiVar.z;
        aD = fiVar.A;
        aE = fiVar.B;
        aF = fiVar.C;
        aG = fiVar.D;
        aH = fiVar.F;
        aI = fiVar.E;
    }

    private static void i() {
        TapjoyLog.i("TapjoyConnect", "Connect Flags:");
        TapjoyLog.i("TapjoyConnect", "--------------------");
        for (Entry entry : ae.entrySet()) {
            StringBuilder sb = new StringBuilder("key: ");
            sb.append((String) entry.getKey());
            sb.append(", value: ");
            sb.append(Uri.encode(entry.getValue().toString()));
            TapjoyLog.i("TapjoyConnect", sb.toString());
        }
        StringBuilder sb2 = new StringBuilder("hostURL: [");
        sb2.append(getConnectFlagValue(TapjoyConnectFlag.SERVICE_URL));
        sb2.append(RequestParameters.RIGHT_BRACKETS);
        TapjoyLog.i("TapjoyConnect", sb2.toString());
        StringBuilder sb3 = new StringBuilder("redirectDomain: [");
        sb3.append(P);
        sb3.append(RequestParameters.RIGHT_BRACKETS);
        TapjoyLog.i("TapjoyConnect", sb3.toString());
        TapjoyLog.i("TapjoyConnect", "--------------------");
    }

    private static void j() {
        String[] strArr;
        try {
            if (ac != null) {
                ApplicationInfo applicationInfo = ac.getApplicationInfo(g.getPackageName(), 128);
                if (applicationInfo == null || applicationInfo.metaData == null) {
                    TapjoyLog.d("TapjoyConnect", "No metadata present.");
                } else {
                    for (String str : TapjoyConnectFlag.FLAG_ARRAY) {
                        Bundle bundle = applicationInfo.metaData;
                        StringBuilder sb = new StringBuilder("tapjoy.");
                        sb.append(str);
                        String string = bundle.getString(sb.toString());
                        if (string != null) {
                            StringBuilder sb2 = new StringBuilder("Found manifest flag: ");
                            sb2.append(str);
                            sb2.append(", ");
                            sb2.append(string);
                            TapjoyLog.d("TapjoyConnect", sb2.toString());
                            a(str, string);
                        }
                    }
                    TapjoyLog.d("TapjoyConnect", "Metadata successfully loaded");
                }
            }
        } catch (Exception e2) {
            ErrorType errorType = ErrorType.SDK_ERROR;
            StringBuilder sb3 = new StringBuilder("Error reading manifest meta-data -- ");
            sb3.append(e2.toString());
            TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(errorType, sb3.toString()));
        }
    }

    private static void a(Properties properties) {
        Enumeration keys = properties.keys();
        while (keys.hasMoreElements()) {
            try {
                String str = (String) keys.nextElement();
                a(str, (String) properties.get(str));
            } catch (ClassCastException unused) {
                TapjoyLog.e("TapjoyConnect", "Error parsing configuration properties in tapjoy_config.txt");
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r1 = new java.lang.StringBuilder("[ClassNotFoundException] Could not find dependency class ");
        r1.append((java.lang.String) m.get(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0135, code lost:
        throw new com.tapjoy.TapjoyIntegrationException(r1.toString());
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x011a */
    private static void k() {
        try {
            List<ActivityInfo> asList = Arrays.asList(ac.getPackageInfo(g.getPackageName(), 1).activities);
            if (asList != null) {
                for (ActivityInfo activityInfo : asList) {
                    if (m.contains(activityInfo.name)) {
                        int indexOf = m.indexOf(activityInfo.name);
                        Class.forName((String) m.get(indexOf));
                        Vector vector = new Vector();
                        if ((activityInfo.configChanges & 128) != 128) {
                            vector.add("orientation");
                        }
                        if ((activityInfo.configChanges & 32) != 32) {
                            vector.add("keyboardHidden");
                        }
                        if (vector.size() == 0) {
                            if (VERSION.SDK_INT >= 13 && (activityInfo.configChanges & 1024) != 1024) {
                                StringBuilder sb = new StringBuilder("WARNING -- screenSize property is not specified in manifest configChanges for ");
                                sb.append((String) m.get(indexOf));
                                TapjoyLog.w("TapjoyConnect", sb.toString());
                            }
                            if (VERSION.SDK_INT >= 11 && activityInfo.name.equals("com.tapjoy.TJAdUnitActivity")) {
                                if ((activityInfo.flags & 512) != 512) {
                                    StringBuilder sb2 = new StringBuilder("'hardwareAccelerated' property not specified in manifest for ");
                                    sb2.append((String) m.get(indexOf));
                                    throw new TapjoyIntegrationException(sb2.toString());
                                }
                            }
                            m.remove(indexOf);
                        } else if (vector.size() == 1) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(vector.toString());
                            sb3.append(" property is not specified in manifest configChanges for ");
                            sb3.append((String) m.get(indexOf));
                            throw new TapjoyIntegrationException(sb3.toString());
                        } else {
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append(vector.toString());
                            sb4.append(" properties are not specified in manifest configChanges for ");
                            sb4.append((String) m.get(indexOf));
                            throw new TapjoyIntegrationException(sb4.toString());
                        }
                    }
                }
            }
            if (m.size() == 0) {
                l();
                try {
                    try {
                        Class.forName("com.tapjoy.TJAdUnitJSBridge").getMethod(String.CLOSE_REQUESTED, new Class[]{Boolean.class});
                        if (getConnectFlagValue(TapjoyConnectFlag.DISABLE_ADVERTISING_ID_CHECK) == null || !getConnectFlagValue(TapjoyConnectFlag.DISABLE_ADVERTISING_ID_CHECK).equals("true")) {
                            ad.checkGooglePlayIntegration();
                        } else {
                            TapjoyLog.i("TapjoyConnect", "Skipping integration check for Google Play Services and Advertising ID. Do this only if you do not have access to Google Play Services.");
                        }
                    } catch (NoSuchMethodException unused) {
                        throw new TapjoyIntegrationException("Try configuring Proguard or other code obfuscators to ignore com.tapjoy classes. Visit http://dev.tapjoy.comfor more information.");
                    }
                } catch (ClassNotFoundException unused2) {
                    throw new TapjoyIntegrationException("ClassNotFoundException: com.tapjoy.TJAdUnitJSBridge was not found.");
                }
            } else if (m.size() == 1) {
                StringBuilder sb5 = new StringBuilder("Missing ");
                sb5.append(m.size());
                sb5.append(" dependency class in manifest: ");
                sb5.append(m.toString());
                throw new TapjoyIntegrationException(sb5.toString());
            } else {
                StringBuilder sb6 = new StringBuilder("Missing ");
                sb6.append(m.size());
                sb6.append(" dependency classes in manifest: ");
                sb6.append(m.toString());
                throw new TapjoyIntegrationException(sb6.toString());
            }
        } catch (Exception unused3) {
            throw new TapjoyIntegrationException("Error while getting package info.");
        }
    }

    private static void l() {
        String[] strArr;
        String[] strArr2;
        Vector vector = new Vector();
        for (String str : TapjoyConstants.dependencyPermissions) {
            if (!f(str)) {
                vector.add(str);
            }
        }
        if (vector.size() == 0) {
            Vector vector2 = new Vector();
            for (String str2 : TapjoyConstants.optionalPermissions) {
                if (!f(str2)) {
                    vector2.add(str2);
                }
            }
            if (vector2.size() != 0) {
                if (vector2.size() == 1) {
                    StringBuilder sb = new StringBuilder("WARNING -- ");
                    sb.append(vector2.toString());
                    sb.append(" permission was not found in manifest. The exclusion of this permission could cause problems.");
                    TapjoyLog.w("TapjoyConnect", sb.toString());
                    return;
                }
                StringBuilder sb2 = new StringBuilder("WARNING -- ");
                sb2.append(vector2.toString());
                sb2.append(" permissions were not found in manifest. The exclusion of these permissions could cause problems.");
                TapjoyLog.w("TapjoyConnect", sb2.toString());
            }
        } else if (vector.size() == 1) {
            StringBuilder sb3 = new StringBuilder("Missing 1 permission in manifest: ");
            sb3.append(vector.toString());
            throw new TapjoyIntegrationException(sb3.toString());
        } else {
            StringBuilder sb4 = new StringBuilder("Missing ");
            sb4.append(vector.size());
            sb4.append(" permissions in manifest: ");
            sb4.append(vector.toString());
            throw new TapjoyIntegrationException(sb4.toString());
        }
    }

    private static boolean m() {
        return c != null && c.length() > 0;
    }

    private static boolean n() {
        return getConnectFlagValue(TapjoyConnectFlag.ALLOW_LEGACY_ID_FALLBACK) != null && getConnectFlagValue(TapjoyConnectFlag.ALLOW_LEGACY_ID_FALLBACK).equals("true");
    }

    private static boolean o() {
        return (ad.isGooglePlayServicesAvailable() && ad.isGooglePlayManifestConfigured()) || getConnectFlagValue(TapjoyConnectFlag.DISABLE_ADVERTISING_ID_CHECK) == null || !getConnectFlagValue(TapjoyConnectFlag.DISABLE_ADVERTISING_ID_CHECK).equals("true");
    }

    private static boolean a(String str, boolean z2) {
        bn bnVar;
        long j2;
        bn bnVar2 = null;
        try {
            bnVar = bn.b(str);
            try {
                Map d2 = bnVar.d();
                String a2 = jr.a((String) d2.get(TapjoyConstants.TJC_APP_GROUP_ID));
                String a3 = jr.a((String) d2.get("store"));
                String a4 = jr.a((String) d2.get(TapjoyConstants.TJC_ANALYTICS_API_KEY));
                String a5 = jr.a((String) d2.get(TapjoyConstants.TJC_MANAGED_DEVICE_ID));
                String a6 = jr.a((String) d2.get(TapjoyConstants.TJC_PACKAGE_NAMES));
                Object obj = d2.get("cache_max_age");
                fp fpVar = new fp(a4);
                if (fpVar.f4407a == a.RPC_ANALYTICS) {
                    String a7 = fp.a(fpVar.b);
                    String str2 = fpVar.c;
                    if (a2 == null) {
                        a2 = a7;
                    }
                    Object obj2 = obj;
                    ha.a().a(g, a4, "12.3.3", TapjoyConfig.TJC_ANALYTICS_SERVICE_URL, a7, str2);
                    S = a2;
                    T = a3;
                    U = a4;
                    V = a5;
                    ArrayList arrayList = new ArrayList();
                    if (a6 != null) {
                        for (String trim : a6.split(",")) {
                            String trim2 = trim.trim();
                            if (trim2.length() > 0) {
                                arrayList.add(trim2);
                            }
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        a((List) arrayList);
                    }
                    bnVar.close();
                    if (!z2) {
                        if (obj2 instanceof String) {
                            try {
                                j2 = Long.parseLong(((String) obj2).trim());
                            } catch (NumberFormatException unused) {
                            }
                        } else {
                            if (obj2 instanceof Number) {
                                j2 = ((Number) obj2).longValue();
                            }
                            j2 = 0;
                        }
                        if (j2 <= 0) {
                            TapjoyAppSettings.getInstance().removeConnectResult();
                        } else {
                            TapjoyAppSettings.getInstance().saveConnectResultAndParams(str, r(), (j2 * 1000) + v.b());
                        }
                        gb a8 = gb.a();
                        Object obj3 = d2.get(TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS);
                        if (obj3 instanceof Map) {
                            try {
                                a8.f4418a.a((Map) obj3);
                                a8.c().edit().putString(TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS, bh.a(obj3)).apply();
                            } catch (Exception unused2) {
                            }
                        } else if (obj3 == null) {
                            a8.f4418a.a((Map) null);
                            a8.c().edit().remove(TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS).apply();
                        }
                    }
                    ka.a(null);
                    return true;
                }
                throw new IOException("Invalid analytics_api_key");
            } catch (IOException e2) {
                e = e2;
                bnVar2 = bnVar;
                TapjoyLog.v("TapjoyConnect", e.getMessage());
                ka.a(bnVar2);
                return false;
            } catch (RuntimeException e3) {
                e = e3;
                bnVar2 = bnVar;
                TapjoyLog.v("TapjoyConnect", e.getMessage());
                ka.a(bnVar2);
                return false;
            } catch (Throwable th) {
                th = th;
                ka.a(bnVar);
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
            TapjoyLog.v("TapjoyConnect", e.getMessage());
            ka.a(bnVar2);
            return false;
        } catch (RuntimeException e5) {
            e = e5;
            TapjoyLog.v("TapjoyConnect", e.getMessage());
            ka.a(bnVar2);
            return false;
        } catch (Throwable th2) {
            th = th2;
            bnVar = bnVar2;
            ka.a(bnVar);
            throw th;
        }
    }

    /* JADX WARNING: type inference failed for: r8v0, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r8v1, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r8v2, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r8v4 */
    /* JADX WARNING: type inference failed for: r8v6 */
    /* JADX WARNING: type inference failed for: r8v7 */
    /* JADX WARNING: type inference failed for: r8v9 */
    /* JADX WARNING: type inference failed for: r8v11 */
    /* JADX WARNING: type inference failed for: r8v12 */
    /* JADX WARNING: type inference failed for: r8v13, types: [com.tapjoy.internal.bn] */
    /* JADX WARNING: type inference failed for: r8v14 */
    /* JADX WARNING: type inference failed for: r8v15 */
    /* JADX WARNING: type inference failed for: r8v16 */
    /* JADX WARNING: type inference failed for: r8v17 */
    /* JADX WARNING: type inference failed for: r8v18 */
    /* JADX WARNING: type inference failed for: r8v19 */
    /* JADX WARNING: type inference failed for: r8v20 */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.String, code=null, for r8v0, types: [java.lang.String] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r8v4
  assigns: []
  uses: []
  mth insns count: 66
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 6 */
    private static boolean c(String r8) {
        Throwable th;
        IOException e2;
        RuntimeException e3;
        try {
            r8 = r8;
            r8 = bn.b(r8);
            try {
                Map d2 = r8.d();
                String a2 = jr.a((String) d2.get(TapjoyConstants.TJC_APP_GROUP_ID));
                String a3 = jr.a((String) d2.get("store"));
                String a4 = jr.a((String) d2.get(TapjoyConstants.TJC_ANALYTICS_API_KEY));
                String a5 = jr.a((String) d2.get(TapjoyConstants.TJC_MANAGED_DEVICE_ID));
                fp fpVar = new fp(a4);
                if (fpVar.f4407a == a.RPC_ANALYTICS) {
                    String a6 = fp.a(fpVar.b);
                    if (a2 == null) {
                        a2 = a6;
                    }
                    aR = a2;
                    T = a3;
                    V = a5;
                    r8.close();
                    ka.a(null);
                    return true;
                }
                throw new IOException("Invalid analytics_api_key");
            } catch (IOException e4) {
                e2 = e4;
                r8 = r8;
                TapjoyLog.v("TapjoyConnect", e2.getMessage());
                r8 = r8;
                ka.a(r8);
                return false;
            } catch (RuntimeException e5) {
                e3 = e5;
                r8 = r8;
                r8 = r8;
                TapjoyLog.v("TapjoyConnect", e3.getMessage());
                r8 = r8;
                ka.a(r8);
                return false;
            }
        } catch (IOException e6) {
            e2 = e6;
            r8 = 0;
            TapjoyLog.v("TapjoyConnect", e2.getMessage());
            r8 = r8;
            ka.a(r8);
            return false;
        } catch (RuntimeException e7) {
            e3 = e7;
            r8 = 0;
            r8 = r8;
            TapjoyLog.v("TapjoyConnect", e3.getMessage());
            r8 = r8;
            ka.a(r8);
            return false;
        } catch (Throwable th2) {
            th = th2;
            r8 = r8;
            ka.a(r8);
            throw th;
        }
    }

    private static synchronized void a(List list) {
        synchronized (TapjoyConnectCore.class) {
            af = "";
            for (ApplicationInfo applicationInfo : ac.getInstalledApplications(0)) {
                if ((applicationInfo.flags & 1) != 1 && list.contains(applicationInfo.packageName)) {
                    StringBuilder sb = new StringBuilder("MATCH: installed packageName: ");
                    sb.append(applicationInfo.packageName);
                    TapjoyLog.d("TapjoyConnect", sb.toString());
                    if (af.length() > 0) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(af);
                        sb2.append(",");
                        af = sb2.toString();
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(af);
                    sb3.append(applicationInfo.packageName);
                    af = sb3.toString();
                }
            }
        }
    }

    /* JADX WARNING: type inference failed for: r4v0, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r4v1, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r4v4, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r4v5 */
    /* JADX WARNING: type inference failed for: r4v7 */
    /* JADX WARNING: type inference failed for: r4v8 */
    /* JADX WARNING: type inference failed for: r4v10 */
    /* JADX WARNING: type inference failed for: r4v12 */
    /* JADX WARNING: type inference failed for: r4v13 */
    /* JADX WARNING: type inference failed for: r4v14, types: [com.tapjoy.internal.bn] */
    /* JADX WARNING: type inference failed for: r4v15 */
    /* JADX WARNING: type inference failed for: r4v16 */
    /* JADX WARNING: type inference failed for: r4v17 */
    /* JADX WARNING: type inference failed for: r4v18 */
    /* JADX WARNING: type inference failed for: r4v19 */
    /* JADX WARNING: type inference failed for: r4v20 */
    /* JADX WARNING: type inference failed for: r4v21 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.String, code=null, for r4v0, types: [java.lang.String] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r4v5
  assigns: []
  uses: []
  mth insns count: 48
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 6 */
    public static boolean d(String r4) {
        Throwable th;
        IOException e2;
        RuntimeException e3;
        try {
            r4 = r4;
            r4 = bn.b(r4);
            try {
                if (r4.a()) {
                    r4.s();
                    TapjoyLog.d("TapjoyConnect", "Successfully sent completed Pay-Per-Action to Tapjoy server.");
                    r4.close();
                    ka.a(null);
                    return true;
                }
                r4.close();
                ka.a(null);
                TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.SDK_ERROR, "Completed Pay-Per-Action call failed."));
                return false;
            } catch (IOException e4) {
                e2 = e4;
                r4 = r4;
                TapjoyLog.v("TapjoyConnect", e2.getMessage());
                r4 = r4;
                ka.a(r4);
                TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.SDK_ERROR, "Completed Pay-Per-Action call failed."));
                return false;
            } catch (RuntimeException e5) {
                e3 = e5;
                r4 = r4;
                r4 = r4;
                TapjoyLog.v("TapjoyConnect", e3.getMessage());
                r4 = r4;
                ka.a(r4);
                TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.SDK_ERROR, "Completed Pay-Per-Action call failed."));
                return false;
            }
        } catch (IOException e6) {
            e2 = e6;
            r4 = 0;
            TapjoyLog.v("TapjoyConnect", e2.getMessage());
            r4 = r4;
            ka.a(r4);
            TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.SDK_ERROR, "Completed Pay-Per-Action call failed."));
            return false;
        } catch (RuntimeException e7) {
            e3 = e7;
            r4 = 0;
            r4 = r4;
            TapjoyLog.v("TapjoyConnect", e3.getMessage());
            r4 = r4;
            ka.a(r4);
            TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.SDK_ERROR, "Completed Pay-Per-Action call failed."));
            return false;
        } catch (Throwable th2) {
            th = th2;
            r4 = r4;
            ka.a(r4);
            throw th;
        }
    }

    public void release() {
        i = null;
        j = null;
        TapjoyLog.d("TapjoyConnect", "Releasing core static instance.");
    }

    public static String getAppID() {
        return v;
    }

    public static String getLimitedAppID() {
        return aP;
    }

    public static String getUserID() {
        return C;
    }

    public static String getHostURL() {
        return getConnectFlagValue(TapjoyConnectFlag.SERVICE_URL);
    }

    public static String getPlacementURL() {
        return getConnectFlagValue(TapjoyConnectFlag.PLACEMENT_URL);
    }

    public static String getRedirectDomain() {
        return P;
    }

    public static String getCarrierName() {
        return E;
    }

    public static String getConnectionType() {
        String str = "";
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) g.getSystemService("connectivity");
            if (!(connectivityManager == null || connectivityManager.getActiveNetworkInfo() == null)) {
                int type = connectivityManager.getActiveNetworkInfo().getType();
                str = (type == 1 || type == 6) ? "wifi" : TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE;
                StringBuilder sb = new StringBuilder("connectivity: ");
                sb.append(connectivityManager.getActiveNetworkInfo().getType());
                TapjoyLog.d("TapjoyConnect", sb.toString());
                StringBuilder sb2 = new StringBuilder("connection_type: ");
                sb2.append(str);
                TapjoyLog.d("TapjoyConnect", sb2.toString());
            }
        } catch (Exception e2) {
            StringBuilder sb3 = new StringBuilder("getConnectionType error: ");
            sb3.append(e2.toString());
            TapjoyLog.e("TapjoyConnect", sb3.toString());
        }
        return str;
    }

    public static String getConnectionSubType() {
        String str;
        Exception e2;
        String str2 = "";
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) g.getSystemService("connectivity");
            if (connectivityManager == null) {
                return str2;
            }
            str = connectivityManager.getActiveNetworkInfo().getSubtypeName();
            String str3 = "TapjoyConnect";
            try {
                StringBuilder sb = new StringBuilder("connection_sub_type: ");
                sb.append(str);
                TapjoyLog.d(str3, sb.toString());
            } catch (Exception e3) {
                e2 = e3;
            }
            return str;
        } catch (Exception e4) {
            Exception exc = e4;
            str = str2;
            e2 = exc;
            StringBuilder sb2 = new StringBuilder("getConnectionSubType error: ");
            sb2.append(e2.toString());
            TapjoyLog.e("TapjoyConnect", sb2.toString());
            return str;
        }
    }

    private static boolean e(String str) {
        for (ApplicationInfo applicationInfo : ac.getInstalledApplications(0)) {
            if (applicationInfo.packageName.startsWith(str)) {
                return true;
            }
        }
        return false;
    }

    private static String p() {
        TapjoyLog.i("TapjoyConnect", "generating sessionID...");
        String str = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(System.currentTimeMillis() / 1000);
            sb.append(v);
            String SHA256 = TapjoyUtil.SHA256(sb.toString());
            try {
                Z = System.currentTimeMillis();
                return SHA256;
            } catch (Exception e2) {
                String str2 = SHA256;
                e = e2;
                str = str2;
                StringBuilder sb2 = new StringBuilder("unable to generate session id: ");
                sb2.append(e.toString());
                TapjoyLog.e("TapjoyConnect", sb2.toString());
                return str;
            }
        } catch (Exception e3) {
            e = e3;
            StringBuilder sb22 = new StringBuilder("unable to generate session id: ");
            sb22.append(e.toString());
            TapjoyLog.e("TapjoyConnect", sb22.toString());
            return str;
        }
    }

    public static Context getContext() {
        return g;
    }

    private static String q() {
        if (m()) {
            return c;
        }
        if (n() || !o()) {
            boolean z2 = true;
            if (p != null && p.length() > 0) {
                return p;
            }
            if (n == null || n.length() <= 0) {
                z2 = false;
            }
            if (z2) {
                return n;
            }
        }
        TapjoyLog.e("TapjoyConnect", "Error -- no valid device identifier");
        return null;
    }

    private static String a(long j2) {
        String str = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(v);
            sb.append(":");
            sb.append(q());
            sb.append(":");
            sb.append(j2);
            sb.append(":");
            sb.append(L);
            return TapjoyUtil.SHA256(sb.toString());
        } catch (Exception e2) {
            ErrorType errorType = ErrorType.SDK_ERROR;
            StringBuilder sb2 = new StringBuilder("Error in computing verifier value -- ");
            sb2.append(e2.toString());
            TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(errorType, sb2.toString()));
            return str;
        }
    }

    private static String b(long j2) {
        String str = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(aP);
            sb.append(":");
            sb.append(q());
            sb.append(":");
            sb.append(j2);
            sb.append(":");
            sb.append(aQ);
            return TapjoyUtil.SHA256(sb.toString());
        } catch (Exception e2) {
            ErrorType errorType = ErrorType.SDK_ERROR;
            StringBuilder sb2 = new StringBuilder("Error in computing verifier value -- ");
            sb2.append(e2.toString());
            TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(errorType, sb2.toString()));
            return str;
        }
    }

    public static String getAwardCurrencyVerifier(long j2, int i2, String str) {
        String str2 = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(v);
            sb.append(":");
            sb.append(q());
            sb.append(":");
            sb.append(j2);
            sb.append(":");
            sb.append(L);
            sb.append(":");
            sb.append(i2);
            sb.append(":");
            sb.append(str);
            return TapjoyUtil.SHA256(sb.toString());
        } catch (Exception e2) {
            ErrorType errorType = ErrorType.SDK_ERROR;
            StringBuilder sb2 = new StringBuilder("Error in computing awardCurrencyVerifier -- ");
            sb2.append(e2.toString());
            TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(errorType, sb2.toString()));
            return str2;
        }
    }

    private static String a(long j2, String str) {
        String str2 = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(v);
            sb.append(":");
            sb.append(q());
            sb.append(":");
            sb.append(j2);
            sb.append(":");
            sb.append(L);
            sb.append(":");
            sb.append(str);
            return TapjoyUtil.SHA256(sb.toString());
        } catch (Exception e2) {
            ErrorType errorType = ErrorType.SDK_ERROR;
            StringBuilder sb2 = new StringBuilder("Error in computing packageNamesVerifier -- ");
            sb2.append(e2.toString());
            TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(errorType, sb2.toString()));
            return str2;
        }
    }

    public boolean isInitialized() {
        return this.aa;
    }

    public static void setPlugin(String str) {
        N = str;
    }

    public static void setSDKType(String str) {
        O = str;
    }

    public static void setUserID(String str, TJSetUserIDListener tJSetUserIDListener) {
        C = str;
        l = tJSetUserIDListener;
        StringBuilder sb = new StringBuilder("URL parameters: ");
        sb.append(getURLParams());
        TapjoyLog.d("TapjoyConnect", sb.toString());
        new Thread(new Runnable() {
            public final void run() {
                StringBuilder sb = new StringBuilder("Setting userID to ");
                sb.append(TapjoyConnectCore.C);
                TapjoyLog.i("TapjoyConnect", sb.toString());
                TapjoyURLConnection c = TapjoyConnectCore.j;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(TapjoyConnectCore.getHostURL());
                sb2.append(TapjoyConstants.TJC_USER_ID_URL_PATH);
                TapjoyHttpURLResponse responseFromURL = c.getResponseFromURL(sb2.toString(), TapjoyConnectCore.getURLParams());
                TapjoyConnectCore.a(responseFromURL.response != null ? TapjoyConnectCore.a(responseFromURL.response) : false);
            }
        }).start();
    }

    public static void viewDidClose(String str) {
        StringBuilder sb = new StringBuilder("viewDidClose: ");
        sb.append(str);
        TapjoyLog.d("TapjoyConnect", sb.toString());
        ag.remove(str);
        ft.e.notifyObservers();
    }

    public static void viewWillOpen(String str, int i2) {
        StringBuilder sb = new StringBuilder("viewWillOpen: ");
        sb.append(str);
        TapjoyLog.d("TapjoyConnect", sb.toString());
        ag.put(str, Integer.valueOf(i2));
    }

    public static boolean isViewOpen() {
        StringBuilder sb = new StringBuilder("isViewOpen: ");
        sb.append(ag.size());
        TapjoyLog.d("TapjoyConnect", sb.toString());
        return !ag.isEmpty();
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0010  */
    public static boolean isFullScreenViewOpen() {
        for (Integer intValue : ag.values()) {
            switch (intValue.intValue()) {
                case 1:
                case 2:
                    break;
                default:
                    while (r0.hasNext()) {
                    }
                    break;
            }
            return true;
        }
        return false;
    }

    public static void setViewShowing(boolean z2) {
        if (z2) {
            ag.put("", Integer.valueOf(1));
        } else {
            ag.clear();
        }
    }

    private static void a(String str, String str2) {
        if ((str.equals(TapjoyConnectFlag.SERVICE_URL) || str.equals(TapjoyConnectFlag.PLACEMENT_URL)) && !str2.endsWith("/")) {
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append("/");
            str2 = sb.toString();
        }
        ae.put(str, str2);
    }

    private static boolean f(String str) {
        return ac.checkPermission(str, g.getPackageName()) == 0;
    }

    public void actionComplete(String str) {
        StringBuilder sb = new StringBuilder("actionComplete: ");
        sb.append(str);
        TapjoyLog.i("TapjoyConnect", sb.toString());
        Map f2 = f();
        TapjoyUtil.safePut(f2, "app_id", str, true);
        f2.putAll(getTimeStampAndVerifierParams());
        StringBuilder sb2 = new StringBuilder("PPA URL parameters: ");
        sb2.append(f2);
        TapjoyLog.d("TapjoyConnect", sb2.toString());
        new Thread(new PPAThread(f2)).start();
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x0142  */
    public void completeConnectCall() {
        boolean z2;
        TapjoyHttpURLResponse responseFromURL;
        TapjoyLog.d("TapjoyConnect", "starting connect call...");
        String str = TapjoyConfig.TJC_CONNECT_SERVICE_URL;
        if (getHostURL() != TapjoyConfig.TJC_SERVICE_URL) {
            str = getHostURL();
        }
        if (!isConnected()) {
            String connectResult = TapjoyAppSettings.getInstance().getConnectResult(r(), v.b());
            if (connectResult != null && a(connectResult, true)) {
                TapjoyLog.i("TapjoyConnect", "Connect using stored connect result");
                ab = true;
                if (k != null) {
                    k.onConnectSuccess();
                }
                ft.f4412a.notifyObservers();
                z2 = true;
                TapjoyURLConnection tapjoyURLConnection = j;
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(TapjoyConstants.TJC_CONNECT_URL_PATH);
                responseFromURL = tapjoyURLConnection.getResponseFromURL(sb.toString(), (Map) null, (Map) null, getURLParams());
                if (responseFromURL == null && responseFromURL.statusCode == 200) {
                    if (a(responseFromURL.response, false)) {
                        TapjoyLog.i("TapjoyConnect", "Successfully connected to Tapjoy");
                        ab = true;
                        for (Entry entry : getGenericURLParams().entrySet()) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append((String) entry.getKey());
                            sb2.append(": ");
                            sb2.append((String) entry.getValue());
                            TapjoyLog.d("TapjoyConnect", sb2.toString());
                        }
                        if (!z2) {
                            if (k != null) {
                                k.onConnectSuccess();
                            }
                            ft.f4412a.notifyObservers();
                        }
                        ft.b.notifyObservers(Boolean.TRUE);
                    } else {
                        if (!z2) {
                            d();
                        }
                        ft.b.notifyObservers(Boolean.FALSE);
                    }
                    if (af.length() > 0) {
                        Map genericURLParams = getGenericURLParams();
                        TapjoyUtil.safePut(genericURLParams, TapjoyConstants.TJC_PACKAGE_NAMES, af, true);
                        long currentTimeMillis = System.currentTimeMillis() / 1000;
                        String a2 = a(currentTimeMillis, af);
                        TapjoyUtil.safePut(genericURLParams, "timestamp", String.valueOf(currentTimeMillis), true);
                        TapjoyUtil.safePut(genericURLParams, TapjoyConstants.TJC_VERIFIER, a2, true);
                        TapjoyURLConnection tapjoyURLConnection2 = new TapjoyURLConnection();
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(getHostURL());
                        sb3.append(TapjoyConstants.TJC_SDK_LESS_CONNECT);
                        TapjoyHttpURLResponse responseFromURL2 = tapjoyURLConnection2.getResponseFromURL(sb3.toString(), genericURLParams);
                        if (responseFromURL2 != null && responseFromURL2.statusCode == 200) {
                            TapjoyLog.d("TapjoyConnect", "Successfully pinged sdkless api.");
                        }
                        return;
                    }
                } else {
                    if (!z2) {
                        d();
                    }
                    ft.b.notifyObservers(Boolean.FALSE);
                }
            }
        }
        z2 = false;
        TapjoyURLConnection tapjoyURLConnection3 = j;
        StringBuilder sb4 = new StringBuilder();
        sb4.append(str);
        sb4.append(TapjoyConstants.TJC_CONNECT_URL_PATH);
        responseFromURL = tapjoyURLConnection3.getResponseFromURL(sb4.toString(), (Map) null, (Map) null, getURLParams());
        if (responseFromURL == null) {
        }
        if (!z2) {
        }
        ft.b.notifyObservers(Boolean.FALSE);
    }

    public void completeLimitedConnectCall() {
        String str = TapjoyConfig.TJC_CONNECT_SERVICE_URL;
        if (getHostURL() != TapjoyConfig.TJC_SERVICE_URL) {
            str = getHostURL();
        }
        Map limitedURLParams = getLimitedURLParams();
        TapjoyURLConnection tapjoyURLConnection = j;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(TapjoyConstants.TJC_CONNECT_URL_PATH);
        TapjoyHttpURLResponse responseFromURL = tapjoyURLConnection.getResponseFromURL(sb.toString(), (Map) null, (Map) null, limitedURLParams);
        if (responseFromURL == null || responseFromURL.statusCode != 200) {
            e();
            ft.b.notifyObservers(Boolean.FALSE);
        } else if (c(responseFromURL.response)) {
            TapjoyLog.i("TapjoyConnect", "Successfully connected to Tapjoy");
            aS = true;
            for (Entry entry : getLimitedGenericURLParams().entrySet()) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append((String) entry.getKey());
                sb2.append(": ");
                sb2.append((String) entry.getValue());
                TapjoyLog.d("TapjoyConnect", sb2.toString());
            }
            if (aT != null) {
                aT.onConnectSuccess();
            }
            ft.f4412a.notifyObservers();
            ft.b.notifyObservers(Boolean.TRUE);
        } else {
            e();
            ft.b.notifyObservers(Boolean.FALSE);
        }
    }

    public void setCurrencyMultiplier(float f2) {
        StringBuilder sb = new StringBuilder("setVirtualCurrencyMultiplier: ");
        sb.append(f2);
        TapjoyLog.i("TapjoyConnect", sb.toString());
        Q = f2;
    }

    public float getCurrencyMultiplier() {
        return Q;
    }

    public static String getConnectFlagValue(String str) {
        String str2 = "";
        return (ae == null || ae.get(str) == null) ? str2 : ae.get(str).toString();
    }

    public static String getSecretKey() {
        return L;
    }

    public static String getAndroidID() {
        return n;
    }

    public static String getSha1MacAddress() {
        try {
            return TapjoyUtil.SHA1(p);
        } catch (Exception e2) {
            StringBuilder sb = new StringBuilder("Error generating sha1 of macAddress: ");
            sb.append(e2.toString());
            TapjoyLog.e("TapjoyConnect", sb.toString());
            return null;
        }
    }

    public static String getMacAddress() {
        return p;
    }

    public static float getDeviceScreenDensityScale() {
        return A;
    }

    public static String getSupportURL(String str) {
        if (str == null) {
            str = v;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(getHostURL());
        sb.append("support_requests/new?currency_id=");
        sb.append(str);
        sb.append("&app_id=");
        sb.append(v);
        sb.append("&udid=");
        sb.append(V);
        sb.append("&language_code=");
        sb.append(Locale.getDefault().getLanguage());
        return sb.toString();
    }

    public static String getUserToken() {
        return V;
    }

    public static boolean isConnected() {
        return ab;
    }

    public static boolean isLimitedConnected() {
        return aS;
    }

    public static boolean isUnitTestMode() {
        return getConnectFlagValue("unit_test_mode") == "true";
    }

    private static String r() {
        StringBuilder sb = new StringBuilder();
        sb.append(v);
        sb.append(w);
        sb.append(x);
        sb.append(c);
        sb.append(q);
        String sb2 = sb.toString();
        try {
            return TapjoyUtil.SHA1(sb2);
        } catch (Exception unused) {
            return sb2;
        }
    }

    public static void setAdTrackingEnabled() {
        boolean z2;
        if (gf.a() != null) {
            gf a2 = gf.a();
            if (a2.c == null) {
                z2 = false;
            } else {
                z2 = a2.c.booleanValue();
            }
            if (z2) {
                d = false;
                return;
            }
        }
        if (ad != null) {
            d = ad.isAdTrackingEnabled();
        }
    }

    static /* synthetic */ void a() {
        if (!aU) {
            try {
                ad.loadAdvertisingId(!n());
                if (ad.isGooglePlayServicesAvailable() && ad.isGooglePlayManifestConfigured()) {
                    b = ad.getDeviceGooglePlayServicesVersion();
                    f4284a = ad.getPackagedGooglePlayServicesVersion();
                }
                if (ad.isAdIdAvailable()) {
                    setAdTrackingEnabled();
                    c = ad.getAdvertisingId();
                    ha a2 = ha.a();
                    String str = c;
                    boolean z2 = !d;
                    hd hdVar = a2.f;
                    String a3 = hdVar.c.A.a();
                    hdVar.b.q = str;
                    hdVar.b.r = Boolean.valueOf(z2);
                    hdVar.c.A.a(str);
                    hdVar.c.B.a(z2);
                    ho.a(str, z2);
                    if (!jr.c(a3) && !str.equals(a3)) {
                        hdVar.c.a(false);
                    }
                }
            } catch (Exception e2) {
                StringBuilder sb = new StringBuilder("Error fetching advertising id: ");
                sb.append(e2.toString());
                TapjoyLog.i("TapjoyConnect", sb.toString());
                e2.printStackTrace();
            }
            aU = true;
        }
    }

    static /* synthetic */ boolean a(String str) {
        Document buildDocument = TapjoyUtil.buildDocument(str);
        if (buildDocument != null) {
            String nodeTrimValue = TapjoyUtil.getNodeTrimValue(buildDocument.getElementsByTagName("PackageNames"));
            if (nodeTrimValue != null && nodeTrimValue.length() > 0) {
                Vector vector = new Vector();
                int i2 = 0;
                while (true) {
                    int indexOf = nodeTrimValue.indexOf(44, i2);
                    if (indexOf == -1) {
                        break;
                    }
                    StringBuilder sb = new StringBuilder("parse: ");
                    sb.append(nodeTrimValue.substring(i2, indexOf).trim());
                    TapjoyLog.d("TapjoyConnect", sb.toString());
                    vector.add(nodeTrimValue.substring(i2, indexOf).trim());
                    i2 = indexOf + 1;
                }
                StringBuilder sb2 = new StringBuilder("parse: ");
                sb2.append(nodeTrimValue.substring(i2).trim());
                TapjoyLog.d("TapjoyConnect", sb2.toString());
                vector.add(nodeTrimValue.substring(i2).trim());
                a((List) vector);
            }
            String nodeTrimValue2 = TapjoyUtil.getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
            if (nodeTrimValue2 == null || !nodeTrimValue2.equals("true")) {
                return false;
            }
        }
        return true;
    }

    static /* synthetic */ void a(boolean z2) {
        if (z2) {
            TapjoyLog.i("TapjoyConnect", "Set userID is successful");
            if (l != null) {
                l.onSetUserIDSuccess();
            }
        } else {
            String str = "Failed to set userID";
            TapjoyLog.e("TapjoyConnect", new TapjoyErrorMessage(ErrorType.SDK_ERROR, str));
            if (l != null) {
                l.onSetUserIDFailure(str);
            }
        }
    }
}
