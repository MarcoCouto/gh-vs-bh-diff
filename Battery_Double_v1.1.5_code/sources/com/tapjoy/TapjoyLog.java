package com.tapjoy;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.WebView;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.internal.gx;
import com.tapjoy.internal.ha;

public class TapjoyLog {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f4292a = "TapjoyLog";
    private static int b = 6;
    private static int c = 4;
    private static int d = 2;
    private static boolean e = false;
    private static int f = b;

    public static void setDebugEnabled(boolean z) {
        boolean z2;
        e = z;
        ha a2 = ha.a();
        if (gx.f4443a != z) {
            gx.f4443a = z;
            if (z) {
                gx.a("The debug mode has been enabled");
            } else {
                gx.a("The debug mode has been disabled");
            }
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2 && z && a2.k) {
            a2.i.a();
        }
        if (e) {
            a(TapjoyConstants.LOG_LEVEL_DEBUG_ON, false);
        } else {
            a(TapjoyConstants.LOG_LEVEL_DEBUG_OFF, false);
        }
    }

    public static void setInternalLogging(boolean z) {
        if (z) {
            a(TapjoyConstants.LOG_LEVEL_INTERNAL, true);
        }
    }

    @TargetApi(19)
    static void a(String str, boolean z) {
        if (z || TapjoyAppSettings.getInstance() == null || TapjoyAppSettings.getInstance().f4278a == null) {
            if (str.equals(TapjoyConstants.LOG_LEVEL_INTERNAL)) {
                f = d;
                if (VERSION.SDK_INT >= 19) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            TapjoyLog.d(TapjoyLog.f4292a, "Enabling WebView debugging");
                            WebView.setWebContentsDebuggingEnabled(true);
                        }
                    });
                }
            } else if (str.equals(TapjoyConstants.LOG_LEVEL_DEBUG_ON)) {
                f = c;
            } else if (str.equals(TapjoyConstants.LOG_LEVEL_DEBUG_OFF)) {
                f = b;
            } else {
                String str2 = f4292a;
                StringBuilder sb = new StringBuilder("unrecognized loggingLevel: ");
                sb.append(str);
                d(str2, sb.toString());
                f = b;
            }
            String str3 = f4292a;
            StringBuilder sb2 = new StringBuilder("logThreshold=");
            sb2.append(f);
            d(str3, sb2.toString());
            return;
        }
        d(f4292a, "setLoggingLevel -- log setting already persisted");
    }

    public static boolean isLoggingEnabled() {
        return e;
    }

    public static void i(String str, String str2) {
        a(4, str, str2);
    }

    public static void e(String str, String str2) {
        e(str, new TapjoyErrorMessage(ErrorType.INTERNAL_ERROR, str2));
    }

    public static void e(String str, TapjoyErrorMessage tapjoyErrorMessage) {
        if (tapjoyErrorMessage == null) {
            return;
        }
        if (f == d || tapjoyErrorMessage.getType() != ErrorType.INTERNAL_ERROR) {
            a(6, str, tapjoyErrorMessage.toString());
        }
    }

    public static void w(String str, String str2) {
        a(5, str, str2);
    }

    public static void d(String str, String str2) {
        a(3, str, str2);
    }

    public static void v(String str, String str2) {
        a(2, str, str2);
    }

    private static void a(int i, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(f4292a);
        sb.append(":");
        sb.append(str);
        String sb2 = sb.toString();
        if (f <= i) {
            if (str2.length() > 4096) {
                int i2 = 0;
                while (i2 <= str2.length() / 4096) {
                    int i3 = i2 * 4096;
                    i2++;
                    int i4 = i2 * 4096;
                    if (i4 > str2.length()) {
                        i4 = str2.length();
                    }
                    Log.println(i, sb2, str2.substring(i3, i4));
                }
                return;
            }
            Log.println(i, sb2, str2);
        }
    }
}
