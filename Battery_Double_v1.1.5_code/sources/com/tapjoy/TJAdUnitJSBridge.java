package com.tapjoy;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.github.mikephil.charting.utils.Utils;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.TapjoyLog;
import com.tapjoy.internal.al;
import com.tapjoy.internal.er;
import com.tapjoy.internal.jr;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"SetJavaScriptEnabled"})
public class TJAdUnitJSBridge implements TJWebViewJSInterfaceListener {

    /* renamed from: a reason: collision with root package name */
    public TJAdUnit f4222a;
    public boolean allowRedirect;
    public WebView b;
    final ConcurrentLinkedQueue c;
    public boolean closeRequested;
    public boolean customClose;
    private TJWebViewJSInterface d;
    public boolean didLaunchOtherActivity;
    private TJAdUnitJSBridge e;
    /* access modifiers changed from: private */
    public Context f;
    /* access modifiers changed from: private */
    public TJAdUnitActivity g;
    /* access modifiers changed from: private */
    public TJSplitWebView h;
    private ProgressDialog i;
    private View j;
    private boolean k;
    private er l;
    public String otherActivityCallbackID;
    public String splitWebViewCallbackID;

    public interface AdUnitAsyncTaskListner {
        void onComplete(boolean z);
    }

    @TargetApi(11)
    class a extends AsyncTask {

        /* renamed from: a reason: collision with root package name */
        WebView f4236a;

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
            return (Boolean[]) objArr;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            Boolean[] boolArr = (Boolean[]) obj;
            final boolean booleanValue = boolArr[0].booleanValue();
            final boolean booleanValue2 = boolArr[1].booleanValue();
            if (TJAdUnitJSBridge.this.f instanceof Activity) {
                TapjoyUtil.runOnMainThread(new Runnable() {
                    public final void run() {
                        if (booleanValue) {
                            a.this.f4236a.setVisibility(0);
                            if (booleanValue2) {
                                if (a.this.f4236a.getParent() instanceof RelativeLayout) {
                                    ((RelativeLayout) a.this.f4236a.getParent()).getBackground().setAlpha(0);
                                    ((RelativeLayout) a.this.f4236a.getParent()).setBackgroundColor(0);
                                }
                                if (VERSION.SDK_INT >= 11) {
                                    a.this.f4236a.setLayerType(1, null);
                                }
                            } else {
                                if (a.this.f4236a.getParent() instanceof RelativeLayout) {
                                    ((RelativeLayout) a.this.f4236a.getParent()).getBackground().setAlpha(255);
                                    ((RelativeLayout) a.this.f4236a.getParent()).setBackgroundColor(-1);
                                }
                                if (VERSION.SDK_INT >= 11) {
                                    a.this.f4236a.setLayerType(0, null);
                                }
                            }
                        } else {
                            a.this.f4236a.setVisibility(4);
                            if (a.this.f4236a.getParent() instanceof RelativeLayout) {
                                ((RelativeLayout) a.this.f4236a.getParent()).getBackground().setAlpha(0);
                                ((RelativeLayout) a.this.f4236a.getParent()).setBackgroundColor(0);
                            }
                        }
                    }
                });
            } else {
                TapjoyLog.e("TJAdUnitJSBridge", "Unable to present offerwall. No Activity context provided.");
            }
        }

        public a(WebView webView) {
            this.f4236a = webView;
        }
    }

    public void destroy() {
    }

    public TJAdUnitJSBridge(Context context, TJAdUnit tJAdUnit) {
        this(context, (WebView) tJAdUnit.getWebView());
        this.f4222a = tJAdUnit;
    }

    public TJAdUnitJSBridge(Context context, WebView webView) {
        this.j = null;
        this.didLaunchOtherActivity = false;
        this.allowRedirect = true;
        this.otherActivityCallbackID = null;
        this.customClose = false;
        this.closeRequested = false;
        this.splitWebViewCallbackID = null;
        this.l = new er(this);
        this.c = new ConcurrentLinkedQueue();
        TapjoyLog.i("TJAdUnitJSBridge", "creating AdUnit/JS Bridge");
        this.f = context;
        this.b = webView;
        this.e = this;
        if (this.b == null) {
            TapjoyLog.e("TJAdUnitJSBridge", new TapjoyErrorMessage(ErrorType.SDK_ERROR, "Cannot create AdUnitJSBridge -- webview is NULL"));
            return;
        }
        this.d = new TJWebViewJSInterface(this.b, this);
        this.b.addJavascriptInterface(this.d, TJAdUnitConstants.JAVASCRIPT_INTERFACE_ID);
        setEnabled(true);
    }

    public void onDispatchMethod(String str, JSONObject jSONObject) {
        String str2;
        if (this.k) {
            try {
                str2 = jSONObject.optString(String.CALLBACK_ID, null);
                try {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("data");
                    Method method = TJAdUnitJSBridge.class.getMethod(str, new Class[]{JSONObject.class, String.class});
                    StringBuilder sb = new StringBuilder("Dispatching method: ");
                    sb.append(method);
                    sb.append(" with data=");
                    sb.append(jSONObject2);
                    sb.append("; callbackID=");
                    sb.append(str2);
                    TapjoyLog.d("TJAdUnitJSBridge", sb.toString());
                    method.invoke(this.e, new Object[]{jSONObject2, str2});
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    invokeJSCallback(str2, Boolean.FALSE);
                }
            } catch (Exception e3) {
                e = e3;
                str2 = null;
                e.printStackTrace();
                invokeJSCallback(str2, Boolean.FALSE);
            }
        } else {
            StringBuilder sb2 = new StringBuilder("Bridge currently disabled. Adding ");
            sb2.append(str);
            sb2.append(" to message queue");
            TapjoyLog.d("TJAdUnitJSBridge", sb2.toString());
            this.c.add(new Pair(str, jSONObject));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00be  */
    public void alert(JSONObject jSONObject, final String str) {
        JSONArray jSONArray;
        String str2;
        String str3;
        TJAdUnitActivity tJAdUnitActivity;
        AlertDialog alertDialog;
        int i2;
        StringBuilder sb = new StringBuilder("alert_method: ");
        sb.append(jSONObject);
        TapjoyLog.d("TJAdUnitJSBridge", sb.toString());
        String str4 = "";
        try {
            str2 = jSONObject.getString("title");
            try {
                str3 = jSONObject.getString("message");
                try {
                    jSONArray = jSONObject.getJSONArray("buttons");
                } catch (Exception e2) {
                    e = e2;
                    invokeJSCallback(str, Boolean.FALSE);
                    e.printStackTrace();
                    jSONArray = null;
                    tJAdUnitActivity = this.g;
                    if (tJAdUnitActivity == null) {
                    }
                }
            } catch (Exception e3) {
                e = e3;
                str3 = str4;
                invokeJSCallback(str, Boolean.FALSE);
                e.printStackTrace();
                jSONArray = null;
                tJAdUnitActivity = this.g;
                if (tJAdUnitActivity == null) {
                }
            }
        } catch (Exception e4) {
            e = e4;
            str2 = "";
            str3 = str4;
            invokeJSCallback(str, Boolean.FALSE);
            e.printStackTrace();
            jSONArray = null;
            tJAdUnitActivity = this.g;
            if (tJAdUnitActivity == null) {
            }
        }
        tJAdUnitActivity = this.g;
        if (tJAdUnitActivity == null) {
            if (VERSION.SDK_INT >= 21) {
                alertDialog = new Builder(tJAdUnitActivity, 16974394).setTitle(str2).setMessage(str3).create();
            } else {
                alertDialog = new Builder(tJAdUnitActivity).setTitle(str2).setMessage(str3).create();
            }
            if (jSONArray == null || jSONArray.length() == 0) {
                invokeJSCallback(str, Boolean.FALSE);
                return;
            }
            ArrayList arrayList = new ArrayList();
            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                switch (i3) {
                    case 0:
                        i2 = -2;
                        break;
                    case 1:
                        i2 = -3;
                        break;
                    default:
                        i2 = -1;
                        break;
                }
                try {
                    arrayList.add(jSONArray.getString(i3));
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                alertDialog.setButton(i2, (CharSequence) arrayList.get(i3), new OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        int i2;
                        switch (i) {
                            case -3:
                                i2 = 1;
                                break;
                            case -1:
                                i2 = 2;
                                break;
                            default:
                                i2 = 0;
                                break;
                        }
                        try {
                            TJAdUnitJSBridge.this.invokeJSCallback(str, Integer.valueOf(i2));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
            return;
        }
        TapjoyLog.d("TJAdUnitJSBridge", "Cannot alert -- TJAdUnitActivity is null");
    }

    public void checkAppInstalled(JSONObject jSONObject, String str) {
        String str2;
        String str3 = "";
        try {
            str2 = jSONObject.getString(String.BUNDLE);
        } catch (Exception e2) {
            e2.printStackTrace();
            str2 = str3;
        }
        if (str2 != null && str2.length() > 0) {
            for (ApplicationInfo applicationInfo : this.f.getPackageManager().getInstalledApplications(0)) {
                if (applicationInfo.packageName.equals(str2)) {
                    invokeJSCallback(str, Boolean.TRUE);
                    return;
                }
            }
        }
        invokeJSCallback(str, Boolean.FALSE);
    }

    public void getInstalledAppData(JSONObject jSONObject, String str) {
        PackageManager packageManager = this.f.getPackageManager();
        List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(0);
        JSONArray jSONArray = new JSONArray();
        for (ApplicationInfo applicationInfo : installedApplications) {
            if ((applicationInfo.flags & 1) != 1) {
                HashMap hashMap = new HashMap();
                String str2 = applicationInfo.packageName;
                hashMap.put("packageName", str2);
                hashMap.put("targetSdk", Integer.valueOf(applicationInfo.targetSdkVersion));
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(str2, 4096);
                    hashMap.put("installTime", new Date(packageInfo.firstInstallTime));
                    hashMap.put("updateTime", new Date(packageInfo.lastUpdateTime));
                    hashMap.put("versionName", packageInfo.versionName);
                    hashMap.put("versionCode", Integer.valueOf(packageInfo.versionCode));
                    jSONArray.put(new JSONObject(hashMap));
                } catch (Exception unused) {
                }
            }
        }
        invokeJSCallback(str, jSONArray);
    }

    public void closeRequested(Boolean bool) {
        if (this.h == null) {
            this.closeRequested = true;
            HashMap hashMap = new HashMap();
            hashMap.put("forceClose", bool);
            invokeJSAdunitMethod(String.CLOSE_REQUESTED, (Map) hashMap);
        } else if (!this.h.goBack()) {
            this.h.a();
        }
    }

    public void getVolume(JSONObject jSONObject, String str) {
        HashMap volumeArgs = getVolumeArgs();
        if (volumeArgs != null) {
            invokeJSCallback(str, (Map) volumeArgs);
            return;
        }
        invokeJSCallback(str, Boolean.valueOf(false));
    }

    public void onVolumeChanged() {
        invokeJSAdunitMethod(String.VOLUME_CHANGED, (Map) getVolumeArgs());
    }

    public HashMap getVolumeArgs() {
        if (this.f4222a == null) {
            TapjoyLog.d("TJAdUnitJSBridge", "No ad unit provided");
            return null;
        }
        String format = String.format("%.2f", new Object[]{Float.valueOf(this.f4222a.getVolume())});
        boolean isMuted = this.f4222a.isMuted();
        StringBuilder sb = new StringBuilder("getVolumeArgs: volume=");
        sb.append(format);
        sb.append("; isMuted=");
        sb.append(isMuted);
        TapjoyLog.d("TJAdUnitJSBridge", sb.toString());
        HashMap hashMap = new HashMap();
        hashMap.put(String.CURRENT_VOLUME, format);
        hashMap.put(String.IS_MUTED, Boolean.valueOf(isMuted));
        return hashMap;
    }

    public void dismiss(JSONObject jSONObject, String str) {
        TJAdUnitActivity tJAdUnitActivity = this.g;
        if (tJAdUnitActivity != null) {
            invokeJSCallback(str, Boolean.valueOf(true));
            tJAdUnitActivity.finish();
            return;
        }
        TapjoyLog.d("TJAdUnitJSBridge", "Cannot dismiss -- TJAdUnitActivity is null");
        invokeJSCallback(str, Boolean.valueOf(false));
    }

    public void display() {
        invokeJSAdunitMethod("display", new Object[0]);
    }

    public void displayStoreURL(JSONObject jSONObject, String str) {
        displayURL(jSONObject, str);
    }

    public void dismissStoreView(JSONObject jSONObject, String str) {
        dismissSplitView(jSONObject, str);
    }

    public void displayURL(JSONObject jSONObject, String str) {
        final String str2;
        final String str3;
        try {
            String optString = jSONObject.optString("style");
            final String string = jSONObject.getString("url");
            final JSONObject optJSONObject = jSONObject.optJSONObject(String.SPLIT_VIEW_LAYOUT);
            final JSONArray optJSONArray = jSONObject.optJSONArray(String.SPLIT_VIEW_EXIT_HOSTS);
            final String optString2 = jSONObject.optString("userAgent", null);
            JSONObject optJSONObject2 = jSONObject.optJSONObject(String.SPLIT_VIEW_TRIGGER);
            if (optJSONObject2 != null) {
                String optString3 = optJSONObject2.optString(String.SPLIT_VIEW_TRIGGER_ON, null);
                str2 = optJSONObject2.optString("to", null);
                str3 = optString3;
            } else {
                str3 = null;
                str2 = null;
            }
            if (String.STYLE_SPLIT.equals(optString)) {
                final JSONObject jSONObject2 = jSONObject;
                final String str4 = str;
                AnonymousClass6 r3 = new Runnable() {
                    public final void run() {
                        if (TJAdUnitJSBridge.this.b != null) {
                            if (TJAdUnitJSBridge.this.h == null) {
                                ViewParent parent = TJAdUnitJSBridge.this.b.getParent();
                                if (parent instanceof ViewGroup) {
                                    ViewGroup viewGroup = (ViewGroup) parent;
                                    TJAdUnitJSBridge.this.h = new TJSplitWebView(TJAdUnitJSBridge.this.g, jSONObject2, TJAdUnitJSBridge.this);
                                    viewGroup.addView(TJAdUnitJSBridge.this.h, new LayoutParams(-1, -1));
                                    TJAdUnitJSBridge.this.h.animateOpen(viewGroup);
                                }
                            } else {
                                TJAdUnitJSBridge.this.h.setExitHosts(optJSONArray);
                                TJAdUnitJSBridge.this.h.applyLayoutOption(optJSONObject);
                            }
                            if (TJAdUnitJSBridge.this.h != null) {
                                if (optString2 != null) {
                                    TJAdUnitJSBridge.this.h.setUserAgent(optString2);
                                }
                                TJAdUnitJSBridge.this.h.setTrigger(str3, str2);
                                TJAdUnitJSBridge.this.splitWebViewCallbackID = str4;
                                TJAdUnitJSBridge.this.h.loadUrl(string);
                                return;
                            }
                        }
                        TJAdUnitJSBridge.this.h = null;
                        TJAdUnitJSBridge.this.splitWebViewCallbackID = null;
                        TJAdUnitJSBridge.this.invokeJSCallback(str4, Boolean.FALSE);
                    }
                };
                TapjoyUtil.runOnMainThread(r3);
                return;
            }
            this.didLaunchOtherActivity = true;
            this.otherActivityCallbackID = str;
            this.f.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(string)));
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.TRUE);
            e2.printStackTrace();
        }
    }

    public void clearCache(JSONObject jSONObject, String str) {
        if (TapjoyCache.getInstance() != null) {
            TapjoyCache.getInstance().clearTapjoyCache();
            invokeJSCallback(str, Boolean.TRUE);
            return;
        }
        invokeJSCallback(str, Boolean.FALSE);
    }

    public void setPrerenderLimit(JSONObject jSONObject, String str) {
        try {
            TJPlacementManager.setPreRenderedPlacementLimit(jSONObject.getInt(String.TJC_PLACEMENT_PRE_RENDERED_LIMIT));
            invokeJSCallback(str, Boolean.TRUE);
        } catch (Exception unused) {
            TapjoyLog.w("TJAdUnitJSBridge", "Unable to set Tapjoy placement pre-render limit. Invalid parameters.");
            invokeJSCallback(str, Boolean.FALSE);
        }
    }

    public void setEventPreloadLimit(JSONObject jSONObject, String str) {
        if (TapjoyCache.getInstance() != null) {
            try {
                TJPlacementManager.setCachedPlacementLimit(jSONObject.getInt(String.TJC_PLACEMENT_CACHE_LIMIT));
                invokeJSCallback(str, Boolean.TRUE);
            } catch (Exception unused) {
                TapjoyLog.w("TJAdUnitJSBridge", "Unable to set Tapjoy cache's event preload limit. Invalid parameters.");
                invokeJSCallback(str, Boolean.FALSE);
            }
        } else {
            invokeJSCallback(str, Boolean.FALSE);
        }
    }

    public void removeAssetFromCache(JSONObject jSONObject, String str) {
        try {
            String string = jSONObject.getString("url");
            if (TapjoyCache.getInstance() != null) {
                invokeJSCallback(str, Boolean.valueOf(TapjoyCache.getInstance().removeAssetFromCache(string)));
                return;
            }
            invokeJSCallback(str, Boolean.FALSE);
        } catch (Exception unused) {
            TapjoyLog.w("TJAdUnitJSBridge", "Unable to cache video. Invalid parameters.");
            invokeJSCallback(str, Boolean.FALSE);
        }
    }

    public void cacheAsset(JSONObject jSONObject, String str) {
        Long l2;
        String str2 = "";
        Long valueOf = Long.valueOf(0);
        try {
            String string = jSONObject.getString("url");
            try {
                str2 = jSONObject.getString(TapjoyConstants.TJC_PLACEMENT_OFFER_ID);
            } catch (Exception unused) {
            }
            try {
                l2 = Long.valueOf(jSONObject.getLong(TapjoyConstants.TJC_TIME_TO_LIVE));
            } catch (Exception unused2) {
                l2 = valueOf;
            }
            if (TapjoyCache.getInstance() != null) {
                invokeJSCallback(str, TapjoyCache.getInstance().cacheAssetFromURL(string, str2, l2.longValue()));
                return;
            }
            invokeJSCallback(str, Boolean.FALSE);
        } catch (Exception unused3) {
            TapjoyLog.w("TJAdUnitJSBridge", "Unable to cache video. Invalid parameters.");
            invokeJSCallback(str, Boolean.FALSE);
        }
    }

    public void cachePathForURL(JSONObject jSONObject, String str) {
        try {
            String string = jSONObject.getString("url");
            if (TapjoyCache.getInstance() != null) {
                invokeJSCallback(str, TapjoyCache.getInstance().getPathOfCachedURL(string));
                return;
            }
            invokeJSCallback(str, "");
        } catch (Exception unused) {
            invokeJSCallback(str, "");
        }
    }

    public void getCachedAssets(JSONObject jSONObject, String str) {
        if (TapjoyCache.getInstance() != null) {
            invokeJSCallback(str, TapjoyCache.getInstance().cachedAssetsToJSON());
            return;
        }
        invokeJSCallback(str, "");
    }

    public void contentReady(JSONObject jSONObject, String str) {
        if (this.f4222a != null) {
            this.f4222a.fireContentReady();
            invokeJSCallback(str, Boolean.valueOf(true));
            return;
        }
        invokeJSCallback(str, Boolean.valueOf(false));
    }

    public void getOrientation(JSONObject jSONObject, String str) {
        if (this.f4222a == null) {
            TapjoyLog.d("TJAdUnitJSBridge", "No ad unit provided");
            invokeJSCallback(str, JSONObject.NULL);
            return;
        }
        String screenOrientationString = this.f4222a.getScreenOrientationString();
        HashMap hashMap = new HashMap();
        hashMap.put("orientation", screenOrientationString);
        hashMap.put("width", Integer.valueOf(this.f4222a.getScreenWidth()));
        hashMap.put("height", Integer.valueOf(this.f4222a.getScreenHeight()));
        invokeJSCallback(str, (Map) hashMap);
    }

    public void setOrientation(JSONObject jSONObject, String str) {
        int i2;
        if (this.f4222a == null) {
            TapjoyLog.d("TJAdUnitJSBridge", "No ad unit provided");
            invokeJSCallback(str, Boolean.valueOf(false));
            return;
        }
        try {
            String string = jSONObject.getString("orientation");
            if (!string.equals("landscape")) {
                if (!string.equals(String.LANDSCAPE_LEFT)) {
                    i2 = string.equals(String.LANDSCAPE_RIGHT) ? 8 : 1;
                    this.f4222a.setOrientation(i2);
                    invokeJSCallback(str, Boolean.valueOf(true));
                }
            }
            i2 = 0;
            this.f4222a.setOrientation(i2);
            invokeJSCallback(str, Boolean.valueOf(true));
        } catch (Exception unused) {
            invokeJSCallback(str, Boolean.valueOf(false));
        }
    }

    public void unsetOrientation(JSONObject jSONObject, String str) {
        if (this.f4222a == null) {
            TapjoyLog.d("TJAdUnitJSBridge", "No ad unit provided");
            invokeJSCallback(str, Boolean.valueOf(false));
            return;
        }
        try {
            this.f4222a.unsetOrientation();
            invokeJSCallback(str, Boolean.valueOf(true));
        } catch (Exception unused) {
            invokeJSCallback(str, Boolean.valueOf(false));
        }
    }

    public void setBackgroundColor(JSONObject jSONObject, final String str) {
        try {
            String string = jSONObject.getString("backgroundColor");
            if (this.f4222a != null) {
                this.f4222a.setBackgroundColor(string, new AdUnitAsyncTaskListner() {
                    public final void onComplete(boolean z) {
                        TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.valueOf(z));
                    }
                });
                return;
            }
            invokeJSCallback(str, Boolean.valueOf(false));
        } catch (Exception unused) {
            TapjoyLog.w("TJAdUnitJSBridge", "Unable to set background color. Invalid parameters.");
            invokeJSCallback(str, Boolean.valueOf(false));
        }
    }

    public void setBackgroundWebViewContent(JSONObject jSONObject, final String str) {
        TapjoyLog.d("TJAdUnitJSBridge", "setBackgroundWebViewContent");
        try {
            String string = jSONObject.getString(String.BACKGROUND_CONTENT);
            if (this.f4222a != null) {
                this.f4222a.setBackgroundContent(string, new AdUnitAsyncTaskListner() {
                    public final void onComplete(boolean z) {
                        TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.valueOf(z));
                    }
                });
                return;
            }
            invokeJSCallback(str, Boolean.valueOf(false));
        } catch (Exception unused) {
            TapjoyLog.w("TJAdUnitJSBridge", "Unable to set background content. Invalid parameters.");
            invokeJSCallback(str, Boolean.valueOf(false));
        }
    }

    public void displayVideo(JSONObject jSONObject, final String str) {
        try {
            String string = jSONObject.getString("url");
            if (string.length() <= 0 || string == "") {
                invokeJSCallback(str, Boolean.FALSE);
                return;
            }
            this.f4222a.loadVideoUrl(string, new AdUnitAsyncTaskListner() {
                public final void onComplete(boolean z) {
                    TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.valueOf(z));
                }
            });
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.FALSE);
            e2.printStackTrace();
        }
    }

    public void playVideo(JSONObject jSONObject, String str) {
        if (this.f4222a != null) {
            invokeJSCallback(str, Boolean.valueOf(this.f4222a.playVideo()));
        }
    }

    public void pauseVideo(JSONObject jSONObject, String str) {
        if (this.f4222a != null) {
            invokeJSCallback(str, Boolean.valueOf(this.f4222a.pauseVideo()));
        }
    }

    public void clearVideo(JSONObject jSONObject, final String str) {
        if (this.f4222a != null) {
            this.f4222a.clearVideo(new AdUnitAsyncTaskListner() {
                public final void onComplete(boolean z) {
                    TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.valueOf(z));
                }
            }, jSONObject.optBoolean(String.VISIBLE, false));
        }
    }

    public void setVideoMute(JSONObject jSONObject, String str) {
        try {
            this.f4222a.a(jSONObject.getBoolean(String.ENABLED));
            invokeJSCallback(str, Boolean.TRUE);
        } catch (JSONException unused) {
            TapjoyLog.d("TJAdUnitJSBridge", "Failed to parse 'enabled' from json params.");
            invokeJSCallback(str, Boolean.FALSE);
        }
    }

    public void setVideoMargins(JSONObject jSONObject, String str) {
        JSONObject jSONObject2 = jSONObject;
        String str2 = str;
        try {
            final float optDouble = (float) jSONObject.optDouble(String.TOP, Utils.DOUBLE_EPSILON);
            final float optDouble2 = (float) jSONObject.optDouble("right", Utils.DOUBLE_EPSILON);
            final float optDouble3 = (float) jSONObject.optDouble(String.BOTTOM, Utils.DOUBLE_EPSILON);
            final float optDouble4 = (float) jSONObject.optDouble("left", Utils.DOUBLE_EPSILON);
            final TJAdUnitActivity tJAdUnitActivity = this.g;
            if (tJAdUnitActivity != null) {
                AnonymousClass11 r1 = new Runnable() {
                    public final void run() {
                        TJAdUnitActivity tJAdUnitActivity = tJAdUnitActivity;
                        float f2 = optDouble4;
                        float f3 = optDouble;
                        float f4 = optDouble2;
                        float f5 = optDouble3;
                        DisplayMetrics displayMetrics = tJAdUnitActivity.getResources().getDisplayMetrics();
                        ViewGroup viewGroup = (ViewGroup) tJAdUnitActivity.f4217a.getVideoView().getParent();
                        ((MarginLayoutParams) viewGroup.getLayoutParams()).setMargins((int) TypedValue.applyDimension(1, f2, displayMetrics), (int) TypedValue.applyDimension(1, f3, displayMetrics), (int) TypedValue.applyDimension(1, f4, displayMetrics), (int) TypedValue.applyDimension(1, f5, displayMetrics));
                        viewGroup.requestLayout();
                    }
                };
                TapjoyUtil.runOnMainThread(r1);
                invokeJSCallback(str2, Boolean.valueOf(true));
                return;
            }
            TapjoyLog.d("TJAdUnitJSBridge", "Cannot setVideoMargins -- TJAdUnitActivity is null");
            invokeJSCallback(str2, Boolean.valueOf(false));
        } catch (Exception e2) {
            invokeJSCallback(str2, Boolean.valueOf(false));
            e2.printStackTrace();
        }
    }

    public void log(JSONObject jSONObject, String str) {
        String str2 = "TJAdUnitJSBridge";
        try {
            StringBuilder sb = new StringBuilder("Logging message=");
            sb.append(jSONObject.getString("message"));
            TapjoyLog.d(str2, sb.toString());
            invokeJSCallback(str, Boolean.TRUE);
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.FALSE);
            e2.printStackTrace();
        }
    }

    public void openApp(JSONObject jSONObject, String str) {
        try {
            this.f.startActivity(this.f.getPackageManager().getLaunchIntentForPackage(jSONObject.getString(String.BUNDLE)));
            invokeJSCallback(str, Boolean.TRUE);
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.FALSE);
            e2.printStackTrace();
        }
    }

    @TargetApi(19)
    public void nativeEval(final JSONObject jSONObject, final String str) {
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                try {
                    if (VERSION.SDK_INT >= 19) {
                        TJAdUnitJSBridge.this.b.evaluateJavascript(jSONObject.getString("command"), null);
                    } else {
                        WebView webView = TJAdUnitJSBridge.this.b;
                        StringBuilder sb = new StringBuilder("javascript:");
                        sb.append(jSONObject.getString("command"));
                        webView.loadUrl(sb.toString());
                    }
                    TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.TRUE);
                } catch (Exception unused) {
                    TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.FALSE);
                }
            }
        });
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(11:0|1|2|3|(3:4|5|6)|7|9|10|11|12|13) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002e */
    public void present(JSONObject jSONObject, String str) {
        try {
            Boolean.valueOf(false);
            Boolean valueOf = Boolean.valueOf(false);
            Boolean valueOf2 = Boolean.valueOf(jSONObject.getString(String.VISIBLE));
            try {
                valueOf = Boolean.valueOf(jSONObject.getString("transparent"));
            } catch (Exception unused) {
            }
            this.customClose = Boolean.valueOf(jSONObject.getString(String.CUSTOM_CLOSE)).booleanValue();
            new a(this.b).execute(new Boolean[]{valueOf2, valueOf});
            invokeJSCallback(str, Boolean.TRUE);
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.FALSE);
            e2.printStackTrace();
        }
    }

    public void triggerEvent(JSONObject jSONObject, String str) {
        if (this.f4222a != null) {
            try {
                String string = jSONObject.getString("eventName");
                if (string.equals("start")) {
                    this.f4222a.fireOnVideoStart();
                } else if (string.equals("complete")) {
                    this.f4222a.fireOnVideoComplete();
                } else if (string.equals("error")) {
                    this.f4222a.fireOnVideoError("Error while trying to play video.");
                } else if (string.equals("click")) {
                    this.f4222a.fireOnClick();
                }
            } catch (Exception unused) {
                TapjoyLog.w("TJAdUnitJSBridge", "Unable to triggerEvent. No event name.");
            }
        }
    }

    public void invokeJSAdunitMethod(String str, Object... objArr) {
        this.d.callback(new ArrayList(Arrays.asList(objArr)), str, (String) null);
    }

    public void invokeJSAdunitMethod(String str, Map map) {
        this.d.callback(map, str, (String) null);
    }

    public void invokeJSCallback(String str, Object... objArr) {
        if (jr.c(str)) {
            TapjoyLog.d("TJAdUnitJSBridge", "invokeJSCallback -- no callbackID provided");
            return;
        }
        this.d.callback(new ArrayList(Arrays.asList(objArr)), "", str);
    }

    public void invokeJSCallback(String str, Map map) {
        this.d.callback(map, "", str);
    }

    public void flushBacklogMessageQueue() {
        while (true) {
            Pair pair = (Pair) this.c.poll();
            if (pair != null) {
                onDispatchMethod((String) pair.first, (JSONObject) pair.second);
            } else {
                return;
            }
        }
    }

    public void flushMessageQueue() {
        this.d.flushMessageQueue();
    }

    public void setAllowRedirect(JSONObject jSONObject, String str) {
        boolean z;
        try {
            z = jSONObject.getBoolean(String.ENABLED);
        } catch (Exception unused) {
            z = true;
        }
        this.allowRedirect = z;
        invokeJSCallback(str, Boolean.TRUE);
    }

    public void setAdUnitActivity(TJAdUnitActivity tJAdUnitActivity) {
        this.g = tJAdUnitActivity;
    }

    public void setSpinnerVisible(JSONObject jSONObject, String str) {
        try {
            boolean z = jSONObject.getBoolean(String.VISIBLE);
            String optString = jSONObject.optString("title");
            String optString2 = jSONObject.optString("message");
            TJAdUnitActivity tJAdUnitActivity = this.g;
            if (tJAdUnitActivity != null) {
                if (z) {
                    this.i = ProgressDialog.show(tJAdUnitActivity, optString, optString2);
                } else if (this.i != null) {
                    this.i.dismiss();
                }
                invokeJSCallback(str, Boolean.TRUE);
                return;
            }
            TapjoyLog.d("TJAdUnitJSBridge", "Cannot setSpinnerVisible -- TJAdUnitActivity is null");
            invokeJSCallback(str, Boolean.FALSE);
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.FALSE);
            e2.printStackTrace();
        }
    }

    public void setCloseButtonVisible(JSONObject jSONObject, String str) {
        try {
            final boolean z = jSONObject.getBoolean(String.VISIBLE);
            TapjoyUtil.runOnMainThread(new Runnable() {
                public final void run() {
                    TJAdUnitActivity b2 = TJAdUnitJSBridge.this.g;
                    if (b2 != null) {
                        b2.setCloseButtonVisibility(z);
                    } else {
                        TapjoyLog.d("TJAdUnitJSBridge", "Cannot setCloseButtonVisible -- TJAdUnitActivity is null");
                    }
                }
            });
            invokeJSCallback(str, Boolean.valueOf(true));
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.valueOf(false));
            e2.printStackTrace();
        }
    }

    public void setCloseButtonClickable(JSONObject jSONObject, String str) {
        try {
            final boolean optBoolean = jSONObject.optBoolean(String.CLICKABLE);
            TapjoyUtil.runOnMainThread(new Runnable() {
                public final void run() {
                    TJAdUnitActivity b2 = TJAdUnitJSBridge.this.g;
                    if (b2 != null) {
                        b2.setCloseButtonClickable(optBoolean);
                    } else {
                        TapjoyLog.d("TJAdUnitJSBridge", "Cannot setCloseButtonClickable -- TJAdUnitActivity is null");
                    }
                }
            });
            invokeJSCallback(str, Boolean.valueOf(true));
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.valueOf(false));
            e2.printStackTrace();
        }
    }

    public void shouldClose(JSONObject jSONObject, String str) {
        TJAdUnitActivity tJAdUnitActivity = this.g;
        try {
            Boolean.valueOf(false);
            if (Boolean.valueOf(jSONObject.getString("close")).booleanValue() && tJAdUnitActivity != null) {
                tJAdUnitActivity.finish();
            }
            invokeJSCallback(str, Boolean.TRUE);
        } catch (Exception e2) {
            invokeJSCallback(str, Boolean.FALSE);
            if (tJAdUnitActivity != null) {
                tJAdUnitActivity.finish();
            }
            e2.printStackTrace();
        }
        this.closeRequested = false;
    }

    public void setLoggingLevel(JSONObject jSONObject, String str) {
        try {
            TapjoyAppSettings.getInstance().saveLoggingLevel(String.valueOf(jSONObject.getString(String.LOGGING_LEVEL)));
        } catch (Exception e2) {
            StringBuilder sb = new StringBuilder("setLoggingLevel exception ");
            sb.append(e2.getLocalizedMessage());
            TapjoyLog.d("TJAdUnitJSBridge", sb.toString());
            invokeJSCallback(str, Boolean.valueOf(false));
            e2.printStackTrace();
        }
    }

    public void clearLoggingLevel(JSONObject jSONObject, String str) {
        TapjoyAppSettings.getInstance().clearLoggingLevel();
    }

    public void attachVolumeListener(JSONObject jSONObject, String str) {
        try {
            boolean z = jSONObject.getBoolean(String.ATTACH);
            int optInt = jSONObject.optInt(String.INTERVAL, TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
            if (optInt > 0) {
                this.f4222a.attachVolumeListener(z, optInt);
                invokeJSCallback(str, Boolean.valueOf(true));
                return;
            }
            StringBuilder sb = new StringBuilder("Invalid `interval` value passed to attachVolumeListener(): interval=");
            sb.append(optInt);
            TapjoyLog.d("TJAdUnitJSBridge", sb.toString());
            invokeJSCallback(str, Boolean.valueOf(false));
        } catch (Exception e2) {
            StringBuilder sb2 = new StringBuilder("attachVolumeListener exception ");
            sb2.append(e2.toString());
            TapjoyLog.d("TJAdUnitJSBridge", sb2.toString());
            invokeJSCallback(str, Boolean.valueOf(false));
            e2.printStackTrace();
        }
    }

    public void initMoatVideoTracker(JSONObject jSONObject, String str) {
        invokeJSCallback(str, Boolean.valueOf(true));
    }

    public void startMoatVideoTracker(JSONObject jSONObject, String str) {
        invokeJSCallback(str, Boolean.valueOf(true));
    }

    public void triggerMoatVideoEvent(JSONObject jSONObject, String str) {
        invokeJSCallback(str, Boolean.valueOf(true));
    }

    public void initViewabilityTracker(JSONObject jSONObject, String str) {
        er erVar = this.l;
        if (!erVar.a(jSONObject)) {
            erVar.f4390a.invokeJSCallback(str, Boolean.valueOf(false));
            return;
        }
        er.b(jSONObject);
        if (al.a(er.b)) {
            erVar.f4390a.invokeJSCallback(str, Boolean.valueOf(false));
            return;
        }
        TapjoyUtil.runOnMainThread(new Runnable(jSONObject, str) {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ JSONObject f4391a;
            final /* synthetic */ String b;

            {
                this.f4391a = r2;
                this.b = r3;
            }

            public final void run() {
                try {
                    if (!er.this.c) {
                        er.this.c = ck.a(ck.a(), er.this.f4390a.b.getContext());
                    }
                    if (er.this.c) {
                        TapjoyLog.d("TJOMViewabilityAgent", "initialized");
                        cu a2 = cu.a(er.this.f, er.b, er.b(this.f4391a.optJSONArray(String.VENDORS)), "");
                        er.this.e = cs.a(ct.a(cw.f4350a, cw.f4350a), a2);
                        er.this.e.a(er.this.f4390a.f4222a.getWebView());
                        er.this.g = dc.a(er.this.e);
                        er.this.d = cr.a(er.this.e);
                        er.this.f4390a.invokeJSCallback(this.b, Boolean.valueOf(true));
                        return;
                    }
                    TapjoyLog.d("TJOMViewabilityAgent", "Failed to initialize");
                    er.this.f4390a.invokeJSCallback(this.b, Boolean.valueOf(false));
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder("Failed to init with exception: ");
                    sb.append(e.getMessage());
                    TapjoyLog.d("TJOMViewabilityAgent", sb.toString());
                    er.this.f4390a.invokeJSCallback(this.b, Boolean.valueOf(false));
                }
            }
        });
    }

    public void startViewabilityTracker(JSONObject jSONObject, String str) {
        er erVar = this.l;
        if (!erVar.c) {
            TapjoyLog.d("TJOMViewabilityAgent", "Can not start -- TJOMViewabilityAgent is not initialized");
            erVar.f4390a.invokeJSCallback(str, Boolean.valueOf(false));
            return;
        }
        erVar.f4390a.invokeJSCallback(str, Boolean.valueOf(true));
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                try {
                    er.this.e.a();
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder("Failed to start with exception: ");
                    sb.append(e.getMessage());
                    TapjoyLog.d("TJOMViewabilityAgent", sb.toString());
                }
            }
        });
    }

    public void triggerViewabilityEvent(JSONObject jSONObject, String str) {
        er erVar = this.l;
        if (!erVar.c) {
            TapjoyLog.d("TJOMViewabilityAgent", "Can not triggerEvent -- TJOMViewabilityAgent is not initialized");
            erVar.f4390a.invokeJSCallback(str, Boolean.valueOf(false));
        } else if (jSONObject == null) {
            TapjoyLog.d("TJOMViewabilityAgent", "Can not triggerEvent -- json parameter is null");
            erVar.f4390a.invokeJSCallback(str, Boolean.valueOf(false));
        } else {
            String optString = jSONObject.optString("eventName", null);
            if (optString == null) {
                TapjoyLog.d("TJOMViewabilityAgent", "triggerEvent: params json did not contain 'eventName'");
                erVar.f4390a.invokeJSCallback(str, Boolean.valueOf(false));
                return;
            }
            TapjoyUtil.runOnMainThread(new Runnable(optString, str) {

                /* renamed from: a reason: collision with root package name */
                final /* synthetic */ String f4393a;
                final /* synthetic */ String b;

                {
                    this.f4393a = r2;
                    this.b = r3;
                }

                public final void run() {
                    try {
                        if (this.f4393a.equals(String.VIDEO_RENDERED)) {
                            er.this.g.a(db.a(da.d));
                            er.this.d.a();
                        } else if (this.f4393a.equals(String.VIDEO_BUFFER_START)) {
                            er.this.g.g();
                        } else if (this.f4393a.equals(String.VIDEO_BUFFER_END)) {
                            er.this.g.h();
                        } else if (this.f4393a.equals("start")) {
                            er.this.g.a((float) er.this.f4390a.f4222a.getVideoView().getDuration(), er.this.f4390a.f4222a.getVolume());
                        } else if (this.f4393a.equals("firstQuartile")) {
                            er.this.g.a();
                        } else if (this.f4393a.equals("midpoint")) {
                            er.this.g.b();
                        } else if (this.f4393a.equals("thirdQuartile")) {
                            er.this.g.c();
                        } else if (this.f4393a.equals("paused")) {
                            er.this.g.e();
                        } else if (this.f4393a.equals("playing")) {
                            er.this.g.f();
                        } else if (this.f4393a.equals(String.VIDEO_SKIPPED)) {
                            er.this.g.i();
                        } else if (this.f4393a.equals(String.VOLUME_CHANGED)) {
                            er.this.g.a(er.this.f4390a.f4222a.getVolume());
                        } else if (this.f4393a.equals("complete")) {
                            er.this.g.d();
                            er.this.e.b();
                            er.this.e = null;
                        } else {
                            String str = "TJOMViewabilityAgent";
                            StringBuilder sb = new StringBuilder("triggerEvent: event name '");
                            sb.append(this.f4393a);
                            sb.append("' not found");
                            TapjoyLog.d(str, sb.toString());
                            er.this.f4390a.invokeJSCallback(this.b, Boolean.valueOf(false));
                            return;
                        }
                        StringBuilder sb2 = new StringBuilder("triggerEvent: event name '");
                        sb2.append(this.f4393a);
                        sb2.append("'");
                        TapjoyLog.d("TJOMViewabilityAgent", sb2.toString());
                        er.this.f4390a.invokeJSCallback(this.b, Boolean.valueOf(true));
                    } catch (Exception e) {
                        StringBuilder sb3 = new StringBuilder("triggerEvent exception:");
                        sb3.append(e.getMessage());
                        TapjoyLog.d("TJOMViewabilityAgent", sb3.toString());
                        er.this.f4390a.invokeJSCallback(this.b, Boolean.valueOf(false));
                    }
                }
            });
        }
    }

    public void startUsageTrackingEvent(JSONObject jSONObject, String str) {
        try {
            String string = jSONObject.getString("name");
            if (string.isEmpty()) {
                TapjoyLog.d("TJAdUnitJSBridge", "Empty name for startUsageTrackingEvent");
                invokeJSCallback(str, Boolean.valueOf(false));
                return;
            }
            if (this.f4222a != null) {
                this.f4222a.startAdContentTracking(string, jSONObject);
                invokeJSCallback(str, Boolean.valueOf(true));
                return;
            }
            invokeJSCallback(str, Boolean.valueOf(false));
        } catch (JSONException e2) {
            StringBuilder sb = new StringBuilder("Unable to startUsageTrackingEvent. Invalid parameters: ");
            sb.append(e2);
            TapjoyLog.w("TJAdUnitJSBridge", sb.toString());
        }
    }

    public void endUsageTrackingEvent(JSONObject jSONObject, String str) {
        try {
            String string = jSONObject.getString("name");
            if (string.isEmpty()) {
                TapjoyLog.d("TJAdUnitJSBridge", "Empty name for endUsageTrackingEvent");
                invokeJSCallback(str, Boolean.valueOf(false));
                return;
            }
            if (this.f4222a != null) {
                this.f4222a.endAdContentTracking(string, jSONObject);
                invokeJSCallback(str, Boolean.valueOf(true));
                return;
            }
            invokeJSCallback(str, Boolean.valueOf(false));
        } catch (JSONException e2) {
            StringBuilder sb = new StringBuilder("Unable to endUsageTrackingEvent. Invalid parameters: ");
            sb.append(e2);
            TapjoyLog.w("TJAdUnitJSBridge", sb.toString());
        }
    }

    public void sendUsageTrackingEvent(JSONObject jSONObject, String str) {
        try {
            String string = jSONObject.getString("name");
            if (string.isEmpty()) {
                TapjoyLog.d("TJAdUnitJSBridge", "Empty name for sendUsageTrackingEvent");
                invokeJSCallback(str, Boolean.valueOf(false));
                return;
            }
            if (this.f4222a != null) {
                this.f4222a.sendAdContentTracking(string, jSONObject);
                invokeJSCallback(str, Boolean.valueOf(true));
                return;
            }
            invokeJSCallback(str, Boolean.valueOf(false));
        } catch (JSONException e2) {
            StringBuilder sb = new StringBuilder("Unable to sendUsageTrackingEvent. Invalid parameters: ");
            sb.append(e2);
            TapjoyLog.w("TJAdUnitJSBridge", sb.toString());
        }
    }

    public void hasSplitView(JSONObject jSONObject, final String str) {
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                if (TJAdUnitJSBridge.this.h != null) {
                    TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.TRUE);
                    return;
                }
                TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.FALSE);
            }
        });
    }

    public void dismissSplitView(JSONObject jSONObject, final String str) {
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                if (TJAdUnitJSBridge.this.h != null) {
                    if (str != null) {
                        TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.TRUE);
                    }
                    if (TJAdUnitJSBridge.this.splitWebViewCallbackID != null) {
                        TJAdUnitJSBridge.this.invokeJSCallback(TJAdUnitJSBridge.this.splitWebViewCallbackID, Boolean.TRUE);
                        TJAdUnitJSBridge.this.splitWebViewCallbackID = null;
                    }
                    ((ViewGroup) TJAdUnitJSBridge.this.h.getParent()).removeView(TJAdUnitJSBridge.this.h);
                    TJAdUnitJSBridge.this.h = null;
                    return;
                }
                if (str != null) {
                    TJAdUnitJSBridge.this.invokeJSCallback(str, Boolean.FALSE);
                }
            }
        });
    }

    public void getSplitViewURL(JSONObject jSONObject, final String str) {
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                if (TJAdUnitJSBridge.this.h != null) {
                    TJAdUnitJSBridge.this.invokeJSCallback(str, TJAdUnitJSBridge.this.h.getLastUrl());
                    return;
                }
                TJAdUnitJSBridge.this.invokeJSCallback(str, JSONObject.NULL);
            }
        });
    }

    public void isNetworkAvailable(JSONObject jSONObject, String str) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f.getSystemService("connectivity")).getActiveNetworkInfo();
            invokeJSCallback(str, Boolean.valueOf(activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()));
        } catch (Exception unused) {
            invokeJSCallback(str, Boolean.FALSE);
        }
    }

    public void setEnabled(boolean z) {
        this.k = z;
        if (this.k) {
            flushBacklogMessageQueue();
        }
    }

    public void onVideoReady(int i2, int i3, int i4) {
        HashMap hashMap = new HashMap();
        hashMap.put(String.VIDEO_EVENT_NAME, String.VIDEO_READY_EVENT);
        hashMap.put(String.VIDEO_DURATION, Integer.valueOf(i2));
        hashMap.put(String.VIDEO_WIDTH, Integer.valueOf(i3));
        hashMap.put(String.VIDEO_HEIGHT, Integer.valueOf(i4));
        invokeJSAdunitMethod(String.VIDEO_EVENT, (Map) hashMap);
    }

    public void onVideoStarted(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put(String.VIDEO_EVENT_NAME, String.VIDEO_START_EVENT);
        hashMap.put(String.VIDEO_CURRENT_TIME, Integer.valueOf(i2));
        invokeJSAdunitMethod(String.VIDEO_EVENT, (Map) hashMap);
    }

    public void onVideoProgress(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put(String.VIDEO_EVENT_NAME, String.VIDEO_PROGRESS_EVENT);
        hashMap.put(String.VIDEO_CURRENT_TIME, Integer.valueOf(i2));
        invokeJSAdunitMethod(String.VIDEO_EVENT, (Map) hashMap);
    }

    public void onVideoPaused(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put(String.VIDEO_EVENT_NAME, String.VIDEO_PAUSE_EVENT);
        hashMap.put(String.VIDEO_CURRENT_TIME, Integer.valueOf(i2));
        invokeJSAdunitMethod(String.VIDEO_EVENT, (Map) hashMap);
    }

    public void onVideoCompletion() {
        HashMap hashMap = new HashMap();
        hashMap.put(String.VIDEO_EVENT_NAME, String.VIDEO_COMPLETE_EVENT);
        invokeJSAdunitMethod(String.VIDEO_EVENT, (Map) hashMap);
    }

    public void onVideoInfo(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(String.VIDEO_EVENT_NAME, String.VIDEO_INFO_EVENT);
        hashMap.put(String.VIDEO_INFO, str);
        invokeJSAdunitMethod(String.VIDEO_EVENT, (Map) hashMap);
    }

    public void onVideoError(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(String.VIDEO_EVENT_NAME, String.VIDEO_ERROR_EVENT);
        hashMap.put("error", str);
        invokeJSAdunitMethod(String.VIDEO_EVENT, (Map) hashMap);
    }

    public void notifyOrientationChanged(String str, int i2, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put("orientation", str);
        hashMap.put("width", Integer.valueOf(i2));
        hashMap.put("height", Integer.valueOf(i3));
        invokeJSAdunitMethod(String.ORIENTATION_CHANGED_EVENT, (Map) hashMap);
    }
}
