package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface UserSettings {

    public enum Gender {
        OTHER(0, "O"),
        FEMALE(1, "F"),
        MALE(2, "M");
        

        /* renamed from: a reason: collision with root package name */
        private final int f1504a;
        private final String b;

        private Gender(int i, String str) {
            this.f1504a = i;
            this.b = str;
        }

        @Nullable
        public static Gender fromInteger(Integer num) {
            if (num == null) {
                return null;
            }
            switch (num.intValue()) {
                case 0:
                    return OTHER;
                case 1:
                    return FEMALE;
                case 2:
                    return MALE;
                default:
                    return null;
            }
        }

        public int getIntValue() {
            return this.f1504a;
        }

        @NonNull
        public String getStringValue() {
            return this.b;
        }
    }

    @Nullable
    Integer getAge();

    @Nullable
    Gender getGender();

    @Nullable
    String getUserId();

    UserSettings setAge(int i);

    UserSettings setGender(@NonNull Gender gender);

    UserSettings setUserId(@NonNull String str);
}
