package com.appodeal.ads;

import android.support.annotation.Nullable;
import com.appodeal.ads.utils.LogConstants;
import com.smaato.sdk.core.api.VideoType;

public enum AdType {
    Banner(4, "banner", "Banner"),
    Mrec(256, "mrec", LogConstants.KEY_MREC),
    Interstitial(1, VideoType.INTERSTITIAL, "Interstitial"),
    Video(2, "video", "Video"),
    Rewarded(128, "rewarded_video", LogConstants.KEY_REWARDED_VIDEO),
    Native(512, "native", LogConstants.KEY_NATIVE);
    
    private final int code;
    private final String codeName;
    private final String displayName;

    private AdType(int i, String str, String str2) {
        this.code = i;
        this.codeName = str;
        this.displayName = str2;
    }

    @Nullable
    public static AdType fromCode(int i) {
        AdType[] values;
        for (AdType adType : values()) {
            if (adType.code == i) {
                return adType;
            }
        }
        return null;
    }

    public int getCode() {
        return this.code;
    }

    public String getCodeName() {
        return this.codeName;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
