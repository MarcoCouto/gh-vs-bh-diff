package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.LazyStringArrayList;
import com.explorestack.protobuf.LazyStringList;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.ProtocolStringList;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

public final class Get extends GeneratedMessageV3 implements GetOrBuilder {
    public static final int CHECK_SDK_VERSION_FIELD_NUMBER = 6;
    public static final int DEBUG_FIELD_NUMBER = 4;
    private static final Get DEFAULT_INSTANCE = new Get();
    public static final int LARGE_BANNERS_FIELD_NUMBER = 2;
    /* access modifiers changed from: private */
    public static final Parser<Get> PARSER = new AbstractParser<Get>() {
        public Get parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Get(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int REWARDED_VIDEO_FIELD_NUMBER = 3;
    public static final int SHOW_ARRAY_FIELD_NUMBER = 5;
    public static final int TYPE_FIELD_NUMBER = 1;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public boolean checkSdkVersion_;
    /* access modifiers changed from: private */
    public boolean debug_;
    /* access modifiers changed from: private */
    public boolean largeBanners_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public boolean rewardedVideo_;
    /* access modifiers changed from: private */
    public LazyStringList showArray_;
    /* access modifiers changed from: private */
    public volatile Object type_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements GetOrBuilder {
        private int bitField0_;
        private boolean checkSdkVersion_;
        private boolean debug_;
        private boolean largeBanners_;
        private boolean rewardedVideo_;
        private LazyStringList showArray_;
        private Object type_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Get_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Get_fieldAccessorTable.ensureFieldAccessorsInitialized(Get.class, Builder.class);
        }

        private Builder() {
            this.type_ = "";
            this.showArray_ = LazyStringArrayList.EMPTY;
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.type_ = "";
            this.showArray_ = LazyStringArrayList.EMPTY;
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            Get.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.type_ = "";
            this.largeBanners_ = false;
            this.rewardedVideo_ = false;
            this.debug_ = false;
            this.showArray_ = LazyStringArrayList.EMPTY;
            this.bitField0_ &= -2;
            this.checkSdkVersion_ = false;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Get_descriptor;
        }

        public Get getDefaultInstanceForType() {
            return Get.getDefaultInstance();
        }

        public Get build() {
            Get buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Get buildPartial() {
            Get get = new Get((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            int i = this.bitField0_;
            get.type_ = this.type_;
            get.largeBanners_ = this.largeBanners_;
            get.rewardedVideo_ = this.rewardedVideo_;
            get.debug_ = this.debug_;
            if ((this.bitField0_ & 1) != 0) {
                this.showArray_ = this.showArray_.getUnmodifiableView();
                this.bitField0_ &= -2;
            }
            get.showArray_ = this.showArray_;
            get.checkSdkVersion_ = this.checkSdkVersion_;
            onBuilt();
            return get;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Get) {
                return mergeFrom((Get) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Get get) {
            if (get == Get.getDefaultInstance()) {
                return this;
            }
            if (!get.getType().isEmpty()) {
                this.type_ = get.type_;
                onChanged();
            }
            if (get.getLargeBanners()) {
                setLargeBanners(get.getLargeBanners());
            }
            if (get.getRewardedVideo()) {
                setRewardedVideo(get.getRewardedVideo());
            }
            if (get.getDebug()) {
                setDebug(get.getDebug());
            }
            if (!get.showArray_.isEmpty()) {
                if (this.showArray_.isEmpty()) {
                    this.showArray_ = get.showArray_;
                    this.bitField0_ &= -2;
                } else {
                    ensureShowArrayIsMutable();
                    this.showArray_.addAll(get.showArray_);
                }
                onChanged();
            }
            if (get.getCheckSdkVersion()) {
                setCheckSdkVersion(get.getCheckSdkVersion());
            }
            mergeUnknownFields(get.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Get get;
            Get get2 = null;
            try {
                Get get3 = (Get) Get.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (get3 != null) {
                    mergeFrom(get3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                get = (Get) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                get2 = get;
                if (get2 != null) {
                }
                throw th;
            }
        }

        public String getType() {
            Object obj = this.type_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.type_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getTypeBytes() {
            Object obj = this.type_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.type_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setType(String str) {
            if (str != null) {
                this.type_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearType() {
            this.type_ = Get.getDefaultInstance().getType();
            onChanged();
            return this;
        }

        public Builder setTypeBytes(ByteString byteString) {
            if (byteString != null) {
                Get.checkByteStringIsUtf8(byteString);
                this.type_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public boolean getLargeBanners() {
            return this.largeBanners_;
        }

        public Builder setLargeBanners(boolean z) {
            this.largeBanners_ = z;
            onChanged();
            return this;
        }

        public Builder clearLargeBanners() {
            this.largeBanners_ = false;
            onChanged();
            return this;
        }

        public boolean getRewardedVideo() {
            return this.rewardedVideo_;
        }

        public Builder setRewardedVideo(boolean z) {
            this.rewardedVideo_ = z;
            onChanged();
            return this;
        }

        public Builder clearRewardedVideo() {
            this.rewardedVideo_ = false;
            onChanged();
            return this;
        }

        public boolean getDebug() {
            return this.debug_;
        }

        public Builder setDebug(boolean z) {
            this.debug_ = z;
            onChanged();
            return this;
        }

        public Builder clearDebug() {
            this.debug_ = false;
            onChanged();
            return this;
        }

        private void ensureShowArrayIsMutable() {
            if ((this.bitField0_ & 1) == 0) {
                this.showArray_ = new LazyStringArrayList(this.showArray_);
                this.bitField0_ |= 1;
            }
        }

        public ProtocolStringList getShowArrayList() {
            return this.showArray_.getUnmodifiableView();
        }

        public int getShowArrayCount() {
            return this.showArray_.size();
        }

        public String getShowArray(int i) {
            return (String) this.showArray_.get(i);
        }

        public ByteString getShowArrayBytes(int i) {
            return this.showArray_.getByteString(i);
        }

        public Builder setShowArray(int i, String str) {
            if (str != null) {
                ensureShowArrayIsMutable();
                this.showArray_.set(i, str);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder addShowArray(String str) {
            if (str != null) {
                ensureShowArrayIsMutable();
                this.showArray_.add(str);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder addAllShowArray(Iterable<String> iterable) {
            ensureShowArrayIsMutable();
            com.explorestack.protobuf.AbstractMessageLite.Builder.addAll(iterable, (List<? super T>) this.showArray_);
            onChanged();
            return this;
        }

        public Builder clearShowArray() {
            this.showArray_ = LazyStringArrayList.EMPTY;
            this.bitField0_ &= -2;
            onChanged();
            return this;
        }

        public Builder addShowArrayBytes(ByteString byteString) {
            if (byteString != null) {
                Get.checkByteStringIsUtf8(byteString);
                ensureShowArrayIsMutable();
                this.showArray_.add(byteString);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        @Deprecated
        public boolean getCheckSdkVersion() {
            return this.checkSdkVersion_;
        }

        @Deprecated
        public Builder setCheckSdkVersion(boolean z) {
            this.checkSdkVersion_ = z;
            onChanged();
            return this;
        }

        @Deprecated
        public Builder clearCheckSdkVersion() {
            this.checkSdkVersion_ = false;
            onChanged();
            return this;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private Get(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Get() {
        this.memoizedIsInitialized = -1;
        this.type_ = "";
        this.showArray_ = LazyStringArrayList.EMPTY;
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Get();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private Get(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 10) {
                            this.type_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 16) {
                            this.largeBanners_ = codedInputStream.readBool();
                        } else if (readTag == 24) {
                            this.rewardedVideo_ = codedInputStream.readBool();
                        } else if (readTag == 32) {
                            this.debug_ = codedInputStream.readBool();
                        } else if (readTag == 42) {
                            String readStringRequireUtf8 = codedInputStream.readStringRequireUtf8();
                            if (!z2 || !true) {
                                this.showArray_ = new LazyStringArrayList();
                                z2 |= true;
                            }
                            this.showArray_.add(readStringRequireUtf8);
                        } else if (readTag == 48) {
                            this.checkSdkVersion_ = codedInputStream.readBool();
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    if (z2 && true) {
                        this.showArray_ = this.showArray_.getUnmodifiableView();
                    }
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            if (z2 && true) {
                this.showArray_ = this.showArray_.getUnmodifiableView();
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Get_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Get_fieldAccessorTable.ensureFieldAccessorsInitialized(Get.class, Builder.class);
    }

    public String getType() {
        Object obj = this.type_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.type_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getTypeBytes() {
        Object obj = this.type_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.type_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean getLargeBanners() {
        return this.largeBanners_;
    }

    public boolean getRewardedVideo() {
        return this.rewardedVideo_;
    }

    public boolean getDebug() {
        return this.debug_;
    }

    public ProtocolStringList getShowArrayList() {
        return this.showArray_;
    }

    public int getShowArrayCount() {
        return this.showArray_.size();
    }

    public String getShowArray(int i) {
        return (String) this.showArray_.get(i);
    }

    public ByteString getShowArrayBytes(int i) {
        return this.showArray_.getByteString(i);
    }

    @Deprecated
    public boolean getCheckSdkVersion() {
        return this.checkSdkVersion_;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (!getTypeBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 1, this.type_);
        }
        if (this.largeBanners_) {
            codedOutputStream.writeBool(2, this.largeBanners_);
        }
        if (this.rewardedVideo_) {
            codedOutputStream.writeBool(3, this.rewardedVideo_);
        }
        if (this.debug_) {
            codedOutputStream.writeBool(4, this.debug_);
        }
        for (int i = 0; i < this.showArray_.size(); i++) {
            GeneratedMessageV3.writeString(codedOutputStream, 5, this.showArray_.getRaw(i));
        }
        if (this.checkSdkVersion_) {
            codedOutputStream.writeBool(6, this.checkSdkVersion_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int computeStringSize = !getTypeBytes().isEmpty() ? GeneratedMessageV3.computeStringSize(1, this.type_) + 0 : 0;
        if (this.largeBanners_) {
            computeStringSize += CodedOutputStream.computeBoolSize(2, this.largeBanners_);
        }
        if (this.rewardedVideo_) {
            computeStringSize += CodedOutputStream.computeBoolSize(3, this.rewardedVideo_);
        }
        if (this.debug_) {
            computeStringSize += CodedOutputStream.computeBoolSize(4, this.debug_);
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.showArray_.size(); i3++) {
            i2 += computeStringSizeNoTag(this.showArray_.getRaw(i3));
        }
        int size = computeStringSize + i2 + (getShowArrayList().size() * 1);
        if (this.checkSdkVersion_) {
            size += CodedOutputStream.computeBoolSize(6, this.checkSdkVersion_);
        }
        int serializedSize = size + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Get)) {
            return super.equals(obj);
        }
        Get get = (Get) obj;
        if (getType().equals(get.getType()) && getLargeBanners() == get.getLargeBanners() && getRewardedVideo() == get.getRewardedVideo() && getDebug() == get.getDebug() && getShowArrayList().equals(get.getShowArrayList()) && getCheckSdkVersion() == get.getCheckSdkVersion() && this.unknownFields.equals(get.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getType().hashCode()) * 37) + 2) * 53) + Internal.hashBoolean(getLargeBanners())) * 37) + 3) * 53) + Internal.hashBoolean(getRewardedVideo())) * 37) + 4) * 53) + Internal.hashBoolean(getDebug());
        if (getShowArrayCount() > 0) {
            hashCode = (((hashCode * 37) + 5) * 53) + getShowArrayList().hashCode();
        }
        int hashBoolean = (((((hashCode * 37) + 6) * 53) + Internal.hashBoolean(getCheckSdkVersion())) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashBoolean;
        return hashBoolean;
    }

    public static Get parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Get) PARSER.parseFrom(byteBuffer);
    }

    public static Get parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Get) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Get parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Get) PARSER.parseFrom(byteString);
    }

    public static Get parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Get) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Get parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Get) PARSER.parseFrom(bArr);
    }

    public static Get parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Get) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Get parseFrom(InputStream inputStream) throws IOException {
        return (Get) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Get parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Get) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Get parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Get) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Get parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Get) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Get parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Get) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Get parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Get) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Get get) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(get);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Get getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Get> parser() {
        return PARSER;
    }

    public Parser<Get> getParserForType() {
        return PARSER;
    }

    public Get getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
