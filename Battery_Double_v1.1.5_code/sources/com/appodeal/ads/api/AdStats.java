package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class AdStats extends GeneratedMessageV3 implements AdStatsOrBuilder {
    public static final int BANNER_320_CLICK_FIELD_NUMBER = 13;
    public static final int BANNER_320_SHOW_FIELD_NUMBER = 12;
    public static final int BANNER_CLICK_FIELD_NUMBER = 5;
    public static final int BANNER_MREC_CLICK_FIELD_NUMBER = 15;
    public static final int BANNER_MREC_SHOW_FIELD_NUMBER = 14;
    public static final int BANNER_SHOW_FIELD_NUMBER = 4;
    public static final int CLICK_FIELD_NUMBER = 2;
    private static final AdStats DEFAULT_INSTANCE = new AdStats();
    public static final int FINISH_FIELD_NUMBER = 3;
    public static final int NATIVE_CLICK_FIELD_NUMBER = 17;
    public static final int NATIVE_SHOW_FIELD_NUMBER = 16;
    /* access modifiers changed from: private */
    public static final Parser<AdStats> PARSER = new AbstractParser<AdStats>() {
        public AdStats parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new AdStats(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int REWARDED_VIDEO_CLICK_FIELD_NUMBER = 10;
    public static final int REWARDED_VIDEO_FINISH_FIELD_NUMBER = 11;
    public static final int REWARDED_VIDEO_SHOW_FIELD_NUMBER = 9;
    public static final int SHOW_FIELD_NUMBER = 1;
    public static final int VIDEO_CLICK_FIELD_NUMBER = 7;
    public static final int VIDEO_FINISH_FIELD_NUMBER = 8;
    public static final int VIDEO_SHOW_FIELD_NUMBER = 6;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public int banner320Click_;
    /* access modifiers changed from: private */
    public int banner320Show_;
    /* access modifiers changed from: private */
    public int bannerClick_;
    /* access modifiers changed from: private */
    public int bannerMrecClick_;
    /* access modifiers changed from: private */
    public int bannerMrecShow_;
    /* access modifiers changed from: private */
    public int bannerShow_;
    /* access modifiers changed from: private */
    public int click_;
    /* access modifiers changed from: private */
    public int finish_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public int nativeClick_;
    /* access modifiers changed from: private */
    public int nativeShow_;
    /* access modifiers changed from: private */
    public int rewardedVideoClick_;
    /* access modifiers changed from: private */
    public int rewardedVideoFinish_;
    /* access modifiers changed from: private */
    public int rewardedVideoShow_;
    /* access modifiers changed from: private */
    public int show_;
    /* access modifiers changed from: private */
    public int videoClick_;
    /* access modifiers changed from: private */
    public int videoFinish_;
    /* access modifiers changed from: private */
    public int videoShow_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements AdStatsOrBuilder {
        private int banner320Click_;
        private int banner320Show_;
        private int bannerClick_;
        private int bannerMrecClick_;
        private int bannerMrecShow_;
        private int bannerShow_;
        private int click_;
        private int finish_;
        private int nativeClick_;
        private int nativeShow_;
        private int rewardedVideoClick_;
        private int rewardedVideoFinish_;
        private int rewardedVideoShow_;
        private int show_;
        private int videoClick_;
        private int videoFinish_;
        private int videoShow_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_AdStats_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_AdStats_fieldAccessorTable.ensureFieldAccessorsInitialized(AdStats.class, Builder.class);
        }

        private Builder() {
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            AdStats.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.show_ = 0;
            this.click_ = 0;
            this.finish_ = 0;
            this.bannerShow_ = 0;
            this.bannerClick_ = 0;
            this.videoShow_ = 0;
            this.videoClick_ = 0;
            this.videoFinish_ = 0;
            this.rewardedVideoShow_ = 0;
            this.rewardedVideoClick_ = 0;
            this.rewardedVideoFinish_ = 0;
            this.banner320Show_ = 0;
            this.banner320Click_ = 0;
            this.bannerMrecShow_ = 0;
            this.bannerMrecClick_ = 0;
            this.nativeShow_ = 0;
            this.nativeClick_ = 0;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_AdStats_descriptor;
        }

        public AdStats getDefaultInstanceForType() {
            return AdStats.getDefaultInstance();
        }

        public AdStats build() {
            AdStats buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public AdStats buildPartial() {
            AdStats adStats = new AdStats((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            adStats.show_ = this.show_;
            adStats.click_ = this.click_;
            adStats.finish_ = this.finish_;
            adStats.bannerShow_ = this.bannerShow_;
            adStats.bannerClick_ = this.bannerClick_;
            adStats.videoShow_ = this.videoShow_;
            adStats.videoClick_ = this.videoClick_;
            adStats.videoFinish_ = this.videoFinish_;
            adStats.rewardedVideoShow_ = this.rewardedVideoShow_;
            adStats.rewardedVideoClick_ = this.rewardedVideoClick_;
            adStats.rewardedVideoFinish_ = this.rewardedVideoFinish_;
            adStats.banner320Show_ = this.banner320Show_;
            adStats.banner320Click_ = this.banner320Click_;
            adStats.bannerMrecShow_ = this.bannerMrecShow_;
            adStats.bannerMrecClick_ = this.bannerMrecClick_;
            adStats.nativeShow_ = this.nativeShow_;
            adStats.nativeClick_ = this.nativeClick_;
            onBuilt();
            return adStats;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof AdStats) {
                return mergeFrom((AdStats) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(AdStats adStats) {
            if (adStats == AdStats.getDefaultInstance()) {
                return this;
            }
            if (adStats.getShow() != 0) {
                setShow(adStats.getShow());
            }
            if (adStats.getClick() != 0) {
                setClick(adStats.getClick());
            }
            if (adStats.getFinish() != 0) {
                setFinish(adStats.getFinish());
            }
            if (adStats.getBannerShow() != 0) {
                setBannerShow(adStats.getBannerShow());
            }
            if (adStats.getBannerClick() != 0) {
                setBannerClick(adStats.getBannerClick());
            }
            if (adStats.getVideoShow() != 0) {
                setVideoShow(adStats.getVideoShow());
            }
            if (adStats.getVideoClick() != 0) {
                setVideoClick(adStats.getVideoClick());
            }
            if (adStats.getVideoFinish() != 0) {
                setVideoFinish(adStats.getVideoFinish());
            }
            if (adStats.getRewardedVideoShow() != 0) {
                setRewardedVideoShow(adStats.getRewardedVideoShow());
            }
            if (adStats.getRewardedVideoClick() != 0) {
                setRewardedVideoClick(adStats.getRewardedVideoClick());
            }
            if (adStats.getRewardedVideoFinish() != 0) {
                setRewardedVideoFinish(adStats.getRewardedVideoFinish());
            }
            if (adStats.getBanner320Show() != 0) {
                setBanner320Show(adStats.getBanner320Show());
            }
            if (adStats.getBanner320Click() != 0) {
                setBanner320Click(adStats.getBanner320Click());
            }
            if (adStats.getBannerMrecShow() != 0) {
                setBannerMrecShow(adStats.getBannerMrecShow());
            }
            if (adStats.getBannerMrecClick() != 0) {
                setBannerMrecClick(adStats.getBannerMrecClick());
            }
            if (adStats.getNativeShow() != 0) {
                setNativeShow(adStats.getNativeShow());
            }
            if (adStats.getNativeClick() != 0) {
                setNativeClick(adStats.getNativeClick());
            }
            mergeUnknownFields(adStats.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            AdStats adStats;
            AdStats adStats2 = null;
            try {
                AdStats adStats3 = (AdStats) AdStats.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (adStats3 != null) {
                    mergeFrom(adStats3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                adStats = (AdStats) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                adStats2 = adStats;
                if (adStats2 != null) {
                }
                throw th;
            }
        }

        public int getShow() {
            return this.show_;
        }

        public Builder setShow(int i) {
            this.show_ = i;
            onChanged();
            return this;
        }

        public Builder clearShow() {
            this.show_ = 0;
            onChanged();
            return this;
        }

        public int getClick() {
            return this.click_;
        }

        public Builder setClick(int i) {
            this.click_ = i;
            onChanged();
            return this;
        }

        public Builder clearClick() {
            this.click_ = 0;
            onChanged();
            return this;
        }

        public int getFinish() {
            return this.finish_;
        }

        public Builder setFinish(int i) {
            this.finish_ = i;
            onChanged();
            return this;
        }

        public Builder clearFinish() {
            this.finish_ = 0;
            onChanged();
            return this;
        }

        public int getBannerShow() {
            return this.bannerShow_;
        }

        public Builder setBannerShow(int i) {
            this.bannerShow_ = i;
            onChanged();
            return this;
        }

        public Builder clearBannerShow() {
            this.bannerShow_ = 0;
            onChanged();
            return this;
        }

        public int getBannerClick() {
            return this.bannerClick_;
        }

        public Builder setBannerClick(int i) {
            this.bannerClick_ = i;
            onChanged();
            return this;
        }

        public Builder clearBannerClick() {
            this.bannerClick_ = 0;
            onChanged();
            return this;
        }

        public int getVideoShow() {
            return this.videoShow_;
        }

        public Builder setVideoShow(int i) {
            this.videoShow_ = i;
            onChanged();
            return this;
        }

        public Builder clearVideoShow() {
            this.videoShow_ = 0;
            onChanged();
            return this;
        }

        public int getVideoClick() {
            return this.videoClick_;
        }

        public Builder setVideoClick(int i) {
            this.videoClick_ = i;
            onChanged();
            return this;
        }

        public Builder clearVideoClick() {
            this.videoClick_ = 0;
            onChanged();
            return this;
        }

        public int getVideoFinish() {
            return this.videoFinish_;
        }

        public Builder setVideoFinish(int i) {
            this.videoFinish_ = i;
            onChanged();
            return this;
        }

        public Builder clearVideoFinish() {
            this.videoFinish_ = 0;
            onChanged();
            return this;
        }

        public int getRewardedVideoShow() {
            return this.rewardedVideoShow_;
        }

        public Builder setRewardedVideoShow(int i) {
            this.rewardedVideoShow_ = i;
            onChanged();
            return this;
        }

        public Builder clearRewardedVideoShow() {
            this.rewardedVideoShow_ = 0;
            onChanged();
            return this;
        }

        public int getRewardedVideoClick() {
            return this.rewardedVideoClick_;
        }

        public Builder setRewardedVideoClick(int i) {
            this.rewardedVideoClick_ = i;
            onChanged();
            return this;
        }

        public Builder clearRewardedVideoClick() {
            this.rewardedVideoClick_ = 0;
            onChanged();
            return this;
        }

        public int getRewardedVideoFinish() {
            return this.rewardedVideoFinish_;
        }

        public Builder setRewardedVideoFinish(int i) {
            this.rewardedVideoFinish_ = i;
            onChanged();
            return this;
        }

        public Builder clearRewardedVideoFinish() {
            this.rewardedVideoFinish_ = 0;
            onChanged();
            return this;
        }

        public int getBanner320Show() {
            return this.banner320Show_;
        }

        public Builder setBanner320Show(int i) {
            this.banner320Show_ = i;
            onChanged();
            return this;
        }

        public Builder clearBanner320Show() {
            this.banner320Show_ = 0;
            onChanged();
            return this;
        }

        public int getBanner320Click() {
            return this.banner320Click_;
        }

        public Builder setBanner320Click(int i) {
            this.banner320Click_ = i;
            onChanged();
            return this;
        }

        public Builder clearBanner320Click() {
            this.banner320Click_ = 0;
            onChanged();
            return this;
        }

        public int getBannerMrecShow() {
            return this.bannerMrecShow_;
        }

        public Builder setBannerMrecShow(int i) {
            this.bannerMrecShow_ = i;
            onChanged();
            return this;
        }

        public Builder clearBannerMrecShow() {
            this.bannerMrecShow_ = 0;
            onChanged();
            return this;
        }

        public int getBannerMrecClick() {
            return this.bannerMrecClick_;
        }

        public Builder setBannerMrecClick(int i) {
            this.bannerMrecClick_ = i;
            onChanged();
            return this;
        }

        public Builder clearBannerMrecClick() {
            this.bannerMrecClick_ = 0;
            onChanged();
            return this;
        }

        public int getNativeShow() {
            return this.nativeShow_;
        }

        public Builder setNativeShow(int i) {
            this.nativeShow_ = i;
            onChanged();
            return this;
        }

        public Builder clearNativeShow() {
            this.nativeShow_ = 0;
            onChanged();
            return this;
        }

        public int getNativeClick() {
            return this.nativeClick_;
        }

        public Builder setNativeClick(int i) {
            this.nativeClick_ = i;
            onChanged();
            return this;
        }

        public Builder clearNativeClick() {
            this.nativeClick_ = 0;
            onChanged();
            return this;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private AdStats(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private AdStats() {
        this.memoizedIsInitialized = -1;
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new AdStats();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private AdStats(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            z = true;
                            break;
                        case 8:
                            this.show_ = codedInputStream.readInt32();
                            break;
                        case 16:
                            this.click_ = codedInputStream.readInt32();
                            break;
                        case 24:
                            this.finish_ = codedInputStream.readInt32();
                            break;
                        case 32:
                            this.bannerShow_ = codedInputStream.readInt32();
                            break;
                        case 40:
                            this.bannerClick_ = codedInputStream.readInt32();
                            break;
                        case 48:
                            this.videoShow_ = codedInputStream.readInt32();
                            break;
                        case 56:
                            this.videoClick_ = codedInputStream.readInt32();
                            break;
                        case 64:
                            this.videoFinish_ = codedInputStream.readInt32();
                            break;
                        case 72:
                            this.rewardedVideoShow_ = codedInputStream.readInt32();
                            break;
                        case 80:
                            this.rewardedVideoClick_ = codedInputStream.readInt32();
                            break;
                        case 88:
                            this.rewardedVideoFinish_ = codedInputStream.readInt32();
                            break;
                        case 96:
                            this.banner320Show_ = codedInputStream.readInt32();
                            break;
                        case 104:
                            this.banner320Click_ = codedInputStream.readInt32();
                            break;
                        case 112:
                            this.bannerMrecShow_ = codedInputStream.readInt32();
                            break;
                        case 120:
                            this.bannerMrecClick_ = codedInputStream.readInt32();
                            break;
                        case 128:
                            this.nativeShow_ = codedInputStream.readInt32();
                            break;
                        case 136:
                            this.nativeClick_ = codedInputStream.readInt32();
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                                break;
                            }
                            z = true;
                            break;
                    }
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_AdStats_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_AdStats_fieldAccessorTable.ensureFieldAccessorsInitialized(AdStats.class, Builder.class);
    }

    public int getShow() {
        return this.show_;
    }

    public int getClick() {
        return this.click_;
    }

    public int getFinish() {
        return this.finish_;
    }

    public int getBannerShow() {
        return this.bannerShow_;
    }

    public int getBannerClick() {
        return this.bannerClick_;
    }

    public int getVideoShow() {
        return this.videoShow_;
    }

    public int getVideoClick() {
        return this.videoClick_;
    }

    public int getVideoFinish() {
        return this.videoFinish_;
    }

    public int getRewardedVideoShow() {
        return this.rewardedVideoShow_;
    }

    public int getRewardedVideoClick() {
        return this.rewardedVideoClick_;
    }

    public int getRewardedVideoFinish() {
        return this.rewardedVideoFinish_;
    }

    public int getBanner320Show() {
        return this.banner320Show_;
    }

    public int getBanner320Click() {
        return this.banner320Click_;
    }

    public int getBannerMrecShow() {
        return this.bannerMrecShow_;
    }

    public int getBannerMrecClick() {
        return this.bannerMrecClick_;
    }

    public int getNativeShow() {
        return this.nativeShow_;
    }

    public int getNativeClick() {
        return this.nativeClick_;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.show_ != 0) {
            codedOutputStream.writeInt32(1, this.show_);
        }
        if (this.click_ != 0) {
            codedOutputStream.writeInt32(2, this.click_);
        }
        if (this.finish_ != 0) {
            codedOutputStream.writeInt32(3, this.finish_);
        }
        if (this.bannerShow_ != 0) {
            codedOutputStream.writeInt32(4, this.bannerShow_);
        }
        if (this.bannerClick_ != 0) {
            codedOutputStream.writeInt32(5, this.bannerClick_);
        }
        if (this.videoShow_ != 0) {
            codedOutputStream.writeInt32(6, this.videoShow_);
        }
        if (this.videoClick_ != 0) {
            codedOutputStream.writeInt32(7, this.videoClick_);
        }
        if (this.videoFinish_ != 0) {
            codedOutputStream.writeInt32(8, this.videoFinish_);
        }
        if (this.rewardedVideoShow_ != 0) {
            codedOutputStream.writeInt32(9, this.rewardedVideoShow_);
        }
        if (this.rewardedVideoClick_ != 0) {
            codedOutputStream.writeInt32(10, this.rewardedVideoClick_);
        }
        if (this.rewardedVideoFinish_ != 0) {
            codedOutputStream.writeInt32(11, this.rewardedVideoFinish_);
        }
        if (this.banner320Show_ != 0) {
            codedOutputStream.writeInt32(12, this.banner320Show_);
        }
        if (this.banner320Click_ != 0) {
            codedOutputStream.writeInt32(13, this.banner320Click_);
        }
        if (this.bannerMrecShow_ != 0) {
            codedOutputStream.writeInt32(14, this.bannerMrecShow_);
        }
        if (this.bannerMrecClick_ != 0) {
            codedOutputStream.writeInt32(15, this.bannerMrecClick_);
        }
        if (this.nativeShow_ != 0) {
            codedOutputStream.writeInt32(16, this.nativeShow_);
        }
        if (this.nativeClick_ != 0) {
            codedOutputStream.writeInt32(17, this.nativeClick_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.show_ != 0) {
            i2 = 0 + CodedOutputStream.computeInt32Size(1, this.show_);
        }
        if (this.click_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(2, this.click_);
        }
        if (this.finish_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(3, this.finish_);
        }
        if (this.bannerShow_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(4, this.bannerShow_);
        }
        if (this.bannerClick_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(5, this.bannerClick_);
        }
        if (this.videoShow_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(6, this.videoShow_);
        }
        if (this.videoClick_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(7, this.videoClick_);
        }
        if (this.videoFinish_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(8, this.videoFinish_);
        }
        if (this.rewardedVideoShow_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(9, this.rewardedVideoShow_);
        }
        if (this.rewardedVideoClick_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(10, this.rewardedVideoClick_);
        }
        if (this.rewardedVideoFinish_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(11, this.rewardedVideoFinish_);
        }
        if (this.banner320Show_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(12, this.banner320Show_);
        }
        if (this.banner320Click_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(13, this.banner320Click_);
        }
        if (this.bannerMrecShow_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(14, this.bannerMrecShow_);
        }
        if (this.bannerMrecClick_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(15, this.bannerMrecClick_);
        }
        if (this.nativeShow_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(16, this.nativeShow_);
        }
        if (this.nativeClick_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(17, this.nativeClick_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AdStats)) {
            return super.equals(obj);
        }
        AdStats adStats = (AdStats) obj;
        if (getShow() == adStats.getShow() && getClick() == adStats.getClick() && getFinish() == adStats.getFinish() && getBannerShow() == adStats.getBannerShow() && getBannerClick() == adStats.getBannerClick() && getVideoShow() == adStats.getVideoShow() && getVideoClick() == adStats.getVideoClick() && getVideoFinish() == adStats.getVideoFinish() && getRewardedVideoShow() == adStats.getRewardedVideoShow() && getRewardedVideoClick() == adStats.getRewardedVideoClick() && getRewardedVideoFinish() == adStats.getRewardedVideoFinish() && getBanner320Show() == adStats.getBanner320Show() && getBanner320Click() == adStats.getBanner320Click() && getBannerMrecShow() == adStats.getBannerMrecShow() && getBannerMrecClick() == adStats.getBannerMrecClick() && getNativeShow() == adStats.getNativeShow() && getNativeClick() == adStats.getNativeClick() && this.unknownFields.equals(adStats.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getShow()) * 37) + 2) * 53) + getClick()) * 37) + 3) * 53) + getFinish()) * 37) + 4) * 53) + getBannerShow()) * 37) + 5) * 53) + getBannerClick()) * 37) + 6) * 53) + getVideoShow()) * 37) + 7) * 53) + getVideoClick()) * 37) + 8) * 53) + getVideoFinish()) * 37) + 9) * 53) + getRewardedVideoShow()) * 37) + 10) * 53) + getRewardedVideoClick()) * 37) + 11) * 53) + getRewardedVideoFinish()) * 37) + 12) * 53) + getBanner320Show()) * 37) + 13) * 53) + getBanner320Click()) * 37) + 14) * 53) + getBannerMrecShow()) * 37) + 15) * 53) + getBannerMrecClick()) * 37) + 16) * 53) + getNativeShow()) * 37) + 17) * 53) + getNativeClick()) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode;
        return hashCode;
    }

    public static AdStats parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (AdStats) PARSER.parseFrom(byteBuffer);
    }

    public static AdStats parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AdStats) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static AdStats parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (AdStats) PARSER.parseFrom(byteString);
    }

    public static AdStats parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AdStats) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static AdStats parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (AdStats) PARSER.parseFrom(bArr);
    }

    public static AdStats parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AdStats) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static AdStats parseFrom(InputStream inputStream) throws IOException {
        return (AdStats) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static AdStats parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AdStats) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static AdStats parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (AdStats) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static AdStats parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AdStats) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static AdStats parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (AdStats) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static AdStats parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AdStats) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(AdStats adStats) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(adStats);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static AdStats getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<AdStats> parser() {
        return PARSER;
    }

    public Parser<AdStats> getParserForType() {
        return PARSER;
    }

    public AdStats getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
