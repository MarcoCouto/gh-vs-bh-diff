package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class Regs extends GeneratedMessageV3 implements RegsOrBuilder {
    public static final int COPPA_FIELD_NUMBER = 1;
    private static final Regs DEFAULT_INSTANCE = new Regs();
    /* access modifiers changed from: private */
    public static final Parser<Regs> PARSER = new AbstractParser<Regs>() {
        public Regs parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Regs(codedInputStream, extensionRegistryLite);
        }
    };
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public boolean coppa_;
    private byte memoizedIsInitialized;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements RegsOrBuilder {
        private boolean coppa_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Regs_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Regs_fieldAccessorTable.ensureFieldAccessorsInitialized(Regs.class, Builder.class);
        }

        private Builder() {
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            Regs.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.coppa_ = false;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Regs_descriptor;
        }

        public Regs getDefaultInstanceForType() {
            return Regs.getDefaultInstance();
        }

        public Regs build() {
            Regs buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Regs buildPartial() {
            Regs regs = new Regs((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            regs.coppa_ = this.coppa_;
            onBuilt();
            return regs;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Regs) {
                return mergeFrom((Regs) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Regs regs) {
            if (regs == Regs.getDefaultInstance()) {
                return this;
            }
            if (regs.getCoppa()) {
                setCoppa(regs.getCoppa());
            }
            mergeUnknownFields(regs.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Regs regs;
            Regs regs2 = null;
            try {
                Regs regs3 = (Regs) Regs.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (regs3 != null) {
                    mergeFrom(regs3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                regs = (Regs) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                regs2 = regs;
                if (regs2 != null) {
                }
                throw th;
            }
        }

        public boolean getCoppa() {
            return this.coppa_;
        }

        public Builder setCoppa(boolean z) {
            this.coppa_ = z;
            onChanged();
            return this;
        }

        public Builder clearCoppa() {
            this.coppa_ = false;
            onChanged();
            return this;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private Regs(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Regs() {
        this.memoizedIsInitialized = -1;
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Regs();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private Regs(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            this.coppa_ = codedInputStream.readBool();
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Regs_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Regs_fieldAccessorTable.ensureFieldAccessorsInitialized(Regs.class, Builder.class);
    }

    public boolean getCoppa() {
        return this.coppa_;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.coppa_) {
            codedOutputStream.writeBool(1, this.coppa_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.coppa_) {
            i2 = 0 + CodedOutputStream.computeBoolSize(1, this.coppa_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Regs)) {
            return super.equals(obj);
        }
        Regs regs = (Regs) obj;
        if (getCoppa() == regs.getCoppa() && this.unknownFields.equals(regs.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + Internal.hashBoolean(getCoppa())) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode;
        return hashCode;
    }

    public static Regs parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Regs) PARSER.parseFrom(byteBuffer);
    }

    public static Regs parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Regs) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Regs parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Regs) PARSER.parseFrom(byteString);
    }

    public static Regs parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Regs) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Regs parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Regs) PARSER.parseFrom(bArr);
    }

    public static Regs parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Regs) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Regs parseFrom(InputStream inputStream) throws IOException {
        return (Regs) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Regs parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Regs) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Regs parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Regs) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Regs parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Regs) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Regs parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Regs) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Regs parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Regs) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Regs regs) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(regs);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Regs getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Regs> parser() {
        return PARSER;
    }

    public Parser<Regs> getParserForType() {
        return PARSER;
    }

    public Regs getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
