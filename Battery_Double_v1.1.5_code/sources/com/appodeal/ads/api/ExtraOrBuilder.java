package com.appodeal.ads.api;

import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.MessageOrBuilder;
import java.util.List;

public interface ExtraOrBuilder extends MessageOrBuilder {
    String getAdUnitStat();

    ByteString getAdUnitStatBytes();

    String getApps(int i);

    ByteString getAppsBytes(int i);

    int getAppsCount();

    List<String> getAppsList();

    float getPriceFloor();

    String getSa(int i);

    ByteString getSaBytes(int i);

    int getSaCount();

    List<String> getSaList();
}
