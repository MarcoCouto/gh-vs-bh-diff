package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.LazyStringArrayList;
import com.explorestack.protobuf.LazyStringList;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.ProtocolStringList;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

public final class Extra extends GeneratedMessageV3 implements ExtraOrBuilder {
    public static final int AD_UNIT_STAT_FIELD_NUMBER = 2;
    public static final int APPS_FIELD_NUMBER = 3;
    private static final Extra DEFAULT_INSTANCE = new Extra();
    /* access modifiers changed from: private */
    public static final Parser<Extra> PARSER = new AbstractParser<Extra>() {
        public Extra parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Extra(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int PRICE_FLOOR_FIELD_NUMBER = 1;
    public static final int SA_FIELD_NUMBER = 4;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public volatile Object adUnitStat_;
    /* access modifiers changed from: private */
    public LazyStringList apps_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public float priceFloor_;
    /* access modifiers changed from: private */
    public LazyStringList sa_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements ExtraOrBuilder {
        private Object adUnitStat_;
        private LazyStringList apps_;
        private int bitField0_;
        private float priceFloor_;
        private LazyStringList sa_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Extra_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Extra_fieldAccessorTable.ensureFieldAccessorsInitialized(Extra.class, Builder.class);
        }

        private Builder() {
            this.adUnitStat_ = "";
            this.apps_ = LazyStringArrayList.EMPTY;
            this.sa_ = LazyStringArrayList.EMPTY;
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.adUnitStat_ = "";
            this.apps_ = LazyStringArrayList.EMPTY;
            this.sa_ = LazyStringArrayList.EMPTY;
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            Extra.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.priceFloor_ = 0.0f;
            this.adUnitStat_ = "";
            this.apps_ = LazyStringArrayList.EMPTY;
            this.bitField0_ &= -2;
            this.sa_ = LazyStringArrayList.EMPTY;
            this.bitField0_ &= -3;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Extra_descriptor;
        }

        public Extra getDefaultInstanceForType() {
            return Extra.getDefaultInstance();
        }

        public Extra build() {
            Extra buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Extra buildPartial() {
            Extra extra = new Extra((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            int i = this.bitField0_;
            extra.priceFloor_ = this.priceFloor_;
            extra.adUnitStat_ = this.adUnitStat_;
            if ((this.bitField0_ & 1) != 0) {
                this.apps_ = this.apps_.getUnmodifiableView();
                this.bitField0_ &= -2;
            }
            extra.apps_ = this.apps_;
            if ((this.bitField0_ & 2) != 0) {
                this.sa_ = this.sa_.getUnmodifiableView();
                this.bitField0_ &= -3;
            }
            extra.sa_ = this.sa_;
            onBuilt();
            return extra;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Extra) {
                return mergeFrom((Extra) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Extra extra) {
            if (extra == Extra.getDefaultInstance()) {
                return this;
            }
            if (extra.getPriceFloor() != 0.0f) {
                setPriceFloor(extra.getPriceFloor());
            }
            if (!extra.getAdUnitStat().isEmpty()) {
                this.adUnitStat_ = extra.adUnitStat_;
                onChanged();
            }
            if (!extra.apps_.isEmpty()) {
                if (this.apps_.isEmpty()) {
                    this.apps_ = extra.apps_;
                    this.bitField0_ &= -2;
                } else {
                    ensureAppsIsMutable();
                    this.apps_.addAll(extra.apps_);
                }
                onChanged();
            }
            if (!extra.sa_.isEmpty()) {
                if (this.sa_.isEmpty()) {
                    this.sa_ = extra.sa_;
                    this.bitField0_ &= -3;
                } else {
                    ensureSaIsMutable();
                    this.sa_.addAll(extra.sa_);
                }
                onChanged();
            }
            mergeUnknownFields(extra.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Extra extra;
            Extra extra2 = null;
            try {
                Extra extra3 = (Extra) Extra.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (extra3 != null) {
                    mergeFrom(extra3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                extra = (Extra) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                extra2 = extra;
                if (extra2 != null) {
                }
                throw th;
            }
        }

        public float getPriceFloor() {
            return this.priceFloor_;
        }

        public Builder setPriceFloor(float f) {
            this.priceFloor_ = f;
            onChanged();
            return this;
        }

        public Builder clearPriceFloor() {
            this.priceFloor_ = 0.0f;
            onChanged();
            return this;
        }

        public String getAdUnitStat() {
            Object obj = this.adUnitStat_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.adUnitStat_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getAdUnitStatBytes() {
            Object obj = this.adUnitStat_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.adUnitStat_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setAdUnitStat(String str) {
            if (str != null) {
                this.adUnitStat_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearAdUnitStat() {
            this.adUnitStat_ = Extra.getDefaultInstance().getAdUnitStat();
            onChanged();
            return this;
        }

        public Builder setAdUnitStatBytes(ByteString byteString) {
            if (byteString != null) {
                Extra.checkByteStringIsUtf8(byteString);
                this.adUnitStat_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        private void ensureAppsIsMutable() {
            if ((this.bitField0_ & 1) == 0) {
                this.apps_ = new LazyStringArrayList(this.apps_);
                this.bitField0_ |= 1;
            }
        }

        public ProtocolStringList getAppsList() {
            return this.apps_.getUnmodifiableView();
        }

        public int getAppsCount() {
            return this.apps_.size();
        }

        public String getApps(int i) {
            return (String) this.apps_.get(i);
        }

        public ByteString getAppsBytes(int i) {
            return this.apps_.getByteString(i);
        }

        public Builder setApps(int i, String str) {
            if (str != null) {
                ensureAppsIsMutable();
                this.apps_.set(i, str);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder addApps(String str) {
            if (str != null) {
                ensureAppsIsMutable();
                this.apps_.add(str);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder addAllApps(Iterable<String> iterable) {
            ensureAppsIsMutable();
            com.explorestack.protobuf.AbstractMessageLite.Builder.addAll(iterable, (List<? super T>) this.apps_);
            onChanged();
            return this;
        }

        public Builder clearApps() {
            this.apps_ = LazyStringArrayList.EMPTY;
            this.bitField0_ &= -2;
            onChanged();
            return this;
        }

        public Builder addAppsBytes(ByteString byteString) {
            if (byteString != null) {
                Extra.checkByteStringIsUtf8(byteString);
                ensureAppsIsMutable();
                this.apps_.add(byteString);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        private void ensureSaIsMutable() {
            if ((this.bitField0_ & 2) == 0) {
                this.sa_ = new LazyStringArrayList(this.sa_);
                this.bitField0_ |= 2;
            }
        }

        public ProtocolStringList getSaList() {
            return this.sa_.getUnmodifiableView();
        }

        public int getSaCount() {
            return this.sa_.size();
        }

        public String getSa(int i) {
            return (String) this.sa_.get(i);
        }

        public ByteString getSaBytes(int i) {
            return this.sa_.getByteString(i);
        }

        public Builder setSa(int i, String str) {
            if (str != null) {
                ensureSaIsMutable();
                this.sa_.set(i, str);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder addSa(String str) {
            if (str != null) {
                ensureSaIsMutable();
                this.sa_.add(str);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder addAllSa(Iterable<String> iterable) {
            ensureSaIsMutable();
            com.explorestack.protobuf.AbstractMessageLite.Builder.addAll(iterable, (List<? super T>) this.sa_);
            onChanged();
            return this;
        }

        public Builder clearSa() {
            this.sa_ = LazyStringArrayList.EMPTY;
            this.bitField0_ &= -3;
            onChanged();
            return this;
        }

        public Builder addSaBytes(ByteString byteString) {
            if (byteString != null) {
                Extra.checkByteStringIsUtf8(byteString);
                ensureSaIsMutable();
                this.sa_.add(byteString);
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private Extra(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Extra() {
        this.memoizedIsInitialized = -1;
        this.adUnitStat_ = "";
        this.apps_ = LazyStringArrayList.EMPTY;
        this.sa_ = LazyStringArrayList.EMPTY;
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Extra();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private Extra(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 13) {
                            this.priceFloor_ = codedInputStream.readFloat();
                        } else if (readTag == 18) {
                            this.adUnitStat_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 26) {
                            String readStringRequireUtf8 = codedInputStream.readStringRequireUtf8();
                            if (!z2 || !true) {
                                this.apps_ = new LazyStringArrayList();
                                z2 |= true;
                            }
                            this.apps_.add(readStringRequireUtf8);
                        } else if (readTag == 34) {
                            String readStringRequireUtf82 = codedInputStream.readStringRequireUtf8();
                            if (!z2 || !true) {
                                this.sa_ = new LazyStringArrayList();
                                z2 |= true;
                            }
                            this.sa_.add(readStringRequireUtf82);
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    if (z2 && true) {
                        this.apps_ = this.apps_.getUnmodifiableView();
                    }
                    if (z2 && true) {
                        this.sa_ = this.sa_.getUnmodifiableView();
                    }
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            if (z2 && true) {
                this.apps_ = this.apps_.getUnmodifiableView();
            }
            if (z2 && true) {
                this.sa_ = this.sa_.getUnmodifiableView();
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Extra_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Extra_fieldAccessorTable.ensureFieldAccessorsInitialized(Extra.class, Builder.class);
    }

    public float getPriceFloor() {
        return this.priceFloor_;
    }

    public String getAdUnitStat() {
        Object obj = this.adUnitStat_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.adUnitStat_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getAdUnitStatBytes() {
        Object obj = this.adUnitStat_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.adUnitStat_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public ProtocolStringList getAppsList() {
        return this.apps_;
    }

    public int getAppsCount() {
        return this.apps_.size();
    }

    public String getApps(int i) {
        return (String) this.apps_.get(i);
    }

    public ByteString getAppsBytes(int i) {
        return this.apps_.getByteString(i);
    }

    public ProtocolStringList getSaList() {
        return this.sa_;
    }

    public int getSaCount() {
        return this.sa_.size();
    }

    public String getSa(int i) {
        return (String) this.sa_.get(i);
    }

    public ByteString getSaBytes(int i) {
        return this.sa_.getByteString(i);
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.priceFloor_ != 0.0f) {
            codedOutputStream.writeFloat(1, this.priceFloor_);
        }
        if (!getAdUnitStatBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 2, this.adUnitStat_);
        }
        for (int i = 0; i < this.apps_.size(); i++) {
            GeneratedMessageV3.writeString(codedOutputStream, 3, this.apps_.getRaw(i));
        }
        for (int i2 = 0; i2 < this.sa_.size(); i2++) {
            GeneratedMessageV3.writeString(codedOutputStream, 4, this.sa_.getRaw(i2));
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int computeFloatSize = this.priceFloor_ != 0.0f ? CodedOutputStream.computeFloatSize(1, this.priceFloor_) + 0 : 0;
        if (!getAdUnitStatBytes().isEmpty()) {
            computeFloatSize += GeneratedMessageV3.computeStringSize(2, this.adUnitStat_);
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.apps_.size(); i3++) {
            i2 += computeStringSizeNoTag(this.apps_.getRaw(i3));
        }
        int size = computeFloatSize + i2 + (getAppsList().size() * 1);
        int i4 = 0;
        for (int i5 = 0; i5 < this.sa_.size(); i5++) {
            i4 += computeStringSizeNoTag(this.sa_.getRaw(i5));
        }
        int size2 = size + i4 + (getSaList().size() * 1) + this.unknownFields.getSerializedSize();
        this.memoizedSize = size2;
        return size2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Extra)) {
            return super.equals(obj);
        }
        Extra extra = (Extra) obj;
        if (Float.floatToIntBits(getPriceFloor()) == Float.floatToIntBits(extra.getPriceFloor()) && getAdUnitStat().equals(extra.getAdUnitStat()) && getAppsList().equals(extra.getAppsList()) && getSaList().equals(extra.getSaList()) && this.unknownFields.equals(extra.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + Float.floatToIntBits(getPriceFloor())) * 37) + 2) * 53) + getAdUnitStat().hashCode();
        if (getAppsCount() > 0) {
            hashCode = (((hashCode * 37) + 3) * 53) + getAppsList().hashCode();
        }
        if (getSaCount() > 0) {
            hashCode = (((hashCode * 37) + 4) * 53) + getSaList().hashCode();
        }
        int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode2;
        return hashCode2;
    }

    public static Extra parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Extra) PARSER.parseFrom(byteBuffer);
    }

    public static Extra parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Extra) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Extra parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Extra) PARSER.parseFrom(byteString);
    }

    public static Extra parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Extra) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Extra parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Extra) PARSER.parseFrom(bArr);
    }

    public static Extra parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Extra) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Extra parseFrom(InputStream inputStream) throws IOException {
        return (Extra) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Extra parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Extra) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Extra parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Extra) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Extra parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Extra) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Extra parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Extra) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Extra parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Extra) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Extra extra) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(extra);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Extra getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Extra> parser() {
        return PARSER;
    }

    public Parser<Extra> getParserForType() {
        return PARSER;
    }

    public Extra getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
