package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.EnumDescriptor;
import com.explorestack.protobuf.Descriptors.EnumValueDescriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.Internal.EnumLiteMap;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.ProtocolMessageEnum;
import com.explorestack.protobuf.UnknownFieldSet;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class Device extends GeneratedMessageV3 implements DeviceOrBuilder {
    public static final int ADIDG_FIELD_NUMBER = 18;
    public static final int BATTERY_FIELD_NUMBER = 12;
    public static final int CONNECTIONTYPE_FIELD_NUMBER = 15;
    private static final Device DEFAULT_INSTANCE = new Device();
    public static final int DEVICETYPE_FIELD_NUMBER = 7;
    public static final int H_FIELD_NUMBER = 5;
    public static final int IFA_FIELD_NUMBER = 16;
    public static final int LMT_FIELD_NUMBER = 17;
    public static final int LOCALE_FIELD_NUMBER = 14;
    public static final int MAKE_FIELD_NUMBER = 8;
    public static final int MCCMNC_FIELD_NUMBER = 13;
    public static final int MODEL_FIELD_NUMBER = 9;
    public static final int OSV_FIELD_NUMBER = 2;
    public static final int OS_FIELD_NUMBER = 3;
    /* access modifiers changed from: private */
    public static final Parser<Device> PARSER = new AbstractParser<Device>() {
        public Device parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Device(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int PXRATIO_FIELD_NUMBER = 6;
    public static final int ROOTED_FIELD_NUMBER = 10;
    public static final int UA_FIELD_NUMBER = 1;
    public static final int WEBVIEW_VERSION_FIELD_NUMBER = 11;
    public static final int W_FIELD_NUMBER = 4;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public boolean adidg_;
    /* access modifiers changed from: private */
    public int battery_;
    /* access modifiers changed from: private */
    public int connectiontype_;
    /* access modifiers changed from: private */
    public int devicetype_;
    /* access modifiers changed from: private */
    public int h_;
    /* access modifiers changed from: private */
    public volatile Object ifa_;
    /* access modifiers changed from: private */
    public int lmt_;
    /* access modifiers changed from: private */
    public volatile Object locale_;
    /* access modifiers changed from: private */
    public volatile Object make_;
    /* access modifiers changed from: private */
    public volatile Object mccmnc_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public volatile Object model_;
    /* access modifiers changed from: private */
    public volatile Object os_;
    /* access modifiers changed from: private */
    public volatile Object osv_;
    /* access modifiers changed from: private */
    public float pxratio_;
    /* access modifiers changed from: private */
    public boolean rooted_;
    /* access modifiers changed from: private */
    public volatile Object ua_;
    /* access modifiers changed from: private */
    public int w_;
    /* access modifiers changed from: private */
    public volatile Object webviewVersion_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements DeviceOrBuilder {
        private boolean adidg_;
        private int battery_;
        private int connectiontype_;
        private int devicetype_;
        private int h_;
        private Object ifa_;
        private int lmt_;
        private Object locale_;
        private Object make_;
        private Object mccmnc_;
        private Object model_;
        private Object os_;
        private Object osv_;
        private float pxratio_;
        private boolean rooted_;
        private Object ua_;
        private int w_;
        private Object webviewVersion_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Device_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Device_fieldAccessorTable.ensureFieldAccessorsInitialized(Device.class, Builder.class);
        }

        private Builder() {
            this.ua_ = "";
            this.osv_ = "";
            this.os_ = "";
            this.devicetype_ = 0;
            this.make_ = "";
            this.model_ = "";
            this.webviewVersion_ = "";
            this.mccmnc_ = "";
            this.locale_ = "";
            this.connectiontype_ = 0;
            this.ifa_ = "";
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.ua_ = "";
            this.osv_ = "";
            this.os_ = "";
            this.devicetype_ = 0;
            this.make_ = "";
            this.model_ = "";
            this.webviewVersion_ = "";
            this.mccmnc_ = "";
            this.locale_ = "";
            this.connectiontype_ = 0;
            this.ifa_ = "";
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            Device.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.ua_ = "";
            this.osv_ = "";
            this.os_ = "";
            this.w_ = 0;
            this.h_ = 0;
            this.pxratio_ = 0.0f;
            this.devicetype_ = 0;
            this.make_ = "";
            this.model_ = "";
            this.rooted_ = false;
            this.webviewVersion_ = "";
            this.battery_ = 0;
            this.mccmnc_ = "";
            this.locale_ = "";
            this.connectiontype_ = 0;
            this.ifa_ = "";
            this.lmt_ = 0;
            this.adidg_ = false;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Device_descriptor;
        }

        public Device getDefaultInstanceForType() {
            return Device.getDefaultInstance();
        }

        public Device build() {
            Device buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Device buildPartial() {
            Device device = new Device((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            device.ua_ = this.ua_;
            device.osv_ = this.osv_;
            device.os_ = this.os_;
            device.w_ = this.w_;
            device.h_ = this.h_;
            device.pxratio_ = this.pxratio_;
            device.devicetype_ = this.devicetype_;
            device.make_ = this.make_;
            device.model_ = this.model_;
            device.rooted_ = this.rooted_;
            device.webviewVersion_ = this.webviewVersion_;
            device.battery_ = this.battery_;
            device.mccmnc_ = this.mccmnc_;
            device.locale_ = this.locale_;
            device.connectiontype_ = this.connectiontype_;
            device.ifa_ = this.ifa_;
            device.lmt_ = this.lmt_;
            device.adidg_ = this.adidg_;
            onBuilt();
            return device;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Device) {
                return mergeFrom((Device) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Device device) {
            if (device == Device.getDefaultInstance()) {
                return this;
            }
            if (!device.getUa().isEmpty()) {
                this.ua_ = device.ua_;
                onChanged();
            }
            if (!device.getOsv().isEmpty()) {
                this.osv_ = device.osv_;
                onChanged();
            }
            if (!device.getOs().isEmpty()) {
                this.os_ = device.os_;
                onChanged();
            }
            if (device.getW() != 0) {
                setW(device.getW());
            }
            if (device.getH() != 0) {
                setH(device.getH());
            }
            if (device.getPxratio() != 0.0f) {
                setPxratio(device.getPxratio());
            }
            if (device.devicetype_ != 0) {
                setDevicetypeValue(device.getDevicetypeValue());
            }
            if (!device.getMake().isEmpty()) {
                this.make_ = device.make_;
                onChanged();
            }
            if (!device.getModel().isEmpty()) {
                this.model_ = device.model_;
                onChanged();
            }
            if (device.getRooted()) {
                setRooted(device.getRooted());
            }
            if (!device.getWebviewVersion().isEmpty()) {
                this.webviewVersion_ = device.webviewVersion_;
                onChanged();
            }
            if (device.getBattery() != 0) {
                setBattery(device.getBattery());
            }
            if (!device.getMccmnc().isEmpty()) {
                this.mccmnc_ = device.mccmnc_;
                onChanged();
            }
            if (!device.getLocale().isEmpty()) {
                this.locale_ = device.locale_;
                onChanged();
            }
            if (device.connectiontype_ != 0) {
                setConnectiontypeValue(device.getConnectiontypeValue());
            }
            if (!device.getIfa().isEmpty()) {
                this.ifa_ = device.ifa_;
                onChanged();
            }
            if (device.getLmt() != 0) {
                setLmt(device.getLmt());
            }
            if (device.getAdidg()) {
                setAdidg(device.getAdidg());
            }
            mergeUnknownFields(device.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Device device;
            Device device2 = null;
            try {
                Device device3 = (Device) Device.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (device3 != null) {
                    mergeFrom(device3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                device = (Device) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                device2 = device;
                if (device2 != null) {
                }
                throw th;
            }
        }

        public String getUa() {
            Object obj = this.ua_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ua_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getUaBytes() {
            Object obj = this.ua_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.ua_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setUa(String str) {
            if (str != null) {
                this.ua_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearUa() {
            this.ua_ = Device.getDefaultInstance().getUa();
            onChanged();
            return this;
        }

        public Builder setUaBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.ua_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getOsv() {
            Object obj = this.osv_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.osv_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getOsvBytes() {
            Object obj = this.osv_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.osv_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setOsv(String str) {
            if (str != null) {
                this.osv_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearOsv() {
            this.osv_ = Device.getDefaultInstance().getOsv();
            onChanged();
            return this;
        }

        public Builder setOsvBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.osv_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getOs() {
            Object obj = this.os_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.os_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getOsBytes() {
            Object obj = this.os_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.os_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setOs(String str) {
            if (str != null) {
                this.os_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearOs() {
            this.os_ = Device.getDefaultInstance().getOs();
            onChanged();
            return this;
        }

        public Builder setOsBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.os_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public int getW() {
            return this.w_;
        }

        public Builder setW(int i) {
            this.w_ = i;
            onChanged();
            return this;
        }

        public Builder clearW() {
            this.w_ = 0;
            onChanged();
            return this;
        }

        public int getH() {
            return this.h_;
        }

        public Builder setH(int i) {
            this.h_ = i;
            onChanged();
            return this;
        }

        public Builder clearH() {
            this.h_ = 0;
            onChanged();
            return this;
        }

        public float getPxratio() {
            return this.pxratio_;
        }

        public Builder setPxratio(float f) {
            this.pxratio_ = f;
            onChanged();
            return this;
        }

        public Builder clearPxratio() {
            this.pxratio_ = 0.0f;
            onChanged();
            return this;
        }

        public int getDevicetypeValue() {
            return this.devicetype_;
        }

        public Builder setDevicetypeValue(int i) {
            this.devicetype_ = i;
            onChanged();
            return this;
        }

        public DeviceType getDevicetype() {
            DeviceType valueOf = DeviceType.valueOf(this.devicetype_);
            return valueOf == null ? DeviceType.UNRECOGNIZED : valueOf;
        }

        public Builder setDevicetype(DeviceType deviceType) {
            if (deviceType != null) {
                this.devicetype_ = deviceType.getNumber();
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearDevicetype() {
            this.devicetype_ = 0;
            onChanged();
            return this;
        }

        public String getMake() {
            Object obj = this.make_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.make_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getMakeBytes() {
            Object obj = this.make_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.make_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setMake(String str) {
            if (str != null) {
                this.make_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearMake() {
            this.make_ = Device.getDefaultInstance().getMake();
            onChanged();
            return this;
        }

        public Builder setMakeBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.make_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getModel() {
            Object obj = this.model_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.model_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getModelBytes() {
            Object obj = this.model_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.model_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setModel(String str) {
            if (str != null) {
                this.model_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearModel() {
            this.model_ = Device.getDefaultInstance().getModel();
            onChanged();
            return this;
        }

        public Builder setModelBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.model_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        @Deprecated
        public boolean getRooted() {
            return this.rooted_;
        }

        @Deprecated
        public Builder setRooted(boolean z) {
            this.rooted_ = z;
            onChanged();
            return this;
        }

        @Deprecated
        public Builder clearRooted() {
            this.rooted_ = false;
            onChanged();
            return this;
        }

        @Deprecated
        public String getWebviewVersion() {
            Object obj = this.webviewVersion_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.webviewVersion_ = stringUtf8;
            return stringUtf8;
        }

        @Deprecated
        public ByteString getWebviewVersionBytes() {
            Object obj = this.webviewVersion_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.webviewVersion_ = copyFromUtf8;
            return copyFromUtf8;
        }

        @Deprecated
        public Builder setWebviewVersion(String str) {
            if (str != null) {
                this.webviewVersion_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        @Deprecated
        public Builder clearWebviewVersion() {
            this.webviewVersion_ = Device.getDefaultInstance().getWebviewVersion();
            onChanged();
            return this;
        }

        @Deprecated
        public Builder setWebviewVersionBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.webviewVersion_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public int getBattery() {
            return this.battery_;
        }

        public Builder setBattery(int i) {
            this.battery_ = i;
            onChanged();
            return this;
        }

        public Builder clearBattery() {
            this.battery_ = 0;
            onChanged();
            return this;
        }

        public String getMccmnc() {
            Object obj = this.mccmnc_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.mccmnc_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getMccmncBytes() {
            Object obj = this.mccmnc_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.mccmnc_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setMccmnc(String str) {
            if (str != null) {
                this.mccmnc_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearMccmnc() {
            this.mccmnc_ = Device.getDefaultInstance().getMccmnc();
            onChanged();
            return this;
        }

        public Builder setMccmncBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.mccmnc_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getLocale() {
            Object obj = this.locale_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.locale_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getLocaleBytes() {
            Object obj = this.locale_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.locale_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setLocale(String str) {
            if (str != null) {
                this.locale_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearLocale() {
            this.locale_ = Device.getDefaultInstance().getLocale();
            onChanged();
            return this;
        }

        public Builder setLocaleBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.locale_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public int getConnectiontypeValue() {
            return this.connectiontype_;
        }

        public Builder setConnectiontypeValue(int i) {
            this.connectiontype_ = i;
            onChanged();
            return this;
        }

        public ConnectionType getConnectiontype() {
            ConnectionType valueOf = ConnectionType.valueOf(this.connectiontype_);
            return valueOf == null ? ConnectionType.UNRECOGNIZED : valueOf;
        }

        public Builder setConnectiontype(ConnectionType connectionType) {
            if (connectionType != null) {
                this.connectiontype_ = connectionType.getNumber();
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearConnectiontype() {
            this.connectiontype_ = 0;
            onChanged();
            return this;
        }

        public String getIfa() {
            Object obj = this.ifa_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ifa_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getIfaBytes() {
            Object obj = this.ifa_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.ifa_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setIfa(String str) {
            if (str != null) {
                this.ifa_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearIfa() {
            this.ifa_ = Device.getDefaultInstance().getIfa();
            onChanged();
            return this;
        }

        public Builder setIfaBytes(ByteString byteString) {
            if (byteString != null) {
                Device.checkByteStringIsUtf8(byteString);
                this.ifa_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public int getLmt() {
            return this.lmt_;
        }

        public Builder setLmt(int i) {
            this.lmt_ = i;
            onChanged();
            return this;
        }

        public Builder clearLmt() {
            this.lmt_ = 0;
            onChanged();
            return this;
        }

        public boolean getAdidg() {
            return this.adidg_;
        }

        public Builder setAdidg(boolean z) {
            this.adidg_ = z;
            onChanged();
            return this;
        }

        public Builder clearAdidg() {
            this.adidg_ = false;
            onChanged();
            return this;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    public enum ConnectionType implements ProtocolMessageEnum {
        CONNECTIONTYPE_UNKNOWN(0),
        ETHERNET(1),
        WIFI(2),
        MOBILE_UNKNOWN(3),
        MOBILE_2G(4),
        MOBILE_3G(5),
        MOBILE_4G(6),
        UNRECOGNIZED(-1);
        
        public static final int CONNECTIONTYPE_UNKNOWN_VALUE = 0;
        public static final int ETHERNET_VALUE = 1;
        public static final int MOBILE_2G_VALUE = 4;
        public static final int MOBILE_3G_VALUE = 5;
        public static final int MOBILE_4G_VALUE = 6;
        public static final int MOBILE_UNKNOWN_VALUE = 3;
        private static final ConnectionType[] VALUES = null;
        public static final int WIFI_VALUE = 2;
        private static final EnumLiteMap<ConnectionType> internalValueMap = null;
        private final int value;

        static {
            internalValueMap = new EnumLiteMap<ConnectionType>() {
                public ConnectionType findValueByNumber(int i) {
                    return ConnectionType.forNumber(i);
                }
            };
            VALUES = values();
        }

        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static ConnectionType valueOf(int i) {
            return forNumber(i);
        }

        public static ConnectionType forNumber(int i) {
            switch (i) {
                case 0:
                    return CONNECTIONTYPE_UNKNOWN;
                case 1:
                    return ETHERNET;
                case 2:
                    return WIFI;
                case 3:
                    return MOBILE_UNKNOWN;
                case 4:
                    return MOBILE_2G;
                case 5:
                    return MOBILE_3G;
                case 6:
                    return MOBILE_4G;
                default:
                    return null;
            }
        }

        public static EnumLiteMap<ConnectionType> internalGetValueMap() {
            return internalValueMap;
        }

        public final EnumValueDescriptor getValueDescriptor() {
            return (EnumValueDescriptor) getDescriptor().getValues().get(ordinal());
        }

        public final EnumDescriptor getDescriptorForType() {
            return getDescriptor();
        }

        public static final EnumDescriptor getDescriptor() {
            return (EnumDescriptor) Device.getDescriptor().getEnumTypes().get(0);
        }

        public static ConnectionType valueOf(EnumValueDescriptor enumValueDescriptor) {
            if (enumValueDescriptor.getType() != getDescriptor()) {
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            } else if (enumValueDescriptor.getIndex() == -1) {
                return UNRECOGNIZED;
            } else {
                return VALUES[enumValueDescriptor.getIndex()];
            }
        }

        private ConnectionType(int i) {
            this.value = i;
        }
    }

    public enum DeviceType implements ProtocolMessageEnum {
        DEVICETYPE_UNKNOWN(0),
        PHONE(4),
        TABLET(5),
        UNRECOGNIZED(-1);
        
        public static final int DEVICETYPE_UNKNOWN_VALUE = 0;
        public static final int PHONE_VALUE = 4;
        public static final int TABLET_VALUE = 5;
        private static final DeviceType[] VALUES = null;
        private static final EnumLiteMap<DeviceType> internalValueMap = null;
        private final int value;

        static {
            internalValueMap = new EnumLiteMap<DeviceType>() {
                public DeviceType findValueByNumber(int i) {
                    return DeviceType.forNumber(i);
                }
            };
            VALUES = values();
        }

        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static DeviceType valueOf(int i) {
            return forNumber(i);
        }

        public static DeviceType forNumber(int i) {
            if (i == 0) {
                return DEVICETYPE_UNKNOWN;
            }
            switch (i) {
                case 4:
                    return PHONE;
                case 5:
                    return TABLET;
                default:
                    return null;
            }
        }

        public static EnumLiteMap<DeviceType> internalGetValueMap() {
            return internalValueMap;
        }

        public final EnumValueDescriptor getValueDescriptor() {
            return (EnumValueDescriptor) getDescriptor().getValues().get(ordinal());
        }

        public final EnumDescriptor getDescriptorForType() {
            return getDescriptor();
        }

        public static final EnumDescriptor getDescriptor() {
            return (EnumDescriptor) Device.getDescriptor().getEnumTypes().get(1);
        }

        public static DeviceType valueOf(EnumValueDescriptor enumValueDescriptor) {
            if (enumValueDescriptor.getType() != getDescriptor()) {
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            } else if (enumValueDescriptor.getIndex() == -1) {
                return UNRECOGNIZED;
            } else {
                return VALUES[enumValueDescriptor.getIndex()];
            }
        }

        private DeviceType(int i) {
            this.value = i;
        }
    }

    private Device(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Device() {
        this.memoizedIsInitialized = -1;
        this.ua_ = "";
        this.osv_ = "";
        this.os_ = "";
        this.devicetype_ = 0;
        this.make_ = "";
        this.model_ = "";
        this.webviewVersion_ = "";
        this.mccmnc_ = "";
        this.locale_ = "";
        this.connectiontype_ = 0;
        this.ifa_ = "";
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Device();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private Device(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            z = true;
                            break;
                        case 10:
                            this.ua_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 18:
                            this.osv_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 26:
                            this.os_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 32:
                            this.w_ = codedInputStream.readInt32();
                            break;
                        case 40:
                            this.h_ = codedInputStream.readInt32();
                            break;
                        case 53:
                            this.pxratio_ = codedInputStream.readFloat();
                            break;
                        case 56:
                            this.devicetype_ = codedInputStream.readEnum();
                            break;
                        case 66:
                            this.make_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 74:
                            this.model_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 80:
                            this.rooted_ = codedInputStream.readBool();
                            break;
                        case 90:
                            this.webviewVersion_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 96:
                            this.battery_ = codedInputStream.readInt32();
                            break;
                        case 106:
                            this.mccmnc_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 114:
                            this.locale_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 120:
                            this.connectiontype_ = codedInputStream.readEnum();
                            break;
                        case TsExtractor.TS_STREAM_TYPE_HDMV_DTS /*130*/:
                            this.ifa_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 136:
                            this.lmt_ = codedInputStream.readInt32();
                            break;
                        case 144:
                            this.adidg_ = codedInputStream.readBool();
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                                break;
                            }
                            z = true;
                            break;
                    }
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Device_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Device_fieldAccessorTable.ensureFieldAccessorsInitialized(Device.class, Builder.class);
    }

    public String getUa() {
        Object obj = this.ua_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.ua_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getUaBytes() {
        Object obj = this.ua_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.ua_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getOsv() {
        Object obj = this.osv_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.osv_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getOsvBytes() {
        Object obj = this.osv_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.osv_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getOs() {
        Object obj = this.os_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.os_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getOsBytes() {
        Object obj = this.os_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.os_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public int getW() {
        return this.w_;
    }

    public int getH() {
        return this.h_;
    }

    public float getPxratio() {
        return this.pxratio_;
    }

    public int getDevicetypeValue() {
        return this.devicetype_;
    }

    public DeviceType getDevicetype() {
        DeviceType valueOf = DeviceType.valueOf(this.devicetype_);
        return valueOf == null ? DeviceType.UNRECOGNIZED : valueOf;
    }

    public String getMake() {
        Object obj = this.make_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.make_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getMakeBytes() {
        Object obj = this.make_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.make_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getModel() {
        Object obj = this.model_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.model_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getModelBytes() {
        Object obj = this.model_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.model_ = copyFromUtf8;
        return copyFromUtf8;
    }

    @Deprecated
    public boolean getRooted() {
        return this.rooted_;
    }

    @Deprecated
    public String getWebviewVersion() {
        Object obj = this.webviewVersion_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.webviewVersion_ = stringUtf8;
        return stringUtf8;
    }

    @Deprecated
    public ByteString getWebviewVersionBytes() {
        Object obj = this.webviewVersion_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.webviewVersion_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public int getBattery() {
        return this.battery_;
    }

    public String getMccmnc() {
        Object obj = this.mccmnc_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.mccmnc_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getMccmncBytes() {
        Object obj = this.mccmnc_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.mccmnc_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getLocale() {
        Object obj = this.locale_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.locale_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getLocaleBytes() {
        Object obj = this.locale_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.locale_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public int getConnectiontypeValue() {
        return this.connectiontype_;
    }

    public ConnectionType getConnectiontype() {
        ConnectionType valueOf = ConnectionType.valueOf(this.connectiontype_);
        return valueOf == null ? ConnectionType.UNRECOGNIZED : valueOf;
    }

    public String getIfa() {
        Object obj = this.ifa_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.ifa_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getIfaBytes() {
        Object obj = this.ifa_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.ifa_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public int getLmt() {
        return this.lmt_;
    }

    public boolean getAdidg() {
        return this.adidg_;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (!getUaBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 1, this.ua_);
        }
        if (!getOsvBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 2, this.osv_);
        }
        if (!getOsBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 3, this.os_);
        }
        if (this.w_ != 0) {
            codedOutputStream.writeInt32(4, this.w_);
        }
        if (this.h_ != 0) {
            codedOutputStream.writeInt32(5, this.h_);
        }
        if (this.pxratio_ != 0.0f) {
            codedOutputStream.writeFloat(6, this.pxratio_);
        }
        if (this.devicetype_ != DeviceType.DEVICETYPE_UNKNOWN.getNumber()) {
            codedOutputStream.writeEnum(7, this.devicetype_);
        }
        if (!getMakeBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 8, this.make_);
        }
        if (!getModelBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 9, this.model_);
        }
        if (this.rooted_) {
            codedOutputStream.writeBool(10, this.rooted_);
        }
        if (!getWebviewVersionBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 11, this.webviewVersion_);
        }
        if (this.battery_ != 0) {
            codedOutputStream.writeInt32(12, this.battery_);
        }
        if (!getMccmncBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 13, this.mccmnc_);
        }
        if (!getLocaleBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 14, this.locale_);
        }
        if (this.connectiontype_ != ConnectionType.CONNECTIONTYPE_UNKNOWN.getNumber()) {
            codedOutputStream.writeEnum(15, this.connectiontype_);
        }
        if (!getIfaBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 16, this.ifa_);
        }
        if (this.lmt_ != 0) {
            codedOutputStream.writeInt32(17, this.lmt_);
        }
        if (this.adidg_) {
            codedOutputStream.writeBool(18, this.adidg_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!getUaBytes().isEmpty()) {
            i2 = 0 + GeneratedMessageV3.computeStringSize(1, this.ua_);
        }
        if (!getOsvBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(2, this.osv_);
        }
        if (!getOsBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(3, this.os_);
        }
        if (this.w_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(4, this.w_);
        }
        if (this.h_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(5, this.h_);
        }
        if (this.pxratio_ != 0.0f) {
            i2 += CodedOutputStream.computeFloatSize(6, this.pxratio_);
        }
        if (this.devicetype_ != DeviceType.DEVICETYPE_UNKNOWN.getNumber()) {
            i2 += CodedOutputStream.computeEnumSize(7, this.devicetype_);
        }
        if (!getMakeBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(8, this.make_);
        }
        if (!getModelBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(9, this.model_);
        }
        if (this.rooted_) {
            i2 += CodedOutputStream.computeBoolSize(10, this.rooted_);
        }
        if (!getWebviewVersionBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(11, this.webviewVersion_);
        }
        if (this.battery_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(12, this.battery_);
        }
        if (!getMccmncBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(13, this.mccmnc_);
        }
        if (!getLocaleBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(14, this.locale_);
        }
        if (this.connectiontype_ != ConnectionType.CONNECTIONTYPE_UNKNOWN.getNumber()) {
            i2 += CodedOutputStream.computeEnumSize(15, this.connectiontype_);
        }
        if (!getIfaBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(16, this.ifa_);
        }
        if (this.lmt_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(17, this.lmt_);
        }
        if (this.adidg_) {
            i2 += CodedOutputStream.computeBoolSize(18, this.adidg_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Device)) {
            return super.equals(obj);
        }
        Device device = (Device) obj;
        if (getUa().equals(device.getUa()) && getOsv().equals(device.getOsv()) && getOs().equals(device.getOs()) && getW() == device.getW() && getH() == device.getH() && Float.floatToIntBits(getPxratio()) == Float.floatToIntBits(device.getPxratio()) && this.devicetype_ == device.devicetype_ && getMake().equals(device.getMake()) && getModel().equals(device.getModel()) && getRooted() == device.getRooted() && getWebviewVersion().equals(device.getWebviewVersion()) && getBattery() == device.getBattery() && getMccmnc().equals(device.getMccmnc()) && getLocale().equals(device.getLocale()) && this.connectiontype_ == device.connectiontype_ && getIfa().equals(device.getIfa()) && getLmt() == device.getLmt() && getAdidg() == device.getAdidg() && this.unknownFields.equals(device.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getUa().hashCode()) * 37) + 2) * 53) + getOsv().hashCode()) * 37) + 3) * 53) + getOs().hashCode()) * 37) + 4) * 53) + getW()) * 37) + 5) * 53) + getH()) * 37) + 6) * 53) + Float.floatToIntBits(getPxratio())) * 37) + 7) * 53) + this.devicetype_) * 37) + 8) * 53) + getMake().hashCode()) * 37) + 9) * 53) + getModel().hashCode()) * 37) + 10) * 53) + Internal.hashBoolean(getRooted())) * 37) + 11) * 53) + getWebviewVersion().hashCode()) * 37) + 12) * 53) + getBattery()) * 37) + 13) * 53) + getMccmnc().hashCode()) * 37) + 14) * 53) + getLocale().hashCode()) * 37) + 15) * 53) + this.connectiontype_) * 37) + 16) * 53) + getIfa().hashCode()) * 37) + 17) * 53) + getLmt()) * 37) + 18) * 53) + Internal.hashBoolean(getAdidg())) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode;
        return hashCode;
    }

    public static Device parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Device) PARSER.parseFrom(byteBuffer);
    }

    public static Device parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Device) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Device parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Device) PARSER.parseFrom(byteString);
    }

    public static Device parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Device) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Device parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Device) PARSER.parseFrom(bArr);
    }

    public static Device parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Device) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Device parseFrom(InputStream inputStream) throws IOException {
        return (Device) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Device parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Device) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Device parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Device) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Device parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Device) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Device parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Device) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Device parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Device) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Device device) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(device);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Device getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Device> parser() {
        return PARSER;
    }

    public Parser<Device> getParserForType() {
        return PARSER;
    }

    public Device getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
