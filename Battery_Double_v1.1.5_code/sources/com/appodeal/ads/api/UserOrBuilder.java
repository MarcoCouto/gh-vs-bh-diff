package com.appodeal.ads.api;

import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.MessageOrBuilder;

public interface UserOrBuilder extends MessageOrBuilder {
    boolean getConsent();

    String getId();

    ByteString getIdBytes();

    UserSettings getUserSettings();

    UserSettingsOrBuilder getUserSettingsOrBuilder();

    boolean hasUserSettings();
}
