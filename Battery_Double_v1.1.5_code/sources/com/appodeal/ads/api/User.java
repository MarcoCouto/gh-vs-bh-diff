package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.SingleFieldBuilderV3;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class User extends GeneratedMessageV3 implements UserOrBuilder {
    public static final int CONSENT_FIELD_NUMBER = 1;
    private static final User DEFAULT_INSTANCE = new User();
    public static final int ID_FIELD_NUMBER = 3;
    /* access modifiers changed from: private */
    public static final Parser<User> PARSER = new AbstractParser<User>() {
        public User parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new User(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int USERSETTINGS_FIELD_NUMBER = 2;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public boolean consent_;
    /* access modifiers changed from: private */
    public volatile Object id_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public UserSettings userSettings_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements UserOrBuilder {
        private boolean consent_;
        private Object id_;
        private SingleFieldBuilderV3<UserSettings, com.appodeal.ads.api.UserSettings.Builder, UserSettingsOrBuilder> userSettingsBuilder_;
        private UserSettings userSettings_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_User_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_User_fieldAccessorTable.ensureFieldAccessorsInitialized(User.class, Builder.class);
        }

        private Builder() {
            this.id_ = "";
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.id_ = "";
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            User.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.consent_ = false;
            if (this.userSettingsBuilder_ == null) {
                this.userSettings_ = null;
            } else {
                this.userSettings_ = null;
                this.userSettingsBuilder_ = null;
            }
            this.id_ = "";
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_User_descriptor;
        }

        public User getDefaultInstanceForType() {
            return User.getDefaultInstance();
        }

        public User build() {
            User buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public User buildPartial() {
            User user = new User((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            user.consent_ = this.consent_;
            if (this.userSettingsBuilder_ == null) {
                user.userSettings_ = this.userSettings_;
            } else {
                user.userSettings_ = (UserSettings) this.userSettingsBuilder_.build();
            }
            user.id_ = this.id_;
            onBuilt();
            return user;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof User) {
                return mergeFrom((User) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(User user) {
            if (user == User.getDefaultInstance()) {
                return this;
            }
            if (user.getConsent()) {
                setConsent(user.getConsent());
            }
            if (user.hasUserSettings()) {
                mergeUserSettings(user.getUserSettings());
            }
            if (!user.getId().isEmpty()) {
                this.id_ = user.id_;
                onChanged();
            }
            mergeUnknownFields(user.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            User user;
            User user2 = null;
            try {
                User user3 = (User) User.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (user3 != null) {
                    mergeFrom(user3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                user = (User) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                user2 = user;
                if (user2 != null) {
                }
                throw th;
            }
        }

        public boolean getConsent() {
            return this.consent_;
        }

        public Builder setConsent(boolean z) {
            this.consent_ = z;
            onChanged();
            return this;
        }

        public Builder clearConsent() {
            this.consent_ = false;
            onChanged();
            return this;
        }

        public boolean hasUserSettings() {
            return (this.userSettingsBuilder_ == null && this.userSettings_ == null) ? false : true;
        }

        public UserSettings getUserSettings() {
            if (this.userSettingsBuilder_ != null) {
                return (UserSettings) this.userSettingsBuilder_.getMessage();
            }
            return this.userSettings_ == null ? UserSettings.getDefaultInstance() : this.userSettings_;
        }

        public Builder setUserSettings(UserSettings userSettings) {
            if (this.userSettingsBuilder_ != null) {
                this.userSettingsBuilder_.setMessage(userSettings);
            } else if (userSettings != null) {
                this.userSettings_ = userSettings;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setUserSettings(com.appodeal.ads.api.UserSettings.Builder builder) {
            if (this.userSettingsBuilder_ == null) {
                this.userSettings_ = builder.build();
                onChanged();
            } else {
                this.userSettingsBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeUserSettings(UserSettings userSettings) {
            if (this.userSettingsBuilder_ == null) {
                if (this.userSettings_ != null) {
                    this.userSettings_ = UserSettings.newBuilder(this.userSettings_).mergeFrom(userSettings).buildPartial();
                } else {
                    this.userSettings_ = userSettings;
                }
                onChanged();
            } else {
                this.userSettingsBuilder_.mergeFrom(userSettings);
            }
            return this;
        }

        public Builder clearUserSettings() {
            if (this.userSettingsBuilder_ == null) {
                this.userSettings_ = null;
                onChanged();
            } else {
                this.userSettings_ = null;
                this.userSettingsBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.UserSettings.Builder getUserSettingsBuilder() {
            onChanged();
            return (com.appodeal.ads.api.UserSettings.Builder) getUserSettingsFieldBuilder().getBuilder();
        }

        public UserSettingsOrBuilder getUserSettingsOrBuilder() {
            if (this.userSettingsBuilder_ != null) {
                return (UserSettingsOrBuilder) this.userSettingsBuilder_.getMessageOrBuilder();
            }
            return this.userSettings_ == null ? UserSettings.getDefaultInstance() : this.userSettings_;
        }

        private SingleFieldBuilderV3<UserSettings, com.appodeal.ads.api.UserSettings.Builder, UserSettingsOrBuilder> getUserSettingsFieldBuilder() {
            if (this.userSettingsBuilder_ == null) {
                this.userSettingsBuilder_ = new SingleFieldBuilderV3<>(getUserSettings(), getParentForChildren(), isClean());
                this.userSettings_ = null;
            }
            return this.userSettingsBuilder_;
        }

        public String getId() {
            Object obj = this.id_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.id_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getIdBytes() {
            Object obj = this.id_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.id_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setId(String str) {
            if (str != null) {
                this.id_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearId() {
            this.id_ = User.getDefaultInstance().getId();
            onChanged();
            return this;
        }

        public Builder setIdBytes(ByteString byteString) {
            if (byteString != null) {
                User.checkByteStringIsUtf8(byteString);
                this.id_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private User(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private User() {
        this.memoizedIsInitialized = -1;
        this.id_ = "";
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new User();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private User(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            this.consent_ = codedInputStream.readBool();
                        } else if (readTag == 18) {
                            com.appodeal.ads.api.UserSettings.Builder builder = null;
                            if (this.userSettings_ != null) {
                                builder = this.userSettings_.toBuilder();
                            }
                            this.userSettings_ = (UserSettings) codedInputStream.readMessage(UserSettings.parser(), extensionRegistryLite);
                            if (builder != null) {
                                builder.mergeFrom(this.userSettings_);
                                this.userSettings_ = builder.buildPartial();
                            }
                        } else if (readTag == 26) {
                            this.id_ = codedInputStream.readStringRequireUtf8();
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_User_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_User_fieldAccessorTable.ensureFieldAccessorsInitialized(User.class, Builder.class);
    }

    public boolean getConsent() {
        return this.consent_;
    }

    public boolean hasUserSettings() {
        return this.userSettings_ != null;
    }

    public UserSettings getUserSettings() {
        return this.userSettings_ == null ? UserSettings.getDefaultInstance() : this.userSettings_;
    }

    public UserSettingsOrBuilder getUserSettingsOrBuilder() {
        return getUserSettings();
    }

    public String getId() {
        Object obj = this.id_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.id_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getIdBytes() {
        Object obj = this.id_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.id_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.consent_) {
            codedOutputStream.writeBool(1, this.consent_);
        }
        if (this.userSettings_ != null) {
            codedOutputStream.writeMessage(2, getUserSettings());
        }
        if (!getIdBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 3, this.id_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.consent_) {
            i2 = 0 + CodedOutputStream.computeBoolSize(1, this.consent_);
        }
        if (this.userSettings_ != null) {
            i2 += CodedOutputStream.computeMessageSize(2, getUserSettings());
        }
        if (!getIdBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(3, this.id_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof User)) {
            return super.equals(obj);
        }
        User user = (User) obj;
        if (getConsent() != user.getConsent() || hasUserSettings() != user.hasUserSettings()) {
            return false;
        }
        if ((!hasUserSettings() || getUserSettings().equals(user.getUserSettings())) && getId().equals(user.getId()) && this.unknownFields.equals(user.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + Internal.hashBoolean(getConsent());
        if (hasUserSettings()) {
            hashCode = (((hashCode * 37) + 2) * 53) + getUserSettings().hashCode();
        }
        int hashCode2 = (((((hashCode * 37) + 3) * 53) + getId().hashCode()) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode2;
        return hashCode2;
    }

    public static User parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (User) PARSER.parseFrom(byteBuffer);
    }

    public static User parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (User) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static User parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (User) PARSER.parseFrom(byteString);
    }

    public static User parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (User) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static User parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (User) PARSER.parseFrom(bArr);
    }

    public static User parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (User) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static User parseFrom(InputStream inputStream) throws IOException {
        return (User) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static User parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (User) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static User parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (User) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static User parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (User) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static User parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (User) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static User parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (User) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(User user) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(user);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static User getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<User> parser() {
        return PARSER;
    }

    public Parser<User> getParserForType() {
        return PARSER;
    }

    public User getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
