package com.appodeal.ads.api;

import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.MessageOrBuilder;

public interface SessionOrBuilder extends MessageOrBuilder {
    AdStats getAdStats();

    AdStatsOrBuilder getAdStatsOrBuilder();

    String getExt();

    ByteString getExtBytes();

    int getSegmentId();

    long getSessionId();

    long getSessionUptime();

    String getSessionUuid();

    ByteString getSessionUuidBytes();

    boolean getTest();

    String getToken();

    ByteString getTokenBytes();

    boolean hasAdStats();
}
