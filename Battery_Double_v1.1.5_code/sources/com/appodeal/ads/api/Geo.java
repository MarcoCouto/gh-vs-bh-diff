package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.EnumDescriptor;
import com.explorestack.protobuf.Descriptors.EnumValueDescriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.Internal.EnumLiteMap;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.ProtocolMessageEnum;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class Geo extends GeneratedMessageV3 implements GeoOrBuilder {
    private static final Geo DEFAULT_INSTANCE = new Geo();
    public static final int LAT_FIELD_NUMBER = 4;
    public static final int LOCAL_TIME_FIELD_NUMBER = 2;
    public static final int LON_FIELD_NUMBER = 5;
    public static final int LT_FIELD_NUMBER = 3;
    /* access modifiers changed from: private */
    public static final Parser<Geo> PARSER = new AbstractParser<Geo>() {
        public Geo parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Geo(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int UTCOFFSET_FIELD_NUMBER = 1;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public float lat_;
    /* access modifiers changed from: private */
    public long localTime_;
    /* access modifiers changed from: private */
    public float lon_;
    /* access modifiers changed from: private */
    public int lt_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public int utcoffset_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements GeoOrBuilder {
        private float lat_;
        private long localTime_;
        private float lon_;
        private int lt_;
        private int utcoffset_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Geo_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Geo_fieldAccessorTable.ensureFieldAccessorsInitialized(Geo.class, Builder.class);
        }

        private Builder() {
            this.lt_ = 0;
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.lt_ = 0;
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            Geo.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.utcoffset_ = 0;
            this.localTime_ = 0;
            this.lt_ = 0;
            this.lat_ = 0.0f;
            this.lon_ = 0.0f;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Geo_descriptor;
        }

        public Geo getDefaultInstanceForType() {
            return Geo.getDefaultInstance();
        }

        public Geo build() {
            Geo buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Geo buildPartial() {
            Geo geo = new Geo((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            geo.utcoffset_ = this.utcoffset_;
            geo.localTime_ = this.localTime_;
            geo.lt_ = this.lt_;
            geo.lat_ = this.lat_;
            geo.lon_ = this.lon_;
            onBuilt();
            return geo;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Geo) {
                return mergeFrom((Geo) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Geo geo) {
            if (geo == Geo.getDefaultInstance()) {
                return this;
            }
            if (geo.getUtcoffset() != 0) {
                setUtcoffset(geo.getUtcoffset());
            }
            if (geo.getLocalTime() != 0) {
                setLocalTime(geo.getLocalTime());
            }
            if (geo.lt_ != 0) {
                setLtValue(geo.getLtValue());
            }
            if (geo.getLat() != 0.0f) {
                setLat(geo.getLat());
            }
            if (geo.getLon() != 0.0f) {
                setLon(geo.getLon());
            }
            mergeUnknownFields(geo.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Geo geo;
            Geo geo2 = null;
            try {
                Geo geo3 = (Geo) Geo.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (geo3 != null) {
                    mergeFrom(geo3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                geo = (Geo) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                geo2 = geo;
                if (geo2 != null) {
                }
                throw th;
            }
        }

        public int getUtcoffset() {
            return this.utcoffset_;
        }

        public Builder setUtcoffset(int i) {
            this.utcoffset_ = i;
            onChanged();
            return this;
        }

        public Builder clearUtcoffset() {
            this.utcoffset_ = 0;
            onChanged();
            return this;
        }

        public long getLocalTime() {
            return this.localTime_;
        }

        public Builder setLocalTime(long j) {
            this.localTime_ = j;
            onChanged();
            return this;
        }

        public Builder clearLocalTime() {
            this.localTime_ = 0;
            onChanged();
            return this;
        }

        public int getLtValue() {
            return this.lt_;
        }

        public Builder setLtValue(int i) {
            this.lt_ = i;
            onChanged();
            return this;
        }

        public LocationType getLt() {
            LocationType valueOf = LocationType.valueOf(this.lt_);
            return valueOf == null ? LocationType.UNRECOGNIZED : valueOf;
        }

        public Builder setLt(LocationType locationType) {
            if (locationType != null) {
                this.lt_ = locationType.getNumber();
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearLt() {
            this.lt_ = 0;
            onChanged();
            return this;
        }

        public float getLat() {
            return this.lat_;
        }

        public Builder setLat(float f) {
            this.lat_ = f;
            onChanged();
            return this;
        }

        public Builder clearLat() {
            this.lat_ = 0.0f;
            onChanged();
            return this;
        }

        public float getLon() {
            return this.lon_;
        }

        public Builder setLon(float f) {
            this.lon_ = f;
            onChanged();
            return this;
        }

        public Builder clearLon() {
            this.lon_ = 0.0f;
            onChanged();
            return this;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    public enum LocationType implements ProtocolMessageEnum {
        LOCATIONTYPE_UNKNOWN(0),
        GPS(1),
        IP(2),
        USERPROVIDED(3),
        UNRECOGNIZED(-1);
        
        public static final int GPS_VALUE = 1;
        public static final int IP_VALUE = 2;
        public static final int LOCATIONTYPE_UNKNOWN_VALUE = 0;
        public static final int USERPROVIDED_VALUE = 3;
        private static final LocationType[] VALUES = null;
        private static final EnumLiteMap<LocationType> internalValueMap = null;
        private final int value;

        static {
            internalValueMap = new EnumLiteMap<LocationType>() {
                public LocationType findValueByNumber(int i) {
                    return LocationType.forNumber(i);
                }
            };
            VALUES = values();
        }

        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static LocationType valueOf(int i) {
            return forNumber(i);
        }

        public static LocationType forNumber(int i) {
            switch (i) {
                case 0:
                    return LOCATIONTYPE_UNKNOWN;
                case 1:
                    return GPS;
                case 2:
                    return IP;
                case 3:
                    return USERPROVIDED;
                default:
                    return null;
            }
        }

        public static EnumLiteMap<LocationType> internalGetValueMap() {
            return internalValueMap;
        }

        public final EnumValueDescriptor getValueDescriptor() {
            return (EnumValueDescriptor) getDescriptor().getValues().get(ordinal());
        }

        public final EnumDescriptor getDescriptorForType() {
            return getDescriptor();
        }

        public static final EnumDescriptor getDescriptor() {
            return (EnumDescriptor) Geo.getDescriptor().getEnumTypes().get(0);
        }

        public static LocationType valueOf(EnumValueDescriptor enumValueDescriptor) {
            if (enumValueDescriptor.getType() != getDescriptor()) {
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            } else if (enumValueDescriptor.getIndex() == -1) {
                return UNRECOGNIZED;
            } else {
                return VALUES[enumValueDescriptor.getIndex()];
            }
        }

        private LocationType(int i) {
            this.value = i;
        }
    }

    private Geo(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Geo() {
        this.memoizedIsInitialized = -1;
        this.lt_ = 0;
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Geo();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private Geo(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            this.utcoffset_ = codedInputStream.readInt32();
                        } else if (readTag == 16) {
                            this.localTime_ = codedInputStream.readInt64();
                        } else if (readTag == 24) {
                            this.lt_ = codedInputStream.readEnum();
                        } else if (readTag == 37) {
                            this.lat_ = codedInputStream.readFloat();
                        } else if (readTag == 45) {
                            this.lon_ = codedInputStream.readFloat();
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Geo_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Geo_fieldAccessorTable.ensureFieldAccessorsInitialized(Geo.class, Builder.class);
    }

    public int getUtcoffset() {
        return this.utcoffset_;
    }

    public long getLocalTime() {
        return this.localTime_;
    }

    public int getLtValue() {
        return this.lt_;
    }

    public LocationType getLt() {
        LocationType valueOf = LocationType.valueOf(this.lt_);
        return valueOf == null ? LocationType.UNRECOGNIZED : valueOf;
    }

    public float getLat() {
        return this.lat_;
    }

    public float getLon() {
        return this.lon_;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.utcoffset_ != 0) {
            codedOutputStream.writeInt32(1, this.utcoffset_);
        }
        if (this.localTime_ != 0) {
            codedOutputStream.writeInt64(2, this.localTime_);
        }
        if (this.lt_ != LocationType.LOCATIONTYPE_UNKNOWN.getNumber()) {
            codedOutputStream.writeEnum(3, this.lt_);
        }
        if (this.lat_ != 0.0f) {
            codedOutputStream.writeFloat(4, this.lat_);
        }
        if (this.lon_ != 0.0f) {
            codedOutputStream.writeFloat(5, this.lon_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.utcoffset_ != 0) {
            i2 = 0 + CodedOutputStream.computeInt32Size(1, this.utcoffset_);
        }
        if (this.localTime_ != 0) {
            i2 += CodedOutputStream.computeInt64Size(2, this.localTime_);
        }
        if (this.lt_ != LocationType.LOCATIONTYPE_UNKNOWN.getNumber()) {
            i2 += CodedOutputStream.computeEnumSize(3, this.lt_);
        }
        if (this.lat_ != 0.0f) {
            i2 += CodedOutputStream.computeFloatSize(4, this.lat_);
        }
        if (this.lon_ != 0.0f) {
            i2 += CodedOutputStream.computeFloatSize(5, this.lon_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Geo)) {
            return super.equals(obj);
        }
        Geo geo = (Geo) obj;
        if (getUtcoffset() == geo.getUtcoffset() && getLocalTime() == geo.getLocalTime() && this.lt_ == geo.lt_ && Float.floatToIntBits(getLat()) == Float.floatToIntBits(geo.getLat()) && Float.floatToIntBits(getLon()) == Float.floatToIntBits(geo.getLon()) && this.unknownFields.equals(geo.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getUtcoffset()) * 37) + 2) * 53) + Internal.hashLong(getLocalTime())) * 37) + 3) * 53) + this.lt_) * 37) + 4) * 53) + Float.floatToIntBits(getLat())) * 37) + 5) * 53) + Float.floatToIntBits(getLon())) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode;
        return hashCode;
    }

    public static Geo parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Geo) PARSER.parseFrom(byteBuffer);
    }

    public static Geo parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Geo) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Geo parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Geo) PARSER.parseFrom(byteString);
    }

    public static Geo parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Geo) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Geo parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Geo) PARSER.parseFrom(bArr);
    }

    public static Geo parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Geo) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Geo parseFrom(InputStream inputStream) throws IOException {
        return (Geo) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Geo parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Geo) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Geo parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Geo) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Geo parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Geo) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Geo parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Geo) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Geo parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Geo) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Geo geo) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(geo);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Geo getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Geo> parser() {
        return PARSER;
    }

    public Parser<Geo> getParserForType() {
        return PARSER;
    }

    public Geo getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
