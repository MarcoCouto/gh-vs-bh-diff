package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class UserSettings extends GeneratedMessageV3 implements UserSettingsOrBuilder {
    public static final int AGE_FIELD_NUMBER = 3;
    private static final UserSettings DEFAULT_INSTANCE = new UserSettings();
    public static final int GENDER_FIELD_NUMBER = 2;
    /* access modifiers changed from: private */
    public static final Parser<UserSettings> PARSER = new AbstractParser<UserSettings>() {
        public UserSettings parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new UserSettings(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int USER_ID_FIELD_NUMBER = 1;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public int age_;
    /* access modifiers changed from: private */
    public volatile Object gender_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public volatile Object userId_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements UserSettingsOrBuilder {
        private int age_;
        private Object gender_;
        private Object userId_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_UserSettings_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_UserSettings_fieldAccessorTable.ensureFieldAccessorsInitialized(UserSettings.class, Builder.class);
        }

        private Builder() {
            this.userId_ = "";
            this.gender_ = "";
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.userId_ = "";
            this.gender_ = "";
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            UserSettings.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.userId_ = "";
            this.gender_ = "";
            this.age_ = 0;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_UserSettings_descriptor;
        }

        public UserSettings getDefaultInstanceForType() {
            return UserSettings.getDefaultInstance();
        }

        public UserSettings build() {
            UserSettings buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public UserSettings buildPartial() {
            UserSettings userSettings = new UserSettings((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            userSettings.userId_ = this.userId_;
            userSettings.gender_ = this.gender_;
            userSettings.age_ = this.age_;
            onBuilt();
            return userSettings;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof UserSettings) {
                return mergeFrom((UserSettings) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(UserSettings userSettings) {
            if (userSettings == UserSettings.getDefaultInstance()) {
                return this;
            }
            if (!userSettings.getUserId().isEmpty()) {
                this.userId_ = userSettings.userId_;
                onChanged();
            }
            if (!userSettings.getGender().isEmpty()) {
                this.gender_ = userSettings.gender_;
                onChanged();
            }
            if (userSettings.getAge() != 0) {
                setAge(userSettings.getAge());
            }
            mergeUnknownFields(userSettings.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            UserSettings userSettings;
            UserSettings userSettings2 = null;
            try {
                UserSettings userSettings3 = (UserSettings) UserSettings.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (userSettings3 != null) {
                    mergeFrom(userSettings3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                userSettings = (UserSettings) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                userSettings2 = userSettings;
                if (userSettings2 != null) {
                }
                throw th;
            }
        }

        @Deprecated
        public String getUserId() {
            Object obj = this.userId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.userId_ = stringUtf8;
            return stringUtf8;
        }

        @Deprecated
        public ByteString getUserIdBytes() {
            Object obj = this.userId_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.userId_ = copyFromUtf8;
            return copyFromUtf8;
        }

        @Deprecated
        public Builder setUserId(String str) {
            if (str != null) {
                this.userId_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        @Deprecated
        public Builder clearUserId() {
            this.userId_ = UserSettings.getDefaultInstance().getUserId();
            onChanged();
            return this;
        }

        @Deprecated
        public Builder setUserIdBytes(ByteString byteString) {
            if (byteString != null) {
                UserSettings.checkByteStringIsUtf8(byteString);
                this.userId_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getGender() {
            Object obj = this.gender_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.gender_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getGenderBytes() {
            Object obj = this.gender_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.gender_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setGender(String str) {
            if (str != null) {
                this.gender_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearGender() {
            this.gender_ = UserSettings.getDefaultInstance().getGender();
            onChanged();
            return this;
        }

        public Builder setGenderBytes(ByteString byteString) {
            if (byteString != null) {
                UserSettings.checkByteStringIsUtf8(byteString);
                this.gender_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public int getAge() {
            return this.age_;
        }

        public Builder setAge(int i) {
            this.age_ = i;
            onChanged();
            return this;
        }

        public Builder clearAge() {
            this.age_ = 0;
            onChanged();
            return this;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private UserSettings(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private UserSettings() {
        this.memoizedIsInitialized = -1;
        this.userId_ = "";
        this.gender_ = "";
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new UserSettings();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private UserSettings(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 10) {
                            this.userId_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 18) {
                            this.gender_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 24) {
                            this.age_ = codedInputStream.readInt32();
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_UserSettings_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_UserSettings_fieldAccessorTable.ensureFieldAccessorsInitialized(UserSettings.class, Builder.class);
    }

    @Deprecated
    public String getUserId() {
        Object obj = this.userId_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.userId_ = stringUtf8;
        return stringUtf8;
    }

    @Deprecated
    public ByteString getUserIdBytes() {
        Object obj = this.userId_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.userId_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getGender() {
        Object obj = this.gender_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.gender_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getGenderBytes() {
        Object obj = this.gender_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.gender_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public int getAge() {
        return this.age_;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (!getUserIdBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 1, this.userId_);
        }
        if (!getGenderBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 2, this.gender_);
        }
        if (this.age_ != 0) {
            codedOutputStream.writeInt32(3, this.age_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!getUserIdBytes().isEmpty()) {
            i2 = 0 + GeneratedMessageV3.computeStringSize(1, this.userId_);
        }
        if (!getGenderBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(2, this.gender_);
        }
        if (this.age_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(3, this.age_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof UserSettings)) {
            return super.equals(obj);
        }
        UserSettings userSettings = (UserSettings) obj;
        if (getUserId().equals(userSettings.getUserId()) && getGender().equals(userSettings.getGender()) && getAge() == userSettings.getAge() && this.unknownFields.equals(userSettings.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getUserId().hashCode()) * 37) + 2) * 53) + getGender().hashCode()) * 37) + 3) * 53) + getAge()) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode;
        return hashCode;
    }

    public static UserSettings parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (UserSettings) PARSER.parseFrom(byteBuffer);
    }

    public static UserSettings parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (UserSettings) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static UserSettings parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (UserSettings) PARSER.parseFrom(byteString);
    }

    public static UserSettings parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (UserSettings) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static UserSettings parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (UserSettings) PARSER.parseFrom(bArr);
    }

    public static UserSettings parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (UserSettings) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static UserSettings parseFrom(InputStream inputStream) throws IOException {
        return (UserSettings) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static UserSettings parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (UserSettings) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static UserSettings parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (UserSettings) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static UserSettings parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (UserSettings) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static UserSettings parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (UserSettings) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static UserSettings parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (UserSettings) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(UserSettings userSettings) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(userSettings);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static UserSettings getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<UserSettings> parser() {
        return PARSER;
    }

    public Parser<UserSettings> getParserForType() {
        return PARSER;
    }

    public UserSettings getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
