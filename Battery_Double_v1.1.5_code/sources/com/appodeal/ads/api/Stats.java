package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.EnumDescriptor;
import com.explorestack.protobuf.Descriptors.EnumValueDescriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.Internal.EnumLiteMap;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.MessageLite;
import com.explorestack.protobuf.MessageOrBuilder;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.ProtocolMessageEnum;
import com.explorestack.protobuf.RepeatedFieldBuilderV3;
import com.explorestack.protobuf.UnknownFieldSet;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Stats extends GeneratedMessageV3 implements StatsOrBuilder {
    public static final int AD_UNIT_FIELD_NUMBER = 6;
    public static final int CAPACITY_FIELD_NUMBER = 1;
    public static final int COMPLETED_FIELD_NUMBER = 5;
    private static final Stats DEFAULT_INSTANCE = new Stats();
    public static final int FINISH_FIELD_NUMBER = 3;
    /* access modifiers changed from: private */
    public static final Parser<Stats> PARSER = new AbstractParser<Stats>() {
        public Stats parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Stats(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int START_FIELD_NUMBER = 2;
    public static final int SUCCESSFUL_FIELD_NUMBER = 4;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public List<AdUnit> adUnit_;
    /* access modifiers changed from: private */
    public int capacity_;
    /* access modifiers changed from: private */
    public boolean completed_;
    /* access modifiers changed from: private */
    public long finish_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public long start_;
    /* access modifiers changed from: private */
    public boolean successful_;

    public static final class AdUnit extends GeneratedMessageV3 implements AdUnitOrBuilder {
        private static final AdUnit DEFAULT_INSTANCE = new AdUnit();
        public static final int ECPM_FIELD_NUMBER = 6;
        public static final int FINISH_FIELD_NUMBER = 3;
        public static final int ID_FIELD_NUMBER = 1;
        /* access modifiers changed from: private */
        public static final Parser<AdUnit> PARSER = new AbstractParser<AdUnit>() {
            public AdUnit parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return new AdUnit(codedInputStream, extensionRegistryLite);
            }
        };
        public static final int PRECACHE_FIELD_NUMBER = 5;
        public static final int RESULT_FIELD_NUMBER = 4;
        public static final int START_FIELD_NUMBER = 2;
        private static final long serialVersionUID = 0;
        /* access modifiers changed from: private */
        public double ecpm_;
        /* access modifiers changed from: private */
        public long finish_;
        /* access modifiers changed from: private */
        public volatile Object id_;
        private byte memoizedIsInitialized;
        /* access modifiers changed from: private */
        public boolean precache_;
        /* access modifiers changed from: private */
        public int result_;
        /* access modifiers changed from: private */
        public long start_;

        public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements AdUnitOrBuilder {
            private double ecpm_;
            private long finish_;
            private Object id_;
            private boolean precache_;
            private int result_;
            private long start_;

            public final boolean isInitialized() {
                return true;
            }

            public static final Descriptor getDescriptor() {
                return Api.internal_static_com_appodeal_ads_Stats_AdUnit_descriptor;
            }

            /* access modifiers changed from: protected */
            public FieldAccessorTable internalGetFieldAccessorTable() {
                return Api.internal_static_com_appodeal_ads_Stats_AdUnit_fieldAccessorTable.ensureFieldAccessorsInitialized(AdUnit.class, Builder.class);
            }

            private Builder() {
                this.id_ = "";
                this.result_ = 0;
                maybeForceBuilderInitialization();
            }

            private Builder(BuilderParent builderParent) {
                super(builderParent);
                this.id_ = "";
                this.result_ = 0;
                maybeForceBuilderInitialization();
            }

            private void maybeForceBuilderInitialization() {
                AdUnit.alwaysUseFieldBuilders;
            }

            public Builder clear() {
                super.clear();
                this.id_ = "";
                this.start_ = 0;
                this.finish_ = 0;
                this.result_ = 0;
                this.precache_ = false;
                this.ecpm_ = Utils.DOUBLE_EPSILON;
                return this;
            }

            public Descriptor getDescriptorForType() {
                return Api.internal_static_com_appodeal_ads_Stats_AdUnit_descriptor;
            }

            public AdUnit getDefaultInstanceForType() {
                return AdUnit.getDefaultInstance();
            }

            public AdUnit build() {
                AdUnit buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw newUninitializedMessageException(buildPartial);
            }

            public AdUnit buildPartial() {
                AdUnit adUnit = new AdUnit((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
                adUnit.id_ = this.id_;
                adUnit.start_ = this.start_;
                adUnit.finish_ = this.finish_;
                adUnit.result_ = this.result_;
                adUnit.precache_ = this.precache_;
                adUnit.ecpm_ = this.ecpm_;
                onBuilt();
                return adUnit;
            }

            public Builder clone() {
                return (Builder) super.clone();
            }

            public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.setField(fieldDescriptor, obj);
            }

            public Builder clearField(FieldDescriptor fieldDescriptor) {
                return (Builder) super.clearField(fieldDescriptor);
            }

            public Builder clearOneof(OneofDescriptor oneofDescriptor) {
                return (Builder) super.clearOneof(oneofDescriptor);
            }

            public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
                return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
            }

            public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
                return (Builder) super.addRepeatedField(fieldDescriptor, obj);
            }

            public Builder mergeFrom(Message message) {
                if (message instanceof AdUnit) {
                    return mergeFrom((AdUnit) message);
                }
                super.mergeFrom(message);
                return this;
            }

            public Builder mergeFrom(AdUnit adUnit) {
                if (adUnit == AdUnit.getDefaultInstance()) {
                    return this;
                }
                if (!adUnit.getId().isEmpty()) {
                    this.id_ = adUnit.id_;
                    onChanged();
                }
                if (adUnit.getStart() != 0) {
                    setStart(adUnit.getStart());
                }
                if (adUnit.getFinish() != 0) {
                    setFinish(adUnit.getFinish());
                }
                if (adUnit.result_ != 0) {
                    setResultValue(adUnit.getResultValue());
                }
                if (adUnit.getPrecache()) {
                    setPrecache(adUnit.getPrecache());
                }
                if (adUnit.getEcpm() != Utils.DOUBLE_EPSILON) {
                    setEcpm(adUnit.getEcpm());
                }
                mergeUnknownFields(adUnit.unknownFields);
                onChanged();
                return this;
            }

            /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
            public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                AdUnit adUnit;
                AdUnit adUnit2 = null;
                try {
                    AdUnit adUnit3 = (AdUnit) AdUnit.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                    if (adUnit3 != null) {
                        mergeFrom(adUnit3);
                    }
                    return this;
                } catch (InvalidProtocolBufferException e) {
                    adUnit = (AdUnit) e.getUnfinishedMessage();
                    throw e.unwrapIOException();
                } catch (Throwable th) {
                    th = th;
                    adUnit2 = adUnit;
                    if (adUnit2 != null) {
                    }
                    throw th;
                }
            }

            public String getId() {
                Object obj = this.id_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                String stringUtf8 = ((ByteString) obj).toStringUtf8();
                this.id_ = stringUtf8;
                return stringUtf8;
            }

            public ByteString getIdBytes() {
                Object obj = this.id_;
                if (!(obj instanceof String)) {
                    return (ByteString) obj;
                }
                ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
                this.id_ = copyFromUtf8;
                return copyFromUtf8;
            }

            public Builder setId(String str) {
                if (str != null) {
                    this.id_ = str;
                    onChanged();
                    return this;
                }
                throw new NullPointerException();
            }

            public Builder clearId() {
                this.id_ = AdUnit.getDefaultInstance().getId();
                onChanged();
                return this;
            }

            public Builder setIdBytes(ByteString byteString) {
                if (byteString != null) {
                    AdUnit.checkByteStringIsUtf8(byteString);
                    this.id_ = byteString;
                    onChanged();
                    return this;
                }
                throw new NullPointerException();
            }

            public long getStart() {
                return this.start_;
            }

            public Builder setStart(long j) {
                this.start_ = j;
                onChanged();
                return this;
            }

            public Builder clearStart() {
                this.start_ = 0;
                onChanged();
                return this;
            }

            public long getFinish() {
                return this.finish_;
            }

            public Builder setFinish(long j) {
                this.finish_ = j;
                onChanged();
                return this;
            }

            public Builder clearFinish() {
                this.finish_ = 0;
                onChanged();
                return this;
            }

            public int getResultValue() {
                return this.result_;
            }

            public Builder setResultValue(int i) {
                this.result_ = i;
                onChanged();
                return this;
            }

            public AdUnitRequestResult getResult() {
                AdUnitRequestResult valueOf = AdUnitRequestResult.valueOf(this.result_);
                return valueOf == null ? AdUnitRequestResult.UNRECOGNIZED : valueOf;
            }

            public Builder setResult(AdUnitRequestResult adUnitRequestResult) {
                if (adUnitRequestResult != null) {
                    this.result_ = adUnitRequestResult.getNumber();
                    onChanged();
                    return this;
                }
                throw new NullPointerException();
            }

            public Builder clearResult() {
                this.result_ = 0;
                onChanged();
                return this;
            }

            public boolean getPrecache() {
                return this.precache_;
            }

            public Builder setPrecache(boolean z) {
                this.precache_ = z;
                onChanged();
                return this;
            }

            public Builder clearPrecache() {
                this.precache_ = false;
                onChanged();
                return this;
            }

            public double getEcpm() {
                return this.ecpm_;
            }

            public Builder setEcpm(double d) {
                this.ecpm_ = d;
                onChanged();
                return this;
            }

            public Builder clearEcpm() {
                this.ecpm_ = Utils.DOUBLE_EPSILON;
                onChanged();
                return this;
            }

            public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
                return (Builder) super.setUnknownFields(unknownFieldSet);
            }

            public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
                return (Builder) super.mergeUnknownFields(unknownFieldSet);
            }
        }

        private AdUnit(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
            super(builder);
            this.memoizedIsInitialized = -1;
        }

        private AdUnit() {
            this.memoizedIsInitialized = -1;
            this.id_ = "";
            this.result_ = 0;
        }

        /* access modifiers changed from: protected */
        public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
            return new AdUnit();
        }

        public final UnknownFieldSet getUnknownFields() {
            return this.unknownFields;
        }

        private AdUnit(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            this();
            if (extensionRegistryLite != null) {
                com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
                boolean z = false;
                while (!z) {
                    try {
                        int readTag = codedInputStream.readTag();
                        if (readTag != 0) {
                            if (readTag == 10) {
                                this.id_ = codedInputStream.readStringRequireUtf8();
                            } else if (readTag == 16) {
                                this.start_ = codedInputStream.readInt64();
                            } else if (readTag == 24) {
                                this.finish_ = codedInputStream.readInt64();
                            } else if (readTag == 32) {
                                this.result_ = codedInputStream.readEnum();
                            } else if (readTag == 40) {
                                this.precache_ = codedInputStream.readBool();
                            } else if (readTag == 49) {
                                this.ecpm_ = codedInputStream.readDouble();
                            } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                    } catch (Throwable th) {
                        this.unknownFields = newBuilder.build();
                        makeExtensionsImmutable();
                        throw th;
                    }
                }
                this.unknownFields = newBuilder.build();
                makeExtensionsImmutable();
                return;
            }
            throw new NullPointerException();
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Stats_AdUnit_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Stats_AdUnit_fieldAccessorTable.ensureFieldAccessorsInitialized(AdUnit.class, Builder.class);
        }

        public String getId() {
            Object obj = this.id_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.id_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getIdBytes() {
            Object obj = this.id_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.id_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public long getStart() {
            return this.start_;
        }

        public long getFinish() {
            return this.finish_;
        }

        public int getResultValue() {
            return this.result_;
        }

        public AdUnitRequestResult getResult() {
            AdUnitRequestResult valueOf = AdUnitRequestResult.valueOf(this.result_);
            return valueOf == null ? AdUnitRequestResult.UNRECOGNIZED : valueOf;
        }

        public boolean getPrecache() {
            return this.precache_;
        }

        public double getEcpm() {
            return this.ecpm_;
        }

        public final boolean isInitialized() {
            byte b = this.memoizedIsInitialized;
            if (b == 1) {
                return true;
            }
            if (b == 0) {
                return false;
            }
            this.memoizedIsInitialized = 1;
            return true;
        }

        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            if (!getIdBytes().isEmpty()) {
                GeneratedMessageV3.writeString(codedOutputStream, 1, this.id_);
            }
            if (this.start_ != 0) {
                codedOutputStream.writeInt64(2, this.start_);
            }
            if (this.finish_ != 0) {
                codedOutputStream.writeInt64(3, this.finish_);
            }
            if (this.result_ != AdUnitRequestResult.SUCCESSFUL.getNumber()) {
                codedOutputStream.writeEnum(4, this.result_);
            }
            if (this.precache_) {
                codedOutputStream.writeBool(5, this.precache_);
            }
            if (this.ecpm_ != Utils.DOUBLE_EPSILON) {
                codedOutputStream.writeDouble(6, this.ecpm_);
            }
            this.unknownFields.writeTo(codedOutputStream);
        }

        public int getSerializedSize() {
            int i = this.memoizedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (!getIdBytes().isEmpty()) {
                i2 = 0 + GeneratedMessageV3.computeStringSize(1, this.id_);
            }
            if (this.start_ != 0) {
                i2 += CodedOutputStream.computeInt64Size(2, this.start_);
            }
            if (this.finish_ != 0) {
                i2 += CodedOutputStream.computeInt64Size(3, this.finish_);
            }
            if (this.result_ != AdUnitRequestResult.SUCCESSFUL.getNumber()) {
                i2 += CodedOutputStream.computeEnumSize(4, this.result_);
            }
            if (this.precache_) {
                i2 += CodedOutputStream.computeBoolSize(5, this.precache_);
            }
            if (this.ecpm_ != Utils.DOUBLE_EPSILON) {
                i2 += CodedOutputStream.computeDoubleSize(6, this.ecpm_);
            }
            int serializedSize = i2 + this.unknownFields.getSerializedSize();
            this.memoizedSize = serializedSize;
            return serializedSize;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof AdUnit)) {
                return super.equals(obj);
            }
            AdUnit adUnit = (AdUnit) obj;
            if (getId().equals(adUnit.getId()) && getStart() == adUnit.getStart() && getFinish() == adUnit.getFinish() && this.result_ == adUnit.result_ && getPrecache() == adUnit.getPrecache() && Double.doubleToLongBits(getEcpm()) == Double.doubleToLongBits(adUnit.getEcpm()) && this.unknownFields.equals(adUnit.unknownFields)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            if (this.memoizedHashCode != 0) {
                return this.memoizedHashCode;
            }
            int hashCode = ((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getId().hashCode()) * 37) + 2) * 53) + Internal.hashLong(getStart())) * 37) + 3) * 53) + Internal.hashLong(getFinish())) * 37) + 4) * 53) + this.result_) * 37) + 5) * 53) + Internal.hashBoolean(getPrecache())) * 37) + 6) * 53) + Internal.hashLong(Double.doubleToLongBits(getEcpm()))) * 29) + this.unknownFields.hashCode();
            this.memoizedHashCode = hashCode;
            return hashCode;
        }

        public static AdUnit parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (AdUnit) PARSER.parseFrom(byteBuffer);
        }

        public static AdUnit parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (AdUnit) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
        }

        public static AdUnit parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (AdUnit) PARSER.parseFrom(byteString);
        }

        public static AdUnit parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (AdUnit) PARSER.parseFrom(byteString, extensionRegistryLite);
        }

        public static AdUnit parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (AdUnit) PARSER.parseFrom(bArr);
        }

        public static AdUnit parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (AdUnit) PARSER.parseFrom(bArr, extensionRegistryLite);
        }

        public static AdUnit parseFrom(InputStream inputStream) throws IOException {
            return (AdUnit) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
        }

        public static AdUnit parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (AdUnit) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
        }

        public static AdUnit parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (AdUnit) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
        }

        public static AdUnit parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (AdUnit) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
        }

        public static AdUnit parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (AdUnit) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
        }

        public static AdUnit parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (AdUnit) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
        }

        public Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Builder newBuilder(AdUnit adUnit) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(adUnit);
        }

        public Builder toBuilder() {
            if (this == DEFAULT_INSTANCE) {
                return new Builder();
            }
            return new Builder().mergeFrom(this);
        }

        /* access modifiers changed from: protected */
        public Builder newBuilderForType(BuilderParent builderParent) {
            return new Builder(builderParent);
        }

        public static AdUnit getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<AdUnit> parser() {
            return PARSER;
        }

        public Parser<AdUnit> getParserForType() {
            return PARSER;
        }

        public AdUnit getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }
    }

    public interface AdUnitOrBuilder extends MessageOrBuilder {
        double getEcpm();

        long getFinish();

        String getId();

        ByteString getIdBytes();

        boolean getPrecache();

        AdUnitRequestResult getResult();

        int getResultValue();

        long getStart();
    }

    public enum AdUnitRequestResult implements ProtocolMessageEnum {
        SUCCESSFUL(0),
        NOFILL(1),
        TIMEOUTREACHED(2),
        EXCEPTION(3),
        UNDEFINEDADAPTER(4),
        INCORRECTADUNIT(5),
        INVALIDASSETS(6),
        CANCELED(7),
        UNRECOGNIZED(-1);
        
        public static final int CANCELED_VALUE = 7;
        public static final int EXCEPTION_VALUE = 3;
        public static final int INCORRECTADUNIT_VALUE = 5;
        public static final int INVALIDASSETS_VALUE = 6;
        public static final int NOFILL_VALUE = 1;
        public static final int SUCCESSFUL_VALUE = 0;
        public static final int TIMEOUTREACHED_VALUE = 2;
        public static final int UNDEFINEDADAPTER_VALUE = 4;
        private static final AdUnitRequestResult[] VALUES = null;
        private static final EnumLiteMap<AdUnitRequestResult> internalValueMap = null;
        private final int value;

        static {
            internalValueMap = new EnumLiteMap<AdUnitRequestResult>() {
                public AdUnitRequestResult findValueByNumber(int i) {
                    return AdUnitRequestResult.forNumber(i);
                }
            };
            VALUES = values();
        }

        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static AdUnitRequestResult valueOf(int i) {
            return forNumber(i);
        }

        public static AdUnitRequestResult forNumber(int i) {
            switch (i) {
                case 0:
                    return SUCCESSFUL;
                case 1:
                    return NOFILL;
                case 2:
                    return TIMEOUTREACHED;
                case 3:
                    return EXCEPTION;
                case 4:
                    return UNDEFINEDADAPTER;
                case 5:
                    return INCORRECTADUNIT;
                case 6:
                    return INVALIDASSETS;
                case 7:
                    return CANCELED;
                default:
                    return null;
            }
        }

        public static EnumLiteMap<AdUnitRequestResult> internalGetValueMap() {
            return internalValueMap;
        }

        public final EnumValueDescriptor getValueDescriptor() {
            return (EnumValueDescriptor) getDescriptor().getValues().get(ordinal());
        }

        public final EnumDescriptor getDescriptorForType() {
            return getDescriptor();
        }

        public static final EnumDescriptor getDescriptor() {
            return (EnumDescriptor) Stats.getDescriptor().getEnumTypes().get(0);
        }

        public static AdUnitRequestResult valueOf(EnumValueDescriptor enumValueDescriptor) {
            if (enumValueDescriptor.getType() != getDescriptor()) {
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            } else if (enumValueDescriptor.getIndex() == -1) {
                return UNRECOGNIZED;
            } else {
                return VALUES[enumValueDescriptor.getIndex()];
            }
        }

        private AdUnitRequestResult(int i) {
            this.value = i;
        }
    }

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements StatsOrBuilder {
        private RepeatedFieldBuilderV3<AdUnit, Builder, AdUnitOrBuilder> adUnitBuilder_;
        private List<AdUnit> adUnit_;
        private int bitField0_;
        private int capacity_;
        private boolean completed_;
        private long finish_;
        private long start_;
        private boolean successful_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Stats_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Stats_fieldAccessorTable.ensureFieldAccessorsInitialized(Stats.class, Builder.class);
        }

        private Builder() {
            this.adUnit_ = Collections.emptyList();
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.adUnit_ = Collections.emptyList();
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            if (Stats.alwaysUseFieldBuilders) {
                getAdUnitFieldBuilder();
            }
        }

        public Builder clear() {
            super.clear();
            this.capacity_ = 0;
            this.start_ = 0;
            this.finish_ = 0;
            this.successful_ = false;
            this.completed_ = false;
            if (this.adUnitBuilder_ == null) {
                this.adUnit_ = Collections.emptyList();
                this.bitField0_ &= -2;
            } else {
                this.adUnitBuilder_.clear();
            }
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Stats_descriptor;
        }

        public Stats getDefaultInstanceForType() {
            return Stats.getDefaultInstance();
        }

        public Stats build() {
            Stats buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Stats buildPartial() {
            Stats stats = new Stats((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            int i = this.bitField0_;
            stats.capacity_ = this.capacity_;
            stats.start_ = this.start_;
            stats.finish_ = this.finish_;
            stats.successful_ = this.successful_;
            stats.completed_ = this.completed_;
            if (this.adUnitBuilder_ == null) {
                if ((this.bitField0_ & 1) != 0) {
                    this.adUnit_ = Collections.unmodifiableList(this.adUnit_);
                    this.bitField0_ &= -2;
                }
                stats.adUnit_ = this.adUnit_;
            } else {
                stats.adUnit_ = this.adUnitBuilder_.build();
            }
            onBuilt();
            return stats;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Stats) {
                return mergeFrom((Stats) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Stats stats) {
            if (stats == Stats.getDefaultInstance()) {
                return this;
            }
            if (stats.getCapacity() != 0) {
                setCapacity(stats.getCapacity());
            }
            if (stats.getStart() != 0) {
                setStart(stats.getStart());
            }
            if (stats.getFinish() != 0) {
                setFinish(stats.getFinish());
            }
            if (stats.getSuccessful()) {
                setSuccessful(stats.getSuccessful());
            }
            if (stats.getCompleted()) {
                setCompleted(stats.getCompleted());
            }
            if (this.adUnitBuilder_ == null) {
                if (!stats.adUnit_.isEmpty()) {
                    if (this.adUnit_.isEmpty()) {
                        this.adUnit_ = stats.adUnit_;
                        this.bitField0_ &= -2;
                    } else {
                        ensureAdUnitIsMutable();
                        this.adUnit_.addAll(stats.adUnit_);
                    }
                    onChanged();
                }
            } else if (!stats.adUnit_.isEmpty()) {
                if (this.adUnitBuilder_.isEmpty()) {
                    this.adUnitBuilder_.dispose();
                    RepeatedFieldBuilderV3<AdUnit, Builder, AdUnitOrBuilder> repeatedFieldBuilderV3 = null;
                    this.adUnitBuilder_ = null;
                    this.adUnit_ = stats.adUnit_;
                    this.bitField0_ &= -2;
                    if (Stats.alwaysUseFieldBuilders) {
                        repeatedFieldBuilderV3 = getAdUnitFieldBuilder();
                    }
                    this.adUnitBuilder_ = repeatedFieldBuilderV3;
                } else {
                    this.adUnitBuilder_.addAllMessages(stats.adUnit_);
                }
            }
            mergeUnknownFields(stats.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Stats stats;
            Stats stats2 = null;
            try {
                Stats stats3 = (Stats) Stats.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (stats3 != null) {
                    mergeFrom(stats3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                stats = (Stats) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                stats2 = stats;
                if (stats2 != null) {
                }
                throw th;
            }
        }

        public int getCapacity() {
            return this.capacity_;
        }

        public Builder setCapacity(int i) {
            this.capacity_ = i;
            onChanged();
            return this;
        }

        public Builder clearCapacity() {
            this.capacity_ = 0;
            onChanged();
            return this;
        }

        public long getStart() {
            return this.start_;
        }

        public Builder setStart(long j) {
            this.start_ = j;
            onChanged();
            return this;
        }

        public Builder clearStart() {
            this.start_ = 0;
            onChanged();
            return this;
        }

        public long getFinish() {
            return this.finish_;
        }

        public Builder setFinish(long j) {
            this.finish_ = j;
            onChanged();
            return this;
        }

        public Builder clearFinish() {
            this.finish_ = 0;
            onChanged();
            return this;
        }

        public boolean getSuccessful() {
            return this.successful_;
        }

        public Builder setSuccessful(boolean z) {
            this.successful_ = z;
            onChanged();
            return this;
        }

        public Builder clearSuccessful() {
            this.successful_ = false;
            onChanged();
            return this;
        }

        public boolean getCompleted() {
            return this.completed_;
        }

        public Builder setCompleted(boolean z) {
            this.completed_ = z;
            onChanged();
            return this;
        }

        public Builder clearCompleted() {
            this.completed_ = false;
            onChanged();
            return this;
        }

        private void ensureAdUnitIsMutable() {
            if ((this.bitField0_ & 1) == 0) {
                this.adUnit_ = new ArrayList(this.adUnit_);
                this.bitField0_ |= 1;
            }
        }

        public List<AdUnit> getAdUnitList() {
            if (this.adUnitBuilder_ == null) {
                return Collections.unmodifiableList(this.adUnit_);
            }
            return this.adUnitBuilder_.getMessageList();
        }

        public int getAdUnitCount() {
            if (this.adUnitBuilder_ == null) {
                return this.adUnit_.size();
            }
            return this.adUnitBuilder_.getCount();
        }

        public AdUnit getAdUnit(int i) {
            if (this.adUnitBuilder_ == null) {
                return (AdUnit) this.adUnit_.get(i);
            }
            return (AdUnit) this.adUnitBuilder_.getMessage(i);
        }

        public Builder setAdUnit(int i, AdUnit adUnit) {
            if (this.adUnitBuilder_ != null) {
                this.adUnitBuilder_.setMessage(i, adUnit);
            } else if (adUnit != null) {
                ensureAdUnitIsMutable();
                this.adUnit_.set(i, adUnit);
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setAdUnit(int i, Builder builder) {
            if (this.adUnitBuilder_ == null) {
                ensureAdUnitIsMutable();
                this.adUnit_.set(i, builder.build());
                onChanged();
            } else {
                this.adUnitBuilder_.setMessage(i, builder.build());
            }
            return this;
        }

        public Builder addAdUnit(AdUnit adUnit) {
            if (this.adUnitBuilder_ != null) {
                this.adUnitBuilder_.addMessage(adUnit);
            } else if (adUnit != null) {
                ensureAdUnitIsMutable();
                this.adUnit_.add(adUnit);
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder addAdUnit(int i, AdUnit adUnit) {
            if (this.adUnitBuilder_ != null) {
                this.adUnitBuilder_.addMessage(i, adUnit);
            } else if (adUnit != null) {
                ensureAdUnitIsMutable();
                this.adUnit_.add(i, adUnit);
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder addAdUnit(Builder builder) {
            if (this.adUnitBuilder_ == null) {
                ensureAdUnitIsMutable();
                this.adUnit_.add(builder.build());
                onChanged();
            } else {
                this.adUnitBuilder_.addMessage(builder.build());
            }
            return this;
        }

        public Builder addAdUnit(int i, Builder builder) {
            if (this.adUnitBuilder_ == null) {
                ensureAdUnitIsMutable();
                this.adUnit_.add(i, builder.build());
                onChanged();
            } else {
                this.adUnitBuilder_.addMessage(i, builder.build());
            }
            return this;
        }

        public Builder addAllAdUnit(Iterable<? extends AdUnit> iterable) {
            if (this.adUnitBuilder_ == null) {
                ensureAdUnitIsMutable();
                com.explorestack.protobuf.AbstractMessageLite.Builder.addAll(iterable, this.adUnit_);
                onChanged();
            } else {
                this.adUnitBuilder_.addAllMessages(iterable);
            }
            return this;
        }

        public Builder clearAdUnit() {
            if (this.adUnitBuilder_ == null) {
                this.adUnit_ = Collections.emptyList();
                this.bitField0_ &= -2;
                onChanged();
            } else {
                this.adUnitBuilder_.clear();
            }
            return this;
        }

        public Builder removeAdUnit(int i) {
            if (this.adUnitBuilder_ == null) {
                ensureAdUnitIsMutable();
                this.adUnit_.remove(i);
                onChanged();
            } else {
                this.adUnitBuilder_.remove(i);
            }
            return this;
        }

        public Builder getAdUnitBuilder(int i) {
            return (Builder) getAdUnitFieldBuilder().getBuilder(i);
        }

        public AdUnitOrBuilder getAdUnitOrBuilder(int i) {
            if (this.adUnitBuilder_ == null) {
                return (AdUnitOrBuilder) this.adUnit_.get(i);
            }
            return (AdUnitOrBuilder) this.adUnitBuilder_.getMessageOrBuilder(i);
        }

        public List<? extends AdUnitOrBuilder> getAdUnitOrBuilderList() {
            if (this.adUnitBuilder_ != null) {
                return this.adUnitBuilder_.getMessageOrBuilderList();
            }
            return Collections.unmodifiableList(this.adUnit_);
        }

        public Builder addAdUnitBuilder() {
            return (Builder) getAdUnitFieldBuilder().addBuilder(AdUnit.getDefaultInstance());
        }

        public Builder addAdUnitBuilder(int i) {
            return (Builder) getAdUnitFieldBuilder().addBuilder(i, AdUnit.getDefaultInstance());
        }

        public List<Builder> getAdUnitBuilderList() {
            return getAdUnitFieldBuilder().getBuilderList();
        }

        private RepeatedFieldBuilderV3<AdUnit, Builder, AdUnitOrBuilder> getAdUnitFieldBuilder() {
            if (this.adUnitBuilder_ == null) {
                List<AdUnit> list = this.adUnit_;
                boolean z = true;
                if ((this.bitField0_ & 1) == 0) {
                    z = false;
                }
                this.adUnitBuilder_ = new RepeatedFieldBuilderV3<>(list, z, getParentForChildren(), isClean());
                this.adUnit_ = null;
            }
            return this.adUnitBuilder_;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private Stats(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Stats() {
        this.memoizedIsInitialized = -1;
        this.adUnit_ = Collections.emptyList();
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Stats();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private Stats(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            boolean z2 = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            this.capacity_ = codedInputStream.readInt32();
                        } else if (readTag == 16) {
                            this.start_ = codedInputStream.readInt64();
                        } else if (readTag == 24) {
                            this.finish_ = codedInputStream.readInt64();
                        } else if (readTag == 32) {
                            this.successful_ = codedInputStream.readBool();
                        } else if (readTag == 40) {
                            this.completed_ = codedInputStream.readBool();
                        } else if (readTag == 50) {
                            if (!z2 || !true) {
                                this.adUnit_ = new ArrayList();
                                z2 |= true;
                            }
                            this.adUnit_.add(codedInputStream.readMessage(AdUnit.parser(), extensionRegistryLite));
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    if (z2 && true) {
                        this.adUnit_ = Collections.unmodifiableList(this.adUnit_);
                    }
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            if (z2 && true) {
                this.adUnit_ = Collections.unmodifiableList(this.adUnit_);
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Stats_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Stats_fieldAccessorTable.ensureFieldAccessorsInitialized(Stats.class, Builder.class);
    }

    public int getCapacity() {
        return this.capacity_;
    }

    public long getStart() {
        return this.start_;
    }

    public long getFinish() {
        return this.finish_;
    }

    public boolean getSuccessful() {
        return this.successful_;
    }

    public boolean getCompleted() {
        return this.completed_;
    }

    public List<AdUnit> getAdUnitList() {
        return this.adUnit_;
    }

    public List<? extends AdUnitOrBuilder> getAdUnitOrBuilderList() {
        return this.adUnit_;
    }

    public int getAdUnitCount() {
        return this.adUnit_.size();
    }

    public AdUnit getAdUnit(int i) {
        return (AdUnit) this.adUnit_.get(i);
    }

    public AdUnitOrBuilder getAdUnitOrBuilder(int i) {
        return (AdUnitOrBuilder) this.adUnit_.get(i);
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.capacity_ != 0) {
            codedOutputStream.writeInt32(1, this.capacity_);
        }
        if (this.start_ != 0) {
            codedOutputStream.writeInt64(2, this.start_);
        }
        if (this.finish_ != 0) {
            codedOutputStream.writeInt64(3, this.finish_);
        }
        if (this.successful_) {
            codedOutputStream.writeBool(4, this.successful_);
        }
        if (this.completed_) {
            codedOutputStream.writeBool(5, this.completed_);
        }
        for (int i = 0; i < this.adUnit_.size(); i++) {
            codedOutputStream.writeMessage(6, (MessageLite) this.adUnit_.get(i));
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = this.capacity_ != 0 ? CodedOutputStream.computeInt32Size(1, this.capacity_) + 0 : 0;
        if (this.start_ != 0) {
            computeInt32Size += CodedOutputStream.computeInt64Size(2, this.start_);
        }
        if (this.finish_ != 0) {
            computeInt32Size += CodedOutputStream.computeInt64Size(3, this.finish_);
        }
        if (this.successful_) {
            computeInt32Size += CodedOutputStream.computeBoolSize(4, this.successful_);
        }
        if (this.completed_) {
            computeInt32Size += CodedOutputStream.computeBoolSize(5, this.completed_);
        }
        for (int i2 = 0; i2 < this.adUnit_.size(); i2++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(6, (MessageLite) this.adUnit_.get(i2));
        }
        int serializedSize = computeInt32Size + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Stats)) {
            return super.equals(obj);
        }
        Stats stats = (Stats) obj;
        if (getCapacity() == stats.getCapacity() && getStart() == stats.getStart() && getFinish() == stats.getFinish() && getSuccessful() == stats.getSuccessful() && getCompleted() == stats.getCompleted() && getAdUnitList().equals(stats.getAdUnitList()) && this.unknownFields.equals(stats.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getCapacity()) * 37) + 2) * 53) + Internal.hashLong(getStart())) * 37) + 3) * 53) + Internal.hashLong(getFinish())) * 37) + 4) * 53) + Internal.hashBoolean(getSuccessful())) * 37) + 5) * 53) + Internal.hashBoolean(getCompleted());
        if (getAdUnitCount() > 0) {
            hashCode = (((hashCode * 37) + 6) * 53) + getAdUnitList().hashCode();
        }
        int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode2;
        return hashCode2;
    }

    public static Stats parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Stats) PARSER.parseFrom(byteBuffer);
    }

    public static Stats parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Stats) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Stats parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Stats) PARSER.parseFrom(byteString);
    }

    public static Stats parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Stats) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Stats parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Stats) PARSER.parseFrom(bArr);
    }

    public static Stats parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Stats) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Stats parseFrom(InputStream inputStream) throws IOException {
        return (Stats) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Stats parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Stats) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Stats parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Stats) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Stats parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Stats) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Stats parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Stats) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Stats parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Stats) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Stats stats) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(stats);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Stats getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Stats> parser() {
        return PARSER;
    }

    public Parser<Stats> getParserForType() {
        return PARSER;
    }

    public Stats getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
