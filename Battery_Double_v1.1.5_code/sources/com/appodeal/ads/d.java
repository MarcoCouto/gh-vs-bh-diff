package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import org.json.JSONObject;

public interface d {
    void a(@NonNull Context context, @NonNull RestrictedData restrictedData);

    void a(@NonNull Context context, @NonNull JSONObject jSONObject, @NonNull RestrictedData restrictedData);

    void b(@NonNull Context context, @NonNull RestrictedData restrictedData);
}
