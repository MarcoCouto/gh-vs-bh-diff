package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.b.e;
import org.json.JSONObject;

final class an {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static b f1534a;
    @VisibleForTesting
    static a b;
    private static am<aq, ao> c;

    @VisibleForTesting(otherwise = 3)
    static class a extends p<ao, aq, c> {
        a(q<ao, aq, ?> qVar) {
            super(qVar, e.c(), 3, 1, 3);
            this.e = 1.1f;
            this.f = 1.4f;
        }

        /* access modifiers changed from: protected */
        public ao a(@NonNull aq aqVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
            return new ao(aqVar, adNetwork, boVar);
        }

        /* access modifiers changed from: protected */
        public aq a(c cVar) {
            return new aq(cVar);
        }

        public void a(Activity activity) {
            if (l() && r()) {
                aq aqVar = (aq) y();
                if (aqVar == null || aqVar.M()) {
                    d((Context) activity);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(Context context, c cVar) {
            if (Appodeal.d) {
                br.a((Runnable) new Runnable() {
                    public void run() {
                        ap.a().f1538a.a(null, null, (LoadingError) null);
                    }
                });
            } else {
                super.a(context, cVar);
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
        }

        /* access modifiers changed from: protected */
        public boolean a(AdNetwork adNetwork, JSONObject jSONObject, String str, boolean z) {
            if (!z && adNetwork != null && adNetwork.isInterstitialShowing() && x().size() > 1) {
                aq aqVar = (aq) A();
                aq aqVar2 = (aq) z();
                if (!(aqVar == null || aqVar2 == null || aqVar2.B() == null)) {
                    if (str.equals(((ao) aqVar2.B()).getId())) {
                        aqVar.b(jSONObject);
                    }
                    an.a(aqVar, 0, false, false);
                    return true;
                }
            }
            return super.a(adNetwork, jSONObject, str, z);
        }

        /* access modifiers changed from: protected */
        public boolean a(aq aqVar, int i) {
            if (aqVar.y() != 1 || aqVar.v() == null || aqVar.v() != aqVar.a(i)) {
                return super.a(aqVar, i);
            }
            String optString = aqVar.v().optString("status");
            boolean z = false;
            if (TextUtils.isEmpty(optString)) {
                return false;
            }
            AdNetwork c = q().c(optString);
            if (c != null && c.isInterstitialShowing()) {
                z = true;
            }
            return z;
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            ap.a().f1538a.a(context, new c());
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "interstitials_disabled";
        }

        public boolean r() {
            return ap.a().b();
        }
    }

    @VisibleForTesting
    static class b extends bd<ao, aq> {
        b() {
            super(ap.a().f1538a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void b(aq aqVar) {
            an.a(aqVar, 0, false, false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void q(aq aqVar, ao aoVar) {
            aoVar.b().setInterstitialShowing(true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void c(aq aqVar, ao aoVar, LoadingError loadingError) {
            super.c(aqVar, aoVar, loadingError);
            ap.a().f1538a.b();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(aq aqVar, ao aoVar, boolean z) {
            super.b(aqVar, aoVar, z);
            if (c(aqVar, aoVar)) {
                an.a(Appodeal.e, new l(this.f1662a.t()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(aq aqVar, ao aoVar, Object obj) {
            return super.j(aqVar, aoVar, obj) && this.f1662a.D() > 0;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void c(aq aqVar) {
            an.a(aqVar, 0, false, true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(@Nullable aq aqVar, @Nullable ao aoVar, @Nullable LoadingError loadingError) {
            super.a(aqVar, aoVar, loadingError);
            am.a();
            if (aqVar != null && !this.f1662a.x().isEmpty()) {
                ap.a().f1538a.b();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public boolean e(aq aqVar, ao aoVar) {
            return (!c(aqVar, aoVar) && (aqVar.a(0) == aqVar.v() || aoVar.isPrecache() || aoVar.h() || this.f1662a.a(aqVar, aoVar))) && super.e(aqVar, aoVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void e(aq aqVar) {
            if (!aqVar.a() && ap.a().b()) {
                aq aqVar2 = (aq) this.f1662a.y();
                if (aqVar2 == null || aqVar2.M()) {
                    this.f1662a.d(Appodeal.f);
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean c(aq aqVar, ao aoVar) {
            return aqVar.u() && !aoVar.h();
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean a(aq aqVar) {
            return aqVar.v() == null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean l(aq aqVar, ao aoVar) {
            return aqVar.v() == null || (aoVar != null && aqVar.v().optString("id").equals(aoVar.getId()));
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public void c(aq aqVar, ao aoVar) {
            super.c(aqVar, aoVar);
            if (aqVar.v() == aoVar.getJsonData()) {
                aqVar.b((JSONObject) null);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public void n(aq aqVar, ao aoVar) {
            an.d().b();
            am.a();
            this.f1662a.d(null);
            aoVar.b().setInterstitialShowing(false);
            if (!aqVar.n() && this.f1662a.D() > 0 && System.currentTimeMillis() - (aqVar.O() * 1000) >= ((long) this.f1662a.D())) {
                o(aqVar, aoVar);
            }
            f(aqVar);
        }
    }

    static class c extends n<c> {
        c() {
            super("banner", "debug");
        }
    }

    static p<ao, aq, c> a() {
        if (b == null) {
            b = new a(b());
        }
        return b;
    }

    static void a(@Nullable aq aqVar, int i, boolean z, boolean z2) {
        a().a(aqVar, i, z2, z);
    }

    static boolean a(Activity activity, l lVar) {
        return d().a(activity, lVar, a());
    }

    static q<ao, aq, Object> b() {
        if (f1534a == null) {
            f1534a = new b();
        }
        return f1534a;
    }

    /* access modifiers changed from: private */
    public static am<aq, ao> d() {
        if (c == null) {
            c = new am<>("debug");
        }
        return c;
    }
}
