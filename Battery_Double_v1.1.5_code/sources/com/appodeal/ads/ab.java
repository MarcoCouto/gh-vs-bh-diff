package com.appodeal.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.appodeal.ads.b.e;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.o;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import java.util.Map.Entry;
import org.json.JSONObject;

final class ab {

    /* renamed from: a reason: collision with root package name */
    static final ac f1514a = new ac();
    static boolean b = true;
    static boolean c = true;
    @VisibleForTesting
    static c d;
    @VisibleForTesting
    static b e;
    /* access modifiers changed from: private */
    public static Integer f = null;
    @SuppressLint({"StaticFieldLeak"})
    private static a g;

    static class a extends bw<ae, ad> {
        /* access modifiers changed from: private */
        @Nullable
        public b b;

        a() {
            super("debug_banner_320", b.BOTTOM);
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull Activity activity, @NonNull b bVar) {
            ab.a((Context) activity, new d(bVar));
        }

        /* access modifiers changed from: 0000 */
        public boolean a(@NonNull Activity activity, @NonNull bx bxVar, @NonNull p<ad, ae, ?> pVar) {
            if (pVar.l() || !Appodeal.b) {
                return super.a(activity, bxVar, pVar);
            }
            this.b = bxVar.c;
            return false;
        }

        /* access modifiers changed from: 0000 */
        public boolean a(View view) {
            return view instanceof BannerView;
        }
    }

    @VisibleForTesting(otherwise = 3)
    static class b extends p<ad, ae, d> {
        b(q<ad, ae, ?> qVar) {
            super(qVar, e.c(), 4);
        }

        /* access modifiers changed from: protected */
        public ad a(@NonNull ae aeVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
            return new ad(aeVar, adNetwork, boVar);
        }

        /* access modifiers changed from: protected */
        public ae a(d dVar) {
            return new ae(dVar);
        }

        public void a(Activity activity) {
            if (r() && l()) {
                ae aeVar = (ae) y();
                if (aeVar == null || (aeVar.M() && !aeVar.t())) {
                    u d = ab.a().d();
                    if (d == u.HIDDEN || d == u.NEVER_SHOWN) {
                        d((Context) activity);
                    } else {
                        ab.a((Context) activity, new d(ab.a().b()));
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("refresh_period")) {
                ab.f = Integer.valueOf(jSONObject.optInt("refresh_period") * 1000);
            }
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return super.c() && y() == null;
        }

        /* access modifiers changed from: protected */
        public void d() {
            d dVar;
            if (ab.a().b != null) {
                dVar = new d(ab.a().b);
                ab.a().b = null;
            } else {
                dVar = new d();
            }
            ab.a(Appodeal.f, dVar);
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            ab.a(context, new d());
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "banners_disabled";
        }
    }

    @VisibleForTesting
    static class c extends bd<ad, ae> {
        c() {
            super(ab.f1514a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void c(ae aeVar) {
            ab.a(aeVar, 0, false, true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(ae aeVar, ad adVar, boolean z) {
            super.b(aeVar, adVar, z);
            if (b(aeVar, adVar) && !com.appodeal.ads.utils.c.f1710a.contains(Appodeal.e.getClass().getName())) {
                ab.a(Appodeal.e, new bx(this.f1662a.t(), aeVar.U(), true, false));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean e(ae aeVar, ad adVar) {
            return super.e(aeVar, adVar) && !b(aeVar, adVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(ae aeVar, ad adVar, Object obj) {
            return super.j(aeVar, adVar, obj) && this.f1662a.D() > 0;
        }

        /* access modifiers changed from: 0000 */
        public void b(ae aeVar) {
            ab.a(aeVar, 0, false, false);
        }

        /* access modifiers changed from: protected */
        public boolean b(ae aeVar, ad adVar) {
            return aeVar.u() && !adVar.h();
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void d(@Nullable ae aeVar) {
            if (aeVar != null) {
                if (aeVar.M()) {
                    u d = ab.a().d();
                    if (!(d == u.HIDDEN || d == u.NEVER_SHOWN)) {
                        ab.a(Appodeal.f, new d(ab.a().b()));
                        return;
                    }
                } else {
                    return;
                }
            }
            this.f1662a.d(Appodeal.f);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public void j(ae aeVar, ad adVar) {
            if (this.f1662a.r()) {
                u d = ab.a().d();
                if (d == u.HIDDEN || d == u.NEVER_SHOWN) {
                    this.f1662a.d(Appodeal.f);
                } else {
                    ab.a(Appodeal.f, new d(ab.a().b()));
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public void q(final ae aeVar, ad adVar) {
            if (!aeVar.a() && this.f1662a.r()) {
                ae aeVar2 = (ae) this.f1662a.y();
                if (aeVar2 == null || aeVar2.M()) {
                    this.f1662a.d(Appodeal.f);
                }
            }
            if (this.f1662a.r()) {
                br.a((Runnable) new Runnable() {
                    public void run() {
                        try {
                            u d = ab.a().d();
                            if (c.this.f1662a.c(c.this.f1662a.b(aeVar)) && d != u.HIDDEN && d != u.NEVER_SHOWN) {
                                View c = ab.a().c();
                                if (c != null && c.isShown()) {
                                    if (!com.appodeal.ads.utils.c.f1710a.contains(Appodeal.e.getClass().getName())) {
                                        ab.a(Appodeal.e, new bx(c.this.f1662a.t(), ab.a().b(), false, false));
                                    } else {
                                        br.a((Runnable) this, 1000);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.log(e);
                        }
                    }
                }, (long) ab.i().intValue());
            }
        }
    }

    static class d extends n<d> {

        /* renamed from: a reason: collision with root package name */
        private b f1516a;

        d() {
            super("banner_320", "debug_banner_320");
        }

        d(b bVar) {
            this();
            this.f1516a = bVar;
            b(true);
        }

        /* access modifiers changed from: 0000 */
        public b f() {
            return this.f1516a;
        }
    }

    static a a() {
        if (g == null) {
            g = new a();
        }
        return g;
    }

    static void a(Context context, d dVar) {
        b().b(context, dVar);
    }

    static void a(ae aeVar, int i, boolean z, boolean z2) {
        b().a(aeVar, i, z2, z);
    }

    static boolean a(Activity activity) {
        return a().a(activity, b());
    }

    static boolean a(Activity activity, bx bxVar) {
        return a().a(activity, bxVar, b());
    }

    static p<ad, ae, d> b() {
        if (e == null) {
            e = new b(c());
        }
        return e;
    }

    static q<ad, ae, Object> c() {
        if (d == null) {
            d = new c();
        }
        return d;
    }

    static int d() {
        return ((b || c) && br.g(Appodeal.f) > 720.0f) ? 90 : 50;
    }

    static int e() {
        return b ? Math.round(br.f(Appodeal.f)) : (!c || br.f(Appodeal.f) < 728.0f) ? ModuleDescriptor.MODULE_VERSION : Math.min(Math.round(br.f(Appodeal.f)), 728);
    }

    static boolean f() {
        return c && br.f(Appodeal.f) >= 728.0f && br.g(Appodeal.f) > 720.0f;
    }

    static void g() {
        if (a().d() != u.VISIBLE) {
            Log.log("Banner", LogConstants.EVENT_AD_DESTROY, LogLevel.debug);
            ae aeVar = (ae) b().y();
            if (aeVar != null) {
                if (aeVar.B() != null) {
                    o.a(aeVar.B());
                    ((ad) aeVar.B()).q();
                }
                for (Entry value : aeVar.F().entrySet()) {
                    i iVar = (i) value.getValue();
                    if (iVar != null) {
                        o.a(iVar);
                        iVar.q();
                    }
                }
                c().g(aeVar);
                aeVar.R();
                aeVar.Q();
            }
        }
    }

    /* access modifiers changed from: private */
    public static Integer i() {
        int i;
        com.appodeal.ads.b.d t = b().t();
        if (t == null || t.c() <= 0) {
            if (f == null) {
                i = 15000;
            }
            return f;
        }
        i = t.c();
        f = Integer.valueOf(i);
        return f;
    }
}
