package com.appodeal.ads;

import android.location.Location;
import android.support.annotation.Nullable;

public interface LocationData {
    @Nullable
    Location getDeviceLocation();

    @Nullable
    Integer getDeviceLocationType();

    @Nullable
    Float obtainLatitude();

    @Nullable
    Location obtainLocation();

    @Nullable
    Float obtainLongitude();
}
