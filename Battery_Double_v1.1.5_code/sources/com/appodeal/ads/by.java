package com.appodeal.ads;

import android.support.annotation.Nullable;
import com.appodeal.ads.i;

abstract class by<AdObjectType extends i> extends m<AdObjectType> {
    private b d = b.BOTTOM;

    by(@Nullable n nVar) {
        super(nVar);
    }

    /* access modifiers changed from: 0000 */
    public b U() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public void a(b bVar) {
        this.d = bVar;
    }
}
