package com.appodeal.ads.unified;

import android.content.Context;
import android.support.annotation.NonNull;

public interface UnifiedBannerParams extends UnifiedViewAdParams {
    int getMaxHeight(@NonNull Context context);

    int getMaxWidth(@NonNull Context context);

    boolean needLeaderBoard(@NonNull Context context);

    boolean useSmartBanners(@NonNull Context context);
}
