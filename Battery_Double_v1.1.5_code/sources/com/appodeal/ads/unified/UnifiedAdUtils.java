package com.appodeal.ads.unified;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.ConnectionData;
import com.appodeal.ads.LocationData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.br;
import com.appodeal.ads.utils.c;
import com.facebook.internal.NativeProtocol;
import com.ironsource.mediationsdk.IronSourceSegment;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Set;
import org.json.JSONObject;

public class UnifiedAdUtils {
    private static final String DAA_AD_MARKER = "iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AEXCTIRoi+4uAAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAANlBMVEUAAAAbsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM0bsM3///+RYoH0AAAAEHRSTlMAECAwQFBgcICPn6+/z9/vIxqCigAAAAFiS0dEEeK1PboAAALmSURBVHja7ZrdtqMgDIVBUFER8v5POxen02oPQf4SZq1h33a1fJYdIkmEGBoaGhoaGvo3JdfDWmvmXuuvHn7kt6nH+jt85I1kX9/ATX5lXn+Cb7mFFWCD37KaEcBBSHbqtwMv7UwIGlDxBITBAcCbzgAsAREHADh1ZwAAqzoDEAdECgBpQHwBHMZsjjUgbgD+x3GLZwyIG8DfRCiDG+M0NYC7nNAHMCWpK8B+O6NPnoC4AnwZbQkmyk2yAQhpfDAgJBeAENNOniHiAEJoSxwQTwCYFZoFxDMAYoVWAZEAgFkBdskFgFmhRUAkAmBW8AsbAGYFN3MBoFaoC4gHgMlYu30WUEErwDFRASy/stTsGgdEFGB+Z6DrN3zTgIgCfJ72+h/LsBUKAyIGoH6/K0WtUJSkYgAa/wixQkFAFAJgVsi/10c98FkjcDtCrJCbpKIA7w9t8LuIFfICIn4OvB7yxH4RsULO8fxwEi4ngIs90Rq2wkaSC8I5agubUXIBCDEFrXDyAQixVuxCHEAaB3A+W2oJEUz1APIM3NmCCu2CqQd4HzVzySbY+qtZuqMWEoBLLnj6mZ1kC9IBNI0JkwGUL/4DWgCECzq7YALAbo6CCQB5L0nv+9QBLPXtjhoAZPm86kU5ALZ85qtxKUC4ildQ0i0D0C3eBssBkOXLijb5AMi7cGmnLxcAqRGUFwnyALDlK8okOQDo8jWFonQAaaCZ9QsAkJxT305KvR17qoZaIgBdS7ECoE29uhigVdOgEMA361kUAbTs2pQANO1b5QM07tyZyIVac/QuTaSmoDjGGW4HvEZLtXT96xvAqxqmtNb3YjXhjNc9xZ2zUKsDAHDqfusmm3JDJyj8dH3/opviwEc4Xrc7ZYxZCYdIcAAvWBQZYvnvAY7eAExDjctDEJBLd14fmaj0jHOtjnxMJbvGufOOVyve8cGADvp5sbjk2Wum+qvW6XYtuklrLcXQ0NDQ0NDQEIf+APO1za6n5oaAAAAAAElFTkSuQmCC";

    public static boolean checkExistApacheLegacyInManifest() {
        return br.g();
    }

    public static ConnectionData getConnectionData(@NonNull Context context) {
        return br.b(context);
    }

    public static ImageView getDAAImage(Context context) {
        ImageView imageView = new ImageView(context);
        int round = Math.round(((float) 16) * br.h(context));
        imageView.setLayoutParams(new LayoutParams(round, round));
        imageView.setScaleType(ScaleType.FIT_CENTER);
        byte[] decode = Base64.decode(DAA_AD_MARKER, 0);
        imageView.setImageBitmap(BitmapFactory.decodeByteArray(decode, 0, decode.length));
        return imageView;
    }

    public static Object getObjectByName(Object obj, String str) throws Exception {
        return br.a(obj, str);
    }

    public static float getScreenDensity(Context context) {
        return br.h(context);
    }

    public static float getScreenHeightInDp(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Point point = new Point();
        defaultDisplay.getSize(point);
        return ((float) point.y) / displayMetrics.density;
    }

    public static String getStringOrNullFromJson(@Nullable JSONObject jSONObject, @Nullable String str) {
        return br.a(jSONObject, str, (String) null);
    }

    @SafeVarargs
    public static Object invokeMethodByName(Object obj, String str, Pair<Class, Object>... pairArr) throws Exception {
        return br.a(obj, str, pairArr);
    }

    public static boolean isAdActivity(@Nullable Activity activity) {
        return c.b(activity);
    }

    public static boolean isClassAvailable(String... strArr) {
        return br.a(strArr);
    }

    public static boolean isGooglePlayServicesAvailable(@NonNull Context context) {
        return br.w(context);
    }

    public static boolean isPackageInstalled(Context context, String str) {
        return br.a(context, str);
    }

    public static boolean openBrowser(Context context, String str, Runnable runnable) {
        return br.a(context, str, runnable);
    }

    public static String parseUrlWithTopParams(@NonNull Context context, @NonNull String str, @NonNull AdNetworkMediationParams adNetworkMediationParams) {
        Uri parse = Uri.parse(str);
        Set<String> queryParameterNames = parse.getQueryParameterNames();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (String str2 : queryParameterNames) {
            String queryParameter = parse.getQueryParameter(str2);
            if (queryParameter != null) {
                linkedHashMap.put(str2, queryParameter);
            }
        }
        Builder clearQuery = Uri.parse(str).buildUpon().clearQuery();
        if (adNetworkMediationParams.getRestrictedData().isUserAgeRestricted()) {
            linkedHashMap.put("coppa", "1");
        }
        LocationData location = adNetworkMediationParams.getRestrictedData().getLocation(context);
        Integer deviceLocationType = location.getDeviceLocationType();
        if (deviceLocationType != null && deviceLocationType.intValue() == 1) {
            linkedHashMap.put("geo_type", "1");
        }
        Float obtainLatitude = location.obtainLatitude();
        if (obtainLatitude != null) {
            linkedHashMap.put("lat", String.valueOf(obtainLatitude));
        }
        Float obtainLongitude = location.obtainLongitude();
        if (obtainLongitude != null) {
            linkedHashMap.put("lon", String.valueOf(obtainLongitude));
        }
        Gender gender = adNetworkMediationParams.getRestrictedData().getGender();
        if (gender != null) {
            linkedHashMap.put("gender", gender.getStringValue());
        }
        Integer age = adNetworkMediationParams.getRestrictedData().getAge();
        if (age != null) {
            linkedHashMap.put(IronSourceSegment.AGE, String.valueOf(age));
        }
        String userId = adNetworkMediationParams.getRestrictedData().getUserId();
        if (userId != null) {
            linkedHashMap.put("userId", userId);
        }
        linkedHashMap.put("language", Locale.getDefault().getLanguage());
        linkedHashMap.put("connectiontype", String.valueOf(br.s(context)));
        String c = br.c(context);
        if (c != null) {
            linkedHashMap.put("carrier", c);
        }
        if (!adNetworkMediationParams.getRestrictedData().isUserGdprProtected()) {
            if (Appodeal.isInitialized(512)) {
                linkedHashMap.put("native_ad_type", Appodeal.getNativeAdType().toString());
            }
            linkedHashMap.put("device_ext", br.t(context).toString());
            linkedHashMap.put("app_ext", br.u(context).toString());
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            linkedHashMap.put("ppi", String.valueOf(displayMetrics.densityDpi));
            linkedHashMap.put("pxratio", String.valueOf(displayMetrics.density));
        }
        if (adNetworkMediationParams.isCoronaApp()) {
            linkedHashMap.put("corona", "1");
            if (!adNetworkMediationParams.getRestrictedData().isUserGdprProtected()) {
                linkedHashMap.put("store_url", adNetworkMediationParams.getStoreUrl());
                linkedHashMap.put("bundle_id", context.getPackageName());
                linkedHashMap.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, adNetworkMediationParams.getAppName());
            }
        }
        linkedHashMap.put("impid", adNetworkMediationParams.getImpressionId());
        linkedHashMap.put("metadata_headers", "1");
        for (String str3 : linkedHashMap.keySet()) {
            clearQuery.appendQueryParameter(str3, (String) linkedHashMap.get(str3));
        }
        return clearQuery.build().toString();
    }

    public static void sendGetRequest(@Nullable String str) {
        br.e(str);
    }

    public static void setObjectByName(Object obj, String str, Object obj2) throws Exception {
        br.a(obj, str, obj2);
    }
}
