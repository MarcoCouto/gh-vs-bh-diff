package com.appodeal.ads.unified;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.NativeMediaView;
import java.util.List;

public abstract class UnifiedNativeAd {
    private String ageRestriction;
    private String callToAction;
    private List<String> clickNotifyUrls;
    private String clickUrl;
    private String description;
    private List<String> finishNotifyUrls;
    private String iconUrl;
    private String imageUrl;
    private List<String> impressionNotifyUrls;
    private Float rating;
    private String title;
    private long trackingPackageExpiry;
    private String trackingPackageName;
    private String vastVideoTag;
    private String videoUrl;

    public UnifiedNativeAd(String str, String str2, String str3) {
        this(str, str2, str3, null);
    }

    public UnifiedNativeAd(String str, String str2, String str3, Float f) {
        this(str, str2, str3, null, null, f);
    }

    public UnifiedNativeAd(String str, String str2, String str3, String str4, String str5) {
        this(str, str2, str3, str4, str5, null);
    }

    public UnifiedNativeAd(String str, String str2, String str3, String str4, String str5, Float f) {
        this.trackingPackageExpiry = -1;
        this.title = str;
        this.description = str2;
        this.callToAction = str3;
        this.imageUrl = str4;
        this.iconUrl = str5;
        this.rating = f;
    }

    public boolean containsVideo() {
        return false;
    }

    public int getAdId() {
        return hashCode();
    }

    public String getAgeRestriction() {
        return this.ageRestriction;
    }

    public String getCallToAction() {
        return this.callToAction;
    }

    public List<String> getClickNotifyUrls() {
        return this.clickNotifyUrls;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public String getDescription() {
        return this.description;
    }

    public List<String> getFinishNotifyUrls() {
        return this.finishNotifyUrls;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public List<String> getImpressionNotifyUrls() {
        return this.impressionNotifyUrls;
    }

    public Float getRating() {
        return this.rating;
    }

    public String getTitle() {
        return this.title;
    }

    public long getTrackingPackageExpiry() {
        return this.trackingPackageExpiry;
    }

    public String getTrackingPackageName() {
        return this.trackingPackageName;
    }

    public String getVastVideoTag() {
        return this.vastVideoTag;
    }

    public String getVideoUrl() {
        return this.videoUrl;
    }

    public boolean hasVideo() {
        return false;
    }

    @Nullable
    public View obtainIconView(@NonNull Context context) {
        return null;
    }

    @Nullable
    public View obtainProviderView(@NonNull Context context) {
        return null;
    }

    @CallSuper
    public void onAdClick(@Nullable View view) {
    }

    @CallSuper
    public void onAdFinish() {
    }

    @CallSuper
    public void onAdImpression(@Nullable View view) {
    }

    @CallSuper
    public void onAdVideoFinish() {
    }

    public void onConfigure(@NonNull NativeAdView nativeAdView) {
    }

    public boolean onConfigureMediaView(@NonNull NativeMediaView nativeMediaView) {
        return false;
    }

    @CallSuper
    public void onDestroy() {
    }

    public void onRegisterForInteraction(@NonNull NativeAdView nativeAdView) {
    }

    public void onUnregisterForInteraction() {
    }

    public void setAgeRestriction(String str) {
        this.ageRestriction = str;
    }

    /* access modifiers changed from: protected */
    public void setClickNotifyUrls(List<String> list) {
        this.clickNotifyUrls = list;
    }

    /* access modifiers changed from: protected */
    public void setClickUrl(String str) {
        this.clickUrl = str;
    }

    /* access modifiers changed from: protected */
    public void setFinishNotifyUrls(List<String> list) {
        this.finishNotifyUrls = list;
    }

    /* access modifiers changed from: protected */
    public void setImpressionNotifyUrls(List<String> list) {
        this.impressionNotifyUrls = list;
    }

    public void setTrackingPackage(String str, long j) {
        this.trackingPackageName = str;
        this.trackingPackageExpiry = j;
    }

    /* access modifiers changed from: protected */
    public void setVastVideoTag(String str) {
        this.vastVideoTag = str;
    }

    /* access modifiers changed from: protected */
    public void setVideoUrl(String str) {
        this.videoUrl = str;
    }
}
