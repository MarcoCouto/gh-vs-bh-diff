package com.appodeal.ads.unified.mraid;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams.Builder;
import com.explorestack.iab.mraid.MRAIDView;

public abstract class UnifiedMraidBanner<NetworkRequestParams> extends UnifiedBanner<NetworkRequestParams> implements UnifiedViewMraid<UnifiedBannerParams, UnifiedBannerCallback, NetworkRequestParams> {
    private UnifiedMraidViewAd<UnifiedBannerParams, UnifiedBannerCallback, NetworkRequestParams> unifiedMraid = new UnifiedMraidViewAd<UnifiedBannerParams, UnifiedBannerCallback, NetworkRequestParams>(this) {
        public void loadMraid(@NonNull Context context, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) {
            int i = unifiedMraidNetworkParams.width;
            int i2 = unifiedMraidNetworkParams.height;
            if (i > unifiedBannerParams.getMaxWidth(context) || i2 > unifiedBannerParams.getMaxHeight(context)) {
                int i3 = (i * 50) / i2;
                if (i3 > unifiedBannerParams.getMaxWidth(context) || 50 > unifiedBannerParams.getMaxHeight(context)) {
                    unifiedBannerCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
                    return;
                }
                unifiedMraidNetworkParams = new Builder(unifiedMraidNetworkParams).setWidth(i3).setHeight(50).build();
            }
            super.loadMraid(context, unifiedBannerParams, unifiedMraidNetworkParams, unifiedBannerCallback);
        }

        public void performMraidRequest(@NonNull Context context, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedBannerCallback unifiedBannerCallback, @NonNull String str) {
            int i = unifiedMraidNetworkParams.width;
            int i2 = unifiedMraidNetworkParams.height;
            if ((i > unifiedBannerParams.getMaxWidth(context) || i2 > unifiedBannerParams.getMaxHeight(context)) && ((i * 50) / i2 > unifiedBannerParams.getMaxWidth(context) || 50 > unifiedBannerParams.getMaxHeight(context))) {
                unifiedBannerCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
            } else {
                super.performMraidRequest(context, unifiedBannerParams, unifiedMraidNetworkParams, unifiedBannerCallback, str);
            }
        }
    };

    public UnifiedMraidViewListener<UnifiedBannerCallback> createListener(@NonNull Context context, @NonNull MRAIDView mRAIDView, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) {
        UnifiedMraidBannerListener unifiedMraidBannerListener = new UnifiedMraidBannerListener(unifiedBannerCallback, unifiedMraidNetworkParams, mRAIDView, unifiedMraidNetworkParams.width, unifiedMraidNetworkParams.height);
        return unifiedMraidBannerListener;
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedBannerCallback unifiedBannerCallback) {
        this.unifiedMraid.load(activity, unifiedBannerParams, networkrequestparams, unifiedBannerCallback);
    }

    public void loadMraid(@NonNull Context context, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) {
        this.unifiedMraid.loadMraid(context, unifiedBannerParams, unifiedMraidNetworkParams, unifiedBannerCallback);
    }

    public void onClicked() {
        super.onClicked();
        this.unifiedMraid.onClicked();
    }

    public void onDestroy() {
        this.unifiedMraid.onDestroy();
    }

    public void onFinished() {
        super.onFinished();
        this.unifiedMraid.onFinished();
    }

    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull NetworkRequestParams networkrequestparams) {
        super.onPrepareToShow(activity, unifiedBannerParams, networkrequestparams);
        this.unifiedMraid.onPrepareToShow(activity, unifiedBannerParams, networkrequestparams);
    }
}
