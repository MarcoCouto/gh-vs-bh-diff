package com.appodeal.ads.unified.mraid;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.explorestack.iab.mraid.activity.MraidActivity.MraidType;

public abstract class UnifiedMraidInterstitial<NetworkRequestParams> extends UnifiedInterstitial<NetworkRequestParams> implements UnifiedFullscreenMraid<UnifiedInterstitialParams, UnifiedInterstitialCallback, NetworkRequestParams> {
    private final UnifiedMraidFullscreenAd<UnifiedInterstitialParams, UnifiedInterstitialCallback, NetworkRequestParams> unifiedAd = new UnifiedMraidFullscreenAd<>(this);

    public UnifiedMraidFullscreenListener<UnifiedInterstitialCallback> createListener(@NonNull Context context, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        return new UnifiedMraidInterstitialListener(unifiedInterstitialCallback, unifiedMraidNetworkParams);
    }

    public MraidType getMraidType() {
        return MraidType.Static;
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.unifiedAd.load(activity, unifiedInterstitialParams, networkrequestparams, unifiedInterstitialCallback);
    }

    public void loadMraid(@NonNull Context context, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.unifiedAd.loadMraid(context, unifiedInterstitialParams, unifiedMraidNetworkParams, unifiedInterstitialCallback);
    }

    public void onClicked() {
        super.onClicked();
        this.unifiedAd.onClicked();
    }

    public void onDestroy() {
        this.unifiedAd.onDestroy();
    }

    public void onFinished() {
        super.onFinished();
        this.unifiedAd.onFinished();
    }

    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull NetworkRequestParams networkrequestparams) {
        super.onPrepareToShow(activity, unifiedInterstitialParams, networkrequestparams);
        this.unifiedAd.onPrepareToShow(activity, unifiedInterstitialParams, networkrequestparams);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.unifiedAd.show(activity, unifiedInterstitialCallback);
    }
}
