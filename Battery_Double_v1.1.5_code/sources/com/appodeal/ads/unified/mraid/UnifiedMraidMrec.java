package com.appodeal.ads.unified.mraid;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.explorestack.iab.mraid.MRAIDView;

public abstract class UnifiedMraidMrec<NetworkRequestParams> extends UnifiedMrec<NetworkRequestParams> implements UnifiedViewMraid<UnifiedMrecParams, UnifiedMrecCallback, NetworkRequestParams> {
    private UnifiedMraidViewAd<UnifiedMrecParams, UnifiedMrecCallback, NetworkRequestParams> unifiedMraid = new UnifiedMraidViewAd<>(this);

    public UnifiedMraidViewListener<UnifiedMrecCallback> createListener(@NonNull Context context, @NonNull MRAIDView mRAIDView, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) {
        return new UnifiedMraidMrecListener(unifiedMrecCallback, unifiedMraidNetworkParams, mRAIDView);
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedMrecCallback unifiedMrecCallback) {
        this.unifiedMraid.load(activity, unifiedMrecParams, networkrequestparams, unifiedMrecCallback);
    }

    public void loadMraid(@NonNull Context context, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) {
        this.unifiedMraid.loadMraid(context, unifiedMrecParams, unifiedMraidNetworkParams, unifiedMrecCallback);
    }

    public void onClicked() {
        super.onClicked();
        this.unifiedMraid.onClicked();
    }

    public void onDestroy() {
        this.unifiedMraid.onDestroy();
    }

    public void onFinished() {
        super.onFinished();
        this.unifiedMraid.onFinished();
    }

    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull NetworkRequestParams networkrequestparams) {
        super.onPrepareToShow(activity, unifiedMrecParams, networkrequestparams);
        this.unifiedMraid.onPrepareToShow(activity, unifiedMrecParams, networkrequestparams);
    }
}
