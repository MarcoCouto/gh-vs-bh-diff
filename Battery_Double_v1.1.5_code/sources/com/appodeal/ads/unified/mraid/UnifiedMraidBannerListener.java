package com.appodeal.ads.unified.mraid;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.explorestack.iab.mraid.MRAIDView;

public class UnifiedMraidBannerListener extends UnifiedMraidViewListener<UnifiedBannerCallback> {
    private final int height;
    private final int width;

    public UnifiedMraidBannerListener(@NonNull UnifiedBannerCallback unifiedBannerCallback, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull MRAIDView mRAIDView, int i, int i2) {
        super(unifiedBannerCallback, unifiedMraidNetworkParams, mRAIDView);
        this.width = i;
        this.height = i2;
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureCallTel(String str) {
        super.mraidNativeFeatureCallTel(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureCreateCalendarEvent(String str) {
        super.mraidNativeFeatureCreateCalendarEvent(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureOpenBrowser(String str, WebView webView) {
        super.mraidNativeFeatureOpenBrowser(str, webView);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeaturePlayVideo(String str) {
        super.mraidNativeFeaturePlayVideo(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureSendSms(String str) {
        super.mraidNativeFeatureSendSms(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureStorePicture(String str) {
        super.mraidNativeFeatureStorePicture(str);
    }

    public /* bridge */ /* synthetic */ void mraidViewClose(MRAIDView mRAIDView) {
        super.mraidViewClose(mRAIDView);
    }

    public /* bridge */ /* synthetic */ void mraidViewExpand(MRAIDView mRAIDView) {
        super.mraidViewExpand(mRAIDView);
    }

    public void mraidViewLoaded(MRAIDView mRAIDView) {
        ((UnifiedBannerCallback) this.callback).onAdLoaded(mRAIDView, this.width, this.height);
    }

    public /* bridge */ /* synthetic */ void mraidViewNoFill(MRAIDView mRAIDView) {
        super.mraidViewNoFill(mRAIDView);
    }

    public /* bridge */ /* synthetic */ boolean mraidViewResize(MRAIDView mRAIDView, int i, int i2, int i3, int i4) {
        return super.mraidViewResize(mRAIDView, i, i2, i3, i4);
    }
}
