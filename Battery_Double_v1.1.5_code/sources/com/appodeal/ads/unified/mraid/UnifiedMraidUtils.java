package com.appodeal.ads.unified.mraid;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.br;
import com.appodeal.ads.unified.UnifiedAdCallback;
import com.appodeal.ads.unified.UnifiedAdParams;
import com.appodeal.ads.unified.UnifiedFullscreenAd;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.appodeal.ads.unified.UnifiedFullscreenAdParams;
import com.appodeal.ads.unified.UnifiedViewAd;
import com.appodeal.ads.unified.UnifiedViewAdCallback;
import com.appodeal.ads.unified.UnifiedViewAdParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams.Builder;
import com.appodeal.ads.utils.Log;
import com.explorestack.iab.mraid.MRAIDInterstitial;
import com.explorestack.iab.mraid.MRAIDView;
import com.explorestack.iab.mraid.MRAIDView.builder;
import com.explorestack.iab.mraid.activity.MraidActivity;
import com.explorestack.iab.mraid.activity.MraidActivity.MraidType;
import com.explorestack.iab.utils.CircularProgressBar;

class UnifiedMraidUtils {
    private static final String MRAID_FULLSCREEN_PROGRESS_TAG = "AppodealProgressView_fullscreen";
    private static final String MRAID_VIEW_PROGRESS_TAG = "AppodealProgressView_view";

    interface UnifiedFullscreenMraid<UnifiedAdParamsType extends UnifiedFullscreenAdParams, UnifiedAdCallbackType extends UnifiedFullscreenAdCallback, NetworkRequestParams> extends UnifiedMraid<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> {
        UnifiedMraidFullscreenListener<UnifiedAdCallbackType> createListener(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype);

        MraidType getMraidType();
    }

    interface UnifiedMraid<UnifiedAdParamsType extends UnifiedAdParams, UnifiedAdCallbackType extends UnifiedAdCallback, NetworkRequestParams> {
        void loadMraid(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype);

        @Nullable
        UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype);

        void requestMraid(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype, @NonNull String str);
    }

    interface UnifiedMraidAd<UnifiedAdParamsType extends UnifiedAdParams, UnifiedAdCallbackType extends UnifiedAdCallback> {
        void loadMraid(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype);

        void performMraidRequest(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype, @NonNull String str);
    }

    static class UnifiedMraidFullscreenAd<UnifiedAdParamsType extends UnifiedFullscreenAdParams, UnifiedAdCallbackType extends UnifiedFullscreenAdCallback, NetworkRequestParams> extends UnifiedFullscreenAd<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> implements UnifiedMraidAd<UnifiedAdParamsType, UnifiedAdCallbackType> {
        @Nullable
        private MRAIDInterstitial mraidInterstitial;
        @Nullable
        private UnifiedMraidFullscreenListener mraidListener;
        @NonNull
        private UnifiedFullscreenMraid<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> unifiedMraidViewAd;

        UnifiedMraidFullscreenAd(@NonNull UnifiedFullscreenMraid<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> unifiedFullscreenMraid) {
            this.unifiedMraidViewAd = unifiedFullscreenMraid;
        }

        public void load(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype) throws Exception {
            UnifiedMraidUtils.performLoadMraid(activity, unifiedadparamstype, this.unifiedMraidViewAd.obtainMraidParams(activity, unifiedadparamstype, networkrequestparams, unifiedadcallbacktype), unifiedadcallbacktype, this);
        }

        public void loadMraid(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype) {
            Builder builder;
            int i;
            if (!UnifiedMraidUtils.isValidAdm(unifiedMraidNetworkParams.adm)) {
                unifiedadcallbacktype.onAdLoadFailed(LoadingError.IncorrectAdunit);
                return;
            }
            MraidType mraidType = this.unifiedMraidViewAd.getMraidType();
            if (mraidType == MraidType.Video) {
                builder = new Builder(unifiedMraidNetworkParams);
                i = 5;
            } else {
                if (mraidType == MraidType.Rewarded) {
                    builder = new Builder(unifiedMraidNetworkParams);
                    i = unifiedMraidNetworkParams.closeTime;
                }
                UnifiedMraidNetworkParams unifiedMraidNetworkParams2 = unifiedMraidNetworkParams;
                this.mraidListener = this.unifiedMraidViewAd.createListener(context, unifiedadparamstype, unifiedMraidNetworkParams2, unifiedadcallbacktype);
                this.mraidInterstitial = UnifiedMraidUtils.createMraidInterstitial(context, unifiedMraidNetworkParams2, this.mraidListener, unifiedMraidNetworkParams2.adm, unifiedMraidNetworkParams2.width, unifiedMraidNetworkParams2.height);
                this.mraidInterstitial.load();
            }
            unifiedMraidNetworkParams = builder.setCloseTime(i).build();
            UnifiedMraidNetworkParams unifiedMraidNetworkParams22 = unifiedMraidNetworkParams;
            this.mraidListener = this.unifiedMraidViewAd.createListener(context, unifiedadparamstype, unifiedMraidNetworkParams22, unifiedadcallbacktype);
            this.mraidInterstitial = UnifiedMraidUtils.createMraidInterstitial(context, unifiedMraidNetworkParams22, this.mraidListener, unifiedMraidNetworkParams22.adm, unifiedMraidNetworkParams22.width, unifiedMraidNetworkParams22.height);
            this.mraidInterstitial.load();
        }

        public void onDestroy() {
            if (this.mraidInterstitial != null) {
                this.mraidInterstitial.destroy();
            }
        }

        public void onFinished() {
            super.onFinished();
            if (this.mraidInterstitial != null) {
                this.mraidInterstitial.trackAppodealXFinish();
            }
        }

        public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParams networkrequestparams) {
            super.onPrepareToShow(activity, unifiedadparamstype, networkrequestparams);
            if (this.mraidInterstitial != null) {
                this.mraidInterstitial.setSegmentAndPlacement(unifiedadparamstype.obtainSegmentId(), unifiedadparamstype.obtainPlacementId());
                if (unifiedadparamstype.getAfd() > 0) {
                    this.mraidInterstitial.setAfd(unifiedadparamstype.getAfd());
                }
            }
        }

        public void performMraidRequest(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype, @NonNull String str) {
            this.unifiedMraidViewAd.requestMraid(context, unifiedadparamstype, unifiedMraidNetworkParams, unifiedadcallbacktype, str);
        }

        public void show(@NonNull Activity activity, @NonNull UnifiedFullscreenAdCallback unifiedFullscreenAdCallback) {
            MraidActivity.show(activity, this.mraidInterstitial, this.unifiedMraidViewAd.getMraidType(), this.mraidListener);
        }
    }

    static class UnifiedMraidViewAd<UnifiedAdParamsType extends UnifiedViewAdParams, UnifiedAdCallbackType extends UnifiedViewAdCallback, NetworkRequestParams> extends UnifiedViewAd<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> implements UnifiedMraidAd<UnifiedAdParamsType, UnifiedAdCallbackType> {
        @Nullable
        private MRAIDView mraidView;
        @NonNull
        private UnifiedViewMraid<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> unifiedMraidViewAd;

        UnifiedMraidViewAd(@NonNull UnifiedViewMraid<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> unifiedViewMraid) {
            this.unifiedMraidViewAd = unifiedViewMraid;
        }

        public void load(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype) {
            UnifiedMraidUtils.performLoadMraid(activity, unifiedadparamstype, this.unifiedMraidViewAd.obtainMraidParams(activity, unifiedadparamstype, networkrequestparams, unifiedadcallbacktype), unifiedadcallbacktype, this);
        }

        public void loadMraid(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype) {
            if (!UnifiedMraidUtils.isValidAdm(unifiedMraidNetworkParams.adm)) {
                unifiedadcallbacktype.onAdLoadFailed(LoadingError.IncorrectAdunit);
                return;
            }
            this.mraidView = UnifiedMraidUtils.createMraid(context, unifiedMraidNetworkParams, unifiedMraidNetworkParams.adm, unifiedMraidNetworkParams.width, unifiedMraidNetworkParams.height);
            UnifiedMraidViewListener createListener = this.unifiedMraidViewAd.createListener(context, this.mraidView, unifiedadparamstype, unifiedMraidNetworkParams, unifiedadcallbacktype);
            this.mraidView.setListener(createListener);
            this.mraidView.setNativeFeatureListener(createListener);
            this.mraidView.load();
        }

        public void onClicked() {
            super.onClicked();
            if (this.mraidView != null) {
                this.mraidView.trackAppodealXClick();
            }
        }

        public void onDestroy() {
            if (this.mraidView != null) {
                this.mraidView.destroy();
                this.mraidView = null;
            }
        }

        public void onFinished() {
            super.onFinished();
            if (this.mraidView != null) {
                this.mraidView.trackAppodealXFinish();
            }
        }

        public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParams networkrequestparams) {
            super.onPrepareToShow(activity, unifiedadparamstype, networkrequestparams);
            if (this.mraidView != null) {
                this.mraidView.setSegmentAndPlacement(unifiedadparamstype.obtainSegmentId(), unifiedadparamstype.obtainPlacementId());
                this.mraidView.show();
            }
        }

        public void performMraidRequest(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype, @NonNull String str) {
            this.unifiedMraidViewAd.requestMraid(context, unifiedadparamstype, unifiedMraidNetworkParams, unifiedadcallbacktype, str);
        }
    }

    interface UnifiedViewMraid<UnifiedAdParamsType extends UnifiedViewAdParams, UnifiedAdCallbackType extends UnifiedViewAdCallback, NetworkRequestParams> extends UnifiedMraid<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> {
        UnifiedMraidViewListener<UnifiedAdCallbackType> createListener(@NonNull Context context, @NonNull MRAIDView mRAIDView, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype);
    }

    UnifiedMraidUtils() {
    }

    static void addBannerSpinnerView(View view) {
        if (view instanceof MRAIDView) {
            final MRAIDView mRAIDView = (MRAIDView) view;
            View findViewWithTag = mRAIDView.findViewWithTag(MRAID_VIEW_PROGRESS_TAG);
            if (findViewWithTag != null) {
                findViewWithTag.setVisibility(0);
                return;
            }
            final ProgressBar progressBar = new ProgressBar(view.getContext());
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            progressBar.setLayoutParams(layoutParams);
            progressBar.setBackgroundColor(0);
            progressBar.setTag(MRAID_VIEW_PROGRESS_TAG);
            br.a((Runnable) new Runnable() {
                public void run() {
                    mRAIDView.addView(progressBar);
                }
            });
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x001b  */
    static void addProgressView(View view) {
        boolean z;
        ViewParent parent = view.getParent();
        while (true) {
            z = parent instanceof ViewGroup;
            if (z && ((ViewGroup) parent).getId() != 16908290) {
                parent = parent.getParent();
            } else if (z) {
                final ViewGroup viewGroup = (ViewGroup) parent;
                if (viewGroup.getId() == 16908290) {
                    if (viewGroup.findViewWithTag(MRAID_FULLSCREEN_PROGRESS_TAG) == null) {
                        final CircularProgressBar circularProgressBar = new CircularProgressBar(view.getContext());
                        final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2, 17);
                        circularProgressBar.setTag(MRAID_FULLSCREEN_PROGRESS_TAG);
                        circularProgressBar.setColorSchemeColors(Color.parseColor("#B4FFFFFF"));
                        circularProgressBar.setProgressBackgroundColor(Color.parseColor("#52000000"));
                        br.a((Runnable) new Runnable() {
                            public void run() {
                                viewGroup.addView(circularProgressBar, layoutParams);
                            }
                        });
                    }
                    return;
                }
            }
        }
        if (z) {
        }
        Log.log(new IllegalArgumentException("Can't display progress bar"));
    }

    @VisibleForTesting
    static MRAIDView createMraid(@NonNull Context context, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull String str, int i, int i2) {
        return new builder(context, str, i, i2).setBaseUrl(unifiedMraidNetworkParams.baseUrl).setPreload(unifiedMraidNetworkParams.preload).setIsTag(unifiedMraidNetworkParams.isTag).setUseLayout(unifiedMraidNetworkParams.useLayout).build();
    }

    @VisibleForTesting
    static MRAIDInterstitial createMraidInterstitial(@NonNull Context context, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedMraidFullscreenListener unifiedMraidFullscreenListener, @NonNull String str, int i, int i2) {
        return MRAIDInterstitial.newBuilder(context, str, i, i2).setBaseUrl(unifiedMraidNetworkParams.baseUrl).setPreload(unifiedMraidNetworkParams.preload).setIsTag(unifiedMraidNetworkParams.isTag).setUseLayout(unifiedMraidNetworkParams.useLayout).setListener(unifiedMraidFullscreenListener).setCloseTime(unifiedMraidNetworkParams.closeTime).setNativeFeatureListener(unifiedMraidFullscreenListener).build();
    }

    static void hideBannerSpinnerView(final View view) {
        if (view instanceof MRAIDView) {
            br.a((Runnable) new Runnable() {
                public void run() {
                    try {
                        int childCount = ((MRAIDView) view).getChildCount();
                        for (int i = 0; i < childCount; i++) {
                            View childAt = ((MRAIDView) view).getChildAt(i);
                            Object tag = childAt.getTag();
                            if (tag != null && tag.equals(UnifiedMraidUtils.MRAID_VIEW_PROGRESS_TAG)) {
                                childAt.setVisibility(8);
                            }
                        }
                    } catch (Exception e) {
                        Log.log(e);
                    }
                }
            });
        }
    }

    @VisibleForTesting
    static boolean isValidAdm(@Nullable String str) {
        return !TextUtils.isEmpty(str) && TextUtils.getTrimmedLength(str) > 0;
    }

    /* access modifiers changed from: private */
    public static <UnifiedAdParamsType extends UnifiedAdParams, UnifiedAdCallbackType extends UnifiedAdCallback> void performLoadMraid(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @Nullable UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype, @NonNull UnifiedMraidAd<UnifiedAdParamsType, UnifiedAdCallbackType> unifiedMraidAd) {
        if (unifiedMraidNetworkParams != null) {
            if (isValidAdm(unifiedMraidNetworkParams.adm)) {
                unifiedMraidAd.loadMraid(context, unifiedadparamstype, unifiedMraidNetworkParams, unifiedadcallbacktype);
                return;
            } else if (!TextUtils.isEmpty(unifiedMraidNetworkParams.adUrl) && TextUtils.getTrimmedLength(unifiedMraidNetworkParams.adUrl) > 0) {
                unifiedMraidAd.performMraidRequest(context, unifiedadparamstype, unifiedMraidNetworkParams, unifiedadcallbacktype, unifiedMraidNetworkParams.adUrl);
                return;
            }
        }
        unifiedadcallbacktype.onAdLoadFailed(LoadingError.IncorrectAdunit);
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x001b  */
    static void removeProgressView(View view) {
        boolean z;
        ViewParent parent = view.getParent();
        while (true) {
            z = parent instanceof ViewGroup;
            if (z && ((ViewGroup) parent).getId() != 16908290) {
                parent = parent.getParent();
            } else if (z) {
                ViewGroup viewGroup = (ViewGroup) parent;
                if (viewGroup.getId() == 16908290) {
                    final View findViewWithTag = viewGroup.findViewWithTag(MRAID_FULLSCREEN_PROGRESS_TAG);
                    if (findViewWithTag != null && (findViewWithTag.getParent() instanceof ViewGroup)) {
                        br.a((Runnable) new Runnable() {
                            public void run() {
                                ((ViewGroup) findViewWithTag.getParent()).removeView(findViewWithTag);
                            }
                        });
                        return;
                    }
                    return;
                }
            }
        }
        if (z) {
        }
        Log.log(new IllegalArgumentException("Can't remove progress bar"));
    }
}
