package com.appodeal.ads.unified.mraid;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.explorestack.iab.mraid.MRAIDInterstitial;
import com.explorestack.iab.mraid.MRAIDInterstitialListener;
import com.explorestack.iab.mraid.activity.MraidActivity.MraidActivityListener;

class UnifiedMraidFullscreenListener<UnifiedCallbackType extends UnifiedFullscreenAdCallback> extends UnifiedMraidListener<UnifiedCallbackType> implements MRAIDInterstitialListener, MraidActivityListener {
    UnifiedMraidFullscreenListener(@NonNull UnifiedCallbackType unifiedcallbacktype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
        super(unifiedcallbacktype, unifiedMraidNetworkParams);
    }

    /* access modifiers changed from: 0000 */
    public void displayProgress(WebView webView) {
        UnifiedMraidUtils.addProgressView(webView);
    }

    /* access modifiers changed from: 0000 */
    public void hideProgress(WebView webView) {
        UnifiedMraidUtils.removeProgressView(webView);
    }

    public void mraidInterstitialHide(MRAIDInterstitial mRAIDInterstitial) {
        ((UnifiedFullscreenAdCallback) this.callback).onAdClosed();
    }

    public void mraidInterstitialLoaded(MRAIDInterstitial mRAIDInterstitial) {
        ((UnifiedFullscreenAdCallback) this.callback).onAdLoaded();
    }

    public void mraidInterstitialNoFill(MRAIDInterstitial mRAIDInterstitial) {
        ((UnifiedFullscreenAdCallback) this.callback).onAdLoadFailed(LoadingError.NoFill);
    }

    public void mraidInterstitialShow(MRAIDInterstitial mRAIDInterstitial) {
        ((UnifiedFullscreenAdCallback) this.callback).onAdShown();
    }

    public void onMraidActivityClose() {
        ((UnifiedFullscreenAdCallback) this.callback).onAdClosed();
    }

    public void onMraidActivityShowFailed() {
        ((UnifiedFullscreenAdCallback) this.callback).onAdShowFailed();
    }
}
