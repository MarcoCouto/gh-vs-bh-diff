package com.appodeal.ads.unified.mraid;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.explorestack.iab.mraid.activity.MraidActivity.MraidType;

public abstract class UnifiedMraidVideo<NetworkRequestParams> extends UnifiedVideo<NetworkRequestParams> implements UnifiedFullscreenMraid<UnifiedVideoParams, UnifiedVideoCallback, NetworkRequestParams> {
    private final UnifiedMraidFullscreenAd<UnifiedVideoParams, UnifiedVideoCallback, NetworkRequestParams> unifiedAd = new UnifiedMraidFullscreenAd<>(this);

    public UnifiedMraidFullscreenListener<UnifiedVideoCallback> createListener(@NonNull Context context, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        return new UnifiedMraidVideoListener(unifiedVideoCallback, unifiedMraidNetworkParams);
    }

    public MraidType getMraidType() {
        return MraidType.Video;
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.unifiedAd.load(activity, unifiedVideoParams, networkrequestparams, unifiedVideoCallback);
    }

    public void loadMraid(@NonNull Context context, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        this.unifiedAd.loadMraid(context, unifiedVideoParams, unifiedMraidNetworkParams, unifiedVideoCallback);
    }

    public void onClicked() {
        super.onClicked();
        this.unifiedAd.onClicked();
    }

    public void onDestroy() {
        this.unifiedAd.onDestroy();
    }

    public void onFinished() {
        super.onFinished();
        this.unifiedAd.onFinished();
    }

    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull NetworkRequestParams networkrequestparams) {
        super.onPrepareToShow(activity, unifiedVideoParams, networkrequestparams);
        this.unifiedAd.onPrepareToShow(activity, unifiedVideoParams, networkrequestparams);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        this.unifiedAd.show(activity, unifiedVideoCallback);
    }
}
