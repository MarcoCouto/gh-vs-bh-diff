package com.appodeal.ads.unified;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.app.AppState;

public interface UnifiedAppStateChangeListener {
    void onAppStateChanged(@Nullable Activity activity, @NonNull AppState appState, boolean z);
}
