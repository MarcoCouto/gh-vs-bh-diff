package com.appodeal.ads.unified.vast;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.appodeal.ads.utils.q;
import com.appodeal.ads.utils.q.a;
import com.explorestack.iab.vast.VastActivityListener;
import com.explorestack.iab.vast.VastClickCallback;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.VastRequestListener;
import com.explorestack.iab.vast.activity.VastActivity;

abstract class UnifiedVastFullscreenListener<UnifiedCallbackType extends UnifiedFullscreenAdCallback> implements VastActivityListener, VastRequestListener {
    @NonNull
    protected final UnifiedCallbackType callback;
    @NonNull
    private final q clickHandler = new q();
    @NonNull
    protected final UnifiedVastNetworkParams vastParams;

    UnifiedVastFullscreenListener(@NonNull UnifiedCallbackType unifiedcallbacktype, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams) {
        this.callback = unifiedcallbacktype;
        this.vastParams = unifiedVastNetworkParams;
    }

    public void onVastClick(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest, @NonNull final VastClickCallback vastClickCallback, @Nullable String str) {
        this.clickHandler.a(vastActivity, str, this.vastParams.packageName, this.vastParams.expiryTime, new a() {
            public void onHandleError() {
                vastClickCallback.clickHandleError();
            }

            public void onHandled() {
                vastClickCallback.clickHandled();
            }

            public void processClick(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
                UnifiedVastFullscreenListener.this.callback.onAdClicked(unifiedAdCallbackClickTrackListener);
            }
        });
    }

    public void onVastComplete(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest) {
    }

    public void onVastDismiss(@NonNull VastActivity vastActivity, @Nullable VastRequest vastRequest, boolean z) {
        if (z) {
            this.callback.onAdFinished();
        }
        this.callback.onAdClosed();
    }

    public void onVastError(@NonNull Context context, @NonNull VastRequest vastRequest, int i) {
        this.callback.printError(null, Integer.valueOf(i));
        this.callback.onAdLoadFailed(null);
    }

    public void onVastLoaded(@NonNull VastRequest vastRequest) {
        this.callback.onAdLoaded();
    }

    public void onVastShown(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest) {
        this.callback.onAdShown();
    }
}
