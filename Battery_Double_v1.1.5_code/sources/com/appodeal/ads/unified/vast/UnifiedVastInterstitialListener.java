package com.appodeal.ads.unified.vast;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.explorestack.iab.vast.VastClickCallback;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.activity.VastActivity;

public class UnifiedVastInterstitialListener extends UnifiedVastFullscreenListener<UnifiedInterstitialCallback> {
    public UnifiedVastInterstitialListener(@NonNull UnifiedInterstitialCallback unifiedInterstitialCallback, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams) {
        super(unifiedInterstitialCallback, unifiedVastNetworkParams);
    }

    public /* bridge */ /* synthetic */ void onVastClick(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest, @NonNull VastClickCallback vastClickCallback, @Nullable String str) {
        super.onVastClick(vastActivity, vastRequest, vastClickCallback, str);
    }

    public /* bridge */ /* synthetic */ void onVastComplete(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest) {
        super.onVastComplete(vastActivity, vastRequest);
    }

    public /* bridge */ /* synthetic */ void onVastDismiss(@NonNull VastActivity vastActivity, @Nullable VastRequest vastRequest, boolean z) {
        super.onVastDismiss(vastActivity, vastRequest, z);
    }

    public /* bridge */ /* synthetic */ void onVastError(@NonNull Context context, @NonNull VastRequest vastRequest, int i) {
        super.onVastError(context, vastRequest, i);
    }

    public /* bridge */ /* synthetic */ void onVastLoaded(@NonNull VastRequest vastRequest) {
        super.onVastLoaded(vastRequest);
    }

    public /* bridge */ /* synthetic */ void onVastShown(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest) {
        super.onVastShown(vastActivity, vastRequest);
    }
}
