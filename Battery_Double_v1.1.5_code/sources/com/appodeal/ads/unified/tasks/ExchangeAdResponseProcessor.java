package com.appodeal.ads.unified.tasks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.b.i;
import com.appodeal.ads.utils.ExchangeAd;
import java.net.URLConnection;

final class ExchangeAdResponseProcessor implements AdResponseProcessor<ExchangeAd> {
    ExchangeAdResponseProcessor() {
    }

    public void processResponse(@NonNull URLConnection uRLConnection, @Nullable String str, @NonNull AdParamsProcessorCallback<ExchangeAd> adParamsProcessorCallback) throws Exception {
        if (TextUtils.isEmpty(str) || TextUtils.getTrimmedLength(str) == 0) {
            adParamsProcessorCallback.onProcessFail(LoadingError.NoFill);
        } else {
            adParamsProcessorCallback.onProcessSuccess(new ExchangeAd(str, uRLConnection.getHeaderFields(), i.a().c()));
        }
    }
}
