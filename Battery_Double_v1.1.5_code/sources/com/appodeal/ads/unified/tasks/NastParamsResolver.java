package com.appodeal.ads.unified.tasks;

import android.support.annotation.NonNull;
import com.appodeal.ads.utils.ExchangeAd;

final class NastParamsResolver implements AdParamsResolver<ExchangeAd, String> {
    NastParamsResolver() {
    }

    public void processResponse(@NonNull ExchangeAd exchangeAd, @NonNull AdParamsResolverCallback<String> adParamsResolverCallback) throws Exception {
        adParamsResolverCallback.onResolveSuccess(exchangeAd.getAdm());
    }
}
