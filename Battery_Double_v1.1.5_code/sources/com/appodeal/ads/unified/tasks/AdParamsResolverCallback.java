package com.appodeal.ads.unified.tasks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;

public interface AdParamsResolverCallback<OutputType> {
    void onResolveFail(@Nullable LoadingError loadingError);

    void onResolveSuccess(@NonNull OutputType outputtype);
}
