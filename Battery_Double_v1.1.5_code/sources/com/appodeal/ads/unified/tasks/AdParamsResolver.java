package com.appodeal.ads.unified.tasks;

import android.support.annotation.NonNull;

public interface AdParamsResolver<InputType, OutputType> {
    void processResponse(@NonNull InputType inputtype, @NonNull AdParamsResolverCallback<OutputType> adParamsResolverCallback) throws Exception;
}
