package com.appodeal.ads.unified.tasks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.net.URLConnection;

public interface AdResponseProcessor<ResponseType> {
    void processResponse(@NonNull URLConnection uRLConnection, @Nullable String str, @NonNull AdParamsProcessorCallback<ResponseType> adParamsProcessorCallback) throws Exception;
}
