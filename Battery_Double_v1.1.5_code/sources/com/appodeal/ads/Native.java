package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.b.d;
import java.util.List;
import org.json.JSONObject;

public final class Native {

    /* renamed from: a reason: collision with root package name */
    static int f1497a = 1;
    static NativeAdType b = NativeAdType.Auto;
    static MediaAssetType c = MediaAssetType.ALL;
    static String d;
    static boolean e = false;
    @Nullable
    @VisibleForTesting
    static az f;
    @VisibleForTesting
    static b g;
    @VisibleForTesting
    static a h;

    public enum MediaAssetType {
        ICON,
        IMAGE,
        ALL
    }

    public enum NativeAdType {
        Auto,
        NoVideo,
        Video
    }

    @VisibleForTesting(otherwise = 3)
    static class a extends p<ba, bb, c> {
        a(q<ba, bb, ?> qVar) {
            super(qVar, null, 512);
        }

        /* access modifiers changed from: protected */
        public int a(bb bbVar, ba baVar, boolean z) {
            if (z) {
                return 1;
            }
            return Native.f1497a;
        }

        /* access modifiers changed from: protected */
        public ba a(@NonNull bb bbVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
            return new ba(bbVar, adNetwork, boVar);
        }

        /* access modifiers changed from: protected */
        public bb a(c cVar) {
            return new bb(cVar);
        }

        public void a(Activity activity) {
            if (r() && l()) {
                bb bbVar = (bb) y();
                if (bbVar == null || bbVar.M()) {
                    d((Context) activity);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("video_native_autostart")) {
                Native.e = jSONObject.optBoolean("video_native_autostart", false);
            }
            if (jSONObject.has("diu")) {
                Native.d = jSONObject.optString("diu");
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(bb bbVar) {
            return super.a(bbVar) && !Native.c().b();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean b(bb bbVar, ba baVar) {
            return true;
        }

        /* access modifiers changed from: protected */
        public void b(Context context) {
            com.appodeal.ads.utils.c.b(context);
        }

        /* access modifiers changed from: protected */
        public void d() {
            Native.c().a();
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            Native.a().b(context, new c());
        }

        /* access modifiers changed from: protected */
        public boolean e() {
            return false;
        }

        /* access modifiers changed from: protected */
        public void f() {
            for (int i = 0; i < x().size() - 3; i++) {
                bb bbVar = (bb) a(i);
                if (bbVar != null && !bbVar.s()) {
                    bbVar.P();
                }
            }
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "native_disabled";
        }

        /* access modifiers changed from: protected */
        public boolean h() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean i() {
            return false;
        }
    }

    @VisibleForTesting
    static class b extends q<ba, bb, ay> {
        b() {
            super(Native.c());
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void b(bb bbVar) {
            Native.a(bbVar, 0, false, false);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void c(bb bbVar, ba baVar) {
            super.c(bbVar, baVar);
            bbVar.d = baVar.y();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void g(bb bbVar, ba baVar, ay ayVar) {
            if (bbVar != null && ayVar != null) {
                bbVar.e.add(Integer.valueOf(ayVar.l()));
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(bb bbVar, ba baVar, boolean z) {
            return true;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void c(bb bbVar) {
            Native.a(bbVar, 0, false, true);
        }

        /* access modifiers changed from: 0000 */
        public boolean b() {
            return false;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public boolean e(bb bbVar, ba baVar) {
            return baVar.isPrecache() || this.f1662a.a(bbVar, baVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public boolean f(bb bbVar, ba baVar, ay ayVar) {
            return bbVar.e.contains(Integer.valueOf(ayVar.l()));
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public void l(@Nullable bb bbVar, ba baVar, ay ayVar) {
            if (bbVar != null && ayVar != null) {
                bbVar.f.add(Integer.valueOf(ayVar.l()));
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public boolean h(bb bbVar, ba baVar) {
            return false;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: d */
        public void j(bb bbVar, ba baVar) {
            if (baVar != null) {
                List x = baVar.x();
                if (x != null) {
                    Native.c().b.removeAll(x);
                }
            }
            if (this.f1662a.r()) {
                Native.c().a();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean h(bb bbVar, ba baVar, ay ayVar) {
            return bbVar.f.contains(Integer.valueOf(ayVar.l()));
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public boolean k(bb bbVar, ba baVar) {
            return bbVar.h();
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public boolean i(bb bbVar, ba baVar, ay ayVar) {
            return !bbVar.e.contains(Integer.valueOf(ayVar.l()));
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public boolean j(bb bbVar, ba baVar, ay ayVar) {
            return !bbVar.g.contains(Integer.valueOf(ayVar.l())) && this.f1662a.D() > 0;
        }

        /* access modifiers changed from: protected */
        /* renamed from: g */
        public d k(bb bbVar, ba baVar, ay ayVar) {
            return ayVar.n();
        }

        /* access modifiers changed from: protected */
        /* renamed from: h */
        public void c(@Nullable bb bbVar, ba baVar, @Nullable ay ayVar) {
            if (bbVar != null && ayVar != null) {
                bbVar.g.add(Integer.valueOf(ayVar.l()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: i */
        public boolean b(bb bbVar, ba baVar, ay ayVar) {
            return bbVar.g.contains(Integer.valueOf(ayVar.l()));
        }
    }

    static class c extends n<c> {
        c() {
            super("native", "debug_native");
        }
    }

    static p<ba, bb, c> a() {
        if (h == null) {
            h = new a(b());
        }
        return h;
    }

    static void a(bb bbVar, int i, boolean z, boolean z2) {
        a().a(bbVar, i, z2, z);
    }

    static q<ba, bb, ay> b() {
        if (g == null) {
            g = new b();
        }
        return g;
    }

    @NonNull
    static az c() {
        if (f == null) {
            f = new az();
        }
        return f;
    }
}
