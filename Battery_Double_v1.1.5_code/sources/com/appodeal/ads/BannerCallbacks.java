package com.appodeal.ads;

public interface BannerCallbacks {
    void onBannerClicked();

    void onBannerExpired();

    void onBannerFailedToLoad();

    void onBannerLoaded(int i, boolean z);

    void onBannerShowFailed();

    void onBannerShown();
}
