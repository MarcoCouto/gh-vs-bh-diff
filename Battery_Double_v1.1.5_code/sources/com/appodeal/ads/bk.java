package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.b.e;
import org.json.JSONObject;

final class bk {

    /* renamed from: a reason: collision with root package name */
    static final bl f1604a = new bl();
    static int b = 90000;
    @VisibleForTesting
    static c c;
    @VisibleForTesting
    static b d;
    private static bt<bj, bi> e;

    static class a extends n<a> {
        a() {
            super("rewarded_video", "debug_rewarded_video");
        }
    }

    @VisibleForTesting
    static class b extends p<bi, bj, a> {
        b(q<bi, bj, ?> qVar) {
            super(qVar, e.c(), 128);
        }

        /* access modifiers changed from: protected */
        public bi a(@NonNull bj bjVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
            return new bi(bjVar, adNetwork, boVar);
        }

        /* access modifiers changed from: protected */
        public bj a(a aVar) {
            return new bj(aVar);
        }

        public void a(Activity activity) {
            if (r() && l()) {
                bj bjVar = (bj) y();
                if (bjVar == null || bjVar.M()) {
                    d((Context) activity);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("max_duration")) {
                bk.b = jSONObject.optInt("max_duration", 0);
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(AdNetwork adNetwork, JSONObject jSONObject, String str, boolean z) {
            if (!z && adNetwork != null && adNetwork.isRewardedShowing() && x().size() > 1) {
                bj bjVar = (bj) A();
                bj bjVar2 = (bj) z();
                if (!(bjVar == null || bjVar2 == null || bjVar2.B() == null)) {
                    if (str.equals(((bi) bjVar2.B()).getId())) {
                        bjVar.b(jSONObject);
                    }
                    bk.a(bjVar, 0, false, false);
                    return true;
                }
            }
            return super.a(adNetwork, jSONObject, str, z);
        }

        /* access modifiers changed from: protected */
        public boolean a(bj bjVar, int i) {
            if (bjVar.y() != 1 || bjVar.v() == null || bjVar.v() != bjVar.a(i)) {
                return super.a(bjVar, i);
            }
            String optString = bjVar.v().optString("status");
            boolean z = false;
            if (TextUtils.isEmpty(optString)) {
                return false;
            }
            AdNetwork c = q().c(optString);
            if (c != null && c.isRewardedShowing()) {
                z = true;
            }
            return z;
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            bk.a(context, new a());
        }

        /* access modifiers changed from: protected */
        public String g() {
            return null;
        }
    }

    @VisibleForTesting
    static class c extends bd<bi, bj> {
        c() {
            super(bk.f1604a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void b(bj bjVar) {
            bk.a(bjVar, 0, false, false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void q(bj bjVar, bi biVar) {
            biVar.b().setRewardedShowing(true);
            if (!bjVar.a() && this.f1662a.r()) {
                bj bjVar2 = (bj) this.f1662a.y();
                if (bjVar2 == null || bjVar2.M()) {
                    this.f1662a.d(Appodeal.f);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(@Nullable bj bjVar, @Nullable bi biVar, @Nullable LoadingError loadingError) {
            super.a(bjVar, biVar, loadingError);
            am.a();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(bj bjVar, bi biVar, boolean z) {
            super.b(bjVar, biVar, z);
            if (!biVar.h() && e(bjVar, biVar)) {
                bk.a(Appodeal.e, new l(this.f1662a.t()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(bj bjVar, bi biVar, Object obj) {
            return false;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void c(bj bjVar) {
            bk.a(bjVar, 0, false, true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void n(bj bjVar, bi biVar) {
            bk.f().b();
            am.a();
            this.f1662a.d(null);
            biVar.b().setRewardedShowing(false);
            f(bjVar);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public void j(bj bjVar, bi biVar) {
            if (this.f1662a.r()) {
                this.f1662a.d(Appodeal.f);
            }
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public boolean a(bj bjVar) {
            return bjVar.v() == null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean e(bj bjVar, bi biVar) {
            return (!e(bjVar, biVar) && (bjVar.a(0) == bjVar.v() || biVar.isPrecache() || biVar.h() || this.f1662a.a(bjVar, biVar))) && super.e(bjVar, biVar);
        }

        /* access modifiers changed from: protected */
        public boolean e(bj bjVar, bi biVar) {
            return bjVar.u();
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public boolean l(bj bjVar, bi biVar) {
            return bjVar.v() == null || (biVar != null && bjVar.v().optString("id").equals(biVar.getId()));
        }

        /* access modifiers changed from: protected */
        /* renamed from: g */
        public void c(bj bjVar, bi biVar) {
            super.c(bjVar, biVar);
            if (bjVar.v() == biVar.getJsonData()) {
                bjVar.b((JSONObject) null);
            }
        }
    }

    static p<bi, bj, a> a() {
        if (d == null) {
            d = new b(b());
        }
        return d;
    }

    static void a(Context context, a aVar) {
        a().b(context, aVar);
    }

    static void a(bj bjVar, int i, boolean z, boolean z2) {
        a().a(bjVar, i, z2, z);
    }

    static boolean a(Activity activity, l lVar) {
        return f().a(activity, lVar, a());
    }

    static q<bi, bj, Object> b() {
        if (c == null) {
            c = new c();
        }
        return c;
    }

    static double c() {
        return a().t().h();
    }

    static String d() {
        return a().t().g();
    }

    /* access modifiers changed from: private */
    public static bt<bj, bi> f() {
        if (e == null) {
            e = new bt<>("debug_rewarded_video");
        }
        return e;
    }
}
