package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.i;
import com.appodeal.ads.m;

abstract class bd<AdObjectType extends i, AdRequestType extends m<AdObjectType>> extends q<AdObjectType, AdRequestType, Object> {
    bd(@NonNull c<AdRequestType, AdObjectType, Object> cVar) {
        super(cVar);
    }

    /* access modifiers changed from: protected */
    public void c(AdRequestType adrequesttype, AdObjectType adobjecttype, @Nullable Object obj) {
        r(adrequesttype, adobjecttype);
    }

    /* access modifiers changed from: protected */
    public void g(AdRequestType adrequesttype, AdObjectType adobjecttype, Object obj) {
        q(adrequesttype, adobjecttype);
    }

    /* access modifiers changed from: 0000 */
    public void l(@Nullable AdRequestType adrequesttype, AdObjectType adobjecttype, Object obj) {
        s(adrequesttype, adobjecttype);
    }

    /* access modifiers changed from: 0000 */
    public abstract void q(AdRequestType adrequesttype, AdObjectType adobjecttype);

    /* access modifiers changed from: protected */
    public void r(AdRequestType adrequesttype, AdObjectType adobjecttype) {
    }

    /* access modifiers changed from: protected */
    public void s(@Nullable AdRequestType adrequesttype, AdObjectType adobjecttype) {
    }
}
