package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;

class bl extends c<bj, bi, Object> {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private RewardedVideoCallbacks f1605a;
    @Nullable
    private NonSkippableVideoCallbacks b;

    bl() {
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable NonSkippableVideoCallbacks nonSkippableVideoCallbacks) {
        this.b = nonSkippableVideoCallbacks;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable RewardedVideoCallbacks rewardedVideoCallbacks) {
        this.f1605a = rewardedVideoCallbacks;
    }

    public void a(@NonNull bj bjVar, @NonNull bi biVar) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_LOADED, String.format("isPrecache: %s", new Object[]{Boolean.valueOf(biVar.isPrecache())}), LogLevel.verbose);
        Appodeal.b();
        if (this.f1605a != null) {
            this.f1605a.onRewardedVideoLoaded(biVar.isPrecache());
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoLoaded(biVar.isPrecache());
        }
    }

    public void a(@Nullable bj bjVar, @Nullable bi biVar, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_LOAD_FAILED, LogLevel.verbose);
        if (this.f1605a != null) {
            this.f1605a.onRewardedVideoFailedToLoad();
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoFailedToLoad();
        }
    }

    public void a(@Nullable bj bjVar, @Nullable bi biVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_SHOWN, LogLevel.verbose);
        if (this.f1605a != null) {
            this.f1605a.onRewardedVideoShown();
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoShown();
        }
    }

    public void a(@Nullable bj bjVar, @Nullable bi biVar, @Nullable Object obj, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_SHOW_FAILED, LogLevel.verbose);
        if (this.f1605a != null) {
            this.f1605a.onRewardedVideoShowFailed();
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoShowFailed();
        }
    }

    /* renamed from: b */
    public void c(@NonNull bj bjVar, @NonNull bi biVar) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_CLOSED, String.format("finished: %s", new Object[]{Boolean.valueOf(bjVar.n())}), LogLevel.verbose);
        if (this.f1605a != null) {
            this.f1605a.onRewardedVideoClosed(bjVar.n());
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoClosed(bjVar.n());
        }
    }

    public void b(@NonNull bj bjVar, @NonNull bi biVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_CLICKED, LogLevel.verbose);
        if (this.f1605a != null) {
            this.f1605a.onRewardedVideoClicked();
        }
    }

    /* renamed from: c */
    public void b(@NonNull bj bjVar, @NonNull bi biVar) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_EXPIRED, LogLevel.verbose);
        if (this.f1605a != null) {
            this.f1605a.onRewardedVideoExpired();
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoExpired();
        }
    }

    public void c(@NonNull bj bjVar, @NonNull bi biVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_FINISHED, LogLevel.verbose);
        if (this.f1605a != null) {
            this.f1605a.onRewardedVideoFinished(bk.c(), bk.d());
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoFinished();
        }
    }
}
