package com.appodeal.ads;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import com.appodeal.ads.unified.UnifiedAppStateChangeListener;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import java.util.List;

public abstract class AdNetwork<NetworkRequestParams> {
    private final ActivityRule[] adActivityRules;
    private final String adapterVersion;
    private boolean isInterstitialShowing = false;
    private final boolean isOptional;
    private boolean isRewardedShowing = false;
    private boolean isVideoShowing = false;
    private final String name;
    private final String[] requiredProvidersClassName;
    private final String[] requiredReceiverClassName;
    private final List<Pair<String, Pair<String, String>>> requiredServiceWithData;

    public AdNetwork(AdNetworkBuilder adNetworkBuilder) {
        this.name = adNetworkBuilder.getName();
        this.adapterVersion = adNetworkBuilder.getAdapterVersion();
        this.isOptional = adNetworkBuilder.isOptional();
        this.adActivityRules = adNetworkBuilder.getAdActivityRules();
        this.requiredReceiverClassName = adNetworkBuilder.getRequiredReceiverClassName();
        this.requiredServiceWithData = adNetworkBuilder.getRequiredServiceWithData();
        this.requiredProvidersClassName = adNetworkBuilder.getRequiredProvidersClassName();
    }

    public boolean canLoadInterstitialWhenDisplaying() {
        return true;
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return true;
    }

    public boolean canLoadVideoWhenDisplaying() {
        return true;
    }

    @Nullable
    public UnifiedBanner<NetworkRequestParams> createBanner() {
        return null;
    }

    @Nullable
    public UnifiedInterstitial<NetworkRequestParams> createInterstitial() {
        return null;
    }

    @Nullable
    public UnifiedMrec<NetworkRequestParams> createMrec() {
        return null;
    }

    @Nullable
    public UnifiedNative<NetworkRequestParams> createNativeAd() {
        return null;
    }

    @Nullable
    public UnifiedRewarded<NetworkRequestParams> createRewarded() {
        return null;
    }

    @Nullable
    public UnifiedVideo<NetworkRequestParams> createVideo() {
        return null;
    }

    public final ActivityRule[] getAdActivityRules() {
        return this.adActivityRules;
    }

    public final String getAdapterVersion() {
        return this.adapterVersion;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public UnifiedAppStateChangeListener getAppStateChangeListener() {
        return null;
    }

    public final String getName() {
        return this.name;
    }

    public final String[] getRequiredProvidersClassName() {
        return this.requiredProvidersClassName;
    }

    public final String[] getRequiredReceiverClassName() {
        return this.requiredReceiverClassName;
    }

    public final List<Pair<String, Pair<String, String>>> getRequiredServiceWithData() {
        return this.requiredServiceWithData;
    }

    public abstract String getVersion();

    /* access modifiers changed from: protected */
    public abstract void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<NetworkRequestParams> networkInitializationListener) throws Exception;

    /* access modifiers changed from: protected */
    public final boolean isInterstitialShowing() {
        return this.isInterstitialShowing;
    }

    public final boolean isOptional() {
        return this.isOptional;
    }

    public boolean isPermissionRequired(@NonNull String str, AdType adType) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isRewardedShowing() {
        return this.isRewardedShowing;
    }

    public boolean isSupportSmartBanners() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isVideoShowing() {
        return this.isVideoShowing;
    }

    /* access modifiers changed from: 0000 */
    public final void setInterstitialShowing(boolean z) {
        if (!canLoadInterstitialWhenDisplaying()) {
            this.isInterstitialShowing = z;
        }
    }

    public void setLogging(boolean z) {
    }

    /* access modifiers changed from: 0000 */
    public final void setRewardedShowing(boolean z) {
        if (!canLoadRewardedWhenDisplaying()) {
            this.isRewardedShowing = z;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void setVideoShowing(boolean z) {
        if (!canLoadVideoWhenDisplaying()) {
            this.isVideoShowing = z;
        }
    }

    public LoadingError verifyLoadAvailability(@NonNull AdType adType) {
        return null;
    }

    public boolean worksInM() {
        return true;
    }
}
