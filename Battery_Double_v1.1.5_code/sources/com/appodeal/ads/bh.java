package com.appodeal.ads;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.utils.Log;
import java.util.concurrent.CountDownLatch;

class bh implements RestrictedData {

    /* renamed from: a reason: collision with root package name */
    static final bh f1600a = new bh();
    @VisibleForTesting
    static String b = null;

    private bh() {
    }

    @Nullable
    private static String a(@Nullable final Context context) {
        if (context == null) {
            return null;
        }
        if (b != null) {
            return b;
        }
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        br.a((Runnable) new Runnable() {
            public void run() {
                String userAgentString;
                try {
                    if (VERSION.SDK_INT < 17) {
                        userAgentString = new WebView(context).getSettings().getUserAgentString();
                    } else {
                        bh.b = WebSettings.getDefaultUserAgent(context);
                        countDownLatch.countDown();
                    }
                } catch (IllegalArgumentException e) {
                    Log.log(e);
                    userAgentString = new WebView(context).getSettings().getUserAgentString();
                } catch (Throwable th) {
                    countDownLatch.countDown();
                    throw th;
                }
                bh.b = userAgentString;
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            Log.log(e);
        }
        return b;
    }

    public boolean canSendLocation() {
        return !isUserAgeRestricted() && !isParameterBlocked("lat") && !isParameterBlocked("lon");
    }

    public boolean canSendLocationType() {
        return !isUserAgeRestricted() && !isParameterBlocked("lt");
    }

    public boolean canSendUserSettings() {
        return !isUserAgeRestricted() && !isParameterBlocked("user_settings");
    }

    @Nullable
    public String getAddress() {
        if (canSendUserSettings()) {
            return bq.a().getAddress();
        }
        return null;
    }

    @Nullable
    public Integer getAge() {
        if (canSendUserSettings()) {
            return bq.a().getAge();
        }
        return null;
    }

    @Nullable
    public String getCity() {
        if (canSendUserSettings()) {
            return bq.a().getCity();
        }
        return null;
    }

    @Nullable
    public ConnectionData getConnectionData(@NonNull Context context) {
        return br.b(context);
    }

    @Nullable
    public String getCountry() {
        if (canSendUserSettings()) {
            return bq.a().getCountryId();
        }
        return null;
    }

    @Nullable
    public Gender getGender() {
        if (canSendUserSettings()) {
            return bq.a().getGender();
        }
        return null;
    }

    @Nullable
    public String getHttpAgent(@NonNull Context context) {
        if (canSendUserSettings()) {
            return a(context);
        }
        return null;
    }

    @NonNull
    public String getIfa() {
        String h = bf.h();
        return h != null ? h : "00000000-0000-0000-0000-000000000000";
    }

    @Nullable
    public String getIp() {
        if (canSendUserSettings()) {
            return bq.a().getIp();
        }
        return null;
    }

    @Nullable
    public String getIpv6() {
        if (canSendUserSettings()) {
            return bq.a().getIpv6();
        }
        return null;
    }

    @NonNull
    public LocationData getLocation(@NonNull Context context) {
        return new as(context, this);
    }

    @Nullable
    public String getUserId() {
        return bq.a().getUserId();
    }

    @Nullable
    public String getZip() {
        if (canSendUserSettings()) {
            return bq.a().getZip();
        }
        return null;
    }

    public boolean isLimitAdTrackingEnabled() {
        return bf.f();
    }

    public boolean isParameterBlocked(String str) {
        return isUserGdprProtected() && bf.b(str);
    }

    public boolean isUserAgeRestricted() {
        return w.b();
    }

    public boolean isUserGdprProtected() {
        return bf.g();
    }

    public boolean isUserHasConsent() {
        return bf.c();
    }

    public boolean isUserInGdprScope() {
        return bf.d();
    }
}
