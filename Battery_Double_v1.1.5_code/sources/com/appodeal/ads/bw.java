package com.appodeal.ads;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.appodeal.ads.b.d;
import com.appodeal.ads.bv;
import com.appodeal.ads.by;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.ab;
import java.lang.ref.WeakReference;

abstract class bw<AdRequestType extends by<AdObjectType>, AdObjectType extends bv> extends k<AdRequestType, AdObjectType, bx> {
    /* access modifiers changed from: private */
    @Nullable
    public View b;
    @Nullable
    private View c;
    private int d = -1;
    /* access modifiers changed from: private */
    @Nullable
    public FrameLayout e;
    @NonNull
    private b f;
    private u g = u.NEVER_SHOWN;
    /* access modifiers changed from: private */
    @Nullable
    public WeakReference<Animator> h;
    private boolean i = true;

    private interface a {
        void a(View view);
    }

    private class b extends AnimatorListenerAdapter {
        private final AdRequestType b;
        private AdObjectType c;
        private p<AdObjectType, AdRequestType, ?> d;
        private View e;
        private View f;
        private boolean g;
        private boolean h;

        b(AdRequestType adrequesttype, AdObjectType adobjecttype, p<AdObjectType, AdRequestType, ?> pVar, View view, View view2, boolean z, boolean z2) {
            this.b = adrequesttype;
            this.c = adobjecttype;
            this.d = pVar;
            this.e = view;
            this.f = view2;
            this.g = z;
            this.h = z2;
        }

        private void a(Animator animator) {
            animator.removeAllListeners();
            if (this.e != null) {
                if (this.e.getAnimation() != null) {
                    this.e.getAnimation().setAnimationListener(null);
                }
                this.e.clearAnimation();
                this.e.animate().setListener(null);
            }
            bw.this.h = null;
        }

        public void onAnimationCancel(Animator animator) {
            a(animator);
            try {
                bw.this.a(this.e, this.g, this.h);
            } catch (Exception e2) {
                Log.log(e2);
            }
        }

        public void onAnimationEnd(Animator animator) {
            a(animator);
            bw.this.a(this.b, this.c, this.d, this.f);
            if (!this.f.equals(this.e)) {
                try {
                    bw.this.a(this.e, this.g, this.h);
                } catch (Exception e2) {
                    Log.log(e2);
                }
            }
        }

        public void onAnimationStart(Animator animator) {
            bw.this.h = new WeakReference(animator);
        }
    }

    bw(@NonNull String str, @NonNull b bVar) {
        super(str);
        this.f = bVar;
    }

    private FrameLayout a(@NonNull Activity activity, @NonNull AdObjectType adobjecttype) {
        FrameLayout frameLayout = new FrameLayout(activity);
        frameLayout.setLayoutParams(new LayoutParams(-1, adobjecttype.c(activity)));
        frameLayout.setTag("Appodeal");
        return frameLayout;
    }

    private void a(@NonNull Activity activity, @NonNull AdObjectType adobjecttype, @NonNull FrameLayout frameLayout, int i2) {
        final Activity activity2 = activity;
        final int i3 = i2;
        final AdObjectType adobjecttype2 = adobjecttype;
        final FrameLayout frameLayout2 = frameLayout;
        AnonymousClass4 r0 = new Runnable() {
            public void run() {
                if (br.e(activity2)) {
                    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                    layoutParams.type = 1000;
                    layoutParams.gravity = i3;
                    layoutParams.x = 0;
                    if ((i3 & 48) != 48 || VERSION.SDK_INT > 21) {
                        layoutParams.y = 0;
                    } else {
                        layoutParams.y = br.d(activity2);
                    }
                    layoutParams.height = adobjecttype2.c(activity2);
                    layoutParams.width = -1;
                    layoutParams.flags = 8519688;
                    if (VERSION.SDK_INT > 21) {
                        layoutParams.flags |= 1073741824;
                    }
                    layoutParams.format = -3;
                    layoutParams.windowAnimations = 0;
                    layoutParams.token = activity2.getWindow().getDecorView().getWindowToken();
                    activity2.getWindowManager().addView(frameLayout2, layoutParams);
                    bw.d(frameLayout2);
                    bw.this.e = frameLayout2;
                    return;
                }
                br.a((Runnable) this, 100);
            }
        };
        br.a((Runnable) r0);
    }

    private static void a(View view, a aVar) {
        if (view instanceof WebView) {
            aVar.a(view);
            return;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                a(viewGroup.getChildAt(i2), aVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(@Nullable View view, boolean z, boolean z2) {
        if (view != null) {
            ab.a(view);
            ViewGroup viewGroup = null;
            if (view.getParent() != null && (view.getParent() instanceof ViewGroup)) {
                viewGroup = (ViewGroup) view.getParent();
            }
            if (viewGroup != null) {
                if ((viewGroup instanceof BannerView) && z) {
                    viewGroup.setVisibility(8);
                }
                if ((viewGroup instanceof MrecView) && z) {
                    viewGroup.setVisibility(8);
                }
                viewGroup.removeView(view);
            }
            if (viewGroup != null && viewGroup.getTag() != null && viewGroup.getTag().equals("Appodeal") && z2) {
                ViewParent parent = viewGroup.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(viewGroup);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(@NonNull final AdRequestType adrequesttype, @NonNull final AdObjectType adobjecttype, @NonNull final p<AdObjectType, AdRequestType, ?> pVar, @NonNull View view) {
        ab.a(adobjecttype, view, (long) pVar.D(), new com.appodeal.ads.utils.ab.b() {
            public void a() {
                pVar.a().p(adrequesttype, adobjecttype);
            }

            public void b() {
                pVar.a().o(adrequesttype, adobjecttype);
            }
        });
    }

    private boolean a(@NonNull Activity activity, @NonNull View view, @NonNull b bVar, @NonNull b bVar2) {
        boolean z = bVar == bVar2;
        if (view.equals(this.b) && view.getParent() != null) {
            if (bVar == b.VIEW) {
                ViewGroup b2 = b(activity);
                if (z && view.getContext().equals(activity) && view.getParent() != null && view.getParent().equals(b2)) {
                    return true;
                }
            } else if (z && view.getContext().equals(activity)) {
                return true;
            }
            a(view, true, true);
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d6, code lost:
        if (com.appodeal.ads.aa.h != false) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00d8, code lost:
        a(r0, r11, r13, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00ea, code lost:
        if (com.appodeal.ads.aa.h != false) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00ff, code lost:
        r0.addContentView(r13, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0102, code lost:
        r13.addView(r8, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0105, code lost:
        r10.b = r8;
        r10.f = r1;
        r5.d(r3);
        r4 = false;
        r8.setVisibility(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0114, code lost:
        if (android.os.Build.VERSION.SDK_INT <= 15) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0116, code lost:
        if (r1 != r2) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0118, code lost:
        if (r6 == null) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x011e, code lost:
        if (r8.equals(r6) != false) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0122, code lost:
        if (r10.i == false) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0124, code lost:
        r6.bringToFront();
        r0 = r6.animate().alpha(0.0f).setDuration(800).withLayer();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x013e, code lost:
        if (r1 == com.appodeal.ads.b.VIEW) goto L_0x0142;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0140, code lost:
        r9 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0142, code lost:
        r9 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0143, code lost:
        r1 = new com.appodeal.ads.bw.b(r17, r19, r20, r23, r6, r8, r9, r14);
        r0.setListener(r1).start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x015a, code lost:
        a(r3, r11, r5, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0161, code lost:
        if (r8.equals(r6) != false) goto L_0x0170;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0165, code lost:
        if (r1 == com.appodeal.ads.b.VIEW) goto L_0x0168;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0167, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0168, code lost:
        a(r6, r4, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x016c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x016d, code lost:
        com.appodeal.ads.utils.Log.log(r0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00ed  */
    public boolean a(@NonNull Activity activity, @NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype, @NonNull b bVar, @NonNull b bVar2, @NonNull p<AdObjectType, AdRequestType, ?> pVar, boolean z) {
        FrameLayout frameLayout;
        boolean z2;
        int i2;
        FrameLayout frameLayout2;
        Activity activity2 = activity;
        AdRequestType adrequesttype2 = adrequesttype;
        AdObjectType adobjecttype2 = adobjecttype;
        b bVar3 = bVar;
        b bVar4 = bVar2;
        p<AdObjectType, AdRequestType, ?> pVar2 = pVar;
        if (this.b != null || !z) {
            View view = this.b;
            View v = (view == null || !z) ? adobjecttype.v() : view;
            if (v != null) {
                adobjecttype2.a(activity2);
                boolean a2 = a(activity2, v, bVar3, bVar4);
                int b2 = adobjecttype2.b(activity2);
                int c2 = adobjecttype2.c(activity2);
                if (view != null && (view.getParent() instanceof FrameLayout)) {
                    FrameLayout frameLayout3 = (FrameLayout) view.getParent();
                    if ("Appodeal".equals(frameLayout3.getTag())) {
                        frameLayout = frameLayout3;
                        if (bVar3 != b.VIEW) {
                            if (frameLayout != null) {
                                frameLayout2 = null;
                            }
                            z2 = false;
                            LayoutParams layoutParams = new LayoutParams(b2, c2);
                            switch (bVar) {
                                case VIEW:
                                    break;
                                case TOP:
                                    break;
                            }
                        } else {
                            FrameLayout frameLayout4 = this.e;
                            if (bVar3 != bVar4) {
                                a(activity);
                            }
                            if (frameLayout4 != null && frameLayout4.getWindowToken() != null) {
                                frameLayout = frameLayout4;
                                z2 = false;
                                LayoutParams layoutParams2 = new LayoutParams(b2, c2);
                                switch (bVar) {
                                    case VIEW:
                                        break;
                                    case TOP:
                                        break;
                                }
                            } else {
                                if (frameLayout != null && (frameLayout.getParent() == null || frameLayout.getHeight() != c2 || frameLayout.getLayoutParams().width != b2 || !((View) frameLayout.getParent()).getContext().equals(activity2) || bVar3 != bVar4 || !v.getContext().equals(activity2))) {
                                    frameLayout = null;
                                }
                                if (frameLayout == null) {
                                    frameLayout2 = a(activity2, adobjecttype2);
                                }
                                z2 = false;
                                LayoutParams layoutParams22 = new LayoutParams(b2, c2);
                                switch (bVar) {
                                    case VIEW:
                                        layoutParams22.gravity = 1;
                                        ViewGroup b3 = b(activity);
                                        if (b3 != null) {
                                            if (!a2) {
                                                b3.addView(v, layoutParams22);
                                            }
                                            b3.setVisibility(0);
                                            break;
                                        }
                                        break;
                                    case TOP:
                                        i2 = 49;
                                        layoutParams22.gravity = 49;
                                        if (!a2) {
                                            if (frameLayout.getParent() == null) {
                                                break;
                                            }
                                        }
                                        break;
                                    default:
                                        i2 = 81;
                                        layoutParams22.gravity = 81;
                                        if (!a2) {
                                            if (frameLayout.getParent() == null) {
                                                break;
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                        z2 = true;
                        LayoutParams layoutParams222 = new LayoutParams(b2, c2);
                        switch (bVar) {
                            case VIEW:
                                break;
                            case TOP:
                                break;
                        }
                    }
                }
                frameLayout = null;
                if (bVar3 != b.VIEW) {
                }
                z2 = true;
                LayoutParams layoutParams2222 = new LayoutParams(b2, c2);
                switch (bVar) {
                    case VIEW:
                        break;
                    case TOP:
                        break;
                }
            } else {
                pVar.a().a(adrequesttype2, adobjecttype2, LoadingError.ShowFailed);
                return false;
            }
        } else {
            adobjecttype2.a(LoadingError.ShowFailed);
            return false;
        }
        adobjecttype.k();
        return true;
    }

    private boolean a(@NonNull Activity activity, @NonNull p<AdObjectType, AdRequestType, ?> pVar, @NonNull AdRequestType adrequesttype, @NonNull b bVar, @NonNull b bVar2) {
        adrequesttype.i(true);
        adrequesttype.a(bVar);
        final by byVar = (by) pVar.B();
        if (!(byVar == null || this.b == null)) {
            final bv bvVar = (bv) byVar.B();
            if (bvVar != null && byVar.q() && !byVar.s()) {
                ViewGroup b2 = b(activity);
                if (bVar == b.VIEW && b2 == null) {
                    pVar.a(LogConstants.EVENT_SHOW_FAILED, LogConstants.MSG_VIEW_NOT_FOUND);
                    return false;
                }
                final Activity activity2 = activity;
                final b bVar3 = bVar;
                final b bVar4 = bVar2;
                final p<AdObjectType, AdRequestType, ?> pVar2 = pVar;
                AnonymousClass2 r1 = new Runnable() {
                    public void run() {
                        bw.this.a(activity2, byVar, bvVar, bVar3, bVar4, pVar2, true);
                    }
                };
                activity.runOnUiThread(r1);
            }
        }
        return true;
    }

    private ViewGroup b(@NonNull Activity activity) {
        View findViewById = activity.findViewById(this.d);
        if (findViewById == null) {
            findViewById = this.c;
        }
        if (findViewById == null || a(findViewById)) {
            return (ViewGroup) findViewById;
        }
        throw new IllegalArgumentException("Only BannerView.class and MrecView.class are supported as target container for position type == AdDisplayPosition.VIEW");
    }

    /* access modifiers changed from: private */
    public static void d(View view) {
        a(view, (a) new a() {
            public void a(View view) {
                view.setFocusable(false);
                view.clearAnimation();
                view.setAnimation(null);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        this.d = i2;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull Activity activity) {
        FrameLayout frameLayout = this.e;
        if (frameLayout != null) {
            if (frameLayout.getWindowToken() != null) {
                activity.getWindowManager().removeViewImmediate(frameLayout);
            }
            this.e = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(@NonNull Activity activity, @NonNull b bVar);

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.i = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull final Activity activity, @NonNull final p<AdObjectType, AdRequestType, ?> pVar) {
        if (this.b == null) {
            return false;
        }
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    View a2 = bw.this.b;
                    if (a2 != null) {
                        by byVar = (by) pVar.B();
                        if (!(byVar == null || byVar.B() == null)) {
                            ((bv) byVar.B()).l();
                        }
                        a2.setVisibility(8);
                        WeakReference b2 = bw.this.h;
                        if (!(b2 == null || b2.get() == null)) {
                            ((Animator) b2.get()).cancel();
                        }
                        bw.this.a(a2, true, true);
                        bw.this.a(activity);
                    }
                } catch (Exception e) {
                    Log.log(e);
                }
            }
        });
        this.g = u.HIDDEN;
        return true;
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean a(View view);

    /* access modifiers changed from: 0000 */
    @NonNull
    public b b() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable View view) {
        this.c = view;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ee, code lost:
        if (r19.r() != false) goto L_0x0066;
     */
    public boolean b(@NonNull Activity activity, @NonNull bx bxVar, @NonNull p<AdObjectType, AdRequestType, ?> pVar) {
        Activity activity2 = activity;
        bx bxVar2 = bxVar;
        p<AdObjectType, AdRequestType, ?> pVar2 = pVar;
        if (bxVar2.d && this.g == u.HIDDEN) {
            return false;
        }
        b bVar = this.f;
        b bVar2 = bxVar2.c;
        this.f = bVar2;
        d dVar = bxVar2.f1648a;
        boolean z = bxVar2.b;
        final by byVar = (by) pVar.y();
        if (byVar == null) {
            pVar2.a(LogConstants.EVENT_SHOW, String.format("isDebug: %s, isLoaded: %s, isLoading: %s, placement: '%s'", new Object[]{Boolean.valueOf(bxVar2.b), Boolean.valueOf(false), Boolean.valueOf(false), dVar.n()}));
            if (!dVar.a((Context) activity2, pVar.m(), (m) null) || z || !pVar.r()) {
                return false;
            }
        } else {
            pVar2.a(LogConstants.EVENT_SHOW, String.format("isDebug: %s, isLoaded: %s, isLoading: %s, placement: '%s', inPopup: %s", new Object[]{Boolean.valueOf(bxVar2.b), Boolean.valueOf(byVar.h()), Boolean.valueOf(byVar.J()), dVar.n(), Boolean.valueOf(aa.h)}));
            if (!dVar.a((Context) activity2, pVar.m(), (m) byVar)) {
                return false;
            }
            if (byVar.h() || byVar.i() || byVar.d(dVar.n())) {
                final bv bvVar = (bv) byVar.c(dVar.n());
                if (bvVar != null) {
                    ViewGroup b2 = b(activity);
                    if (bVar2 == b.VIEW && b2 == null) {
                        pVar2.a(LogConstants.EVENT_SHOW_FAILED, LogConstants.MSG_VIEW_NOT_FOUND);
                        return false;
                    }
                    final Activity activity3 = activity;
                    final b bVar3 = bVar2;
                    final b bVar4 = bVar;
                    final p<AdObjectType, AdRequestType, ?> pVar3 = pVar;
                    AnonymousClass1 r0 = new Runnable() {
                        public void run() {
                            bw.this.a(activity3, byVar, bvVar, bVar3, bVar4, pVar3, false);
                        }
                    };
                    activity2.runOnUiThread(r0);
                    this.g = u.VISIBLE;
                    return true;
                }
            } else if (byVar.J() || (byVar.q() && !pVar.r())) {
                if (!a(activity, pVar, (AdRequestType) byVar, bVar2, bVar)) {
                    return false;
                }
                this.g = u.VISIBLE;
                return true;
            } else {
                a(activity, pVar, (AdRequestType) byVar, bVar2, bVar);
                if (!z) {
                }
            }
            return false;
        }
        a(activity2, bVar2);
        this.g = u.VISIBLE;
        return true;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public View c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public u d() {
        return this.g;
    }
}
