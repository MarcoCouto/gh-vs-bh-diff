package com.appodeal.ads;

import android.support.annotation.NonNull;
import com.appodeal.ads.i;

abstract class o<AdObjectType extends i> {

    /* renamed from: a reason: collision with root package name */
    private AdObjectType f1653a;

    o() {
    }

    public void a() {
        this.f1653a = null;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull m<AdObjectType> mVar, @NonNull AdObjectType adobjecttype) {
        if (adobjecttype.h()) {
            return true;
        }
        if (this.f1653a == null || this.f1653a.getEcpm() < adobjecttype.getEcpm()) {
            this.f1653a = adobjecttype;
        }
        return !mVar.k(adobjecttype);
    }

    /* access modifiers changed from: 0000 */
    public AdObjectType b() {
        return this.f1653a;
    }
}
