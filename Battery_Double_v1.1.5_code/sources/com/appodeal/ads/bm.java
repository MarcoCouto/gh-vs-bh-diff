package com.appodeal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;

public class bm {

    /* renamed from: a reason: collision with root package name */
    private static Map<String, bm> f1606a = new HashMap(5);
    private SharedPreferences b;

    private bm(Context context, String str) {
        this.b = context.getSharedPreferences(str, 0);
    }

    public static bm a(@NonNull Context context) {
        return a(context, "appodeal");
    }

    public static bm a(@NonNull Context context, String str) {
        bm bmVar = (bm) f1606a.get(str);
        if (bmVar == null) {
            synchronized (bm.class) {
                bmVar = (bm) f1606a.get(str);
                if (bmVar == null) {
                    bmVar = new bm(context, str);
                    f1606a.put(str, bmVar);
                }
            }
        }
        return bmVar;
    }

    public Editor a() {
        return this.b.edit();
    }

    public SharedPreferences b() {
        return this.b;
    }
}
