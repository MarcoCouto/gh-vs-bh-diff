package com.appodeal.ads;

import android.app.Activity;
import android.support.annotation.NonNull;

class ai extends AdNetwork<a> {

    static final class a {
        a() {
        }
    }

    public static class b extends AdNetworkBuilder {
        public AdNetwork build() {
            return new ai(this);
        }

        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "debug";
        }
    }

    private ai(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    public String getVersion() {
        return "1";
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<a> networkInitializationListener) throws Exception {
        final p pVar = null;
        final m mVar = adNetworkMediationParams instanceof g ? ((g) adNetworkMediationParams).f1631a : null;
        if (mVar == null) {
            networkInitializationListener.onInitializationFailed(LoadingError.AdTypeNotSupportedInAdapter);
            return;
        }
        if (mVar instanceof aq) {
            pVar = an.a();
        } else if (mVar instanceof ae) {
            pVar = ab.b();
        } else if (mVar instanceof bb) {
            pVar = Native.a();
        } else if (mVar instanceof ax) {
            pVar = au.a();
        } else if (mVar instanceof bu) {
            pVar = be.a();
        } else if (mVar instanceof bj) {
            pVar = bk.a();
        }
        if (pVar == null) {
            networkInitializationListener.onInitializationFailed(LoadingError.AdTypeNotSupportedInAdapter);
        } else {
            ah.a(activity, mVar, (com.appodeal.ads.ah.b) new com.appodeal.ads.ah.b() {
                public void a(int i, boolean z) {
                    pVar.a(mVar, i, z, true);
                }
            });
            networkInitializationListener.onInitializationFinished(new a());
        }
    }

    public void setLogging(boolean z) {
    }
}
