package com.appodeal.ads;

import com.appodeal.ads.api.Stats.AdUnitRequestResult;

public enum t {
    Successful(AdUnitRequestResult.SUCCESSFUL),
    NoFill(AdUnitRequestResult.NOFILL),
    TimeOutReached(AdUnitRequestResult.TIMEOUTREACHED),
    Exception(AdUnitRequestResult.EXCEPTION),
    UndefinedAdapter(AdUnitRequestResult.UNDEFINEDADAPTER),
    IncorrectAdunit(AdUnitRequestResult.INCORRECTADUNIT),
    InvalidAssets(AdUnitRequestResult.INVALIDASSETS),
    Unrecognized(AdUnitRequestResult.UNRECOGNIZED),
    Canceled(AdUnitRequestResult.CANCELED),
    IncorrectCreative(AdUnitRequestResult.EXCEPTION);
    
    private AdUnitRequestResult k;

    private t(AdUnitRequestResult adUnitRequestResult) {
        this.k = adUnitRequestResult;
    }

    public AdUnitRequestResult a() {
        return this.k;
    }
}
