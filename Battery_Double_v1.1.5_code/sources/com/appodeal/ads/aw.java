package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;

class aw extends bv<ax, UnifiedMrec, UnifiedMrecParams, UnifiedMrecCallback> {

    private final class a extends UnifiedMrecCallback {
        private a() {
        }

        public void onAdClicked() {
            au.b().a(aw.this.a(), aw.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            au.b().a(aw.this.a(), aw.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdExpired() {
            au.b().i(aw.this.a(), aw.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            aw.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            au.b().b(aw.this.a(), aw.this, loadingError);
        }

        public void onAdLoaded(View view) {
            aw.this.a(view);
            au.b().b(aw.this.a(), aw.this);
        }

        public void onAdShowFailed() {
            au.b().a(aw.this.a(), aw.this, null, LoadingError.ShowFailed);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((ax) aw.this.a()).a((AdUnit) aw.this, str, obj);
        }
    }

    private final class b implements UnifiedMrecParams {
        private b() {
        }

        public String obtainPlacementId() {
            return au.a().u();
        }

        public String obtainSegmentId() {
            return au.a().s();
        }
    }

    aw(@NonNull ax axVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
        super(axVar, adNetwork, boVar, 5000);
    }

    /* access modifiers changed from: protected */
    public int b(Context context) {
        return -1;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedMrec a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createMrec();
    }

    /* access modifiers changed from: protected */
    public int c(Context context) {
        return -2;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedMrecParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedMrecCallback o() {
        return new a();
    }
}
