package com.appodeal.ads;

import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import android.util.SparseArray;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.s;
import com.appodeal.ads.utils.v;
import com.facebook.places.model.PlaceFields;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class z {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    SparseArray<JSONObject> f1752a = new SparseArray<>();
    @VisibleForTesting
    JSONObject b;
    private AppodealRequestCallbacks c;
    private SparseArray<Pair<String, Long>> d = new SparseArray<>();

    z() {
    }

    z(AppodealRequestCallbacks appodealRequestCallbacks) {
        this.c = appodealRequestCallbacks;
    }

    private boolean b(int i) {
        p a2;
        if (i == 128) {
            a2 = bk.a();
        } else if (i == 256) {
            a2 = au.a();
        } else if (i != 512) {
            boolean z = false;
            switch (i) {
                case 1:
                    a2 = an.a();
                    break;
                case 2:
                    a2 = be.a();
                    break;
                case 3:
                    if (an.a().E() || be.a().E()) {
                        z = true;
                    }
                    return z;
                case 4:
                    a2 = ab.b();
                    break;
                default:
                    return false;
            }
        } else {
            a2 = Native.a();
        }
        return a2.E();
    }

    private synchronized JSONObject c(int i) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        String str;
        String str2;
        try {
            if (this.b == null) {
                this.b = new JSONObject();
                this.b.put("device_id", bh.f1600a.getIfa());
                this.b.put(CampaignEx.JSON_KEY_PACKAGE_NAME, Appodeal.f.getPackageName());
                this.b.put("os", Constants.JAVASCRIPT_INTERFACE_NAME);
                this.b.put("sdk_version", "2.6.0");
                this.b.put(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
                if (br.k(Appodeal.f)) {
                    jSONObject2 = this.b;
                    str = TapjoyConstants.TJC_DEVICE_TYPE_NAME;
                    str2 = "tablet";
                } else {
                    jSONObject2 = this.b;
                    str = TapjoyConstants.TJC_DEVICE_TYPE_NAME;
                    str2 = PlaceFields.PHONE;
                }
                jSONObject2.put(str, str2);
                ConnectionData connectionData = bh.f1600a.getConnectionData(Appodeal.f);
                if (connectionData != null) {
                    this.b.put(TapjoyConstants.TJC_CONNECTION_TYPE, connectionData.type);
                }
                this.b.put("user_agent", bh.f1600a.getHttpAgent(Appodeal.f));
                this.b.put("model", String.format("%s %s", new Object[]{Build.MANUFACTURER, Build.MODEL}));
            }
            jSONObject = new JSONObject();
            Iterator keys = this.b.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                jSONObject.put(str3, this.b.get(str3));
            }
            jSONObject.put("waterfall_ad_type", i);
            jSONObject.put("waterfall_start_time", System.currentTimeMillis());
            jSONObject.put("ad_units", new JSONArray());
        } catch (Exception e) {
            Log.log(e);
            return null;
        }
        return jSONObject;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public String a() {
        return "https://rri.appodeal.com/api/stat";
    }

    public void a(int i) {
        if (b(i)) {
            this.f1752a.put(i, c(i));
        }
        if (this.c != null) {
            this.c.onWaterfallStart(i);
        }
    }

    public void a(int i, String str) {
        if (this.c != null) {
            this.c.onImpression(i, str);
        }
    }

    public void a(int i, String str, String str2) {
        if (b(i)) {
            this.d.put(i, new Pair(str2, Long.valueOf(System.currentTimeMillis())));
        }
        if (this.c != null) {
            this.c.onRequestStart(i, str, "");
        }
    }

    public void a(int i, String str, boolean z) {
        a(i, str, z, 0);
    }

    public void a(int i, String str, boolean z, int i2) {
        try {
            if (b(i)) {
                Pair pair = (Pair) this.d.get(i);
                if (pair != null) {
                    String str2 = (String) pair.first;
                    Long l = (Long) pair.second;
                    JSONObject jSONObject = (JSONObject) this.f1752a.get(i);
                    if (!(l == null || jSONObject == null)) {
                        Long valueOf = Long.valueOf(System.currentTimeMillis() - l.longValue());
                        JSONArray jSONArray = jSONObject.getJSONArray("ad_units");
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("aid", str2);
                        jSONObject2.put("network_name", str);
                        jSONObject2.put("fill", z);
                        jSONObject2.put("delta", valueOf);
                        if (!z) {
                            jSONObject2.put(IronSourceConstants.EVENTS_ERROR_REASON, i2);
                        }
                        jSONArray.put(jSONObject2);
                    }
                }
            }
            if (this.c != null) {
                this.c.onRequestFinish(i, str, z);
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public void a(int i, boolean z) {
        try {
            if (b(i)) {
                JSONObject jSONObject = (JSONObject) this.f1752a.get(i);
                if (jSONObject != null) {
                    jSONObject.put(IronSourceConstants.EVENTS_RESULT, z);
                    a(jSONObject, i);
                }
            }
            if (this.c != null) {
                this.c.onWaterfallFinish(i, z);
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(JSONObject jSONObject, int i) {
        this.f1752a.remove(i);
        this.d.remove(i);
        s.f1738a.execute(new v(jSONObject.toString(), a()));
    }

    public void b(int i, String str) {
        if (this.c != null) {
            this.c.onClick(i, str);
        }
    }
}
