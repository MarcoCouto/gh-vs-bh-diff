package com.appodeal.ads;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import com.appodeal.ads.Native.MediaAssetType;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.b.d;
import com.appodeal.ads.b.e;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.ab;
import com.appodeal.ads.utils.ab.b;
import com.appodeal.ads.utils.b.a;
import com.appodeal.ads.utils.o;
import com.appodeal.ads.utils.p;
import com.appodeal.ads.utils.q;
import com.explorestack.iab.vast.VastRequest;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ay implements OnClickListener, NativeAd {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    final String f1565a;
    private ba b;
    /* access modifiers changed from: private */
    public UnifiedNativeAd c;
    /* access modifiers changed from: private */
    public UnifiedNativeCallback d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;
    private final String i;
    private String j;
    private Bitmap k;
    private String l;
    private Bitmap m;
    /* access modifiers changed from: private */
    public NativeAdView n;
    private at o;
    private ProgressDialog p;
    private Uri q;
    private VastRequest r;
    private Handler s;
    private Runnable t;
    private d u;
    private double v;
    /* access modifiers changed from: private */
    public boolean w;
    private q x = new q();

    public ay(@NonNull ba baVar, @NonNull UnifiedNativeAd unifiedNativeAd, @NonNull UnifiedNativeCallback unifiedNativeCallback) {
        this.b = baVar;
        this.c = unifiedNativeAd;
        this.d = unifiedNativeCallback;
        this.e = a(unifiedNativeAd.getTitle(), 25);
        this.f = a(unifiedNativeAd.getDescription(), 100);
        this.g = a(unifiedNativeAd.getCallToAction(), 25);
        this.l = unifiedNativeAd.getImageUrl();
        this.j = unifiedNativeAd.getIconUrl();
        this.f1565a = unifiedNativeAd.getClickUrl();
        this.h = unifiedNativeAd.getVideoUrl();
        this.i = unifiedNativeAd.getVastVideoTag();
        this.v = baVar.getEcpm();
    }

    private View a(Context context) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ScaleType.FIT_CENTER);
        if (Native.c != MediaAssetType.IMAGE) {
            a(imageView, this.j, this.k);
        }
        return imageView;
    }

    private static Map<View, String> a(Rect rect, View view, Map<View, String> map) {
        if (map.containsKey(view)) {
            if (br.b(view) && view.isShown() && !br.a(view) && br.a(rect, view)) {
                map.remove(view);
            }
        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                a(rect, viewGroup.getChildAt(i2), map);
            }
        }
        return map;
    }

    private void a(ViewGroup viewGroup) {
        for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if (!(childAt instanceof at)) {
                if (childAt instanceof Button) {
                    ((Button) childAt).setOnClickListener(this);
                }
                if (childAt instanceof ViewGroup) {
                    a((ViewGroup) childAt);
                }
            }
        }
    }

    private void a(NativeAdView nativeAdView) {
        a(this.n, (OnClickListener) null);
        a(nativeAdView, (OnClickListener) this);
        a((ViewGroup) nativeAdView);
        this.n = nativeAdView;
        if (!this.w) {
            ab.a(this, this.n, (long) Native.a().D(), new b() {
                public void a() {
                    ay.this.w = true;
                    ay.this.a((View) ay.this.n);
                    ay.this.d.onAdShown(ay.this.c);
                    ay.this.b(ay.this.n);
                }

                public void b() {
                    ay.this.a();
                    ay.this.d.onAdFinished(ay.this.c);
                }
            });
        }
        if (this.o != null) {
            this.o.b();
            if (Native.e && Native.b != NativeAdType.NoVideo) {
                this.o.c();
            }
        }
        this.c.onRegisterForInteraction(nativeAdView);
    }

    private void a(@Nullable NativeAdView nativeAdView, @Nullable OnClickListener onClickListener) {
        if (nativeAdView != null) {
            nativeAdView.setOnClickListener(onClickListener);
            for (View view : nativeAdView.getClickableViews()) {
                if (!(view instanceof at)) {
                    view.setOnClickListener(onClickListener);
                }
            }
        }
    }

    private void a(@Nullable List<String> list) {
        String f2 = ((bb) this.b.a()).f();
        String a2 = this.u == null ? null : d.a(this.u);
        if (list != null) {
            for (String str : list) {
                if (str != null) {
                    if (f2 != null && str.contains("${APPODEALX_SEGMENT_ID}")) {
                        str = str.replace("${APPODEALX_SEGMENT_ID}", f2);
                    }
                    if (a2 != null && str.contains("${APPODEALX_PLACEMENT_ID}")) {
                        str = str.replace("${APPODEALX_PLACEMENT_ID}", a2);
                    }
                    br.e(str);
                }
            }
        }
    }

    private void b(Context context) {
        if (this.n != null && (context instanceof Activity)) {
            if (this.p == null || !this.p.isShowing()) {
                Activity activity = (Activity) context;
                if (br.e(activity)) {
                    Log.log(LogConstants.KEY_NATIVE, LogConstants.EVENT_SHOW_PROGRESS);
                    this.n.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
                        public void onViewAttachedToWindow(View view) {
                        }

                        public void onViewDetachedFromWindow(View view) {
                            view.removeOnAttachStateChangeListener(this);
                            ay.this.r();
                        }
                    });
                    this.p = ProgressDialog.show(activity, "", TJAdUnitConstants.SPINNER_TITLE);
                    this.p.setProgressStyle(0);
                    this.p.setCancelable(false);
                    this.t = new Runnable() {
                        public void run() {
                            ay.this.r();
                        }
                    };
                    this.s = new Handler(Looper.getMainLooper());
                    this.s.postDelayed(this.t, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(NativeAdView nativeAdView) {
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (nativeAdView.getTitleView() == null) {
            arrayList2.add("Title");
        } else {
            hashMap.put(nativeAdView.getTitleView(), "Title");
        }
        if (nativeAdView.getCallToActionView() == null) {
            arrayList2.add("CallToAction");
        } else {
            hashMap.put(nativeAdView.getCallToActionView(), "CallToAction");
        }
        if (nativeAdView.getNativeIconView() == null && nativeAdView.getNativeMediaView() == null) {
            arrayList2.add("NativeIconView/NativeMediaView");
        } else {
            if (Native.c != MediaAssetType.IMAGE) {
                hashMap.put(nativeAdView.getNativeIconView(), "NativeIconView");
            } else if (nativeAdView.getNativeIconView() != null) {
                arrayList.add("NativeIconView");
            }
            if (Native.c != MediaAssetType.ICON) {
                hashMap.put(nativeAdView.getNativeMediaView(), "NativeMediaView");
            } else if (nativeAdView.getNativeMediaView() != null) {
                arrayList.add("NativeMediaView");
            }
        }
        if (getProviderView(nativeAdView.getContext()) != null) {
            if (nativeAdView.getProviderView() == null) {
                arrayList2.add("ProviderView");
            } else {
                hashMap.put(nativeAdView.getProviderView(), "ProviderView");
            }
        }
        if (!arrayList2.isEmpty()) {
            Log.log(new a(String.format("Required assets: %s are not added to NativeAdView", new Object[]{arrayList2.toString()})));
        }
        if (!arrayList.isEmpty()) {
            Log.log(new a(String.format("Non necessary assets: %s are not added to NativeAdView", new Object[]{arrayList.toString()})));
        }
        Map a2 = a(br.c((View) nativeAdView), (View) nativeAdView, (Map<View, String>) hashMap);
        if (!a2.isEmpty()) {
            Log.log(new a(String.format("Required assets: %s are not visible or not found", new Object[]{a2.values().toString()})));
        }
    }

    private void c(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                bitmap.recycle();
            } catch (Exception e2) {
                Log.log(LogConstants.KEY_NATIVE, LogConstants.EVENT_ASSETS, "bitmap recycling error");
                Log.log(e2);
            }
        }
    }

    private void p() {
        c(this.k);
        this.k = null;
        c(this.m);
        this.m = null;
        q();
    }

    private void q() {
        if (this.q != null && this.q.getPath() != null) {
            File file = new File(this.q.getPath());
            if (file.exists()) {
                file.delete();
            }
            this.q = null;
        }
    }

    /* access modifiers changed from: private */
    public void r() {
        if (this.p != null && this.p.isShowing()) {
            this.p.dismiss();
            this.p = null;
        }
        if (this.t != null && this.s != null) {
            this.s.removeCallbacks(this.t);
            this.s = null;
            this.t = null;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public String a(String str, int i2) {
        if (str == null || str.length() <= i2) {
            return str;
        }
        String substring = str.substring(0, i2);
        if (str.charAt(i2) != ' ' && substring.lastIndexOf(" ") > 0) {
            substring = substring.substring(0, substring.lastIndexOf(" "));
        }
        StringBuilder sb = new StringBuilder();
        sb.append(substring);
        sb.append("…");
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a() {
        this.c.onAdFinish();
        a(this.c.getFinishNotifyUrls());
    }

    /* access modifiers changed from: 0000 */
    public void a(Bitmap bitmap) {
        this.k = bitmap;
    }

    /* access modifiers changed from: 0000 */
    public void a(Uri uri) {
        this.q = uri;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(View view) {
        this.c.onAdImpression(view);
        a(this.c.getImpressionNotifyUrls());
    }

    /* access modifiers changed from: 0000 */
    public void a(ImageView imageView, String str, Bitmap bitmap) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            p.a(str, imageView, (p.b) new p.b() {
                public void a(@NonNull ImageView imageView, @NonNull Bitmap bitmap) {
                    imageView.setImageBitmap(bitmap);
                }

                public void a(String str) {
                    Log.log(LogConstants.KEY_NATIVE, LogConstants.EVENT_ASSETS_ERROR, str);
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(NativeAdView nativeAdView, String str) {
        c(str);
        nativeAdView.deconfigureContainer();
        this.c.onConfigure(nativeAdView);
        a(nativeAdView);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull NativeIconView nativeIconView) {
        Context context = nativeIconView.getContext();
        View obtainIconView = this.c.obtainIconView(context);
        if (obtainIconView == null) {
            obtainIconView = a(context);
        }
        br.d(obtainIconView);
        nativeIconView.removeAllViews();
        nativeIconView.addView(obtainIconView, new LayoutParams(-1, -1));
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull NativeMediaView nativeMediaView) {
        if (!this.c.onConfigureMediaView(nativeMediaView)) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(13, -1);
            this.o = new at(nativeMediaView.getContext());
            if (Native.c != MediaAssetType.ICON) {
                this.o.setNativeAd(this);
            }
            nativeMediaView.removeAllViews();
            nativeMediaView.addView(this.o, layoutParams);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(VastRequest vastRequest) {
        this.r = vastRequest;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.j = str;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a(this.n, (OnClickListener) null);
        ab.a((Object) this);
        if (this.o != null) {
            this.o.d();
        }
        if (this.c != null) {
            this.c.onUnregisterForInteraction();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(Bitmap bitmap) {
        this.m = bitmap;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.l = str;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        return this.j;
    }

    @VisibleForTesting
    public void c(String str) {
        this.u = e.a(str);
        Native.a().a(this.u);
    }

    public boolean canShow(@NonNull Context context, @NonNull String str) {
        if (str != null) {
            return e.a(str).a(context, 512, this.v);
        }
        Log.log(new a("Unable to check: placement = null"));
        return false;
    }

    public boolean containsVideo() {
        return this.c.containsVideo() || !TextUtils.isEmpty(g()) || !TextUtils.isEmpty(h());
    }

    /* access modifiers changed from: 0000 */
    public Bitmap d() {
        return this.k;
    }

    @CallSuper
    public void destroy() {
        o.a((i) this.b);
        if (this.c != null) {
            this.c.onDestroy();
        }
        b();
        p();
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public Bitmap f() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String g() {
        return this.h;
    }

    public String getAdProvider() {
        return this.b.d();
    }

    @Nullable
    public String getAgeRestrictions() {
        return this.c.getAgeRestriction();
    }

    public String getCallToAction() {
        return !TextUtils.isEmpty(this.g) ? this.g : "Install";
    }

    public String getDescription() {
        return this.f;
    }

    public double getPredictedEcpm() {
        return this.v;
    }

    public View getProviderView(Context context) {
        return this.c.obtainProviderView(context);
    }

    public float getRating() {
        Float rating = this.c.getRating();
        if (rating == null) {
            return 5.0f;
        }
        return rating.floatValue();
    }

    public String getTitle() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String h() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public VastRequest i() {
        return this.r;
    }

    public boolean isPrecache() {
        return this.b.isPrecache();
    }

    /* access modifiers changed from: 0000 */
    public Uri j() {
        return this.q;
    }

    /* access modifiers changed from: 0000 */
    public boolean k() {
        return this.c.hasVideo() || j() != null;
    }

    /* access modifiers changed from: 0000 */
    public int l() {
        return this.c != null ? this.c.getAdId() : hashCode();
    }

    /* access modifiers changed from: 0000 */
    public void m() {
        if (this.c != null) {
            this.c.onAdVideoFinish();
        }
    }

    /* access modifiers changed from: protected */
    public d n() {
        return this.u == null ? e.c() : this.u;
    }

    /* access modifiers changed from: 0000 */
    public ba o() {
        return this.b;
    }

    public void onClick(View view) {
        this.c.onAdClick(view);
        a(this.c.getClickNotifyUrls());
        Context context = view.getContext();
        b(context);
        this.x.a(context, this.f1565a, this.c.getTrackingPackageName(), this.c.getTrackingPackageExpiry(), new q.a() {
            public void onHandleError() {
                ay.this.r();
            }

            public void onHandled() {
                ay.this.r();
            }

            public void processClick(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
                ay.this.d.onAdClicked(ay.this.c, unifiedAdCallbackClickTrackListener);
            }
        });
    }
}
