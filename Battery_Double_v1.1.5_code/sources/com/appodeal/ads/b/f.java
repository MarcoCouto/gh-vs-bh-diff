package com.appodeal.ads.b;

import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.utils.Version;
import com.facebook.appevents.UserDataStore;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.tapjoy.TapjoyConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class f {

    /* renamed from: a reason: collision with root package name */
    final String f1579a;
    final c b;
    Object c;
    a d = a(this.f1579a);

    enum a {
        Version,
        String,
        StringArray,
        Integer,
        IntegerArray,
        Float,
        Boolean,
        Mask,
        Unknown
    }

    f(JSONObject jSONObject) throws JSONException {
        this.f1579a = jSONObject.getString("name");
        this.b = c.a(jSONObject.getString("op"));
        this.c = a(jSONObject);
    }

    static a a(Object obj) {
        return obj instanceof Integer ? a.Integer : obj instanceof Float ? a.Float : obj instanceof Boolean ? a.Boolean : obj instanceof String ? a.String : a.Unknown;
    }

    private Object a(JSONObject jSONObject) throws JSONException {
        switch (this.d) {
            case Version:
                return new Version(jSONObject.getString("value"));
            case String:
                return jSONObject.getString("value");
            case StringArray:
                return c(jSONObject);
            case Integer:
                return Integer.valueOf(jSONObject.getInt("value"));
            case IntegerArray:
                return b(jSONObject);
            case Float:
                return Float.valueOf(jSONObject.getString("value"));
            case Boolean:
                return Boolean.valueOf(jSONObject.getString("value"));
            case Mask:
                return jSONObject.getString("value");
            case Unknown:
                return jSONObject.getString("value");
            default:
                return null;
        }
    }

    private Float b(Object obj) {
        if (obj instanceof String) {
            return Float.valueOf((String) obj);
        }
        if (obj instanceof Float) {
            return (Float) obj;
        }
        return null;
    }

    private Integer[] b(JSONObject jSONObject) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray("value");
        Integer[] numArr = new Integer[optJSONArray.length()];
        for (int i = 0; i < optJSONArray.length(); i++) {
            numArr[i] = Integer.valueOf(optJSONArray.getString(i));
        }
        return numArr;
    }

    private Integer c(Object obj) {
        if (obj instanceof String) {
            return Integer.valueOf((String) obj);
        }
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        return null;
    }

    private String[] c(JSONObject jSONObject) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray("value");
        String[] strArr = new String[optJSONArray.length()];
        for (int i = 0; i < optJSONArray.length(); i++) {
            strArr[i] = optJSONArray.getString(i);
        }
        return strArr;
    }

    private Boolean d(Object obj) {
        if (obj instanceof String) {
            return Boolean.valueOf((String) obj);
        }
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public a a(String str) {
        return str.equals("app") ? a.StringArray : str.equals(TapjoyConstants.TJC_APP_VERSION_NAME) ? a.Version : str.equals("sdk_version") ? a.Version : str.equals(UserDataStore.COUNTRY) ? a.StringArray : str.equals(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME) ? a.Version : str.equals("session_count") ? a.Integer : str.equals("average_session_length") ? a.Integer : str.equals(TapjoyConstants.TJC_CONNECTION_TYPE) ? a.StringArray : str.equals("gender") ? a.StringArray : str.equals(IronSourceSegment.AGE) ? a.Integer : str.equals("bought_inapps") ? a.Boolean : str.equals("inapp_amount") ? a.Float : str.equals(TapjoyConstants.TJC_DEVICE_TYPE_NAME) ? a.String : str.equals("session_time") ? a.Mask : str.equals("part_of_audience") ? a.Integer : a.Unknown;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        Object d2;
        if (this.d == a.Integer) {
            d2 = c(this.c);
        } else if (this.d == a.Float) {
            d2 = b(this.c);
        } else if (this.d == a.Boolean) {
            d2 = d(this.c);
        } else {
            return;
        }
        this.c = d2;
    }
}
