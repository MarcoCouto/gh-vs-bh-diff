package com.appodeal.ads.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.AdType;
import com.appodeal.ads.bm;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.ironsource.sdk.precache.DownloadManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
    @VisibleForTesting(otherwise = 3)

    /* renamed from: a reason: collision with root package name */
    public static final Map<String, d> f1578a = new TreeMap();
    @VisibleForTesting
    public static final Map<String, d> b = new TreeMap();
    private static d c;
    private static ArrayList<a> d = new ArrayList<>(AdType.values().length);

    public interface a {
        d a();

        void a(d dVar);

        String b();
    }

    public static d a(String str) {
        if (f1578a.containsKey(str)) {
            return (d) f1578a.get(str);
        }
        if (b.containsKey(str)) {
            return (d) b.get(str);
        }
        if (!str.equals("default")) {
            Log.log("Placement", LogConstants.EVENT_GET, String.format("'%s' not found, using default placement", new Object[]{str}));
        }
        if (f1578a.containsKey("default")) {
            return (d) f1578a.get("default");
        }
        if (b.containsKey("default")) {
            return (d) b.get("default");
        }
        d();
        return c;
    }

    public static void a(@NonNull Context context) {
        SharedPreferences b2 = bm.a(context, "placements_freq").b();
        long currentTimeMillis = ((System.currentTimeMillis() / 1000) / 60) - 43200;
        for (String str : b2.getAll().keySet()) {
            try {
                JSONArray jSONArray = new JSONArray(b2.getString(str, ""));
                JSONArray jSONArray2 = new JSONArray();
                for (int i = 0; i < jSONArray.length(); i++) {
                    long j = jSONArray.getLong(i);
                    if (j > currentTimeMillis) {
                        jSONArray2.put(j);
                    }
                }
                b2.edit().putString(str, jSONArray2.toString()).apply();
            } catch (Exception e) {
                Log.log(e);
            }
        }
    }

    public static void a(@NonNull a aVar) {
        d.add(aVar);
    }

    public static void a(JSONArray jSONArray) throws JSONException {
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                int i2 = jSONObject.getInt("id");
                String string = jSONObject.getString("name");
                JSONObject jSONObject2 = jSONObject.getJSONObject(DownloadManager.SETTINGS);
                d dVar = (d) b.get(string);
                if (dVar == null) {
                    dVar = new d(i2, string, jSONObject2);
                } else {
                    dVar.a(jSONObject2);
                }
                b.put(string, dVar);
            }
        }
    }

    public static boolean a() {
        return !b.isEmpty() && !i.b();
    }

    public static boolean a(d dVar) {
        return dVar == null || dVar.equals(c);
    }

    public static void b() {
        Iterator it = d.iterator();
        while (it.hasNext()) {
            a aVar = (a) it.next();
            String b2 = aVar.b();
            if (b2 != null && a(aVar.a())) {
                aVar.a(a(b2));
            }
        }
    }

    public static d c() {
        return a("default");
    }

    private static void d() {
        if (c == null) {
            c = new d(-1, "default", new JSONObject());
        }
    }
}
