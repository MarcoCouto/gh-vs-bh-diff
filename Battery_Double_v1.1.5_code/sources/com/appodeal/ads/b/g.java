package com.appodeal.ads.b;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.utils.Log;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.precache.DownloadManager;
import com.smaato.sdk.core.api.VideoType;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class g {

    /* renamed from: a reason: collision with root package name */
    final b f1582a;
    final f[] b;
    @VisibleForTesting
    final JSONArray c;
    @VisibleForTesting(otherwise = 3)
    final a d;
    private final long e;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        final JSONObject f1583a;

        public a(JSONObject jSONObject) {
            JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
            if (optJSONObject == null) {
                optJSONObject = new JSONObject();
            }
            this.f1583a = optJSONObject;
        }

        private Set<String> a(JSONObject jSONObject) {
            HashSet hashSet = new HashSet();
            if (jSONObject != null) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    hashSet.add(keys.next());
                }
            }
            return hashSet;
        }

        private JSONObject a(String str) {
            try {
                if (i() != null) {
                    JSONObject jSONObject = new JSONObject();
                    Iterator keys = i().keys();
                    while (keys.hasNext()) {
                        String str2 = (String) keys.next();
                        JSONObject optJSONObject = i().optJSONObject(str2);
                        if (optJSONObject != null && optJSONObject.has(str)) {
                            jSONObject.put(str2, optJSONObject.optDouble(str));
                        }
                    }
                    return jSONObject;
                }
            } catch (Exception e) {
                Log.log(e);
            }
            return null;
        }

        private void a(List<JSONObject> list, final Set<String> set) {
            if (!set.isEmpty()) {
                Collections.sort(list, new Comparator<JSONObject>() {
                    /* renamed from: a */
                    public int compare(JSONObject jSONObject, JSONObject jSONObject2) {
                        if (!set.contains(jSONObject.optString("status")) && !set.contains(jSONObject2.optString("status"))) {
                            return 0;
                        }
                        double optDouble = jSONObject2.optDouble(RequestInfoKeys.APPODEAL_ECPM) - jSONObject.optDouble(RequestInfoKeys.APPODEAL_ECPM);
                        if (optDouble == Utils.DOUBLE_EPSILON) {
                            return 0;
                        }
                        return optDouble < Utils.DOUBLE_EPSILON ? -1 : 1;
                    }
                });
            }
        }

        private void a(List<JSONObject> list, Set<String> set, JSONObject jSONObject) throws JSONException {
            if (!set.isEmpty()) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    JSONObject jSONObject2 = (JSONObject) it.next();
                    String optString = jSONObject2.optString("status", null);
                    String optString2 = jSONObject2.optString("name", null);
                    if (optString2 != null && !optString2.isEmpty()) {
                        optString = optString2;
                    }
                    if (optString != null && !optString.isEmpty() && set.contains(optString)) {
                        if (jSONObject2.has("cap")) {
                            if (jSONObject2.getBoolean("cap")) {
                                it.remove();
                            }
                        } else if (a(list, jSONObject2)) {
                        }
                        jSONObject2.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(optString));
                    }
                }
            }
        }

        private boolean a(List<JSONObject> list, JSONObject jSONObject) {
            String optString = jSONObject.optString("id");
            String optString2 = jSONObject.optString("status");
            String optString3 = jSONObject.optString("name", null);
            if (optString3 == null || optString3.isEmpty()) {
                optString3 = optString2;
            }
            for (JSONObject jSONObject2 : list) {
                String optString4 = jSONObject2.optString("id");
                if (optString4 == null || !optString4.equals(optString)) {
                    String optString5 = jSONObject2.optString("status");
                    String optString6 = jSONObject2.optString("name", null);
                    if (optString6 == null || optString6.isEmpty()) {
                        optString6 = optString5;
                    }
                    if (optString6 != null && optString6.equals(optString3)) {
                        return true;
                    }
                }
            }
            return false;
        }

        private void b(List<JSONObject> list, int i) {
            try {
                JSONArray c = c(i);
                if (c != null) {
                    if (c.length() != 0) {
                        HashSet hashSet = new HashSet(c.length());
                        for (int i2 = 0; i2 < c.length(); i2++) {
                            hashSet.add(c.getString(i2));
                        }
                        Iterator it = list.iterator();
                        while (it.hasNext()) {
                            JSONObject jSONObject = (JSONObject) it.next();
                            String optString = jSONObject.optString("status", null);
                            String optString2 = jSONObject.optString("name", null);
                            if ((optString != null && !optString.isEmpty() && hashSet.contains(optString)) || (optString2 != null && !optString2.isEmpty() && hashSet.contains(optString2))) {
                                it.remove();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.log(e);
            }
        }

        private JSONArray g() {
            return this.f1583a.optJSONArray("disable_type");
        }

        private JSONObject h() {
            return this.f1583a.optJSONObject("price_floor");
        }

        private JSONObject i() {
            return this.f1583a.optJSONObject("overridden_ecpm");
        }

        public double a() {
            if (h() != null) {
                return h().optDouble("banner", -1.0d);
            }
            return -1.0d;
        }

        public List<JSONObject> a(List<JSONObject> list, int i) {
            JSONObject jSONObject = null;
            double d = -1.0d;
            if (i == 4) {
                jSONObject = a("banner");
                d = a();
            } else if (i == 128) {
                jSONObject = a("rewarded_video");
                d = d();
            } else if (i == 256) {
                jSONObject = a("mrec");
                d = e();
            } else if (i != 512) {
                switch (i) {
                    case 1:
                        jSONObject = a(VideoType.INTERSTITIAL);
                        d = b();
                        break;
                    case 2:
                        try {
                            jSONObject = a("video");
                            d = c();
                            break;
                        } catch (Exception e) {
                            Log.log(e);
                            return list;
                        }
                }
            } else {
                jSONObject = a("native");
                d = f();
            }
            Set a2 = a(jSONObject);
            b(list, i);
            a(list, a2, jSONObject);
            a(list, d);
            a(list, a2);
            return list;
        }

        public void a(List<JSONObject> list, double d) {
            try {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    if (((JSONObject) it.next()).optDouble(RequestInfoKeys.APPODEAL_ECPM, Utils.DOUBLE_EPSILON) < d) {
                        it.remove();
                    }
                }
            } catch (Exception e) {
                Log.log(e);
            }
        }

        public boolean a(int i) {
            if (g() == null) {
                return false;
            }
            return g().toString().contains(String.format("\"%s\"", new Object[]{com.appodeal.ads.b.h.a.a(i)}));
        }

        public double b() {
            if (h() != null) {
                return h().optDouble(VideoType.INTERSTITIAL, -1.0d);
            }
            return -1.0d;
        }

        public double b(int i) {
            if (h() != null) {
                return h().optDouble(com.appodeal.ads.b.h.a.a(i), -1.0d);
            }
            return -1.0d;
        }

        public double c() {
            if (h() != null) {
                return h().optDouble("video", -1.0d);
            }
            return -1.0d;
        }

        /* JADX WARNING: Removed duplicated region for block: B:20:0x0036 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0037  */
        public JSONArray c(int i) {
            JSONArray jSONArray;
            String str;
            JSONObject optJSONObject = this.f1583a.optJSONObject("disable_networks");
            if (optJSONObject != null) {
                if (i == 4) {
                    str = "banner";
                } else if (i == 128) {
                    str = "rewarded_video";
                } else if (i == 256) {
                    str = "mrec";
                } else if (i != 512) {
                    switch (i) {
                        case 1:
                            str = VideoType.INTERSTITIAL;
                            break;
                        case 2:
                            str = "video";
                            break;
                    }
                } else {
                    str = "native";
                }
                jSONArray = optJSONObject.optJSONArray(str);
                return jSONArray == null ? jSONArray : new JSONArray();
            }
            jSONArray = null;
            if (jSONArray == null) {
            }
        }

        public double d() {
            if (h() != null) {
                return h().optDouble("rewarded_video", -1.0d);
            }
            return -1.0d;
        }

        public double e() {
            if (h() != null) {
                return h().optDouble("mrec", -1.0d);
            }
            return -1.0d;
        }

        public double f() {
            if (h() != null) {
                return h().optDouble("native", -1.0d);
            }
            return -1.0d;
        }
    }

    g(@NonNull JSONObject jSONObject) {
        this.e = (long) jSONObject.optInt("id", -1);
        this.f1582a = b.a(jSONObject.optString("match_rule", ""));
        JSONArray optJSONArray = jSONObject.optJSONArray("restrictions");
        if (optJSONArray != null) {
            this.b = new f[optJSONArray.length()];
            a(optJSONArray);
        } else {
            this.b = null;
        }
        this.c = jSONObject.optJSONArray("placements");
        this.d = new a(jSONObject);
    }

    private void a(JSONArray jSONArray) {
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                this.b[i] = new f(jSONArray.optJSONObject(i));
            } catch (JSONException e2) {
                Log.log(e2);
            }
        }
    }

    public void a() throws JSONException {
        e.f1578a.clear();
        if (this.c != null) {
            for (int i = 0; i < this.c.length(); i++) {
                JSONObject jSONObject = this.c.getJSONObject(i);
                int i2 = jSONObject.getInt("id");
                String string = jSONObject.getString("name");
                e.f1578a.put(string, new d(i2, string, jSONObject.getJSONObject(DownloadManager.SETTINGS)));
            }
        }
    }

    public a b() {
        return this.d;
    }

    public long c() {
        return this.e;
    }
}
