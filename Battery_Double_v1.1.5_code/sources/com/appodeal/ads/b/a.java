package com.appodeal.ads.b;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.m;
import com.github.mikephil.charting.utils.Utils;
import org.json.JSONObject;

public class a implements com.appodeal.ads.ar.a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f1574a;

    public a(@NonNull Context context) {
        this.f1574a = context;
    }

    public void a(m mVar) {
    }

    public void a(JSONObject jSONObject, @Nullable m mVar, String str) {
        if (jSONObject.has("inapp_amount")) {
            i.a((float) jSONObject.optDouble("inapp_amount", Utils.DOUBLE_EPSILON));
            i.b(this.f1574a);
        }
    }
}
