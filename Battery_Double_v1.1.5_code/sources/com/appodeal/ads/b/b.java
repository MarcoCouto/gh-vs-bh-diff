package com.appodeal.ads.b;

public enum b {
    AND("AND"),
    OR("OR");
    
    private final String c;

    private b(String str) {
        this.c = str;
    }

    static b a(String str) {
        b[] values;
        for (b bVar : values()) {
            if (bVar.c.equals(str)) {
                return bVar;
            }
        }
        return null;
    }
}
