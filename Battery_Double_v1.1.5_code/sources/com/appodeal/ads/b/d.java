package com.appodeal.ads.b;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.bm;
import com.appodeal.ads.br;
import com.appodeal.ads.m;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.github.mikephil.charting.utils.Utils;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.smaato.sdk.core.api.VideoType;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import org.json.JSONArray;
import org.json.JSONObject;

public class d {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    long f1577a;
    @VisibleForTesting
    int b;
    private final int c;
    private final String d;
    private JSONObject e;

    public d(int i, @NonNull String str, @NonNull JSONObject jSONObject) {
        this.c = i;
        this.d = str;
        this.e = jSONObject;
    }

    public static String a(@Nullable d dVar) {
        return dVar == null ? "-1" : String.valueOf(dVar.b());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public JSONArray a(Context context) throws Exception {
        String string = bm.a(context, "placements_freq").b().getString(String.valueOf(b()), "");
        return !string.isEmpty() ? new JSONArray(string) : new JSONArray();
    }

    public void a(Context context, int i) {
        if (e(i)) {
            o();
            a(context, System.currentTimeMillis() / 1000);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(Context context, long j) {
        this.b++;
        if (context != null) {
            try {
                JSONArray a2 = a(context);
                a2.put(j);
                bm.a(context, "placements_freq").a().putString(String.valueOf(b()), a2.toString()).apply();
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
    }

    public void a(JSONObject jSONObject) {
        this.e = jSONObject;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a() {
        return this.e.optBoolean("disable", false);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(int i) {
        String jSONArray;
        String str;
        Object[] objArr;
        JSONArray optJSONArray = this.e.optJSONArray("disable_type");
        if (optJSONArray == null) {
            return false;
        }
        if (i == 4) {
            jSONArray = optJSONArray.toString();
            str = "\"%s\"";
            objArr = new Object[]{"banner"};
        } else if (i == 128) {
            jSONArray = optJSONArray.toString();
            str = "\"%s\"";
            objArr = new Object[]{"rewarded_video"};
        } else if (i == 256) {
            jSONArray = optJSONArray.toString();
            str = "\"%s\"";
            objArr = new Object[]{"mrec"};
        } else if (i != 512) {
            switch (i) {
                case 1:
                    jSONArray = optJSONArray.toString();
                    str = "\"%s\"";
                    objArr = new Object[]{VideoType.INTERSTITIAL};
                    break;
                case 2:
                    jSONArray = optJSONArray.toString();
                    str = "\"%s\"";
                    objArr = new Object[]{"video"};
                    break;
                default:
                    return false;
            }
        } else {
            jSONArray = optJSONArray.toString();
            str = "\"%s\"";
            objArr = new Object[]{"native"};
        }
        return jSONArray.contains(String.format(str, objArr));
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(int i, double d2) {
        boolean z = false;
        if (i == 4) {
            if (d2 >= d()) {
                z = true;
            }
            return z;
        } else if (i == 128) {
            if (d2 >= i()) {
                z = true;
            }
            return z;
        } else if (i == 256) {
            if (d2 >= j()) {
                z = true;
            }
            return z;
        } else if (i != 512) {
            switch (i) {
                case 1:
                    if (d2 >= e()) {
                        z = true;
                    }
                    return z;
                case 2:
                    if (d2 >= f()) {
                        z = true;
                    }
                    return z;
                default:
                    return true;
            }
        } else {
            if (d2 >= k()) {
                z = true;
            }
            return z;
        }
    }

    public boolean a(@Nullable Context context, int i, double d2) {
        if (a()) {
            Log.log("Placement", LogConstants.EVENT_CAN_SHOW, String.format("'%s' - ad disabled", new Object[]{n()}));
            return false;
        } else if (a(i)) {
            Log.log("Placement", LogConstants.EVENT_CAN_SHOW, String.format("'%s' - %s disabled", new Object[]{n(), br.a(i)}));
            return false;
        } else if (!c(i)) {
            Log.log("Placement", LogConstants.EVENT_CAN_SHOW, String.format("'%s' - impression count per session exceeded", new Object[]{n()}));
            return false;
        } else if (!d(i)) {
            Log.log("Placement", LogConstants.EVENT_CAN_SHOW, String.format("'%s' - impression interval hasn't passed yet", new Object[]{n()}));
            return false;
        } else if (!b(context, i)) {
            Log.log("Placement", LogConstants.EVENT_CAN_SHOW, String.format("'%s' - impression count per period exceeded", new Object[]{n()}));
            return false;
        } else if (!a(i, d2)) {
            Log.log("Placement", LogConstants.EVENT_CAN_SHOW, String.format("'%s' - %s impression eCPM $%s lower than price floor", new Object[]{n(), br.a(i), Double.valueOf(d2)}));
            return false;
        } else if (b(i)) {
            return true;
        } else {
            Log.log("Placement", LogConstants.EVENT_CAN_SHOW, String.format("'%s' - interstitial type disabled", new Object[]{n()}));
            return false;
        }
    }

    public boolean a(@Nullable Context context, int i, m mVar) {
        Double valueOf = Double.valueOf(Utils.DOUBLE_EPSILON);
        if (mVar != null) {
            valueOf = Double.valueOf(mVar.C());
        }
        return a(context, i, valueOf.doubleValue());
    }

    public int b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001a, code lost:
        return r0.equals(r4);
     */
    @VisibleForTesting
    public boolean b(int i) {
        String str;
        String optString = this.e.optString("interstitial_type", "");
        if (!optString.isEmpty()) {
            switch (i) {
                case 1:
                    str = "static";
                    break;
                case 2:
                    str = "video";
                    break;
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean b(@Nullable Context context, int i) {
        boolean z = true;
        try {
            if (!e(i)) {
                return true;
            }
            JSONObject optJSONObject = this.e.optJSONObject("impressions_per_period");
            if (!(context == null || optJSONObject == null)) {
                JSONArray a2 = a(context);
                long currentTimeMillis = (System.currentTimeMillis() / 1000) - ((long) optJSONObject.getInt("period"));
                int i2 = 0;
                for (int i3 = 0; i3 < a2.length(); i3++) {
                    if (a2.getLong(i3) >= currentTimeMillis) {
                        i2++;
                    }
                }
                if (i2 >= optJSONObject.getInt("amount")) {
                    z = false;
                }
            }
            return z;
        } catch (Exception unused) {
        }
    }

    public int c() {
        JSONObject optJSONObject = this.e.optJSONObject("impression_interval");
        if (optJSONObject != null) {
            return optJSONObject.optInt("banner", -1) * 1000;
        }
        return -1;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean c(int i) {
        return !e(i) || l() <= 0 || l() > this.b;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public double d() {
        JSONObject optJSONObject = this.e.optJSONObject("price_floor");
        if (optJSONObject != null) {
            return optJSONObject.optDouble("banner", -1.0d);
        }
        return -1.0d;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean d(int i) {
        return !e(i) || m() <= 0 || this.f1577a <= 0 || System.currentTimeMillis() - this.f1577a >= ((long) m());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public double e() {
        JSONObject optJSONObject = this.e.optJSONObject("price_floor");
        if (optJSONObject != null) {
            return optJSONObject.optDouble(VideoType.INTERSTITIAL, -1.0d);
        }
        return -1.0d;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean e(int i) {
        return i == 1 || i == 2 || i == 128;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public double f() {
        JSONObject optJSONObject = this.e.optJSONObject("price_floor");
        if (optJSONObject != null) {
            return optJSONObject.optDouble("video", -1.0d);
        }
        return -1.0d;
    }

    @Nullable
    public String g() {
        JSONObject optJSONObject = this.e.optJSONObject(MTGRewardVideoActivity.INTENT_REWARD);
        if (optJSONObject != null) {
            return optJSONObject.optString("currency", null);
        }
        return null;
    }

    public double h() {
        JSONObject optJSONObject = this.e.optJSONObject(MTGRewardVideoActivity.INTENT_REWARD);
        return optJSONObject != null ? optJSONObject.optDouble("amount", Utils.DOUBLE_EPSILON) : Utils.DOUBLE_EPSILON;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public double i() {
        JSONObject optJSONObject = this.e.optJSONObject("price_floor");
        if (optJSONObject != null) {
            return optJSONObject.optDouble("rewarded_video", -1.0d);
        }
        return -1.0d;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public double j() {
        JSONObject optJSONObject = this.e.optJSONObject("price_floor");
        if (optJSONObject != null) {
            return optJSONObject.optDouble("mrec", -1.0d);
        }
        return -1.0d;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public double k() {
        JSONObject optJSONObject = this.e.optJSONObject("price_floor");
        if (optJSONObject != null) {
            return optJSONObject.optDouble("native", -1.0d);
        }
        return -1.0d;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int l() {
        return this.e.optInt("impressions_per_session", 0);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int m() {
        JSONObject optJSONObject = this.e.optJSONObject("impression_interval");
        if (optJSONObject != null) {
            return optJSONObject.optInt(Events.CREATIVE_FULLSCREEN, -1) * 1000;
        }
        return -1;
    }

    public String n() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void o() {
        long currentTimeMillis = System.currentTimeMillis();
        if (m() > 0) {
            this.f1577a = currentTimeMillis;
        }
    }

    public String toString() {
        return this.e.toString();
    }
}
