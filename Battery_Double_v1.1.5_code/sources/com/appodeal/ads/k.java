package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.i;
import com.appodeal.ads.l;
import com.appodeal.ads.m;
import com.appodeal.ads.utils.LogConstants;

abstract class k<AdRequestType extends m<AdObjectType>, AdObjectType extends i, RendererParams extends l> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    final String f1647a;

    k(@NonNull String str) {
        this.f1647a = str;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull Activity activity, @NonNull RendererParams rendererparams, @NonNull p<AdObjectType, AdRequestType, ?> pVar) {
        String str;
        String str2;
        if (!pVar.l()) {
            str = LogConstants.EVENT_SHOW_FAILED;
            str2 = LogConstants.MSG_NOT_INITIALIZED;
        } else {
            pVar.a(rendererparams.f1648a);
            if (!br.a((Context) activity)) {
                str = LogConstants.EVENT_SHOW_FAILED;
                str2 = LogConstants.EVENT_NETWORK_CONNECTION;
            } else if (Appodeal.d || pVar.j() || pVar.k()) {
                return false;
            } else {
                return b(activity, rendererparams, pVar);
            }
        }
        pVar.a(str, str2);
        return false;
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean a(@NonNull Activity activity, @NonNull p<AdObjectType, AdRequestType, ?> pVar);

    /* access modifiers changed from: 0000 */
    public abstract boolean b(@NonNull Activity activity, @NonNull RendererParams rendererparams, @NonNull p<AdObjectType, AdRequestType, ?> pVar);
}
