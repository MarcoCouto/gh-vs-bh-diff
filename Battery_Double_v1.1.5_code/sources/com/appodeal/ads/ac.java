package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;

class ac extends c<ae, ad, Object> {

    /* renamed from: a reason: collision with root package name */
    private BannerCallbacks f1517a;

    ac() {
    }

    /* access modifiers changed from: 0000 */
    public void a(BannerCallbacks bannerCallbacks) {
        this.f1517a = bannerCallbacks;
    }

    public void a(@NonNull ae aeVar, @NonNull ad adVar) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_LOADED, String.format("height: %sdp, isPrecache: %s", new Object[]{Integer.valueOf(adVar.u()), Boolean.valueOf(adVar.isPrecache())}), LogLevel.verbose);
        Appodeal.b();
        if (this.f1517a != null) {
            this.f1517a.onBannerLoaded(adVar.u(), adVar.isPrecache());
        }
    }

    public void a(@Nullable ae aeVar, @Nullable ad adVar, @Nullable LoadingError loadingError) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_LOAD_FAILED, LogLevel.verbose);
        if (this.f1517a != null) {
            this.f1517a.onBannerFailedToLoad();
        }
    }

    public void a(@Nullable ae aeVar, @Nullable ad adVar, @Nullable Object obj) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_SHOWN, LogLevel.verbose);
        if (this.f1517a != null) {
            this.f1517a.onBannerShown();
        }
    }

    public void a(@Nullable ae aeVar, @Nullable ad adVar, @Nullable Object obj, @Nullable LoadingError loadingError) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_SHOW_FAILED, LogLevel.verbose);
        if (this.f1517a != null) {
            this.f1517a.onBannerShowFailed();
        }
    }

    public void b(@Nullable ae aeVar, @Nullable ad adVar) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_EXPIRED, LogLevel.verbose);
        if (this.f1517a != null) {
            this.f1517a.onBannerExpired();
        }
    }

    public void b(@Nullable ae aeVar, @Nullable ad adVar, @Nullable Object obj) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_CLICKED, LogLevel.verbose);
        if (this.f1517a != null) {
            this.f1517a.onBannerClicked();
        }
    }
}
