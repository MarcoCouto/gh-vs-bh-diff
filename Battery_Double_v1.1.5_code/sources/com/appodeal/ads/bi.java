package com.appodeal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;

class bi extends al<bj, UnifiedRewarded, UnifiedRewardedParams, UnifiedRewardedCallback> {

    private final class a extends UnifiedRewardedCallback {
        private a() {
        }

        public void onAdClicked() {
            bk.b().a(bi.this.a(), bi.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            bk.b().a(bi.this.a(), bi.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdClosed() {
            bk.b().m(bi.this.a(), bi.this);
        }

        public void onAdExpired() {
            bk.b().i(bi.this.a(), bi.this);
        }

        public void onAdFinished() {
            bk.b().o(bi.this.a(), bi.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            bi.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            bk.b().b(bi.this.a(), bi.this, loadingError);
        }

        public void onAdLoaded() {
            bk.b().b(bi.this.a(), bi.this);
        }

        public void onAdShowFailed() {
            bk.b().a(bi.this.a(), bi.this, null, LoadingError.ShowFailed);
        }

        public void onAdShown() {
            bk.b().p(bi.this.a(), bi.this);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((bj) bi.this.a()).a((AdUnit) bi.this, str, obj);
        }
    }

    private class b implements UnifiedRewardedParams {
        private b() {
        }

        public int getAfd() {
            return bk.a().D();
        }

        public int getMaxDuration() {
            return bk.b;
        }

        public String obtainPlacementId() {
            return bk.a().u();
        }

        public String obtainSegmentId() {
            return bk.a().s();
        }
    }

    bi(@NonNull bj bjVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
        super(bjVar, adNetwork, boVar, 10000);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedRewarded a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createRewarded();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedRewardedParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: protected */
    public LoadingError s() {
        return b().isRewardedShowing() ? LoadingError.Canceled : super.s();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedRewardedCallback o() {
        return new a();
    }
}
