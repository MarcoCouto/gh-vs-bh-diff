package com.appodeal.ads;

import android.content.Context;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.Version;
import org.json.JSONObject;

public class aa {

    /* renamed from: a reason: collision with root package name */
    public static String f1513a = null;
    static boolean b = false;
    public static LogLevel c = LogLevel.none;
    public static boolean d = false;
    public static int e = -1;
    public static boolean f = true;
    public static boolean g = true;
    public static boolean h = false;
    public static String i = null;
    static Boolean j;
    private static boolean k = false;

    static int a(Integer num) {
        if (num == null) {
            return 600000;
        }
        return num.intValue();
    }

    public static void a() {
        an.a().a(0);
        be.a().a(0);
        bk.a().a(0);
        ab.b().a(0);
        au.a().a(0);
        Native.a().a(0);
    }

    private static void a(Context context, boolean z) {
        if (context != null) {
            bm.a(context).a().putBoolean("show_errors", z).apply();
        }
    }

    static void a(JSONObject jSONObject) {
        try {
            c(jSONObject);
            if (jSONObject.has("randomize_offers")) {
                g = jSONObject.getBoolean("randomize_offers");
            }
            b(jSONObject);
            if (jSONObject.has("show_errors")) {
                a(Appodeal.f, jSONObject.getBoolean("show_errors"));
            }
            if (jSONObject.has("last_sdk_version") && i == null) {
                i = jSONObject.getString("last_sdk_version");
                if (new Version(i).compareTo(new Version("2.6.0")) == 1) {
                    Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_WARNING, String.format("your SDK version %s does not match latest SDK version %s!", new Object[]{"2.6.0", i}));
                }
            }
            if (jSONObject.has("test")) {
                Appodeal.setTesting(jSONObject.getBoolean("test"));
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    static void a(boolean z) {
        k = z;
    }

    static boolean a(long j2, Integer num) {
        return System.currentTimeMillis() - j2 > ((long) a(num));
    }

    static boolean a(Context context) {
        return bm.a(context).b().getBoolean("show_errors", true);
    }

    static void b(JSONObject jSONObject) {
        if (jSONObject.optBoolean("log")) {
            Appodeal.setLogLevel(LogLevel.verbose);
        }
    }

    static boolean b() {
        return k;
    }

    static void c(JSONObject jSONObject) {
        ak a2;
        String str;
        try {
            if (jSONObject.has("ach") && jSONObject.getString("ach") != null) {
                if (jSONObject.getString("ach").equals(ak.c)) {
                    a2 = ak.a();
                    str = ak.c;
                } else if (jSONObject.getString("ach").equals(ak.d)) {
                    a2 = ak.a();
                    str = ak.d;
                } else {
                    a2 = ak.a();
                    str = ak.b;
                }
                a2.a(str);
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    static boolean c() {
        if (j == null) {
            j = Boolean.valueOf(br.h());
        }
        return j.booleanValue();
    }
}
