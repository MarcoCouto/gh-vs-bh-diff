package com.appodeal.ads;

import android.support.annotation.NonNull;
import java.util.List;
import org.json.JSONObject;

class g implements AdNetworkMediationParams {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    m f1631a;
    @NonNull
    i b;
    @NonNull
    private RestrictedData c;

    g(@NonNull m mVar, @NonNull i iVar, @NonNull RestrictedData restrictedData) {
        this.f1631a = mVar;
        this.b = iVar;
        this.c = restrictedData;
    }

    public String getAppName() {
        return w.f1751a;
    }

    public String getImpressionId() {
        return this.f1631a.E();
    }

    @NonNull
    public List<JSONObject> getParallelBiddingAdUnitList() {
        return this.f1631a.c();
    }

    @NonNull
    public RestrictedData getRestrictedData() {
        return this.c;
    }

    public String getStoreUrl() {
        return w.b;
    }

    public boolean isCoronaApp() {
        return w.a();
    }

    public boolean isTestMode() {
        return aa.b;
    }
}
