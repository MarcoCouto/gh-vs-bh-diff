package com.appodeal.ads;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class as implements LocationData {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private RestrictedData f1546a;
    @Nullable
    private Location b;
    @Nullable
    private Integer c;

    as(@Nullable Context context, @NonNull RestrictedData restrictedData) {
        this.f1546a = restrictedData;
        if (context != null) {
            this.b = br.d(context);
            this.c = Integer.valueOf(this.b == null ? 0 : 1);
        }
    }

    @Nullable
    public Location getDeviceLocation() {
        if (this.f1546a.canSendLocation()) {
            return this.b;
        }
        return null;
    }

    @Nullable
    public Integer getDeviceLocationType() {
        if (this.f1546a.canSendLocationType()) {
            return this.c;
        }
        return null;
    }

    @Nullable
    public Float obtainLatitude() {
        if (this.f1546a.canSendLocation()) {
            return this.b != null ? Float.valueOf(Double.valueOf(this.b.getLatitude()).floatValue()) : bq.a().getLat();
        }
        return null;
    }

    @Nullable
    public Location obtainLocation() {
        if (this.f1546a.canSendLocation()) {
            Float obtainLatitude = obtainLatitude();
            if (obtainLatitude != null) {
                Float obtainLongitude = obtainLongitude();
                if (obtainLongitude != null) {
                    Location location = new Location("unknown");
                    location.setLatitude((double) obtainLatitude.floatValue());
                    location.setLongitude((double) obtainLongitude.floatValue());
                    return location;
                }
            }
        }
        return null;
    }

    @Nullable
    public Float obtainLongitude() {
        if (this.f1546a.canSendLocation()) {
            return this.b != null ? Float.valueOf(Double.valueOf(this.b.getLongitude()).floatValue()) : bq.a().getLon();
        }
        return null;
    }
}
