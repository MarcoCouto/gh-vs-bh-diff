package com.appodeal.ads;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.b;
import com.appodeal.ads.utils.b.d;
import com.appodeal.ads.utils.o;
import java.util.Collection;

class a implements com.appodeal.ads.utils.app.a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public long f1508a;
    /* access modifiers changed from: private */
    public long b;

    a() {
    }

    private void a() {
        a(ab.b());
        a(au.a());
        a(an.a());
        a(be.a());
        a(bk.a());
        o.b((Collection<i>) Native.c().d());
    }

    private void a(p pVar) {
        m y = pVar.y();
        if (y != null && !y.t()) {
            o.b(y.B());
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        b(ab.b());
        b(au.a());
        b(an.a());
        b(be.a());
        b(bk.a());
        o.c((Collection<i>) Native.c().d());
    }

    private void b(p pVar) {
        m y = pVar.y();
        if (y != null) {
            o.c(y.B());
        }
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        Appodeal.e = activity;
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
        Appodeal.getSession().c(activity);
        try {
            ab.a().a(activity);
            this.f1508a = System.currentTimeMillis();
            final long j = this.f1508a;
            br.a((Runnable) new Runnable() {
                public void run() {
                    if (j == a.this.f1508a && a.this.b < a.this.f1508a) {
                        Log.log(LogConstants.KEY_SDK, "Pause");
                        Appodeal.d = true;
                        a.this.b();
                        Appodeal.c().b(Appodeal.f);
                    }
                }
            }, 1000);
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public void onActivityResumed(Activity activity) {
        Appodeal.e = activity;
        Appodeal.getSession().b(activity);
        try {
            this.b = System.currentTimeMillis();
            if (Appodeal.d) {
                Appodeal.d = false;
                Appodeal.c().a(activity);
                a();
                b.a(activity, new d() {
                    public void a(@NonNull com.appodeal.ads.utils.b.a aVar) {
                        if (bf.a(aVar)) {
                            aa.a();
                        }
                    }
                }, null);
                an.a().a(activity);
                be.a().a(activity);
                bk.a().a(activity);
                ab.b().a(activity);
                au.a().a(activity);
                Native.a().a(activity);
                Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_RESUME);
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onConfigurationChanged(Configuration configuration) {
        ae aeVar = (ae) ab.b().y();
        if (aeVar == null) {
            return;
        }
        if ((aeVar.a(configuration) || !aeVar.h()) && ab.a().d() == u.VISIBLE) {
            aeVar.i(false);
            ab.a(Appodeal.f, new d(ab.a().b()));
        }
    }

    public void onLowMemory() {
    }
}
