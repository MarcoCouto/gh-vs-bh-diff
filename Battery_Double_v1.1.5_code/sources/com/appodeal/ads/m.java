package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.api.Stats;
import com.appodeal.ads.api.Stats.Builder;
import com.appodeal.ads.b.d;
import com.appodeal.ads.c.a;
import com.appodeal.ads.i;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

public abstract class m<AdObjectType extends i> {
    private boolean A = false;
    private boolean B = false;
    private boolean C = false;
    private boolean D = false;
    private boolean E = false;
    private boolean F = false;
    private JSONObject G;
    private o<AdObjectType> H = new o<AdObjectType>() {
    };
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    public List<JSONObject> f1649a = new ArrayList(0);
    @VisibleForTesting
    public List<JSONObject> b = new ArrayList(0);
    @VisibleForTesting
    long c = 0;
    private List<JSONObject> d = new ArrayList(0);
    private final List<AdObjectType> e = new CopyOnWriteArrayList();
    private final List<AdObjectType> f = new CopyOnWriteArrayList();
    private final List<AdObjectType> g = new CopyOnWriteArrayList();
    private ArrayList<bo> h = new ArrayList<>();
    private boolean i;
    private boolean j;
    private String k;
    private Long l = null;
    private JSONObject m;
    private long n = 0;
    private long o = 0;
    private final Map<String, AdObjectType> p = new HashMap();
    private String q = UUID.randomUUID().toString();
    private AdObjectType r;
    private double s;
    private boolean t = false;
    private boolean u = false;
    private boolean v = false;
    private boolean w = false;
    private boolean x = false;
    private boolean y = false;
    private boolean z = false;

    public m(@Nullable n nVar) {
        if (nVar != null) {
            this.i = nVar.a();
            this.F = nVar.b();
            this.j = nVar.d();
        }
    }

    private void a(@Nullable AdUnit adUnit, @Nullable String str) {
        if (adUnit != null && adUnit.getRequestResult() != t.TimeOutReached && !t() && !q()) {
            Log.log(T().getDisplayName(), LogConstants.EVENT_NETWORK_ERROR, String.format("%s - %s", new Object[]{br.c(adUnit.getStatus()), str}));
        }
    }

    private void c(@NonNull bo boVar) {
        boVar.b(System.currentTimeMillis());
    }

    private void d(JSONObject jSONObject) {
        this.f1649a.add(jSONObject);
    }

    private boolean o(@Nullable AdObjectType adobjecttype) {
        return (adobjecttype == null || adobjecttype.c() == null || TextUtils.isEmpty(adobjecttype.getId())) ? false : true;
    }

    /* access modifiers changed from: 0000 */
    public boolean A() {
        return !this.b.isEmpty();
    }

    @Nullable
    public AdObjectType B() {
        return this.r;
    }

    public double C() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    public void D() {
        if (this.r != null) {
            this.r.q();
            this.r = null;
            this.H.a();
            this.t = false;
            this.u = false;
        }
    }

    public String E() {
        return this.q;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, AdObjectType> F() {
        return this.p;
    }

    /* access modifiers changed from: 0000 */
    public void G() {
        try {
            Iterator it = this.p.values().iterator();
            while (it.hasNext()) {
                i iVar = (i) it.next();
                if (iVar != null) {
                    iVar.q();
                }
                it.remove();
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    /* access modifiers changed from: 0000 */
    public List<AdObjectType> H() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public boolean I() {
        return !this.g.isEmpty();
    }

    public boolean J() {
        return this.v && System.currentTimeMillis() - this.n <= 120000;
    }

    /* access modifiers changed from: 0000 */
    public boolean K() {
        return !this.B && (this.t || this.u);
    }

    /* access modifiers changed from: 0000 */
    public boolean L() {
        return !this.B && !this.t && this.u;
    }

    /* access modifiers changed from: 0000 */
    public boolean M() {
        return !a() && ((!this.t && !J()) || this.B);
    }

    @VisibleForTesting(otherwise = 3)
    public int N() {
        return this.f1649a.size() + this.b.size();
    }

    /* access modifiers changed from: 0000 */
    public long O() {
        if (this.c == 0) {
            this.c = System.currentTimeMillis() / 1000;
        }
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void P() {
        if (this.A) {
            this.f1649a.clear();
            this.b.clear();
            this.g.clear();
            this.e.clear();
            this.f.clear();
            this.h.clear();
            this.D = true;
            D();
            G();
        }
    }

    /* access modifiers changed from: 0000 */
    public void Q() {
        this.B = false;
        this.A = false;
        this.u = false;
        this.t = false;
        this.x = false;
        this.z = false;
        this.C = false;
        this.y = false;
    }

    /* access modifiers changed from: 0000 */
    public void R() {
        this.E = true;
    }

    /* access modifiers changed from: 0000 */
    public Builder S() {
        Builder newBuilder = Stats.newBuilder();
        newBuilder.setStart(this.n);
        newBuilder.setFinish(this.o);
        newBuilder.setSuccessful(this.t || this.u);
        newBuilder.setCompleted(this.w);
        Iterator it = this.h.iterator();
        while (it.hasNext()) {
            bo boVar = (bo) it.next();
            if (boVar.getRequestResult() != null) {
                newBuilder.addAdUnit(boVar.a());
            }
        }
        a(newBuilder);
        return newBuilder;
    }

    public abstract AdType T();

    /* access modifiers changed from: 0000 */
    public AdObjectType a(AdObjectType adobjecttype) {
        this.H.a(this, adobjecttype);
        return this.H.b() != null ? this.H.b() : adobjecttype;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public JSONObject a(int i2) {
        if (i2 < this.f1649a.size()) {
            return (JSONObject) this.f1649a.get(i2);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject a(int i2, boolean z2, boolean z3) {
        JSONObject jSONObject;
        List<JSONObject> list;
        if (z2) {
            jSONObject = (JSONObject) this.b.get(i2);
            if (!this.j) {
                list = this.b;
            }
            if (z3 && !this.j) {
                this.f1649a.clear();
                this.b.clear();
            }
            return jSONObject;
        }
        jSONObject = (JSONObject) this.f1649a.get(i2);
        if (!this.j) {
            list = this.f1649a;
        }
        this.f1649a.clear();
        this.b.clear();
        return jSONObject;
        list.remove(i2);
        this.f1649a.clear();
        this.b.clear();
        return jSONObject;
    }

    /* access modifiers changed from: 0000 */
    public void a(double d2) {
        this.s = d2;
    }

    public void a(AdUnit adUnit) {
        if (adUnit != null) {
            for (AdObjectType adobjecttype : this.e) {
                if (adobjecttype.getId().equals(adUnit.getId())) {
                    this.e.remove(adobjecttype);
                    return;
                }
            }
            this.h.remove(adUnit);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable AdUnit adUnit, @Nullable String str, @Nullable Object obj) {
        if (str != null && obj != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("(");
            sb.append(obj);
            sb.append(") ");
            sb.append(str);
            str = sb.toString();
        } else if (str == null) {
            str = "(network not provided any appropriate text or code)";
        }
        a(adUnit, str);
    }

    /* access modifiers changed from: protected */
    public void a(Builder builder) {
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull bo boVar) {
        this.h.add(boVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull bo boVar, @Nullable LoadingError loadingError) {
        boVar.a(loadingError != null ? loadingError.getRequestResult() : t.Exception);
        c(boVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull a aVar) {
        this.f1649a = aVar.b();
        this.b = aVar.a();
        this.d = aVar.c();
    }

    /* access modifiers changed from: 0000 */
    public void a(p<AdObjectType, ?, ?> pVar, boolean z2) {
        a(pVar, z2, false);
    }

    /* access modifiers changed from: 0000 */
    public void a(p<AdObjectType, ?, ?> pVar, boolean z2, boolean z3) {
        if (!this.v && z2) {
            this.n = System.currentTimeMillis();
            this.w = false;
        } else if (this.v && !z2) {
            this.o = System.currentTimeMillis();
            this.w = z3;
            Iterator it = this.h.iterator();
            while (it.hasNext()) {
                bo boVar = (bo) it.next();
                if (boVar.getRequestResult() == null) {
                    a(boVar, LoadingError.Canceled);
                    pVar.a(LogConstants.EVENT_CANCEL, (AdUnit) boVar, (LoadingError) null);
                }
            }
        }
        this.v = z2;
    }

    @VisibleForTesting(otherwise = 3)
    public void a(Long l2) {
        this.l = l2;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.k = str;
    }

    public void a(JSONObject jSONObject) {
        this.m = jSONObject;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        this.t = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull AdObjectType adobjecttype, d dVar, int i2) {
        boolean z2 = true;
        try {
            if (adobjecttype.h()) {
                int i3 = 0;
                boolean z3 = true;
                while (i3 < adobjecttype.i().size()) {
                    String str = (String) adobjecttype.i().get(i3);
                    if (!d(str)) {
                        return true;
                    }
                    i iVar = (i) this.p.get(str);
                    if (iVar == null || dVar.a(Appodeal.f, i2, iVar.getEcpm())) {
                        i3++;
                        z3 = false;
                    } else {
                        e(iVar.getId());
                        return true;
                    }
                }
                z2 = z3;
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
        return z2;
    }

    /* access modifiers changed from: 0000 */
    public AdObjectType b(String str) {
        return (str == null || !d(str)) ? B() : (i) this.p.get(str);
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull bo boVar) {
        this.h.remove(boVar);
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable AdObjectType adobjecttype) {
        if (adobjecttype != null && !this.e.contains(adobjecttype)) {
            this.e.add(adobjecttype);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(JSONObject jSONObject) {
        this.G = jSONObject;
        if (jSONObject != null) {
            d(jSONObject);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z2) {
        this.u = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public AdObjectType c(String str) {
        AdObjectType b2 = b(str);
        e(b2);
        return b2;
    }

    @NonNull
    public List<JSONObject> c() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public void c(@Nullable AdObjectType adobjecttype) {
        this.e.remove(adobjecttype);
    }

    /* access modifiers changed from: 0000 */
    public void c(JSONObject jSONObject) {
        this.f1649a.remove(this.f1649a.size() - 1);
        this.f1649a.add(0, jSONObject);
    }

    /* access modifiers changed from: 0000 */
    public void c(boolean z2) {
        this.x = z2;
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public void d(AdObjectType adobjecttype) {
        if (adobjecttype != null && !this.f.contains(adobjecttype)) {
            this.f.add(adobjecttype);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(boolean z2) {
        this.y = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean d(String str) {
        return this.p.containsKey(str);
    }

    public Long e() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public void e(AdObjectType adobjecttype) {
        this.r = adobjecttype;
    }

    /* access modifiers changed from: 0000 */
    public void e(String str) {
        try {
            Iterator it = this.p.values().iterator();
            while (it.hasNext()) {
                if (((i) it.next()).getId().equals(str)) {
                    it.remove();
                }
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(boolean z2) {
        this.z = z2;
    }

    public String f() {
        return this.l == null ? "-1" : this.l.toString();
    }

    /* access modifiers changed from: 0000 */
    public void f(boolean z2) {
        this.A = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean f(@Nullable i iVar) {
        return (iVar == null || this.r == null || this.r != iVar) ? false : true;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject g() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public void g(@NonNull AdObjectType adobjecttype) {
        Map<String, AdObjectType> map;
        int i2 = 0;
        while (i2 < adobjecttype.i().size()) {
            try {
                String str = (String) adobjecttype.i().get(i2);
                i iVar = (i) this.p.get(str);
                if (iVar == null) {
                    map = this.p;
                } else if (adobjecttype.getEcpm() > iVar.getEcpm()) {
                    map = this.p;
                } else {
                    i2++;
                }
                map.put(str, adobjecttype);
                i2++;
            } catch (Exception e2) {
                Log.log(e2);
                return;
            }
        }
    }

    public void g(boolean z2) {
        this.B = z2;
    }

    /* access modifiers changed from: 0000 */
    public void h(AdObjectType adobjecttype) {
        this.g.add(adobjecttype);
    }

    public void h(boolean z2) {
        this.C = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean h() {
        return this.t;
    }

    /* access modifiers changed from: 0000 */
    public void i(AdObjectType adobjecttype) {
        this.g.remove(adobjecttype);
    }

    public void i(boolean z2) {
        this.F = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean i() {
        return this.u;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject j(boolean z2) {
        JSONObject jSONObject = (!z2 || this.b == null || this.b.size() <= 0) ? null : (JSONObject) this.b.get(0);
        return (jSONObject != null || this.f1649a == null || this.f1649a.size() <= 0) ? jSONObject : (JSONObject) this.f1649a.get(0);
    }

    /* access modifiers changed from: 0000 */
    public boolean j() {
        return !this.e.isEmpty() || !this.f.isEmpty();
    }

    /* access modifiers changed from: 0000 */
    public boolean j(AdObjectType adobjecttype) {
        return this.g.contains(adobjecttype);
    }

    public List<AdObjectType> k() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public boolean k(AdObjectType adobjecttype) {
        for (AdObjectType ecpm : this.g) {
            if (ecpm.getEcpm() > adobjecttype.getEcpm()) {
                return true;
            }
        }
        return false;
    }

    public List<AdObjectType> l() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void l(AdObjectType adobjecttype) {
    }

    /* access modifiers changed from: 0000 */
    public void m(@Nullable AdObjectType adobjecttype) {
        if (o(adobjecttype)) {
            adobjecttype.c().a(System.currentTimeMillis());
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean m() {
        return this.x;
    }

    /* access modifiers changed from: 0000 */
    public void n(@Nullable AdObjectType adobjecttype) {
        if (o(adobjecttype)) {
            adobjecttype.c().a(t.Successful);
            c(adobjecttype.c());
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean n() {
        return this.y;
    }

    /* access modifiers changed from: 0000 */
    public boolean o() {
        return this.z;
    }

    /* access modifiers changed from: 0000 */
    public boolean p() {
        return this.A;
    }

    public boolean q() {
        return this.B;
    }

    public boolean r() {
        return this.C;
    }

    /* access modifiers changed from: 0000 */
    public boolean s() {
        return this.D;
    }

    /* access modifiers changed from: 0000 */
    public boolean t() {
        return this.E;
    }

    public boolean u() {
        return this.F;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject v() {
        return this.G;
    }

    public List<JSONObject> w() {
        return this.f1649a;
    }

    public List<JSONObject> x() {
        return this.b;
    }

    @VisibleForTesting(otherwise = 3)
    public int y() {
        return this.f1649a.size();
    }

    /* access modifiers changed from: 0000 */
    public boolean z() {
        return !this.f1649a.isEmpty();
    }
}
