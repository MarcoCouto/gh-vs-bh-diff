package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;

class ad extends bv<ae, UnifiedBanner, UnifiedBannerParams, UnifiedBannerCallback> {
    @Deprecated
    int e;
    /* access modifiers changed from: private */
    public int f = -1;

    private class a extends UnifiedBannerCallback {
        private a() {
        }

        public void onAdClicked() {
            ab.c().a(ad.this.a(), ad.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            ab.c().a(ad.this.a(), ad.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdExpired() {
            ab.c().i(ad.this.a(), ad.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            ad.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            ab.c().b(ad.this.a(), ad.this, loadingError);
        }

        public void onAdLoaded(View view, int i, int i2) {
            ad.this.a(view);
            ad.this.e = i2;
            ad.this.f = view.getResources().getConfiguration().orientation;
            ab.c().b(ad.this.a(), ad.this);
        }

        public void onAdShowFailed() {
            ab.c().a(ad.this.a(), ad.this, null, LoadingError.ShowFailed);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((ae) ad.this.a()).a((AdUnit) ad.this, str, obj);
        }
    }

    private class b implements UnifiedBannerParams {
        private b() {
        }

        public int getMaxHeight(@NonNull Context context) {
            return ab.d();
        }

        public int getMaxWidth(@NonNull Context context) {
            return ab.e();
        }

        public boolean needLeaderBoard(@NonNull Context context) {
            return ab.f();
        }

        public String obtainPlacementId() {
            return ab.b().u();
        }

        public String obtainSegmentId() {
            return ab.b().s();
        }

        public boolean useSmartBanners(@NonNull Context context) {
            return ab.b;
        }
    }

    public ad(@NonNull ae aeVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
        super(aeVar, adNetwork, boVar, 5000);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull Configuration configuration) {
        UnifiedBanner unifiedBanner = (UnifiedBanner) n();
        return (unifiedBanner == null || !unifiedBanner.isRefreshOnRotate() || this.f == -1 || this.f == configuration.orientation) ? false : true;
    }

    /* access modifiers changed from: protected */
    public int b(Context context) {
        if (ab.b && b().isSupportSmartBanners()) {
            return -1;
        }
        return Math.round(br.h(context) * (ab.f() ? 728.0f : 320.0f));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedBanner a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createBanner();
    }

    /* access modifiers changed from: protected */
    public int c(Context context) {
        return Math.round(((float) this.e) * br.h(context));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedBannerParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedBannerCallback o() {
        return new a();
    }

    public int u() {
        return this.e;
    }
}
