package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.b.e;
import org.json.JSONObject;

final class be {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static c f1597a;
    @VisibleForTesting
    static b b;
    private static bt<bu, bs> c;

    static class a extends n<a> {
        a() {
            super("video", "debug_video");
        }
    }

    @VisibleForTesting(otherwise = 3)
    static class b extends p<bs, bu, a> {
        b(q<bs, bu, ?> qVar) {
            super(qVar, e.c(), 2, 2, 3);
            this.e = 1.1f;
            this.f = 1.4f;
        }

        /* access modifiers changed from: protected */
        public bs a(@NonNull bu buVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
            return new bs(buVar, adNetwork, boVar);
        }

        /* access modifiers changed from: protected */
        public bu a(a aVar) {
            return new bu(aVar);
        }

        public void a(Activity activity) {
            if (l() && r()) {
                bu buVar = (bu) y();
                if (buVar == null || buVar.M()) {
                    d((Context) activity);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(Context context, a aVar) {
            if (Appodeal.d) {
                br.a((Runnable) new Runnable() {
                    public void run() {
                        ap.a().b.a(null, null, (LoadingError) null);
                    }
                });
            } else {
                super.a(context, aVar);
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
        }

        /* access modifiers changed from: protected */
        public boolean a(AdNetwork adNetwork, JSONObject jSONObject, String str, boolean z) {
            if (!z && adNetwork != null && adNetwork.isVideoShowing() && x().size() > 1) {
                bu buVar = (bu) A();
                bu buVar2 = (bu) z();
                if (!(buVar == null || buVar2 == null || buVar2.B() == null)) {
                    if (str.equals(((bs) buVar2.B()).getId())) {
                        buVar.b(jSONObject);
                    }
                    be.a(buVar, 0, false, false);
                    return true;
                }
            }
            return super.a(adNetwork, jSONObject, str, z);
        }

        /* access modifiers changed from: protected */
        public boolean a(bu buVar, int i) {
            if (buVar.y() != 1 || buVar.v() == null || buVar.v() != buVar.a(i)) {
                return super.a(buVar, i);
            }
            String optString = buVar.v().optString("status");
            boolean z = false;
            if (TextUtils.isEmpty(optString)) {
                return false;
            }
            AdNetwork c = q().c(optString);
            if (c != null && c.isVideoShowing()) {
                z = true;
            }
            return z;
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            ap.a().b.a(context, new a());
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "video_disabled";
        }

        public boolean r() {
            return ap.a().b();
        }
    }

    @VisibleForTesting
    static class c extends bd<bs, bu> {
        c() {
            super(ap.a().b);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void c(bu buVar) {
            be.a(buVar, 0, false, true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void d(bu buVar, bs bsVar) {
            super.d(buVar, bsVar);
            if (buVar.v() == bsVar.getJsonData()) {
                buVar.b((JSONObject) null);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void c(bu buVar, bs bsVar, LoadingError loadingError) {
            super.c(buVar, bsVar, loadingError);
            ap.a().b.b();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(bu buVar, bs bsVar, boolean z) {
            super.b(buVar, bsVar, z);
            if (!bsVar.h() && e(buVar, bsVar)) {
                be.a(Appodeal.e, new l(this.f1662a.t()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(bu buVar, bs bsVar, Object obj) {
            return false;
        }

        /* access modifiers changed from: 0000 */
        public void b(bu buVar) {
            be.a(buVar, 0, false, false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void q(bu buVar, bs bsVar) {
            bsVar.b().setVideoShowing(true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(@Nullable bu buVar, @Nullable bs bsVar, @Nullable LoadingError loadingError) {
            super.a(buVar, bsVar, loadingError);
            am.a();
            if (buVar != null && !this.f1662a.x().isEmpty()) {
                ap.a().b.b();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void e(bu buVar) {
            if (!buVar.a() && ap.a().b()) {
                bu buVar2 = (bu) this.f1662a.y();
                if (buVar2 == null || buVar2.M()) {
                    this.f1662a.d(Appodeal.f);
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void n(bu buVar, bs bsVar) {
            be.d().b();
            am.a();
            this.f1662a.d(null);
            bsVar.b().setVideoShowing(false);
            f(buVar);
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean a(bu buVar) {
            return buVar.v() == null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean e(bu buVar, bs bsVar) {
            return !e(buVar, bsVar) && ((buVar.y() > 0 && buVar.a(0) == buVar.v()) || super.e(buVar, bsVar));
        }

        /* access modifiers changed from: protected */
        public boolean e(bu buVar, bs bsVar) {
            return buVar.u();
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public boolean l(bu buVar, bs bsVar) {
            return buVar.v() == null || (bsVar != null && buVar.v().optString("id").equals(bsVar.getId()));
        }
    }

    static p<bs, bu, a> a() {
        if (b == null) {
            b = new b(b());
        }
        return b;
    }

    static void a(bu buVar, int i, boolean z, boolean z2) {
        a().a(buVar, i, z2, z);
    }

    static boolean a(Activity activity, l lVar) {
        return d().a(activity, lVar, a());
    }

    static q<bs, bu, Object> b() {
        if (f1597a == null) {
            f1597a = new c();
        }
        return f1597a;
    }

    /* access modifiers changed from: private */
    public static bt<bu, bs> d() {
        if (c == null) {
            c = new bt<>("debug_video");
        }
        return c;
    }
}
