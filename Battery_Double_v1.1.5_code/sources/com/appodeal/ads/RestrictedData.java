package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.UserSettings.Gender;

public interface RestrictedData {
    boolean canSendLocation();

    boolean canSendLocationType();

    boolean canSendUserSettings();

    @Nullable
    String getAddress();

    @Nullable
    Integer getAge();

    @Nullable
    String getCity();

    @Nullable
    ConnectionData getConnectionData(@NonNull Context context);

    @Nullable
    String getCountry();

    @Nullable
    Gender getGender();

    @Nullable
    String getHttpAgent(@NonNull Context context);

    @NonNull
    String getIfa();

    @Nullable
    String getIp();

    @Nullable
    String getIpv6();

    @NonNull
    LocationData getLocation(@NonNull Context context);

    @Nullable
    String getUserId();

    @Nullable
    String getZip();

    boolean isLimitAdTrackingEnabled();

    boolean isParameterBlocked(String str);

    boolean isUserAgeRestricted();

    boolean isUserGdprProtected();

    boolean isUserHasConsent();

    boolean isUserInGdprScope();
}
