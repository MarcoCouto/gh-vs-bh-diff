package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.utils.b.a;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

class bf {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static Set<String> f1599a = new HashSet(i);
    private static JSONObject b = null;
    private static String c = null;
    private static boolean d = true;
    private static boolean e = true;
    private static boolean f = false;
    private static boolean g = true;
    private static boolean h;
    private static Set<String> i = new HashSet<String>() {
        {
            add("lt");
            add("lat");
            add("lon");
            add("ad_stats");
            add("user_settings");
            add("inapps");
        }
    };

    static JSONObject a() {
        return b;
    }

    static void a(@Nullable Context context, @Nullable JSONObject jSONObject) {
        if (context != null && jSONObject != null) {
            f1599a.clear();
            if (jSONObject.has("gdpr")) {
                d = true;
                JSONObject optJSONObject = jSONObject.optJSONObject("gdpr");
                if (optJSONObject == null || !optJSONObject.has("do_not_collect")) {
                    f1599a.addAll(i);
                } else {
                    JSONArray optJSONArray = optJSONObject.optJSONArray("do_not_collect");
                    if (optJSONArray != null) {
                        for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                            String optString = optJSONArray.optString(i2, null);
                            if (optString != null) {
                                f1599a.add(optString);
                            }
                        }
                    }
                }
            } else {
                d = false;
            }
            if (jSONObject.has(RequestParameters.CONSENT)) {
                g = jSONObject.optBoolean(RequestParameters.CONSENT);
            }
        }
    }

    static void a(String str) {
        c = str;
    }

    static void a(JSONObject jSONObject) {
        b = jSONObject;
    }

    static void a(@Nullable JSONObject jSONObject, @NonNull RestrictedData restrictedData) {
        if (restrictedData.isUserGdprProtected() && jSONObject != null) {
            for (String remove : f1599a) {
                jSONObject.remove(remove);
            }
        }
    }

    static void a(boolean z) {
        if (e() != z) {
            e = z;
            if (Appodeal.c && d()) {
                aa.a();
            }
        }
    }

    static boolean a(@NonNull a aVar) {
        if (aVar.b() == f()) {
            return false;
        }
        boolean g2 = g();
        b(aVar.b());
        a(aVar.a());
        return g2 != g();
    }

    static JSONObject b() {
        JSONObject a2 = x.a();
        if (a2 == null) {
            return null;
        }
        JSONObject optJSONObject = a2.optJSONObject("token");
        if (optJSONObject == null) {
            optJSONObject = a2.optJSONObject("fingerprint");
        }
        return optJSONObject;
    }

    static void b(boolean z) {
        f = z;
    }

    static boolean b(@Nullable String str) {
        return f1599a.contains(str);
    }

    static boolean c() {
        return g && !f && e;
    }

    static boolean d() {
        return d;
    }

    static boolean e() {
        return e;
    }

    static boolean f() {
        return f;
    }

    static boolean g() {
        return d() && !c();
    }

    @Nullable
    static String h() {
        if (c == null) {
            h = true;
            return (!g() || c()) ? br.j(Appodeal.f) : "00000000-0000-0000-0000-000000000000";
        }
        h = false;
        return c;
    }

    static boolean i() {
        return h;
    }
}
