package com.appodeal.ads;

import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import org.json.JSONException;
import org.json.JSONObject;

class aq extends m<ao> {
    aq(@Nullable c cVar) {
        super(cVar);
    }

    public AdType T() {
        return AdType.Interstitial;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void l(ao aoVar) {
        super.l(aoVar);
        try {
            a(new JSONObject().put("type", "banner"));
        } catch (JSONException e) {
            Log.log(e);
        }
    }
}
