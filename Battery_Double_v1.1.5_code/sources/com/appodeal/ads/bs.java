package com.appodeal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;

class bs extends al<bu, UnifiedVideo, UnifiedVideoParams, UnifiedVideoCallback> {

    private final class a extends UnifiedVideoCallback {
        private a() {
        }

        public void onAdClicked() {
            be.b().a(bs.this.a(), bs.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            be.b().a(bs.this.a(), bs.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdClosed() {
            be.b().m(bs.this.a(), bs.this);
        }

        public void onAdExpired() {
            be.b().i(bs.this.a(), bs.this);
        }

        public void onAdFinished() {
            be.b().o(bs.this.a(), bs.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            bs.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            be.b().b(bs.this.a(), bs.this, loadingError);
        }

        public void onAdLoaded() {
            be.b().b(bs.this.a(), bs.this);
        }

        public void onAdShowFailed() {
            be.b().a(bs.this.a(), bs.this, null, LoadingError.ShowFailed);
        }

        public void onAdShown() {
            be.b().p(bs.this.a(), bs.this);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((bu) bs.this.a()).a((AdUnit) bs.this, str, obj);
        }
    }

    private final class b implements UnifiedVideoParams {
        private b() {
        }

        public int getAfd() {
            return be.a().D();
        }

        public String obtainPlacementId() {
            return be.a().u();
        }

        public String obtainSegmentId() {
            return be.a().s();
        }
    }

    bs(@NonNull bu buVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
        super(buVar, adNetwork, boVar, 10000);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedVideo a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createVideo();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedVideoParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: protected */
    public LoadingError s() {
        return b().isVideoShowing() ? LoadingError.Canceled : super.s();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedVideoCallback o() {
        return new a();
    }
}
