package com.appodeal.ads;

public interface NonSkippableVideoCallbacks {
    void onNonSkippableVideoClosed(boolean z);

    void onNonSkippableVideoExpired();

    void onNonSkippableVideoFailedToLoad();

    void onNonSkippableVideoFinished();

    void onNonSkippableVideoLoaded(boolean z);

    void onNonSkippableVideoShowFailed();

    void onNonSkippableVideoShown();
}
