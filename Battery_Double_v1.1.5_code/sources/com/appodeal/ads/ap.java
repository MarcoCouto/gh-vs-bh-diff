package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;

class ap {
    private static volatile ap c;

    /* renamed from: a reason: collision with root package name */
    final a<aq, ao, c> f1538a = new a<aq, ao, c>() {
        /* access modifiers changed from: 0000 */
        @NonNull
        public p<ao, aq, c> a() {
            return an.a();
        }
    };
    final a<bu, bs, a> b = new a<bu, bs, a>() {
        /* access modifiers changed from: 0000 */
        @NonNull
        public p<bs, bu, a> a() {
            return be.a();
        }
    };
    /* access modifiers changed from: private */
    @Nullable
    public InterstitialCallbacks d;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = true;

    abstract class a<AdRequestType extends m<AdObjectType>, AdObjectType extends i, RequestParamsType extends n> extends c<AdRequestType, AdObjectType, Object> {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public a f1541a;
        @VisibleForTesting
        boolean b = false;
        private boolean d = false;
        private boolean e = true;

        a() {
        }

        private void a(boolean z) {
            this.b = false;
            if (!ap.this.e) {
                ap.this.e = true;
                Appodeal.b();
                Log.log("Interstitial", LogConstants.EVENT_NOTIFY_LOADED, String.format("isPrecache: %s", new Object[]{Boolean.valueOf(z)}), LogLevel.verbose);
                if (ap.this.d != null) {
                    ap.this.d.onInterstitialLoaded(z);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public abstract p<AdObjectType, AdRequestType, RequestParamsType> a();

        /* access modifiers changed from: 0000 */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0050, code lost:
            if (a().F() == false) goto L_0x0053;
         */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0056  */
        public void a(Context context, RequestParamsType requestparamstype) {
            if (requestparamstype.a()) {
                a().b(context, requestparamstype);
                return;
            }
            boolean z = true;
            if (this.e) {
                this.e = false;
                this.d = true;
                this.b = false;
                m y = a().y();
                if (y == null || !y.h() || a().F()) {
                    if (y != null) {
                        if (!y.M()) {
                        }
                    }
                    if (z) {
                        if (requestparamstype.a()) {
                            requestparamstype.b(false);
                        }
                        a().b(context, requestparamstype);
                    }
                }
                a(y.B().isPrecache());
            }
            z = false;
            if (z) {
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype) {
            a(adobjecttype.isPrecache());
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable LoadingError loadingError) {
            this.b = true;
            if (!this.f1541a.d || this.f1541a.b || this.f1541a.a().j()) {
                this.e = true;
                Log.log("Interstitial", LogConstants.EVENT_NOTIFY_LOAD_FAILED, LogLevel.verbose);
                if (ap.this.d != null) {
                    ap.this.d.onInterstitialFailedToLoad();
                }
                if (this.f1541a.d && this.f1541a.b) {
                    this.f1541a.e = true;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable Object obj) {
            Log.log("Interstitial", LogConstants.EVENT_NOTIFY_SHOWN, LogLevel.verbose);
            if (ap.this.d != null) {
                ap.this.d.onInterstitialShown();
            }
            ap.this.e = false;
            this.d = false;
            this.b = false;
            this.e = true;
            if (this.f1541a.d && this.f1541a.b) {
                this.f1541a.e = true;
            } else if (Appodeal.isLoaded(this.f1541a.a().m())) {
                this.f1541a.a(Appodeal.isPrecache(this.f1541a.a().m()));
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable Object obj, @Nullable LoadingError loadingError) {
            this.b = true;
            Log.log("Interstitial", LogConstants.EVENT_NOTIFY_SHOW_FAILED, LogLevel.verbose);
            if (ap.this.d != null) {
                ap.this.d.onInterstitialShowFailed();
            }
            if (!this.f1541a.d || this.f1541a.b || this.f1541a.a().j()) {
                this.e = true;
                if (this.f1541a.d && this.f1541a.b) {
                    this.f1541a.e = true;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.e = true;
        }

        /* access modifiers changed from: 0000 */
        public void b(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype) {
            if (a().r()) {
                b();
                a().d(Appodeal.f);
            }
            m y = this.f1541a.a().y();
            if (y == null || !y.h() || this.f1541a.a().F()) {
                Log.log("Interstitial", LogConstants.EVENT_NOTIFY_EXPIRED, LogLevel.verbose);
                if (ap.this.d != null) {
                    ap.this.d.onInterstitialExpired();
                }
                if (ap.this.f) {
                    ap.this.e = false;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype, @Nullable Object obj) {
            Log.log("Interstitial", LogConstants.EVENT_NOTIFY_CLICKED, LogLevel.verbose);
            if (ap.this.d != null) {
                ap.this.d.onInterstitialClicked();
            }
        }

        /* access modifiers changed from: 0000 */
        public void c(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype) {
            Log.log("Interstitial", LogConstants.EVENT_NOTIFY_CLOSED, LogLevel.verbose);
            if (ap.this.d != null) {
                ap.this.d.onInterstitialClosed();
            }
        }
    }

    private ap() {
        this.f1538a.f1541a = this.b;
        this.b.f1541a = this.f1538a;
    }

    static ap a() {
        if (c == null) {
            synchronized (ap.class) {
                if (c == null) {
                    c = new ap();
                }
            }
        }
        return c;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable InterstitialCallbacks interstitialCallbacks) {
        this.d = interstitialCallbacks;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.f = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        this.e = false;
        this.f1538a.b();
        this.b.b();
    }
}
