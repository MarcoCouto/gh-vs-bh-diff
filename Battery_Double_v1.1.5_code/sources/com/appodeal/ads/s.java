package com.appodeal.ads;

import android.support.annotation.Nullable;
import com.appodeal.ads.api.Stats.AdUnit;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.github.mikephil.charting.utils.Utils;
import org.json.JSONObject;

class s implements bo {

    /* renamed from: a reason: collision with root package name */
    private JSONObject f1674a;
    private String b;
    private String c;
    private boolean d;
    private double e;
    private long f;
    private int g;
    private boolean h;
    private String i;
    private long j;
    private long k;
    private t l;

    s() {
    }

    public static bo a(JSONObject jSONObject, boolean z) {
        s sVar = new s();
        sVar.f1674a = jSONObject;
        sVar.b = jSONObject.optString("id");
        sVar.d = z;
        sVar.c = jSONObject.optString("status");
        sVar.e = jSONObject.optDouble(RequestInfoKeys.APPODEAL_ECPM, Utils.DOUBLE_EPSILON);
        sVar.f = jSONObject.optLong("exptime", 0);
        sVar.g = jSONObject.optInt("tmax", 0);
        sVar.h = jSONObject.optBoolean("async");
        sVar.i = br.a(jSONObject, "mediator");
        return sVar;
    }

    public AdUnit a() {
        return AdUnit.newBuilder().setId(getId()).setEcpm(this.e).setPrecache(isPrecache()).setStart(this.j).setFinish(this.k).setResult(this.l.a()).build();
    }

    public void a(double d2) {
        this.e = d2;
    }

    public void a(long j2) {
        this.j = j2;
    }

    public void a(t tVar) {
        this.l = tVar;
    }

    public void a(String str) {
        this.b = str;
    }

    public void a(boolean z) {
        this.d = z;
    }

    public void b(long j2) {
        this.k = j2;
    }

    public double getEcpm() {
        return this.e;
    }

    public long getExpTime() {
        return this.f;
    }

    public String getId() {
        return this.b;
    }

    public JSONObject getJsonData() {
        return this.f1674a;
    }

    public int getLoadingTimeout() {
        return this.g;
    }

    @Nullable
    public String getMediatorName() {
        return this.i;
    }

    public t getRequestResult() {
        return this.l;
    }

    public String getStatus() {
        return this.c;
    }

    public boolean isAsync() {
        return this.h;
    }

    public boolean isPrecache() {
        return this.d;
    }
}
