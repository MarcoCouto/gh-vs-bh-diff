package com.appodeal.ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.api.AdStats;
import com.appodeal.ads.api.App;
import com.appodeal.ads.api.Device;
import com.appodeal.ads.api.Device.Builder;
import com.appodeal.ads.api.Device.ConnectionType;
import com.appodeal.ads.api.Device.DeviceType;
import com.appodeal.ads.api.Extra;
import com.appodeal.ads.api.Geo;
import com.appodeal.ads.api.Geo.LocationType;
import com.appodeal.ads.api.Regs;
import com.appodeal.ads.api.Request;
import com.appodeal.ads.api.Session;
import com.appodeal.ads.api.User;
import com.appodeal.ads.api.UserSettings;
import com.appodeal.ads.utils.EventsTracker;
import com.appodeal.ads.utils.EventsTracker.EventType;
import com.appodeal.ads.utils.y;
import com.appodeal.ads.v.c;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants;
import com.smaato.sdk.core.api.VideoType;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

class bg {
    @SuppressLint({"MissingPermission"})
    static ConnectionType a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            int type = activeNetworkInfo.getType();
            if (type == 9) {
                return ConnectionType.ETHERNET;
            }
            switch (type) {
                case 0:
                    int subtype = activeNetworkInfo.getSubtype();
                    return subtype != 0 ? subtype != 4 ? subtype != 16 ? ConnectionType.MOBILE_4G : ConnectionType.MOBILE_2G : ConnectionType.MOBILE_3G : ConnectionType.MOBILE_UNKNOWN;
                case 1:
                    return ConnectionType.WIFI;
            }
        }
        return ConnectionType.CONNECTIONTYPE_UNKNOWN;
    }

    static Device a(@NonNull Context context, @NonNull RestrictedData restrictedData) {
        Builder newBuilder = Device.newBuilder();
        String httpAgent = restrictedData.getHttpAgent(context);
        if (!TextUtils.isEmpty(httpAgent)) {
            newBuilder.setUa(httpAgent);
        }
        if (VERSION.RELEASE != null) {
            newBuilder.setOsv(VERSION.RELEASE);
        }
        Pair e = br.e(context);
        newBuilder.setOs(Constants.JAVASCRIPT_INTERFACE_NAME);
        if (e.first != null) {
            newBuilder.setW(((Integer) e.first).intValue());
        }
        if (e.second != null) {
            newBuilder.setH(((Integer) e.second).intValue());
        }
        newBuilder.setPxratio(br.h(context));
        newBuilder.setDevicetype(br.k(context) ? DeviceType.TABLET : DeviceType.PHONE);
        if (Build.MANUFACTURER != null) {
            newBuilder.setMake(Build.MANUFACTURER);
        }
        String format = String.format("%s %s", new Object[]{Build.MANUFACTURER, Build.MODEL});
        if (!TextUtils.isEmpty(format)) {
            newBuilder.setModel(format);
        }
        newBuilder.setConnectiontype(a(context));
        String c = br.c(context);
        if (c != null) {
            newBuilder.setMccmnc(c);
        }
        String locale = Locale.getDefault().toString();
        if (!TextUtils.isEmpty(locale)) {
            newBuilder.setLocale(locale);
        }
        newBuilder.setRooted(br.b());
        String q = br.q(context);
        if (q != null) {
            newBuilder.setWebviewVersion(q);
        }
        newBuilder.setBattery((int) br.i(context));
        newBuilder.setIfa(restrictedData.getIfa());
        newBuilder.setLmt(restrictedData.isLimitAdTrackingEnabled() ^ true ? 1 : 0);
        newBuilder.setAdidg(bf.i());
        return newBuilder.build();
    }

    static Extra a(@NonNull Context context, @Nullable i iVar, double d) {
        Extra.Builder newBuilder = Extra.newBuilder();
        newBuilder.setPriceFloor((float) d);
        if (!(iVar == null || iVar.g() == null)) {
            newBuilder.setAdUnitStat(iVar.g().toString());
        }
        JSONArray a2 = y.a(context);
        if (a2 != null) {
            String jSONArray = a2.toString();
            if (!TextUtils.isEmpty(jSONArray)) {
                newBuilder.addSa(jSONArray);
            }
        }
        return newBuilder.build();
    }

    static Regs a() {
        return Regs.newBuilder().setCoppa(w.b()).build();
    }

    static Request.Builder a(@NonNull Context context, @NonNull RestrictedData restrictedData, @Nullable m mVar, @Nullable i iVar, double d) throws NameNotFoundException {
        Request.Builder newBuilder = Request.newBuilder();
        newBuilder.setApp(b(context));
        newBuilder.setSession(a(context, restrictedData, mVar));
        newBuilder.setDevice(a(context, restrictedData));
        newBuilder.setUser(a(restrictedData));
        newBuilder.setRegs(a());
        newBuilder.setGeo(b(context, restrictedData));
        newBuilder.setExt(a(context, iVar, d));
        newBuilder.setTimestamp(System.currentTimeMillis());
        if (mVar != null) {
            String E = mVar.E();
            if (E != null) {
                newBuilder.setImpid(E);
            }
            String d2 = mVar.d();
            if (d2 != null) {
                newBuilder.setMainId(d2);
            }
        }
        return newBuilder;
    }

    static Session a(@NonNull Context context, @NonNull RestrictedData restrictedData, @Nullable m mVar) {
        Session.Builder newBuilder = Session.newBuilder();
        newBuilder.setTest(aa.b);
        String jSONObject = ExtraData.a().toString();
        if (jSONObject.length() != 0) {
            newBuilder.setExt(jSONObject);
        }
        JSONObject a2 = bf.a();
        if (a2 != null) {
            newBuilder.setToken(a2.toString());
        }
        newBuilder.setSessionId(Appodeal.getSession().d(context));
        String b = Appodeal.getSession().b();
        if (b != null) {
            newBuilder.setSessionUuid(b);
        }
        newBuilder.setSessionUptime(Appodeal.getSession().c());
        if (mVar != null) {
            Long e = mVar.e();
            if (e != null) {
                newBuilder.setSegmentId(e.intValue());
            }
        }
        if (!restrictedData.isParameterBlocked("ad_stats")) {
            newBuilder.setAdStats(b());
        }
        return newBuilder.build();
    }

    static User a(@NonNull RestrictedData restrictedData) {
        User.Builder newBuilder = User.newBuilder();
        String userId = restrictedData.getUserId();
        if (userId != null) {
            newBuilder.setId(userId);
        }
        newBuilder.setConsent(bf.e());
        newBuilder.setUserSettings(b(restrictedData));
        return newBuilder.build();
    }

    static void a(m mVar, i iVar) {
        new c(d.Stats).a(mVar.S().build()).a(mVar).a(iVar).b();
    }

    static AdStats b() {
        AdStats.Builder newBuilder = AdStats.newBuilder();
        newBuilder.setShow(EventsTracker.get().a(EventType.Impression));
        newBuilder.setClick(EventsTracker.get().a(EventType.Click));
        newBuilder.setFinish(EventsTracker.get().a(EventType.Finish));
        newBuilder.setBannerShow(EventsTracker.get().a(VideoType.INTERSTITIAL, EventType.Impression));
        newBuilder.setBannerClick(EventsTracker.get().a(VideoType.INTERSTITIAL, EventType.Click));
        newBuilder.setVideoShow(EventsTracker.get().a("video", EventType.Impression));
        newBuilder.setVideoClick(EventsTracker.get().a("video", EventType.Click));
        newBuilder.setVideoFinish(EventsTracker.get().a("video", EventType.Finish));
        newBuilder.setRewardedVideoShow(EventsTracker.get().a("rewarded_video", EventType.Impression));
        newBuilder.setRewardedVideoClick(EventsTracker.get().a("rewarded_video", EventType.Click));
        newBuilder.setRewardedVideoFinish(EventsTracker.get().a("rewarded_video", EventType.Finish));
        newBuilder.setBanner320Show(EventsTracker.get().a("banner", EventType.Impression));
        newBuilder.setBanner320Click(EventsTracker.get().a("banner", EventType.Click));
        newBuilder.setBannerMrecShow(EventsTracker.get().a("mrec", EventType.Impression));
        newBuilder.setBannerMrecClick(EventsTracker.get().a("mrec", EventType.Click));
        newBuilder.setNativeShow(EventsTracker.get().a("native", EventType.Impression));
        newBuilder.setNativeClick(EventsTracker.get().a("native", EventType.Click));
        return newBuilder.build();
    }

    static App b(@NonNull Context context) throws NameNotFoundException {
        SharedPreferences b = bm.a(context).b();
        App.Builder newBuilder = App.newBuilder();
        String packageName = context.getPackageName();
        if (packageName != null) {
            newBuilder.setBundle(packageName);
        }
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
        if (packageInfo.versionName != null) {
            newBuilder.setVer(packageInfo.versionName);
        }
        newBuilder.setInstallTime(packageInfo.firstInstallTime / 1000);
        String installerPackageName = packageManager.getInstallerPackageName(packageName);
        if (installerPackageName != null) {
            newBuilder.setInstaller(installerPackageName);
        }
        newBuilder.setMultidex(br.a());
        String string = b.getString(ServerResponseWrapper.APP_KEY_FIELD, null);
        if (string != null) {
            newBuilder.setAppKey(string);
        }
        newBuilder.setSdk("2.6.0");
        newBuilder.setVersionCode(packageInfo.versionCode);
        newBuilder.setAppUptime(Appodeal.getSession().e(context));
        if (Appodeal.frameworkName != null) {
            newBuilder.setFramework(Appodeal.frameworkName);
        }
        if (Appodeal.i != null) {
            newBuilder.setFrameworkVersion(Appodeal.i);
        }
        if (Appodeal.pluginVersion != null) {
            newBuilder.setPluginVersion(Appodeal.pluginVersion);
        }
        return newBuilder.build();
    }

    static Geo b(@NonNull Context context, @NonNull RestrictedData restrictedData) {
        Geo.Builder newBuilder = Geo.newBuilder();
        newBuilder.setUtcoffset((int) TimeUnit.MILLISECONDS.toMinutes((long) TimeZone.getDefault().getOffset(System.currentTimeMillis())));
        newBuilder.setLocalTime(System.currentTimeMillis() / 1000);
        LocationData location = restrictedData.getLocation(context);
        Integer deviceLocationType = location.getDeviceLocationType();
        if (deviceLocationType != null) {
            LocationType forNumber = LocationType.forNumber(deviceLocationType.intValue());
            if (forNumber != null) {
                newBuilder.setLt(forNumber);
            }
        }
        Float obtainLatitude = location.obtainLatitude();
        if (obtainLatitude != null) {
            newBuilder.setLat(obtainLatitude.floatValue());
        }
        Float obtainLongitude = location.obtainLongitude();
        if (obtainLongitude != null) {
            newBuilder.setLon(obtainLongitude.floatValue());
        }
        return newBuilder.build();
    }

    static UserSettings b(@NonNull RestrictedData restrictedData) {
        UserSettings.Builder newBuilder = UserSettings.newBuilder();
        String userId = restrictedData.getUserId();
        if (userId != null) {
            newBuilder.setUserId(userId);
        }
        Gender gender = restrictedData.getGender();
        if (gender != null) {
            newBuilder.setGender(gender.getStringValue());
        }
        Integer age = restrictedData.getAge();
        if (age != null) {
            newBuilder.setAge(age.intValue());
        }
        return newBuilder.build();
    }
}
