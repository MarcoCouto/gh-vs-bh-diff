package com.appodeal.ads.utils;

import com.appodeal.ads.br;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

public class v implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final String f1743a;
    private final String b;

    public v(String str, String str2) {
        this.f1743a = str;
        this.b = str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    public void run() {
        HttpURLConnection httpURLConnection;
        Throwable e;
        try {
            httpURLConnection = (HttpURLConnection) new URL(this.b).openConnection();
            try {
                httpURLConnection.setConnectTimeout(20000);
                httpURLConnection.setReadTimeout(20000);
                httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
                dataOutputStream.write(this.f1743a.getBytes(Charset.forName("UTF-8")));
                dataOutputStream.flush();
                dataOutputStream.close();
                br.a(httpURLConnection.getInputStream());
                if (httpURLConnection == null) {
                    return;
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    Log.log(e);
                    if (httpURLConnection == null) {
                        return;
                    }
                    httpURLConnection.disconnect();
                } catch (Throwable th) {
                    th = th;
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    throw th;
                }
            }
        } catch (Exception e3) {
            Throwable th2 = e3;
            httpURLConnection = null;
            e = th2;
            Log.log(e);
            if (httpURLConnection == null) {
            }
            httpURLConnection.disconnect();
        } catch (Throwable th3) {
            Throwable th4 = th3;
            httpURLConnection = null;
            th = th4;
            if (httpURLConnection != null) {
            }
            throw th;
        }
        httpURLConnection.disconnect();
    }
}
