package com.appodeal.ads.utils;

public class DependencyRule {

    /* renamed from: a reason: collision with root package name */
    private final String f1679a;
    private final String b;

    public DependencyRule(String str) {
        this(str, null);
    }

    public DependencyRule(String str, String str2) {
        this.f1679a = str;
        this.b = str2;
    }

    public String getDependency() {
        return this.f1679a;
    }

    public String getErrorMessage() {
        return this.b;
    }
}
