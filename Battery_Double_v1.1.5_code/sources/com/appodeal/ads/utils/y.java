package com.appodeal.ads.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;

public class y {

    /* renamed from: a reason: collision with root package name */
    private static final HashMap<String, String> f1747a = new HashMap<String, String>(3) {
        {
            put("android.permission.SYSTEM_ALERT_WINDOW", "SAW");
            put("android.permission.GET_TASKS", "GT");
            put("android.permission.RECEIVE_BOOT_COMPLETED", "RBC");
        }
    };

    private static List<String> a(PackageInfo packageInfo) {
        String[] strArr;
        ArrayList arrayList = new ArrayList();
        try {
            if (packageInfo.requestedPermissions != null) {
                for (String str : packageInfo.requestedPermissions) {
                    if (f1747a.containsKey(str)) {
                        arrayList.add(f1747a.get(str));
                    }
                }
            }
        } catch (Exception e) {
            Log.log(e);
        }
        return arrayList;
    }

    public static JSONArray a(Context context) {
        JSONArray jSONArray = new JSONArray();
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4100);
            for (String put : a(packageInfo)) {
                jSONArray.put(put);
            }
            if (b(packageInfo)) {
                jSONArray.put("S");
            }
        } catch (Exception e) {
            Log.log(e);
        }
        if (jSONArray.length() == 0) {
            return null;
        }
        return jSONArray;
    }

    private static boolean b(PackageInfo packageInfo) {
        try {
            if (packageInfo.services != null) {
                for (ServiceInfo serviceInfo : packageInfo.services) {
                    if (!serviceInfo.name.equals("com.yandex.metrica.MetricaService")) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            Log.log(e);
        }
        return false;
    }
}
