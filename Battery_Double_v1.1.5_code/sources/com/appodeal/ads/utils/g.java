package com.appodeal.ads.utils;

import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;

public class g {
    static StateListDrawable a(int i, int i2) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.setExitFadeDuration(Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(i2));
        stateListDrawable.addState(new int[0], new ColorDrawable(i));
        return stateListDrawable;
    }

    static ColorStateList b(int i, int i2) {
        return new ColorStateList(new int[][]{new int[]{-16842919}, new int[]{16842919}}, new int[]{i, i2});
    }
}
