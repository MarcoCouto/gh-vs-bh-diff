package com.appodeal.ads.utils;

public class j {

    /* renamed from: a reason: collision with root package name */
    public final int f1718a;
    public final int b;
    public final String c;
    public final String d;
    public final int e;
    public final double f;
    public final boolean g;
    public final int h;

    public j(int i, int i2, int i3, String str, String str2, String str3, String str4, boolean z) {
        this.f1718a = i;
        this.b = i2;
        this.h = i3;
        this.c = str;
        this.d = str2;
        this.e = Integer.parseInt(str3);
        this.f = Double.parseDouble(str4);
        this.g = z;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        j jVar = (j) obj;
        if (this.d != null) {
            z = this.d.equals(jVar.d);
        } else if (jVar.d != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        if (this.d != null) {
            return this.d.hashCode();
        }
        return 0;
    }
}
