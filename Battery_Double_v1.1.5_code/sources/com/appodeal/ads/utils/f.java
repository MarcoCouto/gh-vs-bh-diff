package com.appodeal.ads.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.vast.VastLog;
import com.explorestack.iab.vast.processor.url.UrlProcessor;

public class f implements UrlProcessor {
    @Nullable
    public String prepare(@Nullable String str, @Nullable Bundle bundle) {
        if (!TextUtils.isEmpty(str) && bundle != null && bundle.containsKey("segment_id")) {
            String string = bundle.getString("segment_id");
            if (string != null) {
                VastLog.d("AppodealXSegmentUrlProcessor", String.format("Before prepare url: %s", new Object[]{str}));
                if (str.contains("${APPODEALX_SEGMENT_ID}")) {
                    str = str.replace("${APPODEALX_SEGMENT_ID}", string);
                }
                if (str.contains("%24%7BAPPODEALX_SEGMENT_ID%7D")) {
                    str = str.replace("%24%7BAPPODEALX_SEGMENT_ID%7D", string);
                }
                VastLog.d("AppodealXSegmentUrlProcessor", String.format("After prepare url: %s", new Object[]{str}));
            }
        }
        return str;
    }
}
