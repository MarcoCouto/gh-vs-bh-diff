package com.appodeal.ads.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Pair;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.br;
import com.appodeal.ads.h;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class c {

    /* renamed from: a reason: collision with root package name */
    public static Set<String> f1710a = new HashSet();
    private static final ArrayList<String> b = new ArrayList<String>() {
        {
            add("android.permission.ACCESS_NETWORK_STATE");
            add("android.permission.INTERNET");
            add("android.permission.ACCESS_COARSE_LOCATION");
            add("android.permission.WRITE_EXTERNAL_STORAGE");
        }
    };
    private static final ArrayList<String> c = new ArrayList<String>() {
        {
            add("com.google.android.gms.version");
        }
    };
    private static PackageInfo d;
    private static PackageInfo e;
    private static PackageInfo f;
    private static PackageInfo g;

    private static PackageInfo a(PackageManager packageManager, String str) throws NameNotFoundException {
        if (d == null) {
            d = packageManager.getPackageInfo(str, 1);
        }
        return d;
    }

    public static void a() {
        b.remove("android.permission.ACCESS_COARSE_LOCATION");
    }

    public static void a(Activity activity) {
        try {
            PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 4096);
            ArrayList<String> arrayList = new ArrayList<>(b);
            if (packageInfo.requestedPermissions != null) {
                arrayList.removeAll(Arrays.asList(packageInfo.requestedPermissions));
            }
            if (!arrayList.isEmpty()) {
                String str = "Missing permissions:";
                for (String str2 : arrayList) {
                    str = String.format("%s\n%s", new Object[]{str, str2});
                }
                Log.log(LogConstants.KEY_SDK, "Integration", str);
                br.c(activity, str);
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    public static void a(Context context) {
        if (!e(context) && !f(context)) {
            Log.log(LogConstants.KEY_SDK, "Integration", "Android Support Library v4 23.0.0+ or AndroidX Legacy Support v4 is required");
            br.c(context, "Android Support Library v4 23.0.0+ or AndroidX Legacy Support v4 is required");
        }
    }

    public static synchronized void a(@NonNull Context context, @NonNull Collection<AdNetwork> collection, @NonNull Collection<AdNetworkBuilder> collection2) {
        synchronized (c.class) {
            HashSet hashSet = new HashSet(collection);
            HashSet hashSet2 = new HashSet(collection2);
            a(context, (Set<? extends AdNetwork>) hashSet, (Set<AdNetworkBuilder>) hashSet2);
            b(context, hashSet, hashSet2);
            c(context, hashSet, hashSet2);
            a(context, (Set<? extends AdNetwork>) hashSet);
            i(context);
        }
    }

    private static void a(Context context, Set<? extends AdNetwork> set) {
        try {
            PackageInfo c2 = c(context.getPackageManager(), context.getPackageName());
            HashSet<String> hashSet = new HashSet<>();
            for (AdNetwork adNetwork : set) {
                if (!adNetwork.isOptional() && adNetwork.getRequiredProvidersClassName() != null) {
                    Collections.addAll(hashSet, adNetwork.getRequiredProvidersClassName());
                }
            }
            if (c2.providers != null) {
                for (ProviderInfo providerInfo : c2.providers) {
                    hashSet.remove(providerInfo.name);
                }
            }
            String str = "Missing Providers";
            StringBuilder sb = new StringBuilder();
            if (!hashSet.isEmpty()) {
                sb.append("Add providers to manifest file: ");
                for (String append : hashSet) {
                    sb.append(append);
                    sb.append(", ");
                }
                sb.delete(sb.length() - 2, sb.length());
            }
            String sb2 = sb.toString();
            if (!sb2.isEmpty()) {
                Log.log(LogConstants.KEY_SDK, "Integration", sb2);
                br.a(context, str, sb2);
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    private static void a(Context context, Set<? extends AdNetwork> set, Set<AdNetworkBuilder> set2) {
        ActivityInfo[] activityInfoArr;
        ActivityRule[] adActivityRules;
        try {
            PackageInfo a2 = a(context.getPackageManager(), context.getPackageName());
            HashSet<String> hashSet = new HashSet<>();
            Iterator it = set.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                AdNetwork adNetwork = (AdNetwork) it.next();
                if (!adNetwork.isOptional()) {
                    for (ActivityRule activityRule : adNetwork.getAdActivityRules()) {
                        hashSet.add(activityRule.a());
                        activityRule.a(context);
                    }
                }
            }
            HashSet hashSet2 = new HashSet();
            HashSet<String> hashSet3 = new HashSet<>();
            for (AdNetworkBuilder adNetworkBuilder : set2) {
                if (a(adNetworkBuilder.getName()) && !adNetworkBuilder.isOptional()) {
                    Collections.addAll(hashSet2, ActivityRule.a(adNetworkBuilder.getAdActivityRules()));
                }
            }
            if (a2.activities != null) {
                for (ActivityInfo activityInfo : a2.activities) {
                    hashSet.remove(activityInfo.name);
                    if (hashSet2.contains(activityInfo.name)) {
                        if (!br.a(activityInfo.name)) {
                            hashSet3.add(activityInfo.name);
                        }
                    }
                }
            }
            if (!hashSet.isEmpty()) {
                String str = "Missing activities:";
                for (String str2 : hashSet) {
                    str = String.format("%s\n%s", new Object[]{str, str2});
                }
                Log.log(LogConstants.KEY_SDK, "Integration", str);
                br.c(context, str);
            }
            if (!hashSet3.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                String str3 = "Unnecessary activities in manifest";
                for (String append : hashSet3) {
                    sb.append(append);
                    sb.append(", ");
                }
                sb.delete(sb.length() - 2, sb.length());
                String sb2 = sb.toString();
                Log.log(LogConstants.KEY_SDK, "Integration", String.format("%s: %s", new Object[]{str3, sb2}));
                br.a(context, str3, sb2);
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    public static boolean a(Context context, AdNetworkBuilder adNetworkBuilder) {
        String[] requiredPermissions;
        try {
            for (String str : adNetworkBuilder.getRequiredPermissions()) {
                if (ContextCompat.checkSelfPermission(context, str) == -1) {
                    Log.log(LogConstants.KEY_SDK, "Integration", String.format("Permission: %s, not found for %s", new Object[]{str, br.c(adNetworkBuilder.getName())}));
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            Log.log(e2);
            return false;
        }
    }

    static boolean a(Bundle bundle, String str, String str2) {
        if (bundle != null) {
            try {
                if (bundle.get(str) != null) {
                    return bundle.get(str).toString().equals(str2);
                }
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
        return false;
    }

    private static boolean a(String str) {
        return h.a(4, str) && h.a(256, str) && h.a(1, str) && h.a(2, str) && h.a(128, str) && h.a(512, str);
    }

    private static PackageInfo b(PackageManager packageManager, String str) throws NameNotFoundException {
        if (e == null) {
            e = packageManager.getPackageInfo(str, 2);
        }
        return e;
    }

    public static void b() {
        b.remove("android.permission.WRITE_EXTERNAL_STORAGE");
    }

    public static void b(Context context) {
        if (!g(context) && !h(context)) {
            Log.log(LogConstants.KEY_SDK, "Integration", "Android Support RecyclerView v7 or AndroidX RecyclerView is missing");
            br.c(context, "Android Support RecyclerView v7 or AndroidX RecyclerView is missing");
        }
    }

    private static void b(Context context, Set<? extends AdNetwork> set, Set<AdNetworkBuilder> set2) {
        ActivityInfo[] activityInfoArr;
        try {
            PackageInfo b2 = b(context.getPackageManager(), context.getPackageName());
            HashSet<String> hashSet = new HashSet<>();
            HashSet<String> hashSet2 = new HashSet<>();
            for (AdNetwork adNetwork : set) {
                if (!adNetwork.isOptional() && adNetwork.getRequiredReceiverClassName() != null) {
                    if (!br.a(adNetwork.getRequiredReceiverClassName())) {
                        Collections.addAll(hashSet, adNetwork.getRequiredReceiverClassName());
                    }
                    Collections.addAll(hashSet2, adNetwork.getRequiredReceiverClassName());
                }
            }
            HashSet hashSet3 = new HashSet();
            HashSet<String> hashSet4 = new HashSet<>();
            for (AdNetworkBuilder requiredReceiverClassName : set2) {
                Collections.addAll(hashSet3, requiredReceiverClassName.getRequiredReceiverClassName());
            }
            if (b2.receivers != null) {
                for (ActivityInfo activityInfo : b2.receivers) {
                    hashSet2.remove(activityInfo.name);
                    if (hashSet3.contains(activityInfo.name)) {
                        if (!br.a(activityInfo.name)) {
                            hashSet4.add(activityInfo.name);
                        }
                    }
                }
            }
            String str = "Missing receivers";
            StringBuilder sb = new StringBuilder();
            if (!hashSet.isEmpty()) {
                sb.append("Classes for receivers: ");
                for (String append : hashSet) {
                    sb.append(append);
                    sb.append(", ");
                }
                sb.delete(sb.length() - 2, sb.length());
                sb.append(" are missing, please check that all jar file are present.");
                sb.append(System.getProperty("line.separator"));
            }
            if (!hashSet2.isEmpty()) {
                sb.append("Add receivers to manifest file: ");
                for (String append2 : hashSet2) {
                    sb.append(append2);
                    sb.append(", ");
                }
                sb.delete(sb.length() - 2, sb.length());
            }
            if (!hashSet4.isEmpty()) {
                sb.append("Unnecessary receivers in manifest file: ");
                for (String append3 : hashSet4) {
                    sb.append(append3);
                    sb.append(", ");
                }
                sb.delete(sb.length() - 2, sb.length());
            }
            String sb2 = sb.toString();
            if (!sb2.isEmpty()) {
                Log.log(LogConstants.KEY_SDK, "Integration", sb2);
                br.a(context, str, sb2);
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    public static boolean b(@Nullable Activity activity) {
        return activity != null && f1710a.contains(activity.getLocalClassName());
    }

    private static PackageInfo c(PackageManager packageManager, String str) throws NameNotFoundException {
        if (g == null) {
            g = packageManager.getPackageInfo(str, 8);
        }
        return g;
    }

    public static void c(Activity activity) {
        try {
            Bundle bundle = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 128).metaData;
            Iterator it = c.iterator();
            String str = "Missing meta-data:";
            boolean z = false;
            while (it.hasNext()) {
                String str2 = (String) it.next();
                if (!bundle.containsKey(str2)) {
                    str = String.format("%s\n%s", new Object[]{str, str2});
                    z = true;
                }
            }
            if (z) {
                Log.log(LogConstants.KEY_SDK, "Integration", str);
                br.c(activity, str);
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    private static synchronized void c(Context context, Set<? extends AdNetwork> set, Set<AdNetworkBuilder> set2) {
        ServiceInfo[] serviceInfoArr;
        Context context2 = context;
        synchronized (c.class) {
            try {
                PackageInfo d2 = d(context.getPackageManager(), context.getPackageName());
                HashSet<String> hashSet = new HashSet<>();
                HashMap hashMap = new HashMap();
                for (AdNetwork adNetwork : set) {
                    List<Pair> requiredServiceWithData = adNetwork.getRequiredServiceWithData();
                    if (!adNetwork.isOptional() && requiredServiceWithData != null && !requiredServiceWithData.isEmpty()) {
                        for (Pair pair : requiredServiceWithData) {
                            if (!br.a((String) pair.first)) {
                                hashSet.add(pair.first);
                            }
                            hashMap.put(pair.first, pair.second);
                        }
                    }
                }
                HashSet hashSet2 = new HashSet();
                HashSet<String> hashSet3 = new HashSet<>();
                for (AdNetworkBuilder adNetworkBuilder : set2) {
                    if (adNetworkBuilder.getRequiredServiceWithData() != null && !adNetworkBuilder.getRequiredServiceWithData().isEmpty()) {
                        for (Pair pair2 : adNetworkBuilder.getRequiredServiceWithData()) {
                            if (pair2.first != null) {
                                hashSet2.add(pair2.first);
                            }
                        }
                    }
                }
                HashSet<String> hashSet4 = new HashSet<>();
                if (d2.services != null) {
                    for (ServiceInfo serviceInfo : d2.services) {
                        if (hashMap.containsKey(serviceInfo.name)) {
                            Pair pair3 = (Pair) hashMap.get(serviceInfo.name);
                            if (!(pair3 == null || pair3.first == null || pair3.second == null)) {
                                if (!a(context.getPackageManager().getServiceInfo(new ComponentName(context2, Class.forName(serviceInfo.name)), 128).metaData, (String) pair3.first, (String) pair3.second)) {
                                    hashSet4.add(String.format("%s - %s(%s)", new Object[]{serviceInfo.name, pair3.first, pair3.second}));
                                }
                            }
                            hashMap.remove(serviceInfo.name);
                        }
                        if (hashSet2.contains(serviceInfo.name)) {
                            if (!br.a(serviceInfo.name)) {
                                hashSet3.add(serviceInfo.name);
                            }
                        }
                    }
                }
                String str = "Missing services";
                StringBuilder sb = new StringBuilder();
                if (!hashSet.isEmpty()) {
                    sb.append("Classes for services: ");
                    for (String append : hashSet) {
                        sb.append(append);
                        sb.append(", ");
                    }
                    sb.delete(sb.length() - 2, sb.length());
                    sb.append(" are missing, please check that all jar file are present.");
                    sb.append(System.getProperty("line.separator"));
                }
                if (!hashMap.isEmpty()) {
                    sb.append("Add services to manifest file: ");
                    for (String append2 : hashMap.keySet()) {
                        sb.append(append2);
                        sb.append(", ");
                    }
                    sb.delete(sb.length() - 2, sb.length());
                    sb.append(System.getProperty("line.separator"));
                }
                if (!hashSet4.isEmpty()) {
                    sb.append("Check services meta-data: ");
                    for (String append3 : hashSet4) {
                        sb.append(append3);
                        sb.append(", ");
                    }
                    sb.delete(sb.length() - 2, sb.length());
                }
                if (!hashSet3.isEmpty()) {
                    sb.append("Unnecessary services in manifest file: ");
                    for (String append4 : hashSet3) {
                        sb.append(append4);
                        sb.append(", ");
                    }
                    sb.delete(sb.length() - 2, sb.length());
                }
                String sb2 = sb.toString();
                if (!sb2.isEmpty()) {
                    Log.log(LogConstants.KEY_SDK, "Integration", sb2);
                    br.a(context2, str, sb2);
                }
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
        return;
    }

    @TargetApi(23)
    public static boolean c(Context context) {
        boolean z = false;
        if (context != null) {
            try {
                if (ContextCompat.checkSelfPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
                    z = true;
                }
                return z;
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
        return false;
    }

    public static int d(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            if (applicationInfo != null) {
                return applicationInfo.targetSdkVersion;
            }
            return 0;
        } catch (Exception e2) {
            Log.log(e2);
            return 0;
        }
    }

    private static PackageInfo d(PackageManager packageManager, String str) throws NameNotFoundException {
        if (f == null) {
            f = packageManager.getPackageInfo(str, 4);
        }
        return f;
    }

    private static boolean e(Context context) {
        try {
            Class.forName("androidx.fragment.app.Fragment", false, context.getClass().getClassLoader());
            Class.forName("androidx.fragment.app.FragmentActivity", false, context.getClass().getClassLoader());
            Class.forName("androidx.fragment.app.FragmentManager", false, context.getClass().getClassLoader());
            Class.forName("androidx.fragment.app.FragmentTransaction", false, context.getClass().getClassLoader());
            Class.forName("androidx.localbroadcastmanager.content.LocalBroadcastManager", false, context.getClass().getClassLoader());
            Class.forName("androidx.collection.LruCache", false, context.getClass().getClassLoader());
            Class.forName("androidx.viewpager.widget.PagerAdapter", false, context.getClass().getClassLoader());
            Class.forName("androidx.viewpager.widget.ViewPager", false, context.getClass().getClassLoader());
            Class.forName("androidx.core.content.ContextCompat", false, context.getClass().getClassLoader());
            return true;
        } catch (ClassNotFoundException | SecurityException unused) {
            return false;
        }
    }

    private static boolean f(Context context) {
        try {
            Class.forName("android.support.v4.app.Fragment", false, context.getClass().getClassLoader());
            Class.forName("android.support.v4.app.FragmentActivity", false, context.getClass().getClassLoader());
            Class.forName("android.support.v4.app.FragmentManager", false, context.getClass().getClassLoader());
            Class.forName("android.support.v4.app.FragmentTransaction", false, context.getClass().getClassLoader());
            Class.forName("android.support.v4.content.LocalBroadcastManager", false, context.getClass().getClassLoader());
            Class.forName("android.support.v4.util.LruCache", false, context.getClass().getClassLoader());
            Class.forName("android.support.v4.view.PagerAdapter", false, context.getClass().getClassLoader());
            Class.forName("android.support.v4.view.ViewPager", false, context.getClass().getClassLoader());
            Class.forName("android.support.v4.content.ContextCompat", false, context.getClass().getClassLoader());
            ContextCompat.class.getDeclaredMethod("checkSelfPermission", new Class[]{Context.class, String.class});
            return true;
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException unused) {
            return false;
        }
    }

    private static boolean g(Context context) {
        try {
            Class.forName("androidx.recyclerview.widget.RecyclerView", false, context.getClass().getClassLoader());
            Class.forName("androidx.recyclerview.widget.LinearLayoutManager", false, context.getClass().getClassLoader());
            return true;
        } catch (ClassNotFoundException | SecurityException unused) {
            return false;
        }
    }

    private static boolean h(Context context) {
        try {
            Class.forName("android.support.v7.widget.RecyclerView", false, context.getClass().getClassLoader());
            Class.forName("android.support.v7.widget.LinearLayoutManager", false, context.getClass().getClassLoader());
            return true;
        } catch (ClassNotFoundException | SecurityException unused) {
            return false;
        }
    }

    private static void i(Context context) {
        if (VERSION.SDK_INT >= 28 && context.getApplicationInfo().targetSdkVersion >= 28 && !br.g()) {
            String str = "If your app is targeting API level 28 or higher, please add in manifest file: \n<uses-library android:name=\"org.apache.http.legacy\" android:required=\"false\" />";
            Log.log(LogConstants.KEY_SDK, "Integration", str);
            br.a(context, "Missing Apache HTTP client", str);
        }
    }
}
