package com.appodeal.ads.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import com.appodeal.ads.br;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import dalvik.system.BaseDexClassLoader;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class k {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static final Map<String, b> f1719a = new ConcurrentHashMap();
    public static List<String> b = null;
    private static ExecutorService c = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
    private static final Object d = new Object();
    private static final HashMap<String, Pair<String, String[]>> e = new HashMap<>();
    private static boolean f = false;

    public interface a {
        void a(boolean z);
    }

    @VisibleForTesting
    static final class b {
        @VisibleForTesting

        /* renamed from: a reason: collision with root package name */
        List<a> f1720a;
        final String b;
        final String c;
        boolean d = false;
        boolean e = false;
        boolean f = false;
        private File g;

        b(@NonNull String str, @NonNull String str2) {
            this.b = str;
            this.c = str2;
        }

        /* access modifiers changed from: 0000 */
        public File a(@NonNull Context context) {
            if (this.g == null) {
                this.g = new File(new File(context.getDir("optimized", 0), this.b), String.format("%s%s.dex", new Object[]{this.b.replace(".dx", ""), this.c}));
            }
            return this.g;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            synchronized (k.class) {
                if (this.f1720a != null) {
                    for (a a2 : this.f1720a) {
                        a2.a(this.f);
                    }
                    this.f1720a = null;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable a aVar) {
            if (aVar != null) {
                synchronized (k.class) {
                    if (this.f1720a == null) {
                        this.f1720a = new ArrayList();
                    }
                    this.f1720a.add(aVar);
                }
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(getClass().getSimpleName());
            sb.append("@");
            sb.append(Integer.toHexString(hashCode()));
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(this.b);
            sb.append(", ");
            sb.append(this.c);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            return sb.toString();
        }
    }

    private static final class c implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private Context f1721a;
        private b b;
        private String[] c;

        c(@NonNull Context context, @NonNull b bVar, @NonNull String[] strArr) {
            this.f1721a = context;
            this.b = bVar;
            this.c = strArr;
        }

        private void a(@NonNull Context context, @NonNull String str) {
            String format = String.format("ERROR: %s not found", new Object[]{br.c(str.split("\\.")[0])});
            Log.log(LogConstants.KEY_SDK, "Integration", format);
            br.c(context, format);
        }

        public void run() {
            try {
                k.c(this.f1721a, this.b);
                if (br.a(this.c)) {
                    this.b.f = true;
                } else {
                    Log.log(LogConstants.KEY_SDK, "Integration", String.format("failed to load classes for DX: %s", new Object[]{br.c(this.b.b.split("\\.")[0])}));
                }
            } catch (FileNotFoundException e) {
                th = e;
                a(this.f1721a, this.b.b);
                Log.log(th);
                this.b.e = true;
                this.b.a();
            } catch (Throwable th) {
                th = th;
                Log.log(th);
                this.b.e = true;
                this.b.a();
            }
            this.b.e = true;
            this.b.a();
        }
    }

    private static Object a(BaseDexClassLoader baseDexClassLoader) throws Exception {
        Field declaredField = BaseDexClassLoader.class.getDeclaredField("pathList");
        declaredField.setAccessible(true);
        Object obj = declaredField.get(baseDexClassLoader);
        Field declaredField2 = obj.getClass().getDeclaredField("dexElements");
        declaredField2.setAccessible(true);
        return declaredField2.get(obj);
    }

    private static Object a(Object obj, Object obj2) {
        Class componentType = obj.getClass().getComponentType();
        if (componentType == obj2.getClass().getComponentType()) {
            int length = Array.getLength(obj);
            int length2 = Array.getLength(obj2);
            Object newInstance = Array.newInstance(componentType, length + length2);
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                Array.set(newInstance, i3, Array.get(obj, i2));
                i2++;
                i3++;
            }
            while (i < length2) {
                Array.set(newInstance, i3, Array.get(obj2, i));
                i++;
                i3++;
            }
            return newInstance;
        }
        throw new IllegalArgumentException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0052, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005f, code lost:
        r4 = new com.appodeal.ads.utils.k.c(r3, r2, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0068, code lost:
        if (a(r3, r2) == false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006a, code lost:
        c.execute(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0070, code lost:
        r4.run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0073, code lost:
        return;
     */
    public static void a(@NonNull Context context, @NonNull String str, @NonNull String str2, @NonNull String[] strArr, @Nullable a aVar) {
        if (br.a(strArr)) {
            if (!f1719a.containsKey(str)) {
                br.c(context, String.format("Integration Error: %s classes are already loaded from jar files, remove unnecessary dex files", new Object[]{br.c(str.split("\\.")[0])}));
            }
            if (aVar != null) {
                aVar.a(true);
            }
            return;
        }
        synchronized (k.class) {
            b bVar = (b) f1719a.get(str);
            if (bVar == null) {
                bVar = new b(str, str2);
                f1719a.put(str, bVar);
            }
            if (!bVar.e) {
                bVar.a(aVar);
                if (!bVar.d) {
                    bVar.d = true;
                }
            } else if (aVar != null) {
                aVar.a(bVar.f);
            }
        }
    }

    private static void a(BaseDexClassLoader baseDexClassLoader, Object obj) throws Exception {
        Field declaredField = BaseDexClassLoader.class.getDeclaredField("pathList");
        declaredField.setAccessible(true);
        Object obj2 = declaredField.get(baseDexClassLoader);
        Field declaredField2 = obj2.getClass().getDeclaredField("dexElements");
        declaredField2.setAccessible(true);
        declaredField2.set(obj2, obj);
    }

    public static boolean a(@NonNull Context context) {
        if (!f) {
            for (Entry entry : e.entrySet()) {
                if (!br.a((String[]) ((Pair) entry.getValue()).second)) {
                    try {
                        a(context, (String) entry.getKey(), (String) ((Pair) entry.getValue()).first, (String[]) ((Pair) entry.getValue()).second, null);
                    } catch (Exception e2) {
                        Log.log(e2);
                        return false;
                    }
                }
            }
            f = true;
        }
        return true;
    }

    @VisibleForTesting
    static boolean a(@NonNull Context context, @NonNull b bVar) {
        return !bVar.a(context).exists();
    }

    public static void b(Context context) {
        br.a(context.getDir(new File("optimized").toString(), 0));
        br.a(context.getDir("working", 0));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007e A[SYNTHETIC, Splitter:B:22:0x007e] */
    public static void c(@NonNull Context context, @NonNull b bVar) throws Exception {
        FileOutputStream fileOutputStream;
        File a2 = bVar.a(context);
        File file = new File(new File(context.getDir("working", 0), bVar.b), a2.getName());
        if (!file.exists()) {
            br.a(file.getParentFile());
            br.a(a2.getParentFile());
        }
        a2.mkdirs();
        if (!file.exists()) {
            AssetManager assets = context.getAssets();
            StringBuilder sb = new StringBuilder();
            sb.append("dex/");
            sb.append(bVar.b);
            InputStream open = assets.open(sb.toString());
            file.getParentFile().mkdirs();
            try {
                fileOutputStream = new FileOutputStream(file);
                try {
                    byte[] bArr = new byte[4096];
                    while (true) {
                        int read = open.read(bArr);
                        if (read != -1) {
                            fileOutputStream.write(bArr, 0, read);
                        } else {
                            try {
                                break;
                            } catch (Exception e2) {
                                Log.log(e2);
                            }
                        }
                    }
                    fileOutputStream.close();
                } catch (Throwable th) {
                    th = th;
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (Exception e3) {
                            Log.log(e3);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = null;
                if (fileOutputStream != null) {
                }
                throw th;
            }
        }
        ClassLoader classLoader = k.class.getClassLoader();
        DexClassLoader dexClassLoader = new DexClassLoader(file.getAbsolutePath(), a2.getAbsolutePath(), null, classLoader);
        if (classLoader instanceof BaseDexClassLoader) {
            synchronized (d) {
                Object a3 = a((BaseDexClassLoader) classLoader);
                a((BaseDexClassLoader) classLoader, a(a((BaseDexClassLoader) dexClassLoader), a3));
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("ClassLoader not supported: ");
        sb2.append(classLoader.getClass());
        throw new UnsupportedOperationException(sb2.toString());
    }
}
