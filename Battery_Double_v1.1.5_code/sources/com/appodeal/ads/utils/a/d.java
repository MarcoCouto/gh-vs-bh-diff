package com.appodeal.ads.utils.a;

import android.content.Context;
import android.os.Environment;
import android.util.Base64;
import com.appodeal.ads.utils.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Iterator;
import org.json.JSONObject;

public class d implements c {

    /* renamed from: a reason: collision with root package name */
    private final String f1698a;

    public d(String str) {
        this.f1698a = str;
    }

    public static JSONObject a() {
        return a(".appodeal");
    }

    private static JSONObject a(String str) {
        BufferedReader bufferedReader;
        try {
            File file = new File(Environment.getExternalStorageDirectory(), str);
            if (file.exists()) {
                bufferedReader = new BufferedReader(new FileReader(file));
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                }
                JSONObject jSONObject = new JSONObject(new String(Base64.decode(sb.toString(), 0), "UTF-8"));
                try {
                    bufferedReader.close();
                } catch (Exception e) {
                    Log.log(e);
                }
                return jSONObject;
            }
        } catch (Exception e2) {
            Log.log(e2);
        } catch (Throwable th) {
            try {
                bufferedReader.close();
            } catch (Exception e3) {
                Log.log(e3);
            }
            throw th;
        }
        return null;
    }

    private static void a(String str, String str2) {
        BufferedWriter bufferedWriter;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(new File(Environment.getExternalStorageDirectory(), str2)));
            bufferedWriter.write(Base64.encodeToString(str.getBytes("UTF-8"), 0));
            try {
                bufferedWriter.close();
            } catch (Exception e) {
                Log.log(e);
            }
        } catch (Exception e2) {
            Log.log(e2);
        } catch (Throwable th) {
            try {
                bufferedWriter.close();
            } catch (Exception e3) {
                Log.log(e3);
            }
            throw th;
        }
    }

    public static void a(JSONObject jSONObject) {
        a(jSONObject.toString(), ".appodeal");
    }

    public static void b() {
        try {
            JSONObject a2 = a(".appodeal2");
            if (a2 != null) {
                long currentTimeMillis = System.currentTimeMillis() - 259200000;
                Iterator keys = a2.keys();
                while (keys.hasNext()) {
                    if (a2.getLong((String) keys.next()) < currentTimeMillis) {
                        keys.remove();
                    }
                }
                a(a2.toString(), ".appodeal2");
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public JSONObject a(Context context) {
        JSONObject a2 = a(".appodeal");
        if (a2 != null && a2.has(this.f1698a)) {
            try {
                return new JSONObject(a2.getString(this.f1698a));
            } catch (Exception e) {
                Log.log(e);
            }
        }
        return null;
    }

    public void a(Context context, JSONObject jSONObject) {
        try {
            JSONObject a2 = a(".appodeal");
            if (a2 == null) {
                a2 = new JSONObject();
            }
            a2.put(this.f1698a, jSONObject.toString());
            a(a2.toString(), ".appodeal");
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public boolean b(Context context) {
        boolean z = false;
        try {
            JSONObject a2 = a(".appodeal2");
            if (a2 != null && a2.has(this.f1698a)) {
                z = true;
            }
            return z;
        } catch (Exception e) {
            Log.log(e);
            return false;
        }
    }

    public void c(Context context) {
        try {
            JSONObject a2 = a(".appodeal2");
            if (a2 == null) {
                a2 = new JSONObject();
            }
            a2.put(this.f1698a, System.currentTimeMillis());
            a(a2.toString(), ".appodeal2");
        } catch (Exception e) {
            Log.log(e);
        }
    }
}
