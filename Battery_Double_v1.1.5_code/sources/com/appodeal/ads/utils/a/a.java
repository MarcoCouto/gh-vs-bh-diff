package com.appodeal.ads.utils.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.appodeal.ads.bm;
import com.appodeal.ads.utils.Log;
import java.util.Iterator;
import java.util.Map.Entry;
import org.json.JSONObject;

public class a implements c {

    /* renamed from: a reason: collision with root package name */
    private final String f1694a;

    public a(String str) {
        this.f1694a = str;
    }

    public static void b(Context context, JSONObject jSONObject) {
        Editor edit = bm.a(context, "freq").b().edit();
        edit.clear();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            try {
                String str = (String) keys.next();
                edit.putString(str, jSONObject.getString(str));
            } catch (Exception e) {
                Log.log(e);
            }
        }
        edit.apply();
    }

    public static JSONObject d(Context context) {
        SharedPreferences b = bm.a(context, "freq").b();
        JSONObject jSONObject = new JSONObject();
        for (Entry entry : b.getAll().entrySet()) {
            try {
                jSONObject.put((String) entry.getKey(), new JSONObject((String) entry.getValue()));
            } catch (Exception e) {
                Log.log(e);
            }
        }
        return jSONObject;
    }

    public static void e(Context context) {
        SharedPreferences b = bm.a(context, "freq_clicks").b();
        Editor edit = b.edit();
        long currentTimeMillis = System.currentTimeMillis() - 259200000;
        for (Entry entry : b.getAll().entrySet()) {
            try {
                if (((Long) entry.getValue()).longValue() < currentTimeMillis) {
                    edit.remove((String) entry.getKey());
                }
            } catch (Exception unused) {
                edit.remove((String) entry.getKey());
            }
        }
        edit.apply();
    }

    public JSONObject a(Context context) {
        SharedPreferences b = bm.a(context, "freq").b();
        if (b.contains(this.f1694a)) {
            try {
                return new JSONObject(b.getString(this.f1694a, null));
            } catch (Exception e) {
                Log.log(e);
            }
        }
        return null;
    }

    public void a(Context context, JSONObject jSONObject) {
        try {
            bm.a(context, "freq").b().edit().putString(this.f1694a, jSONObject.toString()).apply();
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public boolean b(Context context) {
        return bm.a(context, "freq_clicks").b().contains(this.f1694a);
    }

    public void c(Context context) {
        try {
            bm.a(context, "freq_clicks").a().putLong(this.f1694a, System.currentTimeMillis()).apply();
        } catch (Exception e) {
            Log.log(e);
        }
    }
}
