package com.appodeal.ads.utils.a;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import com.appodeal.ads.aa;
import com.appodeal.ads.br;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.c;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.github.mikephil.charting.utils.Utils;
import com.tapjoy.TJAdUnitConstants.String;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

public class b {

    /* renamed from: a reason: collision with root package name */
    public static final HashMap<String, HashMap<String, Integer>> f1695a = new HashMap<>();
    @VisibleForTesting
    c b;
    public String c;
    public String d;
    public a e;
    public int f;
    public int g;
    public int h;
    public int i;
    public boolean j;
    public String k;
    public boolean l;
    public boolean m;

    public enum a {
        CAMPAIGN,
        IMAGE
    }

    public b(Context context, JSONObject jSONObject, String str) {
        c aVar;
        try {
            this.k = str;
            this.c = String.valueOf(jSONObject.getInt("campaign_id"));
            this.d = String.valueOf(jSONObject.getInt("image_id"));
            this.e = jSONObject.getString("cap_type").equals(MessengerShareContentUtility.MEDIA_IMAGE) ? a.IMAGE : a.CAMPAIGN;
            this.f = jSONObject.getInt("impressions");
            this.g = jSONObject.getInt("period");
            this.h = jSONObject.optInt(SettingsJsonConstants.SESSION_KEY, -1);
            this.i = jSONObject.optInt(String.INTERVAL, 0);
            this.j = jSONObject.optBoolean("per_app", false);
            this.l = jSONObject.optBoolean("stop_after_install", false);
            this.m = jSONObject.optBoolean("stop_after_click", false);
            if (c.c(context) && br.j()) {
                if (!this.j) {
                    aVar = new d(this.c);
                    this.b = aVar;
                }
            }
            aVar = new a(this.c);
            this.b = aVar;
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    public static JSONObject a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return new JSONObject();
        }
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            try {
                String str = (String) keys.next();
                JSONObject jSONObject2 = new JSONObject(jSONObject.getString(str));
                Iterator keys2 = jSONObject2.keys();
                while (keys2.hasNext()) {
                    String str2 = (String) keys2.next();
                    JSONArray jSONArray = jSONObject2.getJSONArray(str2);
                    ArrayList arrayList = new ArrayList();
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        arrayList.add(Integer.valueOf(jSONArray.getInt(i2)));
                    }
                    Collections.sort(arrayList);
                    List subList = arrayList.subList(Math.max(arrayList.size() - 20, 0), arrayList.size());
                    long currentTimeMillis = ((System.currentTimeMillis() / 1000) / 60) - 43200;
                    Iterator it = subList.iterator();
                    while (it.hasNext()) {
                        if (((long) ((Integer) it.next()).intValue()) < currentTimeMillis) {
                            it.remove();
                        }
                    }
                    if (subList.size() > 0) {
                        jSONObject2.put(str2, new JSONArray(subList));
                    } else {
                        keys2.remove();
                    }
                }
                if (jSONObject2.length() > 0) {
                    jSONObject.put(str, jSONObject2);
                } else {
                    keys.remove();
                }
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
        return jSONObject;
    }

    public static void a(List<JSONObject> list) {
        try {
            if (aa.g) {
                HashMap hashMap = new HashMap();
                for (int i2 = 0; i2 < list.size(); i2++) {
                    JSONObject jSONObject = (JSONObject) list.get(i2);
                    if (jSONObject.has("freq")) {
                        double d2 = jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM);
                        List arrayList = hashMap.containsKey(Double.valueOf(d2)) ? (List) hashMap.get(Double.valueOf(d2)) : new ArrayList();
                        arrayList.add(new Pair(Integer.valueOf(i2), Double.valueOf(jSONObject.getJSONObject("freq").optDouble("weight", 1.0d))));
                        hashMap.put(Double.valueOf(d2), arrayList);
                    }
                }
                for (List<Pair> list2 : hashMap.values()) {
                    if (list2.size() != 1) {
                        double d3 = Utils.DOUBLE_EPSILON;
                        for (Pair pair : list2) {
                            d3 += ((Double) pair.second).doubleValue();
                        }
                        ArrayList arrayList2 = new ArrayList();
                        for (Pair pair2 : list2) {
                            arrayList2.addAll(Collections.nCopies((int) Math.round((((Double) pair2.second).doubleValue() / d3) * 100.0d), pair2.first));
                        }
                        Collections.shuffle(arrayList2);
                        ArrayList arrayList3 = new ArrayList(new LinkedHashSet(arrayList2));
                        HashMap hashMap2 = new HashMap();
                        for (int i3 = 0; i3 < list2.size(); i3++) {
                            hashMap2.put(arrayList3.get(i3), list.get(((Integer) ((Pair) list2.get(i3)).first).intValue()));
                        }
                        for (Entry entry : hashMap2.entrySet()) {
                            list.set(((Integer) entry.getKey()).intValue(), entry.getValue());
                        }
                    }
                }
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    public static void d(Context context) {
        try {
            a.b(context, a(a.d(context)));
            a.e(context);
        } catch (Exception e2) {
            Log.log(e2);
        }
        try {
            if (c.c(context) && br.j()) {
                d.a(a(d.a()));
                d.b();
            }
        } catch (Exception e3) {
            Log.log(e3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x011e, code lost:
        r15 = 0;
     */
    public boolean a(Context context) {
        int i2;
        int i3;
        String str;
        String str2;
        String format;
        int i4;
        try {
            if (this.k != null && this.l && br.a(context, this.k)) {
                Log.log("CampaignFrequency", LogConstants.EVENT_CAN_LOAD_CAMPAIGN, String.format("%s skipped: %s is already installed", new Object[]{this.c, this.k}));
                return false;
            } else if (!this.m || !this.b.b(context)) {
                ArrayList arrayList = new ArrayList();
                JSONObject a2 = this.b.a(context);
                switch (this.e) {
                    case CAMPAIGN:
                        if (a2 != null) {
                            Iterator keys = a2.keys();
                            i4 = 0;
                            while (keys.hasNext()) {
                                JSONArray jSONArray = a2.getJSONArray((String) keys.next());
                                int i5 = i4;
                                for (int i6 = 0; i6 < jSONArray.length(); i6++) {
                                    int i7 = jSONArray.getInt(i6);
                                    arrayList.add(Integer.valueOf(i7));
                                    if (i7 > i5) {
                                        i5 = i7;
                                    }
                                }
                                i4 = i5;
                            }
                        } else {
                            i4 = 0;
                        }
                        if (f1695a.containsKey(this.c)) {
                            int i8 = 0;
                            for (Integer intValue : ((HashMap) f1695a.get(this.c)).values()) {
                                i8 += intValue.intValue();
                            }
                            i2 = i8;
                            break;
                        }
                    case IMAGE:
                        if (a2 == null || !a2.has(this.d)) {
                            i3 = 0;
                        } else {
                            JSONArray jSONArray2 = a2.getJSONArray(this.d);
                            i3 = 0;
                            for (int i9 = 0; i9 < jSONArray2.length(); i9++) {
                                int i10 = jSONArray2.getInt(i9);
                                arrayList.add(Integer.valueOf(i10));
                                if (i10 > i3) {
                                    i3 = i10;
                                }
                            }
                        }
                        if (f1695a.containsKey(this.c)) {
                            HashMap hashMap = (HashMap) f1695a.get(this.c);
                            if (hashMap.containsKey(this.d)) {
                                i2 = ((Integer) hashMap.get(this.d)).intValue();
                                break;
                            }
                        }
                        break;
                    default:
                        i2 = 0;
                        i3 = 0;
                        break;
                }
                long currentTimeMillis = ((System.currentTimeMillis() / 1000) / 60) - ((long) this.g);
                Iterator it = arrayList.iterator();
                int i11 = 0;
                while (it.hasNext()) {
                    if (((long) ((Integer) it.next()).intValue()) >= currentTimeMillis) {
                        i11++;
                    }
                }
                boolean z = i11 < this.f;
                if (this.h > 0) {
                    z = z && i2 < this.h;
                }
                long currentTimeMillis2 = ((System.currentTimeMillis() / 1000) / 60) - ((long) this.i);
                if (this.i > 0) {
                    z = z && ((long) i3) < currentTimeMillis2;
                }
                if (!z) {
                    if (this.i > 0 && ((long) i3) >= currentTimeMillis2) {
                        str = "CampaignFrequency";
                        str2 = LogConstants.EVENT_CAN_LOAD_CAMPAIGN;
                        format = String.format("%s skipped: impression limit per interval was reached", new Object[]{this.c});
                    } else if (this.h > 0 && i2 >= this.h) {
                        str = "CampaignFrequency";
                        str2 = LogConstants.EVENT_CAN_LOAD_CAMPAIGN;
                        format = String.format("%s skipped: impression limit per session was reached", new Object[]{this.c});
                    } else if (i11 >= this.f) {
                        str = "CampaignFrequency";
                        str2 = LogConstants.EVENT_CAN_LOAD_CAMPAIGN;
                        format = String.format("%s skipped: impression limit was reached", new Object[]{this.c});
                    }
                    Log.log(str, str2, format);
                }
                return z;
            } else {
                Log.log("CampaignFrequency", LogConstants.EVENT_CAN_LOAD_CAMPAIGN, String.format("%s skipped: already clicked", new Object[]{this.c}));
                return false;
            }
        } catch (Exception e2) {
            Log.log(e2);
            return true;
        }
    }

    public void b(Context context) {
        HashMap hashMap;
        try {
            JSONObject a2 = this.b.a(context);
            if (a2 == null) {
                a2 = new JSONObject();
            }
            try {
                JSONArray jSONArray = a2.has(this.d) ? a2.getJSONArray(this.d) : new JSONArray();
                jSONArray.put((System.currentTimeMillis() / 1000) / 60);
                a2.put(this.d, jSONArray);
            } catch (Exception e2) {
                Log.log(e2);
            }
            this.b.a(context, a2);
            if (f1695a.containsKey(this.c)) {
                hashMap = (HashMap) f1695a.get(this.c);
            } else {
                hashMap = new HashMap();
                f1695a.put(this.c, hashMap);
            }
            hashMap.put(this.d, Integer.valueOf((hashMap.containsKey(this.d) ? ((Integer) hashMap.get(this.d)).intValue() : 0) + 1));
        } catch (Exception e3) {
            Log.log(e3);
        }
    }

    public void c(Context context) {
        try {
            this.b.c(context);
        } catch (Exception e2) {
            Log.log(e2);
        }
    }
}
