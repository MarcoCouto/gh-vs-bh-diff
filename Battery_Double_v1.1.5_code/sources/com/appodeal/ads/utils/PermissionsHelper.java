package com.appodeal.ads.utils;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;

public class PermissionsHelper {

    /* renamed from: a reason: collision with root package name */
    public static boolean f1691a = true;
    public static boolean b = true;
    private static PermissionsHelper c;
    @Nullable
    private AppodealPermissionCallbacks d;

    public interface AppodealPermissionCallbacks {
        void accessCoarseLocationResponse(int i);

        void writeExternalStorageResponse(int i);
    }

    public static PermissionsHelper a() {
        if (c == null) {
            c = new PermissionsHelper();
        }
        return c;
    }

    public void a(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        if (i == 1) {
            for (int i2 = 0; i2 < strArr.length; i2++) {
                if ("android.permission.WRITE_EXTERNAL_STORAGE".equals(strArr[i2])) {
                    if (this.d != null) {
                        this.d.writeExternalStorageResponse(iArr[i2]);
                    }
                } else if ("android.permission.ACCESS_COARSE_LOCATION".equals(strArr[i2]) && this.d != null) {
                    this.d.accessCoarseLocationResponse(iArr[i2]);
                }
            }
        }
    }

    public void a(@NonNull Activity activity, @Nullable AppodealPermissionCallbacks appodealPermissionCallbacks) {
        if (VERSION.SDK_INT >= 23) {
            this.d = appodealPermissionCallbacks;
            Bundle bundle = new Bundle();
            ArrayList arrayList = new ArrayList(2);
            if (f1691a) {
                arrayList.add("android.permission.WRITE_EXTERNAL_STORAGE");
            }
            if (b) {
                arrayList.add("android.permission.ACCESS_COARSE_LOCATION");
            }
            bundle.putStringArrayList(NativeProtocol.RESULT_ARGS_PERMISSIONS, arrayList);
            FragmentManager fragmentManager = activity.getFragmentManager();
            PermissionFragment permissionFragment = new PermissionFragment();
            permissionFragment.setArguments(bundle);
            FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
            beginTransaction.add(permissionFragment, "PermissionFragment");
            beginTransaction.commit();
        }
    }
}
