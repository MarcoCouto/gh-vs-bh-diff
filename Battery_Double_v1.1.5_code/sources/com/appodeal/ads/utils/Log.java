package com.appodeal.ads.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.ak;
import java.net.UnknownHostException;

public class Log {

    public enum LogLevel {
        none(0),
        debug(1),
        verbose(2);
        

        /* renamed from: a reason: collision with root package name */
        private int f1689a;

        private LogLevel(int i) {
            this.f1689a = i;
        }

        public static LogLevel fromInteger(Integer num) {
            if (num == null) {
                return none;
            }
            switch (num.intValue()) {
                case 0:
                    return none;
                case 1:
                    return debug;
                case 2:
                    return verbose;
                default:
                    return none;
            }
        }

        public static String[] names() {
            LogLevel[] values = values();
            String[] strArr = new String[values.length];
            for (int i = 0; i < values.length; i++) {
                strArr[i] = values[i].name();
            }
            return strArr;
        }

        public int getValue() {
            return this.f1689a;
        }
    }

    private static void a(@Nullable String str, LogLevel logLevel) {
        if (Appodeal.getLogLevel().getValue() >= logLevel.getValue() && str != null) {
            if (str.length() > 1000) {
                int length = ((str.length() + 1000) - 1) / 1000;
                int i = 0;
                int i2 = 0;
                while (i < length) {
                    int i3 = i2 + 1000;
                    android.util.Log.d(Appodeal.f1493a, str.substring(i2, Math.min(str.length(), i3)));
                    i++;
                    i2 = i3;
                }
                return;
            }
            android.util.Log.d(Appodeal.f1493a, str);
        }
    }

    public static void log(@NonNull String str, @NonNull String str2) {
        log(str, str2, (String) null);
    }

    public static void log(@NonNull String str, @NonNull String str2, @NonNull LogLevel logLevel) {
        log(str, str2, null, logLevel);
    }

    public static void log(@NonNull String str, @NonNull String str2, @Nullable String str3) {
        log(str, str2, str3, LogLevel.debug);
    }

    public static void log(@NonNull String str, @NonNull String str2, @Nullable String str3, @NonNull LogLevel logLevel) {
        String format;
        if (Appodeal.getLogLevel() != LogLevel.none) {
            if (TextUtils.isEmpty(str3)) {
                format = String.format("%s [%s]", new Object[]{str, str2});
            } else {
                format = String.format("%s [%s]: %s", new Object[]{str, str2, str3});
            }
            a(format, logLevel);
        }
    }

    public static void log(@Nullable Throwable th) {
        if (th != null) {
            ak.a().a(th);
            if (Appodeal.getLogLevel().getValue() < LogLevel.debug.getValue()) {
                return;
            }
            if (th instanceof UnknownHostException) {
                android.util.Log.d(Appodeal.f1493a, th.toString());
            } else {
                android.util.Log.d(Appodeal.f1493a, "Exception", th);
            }
        }
    }
}
