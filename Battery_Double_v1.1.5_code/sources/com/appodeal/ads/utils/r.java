package com.appodeal.ads.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.bm;
import java.util.Map;
import java.util.Map.Entry;

public class r {
    private static void a(Context context) {
        SharedPreferences b = bm.a(context, "install_tracking").b();
        Map all = b.getAll();
        long currentTimeMillis = System.currentTimeMillis();
        for (Entry entry : all.entrySet()) {
            if (((Long) entry.getValue()).longValue() < currentTimeMillis) {
                b.edit().remove((String) entry.getKey()).apply();
            }
        }
    }

    static void a(@Nullable Context context, @Nullable String str, long j) {
        if (context != null && !TextUtils.isEmpty(str)) {
            if (j == 0) {
                j = 180;
            }
            bm.a(context, "install_tracking").a().putLong(str, (j * 60 * 1000) + System.currentTimeMillis()).apply();
            a(context);
        }
    }

    public static boolean a(Context context, String str) {
        if (context != null && !str.isEmpty()) {
            SharedPreferences b = bm.a(context, "install_tracking").b();
            if (b.contains(str)) {
                if (b.getLong(str, 0) > System.currentTimeMillis()) {
                    return true;
                }
                b.edit().remove(str).apply();
            }
        }
        return false;
    }
}
