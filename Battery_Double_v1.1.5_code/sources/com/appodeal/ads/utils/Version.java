package com.appodeal.ads.utils;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Version implements Comparable<Version> {

    /* renamed from: a reason: collision with root package name */
    private final List<String> f1692a;

    public Version(String str) {
        this.f1692a = a(str);
    }

    @VisibleForTesting
    static List<String> a(String str) {
        if (str == null) {
            return new LinkedList();
        }
        LinkedList linkedList = new LinkedList();
        for (String split : str.split("\\.")) {
            Collections.addAll(linkedList, split.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)"));
        }
        ListIterator listIterator = linkedList.listIterator(linkedList.size());
        while (listIterator.hasPrevious() && ((String) listIterator.previous()).matches("[0]+")) {
            listIterator.remove();
        }
        return linkedList;
    }

    public int compareTo(@NonNull Version version) {
        String str;
        String str2;
        Iterator it = this.f1692a.iterator();
        Iterator it2 = version.f1692a.iterator();
        while (true) {
            str = null;
            str2 = it2.hasNext() ? (String) it2.next() : null;
            if (it.hasNext()) {
                str = (String) it.next();
            }
            if (str == null || str2 == null) {
                if (str == null || str2 == null) {
                    return (str == null && str2 == null) ? 1 : 0;
                }
                return -1;
            } else if (str.matches("\\d+")) {
                if (!str2.matches("\\d+")) {
                    return 1;
                }
                int compareTo = Integer.valueOf(str).compareTo(Integer.valueOf(str2));
                if (compareTo != 0) {
                    return compareTo;
                }
            } else if (str2.matches("\\d+")) {
                return -1;
            } else {
                int compareTo2 = str.compareTo(str2);
                if (compareTo2 != 0) {
                    return compareTo2;
                }
            }
        }
        if (str == null) {
        }
        if (str == null) {
        }
    }
}
