package com.appodeal.ads.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class t extends SSLSocketFactory {

    /* renamed from: a reason: collision with root package name */
    private final SSLSocketFactory f1739a;

    public class a extends SSLSocket {

        /* renamed from: a reason: collision with root package name */
        protected final SSLSocket f1740a;

        a(SSLSocket sSLSocket) {
            this.f1740a = sSLSocket;
        }

        public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
            this.f1740a.addHandshakeCompletedListener(handshakeCompletedListener);
        }

        public void bind(SocketAddress socketAddress) throws IOException {
            this.f1740a.bind(socketAddress);
        }

        public synchronized void close() throws IOException {
            this.f1740a.close();
        }

        public void connect(SocketAddress socketAddress) throws IOException {
            this.f1740a.connect(socketAddress);
        }

        public void connect(SocketAddress socketAddress, int i) throws IOException {
            this.f1740a.connect(socketAddress, i);
        }

        public boolean equals(Object obj) {
            return this.f1740a.equals(obj);
        }

        public SocketChannel getChannel() {
            return this.f1740a.getChannel();
        }

        public boolean getEnableSessionCreation() {
            return this.f1740a.getEnableSessionCreation();
        }

        public String[] getEnabledCipherSuites() {
            return this.f1740a.getEnabledCipherSuites();
        }

        public String[] getEnabledProtocols() {
            return this.f1740a.getEnabledProtocols();
        }

        public InetAddress getInetAddress() {
            return this.f1740a.getInetAddress();
        }

        public InputStream getInputStream() throws IOException {
            return this.f1740a.getInputStream();
        }

        public boolean getKeepAlive() throws SocketException {
            return this.f1740a.getKeepAlive();
        }

        public InetAddress getLocalAddress() {
            return this.f1740a.getLocalAddress();
        }

        public int getLocalPort() {
            return this.f1740a.getLocalPort();
        }

        public SocketAddress getLocalSocketAddress() {
            return this.f1740a.getLocalSocketAddress();
        }

        public boolean getNeedClientAuth() {
            return this.f1740a.getNeedClientAuth();
        }

        public boolean getOOBInline() throws SocketException {
            return this.f1740a.getOOBInline();
        }

        public OutputStream getOutputStream() throws IOException {
            return this.f1740a.getOutputStream();
        }

        public int getPort() {
            return this.f1740a.getPort();
        }

        public synchronized int getReceiveBufferSize() throws SocketException {
            return this.f1740a.getReceiveBufferSize();
        }

        public SocketAddress getRemoteSocketAddress() {
            return this.f1740a.getRemoteSocketAddress();
        }

        public boolean getReuseAddress() throws SocketException {
            return this.f1740a.getReuseAddress();
        }

        public synchronized int getSendBufferSize() throws SocketException {
            return this.f1740a.getSendBufferSize();
        }

        public SSLSession getSession() {
            return this.f1740a.getSession();
        }

        public int getSoLinger() throws SocketException {
            return this.f1740a.getSoLinger();
        }

        public synchronized int getSoTimeout() throws SocketException {
            return this.f1740a.getSoTimeout();
        }

        public String[] getSupportedCipherSuites() {
            return this.f1740a.getSupportedCipherSuites();
        }

        public String[] getSupportedProtocols() {
            return this.f1740a.getSupportedProtocols();
        }

        public boolean getTcpNoDelay() throws SocketException {
            return this.f1740a.getTcpNoDelay();
        }

        public int getTrafficClass() throws SocketException {
            return this.f1740a.getTrafficClass();
        }

        public boolean getUseClientMode() {
            return this.f1740a.getUseClientMode();
        }

        public boolean getWantClientAuth() {
            return this.f1740a.getWantClientAuth();
        }

        public boolean isBound() {
            return this.f1740a.isBound();
        }

        public boolean isClosed() {
            return this.f1740a.isClosed();
        }

        public boolean isConnected() {
            return this.f1740a.isConnected();
        }

        public boolean isInputShutdown() {
            return this.f1740a.isInputShutdown();
        }

        public boolean isOutputShutdown() {
            return this.f1740a.isOutputShutdown();
        }

        public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
            this.f1740a.removeHandshakeCompletedListener(handshakeCompletedListener);
        }

        public void sendUrgentData(int i) throws IOException {
            this.f1740a.sendUrgentData(i);
        }

        public void setEnableSessionCreation(boolean z) {
            this.f1740a.setEnableSessionCreation(z);
        }

        public void setEnabledCipherSuites(String[] strArr) {
            this.f1740a.setEnabledCipherSuites(strArr);
        }

        public void setEnabledProtocols(String[] strArr) {
            this.f1740a.setEnabledProtocols(strArr);
        }

        public void setKeepAlive(boolean z) throws SocketException {
            this.f1740a.setKeepAlive(z);
        }

        public void setNeedClientAuth(boolean z) {
            this.f1740a.setNeedClientAuth(z);
        }

        public void setOOBInline(boolean z) throws SocketException {
            this.f1740a.setOOBInline(z);
        }

        public void setPerformancePreferences(int i, int i2, int i3) {
            this.f1740a.setPerformancePreferences(i, i2, i3);
        }

        public synchronized void setReceiveBufferSize(int i) throws SocketException {
            this.f1740a.setReceiveBufferSize(i);
        }

        public void setReuseAddress(boolean z) throws SocketException {
            this.f1740a.setReuseAddress(z);
        }

        public synchronized void setSendBufferSize(int i) throws SocketException {
            this.f1740a.setSendBufferSize(i);
        }

        public void setSoLinger(boolean z, int i) throws SocketException {
            this.f1740a.setSoLinger(z, i);
        }

        public synchronized void setSoTimeout(int i) throws SocketException {
            this.f1740a.setSoTimeout(i);
        }

        public void setTcpNoDelay(boolean z) throws SocketException {
            this.f1740a.setTcpNoDelay(z);
        }

        public void setTrafficClass(int i) throws SocketException {
            this.f1740a.setTrafficClass(i);
        }

        public void setUseClientMode(boolean z) {
            this.f1740a.setUseClientMode(z);
        }

        public void setWantClientAuth(boolean z) {
            this.f1740a.setWantClientAuth(z);
        }

        public void shutdownInput() throws IOException {
            this.f1740a.shutdownInput();
        }

        public void shutdownOutput() throws IOException {
            this.f1740a.shutdownOutput();
        }

        public void startHandshake() throws IOException {
            this.f1740a.startHandshake();
        }

        public String toString() {
            return this.f1740a.toString();
        }
    }

    private class b extends a {
        private b(SSLSocket sSLSocket) {
            super(sSLSocket);
        }

        public void setEnabledProtocols(String[] strArr) {
            PrintStream printStream;
            String sb;
            if (strArr != null && strArr.length == 1 && "SSLv3".equals(strArr[0])) {
                ArrayList arrayList = new ArrayList(Arrays.asList(this.f1740a.getEnabledProtocols()));
                if (arrayList.size() > 1) {
                    arrayList.remove("SSLv3");
                    printStream = System.out;
                    sb = "Removed SSLv3 from enabled protocols";
                } else {
                    printStream = System.out;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("SSL stuck with protocol available for ");
                    sb2.append(String.valueOf(arrayList));
                    sb = sb2.toString();
                }
                printStream.println(sb);
                strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
            }
            super.setEnabledProtocols(strArr);
        }
    }

    public t() {
        this.f1739a = HttpsURLConnection.getDefaultSSLSocketFactory();
    }

    public t(SSLSocketFactory sSLSocketFactory) {
        this.f1739a = sSLSocketFactory;
    }

    private Socket a(Socket socket) {
        return socket instanceof SSLSocket ? new b((SSLSocket) socket) : socket;
    }

    public Socket createSocket(String str, int i) throws IOException {
        return a(this.f1739a.createSocket(str, i));
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        return a(this.f1739a.createSocket(str, i, inetAddress, i2));
    }

    public Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return a(this.f1739a.createSocket(inetAddress, i));
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return a(this.f1739a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return a(this.f1739a.createSocket(socket, str, i, z));
    }

    public String[] getDefaultCipherSuites() {
        return this.f1739a.getDefaultCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return this.f1739a.getSupportedCipherSuites();
    }
}
