package com.appodeal.ads.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class h {

    /* renamed from: a reason: collision with root package name */
    private static ArrayList<File> f1712a = new ArrayList<>();

    public static void a(Activity activity) {
        a((Context) activity);
        try {
            Iterator it = f1712a.iterator();
            while (it.hasNext()) {
                a((File) it.next());
            }
        } catch (Exception unused) {
        }
    }

    @Deprecated
    public static void a(Context context) {
        f1712a.clear();
        b(new File(context.getExternalFilesDir(null), "/native_cache_image/"));
        b(new File(context.getFilesDir(), "adc/media"));
        b(new File(context.getFilesDir(), "adc3"));
        b(new File(context.getExternalFilesDir(null), CampaignEx.JSON_KEY_AD_AL));
        b(new File(context.getCacheDir(), CampaignEx.JSON_KEY_AD_AL));
        b(new File(context.getCacheDir(), ".chartboost"));
        b(new File(Environment.getExternalStorageDirectory(), ".chartboost"));
        b(new File(context.getExternalCacheDir(), "UnityAdsVideoCache"));
        b(new File(context.getExternalCacheDir(), "UnityAdsCache"));
        b(new File(Environment.getExternalStorageDirectory(), "UnityAdsVideoCache"));
        b(new File(context.getFilesDir(), "UnityAdsVideoCache"));
        b(new File(context.getFilesDir(), "UnityAdsCache"));
    }

    public static void a(File file) {
        try {
            if (!c(file)) {
                e(file);
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    private static void b(File file) {
        f1712a.add(file);
    }

    private static boolean c(File file) {
        return d(file) < 5242880;
    }

    private static long d(File file) {
        long j = 0;
        if (!file.exists()) {
            return 0;
        }
        if (!file.isDirectory()) {
            return file.length();
        }
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return 0;
        }
        for (File d : listFiles) {
            j += d(d);
        }
        return j;
    }

    private static void e(File file) {
        if (file.isDirectory()) {
            for (File e : file.listFiles()) {
                e(e);
            }
        } else if (!file.exists()) {
            return;
        }
        file.delete();
    }
}
