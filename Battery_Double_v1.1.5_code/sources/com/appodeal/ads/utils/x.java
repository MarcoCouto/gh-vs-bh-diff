package com.appodeal.ads.utils;

import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.util.SparseArray;
import com.appodeal.ads.br;
import com.appodeal.ads.utils.b.a;

public class x {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static SparseArray<Pair<Handler, Runnable>> f1745a = new SparseArray<>();

    public static void a(int i) {
        if (f1745a.get(i) != null) {
            ((Handler) ((Pair) f1745a.get(i)).first).removeCallbacks((Runnable) ((Pair) f1745a.get(i)).second);
            f1745a.remove(i);
        }
    }

    public static void a(final int i, final String str) {
        Handler handler = new Handler(Looper.getMainLooper());
        AnonymousClass1 r1 = new Runnable() {
            public void run() {
                Log.log(x.b(i, str));
                x.f1745a.remove(i);
            }
        };
        handler.postDelayed(r1, 3000);
        f1745a.put(i, new Pair(handler, r1));
    }

    static Exception b(int i, String str) {
        return new a(String.format("%s %s was not shown", new Object[]{br.c(str), br.a(i)}));
    }
}
