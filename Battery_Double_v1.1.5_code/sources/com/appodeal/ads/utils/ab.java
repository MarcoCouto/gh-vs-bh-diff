package com.appodeal.ads.utils;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import com.appodeal.ads.ag;
import com.appodeal.ads.br;
import com.appodeal.ads.utils.Log.LogLevel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class ab {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static final Map<Object, a> f1699a = new HashMap();
    private static float b = 0.8f;
    /* access modifiers changed from: private */
    public static int c = 3;
    /* access modifiers changed from: private */
    public static boolean d = true;
    private static final Handler e = new Handler(Looper.getMainLooper());

    @VisibleForTesting
    static class a {
        @VisibleForTesting

        /* renamed from: a reason: collision with root package name */
        final View f1700a;
        private final ag b;
        private final Rect c = new Rect();
        private final long d;
        /* access modifiers changed from: private */
        public final float e;
        private final b f;
        private OnPreDrawListener g;
        private OnAttachStateChangeListener h;
        private long i;
        private boolean j;
        private boolean k;
        private boolean l;
        /* access modifiers changed from: private */
        public boolean m = true;
        private boolean n = false;
        private final Runnable o = new Runnable() {
            public void run() {
                a.this.b();
            }
        };

        a(@NonNull ag agVar, @NonNull View view, long j2, float f2, @NonNull b bVar) {
            this.b = agVar;
            this.f1700a = view;
            this.d = j2;
            this.e = f2;
            this.f = bVar;
        }

        private float a(Rect rect, Rect rect2) {
            int width = rect.width() * rect.height();
            if (width == 0) {
                return 0.0f;
            }
            return ((float) (width - (Math.max(0, Math.min(rect.right, rect2.right) - Math.max(rect.left, rect2.left)) * Math.max(0, Math.min(rect.bottom, rect2.bottom) - Math.max(rect.top, rect2.top))))) / ((float) width);
        }

        private void a(@NonNull View view) {
            if (!this.j) {
                this.f.a();
                this.j = true;
            }
            if (!this.l && !this.k) {
                ab.b(this.o, this.d);
                this.i = System.currentTimeMillis();
                this.l = true;
            }
        }

        /* JADX WARNING: type inference failed for: r17v0, types: [android.view.View] */
        /* JADX WARNING: type inference failed for: r2v0, types: [android.view.View] */
        /* JADX WARNING: type inference failed for: r2v1, types: [android.view.View] */
        /* JADX WARNING: type inference failed for: r2v2 */
        /* JADX WARNING: type inference failed for: r2v3 */
        /* JADX WARNING: type inference failed for: r2v4, types: [android.view.View] */
        /* JADX WARNING: type inference failed for: r2v5 */
        /* JADX WARNING: type inference failed for: r2v6 */
        /* JADX WARNING: type inference failed for: r2v9 */
        /* JADX WARNING: type inference failed for: r2v10 */
        /* JADX WARNING: type inference failed for: r2v11 */
        /* JADX WARNING: type inference failed for: r2v12 */
        /* JADX WARNING: type inference failed for: r2v13 */
        /* JADX WARNING: type inference failed for: r2v14 */
        /* JADX WARNING: type inference failed for: r2v15 */
        /* JADX WARNING: type inference failed for: r2v16 */
        /* access modifiers changed from: private */
        /* JADX WARNING: Incorrect type for immutable var: ssa=android.view.View, code=null, for r17v0, types: [android.view.View] */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r2v2
  assigns: []
  uses: []
  mth insns count: 148
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0031 A[Catch:{ Exception -> 0x0159 }] */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0037 A[Catch:{ Exception -> 0x0159 }] */
        /* JADX WARNING: Unknown variable types count: 7 */
        public void a(@NonNull View r17, float f2) {
            ? r2;
            ? r22;
            ? r23;
            boolean z;
            ? r24;
            ? r25 = r17;
            try {
                r23 = r25;
                boolean globalVisibleRect = r25.getGlobalVisibleRect(this.c);
                boolean isShown = r17.isShown();
                boolean a2 = br.a((View) r17);
                if (ab.d) {
                    if (!r17.hasWindowFocus()) {
                        z = false;
                        if (!globalVisibleRect && isShown && z && !a2) {
                            a((View) r25, "Ad View is out of screen, show wasn't tracked");
                            return;
                        }
                        float width = (float) (r17.getWidth() * r17.getHeight());
                        if (width == 0.0f) {
                            a((View) r25, "Ad View width or height is zero, show wasn't tracked");
                            return;
                        }
                        float width2 = ((float) (this.c.width() * this.c.height())) / width;
                        if (width2 < f2) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Ad View is not completely visible (");
                            sb.append(width2);
                            sb.append("), show wasn't tracked");
                            a((View) r25, sb.toString());
                            return;
                        }
                        if (ab.d) {
                            Activity a3 = this.b.a();
                            View findViewById = a3 != null ? a3.findViewById(16908290) : null;
                            if (findViewById == null) {
                                a((View) r25, "Activity content layout not found, is your activity running?");
                                return;
                            }
                            Rect rect = new Rect();
                            findViewById.getGlobalVisibleRect(rect);
                            if (!Rect.intersects(this.c, rect)) {
                                a((View) r25, "Ad View is out of current window, show wasn't tracked");
                                return;
                            }
                        }
                        ViewGroup viewGroup = (ViewGroup) r17.getRootView();
                        ViewGroup viewGroup2 = (ViewGroup) r17.getParent();
                        int i2 = 0;
                        ? r26 = r25;
                        while (viewGroup2 != null) {
                            for (int indexOfChild = viewGroup2.indexOfChild(r26) + 1; indexOfChild < viewGroup2.getChildCount(); indexOfChild++) {
                                View childAt = viewGroup2.getChildAt(indexOfChild);
                                if (childAt.getVisibility() == 0) {
                                    childAt.getLocationInWindow(new int[2]);
                                    Rect c2 = br.c(childAt);
                                    if (Rect.intersects(this.c, c2)) {
                                        float a4 = a(this.c, c2);
                                        String valueOf = String.valueOf(childAt.getId());
                                        try {
                                            valueOf = r26.getContext().getResources().getResourceEntryName(childAt.getId());
                                        } catch (Exception unused) {
                                        }
                                        r23 = r26;
                                        Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_VIEWABILITY, String.format("Ad view is overlapped by another visible view (type: %s, id: %s), visible percent: %s", new Object[]{childAt.getClass().getSimpleName(), valueOf, Float.valueOf(a4)}), LogLevel.verbose);
                                        if (a4 < f2) {
                                            a((View) r26, "Ad View is covered by another view, show wasn't tracked");
                                            return;
                                        }
                                        i2++;
                                        if (i2 >= ab.c) {
                                            a((View) r26, "Ad View is covered by too many views, show wasn't tracked");
                                            return;
                                        }
                                    } else {
                                        continue;
                                    }
                                }
                            }
                            if (viewGroup2 != viewGroup) {
                                try {
                                    r24 = viewGroup2;
                                    viewGroup2 = (ViewGroup) viewGroup2.getParent();
                                } catch (Exception e2) {
                                    e = e2;
                                    r22 = viewGroup2;
                                    Log.log(e);
                                    r2 = r22;
                                    a((View) r2);
                                }
                            } else {
                                viewGroup2 = null;
                                r24 = r26;
                            }
                            r26 = r24;
                        }
                        r2 = r26;
                        a((View) r2);
                    }
                }
                z = true;
                if (!globalVisibleRect && isShown && z && !a2) {
                }
            } catch (Exception e3) {
                e = e3;
                r22 = r23;
                Log.log(e);
                r2 = r22;
                a((View) r2);
            }
        }

        private void a(@NonNull View view, @NonNull String str) {
            if (this.m) {
                this.m = false;
            } else if (!this.n) {
                this.n = true;
                Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_VIEWABILITY, str, LogLevel.verbose);
            }
            if (!this.k) {
                ab.b(this.o);
                this.l = false;
                this.i = 0;
            }
        }

        /* access modifiers changed from: private */
        public void b() {
            if (this.j && !this.k && this.d > -1 && this.i > 0 && System.currentTimeMillis() - this.i >= this.d) {
                this.k = true;
                this.f.b();
            }
            this.f1700a.removeOnAttachStateChangeListener(this.h);
            this.f1700a.getViewTreeObserver().removeOnPreDrawListener(this.g);
            ab.b(this.o);
            synchronized (ab.f1699a) {
                ab.f1699a.remove(this);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            if (this.g == null) {
                this.g = new OnPreDrawListener() {
                    public boolean onPreDraw() {
                        a.this.m = false;
                        a.this.a(a.this.f1700a, a.this.e);
                        return true;
                    }
                };
            }
            if (this.h == null) {
                this.h = new OnAttachStateChangeListener() {
                    public void onViewAttachedToWindow(View view) {
                    }

                    public void onViewDetachedFromWindow(View view) {
                        a.this.b();
                    }
                };
            }
            this.f1700a.addOnAttachStateChangeListener(this.h);
            this.f1700a.getViewTreeObserver().addOnPreDrawListener(this.g);
            a(this.f1700a, this.e);
        }
    }

    public interface b {
        void a();

        void b();
    }

    public static void a(@NonNull View view) {
        synchronized (f1699a) {
            Iterator it = f1699a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Entry entry = (Entry) it.next();
                if (((a) entry.getValue()).f1700a == view) {
                    ((a) entry.getValue()).b();
                    f1699a.remove(entry.getKey());
                    break;
                }
            }
        }
    }

    public static void a(@NonNull Object obj) {
        synchronized (f1699a) {
            a aVar = (a) f1699a.get(obj);
            if (aVar != null) {
                aVar.b();
                f1699a.remove(obj);
            }
        }
    }

    public static void a(@NonNull Object obj, @NonNull View view, long j, @FloatRange(from = 0.0d, to = 1.0d) float f, @NonNull b bVar) {
        synchronized (f1699a) {
            a(obj);
            a aVar = new a(new ag(null), view, j, f, bVar);
            f1699a.put(obj, aVar);
            aVar.a();
        }
    }

    public static void a(@NonNull Object obj, @NonNull View view, long j, @NonNull b bVar) {
        a(obj, view, j, b, bVar);
    }

    public static void a(boolean z) {
        d = z;
    }

    /* access modifiers changed from: private */
    public static void b(Runnable runnable) {
        e.removeCallbacks(runnable);
    }

    /* access modifiers changed from: private */
    public static void b(Runnable runnable, long j) {
        e.postDelayed(runnable, j);
    }
}
