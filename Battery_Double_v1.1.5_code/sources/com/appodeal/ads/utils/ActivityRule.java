package com.appodeal.ads.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import com.appodeal.ads.br;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ActivityRule {

    /* renamed from: a reason: collision with root package name */
    private final String f1677a;
    private final List<RuleVerification> b;

    public static class Builder {

        /* renamed from: a reason: collision with root package name */
        private String f1678a;
        private List<RuleVerification> b = new ArrayList();

        public Builder(String str) {
            this.f1678a = str;
        }

        public Builder addVerification(RuleVerification ruleVerification) {
            if (ruleVerification != null) {
                this.b.add(ruleVerification);
            }
            return this;
        }

        public ActivityRule build() {
            return new ActivityRule(this.f1678a, this.b);
        }
    }

    public static class FullscreenVerify implements RuleVerification {
        private static boolean a(Context context, int i) {
            return br.b(context, i, 16842840) || br.b(context, i, 16842839) || (VERSION.SDK_INT >= 20 && !br.a(context, i, 16842840) && br.b(context, i, 16843763));
        }

        public void verify(Context context, ActivityInfo activityInfo) {
            if (context.getApplicationInfo().targetSdkVersion >= 27 && VERSION.SDK_INT >= 26 && a(context, activityInfo.theme)) {
                Log.log(new IllegalStateException(String.format(Locale.ENGLISH, "Attention! Only fullscreen activities can request orientation: %s", new Object[]{activityInfo.name})));
            }
        }
    }

    public interface RuleVerification {
        void verify(Context context, ActivityInfo activityInfo);
    }

    private ActivityRule(String str, List<RuleVerification> list) {
        this.f1677a = str;
        this.b = list;
    }

    public static String[] a(ActivityRule[] activityRuleArr) {
        String[] strArr = new String[activityRuleArr.length];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = activityRuleArr[i].a();
        }
        return strArr;
    }

    public String a() {
        return this.f1677a;
    }

    public void a(Context context) {
        ActivityInfo activityInfo;
        try {
            activityInfo = context.getPackageManager().getActivityInfo(new ComponentName(context, this.f1677a), 128);
        } catch (NameNotFoundException e) {
            Log.log(e);
            activityInfo = null;
        }
        if (activityInfo != null) {
            for (RuleVerification verify : this.b) {
                verify.verify(context, activityInfo);
            }
        }
    }
}
