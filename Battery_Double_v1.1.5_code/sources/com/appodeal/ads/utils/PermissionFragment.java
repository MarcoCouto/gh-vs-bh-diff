package com.appodeal.ads.utils;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;
import java.util.Iterator;

@TargetApi(23)
public class PermissionFragment extends Fragment {

    /* renamed from: a reason: collision with root package name */
    private ArrayList<String> f1690a;

    public void a() {
        ArrayList arrayList = new ArrayList();
        Iterator it = this.f1690a.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (getActivity().checkSelfPermission(str) != 0) {
                arrayList.add(str);
            } else {
                PermissionsHelper.a().a(0, new String[]{str}, new int[]{0});
            }
        }
        if (!arrayList.isEmpty()) {
            requestPermissions((String[]) arrayList.toArray(new String[0]), 1);
        } else {
            getFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f1690a = arguments.getStringArrayList(NativeProtocol.RESULT_ARGS_PERMISSIONS);
            a();
        }
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        PermissionsHelper.a().a(i, strArr, iArr);
        getFragmentManager().beginTransaction().remove(this).commit();
    }
}
