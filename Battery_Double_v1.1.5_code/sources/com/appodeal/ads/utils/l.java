package com.appodeal.ads.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.br;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import javax.net.ssl.HttpsURLConnection;

public class l implements Runnable {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    String f1722a;
    private Handler b = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            if (l.this.h != null) {
                switch (message.what) {
                    case 0:
                        l.this.h.a();
                        return;
                    case 1:
                        l.this.h.a((String) message.obj);
                        return;
                    case 2:
                        l.this.h.a((Bitmap) message.obj);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private final int c = 0;
    private final int d = 1;
    private final int e = 2;
    private File f;
    private boolean g;
    /* access modifiers changed from: private */
    public b h;
    private int i;
    private int j;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private Context f1724a;
        private String b;
        private boolean c;
        private b d;

        public a(Context context, String str) {
            this.f1724a = context;
            this.b = str;
        }

        public a a(b bVar) {
            this.d = bVar;
            return this;
        }

        public a a(boolean z) {
            this.c = z;
            return this;
        }

        public l a() {
            return new l(this.f1724a, this.b, this.c, this.d);
        }
    }

    public interface b {
        void a();

        void a(Bitmap bitmap);

        void a(String str);
    }

    l(@Nullable Context context, String str, boolean z, b bVar) {
        if (context == null) {
            bVar.a();
            return;
        }
        this.f1722a = str;
        this.g = z;
        this.h = bVar;
        this.i = p.a(context);
        this.j = p.a(this.i, z);
        if (br.v(context)) {
            this.f = br.d(context, "native_cache_image");
        }
    }

    private Bitmap a(byte[] bArr, Options options) {
        ByteArrayOutputStream byteArrayOutputStream;
        options.inJustDecodeBounds = false;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream(bArr.length);
            try {
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
                decodeByteArray.compress(CompressFormat.PNG, 85, byteArrayOutputStream);
                decodeByteArray.recycle();
                Bitmap decodeStream = BitmapFactory.decodeStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
                br.a((Flushable) byteArrayOutputStream);
                br.a((Closeable) byteArrayOutputStream);
                return decodeStream;
            } catch (Exception e2) {
                e = e2;
                try {
                    Log.log(e);
                    br.a((Flushable) byteArrayOutputStream);
                    br.a((Closeable) byteArrayOutputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    br.a((Flushable) byteArrayOutputStream);
                    br.a((Closeable) byteArrayOutputStream);
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            byteArrayOutputStream = null;
            Log.log(e);
            br.a((Flushable) byteArrayOutputStream);
            br.a((Closeable) byteArrayOutputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            byteArrayOutputStream = null;
            br.a((Flushable) byteArrayOutputStream);
            br.a((Closeable) byteArrayOutputStream);
            throw th;
        }
    }

    private void a() {
        if (this.b != null) {
            this.b.sendEmptyMessage(0);
        }
    }

    private void a(Bitmap bitmap) {
        if (this.b != null) {
            this.b.sendMessage(this.b.obtainMessage(2, bitmap));
        }
    }

    private void a(File file, byte[] bArr, Options options) {
        options.inJustDecodeBounds = false;
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fileOutputStream2 = new FileOutputStream(file);
            try {
                BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options).compress(CompressFormat.PNG, 85, fileOutputStream2);
                br.a((Flushable) fileOutputStream2);
                br.a((Closeable) fileOutputStream2);
            } catch (Exception e2) {
                e = e2;
                fileOutputStream = fileOutputStream2;
                try {
                    Log.log(e);
                    br.a((Flushable) fileOutputStream);
                    br.a((Closeable) fileOutputStream);
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream2 = fileOutputStream;
                    br.a((Flushable) fileOutputStream2);
                    br.a((Closeable) fileOutputStream2);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                br.a((Flushable) fileOutputStream2);
                br.a((Closeable) fileOutputStream2);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            Log.log(e);
            br.a((Flushable) fileOutputStream);
            br.a((Closeable) fileOutputStream);
        }
    }

    private void a(String str) {
        if (this.b != null) {
            this.b.sendMessage(this.b.obtainMessage(1, str));
        }
    }

    private void a(URLConnection uRLConnection) {
        try {
            if (uRLConnection instanceof HttpsURLConnection) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) uRLConnection;
                httpsURLConnection.setSSLSocketFactory(new t(httpsURLConnection.getSSLSocketFactory()));
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    private void b(String str) {
        File file;
        InputStream inputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        Options options = new Options();
        options.inJustDecodeBounds = true;
        ByteArrayOutputStream byteArrayOutputStream2 = null;
        if (this.f != null) {
            file = new File(this.f, br.g(str));
            if (file.exists() && file.length() > 0) {
                BitmapFactory.decodeFile(file.getPath(), options);
                if (a(options)) {
                    a(file.getAbsolutePath());
                } else {
                    a();
                }
                return;
            }
        } else {
            file = null;
        }
        try {
            inputStream = c(str).getInputStream();
            try {
                byteArrayOutputStream = new ByteArrayOutputStream(inputStream.available());
                try {
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        byteArrayOutputStream.write(bArr, 0, read);
                    }
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                    if (!a(options)) {
                        a();
                        br.a((Flushable) byteArrayOutputStream);
                        br.a((Closeable) byteArrayOutputStream);
                        br.a((Closeable) inputStream);
                        return;
                    }
                    if (file != null) {
                        a(file, byteArray, options);
                        a(file.getAbsolutePath());
                    } else {
                        options.inSampleSize = p.a(options, this.i, this.j);
                        Bitmap a2 = a(byteArray, options);
                        if (a2 != null) {
                            a(a2);
                        } else {
                            a();
                        }
                    }
                    br.a((Flushable) byteArrayOutputStream);
                    br.a((Closeable) byteArrayOutputStream);
                    br.a((Closeable) inputStream);
                } catch (Exception e2) {
                    e = e2;
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    try {
                        Log.log(e);
                        a();
                        br.a((Flushable) byteArrayOutputStream2);
                        br.a((Closeable) byteArrayOutputStream2);
                        br.a((Closeable) inputStream);
                    } catch (Throwable th) {
                        th = th;
                        byteArrayOutputStream = byteArrayOutputStream2;
                        br.a((Flushable) byteArrayOutputStream);
                        br.a((Closeable) byteArrayOutputStream);
                        br.a((Closeable) inputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    br.a((Flushable) byteArrayOutputStream);
                    br.a((Closeable) byteArrayOutputStream);
                    br.a((Closeable) inputStream);
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                Log.log(e);
                a();
                br.a((Flushable) byteArrayOutputStream2);
                br.a((Closeable) byteArrayOutputStream2);
                br.a((Closeable) inputStream);
            }
        } catch (Exception e4) {
            e = e4;
            inputStream = null;
            Log.log(e);
            a();
            br.a((Flushable) byteArrayOutputStream2);
            br.a((Closeable) byteArrayOutputStream2);
            br.a((Closeable) inputStream);
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            byteArrayOutputStream = null;
            br.a((Flushable) byteArrayOutputStream);
            br.a((Closeable) byteArrayOutputStream);
            br.a((Closeable) inputStream);
            throw th;
        }
    }

    private HttpURLConnection c(String str) throws IOException {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(20000);
            httpURLConnection.setReadTimeout(20000);
            a((URLConnection) httpURLConnection);
            httpURLConnection.connect();
            return httpURLConnection;
        } catch (Exception unused) {
            Builder buildUpon = Uri.parse(str).buildUpon();
            buildUpon.scheme("http");
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(buildUpon.build().toString()).openConnection();
            httpURLConnection2.setConnectTimeout(20000);
            httpURLConnection2.setReadTimeout(20000);
            httpURLConnection2.connect();
            return httpURLConnection2;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(Options options) {
        if (!this.g) {
            return true;
        }
        return ((float) options.outWidth) / ((float) options.outHeight) >= 1.5f;
    }

    public void run() {
        if (TextUtils.isEmpty(this.f1722a) || !br.a(this.f1722a)) {
            a();
            return;
        }
        this.f1722a = this.f1722a.replace(" ", "%20");
        b(this.f1722a);
    }
}
