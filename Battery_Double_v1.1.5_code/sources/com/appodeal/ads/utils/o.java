package com.appodeal.ads.utils;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.i;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class o {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static Map<i, a> f1729a = new HashMap();
    private static Handler b = new Handler(Looper.getMainLooper());

    public static class a implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private i f1730a;
        private b b;
        private long c;

        a(i iVar, b bVar) {
            this.f1730a = iVar;
            this.b = bVar;
            this.c = System.currentTimeMillis() + (iVar.getExpTime() * 1000);
        }

        /* access modifiers changed from: 0000 */
        public long a() {
            return this.c;
        }

        public void run() {
            o.a(this.f1730a);
            if (this.b != null) {
                this.b.a(this.f1730a);
            }
        }
    }

    public interface b<AdObjectType extends i> {
        void a(AdObjectType adobjecttype);
    }

    public static void a(@Nullable i iVar) {
        if (iVar != null) {
            c(iVar);
            f1729a.remove(iVar);
        }
    }

    public static void a(@Nullable i iVar, b bVar) {
        if (iVar != null && iVar.getExpTime() > 0) {
            c(iVar);
            f1729a.put(iVar, new a(iVar, bVar));
            b(iVar);
        }
    }

    public static void a(@Nullable Collection<? extends i> collection) {
        if (collection != null) {
            for (i a2 : collection) {
                a(a2);
            }
        }
    }

    public static void b(@Nullable i iVar) {
        if (iVar != null && iVar.getExpTime() > 0) {
            a aVar = (a) f1729a.get(iVar);
            if (aVar != null) {
                long a2 = aVar.a() - System.currentTimeMillis();
                if (a2 > 0) {
                    c(iVar);
                    b.postDelayed(aVar, a2);
                    return;
                }
                aVar.run();
            }
        }
    }

    public static void b(@Nullable Collection<i> collection) {
        if (collection != null) {
            for (i b2 : collection) {
                b(b2);
            }
        }
    }

    public static void c(@Nullable i iVar) {
        if (iVar != null) {
            Runnable runnable = (Runnable) f1729a.get(iVar);
            if (runnable != null) {
                b.removeCallbacks(runnable);
            }
        }
    }

    public static void c(@Nullable Collection<i> collection) {
        if (collection != null) {
            for (i c : collection) {
                c(c);
            }
        }
    }
}
