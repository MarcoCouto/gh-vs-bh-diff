package com.appodeal.ads.utils;

import android.support.annotation.NonNull;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ThreadFactory;

final class u implements ThreadFactory {

    /* renamed from: a reason: collision with root package name */
    private final int f1741a;

    u(int i) {
        this.f1741a = i;
    }

    public Thread newThread(@NonNull Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setPriority(this.f1741a);
        thread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable th) {
                Log.log(th);
            }
        });
        return thread;
    }
}
