package com.appodeal.ads.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.AdType;
import com.appodeal.ads.af;
import com.appodeal.ads.bm;
import com.appodeal.ads.i;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public class EventsTracker {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    static EventsTracker f1680a;
    private final Map<String, a> b = new HashMap();
    private final a c = new a(null);
    private final Map<String, EventsListener> d = new HashMap();

    /* renamed from: com.appodeal.ads.utils.EventsTracker$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f1681a = new int[EventType.values().length];

        static {
            try {
                f1681a[EventType.Impression.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public enum EventType {
        Impression,
        Click,
        Finish,
        FailedToLoad,
        InternalError,
        Expired
    }

    public interface EventsListener {
        void onImpressionStored(int i, @Nullable String str);
    }

    private static class a {

        /* renamed from: a reason: collision with root package name */
        private final EnumMap<EventType, AtomicInteger> f1682a = new EnumMap<>(EventType.class);
        private final EnumMap<EventType, Map<String, AtomicInteger>> b = new EnumMap<>(EventType.class);
        @Nullable
        private final a c;

        a(@Nullable a aVar) {
            this.c = aVar;
        }

        /* access modifiers changed from: 0000 */
        public int a(EventType eventType) {
            if (this.f1682a.containsKey(eventType)) {
                return ((AtomicInteger) this.f1682a.get(eventType)).get();
            }
            return 0;
        }

        /* access modifiers changed from: 0000 */
        public void a(EventType eventType, final String str) {
            if (this.c != null) {
                this.c.a(eventType, str);
            }
            if (this.f1682a.get(eventType) == null) {
                this.f1682a.put(eventType, new AtomicInteger(1));
            } else {
                ((AtomicInteger) this.f1682a.get(eventType)).incrementAndGet();
            }
            if (TextUtils.isEmpty(str)) {
                return;
            }
            if (this.b.get(eventType) == null || !((Map) this.b.get(eventType)).containsKey(str)) {
                this.b.put(eventType, new HashMap<String, AtomicInteger>() {
                    {
                        put(str, new AtomicInteger(1));
                    }
                });
            } else {
                ((AtomicInteger) ((Map) this.b.get(eventType)).get(str)).incrementAndGet();
            }
        }
    }

    private EventsTracker() {
    }

    private a a(String str) {
        if (this.b.containsKey(str)) {
            return (a) this.b.get(str);
        }
        a aVar = new a(this.c);
        this.b.put(str, aVar);
        return aVar;
    }

    private void a(int i, @Nullable String str, EventType eventType) {
        if (AnonymousClass1.f1681a[eventType.ordinal()] == 1) {
            for (EventsListener onImpressionStored : this.d.values()) {
                onImpressionStored.onImpressionStored(i, str);
            }
        }
    }

    private void a(Context context, String str, EventType eventType) {
        try {
            JSONObject jSONObject = new JSONObject(bm.a(context).b().getString(eventType.name(), "{}"));
            jSONObject.put(str, jSONObject.optInt(str, 0) + 1);
            bm.a(context).a().putString(eventType.name(), jSONObject.toString()).apply();
        } catch (JSONException e) {
            Log.log(e);
        }
    }

    private void a(@NonNull Context context, @Nullable String str, @Nullable String str2, @NonNull EventType eventType) {
        if (str != null) {
            a(str).a(eventType, str2);
            a(context, str, eventType);
        }
    }

    public static EventsTracker get() {
        if (f1680a != null) {
            return f1680a;
        }
        EventsTracker eventsTracker = new EventsTracker();
        f1680a = eventsTracker;
        return eventsTracker;
    }

    public int a(@NonNull EventType eventType) {
        return this.c.a(eventType);
    }

    public int a(@Nullable String str, @NonNull EventType... eventTypeArr) {
        if (str == null) {
            return 0;
        }
        int i = 0;
        for (EventType a2 : eventTypeArr) {
            i += a(str).a(a2);
        }
        return i;
    }

    public JSONObject a(@NonNull Context context, @NonNull EventType eventType) {
        AdType[] values;
        try {
            JSONObject jSONObject = new JSONObject(bm.a(context).b().getString(eventType.name(), "{}"));
            for (AdType adType : AdType.values()) {
                if (!jSONObject.has(adType.getCodeName())) {
                    jSONObject.put(adType.getCodeName(), 0);
                }
            }
            return jSONObject;
        } catch (JSONException e) {
            Log.log(e);
            return null;
        }
    }

    public void a(@NonNull Context context, int i, @Nullable i iVar, @NonNull EventType eventType) {
        String name = iVar != null ? iVar.b().getName() : null;
        a(context, af.b(i), name, eventType);
        a(i, name, eventType);
    }

    public JSONObject b(@NonNull EventType eventType) {
        AdType[] values;
        JSONObject jSONObject = new JSONObject();
        try {
            for (AdType adType : AdType.values()) {
                jSONObject.put(adType.getCodeName(), a(adType.getCodeName()).a(eventType));
            }
        } catch (JSONException e) {
            Log.log(e);
        }
        return jSONObject;
    }

    public int getEventCount(@NonNull EventType eventType, @Nullable AdType... adTypeArr) {
        if (adTypeArr == null) {
            return 0;
        }
        int i = 0;
        for (AdType codeName : adTypeArr) {
            i += a(codeName.getCodeName()).a(eventType);
        }
        return i;
    }

    public void subscribeEventsListener(String str, EventsListener eventsListener) {
        this.d.put(str, eventsListener);
    }
}
