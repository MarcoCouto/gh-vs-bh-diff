package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

class az extends c<bb, ba, ay> {
    @Nullable
    private static NativeCallbacks e;

    /* renamed from: a reason: collision with root package name */
    boolean f1571a = false;
    final List<NativeAd> b = new ArrayList();
    private int c = 2;
    private boolean d = false;

    az() {
    }

    static void a(@Nullable NativeCallbacks nativeCallbacks) {
        e = nativeCallbacks;
    }

    private void a(boolean z) {
        synchronized (this.b) {
            p a2 = Native.a();
            if (z || a2.r()) {
                int e2 = e() - (this.b.size() - f());
                if (e2 > 0) {
                    Native.f1497a = e2;
                    bb bbVar = (bb) a2.y();
                    if (bbVar == null || !bbVar.J()) {
                        Native.a().d(Appodeal.f);
                    }
                } else if (!this.d) {
                    this.d = true;
                    if (e != null) {
                        e.onNativeLoaded();
                    }
                }
            }
        }
    }

    private int e() {
        if (w.h > 0 && w.h != this.c) {
            this.c = w.h;
        }
        return this.c;
    }

    private int f() {
        int i;
        synchronized (this.b) {
            i = 0;
            for (NativeAd isPrecache : this.b) {
                if (isPrecache.isPrecache()) {
                    i++;
                }
            }
        }
        return i;
    }

    public void a() {
        a(false, false, false);
    }

    public void a(int i) {
        if (i > 5) {
            i = 5;
        }
        if (i < 2) {
            i = 2;
        }
        this.c = i;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull bb bbVar, @NonNull ba baVar) {
        List x = baVar.x();
        synchronized (this.b) {
            this.b.addAll(x);
            Collections.sort(this.b, new Comparator<NativeAd>() {
                /* renamed from: a */
                public int compare(NativeAd nativeAd, NativeAd nativeAd2) {
                    return Double.compare(nativeAd2.getPredictedEcpm(), nativeAd.getPredictedEcpm());
                }
            });
        }
        if (!this.d) {
            this.d = true;
            Appodeal.b();
            Log.log("NativeAdBox", LogConstants.EVENT_NOTIFY_LOADED, String.format(Locale.ENGLISH, "available count of Native Ads: %d", new Object[]{Integer.valueOf(this.b.size())}));
            if (e != null) {
                e.onNativeLoaded();
            }
        }
        if (!bbVar.a()) {
            a(false);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable bb bbVar, @Nullable ba baVar, @Nullable LoadingError loadingError) {
        if (!this.d && !this.f1571a) {
            this.f1571a = true;
            Log.log(LogConstants.KEY_NATIVE, LogConstants.EVENT_NOTIFY_LOAD_FAILED, LogLevel.verbose);
            if (e != null) {
                e.onNativeFailedToLoad();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable bb bbVar, @Nullable ba baVar, @Nullable ay ayVar) {
        Log.log(LogConstants.KEY_NATIVE, LogConstants.EVENT_NOTIFY_SHOWN, LogLevel.verbose);
        if (e != null) {
            e.onNativeShown(ayVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable bb bbVar, @Nullable ba baVar, @Nullable ay ayVar, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_NATIVE, LogConstants.EVENT_NOTIFY_SHOW_FAILED, LogLevel.verbose);
        if (e != null) {
            e.onNativeShowFailed(ayVar);
        }
    }

    public void a(boolean z, boolean z2, boolean z3) {
        synchronized (this.b) {
            if (this.b.size() == 0) {
                this.d = false;
                this.f1571a = false;
            }
            if (z) {
                this.b.clear();
                Native.a().b(Appodeal.f, ((c) ((c) new c().a(true)).c(z2)).d(z3));
            } else {
                a(true);
            }
        }
    }

    public List<NativeAd> b(int i) {
        ArrayList<NativeAd> arrayList;
        synchronized (this.b) {
            if (i >= this.b.size()) {
                arrayList = new ArrayList<>(this.b);
            } else {
                ArrayList arrayList2 = new ArrayList(i);
                for (int i2 = 0; i2 < i; i2++) {
                    arrayList2.add(this.b.get(i2));
                }
                arrayList = arrayList2;
            }
            for (NativeAd nativeAd : arrayList) {
                o.a((i) ((ay) nativeAd).o());
            }
            this.b.removeAll(arrayList);
            if (this.b.size() == 0) {
                this.d = false;
                this.f1571a = false;
            }
            Log.log("NativeAdBox", LogConstants.EVENT_GET_ADS, String.format(Locale.ENGLISH, "available count of Native Ads: %d", new Object[]{Integer.valueOf(this.b.size())}));
            a(false);
        }
        return arrayList;
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull bb bbVar, @NonNull ba baVar) {
        if (this.b.size() == 0) {
            Log.log(LogConstants.KEY_NATIVE, LogConstants.EVENT_NOTIFY_EXPIRED, LogLevel.verbose);
            if (e != null) {
                e.onNativeExpired();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull bb bbVar, @NonNull ba baVar, @Nullable ay ayVar) {
        Log.log(LogConstants.KEY_NATIVE, LogConstants.EVENT_NOTIFY_CLICKED, LogLevel.verbose);
        if (e != null) {
            e.onNativeClicked(ayVar);
        }
    }

    public boolean b() {
        boolean z;
        synchronized (this.b) {
            z = !this.b.isEmpty();
        }
        return z;
    }

    public int c() {
        int size;
        synchronized (this.b) {
            size = this.b.size();
        }
        return size;
    }

    /* access modifiers changed from: 0000 */
    public Set<i> d() {
        HashSet hashSet;
        synchronized (this.b) {
            hashSet = new HashSet();
            for (NativeAd nativeAd : this.b) {
                hashSet.add(((ay) nativeAd).o());
            }
        }
        return hashSet;
    }
}
