package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.m;
import com.appodeal.ads.unified.UnifiedAd;
import com.appodeal.ads.unified.UnifiedAdCallback;
import com.appodeal.ads.unified.UnifiedAdParams;
import com.appodeal.ads.utils.ExchangeAd;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.ab;
import com.appodeal.ads.utils.app.AppState;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class i<AdRequestType extends m, UnifiedAdType extends UnifiedAd, UnifiedAdParamsType extends UnifiedAdParams, UnifiedAdCallbackType extends UnifiedAdCallback> implements C0137r {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    bo f1637a;
    @VisibleForTesting
    ExchangeAd b;
    @VisibleForTesting
    com.appodeal.ads.utils.a.b c;
    public b d = b.Wait;
    private AdRequestType e;
    /* access modifiers changed from: private */
    public AdNetwork f;
    private String g;
    private List<String> h = new ArrayList();
    /* access modifiers changed from: private */
    @Nullable
    public UnifiedAdType i;
    /* access modifiers changed from: private */
    @Nullable
    public UnifiedAdParamsType j;
    /* access modifiers changed from: private */
    @Nullable
    public UnifiedAdCallbackType k;
    private JSONObject l;
    /* access modifiers changed from: private */
    @Nullable
    public Object m;
    private int n;

    public interface a<AdRequestType extends m> {
        void a(@Nullable AdRequestType adrequesttype, @Nullable LoadingError loadingError);

        void a(@Nullable AdRequestType adrequesttype, @NonNull Throwable th);
    }

    @VisibleForTesting(otherwise = 3)
    public enum b {
        Wait,
        Loaded,
        FailedToLoad
    }

    i(@NonNull AdRequestType adrequesttype, @NonNull AdNetwork adNetwork, @Deprecated @NonNull bo boVar, int i2) {
        this.e = adrequesttype;
        this.f = adNetwork;
        this.f1637a = boVar;
        this.g = adNetwork.getName();
        this.n = i2;
    }

    private void b(@Nullable String str) {
        this.g = str;
    }

    @NonNull
    public AdRequestType a() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public abstract UnifiedAdType a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i2);

    public void a(double d2) {
        this.f1637a.a(d2);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        if (this.b != null) {
            this.b.trackImpression(i2);
        }
        if (this.c != null) {
            this.c.b(Appodeal.f);
        }
        if (this.i != null) {
            this.i.onImpression();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Activity activity) {
        if (this.i == null) {
            return;
        }
        if (this.j != null && this.m != null) {
            this.i.onPrepareToShow(activity, this.j, this.m);
        } else if (this.k != null) {
            this.k.onAdShowFailed();
        }
    }

    public void a(Activity activity, AdRequestType adrequesttype, int i2, a<AdRequestType> aVar) throws Exception {
        JSONObject optJSONObject = this.f1637a.getJsonData().optJSONObject("freq");
        if (optJSONObject != null) {
            this.c = new com.appodeal.ads.utils.a.b(activity, optJSONObject, getJsonData().optString("package"));
            if (!this.c.a((Context) activity)) {
                adrequesttype.a((AdUnit) this);
                aVar.a(adrequesttype, LoadingError.Canceled);
                return;
            }
        }
        LoadingError s = s();
        if (s == null) {
            s = this.f.verifyLoadAvailability(adrequesttype.T());
        }
        if (s != null) {
            aVar.a(adrequesttype, s);
        } else {
            AdNetwork b2 = b();
            g gVar = new g(adrequesttype, this, bh.f1600a);
            final Activity activity2 = activity;
            final int i3 = i2;
            final a<AdRequestType> aVar2 = aVar;
            final AdRequestType adrequesttype2 = adrequesttype;
            AnonymousClass1 r3 = new NetworkInitializationListener() {
                public void onInitializationFailed(@Nullable final LoadingError loadingError) {
                    br.a((Runnable) new Runnable() {
                        public void run() {
                            aVar2.a(adrequesttype2, loadingError);
                        }
                    });
                }

                public void onInitializationFinished(@NonNull Object obj) throws Exception {
                    Runnable r6;
                    if (i.this.c().getRequestResult() == null) {
                        i.this.m = obj;
                        i.this.i = i.this.a(activity2, i.this.f, obj, i3);
                        if (i.this.i == null) {
                            r6 = new Runnable() {
                                public void run() {
                                    aVar2.a(adrequesttype2, LoadingError.AdTypeNotSupportedInAdapter);
                                }
                            };
                        } else {
                            i.this.j = i.this.b(i3);
                            i.this.k = i.this.o();
                            r6 = new Runnable() {
                                public void run() {
                                    try {
                                        i.this.a(activity2, i.this.j, i.this.m, i.this.k, i.this.i);
                                    } catch (Throwable th) {
                                        aVar2.a(adrequesttype2, th);
                                    }
                                }
                            };
                        }
                        br.a(r6);
                    }
                }
            };
            b2.initialize(activity, this, gVar, r3);
        }
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull Object obj, @NonNull UnifiedAdCallbackType unifiedadcallbacktype, @NonNull UnifiedAdType unifiedadtype) throws Exception {
        unifiedadtype.load(activity, unifiedadparamstype, obj, unifiedadcallbacktype);
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable Activity activity, @NonNull AppState appState, boolean z) {
        UnifiedAd n2 = n();
        UnifiedAdCallback p = p();
        if (n2 != null && p != null) {
            n2.onAppStateChanged(activity, appState, p, z);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull Context context) {
        if (this.b != null) {
            this.b.trackClick();
        }
        if (this.c != null) {
            this.c.c(context);
        }
        if (this.i != null) {
            this.i.onClicked();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable Bundle bundle) {
        if (bundle != null) {
            if (bundle.containsKey("exchange_ad")) {
                this.b = (ExchangeAd) bundle.getParcelable("exchange_ad");
            }
            if (bundle.containsKey("id")) {
                a(bundle.getString("id"));
            }
            if (bundle.containsKey("demand_source")) {
                b(bundle.getString("demand_source"));
            }
            if (bundle.containsKey(RequestInfoKeys.APPODEAL_ECPM)) {
                a(bundle.getDouble(RequestInfoKeys.APPODEAL_ECPM));
            }
            if (bundle.containsKey("additional_stats")) {
                try {
                    this.l = new JSONObject(bundle.getString("additional_stats"));
                } catch (Throwable th) {
                    Log.log(th);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull LoadingError loadingError) {
        if (this.b != null && loadingError == LoadingError.TimeoutError) {
            this.b.trackError(1005);
        }
        if (this.i != null) {
            this.i.onError(loadingError);
        }
    }

    public void a(t tVar) {
        this.f1637a.a(tVar);
    }

    public void a(String str) {
        this.f1637a.a(str);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting(otherwise = 3)
    public void a(JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("target_placements");
        this.h.clear();
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                this.h.add(optJSONArray.optString(i2));
            }
        }
    }

    public void a(boolean z) {
        this.f1637a.a(z);
    }

    @NonNull
    public AdNetwork b() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public abstract UnifiedAdParamsType b(int i2);

    @NonNull
    public bo c() {
        return this.f1637a;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String d() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        ab.a((Object) this);
    }

    /* access modifiers changed from: 0000 */
    public boolean f() {
        if (!b().worksInM()) {
            if (!br.a("org.apache.http.HttpResponse")) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject g() {
        return this.l;
    }

    public double getEcpm() {
        return this.f1637a.getEcpm();
    }

    public long getExpTime() {
        return this.f1637a.getExpTime();
    }

    public String getId() {
        return this.f1637a.getId();
    }

    public JSONObject getJsonData() {
        return this.f1637a.getJsonData();
    }

    public int getLoadingTimeout() {
        int loadingTimeout = this.f1637a.getLoadingTimeout();
        return loadingTimeout > 0 ? loadingTimeout : this.n;
    }

    @Nullable
    public String getMediatorName() {
        return this.f1637a.getMediatorName();
    }

    @Nullable
    public t getRequestResult() {
        return this.f1637a.getRequestResult();
    }

    public String getStatus() {
        return this.f1637a.getStatus();
    }

    /* access modifiers changed from: 0000 */
    public boolean h() {
        return !this.h.isEmpty();
    }

    /* access modifiers changed from: 0000 */
    public List<String> i() {
        return this.h;
    }

    public boolean isAsync() {
        return this.f1637a.isAsync();
    }

    public boolean isPrecache() {
        return this.f1637a.isPrecache();
    }

    /* access modifiers changed from: 0000 */
    public void j() {
        if (this.b != null) {
            this.b.trackFill();
        }
        if (this.i != null) {
            this.i.onLoaded();
        }
    }

    /* access modifiers changed from: 0000 */
    public void k() {
        if (this.i != null) {
            this.i.onShow();
        }
    }

    /* access modifiers changed from: 0000 */
    public void l() {
        if (this.i != null) {
            this.i.onHide();
        }
    }

    /* access modifiers changed from: 0000 */
    public void m() {
        if (this.b != null) {
            this.b.trackFinish();
        }
        if (this.i != null) {
            this.i.onFinished();
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public UnifiedAdType n() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public abstract UnifiedAdCallbackType o();

    /* access modifiers changed from: 0000 */
    @Nullable
    public UnifiedAdCallbackType p() {
        return this.k;
    }

    public final void q() {
        br.a((Runnable) new Runnable() {
            public void run() {
                i.this.r();
            }
        });
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void r() {
        UnifiedAd n2 = n();
        if (n2 != null) {
            n2.onDestroy();
        }
    }

    /* access modifiers changed from: protected */
    public LoadingError s() {
        return null;
    }

    @NonNull
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append("[@");
        sb.append(Integer.toHexString(hashCode()));
        sb.append("]: ");
        sb.append(getId());
        return sb.toString();
    }
}
