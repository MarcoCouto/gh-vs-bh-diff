package com.appodeal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;

public class NativeIconView extends FrameLayout {
    public NativeIconView(@NonNull Context context) {
        super(context);
    }

    public NativeIconView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeIconView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @TargetApi(21)
    public NativeIconView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        double d = (double) size;
        double d2 = (double) (((float) size2) * 1.0f);
        Double.isNaN(d2);
        double d3 = d2 + 0.5d;
        if (d > d3) {
            size = (int) d3;
        } else {
            double d4 = (double) (((float) size) / 1.0f);
            Double.isNaN(d4);
            size2 = (int) (d4 + 0.5d);
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2, 1073741824));
    }
}
