package com.appodeal.ads.adapters.vungle.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.vungle.VungleNetwork.RequestParams;
import com.appodeal.ads.adapters.vungle.VungleUnifiedListener;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.vungle.warren.AdConfig;
import com.vungle.warren.Vungle;

public class VungleRewarded extends UnifiedRewarded<RequestParams> {
    private VungleUnifiedListener<UnifiedRewardedCallback> listener;
    private String placementId;

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.placementId = requestParams.placementId;
        this.listener = new VungleUnifiedListener<>(unifiedRewardedCallback, this.placementId);
        if (Vungle.canPlayAd(this.placementId)) {
            unifiedRewardedCallback.onAdLoaded();
        } else {
            Vungle.loadAd(this.placementId, this.listener);
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (Vungle.canPlayAd(this.placementId)) {
            Vungle.playAd(this.placementId, new AdConfig(), this.listener);
        } else {
            unifiedRewardedCallback.onAdShowFailed();
        }
    }
}
