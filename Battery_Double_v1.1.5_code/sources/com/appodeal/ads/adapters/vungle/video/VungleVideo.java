package com.appodeal.ads.adapters.vungle.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.vungle.VungleNetwork.RequestParams;
import com.appodeal.ads.adapters.vungle.VungleUnifiedListener;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.vungle.warren.AdConfig;
import com.vungle.warren.Vungle;

public class VungleVideo extends UnifiedVideo<RequestParams> {
    private VungleUnifiedListener<UnifiedVideoCallback> listener;
    private String placementId;

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.placementId = requestParams.placementId;
        this.listener = new VungleUnifiedListener<>(unifiedVideoCallback, this.placementId);
        if (Vungle.canPlayAd(this.placementId)) {
            unifiedVideoCallback.onAdLoaded();
        } else {
            Vungle.loadAd(this.placementId, this.listener);
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (Vungle.canPlayAd(this.placementId)) {
            Vungle.playAd(this.placementId, new AdConfig(), this.listener);
        } else {
            unifiedVideoCallback.onAdShowFailed();
        }
    }
}
