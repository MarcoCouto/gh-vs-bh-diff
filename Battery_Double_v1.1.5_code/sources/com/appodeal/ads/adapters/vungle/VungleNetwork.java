package com.appodeal.ads.adapters.vungle;

import android.app.Activity;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.vungle.rewarded_video.VungleRewarded;
import com.appodeal.ads.adapters.vungle.video.VungleVideo;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.vungle.warren.BuildConfig;
import com.vungle.warren.InitCallback;
import com.vungle.warren.Vungle;
import com.vungle.warren.Vungle.Consent;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;

public class VungleNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final String placementId;

        RequestParams(String str) {
            this.placementId = str;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "vungle";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.vungle.warren.ui.VungleActivity").build(), new Builder("com.vungle.warren.ui.VungleWebViewActivity").build(), new Builder("com.vungle.warren.ui.VungleFlexViewActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.vungle.warren.Vungle", "com.moat.analytics.mobile.vng.MoatOptions", "okhttp3.logging.HttpLoggingInterceptor", "okhttp3.HttpUrl", "okio.Okio", "retrofit2.Retrofit", "retrofit2.converter.gson.GsonConverterFactory"};
        }

        public VungleNetwork build() {
            return new VungleNetwork(this);
        }
    }

    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public VungleNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new VungleRewarded();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new VungleVideo();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull final NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        final String string = adUnit.getJsonData().getString(AdvertisementColumns.COLUMN_PLACEMENT_ID);
        updateConsent(adNetworkMediationParams.getRestrictedData());
        if (!Vungle.isInitialized()) {
            Vungle.init(adUnit.getJsonData().getString("app_id"), activity.getApplicationContext(), new InitCallback() {
                public void onAutoCacheAdAvailable(String str) {
                }

                public void onSuccess() {
                    try {
                        networkInitializationListener.onInitializationFinished(new RequestParams(string));
                    } catch (Exception unused) {
                        networkInitializationListener.onInitializationFailed(LoadingError.IncorrectAdunit);
                    }
                }

                public void onError(Throwable th) {
                    networkInitializationListener.onInitializationFailed(LoadingError.NoFill);
                }
            });
        } else {
            networkInitializationListener.onInitializationFinished(new RequestParams(string));
        }
    }

    @VisibleForTesting
    public void updateConsent(RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            Vungle.updateConsentStatus(restrictedData.isUserHasConsent() ? Consent.OPTED_IN : Consent.OPTED_OUT, "");
        }
    }

    public boolean isPermissionRequired(@NonNull String str, AdType adType) {
        return "android.permission.WRITE_EXTERNAL_STORAGE".equals(str) && VERSION.SDK_INT <= 18;
    }
}
