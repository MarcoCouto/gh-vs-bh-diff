package com.appodeal.ads.adapters.vungle;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.vungle.warren.LoadAdCallback;
import com.vungle.warren.PlayAdCallback;
import com.vungle.warren.Vungle;
import com.vungle.warren.error.VungleException;

public class VungleUnifiedListener<UnifiedAdCallbackType extends UnifiedFullscreenAdCallback> implements LoadAdCallback, PlayAdCallback {
    private final UnifiedAdCallbackType callback;
    private final String placementId;
    @VisibleForTesting
    public boolean wasLoaded;

    public VungleUnifiedListener(UnifiedAdCallbackType unifiedadcallbacktype, String str) {
        this.callback = unifiedadcallbacktype;
        this.placementId = str;
    }

    public final void onAdLoad(@NonNull String str) {
        if (!TextUtils.equals(str, this.placementId) || !Vungle.canPlayAd(this.placementId)) {
            this.callback.printError(String.format("Placement can't be played (Vungle.canPlayAd(%s) is false).", new Object[]{str}), null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
            return;
        }
        this.callback.onAdLoaded();
        this.wasLoaded = true;
    }

    public final void onError(@NonNull String str, Throwable th) {
        VungleException vungleException = th instanceof VungleException ? (VungleException) th : null;
        if (this.wasLoaded) {
            if (vungleException != null) {
                if (vungleException.getExceptionCode() == 4) {
                    this.callback.onAdExpired();
                    return;
                }
                this.callback.printError(vungleException.getLocalizedMessage(), Integer.valueOf(vungleException.getExceptionCode()));
            }
            this.callback.onAdLoadFailed(LoadingError.InternalError);
        } else {
            if (vungleException != null) {
                this.callback.printError(vungleException.getLocalizedMessage(), Integer.valueOf(vungleException.getExceptionCode()));
            } else if (th != null) {
                this.callback.printError(null, th.getLocalizedMessage());
            }
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }
    }

    public final void onAdStart(@NonNull String str) {
        this.callback.onAdShown();
    }

    public final void onAdEnd(@NonNull String str, boolean z, boolean z2) {
        if (z) {
            this.callback.onAdFinished();
        }
        if (z2) {
            this.callback.onAdClicked();
        }
        this.callback.onAdClosed();
    }
}
