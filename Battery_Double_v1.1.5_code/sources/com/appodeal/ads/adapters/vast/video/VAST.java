package com.appodeal.ads.adapters.vast.video;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.vast.VASTNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.appodeal.ads.unified.vast.UnifiedVastNetworkParams;
import com.appodeal.ads.unified.vast.UnifiedVastVideo;

public class VAST extends UnifiedVastVideo<RequestParams> {
    @Nullable
    public UnifiedVastNetworkParams obtainVastParams(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        return requestParams;
    }

    public void performVastRequest(@NonNull Context context, @NonNull final UnifiedVideoParams unifiedVideoParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull final UnifiedVideoCallback unifiedVideoCallback, @NonNull String str) {
        S2SAdTask.requestVast(context, str, unifiedVastNetworkParams, unifiedVideoCallback, new Callback<UnifiedVastNetworkParams>() {
            public void onSuccess(@NonNull Context context, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams) {
                VAST.this.loadVast(context, unifiedVideoParams, unifiedVastNetworkParams, unifiedVideoCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedVideoCallback.onAdLoadFailed(loadingError);
            }
        });
    }
}
