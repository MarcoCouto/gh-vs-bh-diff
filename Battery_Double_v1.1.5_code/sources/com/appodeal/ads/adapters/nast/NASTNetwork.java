package com.appodeal.ads.adapters.nast;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.nast.native_ad.NAST;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedNative;
import com.explorestack.iab.BuildConfig;
import com.tapjoy.TJAdUnitConstants.String;

public class NASTNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final RestrictedData restrictedData;
        public final String url;

        private RequestParams(String str, RestrictedData restrictedData2) {
            this.url = str;
            this.restrictedData = restrictedData2;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "nast";
        }

        public NASTNetwork build() {
            return new NASTNetwork(this);
        }
    }

    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public NASTNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new NAST();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        String string = adUnit.getJsonData().getString("url");
        if (adUnit.getJsonData().optBoolean(String.TOP)) {
            string = UnifiedAdUtils.parseUrlWithTopParams(activity, string, adNetworkMediationParams);
        }
        networkInitializationListener.onInitializationFinished(new RequestParams(string, adNetworkMediationParams.getRestrictedData()));
    }
}
