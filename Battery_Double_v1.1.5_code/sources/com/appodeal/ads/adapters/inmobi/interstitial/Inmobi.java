package com.appodeal.ads.adapters.inmobi.interstitial;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inmobi.InmobiNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiInterstitial;
import com.inmobi.ads.listeners.InterstitialAdEventListener;
import java.util.Map;

public class Inmobi extends UnifiedInterstitial<RequestParams> {
    @VisibleForTesting
    InMobiInterstitial inMobiInterstitial;

    @VisibleForTesting
    static final class Listener extends InterstitialAdEventListener {
        private final UnifiedInterstitialCallback callback;

        Listener(@NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
            this.callback = unifiedInterstitialCallback;
        }

        public void onAdLoadSucceeded(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdLoaded();
        }

        public void onAdLoadFailed(InMobiInterstitial inMobiInterstitial, InMobiAdRequestStatus inMobiAdRequestStatus) {
            if (inMobiAdRequestStatus != null) {
                this.callback.printError(inMobiAdRequestStatus.getMessage(), inMobiAdRequestStatus.getStatusCode());
            }
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdDisplayed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdShown();
        }

        public void onAdDisplayFailed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdShowFailed();
        }

        public void onAdClicked(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map) {
            this.callback.onAdClicked();
        }

        public void onAdDismissed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdClosed();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.inMobiInterstitial = new InMobiInterstitial((Context) activity, requestParams.placement, (InterstitialAdEventListener) new Listener(unifiedInterstitialCallback));
        this.inMobiInterstitial.setExtras(RequestParams.extras);
        this.inMobiInterstitial.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.inMobiInterstitial == null || !this.inMobiInterstitial.isReady()) {
            unifiedInterstitialCallback.onAdShowFailed();
        } else {
            this.inMobiInterstitial.show();
        }
    }

    public void onDestroy() {
        if (this.inMobiInterstitial != null) {
            this.inMobiInterstitial = null;
        }
    }
}
