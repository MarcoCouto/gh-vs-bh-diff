package com.appodeal.ads.adapters.inmobi.rewarded_video;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inmobi.InmobiNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiInterstitial;
import com.inmobi.ads.listeners.InterstitialAdEventListener;
import java.util.Map;

public class Inmobi extends UnifiedRewarded<RequestParams> {
    @VisibleForTesting
    InMobiInterstitial inMobiInterstitial;

    @VisibleForTesting
    static final class Listener extends InterstitialAdEventListener {
        private final UnifiedRewardedCallback callback;

        Listener(@NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
            this.callback = unifiedRewardedCallback;
        }

        public void onAdLoadSucceeded(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdLoaded();
        }

        public void onAdLoadFailed(InMobiInterstitial inMobiInterstitial, InMobiAdRequestStatus inMobiAdRequestStatus) {
            if (inMobiAdRequestStatus != null) {
                this.callback.printError(inMobiAdRequestStatus.getMessage(), inMobiAdRequestStatus.getStatusCode());
            }
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdDisplayed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdShown();
        }

        public void onAdDisplayFailed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdShowFailed();
        }

        public void onAdClicked(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map) {
            this.callback.onAdClicked();
        }

        public void onRewardsUnlocked(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map) {
            this.callback.onAdFinished();
        }

        public void onAdDismissed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdClosed();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.inMobiInterstitial = new InMobiInterstitial((Context) activity, requestParams.placement, (InterstitialAdEventListener) new Listener(unifiedRewardedCallback));
        this.inMobiInterstitial.setExtras(RequestParams.extras);
        this.inMobiInterstitial.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.inMobiInterstitial == null || !this.inMobiInterstitial.isReady()) {
            unifiedRewardedCallback.onAdShowFailed();
        } else {
            this.inMobiInterstitial.show();
        }
    }

    public void onDestroy() {
        if (this.inMobiInterstitial != null) {
            this.inMobiInterstitial = null;
        }
    }
}
