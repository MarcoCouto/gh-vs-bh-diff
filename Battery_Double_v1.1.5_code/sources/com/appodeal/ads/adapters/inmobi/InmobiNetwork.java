package com.appodeal.ads.adapters.inmobi;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.inmobi.banner.Inmobi;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.DependencyRule;
import com.appodeal.ads.utils.Log;
import com.inmobi.sdk.InMobiSdk;
import com.inmobi.sdk.InMobiSdk.LogLevel;
import com.integralads.avid.library.inmobi.BuildConfig;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class InmobiNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public static final Map<String, String> extras = new HashMap<String, String>() {
            {
                put("tp", "c_appodeal");
                put("tp-ver", Appodeal.getVersion());
            }
        };
        public final long placement;

        RequestParams(long j) {
            this.placement = j;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return BuildConfig.SDK_NAME;
        }

        public String[] getRequiredPermissions() {
            return new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"};
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.inmobi.rendering.InMobiAdActivity").build()};
        }

        public String[] getRequiredReceiverClassName() {
            return new String[0];
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.inmobi.ads.InMobiBanner", "com.inmobi.ads.InMobiInterstitial", "com.squareup.picasso.Picasso"};
        }

        public DependencyRule[] getOptionalClasses() {
            return new DependencyRule[]{new DependencyRule("android.support.v7.widget.RecyclerView", "Required for Fullscreen ads")};
        }

        public InmobiNetwork build() {
            return new InmobiNetwork(this);
        }
    }

    public boolean canLoadInterstitialWhenDisplaying() {
        return false;
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return false;
    }

    public boolean canLoadVideoWhenDisplaying() {
        return false;
    }

    public InmobiNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new Inmobi();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new com.appodeal.ads.adapters.inmobi.mrec.Inmobi();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new com.appodeal.ads.adapters.inmobi.interstitial.Inmobi();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new com.appodeal.ads.adapters.inmobi.video.Inmobi();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new com.appodeal.ads.adapters.inmobi.rewarded_video.Inmobi();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new com.appodeal.ads.adapters.inmobi.native_ad.Inmobi();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        if (VERSION.SDK_INT < 15) {
            networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
            return;
        }
        String string = adUnit.getJsonData().getString("acc_id");
        long j = adUnit.getJsonData().getLong(AdvertisementColumns.COLUMN_PLACEMENT_ID);
        InMobiSdk.init(activity, string);
        RestrictedData restrictedData = adNetworkMediationParams.getRestrictedData();
        updateConsent(restrictedData);
        setTargeting(activity, restrictedData);
        networkInitializationListener.onInitializationFinished(new RequestParams(j));
    }

    public void setLogging(boolean z) {
        if (z) {
            InMobiSdk.setLogLevel(LogLevel.DEBUG);
        } else {
            InMobiSdk.setLogLevel(LogLevel.NONE);
        }
    }

    public String getVersion() {
        return InMobiSdk.getVersion();
    }

    public LoadingError verifyLoadAvailability(@NonNull AdType adType) {
        if ((adType == AdType.Interstitial || adType == AdType.Video || adType == AdType.Rewarded) && (isInterstitialShowing() || isVideoShowing() || isRewardedShowing())) {
            return LoadingError.Canceled;
        }
        return super.verifyLoadAvailability(adType);
    }

    @VisibleForTesting
    public void updateConsent(RestrictedData restrictedData) {
        try {
            if (restrictedData.isUserInGdprScope()) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(InMobiSdk.IM_GDPR_CONSENT_AVAILABLE, restrictedData.isUserHasConsent());
                jSONObject.put("gdpr", restrictedData.isUserInGdprScope() ? "1" : "0");
                InMobiSdk.updateGDPRConsent(jSONObject);
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    private void setTargeting(Context context, RestrictedData restrictedData) {
        Location obtainLocation = restrictedData.getLocation(context).obtainLocation();
        if (obtainLocation != null) {
            InMobiSdk.setLocation(obtainLocation);
        }
        Integer age = restrictedData.getAge();
        if (age != null) {
            InMobiSdk.setAge(age.intValue());
        }
        Gender gender = restrictedData.getGender();
        if (gender != null) {
            switch (gender) {
                case MALE:
                    InMobiSdk.setGender(InMobiSdk.Gender.MALE);
                    return;
                case FEMALE:
                    InMobiSdk.setGender(InMobiSdk.Gender.FEMALE);
                    return;
                default:
                    return;
            }
        }
    }
}
