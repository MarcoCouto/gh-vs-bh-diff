package com.appodeal.ads.adapters.inmobi.mrec;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.ViewGroup.LayoutParams;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inmobi.InmobiNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiBanner;
import com.inmobi.ads.listeners.BannerAdEventListener;
import java.util.Map;

public class Inmobi extends UnifiedMrec<RequestParams> {
    @VisibleForTesting
    InMobiBanner inMobiBanner;

    @VisibleForTesting
    static final class Listener extends BannerAdEventListener {
        private final UnifiedMrecCallback callback;

        Listener(@NonNull UnifiedMrecCallback unifiedMrecCallback) {
            this.callback = unifiedMrecCallback;
        }

        public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
            if (inMobiBanner == null || inMobiBanner.getChildCount() == 0) {
                this.callback.onAdLoadFailed(LoadingError.InvalidAssets);
            } else {
                this.callback.onAdLoaded(inMobiBanner);
            }
        }

        public void onAdLoadFailed(InMobiBanner inMobiBanner, InMobiAdRequestStatus inMobiAdRequestStatus) {
            if (inMobiAdRequestStatus != null) {
                this.callback.printError(inMobiAdRequestStatus.getMessage(), inMobiAdRequestStatus.getStatusCode());
            }
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdClicked(InMobiBanner inMobiBanner, Map<Object, Object> map) {
            this.callback.onAdClicked();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        this.inMobiBanner = new InMobiBanner((Context) new ContextWrapper(activity), requestParams.placement);
        this.inMobiBanner.setLayoutParams(new LayoutParams(Math.round(UnifiedAdUtils.getScreenDensity(activity) * 300.0f), Math.round(UnifiedAdUtils.getScreenDensity(activity) * 250.0f)));
        this.inMobiBanner.setEnableAutoRefresh(false);
        this.inMobiBanner.setListener((BannerAdEventListener) new Listener(unifiedMrecCallback));
        this.inMobiBanner.setExtras(RequestParams.extras);
        this.inMobiBanner.load();
    }

    public void onDestroy() {
        if (this.inMobiBanner != null) {
            this.inMobiBanner = null;
        }
    }
}
