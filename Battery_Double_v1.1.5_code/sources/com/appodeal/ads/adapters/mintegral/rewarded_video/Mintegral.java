package com.appodeal.ads.adapters.mintegral.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.mintegral.MintegralNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.mintegral.msdk.out.MTGRewardVideoHandler;
import com.mintegral.msdk.out.RewardVideoListener;

public class Mintegral extends UnifiedRewarded<RequestParams> {
    @VisibleForTesting
    String rewardId;
    @VisibleForTesting
    MTGRewardVideoHandler rewardVideoHandler;

    @VisibleForTesting
    static final class Listener implements RewardVideoListener {
        private final UnifiedRewardedCallback callback;

        public void onEndcardShow(String str) {
        }

        public void onLoadSuccess(String str) {
        }

        public void onVideoComplete(String str) {
        }

        Listener(@NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
            this.callback = unifiedRewardedCallback;
        }

        public void onVideoLoadSuccess(String str) {
            this.callback.onAdLoaded();
        }

        public void onVideoLoadFail(String str) {
            this.callback.printError(str, null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdShow() {
            this.callback.onAdShown();
        }

        public void onShowFail(String str) {
            this.callback.printError(str, null);
            this.callback.onAdShowFailed();
        }

        public void onVideoAdClicked(String str) {
            this.callback.onAdClicked();
        }

        public void onAdClose(boolean z, String str, float f) {
            if (z) {
                this.callback.onAdFinished();
            }
            this.callback.onAdClosed();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.rewardId = requestParams.rewardId;
        this.rewardVideoHandler = new MTGRewardVideoHandler(activity, requestParams.unitId);
        this.rewardVideoHandler.setRewardVideoListener(new Listener(unifiedRewardedCallback));
        this.rewardVideoHandler.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.rewardVideoHandler == null || !this.rewardVideoHandler.isReady()) {
            unifiedRewardedCallback.onAdShowFailed();
        } else {
            this.rewardVideoHandler.show(this.rewardId);
        }
    }

    public void onDestroy() {
        if (this.rewardVideoHandler != null) {
            this.rewardVideoHandler = null;
        }
        this.rewardId = null;
    }
}
