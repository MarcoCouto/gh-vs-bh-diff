package com.appodeal.ads.adapters.mintegral.native_ad;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.NativeMediaView;
import com.appodeal.ads.adapters.mintegral.MintegralNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.utils.Log;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.nativex.view.MTGMediaView;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.MIntegralSDKFactory;
import com.mintegral.msdk.out.MtgNativeHandler;
import com.mintegral.msdk.out.NativeListener.NativeAdListener;
import com.mintegral.msdk.out.OnMTGMediaViewListener;
import java.util.List;
import java.util.Map;

public class Mintegral extends UnifiedNative<RequestParams> {
    private MtgNativeHandler nativeHandle;

    @VisibleForTesting
    static final class Listener implements NativeAdListener {
        private final UnifiedNativeCallback callback;
        private final MtgNativeHandler nativeHandle;
        private final UnifiedNativeParams params;

        public void onAdFramesLoaded(List<Frame> list) {
        }

        public void onLoggingImpression(int i) {
        }

        Listener(@NonNull UnifiedNativeParams unifiedNativeParams, @NonNull UnifiedNativeCallback unifiedNativeCallback, MtgNativeHandler mtgNativeHandler) {
            this.params = unifiedNativeParams;
            this.callback = unifiedNativeCallback;
            this.nativeHandle = mtgNativeHandler;
        }

        public void onAdLoaded(List<Campaign> list, int i) {
            for (Campaign nativeAdapter : list) {
                this.callback.onAdLoaded(new NativeAdapter(this.params, this.callback, this.nativeHandle, nativeAdapter));
            }
        }

        public void onAdLoadError(String str) {
            this.callback.printError(str, null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdClick(Campaign campaign) {
            this.callback.onAdClicked(campaign.hashCode(), (UnifiedAdCallbackClickTrackListener) null);
        }
    }

    private static class NativeAdapter extends UnifiedNativeAd {
        private View adView;
        /* access modifiers changed from: private */
        public final UnifiedNativeCallback callback;
        private final Campaign campaign;
        private final MtgNativeHandler nativeHandle;
        private final UnifiedNativeParams params;

        NativeAdapter(@NonNull UnifiedNativeParams unifiedNativeParams, @NonNull UnifiedNativeCallback unifiedNativeCallback, MtgNativeHandler mtgNativeHandler, Campaign campaign2) {
            super(campaign2.getAppName(), campaign2.getAppDesc(), campaign2.getAdCall(), campaign2.getImageUrl(), campaign2.getIconUrl(), campaign2.getRating() != Utils.DOUBLE_EPSILON ? Float.valueOf((float) campaign2.getRating()) : null);
            this.params = unifiedNativeParams;
            this.callback = unifiedNativeCallback;
            this.nativeHandle = mtgNativeHandler;
            this.campaign = campaign2;
        }

        public int getAdId() {
            return this.campaign.hashCode();
        }

        public boolean hasVideo() {
            try {
                if (!(this.campaign instanceof CampaignEx)) {
                    return false;
                }
                return !TextUtils.isEmpty(((CampaignEx) this.campaign).getVideoUrlEncode());
            } catch (Exception e) {
                Log.log(e);
                return false;
            }
        }

        public boolean containsVideo() {
            return hasVideo();
        }

        public boolean onConfigureMediaView(@NonNull NativeMediaView nativeMediaView) {
            if (this.params.getNativeAdType() == NativeAdType.NoVideo || !hasVideo()) {
                return super.onConfigureMediaView(nativeMediaView);
            }
            nativeMediaView.removeAllViews();
            MTGMediaView mTGMediaView = new MTGMediaView(nativeMediaView.getContext());
            mTGMediaView.setIsAllowFullScreen(true);
            mTGMediaView.setNativeAd(this.campaign);
            mTGMediaView.setOnMediaViewListener(new OnMTGMediaViewListener() {
                public void onEnterFullscreen() {
                }

                public void onExitFullscreen() {
                }

                public void onRedirectionFailed(Campaign campaign, String str) {
                }

                public void onStartRedirection(Campaign campaign, String str) {
                }

                public void onVideoAdClicked(Campaign campaign) {
                }

                public void onVideoStart() {
                }

                public void onFinishRedirection(Campaign campaign, String str) {
                    NativeAdapter.this.callback.onAdClicked(campaign.hashCode(), (UnifiedAdCallbackClickTrackListener) null);
                }
            });
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.addRule(13, -1);
            nativeMediaView.addView(mTGMediaView, layoutParams);
            return true;
        }

        public void onRegisterForInteraction(@NonNull NativeAdView nativeAdView) {
            super.onRegisterForInteraction(nativeAdView);
            this.adView = nativeAdView;
            this.nativeHandle.registerView(nativeAdView, this.campaign);
        }

        public void onUnregisterForInteraction() {
            super.onUnregisterForInteraction();
            this.nativeHandle.unregisterView(this.adView, this.campaign);
        }

        public void onDestroy() {
            this.adView = null;
            super.onDestroy();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        Map nativeProperties = MtgNativeHandler.getNativeProperties(requestParams.unitId);
        nativeProperties.put("ad_num", Integer.valueOf(unifiedNativeParams.getAdCountToLoad()));
        if (unifiedNativeParams.getNativeAdType() != NativeAdType.NoVideo) {
            nativeProperties.put(MIntegralConstans.NATIVE_VIDEO_WIDTH, Integer.valueOf(IronSourceConstants.RV_INSTANCE_LOAD_FAILED));
            nativeProperties.put(MIntegralConstans.NATIVE_VIDEO_HEIGHT, Integer.valueOf(627));
            nativeProperties.put(MIntegralConstans.NATIVE_VIDEO_SUPPORT, Boolean.valueOf(true));
        } else {
            nativeProperties.put(MIntegralConstans.NATIVE_VIDEO_SUPPORT, Boolean.valueOf(false));
        }
        MIntegralSDKFactory.getMIntegralSDK().preload(nativeProperties);
        this.nativeHandle = new MtgNativeHandler(nativeProperties, activity);
        this.nativeHandle.setAdListener(new Listener(unifiedNativeParams, unifiedNativeCallback, this.nativeHandle));
        this.nativeHandle.load();
    }

    public void onDestroy() {
        if (this.nativeHandle != null) {
            this.nativeHandle = null;
        }
    }
}
