package com.appodeal.ads.adapters.mintegral.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.mintegral.MintegralNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.out.InterstitialListener;
import com.mintegral.msdk.out.MTGInterstitialHandler;
import java.util.HashMap;

public class Mintegral extends UnifiedInterstitial<RequestParams> {
    @VisibleForTesting
    MTGInterstitialHandler interstitialHandler;

    @VisibleForTesting
    static final class Listener implements InterstitialListener {
        private final UnifiedInterstitialCallback callback;

        Listener(@NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
            this.callback = unifiedInterstitialCallback;
        }

        public void onInterstitialLoadSuccess() {
            this.callback.onAdLoaded();
        }

        public void onInterstitialLoadFail(String str) {
            this.callback.printError(str, null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onInterstitialShowSuccess() {
            this.callback.onAdShown();
        }

        public void onInterstitialShowFail(String str) {
            this.callback.printError(str, null);
            this.callback.onAdShowFailed();
        }

        public void onInterstitialAdClick() {
            this.callback.onAdClicked();
        }

        public void onInterstitialClosed() {
            this.callback.onAdClosed();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        HashMap hashMap = new HashMap();
        hashMap.put(MIntegralConstans.PROPERTIES_UNIT_ID, requestParams.unitId);
        this.interstitialHandler = new MTGInterstitialHandler(activity, hashMap);
        this.interstitialHandler.setInterstitialListener(new Listener(unifiedInterstitialCallback));
        this.interstitialHandler.preload();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.interstitialHandler != null) {
            this.interstitialHandler.show();
        } else {
            unifiedInterstitialCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        if (this.interstitialHandler != null) {
            this.interstitialHandler = null;
        }
    }
}
