package com.appodeal.ads.adapters.mintegral.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.mintegral.MintegralNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.mintegral.msdk.interstitialvideo.out.InterstitialVideoListener;
import com.mintegral.msdk.interstitialvideo.out.MTGInterstitialVideoHandler;

public class Mintegral extends UnifiedVideo<RequestParams> {
    @VisibleForTesting
    MTGInterstitialVideoHandler interstitialVideoHandler;

    @VisibleForTesting
    static final class Listener implements InterstitialVideoListener {
        private final UnifiedVideoCallback callback;

        public void onEndcardShow(String str) {
        }

        public void onLoadSuccess(String str) {
        }

        public void onVideoComplete(String str) {
        }

        Listener(@NonNull UnifiedVideoCallback unifiedVideoCallback) {
            this.callback = unifiedVideoCallback;
        }

        public void onVideoLoadSuccess(String str) {
            this.callback.onAdLoaded();
        }

        public void onVideoLoadFail(String str) {
            this.callback.printError(str, null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdShow() {
            this.callback.onAdShown();
        }

        public void onShowFail(String str) {
            this.callback.printError(str, null);
            this.callback.onAdShowFailed();
        }

        public void onVideoAdClicked(String str) {
            this.callback.onAdClicked();
        }

        public void onAdClose(boolean z) {
            if (z) {
                this.callback.onAdFinished();
            }
            this.callback.onAdClosed();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.interstitialVideoHandler = new MTGInterstitialVideoHandler(activity, requestParams.unitId);
        this.interstitialVideoHandler.setInterstitialVideoListener(new Listener(unifiedVideoCallback));
        this.interstitialVideoHandler.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.interstitialVideoHandler == null || !this.interstitialVideoHandler.isReady()) {
            unifiedVideoCallback.onAdShowFailed();
        } else {
            this.interstitialVideoHandler.show();
        }
    }

    public void onDestroy() {
        if (this.interstitialVideoHandler != null) {
            this.interstitialVideoHandler = null;
        }
    }
}
