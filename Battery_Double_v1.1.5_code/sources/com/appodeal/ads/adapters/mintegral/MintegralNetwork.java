package com.appodeal.ads.adapters.mintegral;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.mintegral.interstitial.Mintegral;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.MIntegralUser;
import com.mintegral.msdk.out.MIntegralSDKFactory;
import com.mintegral.msdk.out.MTGConfiguration;
import com.mintegral.msdk.system.a;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.List;

public class MintegralNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final String rewardId;
        public final String unitId;

        RequestParams(String str, String str2) {
            this.unitId = str;
            this.rewardId = str2;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "mintegral";
        }

        public String[] getRequiredPermissions() {
            return new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"};
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.mintegral.msdk.activity.MTGCommonActivity").build(), new Builder("com.mintegral.msdk.reward.player.MTGRewardVideoActivity").build(), new Builder("com.mintegral.msdk.interstitial.view.MTGInterstitialActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.mintegral.msdk.out.MTGInterstitialHandler", "com.mintegral.msdk.interstitialvideo.out.MTGInterstitialVideoHandler", "com.mintegral.msdk.out.MtgNativeHandler", "com.mintegral.msdk.out.MTGRewardVideoHandler", "org.apache.http.HttpResponse"};
        }

        public List<Pair<String, Pair<String, String>>> getRequiredServiceWithData() {
            return new ArrayList<Pair<String, Pair<String, String>>>() {
                {
                    add(new Pair("com.mintegral.msdk.shell.MTGService", null));
                }
            };
        }

        public String[] getRequiredReceiverClassName() {
            return new String[]{"com.mintegral.msdk.click.AppReceiver"};
        }

        public AdNetwork build() {
            return new MintegralNetwork(this);
        }
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return false;
    }

    public boolean canLoadVideoWhenDisplaying() {
        return false;
    }

    public String getVersion() {
        return MTGConfiguration.SDK_VERSION;
    }

    public MintegralNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new Mintegral();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new com.appodeal.ads.adapters.mintegral.video.Mintegral();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new com.appodeal.ads.adapters.mintegral.rewarded_video.Mintegral();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new com.appodeal.ads.adapters.mintegral.native_ad.Mintegral();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        String string = adUnit.getJsonData().getString("app_id");
        String string2 = adUnit.getJsonData().getString(TapjoyConstants.TJC_API_KEY);
        String string3 = adUnit.getJsonData().getString(MIntegralConstans.PROPERTIES_UNIT_ID);
        String optString = adUnit.getJsonData().optString("reward_id", "1");
        MIntegralConstans.INIT_UA_IN = false;
        RestrictedData restrictedData = adNetworkMediationParams.getRestrictedData();
        updateConsent(activity, restrictedData);
        a mIntegralSDK = MIntegralSDKFactory.getMIntegralSDK();
        mIntegralSDK.init(mIntegralSDK.getMTGConfigurationMap(string, string2), activity.getApplicationContext());
        setTargeting(activity, restrictedData);
        networkInitializationListener.onInitializationFinished(new RequestParams(string3, optString));
    }

    @VisibleForTesting
    public void updateConsent(Context context, RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            MIntegralSDKFactory.getMIntegralSDK().setUserPrivateInfoType(context, MIntegralConstans.AUTHORITY_ALL_INFO, restrictedData.isUserGdprProtected() ^ true ? 1 : 0);
        }
    }

    private void setTargeting(Context context, RestrictedData restrictedData) {
        MIntegralUser mIntegralUser = new MIntegralUser();
        Gender gender = restrictedData.getGender();
        if (gender != null) {
            switch (gender) {
                case MALE:
                    mIntegralUser.setGender(1);
                    break;
                case FEMALE:
                    mIntegralUser.setGender(2);
                    break;
            }
        }
        Integer age = restrictedData.getAge();
        if (age != null) {
            mIntegralUser.setAge(age.intValue());
        }
        Location obtainLocation = restrictedData.getLocation(context).obtainLocation();
        if (obtainLocation != null) {
            mIntegralUser.setLat(obtainLocation.getLatitude());
            mIntegralUser.setLng(obtainLocation.getLongitude());
        }
        MIntegralSDKFactory.getMIntegralSDK().reportUser(mIntegralUser);
    }
}
