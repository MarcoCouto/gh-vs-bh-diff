package com.appodeal.ads.adapters.mytarget.mrec;

import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.my.target.ads.MyTargetView;
import com.my.target.ads.MyTargetView.MyTargetViewListener;

class MyTargetMrecListener implements MyTargetViewListener {
    private final UnifiedMrecCallback callback;

    public void onShow(@NonNull MyTargetView myTargetView) {
    }

    MyTargetMrecListener(UnifiedMrecCallback unifiedMrecCallback) {
        this.callback = unifiedMrecCallback;
    }

    public void onLoad(@NonNull MyTargetView myTargetView) {
        this.callback.onAdLoaded(myTargetView);
    }

    public void onNoAd(@NonNull String str, @NonNull MyTargetView myTargetView) {
        this.callback.printError(str, null);
        this.callback.onAdLoadFailed(null);
    }

    public void onClick(@NonNull MyTargetView myTargetView) {
        this.callback.onAdClicked();
    }
}
