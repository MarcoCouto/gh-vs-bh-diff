package com.appodeal.ads.adapters.mytarget.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.mytarget.MyTargetNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.my.target.ads.InterstitialAd;

public class MyTargetRewarded extends UnifiedRewarded<RequestParams> {
    private InterstitialAd rewardedAd;

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.rewardedAd = new InterstitialAd(requestParams.myTargetSlot, activity);
        requestParams.applyTargeting(this.rewardedAd.getCustomParams());
        this.rewardedAd.setListener(new MyTargetRewardedListener(unifiedRewardedCallback));
        this.rewardedAd.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        this.rewardedAd.show();
    }

    public void onDestroy() {
        if (this.rewardedAd != null) {
            this.rewardedAd.destroy();
            this.rewardedAd = null;
        }
    }
}
