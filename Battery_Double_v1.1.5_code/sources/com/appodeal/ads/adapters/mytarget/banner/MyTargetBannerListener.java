package com.appodeal.ads.adapters.mytarget.banner;

import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.my.target.ads.MyTargetView;
import com.my.target.ads.MyTargetView.MyTargetViewListener;

class MyTargetBannerListener implements MyTargetViewListener {
    private int bannerHeight;
    private UnifiedBannerCallback callback;

    public void onShow(@NonNull MyTargetView myTargetView) {
    }

    MyTargetBannerListener(UnifiedBannerCallback unifiedBannerCallback, int i) {
        this.callback = unifiedBannerCallback;
        this.bannerHeight = i;
    }

    public void onLoad(@NonNull MyTargetView myTargetView) {
        this.callback.onAdLoaded(myTargetView, -1, this.bannerHeight);
    }

    public void onNoAd(@NonNull String str, @NonNull MyTargetView myTargetView) {
        this.callback.printError(str, null);
        this.callback.onAdLoadFailed(null);
    }

    public void onClick(@NonNull MyTargetView myTargetView) {
        this.callback.onAdClicked();
    }
}
