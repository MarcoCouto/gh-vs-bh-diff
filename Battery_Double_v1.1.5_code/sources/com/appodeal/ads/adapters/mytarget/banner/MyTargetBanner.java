package com.appodeal.ads.adapters.mytarget.banner;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.mytarget.MyTargetNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.my.target.ads.MyTargetView;

public class MyTargetBanner extends UnifiedBanner<RequestParams> {
    private MyTargetView adView;

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        int i;
        int i2;
        if (unifiedBannerParams.needLeaderBoard(activity)) {
            i = 2;
            i2 = 90;
        } else {
            i2 = 50;
            i = 0;
        }
        this.adView = new MyTargetView(activity.getApplicationContext());
        this.adView.init(requestParams.myTargetSlot, i, false);
        requestParams.applyTargeting(this.adView.getCustomParams());
        this.adView.setListener(new MyTargetBannerListener(unifiedBannerCallback, i2));
        this.adView.load();
    }

    public void onDestroy() {
        if (this.adView != null) {
            this.adView.destroy();
            this.adView = null;
        }
    }
}
