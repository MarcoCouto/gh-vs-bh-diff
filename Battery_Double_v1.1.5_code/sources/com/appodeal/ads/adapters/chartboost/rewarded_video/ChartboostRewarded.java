package com.appodeal.ads.adapters.chartboost.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.chartboost.ChartboostListener;
import com.appodeal.ads.adapters.chartboost.ChartboostNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.chartboost.sdk.Chartboost;

public class ChartboostRewarded extends UnifiedRewarded<RequestParams> {
    private static final String RV_LOCATION = "RewardedVideo";
    private UnifiedRewardedCallback callback;

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.callback = unifiedRewardedCallback;
        ChartboostListener.getInstance().setRewardedCallback(unifiedRewardedCallback);
        if (Chartboost.hasRewardedVideo("RewardedVideo")) {
            unifiedRewardedCallback.onAdLoaded();
        } else {
            Chartboost.cacheRewardedVideo("RewardedVideo");
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (Chartboost.hasRewardedVideo("RewardedVideo")) {
            Chartboost.showRewardedVideo("RewardedVideo");
        } else {
            unifiedRewardedCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        ChartboostListener.getInstance().removeRewardedCallback(this.callback);
    }
}
