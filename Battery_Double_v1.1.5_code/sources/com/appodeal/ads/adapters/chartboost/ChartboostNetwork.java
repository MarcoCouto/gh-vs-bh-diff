package com.appodeal.ads.adapters.chartboost;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.chartboost.interstitial.ChartboostInterstitial;
import com.appodeal.ads.adapters.chartboost.rewarded_video.ChartboostRewarded;
import com.appodeal.ads.adapters.chartboost.video.ChartboostVideo;
import com.appodeal.ads.unified.UnifiedAppStateChangeListener;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.app.AppState;
import com.chartboost.sdk.CBImpressionActivity;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Chartboost.CBFramework;
import com.chartboost.sdk.Chartboost.CBPIDataUseConsent;
import com.chartboost.sdk.Libraries.CBLogging.Level;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.tapjoy.TapjoyConstants;

public class ChartboostNetwork extends AdNetwork<RequestParams> {
    private static final UnifiedAppStateChangeListener appStateChangeListener = new UnifiedAppStateChangeListener() {
        public void onAppStateChanged(@Nullable Activity activity, @NonNull AppState appState, boolean z) {
            if (!z && activity != null && !activity.getClass().equals(CBImpressionActivity.class)) {
                switch (AnonymousClass2.$SwitchMap$com$appodeal$ads$utils$app$AppState[appState.ordinal()]) {
                    case 1:
                        Chartboost.onCreate(activity);
                        return;
                    case 2:
                        Chartboost.onStart(activity);
                        return;
                    case 3:
                        Chartboost.onResume(activity);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private static boolean initialized = false;
    private Level loggingLevel = Level.NONE;

    /* renamed from: com.appodeal.ads.adapters.chartboost.ChartboostNetwork$2 reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$appodeal$ads$utils$app$AppState = new int[AppState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(36:0|(2:1|2)|3|(2:5|6)|7|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|(2:33|34)|35|37|38|39|40|(3:41|42|44)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(37:0|(2:1|2)|3|5|6|7|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|(2:33|34)|35|37|38|39|40|(3:41|42|44)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(39:0|(2:1|2)|3|5|6|7|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|(2:33|34)|35|37|38|39|40|41|42|44) */
        /* JADX WARNING: Can't wrap try/catch for region: R(41:0|1|2|3|5|6|7|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|37|38|39|40|41|42|44) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x009e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00aa */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00c9 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00d3 */
        static {
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError = new int[CBImpressionError.values().length];
            try {
                $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.ASSETS_DOWNLOAD_FAILURE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.INTERNAL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.NO_HOST_ACTIVITY.ordinal()] = 3;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.VIDEO_UNAVAILABLE.ordinal()] = 4;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.SESSION_NOT_STARTED.ordinal()] = 5;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.ERROR_CREATING_VIEW.ordinal()] = 6;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.ERROR_DISPLAYING_VIEW.ordinal()] = 7;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.IMPRESSION_ALREADY_VISIBLE.ordinal()] = 8;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.ERROR_PLAYING_VIDEO.ordinal()] = 9;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.WRONG_ORIENTATION.ordinal()] = 10;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.TOO_MANY_CONNECTIONS.ordinal()] = 11;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.FIRST_SESSION_INTERSTITIALS_DISABLED.ordinal()] = 12;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.INVALID_RESPONSE.ordinal()] = 13;
            $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.VIDEO_ID_MISSING.ordinal()] = 14;
            try {
                $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[CBImpressionError.NO_AD_FOUND.ordinal()] = 15;
            } catch (NoSuchFieldError unused3) {
            }
            $SwitchMap$com$appodeal$ads$utils$app$AppState[AppState.Created.ordinal()] = 1;
            $SwitchMap$com$appodeal$ads$utils$app$AppState[AppState.Started.ordinal()] = 2;
            try {
                $SwitchMap$com$appodeal$ads$utils$app$AppState[AppState.Resumed.ordinal()] = 3;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public static final class RequestParams {
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "chartboost";
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.chartboost.sdk.Chartboost"};
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.chartboost.sdk.CBImpressionActivity").build()};
        }

        public ChartboostNetwork build() {
            return new ChartboostNetwork(this);
        }
    }

    public boolean canLoadInterstitialWhenDisplaying() {
        return false;
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return false;
    }

    public boolean canLoadVideoWhenDisplaying() {
        return false;
    }

    public ChartboostNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public UnifiedAppStateChangeListener getAppStateChangeListener() {
        return appStateChangeListener;
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new ChartboostInterstitial();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new ChartboostVideo();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new ChartboostRewarded();
    }

    public String getVersion() {
        return Chartboost.getSDKVersion();
    }

    public void setLogging(boolean z) {
        if (z) {
            this.loggingLevel = Level.ALL;
        } else {
            this.loggingLevel = Level.NONE;
        }
    }

    public LoadingError verifyLoadAvailability(@NonNull AdType adType) {
        if ((adType == AdType.Interstitial || adType == AdType.Video || adType == AdType.Rewarded) && (isInterstitialShowing() || isVideoShowing() || isRewardedShowing())) {
            return LoadingError.Canceled;
        }
        return super.verifyLoadAvailability(adType);
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        updateConsent(activity, adNetworkMediationParams.getRestrictedData());
        if (!isInitialized()) {
            initialized = true;
            Chartboost.startWithAppId(activity, adUnit.getJsonData().getString("chartboost_id"), adUnit.getJsonData().getString("chartboost_signature"));
            Chartboost.setLoggingLevel(this.loggingLevel);
            Chartboost.setAutoCacheAds(false);
            Chartboost.setActivityCallbacks(false);
            Chartboost.onCreate(activity);
            Chartboost.onStart(activity);
            if (!TextUtils.isEmpty(Appodeal.frameworkName) && TapjoyConstants.TJC_PLUGIN_UNITY.equalsIgnoreCase(Appodeal.frameworkName) && !TextUtils.isEmpty(Appodeal.pluginVersion)) {
                Chartboost.setFramework(CBFramework.CBFrameworkUnity, Appodeal.pluginVersion);
            }
            Chartboost.setDelegate(ChartboostListener.getInstance());
        }
        networkInitializationListener.onInitializationFinished(new RequestParams());
    }

    @VisibleForTesting
    public void updateConsent(Context context, RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            Chartboost.setPIDataUseConsent(context, restrictedData.isUserHasConsent() ? CBPIDataUseConsent.YES_BEHAVIORAL : CBPIDataUseConsent.NO_BEHAVIORAL);
        } else {
            Chartboost.setPIDataUseConsent(context, CBPIDataUseConsent.UNKNOWN);
        }
    }

    @VisibleForTesting
    public boolean isInitialized() {
        return initialized;
    }

    @Nullable
    static LoadingError mapError(@Nullable CBImpressionError cBImpressionError) {
        if (cBImpressionError == null) {
            return null;
        }
        switch (cBImpressionError) {
            case ASSETS_DOWNLOAD_FAILURE:
                return LoadingError.ConnectionError;
            case INTERNAL:
            case NO_HOST_ACTIVITY:
            case VIDEO_UNAVAILABLE:
            case SESSION_NOT_STARTED:
            case ERROR_CREATING_VIEW:
            case ERROR_DISPLAYING_VIEW:
            case IMPRESSION_ALREADY_VISIBLE:
                return LoadingError.InternalError;
            case ERROR_PLAYING_VIDEO:
                return LoadingError.ShowFailed;
            case WRONG_ORIENTATION:
            case TOO_MANY_CONNECTIONS:
            case FIRST_SESSION_INTERSTITIALS_DISABLED:
                return LoadingError.Canceled;
            case INVALID_RESPONSE:
            case VIDEO_ID_MISSING:
                return LoadingError.IncorrectCreative;
            case NO_AD_FOUND:
                return LoadingError.NoFill;
            default:
                return null;
        }
    }
}
