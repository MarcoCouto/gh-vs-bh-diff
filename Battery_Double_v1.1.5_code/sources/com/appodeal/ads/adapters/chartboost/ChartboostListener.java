package com.appodeal.ads.adapters.chartboost;

import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Model.CBError.CBImpressionError;

public class ChartboostListener extends ChartboostDelegate {
    private static final String VIDEO_LOCATION = "Video";
    private static ChartboostListener instance;
    @Nullable
    private UnifiedInterstitialCallback interstitialCallback;
    @Nullable
    private UnifiedRewardedCallback rewardedCallback;
    @Nullable
    private UnifiedVideoCallback videoCallback;

    public void didCompleteInterstitial(String str) {
    }

    public static ChartboostListener getInstance() {
        if (instance == null) {
            instance = new ChartboostListener();
        }
        return instance;
    }

    public void setInterstitialCallback(@Nullable UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.interstitialCallback = unifiedInterstitialCallback;
    }

    public void removeInterstitialCallback(@Nullable UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.interstitialCallback == unifiedInterstitialCallback) {
            this.interstitialCallback = null;
        }
    }

    public void setVideoCallback(@Nullable UnifiedVideoCallback unifiedVideoCallback) {
        this.videoCallback = unifiedVideoCallback;
    }

    public void removeVideoCallback(@Nullable UnifiedVideoCallback unifiedVideoCallback) {
        if (this.videoCallback == unifiedVideoCallback) {
            this.videoCallback = null;
        }
    }

    public void setRewardedCallback(@Nullable UnifiedRewardedCallback unifiedRewardedCallback) {
        this.rewardedCallback = unifiedRewardedCallback;
    }

    public void removeRewardedCallback(@Nullable UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.rewardedCallback == unifiedRewardedCallback) {
            this.rewardedCallback = null;
        }
    }

    public void didCacheInterstitial(String str) {
        UnifiedFullscreenAdCallback callbackForLocation = getCallbackForLocation(str);
        if (callbackForLocation != null) {
            if (Chartboost.hasInterstitial(str)) {
                callbackForLocation.onAdLoaded();
            } else {
                callbackForLocation.onAdLoadFailed(LoadingError.Canceled);
            }
        }
    }

    public void didFailToLoadInterstitial(String str, CBImpressionError cBImpressionError) {
        UnifiedFullscreenAdCallback callbackForLocation = getCallbackForLocation(str);
        if (callbackForLocation != null) {
            if (cBImpressionError != null) {
                callbackForLocation.printError(cBImpressionError.toString(), null);
            }
            callbackForLocation.onAdLoadFailed(ChartboostNetwork.mapError(cBImpressionError));
        }
    }

    public void didDisplayInterstitial(String str) {
        UnifiedFullscreenAdCallback callbackForLocation = getCallbackForLocation(str);
        if (callbackForLocation != null) {
            callbackForLocation.onAdShown();
        }
    }

    public void didClickInterstitial(String str) {
        UnifiedFullscreenAdCallback callbackForLocation = getCallbackForLocation(str);
        if (callbackForLocation != null) {
            callbackForLocation.onAdClicked();
        }
    }

    public void didDismissInterstitial(String str) {
        UnifiedFullscreenAdCallback callbackForLocation = getCallbackForLocation(str);
        if (callbackForLocation != null) {
            callbackForLocation.onAdClosed();
        }
    }

    public void didCacheRewardedVideo(String str) {
        if (this.rewardedCallback == null) {
            return;
        }
        if (Chartboost.hasRewardedVideo(str)) {
            this.rewardedCallback.onAdLoaded();
        } else {
            this.rewardedCallback.onAdLoadFailed(LoadingError.Canceled);
        }
    }

    public void didFailToLoadRewardedVideo(String str, CBImpressionError cBImpressionError) {
        if (this.rewardedCallback != null) {
            if (cBImpressionError != null) {
                this.rewardedCallback.printError(cBImpressionError.toString(), null);
            }
            this.rewardedCallback.onAdLoadFailed(ChartboostNetwork.mapError(cBImpressionError));
        }
    }

    public void didDisplayRewardedVideo(String str) {
        if (this.rewardedCallback != null) {
            this.rewardedCallback.onAdShown();
        }
    }

    public void didCompleteRewardedVideo(String str, int i) {
        if (this.rewardedCallback != null) {
            this.rewardedCallback.onAdFinished();
        }
    }

    public void didClickRewardedVideo(String str) {
        if (this.rewardedCallback != null) {
            this.rewardedCallback.onAdClicked();
        }
    }

    public void didDismissRewardedVideo(String str) {
        if (this.rewardedCallback != null) {
            this.rewardedCallback.onAdClosed();
        }
    }

    private UnifiedFullscreenAdCallback getCallbackForLocation(String str) {
        return "Video".equals(str) ? this.videoCallback : this.interstitialCallback;
    }
}
