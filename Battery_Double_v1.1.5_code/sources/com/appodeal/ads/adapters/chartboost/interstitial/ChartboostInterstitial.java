package com.appodeal.ads.adapters.chartboost.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.chartboost.ChartboostListener;
import com.appodeal.ads.adapters.chartboost.ChartboostNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.chartboost.sdk.Chartboost;

public class ChartboostInterstitial extends UnifiedInterstitial<RequestParams> {
    private static final String INTERSTITIAL_LOCATION = "Interstitial";
    private UnifiedInterstitialCallback callback;

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.callback = unifiedInterstitialCallback;
        ChartboostListener.getInstance().setInterstitialCallback(unifiedInterstitialCallback);
        if (Chartboost.hasInterstitial("Interstitial")) {
            unifiedInterstitialCallback.onAdLoaded();
        } else {
            Chartboost.cacheInterstitial("Interstitial");
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (Chartboost.hasInterstitial("Interstitial")) {
            Chartboost.showInterstitial("Interstitial");
        } else {
            unifiedInterstitialCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        ChartboostListener.getInstance().removeInterstitialCallback(this.callback);
    }
}
