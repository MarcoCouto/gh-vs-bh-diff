package com.appodeal.ads.adapters.chartboost.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.chartboost.ChartboostListener;
import com.appodeal.ads.adapters.chartboost.ChartboostNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.chartboost.sdk.Chartboost;

public class ChartboostVideo extends UnifiedVideo<RequestParams> {
    private static final String VIDEO_LOCATION = "Video";
    private UnifiedVideoCallback callback;

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.callback = unifiedVideoCallback;
        ChartboostListener.getInstance().setVideoCallback(unifiedVideoCallback);
        if (Chartboost.hasInterstitial("Video")) {
            unifiedVideoCallback.onAdLoaded();
        } else {
            Chartboost.cacheInterstitial("Video");
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (Chartboost.hasInterstitial("Video")) {
            Chartboost.showInterstitial("Video");
        } else {
            unifiedVideoCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        ChartboostListener.getInstance().removeVideoCallback(this.callback);
    }
}
