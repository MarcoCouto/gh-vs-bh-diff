package com.appodeal.ads.adapters.ironsource.rewarded_video;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyRewardedVideoListener;

class IronSourceRewardedListener implements ISDemandOnlyRewardedVideoListener {
    @NonNull
    private final UnifiedRewardedCallback callback;
    private boolean isAvailableStateReceived;
    @NonNull
    private final String localInstanceId;

    IronSourceRewardedListener(@NonNull String str, @NonNull UnifiedRewardedCallback unifiedRewardedCallback, boolean z) {
        this.localInstanceId = str;
        this.callback = unifiedRewardedCallback;
        this.isAvailableStateReceived = z;
    }

    public void onRewardedVideoAdOpened(String str) {
        if (TextUtils.equals(str, this.localInstanceId)) {
            this.callback.onAdShown();
        }
    }

    public void onRewardedVideoAdClosed(String str) {
        if (TextUtils.equals(str, this.localInstanceId)) {
            this.callback.onAdClosed();
        }
    }

    public void onRewardedVideoAdLoadSuccess(String str) {
        if (!TextUtils.equals(str, this.localInstanceId)) {
            return;
        }
        if (this.isAvailableStateReceived) {
            this.callback.onAdExpired();
            return;
        }
        this.isAvailableStateReceived = true;
        this.callback.onAdLoaded();
    }

    public void onRewardedVideoAdLoadFailed(String str, IronSourceError ironSourceError) {
        if (!TextUtils.equals(str, this.localInstanceId)) {
            return;
        }
        if (this.isAvailableStateReceived) {
            this.callback.onAdExpired();
            return;
        }
        if (ironSourceError != null) {
            this.callback.printError(ironSourceError.getErrorMessage(), Integer.valueOf(ironSourceError.getErrorCode()));
        }
        this.callback.onAdLoadFailed(null);
    }

    public void onRewardedVideoAdRewarded(String str) {
        if (TextUtils.equals(str, this.localInstanceId)) {
            this.callback.onAdFinished();
        }
    }

    public void onRewardedVideoAdShowFailed(String str, IronSourceError ironSourceError) {
        if (TextUtils.equals(str, this.localInstanceId)) {
            if (ironSourceError != null) {
                this.callback.printError(ironSourceError.getErrorMessage(), Integer.valueOf(ironSourceError.getErrorCode()));
            }
            this.callback.onAdShowFailed();
        }
    }

    public void onRewardedVideoAdClicked(String str) {
        if (TextUtils.equals(str, this.localInstanceId)) {
            this.callback.onAdClicked();
        }
    }
}
