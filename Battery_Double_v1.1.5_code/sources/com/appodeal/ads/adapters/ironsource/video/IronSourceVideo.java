package com.appodeal.ads.adapters.ironsource.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.ironsource.IronSourceNetwork;
import com.appodeal.ads.adapters.ironsource.IronSourceNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.ironsource.mediationsdk.IronSource;

public class IronSourceVideo extends UnifiedVideo<RequestParams> {
    private String instanceId;
    boolean isLoadFailed = false;
    boolean isLoaded = false;

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.instanceId = requestParams.instanceId;
        IronSourceNetwork.registerInterstitialInstances(requestParams.jsonData.optJSONArray("instances"));
        if (IronSourceNetwork.canLoadInstance(this.instanceId)) {
            IronSourceNetwork.subscribeInterstitialListener(this.instanceId, new IronSourceVideoListener(this, unifiedVideoCallback));
            if (IronSource.isISDemandOnlyInterstitialReady(this.instanceId)) {
                unifiedVideoCallback.onAdLoaded();
                return;
            }
            IronSourceNetwork.setInProgressInstance(true);
            IronSource.loadISDemandOnlyInterstitial(this.instanceId);
        } else if (IronSourceNetwork.isInstanceInProgress()) {
            unifiedVideoCallback.onAdLoadFailed(LoadingError.Canceled);
        } else {
            unifiedVideoCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (IronSource.isISDemandOnlyInterstitialReady(this.instanceId)) {
            IronSource.showISDemandOnlyInterstitial(this.instanceId);
        } else {
            unifiedVideoCallback.onAdShowFailed();
        }
    }

    public void onLoaded() {
        super.onLoaded();
        this.isLoaded = true;
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        this.isLoadFailed = true;
    }
}
