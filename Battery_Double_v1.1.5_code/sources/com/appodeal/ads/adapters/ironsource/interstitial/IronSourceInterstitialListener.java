package com.appodeal.ads.adapters.ironsource.interstitial;

import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.ironsource.IronSourceNetwork;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;

class IronSourceInterstitialListener implements ISDemandOnlyInterstitialListener {
    @NonNull
    private final IronSourceInterstitial adObject;
    @NonNull
    private final UnifiedInterstitialCallback callback;

    IronSourceInterstitialListener(@NonNull IronSourceInterstitial ironSourceInterstitial, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.adObject = ironSourceInterstitial;
        this.callback = unifiedInterstitialCallback;
    }

    public void onInterstitialAdReady(String str) {
        if (this.adObject.isLoaded || this.adObject.isLoadFailed) {
            IronSourceNetwork.unsubscribeInterstitialListener(str);
            IronSourceNetwork.setInProgressInstance(false);
            if (this.adObject.isLoaded) {
                this.callback.onAdExpired();
            }
            return;
        }
        this.callback.onAdLoaded();
    }

    public void onInterstitialAdLoadFailed(String str, IronSourceError ironSourceError) {
        IronSourceNetwork.unsubscribeInterstitialListener(str);
        IronSourceNetwork.prepareInstance();
        if (ironSourceError != null) {
            this.callback.printError(ironSourceError.getErrorMessage(), Integer.valueOf(ironSourceError.getErrorCode()));
        }
        this.callback.onAdLoadFailed(null);
    }

    public void onInterstitialAdOpened(String str) {
        this.callback.onAdShown();
    }

    public void onInterstitialAdClosed(String str) {
        IronSourceNetwork.unsubscribeInterstitialListener(str);
        IronSourceNetwork.setInProgressInstance(false);
        this.callback.onAdClosed();
    }

    public void onInterstitialAdShowFailed(String str, IronSourceError ironSourceError) {
        IronSourceNetwork.unsubscribeInterstitialListener(str);
        IronSourceNetwork.setInProgressInstance(false);
        if (ironSourceError != null) {
            this.callback.printError(ironSourceError.getErrorMessage(), Integer.valueOf(ironSourceError.getErrorCode()));
        }
        this.callback.onAdShowFailed();
    }

    public void onInterstitialAdClicked(String str) {
        this.callback.onAdClicked();
    }
}
