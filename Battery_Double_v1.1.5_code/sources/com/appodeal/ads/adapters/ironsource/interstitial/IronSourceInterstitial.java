package com.appodeal.ads.adapters.ironsource.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.ironsource.IronSourceNetwork;
import com.appodeal.ads.adapters.ironsource.IronSourceNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.ironsource.mediationsdk.IronSource;

public class IronSourceInterstitial extends UnifiedInterstitial<RequestParams> {
    private String instanceId;
    boolean isLoadFailed = false;
    boolean isLoaded = false;

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.instanceId = requestParams.instanceId;
        IronSourceNetwork.registerInterstitialInstances(requestParams.jsonData.optJSONArray("instances"));
        if (IronSourceNetwork.canLoadInstance(this.instanceId)) {
            IronSourceNetwork.subscribeInterstitialListener(this.instanceId, new IronSourceInterstitialListener(this, unifiedInterstitialCallback));
            if (IronSource.isISDemandOnlyInterstitialReady(this.instanceId)) {
                unifiedInterstitialCallback.onAdLoaded();
                return;
            }
            IronSourceNetwork.setInProgressInstance(true);
            IronSource.loadISDemandOnlyInterstitial(this.instanceId);
        } else if (IronSourceNetwork.isInstanceInProgress()) {
            unifiedInterstitialCallback.onAdLoadFailed(LoadingError.Canceled);
        } else {
            unifiedInterstitialCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (IronSource.isISDemandOnlyInterstitialReady(this.instanceId)) {
            IronSource.showISDemandOnlyInterstitial(this.instanceId);
        } else {
            unifiedInterstitialCallback.onAdShowFailed();
        }
    }

    public void onLoaded() {
        super.onLoaded();
        this.isLoaded = true;
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        this.isLoadFailed = true;
    }
}
