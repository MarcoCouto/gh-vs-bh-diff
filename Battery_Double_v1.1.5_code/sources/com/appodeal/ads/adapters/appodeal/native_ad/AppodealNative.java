package com.appodeal.ads.adapters.appodeal.native_ad;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.appodeal.AppodealNativeNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.github.mikephil.charting.utils.Utils;
import com.mintegral.msdk.base.entity.CampaignEx;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;

public class AppodealNative extends UnifiedNative<RequestParams> {

    @VisibleForTesting
    static class NativeAdAdapter extends UnifiedNativeAd {
        NativeAdAdapter(String str, String str2, String str3, String str4, String str5, Double d, String str6, long j, String str7, String str8) {
            super(str, str2, str3, str4, str5, d != null ? Float.valueOf(d.floatValue()) : null);
            setClickUrl(str7);
            setVideoUrl(str8);
            String str9 = str6;
            setTrackingPackage(str6, j);
        }
    }

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        RequestParams requestParams2 = requestParams;
        Double valueOf = Double.valueOf(requestParams2.ad.optDouble(CampaignEx.JSON_KEY_STAR));
        if (valueOf.isNaN() || valueOf.doubleValue() == Utils.DOUBLE_EPSILON) {
            valueOf = null;
        }
        NativeAdAdapter nativeAdAdapter = new NativeAdAdapter(requestParams2.ad.getString("title"), requestParams2.ad.getString("description"), UnifiedAdUtils.getStringOrNullFromJson(requestParams2.ad, "button"), UnifiedAdUtils.getStringOrNullFromJson(requestParams2.ad, MessengerShareContentUtility.MEDIA_IMAGE), UnifiedAdUtils.getStringOrNullFromJson(requestParams2.ad, SettingsJsonConstants.APP_ICON_KEY), valueOf, requestParams2.packageName, requestParams2.expiryTime.longValue(), requestParams2.ad.getString("click_url"), UnifiedAdUtils.getStringOrNullFromJson(requestParams2.ad, "video_url"));
        unifiedNativeCallback.onAdLoaded(nativeAdAdapter);
    }
}
