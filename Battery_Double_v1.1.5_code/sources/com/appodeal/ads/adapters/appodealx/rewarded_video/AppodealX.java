package com.appodeal.ads.adapters.appodealx.rewarded_video;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.appodealx.sdk.RewardedVideoAd;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.List;
import org.json.JSONObject;

public class AppodealX extends UnifiedRewarded<RequestParams> {
    @VisibleForTesting
    FullScreenAd fullScreenAd;

    @VisibleForTesting
    static final class Listener implements FullScreenAdListener {
        private final UnifiedRewardedCallback callback;
        private final List<JSONObject> parallelBiddingAds;
        private final UnifiedRewardedParams params;

        public void onFullScreenAdCompleted() {
        }

        Listener(@NonNull UnifiedRewardedCallback unifiedRewardedCallback, @NonNull List<JSONObject> list, @NonNull UnifiedRewardedParams unifiedRewardedParams) {
            this.callback = unifiedRewardedCallback;
            this.parallelBiddingAds = list;
            this.params = unifiedRewardedParams;
        }

        public void onFullScreenAdLoaded(FullScreenAdObject fullScreenAdObject) {
            Bundle bundle = new Bundle();
            bundle.putString("demand_source", fullScreenAdObject.getDemandSource());
            bundle.putDouble(RequestInfoKeys.APPODEAL_ECPM, fullScreenAdObject.getEcpm());
            if (AppodealXNetwork.getWinnerAdUnit(fullScreenAdObject.getAdId(), this.parallelBiddingAds) != null) {
                bundle.putString("id", fullScreenAdObject.getAdId());
            }
            this.callback.onAdInfoRequested(bundle);
            this.callback.onAdLoaded();
        }

        public void onFullScreenAdFailedToLoad(@NonNull AdError adError) {
            this.callback.printError(adError.toString(), null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onFullScreenAdExpired() {
            this.callback.onAdExpired();
        }

        public void onFullScreenAdFailedToShow(@NonNull AdError adError) {
            this.callback.onAdShowFailed();
        }

        public void onFullScreenAdShown() {
            this.callback.onAdShown();
        }

        public void onFullScreenAdClicked() {
            this.callback.onAdClicked();
        }

        public void onFullScreenAdClosed(boolean z) {
            if (z) {
                this.callback.onAdFinished();
            }
            this.callback.onAdClosed();
        }

        public int getPlacementId() {
            return Integer.valueOf(this.params.obtainPlacementId()).intValue();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        List prepareAdUnitListForLoad = AppodealXNetwork.prepareAdUnitListForLoad(requestParams.adUnit, requestParams.parallelBiddingAds, 128);
        AppodealXNetwork.initializeAppodealX(activity, requestParams.restrictedData, prepareAdUnitListForLoad);
        this.fullScreenAd = new RewardedVideoAd();
        this.fullScreenAd.loadAd(activity, requestParams.url, prepareAdUnitListForLoad, Long.valueOf(unifiedRewardedParams.obtainSegmentId()).longValue(), new Listener(unifiedRewardedCallback, prepareAdUnitListForLoad, unifiedRewardedParams));
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.fullScreenAd != null) {
            this.fullScreenAd.show(activity);
        } else {
            unifiedRewardedCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        if (this.fullScreenAd != null) {
            this.fullScreenAd.destroy();
            this.fullScreenAd = null;
        }
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        if (this.fullScreenAd != null && loadingError == LoadingError.TimeoutError) {
            this.fullScreenAd.trackError(1005);
        }
    }
}
