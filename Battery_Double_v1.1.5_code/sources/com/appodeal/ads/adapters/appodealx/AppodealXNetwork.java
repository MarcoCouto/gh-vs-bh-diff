package com.appodeal.ads.adapters.appodealx;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.SparseArray;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.appodealx.banner.AppodealX;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.Log;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class AppodealXNetwork extends AdNetwork<RequestParams> {
    private static final SparseArray<String[]> defaultTypes = new SparseArray<>();

    public static final class RequestParams {
        public final JSONObject adUnit;
        public final List<JSONObject> parallelBiddingAds;
        public final RestrictedData restrictedData;
        public final String url;

        RequestParams(String str, JSONObject jSONObject, RestrictedData restrictedData2, List<JSONObject> list) {
            this.url = str;
            this.adUnit = jSONObject;
            this.restrictedData = restrictedData2;
            this.parallelBiddingAds = list;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "appodealx";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.appodealx.mraid.MraidActivity").build(), new Builder("com.explorestack.iab.vast.activity.VastActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.appodealx.sdk.AppodealX"};
        }

        public AppodealXNetwork build() {
            return new AppodealXNetwork(this);
        }
    }

    static {
        defaultTypes.put(4, new String[]{CampaignEx.JSON_KEY_MRAID});
        defaultTypes.put(256, new String[]{CampaignEx.JSON_KEY_MRAID});
        defaultTypes.put(1, new String[]{CampaignEx.JSON_KEY_MRAID});
        defaultTypes.put(2, new String[]{CampaignEx.JSON_KEY_MRAID, "vast"});
        defaultTypes.put(128, new String[]{CampaignEx.JSON_KEY_MRAID, "vast"});
        defaultTypes.put(512, new String[]{"nast"});
    }

    public AppodealXNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new AppodealX();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new com.appodeal.ads.adapters.appodealx.interstitial.AppodealX();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new com.appodeal.ads.adapters.appodealx.video.AppodealX();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new com.appodeal.ads.adapters.appodealx.rewarded_video.AppodealX();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new com.appodeal.ads.adapters.appodealx.native_ad.AppodealX();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        String string = adUnit.getJsonData().getString("url");
        if (TextUtils.isEmpty(string)) {
            networkInitializationListener.onInitializationFailed(LoadingError.IncorrectAdunit);
            return;
        }
        if (adUnit.getJsonData().optBoolean(String.TOP, false)) {
            string = UnifiedAdUtils.parseUrlWithTopParams(activity, string, adNetworkMediationParams);
        }
        networkInitializationListener.onInitializationFinished(new RequestParams(string, adUnit.getJsonData(), adNetworkMediationParams.getRestrictedData(), adNetworkMediationParams.getParallelBiddingAdUnitList()));
    }

    public String getVersion() {
        return com.appodealx.sdk.AppodealX.getVersion();
    }

    public void setLogging(boolean z) {
        com.appodealx.sdk.AppodealX.setLogging(z);
    }

    public static List<JSONObject> prepareAdUnitListForLoad(@NonNull JSONObject jSONObject, @NonNull List<JSONObject> list, int i) {
        ArrayList arrayList = new ArrayList(list);
        String[] strArr = (String[]) defaultTypes.get(i);
        if (strArr != null) {
            for (String str : strArr) {
                if (!TextUtils.isEmpty(str)) {
                    try {
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("status", str);
                        jSONObject2.put("id", jSONObject.getString("id"));
                        jSONObject2.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getString(RequestInfoKeys.APPODEAL_ECPM));
                        arrayList.add(jSONObject2);
                    } catch (Exception e) {
                        Log.log(e);
                    }
                }
            }
        }
        return arrayList;
    }

    public static void initializeAppodealX(@NonNull Activity activity, @NonNull RestrictedData restrictedData, @NonNull List<JSONObject> list) {
        com.appodealx.sdk.AppodealX.initialize(activity, list);
        updateConsent(activity, restrictedData);
        setTargeting(restrictedData);
    }

    private static void updateConsent(@NonNull Activity activity, @NonNull RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            com.appodealx.sdk.AppodealX.updateConsent(activity, restrictedData.isUserHasConsent(), restrictedData.isUserInGdprScope());
        }
    }

    private static void setTargeting(@NonNull RestrictedData restrictedData) {
        com.appodealx.sdk.AppodealX.updateCoppa(restrictedData.isUserAgeRestricted());
    }

    @Nullable
    public static JSONObject getWinnerAdUnit(@NonNull String str, @NonNull List<JSONObject> list) {
        for (JSONObject jSONObject : list) {
            if (jSONObject.optString("id").equals(str)) {
                return jSONObject;
            }
        }
        return null;
    }
}
