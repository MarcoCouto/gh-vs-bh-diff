package com.appodeal.ads.adapters.appodealx.banner;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.List;
import org.json.JSONObject;

public class AppodealX extends UnifiedBanner<RequestParams> {
    @VisibleForTesting
    BannerView bannerView;
    @VisibleForTesting
    int placementId;

    @VisibleForTesting
    static final class Listener implements BannerListener {
        private final UnifiedBannerCallback callback;
        private final List<JSONObject> parallelBiddingAds;

        Listener(@NonNull UnifiedBannerCallback unifiedBannerCallback, @NonNull List<JSONObject> list) {
            this.callback = unifiedBannerCallback;
            this.parallelBiddingAds = list;
        }

        public void onBannerLoaded(BannerView bannerView) {
            Bundle bundle = new Bundle();
            bundle.putString("demand_source", bannerView.getDemandSource());
            bundle.putDouble(RequestInfoKeys.APPODEAL_ECPM, bannerView.getEcpm());
            if (AppodealXNetwork.getWinnerAdUnit(bannerView.getAdId(), this.parallelBiddingAds) != null) {
                bundle.putString("id", bannerView.getAdId());
            }
            this.callback.onAdInfoRequested(bundle);
            this.callback.onAdLoaded(bannerView, -1, bannerView.getBannerHeight());
        }

        public void onBannerFailedToLoad(@NonNull AdError adError) {
            this.callback.printError(adError.toString(), null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onBannerExpired() {
            this.callback.onAdExpired();
        }

        public void onBannerClicked() {
            this.callback.onAdClicked();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        List prepareAdUnitListForLoad = AppodealXNetwork.prepareAdUnitListForLoad(requestParams.adUnit, requestParams.parallelBiddingAds, 4);
        AppodealXNetwork.initializeAppodealX(activity, requestParams.restrictedData, prepareAdUnitListForLoad);
        this.placementId = Integer.valueOf(unifiedBannerParams.obtainPlacementId()).intValue();
        this.bannerView = new BannerView(activity);
        this.bannerView.loadAd(requestParams.url, prepareAdUnitListForLoad, Long.valueOf(unifiedBannerParams.obtainSegmentId()).longValue(), new Listener(unifiedBannerCallback, prepareAdUnitListForLoad));
    }

    public void onDestroy() {
        if (this.bannerView != null) {
            this.bannerView.destroy();
            this.bannerView = null;
        }
    }

    public void onShow() {
        if (this.bannerView != null) {
            this.bannerView.trackImpression(this.placementId);
        }
        super.onShow();
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        if (this.bannerView != null && loadingError == LoadingError.TimeoutError) {
            this.bannerView.trackError(1005);
        }
    }
}
