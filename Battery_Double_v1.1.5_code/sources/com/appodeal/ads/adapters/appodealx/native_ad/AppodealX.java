package com.appodeal.ads.adapters.appodealx.native_ad;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.NativeMediaView;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.NativeAd;
import com.appodealx.sdk.NativeAdObject;
import com.appodealx.sdk.NativeListener;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

public class AppodealX extends UnifiedNative<RequestParams> {
    @VisibleForTesting
    NativeAd nativeAd;
    @VisibleForTesting
    NativeAdAdapter nativeAdAdapter;

    private final class Listener implements NativeListener {
        private final UnifiedNativeCallback callback;
        private final List<JSONObject> parallelBiddingAds;
        private final UnifiedNativeParams params;

        Listener(@NonNull UnifiedNativeParams unifiedNativeParams, @NonNull UnifiedNativeCallback unifiedNativeCallback, @NonNull List<JSONObject> list) {
            this.params = unifiedNativeParams;
            this.callback = unifiedNativeCallback;
            this.parallelBiddingAds = list;
        }

        public void onNativeLoaded(NativeAdObject nativeAdObject) {
            Bundle bundle = new Bundle();
            bundle.putString("demand_source", nativeAdObject.getDemandSource());
            bundle.putDouble(RequestInfoKeys.APPODEAL_ECPM, nativeAdObject.getEcpm());
            if (AppodealXNetwork.getWinnerAdUnit(nativeAdObject.getAdId(), this.parallelBiddingAds) != null) {
                bundle.putString("id", nativeAdObject.getAdId());
            }
            this.callback.onAdInfoRequested(bundle);
            AppodealX.this.nativeAdAdapter = new NativeAdAdapter(this.params, nativeAdObject);
            this.callback.onAdLoaded(AppodealX.this.nativeAdAdapter);
        }

        public void onNativeFailedToLoad(@NonNull AdError adError) {
            this.callback.printError(adError.toString(), null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onNativeClicked() {
            this.callback.onAdClicked();
        }

        public void onNativeExpired() {
            this.callback.onAdExpired();
        }
    }

    @VisibleForTesting
    static class NativeAdAdapter extends UnifiedNativeAd {
        private final NativeAdObject nativeAdObject;
        private final UnifiedNativeParams params;

        NativeAdAdapter(@NonNull UnifiedNativeParams unifiedNativeParams, NativeAdObject nativeAdObject2) {
            super(nativeAdObject2.getTitle(), nativeAdObject2.getDescription(), nativeAdObject2.getCta(), nativeAdObject2.getImage(), nativeAdObject2.getIcon(), Float.valueOf((float) nativeAdObject2.getRating()));
            setClickUrl(nativeAdObject2.getUrl());
            setVastVideoTag(nativeAdObject2.getVideoTag());
            this.params = unifiedNativeParams;
            this.nativeAdObject = nativeAdObject2;
        }

        public void onAdImpression(@Nullable View view) {
            super.onAdImpression(view);
            this.nativeAdObject.onImpression(Integer.valueOf(this.params.obtainPlacementId()).intValue());
        }

        public void onAdClick(@Nullable View view) {
            super.onAdClick(view);
            this.nativeAdObject.onAdClick();
        }

        public String getAgeRestriction() {
            return this.nativeAdObject.getAgeRestrictions();
        }

        public boolean hasVideo() {
            return this.nativeAdObject.hasVideo();
        }

        public boolean containsVideo() {
            return this.nativeAdObject.containsVideo();
        }

        @Nullable
        public View obtainIconView(@NonNull Context context) {
            return this.nativeAdObject.getIconView(context);
        }

        public boolean onConfigureMediaView(@NonNull NativeMediaView nativeMediaView) {
            View mediaView = this.nativeAdObject.getMediaView(nativeMediaView.getContext());
            if (mediaView == null) {
                return super.onConfigureMediaView(nativeMediaView);
            }
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.addRule(13, -1);
            nativeMediaView.removeAllViews();
            nativeMediaView.addView(mediaView, layoutParams);
            return true;
        }

        @Nullable
        public View obtainProviderView(@NonNull Context context) {
            View providerView = this.nativeAdObject.getProviderView(context);
            if (providerView == null) {
                return super.obtainProviderView(context);
            }
            RelativeLayout relativeLayout = new RelativeLayout(context);
            relativeLayout.addView(providerView, new LayoutParams(Math.round(UnifiedAdUtils.getScreenDensity(context) * 20.0f), Math.round(UnifiedAdUtils.getScreenDensity(context) * 20.0f)));
            return relativeLayout;
        }

        public void onRegisterForInteraction(@NonNull NativeAdView nativeAdView) {
            super.onRegisterForInteraction(nativeAdView);
            this.nativeAdObject.registerViewForInteraction(nativeAdView, nativeAdView.getClickableViews());
        }

        public void onUnregisterForInteraction() {
            super.onUnregisterForInteraction();
            this.nativeAdObject.unregisterViewForInteraction();
        }

        public void onDestroy() {
            this.nativeAdObject.destroy();
            super.onDestroy();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        List prepareAdUnitListForLoad = AppodealXNetwork.prepareAdUnitListForLoad(requestParams.adUnit, requestParams.parallelBiddingAds, 512);
        AppodealXNetwork.initializeAppodealX(activity, requestParams.restrictedData, prepareAdUnitListForLoad);
        HashMap hashMap = new HashMap();
        hashMap.put("media_asset_type", unifiedNativeParams.getMediaAssetType().name());
        hashMap.put("native_ad_type", unifiedNativeParams.getNativeAdType().name());
        this.nativeAd = new NativeAd();
        this.nativeAd.loadAd(activity, requestParams.url, prepareAdUnitListForLoad, hashMap, Long.valueOf(unifiedNativeParams.obtainSegmentId()).longValue(), createListener(unifiedNativeParams, unifiedNativeCallback, prepareAdUnitListForLoad));
    }

    public void onDestroy() {
        if (this.nativeAdAdapter != null) {
            this.nativeAdAdapter.onDestroy();
            this.nativeAdAdapter = null;
        }
        if (this.nativeAd != null) {
            this.nativeAd.destroy();
            this.nativeAd = null;
        }
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        if (this.nativeAd != null && loadingError == LoadingError.TimeoutError) {
            this.nativeAd.trackError(1005);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public NativeListener createListener(@NonNull UnifiedNativeParams unifiedNativeParams, @NonNull UnifiedNativeCallback unifiedNativeCallback, @NonNull List<JSONObject> list) {
        return new Listener(unifiedNativeParams, unifiedNativeCallback, list);
    }
}
