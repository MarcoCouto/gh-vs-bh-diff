package com.appodeal.ads.adapters.facebook.mrec;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.facebook.FacebookNetwork;
import com.appodeal.ads.adapters.facebook.FacebookNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

public class Facebook extends UnifiedMrec<RequestParams> {
    /* access modifiers changed from: private */
    public AdView adView;

    private final class Listener implements AdListener {
        private final UnifiedMrecCallback callback;

        public void onLoggingImpression(Ad ad) {
        }

        Listener(@NonNull UnifiedMrecCallback unifiedMrecCallback) {
            this.callback = unifiedMrecCallback;
        }

        public void onAdLoaded(Ad ad) {
            this.callback.onAdLoaded(Facebook.this.adView);
        }

        public void onError(Ad ad, AdError adError) {
            if (ad != null) {
                ad.destroy();
            }
            if (adError != null) {
                this.callback.printError(adError.getErrorMessage(), Integer.valueOf(adError.getErrorCode()));
            }
            this.callback.onAdLoadFailed(FacebookNetwork.mapError(adError));
        }

        public void onAdClicked(Ad ad) {
            this.callback.onAdClicked();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        this.adView = new AdView((Context) activity, requestParams.facebookKey, AdSize.RECTANGLE_HEIGHT_250);
        this.adView.setAdListener(new Listener(unifiedMrecCallback));
        this.adView.loadAd();
    }

    public void onDestroy() {
        if (this.adView != null) {
            this.adView.setAdListener(null);
            this.adView.destroy();
            this.adView = null;
        }
    }
}
