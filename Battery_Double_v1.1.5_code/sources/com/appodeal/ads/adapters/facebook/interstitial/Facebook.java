package com.appodeal.ads.adapters.facebook.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.facebook.FacebookNetwork;
import com.appodeal.ads.adapters.facebook.FacebookNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;

public class Facebook extends UnifiedInterstitial<RequestParams> {
    private InterstitialAd interstitialAd;

    private static final class Listener implements InterstitialAdListener {
        private final UnifiedInterstitialCallback callback;

        public void onInterstitialDisplayed(Ad ad) {
        }

        Listener(@NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
            this.callback = unifiedInterstitialCallback;
        }

        public void onAdLoaded(Ad ad) {
            this.callback.onAdLoaded();
        }

        public void onError(Ad ad, AdError adError) {
            if (ad != null) {
                ad.destroy();
            }
            if (adError != null) {
                this.callback.printError(adError.getErrorMessage(), Integer.valueOf(adError.getErrorCode()));
            }
            this.callback.onAdLoadFailed(FacebookNetwork.mapError(adError));
        }

        public void onAdClicked(Ad ad) {
            this.callback.onAdClicked();
        }

        public void onInterstitialDismissed(Ad ad) {
            if (ad != null) {
                ad.destroy();
            }
            this.callback.onAdClosed();
        }

        public void onLoggingImpression(Ad ad) {
            this.callback.onAdShown();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.interstitialAd = new InterstitialAd(activity, requestParams.facebookKey);
        this.interstitialAd.setAdListener(new Listener(unifiedInterstitialCallback));
        this.interstitialAd.loadAd();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.interstitialAd == null || !this.interstitialAd.isAdLoaded() || this.interstitialAd.isAdInvalidated()) {
            unifiedInterstitialCallback.onAdShowFailed();
        } else {
            this.interstitialAd.show();
        }
    }

    public void onDestroy() {
        if (this.interstitialAd != null) {
            this.interstitialAd.setAdListener(null);
            this.interstitialAd.destroy();
            this.interstitialAd = null;
        }
    }
}
