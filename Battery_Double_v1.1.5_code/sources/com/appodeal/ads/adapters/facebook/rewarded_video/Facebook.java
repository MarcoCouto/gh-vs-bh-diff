package com.appodeal.ads.adapters.facebook.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.facebook.FacebookNetwork;
import com.appodeal.ads.adapters.facebook.FacebookNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdListener;

public class Facebook extends UnifiedRewarded<RequestParams> {
    private RewardedVideoAd rewardedVideoAd;

    private static final class Listener implements RewardedVideoAdListener {
        private final UnifiedRewardedCallback callback;

        private Listener(@NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
            this.callback = unifiedRewardedCallback;
        }

        public void onAdLoaded(Ad ad) {
            this.callback.onAdLoaded();
        }

        public void onError(Ad ad, AdError adError) {
            if (ad != null) {
                ad.destroy();
            }
            if (adError != null) {
                this.callback.printError(adError.getErrorMessage(), Integer.valueOf(adError.getErrorCode()));
            }
            this.callback.onAdLoadFailed(FacebookNetwork.mapError(adError));
        }

        public void onLoggingImpression(Ad ad) {
            this.callback.onAdShown();
        }

        public void onRewardedVideoCompleted() {
            this.callback.onAdFinished();
        }

        public void onRewardedVideoClosed() {
            this.callback.onAdClosed();
        }

        public void onAdClicked(Ad ad) {
            this.callback.onAdClicked();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.rewardedVideoAd = new RewardedVideoAd(activity, requestParams.facebookKey);
        this.rewardedVideoAd.setAdListener(new Listener(unifiedRewardedCallback));
        this.rewardedVideoAd.loadAd();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.rewardedVideoAd == null || !this.rewardedVideoAd.isAdLoaded() || this.rewardedVideoAd.isAdInvalidated()) {
            unifiedRewardedCallback.onAdShowFailed();
        } else {
            this.rewardedVideoAd.show();
        }
    }

    public void onDestroy() {
        if (this.rewardedVideoAd != null) {
            this.rewardedVideoAd.setAdListener(null);
            this.rewardedVideoAd.destroy();
            this.rewardedVideoAd = null;
        }
    }
}
