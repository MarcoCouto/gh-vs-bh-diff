package com.appodeal.ads.adapters.facebook;

import android.app.Activity;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.adapters.facebook.banner.Facebook;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.DependencyRule;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AdSettings.TestAdType;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.BuildConfig;
import java.util.ArrayList;
import java.util.List;

public class FacebookNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final String facebookKey;

        RequestParams(String str) {
            this.facebookKey = str;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "facebook";
        }

        public String[] getRequiredPermissions() {
            return new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"};
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.facebook.ads.AudienceNetworkActivity").build(), new Builder("com.facebook.ads.internal.ipc.RemoteANActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.facebook.ads.AdView", "com.facebook.ads.NativeAd", "com.facebook.ads.NativeBannerAd", "com.facebook.ads.InterstitialAd", "com.facebook.ads.RewardedVideoAd"};
        }

        public DependencyRule[] getOptionalClasses() {
            return new DependencyRule[]{new DependencyRule("android.support.v7.widget.RecyclerView", "Required for Fullscreen and Native ads")};
        }

        public List<Pair<String, Pair<String, String>>> getRequiredServiceWithData() {
            return new ArrayList<Pair<String, Pair<String, String>>>() {
                {
                    add(new Pair("com.facebook.ads.internal.ipc.AdsProcessPriorityService", null));
                    add(new Pair("com.facebook.ads.internal.ipc.AdsMessengerService", null));
                }
            };
        }

        public String[] getRequiredProvidersClassName() {
            return new String[]{"com.facebook.ads.AudienceNetworkContentProvider"};
        }

        public FacebookNetwork build() {
            return new FacebookNetwork(this);
        }
    }

    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public void setLogging(boolean z) {
    }

    private FacebookNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new Facebook();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new com.appodeal.ads.adapters.facebook.mrec.Facebook();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new com.appodeal.ads.adapters.facebook.interstitial.Facebook();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new com.appodeal.ads.adapters.facebook.rewarded_video.Facebook();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new com.appodeal.ads.adapters.facebook.native_ad.Facebook();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        if (VERSION.SDK_INT < 15) {
            networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
            return;
        }
        String string = adUnit.getJsonData().getString("facebook_key");
        AdSettings.setIsChildDirected(adNetworkMediationParams.getRestrictedData().isUserAgeRestricted());
        if (!AudienceNetworkAds.isInitialized(activity)) {
            AudienceNetworkAds.initialize(activity);
            if (adNetworkMediationParams.isTestMode()) {
                AdSettings.setTestAdType(TestAdType.DEFAULT);
            }
        }
        if (!TextUtils.isEmpty(adUnit.getMediatorName())) {
            AdSettings.setMediationService(adUnit.getMediatorName());
        }
        networkInitializationListener.onInitializationFinished(new RequestParams(string));
    }

    @Nullable
    public static LoadingError mapError(@Nullable AdError adError) {
        if (adError == null) {
            return null;
        }
        switch (adError.getErrorCode()) {
            case 1000:
                return LoadingError.ConnectionError;
            case 1001:
            case 1002:
            case 2000:
            case 2001:
            case 2002:
            case 3001:
                return LoadingError.NoFill;
            case AdError.INTERSTITIAL_AD_TIMEOUT /*2009*/:
                return LoadingError.TimeoutError;
            case 2100:
            case AdError.ICONVIEW_MISSING_ERROR_CODE /*6002*/:
            case AdError.AD_ASSETS_UNSUPPORTED_TYPE_ERROR_CODE /*6003*/:
                return LoadingError.InvalidAssets;
            case AdError.SHOW_CALLED_BEFORE_LOAD_ERROR_CODE /*7001*/:
            case AdError.LOAD_CALLED_WHILE_SHOWING_AD /*7002*/:
            case AdError.MISSING_DEPENDENCIES_ERROR /*7005*/:
            case AdError.API_NOT_SUPPORTED /*7006*/:
                return LoadingError.InternalError;
            case AdError.NATIVE_AD_IS_NOT_LOADED /*7007*/:
                return LoadingError.IncorrectAdunit;
            default:
                return null;
        }
    }
}
