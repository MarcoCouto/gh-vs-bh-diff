package com.appodeal.ads.adapters.admob.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.admob.AdmobNetwork;
import com.appodeal.ads.adapters.admob.AdmobNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class AdmobRewarded extends UnifiedRewarded<RequestParams> {
    @Nullable
    private AdmobRewardedListener listener;
    @Nullable
    private RewardedVideoAd rewardedVideoAd;

    @VisibleForTesting
    static final class AdmobRewardedListener implements RewardedVideoAdListener {
        @NonNull
        private UnifiedRewardedCallback callback;

        public void onRewardedVideoCompleted() {
        }

        public void onRewardedVideoStarted() {
        }

        AdmobRewardedListener(@NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
            this.callback = unifiedRewardedCallback;
        }

        public void onRewardedVideoAdLoaded() {
            this.callback.onAdLoaded();
        }

        public void onRewardedVideoAdOpened() {
            this.callback.onAdShown();
        }

        public void onRewardedVideoAdClosed() {
            this.callback.onAdClosed();
        }

        public void onRewarded(RewardItem rewardItem) {
            this.callback.onAdFinished();
        }

        public void onRewardedVideoAdLeftApplication() {
            this.callback.onAdClicked();
        }

        public void onRewardedVideoAdFailedToLoad(int i) {
            this.callback.printError(null, Integer.valueOf(i));
            this.callback.onAdLoadFailed(AdmobNetwork.mapError(i));
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(activity);
        if (this.rewardedVideoAd == null) {
            unifiedRewardedCallback.onAdLoadFailed(LoadingError.InternalError);
            return;
        }
        this.listener = new AdmobRewardedListener(unifiedRewardedCallback);
        this.rewardedVideoAd.setRewardedVideoAdListener(this.listener);
        this.rewardedVideoAd.loadAd(requestParams.key, requestParams.request);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.rewardedVideoAd == null || !this.rewardedVideoAd.isLoaded()) {
            unifiedRewardedCallback.onAdShowFailed();
        } else {
            this.rewardedVideoAd.show();
        }
    }

    public void onDestroy() {
        if (this.rewardedVideoAd != null) {
            if (this.listener == this.rewardedVideoAd.getRewardedVideoAdListener()) {
                this.rewardedVideoAd.setRewardedVideoAdListener(null);
                this.listener = null;
            }
            this.rewardedVideoAd = null;
        }
    }
}
