package com.appodeal.ads.adapters.admob.mrec;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.admob.AdmobNetwork;
import com.appodeal.ads.adapters.admob.AdmobNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AdmobMrec extends UnifiedMrec<RequestParams> {
    @Nullable
    private AdView adView;

    @VisibleForTesting
    static final class AdmobMrecListener extends AdListener {
        @NonNull
        private AdView adView;
        @NonNull
        private UnifiedMrecCallback callback;

        AdmobMrecListener(@NonNull UnifiedMrecCallback unifiedMrecCallback, @NonNull AdView adView2) {
            this.callback = unifiedMrecCallback;
            this.adView = adView2;
        }

        public void onAdLoaded() {
            super.onAdLoaded();
            this.callback.onAdLoaded(this.adView);
        }

        public void onAdFailedToLoad(int i) {
            super.onAdFailedToLoad(i);
            this.callback.printError(null, Integer.valueOf(i));
            this.callback.onAdLoadFailed(AdmobNetwork.mapError(i));
        }

        public void onAdLeftApplication() {
            super.onAdLeftApplication();
            this.callback.onAdClicked();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        this.adView = new AdView(activity);
        this.adView.setAdUnitId(requestParams.key);
        this.adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        this.adView.setAdListener(new AdmobMrecListener(unifiedMrecCallback, this.adView));
        this.adView.loadAd(requestParams.request);
    }

    public void onDestroy() {
        if (this.adView != null) {
            this.adView.setAdListener(null);
            this.adView.destroy();
            this.adView = null;
        }
    }
}
