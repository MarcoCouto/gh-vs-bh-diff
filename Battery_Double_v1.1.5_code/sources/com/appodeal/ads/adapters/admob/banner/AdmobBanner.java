package com.appodeal.ads.adapters.admob.banner;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.admob.AdmobNetwork;
import com.appodeal.ads.adapters.admob.AdmobNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AdmobBanner extends UnifiedBanner<RequestParams> {
    @Nullable
    private AdView adView;

    @VisibleForTesting
    static final class AdmobBannerListener extends AdListener {
        @NonNull
        private AdView adView;
        @NonNull
        private AdmobBanner banner;
        @NonNull
        private UnifiedBannerCallback callback;
        private int targetHeight;

        AdmobBannerListener(@NonNull AdmobBanner admobBanner, @NonNull UnifiedBannerCallback unifiedBannerCallback, @NonNull AdView adView2, int i) {
            this.banner = admobBanner;
            this.callback = unifiedBannerCallback;
            this.adView = adView2;
            this.targetHeight = i;
        }

        public void onAdLoaded() {
            super.onAdLoaded();
            if (this.adView.getAdSize() == AdSize.SMART_BANNER) {
                this.banner.setRefreshOnRotate(true);
            }
            this.callback.onAdLoaded(this.adView, -1, this.targetHeight);
        }

        public void onAdFailedToLoad(int i) {
            super.onAdFailedToLoad(i);
            this.callback.printError(null, Integer.valueOf(i));
            this.callback.onAdLoadFailed(AdmobNetwork.mapError(i));
        }

        public void onAdLeftApplication() {
            super.onAdLeftApplication();
            this.callback.onAdClicked();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0030, code lost:
        if (r0 > 720.0f) goto L_0x0040;
     */
    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        this.adView = new AdView(activity);
        this.adView.setAdUnitId(requestParams.key);
        float screenHeightInDp = UnifiedAdUtils.getScreenHeightInDp(activity);
        int i = 50;
        if (unifiedBannerParams.useSmartBanners(activity)) {
            this.adView.setAdSize(AdSize.SMART_BANNER);
            if (screenHeightInDp <= 400.0f) {
                i = 32;
            }
            this.adView.setAdListener(new AdmobBannerListener(this, unifiedBannerCallback, this.adView, i));
            this.adView.loadAd(requestParams.request);
        } else if (unifiedBannerParams.needLeaderBoard(activity)) {
            this.adView.setAdSize(AdSize.LEADERBOARD);
        } else {
            this.adView.setAdSize(AdSize.BANNER);
            this.adView.setAdListener(new AdmobBannerListener(this, unifiedBannerCallback, this.adView, i));
            this.adView.loadAd(requestParams.request);
        }
        i = 90;
        this.adView.setAdListener(new AdmobBannerListener(this, unifiedBannerCallback, this.adView, i));
        this.adView.loadAd(requestParams.request);
    }

    public void onDestroy() {
        if (this.adView != null) {
            this.adView.setAdListener(null);
            this.adView.destroy();
            this.adView = null;
        }
    }
}
