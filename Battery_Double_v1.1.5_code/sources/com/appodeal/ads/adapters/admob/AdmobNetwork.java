package com.appodeal.ads.adapters.admob;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.applovin.sdk.AppLovinMediationProvider;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.admob.banner.AdmobBanner;
import com.appodeal.ads.adapters.admob.interstitial.AdmobInterstitial;
import com.appodeal.ads.adapters.admob.mrec.AdmobMrec;
import com.appodeal.ads.adapters.admob.native_ad.AdmobNative;
import com.appodeal.ads.adapters.admob.rewarded_video.AdmobRewarded;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.Log;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdActivity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.tapjoy.TapjoyConstants;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

public class AdmobNetwork extends AdNetwork<RequestParams> {
    private static boolean isInitialized = false;

    public static final class RequestParams {
        public final String key;
        public final AdRequest request;

        RequestParams(String str, AdRequest adRequest) {
            this.key = str;
            this.request = adRequest;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return AppLovinMediationProvider.ADMOB;
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder(AdActivity.CLASS_NAME).build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.google.android.gms.ads.AdView", "com.google.android.gms.ads.formats.NativeAppInstallAdView", "com.google.android.gms.ads.InterstitialAd", "com.google.android.gms.ads.reward.RewardedVideoAd"};
        }

        public AdmobNetwork build() {
            return new AdmobNetwork(this);
        }
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return false;
    }

    public boolean isSupportSmartBanners() {
        return true;
    }

    public AdmobNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    public String getVersion() {
        return String.valueOf(GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new AdmobBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new AdmobMrec();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new AdmobRewarded();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new AdmobInterstitial();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new AdmobNative();
    }

    public boolean isPermissionRequired(@NonNull String str, AdType adType) {
        if (VERSION.SDK_INT > 18 || adType != AdType.Rewarded || !"android.permission.WRITE_EXTERNAL_STORAGE".equals(str)) {
            return super.isPermissionRequired(str, adType);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        String string = adUnit.getJsonData().getString("admob_key");
        if (!isInitialized) {
            isInitialized = true;
            MobileAds.initialize(activity);
        }
        networkInitializationListener.onInitializationFinished(new RequestParams(string, getAdRequest(activity, adUnit, adNetworkMediationParams)));
    }

    private String getTestDeviceId(Context context) {
        try {
            return new BigInteger(1, MessageDigest.getInstance(CommonMD5.TAG).digest(Secure.getString(context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID).getBytes("UTF-8"))).toString(16).toUpperCase(Locale.ENGLISH);
        } catch (Exception e) {
            Log.log(e);
            return "B3EEABB8EE11C2BE770B684D95219ECB";
        }
    }

    private AdRequest getAdRequest(Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams) {
        AdRequest.Builder builder2 = new AdRequest.Builder();
        setMediatorName(builder2, adUnit.getMediatorName());
        updateConsent(builder2, adNetworkMediationParams.getRestrictedData());
        setTargeting(activity, builder2, adNetworkMediationParams.getRestrictedData());
        if (adNetworkMediationParams.isTestMode()) {
            builder2.addTestDevice(getTestDeviceId(activity));
            builder2.addTestDevice("B3EEABB8EE11C2BE770B684D95219ECB");
        }
        return builder2.build();
    }

    @VisibleForTesting
    public void updateConsent(AdRequest.Builder builder2, RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope() && restrictedData.isUserGdprProtected()) {
            Bundle bundle = new Bundle();
            bundle.putString("npa", "1");
            builder2.addNetworkExtrasBundle(AdMobAdapter.class, bundle);
        }
    }

    private void setMediatorName(AdRequest.Builder builder2, @Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            builder2.setRequestAgent(str);
        }
    }

    private void setTargeting(Context context, AdRequest.Builder builder2, RestrictedData restrictedData) {
        builder2.tagForChildDirectedTreatment(restrictedData.isUserAgeRestricted());
        try {
            Location deviceLocation = restrictedData.getLocation(context).getDeviceLocation();
            if (deviceLocation != null) {
                builder2.getClass().getDeclaredMethod("setLocation", new Class[]{Location.class});
                builder2.setLocation(deviceLocation);
            }
        } catch (NoSuchMethodException e) {
            Log.log(e);
        }
        Gender gender = restrictedData.getGender();
        if (gender == Gender.OTHER) {
            builder2.setGender(0);
        } else if (gender == Gender.FEMALE) {
            builder2.setGender(2);
        } else if (gender == Gender.MALE) {
            builder2.setGender(1);
        }
    }

    public static LoadingError mapError(int i) {
        switch (i) {
            case 0:
            case 3:
                return LoadingError.NoFill;
            case 1:
                return LoadingError.IncorrectAdunit;
            case 2:
                return LoadingError.ConnectionError;
            default:
                return null;
        }
    }
}
