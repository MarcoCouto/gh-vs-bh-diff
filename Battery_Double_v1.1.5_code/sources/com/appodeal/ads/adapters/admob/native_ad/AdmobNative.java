package com.appodeal.ads.adapters.admob.native_ad;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.NativeMediaView;
import com.appodeal.ads.adapters.admob.AdmobNetwork;
import com.appodeal.ads.adapters.admob.AdmobNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeAdOptions.Builder;
import com.google.android.gms.ads.formats.UnifiedNativeAd.OnUnifiedNativeAdLoadedListener;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

public class AdmobNative extends UnifiedNative<RequestParams> {

    private static class AdmobNativeAd extends UnifiedNativeAd {
        private MediaView mediaView;
        private final com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
        private UnifiedNativeAdView unifiedNativeAdView;

        AdmobNativeAd(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd2, String str, String str2) {
            super(unifiedNativeAd2.getHeadline(), unifiedNativeAd2.getBody(), unifiedNativeAd2.getCallToAction(), str2, str, (unifiedNativeAd2.getStarRating() == null || unifiedNativeAd2.getStarRating().doubleValue() == Utils.DOUBLE_EPSILON) ? null : Float.valueOf(unifiedNativeAd2.getStarRating().floatValue()));
            this.unifiedNativeAd = unifiedNativeAd2;
        }

        public boolean onConfigureMediaView(@NonNull NativeMediaView nativeMediaView) {
            this.mediaView = new MediaView(nativeMediaView.getContext());
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.addRule(13, -1);
            nativeMediaView.removeAllViews();
            nativeMediaView.addView(this.mediaView, layoutParams);
            return true;
        }

        public void onConfigure(@NonNull NativeAdView nativeAdView) {
            super.onConfigure(nativeAdView);
            this.unifiedNativeAdView = new UnifiedNativeAdView(nativeAdView.getContext());
            this.unifiedNativeAdView.setHeadlineView(nativeAdView.getTitleView());
            this.unifiedNativeAdView.setBodyView(nativeAdView.getDescriptionView());
            this.unifiedNativeAdView.setIconView(nativeAdView.getNativeIconView());
            this.unifiedNativeAdView.setCallToActionView(nativeAdView.getCallToActionView());
            this.unifiedNativeAdView.setStarRatingView(nativeAdView.getRatingView());
            this.unifiedNativeAdView.setMediaView(this.mediaView);
            nativeAdView.configureContainer(this.unifiedNativeAdView);
        }

        public void onRegisterForInteraction(@NonNull NativeAdView nativeAdView) {
            super.onRegisterForInteraction(nativeAdView);
            if (this.unifiedNativeAdView != null) {
                this.unifiedNativeAdView.setNativeAd(this.unifiedNativeAd);
            }
        }

        public boolean hasVideo() {
            return this.unifiedNativeAd.getVideoController().hasVideoContent();
        }

        public boolean containsVideo() {
            return hasVideo();
        }

        public void onDestroy() {
            super.onDestroy();
            this.unifiedNativeAd.destroy();
            if (this.unifiedNativeAdView != null) {
                this.unifiedNativeAdView.destroy();
            }
        }
    }

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull final UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        Builder imageOrientation = new Builder().setReturnUrlsForImageAssets(false).setRequestMultipleImages(false).setImageOrientation(2);
        if (unifiedNativeParams.getNativeAdType() != NativeAdType.NoVideo) {
            imageOrientation.setVideoOptions(new VideoOptions.Builder().setStartMuted(false).build());
        }
        new AdLoader.Builder((Context) activity, requestParams.key).forUnifiedNativeAd(new OnUnifiedNativeAdLoadedListener() {
            public void onUnifiedNativeAdLoaded(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd) {
                if (AdmobNative.this.isNativeValid(unifiedNativeAd)) {
                    unifiedNativeCallback.onAdLoaded(new AdmobNativeAd(unifiedNativeAd, unifiedNativeAd.getIcon().getUri().toString(), ((Image) unifiedNativeAd.getImages().get(0)).getUri().toString()));
                    return;
                }
                unifiedNativeCallback.onAdLoadFailed(LoadingError.IncorrectCreative);
            }
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(int i) {
                unifiedNativeCallback.printError(null, Integer.valueOf(i));
                unifiedNativeCallback.onAdLoadFailed(AdmobNetwork.mapError(i));
            }

            public void onAdOpened() {
                unifiedNativeCallback.onAdClicked();
            }

            public void onAdLeftApplication() {
                unifiedNativeCallback.onAdClicked();
            }
        }).withNativeAdOptions(imageOrientation.build()).build().loadAd(requestParams.request);
    }

    /* access modifiers changed from: private */
    public boolean isNativeValid(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd) {
        if (unifiedNativeAd.getHeadline() == null || unifiedNativeAd.getBody() == null || unifiedNativeAd.getImages() == null || unifiedNativeAd.getImages().size() <= 0 || unifiedNativeAd.getImages().get(0) == null || unifiedNativeAd.getIcon() == null || unifiedNativeAd.getCallToAction() == null) {
            return false;
        }
        return true;
    }
}
