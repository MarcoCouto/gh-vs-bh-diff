package com.appodeal.ads.adapters.tapjoy;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.tapjoy.rewarded_video.TapjoyRewarded;
import com.appodeal.ads.adapters.tapjoy.video.TapjoyVideo;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.tapjoy.TJConnectListener;
import com.tapjoy.TJPlacement;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyLog;

public class TapjoyNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        private final String mediatorName;
        public final String placementName;

        RequestParams(String str, String str2) {
            this.placementName = str;
            this.mediatorName = str2;
        }

        public void applyParams(@NonNull TJPlacement tJPlacement) {
            if (!TextUtils.isEmpty(this.mediatorName)) {
                tJPlacement.setMediationName(this.mediatorName);
                tJPlacement.setAdapterVersion(Appodeal.getVersion());
            }
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "tapjoy";
        }

        public String[] getRequiredPermissions() {
            return new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"};
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.tapjoy.TJContentActivity").build(), new Builder("com.tapjoy.TJAdUnitActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.tapjoy.Tapjoy"};
        }

        public AdNetwork build() {
            return new TapjoyNetwork(this);
        }
    }

    public TapjoyNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new TapjoyVideo();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new TapjoyRewarded();
    }

    public void setLogging(boolean z) {
        TapjoyLog.setDebugEnabled(z);
    }

    public String getVersion() {
        return Tapjoy.getVersion();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull final NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        String string = adUnit.getJsonData().getString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY);
        final String string2 = adUnit.getJsonData().getString("placement");
        final String mediatorName = adUnit.getMediatorName();
        if (TextUtils.isEmpty(string) || TextUtils.isEmpty(string2)) {
            Log.log(LogConstants.KEY_NETWORK, "Error", String.format("missing TapjoyVideo key(%s) or placement(%s)", new Object[]{string, string2}));
            networkInitializationListener.onInitializationFailed(LoadingError.IncorrectAdunit);
            return;
        }
        Tapjoy.setActivity(activity);
        setTargeting(adNetworkMediationParams.getRestrictedData());
        updateConsent(adNetworkMediationParams.getRestrictedData());
        Tapjoy.setDebugEnabled(adNetworkMediationParams.isTestMode());
        if (!Tapjoy.isLimitedConnected()) {
            Tapjoy.limitedConnect(activity.getApplicationContext(), string, new TJConnectListener() {
                public void onConnectSuccess() {
                    try {
                        networkInitializationListener.onInitializationFinished(new RequestParams(string2, mediatorName));
                    } catch (Exception unused) {
                        networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
                    }
                }

                public void onConnectFailure() {
                    networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
                }
            });
        } else {
            networkInitializationListener.onInitializationFinished(new RequestParams(string2, mediatorName));
        }
    }

    @VisibleForTesting
    public void updateConsent(RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            Tapjoy.setUserConsent(restrictedData.isUserHasConsent() ? "1" : "0");
        }
        Tapjoy.subjectToGDPR(restrictedData.isUserInGdprScope());
    }

    private void setTargeting(RestrictedData restrictedData) {
        Tapjoy.belowConsentAge(restrictedData.isUserAgeRestricted());
    }
}
