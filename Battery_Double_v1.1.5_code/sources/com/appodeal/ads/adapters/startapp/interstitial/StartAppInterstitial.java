package com.appodeal.ads.adapters.startapp.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.startapp.StartAppNetwork.RequestParams;
import com.appodeal.ads.adapters.startapp.StartAppUnifiedFullscreenListener;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppAd.AdMode;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.common.model.AdPreferences;

public class StartAppInterstitial extends UnifiedInterstitial<RequestParams> {
    @VisibleForTesting
    StartAppInterstitialListener listener;
    @VisibleForTesting
    StartAppAd startAppAd;

    static final class StartAppInterstitialListener extends StartAppUnifiedFullscreenListener<UnifiedInterstitialCallback> {
        StartAppInterstitialListener(UnifiedInterstitialCallback unifiedInterstitialCallback) {
            super(unifiedInterstitialCallback);
        }

        public void onReceiveAd(Ad ad) {
            processOnReceiveInterstitialAd(ad, false);
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.startAppAd = new StartAppAd(activity);
        this.listener = new StartAppInterstitialListener(unifiedInterstitialCallback);
        this.startAppAd.setVideoListener(this.listener);
        this.startAppAd.loadAd(AdMode.FULLPAGE, requestParams.prepareAdPreferences(activity, new AdPreferences()), this.listener);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.startAppAd == null || !this.startAppAd.isReady()) {
            unifiedInterstitialCallback.onAdShowFailed();
        } else {
            this.startAppAd.showAd((AdDisplayListener) this.listener);
        }
    }

    public void onDestroy() {
        this.startAppAd = null;
        this.listener = null;
    }
}
