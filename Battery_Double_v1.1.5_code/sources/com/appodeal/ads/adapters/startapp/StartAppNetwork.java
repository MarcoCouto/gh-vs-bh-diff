package com.appodeal.ads.adapters.startapp;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LocationData;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.startapp.banner.StartAppBanner;
import com.appodeal.ads.adapters.startapp.interstitial.StartAppInterstitial;
import com.appodeal.ads.adapters.startapp.mrec.StartAppMrec;
import com.appodeal.ads.adapters.startapp.native_ad.StartAppNative;
import com.appodeal.ads.adapters.startapp.rewarded_video.StartAppRewarded;
import com.appodeal.ads.adapters.startapp.video.StartAppVideo;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.github.mikephil.charting.utils.Utils;
import com.startapp.android.publish.GeneratedConstants;
import com.startapp.android.publish.adsCommon.SDKAdPreferences;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;
import com.startapp.android.publish.common.model.AdPreferences;
import java.util.ArrayList;
import java.util.List;

public class StartAppNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        private final AdNetworkMediationParams mediationParams;
        private final double priceFloor;

        RequestParams(AdNetworkMediationParams adNetworkMediationParams, double d) {
            this.mediationParams = adNetworkMediationParams;
            this.priceFloor = d;
        }

        public AdPreferences prepareAdPreferences(@NonNull Context context, @NonNull AdPreferences adPreferences) {
            adPreferences.setTestMode(this.mediationParams.isTestMode());
            if (this.priceFloor > Utils.DOUBLE_EPSILON) {
                adPreferences.setMinCpm(Double.valueOf(this.priceFloor));
            }
            RestrictedData restrictedData = this.mediationParams.getRestrictedData();
            Integer age = restrictedData.getAge();
            if (age != null && age.intValue() > 0) {
                adPreferences.setAge(age);
            }
            Gender gender = restrictedData.getGender();
            if (gender != null) {
                adPreferences.setGender(gender == Gender.MALE ? SDKAdPreferences.Gender.MALE : SDKAdPreferences.Gender.FEMALE);
            }
            LocationData location = restrictedData.getLocation(context);
            Float obtainLatitude = location.obtainLatitude();
            if (obtainLatitude != null) {
                adPreferences.setLatitude((double) obtainLatitude.floatValue());
            }
            Float obtainLongitude = location.obtainLongitude();
            if (obtainLongitude != null) {
                adPreferences.setLongitude((double) obtainLongitude.floatValue());
            }
            return adPreferences;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "startapp";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.startapp.android.publish.adsCommon.activities.FullScreenActivity").build(), new Builder("com.startapp.android.publish.adsCommon.activities.OverlayActivity").build(), new Builder("com.startapp.android.publish.ads.list3d.List3DActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.startapp.android.publish.ads.banner.bannerstandard.BannerStandard", "com.startapp.android.publish.adsCommon.Ad", "com.startapp.android.publish.adsCommon.StartAppAd"};
        }

        public List<Pair<String, Pair<String, String>>> getRequiredServiceWithData() {
            return new ArrayList<Pair<String, Pair<String, String>>>() {
                {
                    add(new Pair("com.startapp.android.publish.common.metaData.PeriodicMetaDataService", null));
                    add(new Pair("com.startapp.android.publish.common.metaData.InfoEventService", null));
                    if (VERSION.SDK_INT >= 21) {
                        add(new Pair("com.startapp.android.publish.common.metaData.PeriodicJobService", null));
                    }
                }
            };
        }

        public String[] getRequiredReceiverClassName() {
            return new String[]{"com.startapp.android.publish.common.metaData.BootCompleteListener"};
        }

        public StartAppNetwork build() {
            return new StartAppNetwork(this);
        }
    }

    public String getVersion() {
        return GeneratedConstants.INAPP_VERSION;
    }

    public StartAppNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new StartAppBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new StartAppMrec();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new StartAppInterstitial();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new StartAppVideo();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new StartAppRewarded();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new StartAppNative();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        String string = adUnit.getJsonData().getString("app_id");
        String optString = adUnit.getJsonData().optString("dev_id", null);
        double optDouble = adUnit.getJsonData().optDouble("pf", Utils.DOUBLE_EPSILON);
        RestrictedData restrictedData = adNetworkMediationParams.getRestrictedData();
        StartAppSDK.init(activity, optString, string, createSdkAdPreferences(restrictedData), false);
        updateConsent(activity, restrictedData);
        StartAppAd.disableSplash();
        networkInitializationListener.onInitializationFinished(new RequestParams(adNetworkMediationParams, optDouble));
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public SDKAdPreferences createSdkAdPreferences(RestrictedData restrictedData) {
        SDKAdPreferences sDKAdPreferences = new SDKAdPreferences();
        Integer age = restrictedData.getAge();
        if (age != null && age.intValue() > 0) {
            sDKAdPreferences.setAge(age.intValue());
        }
        Gender gender = restrictedData.getGender();
        if (gender != null) {
            sDKAdPreferences.setGender(gender == Gender.MALE ? SDKAdPreferences.Gender.MALE : SDKAdPreferences.Gender.FEMALE);
        }
        return sDKAdPreferences;
    }

    @VisibleForTesting
    public void updateConsent(Context context, RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            StartAppSDK.setUserConsent(context, "pas", System.currentTimeMillis(), restrictedData.isUserHasConsent());
        }
    }
}
