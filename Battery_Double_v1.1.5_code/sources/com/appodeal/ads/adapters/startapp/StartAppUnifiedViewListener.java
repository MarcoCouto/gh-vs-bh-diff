package com.appodeal.ads.adapters.startapp;

import android.view.View;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedViewAdCallback;
import com.startapp.android.publish.ads.banner.BannerListener;
import com.startapp.android.publish.ads.banner.bannerstandard.BannerStandard;

public abstract class StartAppUnifiedViewListener<UnifiedAdCallbackType extends UnifiedViewAdCallback> implements BannerListener {
    protected final UnifiedAdCallbackType callback;

    public StartAppUnifiedViewListener(UnifiedAdCallbackType unifiedadcallbacktype) {
        this.callback = unifiedadcallbacktype;
    }

    public void onFailedToReceiveAd(View view) {
        if (view instanceof BannerStandard) {
            this.callback.printError(((BannerStandard) view).getErrorMessage(), null);
        }
        this.callback.onAdLoadFailed(LoadingError.NoFill);
    }

    public void onClick(View view) {
        this.callback.onAdClicked();
    }
}
