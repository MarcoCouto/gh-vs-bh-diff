package com.appodeal.ads.adapters.startapp.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.startapp.StartAppNetwork.RequestParams;
import com.appodeal.ads.adapters.startapp.StartAppUnifiedFullscreenListener;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppAd.AdMode;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.common.model.AdPreferences;

public class StartAppVideo extends UnifiedVideo<RequestParams> {
    @VisibleForTesting
    StartAppVideoListener listener;
    @VisibleForTesting
    StartAppAd startAppAd;

    static final class StartAppVideoListener extends StartAppUnifiedFullscreenListener<UnifiedVideoCallback> {
        StartAppVideoListener(UnifiedVideoCallback unifiedVideoCallback) {
            super(unifiedVideoCallback);
        }

        public void onReceiveAd(Ad ad) {
            processOnReceiveInterstitialAd(ad, true);
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.startAppAd = new StartAppAd(activity);
        this.listener = new StartAppVideoListener(unifiedVideoCallback);
        this.startAppAd.setVideoListener(this.listener);
        this.startAppAd.loadAd(AdMode.VIDEO, requestParams.prepareAdPreferences(activity, new AdPreferences()), this.listener);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.startAppAd == null || !this.startAppAd.isReady()) {
            unifiedVideoCallback.onAdShowFailed();
        } else {
            this.startAppAd.showAd((AdDisplayListener) this.listener);
        }
    }

    public void onDestroy() {
        this.startAppAd = null;
        this.listener = null;
    }
}
