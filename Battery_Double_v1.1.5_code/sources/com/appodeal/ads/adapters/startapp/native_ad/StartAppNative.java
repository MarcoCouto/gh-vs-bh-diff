package com.appodeal.ads.adapters.startapp.native_ad;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.adapters.startapp.StartAppNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.startapp.android.publish.ads.nativead.NativeAdDetails;
import com.startapp.android.publish.ads.nativead.NativeAdDisplayListener;
import com.startapp.android.publish.ads.nativead.NativeAdInterface;
import com.startapp.android.publish.ads.nativead.NativeAdPreferences;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import java.util.ArrayList;

public class StartAppNative extends UnifiedNative<RequestParams> {
    @VisibleForTesting
    com.startapp.android.publish.ads.nativead.StartAppNativeAd startAppNativeAd;

    @VisibleForTesting
    static class StartAppNativeAd extends UnifiedNativeAd {
        private final NativeAdDetails adDetails;
        /* access modifiers changed from: private */
        public final UnifiedNativeCallback callback;

        StartAppNativeAd(NativeAdDetails nativeAdDetails, UnifiedNativeCallback unifiedNativeCallback) {
            super(nativeAdDetails.getTitle(), nativeAdDetails.getDescription(), nativeAdDetails.isApp() ? "Install" : "Learn more", nativeAdDetails.getImageUrl(), nativeAdDetails.getSecondaryImageUrl(), Float.valueOf(nativeAdDetails.getRating()));
            this.adDetails = nativeAdDetails;
            this.callback = unifiedNativeCallback;
        }

        public void onRegisterForInteraction(@NonNull NativeAdView nativeAdView) {
            super.onRegisterForInteraction(nativeAdView);
            this.adDetails.registerViewForInteraction(nativeAdView, nativeAdView.getClickableViews(), new NativeAdDisplayListener() {
                public void adDisplayed(NativeAdInterface nativeAdInterface) {
                }

                public void adHidden(NativeAdInterface nativeAdInterface) {
                }

                public void adNotDisplayed(NativeAdInterface nativeAdInterface) {
                }

                public void adClicked(NativeAdInterface nativeAdInterface) {
                    StartAppNativeAd.this.callback.onAdClicked(StartAppNativeAd.this.getAdId(), (UnifiedAdCallbackClickTrackListener) null);
                }
            });
            this.adDetails.registerViewForInteraction(nativeAdView);
        }

        public void onUnregisterForInteraction() {
            super.onUnregisterForInteraction();
            this.adDetails.unregisterView();
        }
    }

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        if (unifiedNativeParams.getNativeAdType() == NativeAdType.Video) {
            unifiedNativeCallback.onAdLoadFailed(LoadingError.AdTypeNotSupportedInAdapter);
            return;
        }
        NativeAdPreferences createNativeAdPreferences = createNativeAdPreferences(unifiedNativeParams.getAdCountToLoad());
        requestParams.prepareAdPreferences(activity, createNativeAdPreferences);
        this.startAppNativeAd = createStartAppNativeAd(activity);
        this.startAppNativeAd.loadAd(createNativeAdPreferences, createAdEventListener(unifiedNativeCallback));
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public NativeAdPreferences createNativeAdPreferences(int i) {
        NativeAdPreferences nativeAdPreferences = new NativeAdPreferences();
        nativeAdPreferences.setAutoBitmapDownload(false);
        nativeAdPreferences.setAdsNumber(i);
        nativeAdPreferences.setPrimaryImageSize(4);
        nativeAdPreferences.setSecondaryImageSize(2);
        return nativeAdPreferences;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public com.startapp.android.publish.ads.nativead.StartAppNativeAd createStartAppNativeAd(Activity activity) {
        return new com.startapp.android.publish.ads.nativead.StartAppNativeAd(activity);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public AdEventListener createAdEventListener(final UnifiedNativeCallback unifiedNativeCallback) {
        return new AdEventListener() {
            public void onReceiveAd(Ad ad) {
                ArrayList<NativeAdDetails> nativeAds = StartAppNative.this.startAppNativeAd.getNativeAds();
                if (nativeAds == null || nativeAds.size() == 0) {
                    unifiedNativeCallback.onAdLoadFailed(LoadingError.IncorrectCreative);
                    return;
                }
                for (NativeAdDetails startAppNativeAd : nativeAds) {
                    unifiedNativeCallback.onAdLoaded(new StartAppNativeAd(startAppNativeAd, unifiedNativeCallback));
                }
            }

            public void onFailedToReceiveAd(Ad ad) {
                if (ad != null) {
                    unifiedNativeCallback.printError(ad.getErrorMessage(), null);
                }
                unifiedNativeCallback.onAdLoadFailed(LoadingError.NoFill);
            }
        };
    }
}
