package com.appodeal.ads.adapters.startapp;

import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.appodeal.ads.utils.Log;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.VideoListener;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class StartAppUnifiedFullscreenListener<UnifiedAdCallbackType extends UnifiedFullscreenAdCallback> implements AdDisplayListener, AdEventListener, VideoListener {
    private final UnifiedAdCallbackType callback;

    public StartAppUnifiedFullscreenListener(UnifiedAdCallbackType unifiedadcallbacktype) {
        this.callback = unifiedadcallbacktype;
    }

    public void onReceiveAd(Ad ad) {
        this.callback.onAdLoaded();
    }

    /* access modifiers changed from: protected */
    public final void processOnReceiveInterstitialAd(Ad ad, boolean z) {
        try {
            Method declaredMethod = ((StartAppAd) ad).getClass().getDeclaredMethod("getAdHtml", new Class[0]);
            declaredMethod.setAccessible(true);
            Matcher matcher = Pattern.compile("<!-- \\[templateName: (.*?)] -->").matcher((String) declaredMethod.invoke(ad, new Object[0]));
            if (!matcher.find()) {
                this.callback.onAdLoadFailed(LoadingError.NoFill);
            } else if (matcher.group(1).contains("video") ^ z) {
                this.callback.onAdLoadFailed(LoadingError.IncorrectCreative);
            } else {
                this.callback.onAdLoaded();
            }
        } catch (Exception e) {
            Log.log(e);
            this.callback.onAdLoadFailed(LoadingError.InternalError);
        }
    }

    public final void onFailedToReceiveAd(Ad ad) {
        if (ad != null) {
            this.callback.printError(ad.getErrorMessage(), null);
        }
        this.callback.onAdLoadFailed(LoadingError.NoFill);
    }

    public final void adDisplayed(Ad ad) {
        this.callback.onAdShown();
    }

    public final void adNotDisplayed(Ad ad) {
        this.callback.onAdShowFailed();
    }

    public final void adClicked(Ad ad) {
        this.callback.onAdClicked();
    }

    public final void adHidden(Ad ad) {
        this.callback.onAdClosed();
    }

    public void onVideoCompleted() {
        this.callback.onAdFinished();
    }
}
