package com.appodeal.ads.adapters.applovin.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.applovin.adview.AppLovinIncentivizedInterstitial;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.appodeal.ads.adapters.applovin.ApplovinNetwork.RequestParams;
import com.appodeal.ads.adapters.applovin.ApplovinUnifiedListener;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;

public class ApplovinRewarded extends UnifiedRewarded<RequestParams> {
    @VisibleForTesting
    AppLovinIncentivizedInterstitial appLovinAd;
    @VisibleForTesting
    ApplovinRewardedListener listener;

    @VisibleForTesting
    static class ApplovinRewardedListener extends ApplovinUnifiedListener<UnifiedRewardedCallback> implements AppLovinAdDisplayListener, AppLovinAdVideoPlaybackListener {
        public void videoPlaybackBegan(AppLovinAd appLovinAd) {
        }

        ApplovinRewardedListener(UnifiedRewardedCallback unifiedRewardedCallback) {
            super(unifiedRewardedCallback);
        }

        public void adReceived(AppLovinAd appLovinAd) {
            ((UnifiedRewardedCallback) this.callback).onAdLoaded();
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            ((UnifiedRewardedCallback) this.callback).onAdShown();
        }

        public void adHidden(AppLovinAd appLovinAd) {
            ((UnifiedRewardedCallback) this.callback).onAdClosed();
        }

        public void videoPlaybackEnded(AppLovinAd appLovinAd, double d, boolean z) {
            if (z) {
                ((UnifiedRewardedCallback) this.callback).onAdFinished();
            }
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.listener = new ApplovinRewardedListener(unifiedRewardedCallback);
        this.appLovinAd = AppLovinIncentivizedInterstitial.create(requestParams.zoneId, requestParams.sdk);
        this.appLovinAd.preload(this.listener);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.appLovinAd.isAdReadyToDisplay()) {
            this.appLovinAd.show(activity, null, this.listener, this.listener, this.listener);
            return;
        }
        unifiedRewardedCallback.onAdShowFailed();
    }

    public void onDestroy() {
        this.appLovinAd = null;
        this.listener = null;
    }
}
