package com.appodeal.ads.adapters.applovin;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedAdCallback;

public abstract class ApplovinUnifiedListener<UnifiedAdCallbackType extends UnifiedAdCallback> implements AppLovinAdClickListener, AppLovinAdLoadListener {
    protected UnifiedAdCallbackType callback;

    public ApplovinUnifiedListener(UnifiedAdCallbackType unifiedadcallbacktype) {
        this.callback = unifiedadcallbacktype;
    }

    public final void failedToReceiveAd(int i) {
        String str;
        LoadingError loadingError = LoadingError.NoFill;
        if (i == 204) {
            str = "no ad is available";
        } else if (i >= 500) {
            str = "internal server error";
        } else {
            str = "internal errors";
            loadingError = LoadingError.InternalError;
        }
        this.callback.printError(str, Integer.valueOf(i));
        this.callback.onAdLoadFailed(loadingError);
    }

    public final void adClicked(AppLovinAd appLovinAd) {
        this.callback.onAdClicked();
    }
}
