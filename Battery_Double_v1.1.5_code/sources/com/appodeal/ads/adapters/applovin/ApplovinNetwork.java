package com.appodeal.ads.adapters.applovin;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdk.SdkInitializationListener;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.applovin.banner.ApplovinBanner;
import com.appodeal.ads.adapters.applovin.interstitial.ApplovinInterstitial;
import com.appodeal.ads.adapters.applovin.mrec.ApplovinMrec;
import com.appodeal.ads.adapters.applovin.native_ad.ApplovinNative;
import com.appodeal.ads.adapters.applovin.rewarded_video.ApplovinRewarded;
import com.appodeal.ads.adapters.applovin.video.ApplovinVideo;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import org.json.JSONObject;

public class ApplovinNetwork extends AdNetwork<RequestParams> {
    private boolean loggingLevel;

    public static final class RequestParams {
        public JSONObject jsonData;
        @Nullable
        public RestrictedData restrictedData;
        @Nullable
        public AppLovinSdk sdk;
        @Nullable
        public String zoneId;

        RequestParams(@Nullable String str, @Nullable AppLovinSdk appLovinSdk, JSONObject jSONObject) {
            this.zoneId = str;
            this.sdk = appLovinSdk;
            this.jsonData = jSONObject;
        }

        RequestParams(JSONObject jSONObject, @Nullable RestrictedData restrictedData2) {
            this.jsonData = jSONObject;
            this.restrictedData = restrictedData2;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "applovin";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.applovin.adview.AppLovinInterstitialActivity").build(), new Builder("com.applovin.sdk.AppLovinWebViewActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.applovin.sdk.AppLovinSdk", "com.applovin.sdk.AppLovinSdkSettings"};
        }

        public ApplovinNetwork build() {
            return new ApplovinNetwork(this);
        }
    }

    public ApplovinNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new ApplovinBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new ApplovinMrec();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new ApplovinRewarded();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new ApplovinInterstitial();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new ApplovinVideo();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new ApplovinNative();
    }

    public void setLogging(boolean z) {
        this.loggingLevel = z;
    }

    public String getVersion() {
        return AppLovinSdk.VERSION;
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        if (VERSION.SDK_INT < 16) {
            networkInitializationListener.onInitializationFailed(LoadingError.SdkVersionNotSupported);
            return;
        }
        RestrictedData restrictedData = adNetworkMediationParams.getRestrictedData();
        if (adUnit.getJsonData().has("url")) {
            networkInitializationListener.onInitializationFinished(new RequestParams(adUnit.getJsonData(), restrictedData));
        } else {
            String string = adUnit.getJsonData().getString("applovin_key");
            final String optString = adUnit.getJsonData().optString("zone_id");
            AppLovinSdk instance = AppLovinSdk.getInstance(string, new AppLovinSdkSettings(), activity);
            instance.getSettings().setTestAdsEnabled(adNetworkMediationParams.isTestMode());
            instance.getSettings().setVerboseLogging(this.loggingLevel);
            final NetworkInitializationListener<RequestParams> networkInitializationListener2 = networkInitializationListener;
            final AppLovinSdk appLovinSdk = instance;
            final AdUnit adUnit2 = adUnit;
            AnonymousClass1 r4 = new SdkInitializationListener() {
                public void onSdkInitialized(AppLovinSdkConfiguration appLovinSdkConfiguration) {
                    try {
                        networkInitializationListener2.onInitializationFinished(new RequestParams(optString, appLovinSdk, adUnit2.getJsonData()));
                    } catch (Exception unused) {
                        networkInitializationListener2.onInitializationFailed(LoadingError.InternalError);
                    }
                }
            };
            instance.initializeSdk((SdkInitializationListener) r4);
            updateConsent(activity, restrictedData);
            setMediatorName(instance, adUnit.getMediatorName());
            AppLovinPrivacySettings.setIsAgeRestrictedUser(restrictedData.isUserAgeRestricted(), activity);
        }
    }

    @VisibleForTesting
    public void updateConsent(Context context, RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            AppLovinPrivacySettings.setHasUserConsent(restrictedData.isUserHasConsent(), context);
        }
    }

    private void setMediatorName(AppLovinSdk appLovinSdk, @Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            appLovinSdk.setMediationProvider(str);
        }
    }

    public boolean isPermissionRequired(@NonNull String str, AdType adType) {
        return "android.permission.WRITE_EXTERNAL_STORAGE".equals(str) && VERSION.SDK_INT <= 18 && adType != AdType.Native;
    }
}
