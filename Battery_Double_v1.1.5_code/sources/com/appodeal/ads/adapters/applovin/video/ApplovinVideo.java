package com.appodeal.ads.adapters.applovin.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.appodeal.ads.adapters.applovin.ApplovinNetwork.RequestParams;
import com.appodeal.ads.adapters.applovin.ApplovinUnifiedListener;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;

public class ApplovinVideo extends UnifiedVideo<RequestParams> {
    /* access modifiers changed from: private */
    public AppLovinAd appLovinAd;
    private AppLovinSdk appLovinSdk;
    private ApplovinVideoListener listener;

    @VisibleForTesting
    static final class ApplovinVideoListener extends ApplovinUnifiedListener<UnifiedVideoCallback> implements AppLovinAdDisplayListener, AppLovinAdVideoPlaybackListener {
        private final ApplovinVideo adObject;

        public void videoPlaybackBegan(AppLovinAd appLovinAd) {
        }

        ApplovinVideoListener(UnifiedVideoCallback unifiedVideoCallback, ApplovinVideo applovinVideo) {
            super(unifiedVideoCallback);
            this.adObject = applovinVideo;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            this.adObject.appLovinAd = appLovinAd;
            ((UnifiedVideoCallback) this.callback).onAdLoaded();
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            ((UnifiedVideoCallback) this.callback).onAdShown();
        }

        public void adHidden(AppLovinAd appLovinAd) {
            ((UnifiedVideoCallback) this.callback).onAdClosed();
        }

        public void videoPlaybackEnded(AppLovinAd appLovinAd, double d, boolean z) {
            if (z) {
                ((UnifiedVideoCallback) this.callback).onAdFinished();
            }
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.appLovinSdk = requestParams.sdk;
        this.listener = new ApplovinVideoListener(unifiedVideoCallback, this);
        AppLovinAdService adService = this.appLovinSdk.getAdService();
        if (TextUtils.isEmpty(requestParams.zoneId)) {
            adService.loadNextAd(AppLovinAdSize.INTERSTITIAL, this.listener);
        } else {
            adService.loadNextAdForZoneId(requestParams.zoneId, this.listener);
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.appLovinAd != null) {
            AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.appLovinSdk, activity);
            create.setAdDisplayListener(this.listener);
            create.setAdClickListener(this.listener);
            create.setAdVideoPlaybackListener(this.listener);
            create.showAndRender(this.appLovinAd);
            return;
        }
        unifiedVideoCallback.onAdShowFailed();
    }

    public void onDestroy() {
        this.appLovinAd = null;
        this.listener = null;
    }
}
