package com.appodeal.ads.adapters.unityads.banner;

import android.app.Activity;
import android.view.View;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.unityads.ExternalUnityAdsListener;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.unity3d.services.banners.IUnityBannerListener;
import com.unity3d.services.banners.UnityBanners;

class UnityadsBannerListener extends ExternalUnityAdsListener implements IUnityBannerListener {
    private final Activity activity;
    private final UnityadsBanner adObject;
    private final UnifiedBannerCallback callback;
    private boolean isLoaded = false;
    private boolean isShowing = false;

    UnityadsBannerListener(Activity activity2, String str, UnifiedBannerCallback unifiedBannerCallback, UnityadsBanner unityadsBanner) {
        super(str);
        this.activity = activity2;
        this.callback = unifiedBannerCallback;
        this.adObject = unityadsBanner;
    }

    public void onLoadSuccess() {
        if (!this.isLoaded) {
            this.isLoaded = true;
            UnityBanners.setBannerListener(this);
            UnityBanners.loadBanner(this.activity, this.placementId);
        }
    }

    public void onLoadFailed(LoadingError loadingError) {
        if (loadingError != null) {
            this.callback.printError(loadingError.toString(), Integer.valueOf(loadingError.getCode()));
        }
        this.callback.onAdLoadFailed(loadingError);
    }

    public void onShowFailed() {
        this.callback.onAdShowFailed();
    }

    public void onUnityBannerLoaded(String str, View view) {
        this.adObject.handleLoaded(view, this.callback);
    }

    public void onUnityBannerUnloaded(String str) {
        if (this.isShowing) {
            UnityadsNetwork.isBannerShowing = false;
        }
        this.adObject.handleUnloaded();
    }

    public void onUnityBannerShow(String str) {
        this.isShowing = true;
        UnityadsNetwork.isBannerShowing = true;
    }

    public void onUnityBannerClick(String str) {
        this.callback.onAdClicked();
    }

    public void onUnityAdsClick(String str) {
        this.callback.onAdClicked();
    }

    public void onUnityBannerHide(String str) {
        if (this.isShowing) {
            UnityadsNetwork.isBannerShowing = false;
        }
    }

    public void onUnityBannerError(String str) {
        this.callback.onAdLoadFailed(null);
    }
}
