package com.appodeal.ads.adapters.unityads.banner;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.unityads.UnityAdUnit;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.unity3d.services.banners.BannerHide;
import com.unity3d.services.banners.view.BannerView;

public class UnityadsBanner extends UnifiedBanner<RequestParams> implements UnityAdUnit {
    @VisibleForTesting
    ViewGroup bannerView;
    private Context context;
    private boolean isImpressionTracked = false;

    public String getFallbackPlacement() {
        return "";
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        this.context = activity.getApplicationContext();
        final UnityadsBannerListener unityadsBannerListener = new UnityadsBannerListener(activity, requestParams.placementId, unifiedBannerCallback, this);
        new Thread() {
            public void run() {
                super.run();
                try {
                    BannerHide.hide();
                } catch (Exception unused) {
                }
                UnityadsNetwork.load(UnityadsBanner.this, unityadsBannerListener);
            }
        }.start();
    }

    /* access modifiers changed from: 0000 */
    public void handleLoaded(@Nullable View view, @NonNull UnifiedBannerCallback unifiedBannerCallback) {
        if (view != null) {
            this.bannerView = new FrameLayout(view.getContext());
            this.bannerView.addView(view);
            unifiedBannerCallback.onAdLoaded(this.bannerView, -1, view instanceof BannerView ? (int) Math.ceil((double) (((float) view.getLayoutParams().height) / UnifiedAdUtils.getScreenDensity(view.getContext()))) : 50);
            return;
        }
        unifiedBannerCallback.onAdLoadFailed(null);
    }

    /* access modifiers changed from: 0000 */
    public void handleUnloaded() {
        if (this.bannerView != null) {
            this.bannerView.removeAllViews();
            this.bannerView = null;
            UnityadsNetwork.unsubscribeListener(this);
        }
    }

    public void onImpression() {
        super.onImpression();
        if (!this.isImpressionTracked && this.context != null) {
            this.isImpressionTracked = true;
            UnityadsNetwork.trackImpressionAdCount(this.context, 1);
        }
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        if (!this.isImpressionTracked && this.context != null) {
            UnityadsNetwork.trackMissedImpressionOrdinal(this.context, 1);
        }
    }

    public void onDestroy() {
        if (this.bannerView != null) {
            this.bannerView.removeAllViews();
            this.bannerView = null;
        }
        UnityadsNetwork.unsubscribeListener(this);
    }
}
