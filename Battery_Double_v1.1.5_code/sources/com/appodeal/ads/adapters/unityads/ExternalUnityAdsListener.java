package com.appodeal.ads.adapters.unityads;

import com.appodeal.ads.LoadingError;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.UnityAds.FinishState;
import com.unity3d.ads.UnityAds.PlacementState;
import com.unity3d.ads.UnityAds.UnityAdsError;
import com.unity3d.ads.mediation.IUnityAdsExtendedListener;

public abstract class ExternalUnityAdsListener implements IUnityAdsExtendedListener {
    public final String placementId;
    private PlacementState previousPlacementState;

    public abstract void onLoadFailed(LoadingError loadingError);

    public abstract void onLoadSuccess();

    public abstract void onShowFailed();

    public void onUnityAdsFinish(String str, FinishState finishState) {
    }

    public void onUnityAdsPlacementStateChanged(String str, PlacementState placementState, PlacementState placementState2) {
    }

    public void onUnityAdsStart(String str) {
    }

    public ExternalUnityAdsListener(String str) {
        this.placementId = str;
    }

    public void onUnityAdsReady(String str) {
        PlacementState placementState = UnityAds.getPlacementState(str);
        if (this.previousPlacementState != placementState) {
            this.previousPlacementState = placementState;
            switch (placementState) {
                case READY:
                    onLoadSuccess();
                    break;
                case DISABLED:
                case NOT_AVAILABLE:
                    onLoadFailed(LoadingError.IncorrectAdunit);
                    break;
                case NO_FILL:
                    onLoadFailed(LoadingError.NoFill);
                    break;
            }
        }
    }

    public void onUnityAdsError(UnityAdsError unityAdsError, String str) {
        if (unityAdsError == null) {
            onLoadFailed(LoadingError.InternalError);
            return;
        }
        switch (unityAdsError) {
            case SHOW_ERROR:
                onShowFailed();
                return;
            case INVALID_ARGUMENT:
            case INITIALIZE_FAILED:
                onLoadFailed(LoadingError.IncorrectAdunit);
                return;
            default:
                onLoadFailed(LoadingError.InternalError);
                return;
        }
    }
}
