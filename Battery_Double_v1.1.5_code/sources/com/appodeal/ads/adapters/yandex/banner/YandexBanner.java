package com.appodeal.ads.adapters.yandex.banner;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.yandex.YandexNetwork.RequestParams;
import com.appodeal.ads.adapters.yandex.YandexUnifiedViewListener;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.yandex.mobile.ads.AdSize;
import com.yandex.mobile.ads.AdView;

public class YandexBanner extends UnifiedBanner<RequestParams> {
    @Nullable
    private AdView bannerView;

    @VisibleForTesting
    static final class YandexBannerListener extends YandexUnifiedViewListener<UnifiedBannerCallback> {
        private final AdView bannerView;

        YandexBannerListener(UnifiedBannerCallback unifiedBannerCallback, AdView adView) {
            super(unifiedBannerCallback);
            this.bannerView = adView;
        }

        public void onAdLoaded() {
            if (this.bannerView.getAdSize() != null) {
                ((UnifiedBannerCallback) this.callback).onAdLoaded(this.bannerView, this.bannerView.getAdSize().getWidth(), this.bannerView.getAdSize().getHeight());
            } else {
                ((UnifiedBannerCallback) this.callback).onAdLoadFailed(LoadingError.IncorrectAdunit);
            }
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        int optInt = requestParams.jsonData.optInt("width", 728);
        int optInt2 = requestParams.jsonData.optInt("height", 90);
        if (optInt > unifiedBannerParams.getMaxWidth(activity) || optInt2 > unifiedBannerParams.getMaxHeight(activity)) {
            unifiedBannerCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
            return;
        }
        this.bannerView = new AdView(activity);
        this.bannerView.setBlockId(requestParams.yandexKey);
        if (unifiedBannerParams.needLeaderBoard(activity) && optInt == 728 && optInt2 == 90) {
            this.bannerView.setAdSize(AdSize.BANNER_728x90);
        } else if (optInt == 320 && optInt2 == 50) {
            this.bannerView.setAdSize(AdSize.BANNER_320x50);
        } else {
            unifiedBannerCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
            return;
        }
        this.bannerView.setAdEventListener(new YandexBannerListener(unifiedBannerCallback, this.bannerView));
        this.bannerView.setAutoRefreshEnabled(false);
        this.bannerView.loadAd(requestParams.adRequest);
    }

    public void onDestroy() {
        if (this.bannerView != null) {
            this.bannerView.destroy();
            this.bannerView = null;
        }
    }
}
