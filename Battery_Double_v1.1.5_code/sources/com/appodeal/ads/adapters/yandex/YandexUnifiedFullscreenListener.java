package com.appodeal.ads.adapters.yandex;

import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.InterstitialEventListener.SimpleInterstitialEventListener;

public class YandexUnifiedFullscreenListener<UnifiedAdCallbackType extends UnifiedFullscreenAdCallback> extends SimpleInterstitialEventListener {
    protected final UnifiedAdCallbackType callback;

    public YandexUnifiedFullscreenListener(UnifiedAdCallbackType unifiedadcallbacktype) {
        this.callback = unifiedadcallbacktype;
    }

    public void onInterstitialLoaded() {
        this.callback.onAdLoaded();
    }

    public final void onInterstitialFailedToLoad(AdRequestError adRequestError) {
        if (adRequestError != null) {
            this.callback.printError(adRequestError.getDescription(), Integer.valueOf(adRequestError.getCode()));
        }
        this.callback.onAdLoadFailed(YandexNetwork.mapError(adRequestError));
    }

    public void onInterstitialShown() {
        this.callback.onAdShown();
    }

    public void onAdLeftApplication() {
        this.callback.onAdClicked();
    }

    public void onInterstitialDismissed() {
        this.callback.onAdClosed();
    }
}
