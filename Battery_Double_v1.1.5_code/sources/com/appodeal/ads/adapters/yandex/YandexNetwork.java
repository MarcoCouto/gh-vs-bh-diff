package com.appodeal.ads.adapters.yandex;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.yandex.banner.YandexBanner;
import com.appodeal.ads.adapters.yandex.interstitial.YandexInterstitial;
import com.appodeal.ads.adapters.yandex.mrec.YandexMrec;
import com.appodeal.ads.adapters.yandex.video.YandexVideo;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.Version;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.MobileAds;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class YandexNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final AdRequest adRequest;
        public final JSONObject jsonData;
        public final String yandexKey;

        RequestParams(String str, AdRequest adRequest2, JSONObject jSONObject) {
            this.yandexKey = str;
            this.adRequest = adRequest2;
            this.jsonData = jSONObject;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "yandex";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.yandex.mobile.ads.AdActivity").build()};
        }

        public String[] getRequiredReceiverClassName() {
            return new String[]{"com.yandex.metrica.MetricaEventHandler"};
        }

        public List<Pair<String, Pair<String, String>>> getRequiredServiceWithData() {
            return new ArrayList<Pair<String, Pair<String, String>>>() {
                {
                    add(new Pair("com.yandex.metrica.MetricaService", UnifiedAdUtils.isClassAvailable("com.yandex.metrica.YandexMetrica") ? new Pair("metrica:api:level", String.valueOf(YandexMetrica.getLibraryApiLevel())) : null));
                    add(new Pair("com.yandex.metrica.ConfigurationService", null));
                    if (VERSION.SDK_INT >= 26) {
                        add(new Pair("com.yandex.metrica.ConfigurationJobService", null));
                    }
                }
            };
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.yandex.mobile.ads.AdView", "com.yandex.mobile.ads.InterstitialAd", "org.apache.http.HttpResponse"};
        }

        public YandexNetwork build() {
            return new YandexNetwork(this);
        }
    }

    public YandexNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new YandexBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new YandexMrec();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new YandexInterstitial();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new YandexVideo();
    }

    public void setLogging(boolean z) {
        MobileAds.enableLogging(z);
    }

    public String getVersion() {
        StringBuilder sb = new StringBuilder();
        sb.append(MobileAds.getLibraryVersion());
        sb.append("/");
        sb.append(YandexMetrica.getLibraryVersion());
        return sb.toString();
    }

    public boolean worksInM() {
        return UnifiedAdUtils.checkExistApacheLegacyInManifest();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        if (!isValidMetricaVersion()) {
            networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
            return;
        }
        String string = adUnit.getJsonData().getString("metrica_id");
        String string2 = adUnit.getJsonData().getString("block_id");
        YandexMetrica.activate(activity, YandexMetricaConfig.newConfigBuilder(string).build());
        networkInitializationListener.onInitializationFinished(new RequestParams(string2, getAdRequest(activity, adNetworkMediationParams.getRestrictedData()), adUnit.getJsonData()));
    }

    private AdRequest getAdRequest(Context context, RestrictedData restrictedData) {
        AdRequest.Builder builder2 = new AdRequest.Builder();
        updateConsent(restrictedData);
        setTargeting(builder2, context, restrictedData);
        return builder2.build();
    }

    @VisibleForTesting
    public void updateConsent(RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            MobileAds.setUserConsent(restrictedData.isUserHasConsent());
        }
    }

    private void setTargeting(AdRequest.Builder builder2, Context context, RestrictedData restrictedData) {
        if (!restrictedData.isUserAgeRestricted()) {
            if (restrictedData.canSendLocation()) {
                Location deviceLocation = restrictedData.getLocation(context).getDeviceLocation();
                if (deviceLocation != null) {
                    builder2.withLocation(deviceLocation);
                }
            }
            if (restrictedData.canSendUserSettings()) {
                Gender gender = restrictedData.getGender();
                if (gender != null) {
                    switch (gender) {
                        case MALE:
                            builder2.withGender("male");
                            break;
                        case FEMALE:
                            builder2.withGender("female");
                            break;
                    }
                }
                Integer age = restrictedData.getAge();
                if (age != null) {
                    builder2.withAge(String.valueOf(age));
                }
            }
        }
    }

    private boolean isValidMetricaVersion() {
        return new Version(YandexMetrica.getLibraryVersion()).compareTo(new Version("2.40")) > -1;
    }

    @Nullable
    static LoadingError mapError(@Nullable AdRequestError adRequestError) {
        if (adRequestError != null) {
            switch (adRequestError.getCode()) {
                case 1:
                    return LoadingError.InternalError;
                case 2:
                    return LoadingError.IncorrectAdunit;
                case 3:
                    return LoadingError.ConnectionError;
                case 4:
                    return LoadingError.NoFill;
            }
        }
        return null;
    }
}
