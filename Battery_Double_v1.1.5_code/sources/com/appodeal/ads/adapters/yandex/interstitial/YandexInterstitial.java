package com.appodeal.ads.adapters.yandex.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.adapters.yandex.YandexNetwork.RequestParams;
import com.appodeal.ads.adapters.yandex.YandexUnifiedFullscreenListener;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.yandex.mobile.ads.InterstitialAd;

public class YandexInterstitial extends UnifiedInterstitial<RequestParams> {
    @Nullable
    private InterstitialAd interstitialAd;

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.interstitialAd = new InterstitialAd(activity);
        this.interstitialAd.setBlockId(requestParams.yandexKey);
        this.interstitialAd.setInterstitialEventListener(new YandexUnifiedFullscreenListener(unifiedInterstitialCallback));
        this.interstitialAd.loadAd(requestParams.adRequest);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.interstitialAd == null || !this.interstitialAd.isLoaded()) {
            unifiedInterstitialCallback.onAdShowFailed();
        } else {
            this.interstitialAd.show();
        }
    }

    public void onDestroy() {
        if (this.interstitialAd != null) {
            this.interstitialAd.destroy();
            this.interstitialAd = null;
        }
    }
}
