package com.appodeal.ads.adapters.ogury.rewarded_video;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.ogury.OguryNetwork;
import com.appodeal.ads.adapters.ogury.OguryNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import io.presage.common.AdConfig;
import io.presage.common.network.models.RewardItem;
import io.presage.interstitial.optinvideo.PresageOptinVideo;
import io.presage.interstitial.optinvideo.PresageOptinVideoCallback;

public class OguryRewarded extends UnifiedRewarded<RequestParams> {
    private PresageOptinVideo rewardedVideo;

    @VisibleForTesting
    static final class OguryRewardedListener implements PresageOptinVideoCallback {
        private final UnifiedRewardedCallback callback;

        public void onAdAvailable() {
        }

        OguryRewardedListener(UnifiedRewardedCallback unifiedRewardedCallback) {
            this.callback = unifiedRewardedCallback;
        }

        public void onAdNotAvailable() {
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdLoaded() {
            this.callback.onAdLoaded();
        }

        public void onAdNotLoaded() {
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdDisplayed() {
            this.callback.onAdShown();
        }

        public void onAdClosed() {
            this.callback.onAdClosed();
        }

        public void onAdRewarded(RewardItem rewardItem) {
            this.callback.onAdFinished();
        }

        public void onAdError(int i) {
            this.callback.printError(OguryNetwork.mapMessageError(i), Integer.valueOf(i));
            if (i == 4) {
                this.callback.onAdExpired();
            } else {
                this.callback.onAdLoadFailed(OguryNetwork.mapError(i));
            }
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        if (TextUtils.isEmpty(requestParams.adUnitId)) {
            unifiedRewardedCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
            return;
        }
        this.rewardedVideo = new PresageOptinVideo((Context) activity, new AdConfig(requestParams.adUnitId));
        this.rewardedVideo.setOptinVideoCallback(new OguryRewardedListener(unifiedRewardedCallback));
        this.rewardedVideo.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.rewardedVideo == null || !this.rewardedVideo.isLoaded()) {
            unifiedRewardedCallback.onAdShowFailed();
        } else {
            this.rewardedVideo.show();
        }
    }

    public void onDestroy() {
        this.rewardedVideo = null;
    }
}
