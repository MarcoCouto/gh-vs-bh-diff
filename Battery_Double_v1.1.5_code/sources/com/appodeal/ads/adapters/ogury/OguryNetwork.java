package com.appodeal.ads.adapters.ogury;

import android.app.Activity;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.adapters.ogury.interstitial.OguryInterstitial;
import com.appodeal.ads.adapters.ogury.rewarded_video.OguryRewarded;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import io.presage.Presage;
import io.presage.common.PresageSdk;
import java.util.ArrayList;
import java.util.List;

public class OguryNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final String adUnitId;

        RequestParams(String str) {
            this.adUnitId = str;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "ogury";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("io.presage.interstitial.ui.InterstitialActivity").build(), new Builder("io.presage.interstitial.ui.InterstitialAndroid8TransparentActivity").build(), new Builder("io.presage.interstitial.ui.InterstitialAndroid8RotableActivity").build(), new Builder("io.presage.mraid.browser.ShortcutActivity").build(), new Builder("io.presage.mraid.browser.Android8AndLaterShortcutActivity").build()};
        }

        public String[] getRequiredReceiverClassName() {
            return new String[]{"io.presage.core.receiver.AlarmReceiver", "io.presage.common.profig.schedule.ProfigAlarmReceiver"};
        }

        public List<Pair<String, Pair<String, String>>> getRequiredServiceWithData() {
            return new ArrayList<Pair<String, Pair<String, String>>>() {
                {
                    add(new Pair("io.presage.core.service.SdkService", null));
                    add(new Pair("io.presage.common.profig.schedule.ProfigSyncIntentService", null));
                    if (VERSION.SDK_INT >= 21) {
                        add(new Pair("io.presage.common.profig.schedule.ProfigJobService", null));
                    }
                    if (VERSION.SDK_INT >= 26) {
                        add(new Pair("io.presage.core.service.SdkJobService", null));
                        add(new Pair("io.presage.core.service.SMJobService", null));
                    }
                }
            };
        }

        public String[] getRequiredClasses() {
            return new String[]{"io.presage.core.receiver.AlarmReceiver", "io.presage.common.profig.schedule.ProfigAlarmReceiver"};
        }

        public OguryNetwork build() {
            return new OguryNetwork(this);
        }
    }

    @Nullable
    public static String mapMessageError(int i) {
        switch (i) {
            case 0:
                return "load failed";
            case 1:
                return "phone not connected to internet";
            case 2:
                return "ad disabled";
            case 3:
                return "various error (configuration file not synced)";
            case 4:
                return "ad expires in 4 hours if it was not shown";
            case 5:
                return "start method not called";
            default:
                return null;
        }
    }

    public OguryNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new OguryInterstitial();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new OguryRewarded();
    }

    public String getVersion() {
        return PresageSdk.getAdsSdkVersion();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        Presage.getInstance().start(adUnit.getJsonData().getString("ogury_key"), activity);
        networkInitializationListener.onInitializationFinished(new RequestParams(UnifiedAdUtils.getStringOrNullFromJson(adUnit.getJsonData(), "ogury_adunitid")));
    }

    @Nullable
    public static LoadingError mapError(int i) {
        if (i != 5) {
            switch (i) {
                case 0:
                case 2:
                    return LoadingError.NoFill;
                case 1:
                    return LoadingError.ConnectionError;
                case 3:
                    break;
                default:
                    return null;
            }
        }
        return LoadingError.InternalError;
    }
}
