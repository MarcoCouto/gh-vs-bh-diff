package com.appodeal.ads.adapters.mraid;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.mraid.banner.MraidBanner;
import com.appodeal.ads.adapters.mraid.interstitial.MraidInterstitial;
import com.appodeal.ads.adapters.mraid.mrec.MraidMrec;
import com.appodeal.ads.adapters.mraid.rewarded_video.MraidRewarded;
import com.appodeal.ads.adapters.mraid.video.MraidVideo;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.explorestack.iab.BuildConfig;
import com.explorestack.iab.IabSettings;
import com.explorestack.iab.mraid.internal.MRAIDLog;
import com.explorestack.iab.mraid.internal.MRAIDLog.LOG_LEVEL;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public class MraidNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams extends UnifiedMraidNetworkParams {
        public final JSONObject jsonData;

        RequestParams(RestrictedData restrictedData, JSONObject jSONObject, String str, String str2, String str3, String str4, long j, boolean z, boolean z2, boolean z3, int i, int i2) {
            super(restrictedData, str, str2, str3, str4, j, z, z2, z3, i, i2, -1);
            this.jsonData = jSONObject;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return CampaignEx.JSON_KEY_MRAID;
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.explorestack.iab.mraid.activity.MraidActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.explorestack.iab.mraid.MRAIDView", "com.explorestack.iab.mraid.MRAIDInterstitial"};
        }

        public MraidNetwork build() {
            return new MraidNetwork(this);
        }
    }

    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public MraidNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
        IabSettings.mediatorVersion = Appodeal.getVersion();
    }

    public void setLogging(boolean z) {
        if (z) {
            MRAIDLog.setLoggingLevel(LOG_LEVEL.verbose);
        } else {
            MRAIDLog.setLoggingLevel(LOG_LEVEL.none);
        }
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new MraidBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new MraidMrec();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new MraidInterstitial();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new MraidVideo();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new MraidRewarded();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        String optString = adUnit.getJsonData().optString("package");
        String optString2 = adUnit.getJsonData().optString("base_url", null);
        long optLong = adUnit.getJsonData().optLong("expiry");
        boolean optBoolean = adUnit.getJsonData().optBoolean("preload", true);
        boolean optBoolean2 = adUnit.getJsonData().optBoolean("tag");
        boolean optBoolean3 = adUnit.getJsonData().optBoolean("use_layout", true);
        String optString3 = adUnit.getJsonData().optString(String.HTML);
        String optString4 = adUnit.getJsonData().optString("mraid_url");
        if (adUnit.getJsonData().optBoolean(String.TOP, false)) {
            optString4 = UnifiedAdUtils.parseUrlWithTopParams(activity, optString4, adNetworkMediationParams);
        } else {
            AdNetworkMediationParams adNetworkMediationParams2 = adNetworkMediationParams;
        }
        RequestParams requestParams = new RequestParams(adNetworkMediationParams.getRestrictedData(), adUnit.getJsonData(), optString, optString2, optString3, optString4, optLong, optBoolean, optBoolean2, optBoolean3, Integer.parseInt(adUnit.getJsonData().getString("width")), Integer.parseInt(adUnit.getJsonData().getString("height")));
        networkInitializationListener.onInitializationFinished(requestParams);
    }
}
