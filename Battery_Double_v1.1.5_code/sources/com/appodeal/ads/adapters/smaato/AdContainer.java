package com.appodeal.ads.adapters.smaato;

import android.support.annotation.Nullable;

public class AdContainer<T> {
    private T ad;
    private boolean isShown;

    /* access modifiers changed from: protected */
    public void setAd(T t) {
        this.ad = t;
    }

    @Nullable
    public T getAd() {
        return this.ad;
    }

    public void setShown(boolean z) {
        this.isShown = z;
    }

    public boolean isShown() {
        return this.isShown;
    }

    public void destroy() {
        this.ad = null;
    }
}
