package com.appodeal.ads.adapters.smaato;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.smaato.banner.SmaatoBanner;
import com.appodeal.ads.adapters.smaato.interstitial.SmaatoInterstitial;
import com.appodeal.ads.adapters.smaato.mrec.SmaatoMrec;
import com.appodeal.ads.adapters.smaato.rewarded_video.SmaatoRewarded;
import com.appodeal.ads.adapters.smaato.video.SmaatoVideo;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.smaato.sdk.core.Config;
import com.smaato.sdk.core.Config.ConfigBuilder;
import com.smaato.sdk.core.LatLng;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.log.LogLevel;

public class SmaatoNetwork extends AdNetwork<RequestParams> {
    private static boolean isLoggingEnabled = false;

    public static final class RequestParams {
        public final String adSpaceId;
        public final String adapterVersion;
        public final String mediatorName;

        RequestParams(String str, String str2, String str3) {
            this.adSpaceId = str;
            this.mediatorName = str2;
            this.adapterVersion = str3;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "smaato";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.smaato.sdk.core.browser.SmaatoSdkBrowserActivity").build(), new Builder("com.smaato.sdk.interstitial.InterstitialAdActivity").build(), new Builder("com.smaato.sdk.rewarded.widget.RewardedInterstitialAdActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.smaato.sdk.core.SmaatoSdk", "com.smaato.sdk.banner.widget.BannerView", "com.smaato.sdk.interstitial.Interstitial", "com.smaato.sdk.rewarded.RewardedInterstitial"};
        }

        public String[] getRequiredProvidersClassName() {
            return new String[]{"com.smaato.sdk.core.lifecycle.ProcessLifecycleOwnerInitializer"};
        }

        public SmaatoNetwork build() {
            return new SmaatoNetwork(this);
        }
    }

    public boolean canLoadInterstitialWhenDisplaying() {
        return false;
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return false;
    }

    public boolean canLoadVideoWhenDisplaying() {
        return false;
    }

    public SmaatoNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new SmaatoBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new SmaatoMrec();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new SmaatoInterstitial();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new SmaatoVideo();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new SmaatoRewarded();
    }

    public String getVersion() {
        return SmaatoSdk.getVersion();
    }

    public void setLogging(boolean z) {
        isLoggingEnabled = z;
    }

    public LoadingError verifyLoadAvailability(@NonNull AdType adType) {
        if ((adType == AdType.Interstitial || adType == AdType.Video) && (isInterstitialShowing() || isVideoShowing())) {
            return LoadingError.Canceled;
        }
        return super.verifyLoadAvailability(adType);
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        if (VERSION.SDK_INT < 16) {
            networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
            return;
        }
        String string = adUnit.getJsonData().getString("publisher_id");
        String string2 = adUnit.getJsonData().getString("ad_space_id");
        ConfigBuilder builder2 = Config.builder();
        if (isLoggingEnabled) {
            builder2.setLogLevel(LogLevel.DEBUG);
        }
        SmaatoSdk.init(activity.getApplication(), builder2.build(), string);
        setTargeting(activity, adNetworkMediationParams.getRestrictedData());
        networkInitializationListener.onInitializationFinished(new RequestParams(string2, adUnit.getMediatorName(), getAdapterVersion()));
    }

    private void setTargeting(Context context, RestrictedData restrictedData) {
        SmaatoSdk.setCoppa(restrictedData.isUserAgeRestricted());
        Gender gender = restrictedData.getGender();
        if (gender != null) {
            switch (gender) {
                case MALE:
                    SmaatoSdk.setGender(com.smaato.sdk.core.Gender.MALE);
                    break;
                case FEMALE:
                    SmaatoSdk.setGender(com.smaato.sdk.core.Gender.FEMALE);
                    break;
                default:
                    SmaatoSdk.setGender(com.smaato.sdk.core.Gender.OTHER);
                    break;
            }
        }
        Integer age = restrictedData.getAge();
        if (age != null) {
            SmaatoSdk.setAge(age);
        }
        Location obtainLocation = restrictedData.getLocation(context).obtainLocation();
        if (obtainLocation != null) {
            SmaatoSdk.setLatLng(new LatLng(obtainLocation.getLatitude(), obtainLocation.getLongitude()));
        }
        String zip = restrictedData.getZip();
        if (zip != null) {
            SmaatoSdk.setZip(zip);
        }
    }
}
