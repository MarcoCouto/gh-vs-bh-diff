package com.appodeal.ads.adapters.smaato.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.adapters.smaato.InterstitialAdContainer;
import com.appodeal.ads.adapters.smaato.SmaatoNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.smaato.sdk.interstitial.Interstitial;
import com.smaato.sdk.interstitial.InterstitialAd;

public class SmaatoInterstitial extends UnifiedInterstitial<RequestParams> {
    @VisibleForTesting
    InterstitialAdContainer interstitialAdContainer;

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.interstitialAdContainer = new InterstitialAdContainer(unifiedInterstitialCallback);
        if (!TextUtils.isEmpty(requestParams.mediatorName)) {
            Interstitial.setMediationNetworkName(requestParams.mediatorName);
            Interstitial.setMediationNetworkSDKVersion(Appodeal.getVersion());
            Interstitial.setMediationAdapterVersion(requestParams.adapterVersion);
        }
        Interstitial.loadAd(requestParams.adSpaceId, this.interstitialAdContainer);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        InterstitialAd interstitialAd = this.interstitialAdContainer != null ? (InterstitialAd) this.interstitialAdContainer.getAd() : null;
        if (interstitialAd == null || !interstitialAd.isAvailableForPresentation()) {
            unifiedInterstitialCallback.onAdShowFailed();
            return;
        }
        this.interstitialAdContainer.setShown(true);
        interstitialAd.showAd(activity);
    }

    public void onDestroy() {
        if (this.interstitialAdContainer != null) {
            this.interstitialAdContainer.destroy();
            this.interstitialAdContainer = null;
        }
    }
}
