package com.appodeal.ads.adapters.smaato;

import android.support.annotation.NonNull;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedViewAdCallback;
import com.smaato.sdk.banner.widget.BannerError;
import com.smaato.sdk.banner.widget.BannerView;
import com.smaato.sdk.banner.widget.BannerView.EventListener;

public abstract class AdViewListener<Callback extends UnifiedViewAdCallback> implements EventListener {
    protected final Callback callback;

    public void onAdImpression(@NonNull BannerView bannerView) {
    }

    protected AdViewListener(Callback callback2) {
        this.callback = callback2;
    }

    public void onAdFailedToLoad(@NonNull BannerView bannerView, @NonNull BannerError bannerError) {
        if (bannerError != null) {
            this.callback.printError(bannerError.toString(), null);
            switch (bannerError) {
                case NETWORK_ERROR:
                    this.callback.onAdLoadFailed(LoadingError.ConnectionError);
                    return;
                case INVALID_REQUEST:
                    this.callback.onAdLoadFailed(LoadingError.IncorrectAdunit);
                    return;
                case AD_UNLOADED:
                case CREATIVE_RESOURCE_EXPIRED:
                case INTERNAL_ERROR:
                    this.callback.onAdLoadFailed(LoadingError.InternalError);
                    return;
                default:
                    this.callback.onAdLoadFailed(LoadingError.NoFill);
                    return;
            }
        } else {
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }
    }

    public void onAdClicked(@NonNull BannerView bannerView) {
        this.callback.onAdClicked();
    }

    public void onAdTTLExpired(@NonNull BannerView bannerView) {
        this.callback.onAdExpired();
    }
}
