package com.appodeal.ads.adapters.smaato.mrec;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.adapters.smaato.AdViewListener;
import com.appodeal.ads.adapters.smaato.SmaatoNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.smaato.sdk.banner.ad.AutoReloadInterval;
import com.smaato.sdk.banner.ad.BannerAdSize;
import com.smaato.sdk.banner.widget.BannerView;

public class SmaatoMrec extends UnifiedMrec<RequestParams> {
    @VisibleForTesting
    BannerView bannerView;

    @VisibleForTesting
    static final class Listener extends AdViewListener<UnifiedMrecCallback> {
        Listener(UnifiedMrecCallback unifiedMrecCallback) {
            super(unifiedMrecCallback);
        }

        public void onAdLoaded(@NonNull BannerView bannerView) {
            ((UnifiedMrecCallback) this.callback).onAdLoaded(bannerView);
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        this.bannerView = new BannerView(activity);
        this.bannerView.setEventListener(new Listener(unifiedMrecCallback));
        this.bannerView.setAutoReloadInterval(AutoReloadInterval.DISABLED);
        if (!TextUtils.isEmpty(requestParams.mediatorName)) {
            this.bannerView.setMediationNetworkName(requestParams.mediatorName);
            this.bannerView.setMediationNetworkSDKVersion(Appodeal.getVersion());
            this.bannerView.setMediationAdapterVersion(requestParams.adapterVersion);
        }
        this.bannerView.loadAd(requestParams.adSpaceId, BannerAdSize.MEDIUM_RECTANGLE_300x250);
    }

    public void onDestroy() {
        if (this.bannerView != null) {
            this.bannerView.destroy();
            this.bannerView.setEventListener(null);
            this.bannerView = null;
        }
    }
}
