package com.appodeal.ads.adapters.smaato.banner;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.adapters.smaato.AdViewListener;
import com.appodeal.ads.adapters.smaato.SmaatoNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.smaato.sdk.banner.ad.AutoReloadInterval;
import com.smaato.sdk.banner.ad.BannerAdSize;
import com.smaato.sdk.banner.widget.BannerView;

public class SmaatoBanner extends UnifiedBanner<RequestParams> {
    @VisibleForTesting
    BannerView bannerView;

    @VisibleForTesting
    static final class Listener extends AdViewListener<UnifiedBannerCallback> {
        private final int height;

        Listener(UnifiedBannerCallback unifiedBannerCallback, int i) {
            super(unifiedBannerCallback);
            this.height = i;
        }

        public void onAdLoaded(@NonNull BannerView bannerView) {
            ((UnifiedBannerCallback) this.callback).onAdLoaded(bannerView, -1, this.height);
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        BannerAdSize bannerAdSize;
        if (unifiedBannerParams.needLeaderBoard(activity)) {
            bannerAdSize = BannerAdSize.LEADERBOARD_728x90;
        } else {
            bannerAdSize = BannerAdSize.XX_LARGE_320x50;
        }
        this.bannerView = new BannerView(activity);
        this.bannerView.setEventListener(new Listener(unifiedBannerCallback, bannerAdSize.adDimension.getHeight()));
        this.bannerView.setAutoReloadInterval(AutoReloadInterval.DISABLED);
        if (!TextUtils.isEmpty(requestParams.mediatorName)) {
            this.bannerView.setMediationNetworkName(requestParams.mediatorName);
            this.bannerView.setMediationNetworkSDKVersion(Appodeal.getVersion());
            this.bannerView.setMediationAdapterVersion(requestParams.adapterVersion);
        }
        this.bannerView.loadAd(requestParams.adSpaceId, bannerAdSize);
    }

    public void onDestroy() {
        if (this.bannerView != null) {
            this.bannerView.destroy();
            this.bannerView.setEventListener(null);
            this.bannerView = null;
        }
    }
}
