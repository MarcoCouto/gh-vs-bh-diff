package com.appodeal.ads.adapters.smaato;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.smaato.sdk.interstitial.EventListener;
import com.smaato.sdk.interstitial.InterstitialAd;
import com.smaato.sdk.interstitial.InterstitialError;
import com.smaato.sdk.interstitial.InterstitialRequestError;

public class InterstitialAdContainer extends AdContainer<InterstitialAd> implements EventListener {
    private final UnifiedFullscreenAdCallback callback;

    public void onAdImpression(@NonNull InterstitialAd interstitialAd) {
    }

    public InterstitialAdContainer(@NonNull UnifiedFullscreenAdCallback unifiedFullscreenAdCallback) {
        this.callback = unifiedFullscreenAdCallback;
    }

    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
        setAd(interstitialAd);
        this.callback.onAdLoaded();
    }

    public void onAdFailedToLoad(@NonNull InterstitialRequestError interstitialRequestError) {
        onFailedToLoad(interstitialRequestError != null ? interstitialRequestError.getInterstitialError() : null);
    }

    public void onAdError(@NonNull InterstitialAd interstitialAd, @NonNull InterstitialError interstitialError) {
        if (isShown()) {
            if (interstitialError != null) {
                this.callback.printError(interstitialError.toString(), null);
            }
            this.callback.onAdShowFailed();
            return;
        }
        onFailedToLoad(interstitialError);
    }

    public void onAdOpened(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdShown();
    }

    public void onAdClosed(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdClosed();
    }

    public void onAdClicked(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdClicked();
    }

    public void onAdTTLExpired(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdExpired();
    }

    private void onFailedToLoad(@Nullable InterstitialError interstitialError) {
        if (interstitialError != null) {
            this.callback.printError(interstitialError.toString(), null);
            switch (interstitialError) {
                case NETWORK_ERROR:
                    this.callback.onAdLoadFailed(LoadingError.ConnectionError);
                    return;
                case INVALID_REQUEST:
                    this.callback.onAdLoadFailed(LoadingError.IncorrectAdunit);
                    return;
                case AD_UNLOADED:
                case CREATIVE_RESOURCE_EXPIRED:
                case INTERNAL_ERROR:
                    this.callback.onAdLoadFailed(LoadingError.InternalError);
                    return;
                default:
                    this.callback.onAdLoadFailed(LoadingError.NoFill);
                    return;
            }
        } else {
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }
    }
}
