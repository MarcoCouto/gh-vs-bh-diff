package com.appodeal.ads.adapters.adcolony;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAdOptions;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyUserMetadata;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.adcolony.rewarded_video.AdcolonyRewarded;
import com.appodeal.ads.adapters.adcolony.video.AdcolonyVideo;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.Log;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdcolonyV3Network extends AdNetwork<RequestParams> {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static boolean isInitialized = false;
    private static boolean isInitializing = false;

    public static final class RequestParams {
        public final AdColonyAdOptions adOptions;
        public final String zoneId;

        RequestParams(String str, AdColonyAdOptions adColonyAdOptions) {
            this.zoneId = str;
            this.adOptions = adColonyAdOptions;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "adcolony";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.adcolony.sdk.AdColonyInterstitialActivity").build(), new Builder("com.adcolony.sdk.AdColonyAdViewActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.adcolony.sdk.AdColony", "com.adcolony.sdk.AdColonyInterstitial", "com.adcolony.sdk.AdColonyAdViewActivity"};
        }

        public String[] getRequiredPermissions() {
            return new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"};
        }

        public AdcolonyV3Network build() {
            return new AdcolonyV3Network(this);
        }
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return false;
    }

    public boolean canLoadVideoWhenDisplaying() {
        return false;
    }

    public AdcolonyV3Network(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new AdcolonyRewarded();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new AdcolonyVideo();
    }

    public String getVersion() {
        if (isInitialized) {
            return AdColony.getSDKVersion();
        }
        try {
            Class cls = Class.forName("com.adcolony.sdk.h");
            Field declaredField = cls.getDeclaredField("a");
            declaredField.setAccessible(true);
            return (String) declaredField.get(cls);
        } catch (Exception e) {
            Log.log(e);
            return "unknown";
        }
    }

    public LoadingError verifyLoadAvailability(@NonNull AdType adType) {
        if ((adType == AdType.Video || adType == AdType.Rewarded) && (isVideoShowing() || isRewardedShowing())) {
            return LoadingError.Canceled;
        }
        return super.verifyLoadAvailability(adType);
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        String string = adUnit.getJsonData().getString("zone_id");
        JSONObject jSONObject = adUnit.getJsonData().getJSONObject("zones");
        String string2 = adUnit.getJsonData().getString("store");
        String string3 = adUnit.getJsonData().getString("app_id");
        String optString = adUnit.getJsonData().optString("consent_string");
        AdColonyAppOptions appOptions = AdColony.getAppOptions();
        if (appOptions == null) {
            appOptions = new AdColonyAppOptions();
        }
        appOptions.setOriginStore(string2);
        updateConsent(adNetworkMediationParams.getRestrictedData(), appOptions, optString);
        setMediatorName(appOptions, adUnit.getMediatorName());
        if (isInitialized) {
            AdColony.setAppOptions(appOptions);
            networkInitializationListener.onInitializationFinished(new RequestParams(string, setTargeting(activity, adNetworkMediationParams.getRestrictedData())));
        } else if (isInitializing) {
            networkInitializationListener.onInitializationFinished(new RequestParams(string, setTargeting(activity, adNetworkMediationParams.getRestrictedData())));
        } else {
            try {
                appOptions.setAppVersion(activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName);
            } catch (NameNotFoundException e) {
                Log.log(e);
            }
            isInitializing = true;
            AdColony.configure(activity, appOptions, string3, getZones(jSONObject, string));
            isInitialized = true;
            isInitializing = false;
            networkInitializationListener.onInitializationFinished(new RequestParams(string, setTargeting(activity, adNetworkMediationParams.getRestrictedData())));
        }
    }

    @VisibleForTesting
    public void updateConsent(RestrictedData restrictedData, AdColonyAppOptions adColonyAppOptions, @Nullable String str) {
        if (!restrictedData.isUserInGdprScope()) {
            return;
        }
        if (!TextUtils.isEmpty(str)) {
            adColonyAppOptions.setGDPRConsentString(str);
            adColonyAppOptions.setGDPRRequired(true);
            return;
        }
        adColonyAppOptions.setOption("explicit_consent_given", true);
        adColonyAppOptions.setOption("consent_response", restrictedData.isUserHasConsent());
    }

    private void setMediatorName(AdColonyAppOptions adColonyAppOptions, @Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            adColonyAppOptions.setMediationNetwork(str, Appodeal.getVersion());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    private String[] getZones(JSONObject jSONObject, String str) {
        String[] strArr;
        if (jSONObject != null && jSONObject.length() > 0) {
            try {
                ArrayList arrayList = new ArrayList();
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    JSONArray jSONArray = jSONObject.getJSONArray((String) keys.next());
                    for (int i = 0; i < jSONArray.length(); i++) {
                        arrayList.add(jSONArray.getString(i));
                    }
                }
                strArr = (String[]) arrayList.toArray(new String[0]);
            } catch (Exception e) {
                Log.log(e);
            }
            if (strArr == null) {
                return strArr;
            }
            return new String[]{str};
        }
        strArr = null;
        if (strArr == null) {
        }
    }

    private AdColonyAdOptions setTargeting(Context context, RestrictedData restrictedData) {
        AdColonyUserMetadata adColonyUserMetadata = new AdColonyUserMetadata();
        Integer age = restrictedData.getAge();
        if (age != null) {
            adColonyUserMetadata.setUserAge(age.intValue());
        }
        Gender gender = restrictedData.getGender();
        if (gender != null) {
            switch (gender) {
                case MALE:
                    adColonyUserMetadata.setUserGender("male");
                    break;
                case FEMALE:
                    adColonyUserMetadata.setUserGender("female");
                    break;
            }
        }
        String zip = restrictedData.getZip();
        if (zip != null) {
            adColonyUserMetadata.setUserZipCode(zip);
        }
        Location deviceLocation = restrictedData.getLocation(context).getDeviceLocation();
        if (deviceLocation != null) {
            adColonyUserMetadata.setUserLocation(deviceLocation);
        }
        return new AdColonyAdOptions().setUserMetadata(adColonyUserMetadata);
    }
}
