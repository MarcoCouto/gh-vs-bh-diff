package com.appodeal.ads.adapters.adcolony.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyInterstitial;
import com.appodeal.ads.adapters.adcolony.AdcolonyV3Network.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;

public class AdcolonyVideo extends UnifiedVideo<RequestParams> {
    AdColonyInterstitial interstitial;

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        AdColony.requestInterstitial(requestParams.zoneId, new AdcolonyVideoListener(unifiedVideoCallback, this), requestParams.adOptions);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.interstitial == null || this.interstitial.isExpired()) {
            unifiedVideoCallback.onAdShowFailed();
        } else {
            this.interstitial.show();
        }
    }

    public void onDestroy() {
        if (this.interstitial != null) {
            this.interstitial.setListener(null);
            this.interstitial.destroy();
            this.interstitial = null;
        }
    }
}
