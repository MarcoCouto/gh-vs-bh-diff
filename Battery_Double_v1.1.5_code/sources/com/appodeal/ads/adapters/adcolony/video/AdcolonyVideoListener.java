package com.appodeal.ads.adapters.adcolony.video;

import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedVideoCallback;

class AdcolonyVideoListener extends AdColonyInterstitialListener {
    private final AdcolonyVideo adObject;
    private final UnifiedVideoCallback callback;

    AdcolonyVideoListener(UnifiedVideoCallback unifiedVideoCallback, AdcolonyVideo adcolonyVideo) {
        this.callback = unifiedVideoCallback;
        this.adObject = adcolonyVideo;
    }

    public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
        this.adObject.interstitial = adColonyInterstitial;
        this.callback.onAdLoaded();
    }

    public void onRequestNotFilled(AdColonyZone adColonyZone) {
        if (adColonyZone != null) {
            UnifiedVideoCallback unifiedVideoCallback = this.callback;
            StringBuilder sb = new StringBuilder();
            sb.append("request not filled for zoneId: ");
            sb.append(adColonyZone.getZoneID());
            sb.append(", isValid zone: ");
            sb.append(adColonyZone.isValid());
            unifiedVideoCallback.printError(sb.toString(), null);
        }
        this.callback.onAdLoadFailed(LoadingError.NoFill);
    }

    public void onOpened(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onAdShown();
    }

    public void onClicked(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onAdClicked();
    }

    public void onClosed(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onAdClosed();
    }

    public void onExpiring(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onAdExpired();
    }
}
