package com.appodeal.ads.adapters.inneractive.native_ad;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inneractive.InnerActiveAdTask;
import com.appodeal.ads.adapters.inneractive.InnerActiveNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InnerActiveNative extends UnifiedNative<RequestParams> {

    @VisibleForTesting
    static class InnerActiveUnifiedNativeAd extends UnifiedNativeAd {
        InnerActiveUnifiedNativeAd(String str, String str2, String str3, String str4, String str5, Float f, String str6, @Nullable String str7, @Nullable List<String> list) {
            super(str, str2, str3, str4, str5, f);
            setVastVideoTag(str6);
            setClickUrl(str7);
            setImpressionNotifyUrls(list);
        }
    }

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull final UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        InnerActiveAdTask.request(activity, requestParams.url, requestParams.restrictedData, new Callback<String>() {
            public void onSuccess(@NonNull Context context, String str) {
                try {
                    unifiedNativeCallback.onAdLoaded(InnerActiveNative.this.createInnerActiveNativeAd(str));
                } catch (JSONException unused) {
                    unifiedNativeCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
                }
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedNativeCallback.onAdLoadFailed(loadingError);
            }
        }, Integer.valueOf(requestParams.speedLimit));
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ed  */
    public InnerActiveUnifiedNativeAd createInnerActiveNativeAd(@NonNull String str) throws JSONException {
        String str2;
        JSONObject jSONObject = new JSONObject(str).getJSONObject("native");
        JSONArray jSONArray = jSONObject.getJSONArray("assets");
        ArrayList arrayList = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        Float f = null;
        String str8 = null;
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            if (jSONObject2.optInt("id", 0) != 0) {
                int optInt = jSONObject2.optInt("id");
                if (optInt == 4) {
                    str7 = jSONObject2.optJSONObject("img").optString("url");
                } else if (optInt == 6) {
                    str6 = jSONObject2.optJSONObject("img").optString("url");
                } else if (optInt == 12) {
                    str4 = jSONObject2.optJSONObject("data").optString("value");
                } else if (optInt == 22) {
                    str5 = jSONObject2.optJSONObject("data").optString("value");
                } else if (optInt != 32) {
                    switch (optInt) {
                        case 1:
                            str3 = jSONObject2.optJSONObject("title").optString(MimeTypes.BASE_TYPE_TEXT);
                            break;
                        case 2:
                            str8 = jSONObject2.optJSONObject("video").optString("vasttag");
                            break;
                    }
                } else {
                    Double valueOf = Double.valueOf(jSONObject2.optJSONObject("data").optDouble("value", Utils.DOUBLE_EPSILON));
                    if (valueOf.doubleValue() > Utils.DOUBLE_EPSILON) {
                        f = Float.valueOf(valueOf.floatValue());
                    }
                }
            }
        }
        if (jSONObject.getJSONObject("link") != null) {
            String string = jSONObject.getJSONObject("link").getString("url");
            if (!TextUtils.isEmpty(string)) {
                str2 = string;
                if (jSONObject.getJSONArray("imptrackers") != null) {
                    arrayList = new ArrayList();
                    JSONArray jSONArray2 = jSONObject.getJSONArray("imptrackers");
                    for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                        if (!jSONArray2.isNull(i2)) {
                            String optString = jSONArray2.optString(i2);
                            if (!TextUtils.isEmpty(optString) && TextUtils.getTrimmedLength(optString) != 0) {
                                arrayList.add(optString);
                            }
                        }
                    }
                }
                InnerActiveUnifiedNativeAd innerActiveUnifiedNativeAd = new InnerActiveUnifiedNativeAd(str3, str4, str5, str6, str7, f, str8, str2, arrayList);
                return innerActiveUnifiedNativeAd;
            }
        }
        str2 = null;
        if (jSONObject.getJSONArray("imptrackers") != null) {
        }
        InnerActiveUnifiedNativeAd innerActiveUnifiedNativeAd2 = new InnerActiveUnifiedNativeAd(str3, str4, str5, str6, str7, f, str8, str2, arrayList);
        return innerActiveUnifiedNativeAd2;
    }
}
