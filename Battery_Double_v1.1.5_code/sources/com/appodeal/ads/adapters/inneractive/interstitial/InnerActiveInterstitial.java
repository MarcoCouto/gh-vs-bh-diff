package com.appodeal.ads.adapters.inneractive.interstitial;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inneractive.InnerActiveAdTask;
import com.appodeal.ads.adapters.inneractive.InnerActiveNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidInterstitial;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams.Builder;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public class InnerActiveInterstitial extends UnifiedMraidInterstitial<RequestParams> {
    private Integer speedLimit;

    @Nullable
    public UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.speedLimit = Integer.valueOf(requestParams.speedLimit);
        return requestParams.toMraidNetworkParamsBuilder().setWidth(ModuleDescriptor.MODULE_VERSION).setHeight(480).build();
    }

    public void requestMraid(@NonNull Context context, @NonNull final UnifiedInterstitialParams unifiedInterstitialParams, @NonNull final UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull final UnifiedInterstitialCallback unifiedInterstitialCallback, @NonNull String str) {
        InnerActiveAdTask.request(context, str, unifiedMraidNetworkParams.restrictedData, new Callback<String>() {
            public void onSuccess(@NonNull Context context, String str) {
                InnerActiveInterstitial.this.loadMraid(context, unifiedInterstitialParams, new Builder(unifiedMraidNetworkParams).setAdm(str).build(), unifiedInterstitialCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedInterstitialCallback.onAdLoadFailed(loadingError);
            }
        }, this.speedLimit);
    }
}
