package com.appodeal.ads.adapters.inneractive;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.ConnectionData;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.inneractive.banner.InnerActiveBanner;
import com.appodeal.ads.adapters.inneractive.interstitial.InnerActiveInterstitial;
import com.appodeal.ads.adapters.inneractive.mrec.InnerActiveMrec;
import com.appodeal.ads.adapters.inneractive.native_ad.InnerActiveNative;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams.Builder;
import com.appodeal.ads.utils.ActivityRule;
import com.explorestack.iab.BuildConfig;
import org.json.JSONObject;

public class InnerActiveNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final String baseUrl;
        public final JSONObject jsonData;
        public final RestrictedData restrictedData;
        public final int speedLimit;
        public final String url;

        RequestParams(RestrictedData restrictedData2, JSONObject jSONObject, String str, String str2, int i) {
            this.restrictedData = restrictedData2;
            this.jsonData = jSONObject;
            this.baseUrl = str;
            this.url = str2;
            this.speedLimit = i;
        }

        public Builder toMraidNetworkParamsBuilder() {
            return new Builder(this.restrictedData).setBaseUrl(this.baseUrl).setAdUrl(this.url).setUseLayout(true);
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "inner-active";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new ActivityRule.Builder("com.explorestack.iab.mraid.activity.MraidActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.explorestack.iab.mraid.MRAIDView", "com.explorestack.iab.mraid.MRAIDInterstitial"};
        }

        public InnerActiveNetwork build() {
            return new InnerActiveNetwork(this);
        }
    }

    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public InnerActiveNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new InnerActiveBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new InnerActiveMrec();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new InnerActiveInterstitial();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new InnerActiveNative();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        int optInt = adUnit.getJsonData().optInt("speed_limit", 100);
        ConnectionData connectionData = UnifiedAdUtils.getConnectionData(activity);
        if (optInt != -1 || connectionData.isFast) {
            RequestParams requestParams = new RequestParams(adNetworkMediationParams.getRestrictedData(), adUnit.getJsonData(), adUnit.getJsonData().optString("base_url", null), adUnit.getJsonData().getString("url"), optInt);
            networkInitializationListener.onInitializationFinished(requestParams);
            return;
        }
        networkInitializationListener.onInitializationFailed(LoadingError.Canceled);
    }
}
