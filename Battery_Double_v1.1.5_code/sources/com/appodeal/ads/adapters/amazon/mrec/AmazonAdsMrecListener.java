package com.appodeal.ads.adapters.amazon.mrec;

import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdListener;
import com.amazon.device.ads.AdProperties;
import com.appodeal.ads.adapters.amazon.AmazonAdsNetwork;
import com.appodeal.ads.unified.UnifiedMrecCallback;

class AmazonAdsMrecListener implements AdListener {
    private AdLayout adView;
    private UnifiedMrecCallback callback;

    public void onAdCollapsed(Ad ad) {
    }

    public void onAdDismissed(Ad ad) {
    }

    public void onAdExpanded(Ad ad) {
    }

    AmazonAdsMrecListener(AdLayout adLayout, UnifiedMrecCallback unifiedMrecCallback) {
        this.adView = adLayout;
        this.callback = unifiedMrecCallback;
    }

    public void onAdLoaded(Ad ad, AdProperties adProperties) {
        this.callback.onAdLoaded(this.adView);
    }

    public void onAdFailedToLoad(Ad ad, AdError adError) {
        if (adError != null) {
            this.callback.printError(adError.getMessage(), adError.getCode());
        }
        this.callback.onAdLoadFailed(AmazonAdsNetwork.mapError(adError));
    }
}
