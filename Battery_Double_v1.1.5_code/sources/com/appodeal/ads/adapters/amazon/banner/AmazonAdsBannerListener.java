package com.appodeal.ads.adapters.amazon.banner;

import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdListener;
import com.amazon.device.ads.AdProperties;
import com.appodeal.ads.adapters.amazon.AmazonAdsNetwork;
import com.appodeal.ads.unified.UnifiedBannerCallback;

class AmazonAdsBannerListener implements AdListener {
    private AdLayout adView;
    private UnifiedBannerCallback callback;
    private int targetHeight;

    public void onAdCollapsed(Ad ad) {
    }

    public void onAdDismissed(Ad ad) {
    }

    public void onAdExpanded(Ad ad) {
    }

    AmazonAdsBannerListener(AdLayout adLayout, UnifiedBannerCallback unifiedBannerCallback, int i) {
        this.adView = adLayout;
        this.callback = unifiedBannerCallback;
        this.targetHeight = i;
    }

    public void onAdLoaded(Ad ad, AdProperties adProperties) {
        this.callback.onAdLoaded(this.adView, -1, this.targetHeight);
    }

    public void onAdFailedToLoad(Ad ad, AdError adError) {
        if (adError != null) {
            this.callback.printError(adError.getMessage(), adError.getCode());
        }
        this.callback.onAdLoadFailed(AmazonAdsNetwork.mapError(adError));
    }
}
