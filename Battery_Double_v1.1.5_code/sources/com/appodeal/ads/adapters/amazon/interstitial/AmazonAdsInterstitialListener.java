package com.appodeal.ads.adapters.amazon.interstitial;

import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdListener;
import com.amazon.device.ads.AdProperties;
import com.appodeal.ads.adapters.amazon.AmazonAdsNetwork;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;

class AmazonAdsInterstitialListener implements AdListener {
    private final UnifiedInterstitialCallback callback;

    public void onAdCollapsed(Ad ad) {
    }

    public void onAdExpanded(Ad ad) {
    }

    AmazonAdsInterstitialListener(UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.callback = unifiedInterstitialCallback;
    }

    public void onAdLoaded(Ad ad, AdProperties adProperties) {
        this.callback.onAdLoaded();
    }

    public void onAdFailedToLoad(Ad ad, AdError adError) {
        if (adError != null) {
            this.callback.printError(adError.getMessage(), adError.getCode());
        }
        this.callback.onAdLoadFailed(AmazonAdsNetwork.mapError(adError));
    }

    public void onAdDismissed(Ad ad) {
        this.callback.onAdClosed();
    }
}
