package com.appodeal.ads.adapters.amazon.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.amazon.device.ads.InterstitialAd;
import com.appodeal.ads.adapters.amazon.AmazonAdsNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;

public class AmazonAdsInterstitial extends UnifiedInterstitial<RequestParams> {
    private InterstitialAd interstitialAd;

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.interstitialAd = new InterstitialAd(activity);
        this.interstitialAd.setListener(new AmazonAdsInterstitialListener(unifiedInterstitialCallback));
        requestParams.targetingOptions.setAdvancedOption("enableVideoAds", "false");
        this.interstitialAd.loadAd(requestParams.targetingOptions);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.interstitialAd == null || !this.interstitialAd.isReady() || !this.interstitialAd.showAd()) {
            unifiedInterstitialCallback.onAdShowFailed();
        } else {
            unifiedInterstitialCallback.onAdShown();
        }
    }

    public void onDestroy() {
        this.interstitialAd = null;
    }
}
