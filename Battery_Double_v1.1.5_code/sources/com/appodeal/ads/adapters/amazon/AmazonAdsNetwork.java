package com.appodeal.ads.adapters.amazon;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdRegistration;
import com.amazon.device.ads.AdTargetingOptions;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.adapters.amazon.banner.AmazonAdsBanner;
import com.appodeal.ads.adapters.amazon.interstitial.AmazonAdsInterstitial;
import com.appodeal.ads.adapters.amazon.mrec.AmazonAdsMrec;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.Log;
import java.lang.reflect.Field;

public class AmazonAdsNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final AdTargetingOptions targetingOptions;

        RequestParams(AdTargetingOptions adTargetingOptions) {
            this.targetingOptions = adTargetingOptions;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "amazon_ads";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder(AdUtils.REQUIRED_ACTIVITY).build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.amazon.device.ads.AdLayout", "com.amazon.device.ads.AdRegistration"};
        }

        public AmazonAdsNetwork build() {
            return new AmazonAdsNetwork(this);
        }
    }

    public AmazonAdsNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new AmazonAdsBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new AmazonAdsMrec();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new AmazonAdsInterstitial();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        AdRegistration.setAppKey(adUnit.getJsonData().getString("amazon_key"));
        if (adNetworkMediationParams.isTestMode()) {
            AdRegistration.enableTesting(true);
        }
        networkInitializationListener.onInitializationFinished(new RequestParams(new AdTargetingOptions().enableGeoLocation(adNetworkMediationParams.getRestrictedData().canSendLocation())));
    }

    public void setLogging(boolean z) {
        AdRegistration.enableLogging(z);
    }

    public String getVersion() {
        String str = "unknown";
        try {
            Class cls = Class.forName("com.amazon.device.ads.Version");
            Field declaredField = cls.getDeclaredField("buildVersion");
            declaredField.setAccessible(true);
            return (String) declaredField.get(cls);
        } catch (Exception e) {
            Log.log(e);
            return str;
        }
    }

    @Nullable
    public static LoadingError mapError(@Nullable AdError adError) {
        if (adError != null) {
            switch (adError.getCode()) {
                case INTERNAL_ERROR:
                    return LoadingError.InternalError;
                case NETWORK_TIMEOUT:
                    return LoadingError.TimeoutError;
                case NO_FILL:
                    return LoadingError.NoFill;
            }
        }
        return null;
    }
}
