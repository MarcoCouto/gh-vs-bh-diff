package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

class bn {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public static final String[] f1607a = {"appodeal", "appodealx", CampaignEx.JSON_KEY_MRAID, "vast", "nast"};
    private static boolean b = false;

    private static final class a implements Runnable {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private Context f1608a;

        a(@NonNull Context context) {
            this.f1608a = context;
        }

        private boolean a(String[] strArr, String str) {
            for (String str2 : strArr) {
                if (str2 == str || (str2 != null && str2.equals(str))) {
                    return true;
                }
            }
            return false;
        }

        public void run() {
            HashSet hashSet;
            String[] a2;
            if (bn.f1607a.length > 0) {
                String[] strArr = new String[0];
                try {
                    strArr = this.f1608a.getAssets().list("apd_adapters");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (strArr == null || strArr.length == 0) {
                    hashSet = new HashSet();
                    hashSet.addAll(Arrays.asList(bn.f1607a));
                } else {
                    hashSet = null;
                    for (String str : bn.f1607a) {
                        if (!a(strArr, String.format("%s.apdnetwork", new Object[]{str}))) {
                            if (hashSet == null) {
                                hashSet = new HashSet();
                            }
                            hashSet.add(str.toUpperCase());
                        }
                    }
                }
                if (hashSet != null) {
                    Log.e("Appodeal", String.format(" \n\nATTENTION:\n\tAdapters are not registered in you app: \n\t\t%s.\n\tPlease add the dependencies for them to improve your fill rates and increase revenue.\n\tIf you are sure that you do not need these adapters, skip this warning.\n ", new Object[]{TextUtils.join(", ", hashSet)}));
                }
            }
        }
    }

    static void a(@NonNull Context context) {
        if (!b) {
            b = true;
            new Thread(new a(context)).start();
        }
    }
}
