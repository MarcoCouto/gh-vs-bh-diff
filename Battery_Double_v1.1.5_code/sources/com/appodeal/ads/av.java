package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;

class av extends c<ax, aw, Object> {

    /* renamed from: a reason: collision with root package name */
    private MrecCallbacks f1562a;

    av() {
    }

    /* access modifiers changed from: 0000 */
    public void a(MrecCallbacks mrecCallbacks) {
        this.f1562a = mrecCallbacks;
    }

    public void a(@NonNull ax axVar, @NonNull aw awVar) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_LOADED, String.format("isPrecache: %s", new Object[]{Boolean.valueOf(awVar.isPrecache())}), LogLevel.verbose);
        Appodeal.b();
        if (this.f1562a != null) {
            this.f1562a.onMrecLoaded(awVar.isPrecache());
        }
    }

    public void a(@Nullable ax axVar, @Nullable aw awVar, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_LOAD_FAILED, LogLevel.verbose);
        if (this.f1562a != null) {
            this.f1562a.onMrecFailedToLoad();
        }
    }

    public void a(@Nullable ax axVar, @Nullable aw awVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_SHOWN, LogLevel.verbose);
        if (this.f1562a != null) {
            this.f1562a.onMrecShown();
        }
    }

    public void a(@Nullable ax axVar, @Nullable aw awVar, @Nullable Object obj, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_SHOW_FAILED, LogLevel.verbose);
        if (this.f1562a != null) {
            this.f1562a.onMrecShowFailed();
        }
    }

    public void b(@NonNull ax axVar, @NonNull aw awVar) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_EXPIRED, LogLevel.verbose);
        if (this.f1562a != null) {
            this.f1562a.onMrecExpired();
        }
    }

    public void b(@NonNull ax axVar, @NonNull aw awVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_CLICKED, LogLevel.verbose);
        if (this.f1562a != null) {
            this.f1562a.onMrecClicked();
        }
    }
}
