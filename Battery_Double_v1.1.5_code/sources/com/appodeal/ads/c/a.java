package com.appodeal.ads.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.m;
import java.util.List;
import org.json.JSONObject;

public interface a {
    @NonNull
    List<JSONObject> a();

    void a(@Nullable m mVar);

    @NonNull
    List<JSONObject> b();

    @NonNull
    List<JSONObject> c();
}
