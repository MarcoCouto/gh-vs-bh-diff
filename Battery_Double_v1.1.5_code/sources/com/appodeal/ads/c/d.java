package com.appodeal.ads.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.br;
import com.appodeal.ads.h;
import com.appodeal.ads.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

class d extends g {

    /* renamed from: a reason: collision with root package name */
    private int f1628a;

    d(int i) {
        this.f1628a = i;
    }

    public void a(@NonNull f fVar, @NonNull List<JSONObject> list, @Nullable m mVar) {
        h a2 = h.a(this.f1628a);
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            JSONObject jSONObject = (JSONObject) it.next();
            if (jSONObject != null && jSONObject.optBoolean("parallel_bidding")) {
                String a3 = br.a(jSONObject, "status");
                if (!TextUtils.isEmpty(a3) && a2.c(a3) != null) {
                    arrayList.add(jSONObject);
                }
                it.remove();
            }
        }
        fVar.c(arrayList);
    }
}
