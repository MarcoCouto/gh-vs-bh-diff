package com.appodeal.ads.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.i;
import com.appodeal.ads.m;
import com.appodeal.ads.t;
import com.appodeal.ads.utils.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

class c extends g {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, Integer> f1627a = new HashMap();
    private final Map<String, Integer> b = new HashMap();
    private final Map<String, Integer> c = new HashMap();
    private final Map<String, Integer> d = new HashMap();

    c(JSONArray jSONArray) {
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject optJSONObject = jSONArray.optJSONObject(i);
                if (optJSONObject != null && optJSONObject.has("name") && optJSONObject.has("max_requests") && optJSONObject.has("max_requests_pf") && optJSONObject.has("max_nofills")) {
                    String optString = optJSONObject.optString("name");
                    if (!TextUtils.isEmpty(optString)) {
                        this.f1627a.put(optString, Integer.valueOf(optJSONObject.optInt("max_requests", Integer.MAX_VALUE)));
                        this.b.put(optString, Integer.valueOf(optJSONObject.optInt("max_requests_pf", Integer.MAX_VALUE)));
                        this.c.put(optString, Integer.valueOf(optJSONObject.optInt("max_nofills", Integer.MAX_VALUE)));
                    }
                }
            }
        }
    }

    private <T> int a(Map<T, Integer> map, T t) {
        Integer num = (Integer) map.get(t);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    private <T extends i> JSONArray a(List<T> list) {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            jSONArray.put(((i) list.get(i)).getId());
        }
        return jSONArray;
    }

    private <T extends i> boolean b(List<T> list) {
        for (int i = 0; i < list.size(); i++) {
            if (((i) list.get(i)).getRequestResult() == t.Successful) {
                return true;
            }
        }
        return false;
    }

    private void c(List<JSONObject> list) {
        if (!this.d.isEmpty()) {
            HashMap hashMap = new HashMap();
            HashMap hashMap2 = new HashMap();
            HashMap hashMap3 = new HashMap();
            HashMap hashMap4 = new HashMap();
            for (Entry entry : this.c.entrySet()) {
                hashMap.put(entry.getKey(), Integer.valueOf(0));
                hashMap2.put(entry.getKey(), Integer.valueOf(0));
                hashMap3.put(entry.getKey(), new ArrayList());
                hashMap4.put(entry.getKey(), new ArrayList());
            }
            for (JSONObject jSONObject : list) {
                String optString = jSONObject.optString("status");
                String optString2 = jSONObject.optString("id");
                boolean optBoolean = jSONObject.optBoolean("cap", false);
                if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && this.c.containsKey(optString)) {
                    if (a(this.d, optString2) >= a(this.c, optString)) {
                        if (optBoolean) {
                            List list2 = (List) hashMap3.get(optString);
                            list2.add(optString2);
                            hashMap3.put(optString, list2);
                        } else {
                            List list3 = (List) hashMap4.get(optString);
                            list3.add(optString2);
                            hashMap4.put(optString, list3);
                        }
                    } else if (optBoolean) {
                        hashMap.put(optString, Integer.valueOf(a(hashMap, optString) + 1));
                    } else {
                        hashMap2.put(optString, Integer.valueOf(a(hashMap2, optString) + 1));
                    }
                }
            }
            for (Entry entry2 : hashMap.entrySet()) {
                if (((Integer) entry2.getValue()).intValue() == 0) {
                    List list4 = (List) hashMap3.get(entry2.getKey());
                    if (list4 != null && !list4.isEmpty()) {
                        this.d.keySet().removeAll(list4);
                    }
                }
            }
        }
    }

    private <T extends i> void d(List<T> list) {
        JSONArray a2 = a(list);
        if (a2 != null) {
            int length = a2.length();
            boolean b2 = b(list);
            if (b2) {
                length--;
            }
            for (int i = 0; i < length; i++) {
                String optString = a2.optString(i);
                if (!TextUtils.isEmpty(optString)) {
                    this.d.put(optString, Integer.valueOf((this.d.containsKey(optString) ? a(this.d, optString) : 0) + 1));
                }
            }
            if (b2) {
                String optString2 = a2.optString(a2.length() - 1);
                if (!TextUtils.isEmpty(optString2)) {
                    this.d.remove(optString2);
                }
            }
        }
    }

    public void a(@NonNull f fVar, @NonNull List<JSONObject> list, @Nullable m mVar) {
        if (mVar != null) {
            d(mVar.k());
            d(mVar.l());
        }
        b.a(list);
        c(list);
        Iterator it = list.iterator();
        HashMap hashMap = new HashMap(this.f1627a);
        HashMap hashMap2 = new HashMap(this.b);
        while (it.hasNext()) {
            JSONObject jSONObject = (JSONObject) it.next();
            String optString = jSONObject.optString("status");
            String optString2 = jSONObject.optString("id");
            boolean optBoolean = jSONObject.optBoolean("cap", false);
            if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && this.c.containsKey(optString)) {
                int a2 = a(this.c, optString);
                int a3 = a(this.d, optString2);
                if (!optBoolean || a3 < a2) {
                    if (optBoolean && hashMap2.containsKey(optString)) {
                        int a4 = a(hashMap2, optString);
                        if (a4 > 0) {
                            hashMap2.put(optString, Integer.valueOf(a4 - 1));
                        }
                    } else if (!optBoolean && hashMap.containsKey(optString)) {
                        int a5 = a(hashMap, optString);
                        if (a5 > 0) {
                            hashMap.put(optString, Integer.valueOf(a5 - 1));
                        }
                    }
                }
                it.remove();
            }
        }
    }
}
