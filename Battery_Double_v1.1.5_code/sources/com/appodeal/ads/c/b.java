package com.appodeal.ads.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.br;
import com.appodeal.ads.m;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class b extends f implements a {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    final List<g> f1626a = new ArrayList();
    private f f;
    private int g;

    public b(@NonNull JSONObject jSONObject, int i) {
        a(jSONObject);
        JSONArray optJSONArray = jSONObject.optJSONArray("networks");
        this.g = i;
        this.f1626a.add(new e(i));
        this.f1626a.add(new c(optJSONArray));
        this.f1626a.add(new d(i));
        this.f = d();
    }

    @NonNull
    public List<JSONObject> a() {
        return this.f.b;
    }

    public void a(@Nullable m mVar) {
        this.f = d();
        for (g a2 : this.f1626a) {
            a2.a(this.f, this.f.e, mVar);
        }
        this.f.e();
        br.a(this.g, (a) this);
    }

    @NonNull
    public List<JSONObject> b() {
        return this.f.c;
    }

    @NonNull
    public List<JSONObject> c() {
        return this.f.d;
    }
}
