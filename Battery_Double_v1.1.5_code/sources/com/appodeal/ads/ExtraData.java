package com.appodeal.ads;

import android.text.TextUtils;
import com.appodeal.ads.utils.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class ExtraData {
    public static final String APPSFLYER_ID = "appsflyer_id";

    /* renamed from: a reason: collision with root package name */
    private static JSONObject f1495a = new JSONObject();

    static JSONObject a() {
        return f1495a;
    }

    static void a(String str, Object obj) {
        if (!TextUtils.isEmpty(str) && obj != null) {
            try {
                f1495a.put(str, obj);
            } catch (JSONException e) {
                Log.log(e);
            }
        }
    }
}
