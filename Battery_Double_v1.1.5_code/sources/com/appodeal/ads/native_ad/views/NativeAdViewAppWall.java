package com.appodeal.ads.native_ad.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.appodeal.ads.NativeAd;
import com.appodeal.ads.NativeIconView;
import com.appodeal.ads.br;

public class NativeAdViewAppWall extends a {
    final int B = 5;
    final int C = 20;
    final int D = 70;
    final int E = 10;
    final int F = 5;
    final int G = 5;
    final int H = 16;
    final int I = 12;
    final int J = 10;
    final int K = 3;
    final int L = 5;

    public NativeAdViewAppWall(Context context) {
        super(context);
    }

    public NativeAdViewAppWall(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeAdViewAppWall(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public NativeAdViewAppWall(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    public NativeAdViewAppWall(Context context, NativeAd nativeAd) {
        super(context, nativeAd, "default");
    }

    public NativeAdViewAppWall(Context context, NativeAd nativeAd, String str) {
        super(context, nativeAd, str);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        LinearLayout linearLayout;
        int i;
        NativeIconView nativeIconView;
        int i2;
        View view;
        int i3;
        View view2;
        int i4;
        View view3;
        int i5;
        View view4;
        int i6;
        if (!this.n) {
            TypedArray obtainStyledAttributes = this.m.obtainStyledAttributes(new int[]{16843534});
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            if (VERSION.SDK_INT >= 16) {
                setBackground(drawable);
            } else {
                setBackgroundDrawable(drawable);
            }
            this.h = new RelativeLayout(this.m);
            this.h.setLayoutParams(new LayoutParams(-1, -1));
            int round = Math.round(br.h(this.m) * 5.0f);
            this.h.setPadding(round, round, round, round);
            this.h.setVisibility(8);
            addView(this.h);
            this.k = new LinearLayout(this.m);
            this.k.setOrientation(0);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(11);
            layoutParams.addRule(10);
            this.k.setLayoutParams(layoutParams);
            if (VERSION.SDK_INT >= 17) {
                linearLayout = this.k;
                i = View.generateViewId();
            } else {
                linearLayout = this.k;
                i = 77;
            }
            linearLayout.setId(i);
            this.h.addView(this.k);
            this.j = new TextView(this.m);
            this.j.setTextSize(2, 10.0f);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.gravity = 16;
            this.j.setLayoutParams(layoutParams2);
            c();
            this.k.addView(this.j);
            this.i = new RelativeLayout(this.m);
            this.i.setLayoutParams(new LayoutParams(-2, Math.round(br.h(this.m) * 20.0f)));
            this.k.addView(this.i);
            this.f = new NativeIconView(this.m);
            this.o = Math.round(br.h(this.m) * 70.0f);
            LayoutParams layoutParams3 = new LayoutParams(this.o, this.o);
            layoutParams3.setMargins(0, 0, Math.round(br.h(this.m) * 10.0f), 0);
            layoutParams3.addRule(10);
            layoutParams3.addRule(9);
            this.f.setLayoutParams(layoutParams3);
            if (VERSION.SDK_INT >= 17) {
                nativeIconView = this.f;
                i2 = View.generateViewId();
            } else {
                nativeIconView = this.f;
                i2 = 71;
            }
            nativeIconView.setId(i2);
            this.h.addView(this.f);
            this.f1498a = new TextView(this.m);
            ((TextView) this.f1498a).setTextSize(2, 16.0f);
            LayoutParams layoutParams4 = new LayoutParams(-2, -2);
            layoutParams4.setMargins(0, 0, 0, Math.round(br.h(this.m) * 5.0f));
            layoutParams4.addRule(1, this.f.getId());
            layoutParams4.addRule(0, this.k.getId());
            this.f1498a.setLayoutParams(layoutParams4);
            if (VERSION.SDK_INT >= 17) {
                view = this.f1498a;
                i3 = View.generateViewId();
            } else {
                view = this.f1498a;
                i3 = 72;
            }
            view.setId(i3);
            this.h.addView(this.f1498a);
            this.b = new TextView(this.m);
            LayoutParams layoutParams5 = new LayoutParams(-2, -2);
            int round2 = Math.round(br.h(this.m) * 3.0f);
            int round3 = Math.round(br.h(this.m) * 5.0f);
            layoutParams5.setMargins(round2, 0, 3, 3);
            layoutParams5.addRule(11);
            layoutParams5.addRule(15);
            this.b.setLayoutParams(layoutParams5);
            this.b.setPadding(round3, round3, round3, round3);
            if (VERSION.SDK_INT >= 17) {
                view2 = this.b;
                i4 = View.generateViewId();
            } else {
                view2 = this.b;
                i4 = 75;
            }
            view2.setId(i4);
            d();
            this.h.addView(this.b);
            this.d = new TextView(this.m);
            ((TextView) this.d).setTextSize(2, 12.0f);
            LayoutParams layoutParams6 = new LayoutParams(-2, -2);
            layoutParams6.setMargins(0, 0, 0, Math.round(br.h(this.m) * 5.0f));
            layoutParams6.addRule(1, this.f.getId());
            layoutParams6.addRule(0, this.b.getId());
            layoutParams6.addRule(3, this.f1498a.getId());
            this.d.setLayoutParams(layoutParams6);
            ((TextView) this.d).setEllipsize(TruncateAt.END);
            ((TextView) this.d).setMaxLines(3);
            ((TextView) this.d).setMinLines(3);
            if (VERSION.SDK_INT >= 17) {
                view3 = this.d;
                i5 = View.generateViewId();
            } else {
                view3 = this.d;
                i5 = 73;
            }
            view3.setId(i5);
            this.h.addView(this.d);
            this.c = new RatingBar(this.m, null, 16842877);
            this.c.setVisibility(8);
            LayoutParams layoutParams7 = new LayoutParams(-2, -2);
            layoutParams7.addRule(1, this.f.getId());
            layoutParams7.addRule(3, this.d.getId());
            this.c.setLayoutParams(layoutParams7);
            if (VERSION.SDK_INT >= 17) {
                view4 = this.c;
                i6 = View.generateViewId();
            } else {
                view4 = this.c;
                i6 = 74;
            }
            view4.setId(i6);
            this.h.addView(this.c);
            this.n = true;
        }
        b();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (this.l != null) {
            ((TextView) this.f1498a).setText(this.l.getTitle());
            ((TextView) this.d).setText(this.l.getDescription());
            if (this.l.getRating() > 0.0f) {
                ((RatingBar) this.c).setRating(this.l.getRating());
                this.c.setVisibility(0);
            } else {
                this.c.setVisibility(8);
            }
            if (this.l.getCallToAction() == null || this.l.getCallToAction().isEmpty() || this.l.getCallToAction().equals("")) {
                this.b.setVisibility(8);
            } else {
                ((TextView) this.b).setText(this.l.getCallToAction());
                this.b.setVisibility(0);
            }
            this.e = this.l.getProviderView(this.m);
            if (this.e != null) {
                if (this.e.getParent() != null && (this.e.getParent() instanceof ViewGroup)) {
                    ((ViewGroup) this.e.getParent()).removeView(this.e);
                }
                this.i.removeAllViews();
                this.i.addView(this.e, new ViewGroup.LayoutParams(-2, -2));
            } else if (this.i != null) {
                this.i.setVisibility(8);
            }
            registerView(this.l, this.s);
            this.h.setVisibility(0);
        }
    }
}
