package com.appodeal.ads.native_ad.views;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appodeal.ads.NativeAd;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.NativeIconView;
import com.appodeal.ads.NativeMediaView;

public abstract class a extends NativeAdView {
    final int A = 78;
    RelativeLayout h;
    RelativeLayout i;
    TextView j;
    LinearLayout k;
    NativeAd l;
    Context m;
    boolean n = false;
    int o;
    boolean p = false;
    int q = 0;
    int r = 100;
    protected String s = "default";
    final int t = 71;
    final int u = 72;
    final int v = 73;
    final int w = 74;
    final int x = 75;
    final int y = 76;
    final int z = 77;

    public a(Context context) {
        super(context);
        this.m = context;
        a();
    }

    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.m = context;
        a();
    }

    public a(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.m = context;
        a();
    }

    @TargetApi(21)
    public a(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        this.m = context;
        a();
    }

    public a(Context context, NativeAd nativeAd, String str) {
        super(context);
        this.m = context;
        this.l = nativeAd;
        this.s = str;
        a();
    }

    /* access modifiers changed from: 0000 */
    public void a() {
    }

    /* access modifiers changed from: 0000 */
    public void b() {
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"SetTextI18n"})
    public void c() {
        TextView textView;
        int i2;
        if (this.j != null) {
            if (this.p) {
                this.j.setText("Sponsored");
                this.j.setBackgroundColor(0);
                textView = this.j;
                i2 = -3355444;
            } else {
                this.j.setText(" Ad ");
                this.j.setBackgroundColor(Color.parseColor("#fcb41c"));
                textView = this.j;
                i2 = -1;
            }
            textView.setTextColor(i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(0);
        gradientDrawable.setColor(0);
        gradientDrawable.setStroke(2, ((TextView) this.b).getCurrentTextColor());
        gradientDrawable.setCornerRadius(8.0f);
        if (VERSION.SDK_INT >= 16) {
            this.b.setBackground(gradientDrawable);
        } else {
            this.b.setBackgroundDrawable(gradientDrawable);
        }
    }

    public TextView getCallToActionView() {
        return (TextView) this.b;
    }

    public TextView getDescriptionView() {
        return (TextView) this.d;
    }

    public NativeIconView getNativeIconView() {
        return this.f;
    }

    public NativeMediaView getNativeMediaView() {
        return this.g;
    }

    public RatingBar getRatingBar() {
        return (RatingBar) this.c;
    }

    public TextView getTitleView() {
        return (TextView) this.f1498a;
    }

    public void setCallToActionColor(int i2) {
        ((TextView) this.b).setTextColor(i2);
        d();
    }

    public void setCallToActionColor(String str) {
        try {
            ((TextView) this.b).setTextColor(Color.parseColor(str));
        } catch (Exception unused) {
        }
        d();
    }

    public void setNativeAd(NativeAd nativeAd) {
        this.l = nativeAd;
        a();
    }

    public void setPlacement(String str) {
        this.s = str;
    }

    public void showSponsored(boolean z2) {
        this.p = z2;
        c();
    }
}
