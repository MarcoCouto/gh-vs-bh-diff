package com.appodeal.ads.native_ad.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

public class b extends FrameLayout {
    FrameLayout M;

    /* renamed from: a reason: collision with root package name */
    private View f1652a;

    public b(Context context) {
        super(context);
        a(context);
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public b(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    @TargetApi(21)
    public b(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a(context);
    }

    private void a(Context context) {
        this.M = new FrameLayout(context);
        addView((View) this.M, (LayoutParams) new FrameLayout.LayoutParams(-1, -1));
    }

    public void addView(View view) {
        if (view != this.M) {
            this.M.addView(view);
        } else {
            super.addView(view);
        }
    }

    public void addView(View view, int i) {
        if (view != this.M) {
            this.M.addView(view, i);
        } else {
            super.addView(view, i);
        }
    }

    public void addView(View view, int i, int i2) {
        if (view != this.M) {
            this.M.addView(view, i, i2);
        } else {
            super.addView(view, i, i2);
        }
    }

    public void addView(View view, int i, LayoutParams layoutParams) {
        if (view != this.M) {
            this.M.addView(view, i, layoutParams);
        } else {
            super.addView(view, i, layoutParams);
        }
    }

    public void addView(View view, LayoutParams layoutParams) {
        if (view != this.M) {
            this.M.addView(view, layoutParams);
        } else {
            super.addView(view, layoutParams);
        }
    }

    public void bringChildToFront(View view) {
        if (this.M != null) {
            this.M.bringChildToFront(view);
        }
    }

    public void configureContainer(ViewGroup viewGroup) {
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        super.removeView(this.M);
        if (!(this.M == null || this.M.getParent() == null)) {
            ((ViewGroup) this.M.getParent()).removeView(this.M);
        }
        if (!(viewGroup == null || viewGroup.getParent() == null)) {
            ((ViewGroup) viewGroup.getParent()).removeView(viewGroup);
        }
        e();
        super.addView(viewGroup, 0, layoutParams);
        viewGroup.addView(this.M, 0, layoutParams);
        this.f1652a = viewGroup;
    }

    public void deconfigureContainer() {
        if (this.f1652a != null) {
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            if (!(this.M == null || this.M.getParent() == null)) {
                ((ViewGroup) this.M.getParent()).removeView(this.M);
            }
            e();
            super.addView(this.M, 0, layoutParams);
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (this.f1652a != null) {
            super.removeView(this.f1652a);
            this.f1652a = null;
        }
    }

    public void removeAllViews() {
        if (this.M != null) {
            this.M.removeAllViews();
        }
    }

    public void removeView(View view) {
        if (view == this.f1652a) {
            e();
        } else if (this.M != null) {
            this.M.removeView(view);
        }
    }
}
