package com.appodeal.ads;

import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONException;
import org.json.JSONObject;

class ae extends by<ad> {
    ae(@Nullable d dVar) {
        super(dVar);
        if (dVar != null) {
            a(dVar.f());
        }
    }

    public AdType T() {
        return AdType.Banner;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0044 A[Catch:{ JSONException -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004c A[Catch:{ JSONException -> 0x0057 }] */
    /* renamed from: a */
    public void l(ad adVar) {
        String str;
        String str2;
        super.l(adVar);
        try {
            a a2 = ab.a();
            b b = a2.b();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(String.SPLIT_VIEW_ANIMATION, a2.a());
            jSONObject.put("smart", ab.b);
            if (b == b.TOP) {
                str = "type";
                str2 = String.TOP;
            } else if (b == b.BOTTOM) {
                str = "type";
                str2 = String.BOTTOM;
            } else {
                if (b == b.VIEW) {
                    str = "type";
                    str2 = "bannerview";
                }
                if (adVar.e != 50) {
                    jSONObject.put("size", ModuleDescriptor.MODULE_VERSION);
                } else {
                    jSONObject.put("size", 728);
                }
                a(jSONObject);
            }
            jSONObject.put(str, str2);
            if (adVar.e != 50) {
            }
            a(jSONObject);
        } catch (JSONException e) {
            Log.log(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull Configuration configuration) {
        ad adVar = (ad) B();
        return adVar != null && adVar.a(configuration);
    }
}
