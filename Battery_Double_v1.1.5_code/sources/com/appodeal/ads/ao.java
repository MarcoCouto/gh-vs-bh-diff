package com.appodeal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;

class ao extends al<aq, UnifiedInterstitial, UnifiedInterstitialParams, UnifiedInterstitialCallback> {

    private final class a extends UnifiedInterstitialCallback {
        private a() {
        }

        public void onAdClicked() {
            an.b().a(ao.this.a(), ao.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            an.b().a(ao.this.a(), ao.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdClosed() {
            an.b().m(ao.this.a(), ao.this);
        }

        public void onAdExpired() {
            an.b().i(ao.this.a(), ao.this);
        }

        public void onAdFinished() {
            an.b().o(ao.this.a(), ao.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            ao.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            an.b().b(ao.this.a(), ao.this, loadingError);
        }

        public void onAdLoaded() {
            an.b().b(ao.this.a(), ao.this);
        }

        public void onAdShowFailed() {
            an.b().a(ao.this.a(), ao.this, null, LoadingError.ShowFailed);
        }

        public void onAdShown() {
            an.b().p(ao.this.a(), ao.this);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((aq) ao.this.a()).a((AdUnit) ao.this, str, obj);
        }
    }

    private final class b implements UnifiedInterstitialParams {
        private b() {
        }

        public int getAfd() {
            return an.a().D();
        }

        public String obtainPlacementId() {
            return an.a().u();
        }

        public String obtainSegmentId() {
            return an.a().s();
        }
    }

    ao(@NonNull aq aqVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
        super(aqVar, adNetwork, boVar, 10000);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedInterstitial a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createInterstitial();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedInterstitialParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: protected */
    public LoadingError s() {
        return b().isInterstitialShowing() ? LoadingError.Canceled : super.s();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedInterstitialCallback o() {
        return new a();
    }
}
