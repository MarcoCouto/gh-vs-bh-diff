package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.s;
import com.explorestack.protobuf.AbstractMessageLite;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

abstract class bc<RequestDataType, RequestResultType, ErrorResultType> {

    /* renamed from: a reason: collision with root package name */
    public static final c f1593a = new c();
    public static final a b = new a();
    @NonNull
    private String c;
    @NonNull
    private d d;
    @Nullable
    private RequestDataType e;
    /* access modifiers changed from: private */
    @Nullable
    public RequestResultType f;
    /* access modifiers changed from: private */
    @Nullable
    public ErrorResultType g;
    @Nullable
    private g<RequestDataType, RequestResultType, ErrorResultType> h;
    @Nullable
    private ArrayList<h<RequestDataType, RequestResultType, ErrorResultType>> i;
    @Nullable
    private ArrayList<h<RequestDataType, RequestResultType, ErrorResultType>> j;
    /* access modifiers changed from: private */
    @Nullable
    public b<RequestResultType, ErrorResultType> k;
    /* access modifiers changed from: private */
    public i l = i.Idle;

    public static class a extends h {
        /* access modifiers changed from: protected */
        public byte[] a(bc bcVar, URLConnection uRLConnection, byte[] bArr) {
            return Base64.encode(bArr, 0);
        }

        /* access modifiers changed from: protected */
        public byte[] b(bc bcVar, URLConnection uRLConnection, byte[] bArr) {
            return Base64.decode(bArr, 0);
        }
    }

    public interface b<RequestResultType, ErrorResultType> {
        void a(@Nullable RequestResultType requestresulttype);

        void b(@Nullable ErrorResultType errorresulttype);
    }

    public static class c<RequestDataType, RequestResultType, ErrorResultType> extends h<RequestDataType, RequestResultType, ErrorResultType> {
        /* access modifiers changed from: protected */
        public void a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection) {
            uRLConnection.setRequestProperty(HttpRequest.HEADER_ACCEPT_ENCODING, HttpRequest.ENCODING_GZIP);
            uRLConnection.setRequestProperty(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0025  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x002d  */
        public byte[] a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection, byte[] bArr) throws Exception {
            ByteArrayOutputStream byteArrayOutputStream;
            GZIPOutputStream gZIPOutputStream = null;
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(byteArrayOutputStream);
                    try {
                        gZIPOutputStream2.write(bArr);
                        gZIPOutputStream2.close();
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        byteArrayOutputStream.flush();
                        byteArrayOutputStream.close();
                        return byteArray;
                    } catch (Throwable th) {
                        th = th;
                        gZIPOutputStream = gZIPOutputStream2;
                        if (byteArrayOutputStream != null) {
                        }
                        if (gZIPOutputStream != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (byteArrayOutputStream != null) {
                    }
                    if (gZIPOutputStream != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                byteArrayOutputStream = null;
                if (byteArrayOutputStream != null) {
                    byteArrayOutputStream.flush();
                    byteArrayOutputStream.close();
                }
                if (gZIPOutputStream != null) {
                    gZIPOutputStream.flush();
                    gZIPOutputStream.close();
                }
                throw th;
            }
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0050  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0058  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x005d  */
        public byte[] b(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection, byte[] bArr) throws Exception {
            GZIPInputStream gZIPInputStream;
            ByteArrayOutputStream byteArrayOutputStream;
            Throwable th;
            ByteArrayInputStream byteArrayInputStream;
            if (!HttpRequest.ENCODING_GZIP.equals(uRLConnection.getContentEncoding())) {
                return bArr;
            }
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    byteArrayInputStream = new ByteArrayInputStream(bArr);
                    try {
                        gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        gZIPInputStream = null;
                        th = th3;
                        if (byteArrayOutputStream != null) {
                        }
                        if (byteArrayInputStream != null) {
                        }
                        if (gZIPInputStream != null) {
                        }
                        throw th;
                    }
                    try {
                        byte[] bArr2 = new byte[1024];
                        while (true) {
                            int read = gZIPInputStream.read(bArr2);
                            if (read != -1) {
                                byteArrayOutputStream.write(bArr2, 0, read);
                            } else {
                                byte[] byteArray = byteArrayOutputStream.toByteArray();
                                byteArrayOutputStream.flush();
                                byteArrayOutputStream.close();
                                byteArrayInputStream.close();
                                gZIPInputStream.close();
                                return byteArray;
                            }
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        if (byteArrayOutputStream != null) {
                        }
                        if (byteArrayInputStream != null) {
                        }
                        if (gZIPInputStream != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th5) {
                    byteArrayInputStream = null;
                    th = th5;
                    gZIPInputStream = null;
                    if (byteArrayOutputStream != null) {
                        byteArrayOutputStream.flush();
                        byteArrayOutputStream.close();
                    }
                    if (byteArrayInputStream != null) {
                        byteArrayInputStream.close();
                    }
                    if (gZIPInputStream != null) {
                        gZIPInputStream.close();
                    }
                    throw th;
                }
            } catch (Throwable th6) {
                gZIPInputStream = null;
                byteArrayInputStream = null;
                th = th6;
                byteArrayOutputStream = null;
                if (byteArrayOutputStream != null) {
                }
                if (byteArrayInputStream != null) {
                }
                if (gZIPInputStream != null) {
                }
                throw th;
            }
        }
    }

    protected enum d {
        Get(HttpRequest.METHOD_GET),
        Post(HttpRequest.METHOD_POST);
        
        private String c;

        private d(String str) {
            this.c = str;
        }

        public void a(URLConnection uRLConnection) throws ProtocolException {
            if (uRLConnection instanceof HttpURLConnection) {
                ((HttpURLConnection) uRLConnection).setRequestMethod(this.c);
            }
        }
    }

    private final class e implements Runnable {
        private e() {
        }

        public void run() {
            bc.this.d();
            if (bc.this.k == null) {
                return;
            }
            if (bc.this.l == i.Success) {
                bc.this.k.a(bc.this.f);
            } else {
                bc.this.k.b(bc.this.g);
            }
        }
    }

    static abstract class f<RequestDataType extends AbstractMessageLite, RequestResultType, ErrorResultType> extends g<RequestDataType, RequestResultType, ErrorResultType> {
        f() {
        }

        /* access modifiers changed from: protected */
        public void a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection) {
            uRLConnection.setRequestProperty("Content-Type", "application/x-protobuf");
        }

        /* access modifiers changed from: protected */
        @Nullable
        public byte[] a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection, RequestDataType requestdatatype) {
            if (requestdatatype != null) {
                return requestdatatype.toByteArray();
            }
            return null;
        }
    }

    static abstract class g<RequestDataType, RequestResultType, ErrorResultType> {
        g() {
        }

        /* access modifiers changed from: protected */
        public abstract RequestResultType a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection, byte[] bArr) throws Exception;

        /* access modifiers changed from: protected */
        public abstract void a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection);

        /* access modifiers changed from: protected */
        @Nullable
        public abstract byte[] a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection, @Nullable RequestDataType requestdatatype) throws Exception;

        /* access modifiers changed from: protected */
        public ErrorResultType b(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection, byte[] bArr) throws Exception {
            return null;
        }

        /* access modifiers changed from: protected */
        public void b(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection) {
        }
    }

    public static abstract class h<RequestDataType, RequestResultType, ErrorResultType> {
        /* access modifiers changed from: protected */
        public void a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection) {
        }

        /* access modifiers changed from: protected */
        public abstract byte[] a(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection, byte[] bArr) throws Exception;

        /* access modifiers changed from: protected */
        public abstract byte[] b(bc<RequestDataType, RequestResultType, ErrorResultType> bcVar, URLConnection uRLConnection, byte[] bArr) throws Exception;
    }

    public enum i {
        Idle,
        Running,
        Success,
        Fail
    }

    public bc(@NonNull String str, @NonNull d dVar, @Nullable RequestDataType requestdatatype) {
        this.c = str;
        this.d = dVar;
        this.e = requestdatatype;
    }

    private int c(URLConnection uRLConnection) throws IOException {
        if (uRLConnection instanceof HttpURLConnection) {
            return ((HttpURLConnection) uRLConnection).getResponseCode();
        }
        return -1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b4, code lost:
        if (r2 == null) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00b6, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00f0, code lost:
        if (r2 != null) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00f5, code lost:
        if ((r1 instanceof java.net.HttpURLConnection) == false) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00f7, code lost:
        ((java.net.HttpURLConnection) r1).disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00fe, code lost:
        if (r9.g != null) goto L_0x0134;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x012f, code lost:
        if (r9.g == null) goto L_0x0131;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0131, code lost:
        r0 = com.appodeal.ads.bc.i.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0134, code lost:
        r0 = com.appodeal.ads.bc.i.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0136, code lost:
        r9.l = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0138, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0147  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0061 A[Catch:{ Exception -> 0x0110 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00d8 A[Catch:{ all -> 0x0101 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00e3 A[Catch:{ all -> 0x0101 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ea A[SYNTHETIC, Splitter:B:67:0x00ea] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0104 A[SYNTHETIC, Splitter:B:77:0x0104] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x010c A[Catch:{ Exception -> 0x0110 }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0128  */
    public void d() {
        URLConnection uRLConnection;
        Exception e2;
        ByteArrayOutputStream byteArrayOutputStream;
        InputStream inputStream;
        Exception e3;
        ErrorResultType b2;
        this.l = i.Running;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            uRLConnection = (this.c != null ? new URL(String.format("%s/%s", new Object[]{a(), this.c})) : new URL(a())).openConnection();
            try {
                this.d.a(uRLConnection);
                a(uRLConnection);
                byte[] b3 = b(uRLConnection);
                if (b3 != null) {
                    byte[] a2 = a(uRLConnection, b3);
                    uRLConnection.setDoOutput(true);
                    try {
                        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(uRLConnection.getOutputStream());
                        try {
                            bufferedOutputStream2.write(a2);
                            bufferedOutputStream2.flush();
                            bufferedOutputStream2.close();
                        } catch (Throwable th) {
                            BufferedOutputStream bufferedOutputStream3 = bufferedOutputStream2;
                            th = th;
                            bufferedOutputStream = bufferedOutputStream3;
                            if (bufferedOutputStream != null) {
                                bufferedOutputStream.flush();
                                bufferedOutputStream.close();
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        if (bufferedOutputStream != null) {
                        }
                        throw th;
                    }
                }
                try {
                    inputStream = uRLConnection.getInputStream();
                    try {
                        byteArrayOutputStream = new ByteArrayOutputStream();
                    } catch (Exception e4) {
                        byteArrayOutputStream = null;
                        e3 = e4;
                        try {
                            Log.log(e3);
                            this.g = c(uRLConnection) != 200 ? b(uRLConnection, uRLConnection.getOutputStream()) : b(uRLConnection, e3);
                            if (byteArrayOutputStream != null) {
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            if (byteArrayOutputStream != null) {
                                byteArrayOutputStream.flush();
                                byteArrayOutputStream.close();
                            }
                            if (inputStream != null) {
                                inputStream.close();
                            }
                            throw th;
                        }
                    } catch (Throwable th4) {
                        byteArrayOutputStream = null;
                        th = th4;
                        if (byteArrayOutputStream != null) {
                        }
                        if (inputStream != null) {
                        }
                        throw th;
                    }
                    try {
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = inputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            byteArrayOutputStream.write(bArr, 0, read);
                        }
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        if (byteArray != null) {
                            byteArray = b(uRLConnection, byteArray);
                        }
                        if (byteArray != null) {
                            if (byteArray.length != 0) {
                                if (this.h != null) {
                                    this.f = this.h.a(this, uRLConnection, byteArray);
                                    if (this.f == null) {
                                        b2 = this.h.b(this, uRLConnection, byteArray);
                                        this.g = b2;
                                    }
                                }
                                byteArrayOutputStream.flush();
                                byteArrayOutputStream.close();
                            }
                        }
                        b2 = b(uRLConnection, (RequestResultType) null);
                        this.g = b2;
                        byteArrayOutputStream.flush();
                        byteArrayOutputStream.close();
                    } catch (Exception e5) {
                        e3 = e5;
                        Log.log(e3);
                        this.g = c(uRLConnection) != 200 ? b(uRLConnection, uRLConnection.getOutputStream()) : b(uRLConnection, e3);
                        if (byteArrayOutputStream != null) {
                        }
                    }
                } catch (Exception e6) {
                    byteArrayOutputStream = null;
                    e3 = e6;
                    inputStream = null;
                    Log.log(e3);
                    this.g = c(uRLConnection) != 200 ? b(uRLConnection, uRLConnection.getOutputStream()) : b(uRLConnection, e3);
                    if (byteArrayOutputStream != null) {
                        byteArrayOutputStream.flush();
                        byteArrayOutputStream.close();
                    }
                } catch (Throwable th5) {
                    byteArrayOutputStream = null;
                    th = th5;
                    inputStream = null;
                    if (byteArrayOutputStream != null) {
                    }
                    if (inputStream != null) {
                    }
                    throw th;
                }
            } catch (Exception e7) {
                e2 = e7;
                try {
                    Log.log(e2);
                    this.g = b(uRLConnection, e2);
                    if (uRLConnection instanceof HttpURLConnection) {
                        ((HttpURLConnection) uRLConnection).disconnect();
                    }
                } catch (Throwable th6) {
                    th = th6;
                    if (uRLConnection instanceof HttpURLConnection) {
                    }
                    this.l = this.g != null ? i.Success : i.Fail;
                    throw th;
                }
            }
        } catch (Exception e8) {
            Exception exc = e8;
            uRLConnection = null;
            e2 = exc;
            Log.log(e2);
            this.g = b(uRLConnection, e2);
            if (uRLConnection instanceof HttpURLConnection) {
            }
        } catch (Throwable th7) {
            Throwable th8 = th7;
            uRLConnection = null;
            th = th8;
            if (uRLConnection instanceof HttpURLConnection) {
                ((HttpURLConnection) uRLConnection).disconnect();
            }
            this.l = this.g != null ? i.Success : i.Fail;
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public String a() throws Exception {
        return "TODO: implement url";
    }

    public void a(@Nullable b<RequestResultType, ErrorResultType> bVar) {
        this.k = bVar;
    }

    public void a(@Nullable g<RequestDataType, RequestResultType, ErrorResultType> gVar) {
        this.h = gVar;
    }

    public void a(h<RequestDataType, RequestResultType, ErrorResultType> hVar) {
        if (this.j == null) {
            this.j = new ArrayList<>();
        }
        this.j.add(hVar);
    }

    /* access modifiers changed from: protected */
    public void a(URLConnection uRLConnection) {
        uRLConnection.setConnectTimeout(40000);
        uRLConnection.setReadTimeout(40000);
    }

    /* access modifiers changed from: protected */
    public byte[] a(URLConnection uRLConnection, byte[] bArr) throws Exception {
        if (this.i != null) {
            Iterator it = this.i.iterator();
            while (it.hasNext()) {
                h hVar = (h) it.next();
                hVar.a(this, uRLConnection);
                bArr = hVar.a(this, uRLConnection, bArr);
            }
        }
        if (this.j != null) {
            Iterator it2 = this.j.iterator();
            while (it2.hasNext()) {
                h hVar2 = (h) it2.next();
                hVar2.a(this, uRLConnection);
                bArr = hVar2.a(this, uRLConnection, bArr);
            }
        }
        return bArr;
    }

    @NonNull
    public d b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public abstract ErrorResultType b(URLConnection uRLConnection, @Nullable OutputStream outputStream);

    /* access modifiers changed from: 0000 */
    public abstract ErrorResultType b(URLConnection uRLConnection, @Nullable Exception exc);

    /* access modifiers changed from: 0000 */
    public abstract ErrorResultType b(URLConnection uRLConnection, @Nullable RequestResultType requestresulttype);

    /* access modifiers changed from: protected */
    public byte[] b(URLConnection uRLConnection) throws Exception {
        if (this.h == null) {
            return null;
        }
        this.h.b(this, uRLConnection);
        this.h.a(this, uRLConnection);
        return this.h.a(this, uRLConnection, this.e);
    }

    /* access modifiers changed from: protected */
    public byte[] b(URLConnection uRLConnection, byte[] bArr) throws Exception {
        byte[] bArr2;
        if (this.j != null) {
            Iterator it = this.j.iterator();
            bArr2 = bArr;
            while (it.hasNext()) {
                bArr2 = ((h) it.next()).b(this, uRLConnection, bArr2);
            }
        } else {
            bArr2 = bArr;
        }
        if (this.i != null) {
            Iterator it2 = this.i.iterator();
            while (it2.hasNext()) {
                bArr2 = ((h) it2.next()).b(this, uRLConnection, bArr2);
            }
        }
        return bArr;
    }

    public void c() {
        s.f1738a.execute(new e());
    }
}
