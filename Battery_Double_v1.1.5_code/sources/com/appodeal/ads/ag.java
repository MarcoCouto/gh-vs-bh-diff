package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.annotation.Nullable;
import java.lang.reflect.Field;
import java.util.Map;

public class ag {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private Context f1520a;
    @Nullable
    private Activity b;

    public ag(@Nullable Context context) {
        this(context, null);
    }

    public ag(@Nullable Context context, @Nullable Activity activity) {
        this.f1520a = context;
        this.b = activity;
    }

    private static Activity b() {
        Activity activity = null;
        try {
            Class cls = Class.forName("android.app.ActivityThread");
            Object invoke = cls.getMethod("currentActivityThread", new Class[0]).invoke(null, new Object[0]);
            if (invoke == null) {
                return null;
            }
            Field declaredField = cls.getDeclaredField("mActivities");
            declaredField.setAccessible(true);
            Map map = (Map) declaredField.get(invoke);
            if (map != null) {
                if (!map.isEmpty()) {
                    for (Object next : map.values()) {
                        Class cls2 = next.getClass();
                        Field declaredField2 = cls2.getDeclaredField("activityInfo");
                        declaredField2.setAccessible(true);
                        if (((ActivityInfo) declaredField2.get(next)) != null) {
                            Field declaredField3 = cls2.getDeclaredField("paused");
                            declaredField3.setAccessible(true);
                            Field declaredField4 = cls2.getDeclaredField("activity");
                            declaredField4.setAccessible(true);
                            if (!declaredField3.getBoolean(next)) {
                                return (Activity) declaredField4.get(next);
                            }
                            if (activity == null) {
                                activity = (Activity) declaredField4.get(next);
                            }
                        }
                    }
                    return activity;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public Activity a() {
        return this.b != null ? this.b : this.f1520a instanceof Activity ? (Activity) this.f1520a : Appodeal.e != null ? Appodeal.e : b();
    }
}
