package com.appodeal.ads;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.m;
import com.appodeal.ads.unified.UnifiedFullscreenAd;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.appodeal.ads.unified.UnifiedFullscreenAdParams;

abstract class al<AdRequestType extends m, UnifiedAdType extends UnifiedFullscreenAd, UnifiedAdParamsType extends UnifiedFullscreenAdParams, UnifiedAdCallbackType extends UnifiedFullscreenAdCallback> extends i<AdRequestType, UnifiedAdType, UnifiedAdParamsType, UnifiedAdCallbackType> {
    al(@NonNull AdRequestType adrequesttype, @NonNull AdNetwork adNetwork, @NonNull bo boVar, int i) {
        super(adrequesttype, adNetwork, boVar, i);
    }

    public void b(Activity activity) {
        UnifiedFullscreenAd unifiedFullscreenAd = (UnifiedFullscreenAd) n();
        UnifiedFullscreenAdCallback unifiedFullscreenAdCallback = (UnifiedFullscreenAdCallback) p();
        if (unifiedFullscreenAd != null && unifiedFullscreenAdCallback != null) {
            unifiedFullscreenAd.show(activity, unifiedFullscreenAdCallback);
        } else if (unifiedFullscreenAdCallback != null) {
            unifiedFullscreenAdCallback.onAdShowFailed();
        }
    }
}
