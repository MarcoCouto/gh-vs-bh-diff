package com.appodeal.ads;

import android.support.annotation.Nullable;

public interface UserData extends UserSettings {
    @Nullable
    String getAddress();

    @Nullable
    String getCity();

    @Nullable
    String getCountryId();

    @Nullable
    String getIp();

    @Nullable
    String getIpv6();

    @Nullable
    Float getLat();

    @Nullable
    Float getLon();

    @Nullable
    String getZip();
}
