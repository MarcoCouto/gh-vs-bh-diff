package com.appodeal.ads;

public interface AppodealRequestCallbacks {
    void onClick(int i, String str);

    void onImpression(int i, String str);

    void onRequestFinish(int i, String str, boolean z);

    void onRequestStart(int i, String str, String str2);

    void onWaterfallFinish(int i, boolean z);

    void onWaterfallStart(int i);
}
