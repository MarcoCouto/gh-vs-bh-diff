package com.appodeal.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.appodeal.ads.b.e;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.o;
import java.util.Map.Entry;
import org.json.JSONObject;

final class au {

    /* renamed from: a reason: collision with root package name */
    static final av f1560a = new av();
    @VisibleForTesting
    static c b;
    @VisibleForTesting
    static b c;
    /* access modifiers changed from: private */
    public static Integer d = null;
    /* access modifiers changed from: private */
    public static boolean e = false;
    @SuppressLint({"StaticFieldLeak"})
    private static a f;

    static class a extends bw<ax, aw> {
        a() {
            super("debug_mrec", b.VIEW);
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull Activity activity, @NonNull b bVar) {
            au.a((Context) activity, new d(true));
        }

        /* access modifiers changed from: 0000 */
        public boolean a(View view) {
            return view instanceof MrecView;
        }
    }

    @VisibleForTesting(otherwise = 3)
    static class b extends p<aw, ax, d> {
        b(q<aw, ax, ?> qVar) {
            super(qVar, e.c(), 256);
        }

        /* access modifiers changed from: protected */
        public aw a(@NonNull ax axVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
            return new aw(axVar, adNetwork, boVar);
        }

        /* access modifiers changed from: protected */
        public ax a(d dVar) {
            return new ax(dVar);
        }

        public void a(Activity activity) {
            if (r() && l()) {
                ax axVar = (ax) y();
                if (axVar == null || (axVar.M() && !axVar.t())) {
                    u d = au.d().d();
                    if (d == u.HIDDEN || d == u.NEVER_SHOWN) {
                        d((Context) activity);
                    } else {
                        au.a((Context) activity, new d(true));
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("refresh_period")) {
                au.d = Integer.valueOf(jSONObject.optInt("refresh_period") * 1000);
            }
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return super.c() && y() == null;
        }

        /* access modifiers changed from: protected */
        public void d() {
            d dVar = new d(au.e);
            au.e = false;
            au.a(Appodeal.f, dVar);
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            au.a(context, new d());
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "mrec_disabled";
        }
    }

    @VisibleForTesting
    static class c extends bd<aw, ax> {
        c() {
            super(au.f1560a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void b(ax axVar) {
            au.a(axVar, 0, false, false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void q(final ax axVar, aw awVar) {
            if (!axVar.a() && this.f1662a.r()) {
                ax axVar2 = (ax) this.f1662a.y();
                if (axVar2 == null || axVar2.M()) {
                    this.f1662a.d(Appodeal.f);
                }
            }
            if (this.f1662a.r()) {
                br.a((Runnable) new Runnable() {
                    public void run() {
                        try {
                            u d = au.d().d();
                            if (c.this.f1662a.c(c.this.f1662a.b(axVar)) && d != u.HIDDEN && d != u.NEVER_SHOWN) {
                                View c = au.d().c();
                                if (c != null && c.isShown()) {
                                    if (!com.appodeal.ads.utils.c.f1710a.contains(Appodeal.e.getClass().getName())) {
                                        au.a(Appodeal.e, new bx(c.this.f1662a.t(), b.VIEW));
                                    } else {
                                        br.a((Runnable) this, 1000);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.log(e);
                        }
                    }
                }, (long) au.g().intValue());
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(ax axVar, aw awVar, boolean z) {
            super.b(axVar, awVar, z);
            if (d(axVar, awVar)) {
                au.a(Appodeal.e, new bx(this.f1662a.t(), b.VIEW, true, false));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(ax axVar, aw awVar, Object obj) {
            return super.j(axVar, awVar, obj) && this.f1662a.D() > 0;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void c(ax axVar) {
            au.a(axVar, 0, false, true);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void j(ax axVar, aw awVar) {
            if (this.f1662a.r()) {
                u d = au.d().d();
                if (d == u.HIDDEN || d == u.NEVER_SHOWN) {
                    this.f1662a.d(Appodeal.f);
                } else {
                    au.a(Appodeal.f, new d(true));
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void d(@Nullable ax axVar) {
            if (axVar != null) {
                if (axVar.M()) {
                    u d = au.d().d();
                    if (!(d == u.HIDDEN || d == u.NEVER_SHOWN)) {
                        au.a(Appodeal.f, new d(true));
                        return;
                    }
                } else {
                    return;
                }
            }
            this.f1662a.d(Appodeal.f);
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public boolean e(ax axVar, aw awVar) {
            return super.e(axVar, awVar) && !d(axVar, awVar);
        }

        /* access modifiers changed from: protected */
        public boolean d(ax axVar, aw awVar) {
            return axVar.u() && !awVar.h();
        }
    }

    static class d extends n<d> {
        d() {
            super("banner_mrec", "debug_mrec");
        }

        d(boolean z) {
            this();
            b(z);
        }
    }

    static p<aw, ax, d> a() {
        if (c == null) {
            c = new b(b());
        }
        return c;
    }

    static void a(Activity activity) {
        d().a(activity, a());
    }

    static void a(Context context, d dVar) {
        a().b(context, dVar);
    }

    static void a(ax axVar, int i, boolean z, boolean z2) {
        a().a(axVar, i, z2, z);
    }

    static boolean a(Activity activity, bx bxVar) {
        return d().a(activity, bxVar, a());
    }

    static q<aw, ax, ?> b() {
        if (b == null) {
            b = new c();
        }
        return b;
    }

    static void c() {
        if (d().d() != u.VISIBLE) {
            Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_AD_DESTROY, LogLevel.debug);
            ax axVar = (ax) a().y();
            if (axVar != null) {
                if (axVar.B() != null) {
                    o.a(axVar.B());
                    ((aw) axVar.B()).q();
                }
                for (Entry value : axVar.F().entrySet()) {
                    i iVar = (i) value.getValue();
                    if (iVar != null) {
                        o.a(iVar);
                        iVar.q();
                    }
                }
                b().g(axVar);
                axVar.Q();
                axVar.R();
            }
        }
    }

    static a d() {
        if (f == null) {
            f = new a();
        }
        return f;
    }

    /* access modifiers changed from: private */
    public static Integer g() {
        int i;
        com.appodeal.ads.b.d t = a().t();
        if (t == null || t.c() <= 0) {
            if (d == null) {
                i = 15000;
            }
            return d;
        }
        i = t.c();
        d = Integer.valueOf(i);
        return d;
    }
}
