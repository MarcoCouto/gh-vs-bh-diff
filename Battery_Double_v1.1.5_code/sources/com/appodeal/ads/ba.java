package com.appodeal.ads;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.Native.MediaAssetType;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.m;
import com.appodeal.ads.utils.n;
import com.appodeal.ads.utils.s;
import com.explorestack.iab.vast.VastRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class ba extends i<bb, UnifiedNative, UnifiedNativeParams, UnifiedNativeCallback> {
    @Nullable
    List<NativeAd> e;
    @VisibleForTesting
    int f = 0;
    @VisibleForTesting
    boolean g = false;

    private final class a extends UnifiedNativeCallback {
        private a() {
        }

        @Nullable
        private ay a(int i) {
            if (ba.this.e == null || ba.this.e.size() == 0) {
                return null;
            }
            for (NativeAd nativeAd : ba.this.e) {
                if (nativeAd instanceof ay) {
                    ay ayVar = (ay) nativeAd;
                    if (i == ayVar.l()) {
                        return ayVar;
                    }
                }
            }
            return (ay) ba.this.e.get(0);
        }

        public void onAdClicked() {
            Native.b().a(ba.this.a(), ba.this, a(-1), (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(int i, @Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            Native.b().a(ba.this.a(), ba.this, a(i), unifiedAdCallbackClickTrackListener);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            onAdClicked(-1, unifiedAdCallbackClickTrackListener);
        }

        public void onAdExpired() {
            Native.b().i(ba.this.a(), ba.this);
        }

        public void onAdFinished(int i) {
            Native.b().a(ba.this.a(), ba.this, a(i));
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            ba.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            Native.b().b(ba.this.a(), ba.this, loadingError);
        }

        public void onAdLoaded(@NonNull UnifiedNativeAd unifiedNativeAd) {
            UnifiedNativeCallback unifiedNativeCallback = (UnifiedNativeCallback) ba.this.p();
            if (ba.this.e == null || unifiedNativeCallback == null) {
                onAdLoadFailed(LoadingError.InternalError);
                return;
            }
            ba.this.e.add(new ay(ba.this, unifiedNativeAd, unifiedNativeCallback));
            ba.this.u();
        }

        public void onAdShowFailed() {
            Native.b().a(ba.this.a(), ba.this, a(-1), LoadingError.ShowFailed);
        }

        public void onAdShown(int i) {
            Native.b().e(ba.this.a(), ba.this, a(i));
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((bb) ba.this.a()).a((AdUnit) ba.this, str, obj);
        }
    }

    private final class b implements UnifiedNativeParams {
        private int b;

        b(int i) {
            this.b = i;
        }

        public int getAdCountToLoad() {
            return this.b;
        }

        public MediaAssetType getMediaAssetType() {
            return Native.c;
        }

        public NativeAdType getNativeAdType() {
            return Native.b;
        }

        public String obtainPlacementId() {
            return Native.a().u();
        }

        public String obtainSegmentId() {
            return Native.a().s();
        }
    }

    ba(@NonNull bb bbVar, @NonNull AdNetwork adNetwork, @NonNull bo boVar) {
        super(bbVar, adNetwork, boVar, 5000);
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull Object obj, @NonNull UnifiedNativeCallback unifiedNativeCallback, @NonNull UnifiedNative unifiedNative) throws Exception {
        this.e = new ArrayList(unifiedNativeParams.getAdCountToLoad());
        super.a(activity, unifiedNativeParams, obj, unifiedNativeCallback, unifiedNative);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(ay ayVar) {
        String c = ayVar.c();
        String e2 = ayVar.e();
        if (ayVar.containsVideo() && TextUtils.isEmpty(e2) && Native.d != null) {
            ayVar.b(Native.d);
            e2 = Native.d;
        }
        String g2 = ayVar.g();
        String h = ayVar.h();
        if (Native.c != MediaAssetType.IMAGE) {
            this.f++;
        }
        if (Native.c != MediaAssetType.ICON) {
            this.f++;
        }
        if (Native.c != MediaAssetType.IMAGE) {
            a(ayVar, c);
        }
        if (Native.c != MediaAssetType.ICON) {
            b(ayVar, e2);
            if (Native.b != NativeAdType.Video) {
                return;
            }
            if (g2 != null && !g2.isEmpty()) {
                this.f++;
                c(ayVar, g2);
            } else if (h != null && !h.isEmpty()) {
                this.f++;
                d(ayVar, h);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(final ay ayVar, String str) {
        if (str == null || str.isEmpty()) {
            this.f--;
        } else {
            a((Runnable) new com.appodeal.ads.utils.l.a(Appodeal.f, str).a((com.appodeal.ads.utils.l.b) new com.appodeal.ads.utils.l.b() {
                public void a() {
                    ba.this.f--;
                    ba.this.v();
                }

                public void a(Bitmap bitmap) {
                    ayVar.a(bitmap);
                    ba.this.f--;
                    ba.this.v();
                }

                public void a(String str) {
                    ayVar.a(str);
                    ba.this.f--;
                    ba.this.v();
                }
            }).a());
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(Runnable runnable) {
        s.f1738a.execute(runnable);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedNative a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createNativeAd();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void b(final ay ayVar, String str) {
        if (str == null || str.isEmpty()) {
            this.f--;
        } else {
            a((Runnable) new com.appodeal.ads.utils.l.a(Appodeal.f, str).a(true).a((com.appodeal.ads.utils.l.b) new com.appodeal.ads.utils.l.b() {
                public void a() {
                    ba.this.f--;
                    ba.this.v();
                }

                public void a(Bitmap bitmap) {
                    ayVar.b(bitmap);
                    ba.this.f--;
                    ba.this.v();
                }

                public void a(String str) {
                    ayVar.b(str);
                    ba.this.f--;
                    ba.this.v();
                }
            }).a());
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean b(ay ayVar) {
        if (ayVar == null) {
            return false;
        }
        try {
            return !TextUtils.isEmpty(ayVar.getTitle()) && !TextUtils.isEmpty(ayVar.getDescription()) && c(ayVar) && d(ayVar) && e(ayVar);
        } catch (Exception e2) {
            Log.log(e2);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedNativeParams b(int i) {
        return new b(i);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void c(final ay ayVar, String str) {
        if (str == null || str.isEmpty()) {
            this.f--;
        } else {
            a((Runnable) new m(Appodeal.f, new com.appodeal.ads.utils.m.a() {
                public void a() {
                    ba.this.f--;
                    ba.this.v();
                }

                public void a(Uri uri) {
                    ayVar.a(uri);
                    if (TextUtils.isEmpty(ayVar.e()) && uri != null && new File(uri.getPath()).exists()) {
                        ayVar.b(br.a(uri, "native_cache_image"));
                    }
                    ba.this.f--;
                    ba.this.v();
                }
            }, str));
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean c(ay ayVar) {
        return Native.c == MediaAssetType.IMAGE || !TextUtils.isEmpty(ayVar.c()) || ayVar.d() != null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void d(final ay ayVar, String str) {
        a((Runnable) new n(Appodeal.f, new com.appodeal.ads.utils.n.a() {
            public void a() {
                ba.this.f--;
                ba.this.v();
            }

            public void a(Uri uri, VastRequest vastRequest) {
                ayVar.a(vastRequest);
                ayVar.a(uri);
                if (TextUtils.isEmpty(ayVar.e()) && uri != null && new File(uri.getPath()).exists()) {
                    ayVar.b(br.a(uri, "native_cache_image"));
                }
                ba.this.f--;
                ba.this.v();
            }
        }, str));
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean d(ay ayVar) {
        return Native.c == MediaAssetType.ICON || !TextUtils.isEmpty(ayVar.e()) || ayVar.f() != null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean e(ay ayVar) {
        if (Native.c == MediaAssetType.ICON || Native.b != NativeAdType.Video) {
            return true;
        }
        return ayVar.k();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedNativeCallback o() {
        return new a();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void u() {
        if (this.e == null) {
            Native.b().g(a(), this);
            return;
        }
        for (NativeAd nativeAd : this.e) {
            a((ay) nativeAd);
        }
        this.g = true;
        v();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void v() {
        if (this.f == 0) {
            w();
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0073, code lost:
        return;
     */
    public synchronized void w() {
        if (this.e == null) {
            Native.b().g(a(), this);
        } else if (this.g) {
            Iterator it = this.e.iterator();
            int size = this.e.size();
            while (it.hasNext()) {
                NativeAd nativeAd = (NativeAd) it.next();
                if (!b((ay) nativeAd)) {
                    try {
                        it.remove();
                        nativeAd.destroy();
                    } catch (Exception e2) {
                        Log.log(e2);
                    }
                }
            }
            if (this.e.size() > 0) {
                Native.b().b(a(), this);
            } else if (size > 0) {
                Native.b().b(a(), this, LoadingError.InvalidAssets);
            } else {
                Native.b().g(a(), this);
            }
        }
    }

    @Nullable
    public List<NativeAd> x() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public int y() {
        if (this.e != null) {
            return this.e.size();
        }
        return 0;
    }
}
