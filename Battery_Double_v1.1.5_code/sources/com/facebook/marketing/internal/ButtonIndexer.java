package com.facebook.marketing.internal;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.View.AccessibilityDelegate;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.codeless.CodelessLoggingEventListener.AutoLoggingAccessibilityDelegate;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.internal.Utility;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ButtonIndexer {
    /* access modifiers changed from: private */
    public static final String TAG = ButtonIndexer.class.getCanonicalName();
    private Set<Activity> activitiesSet = new HashSet();
    private HashSet<String> delegateSet = new HashSet<>();
    private final Handler uiThreadHandler = new Handler(Looper.getMainLooper());
    private Set<ViewProcessor> viewProcessors = new HashSet();

    protected static class ViewProcessor implements OnGlobalLayoutListener, OnScrollChangedListener, Runnable {
        private static volatile float displayDensity = -1.0f;
        public static volatile Set<String> loadedKeySet = new HashSet();
        private final String activityName;
        private HashSet<String> delegateSet;
        private final Handler handler;
        private WeakReference<View> rootView;
        private HashMap<String, WeakReference<View>> viewMap = new HashMap<>();

        public ViewProcessor(View view, String str, HashSet<String> hashSet, Handler handler2) {
            this.rootView = new WeakReference<>(view);
            this.handler = handler2;
            this.activityName = str;
            this.delegateSet = hashSet;
            if (displayDensity < 0.0f) {
                displayDensity = view.getContext().getResources().getDisplayMetrics().density;
            }
            this.handler.postDelayed(this, 200);
        }

        public void run() {
            RemoteConfig remoteConfigWithoutQuery = RemoteConfigManager.getRemoteConfigWithoutQuery(FacebookSdk.getApplicationId());
            if (remoteConfigWithoutQuery != null && remoteConfigWithoutQuery.getEnableButtonIndexing()) {
                process();
            }
        }

        public void onGlobalLayout() {
            process();
        }

        public void onScrollChanged() {
            process();
        }

        private void process() {
            View view = (View) this.rootView.get();
            if (view != null) {
                attachListeners(view);
            }
        }

        public void attachListeners(View view) {
            JSONObject clickableElementsOfView = getClickableElementsOfView(view, -1, this.activityName, false);
            if (clickableElementsOfView != null) {
                ButtonIndexingLogger.logAllIndexing(clickableElementsOfView, this.activityName);
            }
            for (Entry entry : this.viewMap.entrySet()) {
                attachListener((View) ((WeakReference) entry.getValue()).get(), (String) entry.getKey());
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:27:0x0064 A[Catch:{ JSONException -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x0067 A[SYNTHETIC] */
        @Nullable
        public JSONObject getClickableElementsOfView(View view, int i, String str, boolean z) {
            boolean z2;
            JSONObject clickableElementsOfView;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".");
            sb.append(String.valueOf(i));
            String sb2 = sb.toString();
            if (view == null) {
                return null;
            }
            JSONObject jSONObject = new JSONObject();
            try {
                boolean isClickableView = ViewHierarchy.isClickableView(view);
                if (isClickableView) {
                    this.viewMap.put(sb2, new WeakReference(view));
                }
                if ((view instanceof TextView) || (view instanceof ImageView)) {
                    if (!z) {
                        if (isClickableView) {
                        }
                    }
                    if (loadedKeySet.contains(sb2)) {
                        return null;
                    }
                    loadedKeySet.add(sb2);
                    return ViewHierarchy.setAppearanceOfView(view, ViewHierarchy.setBasicInfoOfView(view, jSONObject), displayDensity);
                }
                JSONArray jSONArray = new JSONArray();
                if (view instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view;
                    int childCount = viewGroup.getChildCount();
                    for (int i2 = 0; i2 < childCount; i2++) {
                        View childAt = viewGroup.getChildAt(i2);
                        if (!z) {
                            if (!isClickableView) {
                                z2 = false;
                                clickableElementsOfView = getClickableElementsOfView(childAt, i2, sb2, z2);
                                if (clickableElementsOfView == null) {
                                    jSONArray.put(clickableElementsOfView);
                                }
                            }
                        }
                        z2 = true;
                        clickableElementsOfView = getClickableElementsOfView(childAt, i2, sb2, z2);
                        if (clickableElementsOfView == null) {
                        }
                    }
                }
                if (jSONArray.length() > 0) {
                    JSONObject basicInfoOfView = ViewHierarchy.setBasicInfoOfView(view, jSONObject);
                    basicInfoOfView.put("childviews", jSONArray);
                    return basicInfoOfView;
                }
            } catch (JSONException e) {
                Utility.logd(ButtonIndexer.TAG, (Exception) e);
            }
            return null;
        }

        private void attachListener(View view, String str) {
            if (view != null) {
                try {
                    AccessibilityDelegate existingDelegate = ViewHierarchy.getExistingDelegate(view);
                    boolean z = false;
                    boolean z2 = existingDelegate != null;
                    boolean z3 = z2 && (existingDelegate instanceof AutoLoggingAccessibilityDelegate);
                    if (z3 && ((AutoLoggingAccessibilityDelegate) existingDelegate).getSupportButtonIndexing()) {
                        z = true;
                    }
                    if (!this.delegateSet.contains(str) && (!z2 || !z3 || !z)) {
                        view.setAccessibilityDelegate(ButtonIndexingEventListener.getAccessibilityDelegate(view, str));
                        this.delegateSet.add(str);
                    }
                } catch (FacebookException e) {
                    Log.e(ButtonIndexer.TAG, "Failed to attach auto logging event listener.", e);
                }
            }
        }
    }

    public void add(Activity activity) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            this.activitiesSet.add(activity);
            this.delegateSet.clear();
            startTracking();
            return;
        }
        throw new FacebookException("Can't add activity to ButtonIndexer on non-UI thread");
    }

    public void remove(Activity activity) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            this.activitiesSet.remove(activity);
            this.viewProcessors.clear();
            this.delegateSet.clear();
            return;
        }
        throw new FacebookException("Can't remove activity from ButtonIndexer on non-UI thread");
    }

    private void startTracking() {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            processViews();
        } else {
            this.uiThreadHandler.post(new Runnable() {
                public void run() {
                    ButtonIndexer.this.processViews();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void processViews() {
        for (Activity activity : this.activitiesSet) {
            this.viewProcessors.add(new ViewProcessor(activity.getWindow().getDecorView().getRootView(), activity.getClass().getSimpleName(), this.delegateSet, this.uiThreadHandler));
        }
    }
}
