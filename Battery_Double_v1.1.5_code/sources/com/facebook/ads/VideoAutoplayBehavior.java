package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
@Deprecated
public enum VideoAutoplayBehavior {
    DEFAULT,
    ON,
    OFF
}
