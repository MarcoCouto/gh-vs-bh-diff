package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public interface RewardedVideoAdExtendedListener extends RewardedVideoAdListener {
    void onRewardedVideoActivityDestroyed();
}
