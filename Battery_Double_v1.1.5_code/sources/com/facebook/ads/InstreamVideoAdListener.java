package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public interface InstreamVideoAdListener extends AdListener {
    void onAdVideoComplete(Ad ad);
}
