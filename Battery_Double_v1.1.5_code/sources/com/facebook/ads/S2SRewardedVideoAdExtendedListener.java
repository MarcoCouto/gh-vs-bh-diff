package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public interface S2SRewardedVideoAdExtendedListener extends RewardedVideoAdExtendedListener, S2SRewardedVideoAdListener {
}
