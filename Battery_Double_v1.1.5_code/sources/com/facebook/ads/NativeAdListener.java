package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public interface NativeAdListener extends AdListener {
    void onMediaDownloaded(Ad ad);
}
