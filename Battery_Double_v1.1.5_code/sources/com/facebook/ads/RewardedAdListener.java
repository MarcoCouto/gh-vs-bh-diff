package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public interface RewardedAdListener {
    void onRewardedAdCompleted();

    void onRewardedAdServerFailed();

    void onRewardedAdServerSucceeded();
}
