package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public abstract class AbstractAdListener implements AdListener, InterstitialAdListener {
    public void onAdClicked(Ad ad) {
    }

    public void onAdLoaded(Ad ad) {
    }

    public void onError(Ad ad, AdError adError) {
    }

    public void onInterstitialDismissed(Ad ad) {
    }

    public void onInterstitialDisplayed(Ad ad) {
    }

    public void onLoggingImpression(Ad ad) {
    }
}
