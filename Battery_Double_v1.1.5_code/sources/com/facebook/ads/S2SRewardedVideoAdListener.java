package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public interface S2SRewardedVideoAdListener extends RewardedVideoAdListener {
    void onRewardServerFailed();

    void onRewardServerSuccess();
}
