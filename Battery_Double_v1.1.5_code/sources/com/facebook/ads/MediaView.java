package com.facebook.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.Keep;
import android.support.annotation.UiThread;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.facebook.ads.internal.api.AdNativeComponentView;
import com.facebook.ads.internal.api.AdViewConstructorParams;
import com.facebook.ads.internal.api.MediaViewApi;
import com.facebook.ads.internal.api.MediaViewParentApi;
import com.facebook.ads.internal.dynamicloading.DynamicLoaderFactory;

@Keep
@UiThread
public class MediaView extends AdNativeComponentView {
    /* access modifiers changed from: private */
    public boolean mImmutable;
    private MediaViewApi mMediaViewApi;

    public MediaView(Context context) {
        super(context);
        initializeSelf(new AdViewConstructorParams(context));
    }

    public MediaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initializeSelf(new AdViewConstructorParams(context, attributeSet));
    }

    public MediaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initializeSelf(new AdViewConstructorParams(context, attributeSet, i));
    }

    @TargetApi(21)
    public MediaView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        initializeSelf(new AdViewConstructorParams(context, attributeSet, i, i2));
    }

    private void initializeSelf(AdViewConstructorParams adViewConstructorParams) {
        this.mMediaViewApi = DynamicLoaderFactory.makeLoader(adViewConstructorParams.getContext()).createMediaViewApi();
        this.mMediaViewApi.initialize(adViewConstructorParams, this, new MediaViewParentApi() {
            public void bringChildToFront(View view) {
                MediaView.super.bringChildToFront(view);
            }

            public void setImmutable(boolean z) {
                MediaView.this.mImmutable = z;
            }
        });
        this.mImmutable = true;
    }

    public MediaViewApi getMediaViewApi() {
        return this.mMediaViewApi;
    }

    public void setVideoRenderer(MediaViewVideoRenderer mediaViewVideoRenderer) {
        this.mMediaViewApi.setVideoRenderer(mediaViewVideoRenderer);
    }

    public int getMediaWidth() {
        return this.mMediaViewApi.getMediaWidth();
    }

    public int getMediaHeight() {
        return this.mMediaViewApi.getMediaHeight();
    }

    public View getAdContentsView() {
        return this.mMediaViewApi.getAdContentsView();
    }

    public void setListener(MediaViewListener mediaViewListener) {
        this.mMediaViewApi.setListener(mediaViewListener);
    }

    public void destroy() {
        this.mMediaViewApi.destroy();
    }

    public void addView(View view) {
        if (!this.mImmutable) {
            super.addView(view);
        }
    }

    public void addView(View view, int i) {
        if (!this.mImmutable) {
            super.addView(view, i);
        }
    }

    public void addView(View view, LayoutParams layoutParams) {
        if (!this.mImmutable) {
            super.addView(view, layoutParams);
        }
    }

    public void addView(View view, int i, int i2) {
        if (!this.mImmutable) {
            super.addView(view, i, i2);
        }
    }

    public void addView(View view, int i, LayoutParams layoutParams) {
        if (!this.mImmutable) {
            super.addView(view, i, layoutParams);
        }
    }

    public void bringChildToFront(View view) {
        this.mMediaViewApi.bringChildToFront(view);
    }
}
