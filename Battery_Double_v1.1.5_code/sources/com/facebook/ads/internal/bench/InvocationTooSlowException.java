package com.facebook.ads.internal.bench;

public class InvocationTooSlowException extends RuntimeException {
    InvocationTooSlowException(String str) {
        super(str);
    }
}
