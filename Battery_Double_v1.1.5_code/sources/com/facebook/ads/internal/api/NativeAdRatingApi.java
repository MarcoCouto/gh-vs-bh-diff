package com.facebook.ads.internal.api;

import android.support.annotation.Keep;

@Keep
public interface NativeAdRatingApi {
    double getScale();

    double getValue();
}
