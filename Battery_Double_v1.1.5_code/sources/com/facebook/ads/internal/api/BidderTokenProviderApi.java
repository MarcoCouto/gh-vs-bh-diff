package com.facebook.ads.internal.api;

import android.content.Context;
import android.support.annotation.Keep;
import android.support.annotation.WorkerThread;

@Keep
public interface BidderTokenProviderApi {
    @WorkerThread
    String getBidderToken(Context context);
}
