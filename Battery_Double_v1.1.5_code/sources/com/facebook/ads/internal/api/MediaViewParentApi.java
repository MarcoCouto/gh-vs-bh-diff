package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.view.View;

@Keep
public interface MediaViewParentApi {
    void bringChildToFront(View view);

    void setImmutable(boolean z);
}
