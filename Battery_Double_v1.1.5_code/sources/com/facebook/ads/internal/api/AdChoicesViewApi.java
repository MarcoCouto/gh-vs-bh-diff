package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import com.facebook.ads.NativeAdLayout;

@Keep
public interface AdChoicesViewApi {
    void initialize(boolean z, @Nullable NativeAdLayout nativeAdLayout);
}
