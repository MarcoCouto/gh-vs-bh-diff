package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.support.annotation.UiThread;

@Keep
@UiThread
public interface AdComponentViewApi extends AdComponentViewParentApi {
    void onAttachedToView(AdComponentView adComponentView, AdComponentViewParentApi adComponentViewParentApi);
}
