package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.support.annotation.UiThread;
import com.facebook.ads.Ad;
import com.facebook.ads.ExtraHints;
import com.facebook.ads.RewardData;
import com.facebook.ads.RewardedVideoAdListener;

@Keep
@UiThread
public interface RewardedVideoAdApi extends Ad {
    void destroy();

    void enableRVChain(boolean z);

    String getPlacementId();

    int getVideoDuration();

    boolean isAdInvalidated();

    boolean isAdLoaded();

    void loadAd();

    void loadAd(boolean z);

    void loadAdFromBid(String str);

    void loadAdFromBid(String str, boolean z);

    void setAdListener(RewardedVideoAdListener rewardedVideoAdListener);

    void setExtraHints(ExtraHints extraHints);

    void setRewardData(RewardData rewardData);

    boolean show();

    boolean show(int i);
}
