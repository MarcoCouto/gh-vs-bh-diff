package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import com.facebook.ads.BuildConfig;

@Keep
public class BuildConfigApi {
    public static String getVersionName() {
        return BuildConfig.VERSION_NAME;
    }

    public static boolean isDebug() {
        return BuildConfig.DEBUG;
    }
}
