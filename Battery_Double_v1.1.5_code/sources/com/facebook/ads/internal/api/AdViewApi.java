package com.facebook.ads.internal.api;

import android.content.res.Configuration;
import android.support.annotation.Keep;
import android.support.annotation.UiThread;
import com.facebook.ads.Ad;
import com.facebook.ads.AdListener;
import com.facebook.ads.ExtraHints;

@Keep
@UiThread
public interface AdViewApi extends AdViewParentApi, Ad {
    void onConfigurationChanged(Configuration configuration);

    void setAdListener(AdListener adListener);

    void setExtraHints(ExtraHints extraHints);
}
