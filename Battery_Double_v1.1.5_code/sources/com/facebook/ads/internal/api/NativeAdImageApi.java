package com.facebook.ads.internal.api;

import android.support.annotation.Keep;

@Keep
public interface NativeAdImageApi {
    int getHeight();

    String getUrl();

    int getWidth();
}
