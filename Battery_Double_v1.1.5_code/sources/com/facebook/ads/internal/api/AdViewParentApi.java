package com.facebook.ads.internal.api;

import android.content.res.Configuration;
import android.support.annotation.Keep;
import android.support.annotation.UiThread;

@Keep
@UiThread
public interface AdViewParentApi {
    void onConfigurationChanged(Configuration configuration);
}
