package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.support.annotation.UiThread;
import com.facebook.ads.Ad;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.ExtraHints;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.RewardedAdListener;
import java.util.EnumSet;

@Keep
@UiThread
public interface InterstitialAdApi extends Ad {
    boolean isAdLoaded();

    void loadAd(EnumSet<CacheFlag> enumSet);

    void loadAdFromBid(EnumSet<CacheFlag> enumSet, String str);

    void setAdListener(InterstitialAdListener interstitialAdListener);

    void setExtraHints(ExtraHints extraHints);

    void setRewardedAdListener(RewardedAdListener rewardedAdListener);

    boolean show();
}
