package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import com.facebook.ads.MediaView;
import java.util.List;

@Keep
public interface NativeBannerAdApi {
    void registerViewForInteraction(View view, ImageView imageView);

    void registerViewForInteraction(View view, ImageView imageView, @Nullable List<View> list);

    void registerViewForInteraction(View view, MediaView mediaView);

    void registerViewForInteraction(View view, MediaView mediaView, @Nullable List<View> list);
}
