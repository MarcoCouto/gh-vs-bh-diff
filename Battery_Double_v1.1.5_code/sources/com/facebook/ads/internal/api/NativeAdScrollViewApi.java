package com.facebook.ads.internal.api;

import android.support.annotation.Keep;

@Keep
public interface NativeAdScrollViewApi {
    void setInset(int i);
}
