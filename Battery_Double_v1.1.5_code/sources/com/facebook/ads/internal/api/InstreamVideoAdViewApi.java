package com.facebook.ads.internal.api;

import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import com.facebook.ads.Ad;
import com.facebook.ads.ExtraHints;
import com.facebook.ads.InstreamVideoAdListener;

@Keep
public interface InstreamVideoAdViewApi extends Ad {
    void destroy();

    String getPlacementId();

    @Nullable
    Bundle getSaveInstanceState();

    boolean isAdInvalidated();

    boolean isAdLoaded();

    void loadAd();

    void loadAdFromBid(String str);

    void setAdListener(@Nullable InstreamVideoAdListener instreamVideoAdListener);

    void setExtraHints(ExtraHints extraHints);

    boolean show();
}
