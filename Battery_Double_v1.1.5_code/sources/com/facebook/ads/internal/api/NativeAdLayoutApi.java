package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import com.facebook.ads.NativeAdLayout;

@Keep
public interface NativeAdLayoutApi extends AdComponentViewApiProvider {
    void initialize(NativeAdLayout nativeAdLayout);

    void setMaxWidth(int i);

    void setMinWidth(int i);
}
