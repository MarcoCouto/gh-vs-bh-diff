package com.facebook.ads.internal.util.common;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.Deque;

@Keep
public class ANActivityLifecycleCallbacksListener implements ActivityLifecycleCallbacks {
    @Nullable
    private static ANActivityLifecycleCallbacksListener sANActivityLifecycleCallbacksListener;
    private final Deque<WeakReference<Activity>> mActivityStack = new ArrayDeque();

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
        this.mActivityStack.push(new WeakReference(activity));
    }

    public void onActivityPaused(Activity activity) {
        if (!this.mActivityStack.isEmpty()) {
            this.mActivityStack.pop();
        }
    }

    public Deque<WeakReference<Activity>> getActivityStack() {
        return this.mActivityStack;
    }

    public static void registerActivityCallbacks(Context context) {
        Context applicationContext = context.getApplicationContext();
        synchronized (ANActivityLifecycleCallbacksListener.class) {
            if ((applicationContext instanceof Application) && sANActivityLifecycleCallbacksListener == null) {
                sANActivityLifecycleCallbacksListener = new ANActivityLifecycleCallbacksListener();
                ((Application) applicationContext).registerActivityLifecycleCallbacks(sANActivityLifecycleCallbacksListener);
            }
        }
    }

    @Nullable
    public static synchronized ANActivityLifecycleCallbacksListener getANActivityLifecycleCallbacksListener() {
        ANActivityLifecycleCallbacksListener aNActivityLifecycleCallbacksListener;
        synchronized (ANActivityLifecycleCallbacksListener.class) {
            aNActivityLifecycleCallbacksListener = sANActivityLifecycleCallbacksListener;
        }
        return aNActivityLifecycleCallbacksListener;
    }

    public static void unregisterActivityCallbacks(Context context) {
        Context applicationContext = context.getApplicationContext();
        synchronized (ANActivityLifecycleCallbacksListener.class) {
            if (applicationContext instanceof Application) {
                ((Application) applicationContext).registerActivityLifecycleCallbacks(sANActivityLifecycleCallbacksListener);
                sANActivityLifecycleCallbacksListener = null;
            }
        }
    }
}
