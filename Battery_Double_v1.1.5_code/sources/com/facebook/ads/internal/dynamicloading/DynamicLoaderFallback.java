package com.facebook.ads.internal.dynamicloading;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.internal.api.AdViewApi;
import com.facebook.ads.internal.api.AdViewParentApi;
import com.facebook.ads.internal.api.InstreamVideoAdViewApi;
import com.facebook.ads.internal.api.InterstitialAdApi;
import com.facebook.ads.internal.api.NativeAdBaseApi;
import com.facebook.ads.internal.api.RewardedVideoAdApi;
import com.github.mikephil.charting.utils.Utils;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

@Keep
class DynamicLoaderFallback {
    /* access modifiers changed from: private */
    public static final WeakHashMap<Object, AdListener> sListenersMap = new WeakHashMap<>();

    private static class SimpleMethodCaptor {
        private final InvocationHandler mInvocationHandler;
        /* access modifiers changed from: private */
        @Nullable
        public Method mLastInvokedMethod;

        private SimpleMethodCaptor() {
            this.mInvocationHandler = new InvocationHandler() {
                @Nullable
                public Object invoke(Object obj, Method method, Object[] objArr) {
                    if (!"toString".equals(method.getName())) {
                        SimpleMethodCaptor.this.mLastInvokedMethod = method;
                    }
                    return null;
                }
            };
        }

        public <T> T mock(Class<T> cls) {
            return Proxy.newProxyInstance(DynamicLoaderFallback.class.getClassLoader(), new Class[]{cls}, this.mInvocationHandler);
        }

        /* access modifiers changed from: 0000 */
        @Nullable
        public Method getLastMethod() {
            return this.mLastInvokedMethod;
        }
    }

    DynamicLoaderFallback() {
    }

    @SuppressLint({"Parameter Not Nullable", "CatchGeneralException"})
    static DynamicLoader makeFallbackLoader() {
        final ArrayList arrayList = new ArrayList();
        final ArrayList arrayList2 = new ArrayList();
        final ArrayList arrayList3 = new ArrayList();
        final HashMap hashMap = new HashMap();
        SimpleMethodCaptor simpleMethodCaptor = new SimpleMethodCaptor();
        DynamicLoader dynamicLoader = (DynamicLoader) simpleMethodCaptor.mock(DynamicLoader.class);
        dynamicLoader.createInterstitialAd(null, null, null);
        arrayList3.add(simpleMethodCaptor.getLastMethod());
        dynamicLoader.createRewardedVideoAd(null, null, null);
        arrayList3.add(simpleMethodCaptor.getLastMethod());
        dynamicLoader.createInstreamVideoAdViewApi(null, null, null, null);
        arrayList3.add(simpleMethodCaptor.getLastMethod());
        dynamicLoader.createAdViewApi((Context) null, (String) null, (AdSize) null, (AdViewParentApi) null, (AdView) null);
        arrayList3.add(simpleMethodCaptor.getLastMethod());
        try {
            dynamicLoader.createAdViewApi((Context) null, (String) null, (String) null, (AdViewParentApi) null, (AdView) null);
        } catch (Exception unused) {
        }
        arrayList3.add(simpleMethodCaptor.getLastMethod());
        dynamicLoader.createNativeAdApi(null, null);
        Method lastMethod = simpleMethodCaptor.getLastMethod();
        dynamicLoader.createNativeBannerAdApi(null, null);
        final Method lastMethod2 = simpleMethodCaptor.getLastMethod();
        NativeAdBaseApi nativeAdBaseApi = (NativeAdBaseApi) simpleMethodCaptor.mock(NativeAdBaseApi.class);
        nativeAdBaseApi.loadAd();
        arrayList.add(simpleMethodCaptor.getLastMethod());
        nativeAdBaseApi.loadAd(null);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        nativeAdBaseApi.loadAdFromBid(null);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        nativeAdBaseApi.loadAdFromBid(null, null);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        nativeAdBaseApi.buildLoadAdConfig();
        Method lastMethod3 = simpleMethodCaptor.getLastMethod();
        nativeAdBaseApi.setAdListener(null, null);
        arrayList2.add(simpleMethodCaptor.getLastMethod());
        InterstitialAdApi interstitialAdApi = (InterstitialAdApi) simpleMethodCaptor.mock(InterstitialAdApi.class);
        interstitialAdApi.loadAd();
        arrayList.add(simpleMethodCaptor.getLastMethod());
        interstitialAdApi.loadAd(null);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        interstitialAdApi.loadAdFromBid(null, null);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        interstitialAdApi.setAdListener(null);
        arrayList2.add(simpleMethodCaptor.getLastMethod());
        RewardedVideoAdApi rewardedVideoAdApi = (RewardedVideoAdApi) simpleMethodCaptor.mock(RewardedVideoAdApi.class);
        rewardedVideoAdApi.loadAd();
        arrayList.add(simpleMethodCaptor.getLastMethod());
        rewardedVideoAdApi.loadAd(false);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        rewardedVideoAdApi.loadAdFromBid(null, false);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        rewardedVideoAdApi.setAdListener(null);
        arrayList2.add(simpleMethodCaptor.getLastMethod());
        InstreamVideoAdViewApi instreamVideoAdViewApi = (InstreamVideoAdViewApi) simpleMethodCaptor.mock(InstreamVideoAdViewApi.class);
        instreamVideoAdViewApi.loadAd();
        arrayList.add(simpleMethodCaptor.getLastMethod());
        instreamVideoAdViewApi.loadAdFromBid(null);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        instreamVideoAdViewApi.setAdListener(null);
        arrayList2.add(simpleMethodCaptor.getLastMethod());
        AdViewApi adViewApi = (AdViewApi) simpleMethodCaptor.mock(AdViewApi.class);
        adViewApi.loadAd();
        arrayList.add(simpleMethodCaptor.getLastMethod());
        adViewApi.loadAdFromBid(null);
        arrayList.add(simpleMethodCaptor.getLastMethod());
        adViewApi.setAdListener(null);
        arrayList2.add(simpleMethodCaptor.getLastMethod());
        final Method method = lastMethod;
        final Method method2 = lastMethod3;
        AnonymousClass1 r0 = new InvocationHandler() {
            @Nullable
            public Object invoke(Object obj, Method method, Object[] objArr) {
                if (method.getReturnType().isPrimitive()) {
                    if (method.getReturnType().equals(Boolean.TYPE)) {
                        return Boolean.valueOf(false);
                    }
                    if (method.getReturnType().equals(Integer.TYPE)) {
                        return Integer.valueOf(-1);
                    }
                    if (method.getReturnType().equals(Byte.TYPE)) {
                        return Integer.valueOf(-1);
                    }
                    if (method.getReturnType().equals(Short.TYPE)) {
                        return Integer.valueOf(-1);
                    }
                    if (method.getReturnType().equals(Long.TYPE)) {
                        return Long.valueOf(-1);
                    }
                    if (method.getReturnType().equals(Double.TYPE)) {
                        return Double.valueOf(Utils.DOUBLE_EPSILON);
                    }
                    if (method.getReturnType().equals(Float.TYPE)) {
                        return Float.valueOf(0.0f);
                    }
                    if (method.getReturnType().equals(Character.TYPE)) {
                        return Integer.valueOf(0);
                    }
                    if (!method.getReturnType().equals(Void.TYPE)) {
                        return null;
                    }
                    Iterator it = arrayList2.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (DynamicLoaderFallback.equalsMethods(method, (Method) it.next())) {
                                DynamicLoaderFallback.sListenersMap.put(obj, objArr[0]);
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext() && (!DynamicLoaderFallback.equalsMethods(method, (Method) it2.next()) || !DynamicLoaderFallback.reportError(obj, hashMap))) {
                    }
                    return null;
                } else if (method.getReturnType().equals(String.class)) {
                    return "";
                } else {
                    Object newProxyInstance = Proxy.newProxyInstance(DynamicLoaderFallback.class.getClassLoader(), new Class[]{method.getReturnType()}, this);
                    for (Method access$100 : arrayList3) {
                        if (DynamicLoaderFallback.equalsMethods(method, access$100)) {
                            for (Ad ad : objArr) {
                                if (ad instanceof Ad) {
                                    hashMap.put(newProxyInstance, ad);
                                }
                            }
                        }
                    }
                    if (DynamicLoaderFallback.equalsMethods(method, method)) {
                        hashMap.put(objArr[1], objArr[0]);
                    }
                    if (DynamicLoaderFallback.equalsMethods(method, lastMethod2)) {
                        hashMap.put(objArr[1], objArr[0]);
                    }
                    if (DynamicLoaderFallback.equalsMethods(method, method2)) {
                        DynamicLoaderFallback.reportError(obj, hashMap);
                    }
                    return newProxyInstance;
                }
            }
        };
        return (DynamicLoader) Proxy.newProxyInstance(DynamicLoaderFallback.class.getClassLoader(), new Class[]{DynamicLoader.class}, r0);
    }

    /* access modifiers changed from: private */
    public static boolean reportError(Object obj, Map<Object, Ad> map) {
        final AdListener adListener = (AdListener) sListenersMap.get(obj);
        final Ad ad = (Ad) map.get(obj);
        if (adListener == null) {
            return false;
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            public void run() {
                adListener.onError(ad, new AdError(-1, "Can't load Audience Network Dex. Please, check that audience_network.dex is inside of assets folder."));
            }
        }, 500);
        return true;
    }

    /* access modifiers changed from: private */
    public static boolean equalsMethods(Method method, Method method2) {
        return method.getDeclaringClass().equals(method2.getDeclaringClass()) && method.getName().equals(method2.getName());
    }
}
