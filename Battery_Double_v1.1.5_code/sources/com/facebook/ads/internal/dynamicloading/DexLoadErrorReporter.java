package com.facebook.ads.internal.dynamicloading;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.Log;
import com.appodeal.ads.utils.LogConstants;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.BuildConfig;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.LocationConst;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DexLoadErrorReporter {
    private static final String LOGGING_URL = "https://www.facebook.com/adnw_logging/";
    public static final double SAMPLING = 0.1d;
    private static final AtomicBoolean sAlreadyReported = new AtomicBoolean();

    @SuppressLint({"CatchGeneralException"})
    public static void reportDexLoadingIssue(final Context context, final String str, double d) {
        if (!sAlreadyReported.get() && Math.random() < d) {
            sAlreadyReported.set(true);
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:45:0x017d A[SYNTHETIC, Splitter:B:45:0x017d] */
                /* JADX WARNING: Removed duplicated region for block: B:50:0x018b A[SYNTHETIC, Splitter:B:50:0x018b] */
                /* JADX WARNING: Removed duplicated region for block: B:58:0x01a0 A[SYNTHETIC, Splitter:B:58:0x01a0] */
                /* JADX WARNING: Removed duplicated region for block: B:63:0x01ae A[SYNTHETIC, Splitter:B:63:0x01ae] */
                /* JADX WARNING: Removed duplicated region for block: B:68:0x01bc  */
                /* JADX WARNING: Removed duplicated region for block: B:73:? A[RETURN, SYNTHETIC] */
                public void run() {
                    InputStream inputStream;
                    HttpURLConnection httpURLConnection;
                    String jSONObject;
                    DataOutputStream dataOutputStream;
                    super.run();
                    DataOutputStream dataOutputStream2 = null;
                    try {
                        httpURLConnection = (HttpURLConnection) new URL(DexLoadErrorReporter.LOGGING_URL).openConnection();
                        try {
                            httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                            httpURLConnection.setRequestProperty("Accept", "application/json");
                            httpURLConnection.setRequestProperty(HttpRequest.HEADER_ACCEPT_CHARSET, "UTF-8");
                            httpURLConnection.setRequestProperty("user-agent", "[FBAN/AudienceNetworkForAndroid;FBSN/Android]");
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setDoInput(true);
                            httpURLConnection.connect();
                            String uuid = UUID.randomUUID().toString();
                            JSONObject jSONObject2 = new JSONObject();
                            jSONObject2.put("attempt", "0");
                            DexLoadErrorReporter.addEnvFields(context, jSONObject2, uuid);
                            JSONObject jSONObject3 = new JSONObject();
                            jSONObject3.put("subtype", MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE);
                            jSONObject3.put("subtype_code", "1320");
                            jSONObject3.put("caught_exception", "1");
                            jSONObject3.put("stacktrace", str);
                            JSONObject jSONObject4 = new JSONObject();
                            jSONObject4.put("id", UUID.randomUUID().toString());
                            jSONObject4.put("type", "debug");
                            StringBuilder sb = new StringBuilder();
                            sb.append("");
                            sb.append(System.currentTimeMillis() / 1000);
                            jSONObject4.put("session_time", sb.toString());
                            String str = LocationConst.TIME;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("");
                            sb2.append(System.currentTimeMillis() / 1000);
                            jSONObject4.put(str, sb2.toString());
                            jSONObject4.put("session_id", uuid);
                            jSONObject4.put("data", jSONObject3);
                            jSONObject4.put("attempt", "0");
                            DexLoadErrorReporter.addEnvFields(context, jSONObject3, uuid);
                            JSONArray jSONArray = new JSONArray();
                            jSONArray.put(jSONObject4);
                            JSONObject jSONObject5 = new JSONObject();
                            jSONObject5.put("data", jSONObject2);
                            jSONObject5.put(EventEntry.TABLE_NAME, jSONArray);
                            jSONObject = jSONObject5.toString();
                            dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
                        } catch (Throwable th) {
                            th = th;
                            inputStream = null;
                            if (dataOutputStream2 != null) {
                                try {
                                    dataOutputStream2.close();
                                } catch (Exception e) {
                                    Log.e(AudienceNetworkAds.TAG, "Can't close connection.", e);
                                }
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (Exception e2) {
                                    Log.e(AudienceNetworkAds.TAG, "Can't close connection.", e2);
                                }
                            }
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            throw th;
                        }
                        try {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("payload=");
                            sb3.append(URLEncoder.encode(jSONObject, "UTF-8"));
                            dataOutputStream.writeBytes(sb3.toString());
                            dataOutputStream.flush();
                            byte[] bArr = new byte[16384];
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            inputStream = httpURLConnection.getInputStream();
                            while (true) {
                                try {
                                    int read = inputStream.read(bArr);
                                    if (read == -1) {
                                        break;
                                    }
                                    byteArrayOutputStream.write(bArr, 0, read);
                                } catch (Throwable th2) {
                                    th = th2;
                                    dataOutputStream2 = dataOutputStream;
                                    if (dataOutputStream2 != null) {
                                    }
                                    if (inputStream != null) {
                                    }
                                    if (httpURLConnection != null) {
                                    }
                                    throw th;
                                }
                            }
                            byteArrayOutputStream.flush();
                            try {
                                dataOutputStream.close();
                            } catch (Exception e3) {
                                Log.e(AudienceNetworkAds.TAG, "Can't close connection.", e3);
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (Exception e4) {
                                    Log.e(AudienceNetworkAds.TAG, "Can't close connection.", e4);
                                }
                            }
                            if (httpURLConnection == null) {
                                return;
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            inputStream = null;
                            dataOutputStream2 = dataOutputStream;
                            if (dataOutputStream2 != null) {
                            }
                            if (inputStream != null) {
                            }
                            if (httpURLConnection != null) {
                            }
                            throw th;
                        }
                    } catch (Throwable th4) {
                        inputStream = null;
                        th = th4;
                        httpURLConnection = null;
                        if (dataOutputStream2 != null) {
                        }
                        if (inputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                    httpURLConnection.disconnect();
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public static void addEnvFields(Context context, JSONObject jSONObject, String str) throws JSONException, NameNotFoundException {
        String packageName = context.getPackageName();
        jSONObject.put("APPBUILD", context.getPackageManager().getPackageInfo(packageName, 0).versionCode);
        jSONObject.put("APPNAME", context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(packageName, 0)));
        jSONObject.put("APPVERS", context.getPackageManager().getPackageInfo(packageName, 0).versionName);
        jSONObject.put("OSVERS", VERSION.RELEASE);
        jSONObject.put(LogConstants.KEY_SDK, "android");
        jSONObject.put("SESSION_ID", str);
        jSONObject.put("MODEL", Build.MODEL);
        jSONObject.put("BUNDLE", packageName);
        jSONObject.put("SDK_VERSION", BuildConfig.VERSION_NAME);
        jSONObject.put("OS", Constants.JAVASCRIPT_INTERFACE_NAME);
    }
}
