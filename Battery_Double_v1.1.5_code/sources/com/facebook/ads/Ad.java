package com.facebook.ads;

import android.support.annotation.Keep;
import com.facebook.ads.internal.bench.Benchmark;

@Keep
public interface Ad {
    @Benchmark
    void destroy();

    @Benchmark(failAtMillis = 5, warnAtMillis = 1)
    String getPlacementId();

    @Benchmark(failAtMillis = 5, warnAtMillis = 1)
    boolean isAdInvalidated();

    @Benchmark
    void loadAd();

    @Benchmark
    void loadAdFromBid(String str);

    @Benchmark(failAtMillis = 5, warnAtMillis = 1)
    void setExtraHints(ExtraHints extraHints);
}
