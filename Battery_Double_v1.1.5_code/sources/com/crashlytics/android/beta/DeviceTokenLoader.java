package com.crashlytics.android.beta;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.cache.ValueLoader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DeviceTokenLoader implements ValueLoader<String> {
    private static final String BETA_APP_PACKAGE_NAME = "io.crash.air";
    private static final String DIRFACTOR_DEVICE_TOKEN_PREFIX = "assets/com.crashlytics.android.beta/dirfactor-device-token=";

    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x005b */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0045 A[SYNTHETIC, Splitter:B:24:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0057 A[SYNTHETIC, Splitter:B:30:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0068 A[SYNTHETIC, Splitter:B:35:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a9 A[SYNTHETIC, Splitter:B:42:0x00a9] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0038=Splitter:B:21:0x0038, B:27:0x004a=Splitter:B:27:0x004a, B:32:0x005b=Splitter:B:32:0x005b} */
    public String load(Context context) throws Exception {
        ZipInputStream zipInputStream;
        long nanoTime = System.nanoTime();
        String str = "";
        zipInputStream = null;
        try {
            ZipInputStream zipInputStreamOfApkFrom = getZipInputStreamOfApkFrom(context, "io.crash.air");
            try {
                String determineDeviceToken = determineDeviceToken(zipInputStreamOfApkFrom);
                if (zipInputStreamOfApkFrom != null) {
                    try {
                        zipInputStreamOfApkFrom.close();
                    } catch (IOException e) {
                        Fabric.getLogger().e(Beta.TAG, "Failed to close the APK file", e);
                    }
                }
                str = determineDeviceToken;
            } catch (NameNotFoundException unused) {
                zipInputStream = zipInputStreamOfApkFrom;
                Fabric.getLogger().d(Beta.TAG, "Beta by Crashlytics app is not installed");
                if (zipInputStream != null) {
                }
                double nanoTime2 = (double) (System.nanoTime() - nanoTime);
                Double.isNaN(nanoTime2);
                double d = nanoTime2 / 1000000.0d;
                Logger logger = Fabric.getLogger();
                String str2 = Beta.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Beta device token load took ");
                sb.append(d);
                sb.append("ms");
                logger.d(str2, sb.toString());
                return str;
            } catch (FileNotFoundException e2) {
                Throwable th = e2;
                zipInputStream = zipInputStreamOfApkFrom;
                e = th;
                Fabric.getLogger().e(Beta.TAG, "Failed to find the APK file", e);
                if (zipInputStream != null) {
                }
                double nanoTime22 = (double) (System.nanoTime() - nanoTime);
                Double.isNaN(nanoTime22);
                double d2 = nanoTime22 / 1000000.0d;
                Logger logger2 = Fabric.getLogger();
                String str22 = Beta.TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Beta device token load took ");
                sb2.append(d2);
                sb2.append("ms");
                logger2.d(str22, sb2.toString());
                return str;
            } catch (IOException e3) {
                Throwable th2 = e3;
                zipInputStream = zipInputStreamOfApkFrom;
                e = th2;
                try {
                    Fabric.getLogger().e(Beta.TAG, "Failed to read the APK file", e);
                    if (zipInputStream != null) {
                    }
                    double nanoTime222 = (double) (System.nanoTime() - nanoTime);
                    Double.isNaN(nanoTime222);
                    double d22 = nanoTime222 / 1000000.0d;
                    Logger logger22 = Fabric.getLogger();
                    String str222 = Beta.TAG;
                    StringBuilder sb22 = new StringBuilder();
                    sb22.append("Beta device token load took ");
                    sb22.append(d22);
                    sb22.append("ms");
                    logger22.d(str222, sb22.toString());
                    return str;
                } catch (Throwable th3) {
                    th = th3;
                    if (zipInputStream != null) {
                        try {
                            zipInputStream.close();
                        } catch (IOException e4) {
                            Fabric.getLogger().e(Beta.TAG, "Failed to close the APK file", e4);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                zipInputStream = zipInputStreamOfApkFrom;
                if (zipInputStream != null) {
                }
                throw th;
            }
        } catch (NameNotFoundException ) {
            Fabric.getLogger().d(Beta.TAG, "Beta by Crashlytics app is not installed");
            if (zipInputStream != null) {
                zipInputStream.close();
            }
        } catch (FileNotFoundException e5) {
            e = e5;
            Fabric.getLogger().e(Beta.TAG, "Failed to find the APK file", e);
            if (zipInputStream != null) {
                zipInputStream.close();
            }
        } catch (IOException e6) {
            e = e6;
            Fabric.getLogger().e(Beta.TAG, "Failed to read the APK file", e);
            if (zipInputStream != null) {
                try {
                    zipInputStream.close();
                } catch (IOException e7) {
                    Fabric.getLogger().e(Beta.TAG, "Failed to close the APK file", e7);
                }
            }
        }
        double nanoTime2222 = (double) (System.nanoTime() - nanoTime);
        Double.isNaN(nanoTime2222);
        double d222 = nanoTime2222 / 1000000.0d;
        Logger logger222 = Fabric.getLogger();
        String str2222 = Beta.TAG;
        StringBuilder sb222 = new StringBuilder();
        sb222.append("Beta device token load took ");
        sb222.append(d222);
        sb222.append("ms");
        logger222.d(str2222, sb222.toString());
        return str;
    }

    /* access modifiers changed from: 0000 */
    public ZipInputStream getZipInputStreamOfApkFrom(Context context, String str) throws NameNotFoundException, FileNotFoundException {
        return new ZipInputStream(new FileInputStream(context.getPackageManager().getApplicationInfo(str, 0).sourceDir));
    }

    /* access modifiers changed from: 0000 */
    public String determineDeviceToken(ZipInputStream zipInputStream) throws IOException {
        ZipEntry nextEntry = zipInputStream.getNextEntry();
        if (nextEntry != null) {
            String name = nextEntry.getName();
            if (name.startsWith(DIRFACTOR_DEVICE_TOKEN_PREFIX)) {
                return name.substring(DIRFACTOR_DEVICE_TOKEN_PREFIX.length(), name.length() - 1);
            }
        }
        return "";
    }
}
