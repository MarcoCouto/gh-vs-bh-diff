package com.smaato.sdk.openmeasurement;

import android.app.Application;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.iab.omid.library.smaato.Omid;
import com.iab.omid.library.smaato.adsession.Partner;
import com.smaato.sdk.core.BuildConfig;
import com.smaato.sdk.core.analytics.NativeViewabilityTracker;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.analytics.ViewabilityPlugin;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;

public final class OMViewabilityPlugin implements ViewabilityPlugin {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private Partner f3547a;
    @Nullable
    private Logger b;

    @NonNull
    public final String getName() {
        return "omid";
    }

    @NonNull
    public final NativeViewabilityTracker getNativeTracker() {
        return null;
    }

    @NonNull
    public final VideoViewabilityTracker getVideoTracker() {
        return null;
    }

    @MainThread
    public final void setup(@NonNull Application application, @NonNull Logger logger) {
        Threads.ensureMainThread();
        this.b = logger;
        try {
            if (!Omid.activateWithOmidApiVersion(Omid.getVersion(), application)) {
                logger.error(LogDomain.OPEN_MEASUREMENT, "Failed to activate Open Measurement SDK", new Object[0]);
            } else {
                this.f3547a = Partner.createPartner("Smaato", BuildConfig.VERSION_NAME);
            }
        } catch (IllegalArgumentException e) {
            logger.error(LogDomain.OPEN_MEASUREMENT, "Failed to activate Open Measurement SDK due to an IllegalArgumentException", e);
        }
    }

    @NonNull
    public final WebViewViewabilityTracker getWebViewTracker() {
        Objects.requireNonNull(this.f3547a, "Makes sure to call the 'viewabilityPlugin.setup(application, logger)' method first");
        Objects.requireNonNull(this.b, "Makes sure to call the 'viewabilityPlugin.setup(application, logger)' method first");
        return new a(this.f3547a, this.b);
    }
}
