package com.smaato.sdk.openmeasurement;

import android.view.View;
import android.webkit.WebView;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.iab.omid.library.smaato.adsession.AdEvents;
import com.iab.omid.library.smaato.adsession.AdSession;
import com.iab.omid.library.smaato.adsession.AdSessionConfiguration;
import com.iab.omid.library.smaato.adsession.AdSessionContext;
import com.iab.omid.library.smaato.adsession.Owner;
import com.iab.omid.library.smaato.adsession.Partner;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;

public final class a implements WebViewViewabilityTracker {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3548a;
    @NonNull
    private final Partner b;
    @Nullable
    private AdSession c;

    a(@NonNull Partner partner, @NonNull Logger logger) {
        this.b = (Partner) Objects.requireNonNull(partner);
        this.f3548a = (Logger) Objects.requireNonNull(logger);
    }

    @MainThread
    public final void registerAdView(@NonNull WebView webView) {
        Threads.ensureMainThread();
        try {
            this.c = AdSession.createAdSession(AdSessionConfiguration.createAdSessionConfiguration(Owner.NATIVE, null, false), AdSessionContext.createHtmlAdSessionContext(this.b, webView, ""));
            this.c.registerAdView(webView);
            this.c.start();
        } catch (IllegalArgumentException e) {
            this.f3548a.error(LogDomain.OPEN_MEASUREMENT, "Failed to register AdView", e);
        }
    }

    @MainThread
    public final void updateAdView(@NonNull WebView webView) {
        Threads.ensureMainThread();
        try {
            Objects.onNotNull(this.c, new Consumer(webView) {
                private final /* synthetic */ WebView f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((AdSession) obj).registerAdView(this.f$0);
                }
            });
        } catch (IllegalArgumentException e) {
            this.f3548a.error(LogDomain.OPEN_MEASUREMENT, "Failed to update AdView", e);
        }
    }

    @MainThread
    public final void startTracking() {
        Threads.ensureMainThread();
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                a.this.b((AdSession) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(AdSession adSession) {
        try {
            AdEvents.createAdEvents(adSession).impressionOccurred();
        } catch (IllegalArgumentException | IllegalStateException e) {
            this.f3548a.error(LogDomain.OPEN_MEASUREMENT, "Failed to signal impression occurred", e);
        }
    }

    @MainThread
    public final void stopTracking() {
        Threads.ensureMainThread();
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                a.this.a((AdSession) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdSession adSession) {
        adSession.finish();
        this.c = null;
    }

    @MainThread
    public final void registerFriendlyObstruction(@NonNull View view) {
        Threads.ensureMainThread();
        try {
            Objects.onNotNull(this.c, new Consumer(view) {
                private final /* synthetic */ View f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((AdSession) obj).addFriendlyObstruction(this.f$0);
                }
            });
        } catch (IllegalArgumentException e) {
            this.f3548a.error(LogDomain.OPEN_MEASUREMENT, "Failed to register friendly obstruction", e);
        }
    }

    @MainThread
    public final void removeFriendlyObstruction(@NonNull View view) {
        Threads.ensureMainThread();
        Objects.onNotNull(this.c, new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((AdSession) obj).removeFriendlyObstruction(this.f$0);
            }
        });
    }
}
