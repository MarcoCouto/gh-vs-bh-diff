package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow.CC;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

class FlowFlatMap<T, U> implements Flow<U> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Flow<T> f3490a;
    @NonNull
    private final Function<? super T, ? extends Publisher<? extends U>> b;

    private static class InnerSubscriber<T, U> implements Subscriber<U> {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final MergeSubscriber<T, U> f3491a;
        /* access modifiers changed from: private */
        @Nullable
        public volatile Subscription b;
        /* access modifiers changed from: private */
        public volatile boolean c;

        InnerSubscriber(@NonNull MergeSubscriber<T, U> mergeSubscriber) {
            this.f3491a = mergeSubscriber;
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            this.b = subscription;
            subscription.request(this.f3491a.e.longValue());
        }

        public void onNext(@NonNull U u) {
            if (!this.c && this.f3491a.d.offer(u)) {
                this.f3491a.a();
            }
        }

        public void onError(@NonNull Throwable th) {
            if (!this.c && this.f3491a.c.compareAndSet(null, th)) {
                this.f3491a.a();
            }
        }

        public void onComplete() {
            if (!this.c) {
                this.c = true;
                this.f3491a.a();
            }
        }
    }

    private static class MergeSubscriber<T, U> implements Subscriber<T> {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final AtomicReference<Subscription> f3492a = new AtomicReference<>();
        @NonNull
        private final List<InnerSubscriber<T, U>> b = new CopyOnWriteArrayList();
        /* access modifiers changed from: private */
        @NonNull
        public final AtomicReference<Throwable> c = new AtomicReference<>();
        /* access modifiers changed from: private */
        @NonNull
        public final Queue<U> d = new ConcurrentLinkedQueue();
        /* access modifiers changed from: private */
        @NonNull
        public final AtomicLong e = new AtomicLong();
        @NonNull
        private final Subscriber<? super U> f;
        @NonNull
        private final Function<? super T, ? extends Publisher<? extends U>> g;
        /* access modifiers changed from: private */
        public volatile boolean h;

        MergeSubscriber(@NonNull Subscriber<? super U> subscriber, @NonNull Function<? super T, ? extends Publisher<? extends U>> function) {
            this.f = subscriber;
            this.g = function;
        }

        public void onSubscribe(@NonNull final Subscription subscription) {
            if (this.f3492a.compareAndSet(null, subscription)) {
                this.f.onSubscribe(new Subscription() {
                    public void request(long j) {
                        MergeSubscriber.this.e.set(j);
                        subscription.request(j);
                    }

                    public void cancel() {
                        MergeSubscriber.this.h = true;
                        subscription.cancel();
                    }
                });
            }
        }

        public void onNext(@NonNull T t) {
            if (!this.h) {
                try {
                    Publisher publisher = (Publisher) Objects.requireNonNull(this.g.apply(t), "The mapper returned a null Publisher");
                    InnerSubscriber innerSubscriber = new InnerSubscriber(this);
                    if (this.b.add(innerSubscriber)) {
                        publisher.subscribe(innerSubscriber);
                    }
                } catch (Exception e2) {
                    Subscription subscription = (Subscription) this.f3492a.get();
                    if (subscription != null) {
                        subscription.cancel();
                    }
                    onError(e2);
                }
            }
        }

        public void onError(@NonNull Throwable th) {
            if (!this.h) {
                try {
                    Objects.requireNonNull(th, "'null' error has been emitted");
                    if (this.c.compareAndSet(null, th)) {
                        a();
                    }
                } catch (Exception e2) {
                    if (this.c.compareAndSet(null, e2)) {
                        a();
                    }
                }
            }
        }

        public void onComplete() {
            if (!this.h) {
                this.h = true;
                a();
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            while (!this.d.isEmpty()) {
                this.f.onNext(this.d.poll());
            }
            ArrayList<InnerSubscriber> arrayList = new ArrayList<>();
            for (InnerSubscriber innerSubscriber : this.b) {
                if (innerSubscriber.c) {
                    arrayList.add(innerSubscriber);
                }
            }
            for (InnerSubscriber remove : arrayList) {
                this.b.remove(remove);
            }
            Throwable th = (Throwable) this.c.get();
            if (th != null) {
                for (InnerSubscriber innerSubscriber2 : this.b) {
                    Subscription b2 = innerSubscriber2.b;
                    if (!innerSubscriber2.c && b2 != null) {
                        b2.cancel();
                    }
                }
                this.b.clear();
                Subscription subscription = (Subscription) this.f3492a.get();
                if (subscription != null) {
                    subscription.cancel();
                }
                this.f.onError(th);
                this.h = true;
                return;
            }
            if (this.h && this.b.isEmpty()) {
                this.f.onComplete();
            }
        }
    }

    @NonNull
    public /* synthetic */ Flow<T> concat(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$concat(this, supplier);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnError(@NonNull Consumer<Throwable> consumer) {
        return CC.$default$doOnError(this, consumer);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnNext(@NonNull Consumer<T> consumer) {
        return CC.$default$doOnNext(this, consumer);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> flatMap(@NonNull Function<T, Publisher<U>> function) {
        return CC.$default$flatMap(this, function);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> map(@NonNull Function<T, U> function) {
        return CC.$default$map(this, function);
    }

    @NonNull
    public /* synthetic */ Flow<T> observeOn(@NonNull Executor executor) {
        return CC.$default$observeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> onErrorResume(@NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        return CC.$default$onErrorResume(this, function);
    }

    public /* synthetic */ void subscribe() {
        CC.$default$subscribe(this);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer) {
        CC.$default$subscribe(this, consumer);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, (Consumer) consumer2);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        CC.$default$subscribe(this, consumer, consumer2, runnable);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Runnable runnable) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, runnable);
    }

    @NonNull
    public /* synthetic */ Flow<T> subscribeOn(@NonNull Executor executor) {
        return CC.$default$subscribeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> switchIfEmpty(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$switchIfEmpty(this, supplier);
    }

    @NonNull
    public /* synthetic */ TestSubscriber<T> test() {
        return CC.$default$test(this);
    }

    FlowFlatMap(@NonNull Flow<T> flow, @NonNull Function<? super T, ? extends Publisher<? extends U>> function) {
        this.f3490a = flow;
        this.b = function;
    }

    public void subscribe(@NonNull Subscriber<? super U> subscriber) {
        this.f3490a.subscribe(new MergeSubscriber(subscriber, this.b));
    }
}
