package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow.CC;
import java.util.concurrent.Executor;

class FlowSubscribeOn<T> implements Flow<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Flow<T> f3500a;
    @NonNull
    private final Executor b;

    @NonNull
    public /* synthetic */ Flow<T> concat(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$concat(this, supplier);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnError(@NonNull Consumer<Throwable> consumer) {
        return CC.$default$doOnError(this, consumer);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnNext(@NonNull Consumer<T> consumer) {
        return CC.$default$doOnNext(this, consumer);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> flatMap(@NonNull Function<T, Publisher<U>> function) {
        return CC.$default$flatMap(this, function);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> map(@NonNull Function<T, U> function) {
        return CC.$default$map(this, function);
    }

    @NonNull
    public /* synthetic */ Flow<T> observeOn(@NonNull Executor executor) {
        return CC.$default$observeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> onErrorResume(@NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        return CC.$default$onErrorResume(this, function);
    }

    public /* synthetic */ void subscribe() {
        CC.$default$subscribe(this);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer) {
        CC.$default$subscribe(this, consumer);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, (Consumer) consumer2);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        CC.$default$subscribe(this, consumer, consumer2, runnable);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Runnable runnable) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, runnable);
    }

    @NonNull
    public /* synthetic */ Flow<T> subscribeOn(@NonNull Executor executor) {
        return CC.$default$subscribeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> switchIfEmpty(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$switchIfEmpty(this, supplier);
    }

    @NonNull
    public /* synthetic */ TestSubscriber<T> test() {
        return CC.$default$test(this);
    }

    FlowSubscribeOn(@NonNull Flow<T> flow, @NonNull Executor executor) {
        this.f3500a = flow;
        this.b = executor;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Subscriber subscriber) {
        this.f3500a.subscribe(subscriber);
    }

    public void subscribe(@NonNull Subscriber<? super T> subscriber) {
        this.b.execute(new Runnable(subscriber) {
            private final /* synthetic */ Subscriber f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                FlowSubscribeOn.this.a(this.f$1);
            }
        });
    }
}
