package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow.CC;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

class FlowConcat<T> implements Flow<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Flow<T> f3485a;
    @NonNull
    private final Supplier<? extends Publisher<? extends T>> b;
    private final boolean c;

    private static class ConcatSubscriber<T> implements Subscriber<T>, Subscription {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final Subscriber<? super T> f3486a;
        @NonNull
        private final Supplier<? extends Publisher<? extends T>> b;
        private final boolean c;
        @NonNull
        private final AtomicReference<Subscription> d = new AtomicReference<>();
        @NonNull
        private final AtomicLong e = new AtomicLong();
        private volatile boolean f = true;
        private volatile boolean g;
        private volatile boolean h;

        ConcatSubscriber(@NonNull Subscriber<? super T> subscriber, @NonNull Supplier<? extends Publisher<? extends T>> supplier, boolean z) {
            this.f3486a = subscriber;
            this.b = supplier;
            this.c = z;
        }

        public void request(long j) {
            SubscriptionHelper.a(this.e, j);
        }

        public void cancel() {
            if (!this.g) {
                SubscriptionHelper.a(this.d);
            }
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            Subscription subscription2 = (Subscription) this.d.getAndSet(subscription);
            if (subscription2 != null) {
                subscription2.cancel();
            }
            subscription.request(this.e.longValue());
        }

        public void onNext(@NonNull T t) {
            if (!this.g) {
                this.f = false;
                this.f3486a.onNext(t);
            }
        }

        public void onError(@NonNull Throwable th) {
            if (!this.g) {
                this.f3486a.onError(th);
            }
            this.g = true;
            this.h = true;
        }

        public void onComplete() {
            if (this.h) {
                this.f3486a.onComplete();
                return;
            }
            this.h = true;
            if (!this.c || this.f) {
                try {
                    ((Publisher) Objects.requireNonNull(this.b.get(), "concat function returned 'null' publisher")).subscribe(this);
                } catch (Exception e2) {
                    this.f3486a.onError(e2);
                    this.g = true;
                }
            } else {
                this.f3486a.onComplete();
            }
        }
    }

    @NonNull
    public /* synthetic */ Flow<T> concat(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$concat(this, supplier);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnError(@NonNull Consumer<Throwable> consumer) {
        return CC.$default$doOnError(this, consumer);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnNext(@NonNull Consumer<T> consumer) {
        return CC.$default$doOnNext(this, consumer);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> flatMap(@NonNull Function<T, Publisher<U>> function) {
        return CC.$default$flatMap(this, function);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> map(@NonNull Function<T, U> function) {
        return CC.$default$map(this, function);
    }

    @NonNull
    public /* synthetic */ Flow<T> observeOn(@NonNull Executor executor) {
        return CC.$default$observeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> onErrorResume(@NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        return CC.$default$onErrorResume(this, function);
    }

    public /* synthetic */ void subscribe() {
        CC.$default$subscribe(this);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer) {
        CC.$default$subscribe(this, consumer);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, (Consumer) consumer2);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        CC.$default$subscribe(this, consumer, consumer2, runnable);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Runnable runnable) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, runnable);
    }

    @NonNull
    public /* synthetic */ Flow<T> subscribeOn(@NonNull Executor executor) {
        return CC.$default$subscribeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> switchIfEmpty(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$switchIfEmpty(this, supplier);
    }

    @NonNull
    public /* synthetic */ TestSubscriber<T> test() {
        return CC.$default$test(this);
    }

    FlowConcat(@NonNull Flow<T> flow, @NonNull Supplier<? extends Publisher<? extends T>> supplier, boolean z) {
        this.f3485a = flow;
        this.b = supplier;
        this.c = z;
    }

    public void subscribe(@NonNull Subscriber<? super T> subscriber) {
        ConcatSubscriber concatSubscriber = new ConcatSubscriber(subscriber, this.b, this.c);
        try {
            subscriber.onSubscribe(concatSubscriber);
            this.f3485a.subscribe(concatSubscriber);
        } catch (Exception e) {
            subscriber.onError(e);
        }
    }
}
