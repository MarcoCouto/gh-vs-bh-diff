package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow.CC;
import java.util.concurrent.Executor;

class FlowDoOnEach<T> implements Flow<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Flow<T> f3487a;
    /* access modifiers changed from: private */
    @NonNull
    public final Consumer<T> b;
    /* access modifiers changed from: private */
    @NonNull
    public final Consumer<Throwable> c;
    /* access modifiers changed from: private */
    @NonNull
    public final Runnable d;

    @NonNull
    public /* synthetic */ Flow<T> concat(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$concat(this, supplier);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnError(@NonNull Consumer<Throwable> consumer) {
        return CC.$default$doOnError(this, consumer);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnNext(@NonNull Consumer<T> consumer) {
        return CC.$default$doOnNext(this, consumer);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> flatMap(@NonNull Function<T, Publisher<U>> function) {
        return CC.$default$flatMap(this, function);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> map(@NonNull Function<T, U> function) {
        return CC.$default$map(this, function);
    }

    @NonNull
    public /* synthetic */ Flow<T> observeOn(@NonNull Executor executor) {
        return CC.$default$observeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> onErrorResume(@NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        return CC.$default$onErrorResume(this, function);
    }

    public /* synthetic */ void subscribe() {
        CC.$default$subscribe(this);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer) {
        CC.$default$subscribe(this, consumer);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, (Consumer) consumer2);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        CC.$default$subscribe(this, consumer, consumer2, runnable);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Runnable runnable) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, runnable);
    }

    @NonNull
    public /* synthetic */ Flow<T> subscribeOn(@NonNull Executor executor) {
        return CC.$default$subscribeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> switchIfEmpty(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$switchIfEmpty(this, supplier);
    }

    @NonNull
    public /* synthetic */ TestSubscriber<T> test() {
        return CC.$default$test(this);
    }

    FlowDoOnEach(@NonNull Flow<T> flow, @NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        this.f3487a = flow;
        this.b = consumer;
        this.c = consumer2;
        this.d = runnable;
    }

    public void subscribe(@NonNull final Subscriber<? super T> subscriber) {
        this.f3487a.subscribe(new Subscriber<T>() {
            public void onSubscribe(@NonNull Subscription subscription) {
                subscriber.onSubscribe(subscription);
            }

            public void onNext(@NonNull T t) {
                FlowDoOnEach.this.b.accept(t);
                subscriber.onNext(t);
            }

            public void onError(@NonNull Throwable th) {
                FlowDoOnEach.this.c.accept(th);
                subscriber.onError(th);
            }

            public void onComplete() {
                FlowDoOnEach.this.d.run();
                subscriber.onComplete();
            }
        });
    }
}
