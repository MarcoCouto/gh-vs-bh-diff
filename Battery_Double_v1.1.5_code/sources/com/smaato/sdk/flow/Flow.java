package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.FunctionUtils;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow.CC;
import com.smaato.sdk.flow.Flow.Emitter;
import java.util.concurrent.Executor;

public interface Flow<T> extends Publisher<T> {

    /* renamed from: com.smaato.sdk.flow.Flow$-CC reason: invalid class name */
    public final /* synthetic */ class CC {
        @NonNull
        public static <T> Flow<T> create(@NonNull OnSubscribe<T> onSubscribe) {
            return new CompositeFlow((OnSubscribe) Objects.requireNonNull(onSubscribe, "'onSubscribe' must not be null"));
        }

        @NonNull
        public static <T> Flow<T> supply(@NonNull Supplier<T> supplier) {
            Objects.requireNonNull(supplier, "'supplier' must not be null");
            return create(new OnSubscribe() {
                public final void accept(Emitter emitter) {
                    CC.b(Supplier.this, emitter);
                }
            });
        }

        public static /* synthetic */ void b(Supplier supplier, Emitter emitter) throws Exception {
            try {
                emitter.next(supplier.get());
                emitter.complete();
            } catch (Exception e) {
                emitter.error(e);
            }
        }

        @NonNull
        public static <T> Flow<T> fromAction(@NonNull Runnable runnable) {
            Objects.requireNonNull(runnable, "'action' must not be null");
            return create(new OnSubscribe(runnable) {
                private final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Emitter emitter) {
                    CC.a(this.f$0, emitter);
                }
            });
        }

        public static /* synthetic */ void a(Runnable runnable, Emitter emitter) throws Exception {
            try {
                runnable.run();
                emitter.complete();
            } catch (Exception e) {
                emitter.error(e);
            }
        }

        @NonNull
        public static <T> Flow<T> iterate(@NonNull Iterable<T> iterable) {
            Objects.requireNonNull(iterable, "'values' must not be null");
            return create(new OnSubscribe(iterable) {
                private final /* synthetic */ Iterable f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Emitter emitter) {
                    CC.a(this.f$0, emitter);
                }
            });
        }

        /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.Iterable, code=java.lang.Iterable<java.lang.Object>, for r1v0, types: [java.lang.Iterable<java.lang.Object>, java.lang.Iterable] */
        public static /* synthetic */ void a(Iterable<Object> iterable, Emitter emitter) throws Exception {
            try {
                for (Object next : iterable) {
                    emitter.next(next);
                }
                emitter.complete();
            } catch (Exception e) {
                emitter.error(e);
            }
        }

        @NonNull
        public static <T> Flow<T> just(@NonNull T t) {
            Objects.requireNonNull(t, "'value' must not be null");
            return create(new OnSubscribe(t) {
                private final /* synthetic */ Object f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Emitter emitter) {
                    CC.a(this.f$0, emitter);
                }
            });
        }

        public static /* synthetic */ void a(Object obj, Emitter emitter) throws Exception {
            try {
                emitter.next(obj);
                emitter.complete();
            } catch (Exception e) {
                emitter.error(e);
            }
        }

        @NonNull
        public static <T> Flow<T> empty() {
            return create($$Lambda$mmuig55wJaavJUKLiCMnCv7oF0Q.INSTANCE);
        }

        @NonNull
        public static <T> Flow<T> error(@NonNull Supplier<Throwable> supplier) {
            Objects.requireNonNull(supplier, "'supplier' must not be null");
            return new CompositeFlow(new OnSubscribe() {
                public final void accept(Emitter emitter) {
                    CC.a(Supplier.this, emitter);
                }
            });
        }

        public static /* synthetic */ void a(Supplier supplier, Emitter emitter) throws Exception {
            try {
                emitter.error((Throwable) supplier.get());
            } catch (Exception e) {
                emitter.error(e);
            }
        }

        @NonNull
        public static <U> Flow $default$flatMap(@NonNull Flow flow, Function function) {
            return new FlowFlatMap(flow, (Function) Objects.requireNonNull(function, "'mapper' must not be null"));
        }

        @NonNull
        public static <U> Flow $default$map(@NonNull Flow flow, Function function) {
            return new FlowMap(flow, (Function) Objects.requireNonNull(function, "'mapper' must not be null"));
        }

        @NonNull
        public static Flow $default$concat(@NonNull Flow flow, Supplier supplier) {
            return new FlowConcat(flow, (Supplier) Objects.requireNonNull(supplier, "'concat' must not be null"), false);
        }

        @NonNull
        public static Flow $default$switchIfEmpty(@NonNull Flow flow, Supplier supplier) {
            return new FlowConcat(flow, (Supplier) Objects.requireNonNull(supplier, "'concat' must not be null"), true);
        }

        @NonNull
        public static Flow $default$onErrorResume(@NonNull Flow flow, Function function) {
            return new FlowOnErrorNext(flow, (Function) Objects.requireNonNull(function, "'resume' must not be null"));
        }

        @NonNull
        public static Flow $default$doOnNext(@NonNull Flow flow, Consumer consumer) {
            return new FlowDoOnEach(flow, (Consumer) Objects.requireNonNull(consumer, "'onNext' must not be null"), FunctionUtils.emptyConsumer(), FunctionUtils.emptyAction());
        }

        @NonNull
        public static Flow $default$doOnError(@NonNull Flow flow, Consumer consumer) {
            return new FlowDoOnEach(flow, FunctionUtils.emptyConsumer(), (Consumer) Objects.requireNonNull(consumer, "'onError' must not be null"), FunctionUtils.emptyAction());
        }

        @NonNull
        public static Flow $default$subscribeOn(@NonNull Flow flow, Executor executor) {
            return new FlowSubscribeOn(flow, (Executor) Objects.requireNonNull(executor, "'executor' must not be null"));
        }

        @NonNull
        public static Flow $default$observeOn(@NonNull Flow flow, Executor executor) {
            return new FlowObserveOn(flow, (Executor) Objects.requireNonNull(executor, "'executor' must not be null"));
        }

        public static void $default$subscribe(@NonNull Flow flow, @NonNull Consumer consumer, @NonNull Consumer consumer2, Runnable runnable) {
            flow.subscribe(new SafeSubscriber((Consumer) Objects.requireNonNull(consumer, "'onNext' must not be null"), (Consumer) Objects.requireNonNull(consumer2, "'onNext' must not be null"), (Runnable) Objects.requireNonNull(runnable, "'onNext' must not be null")));
        }

        public static void $default$subscribe(@NonNull Flow flow, @NonNull Consumer consumer, Consumer consumer2) {
            flow.subscribe(consumer, consumer2, FunctionUtils.emptyAction());
        }

        public static void $default$subscribe(@NonNull Flow flow, @NonNull Consumer consumer, Runnable runnable) {
            flow.subscribe(consumer, FunctionUtils.emptyConsumer(), runnable);
        }

        public static void $default$subscribe(@NonNull Flow flow, Consumer consumer) {
            flow.subscribe(consumer, FunctionUtils.emptyConsumer());
        }

        public static void $default$subscribe(Flow flow) {
            flow.subscribe(FunctionUtils.emptyConsumer());
        }

        @NonNull
        public static TestSubscriber $default$test(Flow flow) {
            TestSubscriber testSubscriber = new TestSubscriber();
            flow.subscribe(testSubscriber);
            return testSubscriber;
        }
    }

    public interface Emitter<T> {
        void complete();

        void error(@NonNull Throwable th);

        void next(@NonNull T t);
    }

    public interface OnSubscribe<T> {
        void accept(@NonNull Emitter<? super T> emitter) throws Exception;
    }

    @NonNull
    Flow<T> concat(@NonNull Supplier<? extends Publisher<? extends T>> supplier);

    @NonNull
    Flow<T> doOnError(@NonNull Consumer<Throwable> consumer);

    @NonNull
    Flow<T> doOnNext(@NonNull Consumer<T> consumer);

    @NonNull
    <U> Flow<U> flatMap(@NonNull Function<T, Publisher<U>> function);

    @NonNull
    <U> Flow<U> map(@NonNull Function<T, U> function);

    @NonNull
    Flow<T> observeOn(@NonNull Executor executor);

    @NonNull
    Flow<T> onErrorResume(@NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function);

    void subscribe();

    void subscribe(@NonNull Consumer<T> consumer);

    void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2);

    void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable);

    void subscribe(@NonNull Consumer<T> consumer, @NonNull Runnable runnable);

    @NonNull
    Flow<T> subscribeOn(@NonNull Executor executor);

    @NonNull
    Flow<T> switchIfEmpty(@NonNull Supplier<? extends Publisher<? extends T>> supplier);

    @NonNull
    TestSubscriber<T> test();
}
