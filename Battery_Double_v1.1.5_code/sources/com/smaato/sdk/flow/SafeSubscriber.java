package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;

class SafeSubscriber<T> implements Subscriber<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Consumer<T> f3501a;
    @NonNull
    private final Consumer<Throwable> b;
    @NonNull
    private final Runnable c;

    SafeSubscriber(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        this.f3501a = consumer;
        this.b = consumer2;
        this.c = runnable;
    }

    public void onSubscribe(@NonNull Subscription subscription) {
        subscription.request(Long.MAX_VALUE);
    }

    public void onNext(@NonNull T t) {
        this.f3501a.accept(t);
    }

    public void onError(@NonNull Throwable th) {
        this.b.accept(th);
    }

    public void onComplete() {
        this.c.run();
    }
}
