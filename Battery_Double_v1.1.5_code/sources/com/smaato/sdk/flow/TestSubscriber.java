package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class TestSubscriber<T> implements Subscriber<T> {

    /* renamed from: a reason: collision with root package name */
    private final List<T> f3503a = new CopyOnWriteArrayList();
    private final AtomicReference<Throwable> b = new AtomicReference<>();
    private final AtomicBoolean c = new AtomicBoolean();

    public void onSubscribe(@NonNull Subscription subscription) {
        subscription.request(Long.MAX_VALUE);
    }

    public void onNext(@NonNull T t) {
        this.f3503a.add(t);
    }

    public void onError(@NonNull Throwable th) {
        this.b.compareAndSet(null, th);
    }

    public void onComplete() {
        this.c.compareAndSet(false, true);
    }

    @NonNull
    public List<T> values() {
        return Collections.unmodifiableList(this.f3503a);
    }

    public boolean isCompleted() {
        return this.c.get();
    }

    @Nullable
    public Throwable error() {
        return (Throwable) this.b.get();
    }
}
