package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public interface FlowExecutors {

    public static class NamedFactory implements ThreadFactory {

        /* renamed from: a reason: collision with root package name */
        private final AtomicInteger f3489a = new AtomicInteger();
        private final String b;
        private final int c;

        public NamedFactory(@NonNull String str, int i) {
            this.b = (String) Objects.requireNonNull(str);
            this.c = i;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            StringBuilder sb = new StringBuilder();
            sb.append(this.b);
            sb.append("-");
            sb.append(this.f3489a.incrementAndGet());
            thread.setName(sb.toString());
            thread.setPriority(this.c);
            thread.setDaemon(true);
            return thread;
        }
    }

    @NonNull
    Executor io();

    @NonNull
    Executor main();
}
