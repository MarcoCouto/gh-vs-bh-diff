package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow.CC;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

class FlowOnErrorNext<T> implements Flow<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Flow<T> f3498a;
    @NonNull
    private final Function<? super Throwable, ? extends Publisher<? extends T>> b;

    private static class OnErrorNextSubscriber<T> implements Subscriber<T>, Subscription {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final Subscriber<? super T> f3499a;
        @NonNull
        private final Function<? super Throwable, ? extends Publisher<? extends T>> b;
        @NonNull
        private final AtomicReference<Subscription> c = new AtomicReference<>();
        @NonNull
        private final AtomicLong d = new AtomicLong();
        private volatile boolean e;
        private volatile boolean f;

        OnErrorNextSubscriber(@NonNull Subscriber<? super T> subscriber, @NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
            this.f3499a = subscriber;
            this.b = function;
        }

        public void request(long j) {
            SubscriptionHelper.a(this.d, j);
        }

        public void cancel() {
            if (!this.e) {
                SubscriptionHelper.a(this.c);
            }
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            Subscription subscription2 = (Subscription) this.c.getAndSet(subscription);
            if (subscription2 != null) {
                subscription2.cancel();
            }
            subscription.request(this.d.longValue());
        }

        public void onNext(@NonNull T t) {
            if (!this.e) {
                this.f3499a.onNext(t);
            }
        }

        public void onError(@NonNull Throwable th) {
            if (this.f) {
                this.f3499a.onError(th);
                return;
            }
            this.f = true;
            try {
                ((Publisher) Objects.requireNonNull(this.b.apply(th), "resume function returned 'null' publisher")).subscribe(this);
            } catch (Exception unused) {
                this.f3499a.onError(th);
            }
        }

        public void onComplete() {
            if (!this.e) {
                this.f3499a.onComplete();
            }
            this.e = true;
            this.f = true;
        }
    }

    @NonNull
    public /* synthetic */ Flow<T> concat(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$concat(this, supplier);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnError(@NonNull Consumer<Throwable> consumer) {
        return CC.$default$doOnError(this, consumer);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnNext(@NonNull Consumer<T> consumer) {
        return CC.$default$doOnNext(this, consumer);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> flatMap(@NonNull Function<T, Publisher<U>> function) {
        return CC.$default$flatMap(this, function);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> map(@NonNull Function<T, U> function) {
        return CC.$default$map(this, function);
    }

    @NonNull
    public /* synthetic */ Flow<T> observeOn(@NonNull Executor executor) {
        return CC.$default$observeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> onErrorResume(@NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        return CC.$default$onErrorResume(this, function);
    }

    public /* synthetic */ void subscribe() {
        CC.$default$subscribe(this);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer) {
        CC.$default$subscribe(this, consumer);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, (Consumer) consumer2);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        CC.$default$subscribe(this, consumer, consumer2, runnable);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Runnable runnable) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, runnable);
    }

    @NonNull
    public /* synthetic */ Flow<T> subscribeOn(@NonNull Executor executor) {
        return CC.$default$subscribeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> switchIfEmpty(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$switchIfEmpty(this, supplier);
    }

    @NonNull
    public /* synthetic */ TestSubscriber<T> test() {
        return CC.$default$test(this);
    }

    FlowOnErrorNext(@NonNull Flow<T> flow, @NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        this.f3498a = flow;
        this.b = function;
    }

    public void subscribe(@NonNull Subscriber<? super T> subscriber) {
        OnErrorNextSubscriber onErrorNextSubscriber = new OnErrorNextSubscriber(subscriber, this.b);
        try {
            subscriber.onSubscribe(onErrorNextSubscriber);
            this.f3498a.subscribe(onErrorNextSubscriber);
        } catch (Exception e) {
            subscriber.onError(e);
        }
    }
}
