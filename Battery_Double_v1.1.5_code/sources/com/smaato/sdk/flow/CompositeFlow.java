package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow.CC;
import com.smaato.sdk.flow.Flow.Emitter;
import com.smaato.sdk.flow.Flow.OnSubscribe;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

class CompositeFlow<T> implements Flow<T>, Emitter<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<InnerSubscriber<? super T>> f3483a = new CopyOnWriteArrayList();
    @NonNull
    private final AtomicReference<Throwable> b = new AtomicReference<>();
    @NonNull
    private final Queue<T> c = new ConcurrentLinkedQueue();
    @NonNull
    private final OnSubscribe<T> d;
    private volatile boolean e;

    private static class InnerSubscriber<T> implements Subscriber<T>, Subscription {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final AtomicLong f3484a = new AtomicLong();
        /* access modifiers changed from: private */
        public final AtomicLong b = new AtomicLong();
        /* access modifiers changed from: private */
        public volatile boolean c;
        @NonNull
        private final Subscriber<? super T> d;

        InnerSubscriber(@NonNull Subscriber<? super T> subscriber) {
            this.d = subscriber;
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            throw new UnsupportedOperationException();
        }

        public void onNext(@NonNull T t) {
            this.d.onNext(t);
        }

        public void onError(@NonNull Throwable th) {
            this.d.onError(th);
        }

        public void onComplete() {
            this.d.onComplete();
        }

        public void request(long j) {
            SubscriptionHelper.a(this.f3484a, j);
        }

        public void cancel() {
            this.c = true;
        }
    }

    @NonNull
    public /* synthetic */ Flow<T> concat(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$concat(this, supplier);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnError(@NonNull Consumer<Throwable> consumer) {
        return CC.$default$doOnError(this, consumer);
    }

    @NonNull
    public /* synthetic */ Flow<T> doOnNext(@NonNull Consumer<T> consumer) {
        return CC.$default$doOnNext(this, consumer);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> flatMap(@NonNull Function<T, Publisher<U>> function) {
        return CC.$default$flatMap(this, function);
    }

    @NonNull
    public /* synthetic */ <U> Flow<U> map(@NonNull Function<T, U> function) {
        return CC.$default$map(this, function);
    }

    @NonNull
    public /* synthetic */ Flow<T> observeOn(@NonNull Executor executor) {
        return CC.$default$observeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> onErrorResume(@NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        return CC.$default$onErrorResume(this, function);
    }

    public /* synthetic */ void subscribe() {
        CC.$default$subscribe(this);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer) {
        CC.$default$subscribe(this, consumer);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, (Consumer) consumer2);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        CC.$default$subscribe(this, consumer, consumer2, runnable);
    }

    public /* synthetic */ void subscribe(@NonNull Consumer<T> consumer, @NonNull Runnable runnable) {
        CC.$default$subscribe((Flow) this, (Consumer) consumer, runnable);
    }

    @NonNull
    public /* synthetic */ Flow<T> subscribeOn(@NonNull Executor executor) {
        return CC.$default$subscribeOn(this, executor);
    }

    @NonNull
    public /* synthetic */ Flow<T> switchIfEmpty(@NonNull Supplier<? extends Publisher<? extends T>> supplier) {
        return CC.$default$switchIfEmpty(this, supplier);
    }

    @NonNull
    public /* synthetic */ TestSubscriber<T> test() {
        return CC.$default$test(this);
    }

    CompositeFlow(@NonNull OnSubscribe<T> onSubscribe) {
        this.d = onSubscribe;
    }

    public void subscribe(@NonNull Subscriber<? super T> subscriber) {
        Objects.requireNonNull(subscriber, "'subscriber' must not be null");
        InnerSubscriber innerSubscriber = new InnerSubscriber(subscriber);
        if (this.f3483a.add(innerSubscriber)) {
            subscriber.onSubscribe(innerSubscriber);
            a();
        }
        try {
            if (this.f3483a.size() == 1) {
                this.d.accept(this);
            }
        } catch (Exception e2) {
            error(e2);
        }
    }

    public void next(@NonNull T t) {
        if (!this.e && !this.f3483a.isEmpty()) {
            try {
                Objects.requireNonNull(t, "'null' value has been emitted");
                if (this.c.offer(t)) {
                    a();
                }
            } catch (Exception e2) {
                error(e2);
            }
        }
    }

    public void error(@NonNull Throwable th) {
        if (!this.e && !this.f3483a.isEmpty()) {
            try {
                Objects.requireNonNull(th, "'null' error has been emitted");
                if (this.b.compareAndSet(null, th)) {
                    a();
                }
            } catch (Exception e2) {
                if (this.b.compareAndSet(null, e2)) {
                    a();
                }
            }
        }
    }

    public void complete() {
        if (!this.e && !this.f3483a.isEmpty()) {
            this.e = true;
            a();
        }
    }

    private void a() {
        ArrayList<InnerSubscriber> arrayList = new ArrayList<>();
        while (!this.c.isEmpty()) {
            Object poll = this.c.poll();
            for (InnerSubscriber innerSubscriber : this.f3483a) {
                if (innerSubscriber.c) {
                    arrayList.add(innerSubscriber);
                } else {
                    if (innerSubscriber.b.getAndIncrement() < innerSubscriber.f3484a.longValue()) {
                        innerSubscriber.onNext(poll);
                    }
                    if (innerSubscriber.b.longValue() >= innerSubscriber.f3484a.longValue()) {
                        innerSubscriber.onComplete();
                        arrayList.add(innerSubscriber);
                    }
                }
            }
        }
        for (InnerSubscriber remove : arrayList) {
            this.f3483a.remove(remove);
        }
        Throwable th = (Throwable) this.b.get();
        if (th != null) {
            for (InnerSubscriber innerSubscriber2 : this.f3483a) {
                if (!innerSubscriber2.c) {
                    innerSubscriber2.onError(th);
                }
            }
            this.f3483a.clear();
            this.e = true;
            return;
        }
        if (this.e) {
            for (InnerSubscriber innerSubscriber3 : this.f3483a) {
                if (!innerSubscriber3.c) {
                    innerSubscriber3.onComplete();
                }
            }
            this.f3483a.clear();
        }
    }
}
