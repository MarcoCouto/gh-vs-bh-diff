package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

abstract class SubscriptionHelper {

    /* renamed from: a reason: collision with root package name */
    private static final Subscription f3502a = new Subscription() {
        public final void cancel() {
        }

        public final void request(long j) {
        }
    };

    private SubscriptionHelper() {
    }

    static void a(@NonNull AtomicLong atomicLong, long j) {
        if (j <= 0) {
            throw new IllegalArgumentException("negative sequence size requested");
        } else if (!atomicLong.compareAndSet(0, j)) {
            throw new IllegalArgumentException("request called more than once");
        }
    }

    static void a(@NonNull AtomicReference<Subscription> atomicReference) {
        if (((Subscription) atomicReference.get()) != f3502a) {
            Subscription subscription = (Subscription) atomicReference.getAndSet(f3502a);
            if (subscription != f3502a && subscription != null) {
                subscription.cancel();
            }
        }
    }
}
