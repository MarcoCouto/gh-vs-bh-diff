package com.smaato.sdk.core.config;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.util.fi.Function;
import java.util.HashMap;

public final class DiConfiguration {

    public interface Provider extends Function<Configuration, ConfigurationRepository> {
    }

    private DiConfiguration() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiConfiguration$gHZ2A0E4MMi8GpIMSiQchgw59X4.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConfigurationRepository a(Configuration configuration) {
        return new ConfigurationRepository(new HashMap(), configuration);
    }
}
