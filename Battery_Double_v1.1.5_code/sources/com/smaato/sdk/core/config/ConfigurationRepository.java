package com.smaato.sdk.core.config;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.util.Objects;
import java.util.Collections;
import java.util.Map;

public final class ConfigurationRepository {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Map<String, Configuration> f3357a;
    @NonNull
    private final Configuration b;

    public ConfigurationRepository(@NonNull Map<String, Configuration> map, @NonNull Configuration configuration) {
        this.f3357a = Collections.synchronizedMap((Map) Objects.requireNonNull(map));
        this.b = (Configuration) Objects.requireNonNull(configuration);
    }

    @NonNull
    public final Configuration get() {
        Configuration configuration = (Configuration) this.f3357a.get(SmaatoSdk.getPublisherId());
        return configuration == null ? this.b : configuration;
    }
}
