package com.smaato.sdk.core.config;

import com.smaato.sdk.core.config.DiConfiguration.Provider;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.util.fi.Consumer;

/* renamed from: com.smaato.sdk.core.config.-$$Lambda$DiConfiguration$gHZ2A0E4MMi8GpIMSiQchgw59X4 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiConfiguration$gHZ2A0E4MMi8GpIMSiQchgw59X4 implements Consumer {
    public static final /* synthetic */ $$Lambda$DiConfiguration$gHZ2A0E4MMi8GpIMSiQchgw59X4 INSTANCE = new $$Lambda$DiConfiguration$gHZ2A0E4MMi8GpIMSiQchgw59X4();

    private /* synthetic */ $$Lambda$DiConfiguration$gHZ2A0E4MMi8GpIMSiQchgw59X4() {
    }

    public final void accept(Object obj) {
        ((DiRegistry) obj).registerFactory(Provider.class, $$Lambda$DiConfiguration$RnP7t4GMw45LHEq0bKgAhAjnds.INSTANCE);
    }
}
