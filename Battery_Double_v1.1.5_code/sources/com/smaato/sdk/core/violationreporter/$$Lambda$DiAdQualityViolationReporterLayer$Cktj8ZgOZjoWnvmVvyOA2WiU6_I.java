package com.smaato.sdk.core.violationreporter;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.core.violationreporter.-$$Lambda$DiAdQualityViolationReporterLayer$Cktj8ZgOZjoWnvmVvyOA2WiU6_I reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiAdQualityViolationReporterLayer$Cktj8ZgOZjoWnvmVvyOA2WiU6_I implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiAdQualityViolationReporterLayer$Cktj8ZgOZjoWnvmVvyOA2WiU6_I INSTANCE = new $$Lambda$DiAdQualityViolationReporterLayer$Cktj8ZgOZjoWnvmVvyOA2WiU6_I();

    private /* synthetic */ $$Lambda$DiAdQualityViolationReporterLayer$Cktj8ZgOZjoWnvmVvyOA2WiU6_I() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newSingleThreadExecutor();
    }
}
