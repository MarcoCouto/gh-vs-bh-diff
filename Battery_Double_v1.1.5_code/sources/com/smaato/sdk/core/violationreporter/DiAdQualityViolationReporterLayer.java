package com.smaato.sdk.core.violationreporter;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.datacollector.DataCollector;
import com.smaato.sdk.core.di.CoreDiNames;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.network.NetworkClient;
import com.smaato.sdk.core.network.NetworkHttpClient;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.Executioner;
import com.smaato.sdk.core.network.execution.HttpTasksExecutioner;
import com.smaato.sdk.core.util.HeaderUtils;
import java.util.concurrent.ExecutorService;

public final class DiAdQualityViolationReporterLayer {
    private DiAdQualityViolationReporterLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiAdQualityViolationReporterLayer$VlGRWqdzdZAtgbzuJXpqNIPl77k.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(CoreDiNames.SOMA_VIOLATIONS_AGGREGATOR_URL, String.class, $$Lambda$DiAdQualityViolationReporterLayer$PH_A_Gv4653oGIchbQfTDMXNlyI.INSTANCE);
        diRegistry.registerSingletonFactory("ad_quality_violation_reporter", ExecutorService.class, $$Lambda$DiAdQualityViolationReporterLayer$Cktj8ZgOZjoWnvmVvyOA2WiU6_I.INSTANCE);
        diRegistry.registerFactory("ad_quality_violation_reporter", Executioner.class, $$Lambda$DiAdQualityViolationReporterLayer$Dgsnx_G7N1AYeEY1zpxe9YhyxfY.INSTANCE);
        diRegistry.registerFactory("ad_quality_violation_reporter", NetworkClient.class, $$Lambda$DiAdQualityViolationReporterLayer$8YYHri0dXA_FxzrSrocJtvezKnU.INSTANCE);
        diRegistry.registerFactory(AdQualityViolationReportMapper.class, $$Lambda$DiAdQualityViolationReporterLayer$ejO7BskTThqYa6Rv3i_wKzf1Ux0.INSTANCE);
        diRegistry.registerSingletonFactory(AdQualityViolationReporter.class, $$Lambda$DiAdQualityViolationReporterLayer$qLyNyCLr7tKt_Cp4ixceTryfZkY.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Executioner d(DiConstructor diConstructor) {
        return new HttpTasksExecutioner(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getNetworkActionsFrom(diConstructor), (ExecutorService) diConstructor.get("ad_quality_violation_reporter", ExecutorService.class), ErrorMapper.NETWORK_LAYER_EXCEPTION);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ NetworkClient c(DiConstructor diConstructor) {
        return new NetworkHttpClient(DiLogLayer.getLoggerFrom(diConstructor), (Executioner) diConstructor.get("ad_quality_violation_reporter", Executioner.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdQualityViolationReportMapper b(DiConstructor diConstructor) {
        return new AdQualityViolationReportMapper(DiLogLayer.getLoggerFrom(diConstructor), (HeaderUtils) diConstructor.get(HeaderUtils.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdQualityViolationReporter a(DiConstructor diConstructor) {
        AdQualityViolationReporter adQualityViolationReporter = new AdQualityViolationReporter(DiLogLayer.getLoggerFrom(diConstructor), (NetworkClient) diConstructor.get("ad_quality_violation_reporter", NetworkClient.class), (AdQualityViolationReportMapper) diConstructor.get(AdQualityViolationReportMapper.class), (String) diConstructor.get(CoreDiNames.SOMA_VIOLATIONS_AGGREGATOR_URL, String.class), ((DataCollector) diConstructor.get(DataCollector.class)).getSystemInfo().getUserAgent());
        return adQualityViolationReporter;
    }
}
