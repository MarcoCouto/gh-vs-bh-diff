package com.smaato.sdk.core.hooks;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.collections.Maps;
import java.util.Map;

public abstract class Hook1061 {

    /* renamed from: a reason: collision with root package name */
    private static final Map<String, String> f3389a = Maps.mapOf(Maps.entryOf("130626424", "1100042525"), Maps.entryOf("130635694", "1100042525"), Maps.entryOf("130635706", "1100042525"), Maps.entryOf("130626426", "1100042525"), Maps.entryOf("130626427", "1100042525"), Maps.entryOf("130626428", "1100042525"), Maps.entryOf("130635048", "1100042525"), Maps.entryOf("130563103", "1100042525"), Maps.entryOf("130560861", "1100042525"), Maps.entryOf("130619168", "1100042525"), Maps.entryOf("130620582", "1100042525"), Maps.entryOf("0", "0"), Maps.entryOf("3090", "0"), Maps.entryOf("130512792", "0"), Maps.entryOf("130699906", "1100044195"), Maps.entryOf("130702154", "1100044195"), Maps.entryOf("130702184", "1100044195"), Maps.entryOf("130702490", "1100044195"), Maps.entryOf("130702855", "1100044195"), Maps.entryOf("130706712", "1100044195"), Maps.entryOf("130706713", "1100044195"), Maps.entryOf("130706714", "1100044195"), Maps.entryOf("130706715", "1100044195"), Maps.entryOf("130706716", "1100044195"), Maps.entryOf("130706717", "1100044195"));

    private Hook1061() {
    }

    @NonNull
    public static String onGetPublisherId(@NonNull String str, @Nullable String str2) {
        if (str2 != null) {
            String str3 = (String) f3389a.get(str2);
            if (str3 != null) {
                return str3;
            }
        }
        return str;
    }
}
