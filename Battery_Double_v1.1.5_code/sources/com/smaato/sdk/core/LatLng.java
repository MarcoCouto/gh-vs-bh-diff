package com.smaato.sdk.core;

public final class LatLng {

    /* renamed from: a reason: collision with root package name */
    private final double f3284a;
    private final double b;

    public LatLng(double d, double d2) {
        this.f3284a = d;
        this.b = d2;
    }

    public final double getLatitude() {
        return this.f3284a;
    }

    public final double getLongitude() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LatLng latLng = (LatLng) obj;
        return Double.compare(latLng.f3284a, this.f3284a) == 0 && Double.compare(latLng.b, this.b) == 0;
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.f3284a);
        int i = (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        long doubleToLongBits2 = Double.doubleToLongBits(this.b);
        return (i * 31) + ((int) ((doubleToLongBits2 >>> 32) ^ doubleToLongBits2));
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("LatLng{latitude=");
        sb.append(this.f3284a);
        sb.append(", longitude=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
