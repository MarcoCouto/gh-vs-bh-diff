package com.smaato.sdk.core.deeplink;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.browser.SmaatoSdkBrowserActivity;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.UrlCreator;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Intents;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Consumer;
import java.net.URISyntaxException;
import java.util.List;

public final class LinkResolver {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3369a;
    @NonNull
    private final Application b;
    @NonNull
    private final RedirectResolver c;
    @NonNull
    private final UrlCreator d;

    LinkResolver(@NonNull Logger logger, @NonNull Application application, @NonNull RedirectResolver redirectResolver, @NonNull UrlCreator urlCreator) {
        this.f3369a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for LinkResolver::new");
        this.b = (Application) Objects.requireNonNull(application, "Parameter application cannot be null for LinkResolver::new");
        this.c = (RedirectResolver) Objects.requireNonNull(redirectResolver, "Parameter redirectResolver cannot be null for LinkResolver::new");
        this.d = (UrlCreator) Objects.requireNonNull(urlCreator, "Parameter urlCreator cannot be null for LinkResolver::new");
    }

    @NonNull
    public final Task handleClickThroughUrl(@NonNull SomaApiContext somaApiContext, @NonNull final String str, @NonNull final UrlResolveListener urlResolveListener) {
        final Either findExternalAppForUrl = findExternalAppForUrl(str);
        return findExternalAppForUrl != null ? new Task() {
            public void cancel() {
            }

            public void start() {
                Consumer a2 = LinkResolver.a(LinkResolver.this, findExternalAppForUrl);
                if (a2 != null) {
                    urlResolveListener.onSuccess(a2);
                } else {
                    urlResolveListener.onError();
                }
            }
        } : this.c.submitRequest(str, somaApiContext, (Listener<String, Exception>) new Listener<String, Exception>() {
            public void onSuccess(@NonNull Task task, @NonNull String str) {
                Consumer consumer;
                Either findExternalAppForUrl = LinkResolver.this.findExternalAppForUrl(str);
                if (findExternalAppForUrl != null) {
                    consumer = LinkResolver.a(LinkResolver.this, findExternalAppForUrl);
                } else {
                    consumer = LinkResolver.this.a(str);
                }
                if (consumer != null) {
                    urlResolveListener.onSuccess(consumer);
                } else {
                    urlResolveListener.onError();
                }
            }

            public void onFailure(@NonNull Task task, @NonNull Exception exc) {
                LinkResolver.this.f3369a.error(LogDomain.CORE, exc, "Failed to resolve url: %s", str);
                urlResolveListener.onError();
            }
        });
    }

    /* access modifiers changed from: private */
    @Nullable
    public Consumer<Context> a(@NonNull String str) {
        if (this.d.isSupportedForNetworking(str)) {
            return new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    Intents.startIntent((Context) obj, SmaatoSdkBrowserActivity.createIntent((Context) obj, this.f$0));
                }
            };
        }
        return null;
    }

    @Nullable
    private Either<Intent, String> b(@NonNull String str) {
        try {
            Intent parseUri = Intent.parseUri(str, 1);
            if (Intents.canHandleIntent(this.b, parseUri)) {
                return Either.left(parseUri);
            }
            String stringExtra = parseUri.getStringExtra("browser_fallback_url");
            if (!TextUtils.isEmpty(stringExtra)) {
                return Either.right(stringExtra);
            }
            String stringExtra2 = parseUri.getStringExtra("package");
            if (TextUtils.isEmpty(stringExtra2)) {
                return null;
            }
            StringBuilder sb = new StringBuilder("market://details?id=");
            sb.append(stringExtra2);
            return Either.left(Intents.createViewIntent(sb.toString()));
        } catch (URISyntaxException e) {
            this.f3369a.error(LogDomain.CORE, e, "Failed to parse intent: uri", new Object[0]);
            return null;
        }
    }

    @Nullable
    public final Intent getExternalBrowserIntent(@NonNull String str) {
        Intent createViewIntent = Intents.createViewIntent(str);
        createViewIntent.addFlags(268435456);
        if (Intents.canHandleIntent(this.b, createViewIntent)) {
            return createViewIntent;
        }
        return null;
    }

    @Nullable
    public final Intent getExternalBrowserAppInstallIntent(@NonNull String str) {
        Intent createViewIntent = Intents.createViewIntent("market://search?q=browser&c=apps");
        if (Intents.canHandleIntent(this.b, createViewIntent)) {
            return createViewIntent;
        }
        return null;
    }

    @Nullable
    public final Either<Intent, String> findExternalAppForUrl(@NonNull String str) {
        if (this.d.isSupportedForNetworking(str)) {
            PackageManager packageManager = this.b.getPackageManager();
            boolean z = false;
            List queryIntentActivities = packageManager.queryIntentActivities(Intents.createViewIntent("https://"), 0);
            Intent addCategory = Intents.createViewIntent(str).addCategory("android.intent.category.BROWSABLE").addCategory("android.intent.category.DEFAULT");
            List queryIntentActivities2 = packageManager.queryIntentActivities(addCategory, 0);
            if (queryIntentActivities.size() == queryIntentActivities2.size()) {
                z = Lists.map(queryIntentActivities, $$Lambda$LinkResolver$8kC9m1megDPxOGRCTHbbNVV3u0.INSTANCE).equals(Lists.map(queryIntentActivities2, $$Lambda$LinkResolver$ihf_GXVNDxE7sD_BZFu5apEQQc.INSTANCE));
            }
            Object addFlags = z ? null : addCategory.addFlags(268435456);
            if (addFlags != null) {
                return Either.left(addFlags);
            }
            return null;
        } else if ("intent".equalsIgnoreCase(this.d.extractScheme(str))) {
            return b(str);
        } else {
            Intent createViewIntent = Intents.createViewIntent(str);
            if (Intents.canHandleIntent(this.b, createViewIntent)) {
                return Either.left(createViewIntent);
            }
            return null;
        }
    }

    static /* synthetic */ Consumer a(LinkResolver linkResolver, Either either) {
        Intent intent = (Intent) either.left();
        if (intent != null) {
            return new Consumer(intent) {
                private final /* synthetic */ Intent f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    Intents.startIntent((Context) obj, this.f$0);
                }
            };
        }
        return linkResolver.a((String) either.right());
    }
}
