package com.smaato.sdk.core.deeplink;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.execution.ClickThroughUrlRedirectResolver;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.Executioner;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.network.execution.RedirectResolveTask;
import com.smaato.sdk.core.util.Objects;
import java.util.concurrent.ExecutorService;

public class RedirectResolver implements Executioner<String, String, Exception> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3372a;
    @NonNull
    private final ExecutorService b;
    @NonNull
    private final NetworkActions c;
    @NonNull
    private final ErrorMapper<Exception> d;
    @NonNull
    private final ClickThroughUrlRedirectResolver e;

    RedirectResolver(@NonNull Logger logger, @NonNull ExecutorService executorService, @NonNull NetworkActions networkActions, @NonNull ErrorMapper<Exception> errorMapper, @NonNull ClickThroughUrlRedirectResolver clickThroughUrlRedirectResolver) {
        this.f3372a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for RedirectResolver::new");
        this.b = (ExecutorService) Objects.requireNonNull(executorService, "Parameter executorService cannot be null for RedirectResolver::new");
        this.c = (NetworkActions) Objects.requireNonNull(networkActions, "Parameter networkActions cannot be null for RedirectResolver::new");
        this.d = (ErrorMapper) Objects.requireNonNull(errorMapper, "Parameter errorMapper cannot be null for RedirectResolver::new");
        this.e = (ClickThroughUrlRedirectResolver) Objects.requireNonNull(clickThroughUrlRedirectResolver, "Parameter clickThroughUrlRedirectResolver cannot be null for RedirectResolver::new");
    }

    @NonNull
    public Task submitRequest(@NonNull String str, @Nullable SomaApiContext somaApiContext, @NonNull Listener<String, Exception> listener) {
        return RedirectResolveTask.create(this.f3372a, this.b, str, listener, this.c, this.d, this.e, somaApiContext);
    }
}
