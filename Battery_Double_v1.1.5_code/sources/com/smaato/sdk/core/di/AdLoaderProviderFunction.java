package com.smaato.sdk.core.di;

import com.smaato.sdk.core.ad.AdLoader;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.util.fi.Function;

public interface AdLoaderProviderFunction extends Function<AdLoaderPlugin, AdLoader> {
}
