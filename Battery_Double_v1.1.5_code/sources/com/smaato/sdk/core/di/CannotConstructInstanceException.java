package com.smaato.sdk.core.di;

final class CannotConstructInstanceException extends RuntimeException {
    public CannotConstructInstanceException(String str) {
        super(str);
    }

    public CannotConstructInstanceException(String str, Exception exc) {
        super(str, exc);
    }
}
