package com.smaato.sdk.core.di;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

final class DiKey {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private String f3374a;
    @NonNull
    private Class b;

    DiKey(@Nullable String str, @NonNull Class cls) {
        this.f3374a = str;
        this.b = cls;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DiKey diKey = (DiKey) obj;
        if (this.f3374a == null ? diKey.f3374a == null : this.f3374a.equals(diKey.f3374a)) {
            return this.b.equals(diKey.b);
        }
        return false;
    }

    public final int hashCode() {
        return ((this.f3374a != null ? this.f3374a.hashCode() : 0) * 31) + this.b.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("DiKey{name='");
        sb.append(this.f3374a);
        sb.append('\'');
        sb.append(", clazz=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
