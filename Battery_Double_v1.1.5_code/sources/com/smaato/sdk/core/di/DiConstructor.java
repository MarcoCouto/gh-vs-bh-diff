package com.smaato.sdk.core.di;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class DiConstructor {

    /* renamed from: a reason: collision with root package name */
    private final Map<DiKey, ClassFactory> f3373a = new HashMap();

    @SafeVarargs
    private DiConstructor(Map<DiKey, ClassFactory>... mapArr) {
        for (Map<DiKey, ClassFactory> entrySet : mapArr) {
            for (Entry entry : entrySet.entrySet()) {
                DiKey diKey = (DiKey) entry.getKey();
                if (!this.f3373a.containsKey(diKey)) {
                    this.f3373a.put(entry.getKey(), entry.getValue());
                } else {
                    StringBuilder sb = new StringBuilder("There is already registered factory for ");
                    sb.append(diKey);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }
    }

    @NonNull
    public static DiConstructor create(@NonNull DiRegistry diRegistry) {
        return new DiConstructor(diRegistry.a());
    }

    @NonNull
    public static DiConstructor create(@NonNull DiRegistry... diRegistryArr) {
        Objects.requireNonNull(diRegistryArr);
        if (diRegistryArr.length != 0) {
            ArrayList arrayList = new ArrayList(diRegistryArr.length);
            for (DiRegistry a2 : diRegistryArr) {
                arrayList.add(a2.a());
            }
            return a((Map[]) arrayList.toArray(new Map[0]));
        }
        throw new IllegalStateException("No registries passed");
    }

    @NonNull
    public static DiConstructor create(@NonNull Set<DiRegistry> set) {
        return create((DiRegistry[]) set.toArray(new DiRegistry[0]));
    }

    @SafeVarargs
    private static DiConstructor a(Map<DiKey, ClassFactory>... mapArr) {
        return new DiConstructor(mapArr);
    }

    public final <T> T get(Class<T> cls) {
        return get(null, cls);
    }

    @NonNull
    public final <T> T get(@Nullable String str, @NonNull Class<T> cls) {
        Objects.requireNonNull(cls);
        try {
            ClassFactory classFactory = (ClassFactory) this.f3373a.get(new DiKey(str, cls));
            if (classFactory != null) {
                return Objects.requireNonNull(cls.cast(classFactory.get(this)), "FACTORY RETURNED NULL.");
            }
            StringBuilder sb = new StringBuilder("NO FACTORY PROVIDED. Cannot create instance of ");
            sb.append(cls);
            sb.append(" named '");
            sb.append(str);
            sb.append("'");
            throw new CannotConstructInstanceException(sb.toString());
        } catch (ClassCastException e) {
            StringBuilder sb2 = new StringBuilder("FACTORY RETURNED WRONG INSTANCE. Cannot create instance of ");
            sb2.append(cls);
            sb2.append(" named '");
            sb2.append(str);
            sb2.append("'");
            throw new CannotConstructInstanceException(sb2.toString(), e);
        } catch (Exception e2) {
            if (e2 instanceof CannotConstructInstanceException) {
                throw e2;
            }
            StringBuilder sb3 = new StringBuilder("Cannot create instance of ");
            sb3.append(cls);
            sb3.append(" named '");
            sb3.append(str);
            sb3.append("'");
            throw new CannotConstructInstanceException(sb3.toString(), e2);
        }
    }

    @Nullable
    public final <T> T getOrNull(@NonNull Class<T> cls) {
        return getOrNull(null, cls);
    }

    @Nullable
    public final <T> T getOrNull(@Nullable String str, @NonNull Class<T> cls) {
        try {
            return get(str, cls);
        } catch (CannotConstructInstanceException unused) {
            return null;
        }
    }

    public final DiConstructor addRegistry(@NonNull DiRegistry diRegistry) {
        Objects.requireNonNull(diRegistry);
        return a(this.f3373a, diRegistry.a());
    }
}
