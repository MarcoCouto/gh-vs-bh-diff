package com.smaato.sdk.core.di;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

final class SingletonFactory<T> implements ClassFactory<T> {

    /* renamed from: a reason: collision with root package name */
    private static final Object f3376a = new Object();
    private volatile ClassFactory<T> b;
    private volatile Object c = f3376a;

    private SingletonFactory(ClassFactory<T> classFactory) {
        this.b = (ClassFactory) Objects.requireNonNull(classFactory);
    }

    @NonNull
    public final T get(DiConstructor diConstructor) {
        T t = this.c;
        if (t == f3376a) {
            synchronized (this) {
                t = this.c;
                if (t == f3376a) {
                    T t2 = this.b.get(diConstructor);
                    T t3 = this.c;
                    if (t3 != f3376a) {
                        if (t3 != t2) {
                            StringBuilder sb = new StringBuilder("Scoped provider was invoked recursively returning different results: ");
                            sb.append(t3);
                            sb.append(" & ");
                            sb.append(t2);
                            sb.append(". This is likely due to a circular dependency.");
                            throw new IllegalStateException(sb.toString());
                        }
                    }
                    this.c = t2;
                    this.b = null;
                    t = t2;
                }
            }
        }
        return t;
    }

    public static <T> ClassFactory<T> wrap(ClassFactory<T> classFactory) {
        Objects.requireNonNull(classFactory);
        if (classFactory instanceof SingletonFactory) {
            return classFactory;
        }
        return new SingletonFactory(classFactory);
    }
}
