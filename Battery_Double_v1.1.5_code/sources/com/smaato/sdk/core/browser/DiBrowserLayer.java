package com.smaato.sdk.core.browser;

import android.app.Application;
import android.content.ClipboardManager;
import android.webkit.CookieManager;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.deeplink.DiDeepLinkLayer;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.webview.DiWebViewLayer;

public final class DiBrowserLayer {
    private DiBrowserLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiBrowserLayer$xwZvtR9dZS3nneSoR_e7UBfdOic.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(CookieSyncManagerHolder.class, $$Lambda$DiBrowserLayer$FoLc0ytLaJGA0OHNzlUqKfNWm8.INSTANCE);
        diRegistry.registerFactory(ClipboardManager.class, $$Lambda$DiBrowserLayer$8hPXy07TwWjQLlUlcgsPKCi9Xqk.INSTANCE);
        diRegistry.registerFactory(CookieManager.class, $$Lambda$DiBrowserLayer$y754uA1byKZ9VYTGq26W5LVQ8.INSTANCE);
        diRegistry.registerFactory(SmaatoCookieManager.class, $$Lambda$DiBrowserLayer$KW3auf63ipYCbZkNiUdfy5MvlEI.INSTANCE);
        diRegistry.registerFactory(BrowserModel.class, $$Lambda$DiBrowserLayer$mBXHeTeVG1WcAUYwYY1CHIkfY48.INSTANCE);
        diRegistry.registerFactory(BrowserPresenter.class, $$Lambda$DiBrowserLayer$VLKjMdkwiEHfcFSLojLwybYapT4.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ CookieSyncManagerHolder f(DiConstructor diConstructor) {
        return new CookieSyncManagerHolder((Application) diConstructor.get(Application.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ClipboardManager e(DiConstructor diConstructor) {
        return (ClipboardManager) Objects.requireNonNull((ClipboardManager) ((Application) diConstructor.get(Application.class)).getSystemService("clipboard"));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SmaatoCookieManager c(DiConstructor diConstructor) {
        return new SmaatoCookieManager((CookieManager) diConstructor.get(CookieManager.class), (CookieSyncManagerHolder) diConstructor.get(CookieSyncManagerHolder.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BrowserModel b(DiConstructor diConstructor) {
        return new BrowserModel(DiLogLayer.getLoggerFrom(diConstructor), DiWebViewLayer.getBaseWebViewClientFrom(diConstructor), DiWebViewLayer.getBaseWebChromeClientFrom(diConstructor), (SmaatoCookieManager) diConstructor.get(SmaatoCookieManager.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BrowserPresenter a(DiConstructor diConstructor) {
        BrowserPresenter browserPresenter = new BrowserPresenter(DiLogLayer.getLoggerFrom(diConstructor), (BrowserModel) diConstructor.get(BrowserModel.class), DiNetworkLayer.getUrlCreatorFrom(diConstructor), DiDeepLinkLayer.getLinkResolverFrom(diConstructor), (ClipboardManager) diConstructor.get(ClipboardManager.class));
        return browserPresenter;
    }
}
