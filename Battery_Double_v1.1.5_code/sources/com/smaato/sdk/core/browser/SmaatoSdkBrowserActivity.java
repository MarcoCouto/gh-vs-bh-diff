package com.smaato.sdk.core.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.R;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.DoubleClickPreventionListener;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.diinjection.Inject;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.webview.WebViewHelperUtil;

public class SmaatoSdkBrowserActivity extends Activity implements BrowserView {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private WebView f3351a;
    @NonNull
    private TextView b;
    @NonNull
    private ProgressBar c;
    @NonNull
    private View d;
    @NonNull
    private View e;
    /* access modifiers changed from: private */
    @Inject
    @Nullable
    public BrowserPresenter f;
    @Inject
    @Nullable
    private Logger g;

    public static Intent createIntent(@NonNull Context context, @NonNull String str) {
        Objects.requireNonNull(context, "Parameter context cannot be null for SmaatoSdkBrowserActivity::createIntent");
        Objects.requireNonNull(str, "Parameter url cannot be null for SmaatoSdkBrowserActivity::createIntent");
        Intent intent = new Intent(context, SmaatoSdkBrowserActivity.class);
        intent.addFlags(536870912);
        intent.putExtra("KEY_CTA_URL", str);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.smaato_sdk_core_activity_internal_browser);
        this.f3351a = (WebView) findViewById(R.id.webView);
        this.b = (TextView) findViewById(R.id.tvHostname);
        this.c = (ProgressBar) findViewById(R.id.progressBar);
        View findViewById = findViewById(R.id.btnClose);
        View findViewById2 = findViewById(R.id.btnRefresh);
        this.d = findViewById(R.id.btnBackward);
        this.e = findViewById(R.id.btnForward);
        View findViewById3 = findViewById(R.id.btnOpenExternal);
        findViewById.setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                SmaatoSdkBrowserActivity.this.finish();
            }
        });
        findViewById2.setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                Objects.onNotNull(SmaatoSdkBrowserActivity.this.f, $$Lambda$OycmKNItiN79aTZc0jQAv7_zk.INSTANCE);
            }
        });
        this.d.setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                Objects.onNotNull(SmaatoSdkBrowserActivity.this.f, $$Lambda$zg6nko2q9HAOFDuGvnWKuuxwxIc.INSTANCE);
            }
        });
        this.e.setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                Objects.onNotNull(SmaatoSdkBrowserActivity.this.f, $$Lambda$3Who52wYNLeQhFdpTsxGL2qI3mk.INSTANCE);
            }
        });
        findViewById3.setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                Objects.onNotNull(SmaatoSdkBrowserActivity.this.f, $$Lambda$SZooymytBc3co7R0LbcHuAKC8E.INSTANCE);
            }
        });
        this.b.setOnLongClickListener(new OnLongClickListener() {
            public final boolean onLongClick(View view) {
                return SmaatoSdkBrowserActivity.this.a(view);
            }
        });
        WebSettings settings = this.f3351a.getSettings();
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        AndroidsInjector.inject((Activity) this);
        Objects.onNotNull(this.f, new Consumer() {
            public final void accept(Object obj) {
                SmaatoSdkBrowserActivity.this.a((BrowserPresenter) obj);
            }
        });
        Objects.onNotNull(this.f, new Consumer(getIntent().getStringExtra("KEY_CTA_URL")) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((BrowserPresenter) obj).loadUrl(this.f$0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Objects.onNotNull(this.f, $$Lambda$_xH7XsVTZB0hRcN5JSoYlEDrFv4.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Objects.onNotNull(this.f, $$Lambda$dWdInfO6XN4ddHAUECmtK31crFc.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Objects.onNotNull(this.f, $$Lambda$JO76bjlBJ5VXWw5msPJKAupQnvM.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Objects.onNotNull(this.f, $$Lambda$mhUkOmav4YNRhITdGxUXfCL8MfA.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        WebViewHelperUtil.resetAndDestroyWebViewSafely(this.f3351a, this.g);
        Objects.onNotNull(this.f, $$Lambda$R86vL1LYd4bfRfllP6LfOcvV7k.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(View view) {
        Objects.onNotNull(this.f, $$Lambda$3kQ6XBVCKF9lr3VvE5pBizjEdg.INSTANCE);
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(BrowserPresenter browserPresenter) {
        browserPresenter.initWithView(this, this.f3351a);
    }

    public void showHostname(@Nullable String str) {
        this.b.setText(str);
    }

    public void showConnectionSecure(boolean z) {
        this.b.setCompoundDrawablesWithIntrinsicBounds(z ? R.drawable.smaato_sdk_core_ic_browser_secure_connection : 0, 0, 0, 0);
    }

    public void setPageNavigationBackEnabled(boolean z) {
        this.d.setEnabled(z);
    }

    public void setPageNavigationForwardEnabled(boolean z) {
        this.e.setEnabled(z);
    }

    public void launchExternalBrowser(@NonNull Intent intent) {
        startActivity(intent);
        finish();
    }

    public void redirectToExternalApp(@NonNull Intent intent) {
        startActivity(intent);
    }

    public void showProgressIndicator() {
        this.c.setVisibility(0);
    }

    public void hideProgressIndicator() {
        this.c.setVisibility(4);
    }

    public void updateProgressIndicator(int i) {
        this.c.setProgress(i);
    }

    public void closeBrowser() {
        finish();
    }
}
