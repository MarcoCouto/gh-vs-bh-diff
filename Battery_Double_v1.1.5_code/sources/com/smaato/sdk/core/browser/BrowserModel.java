package com.smaato.sdk.core.browser;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.browser.BrowserModel.Callback;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.webview.BaseWebChromeClient;
import com.smaato.sdk.core.webview.BaseWebChromeClient.WebChromeClientCallback;
import com.smaato.sdk.core.webview.BaseWebViewClient;
import com.smaato.sdk.core.webview.BaseWebViewClient.WebViewClientCallback;

class BrowserModel {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3344a;
    @NonNull
    private final BaseWebViewClient b;
    @NonNull
    private final BaseWebChromeClient c;
    /* access modifiers changed from: private */
    @NonNull
    public final SmaatoCookieManager d;
    /* access modifiers changed from: private */
    @Nullable
    public WebView e;
    /* access modifiers changed from: private */
    @Nullable
    public Callback f;
    @NonNull
    private final WebChromeClientCallback g = new WebChromeClientCallback() {
        public void onProgressChanged(int i) {
            if (BrowserModel.this.f != null) {
                BrowserModel.this.f.onProgressChanged(i);
                if (BrowserModel.this.e != null) {
                    BrowserModel.this.f.onPageNavigationStackChanged(BrowserModel.this.e.canGoBack(), BrowserModel.this.e.canGoForward());
                }
            }
        }
    };
    /* access modifiers changed from: private */
    @Nullable
    public String h;
    @NonNull
    private final WebViewClientCallback i = new WebViewClientCallback() {
        public boolean shouldOverrideUrlLoading(@NonNull String str) {
            if (BrowserModel.this.f != null) {
                return BrowserModel.this.f.shouldOverrideUrlLoading(str);
            }
            return false;
        }

        public void onPageStartedLoading(@NonNull String str) {
            BrowserModel.this.h = str;
            if (BrowserModel.this.f != null) {
                BrowserModel.this.f.onUrlLoadingStarted(str);
            }
        }

        public void onPageFinishedLoading(@NonNull String str) {
            BrowserModel.this.d.forceCookieSync();
        }

        public void onHttpError(@NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse) {
            if (VERSION.SDK_INT >= 21) {
                BrowserModel.this.f3344a.debug(LogDomain.BROWSER, "BrowserModel.onHttpError statusCode=%d", Integer.valueOf(webResourceResponse.getStatusCode()));
            }
            Objects.onNotNull(BrowserModel.this.f, new Consumer(webResourceRequest, webResourceResponse) {
                private final /* synthetic */ WebResourceRequest f$0;
                private final /* synthetic */ WebResourceResponse f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    ((Callback) obj).onHttpError(this.f$0, this.f$1);
                }
            });
        }

        public void onGeneralError(int i, @NonNull String str, @NonNull String str2) {
            BrowserModel.this.f3344a.debug(LogDomain.BROWSER, "BrowserModel.onGeneralError errorCode=%d, description=%s, url=%s", Integer.valueOf(i), str, str2);
            Objects.onNotNull(BrowserModel.this.f, new Consumer(i, str, str2) {
                private final /* synthetic */ int f$0;
                private final /* synthetic */ String f$1;
                private final /* synthetic */ String f$2;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    ((Callback) obj).onGeneralError(this.f$0, this.f$1, this.f$2);
                }
            });
        }

        public void onRenderProcessGone() {
            BrowserModel.this.f3344a.error(LogDomain.BROWSER, "WebView's render process has exited", new Object[0]);
            Objects.onNotNull(BrowserModel.this.f, $$Lambda$ndBoXqtHDXQyXe5fECTsjjJCnos.INSTANCE);
        }
    };

    public interface Callback {
        void onGeneralError(int i, @NonNull String str, @NonNull String str2);

        @TargetApi(23)
        void onHttpError(@NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse);

        void onPageNavigationStackChanged(boolean z, boolean z2);

        void onProgressChanged(@IntRange(from = 0, to = 100) int i);

        @TargetApi(26)
        void onRenderProcessGone();

        void onUrlLoadingStarted(@NonNull String str);

        boolean shouldOverrideUrlLoading(@NonNull String str);
    }

    BrowserModel(@NonNull Logger logger, @NonNull BaseWebViewClient baseWebViewClient, @NonNull BaseWebChromeClient baseWebChromeClient, @NonNull SmaatoCookieManager smaatoCookieManager) {
        this.f3344a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for BrowserModel::new");
        this.b = (BaseWebViewClient) Objects.requireNonNull(baseWebViewClient, "Parameter webViewClient cannot be null for BrowserModel::new");
        this.c = (BaseWebChromeClient) Objects.requireNonNull(baseWebChromeClient, "Parameter webChromeClient cannot be null for BrowserModel::new");
        this.d = (SmaatoCookieManager) Objects.requireNonNull(smaatoCookieManager, "Parameter smaatoCookieManager cannot be null for BrowserModel::BrowserModel");
        baseWebViewClient.setWebViewClientCallback(this.i);
        baseWebChromeClient.setWebChromeClientCallback(this.g);
    }

    public void setWebView(@NonNull WebView webView) {
        this.e = (WebView) Objects.requireNonNull(webView, "Parameter webView cannot be null for BrowserModel::setWebView");
        webView.setWebViewClient(this.b);
        webView.setWebChromeClient(this.c);
        this.d.setupCookiePolicy(webView);
    }

    public void setBrowserModelCallback(@Nullable Callback callback) {
        this.f = callback;
    }

    public void load(@NonNull String str) {
        Objects.requireNonNull(str, "Parameter url cannot be null for BrowserModel::load");
        this.h = str;
        ((WebView) Objects.requireNonNull(this.e)).loadUrl(str);
    }

    public void reload() {
        ((WebView) Objects.requireNonNull(this.e)).reload();
    }

    public void goBack() {
        ((WebView) Objects.requireNonNull(this.e)).goBack();
    }

    public void goForward() {
        ((WebView) Objects.requireNonNull(this.e)).goForward();
    }

    @Nullable
    public String getCurrentUrl() {
        if (this.h == null) {
            this.f3344a.error(LogDomain.BROWSER, "Internal error: loadUrl() was not called", new Object[0]);
        }
        return this.h;
    }

    public void start() {
        this.d.startSync();
    }

    public void resume() {
        ((WebView) Objects.requireNonNull(this.e)).onResume();
    }

    public void pause() {
        ((WebView) Objects.requireNonNull(this.e)).onPause();
    }

    public void stop() {
        this.d.stopSync();
        this.d.forceCookieSync();
    }
}
