package com.smaato.sdk.core.browser;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.browser.BrowserModel.Callback;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.UrlCreator;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;

class BrowserPresenter {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3347a;
    @NonNull
    private final BrowserModel b;
    @NonNull
    private final UrlCreator c;
    /* access modifiers changed from: private */
    @NonNull
    public final LinkResolver d;
    @NonNull
    private final ClipboardManager e;
    /* access modifiers changed from: private */
    @Nullable
    public BrowserView f;
    @NonNull
    private final Callback g = new Callback() {
        public void onGeneralError(int i, @NonNull String str, @NonNull String str2) {
        }

        @TargetApi(23)
        public void onHttpError(@NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse) {
        }

        public boolean shouldOverrideUrlLoading(@NonNull String str) {
            Either findExternalAppForUrl = BrowserPresenter.this.d.findExternalAppForUrl(str);
            if (findExternalAppForUrl == null) {
                return false;
            }
            Objects.onNotNull(findExternalAppForUrl.left(), new Consumer() {
                public final void accept(Object obj) {
                    AnonymousClass1.this.a((Intent) obj);
                }
            });
            Objects.onNotNull(findExternalAppForUrl.right(), new Consumer() {
                public final void accept(Object obj) {
                    AnonymousClass1.this.a((String) obj);
                }
            });
            return true;
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Intent intent) {
            Objects.onNotNull(BrowserPresenter.this.f, new Consumer(intent) {
                private final /* synthetic */ Intent f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AnonymousClass1.this.a(this.f$1, (BrowserView) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Intent intent, BrowserView browserView) {
            BrowserPresenter.this.f3347a.debug(LogDomain.BROWSER, "Redirecting to the external app: %s", intent.toString());
            browserView.redirectToExternalApp(intent);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(String str) {
            Objects.onNotNull(BrowserPresenter.this.f, new Consumer(str) {
                private final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AnonymousClass1.this.a(this.f$1, (BrowserView) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(String str, BrowserView browserView) {
            BrowserPresenter.this.f3347a.debug(LogDomain.BROWSER, "Redirecting to other url: %s", str);
            BrowserPresenter.this.loadUrl(str);
        }

        public void onUrlLoadingStarted(@NonNull String str) {
            BrowserPresenter.a(BrowserPresenter.this, str);
        }

        public void onPageNavigationStackChanged(boolean z, boolean z2) {
            BrowserPresenter.a(BrowserPresenter.this, z, z2);
        }

        public void onProgressChanged(int i) {
            if (BrowserPresenter.this.f != null) {
                if (i == 100) {
                    BrowserPresenter.this.f.hideProgressIndicator();
                    return;
                }
                BrowserPresenter.this.f.updateProgressIndicator(i);
                BrowserPresenter.this.f.showProgressIndicator();
            }
        }

        @TargetApi(26)
        public void onRenderProcessGone() {
            Objects.onNotNull(BrowserPresenter.this.f, $$Lambda$DaQydZ6XkRTJg5P6T_WjSqSHomg.INSTANCE);
        }
    };

    BrowserPresenter(@NonNull Logger logger, @NonNull BrowserModel browserModel, @NonNull UrlCreator urlCreator, @NonNull LinkResolver linkResolver, @NonNull ClipboardManager clipboardManager) {
        this.f3347a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for BrowserPresenter::new");
        this.b = (BrowserModel) Objects.requireNonNull(browserModel, "Parameter browserModel cannot be null for BrowserPresenter::new");
        this.c = (UrlCreator) Objects.requireNonNull(urlCreator, "Parameter urlCreator cannot be null for BrowserPresenter::new");
        this.d = (LinkResolver) Objects.requireNonNull(linkResolver, "Parameter linkResolver cannot be null for BrowserPresenter::new");
        this.e = (ClipboardManager) Objects.requireNonNull(clipboardManager, "Parameter clipboardManager cannot be null for BrowserPresenter::new");
        browserModel.setBrowserModelCallback(this.g);
    }

    public void initWithView(@NonNull BrowserView browserView, @NonNull WebView webView) {
        this.f = (BrowserView) Objects.requireNonNull(browserView, "Parameter browserView cannot be null for BrowserPresenter::initWithView");
        Objects.requireNonNull(webView, "Parameter webView cannot be null for BrowserPresenter::initWithView");
        this.b.setWebView(webView);
    }

    public void onStart() {
        this.b.start();
    }

    public void onResume() {
        this.b.resume();
    }

    public void onPause() {
        this.b.pause();
    }

    public void onStop() {
        this.b.stop();
    }

    public void dropView() {
        this.f = null;
    }

    public void loadUrl(@NonNull String str) {
        this.b.load(str);
    }

    public void onReloadClicked() {
        this.b.reload();
    }

    public void onPageNavigationBackClicked() {
        this.b.goBack();
    }

    public void onPageNavigationForwardClicked() {
        this.b.goForward();
    }

    public void onOpenExternalBrowserClicked() {
        if (this.f != null) {
            String currentUrl = this.b.getCurrentUrl();
            if (currentUrl != null) {
                Intent externalBrowserIntent = this.d.getExternalBrowserIntent(currentUrl);
                if (externalBrowserIntent == null) {
                    this.f3347a.debug(LogDomain.BROWSER, "No external browser app found", new Object[0]);
                    externalBrowserIntent = this.d.getExternalBrowserAppInstallIntent(currentUrl);
                    if (externalBrowserIntent == null) {
                        this.f3347a.debug(LogDomain.BROWSER, "No store app found", new Object[0]);
                        return;
                    } else {
                        this.f3347a.debug(LogDomain.BROWSER, "Redirecting to the store app: %s", externalBrowserIntent.toString());
                    }
                } else {
                    this.f3347a.debug(LogDomain.BROWSER, "Redirecting to the external browser: %s", externalBrowserIntent.toString());
                }
                this.f.launchExternalBrowser(externalBrowserIntent);
            }
        }
    }

    public void onCopyHostnameClicked() {
        this.e.setPrimaryClip(ClipData.newPlainText(null, this.b.getCurrentUrl()));
        this.f3347a.debug(LogDomain.BROWSER, "Link copied", new Object[0]);
    }

    static /* synthetic */ void a(BrowserPresenter browserPresenter, String str) {
        if (browserPresenter.f != null) {
            browserPresenter.f.showHostname(browserPresenter.c.extractHostname(str));
            browserPresenter.f.showConnectionSecure(browserPresenter.c.isSecureScheme(browserPresenter.c.extractScheme(str)));
        }
    }

    static /* synthetic */ void a(BrowserPresenter browserPresenter, boolean z, boolean z2) {
        if (browserPresenter.f != null) {
            browserPresenter.f.setPageNavigationBackEnabled(z);
            browserPresenter.f.setPageNavigationForwardEnabled(z2);
        }
    }
}
