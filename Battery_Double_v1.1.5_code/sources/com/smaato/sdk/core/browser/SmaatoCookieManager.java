package com.smaato.sdk.core.browser;

import android.os.Build.VERSION;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;

public class SmaatoCookieManager {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final CookieManager f3350a;
    @NonNull
    private final CookieSyncManagerHolder b;

    public SmaatoCookieManager(@NonNull CookieManager cookieManager, @NonNull CookieSyncManagerHolder cookieSyncManagerHolder) {
        this.f3350a = (CookieManager) Objects.requireNonNull(cookieManager, "Parameter cookieManager cannot be null for SmaatoCookieManager::new");
        this.b = (CookieSyncManagerHolder) Objects.requireNonNull(cookieSyncManagerHolder, "Parameter cookieSyncManagerHolder cannot be null for SmaatoCookieManager::new");
    }

    public void setupCookiePolicy(@NonNull WebView webView) {
        Objects.requireNonNull(webView, "Parameter webView cannot be null for SmaatoCookieManager::setupCookiePolicy");
        if (VERSION.SDK_INT >= 21) {
            this.f3350a.setAcceptThirdPartyCookies(webView, true);
        }
    }

    public void forceCookieSync() {
        if (VERSION.SDK_INT < 21) {
            a($$Lambda$3mY5KtXw7WSTiZjpr5J4GiRUs4w.INSTANCE);
        } else {
            this.f3350a.flush();
        }
    }

    public void startSync() {
        if (VERSION.SDK_INT < 21) {
            a($$Lambda$mIJ4sY492qGdEvdQ5h6015vXGI.INSTANCE);
        }
    }

    public void stopSync() {
        if (VERSION.SDK_INT < 21) {
            a($$Lambda$o5NiJHDCL25JDPEJdVszPKwKNQ.INSTANCE);
        }
    }

    private void a(@NonNull Consumer<CookieSyncManager> consumer) {
        CookieSyncManager cookieSyncManager = this.b.getCookieSyncManager();
        if (cookieSyncManager != null) {
            consumer.accept(cookieSyncManager);
            return;
        }
        throw new IllegalStateException("CookieSyncManager is expected to be present on API < 21");
    }
}
