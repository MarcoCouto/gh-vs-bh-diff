package com.smaato.sdk.core.browser;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.browser.-$$Lambda$DiBrowserLayer$FoLc0ytLaJ-GA0OHNzlUqKfNWm8 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiBrowserLayer$FoLc0ytLaJGA0OHNzlUqKfNWm8 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiBrowserLayer$FoLc0ytLaJGA0OHNzlUqKfNWm8 INSTANCE = new $$Lambda$DiBrowserLayer$FoLc0ytLaJGA0OHNzlUqKfNWm8();

    private /* synthetic */ $$Lambda$DiBrowserLayer$FoLc0ytLaJGA0OHNzlUqKfNWm8() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiBrowserLayer.f(diConstructor);
    }
}
