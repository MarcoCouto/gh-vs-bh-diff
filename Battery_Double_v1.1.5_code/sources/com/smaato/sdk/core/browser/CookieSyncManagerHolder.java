package com.smaato.sdk.core.browser;

import android.app.Application;
import android.os.Build.VERSION;
import android.webkit.CookieSyncManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class CookieSyncManagerHolder {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final CookieSyncManager f3349a;

    public CookieSyncManagerHolder(@NonNull Application application) {
        if (VERSION.SDK_INT < 21) {
            this.f3349a = CookieSyncManager.createInstance(application);
        } else {
            this.f3349a = null;
        }
    }

    @Nullable
    public final CookieSyncManager getCookieSyncManager() {
        return this.f3349a;
    }
}
