package com.smaato.sdk.core.analytics;

import android.app.Application;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;

public interface ViewabilityService {
    @NonNull
    NativeViewabilityTracker getNativeTracker();

    @NonNull
    VideoViewabilityTracker getVideoTracker();

    @NonNull
    WebViewViewabilityTracker getWebViewTracker();

    @MainThread
    void setup(@NonNull Application application, @NonNull Logger logger);
}
