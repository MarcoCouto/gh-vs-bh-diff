package com.smaato.sdk.core.analytics;

import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.collections.Lists;
import java.util.List;

final class NativeViewabilityTrackerComposite implements NativeViewabilityTracker {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<NativeViewabilityTracker> f3321a;

    NativeViewabilityTrackerComposite(@NonNull List<NativeViewabilityTracker> list) {
        this.f3321a = Lists.toImmutableList(list);
    }

    @MainThread
    public final void registerAdView(@NonNull View view) {
        Threads.ensureMainThread();
        for (NativeViewabilityTracker registerAdView : this.f3321a) {
            registerAdView.registerAdView(view);
        }
    }

    @MainThread
    public final void startTracking() {
        Threads.ensureMainThread();
        for (NativeViewabilityTracker startTracking : this.f3321a) {
            startTracking.startTracking();
        }
    }

    @MainThread
    public final void stopTracking() {
        Threads.ensureMainThread();
        for (NativeViewabilityTracker stopTracking : this.f3321a) {
            stopTracking.stopTracking();
        }
    }

    @MainThread
    public final void registerFriendlyObstruction(@NonNull View view) {
        Threads.ensureMainThread();
        for (NativeViewabilityTracker registerFriendlyObstruction : this.f3321a) {
            registerFriendlyObstruction.registerFriendlyObstruction(view);
        }
    }

    @MainThread
    public final void removeFriendlyObstruction(@NonNull View view) {
        Threads.ensureMainThread();
        for (NativeViewabilityTracker removeFriendlyObstruction : this.f3321a) {
            removeFriendlyObstruction.removeFriendlyObstruction(view);
        }
    }
}
