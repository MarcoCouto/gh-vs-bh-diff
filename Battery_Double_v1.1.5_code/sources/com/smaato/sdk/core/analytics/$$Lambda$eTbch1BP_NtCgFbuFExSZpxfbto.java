package com.smaato.sdk.core.analytics;

import com.smaato.sdk.core.util.fi.NullableFunction;

/* renamed from: com.smaato.sdk.core.analytics.-$$Lambda$eTbch1BP_NtCgFbuFExSZpxfbto reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$eTbch1BP_NtCgFbuFExSZpxfbto implements NullableFunction {
    public static final /* synthetic */ $$Lambda$eTbch1BP_NtCgFbuFExSZpxfbto INSTANCE = new $$Lambda$eTbch1BP_NtCgFbuFExSZpxfbto();

    private /* synthetic */ $$Lambda$eTbch1BP_NtCgFbuFExSZpxfbto() {
    }

    public final Object apply(Object obj) {
        return ((ViewabilityPlugin) obj).getWebViewTracker();
    }
}
