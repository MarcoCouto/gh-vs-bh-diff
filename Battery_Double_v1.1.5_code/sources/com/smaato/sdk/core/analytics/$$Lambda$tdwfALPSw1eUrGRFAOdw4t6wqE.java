package com.smaato.sdk.core.analytics;

import com.smaato.sdk.core.util.fi.NullableFunction;

/* renamed from: com.smaato.sdk.core.analytics.-$$Lambda$tdw-fALPSw1eUrGRFAOdw4t6wqE reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$tdwfALPSw1eUrGRFAOdw4t6wqE implements NullableFunction {
    public static final /* synthetic */ $$Lambda$tdwfALPSw1eUrGRFAOdw4t6wqE INSTANCE = new $$Lambda$tdwfALPSw1eUrGRFAOdw4t6wqE();

    private /* synthetic */ $$Lambda$tdwfALPSw1eUrGRFAOdw4t6wqE() {
    }

    public final Object apply(Object obj) {
        return ((ViewabilityPlugin) obj).getVideoTracker();
    }
}
