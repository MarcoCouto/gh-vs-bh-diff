package com.smaato.sdk.core.analytics;

import android.app.Application;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.ArrayList;
import java.util.ServiceLoader;

public final class DiAnalyticsLayer {
    private DiAnalyticsLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry(@NonNull Application application) {
        return DiRegistry.of(new Consumer(application) {
            private final /* synthetic */ Application f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((DiRegistry) obj).registerSingletonFactory(Analytics.class, new ClassFactory(this.f$0) {
                    private final /* synthetic */ Application f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final Object get(DiConstructor diConstructor) {
                        return DiAnalyticsLayer.a(this.f$0, diConstructor);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Analytics a(Application application, DiConstructor diConstructor) {
        ArrayList arrayList = new ArrayList();
        for (ViewabilityPlugin add : ServiceLoader.load(ViewabilityPlugin.class, application.getClassLoader())) {
            arrayList.add(add);
        }
        Analytics analytics = new Analytics(arrayList);
        analytics.setup(application, DiLogLayer.getLoggerFrom(diConstructor));
        return analytics;
    }
}
