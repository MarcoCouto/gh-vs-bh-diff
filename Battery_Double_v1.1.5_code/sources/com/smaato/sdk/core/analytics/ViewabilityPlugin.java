package com.smaato.sdk.core.analytics;

import androidx.annotation.NonNull;

public interface ViewabilityPlugin extends ViewabilityService {
    @NonNull
    String getName();
}
