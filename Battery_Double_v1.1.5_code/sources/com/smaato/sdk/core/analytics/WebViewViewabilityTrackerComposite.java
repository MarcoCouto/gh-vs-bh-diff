package com.smaato.sdk.core.analytics;

import android.view.View;
import android.webkit.WebView;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.collections.Lists;
import java.util.List;

final class WebViewViewabilityTrackerComposite implements WebViewViewabilityTracker {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<WebViewViewabilityTracker> f3323a;

    WebViewViewabilityTrackerComposite(@NonNull List<WebViewViewabilityTracker> list) {
        this.f3323a = Lists.toImmutableList(list);
    }

    @MainThread
    public final void registerAdView(@NonNull WebView webView) {
        Threads.ensureMainThread();
        for (WebViewViewabilityTracker registerAdView : this.f3323a) {
            registerAdView.registerAdView(webView);
        }
    }

    @MainThread
    public final void updateAdView(@NonNull WebView webView) {
        Threads.ensureMainThread();
        for (WebViewViewabilityTracker updateAdView : this.f3323a) {
            updateAdView.updateAdView(webView);
        }
    }

    @MainThread
    public final void startTracking() {
        Threads.ensureMainThread();
        for (WebViewViewabilityTracker startTracking : this.f3323a) {
            startTracking.startTracking();
        }
    }

    @MainThread
    public final void stopTracking() {
        Threads.ensureMainThread();
        for (WebViewViewabilityTracker stopTracking : this.f3323a) {
            stopTracking.stopTracking();
        }
    }

    @MainThread
    public final void registerFriendlyObstruction(@NonNull View view) {
        Threads.ensureMainThread();
        for (WebViewViewabilityTracker registerFriendlyObstruction : this.f3323a) {
            registerFriendlyObstruction.registerFriendlyObstruction(view);
        }
    }

    @MainThread
    public final void removeFriendlyObstruction(@NonNull View view) {
        Threads.ensureMainThread();
        for (WebViewViewabilityTracker removeFriendlyObstruction : this.f3323a) {
            removeFriendlyObstruction.removeFriendlyObstruction(view);
        }
    }
}
