package com.smaato.sdk.core.analytics;

import android.app.Application;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.collections.Lists;
import java.util.Collections;
import java.util.List;

public final class Analytics implements ViewabilityService {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<ViewabilityPlugin> f3320a;

    Analytics(@NonNull List<ViewabilityPlugin> list) {
        this.f3320a = Collections.unmodifiableList(list);
    }

    @NonNull
    public final List<String> getConnectedPluginNames() {
        return Lists.map(this.f3320a, $$Lambda$7BHA8E0EoAkYhBuhF04jzRqNiU.INSTANCE);
    }

    @MainThread
    public final void setup(@NonNull Application application, @NonNull Logger logger) {
        Threads.runOnUi(new Runnable(application, logger) {
            private final /* synthetic */ Application f$1;
            private final /* synthetic */ Logger f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                Analytics.this.a(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Application application, Logger logger) {
        for (ViewabilityPlugin upVar : this.f3320a) {
            upVar.setup(application, logger);
        }
    }

    @NonNull
    public final WebViewViewabilityTracker getWebViewTracker() {
        return new WebViewViewabilityTrackerComposite(Lists.map(this.f3320a, $$Lambda$eTbch1BP_NtCgFbuFExSZpxfbto.INSTANCE));
    }

    @NonNull
    public final NativeViewabilityTracker getNativeTracker() {
        return new NativeViewabilityTrackerComposite(Lists.map(this.f3320a, $$Lambda$5Ed79fmCtO_iWZgcL2bgHiL68UE.INSTANCE));
    }

    @NonNull
    public final VideoViewabilityTracker getVideoTracker() {
        return new VideoViewabilityTrackerComposite(Lists.map(this.f3320a, $$Lambda$tdwfALPSw1eUrGRFAOdw4t6wqE.INSTANCE));
    }
}
