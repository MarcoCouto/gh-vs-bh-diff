package com.smaato.sdk.core.analytics;

import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.collections.Lists;
import java.util.List;

final class VideoViewabilityTrackerComposite implements VideoViewabilityTracker {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<VideoViewabilityTracker> f3322a;

    VideoViewabilityTrackerComposite(@NonNull List<VideoViewabilityTracker> list) {
        this.f3322a = Lists.toImmutableList(list);
    }

    @MainThread
    public final void startTracking() {
        Threads.ensureMainThread();
        for (VideoViewabilityTracker startTracking : this.f3322a) {
            startTracking.startTracking();
        }
    }

    @MainThread
    public final void stopTracking() {
        Threads.ensureMainThread();
        for (VideoViewabilityTracker stopTracking : this.f3322a) {
            stopTracking.stopTracking();
        }
    }

    @MainThread
    public final void registerFriendlyObstruction(@NonNull View view) {
        Threads.ensureMainThread();
        for (VideoViewabilityTracker registerFriendlyObstruction : this.f3322a) {
            registerFriendlyObstruction.registerFriendlyObstruction(view);
        }
    }

    @MainThread
    public final void removeFriendlyObstruction(@NonNull View view) {
        Threads.ensureMainThread();
        for (VideoViewabilityTracker removeFriendlyObstruction : this.f3322a) {
            removeFriendlyObstruction.removeFriendlyObstruction(view);
        }
    }
}
