package com.smaato.sdk.core.analytics;

import com.smaato.sdk.core.util.fi.NullableFunction;

/* renamed from: com.smaato.sdk.core.analytics.-$$Lambda$5Ed79fmCtO_iWZgcL2bgHiL68UE reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$5Ed79fmCtO_iWZgcL2bgHiL68UE implements NullableFunction {
    public static final /* synthetic */ $$Lambda$5Ed79fmCtO_iWZgcL2bgHiL68UE INSTANCE = new $$Lambda$5Ed79fmCtO_iWZgcL2bgHiL68UE();

    private /* synthetic */ $$Lambda$5Ed79fmCtO_iWZgcL2bgHiL68UE() {
    }

    public final Object apply(Object obj) {
        return ((ViewabilityPlugin) obj).getNativeTracker();
    }
}
