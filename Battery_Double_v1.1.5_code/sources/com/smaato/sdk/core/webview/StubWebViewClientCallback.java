package com.smaato.sdk.core.webview;

import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.webview.BaseWebViewClient.WebViewClientCallback;

public class StubWebViewClientCallback implements WebViewClientCallback {
    public void onGeneralError(int i, @NonNull String str, @NonNull String str2) {
    }

    public void onHttpError(@NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse) {
    }

    public void onPageFinishedLoading(@NonNull String str) {
    }

    public void onPageStartedLoading(@NonNull String str) {
    }

    public void onRenderProcessGone() {
    }

    public boolean shouldOverrideUrlLoading(@NonNull String str) {
        return false;
    }
}
