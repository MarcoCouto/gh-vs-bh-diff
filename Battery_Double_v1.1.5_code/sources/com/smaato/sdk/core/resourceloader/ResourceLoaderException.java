package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.ResourceLoader.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkComponentException;

public class ResourceLoaderException extends Exception implements SdkComponentException<Error> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Error f3444a;
    @NonNull
    private final Exception b;

    public ResourceLoaderException(@NonNull Error error, @NonNull Exception exc) {
        this.f3444a = (Error) Objects.requireNonNull(error);
        this.b = (Exception) Objects.requireNonNull(exc);
    }

    @NonNull
    public Exception getReason() {
        return this.b;
    }

    @NonNull
    public Error getErrorType() {
        return this.f3444a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ResourceLoaderException { errorType = ");
        sb.append(this.f3444a);
        sb.append(", reason = ");
        sb.append(this.b);
        sb.append(" }");
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ResourceLoaderException resourceLoaderException = (ResourceLoaderException) obj;
        return this.f3444a == resourceLoaderException.f3444a && Objects.equals(this.b, resourceLoaderException.b);
    }

    public int hashCode() {
        return Objects.hash(this.f3444a, this.b);
    }
}
