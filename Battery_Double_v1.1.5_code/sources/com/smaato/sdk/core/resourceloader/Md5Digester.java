package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.smaato.sdk.core.util.Objects;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Digester {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Charset f3437a;
    @NonNull
    private final HexEncoder b;

    public Md5Digester(@NonNull Charset charset, @NonNull HexEncoder hexEncoder) {
        this.f3437a = (Charset) Objects.requireNonNull(charset);
        this.b = (HexEncoder) Objects.requireNonNull(hexEncoder);
    }

    @NonNull
    public String md5Hex(@NonNull String str) throws NoSuchAlgorithmException {
        Objects.requireNonNull(str);
        return this.b.encodeHexString(a().digest(str.getBytes(this.f3437a)));
    }

    @NonNull
    private synchronized MessageDigest a() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(CommonMD5.TAG);
    }
}
