package com.smaato.sdk.core.resourceloader;

import android.app.Application;
import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.util.Objects;
import java.nio.charset.Charset;

public final class DiResourceLoaderLayer {
    private DiResourceLoaderLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiResourceLoaderLayer$9DqWEdayB9ZzCxE1UyrRh9DhGT8.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(Md5Digester.class, $$Lambda$DiResourceLoaderLayer$C80H6zCMdetAIJjVS3Wb2cIkZAE.INSTANCE);
        diRegistry.registerFactory("resource_loader_di_layer", Charset.class, $$Lambda$DiResourceLoaderLayer$LceaUHu_ApAXFaLDEO1DB2FfyOw.INSTANCE);
        diRegistry.registerFactory(HexEncoder.class, $$Lambda$DiResourceLoaderLayer$uU0sj29AP37jdIgP1iZKt_C605g.INSTANCE);
        diRegistry.registerFactory(BaseStoragePersistingStrategyFileUtils.class, $$Lambda$DiResourceLoaderLayer$TLA0BhocxfOMAMn7DsITcS5WuU.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Md5Digester d(DiConstructor diConstructor) {
        return new Md5Digester(Charset.forName("UTF-8"), (HexEncoder) diConstructor.get(HexEncoder.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ HexEncoder b(DiConstructor diConstructor) {
        return new HexEncoder();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BaseStoragePersistingStrategyFileUtils a(DiConstructor diConstructor) {
        return new BaseStoragePersistingStrategyFileUtils(DiLogLayer.getLoggerFrom(diConstructor), (Context) diConstructor.get(Application.class));
    }

    @NonNull
    public static BaseStoragePersistingStrategyFileUtils getBaseStoragePersistingStrategyFileUtils(@NonNull DiConstructor diConstructor) {
        return (BaseStoragePersistingStrategyFileUtils) diConstructor.get(BaseStoragePersistingStrategyFileUtils.class);
    }

    @NonNull
    public static Md5Digester getMd5Digester(@NonNull DiConstructor diConstructor) {
        Objects.requireNonNull(diConstructor);
        return (Md5Digester) diConstructor.get(Md5Digester.class);
    }
}
