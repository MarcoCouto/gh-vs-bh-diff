package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

public interface NetworkResourceStreamPreparationStrategy {
    @NonNull
    InputStream prepare(@NonNull URLConnection uRLConnection) throws IOException;
}
