package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkHttpRequest.Builder;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.IoFunction;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.network.execution.NetworkLayerException;
import com.smaato.sdk.core.network.execution.TaskStepResult;
import com.smaato.sdk.core.util.Objects;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;

public class ResourceLoadingNetworkTaskCreator<OutputResourceType> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3445a;
    @NonNull
    private final NetworkActions b;
    @NonNull
    private final ErrorMapper<NetworkLayerException> c;
    @NonNull
    private final ExecutorService d;
    @NonNull
    private final NetworkResourceStreamPreparationStrategy e;

    public ResourceLoadingNetworkTaskCreator(@NonNull Logger logger, @NonNull NetworkActions networkActions, @NonNull ErrorMapper<NetworkLayerException> errorMapper, @NonNull ExecutorService executorService, @NonNull NetworkResourceStreamPreparationStrategy networkResourceStreamPreparationStrategy) {
        this.f3445a = (Logger) Objects.requireNonNull(logger);
        this.b = (NetworkActions) Objects.requireNonNull(networkActions);
        this.c = (ErrorMapper) Objects.requireNonNull(errorMapper);
        this.d = (ExecutorService) Objects.requireNonNull(executorService);
        this.e = (NetworkResourceStreamPreparationStrategy) Objects.requireNonNull(networkResourceStreamPreparationStrategy);
    }

    @NonNull
    public Task createTask(@NonNull String str, @NonNull SomaApiContext somaApiContext, @NonNull IoFunction<InputStream, TaskStepResult<OutputResourceType, Exception>> ioFunction, @NonNull Listener<OutputResourceType, NetworkLayerException> listener) {
        Objects.requireNonNull(somaApiContext);
        Objects.requireNonNull(ioFunction);
        Objects.requireNonNull(listener);
        ResourceLoadingTask resourceLoadingTask = new ResourceLoadingTask(this.f3445a, this.b, new Builder().setMethod(Method.GET).setUrl(str).build(), somaApiContext, this.c, this.d, this.e, ioFunction, listener);
        return resourceLoadingTask;
    }
}
