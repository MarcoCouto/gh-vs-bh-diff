package com.smaato.sdk.core.resourceloader;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.nio.charset.Charset;

/* renamed from: com.smaato.sdk.core.resourceloader.-$$Lambda$DiResourceLoaderLayer$LceaUHu_ApAXFaLDEO1DB2FfyOw reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiResourceLoaderLayer$LceaUHu_ApAXFaLDEO1DB2FfyOw implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiResourceLoaderLayer$LceaUHu_ApAXFaLDEO1DB2FfyOw INSTANCE = new $$Lambda$DiResourceLoaderLayer$LceaUHu_ApAXFaLDEO1DB2FfyOw();

    private /* synthetic */ $$Lambda$DiResourceLoaderLayer$LceaUHu_ApAXFaLDEO1DB2FfyOw() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Charset.forName("UTF-8");
    }
}
