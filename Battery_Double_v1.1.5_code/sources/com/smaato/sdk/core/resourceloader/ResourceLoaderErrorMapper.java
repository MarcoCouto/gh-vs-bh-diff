package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.network.execution.NetworkLayerException;
import com.smaato.sdk.core.resourceloader.ResourceLoader.Error;
import com.smaato.sdk.core.util.Objects;

public final class ResourceLoaderErrorMapper {
    private ResourceLoaderErrorMapper() {
    }

    @NonNull
    public static ResourceLoaderException map(@NonNull NetworkLayerException networkLayerException) {
        Error error;
        Error error2;
        Objects.requireNonNull(networkLayerException);
        if (networkLayerException.getReason() instanceof PersistingStrategyException) {
            PersistingStrategyException persistingStrategyException = (PersistingStrategyException) networkLayerException.getReason();
            switch (persistingStrategyException.getErrorType()) {
                case IO_ERROR:
                    error2 = Error.IO_ERROR;
                    break;
                case RESOURCE_EXPIRED:
                    error2 = Error.RESOURCE_EXPIRED;
                    break;
                case GENERIC:
                    error2 = Error.GENERIC;
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{PersistingStrategy.Error.class.getSimpleName(), persistingStrategyException}));
            }
            return new ResourceLoaderException(error2, persistingStrategyException);
        }
        switch (networkLayerException.getErrorType()) {
            case CANCELLED:
                error = Error.CANCELLED;
                break;
            case TIMEOUT:
                error = Error.NETWORK_TIMEOUT;
                break;
            case IO_ERROR:
                error = Error.IO_ERROR;
                break;
            case GENERIC:
                error = Error.GENERIC;
                break;
            case IO_TOO_MANY_REDIRECTS:
                error = Error.NETWORK_IO_TOO_MANY_REDIRECTS;
                break;
            case NO_NETWORK_CONNECTION:
                error = Error.NETWORK_NO_CONNECTION;
                break;
            default:
                throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{Error.class.getSimpleName(), networkLayerException}));
        }
        return new ResourceLoaderException(error, networkLayerException);
    }
}
