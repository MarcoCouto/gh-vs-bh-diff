package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.IoFunction;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.network.execution.NetworkLayerException;
import com.smaato.sdk.core.network.execution.NetworkTask;
import com.smaato.sdk.core.network.execution.TaskStepResult;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Function;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.concurrent.ExecutorService;

class ResourceLoadingTask<OutputResourceType> extends NetworkTask<OutputResourceType> {
    ResourceLoadingTask(@NonNull Logger logger, @NonNull NetworkActions networkActions, @NonNull NetworkRequest networkRequest, @NonNull SomaApiContext somaApiContext, @NonNull ErrorMapper<NetworkLayerException> errorMapper, @NonNull ExecutorService executorService, @NonNull NetworkResourceStreamPreparationStrategy networkResourceStreamPreparationStrategy, @NonNull IoFunction<InputStream, TaskStepResult<OutputResourceType, Exception>> ioFunction, @NonNull Listener<OutputResourceType, NetworkLayerException> listener) {
        ExecutorService executorService2 = (ExecutorService) Objects.requireNonNull(executorService);
        $$Lambda$ResourceLoadingTask$WKRr2udGvj18pnNefvIbT6O3Xk r1 = new Function(networkRequest, somaApiContext, networkResourceStreamPreparationStrategy, ioFunction, logger, errorMapper, listener) {
            private final /* synthetic */ NetworkRequest f$1;
            private final /* synthetic */ SomaApiContext f$2;
            private final /* synthetic */ NetworkResourceStreamPreparationStrategy f$3;
            private final /* synthetic */ IoFunction f$4;
            private final /* synthetic */ Logger f$5;
            private final /* synthetic */ ErrorMapper f$6;
            private final /* synthetic */ Listener f$7;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
            }

            public final Object apply(Object obj) {
                return ResourceLoadingTask.createRunnable((NetworkTask) Objects.requireNonNull((NetworkTask) obj), (NetworkActions) Objects.requireNonNull(NetworkActions.this), (NetworkRequest) Objects.requireNonNull(this.f$1), (SomaApiContext) Objects.requireNonNull(this.f$2), new IoFunction(this.f$4) {
                    private final /* synthetic */ IoFunction f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final Object apply(Object obj) {
                        return ResourceLoadingTask.a(NetworkResourceStreamPreparationStrategy.this, this.f$1, (HttpURLConnection) obj);
                    }
                }, ResourceLoadingTask.standardResultHandler((Logger) Objects.requireNonNull(this.f$5), (ErrorMapper) Objects.requireNonNull(this.f$6), (NetworkTask) obj, (Listener) Objects.requireNonNull(this.f$7)));
            }
        };
        super(executorService2, r1);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ TaskStepResult a(NetworkResourceStreamPreparationStrategy networkResourceStreamPreparationStrategy, IoFunction ioFunction, HttpURLConnection httpURLConnection) throws IOException {
        Throwable th;
        if (httpURLConnection.getResponseCode() != 200) {
            return TaskStepResult.error(new IOException());
        }
        InputStream prepare = ((NetworkResourceStreamPreparationStrategy) Objects.requireNonNull(networkResourceStreamPreparationStrategy)).prepare(httpURLConnection);
        try {
            TaskStepResult taskStepResult = (TaskStepResult) ((IoFunction) Objects.requireNonNull(ioFunction)).apply(prepare);
            if (prepare != null) {
                prepare.close();
            }
            return taskStepResult;
        } catch (Throwable unused) {
        }
        throw th;
    }
}
