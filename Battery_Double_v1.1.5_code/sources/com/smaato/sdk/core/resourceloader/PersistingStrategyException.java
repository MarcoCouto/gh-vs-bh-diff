package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.PersistingStrategy.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkComponentException;
import java.io.IOException;

public class PersistingStrategyException extends IOException implements SdkComponentException<Error> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Error f3439a;
    @NonNull
    private final Exception b;

    public PersistingStrategyException(@NonNull Error error, @NonNull Exception exc) {
        this.f3439a = (Error) Objects.requireNonNull(error);
        this.b = (Exception) Objects.requireNonNull(exc);
    }

    @NonNull
    public Exception getReason() {
        return this.b;
    }

    @NonNull
    public Error getErrorType() {
        return this.f3439a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PersistingStrategyException { errorType = ");
        sb.append(this.f3439a);
        sb.append(", reason = ");
        sb.append(this.b);
        sb.append(" }");
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PersistingStrategyException persistingStrategyException = (PersistingStrategyException) obj;
        return this.f3439a == persistingStrategyException.f3439a && Objects.equals(this.b, persistingStrategyException.b);
    }

    public int hashCode() {
        return Objects.hash(this.f3439a, this.b);
    }
}
