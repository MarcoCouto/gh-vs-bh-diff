package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.ad.ExpirationTimestamp;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.execution.IoFunction;
import com.smaato.sdk.core.network.execution.NetworkLayerException;
import com.smaato.sdk.core.network.execution.TaskStepResult;
import com.smaato.sdk.core.resourceloader.ResourceLoader.Listener;
import com.smaato.sdk.core.util.Objects;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;

public class ResourceLoader<PersistedResourceType, OutputResourceType> {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3440a;
    @NonNull
    private final ResourceLoadingNetworkTaskCreator<OutputResourceType> b;
    @NonNull
    private final ExecutorService c;
    @NonNull
    private final PersistingStrategy<PersistedResourceType> d;
    @NonNull
    private final ResourceTransformer<PersistedResourceType, OutputResourceType> e;

    public enum Error {
        CANCELLED,
        INVALID_RESPONSE,
        RESOURCE_EXPIRED,
        GENERIC,
        IO_ERROR,
        NETWORK_NO_CONNECTION,
        NETWORK_TIMEOUT,
        NETWORK_GENERIC,
        NETWORK_IO_TOO_MANY_REDIRECTS
    }

    public interface Listener<OutputResourceType> {
        void onFailure(@NonNull ResourceLoaderException resourceLoaderException);

        void onResourceLoaded(@NonNull OutputResourceType outputresourcetype);
    }

    public ResourceLoader(@NonNull Logger logger, @NonNull ResourceLoadingNetworkTaskCreator<OutputResourceType> resourceLoadingNetworkTaskCreator, @NonNull ExecutorService executorService, @NonNull PersistingStrategy<PersistedResourceType> persistingStrategy, @NonNull ResourceTransformer<PersistedResourceType, OutputResourceType> resourceTransformer) {
        this.f3440a = (Logger) Objects.requireNonNull(logger);
        this.b = (ResourceLoadingNetworkTaskCreator) Objects.requireNonNull(resourceLoadingNetworkTaskCreator);
        this.c = (ExecutorService) Objects.requireNonNull(executorService);
        this.d = (PersistingStrategy) Objects.requireNonNull(persistingStrategy);
        this.e = (ResourceTransformer) Objects.requireNonNull(resourceTransformer);
    }

    public void loadResource(@NonNull String str, @NonNull SomaApiContext somaApiContext, @NonNull Listener<OutputResourceType> listener) {
        this.c.submit(new Runnable(str, listener, somaApiContext) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ Listener f$2;
            private final /* synthetic */ SomaApiContext f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void run() {
                ResourceLoader.this.a(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(final String str, final Listener listener, SomaApiContext somaApiContext) {
        try {
            Object obj = this.d.get(str);
            if (obj != null) {
                listener.onResourceLoaded(this.e.transform(obj));
                return;
            }
            ExpirationTimestamp expirationTimestamp = somaApiContext.getApiAdResponse().getExpirationTimestamp();
            if (expirationTimestamp.isExpired()) {
                this.f3440a.warning(LogDomain.RESOURCE_LOADER, "Resource already expired, resourceExpirationTimestamp=%d, current time=%d. Skipping the loading.", Long.valueOf(expirationTimestamp.getExpirationTimestamp()), Long.valueOf(System.currentTimeMillis()));
                listener.onFailure(new ResourceLoaderException(Error.RESOURCE_EXPIRED, new IllegalStateException()));
                return;
            }
            this.b.createTask(str, somaApiContext, new IoFunction(somaApiContext, str) {
                private final /* synthetic */ SomaApiContext f$1;
                private final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final Object apply(Object obj) {
                    return ResourceLoader.this.a(this.f$1, this.f$2, (InputStream) obj);
                }
            }, new com.smaato.sdk.core.Task.Listener<OutputResourceType, NetworkLayerException>() {
                public void onSuccess(@NonNull Task task, @NonNull OutputResourceType outputresourcetype) {
                    listener.onResourceLoaded(outputresourcetype);
                }

                public void onFailure(@NonNull Task task, @NonNull NetworkLayerException networkLayerException) {
                    ResourceLoader.this.f3440a.error(LogDomain.RESOURCE_LOADER, "Failed to load resource at url: %s with error: %s", str, networkLayerException);
                    listener.onFailure(ResourceLoaderErrorMapper.map(networkLayerException));
                }
            }).start();
        } catch (PersistingStrategyException e2) {
            listener.onFailure(new ResourceLoaderException(Error.IO_ERROR, e2));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ TaskStepResult a(SomaApiContext somaApiContext, String str, InputStream inputStream) throws IOException {
        return TaskStepResult.success(this.e.transform(this.d.put(inputStream, str, somaApiContext.getApiAdResponse().getExpirationTimestamp().getExpirationTimestamp())));
    }
}
