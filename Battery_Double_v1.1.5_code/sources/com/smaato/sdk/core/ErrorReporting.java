package com.smaato.sdk.core;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.framework.SomaApiContext;

public interface ErrorReporting {
    void report(@NonNull SomaApiContext somaApiContext, @NonNull Error error, long j);
}
