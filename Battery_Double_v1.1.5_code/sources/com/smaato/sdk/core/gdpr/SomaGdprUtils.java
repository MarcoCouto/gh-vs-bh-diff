package com.smaato.sdk.core.gdpr;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Predicate;
import java.util.EnumMap;
import java.util.Set;

public final class SomaGdprUtils {
    @NonNull
    public final SomaGdprData createSomaGdprData(@NonNull CmpData cmpData) {
        PiiParam[] values;
        Objects.requireNonNull(cmpData, "cmpData must not be null for SomaGdprData::from");
        SubjectToGdpr subjectToGdpr = cmpData.getSubjectToGdpr();
        String consentString = cmpData.getConsentString();
        EnumMap enumMap = new EnumMap(PiiParam.class);
        for (PiiParam piiParam : PiiParam.values()) {
            Set<CmpPurpose> set = piiParam.purposes;
            SubjectToGdpr subjectToGdpr2 = cmpData.getSubjectToGdpr();
            boolean z = true;
            if (!(subjectToGdpr2 == SubjectToGdpr.CMP_GDPR_DISABLED || subjectToGdpr2 == SubjectToGdpr.CMP_GDPR_UNKNOWN) && (cmpData.getConsentString().isEmpty() || (!a(cmpData.getVendorsString()) && !a(cmpData.getPurposesString()) && (!a(cmpData.getVendorsString(), 82) || !Lists.all(set, new Predicate(cmpData) {
                private final /* synthetic */ CmpData f$1;

                {
                    this.f$1 = r2;
                }

                public final boolean test(Object obj) {
                    return SomaGdprUtils.this.a(this.f$1, (CmpPurpose) obj);
                }
            }))))) {
                z = false;
            }
            enumMap.put(piiParam, Boolean.valueOf(z));
        }
        return new SomaGdprData(subjectToGdpr, consentString, enumMap);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(CmpData cmpData, CmpPurpose cmpPurpose) {
        return a(cmpData.getPurposesString(), cmpPurpose.id);
    }

    @VisibleForTesting
    private static boolean a(@NonNull String str) {
        return !str.matches("[01]+");
    }

    @VisibleForTesting
    private static boolean a(@NonNull String str, int i) {
        if (i > str.length() || i <= 0 || '1' != str.charAt(i - 1)) {
            return false;
        }
        return true;
    }
}
