package com.smaato.sdk.core.gdpr;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public enum SubjectToGdpr {
    CMP_GDPR_ENABLED("1"),
    CMP_GDPR_DISABLED("0"),
    CMP_GDPR_UNKNOWN("-1");
    
    @NonNull
    public final String id;

    private SubjectToGdpr(String str) {
        this.id = (String) Objects.requireNonNull(str);
    }
}
