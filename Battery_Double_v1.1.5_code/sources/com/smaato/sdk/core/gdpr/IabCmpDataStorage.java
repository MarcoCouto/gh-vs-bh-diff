package com.smaato.sdk.core.gdpr;

import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import com.amazon.device.ads.AdConstants;
import com.smaato.sdk.core.util.Objects;

public final class IabCmpDataStorage {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final SharedPreferences f3384a;

    public IabCmpDataStorage(@NonNull SharedPreferences sharedPreferences) {
        this.f3384a = (SharedPreferences) Objects.requireNonNull(sharedPreferences, "defaultSharedPreferences must not be null for IabCmpDataStorage::new");
    }

    @NonNull
    public final CmpData getCmpData() {
        return new Builder().a(isCmpPresent()).a(getSubjectToGdpr()).a(getConsentString()).c(getVendorsString()).b(getPurposesString()).a();
    }

    @NonNull
    public final SubjectToGdpr getSubjectToGdpr() {
        SubjectToGdpr[] values;
        String string = this.f3384a.getString(AdConstants.IABCONSENT_SUBJECT_TO_GDPR, null);
        for (SubjectToGdpr subjectToGdpr : SubjectToGdpr.values()) {
            if (subjectToGdpr.id.equals(string)) {
                return subjectToGdpr;
            }
        }
        return SubjectToGdpr.CMP_GDPR_UNKNOWN;
    }

    @NonNull
    public final String getConsentString() {
        return this.f3384a.getString(AdConstants.IABCONSENT_CONSENT_STRING, "");
    }

    public final boolean isCmpPresent() {
        return this.f3384a.getBoolean("IABConsent_CMPPresent", false);
    }

    @NonNull
    public final String getVendorsString() {
        return this.f3384a.getString("IABConsent_ParsedVendorConsents", "");
    }

    @NonNull
    public final String getPurposesString() {
        return this.f3384a.getString("IABConsent_ParsedPurposeConsents", "");
    }
}
