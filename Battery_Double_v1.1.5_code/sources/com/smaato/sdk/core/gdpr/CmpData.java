package com.smaato.sdk.core.gdpr;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import java.util.EnumSet;

public final class CmpData {

    /* renamed from: a reason: collision with root package name */
    private final boolean f3380a;
    @NonNull
    private final SubjectToGdpr b;
    @NonNull
    private final String c;
    @NonNull
    private final String d;
    @NonNull
    private final String e;

    static class Builder {

        /* renamed from: a reason: collision with root package name */
        private boolean f3381a;
        @Nullable
        private SubjectToGdpr b;
        @Nullable
        private String c;
        @Nullable
        private String d;
        @Nullable
        private String e;
        @NonNull
        private final EnumSet<Field> f = EnumSet.noneOf(Field.class);

        @VisibleForTesting
        enum Field {
            CMP_PRESENT("cmpPresent"),
            SUBJECT_TO_GDPR("subjectToGdpr"),
            CONSENT_STRING("consentString"),
            PURPOSES_STRING("purposesString"),
            VENDORS_STRING("vendorsString");
            

            /* renamed from: a reason: collision with root package name */
            private String f3382a;

            private Field(String str) {
                this.f3382a = str;
            }

            public final String toString() {
                return this.f3382a;
            }
        }

        Builder() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder a(boolean z) {
            this.f3381a = z;
            this.f.add(Field.CMP_PRESENT);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder a(@NonNull SubjectToGdpr subjectToGdpr) {
            this.b = subjectToGdpr;
            this.f.add(Field.SUBJECT_TO_GDPR);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder a(@NonNull String str) {
            this.c = str;
            this.f.add(Field.CONSENT_STRING);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder b(@NonNull String str) {
            this.d = str;
            this.f.add(Field.PURPOSES_STRING);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder c(@NonNull String str) {
            this.e = str;
            this.f.add(Field.VENDORS_STRING);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final CmpData a() {
            EnumSet allOf = EnumSet.allOf(Field.class);
            allOf.removeAll(this.f);
            if (allOf.size() <= 0) {
                Objects.requireNonNull(this.b, "subjectToGdpr can not be null for CmpData.Builder::build");
                Objects.requireNonNull(this.c, "consentString can not be null for CmpData.Builder::build");
                Objects.requireNonNull(this.e, "vendorsString can not be null for CmpData.Builder::build");
                Objects.requireNonNull(this.d, "purposesString can not be null for CmpData.Builder::build");
                CmpData cmpData = new CmpData(this.f3381a, this.b, this.c, this.e, this.d, 0);
                return cmpData;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(Joiner.join((CharSequence) ", ", (Iterable) allOf));
            sb.append(" field(s) must be set explicitly for CmpData.Builder::build");
            throw new IllegalStateException(sb.toString());
        }
    }

    /* synthetic */ CmpData(boolean z, SubjectToGdpr subjectToGdpr, String str, String str2, String str3, byte b2) {
        this(z, subjectToGdpr, str, str2, str3);
    }

    private CmpData(boolean z, @NonNull SubjectToGdpr subjectToGdpr, @NonNull String str, @NonNull String str2, @NonNull String str3) {
        this.b = subjectToGdpr;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f3380a = z;
    }

    public final boolean isCmpPresent() {
        return this.f3380a;
    }

    @NonNull
    public final SubjectToGdpr getSubjectToGdpr() {
        return this.b;
    }

    @NonNull
    public final String getConsentString() {
        return this.c;
    }

    @NonNull
    public final String getVendorsString() {
        return this.d;
    }

    @NonNull
    public final String getPurposesString() {
        return this.e;
    }
}
