package com.smaato.sdk.core.gdpr;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.EnumMap;
import java.util.Map.Entry;

public class SomaGdprData {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final SubjectToGdpr f3386a;
    @NonNull
    private final String b;
    @NonNull
    private final EnumMap<PiiParam, Boolean> c;

    SomaGdprData(@NonNull SubjectToGdpr subjectToGdpr, @NonNull String str, @NonNull EnumMap<PiiParam, Boolean> enumMap) {
        this.f3386a = (SubjectToGdpr) Objects.requireNonNull(subjectToGdpr, "subjectToGdpr must not be null for SomaGdprData::new");
        this.b = (String) Objects.requireNonNull(str, "consentString must not be null for SomaGdprData::new");
        this.c = new EnumMap<>((EnumMap) Objects.requireNonNull(enumMap, "piiParamToConsentMap must not be null for SomaGdprData::new"));
        for (Entry value : this.c.entrySet()) {
            if (value.getValue() == null) {
                throw new IllegalArgumentException("piiParamToConsentMap must not contain null value for SomaGdprData::new");
            }
        }
    }

    @NonNull
    public SubjectToGdpr getSubjectToGdpr() {
        return this.f3386a;
    }

    @Nullable
    public Boolean isGdprEnabled() {
        if (!this.b.isEmpty()) {
            return Boolean.TRUE;
        }
        if (this.f3386a == SubjectToGdpr.CMP_GDPR_UNKNOWN) {
            return null;
        }
        return Boolean.valueOf(this.f3386a == SubjectToGdpr.CMP_GDPR_ENABLED);
    }

    @NonNull
    public String getConsentString() {
        return this.b;
    }

    public boolean isUsageAllowedFor(@NonNull PiiParam piiParam) {
        return ((Boolean) this.c.get(piiParam)).booleanValue();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SomaGdprData{subjectToGdpr=");
        sb.append(this.f3386a);
        sb.append(", consentString='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", piiParamToConsentMap=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}
