package com.smaato.sdk.core;

public final class BuildConfig {
    @Deprecated
    public static final String APPLICATION_ID = "com.smaato.sdk.core";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final String LIBRARY_PACKAGE_NAME = "com.smaato.sdk.core";
    public static final String SOMA_API_URL = String.valueOf("https://sdk-android.ad.smaato.net/oapi/v6/ad");
    public static final String SOMA_VIOLATIONS_AGGREGATOR_URL = String.valueOf("https://impact.smaato.net/pingback.php");
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "21.1.3";
}
