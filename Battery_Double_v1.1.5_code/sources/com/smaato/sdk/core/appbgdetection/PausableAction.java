package com.smaato.sdk.core.appbgdetection;

import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;

public class PausableAction implements Runnable {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Runnable f3343a;
    @NonNull
    private final Handler b;
    @Nullable
    private final PauseUnpauseListener c;
    private long d;
    private long e;
    private long f = 0;
    @NonNull
    public final String name;

    PausableAction(@NonNull String str, @NonNull Handler handler, @NonNull Runnable runnable, long j, @Nullable PauseUnpauseListener pauseUnpauseListener) {
        this.name = (String) Objects.requireNonNull(str);
        this.b = (Handler) Objects.requireNonNull(handler);
        this.f3343a = (Runnable) Objects.requireNonNull(runnable);
        if (j > 0) {
            this.d = j;
            this.c = pauseUnpauseListener;
            this.e = SystemClock.uptimeMillis();
            return;
        }
        StringBuilder sb = new StringBuilder("delay must be positive for ");
        sb.append(getClass().getSimpleName());
        sb.append("::new");
        throw new IllegalArgumentException(sb.toString());
    }

    public void run() {
        Threads.ensureHandlerThread(this.b);
        this.f3343a.run();
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        Threads.ensureHandlerThread(this.b);
        return this.f > 0;
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        Threads.ensureHandlerThread(this.b);
        if (!a()) {
            this.b.removeCallbacks(this);
            this.f = SystemClock.uptimeMillis();
            this.d -= this.f - this.e;
            if (this.c != null) {
                this.c.onActionPaused();
                return;
            }
            return;
        }
        throw new RuntimeException("unexpected pause call - action has been already paused");
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        Threads.ensureHandlerThread(this.b);
        if (a()) {
            this.f = 0;
            this.e = SystemClock.uptimeMillis();
            if (this.c != null) {
                this.c.onBeforeActionUnpaused();
            }
            this.b.postDelayed(this, this.d);
            return;
        }
        throw new RuntimeException("unexpected unpause call - action has not been paused");
    }
}
