package com.smaato.sdk.core;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.-$$Lambda$SmaatoInstance$-1EAKuS9NNIxR8fb08T7PPAR-c0 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$SmaatoInstance$1EAKuS9NNIxR8fb08T7PPARc0 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$SmaatoInstance$1EAKuS9NNIxR8fb08T7PPARc0 INSTANCE = new $$Lambda$SmaatoInstance$1EAKuS9NNIxR8fb08T7PPARc0();

    private /* synthetic */ $$Lambda$SmaatoInstance$1EAKuS9NNIxR8fb08T7PPARc0() {
    }

    public final Object get(DiConstructor diConstructor) {
        return BuildConfig.SOMA_API_URL;
    }
}
