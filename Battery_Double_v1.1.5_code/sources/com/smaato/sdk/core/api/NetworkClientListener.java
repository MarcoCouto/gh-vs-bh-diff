package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.api.ApiResponseMapper.MappingException;
import com.smaato.sdk.core.api.ApiResponseMapper.MappingException.Type;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkClient;
import com.smaato.sdk.core.network.NetworkClient.Listener;
import com.smaato.sdk.core.network.NetworkResponse;
import com.smaato.sdk.core.network.execution.NetworkLayerException;
import com.smaato.sdk.core.util.Objects;

class NetworkClientListener implements Listener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3337a;
    @NonNull
    private final ApiResponseMapper b;
    @NonNull
    private final Callback c;

    interface Callback {
        void onAdRequestError(@NonNull Task task, @NonNull ApiConnectorException apiConnectorException);

        void onAdRequestSuccess(@NonNull Task task, @NonNull ApiAdResponse apiAdResponse);
    }

    public NetworkClientListener(@NonNull Logger logger, @NonNull ApiResponseMapper apiResponseMapper, @NonNull Callback callback) {
        this.f3337a = (Logger) Objects.requireNonNull(logger);
        this.b = (ApiResponseMapper) Objects.requireNonNull(apiResponseMapper);
        this.c = (Callback) Objects.requireNonNull(callback);
    }

    public void onRequestSuccess(@NonNull NetworkClient networkClient, @NonNull Task task, @NonNull NetworkResponse networkResponse) {
        Objects.requireNonNull(networkClient);
        Objects.requireNonNull(task);
        Objects.requireNonNull(networkResponse);
        this.f3337a.debug(LogDomain.API, "networkClientListener.onRequestSuccess: entered, task = %s, networkResponse = %s", task, networkResponse);
        try {
            ApiAdResponse map = this.b.map(networkResponse);
            this.f3337a.debug(LogDomain.API, "networkClientListener.onRequestSuccess: mapped ApiAdResponse (for task %s): %s", task, map);
            this.c.onAdRequestSuccess(task, map);
        } catch (MappingException e) {
            if (e.type == Type.NO_AD) {
                this.f3337a.error(LogDomain.API, "networkClientListener.onRequestSuccess: (for task %s) error mapping NetworkResponse to ApiAdResponse: %s", task, e);
            } else {
                this.f3337a.error(LogDomain.API, e, "networkClientListener.onRequestSuccess: (for task %s) error mapping NetworkResponse to ApiAdResponse: %s", task, e);
            }
            this.c.onAdRequestError(task, ErrorMappingUtil.a(e));
        }
    }

    public void onRequestError(@NonNull NetworkClient networkClient, @NonNull Task task, @NonNull NetworkLayerException networkLayerException) {
        Objects.requireNonNull(networkClient);
        Objects.requireNonNull(task);
        Objects.requireNonNull(networkLayerException);
        this.f3337a.error(LogDomain.API, "networkClientListener.onRequestError: (for task %s): %s", task, networkLayerException);
        this.c.onAdRequestError(task, ErrorMappingUtil.a(networkLayerException));
    }
}
