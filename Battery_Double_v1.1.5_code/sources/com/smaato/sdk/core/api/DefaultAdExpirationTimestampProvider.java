package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.ExpirationTimestamp;
import com.smaato.sdk.core.util.CurrentTimeProvider;
import com.smaato.sdk.core.util.Objects;

public final class DefaultAdExpirationTimestampProvider {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final CurrentTimeProvider f3334a;
    private final long b = 300000;

    DefaultAdExpirationTimestampProvider(@NonNull CurrentTimeProvider currentTimeProvider, long j) {
        this.f3334a = (CurrentTimeProvider) Objects.requireNonNull(currentTimeProvider);
    }

    @NonNull
    public final ExpirationTimestamp defaultExpirationTimestampFor(@NonNull AdFormat adFormat) {
        Objects.requireNonNull(adFormat);
        return new ExpirationTimestamp(this.f3334a.currentMillisUtc() + this.b, this.f3334a);
    }
}
