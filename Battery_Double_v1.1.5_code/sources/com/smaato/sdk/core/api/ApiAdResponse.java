package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.ExpirationTimestamp;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ApiAdResponse {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final AdFormat f3324a;
    @NonNull
    private final byte[] b;
    @NonNull
    private final Map<String, List<String>> c;
    @NonNull
    private final String d;
    @NonNull
    private final String e;
    @NonNull
    private final String f;
    @NonNull
    private final ExpirationTimestamp g;
    @NonNull
    private final String h;
    @Nullable
    private final String i;

    public ApiAdResponse(@NonNull AdFormat adFormat, @NonNull byte[] bArr, @NonNull Map<String, List<String>> map, @NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull ExpirationTimestamp expirationTimestamp, @NonNull String str4, @Nullable String str5) {
        this.f3324a = (AdFormat) Objects.requireNonNull(adFormat);
        this.b = (byte[]) Objects.requireNonNull(bArr);
        this.c = (Map) Objects.requireNonNull(map);
        this.d = (String) Objects.requireNonNull(str);
        this.e = (String) Objects.requireNonNull(str2);
        this.f = (String) Objects.requireNonNull(str3);
        this.g = (ExpirationTimestamp) Objects.requireNonNull(expirationTimestamp);
        this.h = (String) Objects.requireNonNull(str4);
        this.i = str5;
    }

    @NonNull
    public AdFormat getAdFormat() {
        return this.f3324a;
    }

    @NonNull
    public byte[] getBody() {
        return Arrays.copyOf(this.b, this.b.length);
    }

    @NonNull
    public Map<String, List<String>> getResponseHeaders() {
        HashMap hashMap = new HashMap(this.c);
        for (Entry entry : hashMap.entrySet()) {
            entry.setValue(new ArrayList((Collection) entry.getValue()));
        }
        return hashMap;
    }

    @NonNull
    public String getMimeType() {
        return this.d;
    }

    @NonNull
    public String getCharset() {
        return this.e;
    }

    @NonNull
    public String getRequestUrl() {
        return this.f;
    }

    @NonNull
    public String getSessionId() {
        return this.h;
    }

    @Nullable
    public String getCreativeId() {
        return this.i;
    }

    @NonNull
    public ExpirationTimestamp getExpirationTimestamp() {
        return this.g;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ApiAdResponse{adFormat=");
        sb.append(this.f3324a);
        sb.append(", body.length=");
        sb.append(this.b.length);
        sb.append(" bytes, responseHeaders=");
        sb.append(this.c);
        sb.append(", mimeType='");
        sb.append(this.d);
        sb.append('\'');
        sb.append(", charset='");
        sb.append(this.e);
        sb.append('\'');
        sb.append(", requestUrl='");
        sb.append(this.f);
        sb.append('\'');
        sb.append(", expirationTimestamp='");
        sb.append(this.g.toString());
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
