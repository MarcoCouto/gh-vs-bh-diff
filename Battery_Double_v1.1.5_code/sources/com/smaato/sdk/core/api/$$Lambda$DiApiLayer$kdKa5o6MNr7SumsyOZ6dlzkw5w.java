package com.smaato.sdk.core.api;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.api.-$$Lambda$DiApiLayer$kdKa5o6MNr7SumsyOZ6-dlzkw5w reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiApiLayer$kdKa5o6MNr7SumsyOZ6dlzkw5w implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiApiLayer$kdKa5o6MNr7SumsyOZ6dlzkw5w INSTANCE = new $$Lambda$DiApiLayer$kdKa5o6MNr7SumsyOZ6dlzkw5w();

    private /* synthetic */ $$Lambda$DiApiLayer$kdKa5o6MNr7SumsyOZ6dlzkw5w() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiApiLayer.f(diConstructor);
    }
}
