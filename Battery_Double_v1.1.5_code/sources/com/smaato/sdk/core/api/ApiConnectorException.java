package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.api.ApiConnector.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkComponentException;

public final class ApiConnectorException extends Exception implements SdkComponentException<Error> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Error f3328a;
    @NonNull
    private final Exception b;

    public ApiConnectorException(@NonNull Error error, @NonNull Exception exc) {
        super(exc);
        this.f3328a = (Error) Objects.requireNonNull(error);
        this.b = (Exception) Objects.requireNonNull(exc);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ApiConnectorException { errorType = ");
        sb.append(this.f3328a);
        sb.append(", reason = ");
        sb.append(this.b);
        sb.append(" }");
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ApiConnectorException apiConnectorException = (ApiConnectorException) obj;
        return this.f3328a == apiConnectorException.f3328a && Objects.equals(this.b, apiConnectorException.b);
    }

    public final int hashCode() {
        return Objects.hash(this.f3328a, this.b);
    }

    @NonNull
    public final Error getErrorType() {
        return this.f3328a;
    }

    @NonNull
    public final Exception getReason() {
        return this.b;
    }
}
