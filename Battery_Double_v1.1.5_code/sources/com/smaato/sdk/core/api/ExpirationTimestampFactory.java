package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.ExpirationTimestamp;
import com.smaato.sdk.core.util.CurrentTimeProvider;
import com.smaato.sdk.core.util.Objects;

public class ExpirationTimestampFactory {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final CurrentTimeProvider f3336a;

    ExpirationTimestampFactory(@NonNull CurrentTimeProvider currentTimeProvider) {
        this.f3336a = (CurrentTimeProvider) Objects.requireNonNull(currentTimeProvider);
    }

    @NonNull
    public ExpirationTimestamp createExpirationTimestampFor(long j) {
        return new ExpirationTimestamp(j, this.f3336a);
    }
}
