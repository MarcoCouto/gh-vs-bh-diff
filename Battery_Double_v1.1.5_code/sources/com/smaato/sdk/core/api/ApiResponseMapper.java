package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.appodeal.ads.utils.LogConstants;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.ApiUtils;
import com.smaato.sdk.core.ad.ExpirationTimestamp;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkResponse;
import com.smaato.sdk.core.util.ContentTypeUtil;
import com.smaato.sdk.core.util.HeaderUtils;
import com.smaato.sdk.core.util.Objects;
import java.util.List;
import java.util.Map;

class ApiResponseMapper {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3330a;
    @NonNull
    private final HeaderUtils b;
    @NonNull
    private final ExpirationTimestampFactory c;
    @NonNull
    private final DefaultAdExpirationTimestampProvider d;

    public static class MappingException extends Exception {
        @Nullable
        public final String errorMessage;
        @NonNull
        public final Type type;

        public enum Type {
            NO_AD,
            BAD_REQUEST,
            MISSING_AD_TYPE,
            UNEXPECTED_AD_TYPE,
            MISSING_CONTENT_TYPE,
            MISSING_SESSION_ID,
            MISSING_MIME_TYPE,
            MISSING_CHARSET,
            MISSING_BODY,
            EMPTY_BODY,
            UNEXPECTED_HTTP_RESPONSE_CODE,
            GENERIC
        }

        MappingException(@NonNull Type type2) {
            super(type2.toString());
            this.type = (Type) Objects.requireNonNull(type2);
            this.errorMessage = null;
        }

        MappingException(@NonNull Type type2, @Nullable String str) {
            StringBuilder sb = new StringBuilder();
            sb.append(type2.toString());
            sb.append(": ");
            sb.append(str);
            super(sb.toString());
            this.type = (Type) Objects.requireNonNull(type2);
            this.errorMessage = str;
        }
    }

    public ApiResponseMapper(@NonNull Logger logger, @NonNull HeaderUtils headerUtils, @NonNull ExpirationTimestampFactory expirationTimestampFactory, @NonNull DefaultAdExpirationTimestampProvider defaultAdExpirationTimestampProvider) {
        this.f3330a = (Logger) Objects.requireNonNull(logger);
        this.b = (HeaderUtils) Objects.requireNonNull(headerUtils);
        this.c = (ExpirationTimestampFactory) Objects.requireNonNull(expirationTimestampFactory);
        this.d = (DefaultAdExpirationTimestampProvider) Objects.requireNonNull(defaultAdExpirationTimestampProvider);
    }

    @NonNull
    public ApiAdResponse map(@NonNull NetworkResponse networkResponse) throws MappingException {
        String str;
        String str2;
        AdFormat adFormat;
        this.f3330a.debug(LogDomain.API, "map: entered with %s", networkResponse);
        Objects.requireNonNull(networkResponse);
        int responseCode = networkResponse.getResponseCode();
        this.f3330a.debug(LogDomain.API, "httpResponseCode = %s", Integer.valueOf(responseCode));
        if (responseCode == 200) {
            try {
                Map headers = networkResponse.getHeaders();
                List extractHeaderValueList = this.b.extractHeaderValueList(headers, "Content-Type");
                if (extractHeaderValueList == null) {
                    this.f3330a.debug(LogDomain.API, "%s header is absent in response", "Content-Type");
                    throw new MappingException(Type.MISSING_CONTENT_TYPE);
                } else if (!extractHeaderValueList.isEmpty()) {
                    int size = extractHeaderValueList.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            this.f3330a.debug(LogDomain.API, "mimeType not found in response", new Object[0]);
                            str = null;
                            break;
                        }
                        String str3 = (String) extractHeaderValueList.get(i);
                        if (str3 != null) {
                            String parseMimeType = ContentTypeUtil.parseMimeType(str3);
                            if (parseMimeType != null) {
                                this.f3330a.debug(LogDomain.API, "mimeType found in response = %s", parseMimeType);
                                str = parseMimeType;
                                break;
                            }
                        }
                        i++;
                    }
                    if (str != null) {
                        int size2 = extractHeaderValueList.size();
                        int i2 = 0;
                        while (true) {
                            if (i2 >= size2) {
                                this.f3330a.debug(LogDomain.API, "charset not found in response", new Object[0]);
                                str2 = null;
                                break;
                            }
                            String str4 = (String) extractHeaderValueList.get(i2);
                            if (str4 != null) {
                                String parseCharset = ContentTypeUtil.parseCharset(str4);
                                if (parseCharset != null) {
                                    this.f3330a.debug(LogDomain.API, "charset found in response = %s", parseCharset);
                                    str2 = parseCharset;
                                    break;
                                }
                            }
                            i2++;
                        }
                        if (str2 != null) {
                            String extractHeaderSingleValue = this.b.extractHeaderSingleValue(headers, "X-SMT-ADTYPE");
                            if (extractHeaderSingleValue != null) {
                                this.f3330a.debug(LogDomain.API, "%s header value: %s", "X-SMT-ADTYPE", extractHeaderSingleValue);
                                if (!extractHeaderSingleValue.isEmpty()) {
                                    char c2 = 65535;
                                    int hashCode = extractHeaderSingleValue.hashCode();
                                    if (hashCode != -1968751561) {
                                        if (hashCode != 73635) {
                                            if (hashCode != 82650203) {
                                                if (hashCode == 1173835880) {
                                                    if (extractHeaderSingleValue.equals("Richmedia")) {
                                                        c2 = 2;
                                                    }
                                                }
                                            } else if (extractHeaderSingleValue.equals("Video")) {
                                                c2 = 1;
                                            }
                                        } else if (extractHeaderSingleValue.equals("Img")) {
                                            c2 = 0;
                                        }
                                    } else if (extractHeaderSingleValue.equals(LogConstants.KEY_NATIVE)) {
                                        c2 = 3;
                                    }
                                    switch (c2) {
                                        case 0:
                                            adFormat = AdFormat.STATIC_IMAGE;
                                            break;
                                        case 1:
                                            adFormat = AdFormat.VIDEO;
                                            break;
                                        case 2:
                                            adFormat = AdFormat.RICH_MEDIA;
                                            break;
                                        case 3:
                                            adFormat = AdFormat.NATIVE;
                                            break;
                                        default:
                                            this.f3330a.debug(LogDomain.API, "unexpected X-SMT-ADTYPE response header value: %s", extractHeaderSingleValue);
                                            throw new MappingException(Type.UNEXPECTED_AD_TYPE, extractHeaderSingleValue);
                                    }
                                    AdFormat adFormat2 = adFormat;
                                    this.f3330a.debug(LogDomain.API, "got adFormat = %s", adFormat2);
                                    byte[] body = networkResponse.getBody();
                                    if (body == null) {
                                        throw new MappingException(Type.MISSING_BODY);
                                    } else if (body.length != 0) {
                                        String requestUrl = networkResponse.getRequestUrl();
                                        ExpirationTimestamp a2 = a(adFormat2, headers);
                                        String retrieveSessionId = ApiUtils.retrieveSessionId(headers);
                                        if (retrieveSessionId != null) {
                                            ApiAdResponse apiAdResponse = new ApiAdResponse(adFormat2, body, headers, str, str2, requestUrl, a2, retrieveSessionId, ApiUtils.retrieveSci(headers));
                                            return apiAdResponse;
                                        }
                                        throw new MappingException(Type.MISSING_SESSION_ID, "No X-SMT-SessionId header in ad response. Empty string is returned.");
                                    } else {
                                        throw new MappingException(Type.EMPTY_BODY);
                                    }
                                } else {
                                    this.f3330a.debug(LogDomain.API, "invalid %s response header value", "X-SMT-ADTYPE");
                                    throw new MappingException(Type.MISSING_AD_TYPE);
                                }
                            } else {
                                this.f3330a.debug(LogDomain.API, "missing %s response header", "X-SMT-ADTYPE");
                                throw new MappingException(Type.MISSING_AD_TYPE);
                            }
                        } else {
                            throw new MappingException(Type.MISSING_CHARSET);
                        }
                    } else {
                        throw new MappingException(Type.MISSING_MIME_TYPE);
                    }
                } else {
                    this.f3330a.debug(LogDomain.API, "%s header is empty in response", "Content-Type");
                    throw new MappingException(Type.MISSING_CONTENT_TYPE);
                }
            } catch (Exception e) {
                this.f3330a.debug(LogDomain.API, "error mapping networkResponse: %s", e);
                if (e instanceof MappingException) {
                    throw e;
                }
                this.f3330a.error(LogDomain.API, e, "error mapping networkResponse", new Object[0]);
                throw new MappingException(Type.GENERIC, e.toString());
            }
        } else if (responseCode == 204) {
            this.f3330a.debug(LogDomain.API, "No Ad", new Object[0]);
            throw new MappingException(Type.NO_AD);
        } else if (responseCode < 400 || responseCode >= 500) {
            throw new MappingException(Type.UNEXPECTED_HTTP_RESPONSE_CODE, String.valueOf(responseCode));
        } else {
            String extractHeaderMultiValue = this.b.extractHeaderMultiValue(networkResponse.getHeaders(), "X-SMT-MESSAGE");
            if (extractHeaderMultiValue != null) {
                this.f3330a.debug(LogDomain.API, "errorMessage = %s", extractHeaderMultiValue);
            } else {
                this.f3330a.debug(LogDomain.API, "errorMessage not supplied in response headers", new Object[0]);
                extractHeaderMultiValue = null;
            }
            throw new MappingException(Type.BAD_REQUEST, extractHeaderMultiValue);
        }
    }

    @NonNull
    private ExpirationTimestamp a(@NonNull AdFormat adFormat, @NonNull Map<String, List<String>> map) {
        String extractHeaderSingleValue = this.b.extractHeaderSingleValue(map, "X-SMT-Expires");
        if (extractHeaderSingleValue != null) {
            try {
                return this.c.createExpirationTimestampFor(Long.parseLong(extractHeaderSingleValue.trim()));
            } catch (NumberFormatException unused) {
                this.f3330a.debug(LogDomain.API, "invalid %s response header value", "X-SMT-Expires", extractHeaderSingleValue);
            }
        } else {
            this.f3330a.debug(LogDomain.API, "No X-SMT-Expires header in ad response. Using default expiration timestamp.", new Object[0]);
            return this.d.defaultExpirationTimestampFor(adFormat);
        }
    }
}
