package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.datacollector.DataCollector;
import com.smaato.sdk.core.di.CoreDiNames;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.network.NetworkClient;
import com.smaato.sdk.core.util.CurrentTimeProvider;
import com.smaato.sdk.core.util.HeaderUtils;

public final class DiApiLayer {
    private DiApiLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiApiLayer$f2uaob3vzqpgBFwMwe05BRAXXJk.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(ApiConnector.class, $$Lambda$DiApiLayer$kdKa5o6MNr7SumsyOZ6dlzkw5w.INSTANCE);
        diRegistry.registerFactory(ApiRequestMapper.class, $$Lambda$DiApiLayer$YBJMNxMjYKNqHttmu3KjEMoEaA.INSTANCE);
        diRegistry.registerFactory(CurrentTimeProvider.class, $$Lambda$DiApiLayer$OOi9_IHRqfLh7Om8PM0haUIiJyA.INSTANCE);
        diRegistry.registerFactory(DefaultAdExpirationTimestampProvider.class, $$Lambda$DiApiLayer$hAclBPAl2x7bkjQVcLwORoq2vLU.INSTANCE);
        diRegistry.registerFactory(ExpirationTimestampFactory.class, $$Lambda$DiApiLayer$X136G1wB3Pr8nSrJWDgZp1EOz9U.INSTANCE);
        diRegistry.registerFactory(ApiResponseMapper.class, $$Lambda$DiApiLayer$IhDf8NL7Zf55qi9SbTvMuT1JQIY.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ApiConnector f(DiConstructor diConstructor) {
        return new ApiConnector(DiLogLayer.getLoggerFrom(diConstructor), (ApiRequestMapper) diConstructor.get(ApiRequestMapper.class), (ApiResponseMapper) diConstructor.get(ApiResponseMapper.class), (NetworkClient) diConstructor.get(NetworkClient.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ApiRequestMapper e(DiConstructor diConstructor) {
        return new ApiRequestMapper(DiLogLayer.getLoggerFrom(diConstructor), (String) diConstructor.get(CoreDiNames.SOMA_API_URL, String.class), ((DataCollector) diConstructor.get(DataCollector.class)).getSystemInfo().getUserAgent());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ CurrentTimeProvider d(DiConstructor diConstructor) {
        return new CurrentTimeProvider();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ DefaultAdExpirationTimestampProvider c(DiConstructor diConstructor) {
        return new DefaultAdExpirationTimestampProvider((CurrentTimeProvider) diConstructor.get(CurrentTimeProvider.class), 300000);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ExpirationTimestampFactory b(DiConstructor diConstructor) {
        return new ExpirationTimestampFactory((CurrentTimeProvider) diConstructor.get(CurrentTimeProvider.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ApiResponseMapper a(DiConstructor diConstructor) {
        return new ApiResponseMapper(DiLogLayer.getLoggerFrom(diConstructor), (HeaderUtils) diConstructor.get(HeaderUtils.class), (ExpirationTimestampFactory) diConstructor.get(ExpirationTimestampFactory.class), (DefaultAdExpirationTimestampProvider) diConstructor.get(DefaultAdExpirationTimestampProvider.class));
    }
}
