package com.smaato.sdk.core.log;

import android.util.Log;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

final class LoggerFactory {
    public static final LogLevel DEFAULT_CONSOLE_LOG_LEVEL = LogLevel.WARNING;

    /* renamed from: a reason: collision with root package name */
    private static volatile Logger f3397a;

    private LoggerFactory() {
    }

    public static void initializeLogger(@NonNull LogLevel logLevel) {
        Objects.requireNonNull(logLevel);
        if (f3397a == null) {
            synchronized (LoggerFactory.class) {
                if (f3397a == null) {
                    LoggerImpl loggerImpl = new LoggerImpl(Environment.RELEASE);
                    loggerImpl.a((LogWriter) new ConsoleLogWriter(logLevel));
                    f3397a = loggerImpl;
                }
            }
        }
    }

    @NonNull
    public static Logger getLogger() {
        if (f3397a == null) {
            synchronized (LoggerFactory.class) {
                if (f3397a == null) {
                    Log.e(LoggerFactory.class.getName(), "Logger was not initialized! Going to initialize with a default console log level");
                    initializeLogger(DEFAULT_CONSOLE_LOG_LEVEL);
                }
            }
        }
        return f3397a;
    }
}
