package com.smaato.sdk.core.log;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

abstract class LogWriter {

    /* renamed from: a reason: collision with root package name */
    private final LogLevel f3396a;

    /* access modifiers changed from: protected */
    public abstract boolean isLoggable(@NonNull LogLevel logLevel);

    /* access modifiers changed from: protected */
    public abstract void log(@NonNull LogLevel logLevel, @NonNull String str, @NonNull String str2);

    LogWriter(@NonNull LogLevel logLevel) {
        Objects.requireNonNull(logLevel);
        this.f3396a = logLevel;
    }

    /* access modifiers changed from: protected */
    public final LogLevel getLogLevel() {
        return this.f3396a;
    }
}
