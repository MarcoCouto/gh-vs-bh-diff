package com.smaato.sdk.core.log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.util.fi.Consumer;

public final class DiLogLayer {

    /* renamed from: a reason: collision with root package name */
    private static final Logger f3393a = new Logger() {
        public final void debug(@NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void debug(@NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void error(@NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void error(@NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void info(@NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void info(@NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void log(@NonNull LogLevel logLevel, @NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void log(@NonNull LogLevel logLevel, @NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void setExplicitOneShotTag(@NonNull String str) {
        }

        public final void warning(@NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        }

        public final void warning(@NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        }
    };

    private DiLogLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry(boolean z, @NonNull LogLevel logLevel) {
        return DiRegistry.of(new Consumer(z, logLevel) {
            private final /* synthetic */ boolean f$0;
            private final /* synthetic */ LogLevel f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((DiRegistry) obj).registerSingletonFactory(Logger.class, new ClassFactory(this.f$0, this.f$1) {
                    private final /* synthetic */ boolean f$0;
                    private final /* synthetic */ LogLevel f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    public final Object get(DiConstructor diConstructor) {
                        return DiLogLayer.a(this.f$0, this.f$1, diConstructor);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Logger a(boolean z, LogLevel logLevel, DiConstructor diConstructor) {
        if (!z) {
            return f3393a;
        }
        LoggerFactory.initializeLogger(logLevel);
        return LoggerFactory.getLogger();
    }

    @NonNull
    public static Logger getLoggerFrom(@NonNull DiConstructor diConstructor) {
        return (Logger) diConstructor.get(Logger.class);
    }
}
