package com.smaato.sdk.core.log;

public enum LogLevel {
    DEBUG,
    INFO,
    WARNING,
    ERROR
}
