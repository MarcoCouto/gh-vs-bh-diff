package com.smaato.sdk.core.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.Logger;

public interface ModuleInterface {
    @NonNull
    ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory();

    @NonNull
    ExpectedManifestEntries getExpectedManifestEntries();

    @NonNull
    Class<? extends AdPresenter> getSupportedAdPresenterInterface();

    void init(@NonNull ClassLoader classLoader);

    boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Logger logger);

    boolean isInitialised();

    @NonNull
    String moduleDiName();

    @Nullable
    DiRegistry moduleDiRegistry();

    @NonNull
    String version();
}
