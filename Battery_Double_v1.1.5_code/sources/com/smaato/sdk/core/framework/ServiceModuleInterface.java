package com.smaato.sdk.core.framework;

import androidx.annotation.NonNull;

public interface ServiceModuleInterface extends BaseModuleInterface {
    void init(@NonNull Iterable<ModuleInterface> iterable);
}
