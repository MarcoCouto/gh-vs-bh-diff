package com.smaato.sdk.core.framework;

public final class VisibilityPrivateConfig {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final double f3378a;
    /* access modifiers changed from: private */
    public final long b;

    public static class Builder {

        /* renamed from: a reason: collision with root package name */
        private double f3379a;
        private long b;

        public Builder() {
        }

        public Builder(VisibilityPrivateConfig visibilityPrivateConfig) {
            this.f3379a = visibilityPrivateConfig.f3378a;
            this.b = visibilityPrivateConfig.b;
        }

        public Builder visibilityRatio(double d) {
            this.f3379a = d;
            return this;
        }

        public Builder visibilityTimeMillis(long j) {
            this.b = j;
            return this;
        }

        public VisibilityPrivateConfig build() {
            VisibilityPrivateConfig visibilityPrivateConfig = new VisibilityPrivateConfig(this.f3379a, this.b, 0);
            return visibilityPrivateConfig;
        }
    }

    /* synthetic */ VisibilityPrivateConfig(double d, long j, byte b2) {
        this(d, j);
    }

    private VisibilityPrivateConfig(double d, long j) {
        this.f3378a = d;
        this.b = j;
    }

    public final double getVisibilityRatio() {
        return this.f3378a;
    }

    public final long getVisibilityTimeMillis() {
        return this.b;
    }
}
