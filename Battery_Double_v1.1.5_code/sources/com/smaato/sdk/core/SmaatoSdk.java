package com.smaato.sdk.core;

import android.app.Application;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.configcheck.AppManifestConfigChecker;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.ModuleInterface;
import com.smaato.sdk.core.framework.SdkInitialisationObserver;
import com.smaato.sdk.core.framework.ServiceModuleInterface;
import com.smaato.sdk.core.init.AdPresenterModuleInterfaceUtils;
import com.smaato.sdk.core.init.BaseDiRegistryUtils;
import com.smaato.sdk.core.init.BaseModuleValidationUtils;
import com.smaato.sdk.core.init.ModuleInterfaceUtils;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.ConnectionStatusWatcher;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.Collections;
import java.util.List;
import java.util.ServiceLoader;

public final class SmaatoSdk {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private static volatile SmaatoInstance f3286a;

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterNameShaper a(AdPresenterNameShaper adPresenterNameShaper, DiConstructor diConstructor) {
        return adPresenterNameShaper;
    }

    @NonNull
    public static String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    private SmaatoSdk() {
    }

    public static void init(@NonNull Application application, @NonNull String str) {
        init(application, Config.builder().build(), str);
    }

    public static void init(@NonNull Application application, @NonNull Config config, @NonNull String str) {
        Config config2;
        ClassLoader classLoader = application.getClassLoader();
        ServiceLoader load = ServiceLoader.load(ModuleInterface.class, classLoader);
        ServiceLoader load2 = ServiceLoader.load(AdPresenterModuleInterface.class, classLoader);
        ServiceLoader load3 = ServiceLoader.load(ServiceModuleInterface.class, classLoader);
        ServiceLoader<SdkInitialisationObserver> load4 = ServiceLoader.load(SdkInitialisationObserver.class, classLoader);
        Application application2 = application;
        Objects.requireNonNull(application, "Parameter application cannot be null for SmaatoSdk::init");
        Objects.requireNonNull(load, "Parameter foundModulesToRegister cannot be null for SmaatoSdk::init");
        Objects.requireNonNull(str, "Parameter publisherId cannot be null for SmaatoSdk::init");
        if (!str.isEmpty()) {
            if (f3286a == null) {
                synchronized (SmaatoSdk.class) {
                    if (f3286a == null) {
                        if (config == null) {
                            config2 = Config.builder().build();
                            Log.w(LogDomain.CORE.name(), String.format("null config parameter is ignored, a default config is used instead (logLevel: %s, httpsOnly: %b)", new Object[]{config2.b(), Boolean.valueOf(config2.isHttpsOnly())}));
                        } else {
                            config2 = config;
                        }
                        List validInitialisedModuleInterfaces = ModuleInterfaceUtils.getValidInitialisedModuleInterfaces(application.getClassLoader(), load);
                        List validModuleInterfaces = BaseModuleValidationUtils.getValidModuleInterfaces(getVersion(), load2);
                        List<ServiceModuleInterface> validModuleInterfaces2 = BaseModuleValidationUtils.getValidModuleInterfaces(getVersion(), load3);
                        for (ServiceModuleInterface init : validModuleInterfaces2) {
                            init.init(validInitialisedModuleInterfaces);
                        }
                        AdPresenterNameShaper adPresenterNameShaper = new AdPresenterNameShaper();
                        SmaatoInstance smaatoInstance = new SmaatoInstance(application, config2, Lists.join(Collections.singletonList(DiRegistry.of(new Consumer() {
                            public final void accept(Object obj) {
                                ((DiRegistry) obj).registerSingletonFactory(AdPresenterNameShaper.class, new ClassFactory() {
                                    public final Object get(DiConstructor diConstructor) {
                                        return SmaatoSdk.a(AdPresenterNameShaper.this, diConstructor);
                                    }
                                });
                            }
                        })), ModuleInterfaceUtils.getDiOfModules(validInitialisedModuleInterfaces), BaseDiRegistryUtils.getDiOfModules(validModuleInterfaces2), AdPresenterModuleInterfaceUtils.getDiOfModules(adPresenterNameShaper, validModuleInterfaces)), ModuleInterfaceUtils.getExpectedManifestEntriesFromModules(validInitialisedModuleInterfaces), str);
                        DiConstructor a2 = smaatoInstance.a();
                        boolean isAppManifestConfiguredProperlyForSdk = ((AppManifestConfigChecker) a2.get(AppManifestConfigChecker.class)).check().isAppManifestConfiguredProperlyForSdk();
                        if (!isAppManifestConfiguredProperlyForSdk) {
                            ((Logger) a2.get(Logger.class)).error(LogDomain.CORE, "Cannot initialize SmaatoSdk. Check specific reason(s) in the error/warning message(s) above.", new Object[0]);
                        }
                        if (isAppManifestConfiguredProperlyForSdk) {
                            smaatoInstance.a().get(AppBackgroundDetector.class);
                            smaatoInstance.a().get(ConnectionStatusWatcher.class);
                            f3286a = smaatoInstance;
                        } else {
                            return;
                        }
                    }
                }
            }
            for (SdkInitialisationObserver onInitialised : load4) {
                onInitialised.onInitialised();
            }
            return;
        }
        throw new IllegalArgumentException("Parameter publisherId cannot be empty for SmaatoSdk::init");
    }

    @Nullable
    public static String getKeywords() {
        SmaatoInstance a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.b();
    }

    public static void setKeywords(@Nullable String str) {
        SmaatoInstance a2 = a();
        if (a2 != null) {
            a2.a(str);
        }
    }

    @Nullable
    public static String getSearchQuery() {
        return (String) Objects.transformOrNull(a(), $$Lambda$Acu4pa8q2K7CeUiJdPyk99YzMoI.INSTANCE);
    }

    public static void setSearchQuery(@Nullable String str) {
        Objects.onNotNull(a(), new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((SmaatoInstance) obj).b(this.f$0);
            }
        });
    }

    @Nullable
    public static Gender getGender() {
        SmaatoInstance a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.d();
    }

    public static void setGender(@Nullable Gender gender) {
        SmaatoInstance a2 = a();
        if (a2 != null) {
            a2.a(gender);
        }
    }

    @Nullable
    public static Integer getAge() {
        SmaatoInstance a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.e();
    }

    public static void setAge(@Nullable Integer num) {
        SmaatoInstance a2 = a();
        if (a2 != null) {
            a2.a(num);
        }
    }

    @Nullable
    public static LatLng getLatLng() {
        SmaatoInstance a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.f();
    }

    public static void setLatLng(@Nullable LatLng latLng) {
        SmaatoInstance a2 = a();
        if (a2 != null) {
            a2.a(latLng);
        }
    }

    @Nullable
    public static String getPublisherId() {
        SmaatoInstance a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.k();
    }

    @Nullable
    public static String getRegion() {
        SmaatoInstance a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.g();
    }

    public static void setRegion(@Nullable String str) {
        SmaatoInstance a2 = a();
        if (a2 != null) {
            a2.c(str);
        }
    }

    @Nullable
    public static String getZip() {
        SmaatoInstance a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.h();
    }

    public static void setZip(@Nullable String str) {
        SmaatoInstance a2 = a();
        if (a2 != null) {
            a2.d(str);
        }
    }

    @Nullable
    public static String getLanguage() {
        SmaatoInstance a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.i();
    }

    public static void setLanguage(@Nullable String str) {
        SmaatoInstance a2 = a();
        if (a2 != null) {
            a2.e(str);
        }
    }

    public static boolean getCoppa() {
        SmaatoInstance a2 = a();
        return a2 != null && a2.j();
    }

    public static void setCoppa(boolean z) {
        SmaatoInstance a2 = a();
        if (a2 != null) {
            a2.a(z);
        }
    }

    @Nullable
    private static SmaatoInstance a() {
        SmaatoInstance smaatoInstance = f3286a;
        if (smaatoInstance != null) {
            return smaatoInstance;
        }
        Log.e(LogDomain.CORE.name(), "SmaatoSdk.init() should be called first.");
        return null;
    }
}
