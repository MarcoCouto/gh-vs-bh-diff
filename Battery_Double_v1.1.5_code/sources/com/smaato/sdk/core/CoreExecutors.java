package com.smaato.sdk.core;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Lazy;
import com.smaato.sdk.core.util.Lazy.CC;
import com.smaato.sdk.flow.FlowExecutors;
import com.smaato.sdk.flow.FlowExecutors.NamedFactory;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CoreExecutors implements FlowExecutors {

    /* renamed from: a reason: collision with root package name */
    private final Lazy<Executor> f3280a = CC.of($$Lambda$CoreExecutors$jYOY9js5s6jP7zBnb3j6mOaL4zg.INSTANCE);
    private final Lazy<Executor> b = CC.of($$Lambda$CoreExecutors$KgWSCQodd60zO3HxfFkYjhzvH0.INSTANCE);

    private static class MainExecutor implements Executor {

        /* renamed from: a reason: collision with root package name */
        private final Handler f3281a;

        private MainExecutor() {
            this.f3281a = new Handler(Looper.getMainLooper());
        }

        /* synthetic */ MainExecutor(byte b) {
            this();
        }

        public void execute(Runnable runnable) {
            this.f3281a.post(runnable);
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Executor b() {
        int max = Math.max(2, Runtime.getRuntime().availableProcessors());
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(max, Math.max(2, (Runtime.getRuntime().availableProcessors() * 2) + 1), 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new NamedFactory("io", 1));
        return threadPoolExecutor;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Executor a() {
        return new MainExecutor(0);
    }

    @NonNull
    public Executor io() {
        return (Executor) this.f3280a.get();
    }

    @NonNull
    public Executor main() {
        return (Executor) this.b.get();
    }
}
