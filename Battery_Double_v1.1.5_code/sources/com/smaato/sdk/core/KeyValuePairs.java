package com.smaato.sdk.core;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class KeyValuePairs implements Cloneable {

    /* renamed from: a reason: collision with root package name */
    private static final String f3283a = "KeyValuePairs";
    @NonNull
    private final Map<String, Set<String>> b = Collections.synchronizedMap(new LinkedHashMap(7));

    public final void addTargetingValue(@NonNull String str, @NonNull String str2) {
        if (b(str, str2)) {
            Set set = (Set) this.b.get(str);
            if (set == null) {
                a(str, str2);
            } else if (set.size() >= 7) {
                Log.e(f3283a, String.format("%s values limit has been reached.", new Object[]{str}));
            } else {
                if (!set.add(str2)) {
                    Log.e(f3283a, String.format("%s value is already presents for the %s key.", new Object[]{str2, str}));
                }
            }
        }
    }

    public final void setTargetingValue(@NonNull String str, @NonNull String str2) {
        if (b(str, str2)) {
            a(str, str2);
        }
    }

    public final void setTargetingValues(@NonNull String str, @NonNull String... strArr) {
        if (b(str, strArr)) {
            a(str, strArr);
        }
    }

    public final void removeAllKeyValuePairs() {
        this.b.clear();
    }

    public final void removeKeyValuePair(@NonNull String str) {
        this.b.remove(str);
    }

    private void a(@NonNull String str, @NonNull String... strArr) {
        Objects.requireNonNull(strArr);
        int i = 7;
        LinkedHashSet linkedHashSet = new LinkedHashSet(7);
        if (strArr.length > 7) {
            String[] strArr2 = new String[(strArr.length - 7)];
            System.arraycopy(strArr, 7, strArr2, 0, strArr.length - 7);
            Log.e(f3283a, String.format("Amount of adding values (%s) exceeds maximal capacity (%s);\nSkipped values: %s", new Object[]{Integer.valueOf(strArr.length), Integer.valueOf(7), Joiner.join((CharSequence) ", ", (Object[]) strArr2)}));
        } else {
            i = strArr.length;
        }
        linkedHashSet.addAll(Arrays.asList(strArr).subList(0, i));
        this.b.put(str, Collections.synchronizedSet(linkedHashSet));
    }

    @NonNull
    public final Map<String, Set<String>> getAllKeyValuePairs() {
        Set<Entry> entrySet = this.b.entrySet();
        LinkedHashMap linkedHashMap = new LinkedHashMap(entrySet.size());
        for (Entry entry : entrySet) {
            linkedHashMap.put(entry.getKey(), new LinkedHashSet((Collection) entry.getValue()));
        }
        return linkedHashMap;
    }

    private static boolean b(@Nullable String str, @Nullable String... strArr) {
        if (TextUtils.isEmpty(str)) {
            Log.e(f3283a, "key can not be null or empty.");
            return false;
        } else if (strArr == null || strArr.length == 0) {
            Log.e(f3283a, "values can not be null or empty.");
            return false;
        } else {
            for (String isEmpty : strArr) {
                if (TextUtils.isEmpty(isEmpty)) {
                    Log.e(f3283a, "value can not be null or empty.");
                    return false;
                }
            }
            return true;
        }
    }

    @NonNull
    public final KeyValuePairs clone() {
        KeyValuePairs keyValuePairs = new KeyValuePairs();
        synchronized (this.b) {
            for (Entry entry : this.b.entrySet()) {
                Set set = (Set) entry.getValue();
                keyValuePairs.a((String) entry.getKey(), (String[]) set.toArray(new String[set.size()]));
            }
        }
        return keyValuePairs;
    }
}
