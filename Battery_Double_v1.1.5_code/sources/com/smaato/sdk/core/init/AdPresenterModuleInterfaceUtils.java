package com.smaato.sdk.core.init;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.NullableFunction;
import java.util.ArrayList;
import java.util.List;

public final class AdPresenterModuleInterfaceUtils {
    private AdPresenterModuleInterfaceUtils() {
    }

    @NonNull
    public static List<AdPresenterModuleInterface> getValidModuleInterfaces(@NonNull String str, @NonNull Iterable<AdPresenterModuleInterface> iterable) {
        ArrayList arrayList = new ArrayList();
        for (AdPresenterModuleInterface adPresenterModuleInterface : iterable) {
            if (adPresenterModuleInterface.version().equals(str)) {
                arrayList.add(adPresenterModuleInterface);
            }
        }
        return arrayList;
    }

    @NonNull
    public static List<DiRegistry> getDiOfModules(@NonNull AdPresenterNameShaper adPresenterNameShaper, @NonNull List<AdPresenterModuleInterface> list) {
        Objects.requireNonNull(list);
        return Lists.map(list, new NullableFunction() {
            public final Object apply(Object obj) {
                return AdPresenterModuleInterfaceUtils.a(AdPresenterNameShaper.this, (AdPresenterModuleInterface) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ DiRegistry a(AdPresenterNameShaper adPresenterNameShaper, AdPresenterModuleInterface adPresenterModuleInterface) {
        AdPresenterModuleInterface adPresenterModuleInterface2 = (AdPresenterModuleInterface) Objects.requireNonNull(adPresenterModuleInterface);
        Objects.requireNonNull(adPresenterModuleInterface2);
        return DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                ((DiRegistry) obj).registerFactory(AdPresenterModuleInterface.this.moduleDiName(), AdLoaderPlugin.class, (ClassFactory) Objects.requireNonNull(AdPresenterModuleInterface.this.getAdLoaderPluginFactory()));
            }
        }).addFrom(adPresenterModuleInterface2.moduleDiRegistry()).addFrom(adPresenterModuleInterface2.moduleAdPresenterDiRegistry(adPresenterNameShaper));
    }
}
