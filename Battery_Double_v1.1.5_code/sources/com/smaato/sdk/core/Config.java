package com.smaato.sdk.core;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.LogLevel;
import com.smaato.sdk.core.util.Objects;

public final class Config {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final LogLevel f3278a;
    private final boolean b;
    private final boolean c;

    public static class ConfigBuilder {

        /* renamed from: a reason: collision with root package name */
        private LogLevel f3279a = LogLevel.INFO;
        private boolean b;
        private boolean c;

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        @NonNull
        public final ConfigBuilder a(boolean z) {
            this.c = true;
            return this;
        }

        @NonNull
        public ConfigBuilder setLogLevel(@NonNull LogLevel logLevel) {
            if (logLevel != null) {
                this.f3279a = logLevel;
            } else {
                Log.w(LogDomain.CORE.name(), String.format("setting logLevel to null is ignored, current value = %s", new Object[]{this.f3279a}));
            }
            return this;
        }

        @NonNull
        public ConfigBuilder setHttpsOnly(boolean z) {
            this.b = z;
            return this;
        }

        @NonNull
        public Config build() {
            return new Config(this.c, this.f3279a, this.b, 0);
        }
    }

    /* synthetic */ Config(boolean z, LogLevel logLevel, boolean z2, byte b2) {
        this(z, logLevel, z2);
    }

    private Config(boolean z, @NonNull LogLevel logLevel, boolean z2) {
        this.b = z;
        this.f3278a = (LogLevel) Objects.requireNonNull(logLevel);
        this.c = z2;
    }

    @NonNull
    public static ConfigBuilder builder() {
        return new ConfigBuilder().a(true).setLogLevel(LogLevel.INFO).setHttpsOnly(false);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final LogLevel b() {
        return this.f3278a;
    }

    public final boolean isHttpsOnly() {
        return this.c;
    }
}
