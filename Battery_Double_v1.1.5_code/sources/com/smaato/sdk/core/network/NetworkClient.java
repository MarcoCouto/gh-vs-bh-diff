package com.smaato.sdk.core.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.network.execution.NetworkLayerException;

public interface NetworkClient {

    public enum Error {
        CANCELLED,
        TIMEOUT,
        IO_ERROR,
        GENERIC,
        IO_TOO_MANY_REDIRECTS,
        NO_NETWORK_CONNECTION
    }

    public interface Listener {
        void onRequestError(@NonNull NetworkClient networkClient, @NonNull Task task, @NonNull NetworkLayerException networkLayerException);

        void onRequestSuccess(@NonNull NetworkClient networkClient, @NonNull Task task, @NonNull NetworkResponse networkResponse);
    }

    Task performNetworkRequest(@NonNull NetworkRequest networkRequest, @Nullable SomaApiContext somaApiContext);

    void setListener(@NonNull Listener listener);
}
