package com.smaato.sdk.core.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkClient.Listener;
import com.smaato.sdk.core.network.execution.Executioner;
import com.smaato.sdk.core.network.execution.NetworkLayerException;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.lang.ref.WeakReference;

public class NetworkHttpClient implements NetworkClient {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3405a;
    @NonNull
    private final Executioner<NetworkRequest, NetworkResponse, NetworkLayerException> b;
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<Listener> c = new WeakReference<>(null);

    public NetworkHttpClient(@NonNull Logger logger, @NonNull Executioner<NetworkRequest, NetworkResponse, NetworkLayerException> executioner) {
        this.f3405a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for NetworkHttpClient::new");
        this.b = (Executioner) Objects.requireNonNull(executioner, "Parameter executioner cannot be null for NetworkHttpClient::new");
    }

    public void setListener(@NonNull Listener listener) {
        this.c = new WeakReference<>(Objects.requireNonNull(listener));
    }

    @NonNull
    public Task performNetworkRequest(@NonNull NetworkRequest networkRequest, @Nullable SomaApiContext somaApiContext) {
        Objects.requireNonNull(networkRequest);
        return this.b.submitRequest(networkRequest, somaApiContext, new Task.Listener<NetworkResponse, NetworkLayerException>() {
            public void onSuccess(@NonNull Task task, @NonNull NetworkResponse networkResponse) {
                NetworkHttpClient.this.f3405a.debug(LogDomain.NETWORK, "Task Success result %s", networkResponse);
                Objects.onNotNull(NetworkHttpClient.this.c.get(), new Consumer(task, networkResponse) {
                    private final /* synthetic */ Task f$1;
                    private final /* synthetic */ NetworkResponse f$2;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    public final void accept(Object obj) {
                        AnonymousClass1.this.a(this.f$1, this.f$2, (Listener) obj);
                    }
                });
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void a(Task task, NetworkResponse networkResponse, Listener listener) {
                listener.onRequestSuccess(NetworkHttpClient.this, task, networkResponse);
            }

            public void onFailure(@NonNull Task task, @NonNull NetworkLayerException networkLayerException) {
                NetworkHttpClient.this.f3405a.error(LogDomain.NETWORK, "Task Failure result %s", networkLayerException);
                Objects.onNotNull(NetworkHttpClient.this.c.get(), new Consumer(task, networkLayerException) {
                    private final /* synthetic */ Task f$1;
                    private final /* synthetic */ NetworkLayerException f$2;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    public final void accept(Object obj) {
                        AnonymousClass1.this.a(this.f$1, this.f$2, (Listener) obj);
                    }
                });
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void a(Task task, NetworkLayerException networkLayerException, Listener listener) {
                listener.onRequestError(NetworkHttpClient.this, task, networkLayerException);
            }
        });
    }
}
