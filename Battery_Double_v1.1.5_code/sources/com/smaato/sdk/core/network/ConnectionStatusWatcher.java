package com.smaato.sdk.core.network;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.notifier.ChangeNotifier;
import com.smaato.sdk.core.util.notifier.ChangeSender;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;

public class ConnectionStatusWatcher {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ChangeSender<Whatever> f3400a = ChangeSenderUtils.createDebounceChangeSender(Whatever.INSTANCE, 500);

    private static final class ConnectionBroadcastReceiver extends BroadcastReceiver {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final ChangeSender<Whatever> f3401a;

        /* synthetic */ ConnectionBroadcastReceiver(ChangeSender changeSender, byte b) {
            this(changeSender);
        }

        private ConnectionBroadcastReceiver(@NonNull ChangeSender<Whatever> changeSender) {
            this.f3401a = (ChangeSender) Objects.requireNonNull(changeSender, "Parameter changeSender cannot be null for ConnectionBroadcastReceiver::new");
        }

        public final void onReceive(Context context, Intent intent) {
            this.f3401a.newValue(Whatever.INSTANCE);
        }
    }

    @RequiresApi(28)
    private static final class SomaNetworkCallback extends NetworkCallback {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final ChangeSender<Whatever> f3402a;

        /* synthetic */ SomaNetworkCallback(ChangeSender changeSender, byte b) {
            this(changeSender);
        }

        private SomaNetworkCallback(@NonNull ChangeSender<Whatever> changeSender) {
            this.f3402a = (ChangeSender) Objects.requireNonNull(changeSender, "Parameter changeSender cannot be null for SomaNetworkCallback::new");
        }

        public final void onAvailable(Network network) {
            this.f3402a.newValue(Whatever.INSTANCE);
        }

        public final void onLost(Network network) {
            this.f3402a.newValue(Whatever.INSTANCE);
        }
    }

    ConnectionStatusWatcher(@NonNull Logger logger, @NonNull Application application) {
        Objects.requireNonNull(application, "Parameter application cannot be null for ConnectionStatusWatcher::new");
        Objects.requireNonNull(logger, "Parameter logger cannot be null for ConnectionStatusWatcher::new");
        if (VERSION.SDK_INT >= 28) {
            ConnectivityManager connectivityManager = (ConnectivityManager) application.getSystemService(ConnectivityManager.class);
            if (connectivityManager != null) {
                connectivityManager.registerDefaultNetworkCallback(new SomaNetworkCallback(this.f3400a, 0));
            } else {
                logger.error(LogDomain.CORE, "Current android version does not have ConnectivityManager service", new Object[0]);
            }
        } else {
            application.registerReceiver(new ConnectionBroadcastReceiver(this.f3400a, 0), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @NonNull
    public ChangeNotifier<Whatever> getStatusNotifier() {
        return this.f3400a;
    }
}
