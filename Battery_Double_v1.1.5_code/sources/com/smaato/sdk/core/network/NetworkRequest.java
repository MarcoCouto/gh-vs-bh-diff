package com.smaato.sdk.core.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.List;
import java.util.Map;

public interface NetworkRequest {
    public static final int DEFAULT_CONNECT_TIMEOUT_SECONDS = 30000;
    public static final int DEFAULT_READ_TIMEOUT_SECONDS = 30000;

    public enum Method {
        GET(HttpRequest.METHOD_GET),
        POST(HttpRequest.METHOD_POST),
        PUT(HttpRequest.METHOD_PUT),
        DELETE(HttpRequest.METHOD_DELETE);
        

        /* renamed from: a reason: collision with root package name */
        private static final BodyValidator f3410a = null;
        private static final BodyValidator b = null;
        @NonNull
        private final String c;
        @NonNull
        private BodyValidator d;

        @FunctionalInterface
        private interface BodyValidator {
            boolean validate(@Nullable byte[] bArr);
        }

        static {
            f3410a = $$Lambda$NetworkRequest$Method$y6xLf31tXkbOJXUeRAbfOWteVFg.INSTANCE;
            b = $$Lambda$3Birl1zLlr6NOO9XkWbSulf003w.INSTANCE;
            GET.d = b;
            POST.d = f3410a;
            PUT.d = f3410a;
            DELETE.d = b;
        }

        private Method(String str) {
            this.c = str;
        }

        @NonNull
        public final String getMethodName() {
            return this.c;
        }

        public final boolean validateBody(@Nullable byte[] bArr) {
            return this.d.validate(bArr);
        }
    }

    @Nullable
    byte[] getBody();

    int getConnectTimeout();

    @NonNull
    Map<String, List<String>> getHeaders();

    @NonNull
    Method getMethod();

    @NonNull
    Map<String, String> getQueryItems();

    int getReadTimeout();

    @NonNull
    String getUrl();
}
