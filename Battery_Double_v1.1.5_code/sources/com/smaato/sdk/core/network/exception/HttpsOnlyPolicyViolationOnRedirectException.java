package com.smaato.sdk.core.network.exception;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public class HttpsOnlyPolicyViolationOnRedirectException extends Exception {
    @NonNull
    public final String originalUrl;
    @NonNull
    public final String violatedUrl;

    public HttpsOnlyPolicyViolationOnRedirectException(@NonNull String str, @NonNull String str2) {
        this.originalUrl = (String) Objects.requireNonNull(str);
        this.violatedUrl = (String) Objects.requireNonNull(str2);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HttpsOnlyPolicyViolationOnRedirectException{, originalUrl='");
        sb.append(this.originalUrl);
        sb.append('\'');
        sb.append(", violatedUrl='");
        sb.append(this.violatedUrl);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
