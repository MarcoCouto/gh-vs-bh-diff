package com.smaato.sdk.core.network.exception;

import androidx.annotation.NonNull;

public class NetworkNotAvailableException extends Exception {
    public NetworkNotAvailableException(@NonNull Exception exc) {
        super(exc);
    }
}
