package com.smaato.sdk.core.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;
import java.util.Map;

public interface NetworkResponse {
    @Nullable
    byte[] getBody();

    @NonNull
    Map<String, List<String>> getHeaders();

    @NonNull
    String getRequestUrl();

    int getResponseCode();
}
