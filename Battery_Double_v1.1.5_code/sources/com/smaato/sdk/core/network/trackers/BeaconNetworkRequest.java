package com.smaato.sdk.core.network.trackers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.util.Objects;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class BeaconNetworkRequest implements NetworkRequest {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3420a;

    @Nullable
    public byte[] getBody() {
        return null;
    }

    public int getConnectTimeout() {
        return 30000;
    }

    public int getReadTimeout() {
        return 30000;
    }

    public BeaconNetworkRequest(@NonNull String str) {
        this.f3420a = (String) Objects.requireNonNull(str, "Parameter url cannot be null for BeaconNetworkRequest::BeaconNetworkRequest");
    }

    @NonNull
    public String getUrl() {
        return this.f3420a;
    }

    @NonNull
    public Map<String, String> getQueryItems() {
        return Collections.emptyMap();
    }

    @NonNull
    public Method getMethod() {
        return Method.GET;
    }

    @NonNull
    public Map<String, List<String>> getHeaders() {
        return Collections.EMPTY_MAP;
    }
}
