package com.smaato.sdk.core.network.trackers;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.network.exception.HttpsOnlyPolicyViolationException;
import com.smaato.sdk.core.network.exception.HttpsOnlyPolicyViolationOnRedirectException;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.violationreporter.AdQualityViolationReporter;

public class BeaconTrackerAdQualityViolationUtils {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final AdQualityViolationReporter f3423a;

    public BeaconTrackerAdQualityViolationUtils(@NonNull AdQualityViolationReporter adQualityViolationReporter) {
        this.f3423a = (AdQualityViolationReporter) Objects.requireNonNull(adQualityViolationReporter);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final void a(@NonNull SomaApiContext somaApiContext, @NonNull Exception exc) {
        if (exc instanceof HttpsOnlyPolicyViolationException) {
            this.f3423a.reportAdViolation("SOMAAdViolationSSLBeacon", somaApiContext, ((HttpsOnlyPolicyViolationException) exc).violatedUrl, somaApiContext.getApiAdResponse().getRequestUrl());
            return;
        }
        if (exc instanceof HttpsOnlyPolicyViolationOnRedirectException) {
            HttpsOnlyPolicyViolationOnRedirectException httpsOnlyPolicyViolationOnRedirectException = (HttpsOnlyPolicyViolationOnRedirectException) exc;
            this.f3423a.reportAdViolation("SOMAAdViolationSSLBeaconHTTPRedirect", somaApiContext, httpsOnlyPolicyViolationOnRedirectException.violatedUrl, httpsOnlyPolicyViolationOnRedirectException.originalUrl);
        }
    }
}
