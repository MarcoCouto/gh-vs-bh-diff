package com.smaato.sdk.core.network;

public enum NetworkConnectionType {
    CARRIER_2G,
    CARRIER_3G,
    CARRIER_4G,
    CARRIER_UNKNOWN,
    WIFI,
    ETHERNET,
    OTHER
}
