package com.smaato.sdk.core.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.util.Joiner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class NetworkHttpRequest implements NetworkRequest {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Map<String, String> f3407a;
    /* access modifiers changed from: private */
    @NonNull
    public String b;
    /* access modifiers changed from: private */
    @NonNull
    public Method c;
    /* access modifiers changed from: private */
    @Nullable
    public byte[] d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    @NonNull
    public Map<String, List<String>> g;

    public static final class Builder {

        /* renamed from: a reason: collision with root package name */
        private String f3408a;
        private Method b;
        private int c;
        private int d;
        private Map<String, List<String>> e = new LinkedHashMap();
        private Map<String, String> f = new LinkedHashMap();
        private byte[] g;

        public Builder() {
        }

        public Builder(@NonNull NetworkHttpRequest networkHttpRequest) {
            this.f3408a = networkHttpRequest.b;
            this.f = networkHttpRequest.f3407a;
            this.b = networkHttpRequest.c;
            this.c = networkHttpRequest.e;
            this.d = networkHttpRequest.f;
            this.e = networkHttpRequest.g;
            this.g = networkHttpRequest.d;
        }

        @NonNull
        public final Builder setUrl(@NonNull String str) {
            this.f3408a = str;
            return this;
        }

        @NonNull
        public final Builder setMethod(@NonNull Method method) {
            this.b = method;
            return this;
        }

        @NonNull
        public final Builder setBody(@Nullable byte[] bArr) {
            this.g = bArr;
            return this;
        }

        @NonNull
        public final Builder setConnectionTimeout(int i) {
            this.c = i;
            return this;
        }

        @NonNull
        public final Builder setReadTimeout(int i) {
            this.d = i;
            return this;
        }

        @NonNull
        public final Builder setHeaders(@NonNull Map<String, List<String>> map) {
            this.e = new LinkedHashMap(map);
            return this;
        }

        @NonNull
        public final Builder setQueryItems(@NonNull Map<String, String> map) {
            this.f = new LinkedHashMap(map);
            return this;
        }

        @NonNull
        public final NetworkHttpRequest build() {
            ArrayList arrayList = new ArrayList();
            if (this.f3408a == null) {
                arrayList.add("url");
            }
            if (this.b == null) {
                arrayList.add("method");
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb = new StringBuilder("Missing required properties: ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new IllegalStateException(sb.toString());
            } else if (!this.b.validateBody(this.g)) {
                StringBuilder sb2 = new StringBuilder("Method ");
                sb2.append(this.b);
                sb2.append(" has invalid body. Body exists: ");
                sb2.append(this.g != null);
                throw new IllegalStateException(sb2.toString());
            } else {
                NetworkHttpRequest networkHttpRequest = new NetworkHttpRequest(this.f3408a, this.f, this.b, this.g, this.c, this.d, this.e, 0);
                return networkHttpRequest;
            }
        }
    }

    public static class Headers {
        public static final String KEY_CONTENT_TYPE = "Content-Type";
        public static final String KEY_USER_AGENT = "User-Agent";
    }

    /* synthetic */ NetworkHttpRequest(String str, Map map, Method method, byte[] bArr, int i, int i2, Map map2, byte b2) {
        this(str, map, method, bArr, i, i2, map2);
    }

    private NetworkHttpRequest(@NonNull String str, @NonNull Map<String, String> map, @NonNull Method method, @Nullable byte[] bArr, int i, int i2, @NonNull Map<String, List<String>> map2) {
        this.b = str;
        this.f3407a = map;
        this.c = method;
        this.d = bArr;
        this.e = i;
        this.f = i2;
        this.g = map2;
    }

    @NonNull
    public final String getUrl() {
        return this.b;
    }

    @NonNull
    public final Map<String, String> getQueryItems() {
        return this.f3407a;
    }

    @NonNull
    public final Method getMethod() {
        return this.c;
    }

    @Nullable
    public final byte[] getBody() {
        return this.d;
    }

    public final int getConnectTimeout() {
        if (this.e > 0) {
            return this.e;
        }
        return 30000;
    }

    public final int getReadTimeout() {
        if (this.f > 0) {
            return this.f;
        }
        return 30000;
    }

    @NonNull
    public final Map<String, List<String>> getHeaders() {
        return this.g;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NetworkHttpRequest networkHttpRequest = (NetworkHttpRequest) obj;
        if (this.e == networkHttpRequest.e && this.f == networkHttpRequest.f && this.b.equals(networkHttpRequest.b) && this.f3407a.equals(networkHttpRequest.f3407a) && this.c == networkHttpRequest.c && Arrays.equals(this.d, networkHttpRequest.d)) {
            return this.g.equals(networkHttpRequest.g);
        }
        return false;
    }

    public final int hashCode() {
        return (((((((((((this.b.hashCode() * 31) + this.f3407a.hashCode()) * 31) + this.c.hashCode()) * 31) + Arrays.hashCode(this.d)) * 31) + this.e) * 31) + this.f) * 31) + this.g.hashCode();
    }
}
