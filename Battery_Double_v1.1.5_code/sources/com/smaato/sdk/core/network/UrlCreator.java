package com.smaato.sdk.core.network;

import android.net.Uri;
import android.net.Uri.Builder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.Map;
import java.util.Map.Entry;

public final class UrlCreator {
    @NonNull
    public final String createUrl(@NonNull String str, @NonNull Map<String, String> map) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(map);
        Builder buildUpon = Uri.parse(str).buildUpon();
        for (Entry entry : map.entrySet()) {
            buildUpon.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
        }
        return buildUpon.build().toString();
    }

    @Nullable
    public final String extractHostname(@NonNull String str) {
        Objects.requireNonNull(str, "Parameter url cannot be null for UrlCreator::extractHostname");
        return Uri.parse(str).getHost();
    }

    @Nullable
    public final String extractScheme(@NonNull String str) {
        Objects.requireNonNull(str, "Parameter url cannot be null for UrlCreator::extractScheme");
        return Uri.parse(str).getScheme();
    }

    public final boolean isSecureScheme(@Nullable String str) {
        return "https".equalsIgnoreCase(str);
    }

    public final boolean isInsecureScheme(@Nullable String str) {
        return "http".equalsIgnoreCase(str);
    }

    public final boolean isSupportedForNetworking(@NonNull String str) {
        String extractScheme = extractScheme(str);
        return isSecureScheme(extractScheme) || isInsecureScheme(extractScheme);
    }
}
