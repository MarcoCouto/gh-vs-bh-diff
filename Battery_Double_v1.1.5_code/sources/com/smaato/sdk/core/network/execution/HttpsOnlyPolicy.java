package com.smaato.sdk.core.network.execution;

import android.os.Build.VERSION;
import android.security.NetworkSecurityPolicy;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.UrlCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Optional;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.core.util.fi.Supplier;
import java.util.Collection;
import java.util.Set;

public final class HttpsOnlyPolicy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3415a;
    @NonNull
    private final Set<String> b;
    @NonNull
    private final UrlCreator c;
    @NonNull
    private final Optional<NetworkSecurityPolicy> d;

    public HttpsOnlyPolicy(@NonNull Logger logger, @NonNull Collection<String> collection, @NonNull UrlCreator urlCreator, @NonNull Optional<NetworkSecurityPolicy> optional) {
        this.f3415a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for HttpsOnlyPolicy::new");
        this.b = Sets.toImmutableSet((Collection) Objects.requireNonNull(collection, "Parameter baseUrls cannot be null for HttpsOnlyPolicy::new"));
        this.c = (UrlCreator) Objects.requireNonNull(urlCreator, "Parameter urlCreator cannot be null for HttpsOnlyPolicy::new");
        this.d = (Optional) Objects.requireNonNull(optional, "Parameter networkSecurityPolicyOptional cannot be null for HttpsOnlyPolicy::new");
    }

    public final boolean validateUrl(@Nullable SomaApiContext somaApiContext, @NonNull String str) {
        if (somaApiContext == null) {
            Set<String> set = this.b;
            str.getClass();
            if (Lists.any(set, new Predicate(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final boolean test(Object obj) {
                    return this.f$0.startsWith((String) obj);
                }
            })) {
                return true;
            }
        }
        return ((Boolean) this.d.filter($$Lambda$HttpsOnlyPolicy$dU8yp8odzCXyUQ8jbAKPH96fbuQ.INSTANCE).map(new Function(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return HttpsOnlyPolicy.this.a(this.f$1, (NetworkSecurityPolicy) obj);
            }
        }).map(new Function(str, somaApiContext) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ SomaApiContext f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object apply(Object obj) {
                return HttpsOnlyPolicy.this.a(this.f$1, this.f$2, (Boolean) obj);
            }
        }).orElseGet(new Supplier(str, somaApiContext) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ SomaApiContext f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object get() {
                return HttpsOnlyPolicy.this.a(this.f$1, this.f$2);
            }
        })).booleanValue();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean a(NetworkSecurityPolicy networkSecurityPolicy) {
        return VERSION.SDK_INT >= 23 && !networkSecurityPolicy.isCleartextTrafficPermitted();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Boolean a(String str, NetworkSecurityPolicy networkSecurityPolicy) {
        return Boolean.valueOf(VERSION.SDK_INT >= 24 && networkSecurityPolicy.isCleartextTrafficPermitted(this.c.extractHostname(str)));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Boolean a(String str, SomaApiContext somaApiContext, Boolean bool) {
        String extractScheme = this.c.extractScheme(str);
        boolean z = false;
        boolean z2 = this.c.isSecureScheme(extractScheme) || (this.c.isInsecureScheme(extractScheme) && somaApiContext != null && !somaApiContext.isHttpsOnly());
        if (bool.booleanValue() || z2) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Boolean a(String str, SomaApiContext somaApiContext) {
        String extractScheme = this.c.extractScheme(str);
        boolean z = this.c.isSecureScheme(extractScheme) || (this.c.isInsecureScheme(extractScheme) && somaApiContext != null && !somaApiContext.isHttpsOnly());
        if (!z) {
            this.f3415a.error(LogDomain.NETWORK, "Invalid url or violation of httpsOnly rule: Url: %s , SomaApiContext: %s", str, somaApiContext);
        }
        return Boolean.valueOf(z);
    }
}
