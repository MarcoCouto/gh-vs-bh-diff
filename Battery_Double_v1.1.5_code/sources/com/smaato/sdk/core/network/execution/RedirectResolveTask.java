package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkHttpRequest.Builder;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.network.execution.ClickThroughUrlRedirectResolver.ConnectionConfig;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import java.util.concurrent.ExecutorService;

public final class RedirectResolveTask extends NetworkTask<String> {
    private RedirectResolveTask(@NonNull ExecutorService executorService, @NonNull Function<NetworkTask<String>, Runnable> function) {
        super(executorService, function);
    }

    @NonNull
    public static RedirectResolveTask create(@NonNull Logger logger, @NonNull ExecutorService executorService, @NonNull String str, @NonNull Listener<String, Exception> listener, @NonNull NetworkActions networkActions, @NonNull ErrorMapper<Exception> errorMapper, @NonNull ClickThroughUrlRedirectResolver clickThroughUrlRedirectResolver, @NonNull SomaApiContext somaApiContext) {
        $$Lambda$RedirectResolveTask$3SDv51pA14yj1qBQ05Zs71sTY r1 = new Function(clickThroughUrlRedirectResolver, str, ConnectionConfig.fromRequest(new Builder().setMethod(Method.GET).setUrl(str).build()), somaApiContext, logger, errorMapper, listener) {
            private final /* synthetic */ ClickThroughUrlRedirectResolver f$1;
            private final /* synthetic */ String f$2;
            private final /* synthetic */ ConnectionConfig f$3;
            private final /* synthetic */ SomaApiContext f$4;
            private final /* synthetic */ Logger f$5;
            private final /* synthetic */ ErrorMapper f$6;
            private final /* synthetic */ Listener f$7;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
            }

            public final Object apply(Object obj) {
                return RedirectResolveTask.a(NetworkActions.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, (NetworkTask) obj);
            }
        };
        ExecutorService executorService2 = executorService;
        return new RedirectResolveTask(executorService, r1);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Runnable a(NetworkActions networkActions, ClickThroughUrlRedirectResolver clickThroughUrlRedirectResolver, String str, ConnectionConfig connectionConfig, SomaApiContext somaApiContext, Logger logger, ErrorMapper errorMapper, Listener listener, NetworkTask networkTask) {
        $$Lambda$RedirectResolveTask$2uB636ZWznd_JwAO1ARNaSccEA r0 = new Supplier(clickThroughUrlRedirectResolver, str, connectionConfig, somaApiContext) {
            private final /* synthetic */ ClickThroughUrlRedirectResolver f$1;
            private final /* synthetic */ String f$2;
            private final /* synthetic */ ConnectionConfig f$3;
            private final /* synthetic */ SomaApiContext f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final Object get() {
                return RedirectResolveTask.a(NetworkActions.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        return createRunnable(networkTask, r0, standardResultHandler(logger, errorMapper, networkTask, listener));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ TaskStepResult a(NetworkActions networkActions, ClickThroughUrlRedirectResolver clickThroughUrlRedirectResolver, String str, ConnectionConfig connectionConfig, SomaApiContext somaApiContext) {
        return (TaskStepResult) networkActions.wrapIo(new IoFunction(str, connectionConfig, somaApiContext) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ ConnectionConfig f$2;
            private final /* synthetic */ SomaApiContext f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final Object apply(Object obj) {
                return ClickThroughUrlRedirectResolver.this.resolve(this.f$1, this.f$2, this.f$3);
            }
        }).apply(Whatever.INSTANCE);
    }
}
