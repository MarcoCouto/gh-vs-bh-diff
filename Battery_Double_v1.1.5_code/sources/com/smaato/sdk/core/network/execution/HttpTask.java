package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkHttpResponse;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.NetworkResponse;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Function;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.ExecutorService;

public final class HttpTask<Err> extends NetworkTask<NetworkResponse> {
    private HttpTask(@NonNull ExecutorService executorService, @NonNull Function<NetworkTask<NetworkResponse>, Runnable> function) {
        super(executorService, function);
    }

    static <Err> HttpTask<Err> a(@NonNull Logger logger, @NonNull NetworkActions networkActions, @NonNull ExecutorService executorService, @NonNull NetworkRequest networkRequest, @NonNull ErrorMapper<Err> errorMapper, @NonNull Listener<NetworkResponse, Err> listener) {
        Objects.requireNonNull(logger, "Parameter logger cannot be null for HttpTask::create");
        Objects.requireNonNull(networkActions, "Parameter networkActions cannot be null for HttpTask::create");
        Objects.requireNonNull(executorService, "Parameter executorService cannot be null for HttpTask::create");
        Objects.requireNonNull(networkRequest, "Parameter networkRequest cannot be null for HttpTask::create");
        Objects.requireNonNull(errorMapper, "Parameter errorMapper cannot be null for HttpTask::create");
        Objects.requireNonNull(listener, "Parameter listener cannot be null for HttpTask::create");
        $$Lambda$HttpTask$dTwzXvthT5pX3Rq5eREp4XNRj38 r1 = new Function(networkRequest, logger, errorMapper, listener) {
            private final /* synthetic */ NetworkRequest f$1;
            private final /* synthetic */ Logger f$2;
            private final /* synthetic */ ErrorMapper f$3;
            private final /* synthetic */ Listener f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final Object apply(Object obj) {
                return HttpTask.createRunnable((NetworkTask) obj, NetworkActions.this, this.f$1, null, new IoFunction() {
                    public final Object apply(Object obj) {
                        return HttpTask.a(Logger.this, (HttpURLConnection) obj);
                    }
                }, HttpTask.standardResultHandler(this.f$2, this.f$3, (NetworkTask) obj, this.f$4));
            }
        };
        return new HttpTask<>(executorService, r1);
    }

    /* access modifiers changed from: private */
    @NonNull
    public static <Err> TaskStepResult<NetworkResponse, Err> a(@NonNull Logger logger, @NonNull HttpURLConnection httpURLConnection) throws IOException {
        Throwable th;
        Throwable th2;
        Throwable th3;
        int responseCode = httpURLConnection.getResponseCode();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(responseCode >= 200 && responseCode < 300 ? httpURLConnection.getInputStream() : httpURLConnection.getErrorStream());
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read == -1) {
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        byteArrayOutputStream.close();
                        bufferedInputStream.close();
                        NetworkHttpResponse networkHttpResponse = new NetworkHttpResponse(responseCode, httpURLConnection.getHeaderFields(), byteArray, httpURLConnection.getURL().toString());
                        logger.debug(LogDomain.NETWORK, "NetworkResponse: %s", networkHttpResponse);
                        return TaskStepResult.success(networkHttpResponse);
                    } else if (Thread.currentThread().isInterrupted()) {
                        TaskStepResult<NetworkResponse, Err> cancelled = TaskStepResult.cancelled();
                        byteArrayOutputStream.close();
                        bufferedInputStream.close();
                        return cancelled;
                    } else {
                        byteArrayOutputStream.write(bArr, 0, read);
                    }
                }
            } catch (Throwable th4) {
                Throwable th5 = th4;
                th2 = r10;
                th3 = th5;
            }
            throw th;
            throw th3;
            if (th2 != null) {
                try {
                    byteArrayOutputStream.close();
                } catch (Throwable unused) {
                }
            } else {
                byteArrayOutputStream.close();
            }
            throw th3;
        } catch (Throwable unused2) {
        }
    }
}
