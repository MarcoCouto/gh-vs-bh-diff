package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;

public interface Executioner<Request, Result, Err> {
    @NonNull
    Task submitRequest(@NonNull Request request, @Nullable SomaApiContext somaApiContext, @NonNull Listener<Result, Err> listener);
}
