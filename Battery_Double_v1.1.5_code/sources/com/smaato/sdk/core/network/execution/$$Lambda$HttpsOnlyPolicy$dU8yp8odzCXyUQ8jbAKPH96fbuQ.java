package com.smaato.sdk.core.network.execution;

import android.security.NetworkSecurityPolicy;
import com.smaato.sdk.core.util.fi.Predicate;

/* renamed from: com.smaato.sdk.core.network.execution.-$$Lambda$HttpsOnlyPolicy$dU8yp8odzCXyUQ8jbAKPH96fbuQ reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$HttpsOnlyPolicy$dU8yp8odzCXyUQ8jbAKPH96fbuQ implements Predicate {
    public static final /* synthetic */ $$Lambda$HttpsOnlyPolicy$dU8yp8odzCXyUQ8jbAKPH96fbuQ INSTANCE = new $$Lambda$HttpsOnlyPolicy$dU8yp8odzCXyUQ8jbAKPH96fbuQ();

    private /* synthetic */ $$Lambda$HttpsOnlyPolicy$dU8yp8odzCXyUQ8jbAKPH96fbuQ() {
    }

    public final boolean test(Object obj) {
        return HttpsOnlyPolicy.a((NetworkSecurityPolicy) obj);
    }
}
