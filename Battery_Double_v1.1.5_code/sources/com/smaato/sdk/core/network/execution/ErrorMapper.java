package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.network.exception.HttpsOnlyPolicyViolationException;
import com.smaato.sdk.core.network.exception.HttpsOnlyPolicyViolationOnRedirectException;
import com.smaato.sdk.core.network.exception.NetworkNotAvailableException;
import com.smaato.sdk.core.network.exception.TaskCancelledException;
import com.smaato.sdk.core.network.exception.TooManyRedirectsException;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public interface ErrorMapper<Error> {
    public static final ErrorMapper<Exception> IDENTITY = $$Lambda$ErrorMapper$p7bwQKxDoYJIAtTNrXxbLaHgXKw.INSTANCE;
    public static final ErrorMapper<NetworkLayerException> NETWORK_LAYER_EXCEPTION = $$Lambda$ErrorMapper$BPvuBdpA7Z4rwuTHuW3VgoAXCTw.INSTANCE;
    public static final ErrorMapper<Error> STANDARD = $$Lambda$ErrorMapper$SVgG4JkksHHWwxEcC4AiV9Qyj7g.INSTANCE;

    /* renamed from: com.smaato.sdk.core.network.execution.ErrorMapper$-CC reason: invalid class name */
    public final /* synthetic */ class CC {
        public static /* synthetic */ Exception a(Exception exc) {
            return exc;
        }

        public static /* synthetic */ Error c(Exception exc) {
            try {
                throw exc;
            } catch (TaskCancelledException unused) {
                return Error.CANCELLED;
            } catch (ProtocolException unused2) {
                return Error.IO_ERROR;
            } catch (UnknownHostException unused3) {
                return Error.IO_ERROR;
            } catch (SocketException unused4) {
                return Error.IO_ERROR;
            } catch (NetworkNotAvailableException unused5) {
                return Error.NO_NETWORK_CONNECTION;
            } catch (SocketTimeoutException unused6) {
                return Error.TIMEOUT;
            } catch (IOException unused7) {
                return Error.IO_ERROR;
            } catch (HttpsOnlyPolicyViolationException | HttpsOnlyPolicyViolationOnRedirectException unused8) {
                return Error.GENERIC;
            } catch (TooManyRedirectsException unused9) {
                return Error.IO_TOO_MANY_REDIRECTS;
            } catch (Exception unused10) {
                return Error.GENERIC;
            }
        }

        public static /* synthetic */ NetworkLayerException b(Exception exc) {
            return new NetworkLayerException((Error) ErrorMapper.STANDARD.map(exc), exc);
        }
    }

    @NonNull
    Error map(@NonNull Exception exc);
}
