package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;

public final class TaskStepResult<Success, Error> {
    @Nullable
    public final Error error;
    public final boolean isCancelled;
    @Nullable
    public final Success success;

    private TaskStepResult(@Nullable Success success2, @Nullable Error error2, boolean z) {
        this.success = success2;
        this.error = error2;
        this.isCancelled = z;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final <NewType> TaskStepResult<NewType, Error> a(@NonNull Function<Success, TaskStepResult<NewType, Error>> function) {
        if (this.isCancelled) {
            return cancelled();
        }
        if (this.success == null) {
            return error(this.error);
        }
        if (Thread.currentThread().isInterrupted()) {
            return cancelled();
        }
        return (TaskStepResult) function.apply(this.success);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final TaskStepResult<Success, Error> a(@NonNull Consumer<Error> consumer) {
        if (this.error != null) {
            consumer.accept(this.error);
        }
        return this;
    }

    @NonNull
    public static <Success, Error> TaskStepResult<Success, Error> success(Success success2) {
        return new TaskStepResult<>(success2, null, false);
    }

    @NonNull
    public static <Success, Error> TaskStepResult<Success, Error> error(Error error2) {
        return new TaskStepResult<>(null, error2, false);
    }

    @NonNull
    public static <Success, Error> TaskStepResult<Success, Error> cancelled() {
        return new TaskStepResult<>(null, null, true);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("TaskStepResult{error=");
        sb.append(this.error);
        sb.append(", success=");
        sb.append(this.success);
        sb.append(", isCancelled=");
        sb.append(this.isCancelled);
        sb.append('}');
        return sb.toString();
    }
}
