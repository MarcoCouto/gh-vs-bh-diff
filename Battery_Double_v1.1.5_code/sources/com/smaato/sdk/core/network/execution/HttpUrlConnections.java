package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.CheckedRunnable;
import java.net.HttpURLConnection;
import java.util.concurrent.atomic.AtomicReference;

final class HttpUrlConnections {
    private HttpUrlConnections() {
    }

    /* access modifiers changed from: 0000 */
    public static void a(@NonNull AtomicReference<HttpURLConnection> atomicReference) {
        Objects.onNotNull(atomicReference.get(), $$Lambda$ZK05deLqwlYiTZB8KEMqoPvpkiY.INSTANCE);
    }

    static void a(@NonNull HttpURLConnection httpURLConnection) {
        Objects.doSilently(new CheckedRunnable(httpURLConnection) {
            private final /* synthetic */ HttpURLConnection f$0;

            {
                this.f$0 = r1;
            }

            public final void run() {
                this.f$0.getInputStream().close();
            }
        });
        Objects.doSilently(new CheckedRunnable(httpURLConnection) {
            private final /* synthetic */ HttpURLConnection f$0;

            {
                this.f$0 = r1;
            }

            public final void run() {
                this.f$0.getErrorStream().close();
            }
        });
        httpURLConnection.disconnect();
    }
}
