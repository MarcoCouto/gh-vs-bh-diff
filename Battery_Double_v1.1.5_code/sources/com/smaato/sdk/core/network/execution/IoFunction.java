package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import java.io.IOException;

@FunctionalInterface
public interface IoFunction<I, R> {
    @NonNull
    R apply(@NonNull I i) throws IOException;
}
