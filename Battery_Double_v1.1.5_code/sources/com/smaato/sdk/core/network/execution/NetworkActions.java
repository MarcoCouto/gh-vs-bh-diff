package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.network.NetworkStateMonitor;
import com.smaato.sdk.core.network.UrlCreator;
import com.smaato.sdk.core.network.exception.HttpsOnlyPolicyViolationException;
import com.smaato.sdk.core.network.exception.HttpsOnlyPolicyViolationOnRedirectException;
import com.smaato.sdk.core.network.exception.NetworkNotAvailableException;
import com.smaato.sdk.core.network.exception.NoRedirectLocationException;
import com.smaato.sdk.core.network.exception.TooManyRedirectsException;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;

public final class NetworkActions {
    protected static final int CHUNK_SIZE_4KB = 4096;
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3416a;
    @NonNull
    private final UrlCreator b;
    @NonNull
    private final NetworkStateMonitor c;
    @NonNull
    private final HttpsOnlyPolicy d;

    public NetworkActions(@NonNull Logger logger, @NonNull UrlCreator urlCreator, @NonNull NetworkStateMonitor networkStateMonitor, @NonNull HttpsOnlyPolicy httpsOnlyPolicy) {
        this.f3416a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for NetworkActions::new");
        this.b = (UrlCreator) Objects.requireNonNull(urlCreator, "Parameter urlCreator cannot be null for NetworkActions::new");
        this.c = (NetworkStateMonitor) Objects.requireNonNull(networkStateMonitor, "Parameter networkStateMonitor cannot be null for NetworkActions::new");
        this.d = (HttpsOnlyPolicy) Objects.requireNonNull(httpsOnlyPolicy, "Parameter httpsOnlyPolicy cannot be null for NetworkActions::new");
    }

    @NonNull
    public final TaskStepResult<HttpURLConnection, Exception> executeConnection(@NonNull String str, @NonNull Map<String, String> map, @NonNull final NetworkRequest networkRequest, @Nullable final SomaApiContext somaApiContext, int i) {
        if (i <= 0) {
            this.f3416a.error(LogDomain.NETWORK, "Redirect limit reached", new Object[0]);
            StringBuilder sb = new StringBuilder("Redirect limit reached. Last url: ");
            sb.append(str);
            return TaskStepResult.error(new TooManyRedirectsException(sb.toString()));
        }
        return a(str, map, networkRequest, somaApiContext, i, new RedirectConnection() {
            @NonNull
            public TaskStepResult<HttpURLConnection, Exception> go(@NonNull String str, int i) {
                return NetworkActions.this.a(str, Collections.emptyMap(), networkRequest, somaApiContext, i, this);
            }
        });
    }

    /* access modifiers changed from: private */
    @NonNull
    public TaskStepResult<HttpURLConnection, Exception> a(@NonNull String str, @NonNull Map<String, String> map, @NonNull NetworkRequest networkRequest, @Nullable SomaApiContext somaApiContext, int i, @NonNull RedirectConnection redirectConnection) {
        AtomicReference atomicReference = new AtomicReference();
        SomaApiContext somaApiContext2 = somaApiContext;
        return executeRequest(somaApiContext, str, map, networkRequest.getMethod().getMethodName(), networkRequest.getConnectTimeout(), networkRequest.getReadTimeout(), networkRequest.getHeaders(), networkRequest.getBody()).a(a(somaApiContext, new RedirectConnection(atomicReference, redirectConnection) {
            private final /* synthetic */ AtomicReference f$0;
            private final /* synthetic */ RedirectConnection f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final TaskStepResult go(String str, int i) {
                return HttpUrlConnections.a(this.f$0);
            }
        }, i)).a((Consumer<Error>) new Consumer(atomicReference) {
            private final /* synthetic */ AtomicReference f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                HttpUrlConnections.a(this.f$0);
            }
        });
    }

    @NonNull
    public final TaskStepResult<HttpURLConnection, Exception> executeRequest(@Nullable SomaApiContext somaApiContext, @NonNull String str, Map<String, String> map, String str2, int i, int i2, Map<String, List<String>> map2, @Nullable byte[] bArr) {
        AtomicReference atomicReference = new AtomicReference();
        String createUrl = this.b.createUrl(str, map);
        this.f3416a.debug(LogDomain.NETWORK, "Url: %s", createUrl);
        TaskStepResult a2 = TaskStepResult.success(createUrl).a((Function<Success, TaskStepResult<NewType, Error>>) new Function(somaApiContext) {
            private final /* synthetic */ SomaApiContext f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return NetworkActions.this.a(this.f$1, (String) obj);
            }
        }).a(wrapIo(new IoFunction(atomicReference) {
            private final /* synthetic */ AtomicReference f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return NetworkActions.this.a(this.f$1, (String) obj);
            }
        }));
        $$Lambda$NetworkActions$4BvPHdp45Zs5owXIXmNupj9UAc r1 = new Function(str2, i, i2, map2) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ int f$2;
            private final /* synthetic */ int f$3;
            private final /* synthetic */ Map f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final Object apply(Object obj) {
                return NetworkActions.this.a(this.f$1, this.f$2, this.f$3, this.f$4, (HttpURLConnection) obj);
            }
        };
        return a2.a((Function<Success, TaskStepResult<NewType, Error>>) r1).a(wrapIo(new IoFunction(bArr) {
            private final /* synthetic */ byte[] f$0;

            {
                this.f$0 = r1;
            }

            public final Object apply(Object obj) {
                return NetworkActions.a(this.f$0, (HttpURLConnection) obj);
            }
        })).a((Consumer<Error>) new Consumer(atomicReference) {
            private final /* synthetic */ AtomicReference f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                HttpUrlConnections.a(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ TaskStepResult a(AtomicReference atomicReference, String str) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection(str);
        atomicReference.set(httpURLConnection);
        return TaskStepResult.success(httpURLConnection);
    }

    @NonNull
    public final URLConnection openConnection(@NonNull String str) throws IOException {
        return new URL(str).openConnection();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ TaskStepResult a(String str, int i, int i2, Map map, HttpURLConnection httpURLConnection) {
        try {
            httpURLConnection.setRequestMethod(str);
            httpURLConnection.setConnectTimeout(i);
            httpURLConnection.setReadTimeout(i2);
            for (Entry entry : map.entrySet()) {
                for (String addRequestProperty : (List) entry.getValue()) {
                    httpURLConnection.addRequestProperty((String) entry.getKey(), addRequestProperty);
                }
            }
            return TaskStepResult.success(httpURLConnection);
        } catch (ProtocolException e) {
            this.f3416a.error(LogDomain.NETWORK, e, "You cannot do %s via http/https", str);
            return TaskStepResult.error(e);
        } catch (SecurityException e2) {
            this.f3416a.error(LogDomain.NETWORK, e2, "Security violation during method connection setup", new Object[0]);
            return TaskStepResult.error(e2);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        r7 = com.smaato.sdk.core.network.execution.TaskStepResult.cancelled();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004d, code lost:
        if (r1 == null) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0052, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0055, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005b, code lost:
        r1.flush();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005e, code lost:
        if (r1 == null) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0063, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x007c, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0080, code lost:
        if (r6 != null) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0086, code lost:
        r0.close();
     */
    public static /* synthetic */ TaskStepResult a(byte[] bArr, HttpURLConnection httpURLConnection) throws IOException {
        OutputStream outputStream;
        Throwable th;
        Throwable th2;
        String requestMethod = httpURLConnection.getRequestMethod();
        if (Method.GET.getMethodName().equals(requestMethod)) {
            httpURLConnection.connect();
            return TaskStepResult.success(httpURLConnection);
        } else if (!Method.POST.getMethodName().equals(requestMethod)) {
            return TaskStepResult.error(new UnsupportedOperationException("Only GET and POST requests for now"));
        } else {
            if (bArr != null) {
                httpURLConnection.setDoOutput(true);
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
                outputStream = httpURLConnection.getOutputStream();
                try {
                    byte[] bArr2 = new byte[4096];
                    while (true) {
                        int read = byteArrayInputStream.read(bArr2);
                        if (read == -1) {
                            break;
                        } else if (Thread.currentThread().isInterrupted()) {
                            break;
                        } else {
                            outputStream.write(bArr2, 0, read);
                        }
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    th2 = r7;
                    th = th4;
                }
            } else {
                httpURLConnection.connect();
            }
            return TaskStepResult.success(httpURLConnection);
        }
        if (outputStream != null) {
            if (th2 != null) {
                try {
                    outputStream.close();
                } catch (Throwable unused) {
                }
            } else {
                outputStream.close();
            }
        }
        throw th;
        throw th;
        throw th;
    }

    @VisibleForTesting
    @NonNull
    private Function<HttpURLConnection, TaskStepResult<HttpURLConnection, Exception>> a(@Nullable SomaApiContext somaApiContext, @NonNull RedirectConnection redirectConnection, int i) {
        return wrapIo(new IoFunction(somaApiContext, redirectConnection, i) {
            private final /* synthetic */ SomaApiContext f$1;
            private final /* synthetic */ RedirectConnection f$2;
            private final /* synthetic */ int f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final Object apply(Object obj) {
                return NetworkActions.this.a(this.f$1, this.f$2, this.f$3, (HttpURLConnection) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ TaskStepResult a(SomaApiContext somaApiContext, RedirectConnection redirectConnection, int i, HttpURLConnection httpURLConnection) throws IOException {
        if (!isRedirect(httpURLConnection)) {
            return TaskStepResult.success(httpURLConnection);
        }
        String headerField = httpURLConnection.getHeaderField("Location");
        if (headerField == null) {
            NoRedirectLocationException noRedirectLocationException = new NoRedirectLocationException();
            this.f3416a.error(LogDomain.NETWORK, noRedirectLocationException, "No redirect location found in response", new Object[0]);
            return TaskStepResult.error(noRedirectLocationException);
        }
        String url = httpURLConnection.getURL().toString();
        if (this.d.validateUrl(somaApiContext, headerField)) {
            this.f3416a.debug(LogDomain.NETWORK, "Handle redirect from: %s to: %s", url, headerField);
            return redirectConnection.go(headerField, i - 1);
        }
        HttpsOnlyPolicyViolationOnRedirectException httpsOnlyPolicyViolationOnRedirectException = new HttpsOnlyPolicyViolationOnRedirectException(url, headerField);
        this.f3416a.error(LogDomain.NETWORK, httpsOnlyPolicyViolationOnRedirectException, "Not allowed to redirect from `%s` to `%s`", url, headerField);
        return TaskStepResult.error(httpsOnlyPolicyViolationOnRedirectException);
    }

    public final boolean isRedirect(@NonNull HttpURLConnection httpURLConnection) throws IOException {
        int responseCode = httpURLConnection.getResponseCode();
        this.f3416a.debug(LogDomain.NETWORK, "Response code: %d", Integer.valueOf(responseCode));
        return responseCode == 302 || responseCode == 301 || responseCode == 303 || responseCode == 307 || responseCode == 308;
    }

    @NonNull
    public final <Input, Output> Function<Input, TaskStepResult<Output, Exception>> wrapIo(@NonNull IoFunction<Input, TaskStepResult<Output, Exception>> ioFunction) {
        return new Function(ioFunction) {
            private final /* synthetic */ IoFunction f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return NetworkActions.this.a(this.f$1, obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public /* synthetic */ TaskStepResult a(IoFunction ioFunction, Object obj) {
        try {
            return (TaskStepResult) ioFunction.apply(obj);
        } catch (SocketException | SocketTimeoutException | UnknownHostException e) {
            return a((Input) obj, e);
        } catch (InterruptedIOException e2) {
            this.f3416a.error(LogDomain.NETWORK, e2, "Task was cancelled", new Object[0]);
            return TaskStepResult.cancelled();
        } catch (IOException e3) {
            return a((Input) obj, e3);
        } catch (Exception e4) {
            this.f3416a.error(LogDomain.NETWORK, e4, "Unexpected error type happened", new Object[0]);
            return TaskStepResult.error(e4);
        }
    }

    @NonNull
    private <Input, Output> TaskStepResult<Output, Exception> a(@NonNull Input input, @NonNull IOException iOException) {
        if (!this.c.isOnline()) {
            this.f3416a.error(LogDomain.NETWORK, iOException, "No Internet connection", new Object[0]);
            return TaskStepResult.error(new NetworkNotAvailableException(iOException));
        }
        this.f3416a.error(LogDomain.NETWORK, iOException, "Failed to perform operation for %s", input);
        return TaskStepResult.error(iOException);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ TaskStepResult a(SomaApiContext somaApiContext, String str) {
        if (str == null) {
            return TaskStepResult.error(new NullPointerException("Passed url cannot be null"));
        }
        if (this.d.validateUrl(somaApiContext, str)) {
            return TaskStepResult.success(str);
        }
        HttpsOnlyPolicyViolationException httpsOnlyPolicyViolationException = new HttpsOnlyPolicyViolationException(str);
        this.f3416a.error(LogDomain.NETWORK, httpsOnlyPolicyViolationException, "Not allowed to follow to `%s`", str);
        return TaskStepResult.error(httpsOnlyPolicyViolationException);
    }
}
