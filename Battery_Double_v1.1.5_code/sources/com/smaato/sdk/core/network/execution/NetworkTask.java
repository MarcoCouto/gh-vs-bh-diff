package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.exception.TaskCancelledException;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import java.net.HttpURLConnection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

public class NetworkTask<Response> implements Task {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ExecutorService f3419a;
    @NonNull
    private final Function<NetworkTask<Response>, Runnable> b;
    @Nullable
    private Future c;
    private volatile int d = 0;

    protected NetworkTask(@NonNull ExecutorService executorService, @NonNull Function<NetworkTask<Response>, Runnable> function) {
        this.f3419a = executorService;
        this.b = function;
    }

    public synchronized void start() {
        if (this.d == 0) {
            this.d = 1;
            this.c = this.f3419a.submit((Runnable) this.b.apply(this));
        }
    }

    public synchronized void cancel() {
        if (this.d == 1) {
            this.d = 3;
            Objects.onNotNull(this.c, $$Lambda$NetworkTask$mF4aX1cHIL8GXGrPFaLL7mq_iPc.INSTANCE);
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public static <Success> Runnable createRunnable(@NonNull NetworkTask<Success> networkTask, @NonNull NetworkActions networkActions, @NonNull NetworkRequest networkRequest, @Nullable SomaApiContext somaApiContext, @NonNull IoFunction<HttpURLConnection, TaskStepResult<Success, Exception>> ioFunction, @NonNull Consumer<TaskStepResult<Success, Exception>> consumer) {
        return createRunnable(networkTask, new Supplier(networkRequest, somaApiContext, ioFunction) {
            private final /* synthetic */ NetworkRequest f$1;
            private final /* synthetic */ SomaApiContext f$2;
            private final /* synthetic */ IoFunction f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final Object get() {
                return NetworkTask.a(NetworkActions.this, this.f$1, this.f$2, this.f$3);
            }
        }, consumer);
    }

    @NonNull
    protected static <Success> Runnable createRunnable(@NonNull NetworkTask<Success> networkTask, @NonNull Supplier<TaskStepResult<Success, Exception>> supplier, @NonNull Consumer<TaskStepResult<Success, Exception>> consumer) {
        return new Runnable(networkTask, consumer) {
            private final /* synthetic */ NetworkTask f$1;
            private final /* synthetic */ Consumer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                NetworkTask.a(Supplier.this, this.f$1, this.f$2);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Supplier supplier, NetworkTask networkTask, Consumer consumer) {
        TaskStepResult taskStepResult = (TaskStepResult) supplier.get();
        if (networkTask.d == 1) {
            networkTask.d = 2;
        }
        consumer.accept(taskStepResult);
    }

    /* access modifiers changed from: private */
    @NonNull
    public static <Success> TaskStepResult<Success, Exception> a(@NonNull NetworkActions networkActions, @NonNull NetworkRequest networkRequest, @Nullable SomaApiContext somaApiContext, @NonNull IoFunction<HttpURLConnection, TaskStepResult<Success, Exception>> ioFunction) {
        AtomicReference atomicReference = new AtomicReference();
        try {
            TaskStepResult executeConnection = networkActions.executeConnection(networkRequest.getUrl(), networkRequest.getQueryItems(), networkRequest, somaApiContext, 16);
            atomicReference.getClass();
            $$Lambda$P7ISafwMIGpYaOcf79OGH0DK84 r9 = new Consumer(atomicReference) {
                private final /* synthetic */ AtomicReference f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.set((HttpURLConnection) obj);
                }
            };
            if (executeConnection.success != null) {
                r9.accept(executeConnection.success);
            }
            return executeConnection.a(networkActions.wrapIo(ioFunction));
        } catch (Exception e) {
            return TaskStepResult.error(e);
        } finally {
            HttpUrlConnections.a(atomicReference);
        }
    }

    protected static <S, E> Consumer<TaskStepResult<S, Exception>> standardResultHandler(@NonNull Logger logger, @NonNull ErrorMapper<E> errorMapper, @NonNull Task task, @NonNull Listener<S, E> listener) {
        return new Consumer(task, errorMapper, logger) {
            private final /* synthetic */ Task f$1;
            private final /* synthetic */ ErrorMapper f$2;
            private final /* synthetic */ Logger f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                NetworkTask.a(Listener.this, this.f$1, this.f$2, this.f$3, (TaskStepResult) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Listener listener, Task task, ErrorMapper errorMapper, Logger logger, TaskStepResult taskStepResult) {
        if (taskStepResult.success != null) {
            listener.onSuccess(task, taskStepResult.success);
        } else if (taskStepResult.isCancelled) {
            listener.onFailure(task, errorMapper.map(new TaskCancelledException()));
        } else if (taskStepResult.error != null) {
            listener.onFailure(task, errorMapper.map((Exception) taskStepResult.error));
        } else {
            logger.error(LogDomain.NETWORK, "Network Task finished in unexpected state: %s", taskStepResult);
            listener.onFailure(task, errorMapper.map(new Exception("Generic")));
        }
    }
}
