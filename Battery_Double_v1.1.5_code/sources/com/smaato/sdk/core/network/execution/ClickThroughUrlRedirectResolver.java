package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.UrlCreator;
import com.smaato.sdk.core.network.execution.ClickThroughUrlRedirectResolver.ConnectionConfig;
import com.smaato.sdk.core.util.Objects;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ClickThroughUrlRedirectResolver {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3412a;
    @NonNull
    private final NetworkActions b;
    @NonNull
    private final UrlCreator c;
    @NonNull
    private final HttpsOnlyPolicy d;

    public static class ConnectionConfig {

        /* renamed from: a reason: collision with root package name */
        private final int f3413a;
        private final int b;
        @NonNull
        private final Map<String, List<String>> c;

        public ConnectionConfig(int i, int i2, @NonNull Map<String, List<String>> map) {
            this.f3413a = i;
            this.b = i2;
            this.c = (Map) Objects.requireNonNull(map, "Parameter requestHeaders cannot be null for ConnectionConfig::new");
        }

        public int getConnectionTimeout() {
            return this.f3413a;
        }

        public int getReadTimeout() {
            return this.b;
        }

        @NonNull
        public Map<String, List<String>> getRequestHeaders() {
            return this.c;
        }

        @NonNull
        public static ConnectionConfig fromRequest(@NonNull NetworkRequest networkRequest) {
            return new ConnectionConfig(networkRequest.getConnectTimeout(), networkRequest.getReadTimeout(), networkRequest.getHeaders());
        }
    }

    public ClickThroughUrlRedirectResolver(@NonNull Logger logger, @NonNull NetworkActions networkActions, @NonNull UrlCreator urlCreator, @NonNull HttpsOnlyPolicy httpsOnlyPolicy) {
        this.f3412a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for ClickThroughUrlRedirectResolver::new");
        this.b = (NetworkActions) Objects.requireNonNull(networkActions, "Parameter networkActions cannot be null for ClickThroughUrlRedirectResolver::new");
        this.c = (UrlCreator) Objects.requireNonNull(urlCreator, "Parameter urlCreator cannot be null for ClickThroughUrlRedirectResolver::new");
        this.d = (HttpsOnlyPolicy) Objects.requireNonNull(httpsOnlyPolicy, "Parameter httpsOnlyPolicy cannot be null for ClickThroughUrlRedirectResolver::new");
    }

    @NonNull
    public TaskStepResult<String, Exception> resolve(@NonNull String str, @NonNull ConnectionConfig connectionConfig, @Nullable SomaApiContext somaApiContext) {
        return a(str, connectionConfig, somaApiContext, 16);
    }

    @NonNull
    private TaskStepResult<String, Exception> a(@NonNull String str, @NonNull ConnectionConfig connectionConfig, @Nullable SomaApiContext somaApiContext, int i) {
        if (i <= 0) {
            this.f3412a.debug(LogDomain.NETWORK, "Redirect limit reached", new Object[0]);
            return TaskStepResult.success(str);
        } else if (!this.c.isSupportedForNetworking(str)) {
            return TaskStepResult.success(str);
        } else {
            SomaApiContext somaApiContext2 = somaApiContext;
            String str2 = str;
            TaskStepResult executeRequest = this.b.executeRequest(somaApiContext2, str2, Collections.emptyMap(), HttpRequest.METHOD_GET, connectionConfig.getConnectionTimeout(), connectionConfig.getReadTimeout(), connectionConfig.getRequestHeaders(), null);
            NetworkActions networkActions = this.b;
            $$Lambda$ClickThroughUrlRedirectResolver$vkk2hWWaGrgV5VHVY4DjSgilyc r2 = new IoFunction(somaApiContext, connectionConfig, i, str) {
                private final /* synthetic */ SomaApiContext f$1;
                private final /* synthetic */ ConnectionConfig f$2;
                private final /* synthetic */ int f$3;
                private final /* synthetic */ String f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                }

                public final Object apply(Object obj) {
                    return ClickThroughUrlRedirectResolver.this.a(this.f$1, this.f$2, this.f$3, this.f$4, (HttpURLConnection) obj);
                }
            };
            return executeRequest.a(networkActions.wrapIo(r2));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ TaskStepResult a(SomaApiContext somaApiContext, ConnectionConfig connectionConfig, int i, String str, HttpURLConnection httpURLConnection) throws IOException {
        HttpUrlConnections.a(httpURLConnection);
        if (!this.b.isRedirect(httpURLConnection)) {
            return TaskStepResult.success(str);
        }
        String headerField = httpURLConnection.getHeaderField("Location");
        if (headerField == null) {
            String str2 = "Redirected Location url is null";
            NullPointerException nullPointerException = new NullPointerException(str2);
            this.f3412a.error(LogDomain.NETWORK, nullPointerException, str2, new Object[0]);
            return TaskStepResult.error(nullPointerException);
        } else if (!this.c.isSupportedForNetworking(headerField)) {
            return TaskStepResult.success(headerField);
        } else {
            if (!this.d.validateUrl(somaApiContext, headerField)) {
                return TaskStepResult.success(headerField);
            }
            return a(headerField, connectionConfig, somaApiContext, i - 1);
        }
    }
}
