package com.smaato.sdk.core.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class AdContentView extends FrameLayout {
    public AdContentView(@NonNull Context context) {
        super(context);
    }

    public AdContentView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AdContentView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @TargetApi(21)
    public AdContentView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @NonNull
    protected static LayoutParams generateDefaultLayoutParams(int i, int i2) {
        return new LayoutParams(i, i2, 17);
    }
}
