package com.smaato.sdk.core.ui;

import android.view.View;
import android.view.View.OnClickListener;

public abstract class DoubleClickPreventionListener implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private long f3451a;

    /* access modifiers changed from: protected */
    public void processClick() {
    }

    public final void onClick(View view) {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.f3451a >= 1000) {
            this.f3451a = currentTimeMillis;
            processClick();
        }
    }
}
