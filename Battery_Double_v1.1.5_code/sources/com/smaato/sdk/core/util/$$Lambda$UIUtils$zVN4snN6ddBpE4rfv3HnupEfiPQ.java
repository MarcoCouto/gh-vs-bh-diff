package com.smaato.sdk.core.util;

import android.util.DisplayMetrics;
import com.smaato.sdk.core.util.fi.Function;

/* renamed from: com.smaato.sdk.core.util.-$$Lambda$UIUtils$zVN4snN6ddBpE4rfv3HnupEfiPQ reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$UIUtils$zVN4snN6ddBpE4rfv3HnupEfiPQ implements Function {
    public static final /* synthetic */ $$Lambda$UIUtils$zVN4snN6ddBpE4rfv3HnupEfiPQ INSTANCE = new $$Lambda$UIUtils$zVN4snN6ddBpE4rfv3HnupEfiPQ();

    private /* synthetic */ $$Lambda$UIUtils$zVN4snN6ddBpE4rfv3HnupEfiPQ() {
    }

    public final Object apply(Object obj) {
        return Integer.valueOf(((DisplayMetrics) obj).heightPixels);
    }
}
