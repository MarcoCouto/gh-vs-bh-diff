package com.smaato.sdk.core.util;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class Metadata {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Bundle f3455a;

    public static class Builder {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final Bundle f3456a = new Bundle();

        @NonNull
        public Builder putInt(@NonNull String str, int i) {
            this.f3456a.putInt(str, i);
            return this;
        }

        @NonNull
        public Metadata build() {
            return new Metadata(this.f3456a, 0);
        }
    }

    /* synthetic */ Metadata(Bundle bundle, byte b) {
        this(bundle);
    }

    private Metadata(@NonNull Bundle bundle) {
        this.f3455a = (Bundle) Objects.requireNonNull(bundle);
    }

    @Nullable
    public final Integer getInt(@NonNull String str) {
        int i = this.f3455a.getInt(str, -1);
        if (i == -1) {
            return null;
        }
        return Integer.valueOf(i);
    }
}
