package com.smaato.sdk.core.util;

import android.content.Context;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.configcheck.AppManifestConfigChecker;

public class PermissionChecker {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f3461a;
    @NonNull
    private final AppManifestConfigChecker b;

    public PermissionChecker(@NonNull Context context, @NonNull AppManifestConfigChecker appManifestConfigChecker) {
        this.f3461a = (Context) Objects.requireNonNull(context, "Parameter context cannot be null for PermissionChecker::new");
        this.b = (AppManifestConfigChecker) Objects.requireNonNull(appManifestConfigChecker, "Parameter appManifestConfigChecker cannot be null for PermissionChecker::new");
    }

    public boolean checkPermission(@NonNull String str) {
        Objects.requireNonNull(str);
        if (VERSION.SDK_INT < 23) {
            return this.b.isPermissionDeclared(str);
        }
        return this.f3461a.checkSelfPermission(str) == 0;
    }
}
