package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Supplier;

public interface Lazy<T> {

    /* renamed from: com.smaato.sdk.core.util.Lazy$-CC reason: invalid class name */
    public final /* synthetic */ class CC {
        @NonNull
        public static <T> Lazy<T> of(@NonNull Supplier<T> supplier) {
            return new DoubleCheck((Supplier) Objects.requireNonNull(supplier, "'supplier' must not be null"));
        }
    }

    @NonNull
    T get();
}
