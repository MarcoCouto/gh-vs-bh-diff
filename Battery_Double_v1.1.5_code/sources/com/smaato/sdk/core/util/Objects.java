package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.fi.CheckedRunnable;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import java.util.Arrays;

public final class Objects {
    public static boolean isNull(@Nullable Object obj) {
        return obj == null;
    }

    private Objects() {
    }

    public static boolean equals(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static int hash(Object... objArr) {
        if (objArr.length != 1) {
            return Arrays.hashCode(objArr);
        }
        Object obj = objArr[0];
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    public static boolean notEquals(Object obj, Object obj2) {
        return !equals(obj, obj2);
    }

    @NonNull
    public static <T> T requireNonNull(@Nullable T t) {
        return requireNonNull(t, null);
    }

    @NonNull
    public static <T> T requireNonNull(@Nullable T t, @Nullable String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    public static <T> void onNotNull(@Nullable T t, @NonNull Consumer<T> consumer) {
        requireNonNull(consumer);
        if (t != null) {
            consumer.accept(t);
        }
    }

    @Nullable
    public static <T, R> R transformOrNull(@Nullable T t, @NonNull Function<T, R> function) {
        requireNonNull(function);
        if (t != null) {
            return function.apply(t);
        }
        return null;
    }

    public static void doSilently(@Nullable CheckedRunnable checkedRunnable) {
        if (checkedRunnable != null) {
            try {
                checkedRunnable.run();
            } catch (Exception unused) {
            }
        }
    }
}
