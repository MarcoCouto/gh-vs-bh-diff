package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;
import java.util.Map;

public final class HeaderUtils {
    @Nullable
    public final List<String> extractHeaderValueList(@NonNull Map<String, List<String>> map, @NonNull String str) {
        Objects.requireNonNull(map);
        Objects.requireNonNull(str);
        return (List) map.get(str);
    }

    @Nullable
    public final String extractHeaderMultiValue(@NonNull Map<String, List<String>> map, @NonNull String str) {
        Objects.requireNonNull(map);
        Objects.requireNonNull(str);
        List list = (List) map.get(str);
        if (list != null) {
            return Joiner.join((CharSequence) "", (Iterable) list);
        }
        return null;
    }

    @Nullable
    public final String extractHeaderSingleValue(@NonNull Map<String, List<String>> map, @NonNull String str) {
        Objects.requireNonNull(map);
        Objects.requireNonNull(str);
        List list = (List) map.get(str);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (String) list.get(0);
    }

    public final boolean isChunkedTransferEncoding(@NonNull Map<String, List<String>> map) {
        Objects.requireNonNull(map);
        for (String str : map.keySet()) {
            if ("Transfer-Encoding".equalsIgnoreCase(str)) {
                List<String> list = (List) map.get(str);
                if (list != null) {
                    for (String equalsIgnoreCase : list) {
                        if ("chunked".equalsIgnoreCase(equalsIgnoreCase)) {
                            return true;
                        }
                    }
                    continue;
                } else {
                    continue;
                }
            }
        }
        return false;
    }
}
