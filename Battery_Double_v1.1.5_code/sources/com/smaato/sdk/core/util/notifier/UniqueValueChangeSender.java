package com.smaato.sdk.core.util.notifier;

import androidx.annotation.NonNull;

class UniqueValueChangeSender<T> extends StandardChangeSender<T> {
    UniqueValueChangeSender(@NonNull T t) {
        super(t);
    }

    public void newValue(@NonNull T t) {
        if (!t.equals(getValue())) {
            super.newValue(t);
        }
    }
}
