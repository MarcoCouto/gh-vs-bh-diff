package com.smaato.sdk.core.util.notifier;

import androidx.annotation.NonNull;

public interface ChangeSender<T> extends ChangeNotifier<T> {
    @NonNull
    T getValue();

    void newValue(@NonNull T t);
}
