package com.smaato.sdk.core.util.notifier;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.notifier.ChangeNotifier.Listener;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

class StandardChangeSender<T> implements ChangeSender<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    final Object f3473a = new Object();
    @NonNull
    private final Set<Listener<T>> b = Collections.newSetFromMap(new WeakHashMap());
    @NonNull
    private volatile T c;

    StandardChangeSender(@NonNull T t) {
        this.c = t;
    }

    public void newValue(@NonNull T t) {
        Objects.requireNonNull(t);
        synchronized (this.f3473a) {
            this.c = t;
            Iterator it = new HashSet(this.b).iterator();
            while (it.hasNext()) {
                ((Listener) it.next()).onNextValue(this.c);
            }
        }
    }

    public void addListener(@NonNull Listener<T> listener) {
        Objects.requireNonNull(listener, "Parameter listener cannot be null for StandardChangeSender::addListener");
        synchronized (this.f3473a) {
            if (!this.b.contains(listener)) {
                this.b.add(listener);
                listener.onNextValue(this.c);
            }
        }
    }

    public void removeListener(@Nullable Listener<T> listener) {
        synchronized (this.f3473a) {
            this.b.remove(listener);
        }
    }

    @NonNull
    public T getValue() {
        T t;
        synchronized (this.f3473a) {
            t = this.c;
        }
        return t;
    }
}
