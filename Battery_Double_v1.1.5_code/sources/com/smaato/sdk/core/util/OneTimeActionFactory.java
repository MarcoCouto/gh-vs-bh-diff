package com.smaato.sdk.core.util;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.OneTimeAction.Listener;

public class OneTimeActionFactory {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Handler f3459a;

    public OneTimeActionFactory(@NonNull Handler handler) {
        this.f3459a = (Handler) Objects.requireNonNull(handler);
    }

    @NonNull
    public OneTimeAction createOneTimeAction(@NonNull Listener listener) {
        return new OneTimeAction(this.f3459a, listener);
    }
}
