package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Supplier;

class DoubleCheck<T> implements Lazy<T> {

    /* renamed from: a reason: collision with root package name */
    private volatile Object f3453a;
    private volatile Supplier<T> b;

    DoubleCheck(@NonNull Supplier<T> supplier) {
        this.b = supplier;
    }

    @NonNull
    public T get() {
        T t = this.f3453a;
        if (t == null) {
            synchronized (this) {
                t = this.f3453a;
                if (t == null) {
                    t = this.b.get();
                    this.f3453a = t;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
