package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import java.lang.Enum;

public interface SdkComponentException<ErrorType extends Enum<ErrorType>> {
    @NonNull
    ErrorType getErrorType();

    @NonNull
    Exception getReason();
}
