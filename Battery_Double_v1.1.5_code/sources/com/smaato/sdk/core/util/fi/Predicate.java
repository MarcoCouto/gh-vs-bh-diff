package com.smaato.sdk.core.util.fi;

import androidx.annotation.NonNull;

@FunctionalInterface
public interface Predicate<T> {
    boolean test(@NonNull T t);
}
