package com.smaato.sdk.core.util.fi;

import androidx.annotation.NonNull;

public final class FunctionUtils {

    /* renamed from: a reason: collision with root package name */
    private static final Runnable f3470a = $$Lambda$FunctionUtils$08aOZhbkE2dvDG7_VAoruaXaVa4.INSTANCE;
    private static final Consumer<?> b = $$Lambda$FunctionUtils$HcY7M3AGE9L2qSw5E7LxMNkDHXA.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ Object a(Object obj) {
        return obj;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a() {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(Object obj) {
    }

    private FunctionUtils() {
    }

    @NonNull
    public static <T> Function<T, T> identity() {
        return $$Lambda$FunctionUtils$FXLeQT8CSZG_xEsmqohyemGVqpM.INSTANCE;
    }

    @NonNull
    public static <T> Consumer<T> emptyConsumer() {
        return b;
    }

    @NonNull
    public static Runnable emptyAction() {
        return f3470a;
    }
}
