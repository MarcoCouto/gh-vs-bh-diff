package com.smaato.sdk.core.util.fi;

@FunctionalInterface
public interface Consumer<T> {
    void accept(T t);
}
