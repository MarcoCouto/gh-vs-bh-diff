package com.smaato.sdk.core.util.fi;

import androidx.annotation.NonNull;

@FunctionalInterface
public interface Supplier<T> {
    @NonNull
    T get();
}
