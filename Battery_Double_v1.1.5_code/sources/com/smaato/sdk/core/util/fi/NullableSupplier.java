package com.smaato.sdk.core.util.fi;

import androidx.annotation.Nullable;

@FunctionalInterface
public interface NullableSupplier<T> {
    @Nullable
    T get();
}
