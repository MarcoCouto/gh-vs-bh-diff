package com.smaato.sdk.core.util.fi;

@FunctionalInterface
public interface CheckedRunnable {
    void run() throws Exception;
}
