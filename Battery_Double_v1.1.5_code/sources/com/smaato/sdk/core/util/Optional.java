package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.core.util.fi.Supplier;
import java.util.NoSuchElementException;

public final class Optional<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private static final Optional<?> f3460a = new Optional<>();
    @Nullable
    private final T b;

    private Optional() {
        this.b = null;
    }

    private Optional(@NonNull T t) {
        this.b = Objects.requireNonNull(t);
    }

    public static <T> Optional<T> empty() {
        return f3460a;
    }

    @NonNull
    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    @NonNull
    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    @NonNull
    public final T get() {
        if (this.b != null) {
            return this.b;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.b != null;
    }

    public final boolean isEmpty() {
        return this.b == null;
    }

    public final void ifPresent(@NonNull Consumer<? super T> consumer) {
        if (this.b != null) {
            consumer.accept(this.b);
        }
    }

    @NonNull
    public final Optional<T> filter(@NonNull Predicate<? super T> predicate) {
        Objects.requireNonNull(predicate);
        return (!isEmpty() && !predicate.test(this.b)) ? empty() : this;
    }

    @NonNull
    public final <U> Optional<U> map(@NonNull Function<? super T, ? extends U> function) {
        Objects.requireNonNull(function);
        if (!isPresent()) {
            return empty();
        }
        return ofNullable(function.apply(this.b));
    }

    @NonNull
    public final <U> Optional<U> flatMap(@NonNull Function<? super T, Optional<U>> function) {
        Objects.requireNonNull(function);
        if (!isPresent()) {
            return empty();
        }
        return (Optional) Objects.requireNonNull(function.apply(this.b));
    }

    @NonNull
    public final T orElse(@NonNull T t) {
        return this.b != null ? this.b : t;
    }

    @NonNull
    public final T orElseGet(@NonNull Supplier<? extends T> supplier) {
        return this.b != null ? this.b : supplier.get();
    }

    @NonNull
    public final <X extends Throwable> T orElseThrow(@NonNull Supplier<? extends X> supplier) throws Throwable {
        if (this.b != null) {
            return this.b;
        }
        throw ((Throwable) supplier.get());
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        return Objects.equals(this.b, ((Optional) obj).b);
    }

    public final int hashCode() {
        return Objects.hash(this.b);
    }

    public final String toString() {
        if (this.b == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", new Object[]{this.b});
    }
}
