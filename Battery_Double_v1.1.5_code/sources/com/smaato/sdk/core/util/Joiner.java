package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import java.util.Iterator;

public final class Joiner {
    private Joiner() {
    }

    @NonNull
    public static String join(@NonNull CharSequence charSequence, @NonNull Iterable iterable) {
        Objects.requireNonNull(charSequence);
        Objects.requireNonNull(iterable);
        StringBuilder sb = new StringBuilder();
        Iterator it = iterable.iterator();
        if (it.hasNext()) {
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(charSequence);
                sb.append(it.next());
            }
        }
        return sb.toString();
    }

    @NonNull
    public static String join(@NonNull CharSequence charSequence, @NonNull Object... objArr) {
        Objects.requireNonNull(charSequence);
        Objects.requireNonNull(objArr);
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Object obj : objArr) {
            if (obj != null) {
                if (z) {
                    z = false;
                } else {
                    sb.append(charSequence);
                }
                sb.append(obj);
            }
        }
        return sb.toString();
    }
}
