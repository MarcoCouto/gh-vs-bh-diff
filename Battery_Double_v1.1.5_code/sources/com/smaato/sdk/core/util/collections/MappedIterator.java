package com.smaato.sdk.core.util.collections;

import com.smaato.sdk.core.util.Objects;
import java.util.Iterator;

abstract class MappedIterator<F, T> implements Iterator<T> {

    /* renamed from: a reason: collision with root package name */
    final Iterator<? extends F> f3469a;

    /* access modifiers changed from: 0000 */
    public abstract T a(F f);

    MappedIterator(Iterator<? extends F> it) {
        this.f3469a = (Iterator) Objects.requireNonNull(it);
    }

    public final boolean hasNext() {
        return this.f3469a.hasNext();
    }

    public final T next() {
        return a(this.f3469a.next());
    }

    public final void remove() {
        this.f3469a.remove();
    }
}
