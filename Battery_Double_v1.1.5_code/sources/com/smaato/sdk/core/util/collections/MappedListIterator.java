package com.smaato.sdk.core.util.collections;

import java.util.ListIterator;

abstract class MappedListIterator<F, T> extends MappedIterator<F, T> implements ListIterator<T> {
    MappedListIterator(ListIterator<? extends F> listIterator) {
        super(listIterator);
    }

    public void set(T t) {
        throw new UnsupportedOperationException();
    }

    public void add(T t) {
        throw new UnsupportedOperationException();
    }

    public final boolean hasPrevious() {
        return Iterators.a(this.f3469a).hasPrevious();
    }

    public final T previous() {
        return a(Iterators.a(this.f3469a).previous());
    }

    public final int nextIndex() {
        return Iterators.a(this.f3469a).nextIndex();
    }

    public final int previousIndex() {
        return Iterators.a(this.f3469a).previousIndex();
    }
}
