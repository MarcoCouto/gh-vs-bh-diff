package com.smaato.sdk.core.util.collections;

import java.util.Iterator;
import java.util.ListIterator;

final class Iterators {
    private Iterators() {
    }

    static <T> ListIterator<T> a(Iterator<T> it) {
        return (ListIterator) it;
    }
}
