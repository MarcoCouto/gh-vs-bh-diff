package com.smaato.sdk.core.util.collections;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Function;
import java.util.Iterator;

public final class Iterables {
    private Iterables() {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Iterator a(Iterable iterable, final Function function) {
        return new MappedIterator<F, T>(iterable.iterator()) {
            /* access modifiers changed from: 0000 */
            public final T a(F f) {
                return function.apply(f);
            }
        };
    }

    @NonNull
    public static <F, T> Iterable<T> map(Iterable<F> iterable, Function<F, T> function) {
        return new Iterable(iterable, function) {
            private final /* synthetic */ Iterable f$0;
            private final /* synthetic */ Function f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final Iterator iterator() {
                return Iterables.a(this.f$0, this.f$1);
            }
        };
    }
}
