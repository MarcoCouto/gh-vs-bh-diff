package com.smaato.sdk.core.util.collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Predicate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class Sets {
    private Sets() {
    }

    public static <T> Set<T> retainToSet(Collection<T> collection, Predicate<T> predicate) {
        HashSet hashSet = new HashSet();
        for (Object next : collection) {
            if (predicate.test(next)) {
                hashSet.add(next);
            }
        }
        return hashSet;
    }

    @NonNull
    public static <E> Set<E> toSet(@NonNull Iterable<E> iterable) {
        HashSet hashSet = new HashSet();
        for (E add : iterable) {
            hashSet.add(add);
        }
        return hashSet;
    }

    @NonNull
    public static <E, R> Set<R> toSet(@NonNull Iterable<E> iterable, @NonNull Function<E, R> function) {
        HashSet hashSet = new HashSet();
        for (Object next : iterable) {
            if (next != null) {
                hashSet.add(function.apply(next));
            }
        }
        return hashSet;
    }

    @NonNull
    public static <E> Set<E> toImmutableSet(@Nullable Collection<E> collection) {
        if (collection == null) {
            return Collections.emptySet();
        }
        return Collections.unmodifiableSet(new HashSet(collection));
    }

    @NonNull
    public static <E> Set<E> of(@NonNull E... eArr) {
        return new HashSet(Arrays.asList(eArr));
    }
}
