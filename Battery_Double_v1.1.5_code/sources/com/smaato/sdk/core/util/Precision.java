package com.smaato.sdk.core.util;

public final class Precision {
    public static final double EPSILON = Double.longBitsToDouble(4368491638549381120L);
    public static final double SAFE_MIN = Double.longBitsToDouble(4503599627370496L);

    /* renamed from: a reason: collision with root package name */
    private static final int f3462a = Float.floatToRawIntBits(0.0f);
    private static final int b = Float.floatToRawIntBits(-0.0f);

    private Precision() {
    }

    public static boolean equals(float f, float f2) {
        return equals(f, f2, 1);
    }

    public static boolean equalsIncludingNaN(float f, float f2) {
        if (f == f && f2 == f2) {
            return equals(f, f2, 1);
        }
        return !(((f > f ? 1 : (f == f ? 0 : -1)) != 0) ^ ((f2 > f2 ? 1 : (f2 == f2 ? 0 : -1)) != 0));
    }

    public static boolean equals(float f, float f2, float f3) {
        return equals(f, f2, 1) || Math.abs(f2 - f) <= f3;
    }

    public static boolean equalsIncludingNaN(float f, float f2, float f3) {
        return equalsIncludingNaN(f, f2) || Math.abs(f2 - f) <= f3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        if (r1 <= (r8 - r0)) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0016, code lost:
        if (java.lang.Math.abs(r0 - r1) <= r8) goto L_0x0018;
     */
    public static boolean equals(float f, float f2, int i) {
        boolean z;
        int i2;
        int i3;
        int floatToRawIntBits = Float.floatToRawIntBits(f);
        int floatToRawIntBits2 = Float.floatToRawIntBits(f2);
        if (((floatToRawIntBits ^ floatToRawIntBits2) & Integer.MIN_VALUE) != 0) {
            if (floatToRawIntBits < floatToRawIntBits2) {
                int i4 = floatToRawIntBits2 - f3462a;
                i2 = floatToRawIntBits - b;
                i3 = i4;
            } else {
                i3 = floatToRawIntBits - f3462a;
                i2 = floatToRawIntBits2 - b;
            }
            if (i3 <= i) {
            }
            z = false;
            return !z && !Float.isNaN(f) && !Float.isNaN(f2);
        }
        z = true;
        if (!z) {
        }
    }

    public static boolean equalsIncludingNaN(float f, float f2, int i) {
        if (f == f && f2 == f2) {
            return equals(f, f2, i);
        }
        return !(((f > f ? 1 : (f == f ? 0 : -1)) != 0) ^ ((f2 > f2 ? 1 : (f2 == f2 ? 0 : -1)) != 0));
    }
}
