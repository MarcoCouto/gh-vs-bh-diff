package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;

public final class Size {
    public final int height;
    public final int width;

    public Size(int i, int i2) {
        this.width = i;
        this.height = i2;
    }

    @NonNull
    private static NumberFormatException a(@NonNull String str) {
        StringBuilder sb = new StringBuilder("Invalid Size: \"");
        sb.append(str);
        sb.append("\"");
        throw new NumberFormatException(sb.toString());
    }

    @NonNull
    public static Size parseSize(@NonNull String str) throws NumberFormatException {
        Objects.requireNonNull(str, "string must not be null");
        int indexOf = str.indexOf(42);
        if (indexOf < 0) {
            indexOf = str.indexOf(120);
        }
        if (indexOf >= 0) {
            try {
                return new Size(Integer.parseInt(str.substring(0, indexOf)), Integer.parseInt(str.substring(indexOf + 1)));
            } catch (NumberFormatException unused) {
                throw a(str);
            }
        } else {
            throw a(str);
        }
    }

    public final boolean equals(@Nullable Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Size)) {
            return false;
        }
        Size size = (Size) obj;
        return this.width == size.width && this.height == size.height;
    }

    @NonNull
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.width);
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(this.height);
        return sb.toString();
    }

    public final int hashCode() {
        return this.height ^ ((this.width << 16) | (this.width >>> 16));
    }
}
