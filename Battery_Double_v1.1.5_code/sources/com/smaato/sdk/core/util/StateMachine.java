package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.lang.Enum;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public final class StateMachine<Event extends Enum<Event>, State extends Enum<State>> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Map<Pair<Event, State>, List<State>> f3464a;
    @NonNull
    private State b;
    @NonNull
    private final LinkedHashSet<Listener<State>> c;
    private boolean d;

    public static class Builder<Event extends Enum<Event>, State extends Enum<State>> {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private State f3465a;
        @NonNull
        private final Map<Pair<Event, State>, List<State>> b = new HashMap();

        public Builder<Event, State> setInitialState(@NonNull State state) {
            this.f3465a = (Enum) Objects.requireNonNull(state, "initialState can not be null");
            return this;
        }

        @NonNull
        public Builder<Event, State> addTransition(@NonNull Event event, @NonNull List<State> list) {
            Objects.requireNonNull(event, "event can not be null");
            ArrayList<Enum> arrayList = new ArrayList<>((Collection) Objects.requireNonNull(list, "statePath can not be null"));
            if (arrayList.size() >= 2) {
                for (Enum requireNonNull : arrayList) {
                    Objects.requireNonNull(requireNonNull, "a state can not be null");
                }
                if (arrayList.size() <= EnumSet.copyOf(arrayList).size()) {
                    Enum enumR = (Enum) arrayList.remove(0);
                    Pair of = Pair.of(event, enumR);
                    if (!this.b.containsKey(of)) {
                        this.b.put(of, arrayList);
                        return this;
                    }
                    StringBuilder sb = new StringBuilder("a statePath with the same start state ");
                    sb.append(enumR);
                    sb.append(" is already defined for the event ");
                    sb.append(event);
                    throw new IllegalArgumentException(sb.toString());
                }
                throw new IllegalArgumentException("a statePath must consist of unique states");
            }
            throw new IllegalArgumentException("statePath must have at least 2 states");
        }

        @NonNull
        public Builder<Event, State> addLoopTransition(@NonNull Event event, @NonNull State state) {
            Objects.requireNonNull(event, "event cannot be null");
            Objects.requireNonNull(state, "state cannot be null");
            Pair of = Pair.of(event, state);
            if (!this.b.containsKey(of)) {
                this.b.put(of, Collections.singletonList(state));
                return this;
            }
            StringBuilder sb = new StringBuilder("a statePath with the same start state ");
            sb.append(state);
            sb.append(" is already defined for the event ");
            sb.append(event);
            throw new IllegalArgumentException(sb.toString());
        }

        @NonNull
        public StateMachine<Event, State> build() {
            if (this.f3465a == null) {
                throw new IllegalStateException("initialState must be set");
            } else if (!this.b.isEmpty()) {
                return new StateMachine<>(this.b, this.f3465a, 0);
            } else {
                throw new IllegalStateException("at least one transition must be added");
            }
        }
    }

    public interface Listener<State> {
        void onStateChanged(@NonNull State state, @NonNull State state2, @Nullable Metadata metadata);
    }

    /* synthetic */ StateMachine(Map map, Enum enumR, byte b2) {
        this(map, enumR);
    }

    private StateMachine(@NonNull Map<Pair<Event, State>, List<State>> map, @NonNull State state) {
        this.c = new LinkedHashSet<>();
        this.f3464a = map;
        this.b = state;
    }

    public final void onEvent(Event event) {
        onEvent(event, null);
    }

    public final synchronized void onEvent(@NonNull Event event, @Nullable Metadata metadata) {
        List list = (List) this.f3464a.get(Pair.of(event, this.b));
        if (list != null) {
            if (!this.d) {
                int size = list.size();
                int i = 0;
                while (i < size) {
                    this.d = i < size + -1;
                    State state = this.b;
                    State state2 = (Enum) list.get(i);
                    this.b = state2;
                    Iterator it = new ArrayList(this.c).iterator();
                    while (it.hasNext()) {
                        ((Listener) it.next()).onStateChanged(state, state2, metadata);
                    }
                    i++;
                }
                return;
            }
            throw new IllegalStateException("can not start a new transition, because there is an on-going unfinished transition");
        }
    }

    public final synchronized boolean isTransitionAllowed(@NonNull Event event) {
        return this.f3464a.get(Pair.of(event, this.b)) != null;
    }

    @NonNull
    public final synchronized State getCurrentState() {
        return this.b;
    }

    public final synchronized void addListener(@NonNull Listener<State> listener) {
        Objects.requireNonNull(listener, "listener can not be null");
        this.c.add(listener);
    }

    public final synchronized void deleteListeners() {
        this.c.clear();
    }

    public final synchronized void deleteListener(@Nullable Listener<State> listener) {
        this.c.remove(listener);
    }
}
