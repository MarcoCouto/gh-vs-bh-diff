package com.smaato.sdk.core.util;

import android.net.UrlQuerySanitizer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.collections.Maps;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public final class TextUtils {
    private TextUtils() {
    }

    public static boolean isEmpty(@Nullable CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }

    @NonNull
    public static Map<String, String> parseQuery(@Nullable String str) {
        if (isEmpty(str)) {
            return Collections.emptyMap();
        }
        UrlQuerySanitizer urlQuerySanitizer = new UrlQuerySanitizer();
        urlQuerySanitizer.setAllowUnregisteredParamaters(true);
        urlQuerySanitizer.parseQuery(str);
        return Maps.toMap(urlQuerySanitizer.getParameterList(), $$Lambda$TextUtils$DAQWq3fGGCKwRI_u621E2_NMQ4w.INSTANCE, $$Lambda$TextUtils$FGXFSSJYzJXS5P7LuuigMT2_N5I.INSTANCE);
    }

    @NonNull
    public static TreeMap<String, String> parseQueryToCaseInsensitiveMap(@Nullable String str) {
        TreeMap<String, String> treeMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        treeMap.putAll(parseQuery(str));
        return treeMap;
    }
}
