package com.smaato.sdk.core.util;

import android.util.DisplayMetrics;
import com.smaato.sdk.core.util.fi.Function;

/* renamed from: com.smaato.sdk.core.util.-$$Lambda$UIUtils$IucPmzO3gW6L2VLwwVaod8fCFQM reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$UIUtils$IucPmzO3gW6L2VLwwVaod8fCFQM implements Function {
    public static final /* synthetic */ $$Lambda$UIUtils$IucPmzO3gW6L2VLwwVaod8fCFQM INSTANCE = new $$Lambda$UIUtils$IucPmzO3gW6L2VLwwVaod8fCFQM();

    private /* synthetic */ $$Lambda$UIUtils$IucPmzO3gW6L2VLwwVaod8fCFQM() {
    }

    public final Object apply(Object obj) {
        return Integer.valueOf(((DisplayMetrics) obj).widthPixels);
    }
}
