package com.smaato.sdk.core.util.memory;

import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.util.fi.Consumer;

/* renamed from: com.smaato.sdk.core.util.memory.-$$Lambda$DiLeakProtection$_Z_nIcUvxM5b_aRgOPbVFIsCbjI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiLeakProtection$_Z_nIcUvxM5b_aRgOPbVFIsCbjI implements Consumer {
    public static final /* synthetic */ $$Lambda$DiLeakProtection$_Z_nIcUvxM5b_aRgOPbVFIsCbjI INSTANCE = new $$Lambda$DiLeakProtection$_Z_nIcUvxM5b_aRgOPbVFIsCbjI();

    private /* synthetic */ $$Lambda$DiLeakProtection$_Z_nIcUvxM5b_aRgOPbVFIsCbjI() {
    }

    public final void accept(Object obj) {
        ((DiRegistry) obj).registerSingletonFactory(LeakProtection.class, $$Lambda$DiLeakProtection$a0MDLqJTS2QO8k4oSjhV1sITA.INSTANCE);
    }
}
