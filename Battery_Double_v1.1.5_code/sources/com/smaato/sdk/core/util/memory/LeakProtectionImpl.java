package com.smaato.sdk.core.util.memory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.HashMap;
import java.util.Map;

final class LeakProtectionImpl implements LeakProtection {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ReferenceQueue<Object> f3471a = new ReferenceQueue<>();
    @NonNull
    private final Map<LeakProtectionReference, Runnable> b = new HashMap();

    private static final class LeakProtectionReference<T> extends PhantomReference<T> {

        /* renamed from: a reason: collision with root package name */
        private boolean f3472a;

        /* synthetic */ LeakProtectionReference(Object obj, ReferenceQueue referenceQueue, byte b) {
            this(obj, referenceQueue);
        }

        private LeakProtectionReference(@NonNull T t, @NonNull ReferenceQueue<? super T> referenceQueue) {
            super(t, referenceQueue);
        }

        static /* synthetic */ boolean a(LeakProtectionReference leakProtectionReference) {
            if (leakProtectionReference.f3472a) {
                return false;
            }
            leakProtectionReference.f3472a = true;
            return true;
        }
    }

    LeakProtectionImpl() {
        new Thread(new Runnable() {
            public final void run() {
                LeakProtectionImpl.this.a();
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException unused) {
            }
            while (true) {
                LeakProtectionReference leakProtectionReference = (LeakProtectionReference) this.f3471a.poll();
                if (leakProtectionReference != null) {
                    if (LeakProtectionReference.a(leakProtectionReference)) {
                        Runnable a2 = a(leakProtectionReference);
                        if (a2 != null) {
                            a2.run();
                        }
                        leakProtectionReference.clear();
                    }
                }
            }
        }
    }

    public final synchronized void listenToObject(@NonNull Object obj, @NonNull Runnable runnable) {
        this.b.put(new LeakProtectionReference(obj, this.f3471a, 0), runnable);
    }

    @Nullable
    private synchronized Runnable a(@NonNull LeakProtectionReference leakProtectionReference) {
        return (Runnable) this.b.remove(leakProtectionReference);
    }
}
