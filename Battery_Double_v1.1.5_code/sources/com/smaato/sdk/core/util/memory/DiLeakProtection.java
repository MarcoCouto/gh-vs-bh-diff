package com.smaato.sdk.core.util.memory;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;

public final class DiLeakProtection {
    private DiLeakProtection() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiLeakProtection$_Z_nIcUvxM5b_aRgOPbVFIsCbjI.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ LeakProtection a(DiConstructor diConstructor) {
        return new LeakProtectionImpl();
    }
}
