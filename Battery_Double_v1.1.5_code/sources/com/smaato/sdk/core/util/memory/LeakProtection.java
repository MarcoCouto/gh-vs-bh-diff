package com.smaato.sdk.core.util.memory;

import androidx.annotation.NonNull;

public interface LeakProtection {
    void listenToObject(@NonNull Object obj, @NonNull Runnable runnable);
}
