package com.smaato.sdk.core.datacollector;

import androidx.annotation.Nullable;

public final class GoogleAdvertisingClientInfo {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f3362a;
    private final boolean b;

    public GoogleAdvertisingClientInfo(@Nullable String str, boolean z) {
        this.f3362a = str;
        this.b = z;
    }

    @Nullable
    public final String getAdvertisingId() {
        return this.f3362a;
    }

    public final boolean isLimitAdTrackingEnabled() {
        return this.b;
    }
}
