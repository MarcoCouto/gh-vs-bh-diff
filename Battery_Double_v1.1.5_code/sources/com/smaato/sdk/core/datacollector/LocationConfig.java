package com.smaato.sdk.core.datacollector;

import com.google.android.exoplayer2.DefaultRenderersFactory;

class LocationConfig {

    /* renamed from: a reason: collision with root package name */
    private final long f3363a = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
    private final float b = 0.0f;
    private final long c = Long.MAX_VALUE;

    LocationConfig(long j, float f, long j2) {
    }

    /* access modifiers changed from: 0000 */
    public final long a() {
        return this.f3363a;
    }

    /* access modifiers changed from: 0000 */
    public final float b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final long c() {
        return this.c;
    }
}
