package com.smaato.sdk.core.datacollector;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.core.datacollector.-$$Lambda$DiDataCollectorLayer$8Ph1BA2VXkSTuR7BCpmQAAspjrY reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiDataCollectorLayer$8Ph1BA2VXkSTuR7BCpmQAAspjrY implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiDataCollectorLayer$8Ph1BA2VXkSTuR7BCpmQAAspjrY INSTANCE = new $$Lambda$DiDataCollectorLayer$8Ph1BA2VXkSTuR7BCpmQAAspjrY();

    private /* synthetic */ $$Lambda$DiDataCollectorLayer$8Ph1BA2VXkSTuR7BCpmQAAspjrY() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newSingleThreadExecutor();
    }
}
