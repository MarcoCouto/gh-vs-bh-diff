package com.smaato.sdk.core.datacollector;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import androidx.annotation.NonNull;
import com.facebook.places.model.PlaceFields;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.gdpr.SomaGdprDataSource;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.PermissionChecker;
import java.util.concurrent.ExecutorService;

public final class DiDataCollectorLayer {
    private DiDataCollectorLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiDataCollectorLayer$EXdvsUFX2FARtBQVUObPtYZwgLA.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory("DATA_COLLECTION_EXECUTOR_SERVICE", ExecutorService.class, $$Lambda$DiDataCollectorLayer$8Ph1BA2VXkSTuR7BCpmQAAspjrY.INSTANCE);
        diRegistry.registerSingletonFactory(DataCollector.class, $$Lambda$DiDataCollectorLayer$u0uHoYw_aCudDmAdCnvPLQKv60.INSTANCE);
        diRegistry.registerSingletonFactory(TelephonyManager.class, $$Lambda$DiDataCollectorLayer$VWYvWrOv638MMP0b64kMAC9lbX8.INSTANCE);
        diRegistry.registerSingletonFactory(ContentResolver.class, $$Lambda$DiDataCollectorLayer$fwjqaSztf83QecPVYs_oxbsWmds.INSTANCE);
        diRegistry.registerSingletonFactory(SystemInfoProvider.class, $$Lambda$DiDataCollectorLayer$bzWZLxpCJoes0NnT7u3iaQKlljo.INSTANCE);
        diRegistry.registerSingletonFactory(LocationProvider.class, $$Lambda$DiDataCollectorLayer$DCcrHMFBZVOEHmLGE3Z0ExNiRqw.INSTANCE);
        diRegistry.registerFactory(LocationManager.class, $$Lambda$DiDataCollectorLayer$5GD7ERz0h5szn33QpRgxgJbaTks.INSTANCE);
        diRegistry.registerFactory(LocationConfig.class, $$Lambda$DiDataCollectorLayer$818dvbRcJJ_WdZ4zxRWr9SA_GcE.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ DataCollector g(DiConstructor diConstructor) {
        return new DataCollector((SystemInfoProvider) diConstructor.get(SystemInfoProvider.class), (LocationProvider) diConstructor.get(LocationProvider.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ TelephonyManager f(DiConstructor diConstructor) {
        return (TelephonyManager) Objects.requireNonNull((TelephonyManager) ((Application) diConstructor.get(Application.class)).getSystemService(PlaceFields.PHONE));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ContentResolver e(DiConstructor diConstructor) {
        return (ContentResolver) Objects.requireNonNull(((Application) diConstructor.get(Application.class)).getContentResolver());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SystemInfoProvider d(DiConstructor diConstructor) {
        SystemInfoProvider systemInfoProvider = new SystemInfoProvider((Logger) diConstructor.get(Logger.class), (Context) diConstructor.get(Application.class), DiNetworkLayer.getNetworkStateMonitorFrom(diConstructor), (TelephonyManager) diConstructor.get(TelephonyManager.class), (ExecutorService) diConstructor.get("DATA_COLLECTION_EXECUTOR_SERVICE", ExecutorService.class));
        return systemInfoProvider;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ LocationProvider c(DiConstructor diConstructor) {
        LocationProvider locationProvider = new LocationProvider(DiLogLayer.getLoggerFrom(diConstructor), (LocationManager) diConstructor.get(LocationManager.class), (LocationConfig) diConstructor.get(LocationConfig.class), (PermissionChecker) diConstructor.get(PermissionChecker.class), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), (SomaGdprDataSource) diConstructor.get(SomaGdprDataSource.class));
        return locationProvider;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ LocationManager b(DiConstructor diConstructor) {
        return (LocationManager) Objects.requireNonNull(((Application) diConstructor.get(Application.class)).getSystemService("location"));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ LocationConfig a(DiConstructor diConstructor) {
        LocationConfig locationConfig = new LocationConfig(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, 0.0f, Long.MAX_VALUE);
        return locationConfig;
    }
}
