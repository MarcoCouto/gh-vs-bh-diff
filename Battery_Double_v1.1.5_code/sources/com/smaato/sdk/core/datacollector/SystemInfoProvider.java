package com.smaato.sdk.core.datacollector;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkConnectionType;
import com.smaato.sdk.core.network.NetworkStateMonitor;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Pair;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.NullableSupplier;
import com.smaato.sdk.core.util.reflection.MethodAccessor.Builder;
import com.smaato.sdk.core.util.reflection.MethodAccessor.MethodAccessingException;
import com.smaato.sdk.core.util.reflection.Reflections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class SystemInfoProvider {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3368a;
    @NonNull
    private final Context b;
    @NonNull
    private final NetworkStateMonitor c;
    @NonNull
    private final TelephonyManager d;
    @NonNull
    private final ExecutorService e;
    @Nullable
    private GoogleAdvertisingClientInfo f;
    @Nullable
    private Future g;

    SystemInfoProvider(@NonNull Logger logger, @NonNull Context context, @NonNull NetworkStateMonitor networkStateMonitor, @NonNull TelephonyManager telephonyManager, @NonNull ExecutorService executorService) {
        this.f3368a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for SystemInfoProvider::new");
        this.b = (Context) Objects.requireNonNull(context, "Parameter context cannot be null for SystemInfoProvider::new");
        this.c = (NetworkStateMonitor) Objects.requireNonNull(networkStateMonitor, "Parameter networkStateMonitor cannot be null for SystemInfoProvider::new");
        this.d = (TelephonyManager) Objects.requireNonNull(telephonyManager, "Parameter telephonyManager cannot be null for SystemInfoProvider::new");
        this.e = (ExecutorService) Objects.requireNonNull(executorService, "Parameter executorService cannot be null for SystemInfoProvider::new");
    }

    @NonNull
    public SystemInfo getSystemInfoSnapshot() {
        String simOperatorName = this.d.getSimOperatorName();
        String simOperator = this.d.getSimOperator();
        GoogleAdvertisingClientInfo googleAdvertisingClientInfo = this.f;
        String str = (String) Objects.transformOrNull(googleAdvertisingClientInfo, $$Lambda$GO_lA4WitxSmPAnr6G0BjX_TVKI.INSTANCE);
        Boolean bool = (Boolean) Objects.transformOrNull(googleAdvertisingClientInfo, $$Lambda$gVylY46Wvwp1OInI80o4WgpqcIQ.INSTANCE);
        String str2 = Build.MODEL;
        NetworkConnectionType networkConnectionType = this.c.getNetworkConnectionType();
        String packageName = this.b.getPackageName();
        String str3 = (String) Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier(this.b) {
            private final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            public final Object get() {
                return SystemInfoProvider.a(this.f$0);
            }
        });
        if (str3 == null) {
            str3 = "";
        }
        SystemInfo systemInfo = new SystemInfo(simOperatorName, simOperator, str, bool, str2, networkConnectionType, packageName, str3);
        if (Reflections.isClassInClasspath("com.google.android.gms.ads.identifier.AdvertisingIdClient")) {
            synchronized (this) {
                if (this.g == null) {
                    this.g = this.e.submit(new Runnable() {
                        public final void run() {
                            SystemInfoProvider.this.b();
                        }
                    });
                }
            }
        }
        return systemInfo;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        this.f = a();
        synchronized (this) {
            this.g = null;
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ String a(Context context) {
        WebView webView = new WebView(context);
        String userAgentString = webView.getSettings().getUserAgentString();
        webView.destroy();
        return userAgentString;
    }

    @Nullable
    private GoogleAdvertisingClientInfo a() {
        try {
            Object execute = new Builder().fromClassInstance("com.google.android.gms.ads.identifier.AdvertisingIdClient").setMethodName("getAdvertisingIdInfo").withParametersOfResolvedTypes(Pair.of(Context.class, this.b)).build().execute();
            if (execute != null) {
                return new GoogleAdvertisingClientInfo((String) new Builder().fromObjectInstance(execute).setMethodName("getId").build().execute(), ((Boolean) new Builder().fromObjectInstance(execute).setMethodName(RequestParameters.isLAT).build().execute()).booleanValue());
            }
            this.f3368a.error(LogDomain.DATA_COLLECTOR, "Cannot fetch AdvertisingIdClient.Info: null received", new Object[0]);
            return null;
        } catch (MethodAccessingException | ClassNotFoundException e2) {
            this.f3368a.error(LogDomain.DATA_COLLECTOR, e2, "Cannot fetch AdvertisingIdClient.Info", new Object[0]);
            return null;
        }
    }
}
