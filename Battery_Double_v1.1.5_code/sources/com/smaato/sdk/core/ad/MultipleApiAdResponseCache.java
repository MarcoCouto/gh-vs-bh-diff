package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.config.ConfigurationRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class MultipleApiAdResponseCache implements ApiAdResponseCache {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ConfigurationRepository f3316a;
    @NonNull
    private final Map<String, Queue<AdResponseCacheEntry>> b = new ConcurrentHashMap();

    public MultipleApiAdResponseCache(@NonNull ConfigurationRepository configurationRepository) {
        this.f3316a = configurationRepository;
    }

    @Nullable
    public final AdResponseCacheEntry get(@NonNull String str) {
        AdResponseCacheEntry adResponseCacheEntry = (AdResponseCacheEntry) a(str).poll();
        if (adResponseCacheEntry == null) {
            return null;
        }
        return adResponseCacheEntry;
    }

    public final boolean put(@NonNull String str, @NonNull AdResponseCacheEntry adResponseCacheEntry) {
        return a(str).offer(adResponseCacheEntry);
    }

    public final void remove(@NonNull String str, @NonNull AdResponseCacheEntry adResponseCacheEntry) {
        Queue queue = (Queue) this.b.get(str);
        if (queue != null) {
            queue.remove(adResponseCacheEntry);
        }
    }

    @NonNull
    public final Collection<AdResponseCacheEntry> trim(@NonNull String str) {
        Queue<AdResponseCacheEntry> a2 = a(str);
        ArrayList arrayList = new ArrayList();
        for (AdResponseCacheEntry adResponseCacheEntry : a2) {
            if (adResponseCacheEntry.apiAdResponse.getExpirationTimestamp().isExpired()) {
                arrayList.add(adResponseCacheEntry);
            }
        }
        a2.removeAll(arrayList);
        return arrayList;
    }

    public final int remainingCapacity(@NonNull String str) {
        return this.f3316a.get().cachingCapacity - a(str).size();
    }

    public final int perKeyCapacity() {
        return this.f3316a.get().cachingCapacity;
    }

    @NonNull
    private Queue<AdResponseCacheEntry> a(@NonNull String str) {
        Queue<AdResponseCacheEntry> queue = (Queue) this.b.get(str);
        if (queue != null) {
            return queue;
        }
        ConcurrentLinkedQueue concurrentLinkedQueue = new ConcurrentLinkedQueue();
        this.b.put(str, concurrentLinkedQueue);
        return concurrentLinkedQueue;
    }
}
