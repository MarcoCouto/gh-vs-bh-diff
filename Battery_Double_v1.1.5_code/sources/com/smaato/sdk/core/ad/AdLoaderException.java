package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkComponentException;

public final class AdLoaderException extends Exception implements SdkComponentException<Error> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Error f3298a;
    @NonNull
    private final Exception b;

    public AdLoaderException(@NonNull Error error, @NonNull Exception exc) {
        super(exc);
        this.f3298a = (Error) Objects.requireNonNull(error);
        this.b = (Exception) Objects.requireNonNull(exc);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdLoaderException { errorType = ");
        sb.append(this.f3298a);
        sb.append(", reason = ");
        sb.append(this.b);
        sb.append(" }");
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdLoaderException adLoaderException = (AdLoaderException) obj;
        return this.f3298a == adLoaderException.f3298a && Objects.equals(this.b, adLoaderException.b);
    }

    public final int hashCode() {
        return Objects.hash(this.f3298a, this.b);
    }

    @NonNull
    public final Error getErrorType() {
        return this.f3298a;
    }

    @NonNull
    public final Exception getReason() {
        return this.b;
    }
}
