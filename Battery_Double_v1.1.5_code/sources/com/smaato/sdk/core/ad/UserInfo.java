package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Gender;
import com.smaato.sdk.core.LatLng;

public final class UserInfo {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f3318a;
    @Nullable
    private final String b;
    @Nullable
    private final Gender c;
    @Nullable
    private final Integer d;
    @Nullable
    private final LatLng e;
    @Nullable
    private final String f;
    @Nullable
    private final String g;
    @Nullable
    private final String h;
    private final boolean i;

    public static final class Builder {

        /* renamed from: a reason: collision with root package name */
        private String f3319a;
        private String b;
        private Gender c;
        private Integer d;
        private LatLng e;
        private String f;
        private String g;
        private String h;
        private boolean i;

        @NonNull
        public final Builder setKeywords(@Nullable String str) {
            this.f3319a = str;
            return this;
        }

        @NonNull
        public final Builder setSearchQuery(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final Builder setGender(@Nullable Gender gender) {
            this.c = gender;
            return this;
        }

        @NonNull
        public final Builder setAge(@Nullable Integer num) {
            this.d = num;
            return this;
        }

        @NonNull
        public final Builder setLatLng(@Nullable LatLng latLng) {
            this.e = latLng;
            return this;
        }

        @NonNull
        public final Builder setRegion(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public final Builder setZip(@Nullable String str) {
            this.g = str;
            return this;
        }

        @NonNull
        public final Builder setLanguage(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public final Builder setCoppa(boolean z) {
            this.i = z;
            return this;
        }

        @NonNull
        public final UserInfo build() {
            UserInfo userInfo = new UserInfo(this.f3319a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, 0);
            return userInfo;
        }
    }

    /* synthetic */ UserInfo(String str, String str2, Gender gender, Integer num, LatLng latLng, String str3, String str4, String str5, boolean z, byte b2) {
        this(str, str2, gender, num, latLng, str3, str4, str5, z);
    }

    private UserInfo(@Nullable String str, @Nullable String str2, @Nullable Gender gender, @Nullable Integer num, @Nullable LatLng latLng, @Nullable String str3, @Nullable String str4, @Nullable String str5, boolean z) {
        this.f3318a = str;
        this.b = str2;
        this.c = gender;
        this.d = num;
        this.e = latLng;
        this.f = str3;
        this.g = str4;
        this.h = str5;
        this.i = z;
    }

    @Nullable
    public final String getKeywords() {
        return this.f3318a;
    }

    @Nullable
    public final String getSearchQuery() {
        return this.b;
    }

    @Nullable
    public final Gender getGender() {
        return this.c;
    }

    @Nullable
    public final Integer getAge() {
        return this.d;
    }

    @Nullable
    public final LatLng getLatLng() {
        return this.e;
    }

    @Nullable
    public final String getRegion() {
        return this.f;
    }

    @Nullable
    public final String getZip() {
        return this.g;
    }

    @Nullable
    public final String getLanguage() {
        return this.h;
    }

    public final boolean getCoppa() {
        return this.i;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("UserInfo{keywords='");
        sb.append(this.f3318a);
        sb.append('\'');
        sb.append(", searchQuery='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", gender=");
        sb.append(this.c);
        sb.append(", age=");
        sb.append(this.d);
        sb.append(", latLng=");
        sb.append(this.e);
        sb.append(", region='");
        sb.append(this.f);
        sb.append('\'');
        sb.append(", zip='");
        sb.append(this.g);
        sb.append('\'');
        sb.append(", language='");
        sb.append(this.h);
        sb.append('\'');
        sb.append(", coppa='");
        sb.append(this.i);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
