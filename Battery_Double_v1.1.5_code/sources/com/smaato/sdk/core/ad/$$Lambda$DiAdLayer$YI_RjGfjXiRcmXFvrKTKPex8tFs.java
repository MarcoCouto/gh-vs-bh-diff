package com.smaato.sdk.core.ad;

import android.app.Application;
import android.content.Context;
import android.preference.PreferenceManager;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.ad.-$$Lambda$DiAdLayer$YI_RjGfjXiRcmXFvrKTKPex8tFs reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiAdLayer$YI_RjGfjXiRcmXFvrKTKPex8tFs implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiAdLayer$YI_RjGfjXiRcmXFvrKTKPex8tFs INSTANCE = new $$Lambda$DiAdLayer$YI_RjGfjXiRcmXFvrKTKPex8tFs();

    private /* synthetic */ $$Lambda$DiAdLayer$YI_RjGfjXiRcmXFvrKTKPex8tFs() {
    }

    public final Object get(DiConstructor diConstructor) {
        return PreferenceManager.getDefaultSharedPreferences((Context) diConstructor.get(Application.class));
    }
}
