package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;

public final class FullscreenAdDimensionMapper {
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0062  */
    @NonNull
    public final AdDimension getDimension(@NonNull String str) {
        char c;
        int hashCode = str.hashCode();
        if (hashCode == -1762874990) {
            if (str.equals("full_1024x768")) {
                c = 3;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 667133493) {
            if (str.equals("full_320x480")) {
                c = 0;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 1366559536) {
            if (str.equals("full_768x1024")) {
                c = 2;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 1726410933 && str.equals("full_480x320")) {
            c = 1;
            switch (c) {
                case 0:
                    return AdDimension.FULLSCREEN_PORTRAIT;
                case 1:
                    return AdDimension.FULLSCREEN_LANDSCAPE;
                case 2:
                    return AdDimension.FULLSCREEN_PORTRAIT_TABLET;
                case 3:
                    return AdDimension.FULLSCREEN_LANDSCAPE_TABLET;
                default:
                    StringBuilder sb = new StringBuilder("Unknown dimension: ");
                    sb.append(str);
                    throw new IllegalArgumentException(sb.toString());
            }
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }
}
