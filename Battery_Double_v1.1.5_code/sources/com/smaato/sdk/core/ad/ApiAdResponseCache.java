package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Collection;

public interface ApiAdResponseCache {
    @Nullable
    AdResponseCacheEntry get(@NonNull String str);

    int perKeyCapacity();

    boolean put(@NonNull String str, @NonNull AdResponseCacheEntry adResponseCacheEntry);

    int remainingCapacity(@NonNull String str);

    void remove(@NonNull String str, @NonNull AdResponseCacheEntry adResponseCacheEntry);

    @NonNull
    Collection<AdResponseCacheEntry> trim(@NonNull String str);
}
