package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.CurrentTimeProvider;
import com.smaato.sdk.core.util.Objects;

public class ExpirationTimestamp {

    /* renamed from: a reason: collision with root package name */
    private final long f3313a;
    @NonNull
    private final CurrentTimeProvider b;

    public ExpirationTimestamp(long j, @NonNull CurrentTimeProvider currentTimeProvider) {
        this.f3313a = j;
        this.b = (CurrentTimeProvider) Objects.requireNonNull(currentTimeProvider);
    }

    public long getExpirationTimestamp() {
        return this.f3313a;
    }

    public boolean isExpired() {
        return this.f3313a <= this.b.currentMillisUtc();
    }

    public long getRemainingTime() {
        long currentMillisUtc = this.f3313a - this.b.currentMillisUtc();
        if (currentMillisUtc > 0) {
            return currentMillisUtc;
        }
        return 0;
    }

    @NonNull
    public String toString() {
        return String.valueOf(this.f3313a);
    }
}
