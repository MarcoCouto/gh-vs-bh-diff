package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Error;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.resourceloader.ResourceLoaderException;

public final class AdPresenterBuilderErrorMapper {
    private AdPresenterBuilderErrorMapper() {
    }

    @NonNull
    public static AdPresenterBuilderException mapError(@NonNull ResourceLoaderException resourceLoaderException) {
        Error error;
        switch (resourceLoaderException.getErrorType()) {
            case CANCELLED:
                error = Error.CANCELLED;
                break;
            case RESOURCE_EXPIRED:
                error = Error.RESOURCE_EXPIRED;
                break;
            case GENERIC:
                error = Error.GENERIC;
                break;
            case INVALID_RESPONSE:
                error = Error.TRANSPORT_GENERIC;
                break;
            case IO_ERROR:
                error = Error.TRANSPORT_IO_ERROR;
                break;
            case NETWORK_NO_CONNECTION:
                error = Error.TRANSPORT_NO_NETWORK_CONNECTION;
                break;
            case NETWORK_TIMEOUT:
                error = Error.TRANSPORT_TIMEOUT;
                break;
            case NETWORK_GENERIC:
                error = Error.TRANSPORT_GENERIC;
                break;
            case NETWORK_IO_TOO_MANY_REDIRECTS:
                error = Error.TRANSPORT_IO_TOO_MANY_REDIRECTS;
                break;
            default:
                throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{ResourceLoader.Error.class.getSimpleName(), resourceLoaderException}));
        }
        return new AdPresenterBuilderException(error, resourceLoaderException);
    }
}
