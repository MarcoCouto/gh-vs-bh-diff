package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import java.util.ArrayList;

public final class AdSettings {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3305a;
    @NonNull
    private final String b;
    @NonNull
    private final AdFormat c;
    @Nullable
    private final AdDimension d;
    @Nullable
    private final Integer e;
    @Nullable
    private final Integer f;
    @Nullable
    private final String g;
    @Nullable
    private final String h;
    @Nullable
    private final String i;

    public static final class Builder {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3306a;
        @Nullable
        private String b;
        @Nullable
        private AdFormat c;
        @Nullable
        private AdDimension d;
        @Nullable
        private Integer e;
        @Nullable
        private Integer f;
        @Nullable
        private String g;
        @Nullable
        private String h;
        @Nullable
        private String i;

        @NonNull
        public final Builder setPublisherId(@Nullable String str) {
            this.f3306a = str;
            return this;
        }

        @NonNull
        public final Builder setAdSpaceId(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final Builder setAdFormat(@Nullable AdFormat adFormat) {
            this.c = adFormat;
            return this;
        }

        @NonNull
        public final Builder setAdDimension(@Nullable AdDimension adDimension) {
            this.d = adDimension;
            return this;
        }

        @NonNull
        public final Builder setMediationNetworkName(@Nullable String str) {
            this.g = str;
            return this;
        }

        @NonNull
        public final Builder setMediationNetworkSDKVersion(@Nullable String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public final Builder setMediationAdapterVersion(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public final Builder setWidth(int i2) {
            this.e = Integer.valueOf(i2);
            return this;
        }

        @NonNull
        public final Builder setHeight(int i2) {
            this.f = Integer.valueOf(i2);
            return this;
        }

        @NonNull
        public final AdSettings build() {
            ArrayList arrayList = new ArrayList();
            if (TextUtils.isEmpty(this.f3306a)) {
                arrayList.add("publisherId");
            }
            if (TextUtils.isEmpty(this.b)) {
                arrayList.add("adSpaceId");
            }
            if (this.c == null) {
                arrayList.add("adFormat");
            }
            if (arrayList.isEmpty()) {
                AdSettings adSettings = new AdSettings(this.f3306a, this.b, this.c, this.d, this.e, this.f, this.g, this.i, this.h, 0);
                return adSettings;
            }
            StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
            sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
            throw new IllegalStateException(sb.toString());
        }
    }

    /* synthetic */ AdSettings(String str, String str2, AdFormat adFormat, AdDimension adDimension, Integer num, Integer num2, String str3, String str4, String str5, byte b2) {
        this(str, str2, adFormat, adDimension, num, num2, str3, str4, str5);
    }

    private AdSettings(@NonNull String str, @NonNull String str2, @NonNull AdFormat adFormat, @Nullable AdDimension adDimension, @Nullable Integer num, @Nullable Integer num2, @Nullable String str3, @Nullable String str4, @Nullable String str5) {
        this.f3305a = (String) Objects.requireNonNull(str);
        this.b = (String) Objects.requireNonNull(str2);
        this.c = (AdFormat) Objects.requireNonNull(adFormat);
        this.d = adDimension;
        this.e = num;
        this.f = num2;
        this.h = str3;
        this.g = str4;
        this.i = str5;
    }

    @NonNull
    public final String getPublisherId() {
        return this.f3305a;
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.b;
    }

    @NonNull
    public final AdFormat getAdFormat() {
        return this.c;
    }

    @Nullable
    public final AdDimension getAdDimension() {
        return this.d;
    }

    @Nullable
    public final Integer getWidth() {
        return this.e;
    }

    @Nullable
    public final Integer getHeight() {
        return this.f;
    }

    @Nullable
    public final String getMediationNetworkName() {
        return this.h;
    }

    @Nullable
    public final String getMediationNetworkSDKVersion() {
        return this.g;
    }

    @Nullable
    public final String getMediationAdapterVersion() {
        return this.i;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdSettings{publisherId='");
        sb.append(this.f3305a);
        sb.append('\'');
        sb.append(", adSpaceId='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", adFormat=");
        sb.append(this.c);
        sb.append(", adDimension=");
        sb.append(this.d);
        sb.append(", width=");
        sb.append(this.e);
        sb.append(", height=");
        sb.append(this.f);
        sb.append(", mediationNetworkName='");
        sb.append(this.h);
        sb.append('\'');
        sb.append(", mediationNetworkSDKVersion='");
        sb.append(this.g);
        sb.append('\'');
        sb.append(", mediationAdapterVersion='");
        sb.append(this.i);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
