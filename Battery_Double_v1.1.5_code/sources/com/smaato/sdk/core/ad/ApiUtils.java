package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.ironsource.environment.ConnectivityService;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.core.network.NetworkConnectionType;
import com.smaato.sdk.core.util.HeaderUtils;
import java.util.List;
import java.util.Map;

public final class ApiUtils {

    /* renamed from: a reason: collision with root package name */
    private static final HeaderUtils f3310a = new HeaderUtils();

    private ApiUtils() {
    }

    @Nullable
    public static String retrieveSessionId(@NonNull Map<String, List<String>> map) {
        return f3310a.extractHeaderMultiValue(map, "X-SMT-SessionId");
    }

    @Nullable
    public static String retrieveSci(@NonNull Map<String, List<String>> map) {
        return f3310a.extractHeaderMultiValue(map, "SCI");
    }

    @NonNull
    public static String connectionTypeToApiValue(@NonNull NetworkConnectionType networkConnectionType) {
        switch (networkConnectionType) {
            case CARRIER_2G:
                return "2g";
            case CARRIER_3G:
                return ConnectivityService.NETWORK_TYPE_3G;
            case CARRIER_4G:
                return "4g";
            case CARRIER_UNKNOWN:
                return "carrier";
            case WIFI:
                return "wifi";
            case ETHERNET:
                return "ethernet";
            case OTHER:
                return "Other";
            default:
                throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{NetworkConnectionType.class.getSimpleName(), networkConnectionType}));
        }
    }

    @NonNull
    public static String adFormatToApiValue(@NonNull AdFormat adFormat) {
        switch (adFormat) {
            case DISPLAY:
                return "display";
            case STATIC_IMAGE:
                return "img";
            case RICH_MEDIA:
                return "richmedia";
            case VIDEO:
                return "video";
            case NATIVE:
                return "native";
            case INTERSTITIAL:
                return VideoType.INTERSTITIAL;
            default:
                throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{AdFormat.class.getSimpleName(), adFormat}));
        }
    }
}
