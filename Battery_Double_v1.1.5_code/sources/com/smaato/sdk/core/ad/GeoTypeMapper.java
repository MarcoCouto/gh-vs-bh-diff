package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;

class GeoTypeMapper {
    GeoTypeMapper() {
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Integer a(@NonNull GeoType geoType) {
        switch (geoType) {
            case GPS:
                return Integer.valueOf(1);
            case USER_PROVIDED:
                return Integer.valueOf(3);
            default:
                throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{GeoType.class.getSimpleName(), geoType}));
        }
    }
}
