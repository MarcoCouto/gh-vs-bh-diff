package com.smaato.sdk.core.ad;

import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ErrorReporting;
import com.smaato.sdk.core.analytics.Analytics;
import com.smaato.sdk.core.api.ApiConnector;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.datacollector.DataCollector;
import com.smaato.sdk.core.di.AdLoaderProviderFunction;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.CoreDiNames;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.gdpr.IabCmpDataStorage;
import com.smaato.sdk.core.gdpr.SomaGdprDataSource;
import com.smaato.sdk.core.gdpr.SomaGdprUtils;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.SdkConfigHintBuilder;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.violationreporter.AdQualityViolationReporter;

public final class DiAdLayer {

    private interface AdRequestMapperProviderFunction extends Function<AdLoaderPlugin, AdRequestMapper> {
    }

    private static class DefaultThreading implements Threading {
        private DefaultThreading() {
        }

        /* synthetic */ DefaultThreading(byte b) {
            this();
        }

        public void runOnBackgroundThread(@NonNull Runnable runnable) {
            Threads.runOnBackgroundThread(runnable);
        }
    }

    private DiAdLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry(boolean z) {
        return DiRegistry.of(new Consumer(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                DiAdLayer.a(this.f$0, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(boolean z, DiRegistry diRegistry) {
        diRegistry.registerFactory(AdLoaderProviderFunction.class, $$Lambda$DiAdLayer$QaBNWPuH360pcFSDoTqqjhBNw.INSTANCE);
        diRegistry.registerSingletonFactory(Threading.class, $$Lambda$DiAdLayer$PbhZv9nmnHN9ILNztD98N1KA8H4.INSTANCE);
        diRegistry.registerFactory(AdRequestMapperProviderFunction.class, new ClassFactory(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiAdLayer.a(this.f$0, diConstructor);
            }
        });
        diRegistry.registerSingletonFactory(AdLoaderAdQualityViolationUtils.class, $$Lambda$DiAdLayer$vDtXXVWcmlan04NNh8aSbKJnLI.INSTANCE);
        diRegistry.registerFactory(CoreDiNames.NAME_DEFAULT_SHARED_PREFERENCES, SharedPreferences.class, $$Lambda$DiAdLayer$YI_RjGfjXiRcmXFvrKTKPex8tFs.INSTANCE);
        diRegistry.registerSingletonFactory(IabCmpDataStorage.class, $$Lambda$DiAdLayer$Z4iMR4_eqvaG5LUJfKYMyfymXPk.INSTANCE);
        diRegistry.registerSingletonFactory(SomaGdprUtils.class, $$Lambda$DiAdLayer$e64cdzqMT3JobhNOT4kYmRKCMPw.INSTANCE);
        diRegistry.registerSingletonFactory(SomaGdprDataSource.class, $$Lambda$DiAdLayer$5Klnh6hL65mwkku0TFyOhVjvefo.INSTANCE);
        diRegistry.registerSingletonFactory(UserInfoSupplier.class, $$Lambda$DiAdLayer$8fx8bYLRNlZWKvZYMKfiQf3_RE.INSTANCE);
        diRegistry.registerFactory(FullscreenAdDimensionMapper.class, $$Lambda$DiAdLayer$98MlwooUQItPsVofYS91N9XPPpI.INSTANCE);
        diRegistry.registerFactory(GeoTypeMapper.class, $$Lambda$DiAdLayer$xW0MVZs8spDQhiHmONhDn42lDQ8.INSTANCE);
        diRegistry.registerFactory(AppBackgroundAwareHandler.class, $$Lambda$DiAdLayer$dk0SjnTza1vjI0t6dhbUB6eXhRQ.INSTANCE);
        diRegistry.registerFactory(OneTimeActionFactory.class, $$Lambda$DiAdLayer$H8VyPCFg0h2kY9QANQismGwYl8.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoader a(DiConstructor diConstructor, AdLoaderPlugin adLoaderPlugin) {
        AdLoader adLoader = new AdLoader((Logger) diConstructor.get(Logger.class), (AdRequestMapper) ((AdRequestMapperProviderFunction) diConstructor.get(AdRequestMapperProviderFunction.class)).apply(adLoaderPlugin), adLoaderPlugin, (ApiConnector) diConstructor.get(ApiConnector.class), (SdkConfigHintBuilder) diConstructor.get(SdkConfigHintBuilder.class), (AdLoaderAdQualityViolationUtils) diConstructor.get(AdLoaderAdQualityViolationUtils.class), (ErrorReporting) diConstructor.getOrNull(ErrorReporting.class));
        return adLoader;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderProviderFunction l(DiConstructor diConstructor) {
        return new AdLoaderProviderFunction() {
            public final Object apply(Object obj) {
                return DiAdLayer.a(DiConstructor.this, (AdLoaderPlugin) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Threading k(DiConstructor diConstructor) {
        return new DefaultThreading(0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdRequestMapper a(DiConstructor diConstructor, boolean z, AdLoaderPlugin adLoaderPlugin) {
        AdRequestMapper adRequestMapper = new AdRequestMapper((Logger) diConstructor.get(Logger.class), (DataCollector) diConstructor.get(DataCollector.class), z, adLoaderPlugin, (SomaGdprDataSource) diConstructor.get(SomaGdprDataSource.class), (GeoTypeMapper) diConstructor.get(GeoTypeMapper.class), (Analytics) diConstructor.get(Analytics.class));
        return adRequestMapper;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdRequestMapperProviderFunction a(boolean z, DiConstructor diConstructor) {
        return new AdRequestMapperProviderFunction(z) {
            private final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return DiAdLayer.a(DiConstructor.this, this.f$1, (AdLoaderPlugin) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderAdQualityViolationUtils j(DiConstructor diConstructor) {
        return new AdLoaderAdQualityViolationUtils((AdQualityViolationReporter) diConstructor.get(AdQualityViolationReporter.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ IabCmpDataStorage h(DiConstructor diConstructor) {
        return new IabCmpDataStorage((SharedPreferences) diConstructor.get(CoreDiNames.NAME_DEFAULT_SHARED_PREFERENCES, SharedPreferences.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SomaGdprUtils g(DiConstructor diConstructor) {
        return new SomaGdprUtils();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SomaGdprDataSource f(DiConstructor diConstructor) {
        return new SomaGdprDataSource((IabCmpDataStorage) diConstructor.get(IabCmpDataStorage.class), (SomaGdprUtils) diConstructor.get(SomaGdprUtils.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ UserInfoSupplier e(DiConstructor diConstructor) {
        return new UserInfoSupplier();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ FullscreenAdDimensionMapper d(DiConstructor diConstructor) {
        return new FullscreenAdDimensionMapper();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ GeoTypeMapper c(DiConstructor diConstructor) {
        return new GeoTypeMapper();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AppBackgroundAwareHandler b(DiConstructor diConstructor) {
        return new AppBackgroundAwareHandler(DiLogLayer.getLoggerFrom(diConstructor), Threads.newUiHandler(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ OneTimeActionFactory a(DiConstructor diConstructor) {
        return new OneTimeActionFactory(Threads.newUiHandler());
    }

    @Nullable
    public static <T> T tryGetOrNull(@NonNull DiConstructor diConstructor, @Nullable String str, @NonNull Class<T> cls) {
        Objects.requireNonNull(diConstructor);
        Objects.requireNonNull(cls);
        try {
            return diConstructor.get(str, cls);
        } catch (Exception e) {
            ((Logger) diConstructor.get(Logger.class)).error(LogDomain.CORE, e, "Probably configuration troubles", new Object[0]);
            return null;
        }
    }

    @NonNull
    public static UserInfoSupplier getUserInfoFactoryFrom(@NonNull DiConstructor diConstructor) {
        return (UserInfoSupplier) diConstructor.get(UserInfoSupplier.class);
    }

    @NonNull
    public static FullscreenAdDimensionMapper getFullscreenAdDimensionMapperFrom(@NonNull DiConstructor diConstructor) {
        return (FullscreenAdDimensionMapper) diConstructor.get(FullscreenAdDimensionMapper.class);
    }
}
