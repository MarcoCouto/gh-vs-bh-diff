package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeAction;
import com.smaato.sdk.core.util.OneTimeAction.Listener;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.Threads;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

public abstract class AdInteractor<TAdObject extends AdObject> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final TAdObject f3289a;
    @NonNull
    private final OneTimeAction b;
    @NonNull
    private final Set<TtlListener> c = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    @NonNull
    protected final StateMachine<Event, State> stateMachine;

    public interface TtlListener {
        void onTTLExpired(@NonNull AdInteractor<?> adInteractor);
    }

    public abstract boolean isValid();

    public AdInteractor(@NonNull TAdObject tadobject, @NonNull StateMachine<Event, State> stateMachine2, @NonNull OneTimeActionFactory oneTimeActionFactory) {
        this.f3289a = (AdObject) Objects.requireNonNull(tadobject, "Parameter TAdObject cannot be null for AdInteractor::new");
        this.stateMachine = (StateMachine) Objects.requireNonNull(stateMachine2, "Parameter stateMachine cannot be null for AdInteractor::new");
        this.b = (OneTimeAction) Objects.requireNonNull(oneTimeActionFactory.createOneTimeAction(new Listener() {
            public final void doAction() {
                AdInteractor.this.b();
            }
        }));
        stateMachine2.addListener(new StateMachine.Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                AdInteractor.this.a((State) obj, (State) obj2, metadata);
            }
        });
    }

    @NonNull
    public String getPublisherId() {
        return this.f3289a.getSomaApiContext().getApiAdRequest().getPublisherId();
    }

    @NonNull
    public String getAdSpaceId() {
        return this.f3289a.getSomaApiContext().getApiAdRequest().getAdSpaceId();
    }

    @NonNull
    public String getSessionId() {
        return this.f3289a.getSomaApiContext().getApiAdResponse().getSessionId();
    }

    @Nullable
    public String getCreativeId() {
        return this.f3289a.getSomaApiContext().getApiAdResponse().getCreativeId();
    }

    /* access modifiers changed from: private */
    public void a(@NonNull State state, @NonNull State state2, @NonNull Metadata metadata) {
        switch (state2) {
            case CREATED:
                Threads.runOnUi(new Runnable() {
                    public final void run() {
                        AdInteractor.this.c();
                    }
                });
                return;
            case IMPRESSED:
                a();
                return;
            case COMPLETE:
                a();
                return;
            case TO_BE_DELETED:
                a();
                break;
        }
    }

    private void a() {
        OneTimeAction oneTimeAction = this.b;
        oneTimeAction.getClass();
        Threads.runOnUi(new Runnable() {
            public final void run() {
                OneTimeAction.this.stop();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c() {
        this.b.start(getAdObject().getSomaApiContext().getApiAdResponse().getExpirationTimestamp().getRemainingTime());
    }

    public void addTtlListener(@NonNull TtlListener ttlListener) {
        this.c.add(ttlListener);
    }

    public void removeTtlListener(@NonNull TtlListener ttlListener) {
        this.c.remove(ttlListener);
    }

    /* access modifiers changed from: private */
    public void b() {
        Iterator it = new ArrayList(this.c).iterator();
        while (it.hasNext()) {
            ((TtlListener) it.next()).onTTLExpired(this);
        }
        this.stateMachine.onEvent(Event.EXPIRE_TTL);
    }

    @NonNull
    public TAdObject getAdObject() {
        return this.f3289a;
    }

    public void addStateListener(@NonNull StateMachine.Listener<State> listener) {
        this.stateMachine.addListener(listener);
    }

    public void removeStateListener(@NonNull StateMachine.Listener<State> listener) {
        this.stateMachine.deleteListener(listener);
    }

    public void onEvent(@NonNull Event event) {
        this.stateMachine.onEvent(event);
    }
}
