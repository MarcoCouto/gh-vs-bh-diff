package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Gender;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.LatLng;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.analytics.Analytics;
import com.smaato.sdk.core.api.ApiAdRequest;
import com.smaato.sdk.core.api.ApiAdRequest.Builder;
import com.smaato.sdk.core.datacollector.DataCollector;
import com.smaato.sdk.core.datacollector.SystemInfo;
import com.smaato.sdk.core.gdpr.PiiParam;
import com.smaato.sdk.core.gdpr.SomaGdprData;
import com.smaato.sdk.core.gdpr.SomaGdprDataSource;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration;
import java.util.List;

public class AdRequestMapper {

    /* renamed from: a reason: collision with root package name */
    private static final CharSequence f3303a = ",";
    @NonNull
    private final Logger b;
    @NonNull
    private final DataCollector c;
    private final boolean d;
    @NonNull
    private final AdLoaderPlugin e;
    @NonNull
    private final SomaGdprDataSource f;
    @NonNull
    private final GeoTypeMapper g;
    @NonNull
    private final Analytics h;

    public static final class UnresolvedServerAdFormatException extends RuntimeException {
    }

    public AdRequestMapper(@NonNull Logger logger, @NonNull DataCollector dataCollector, boolean z, @NonNull AdLoaderPlugin adLoaderPlugin, @NonNull SomaGdprDataSource somaGdprDataSource, @NonNull GeoTypeMapper geoTypeMapper, @NonNull Analytics analytics) {
        this.b = (Logger) Objects.requireNonNull(logger);
        this.c = (DataCollector) Objects.requireNonNull(dataCollector);
        this.d = z;
        this.e = (AdLoaderPlugin) Objects.requireNonNull(adLoaderPlugin);
        this.f = (SomaGdprDataSource) Objects.requireNonNull(somaGdprDataSource);
        this.g = (GeoTypeMapper) Objects.requireNonNull(geoTypeMapper);
        this.h = (Analytics) Objects.requireNonNull(analytics);
    }

    @NonNull
    public ApiAdRequest map(@NonNull AdRequest adRequest) {
        Objects.requireNonNull(adRequest);
        AdSettings adSettings = adRequest.adSettings;
        AdFormat resolveAdFormatToServerAdFormat = this.e.resolveAdFormatToServerAdFormat(adSettings.getAdFormat(), this.b);
        if (resolveAdFormatToServerAdFormat != null) {
            UserInfo userInfo = adRequest.userInfo;
            Builder builder = ApiAdRequest.builder();
            SomaGdprData somaGdprData = this.f.getSomaGdprData();
            this.b.debug(LogDomain.AD, "map: somaGdprData = %s", somaGdprData);
            builder.setHttpsOnly(Integer.valueOf(this.d ? 1 : 0));
            builder.setPublisherId(adSettings.getPublisherId()).setAdSpaceId(adSettings.getAdSpaceId()).setAdFormat(ApiUtils.adFormatToApiValue(resolveAdFormatToServerAdFormat)).setAdDimension((String) Objects.transformOrNull(adSettings.getAdDimension(), new Function() {
                public final Object apply(Object obj) {
                    return AdRequestMapper.this.a((AdDimension) obj);
                }
            })).setWidth(adSettings.getWidth()).setHeight(adSettings.getHeight()).setMediationNetworkName(adSettings.getMediationNetworkName()).setMediationNetworkSDKVersion(adSettings.getMediationNetworkSDKVersion()).setMediationAdapterVersion(adSettings.getMediationAdapterVersion());
            Boolean isGdprEnabled = somaGdprData.isGdprEnabled();
            if (isGdprEnabled != null) {
                builder.setGdpr(Integer.valueOf(isGdprEnabled.booleanValue() ? 1 : 0));
            }
            if (!somaGdprData.getConsentString().isEmpty()) {
                builder.setGdprConsent(somaGdprData.getConsentString());
            }
            builder.setCoppa(Integer.valueOf(userInfo.getCoppa() ? 1 : 0));
            builder.setKeywords(userInfo.getKeywords()).setSearchQuery(userInfo.getSearchQuery()).setLanguage(userInfo.getLanguage());
            if (somaGdprData.isUsageAllowedFor(PiiParam.GENDER)) {
                builder.setGender((String) Objects.transformOrNull(userInfo.getGender(), new Function() {
                    public final Object apply(Object obj) {
                        return AdRequestMapper.this.a((Gender) obj);
                    }
                }));
            }
            if (somaGdprData.isUsageAllowedFor(PiiParam.AGE)) {
                builder.setAge(userInfo.getAge());
            }
            LatLng latLng = userInfo.getLatLng();
            builder.setRegion(userInfo.getRegion());
            if (somaGdprData.isUsageAllowedFor(PiiParam.ZIP)) {
                builder.setZip(userInfo.getZip());
            }
            GeoType geoType = null;
            LatLng locationData = this.c.getLocationData();
            if (locationData != null) {
                geoType = GeoType.GPS;
                latLng = locationData;
            } else if (latLng != null) {
                geoType = GeoType.USER_PROVIDED;
            }
            if (latLng != null) {
                if (somaGdprData.isUsageAllowedFor(PiiParam.GPS)) {
                    builder.setGps(Joiner.join((CharSequence) ",", Double.valueOf(latLng.getLatitude()), Double.valueOf(latLng.getLongitude())));
                }
                GeoTypeMapper geoTypeMapper = this.g;
                geoTypeMapper.getClass();
                builder.setGeoType((Integer) Objects.transformOrNull(geoType, new Function() {
                    public final Object apply(Object obj) {
                        return GeoTypeMapper.this.a((GeoType) obj);
                    }
                }));
            }
            SystemInfo systemInfo = this.c.getSystemInfo();
            builder.setCarrierName(systemInfo.getCarrierName()).setCarrierCode(systemInfo.getCarrierCode()).setGoogleDnt(systemInfo.isGoogleLimitAdTrackingEnabled()).setClient(String.format("sdkandroid_%s", new Object[]{SmaatoSdk.getVersion()})).setConnection((String) Objects.transformOrNull(systemInfo.getNetworkConnectionType(), $$Lambda$mFDUD4wzFHv0wAVQXnUEzaDsQ.INSTANCE)).setBundle(systemInfo.getPackageName());
            if (somaGdprData.isUsageAllowedFor(PiiParam.DEVICE_MODEL)) {
                builder.setDeviceModel(systemInfo.getDeviceModelName());
            }
            if (somaGdprData.isUsageAllowedFor(PiiParam.GOOGLE_AD_ID)) {
                builder.setGoogleAdId(systemInfo.getGoogleAdvertisingId());
            }
            ApiAdRequestExtras apiAdRequestExtras = new ApiAdRequestExtras(resolveAdFormatToServerAdFormat);
            this.e.addApiAdRequestExtras(apiAdRequestExtras, this.b);
            builder.setExtraParameters(apiAdRequestExtras.toMap());
            Objects.onNotNull(adRequest.keyValuePairs, new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setKeyValuePairs(((KeyValuePairs) obj).getAllKeyValuePairs());
                }
            });
            builder.setHeaderClient(String.format("sdk/android/%s", new Object[]{SmaatoSdk.getVersion()}));
            List connectedPluginNames = this.h.getConnectedPluginNames();
            if (!connectedPluginNames.isEmpty()) {
                builder.setExtensions(Joiner.join(f3303a, (Iterable) connectedPluginNames));
            }
            return builder.build();
        }
        throw new UnresolvedServerAdFormatException();
    }

    /* access modifiers changed from: private */
    @NonNull
    public String a(@NonNull AdDimension adDimension) {
        switch (adDimension) {
            case XX_LARGE:
                return "xxlarge";
            case X_LARGE:
                return "xlarge";
            case LARGE:
                return NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_LARGE;
            case MEDIUM:
                return "medium";
            case SMALL:
                return NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_SMALL;
            case MEDIUM_RECTANGLE:
                return "medrect";
            case SKYSCRAPER:
                return "sky";
            case LEADERBOARD:
                return "leader";
            case FULLSCREEN_PORTRAIT:
                return "full_320x480";
            case FULLSCREEN_LANDSCAPE:
                return "full_480x320";
            case FULLSCREEN_PORTRAIT_TABLET:
                return "full_768x1024";
            case FULLSCREEN_LANDSCAPE_TABLET:
                return "full_1024x768";
            default:
                throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{AdDimension.class.getSimpleName(), adDimension}));
        }
    }

    /* access modifiers changed from: private */
    @NonNull
    public String a(@NonNull Gender gender) {
        switch (gender) {
            case FEMALE:
                return "f";
            case MALE:
                return "m";
            case OTHER:
                return "o";
            default:
                throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{Gender.class.getSimpleName(), gender}));
        }
    }
}
