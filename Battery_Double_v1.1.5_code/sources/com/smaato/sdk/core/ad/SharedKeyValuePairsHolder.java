package com.smaato.sdk.core.ad;

import androidx.annotation.Nullable;
import com.smaato.sdk.core.KeyValuePairs;

public class SharedKeyValuePairsHolder {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private KeyValuePairs f3317a;

    @Nullable
    public KeyValuePairs getKeyValuePairs() {
        if (this.f3317a == null) {
            return null;
        }
        return this.f3317a.clone();
    }

    public void setKeyValuePairs(@Nullable KeyValuePairs keyValuePairs) {
        this.f3317a = keyValuePairs == null ? null : keyValuePairs.clone();
    }
}
