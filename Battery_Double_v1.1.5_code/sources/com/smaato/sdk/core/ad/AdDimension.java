package com.smaato.sdk.core.ad;

import com.explorestack.iab.vast.VastError;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public enum AdDimension {
    XX_LARGE(ModuleDescriptor.MODULE_VERSION, 50),
    X_LARGE(VastError.ERROR_CODE_GENERAL_WRAPPER, 50),
    LARGE(216, 36),
    MEDIUM(168, 28),
    SMALL(120, 20),
    MEDIUM_RECTANGLE(VastError.ERROR_CODE_GENERAL_WRAPPER, 250),
    SKYSCRAPER(120, 600),
    LEADERBOARD(728, 90),
    FULLSCREEN_PORTRAIT(ModuleDescriptor.MODULE_VERSION, 480),
    FULLSCREEN_LANDSCAPE(480, ModuleDescriptor.MODULE_VERSION),
    FULLSCREEN_PORTRAIT_TABLET(768, 1024),
    FULLSCREEN_LANDSCAPE_TABLET(1024, 768);
    

    /* renamed from: a reason: collision with root package name */
    private final int f3287a;
    private final int b;
    private final float c;

    private AdDimension(int i, int i2) {
        this.f3287a = i;
        this.b = i2;
        this.c = ((float) i) / ((float) i2);
    }

    public final int getWidth() {
        return this.f3287a;
    }

    public final int getHeight() {
        return this.b;
    }

    public final float getAspectRatio() {
        return this.c;
    }
}
