package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkComponentException;

public final class AdPresenterBuilderException extends Exception implements SdkComponentException<Error> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Error f3301a;
    @NonNull
    private final Exception b;

    public AdPresenterBuilderException(@NonNull Error error, @NonNull Exception exc) {
        super(exc);
        this.f3301a = (Error) Objects.requireNonNull(error);
        this.b = (Exception) Objects.requireNonNull(exc);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdPresenterBuilderException { errorType = ");
        sb.append(this.f3301a);
        sb.append(", reason = ");
        sb.append(this.b);
        sb.append(" }");
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdPresenterBuilderException adPresenterBuilderException = (AdPresenterBuilderException) obj;
        return this.f3301a == adPresenterBuilderException.f3301a && Objects.equals(this.b, adPresenterBuilderException.b);
    }

    public final int hashCode() {
        return Objects.hash(this.f3301a, this.b);
    }

    @NonNull
    public final Error getErrorType() {
        return this.f3301a;
    }

    @NonNull
    public final Exception getReason() {
        return this.b;
    }
}
