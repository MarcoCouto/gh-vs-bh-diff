package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;

public interface AdPresenterBuilder {

    public enum Error {
        INVALID_RESPONSE,
        GENERIC,
        RESOURCE_EXPIRED,
        TRANSPORT_NO_NETWORK_CONNECTION,
        TRANSPORT_TIMEOUT,
        TRANSPORT_IO_ERROR,
        TRANSPORT_GENERIC,
        TRANSPORT_IO_TOO_MANY_REDIRECTS,
        CANCELLED
    }

    public interface Listener {
        void onAdPresenterBuildError(@NonNull AdPresenterBuilder adPresenterBuilder, @NonNull AdPresenterBuilderException adPresenterBuilderException);

        void onAdPresenterBuildSuccess(@NonNull AdPresenterBuilder adPresenterBuilder, @NonNull AdPresenter adPresenter);
    }

    void buildAdPresenter(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener);
}
