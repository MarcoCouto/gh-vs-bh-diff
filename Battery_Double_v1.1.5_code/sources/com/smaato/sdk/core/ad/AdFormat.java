package com.smaato.sdk.core.ad;

public enum AdFormat {
    DISPLAY,
    STATIC_IMAGE,
    RICH_MEDIA,
    VIDEO,
    NATIVE,
    INTERSTITIAL
}
