package com.smaato.sdk.core.configcheck;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import java.util.Collections;
import java.util.Set;

public class ExpectedManifestEntries {
    public static final ExpectedManifestEntries EMPTY = new ExpectedManifestEntries(Collections.emptySet(), Collections.emptySet(), Collections.emptySet());
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Set<String> f3360a;
    @NonNull
    private final Set<String> b;
    @NonNull
    private final Set<String> c;

    public ExpectedManifestEntries(@NonNull Set<String> set, @NonNull Set<String> set2, @NonNull Set<String> set3) {
        Objects.requireNonNull(set, "Parameter permissionsMandatory cannot be null for ExpectedManifestEntries::new");
        this.f3360a = Collections.unmodifiableSet(set);
        Objects.requireNonNull(set2, "Parameter permissionsStronglyRecommended cannot be null for ExpectedManifestEntries::new");
        this.b = Collections.unmodifiableSet(set2);
        Objects.requireNonNull(set3, "Parameter activities cannot be null for ExpectedManifestEntries::new");
        this.c = Collections.unmodifiableSet(set3);
    }

    @NonNull
    public Set<String> getPermissionsMandatory() {
        return this.f3360a;
    }

    @NonNull
    public Set<String> getPermissionsStronglyRecommended() {
        return this.b;
    }

    @NonNull
    public Set<String> getActivities() {
        return this.c;
    }
}
