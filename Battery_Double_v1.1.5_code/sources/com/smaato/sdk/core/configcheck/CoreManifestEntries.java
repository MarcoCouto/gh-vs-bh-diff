package com.smaato.sdk.core.configcheck;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.browser.SmaatoSdkBrowserActivity;
import com.smaato.sdk.core.util.collections.Lists;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class CoreManifestEntries {
    @NonNull
    public static final Set<String> ACTIVITIES = Collections.unmodifiableSet(new HashSet(Lists.of((T[]) new String[]{SmaatoSdkBrowserActivity.class.getName()})));
    @NonNull
    public static final Set<String> PERMISSIONS_MANDATORY = Collections.unmodifiableSet(new HashSet(Lists.of((T[]) new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})));
    @NonNull
    public static final Set<String> PERMISSIONS_STRONGLY_RECOMMENDED = Collections.emptySet();

    private CoreManifestEntries() {
    }
}
