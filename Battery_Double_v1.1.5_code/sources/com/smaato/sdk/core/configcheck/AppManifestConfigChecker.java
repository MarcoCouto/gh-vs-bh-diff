package com.smaato.sdk.core.configcheck;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Arrays;
import com.smaato.sdk.core.util.Objects;

public final class AppManifestConfigChecker {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3359a;
    @NonNull
    private final Context b;
    @NonNull
    private final ExpectedManifestEntries c;

    public AppManifestConfigChecker(@NonNull Logger logger, @NonNull Context context, @NonNull ExpectedManifestEntries expectedManifestEntries) {
        this.f3359a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for AppManifestConfigChecker::new");
        this.b = (Context) Objects.requireNonNull(context, "Parameter context cannot be null for AppManifestConfigChecker::new");
        this.c = (ExpectedManifestEntries) Objects.requireNonNull(expectedManifestEntries, "Parameter expectedManifestEntries cannot be null for AppManifestConfigChecker::new");
    }

    @NonNull
    public final AppManifestConfigCheckResult check() {
        Object[] objArr;
        String[] a2 = a();
        boolean z = true;
        for (String str : this.c.getPermissionsMandatory()) {
            if (!(a2 != null ? Arrays.contains(a2, str) : false)) {
                this.f3359a.error(LogDomain.CONFIG_CHECK, "Mandatory permission is not declared in the application manifest: %s", str);
                z = false;
            }
        }
        for (String str2 : this.c.getPermissionsStronglyRecommended()) {
            if (!(a2 != null ? Arrays.contains(a2, str2) : false)) {
                this.f3359a.warning(LogDomain.CONFIG_CHECK, "Strongly recommended permission is not declared in the application manifest: %s", str2);
            }
        }
        ActivityInfo[] activityInfoArr = a(1).activities;
        if (activityInfoArr == null) {
            objArr = null;
        } else {
            String[] strArr = new String[activityInfoArr.length];
            for (int i = 0; i < activityInfoArr.length; i++) {
                strArr[i] = activityInfoArr[i].name;
            }
            objArr = strArr;
        }
        boolean z2 = true;
        for (String str3 : this.c.getActivities()) {
            if (!(objArr != null ? Arrays.contains(objArr, str3) : false)) {
                this.f3359a.error(LogDomain.CONFIG_CHECK, "Mandatory activity is not declared in the application manifest: %s", str3);
                z2 = false;
            }
        }
        return new AppManifestConfigCheckResult(z, z2);
    }

    public final boolean isPermissionDeclared(@NonNull String str) {
        Objects.requireNonNull(str, "Parameter permission cannot be null for AppManifestConfigChecker::isPermissionDeclared");
        String[] a2 = a();
        if (a2 != null) {
            return Arrays.contains(a2, str);
        }
        return false;
    }

    @Nullable
    private String[] a() {
        return a(4096).requestedPermissions;
    }

    @NonNull
    private PackageInfo a(int i) {
        try {
            return this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), i);
        } catch (NameNotFoundException e) {
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Wrong package name passed to PackageManager.getPackageInfo.", e);
            this.f3359a.error(LogDomain.CONFIG_CHECK, illegalArgumentException, "getPackageInfo() failed", new Object[0]);
            throw illegalArgumentException;
        }
    }
}
