package com.smaato.sdk.core.configcheck;

public final class AppManifestConfigCheckResult {

    /* renamed from: a reason: collision with root package name */
    private final boolean f3358a;
    private final boolean b;

    public AppManifestConfigCheckResult(boolean z, boolean z2) {
        this.f3358a = z;
        this.b = z2;
    }

    public final boolean isAppManifestConfiguredProperlyForSdk() {
        return this.f3358a && this.b;
    }

    public final boolean allMandatoryPermissionsDeclared() {
        return this.f3358a;
    }

    public final boolean allActivitiesDeclared() {
        return this.b;
    }
}
