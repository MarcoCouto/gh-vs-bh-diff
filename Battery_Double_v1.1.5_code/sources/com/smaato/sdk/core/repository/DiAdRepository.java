package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoader;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.ApiAdResponseCache;
import com.smaato.sdk.core.ad.MultipleApiAdResponseCache;
import com.smaato.sdk.core.config.Configuration;
import com.smaato.sdk.core.config.ConfigurationRepository;
import com.smaato.sdk.core.di.AdLoaderProviderFunction;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.FlowExecutors;
import java.util.HashMap;

public final class DiAdRepository {
    public static final String CORE_DEFAULT_CONFIGURATION_REPOSITORY = "default_configuration_provider";

    /* renamed from: a reason: collision with root package name */
    private static final Configuration f3430a = new Configuration(20, State.IMPRESSED);

    public interface Provider extends Function<String, AdRepository> {
    }

    private DiAdRepository() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiAdRepository$cHkAhyPmdwkG92NECJlC_lpQUQ.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(Provider.class, $$Lambda$DiAdRepository$mS69cCNEjSB9b3wDjJMpL27zAFs.INSTANCE);
        diRegistry.registerSingletonFactory(ApiAdResponseCache.class, $$Lambda$DiAdRepository$Tna2zxrZsOE6d2jZpfIGzi61E0M.INSTANCE);
        diRegistry.registerSingletonFactory(CORE_DEFAULT_CONFIGURATION_REPOSITORY, ConfigurationRepository.class, $$Lambda$DiAdRepository$23BGipMcR94yssU_nbw1dawlo.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdRepository a(DiConstructor diConstructor, String str) {
        AdRepositoryImpl adRepositoryImpl = new AdRepositoryImpl(DiLogLayer.getLoggerFrom(diConstructor), (ApiAdResponseCache) diConstructor.get(ApiAdResponseCache.class), (AdPresenterCache) diConstructor.get(str, AdPresenterCache.class), (AdLoadersRegistry) diConstructor.get(str, AdLoadersRegistry.class), new Supplier(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object get() {
                return DiAdRepository.b(DiConstructor.this, this.f$1);
            }
        }, (FlowExecutors) diConstructor.get(FlowExecutors.class));
        return adRepositoryImpl;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Provider c(DiConstructor diConstructor) {
        return new Provider() {
            public final Object apply(Object obj) {
                return DiAdRepository.a(DiConstructor.this, (String) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoader b(DiConstructor diConstructor, String str) {
        return (AdLoader) ((AdLoaderProviderFunction) diConstructor.get(AdLoaderProviderFunction.class)).apply(diConstructor.get(str, AdLoaderPlugin.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ApiAdResponseCache b(DiConstructor diConstructor) {
        return new MultipleApiAdResponseCache((ConfigurationRepository) diConstructor.get(CORE_DEFAULT_CONFIGURATION_REPOSITORY, ConfigurationRepository.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConfigurationRepository a(DiConstructor diConstructor) {
        return new ConfigurationRepository(new HashMap(), f3430a);
    }
}
