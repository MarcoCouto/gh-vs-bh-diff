package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenter;

public interface AdTypeStrategy {
    @NonNull
    Class<? extends AdPresenter> getAdPresenterClass();

    @NonNull
    String getUniqueKeyForType();
}
