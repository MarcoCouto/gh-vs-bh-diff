package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoader;
import java.util.Set;

public interface AdLoadersRegistry {
    @NonNull
    Set<AdLoader> getAdLoaders(@NonNull String str);

    void register(@NonNull String str, @NonNull AdLoader adLoader);

    int remainingCapacity(@NonNull String str);

    boolean unregister(@NonNull String str, @NonNull AdLoader adLoader);
}
