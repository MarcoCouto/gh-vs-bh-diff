package com.smaato.sdk.core.repository;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdRequest;

public interface AdRepository {

    public interface Listener {
        void onAdLoadError(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdLoaderException adLoaderException);

        void onAdLoadSuccess(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdPresenter adPresenter);
    }

    @MainThread
    void loadAd(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdRequest adRequest, @NonNull Listener listener);
}
