package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;

public interface AdPresenterCache {
    @Nullable
    AdPresenter get(@NonNull String str);

    int perKeyCapacity();

    boolean put(@NonNull String str, @NonNull AdPresenter adPresenter);

    int remainingCapacity(@NonNull String str);

    void trim(@NonNull String str);
}
