package com.smaato.sdk.core.repository;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoader;
import com.smaato.sdk.core.ad.AdLoader.Listener;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterBuilderException;
import com.smaato.sdk.core.ad.AdRequest;
import com.smaato.sdk.core.ad.AdResponseCacheEntry;
import com.smaato.sdk.core.ad.ApiAdResponseCache;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow;
import com.smaato.sdk.flow.Flow.CC;
import com.smaato.sdk.flow.Flow.Emitter;
import com.smaato.sdk.flow.Flow.OnSubscribe;
import com.smaato.sdk.flow.FlowExecutors;
import com.smaato.sdk.flow.Publisher;

final class AdRepositoryImpl implements AdRepository {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3425a;
    @NonNull
    private final ApiAdResponseCache b;
    @NonNull
    private final AdPresenterCache c;
    @NonNull
    private final AdLoadersRegistry d;
    @NonNull
    private final FlowExecutors e;
    @NonNull
    private Supplier<AdLoader> f;

    private static class AdLoaderListener implements Listener {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final Emitter<? super AdPresenter> f3426a;
        @NonNull
        private final AdLoadersRegistry b;
        @NonNull
        private final AdTypeStrategy c;

        AdLoaderListener(@NonNull Emitter<? super AdPresenter> emitter, @NonNull AdLoadersRegistry adLoadersRegistry, @NonNull AdTypeStrategy adTypeStrategy) {
            this.f3426a = emitter;
            this.b = adLoadersRegistry;
            this.c = adTypeStrategy;
        }

        public void onAdLoadSuccess(@NonNull AdLoader adLoader, @NonNull AdPresenter adPresenter) {
            this.b.unregister(this.c.getUniqueKeyForType(), adLoader);
            this.f3426a.next(adPresenter);
            this.f3426a.complete();
        }

        public void onAdLoadError(@NonNull AdLoader adLoader, @NonNull AdLoaderException adLoaderException) {
            this.b.unregister(this.c.getUniqueKeyForType(), adLoader);
            this.f3426a.error(adLoaderException);
        }
    }

    private static class AdPresenterBuilderListener implements AdPresenterBuilder.Listener {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final Emitter<? super AdPresenter> f3427a;

        AdPresenterBuilderListener(@NonNull Emitter<? super AdPresenter> emitter) {
            this.f3427a = emitter;
        }

        public void onAdPresenterBuildSuccess(@NonNull AdPresenterBuilder adPresenterBuilder, @NonNull AdPresenter adPresenter) {
            this.f3427a.next(adPresenter);
            this.f3427a.complete();
        }

        public void onAdPresenterBuildError(@NonNull AdPresenterBuilder adPresenterBuilder, @NonNull AdPresenterBuilderException adPresenterBuilderException) {
            this.f3427a.error(adPresenterBuilderException);
        }
    }

    AdRepositoryImpl(@NonNull Logger logger, @NonNull ApiAdResponseCache apiAdResponseCache, @NonNull AdPresenterCache adPresenterCache, @NonNull AdLoadersRegistry adLoadersRegistry, @NonNull Supplier<AdLoader> supplier, @NonNull FlowExecutors flowExecutors) {
        this.f3425a = (Logger) Objects.requireNonNull(logger);
        this.b = apiAdResponseCache;
        this.c = (AdPresenterCache) Objects.requireNonNull(adPresenterCache);
        this.d = (AdLoadersRegistry) Objects.requireNonNull(adLoadersRegistry);
        this.f = (Supplier) Objects.requireNonNull(supplier);
        this.e = (FlowExecutors) Objects.requireNonNull(flowExecutors);
    }

    @MainThread
    public final void loadAd(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdRequest adRequest, @NonNull AdRepository.Listener listener) {
        Objects.requireNonNull(adRequest);
        Objects.requireNonNull(adTypeStrategy);
        Objects.requireNonNull(listener);
        CC.fromAction(new Runnable(adTypeStrategy, adRequest, adTypeStrategy.getUniqueKeyForType()) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;
            private final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void run() {
                AdRepositoryImpl.this.a(this.f$1, this.f$2, this.f$3);
            }
        }).concat(new Supplier(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object get() {
                return AdRepositoryImpl.this.c(this.f$1, this.f$2);
            }
        }).switchIfEmpty(new Supplier(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object get() {
                return AdRepositoryImpl.this.b(this.f$1, this.f$2);
            }
        }).switchIfEmpty(new Supplier(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object get() {
                return AdRepositoryImpl.this.a(this.f$1, this.f$2);
            }
        }).subscribeOn(this.e.io()).observeOn(this.e.main()).subscribe((Consumer<T>) new Consumer(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepository.Listener.this.onAdLoadSuccess(this.f$1, (AdPresenter) obj);
            }
        }, (Consumer<Throwable>) new Consumer(listener, adTypeStrategy) {
            private final /* synthetic */ AdRepository.Listener f$1;
            private final /* synthetic */ AdTypeStrategy f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.a(this.f$1, this.f$2, (Throwable) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdRepository.Listener listener, AdTypeStrategy adTypeStrategy, Throwable th) {
        if (th instanceof AdLoaderException) {
            listener.onAdLoadError(adTypeStrategy, (AdLoaderException) th);
            return;
        }
        this.f3425a.error(LogDomain.AD, th.getMessage(), th);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher b(AdTypeStrategy adTypeStrategy, AdRequest adRequest, AdPresenter adPresenter) {
        return a(adTypeStrategy);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher b(AdTypeStrategy adTypeStrategy, AdRequest adRequest) {
        return a(adTypeStrategy);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, AdPresenter adPresenter) {
        return a(adTypeStrategy);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(AdTypeStrategy adTypeStrategy, Emitter emitter) throws Exception {
        AdResponseCacheEntry adResponseCacheEntry = this.b.get(adTypeStrategy.getUniqueKeyForType());
        if (adResponseCacheEntry != null) {
            emitter.next(adResponseCacheEntry.getApiAdResponse());
        }
        emitter.complete();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, Emitter emitter) throws Exception {
        String uniqueKeyForType = adTypeStrategy.getUniqueKeyForType();
        if (this.d.remainingCapacity(uniqueKeyForType) <= 0) {
            this.f3425a.info(LogDomain.CORE, "Ad loading request for provided publisherId and adSpaceId was already in progress, subscribed to that request", new Object[0]);
            return;
        }
        AdLoader adLoader = (AdLoader) this.f.get();
        adLoader.setListener(new AdLoaderListener(emitter, this.d, adTypeStrategy));
        this.d.register(uniqueKeyForType, adLoader);
        adLoader.requestAd(adRequest, adTypeStrategy);
    }

    @NonNull
    private Flow<AdPresenter> a(@NonNull AdTypeStrategy adTypeStrategy) {
        return CC.create(new OnSubscribe(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Emitter emitter) {
                AdRepositoryImpl.this.a(this.f$1, emitter);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdTypeStrategy adTypeStrategy, Emitter emitter) throws Exception {
        String uniqueKeyForType = adTypeStrategy.getUniqueKeyForType();
        AdPresenter adPresenter = this.c.get(uniqueKeyForType);
        if (adPresenter != null && adPresenter.isValid()) {
            emitter.next(adPresenter);
        } else if (this.c.remainingCapacity(uniqueKeyForType) <= 0) {
            emitter.error(new IllegalStateException("Cache is full. Please use the one of previously loaded ADs."));
            return;
        }
        emitter.complete();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, ApiAdResponse apiAdResponse, Emitter emitter) throws Exception {
        ((AdLoader) this.f.get()).buildAdPresenter(adTypeStrategy, adRequest, apiAdResponse, new AdPresenterBuilderListener(emitter));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void c(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdPresenter adPresenter) {
        if (!this.c.put(adTypeStrategy.getUniqueKeyForType(), adPresenter)) {
            throw new IllegalStateException("Cache is full. Please use the one of previously loaded ADs.");
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher a(AdTypeStrategy adTypeStrategy, AdRequest adRequest) {
        return CC.create(new OnSubscribe(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Emitter emitter) {
                AdRepositoryImpl.this.a(this.f$1, this.f$2, emitter);
            }
        }).doOnNext(new Consumer(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.b(this.f$1, (AdPresenter) obj);
            }
        }).flatMap(new Function(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object apply(Object obj) {
                return AdRepositoryImpl.this.a(this.f$1, this.f$2, (AdPresenter) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher c(AdTypeStrategy adTypeStrategy, AdRequest adRequest) {
        return CC.create(new OnSubscribe(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Emitter emitter) {
                AdRepositoryImpl.this.b(this.f$1, emitter);
            }
        }).flatMap(new Function(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object apply(Object obj) {
                return AdRepositoryImpl.this.a(this.f$1, this.f$2, (ApiAdResponse) obj);
            }
        }).doOnNext(new Consumer(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.c(this.f$1, (AdPresenter) obj);
            }
        }).flatMap(new Function(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object apply(Object obj) {
                return AdRepositoryImpl.this.b(this.f$1, this.f$2, (AdPresenter) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, ApiAdResponse apiAdResponse) {
        return CC.create(new OnSubscribe(adTypeStrategy, adRequest, apiAdResponse) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;
            private final /* synthetic */ ApiAdResponse f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Emitter emitter) {
                AdRepositoryImpl.this.a(this.f$1, this.f$2, this.f$3, emitter);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, String str) {
        for (AdResponseCacheEntry adResponseCacheEntry : this.b.trim(adTypeStrategy.getUniqueKeyForType())) {
            ((AdLoader) this.f.get()).reportCacheEntryExpired(adRequest, adResponseCacheEntry.apiAdResponse, adResponseCacheEntry.getRequestTimestamp());
        }
        this.c.trim(str);
    }
}
