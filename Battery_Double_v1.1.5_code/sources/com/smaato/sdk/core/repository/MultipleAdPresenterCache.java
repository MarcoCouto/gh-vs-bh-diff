package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.config.ConfigurationRepository;
import java.util.ArrayList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class MultipleAdPresenterCache implements AdPresenterCache {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ConfigurationRepository f3432a;
    @NonNull
    private final Map<String, CacheHolder> b = new ConcurrentHashMap();

    private static class CacheHolder {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        final Queue<CacheEntry> f3433a;
        @NonNull
        final Queue<CacheEntry> b;

        private CacheHolder() {
            this.f3433a = new ConcurrentLinkedQueue();
            this.b = new ConcurrentLinkedQueue();
        }

        /* synthetic */ CacheHolder(byte b2) {
            this();
        }
    }

    public MultipleAdPresenterCache(@NonNull ConfigurationRepository configurationRepository) {
        this.f3432a = configurationRepository;
    }

    @Nullable
    public final AdPresenter get(@NonNull String str) {
        CacheHolder a2 = a(str);
        CacheEntry cacheEntry = (CacheEntry) a2.b.poll();
        if (cacheEntry == null) {
            for (CacheEntry cacheEntry2 : a2.f3433a) {
                if (cacheEntry2.a()) {
                    a2.b.offer(cacheEntry2);
                }
            }
            cacheEntry = (CacheEntry) a2.b.poll();
        }
        if (cacheEntry != null) {
            return cacheEntry.b();
        }
        return null;
    }

    public final boolean put(@NonNull String str, @NonNull AdPresenter adPresenter) {
        CacheHolder a2 = a(str);
        int i = this.f3432a.get().cachingCapacity;
        if (adPresenter.isValid() && a2.f3433a.size() < i) {
            CacheEntry cacheEntry = new CacheEntry(adPresenter);
            if (a2.f3433a.offer(cacheEntry)) {
                return a2.b.offer(cacheEntry);
            }
        }
        return false;
    }

    public final void trim(@NonNull String str) {
        CacheHolder cacheHolder = (CacheHolder) this.b.get(str);
        if (cacheHolder != null) {
            ArrayList<CacheEntry> arrayList = new ArrayList<>();
            for (CacheEntry cacheEntry : cacheHolder.f3433a) {
                if (cacheEntry.isReadyToBeRemoved()) {
                    arrayList.add(cacheEntry);
                }
            }
            for (CacheEntry cacheEntry2 : arrayList) {
                cacheHolder.f3433a.remove(cacheEntry2);
                cacheHolder.b.remove(cacheEntry2);
            }
        }
    }

    public final int remainingCapacity(@NonNull String str) {
        return this.f3432a.get().cachingCapacity - a(str).f3433a.size();
    }

    public final int perKeyCapacity() {
        return this.f3432a.get().cachingCapacity;
    }

    private CacheHolder a(@NonNull String str) {
        CacheHolder cacheHolder = (CacheHolder) this.b.get(str);
        if (cacheHolder != null) {
            return cacheHolder;
        }
        CacheHolder cacheHolder2 = new CacheHolder(0);
        this.b.put(str, cacheHolder2);
        return cacheHolder2;
    }
}
