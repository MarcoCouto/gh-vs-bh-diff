package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoader;
import com.smaato.sdk.core.config.ConfigurationRepository;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Sets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class MultipleAdLoadersRegistry implements AdLoadersRegistry {

    /* renamed from: a reason: collision with root package name */
    private final ConfigurationRepository f3431a;
    @NonNull
    private final Map<String, Set<AdLoader>> b = new HashMap();
    @NonNull
    private final Logger c;

    public MultipleAdLoadersRegistry(@NonNull ConfigurationRepository configurationRepository, @NonNull Logger logger) {
        this.f3431a = configurationRepository;
        this.c = (Logger) Objects.requireNonNull(logger);
    }

    public final void register(@NonNull String str, @NonNull AdLoader adLoader) {
        synchronized (this.b) {
            Set set = (Set) this.b.get(str);
            if (set == null) {
                set = new HashSet();
            } else {
                int i = this.f3431a.get().cachingCapacity;
                if (set.size() >= i) {
                    this.c.debug(LogDomain.CORE, String.format("Unable to put additional Ad to cache. The capacity of %s entries is exceeded for the key: %s", new Object[]{Integer.valueOf(i), str}), new Object[0]);
                    return;
                }
            }
            set.add(adLoader);
            this.b.put(str, set);
        }
    }

    public final int remainingCapacity(@NonNull String str) {
        int i = this.f3431a.get().cachingCapacity;
        synchronized (this.b) {
            Set set = (Set) this.b.get(str);
            if (set != null) {
                i -= set.size();
            }
        }
        return i;
    }

    public final boolean unregister(@NonNull String str, @NonNull AdLoader adLoader) {
        boolean z;
        synchronized (this.b) {
            Set set = (Set) this.b.get(str);
            z = set != null && set.remove(adLoader);
        }
        return z;
    }

    @NonNull
    public final Set<AdLoader> getAdLoaders(@NonNull String str) {
        Set set;
        synchronized (this.b) {
            set = (Set) this.b.get(str);
        }
        return Sets.toImmutableSet(set);
    }
}
