package com.smaato.sdk.richmedia.mraid.interactor;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.ChangeNotifier.Listener;
import com.smaato.sdk.richmedia.ad.c;
import com.smaato.sdk.richmedia.ad.l;
import com.smaato.sdk.richmedia.mraid.dataprovider.a;
import com.smaato.sdk.richmedia.mraid.dataprovider.b;
import com.smaato.sdk.richmedia.mraid.dataprovider.e;
import com.smaato.sdk.richmedia.mraid.dataprovider.f;
import com.smaato.sdk.richmedia.mraid.dataprovider.g;
import com.smaato.sdk.richmedia.mraid.dataprovider.h;
import com.smaato.sdk.richmedia.mraid.dataprovider.i;
import com.smaato.sdk.richmedia.mraid.interactor.a.C0079a;
import java.util.List;

public final class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final b f3622a;
    @NonNull
    private final StateMachine<com.smaato.sdk.richmedia.mraid.dataprovider.h.a, h.b> b;
    @NonNull
    private final Listener<e> c = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.b((e) obj);
        }
    };
    @NonNull
    private final Listener<com.smaato.sdk.richmedia.mraid.dataprovider.a> d = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.b((a) obj);
        }
    };
    @NonNull
    private final Listener<c> e = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.b((c) obj);
        }
    };
    @NonNull
    private final Listener<Rect> f = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.e((Rect) obj);
        }
    };
    @NonNull
    private final Listener<Rect> g = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.f((Rect) obj);
        }
    };
    @NonNull
    private final Listener<Rect> h = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.g((Rect) obj);
        }
    };
    @NonNull
    private final Listener<Rect> i = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.h((Rect) obj);
        }
    };
    @NonNull
    private final Listener<h.b> j = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.a((h.b) obj);
        }
    };
    @NonNull
    private final Listener<List<String>> k = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.b((List) obj);
        }
    };
    @NonNull
    private final Listener<Boolean> l = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.a((Boolean) obj);
        }
    };
    @NonNull
    private final Listener<l> m = new Listener() {
        public final void onNextValue(Object obj) {
            a.this.b((l) obj);
        }
    };
    @Nullable
    private C0079a n;
    @Nullable
    private String o;

    /* renamed from: com.smaato.sdk.richmedia.mraid.interactor.a$a reason: collision with other inner class name */
    public interface C0079a {
        void a();

        void a(@NonNull Rect rect);

        void a(@NonNull Rect rect, @NonNull Rect rect2);

        void a(@NonNull c cVar);

        void a(@NonNull l lVar);

        void a(@NonNull com.smaato.sdk.richmedia.mraid.dataprovider.a aVar);

        void a(@NonNull e eVar);

        void a(@NonNull f fVar);

        void a(@NonNull h.b bVar);

        void a(@NonNull i iVar);

        void a(@NonNull String str);

        void a(@NonNull String str, @NonNull String str2);

        void a(@NonNull List<String> list);

        void a(boolean z);

        void b();

        void b(@NonNull Rect rect);

        void b(@Nullable String str);

        void c();

        void c(@NonNull Rect rect);

        void d();

        void d(@NonNull Rect rect);
    }

    public a(@NonNull b bVar, @NonNull StateMachine<com.smaato.sdk.richmedia.mraid.dataprovider.h.a, h.b> stateMachine) {
        this.f3622a = (b) Objects.requireNonNull(bVar);
        this.b = (StateMachine) Objects.requireNonNull(stateMachine);
        stateMachine.addListener(new StateMachine.Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                a.this.a((h.b) obj, (h.b) obj2, metadata);
            }
        });
        this.f3622a.a().addListener(this.d);
        this.f3622a.b().addListener(this.c);
        this.f3622a.d().addListener(this.f);
        this.f3622a.c().addListener(this.g);
        this.f3622a.f().addListener(this.h);
        this.f3622a.e().addListener(this.i);
        this.f3622a.g().addListener(this.e);
        this.f3622a.h().addListener(this.j);
        this.f3622a.k().addListener(this.k);
        this.f3622a.l().addListener(this.l);
        this.f3622a.i().addListener(this.m);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void g(C0079a aVar) {
        Rect rect = (Rect) this.f3622a.e().getValue();
        aVar.a((Rect) this.f3622a.d().getValue(), new Rect(0, 0, rect.width(), rect.height()));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void f(C0079a aVar) {
        aVar.b(this.o);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull h.b bVar) {
        Objects.onNotNull(this.n, new Consumer() {
            public final void accept(Object obj) {
                ((C0079a) obj).a(h.b.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull e eVar) {
        Objects.onNotNull(this.n, new Consumer() {
            public final void accept(Object obj) {
                ((C0079a) obj).a(e.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull com.smaato.sdk.richmedia.mraid.dataprovider.a aVar) {
        Objects.onNotNull(this.n, new Consumer() {
            public final void accept(Object obj) {
                ((C0079a) obj).a(a.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull c cVar) {
        Objects.onNotNull(this.n, new Consumer() {
            public final void accept(Object obj) {
                ((C0079a) obj).a(c.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void e(@NonNull Rect rect) {
        Objects.onNotNull(this.n, new Consumer(rect) {
            private final /* synthetic */ Rect f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((C0079a) obj).a(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void f(@NonNull Rect rect) {
        Objects.onNotNull(this.n, new Consumer(rect) {
            private final /* synthetic */ Rect f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((C0079a) obj).b(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void g(@NonNull Rect rect) {
        Objects.onNotNull(this.n, new Consumer(rect) {
            private final /* synthetic */ Rect f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((C0079a) obj).c(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void h(@NonNull Rect rect) {
        Objects.onNotNull(this.n, new Consumer(rect) {
            private final /* synthetic */ Rect f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((C0079a) obj).d(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull l lVar) {
        Objects.onNotNull(this.n, new Consumer() {
            public final void accept(Object obj) {
                ((C0079a) obj).a(l.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Boolean bool) {
        Objects.onNotNull(this.n, new Consumer(bool) {
            private final /* synthetic */ Boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((C0079a) obj).a(this.f$0.booleanValue());
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull List<String> list) {
        Objects.onNotNull(this.n, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((C0079a) obj).a(this.f$0);
            }
        });
    }

    public final void a(@Nullable C0079a aVar) {
        this.n = aVar;
    }

    public final void a(@Nullable String str) {
        if ("audioVolumeChange".equalsIgnoreCase(str)) {
            b((c) this.f3622a.g().getValue());
        }
        if ("exposureChange".equalsIgnoreCase(str)) {
            b((e) this.f3622a.b().getValue());
        }
    }

    public final void b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            Objects.onNotNull(this.n, $$Lambda$a$JepoeLGVNvpjBDDXMTVxw7nUXE.INSTANCE);
        } else {
            Objects.onNotNull(this.n, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((C0079a) obj).a(this.f$0);
                }
            });
        }
    }

    public final void c(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            Objects.onNotNull(this.n, $$Lambda$a$L__wJT7VoFFGZxrucbeIaR2bA34.INSTANCE);
        } else {
            Objects.onNotNull(this.n, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((C0079a) obj).a(this.f$0);
                }
            });
        }
    }

    public final void a(@Nullable g gVar) {
        if (gVar == null) {
            Objects.onNotNull(this.n, $$Lambda$a$ySfNEqpMsbkEhCG9A7WiWizMlc.INSTANCE);
        } else if (this.b.getCurrentState() == h.b.EXPANDED) {
            Objects.onNotNull(this.n, $$Lambda$a$pnRQL5wXI3x8fnXoeiFTypFdgYA.INSTANCE);
        } else {
            this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.RESIZE);
        }
    }

    public final void a(@NonNull e eVar) {
        this.f3622a.b().newValue(eVar);
    }

    public final void a(@NonNull com.smaato.sdk.richmedia.mraid.dataprovider.a aVar) {
        this.f3622a.a().newValue(aVar);
    }

    public final void a(@NonNull Rect rect) {
        this.f3622a.f().newValue(rect);
    }

    public final void b(@NonNull Rect rect) {
        this.f3622a.e().newValue(rect);
    }

    public final void a(@NonNull f fVar) {
        boolean z = false;
        boolean z2 = this.b.getCurrentState() == h.b.EXPANDED;
        if (this.f3622a.j() == i.INTERSTITIAL) {
            z = true;
        }
        if (z2 || z) {
            Objects.onNotNull(this.n, new Consumer() {
                public final void accept(Object obj) {
                    ((C0079a) obj).a(f.this);
                }
            });
        }
    }

    public final void a(@NonNull c cVar) {
        this.f3622a.g().newValue(cVar);
    }

    public final void d(@Nullable String str) {
        if (this.f3622a.j() != i.INTERSTITIAL) {
            this.o = str;
            this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.EXPAND);
        }
    }

    public final void c(@NonNull Rect rect) {
        this.f3622a.c().newValue(i(rect));
    }

    public final void d(@NonNull Rect rect) {
        this.f3622a.d().newValue(i(rect));
    }

    public final void a(boolean z) {
        this.f3622a.l().newValue(Boolean.valueOf(z));
    }

    @NonNull
    private Rect i(@NonNull Rect rect) {
        Rect rect2 = (Rect) this.f3622a.e().getValue();
        Rect rect3 = (Rect) this.f3622a.f().getValue();
        int abs = Math.abs(rect3.left - rect2.left);
        int abs2 = Math.abs(rect3.top - rect2.top);
        return new Rect(rect.left - abs, rect.top - abs2, rect.right - abs, rect.bottom - abs2);
    }

    public final void a(@NonNull List<String> list) {
        this.f3622a.k().newValue(list);
    }

    public final void a(@NonNull l lVar) {
        this.f3622a.i().newValue(lVar);
    }

    public final void b() {
        Objects.onNotNull(this.n, $$Lambda$VsgMZmvpGi5X09K_MJT3IQACS9U.INSTANCE);
        this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.RESIZING_FINISHED);
    }

    public final void c() {
        Objects.onNotNull(this.n, $$Lambda$VsgMZmvpGi5X09K_MJT3IQACS9U.INSTANCE);
        this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.EXPANDING_FINISHED);
    }

    public final void e(@NonNull String str) {
        Objects.onNotNull(this.n, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((C0079a) obj).a("resize", this.f$0);
            }
        });
        if (this.b.getCurrentState() == h.b.RESIZED) {
            Objects.onNotNull(this.n, $$Lambda$a8edez6xVzqFLsH9XD5zDWYrSh0.INSTANCE);
        }
        this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.ERROR);
    }

    public final void d() {
        this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.ERROR);
    }

    public final void e() {
        boolean z = false;
        boolean z2 = this.b.getCurrentState() == h.b.EXPANDED;
        if (this.f3622a.j() == i.INTERSTITIAL) {
            z = true;
        }
        if (z2 || z) {
            Objects.onNotNull(this.n, $$Lambda$tYXtTsxkE27KcAzrkv1tUCJPJ6A.INSTANCE);
        }
        this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.CLOSE);
    }

    public final void f() {
        Objects.onNotNull(this.n, $$Lambda$VsgMZmvpGi5X09K_MJT3IQACS9U.INSTANCE);
        this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.CLOSE_FINISHED);
    }

    public final void g() {
        if (this.b.isTransitionAllowed(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.VISIBILITY_PARAMS_CHECK)) {
            Objects.onNotNull(this.n, $$Lambda$VsgMZmvpGi5X09K_MJT3IQACS9U.INSTANCE);
        }
    }

    public final void a() {
        b((com.smaato.sdk.richmedia.mraid.dataprovider.a) this.f3622a.a().getValue());
        g((Rect) this.f3622a.f().getValue());
        h((Rect) this.f3622a.e().getValue());
        b((l) this.f3622a.i().getValue());
        Objects.onNotNull(this.n, new Consumer() {
            public final void accept(Object obj) {
                ((C0079a) obj).a(i.this);
            }
        });
        b((List) this.f3622a.k().getValue());
        b((c) this.f3622a.g().getValue());
        Objects.onNotNull(this.n, $$Lambda$VsgMZmvpGi5X09K_MJT3IQACS9U.INSTANCE);
        this.b.onEvent(com.smaato.sdk.richmedia.mraid.dataprovider.h.a.LOAD_COMPLETE);
        Objects.onNotNull(this.n, $$Lambda$Orqa5dGHiyFAFiU3GgQ2iL9PUh0.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(h.b bVar, h.b bVar2, Metadata metadata) {
        switch (bVar2) {
            case RESIZE_IN_PROGRESS:
                Objects.onNotNull(this.n, new Consumer() {
                    public final void accept(Object obj) {
                        a.this.g((C0079a) obj);
                    }
                });
                return;
            case EXPAND_IN_PROGRESS:
                Objects.onNotNull(this.n, new Consumer() {
                    public final void accept(Object obj) {
                        a.this.f((C0079a) obj);
                    }
                });
                this.o = null;
                return;
            case CLOSE_IN_PROGRESS:
                Objects.onNotNull(this.n, $$Lambda$a8edez6xVzqFLsH9XD5zDWYrSh0.INSTANCE);
                return;
            default:
                this.f3622a.h().newValue(bVar2);
                return;
        }
    }
}
