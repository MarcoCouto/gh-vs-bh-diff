package com.smaato.sdk.richmedia.mraid.presenter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.mvp.c;
import com.smaato.sdk.richmedia.widget.d;

public interface a extends c<d> {
    void a(@Nullable BiConsumer<String, com.smaato.sdk.richmedia.mraid.dataprovider.d> biConsumer);

    void a(@NonNull String str, boolean z);

    void a_(@Nullable Consumer<String> consumer);

    void b(@Nullable BiConsumer<String, String> biConsumer);

    void b(@Nullable Consumer<Whatever> consumer);

    void c(@Nullable Consumer<Whatever> consumer);

    void c(@NonNull String str);

    void d(@Nullable Consumer<d> consumer);

    void g();

    void h();

    void i();

    void j();

    void k();

    void l();
}
