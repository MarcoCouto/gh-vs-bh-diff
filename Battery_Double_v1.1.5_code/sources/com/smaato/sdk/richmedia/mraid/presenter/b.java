package com.smaato.sdk.richmedia.mraid.presenter;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.PermissionChecker;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.ViewUtils;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.ChangeNotifier.Listener;
import com.smaato.sdk.richmedia.ad.l;
import com.smaato.sdk.richmedia.mraid.bridge.c;
import com.smaato.sdk.richmedia.mraid.bridge.e;
import com.smaato.sdk.richmedia.mraid.bridge.f;
import com.smaato.sdk.richmedia.mraid.dataprovider.g;
import com.smaato.sdk.richmedia.mraid.dataprovider.i;
import com.smaato.sdk.richmedia.mraid.interactor.a.C0079a;
import com.smaato.sdk.richmedia.mraid.mvp.a;
import com.smaato.sdk.richmedia.widget.d;
import java.util.List;

public class b extends a<d> implements a, a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final com.smaato.sdk.richmedia.mraid.interactor.a f3625a;
    @NonNull
    private final f b;
    @NonNull
    private final c c;
    @NonNull
    private final com.smaato.sdk.richmedia.mraid.bridge.d d;
    @NonNull
    private final e e;
    @NonNull
    private final com.smaato.sdk.richmedia.mraid.b f;
    @NonNull
    private final com.smaato.sdk.richmedia.framework.b g;
    @NonNull
    private final Listener<com.smaato.sdk.richmedia.mraid.dataprovider.f> h;
    @NonNull
    private final c i;
    @NonNull
    private final AppBackgroundDetector j;
    @NonNull
    private final PermissionChecker k;
    @NonNull
    private final LocationProvider l;
    @Nullable
    private BiConsumer<String, com.smaato.sdk.richmedia.mraid.dataprovider.d> m;
    @Nullable
    private Consumer<Whatever> n;
    @Nullable
    private Consumer<String> o;
    @Nullable
    private Consumer<d> p;
    @Nullable
    private Consumer<Whatever> q;
    @Nullable
    private BiConsumer<String, String> r;
    @Nullable
    private Listener<Whatever> s;
    @NonNull
    private final C0079a t = this;
    @NonNull
    private final AppBackgroundDetector.Listener u = new AppBackgroundDetector.Listener() {
        public final void onAppEnteredInBackground() {
            b.this.n();
        }

        public final void onAppEnteredInForeground() {
            b.this.m();
        }
    };

    /* JADX WARNING: type inference failed for: r1v0, types: [com.smaato.sdk.richmedia.mraid.presenter.b, com.smaato.sdk.richmedia.mraid.interactor.a$a] */
    public b(@NonNull com.smaato.sdk.richmedia.mraid.interactor.a aVar, @NonNull f fVar, @NonNull c cVar, @NonNull com.smaato.sdk.richmedia.mraid.bridge.d dVar, @NonNull e eVar, @NonNull com.smaato.sdk.richmedia.mraid.b bVar, @NonNull com.smaato.sdk.richmedia.framework.b bVar2, @NonNull c cVar2, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull PermissionChecker permissionChecker, @NonNull LocationProvider locationProvider) {
        this.f3625a = (com.smaato.sdk.richmedia.mraid.interactor.a) Objects.requireNonNull(aVar);
        this.b = (f) Objects.requireNonNull(fVar);
        this.c = (c) Objects.requireNonNull(cVar);
        this.d = (com.smaato.sdk.richmedia.mraid.bridge.d) Objects.requireNonNull(dVar);
        this.e = (e) Objects.requireNonNull(eVar);
        this.f = (com.smaato.sdk.richmedia.mraid.b) Objects.requireNonNull(bVar);
        this.g = (com.smaato.sdk.richmedia.framework.b) Objects.requireNonNull(bVar2);
        this.i = (c) Objects.requireNonNull(cVar2);
        this.j = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.k = (PermissionChecker) Objects.requireNonNull(permissionChecker);
        this.l = (LocationProvider) Objects.requireNonNull(locationProvider);
        aVar.getClass();
        this.h = new Listener() {
            public final void onNextValue(Object obj) {
                com.smaato.sdk.richmedia.mraid.interactor.a.this.a((com.smaato.sdk.richmedia.mraid.dataprovider.f) obj);
            }
        };
        appBackgroundDetector.addListener(this.u, false);
        this.f3625a.a(this.t);
        e eVar2 = this.e;
        C0079a aVar2 = this.t;
        aVar2.getClass();
        eVar2.a((com.smaato.sdk.richmedia.mraid.bridge.a) new com.smaato.sdk.richmedia.mraid.bridge.a() {
            public final void onError(String str, String str2) {
                C0079a.this.a(str, str2);
            }
        });
        com.smaato.sdk.richmedia.mraid.bridge.d dVar2 = this.d;
        com.smaato.sdk.richmedia.mraid.interactor.a aVar3 = this.f3625a;
        aVar3.getClass();
        dVar2.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                com.smaato.sdk.richmedia.mraid.interactor.a.this.a((String) obj);
            }
        });
        com.smaato.sdk.richmedia.mraid.bridge.d dVar3 = this.d;
        com.smaato.sdk.richmedia.mraid.interactor.a aVar4 = this.f3625a;
        aVar4.getClass();
        dVar3.b((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                com.smaato.sdk.richmedia.mraid.interactor.a.this.b((String) obj);
            }
        });
        this.d.d(new Consumer() {
            public final void accept(Object obj) {
                b.this.d((Whatever) obj);
            }
        });
        com.smaato.sdk.richmedia.mraid.bridge.d dVar4 = this.d;
        com.smaato.sdk.richmedia.mraid.interactor.a aVar5 = this.f3625a;
        aVar5.getClass();
        dVar4.e(new Consumer() {
            public final void accept(Object obj) {
                com.smaato.sdk.richmedia.mraid.interactor.a.this.d((String) obj);
            }
        });
        this.d.f(new Consumer() {
            public final void accept(Object obj) {
                b.this.c((Whatever) obj);
            }
        });
        com.smaato.sdk.richmedia.mraid.bridge.d dVar5 = this.d;
        com.smaato.sdk.richmedia.mraid.interactor.a aVar6 = this.f3625a;
        aVar6.getClass();
        dVar5.c((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                com.smaato.sdk.richmedia.mraid.interactor.a.this.c((String) obj);
            }
        });
        this.d.g(new Consumer() {
            public final void accept(Object obj) {
                b.this.b((Whatever) obj);
            }
        });
        this.d.a((BiConsumer<String, String>) new BiConsumer() {
            public final void accept(Object obj, Object obj2) {
                b.this.b((String) obj, (String) obj2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Whatever whatever) {
        this.f3625a.a(this.e.b());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Whatever whatever) {
        Objects.onNotNull(this.n, new Consumer() {
            public final void accept(Object obj) {
                ((Consumer) obj).accept(Whatever.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Whatever whatever) {
        this.f3625a.e();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(String str, String str2) {
        Objects.onNotNull(this.r, new Consumer(str, str2) {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((BiConsumer) obj).accept(this.f$0, this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull d dVar) {
        super.a(dVar);
        this.f3625a.a(com.smaato.sdk.richmedia.ad.c.a(dVar.getContext(), (WebView) dVar.c(), this.k));
        com.smaato.sdk.richmedia.mraid.b bVar = this.f;
        com.smaato.sdk.richmedia.mraid.interactor.a aVar = this.f3625a;
        aVar.getClass();
        bVar.a((Runnable) new Runnable() {
            public final void run() {
                com.smaato.sdk.richmedia.mraid.interactor.a.this.g();
            }
        });
        m();
    }

    public final void e() {
        super.e();
        this.f.a();
        n();
    }

    public final void f() {
        this.j.deleteListener(this.u);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(d dVar) {
        Context context = dVar.getContext();
        com.smaato.sdk.richmedia.widget.e c2 = dVar.c();
        this.f3625a.c(com.smaato.sdk.richmedia.ad.c.a(context, com.smaato.sdk.richmedia.mraid.c.a(dVar)));
        this.f3625a.d(com.smaato.sdk.richmedia.ad.c.a(context, com.smaato.sdk.richmedia.mraid.c.a(c2)));
        com.smaato.sdk.richmedia.mraid.c.a b2 = com.smaato.sdk.richmedia.mraid.c.b(c2);
        com.smaato.sdk.richmedia.mraid.dataprovider.e a2 = com.smaato.sdk.richmedia.mraid.dataprovider.e.a(b2.b, com.smaato.sdk.richmedia.ad.c.a(context, b2.f3607a));
        this.f3625a.a(a2);
        this.f3625a.a(a2.b > 0.0f);
        this.f3625a.a(com.smaato.sdk.richmedia.ad.c.a(context));
        this.f3625a.a(l.a(this.l));
    }

    public final void g() {
        this.f3625a.a();
    }

    public final void a(@NonNull String str, boolean z) {
        this.b.a(str, z);
    }

    public final void a_(@Nullable Consumer<String> consumer) {
        this.o = consumer;
    }

    public final void a(@Nullable BiConsumer<String, com.smaato.sdk.richmedia.mraid.dataprovider.d> biConsumer) {
        this.m = biConsumer;
    }

    public final void h() {
        this.f3625a.e();
    }

    public final void c(@Nullable Consumer<Whatever> consumer) {
        this.q = consumer;
    }

    public final void b(@Nullable Consumer<Whatever> consumer) {
        this.n = consumer;
    }

    public final void d(@Nullable Consumer<d> consumer) {
        this.p = consumer;
    }

    public final void i() {
        this.f3625a.b();
    }

    public final void c(@NonNull String str) {
        this.f3625a.e(str);
    }

    public final void j() {
        this.f3625a.d();
    }

    public final void k() {
        this.f3625a.c();
    }

    public final void l() {
        this.f3625a.f();
    }

    public final void b(@Nullable BiConsumer<String, String> biConsumer) {
        this.r = biConsumer;
    }

    /* access modifiers changed from: private */
    public void m() {
        this.g.a();
        this.s = new Listener() {
            public final void onNextValue(Object obj) {
                b.this.a((Whatever) obj);
            }
        };
        this.g.c().addListener(this.s);
        this.e.a().addListener(this.h);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(d dVar) {
        Context context = dVar.getContext();
        this.f3625a.a(com.smaato.sdk.richmedia.mraid.dataprovider.a.a(context));
        Size displaySizeInDp = UIUtils.getDisplaySizeInDp(context);
        Rect rect = new Rect(0, 0, displaySizeInDp.width, displaySizeInDp.height);
        this.f3625a.a(rect);
        View rootView = ViewUtils.getRootView(dVar);
        if (rootView == null) {
            this.f3625a.b(rect);
            return;
        }
        this.f3625a.b(com.smaato.sdk.richmedia.ad.c.a(context, com.smaato.sdk.richmedia.mraid.c.a(rootView)));
    }

    /* access modifiers changed from: private */
    public void n() {
        this.g.b();
        this.g.c().removeListener(this.s);
        this.e.a().removeListener(this.h);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Whatever whatever) {
        a((Consumer<T>) new Consumer() {
            public final void accept(Object obj) {
                b.this.b((d) obj);
            }
        });
    }

    public void a(com.smaato.sdk.richmedia.mraid.dataprovider.e eVar) {
        this.c.a(eVar);
    }

    public void a(com.smaato.sdk.richmedia.mraid.dataprovider.a aVar) {
        this.e.a(aVar);
    }

    public void a(com.smaato.sdk.richmedia.mraid.dataprovider.h.b bVar) {
        this.c.a(bVar);
    }

    public void a(com.smaato.sdk.richmedia.mraid.dataprovider.f fVar) {
        a((Consumer<T>) new Consumer(fVar) {
            private final /* synthetic */ com.smaato.sdk.richmedia.mraid.dataprovider.f f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                b.this.a(this.f$1, (d) obj);
            }
        });
    }

    public void a() {
        a((Consumer<T>) new Consumer() {
            public final void accept(Object obj) {
                b.this.d((d) obj);
            }
        });
    }

    public void b() {
        Objects.onNotNull(this.q, $$Lambda$b$Hk4HBW3wuqdfdZdXWdr6YV9KhOY.INSTANCE);
    }

    public void a(Rect rect) {
        this.e.a(rect);
        this.c.a(rect);
    }

    public void b(Rect rect) {
        this.e.b(rect);
    }

    public void c(Rect rect) {
        this.e.b(new Size(rect.width(), rect.height()));
    }

    public void d(Rect rect) {
        this.e.a(new Size(rect.width(), rect.height()));
    }

    public void a(String str) {
        Objects.onNotNull(this.o, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Consumer) obj).accept(this.f$0);
            }
        });
    }

    public void a(String str, String str2) {
        this.c.a(str, str2);
    }

    public void b(String str) {
        a((Consumer<T>) new Consumer((com.smaato.sdk.richmedia.mraid.dataprovider.f) this.e.a().getValue(), str) {
            private final /* synthetic */ com.smaato.sdk.richmedia.mraid.dataprovider.f f$1;
            private final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                b.this.a(this.f$1, this.f$2, (d) obj);
            }
        });
    }

    public void a(Rect rect, Rect rect2) {
        g b2 = this.e.b();
        if (b2 == null) {
            this.f3625a.e("Resize properties should be set before resize");
        } else {
            a((Consumer<T>) new Consumer(b2.a(rect, rect2), rect2) {
                private final /* synthetic */ Rect f$1;
                private final /* synthetic */ Rect f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    b.this.a(this.f$1, this.f$2, (d) obj);
                }
            });
        }
    }

    public void a(com.smaato.sdk.richmedia.ad.c cVar) {
        this.c.a(cVar);
    }

    public void a(l lVar) {
        this.e.a(lVar);
    }

    public void a(i iVar) {
        this.e.a(iVar);
    }

    public void a(List<String> list) {
        this.e.a(list);
    }

    public void a(boolean z) {
        this.c.a(z);
    }

    public void c() {
        a((Consumer<T>) new Consumer() {
            public final void accept(Object obj) {
                b.this.c((d) obj);
            }
        });
    }

    public void d() {
        this.b.a();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Rect rect, Rect rect2, d dVar) {
        Context context = dVar.getContext();
        Rect b2 = com.smaato.sdk.richmedia.ad.c.b(context, rect);
        Objects.onNotNull(this.p, new Consumer(com.smaato.sdk.richmedia.ad.c.b(context, rect2), b2) {
            private final /* synthetic */ Rect f$0;
            private final /* synthetic */ Rect f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((Consumer) obj).accept(new d(this.f$0, this.f$1));
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(com.smaato.sdk.richmedia.mraid.dataprovider.f fVar, String str, d dVar) {
        this.i.a(dVar.getContext(), fVar);
        Objects.onNotNull(this.m, new Consumer(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                b.this.a(this.f$1, (BiConsumer) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str, BiConsumer biConsumer) {
        biConsumer.accept(str, this.e.c());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(d dVar) {
        this.i.a(dVar.getContext());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(com.smaato.sdk.richmedia.mraid.dataprovider.f fVar, d dVar) {
        this.i.a(dVar.getContext(), fVar);
    }
}
