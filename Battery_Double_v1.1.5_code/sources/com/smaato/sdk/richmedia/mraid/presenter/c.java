package com.smaato.sdk.richmedia.mraid.presenter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.richmedia.mraid.dataprovider.f;
import com.smaato.sdk.richmedia.util.a;
import com.smaato.sdk.richmedia.util.b;

public final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3627a;
    @NonNull
    private final a b;
    @Nullable
    private Integer c;

    public c(@NonNull Logger logger, @NonNull a aVar) {
        this.f3627a = logger;
        this.b = aVar;
    }

    /* access modifiers changed from: 0000 */
    @MainThread
    public final void a(@NonNull Context context, @NonNull f fVar) {
        Threads.ensureMainThread();
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            boolean z = false;
            if (a.b(activity)) {
                this.f3627a.error(LogDomain.MRAID, "Won't apply orientation properties. Reason: Activity is locked", new Object[0]);
            } else if (a.a(activity)) {
                this.f3627a.error(LogDomain.MRAID, "Won't apply orientation properties. Reason: Activity might be destroyed on orientation change", new Object[0]);
            } else {
                b.a aVar = fVar.b;
                if (aVar == b.a.PORTRAIT || aVar == b.a.LANDSCAPE) {
                    z = true;
                }
                if (z) {
                    a(activity, aVar);
                } else if (fVar.f3615a) {
                    activity.setRequestedOrientation(-1);
                } else {
                    a(activity, b.a(context));
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @MainThread
    public final void a(@NonNull Context context) {
        Threads.ensureMainThread();
        if (this.c != null && (context instanceof Activity)) {
            ((Activity) context).setRequestedOrientation(this.c.intValue());
            this.c = null;
        }
    }

    private void a(@NonNull Activity activity, @NonNull b.a aVar) {
        this.c = Integer.valueOf(activity.getRequestedOrientation());
        activity.setRequestedOrientation(b.a(aVar));
    }
}
