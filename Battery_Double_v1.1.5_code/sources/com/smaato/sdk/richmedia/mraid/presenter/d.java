package com.smaato.sdk.richmedia.mraid.presenter;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public final class d {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Rect f3628a;
    @NonNull
    public final Rect b;

    d(@NonNull Rect rect, @NonNull Rect rect2) {
        this.f3628a = (Rect) Objects.requireNonNull(rect);
        this.b = (Rect) Objects.requireNonNull(rect2);
    }
}
