package com.smaato.sdk.richmedia.mraid;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.ViewUtils;

public final class c {

    public static final class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final Rect f3607a;
        public final float b;

        /* synthetic */ a(float f, Rect rect, byte b2) {
            this(f, rect);
        }

        private a(float f, @NonNull Rect rect) {
            this.b = f;
            this.f3607a = rect;
        }
    }

    @NonNull
    public static Rect a(@NonNull View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        return new Rect(i, i2, view.getWidth() + i, view.getHeight() + i2);
    }

    private static boolean c(@NonNull View view) {
        if (view.hasWindowFocus() && view.getWidth() > 0 && view.getHeight() > 0) {
            return view.isShown();
        }
        return false;
    }

    public static void a(@NonNull final View view, @NonNull final Runnable runnable) {
        view.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            public final boolean onPreDraw() {
                view.getViewTreeObserver().removeOnPreDrawListener(this);
                runnable.run();
                return true;
            }
        });
    }

    @NonNull
    public static a b(@NonNull View view) {
        Rect rect;
        float f;
        Rect rect2;
        View rootView = ViewUtils.getRootView(view);
        if (rootView == null) {
            rect = new Rect();
        } else {
            Rect a2 = a(rootView);
            if (!c(view)) {
                rect2 = new Rect();
            } else {
                rect2 = new Rect();
                if (!view.getGlobalVisibleRect(rect2)) {
                    rect2 = new Rect();
                }
            }
            rect2.offset(-a2.left, -a2.top);
            rect = rect2;
        }
        float width = (float) (rect.width() * rect.height());
        if (!c(view)) {
            f = 0.0f;
        } else {
            f = 100.0f * (width / ((float) (view.getWidth() * view.getHeight())));
        }
        return new a(f, rect, 0);
    }
}
