package com.smaato.sdk.richmedia.mraid.mvp;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.mvp.b;
import java.lang.ref.WeakReference;

public abstract class a<T extends b> implements c<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private WeakReference<T> f3624a = new WeakReference<>(null);

    @CallSuper
    public void a(@NonNull T t) {
        Threads.ensureMainThread();
        this.f3624a = new WeakReference<>(t);
    }

    @CallSuper
    public void e() {
        Threads.ensureMainThread();
        this.f3624a.clear();
    }

    /* access modifiers changed from: protected */
    public final void a(@NonNull Consumer<T> consumer) {
        Objects.requireNonNull(consumer);
        Threads.ensureMainThread();
        b bVar = (b) this.f3624a.get();
        if (bVar != null) {
            consumer.accept(bVar);
        }
    }
}
