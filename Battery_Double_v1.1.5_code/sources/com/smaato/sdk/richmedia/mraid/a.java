package com.smaato.sdk.richmedia.mraid;

import android.content.Context;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.PermissionChecker;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.richmedia.ad.c;
import com.smaato.sdk.richmedia.framework.b;
import com.smaato.sdk.richmedia.mraid.bridge.f;
import com.smaato.sdk.richmedia.mraid.dataprovider.h;
import com.smaato.sdk.richmedia.mraid.dataprovider.i;
import com.smaato.sdk.richmedia.widget.d;
import com.smaato.sdk.richmedia.widget.e;
import java.util.List;

public final class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final PermissionChecker f3598a;
    @NonNull
    private final AppBackgroundAwareHandler b;
    @NonNull
    private final b c;
    @NonNull
    private final AppBackgroundDetector d;
    @NonNull
    private final Logger e;
    @NonNull
    private final LocationProvider f;
    @NonNull
    private final h g;

    public a(@NonNull PermissionChecker permissionChecker, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler, @NonNull b bVar, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull Logger logger, @NonNull LocationProvider locationProvider, @NonNull h hVar) {
        this.f3598a = (PermissionChecker) Objects.requireNonNull(permissionChecker);
        this.b = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
        this.c = (b) Objects.requireNonNull(bVar);
        this.d = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.f = (LocationProvider) Objects.requireNonNull(locationProvider);
        this.e = (Logger) Objects.requireNonNull(logger);
        this.g = (h) Objects.requireNonNull(hVar);
    }

    @NonNull
    public final d a(@NonNull Context context, @NonNull com.smaato.sdk.richmedia.ad.h hVar, @NonNull com.smaato.sdk.richmedia.widget.d.a aVar, @NonNull com.smaato.sdk.richmedia.util.d dVar) {
        e a2 = a(context, dVar);
        return d.a(this.e, context, hVar, aVar, dVar, a2, a(a2, this.g.a(), i.INLINE));
    }

    @NonNull
    public final d b(@NonNull Context context, @NonNull com.smaato.sdk.richmedia.ad.h hVar, @NonNull com.smaato.sdk.richmedia.widget.d.a aVar, @NonNull com.smaato.sdk.richmedia.util.d dVar) {
        e a2 = a(context, dVar);
        return d.a(this.e, context, hVar, aVar, dVar, a2, a(a2, this.g.b(), i.INTERSTITIAL));
    }

    @NonNull
    private e a(@NonNull Context context, @NonNull com.smaato.sdk.richmedia.util.d dVar) {
        return new e(context, this.e, dVar);
    }

    @VisibleForTesting
    @NonNull
    private com.smaato.sdk.richmedia.mraid.presenter.a a(@NonNull WebView webView, @NonNull StateMachine<com.smaato.sdk.richmedia.mraid.dataprovider.h.a, h.b> stateMachine, @NonNull i iVar) {
        WebView webView2 = webView;
        Context context = webView.getContext();
        com.smaato.sdk.richmedia.mraid.interactor.a a2 = a(context, stateMachine, iVar, c.a(context, webView2, this.f3598a));
        f fVar = new f(webView2, this.e);
        com.smaato.sdk.richmedia.mraid.presenter.b bVar = new com.smaato.sdk.richmedia.mraid.presenter.b(a2, fVar, new com.smaato.sdk.richmedia.mraid.bridge.c(this.e, fVar), new com.smaato.sdk.richmedia.mraid.bridge.d(fVar), new com.smaato.sdk.richmedia.mraid.bridge.e(this.e, fVar), a(this.e), this.c, new com.smaato.sdk.richmedia.mraid.presenter.c(this.e, new com.smaato.sdk.richmedia.util.a()), this.d, this.f3598a, this.f);
        return bVar;
    }

    @NonNull
    private com.smaato.sdk.richmedia.mraid.interactor.a a(@NonNull Context context, @NonNull StateMachine<com.smaato.sdk.richmedia.mraid.dataprovider.h.a, h.b> stateMachine, @NonNull i iVar, @NonNull List<String> list) {
        com.smaato.sdk.richmedia.mraid.dataprovider.b bVar = new com.smaato.sdk.richmedia.mraid.dataprovider.b(context, iVar, (h.b) stateMachine.getCurrentState(), this.f, list);
        return new com.smaato.sdk.richmedia.mraid.interactor.a(bVar, stateMachine);
    }

    @NonNull
    private b a(@NonNull Logger logger) {
        return new b(logger, this.b, 200);
    }
}
