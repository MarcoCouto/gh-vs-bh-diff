package com.smaato.sdk.richmedia.mraid;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.atomic.AtomicReference;

public final class b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3599a;
    @NonNull
    private final AppBackgroundAwareHandler b;
    @NonNull
    private final Runnable c = new Runnable() {
        public final void run() {
            Objects.onNotNull(b.this.d.get(), new Consumer() {
                public final void accept(Object obj) {
                    AnonymousClass1.this.a((Runnable) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Runnable runnable) {
            runnable.run();
            b.this.b();
        }
    };
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<Runnable> d = new AtomicReference<>();
    private final long e;

    b(@NonNull Logger logger, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler, long j) {
        this.f3599a = (Logger) Objects.requireNonNull(logger);
        this.b = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
        this.e = 200;
    }

    /* access modifiers changed from: private */
    public void b() {
        this.b.postDelayed("Repeatable action timer", this.c, this.e, null);
    }

    public final void a() {
        this.b.stop();
        this.d.set(null);
    }

    public final void a(@Nullable Runnable runnable) {
        if (!(this.d.get() != null)) {
            if (runnable == null) {
                this.f3599a.info(LogDomain.MRAID, "No action to schedule", new Object[0]);
                return;
            }
            this.d.set(runnable);
            b();
        }
    }
}
