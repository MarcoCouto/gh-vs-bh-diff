package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.richmedia.ad.c;
import java.util.ArrayList;
import java.util.Map;

public final class d {

    /* renamed from: a reason: collision with root package name */
    private int f3612a;
    private int b;
    private boolean c;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private Integer f3613a;
        @Nullable
        private Integer b;

        public a(@NonNull Map<String, String> map) {
            Integer num;
            String str = (String) map.get("width");
            Integer num2 = null;
            if (!TextUtils.isEmpty(str)) {
                Float a2 = c.a(str);
                if (a2 == null) {
                    num = null;
                } else {
                    num = Integer.valueOf(a2.intValue());
                }
                this.f3613a = num;
            }
            String str2 = (String) map.get("height");
            if (!TextUtils.isEmpty(str2)) {
                Float a3 = c.a(str2);
                if (a3 != null) {
                    num2 = Integer.valueOf(a3.intValue());
                }
                this.b = num2;
            }
        }

        @NonNull
        public final d a() throws com.smaato.sdk.richmedia.mraid.exception.a {
            ArrayList arrayList = new ArrayList();
            if (this.f3613a == null) {
                arrayList.add("width");
            }
            if (this.b == null) {
                arrayList.add("height");
            }
            if (arrayList.isEmpty()) {
                if (this.f3613a.intValue() <= 0) {
                    arrayList.add("width");
                }
                if (this.b.intValue() <= 0) {
                    arrayList.add("height");
                }
                if (arrayList.isEmpty()) {
                    return new d(this.f3613a.intValue(), this.b.intValue(), 0);
                }
                StringBuilder sb = new StringBuilder("Invalid parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new com.smaato.sdk.richmedia.mraid.exception.a(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder("Missing required parameter(s): ");
            sb2.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
            throw new com.smaato.sdk.richmedia.mraid.exception.a(sb2.toString());
        }
    }

    /* synthetic */ d(int i, int i2, byte b2) {
        this(i, i2);
    }

    private d(int i, int i2) {
        this.c = true;
        this.f3612a = i;
        this.b = i2;
    }
}
