package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.content.Context;
import android.graphics.Rect;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.notifier.ChangeSender;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;
import com.smaato.sdk.richmedia.ad.c;
import com.smaato.sdk.richmedia.ad.l;
import java.util.List;

public final class b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final i f3609a;
    @NonNull
    private final ChangeSender<a> b;
    @NonNull
    private final ChangeSender<e> c = ChangeSenderUtils.createUniqueValueChangeSender(e.a());
    @NonNull
    private final ChangeSender<c> d;
    @NonNull
    private final ChangeSender<Rect> e;
    @NonNull
    private final ChangeSender<Rect> f;
    @NonNull
    private final ChangeSender<Rect> g;
    @NonNull
    private final ChangeSender<Rect> h;
    @NonNull
    private final ChangeSender<com.smaato.sdk.richmedia.mraid.dataprovider.h.b> i;
    @NonNull
    private final ChangeSender<l> j;
    @NonNull
    private final ChangeSender<List<String>> k;
    @NonNull
    private final ChangeSender<Boolean> l;

    public b(@NonNull Context context, @NonNull i iVar, @NonNull com.smaato.sdk.richmedia.mraid.dataprovider.h.b bVar, @NonNull LocationProvider locationProvider, @NonNull List<String> list) {
        Objects.requireNonNull(context);
        Objects.requireNonNull(locationProvider);
        Objects.requireNonNull(list);
        this.f3609a = (i) Objects.requireNonNull(iVar);
        this.k = ChangeSenderUtils.createUniqueValueChangeSender(Lists.toImmutableList(list));
        this.b = ChangeSenderUtils.createUniqueValueChangeSender(a.a(context));
        this.e = ChangeSenderUtils.createUniqueValueChangeSender(new Rect());
        this.f = ChangeSenderUtils.createUniqueValueChangeSender(new Rect());
        this.g = ChangeSenderUtils.createUniqueValueChangeSender(new Rect());
        Size displaySizeInDp = UIUtils.getDisplaySizeInDp(context);
        this.h = ChangeSenderUtils.createUniqueValueChangeSender(new Rect(0, 0, displaySizeInDp.width, displaySizeInDp.height));
        this.d = ChangeSenderUtils.createUniqueValueChangeSender(c.a(context));
        this.i = ChangeSenderUtils.createUniqueValueChangeSender(bVar);
        this.j = ChangeSenderUtils.createUniqueValueChangeSender(l.a(locationProvider));
        this.l = ChangeSenderUtils.createUniqueValueChangeSender(Boolean.FALSE);
    }

    @NonNull
    public final ChangeSender<a> a() {
        return this.b;
    }

    @NonNull
    public final ChangeSender<e> b() {
        return this.c;
    }

    @NonNull
    public final ChangeSender<Rect> c() {
        return this.e;
    }

    @NonNull
    public final ChangeSender<Rect> d() {
        return this.f;
    }

    @NonNull
    public final ChangeSender<Rect> e() {
        return this.g;
    }

    @NonNull
    public final ChangeSender<Rect> f() {
        return this.h;
    }

    @NonNull
    public final ChangeSender<c> g() {
        return this.d;
    }

    @NonNull
    public final ChangeSender<com.smaato.sdk.richmedia.mraid.dataprovider.h.b> h() {
        return this.i;
    }

    @NonNull
    public final ChangeSender<l> i() {
        return this.j;
    }

    @NonNull
    public final i j() {
        return this.f3609a;
    }

    @NonNull
    public final ChangeSender<List<String>> k() {
        return this.k;
    }

    @NonNull
    public final ChangeSender<Boolean> l() {
        return this.l;
    }
}
