package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.util.b;

public class a {

    /* renamed from: a reason: collision with root package name */
    public final boolean f3608a;
    @NonNull
    public final com.smaato.sdk.richmedia.util.b.a b;

    private a(boolean z, @NonNull com.smaato.sdk.richmedia.util.b.a aVar) {
        this.f3608a = z;
        this.b = aVar;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return this.f3608a == aVar.f3608a && this.b == aVar.b;
    }

    public final int hashCode() {
        return Objects.hash(Boolean.valueOf(this.f3608a), this.b);
    }

    @NonNull
    public static a a(@NonNull Context context) {
        return new a(b.b(context), b.a(context));
    }
}
