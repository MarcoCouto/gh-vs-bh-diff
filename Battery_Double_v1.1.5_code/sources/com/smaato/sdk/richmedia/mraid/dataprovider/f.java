package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.util.b.a;

public final class f {

    /* renamed from: a reason: collision with root package name */
    public final boolean f3615a;
    @NonNull
    public final a b;

    public f(boolean z, @NonNull a aVar) {
        this.f3615a = z;
        this.b = (a) Objects.requireNonNull(aVar);
    }
}
