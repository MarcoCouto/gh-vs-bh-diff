package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Builder;
import java.util.Arrays;

public final class h {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final b f3618a;

    public enum a {
        LOAD_COMPLETE,
        CLOSE,
        RESIZE,
        EXPAND,
        ERROR,
        RESIZING_FINISHED,
        EXPANDING_FINISHED,
        CLOSE_FINISHED,
        VISIBILITY_PARAMS_CHECK
    }

    public enum b {
        HIDDEN,
        LOADING,
        DEFAULT,
        RESIZED,
        EXPANDED,
        RESIZE_IN_PROGRESS,
        EXPAND_IN_PROGRESS,
        CLOSE_IN_PROGRESS
    }

    public h(@NonNull b bVar) {
        this.f3618a = (b) Objects.requireNonNull(bVar);
    }

    @NonNull
    public final StateMachine<a, b> a() {
        return new Builder().setInitialState(this.f3618a).addTransition(a.LOAD_COMPLETE, Arrays.asList(new b[]{b.LOADING, b.DEFAULT})).addTransition(a.RESIZE, Arrays.asList(new b[]{b.DEFAULT, b.RESIZE_IN_PROGRESS})).addTransition(a.RESIZE, Arrays.asList(new b[]{b.RESIZED, b.RESIZE_IN_PROGRESS})).addLoopTransition(a.RESIZE, b.RESIZE_IN_PROGRESS).addTransition(a.RESIZING_FINISHED, Arrays.asList(new b[]{b.RESIZE_IN_PROGRESS, b.RESIZED})).addTransition(a.EXPAND, Arrays.asList(new b[]{b.DEFAULT, b.EXPAND_IN_PROGRESS})).addTransition(a.EXPAND, Arrays.asList(new b[]{b.RESIZED, b.EXPAND_IN_PROGRESS})).addTransition(a.EXPAND, Arrays.asList(new b[]{b.RESIZE_IN_PROGRESS, b.EXPAND_IN_PROGRESS})).addTransition(a.EXPANDING_FINISHED, Arrays.asList(new b[]{b.EXPAND_IN_PROGRESS, b.EXPANDED})).addTransition(a.CLOSE, Arrays.asList(new b[]{b.RESIZED, b.CLOSE_IN_PROGRESS})).addTransition(a.CLOSE, Arrays.asList(new b[]{b.EXPANDED, b.CLOSE_IN_PROGRESS})).addTransition(a.ERROR, Arrays.asList(new b[]{b.RESIZE_IN_PROGRESS, b.DEFAULT})).addTransition(a.ERROR, Arrays.asList(new b[]{b.EXPAND_IN_PROGRESS, b.DEFAULT})).addTransition(a.CLOSE_FINISHED, Arrays.asList(new b[]{b.CLOSE_IN_PROGRESS, b.DEFAULT})).addLoopTransition(a.VISIBILITY_PARAMS_CHECK, b.DEFAULT).addLoopTransition(a.VISIBILITY_PARAMS_CHECK, b.RESIZED).addLoopTransition(a.VISIBILITY_PARAMS_CHECK, b.EXPANDED).build();
    }

    @NonNull
    public final StateMachine<a, b> b() {
        return new Builder().setInitialState(this.f3618a).addTransition(a.LOAD_COMPLETE, Arrays.asList(new b[]{b.LOADING, b.DEFAULT})).addTransition(a.CLOSE, Arrays.asList(new b[]{b.DEFAULT, b.HIDDEN})).addLoopTransition(a.VISIBILITY_PARAMS_CHECK, b.DEFAULT).build();
    }
}
