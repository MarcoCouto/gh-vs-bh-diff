package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;

public final class e {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Rect f3614a;
    public final float b;

    private e() {
        this(-1.0f, new Rect());
    }

    private e(float f, @NonNull Rect rect) {
        this.b = f;
        this.f3614a = rect;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        return Float.compare(eVar.b, this.b) == 0 && Objects.equals(this.f3614a, eVar.f3614a);
    }

    public final int hashCode() {
        return Objects.hash(Float.valueOf(this.b), this.f3614a);
    }

    @NonNull
    public static e a(float f, @NonNull Rect rect) {
        return new e(f, rect);
    }

    @NonNull
    static e a() {
        return new e();
    }
}
