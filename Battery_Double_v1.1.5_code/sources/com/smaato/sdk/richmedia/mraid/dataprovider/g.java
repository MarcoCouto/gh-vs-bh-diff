package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.richmedia.ad.c;
import java.util.ArrayList;
import java.util.Map;

public final class g {

    /* renamed from: a reason: collision with root package name */
    private int f3616a;
    private int b;
    private int c;
    private int d;
    private boolean e;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private Integer f3617a;
        @Nullable
        private Integer b;
        @Nullable
        private Integer c;
        @Nullable
        private Integer d;
        @NonNull
        private Boolean e = Boolean.FALSE;

        public a(@NonNull Map<String, String> map) {
            Integer num;
            Integer num2;
            Integer num3;
            String str = (String) map.get("width");
            Integer num4 = null;
            if (!TextUtils.isEmpty(str)) {
                Float a2 = c.a(str);
                if (a2 == null) {
                    num3 = null;
                } else {
                    num3 = Integer.valueOf(a2.intValue());
                }
                this.f3617a = num3;
            }
            String str2 = (String) map.get("height");
            if (!TextUtils.isEmpty(str2)) {
                Float a3 = c.a(str2);
                if (a3 == null) {
                    num2 = null;
                } else {
                    num2 = Integer.valueOf(a3.intValue());
                }
                this.b = num2;
            }
            String str3 = (String) map.get("offsetX");
            if (!TextUtils.isEmpty(str3)) {
                Float a4 = c.a(str3);
                if (a4 == null) {
                    num = null;
                } else {
                    num = Integer.valueOf(a4.intValue());
                }
                this.c = num;
            }
            String str4 = (String) map.get("offsetY");
            if (!TextUtils.isEmpty(str4)) {
                Float a5 = c.a(str4);
                if (a5 != null) {
                    num4 = Integer.valueOf(a5.intValue());
                }
                this.d = num4;
            }
            String str5 = (String) map.get("allowOffscreen");
            if (!TextUtils.isEmpty(str5)) {
                this.e = Boolean.valueOf(Boolean.parseBoolean(str5));
            }
        }

        @NonNull
        public final g a() throws com.smaato.sdk.richmedia.mraid.exception.a {
            ArrayList arrayList = new ArrayList();
            if (this.f3617a == null) {
                arrayList.add("width");
            }
            if (this.b == null) {
                arrayList.add("height");
            }
            if (this.c == null) {
                arrayList.add("offsetX");
            }
            if (this.d == null) {
                arrayList.add("offsetY");
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new com.smaato.sdk.richmedia.mraid.exception.a(sb.toString());
            } else if (this.f3617a.intValue() < 50 || this.b.intValue() < 50) {
                throw new com.smaato.sdk.richmedia.mraid.exception.a("Expected resize dimension should be >= 50 dp");
            } else {
                g gVar = new g(this.f3617a.intValue(), this.b.intValue(), this.c.intValue(), this.d.intValue(), this.e.booleanValue(), 0);
                return gVar;
            }
        }
    }

    /* synthetic */ g(int i, int i2, int i3, int i4, boolean z, byte b2) {
        this(i, i2, i3, i4, z);
    }

    private g(int i, int i2, int i3, int i4, boolean z) {
        this.f3616a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = z;
    }

    @NonNull
    public final Rect a(@NonNull Rect rect, @NonNull Rect rect2) {
        int i = this.c;
        int i2 = this.d;
        if (!rect.isEmpty()) {
            i += rect.left;
            i2 += rect.top;
        }
        Rect rect3 = new Rect(i, i2, this.f3616a + i, this.b + i2);
        if (this.e || rect2.contains(rect3)) {
            return rect3;
        }
        Rect rect4 = new Rect(rect3);
        if (rect4.left < rect2.left) {
            rect4.left = rect2.left;
            rect4.right = rect4.left + rect3.width();
        }
        if (rect4.right > rect2.right) {
            rect4.left = Math.max(rect2.left, rect2.right - rect3.width());
            rect4.right = rect4.left + Math.min(rect3.width(), rect2.width());
        }
        if (rect4.top < rect2.top) {
            rect4.top = rect2.top;
            rect4.bottom = rect4.top + rect3.height();
        }
        if (rect4.bottom > rect2.bottom) {
            rect4.top = Math.max(rect2.top, rect2.bottom - rect3.height());
            rect4.bottom = rect4.top + Math.min(rect3.height(), rect2.height());
        }
        return rect4;
    }
}
