package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.api.ApiAdRequest;
import com.smaato.sdk.core.util.Objects;

public final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f3610a;
    @NonNull
    public final String b;
    @Nullable
    public final String c;
    @Nullable
    public final Boolean d;
    @Nullable
    public final Integer e;

    public static final class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final String f3611a = SmaatoSdk.getVersion();
        @NonNull
        private final String b;
        @Nullable
        private final String c;
        @Nullable
        private final Boolean d;
        @Nullable
        private final Integer e;

        public a(@NonNull String str, @NonNull ApiAdRequest apiAdRequest) {
            Objects.requireNonNull(str);
            Objects.requireNonNull(apiAdRequest);
            this.b = str;
            this.c = apiAdRequest.getGoogleAdId();
            this.d = apiAdRequest.getGoogleDnt();
            this.e = apiAdRequest.getCoppa();
        }

        @NonNull
        public final c a() {
            Objects.requireNonNull(this.f3611a);
            Objects.requireNonNull(this.b);
            c cVar = new c(this.f3611a, this.b, this.c, this.d, this.e, 0);
            return cVar;
        }
    }

    /* synthetic */ c(String str, String str2, String str3, Boolean bool, Integer num, byte b2) {
        this(str, str2, str3, bool, num);
    }

    private c(@NonNull String str, @NonNull String str2, @Nullable String str3, @Nullable Boolean bool, @Nullable Integer num) {
        this.f3610a = str;
        this.b = str2;
        this.c = str3;
        this.d = bool;
        this.e = num;
    }
}
