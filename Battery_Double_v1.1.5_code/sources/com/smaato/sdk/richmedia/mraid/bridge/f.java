package com.smaato.sdk.richmedia.mraid.bridge;

import android.net.Uri;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.richmedia.ad.c;
import com.smaato.sdk.richmedia.mraid.dataprovider.h.b;
import com.smaato.sdk.richmedia.mraid.dataprovider.i;
import com.smaato.sdk.richmedia.util.b.a;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class f {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final WebView f3604a;
    @NonNull
    private final Logger b;
    @NonNull
    private final Map<String, b> c = Collections.synchronizedMap(new HashMap());

    /* renamed from: com.smaato.sdk.richmedia.mraid.bridge.f$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f3605a = new int[i.values().length];
        static final /* synthetic */ int[] b = new int[a.values().length];
        static final /* synthetic */ int[] c = new int[b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|(2:5|6)|7|9|10|11|12|13|14|15|17|18|19|20|21|22|23|25|26|27|28|30) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|5|6|7|9|10|11|12|13|14|15|17|18|19|20|21|22|23|25|26|27|28|30) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007a */
        static {
            try {
                c[b.HIDDEN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                c[b.EXPANDED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            c[b.RESIZED.ordinal()] = 3;
            c[b.DEFAULT.ordinal()] = 4;
            c[b.LOADING.ordinal()] = 5;
            b[a.PORTRAIT.ordinal()] = 1;
            b[a.LANDSCAPE.ordinal()] = 2;
            try {
                b[a.UNKNOWN.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            f3605a[i.INTERSTITIAL.ordinal()] = 1;
            try {
                f3605a[i.INLINE.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public f(WebView webView, Logger logger) {
        this.f3604a = (WebView) Objects.requireNonNull(webView);
        this.b = (Logger) Objects.requireNonNull(logger);
    }

    public void a(String str, boolean z) {
        Uri parse = Uri.parse(str);
        String host = parse.getHost();
        if (host != null) {
            Map parseQuery = TextUtils.parseQuery(parse.getQuery());
            b bVar = (b) this.c.get(host);
            if (bVar == null) {
                Logger logger = this.b;
                LogDomain logDomain = LogDomain.MRAID;
                StringBuilder sb = new StringBuilder("A handler for command \"");
                sb.append(host);
                sb.append("\" is not registered");
                logger.debug(logDomain, sb.toString(), new Object[0]);
            } else {
                bVar.handle(parseQuery, z);
            }
        }
        a("window.mraidbridge.nativeCallComplete();");
    }

    public void a() {
        a("window.mraidbridge.fireReadyEvent();");
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, b bVar) {
        if (!TextUtils.isEmpty(str)) {
            this.c.put(str, bVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        Logger logger = this.b;
        LogDomain logDomain = LogDomain.MRAID;
        StringBuilder sb = new StringBuilder("Running script: ");
        sb.append(str);
        logger.info(logDomain, sb.toString(), new Object[0]);
        this.f3604a.loadUrl(c.a("javascript:%s", str));
    }
}
