package com.smaato.sdk.richmedia.mraid.bridge;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.ChangeSender;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;
import com.smaato.sdk.richmedia.ad.c;
import com.smaato.sdk.richmedia.ad.l;
import com.smaato.sdk.richmedia.mraid.dataprovider.d;
import com.smaato.sdk.richmedia.mraid.dataprovider.f;
import com.smaato.sdk.richmedia.mraid.dataprovider.g;
import com.smaato.sdk.richmedia.mraid.dataprovider.i;
import com.smaato.sdk.richmedia.util.b.a;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.List;
import java.util.Map;

public final class e {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3603a;
    @NonNull
    private final f b;
    @Nullable
    private a c;
    @Nullable
    private g d;
    @Nullable
    private d e;
    @NonNull
    private final ChangeSender<f> f = ChangeSenderUtils.createUniqueValueChangeSender(new f(true, a.UNKNOWN));

    public e(@NonNull Logger logger, @NonNull f fVar) {
        this.f3603a = (Logger) Objects.requireNonNull(logger);
        this.b = (f) Objects.requireNonNull(fVar);
        this.b.a("setOrientationProperties", (b) new b() {
            public final void handle(Map map, boolean z) {
                e.this.c(map, z);
            }
        });
        this.b.a("setResizeProperties", (b) new b() {
            public final void handle(Map map, boolean z) {
                e.this.b(map, z);
            }
        });
        this.b.a("setExpandProperties", (b) new b() {
            public final void handle(Map map, boolean z) {
                e.this.a(map, z);
            }
        });
    }

    public final void a(@Nullable a aVar) {
        this.c = aVar;
    }

    @NonNull
    public final ChangeSender<f> a() {
        return this.f;
    }

    @Nullable
    public final g b() {
        return this.d;
    }

    @Nullable
    public final d c() {
        return this.e;
    }

    public final void a(@NonNull com.smaato.sdk.richmedia.mraid.dataprovider.a aVar) {
        String str;
        String str2 = "window.mraidbridge.setCurrentAppOrientation('%s', %b);";
        Object[] objArr = new Object[2];
        switch (AnonymousClass1.b[aVar.b.ordinal()]) {
            case 1:
                str = "portrait";
                break;
            case 2:
                str = "landscape";
                break;
            default:
                str = "none";
                break;
        }
        objArr[0] = str;
        objArr[1] = Boolean.valueOf(aVar.f3608a);
        this.b.a(c.a(str2, objArr));
    }

    public final void a(@NonNull Rect rect) {
        if (!rect.isEmpty()) {
            this.b.a(c.a("window.mraidbridge.setCurrentPosition(%d, %d, %d, %d);", Integer.valueOf(rect.left), Integer.valueOf(rect.top), Integer.valueOf(rect.width()), Integer.valueOf(rect.height())));
        }
    }

    public final void b(@NonNull Rect rect) {
        if (!rect.isEmpty()) {
            this.b.a(c.a("window.mraidbridge.setDefaultPosition(%d, %d, %d, %d);", Integer.valueOf(rect.left), Integer.valueOf(rect.top), Integer.valueOf(rect.width()), Integer.valueOf(rect.height())));
        }
    }

    public final void a(@NonNull Size size) {
        this.b.a(c.a("window.mraidbridge.setMaxSize(%d, %d);", Integer.valueOf(size.width), Integer.valueOf(size.height)));
    }

    public final void b(@NonNull Size size) {
        this.b.a(c.a("window.mraidbridge.setScreenSize(%d, %d);", Integer.valueOf(size.width), Integer.valueOf(size.height)));
    }

    public final void a(@NonNull l lVar) {
        if (lVar.f3591a != null) {
            this.b.a(c.a("window.mraidbridge.setCurrentLocation(%f, %f, %d, %f, %f);", Double.valueOf(lVar.f3591a.getLatitude()), Double.valueOf(lVar.f3591a.getLongitude()), Integer.valueOf(1), Float.valueOf(lVar.b), Float.valueOf(lVar.c)));
        }
    }

    public final void a(@NonNull i iVar) {
        String str;
        String str2 = "window.mraidbridge.setPlacementType('%s');";
        try {
            Object[] objArr = new Object[1];
            switch (AnonymousClass1.f3605a[iVar.ordinal()]) {
                case 1:
                    str = VideoType.INTERSTITIAL;
                    break;
                case 2:
                    str = String.INLINE;
                    break;
                default:
                    StringBuilder sb = new StringBuilder("Unknown placement type: ");
                    sb.append(iVar);
                    throw new IllegalArgumentException(sb.toString());
            }
            objArr[0] = str;
            this.b.a(c.a(str2, objArr));
        } catch (IllegalArgumentException e2) {
            Logger logger = this.f3603a;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb2 = new StringBuilder("Failed to call MRAID's setPlacementType method, reason: ");
            sb2.append(e2.getMessage());
            logger.error(logDomain, sb2.toString(), new Object[0]);
        }
    }

    public final void a(@NonNull List<String> list) {
        String[] strArr = {"sms", "tel", MRAIDNativeFeature.CALENDAR, MRAIDNativeFeature.STORE_PICTURE, MRAIDNativeFeature.INLINE_VIDEO, "location", "vpaid"};
        for (int i = 0; i < 7; i++) {
            String str = strArr[i];
            this.b.a(c.a("window.mraidbridge.setSupports('%s', %b);", str, Boolean.valueOf(list.contains(str))));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Map map, boolean z) {
        try {
            this.e = new d.a(map).a();
        } catch (com.smaato.sdk.richmedia.mraid.exception.a e2) {
            Logger logger = this.f3603a;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb = new StringBuilder("Failed to handle a command: setExpandProperties, reason: ");
            sb.append(e2.getMessage());
            logger.error(logDomain, sb.toString(), new Object[0]);
            Objects.onNotNull(this.c, new Consumer() {
                public final void accept(Object obj) {
                    ((a) obj).onError("setExpandProperties", com.smaato.sdk.richmedia.mraid.exception.a.this.getMessage());
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Map map, boolean z) {
        try {
            this.d = new g.a(map).a();
        } catch (com.smaato.sdk.richmedia.mraid.exception.a e2) {
            Logger logger = this.f3603a;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb = new StringBuilder("Failed to handle a command: setResizeProperties, reason: ");
            sb.append(e2.getMessage());
            logger.error(logDomain, sb.toString(), new Object[0]);
            Objects.onNotNull(this.c, new Consumer() {
                public final void accept(Object obj) {
                    ((a) obj).onError("setResizeProperties", com.smaato.sdk.richmedia.mraid.exception.a.this.getMessage());
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003d, code lost:
        if (r5.equals("landscape") == false) goto L_0x004a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0052  */
    public /* synthetic */ void c(Map map, boolean z) {
        a aVar;
        String str = (String) map.get("allowOrientationChange");
        char c2 = 1;
        boolean booleanValue = !TextUtils.isEmpty(str) ? Boolean.valueOf(str).booleanValue() : true;
        String str2 = (String) map.get("forceOrientation");
        if (!TextUtils.isEmpty(str2)) {
            int hashCode = str2.hashCode();
            if (hashCode == 729267099) {
                if (str2.equals("portrait")) {
                    c2 = 0;
                    switch (c2) {
                        case 0:
                            aVar = a.PORTRAIT;
                            break;
                        case 1:
                            aVar = a.LANDSCAPE;
                            break;
                    }
                }
            } else if (hashCode == 1430647483) {
            }
            c2 = 65535;
            switch (c2) {
                case 0:
                    break;
                case 1:
                    break;
            }
        }
        aVar = a.UNKNOWN;
        this.f.newValue(new f(booleanValue, aVar));
    }
}
