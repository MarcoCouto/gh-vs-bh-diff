package com.smaato.sdk.richmedia.mraid.bridge;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.mraid.dataprovider.e;
import com.smaato.sdk.richmedia.mraid.dataprovider.h.b;

public final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3601a;
    @NonNull
    private final f b;

    public c(Logger logger, @NonNull f fVar) {
        this.f3601a = (Logger) Objects.requireNonNull(logger);
        this.b = (f) Objects.requireNonNull(fVar);
    }

    public final void a(@NonNull e eVar) {
        if (eVar.b >= 0.0f) {
            Rect rect = eVar.f3614a;
            this.b.a(com.smaato.sdk.richmedia.ad.c.a("window.mraidbridge.fireExposureChangeEvent(%.2f, %s, %s);", Float.valueOf(eVar.b), com.smaato.sdk.richmedia.ad.c.a("{ \"x\":%d, \"y\":%d, \"width\":%d, \"height\":%d }", Integer.valueOf(rect.left), Integer.valueOf(rect.top), Integer.valueOf(rect.width()), Integer.valueOf(rect.height())), "null"));
        }
    }

    public final void a(@NonNull com.smaato.sdk.richmedia.ad.c cVar) {
        this.b.a(com.smaato.sdk.richmedia.ad.c.a("window.mraidbridge.fireAudioVolumeChangeEvent(%s);", cVar.f3575a));
    }

    public final void a(@NonNull String str, @NonNull String str2) {
        this.b.a(com.smaato.sdk.richmedia.ad.c.a("window.mraidbridge.fireErrorEvent('%s', '%s');", str2, str));
    }

    public final void a(@NonNull b bVar) {
        String str;
        String str2 = "window.mraidbridge.fireStateChangeEvent('%s');";
        try {
            Object[] objArr = new Object[1];
            switch (AnonymousClass1.c[bVar.ordinal()]) {
                case 1:
                    str = "hidden";
                    break;
                case 2:
                    str = "expanded";
                    break;
                case 3:
                    str = "resized";
                    break;
                case 4:
                    str = "default";
                    break;
                case 5:
                    str = "loading";
                    break;
                default:
                    StringBuilder sb = new StringBuilder("Unknown state: ");
                    sb.append(bVar);
                    throw new IllegalArgumentException(sb.toString());
            }
            objArr[0] = str;
            this.b.a(com.smaato.sdk.richmedia.ad.c.a(str2, objArr));
        } catch (IllegalArgumentException e) {
            Logger logger = this.f3601a;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb2 = new StringBuilder("Failed to call MRAID's fireStateChangeEvent method, reason: ");
            sb2.append(e.getMessage());
            logger.error(logDomain, sb2.toString(), new Object[0]);
        }
    }

    public final void a(@NonNull Rect rect) {
        this.b.a(com.smaato.sdk.richmedia.ad.c.a("window.mraidbridge.fireSizeChangeEvent(%d, %d);", Integer.valueOf(rect.width()), Integer.valueOf(rect.height())));
    }

    public final void a(boolean z) {
        this.b.a(com.smaato.sdk.richmedia.ad.c.a("window.mraidbridge.fireViewableChangeEvent(%b);", Boolean.valueOf(z)));
    }
}
