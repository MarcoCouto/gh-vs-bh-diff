package com.smaato.sdk.richmedia.mraid.bridge;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.share.internal.ShareConstants;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.Consumer;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.Map;

public final class d {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private Consumer<String> f3602a;
    @Nullable
    private Consumer<String> b;
    @Nullable
    private Consumer<String> c;
    @Nullable
    private Consumer<String> d;
    @Nullable
    private Consumer<Whatever> e;
    @Nullable
    private Consumer<Whatever> f;
    @Nullable
    private Consumer<Whatever> g;
    @Nullable
    private BiConsumer<String, String> h;
    @NonNull
    private final b i = new b() {
        public final void handle(Map map, boolean z) {
            d.this.g(map, z);
        }
    };
    @NonNull
    private final b j = new b() {
        public final void handle(Map map, boolean z) {
            d.this.f(map, z);
        }
    };
    @NonNull
    private final b k = new b() {
        public final void handle(Map map, boolean z) {
            d.this.e(map, z);
        }
    };
    @NonNull
    private final b l = new b() {
        public final void handle(Map map, boolean z) {
            d.this.d(map, z);
        }
    };
    @NonNull
    private final b m = new b() {
        public final void handle(Map map, boolean z) {
            d.this.c(map, z);
        }
    };
    @NonNull
    private final b n = new b() {
        public final void handle(Map map, boolean z) {
            d.this.b(map, z);
        }
    };
    @NonNull
    private final b o = new b() {
        public final void handle(Map map, boolean z) {
            d.this.a(map, z);
        }
    };

    /* access modifiers changed from: private */
    public /* synthetic */ void g(Map map, boolean z) {
        if (this.f3602a != null) {
            this.f3602a.accept(map.get("event"));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void f(Map map, boolean z) {
        String str = (String) map.get("url");
        if (!z) {
            Objects.onNotNull(this.h, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((BiConsumer) obj).accept("AUTO_OPEN", this.f$0);
                }
            });
            return;
        }
        if (this.b != null) {
            this.b.accept(str);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(Map map, boolean z) {
        String str = (String) map.get("url");
        if (!z) {
            Objects.onNotNull(this.h, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((BiConsumer) obj).accept("AUTO_EXPAND", this.f$0);
                }
            });
            return;
        }
        if (this.d != null) {
            this.d.accept(str);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Map map, boolean z) {
        String str = (String) map.get(ShareConstants.MEDIA_URI);
        if (!z) {
            Objects.onNotNull(this.h, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((BiConsumer) obj).accept("AUTO_PLAY", this.f$0);
                }
            });
            return;
        }
        if (this.c != null) {
            this.c.accept(map.get(ShareConstants.MEDIA_URI));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Map map, boolean z) {
        if (this.g != null) {
            this.g.accept(Whatever.INSTANCE);
        }
        Objects.onNotNull(this.h, $$Lambda$d$g8rESMuFeIo1rKWI3NugmW2saDY.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Map map, boolean z) {
        if (!z) {
            Objects.onNotNull(this.h, $$Lambda$d$PDsrz4GEnVLNVdCCslDgdL548ok.INSTANCE);
            return;
        }
        if (this.e != null) {
            this.e.accept(Whatever.INSTANCE);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Map map, boolean z) {
        if (this.f != null) {
            this.f.accept(Whatever.INSTANCE);
        }
    }

    public d(@NonNull f fVar) {
        f fVar2 = (f) Objects.requireNonNull(fVar);
        fVar2.a("addEventListener", this.i);
        fVar2.a("open", this.j);
        fVar2.a("playVideo", this.l);
        fVar2.a(Events.CREATIVE_EXPAND, this.k);
        fVar2.a("unload", this.m);
        fVar2.a("resize", this.n);
        fVar2.a("close", this.o);
    }

    public final void a(@Nullable Consumer<String> consumer) {
        this.f3602a = consumer;
    }

    public final void b(@Nullable Consumer<String> consumer) {
        this.b = consumer;
    }

    public final void c(@Nullable Consumer<String> consumer) {
        this.c = consumer;
    }

    public final void d(@Nullable Consumer<Whatever> consumer) {
        this.e = consumer;
    }

    public final void e(@Nullable Consumer<String> consumer) {
        this.d = consumer;
    }

    public final void f(@Nullable Consumer<Whatever> consumer) {
        this.g = consumer;
    }

    public final void g(@Nullable Consumer<Whatever> consumer) {
        this.f = consumer;
    }

    public final void a(@Nullable BiConsumer<String, String> biConsumer) {
        this.h = biConsumer;
    }
}
