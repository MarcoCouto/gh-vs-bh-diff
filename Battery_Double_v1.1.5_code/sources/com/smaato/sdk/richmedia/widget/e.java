package com.smaato.sdk.richmedia.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StubOnGestureListener;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.webview.BaseWebView;
import com.smaato.sdk.core.webview.BaseWebViewClient;
import com.smaato.sdk.core.webview.BaseWebViewClient.WebViewClientCallback;
import com.smaato.sdk.core.webview.WebViewHelperUtil;
import com.smaato.sdk.richmedia.mraid.dataprovider.c;
import com.smaato.sdk.richmedia.util.d;
import com.smaato.sdk.richmedia.widget.e.a;

@SuppressLint({"ViewConstructor"})
public final class e extends BaseWebView {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3640a;
    @NonNull
    private final d b;
    /* access modifiers changed from: private */
    @Nullable
    public a c;
    private boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    private boolean f = false;

    public interface a {
        void a();

        void a(@NonNull String str);

        void a(@NonNull String str, @NonNull String str2);

        void a(@NonNull String str, boolean z);

        void b();

        void c();
    }

    public e(@NonNull Context context, @NonNull Logger logger, @NonNull d dVar) {
        super((Context) Objects.requireNonNull(context));
        this.f3640a = (Logger) Objects.requireNonNull(logger);
        this.b = (d) Objects.requireNonNull(dVar);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        setVisibility(4);
        setBackgroundColor(getResources().getColor(17170445));
        WebSettings settings = getSettings();
        settings.setDisplayZoomControls(false);
        settings.setSupportZoom(false);
        BaseWebViewClient baseWebViewClient = new BaseWebViewClient();
        baseWebViewClient.setWebViewClientCallback(new WebViewClientCallback() {
            public final boolean shouldOverrideUrlLoading(@NonNull String str) {
                e.this.f3640a.debug(LogDomain.WIDGET, "shouldOverrideUrlLoading: %s", str);
                if (str.startsWith("smaato://")) {
                    Objects.onNotNull(e.this.c, new Consumer(str) {
                        private final /* synthetic */ String f$1;

                        {
                            this.f$1 = r2;
                        }

                        public final void accept(Object obj) {
                            AnonymousClass2.this.c(this.f$1, (a) obj);
                        }
                    });
                    return true;
                } else if (str.startsWith("mraid://")) {
                    return true;
                } else {
                    if (e.this.e) {
                        e.this.e = false;
                        e.this.f3640a.debug(LogDomain.WIDGET, "shouldOverrideUrlLoading: going to call Callback::onUrlClicked() with %s", str);
                        Objects.onNotNull(e.this.c, new Consumer(str) {
                            private final /* synthetic */ String f$0;

                            {
                                this.f$0 = r1;
                            }

                            public final void accept(Object obj) {
                                ((a) obj).a(this.f$0);
                            }
                        });
                        return true;
                    }
                    Objects.onNotNull(e.this.c, new Consumer(str) {
                        private final /* synthetic */ String f$0;

                        {
                            this.f$0 = r1;
                        }

                        public final void accept(Object obj) {
                            ((a) obj).a("AUTO_REDIRECT", this.f$0);
                        }
                    });
                    return true;
                }
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void c(String str, a aVar) {
                aVar.a(str, e.this.e);
            }

            public final void onPageStartedLoading(@NonNull String str) {
                e.this.f3640a.debug(LogDomain.WIDGET, "onPageStartedLoading: %s", str);
            }

            public final void onPageFinishedLoading(@NonNull String str) {
                e.this.f3640a.debug(LogDomain.WIDGET, "onPageFinishedLoading: %s", str);
                e.this.setVisibility(0);
                Objects.onNotNull(e.this.c, $$Lambda$M8ZoKiqmfQZJpeZFa4hE9eTUz8.INSTANCE);
            }

            public final void onHttpError(@NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse) {
                e.this.f3640a.debug(LogDomain.WIDGET, "onHttpError: request = %s, errorResponse = %s", webResourceRequest, webResourceResponse);
            }

            public final void onGeneralError(int i, @NonNull String str, @NonNull String str2) {
                e.this.f3640a.debug(LogDomain.WIDGET, "onGeneralError: errorCode = %d, description = %s, failingUrl = %s", Integer.valueOf(i), str, str2);
            }

            public final void onRenderProcessGone() {
                Objects.onNotNull(e.this.c, $$Lambda$fapAbGEBbzEYBI2DZejhXZrCyns.INSTANCE);
            }
        });
        setWebViewClient(baseWebViewClient);
        setWebChromeClient(new WebChromeClient() {
            public final boolean onJsAlert(@NonNull WebView webView, @NonNull String str, @NonNull String str2, @NonNull JsResult jsResult) {
                Objects.onNotNull(e.this.c, $$Lambda$e$3$kPKAravOvgwfq50HWqopPAGolXw.INSTANCE);
                jsResult.confirm();
                return true;
            }

            public final boolean onJsConfirm(@NonNull WebView webView, @NonNull String str, @NonNull String str2, @NonNull JsResult jsResult) {
                Objects.onNotNull(e.this.c, $$Lambda$e$3$wFqPGk2G83kcZObqUkqnoCDU.INSTANCE);
                jsResult.confirm();
                return true;
            }

            public final boolean onJsPrompt(@NonNull WebView webView, @NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull JsPromptResult jsPromptResult) {
                Objects.onNotNull(e.this.c, $$Lambda$e$3$qBpVdSGmoOOTORj_fDAXqiTtM.INSTANCE);
                jsPromptResult.confirm();
                return true;
            }

            public final boolean onJsBeforeUnload(@NonNull WebView webView, @NonNull String str, @NonNull String str2, @NonNull JsResult jsResult) {
                Objects.onNotNull(e.this.c, $$Lambda$e$3$dUnXtbt7oyQbBoNHTxR8No4rW14.INSTANCE);
                jsResult.confirm();
                return true;
            }
        });
    }

    public final void a(@NonNull a aVar) {
        this.c = aVar;
    }

    @MainThread
    public final void a(@NonNull String str, @NonNull c cVar) {
        Threads.ensureMainThread();
        if (!this.f) {
            this.f = true;
            b();
            loadData(this.b.a(str, getContext(), cVar), WebRequest.CONTENT_TYPE_HTML, null);
        }
    }

    @MainThread
    public final void a(@NonNull String str) {
        Threads.ensureMainThread();
        if (!this.f) {
            this.f = true;
            b();
            loadUrl(str);
        }
    }

    @MainThread
    public final void destroy() {
        Threads.ensureMainThread();
        if (this.d) {
            this.f3640a.debug(LogDomain.WIDGET, "destroy() has been already called, ignoring this call", new Object[0]);
            return;
        }
        this.d = true;
        WebViewHelperUtil.resetAndDestroyWebViewSafely(this, this.f3640a);
    }

    public final void a() {
        this.e = false;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    private void b() {
        setOnTouchListener(new OnTouchListener(new GestureDetector(getContext(), new StubOnGestureListener() {
            public final boolean onSingleTapUp(MotionEvent motionEvent) {
                e.this.e = true;
                Objects.onNotNull(e.this.c, $$Lambda$mGqCFeXNkU78QflX2VDbQz_4XZM.INSTANCE);
                return true;
            }
        })) {
            private final /* synthetic */ GestureDetector f$0;

            {
                this.f$0 = r1;
            }

            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return this.f$0.onTouchEvent(motionEvent);
            }
        });
    }
}
