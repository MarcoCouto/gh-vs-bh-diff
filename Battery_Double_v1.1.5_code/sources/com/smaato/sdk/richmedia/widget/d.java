package com.smaato.sdk.richmedia.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.ViewUtils;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.R;
import com.smaato.sdk.richmedia.ad.h;
import com.smaato.sdk.richmedia.mraid.c;
import com.smaato.sdk.richmedia.mraid.dataprovider.d;
import com.smaato.sdk.richmedia.mraid.mvp.b;
import com.smaato.sdk.richmedia.widget.d.a;

@SuppressLint({"ViewConstructor"})
public final class d extends AdContentView implements b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3635a;
    /* access modifiers changed from: private */
    @NonNull
    public final e b;
    @NonNull
    private final h c;
    @NonNull
    private final com.smaato.sdk.richmedia.util.d d;
    /* access modifiers changed from: private */
    @NonNull
    public final a e;
    /* access modifiers changed from: private */
    @NonNull
    public final com.smaato.sdk.richmedia.mraid.presenter.a f;
    @NonNull
    private final View g;
    @NonNull
    private final FrameLayout h;
    @Nullable
    private c i;
    @Nullable
    private b j;
    /* access modifiers changed from: private */
    @Nullable
    public e k;

    public interface a {
        void a();

        void a(@NonNull View view);

        void a(@NonNull d dVar);

        void a(@NonNull d dVar, @NonNull String str);

        void a(@NonNull e eVar);

        void a(@NonNull String str, @Nullable String str2);

        void b();

        void b(@NonNull View view);

        void c();

        void d();

        void e();

        void f();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(View view) {
    }

    private d(@NonNull Logger logger, @NonNull Context context, @NonNull h hVar, @NonNull a aVar, @NonNull com.smaato.sdk.richmedia.util.d dVar, @NonNull e eVar, @NonNull com.smaato.sdk.richmedia.mraid.presenter.a aVar2) {
        super(context);
        this.f3635a = logger;
        this.c = hVar;
        this.e = aVar;
        this.d = dVar;
        this.f = aVar2;
        this.b = eVar;
        int dpToPx = UIUtils.dpToPx(context, (float) hVar.b());
        int dpToPx2 = UIUtils.dpToPx(context, (float) hVar.c());
        this.h = new FrameLayout(context);
        addView(this.h, generateDefaultLayoutParams(dpToPx, dpToPx2));
        eVar.a((com.smaato.sdk.richmedia.widget.e.a) new com.smaato.sdk.richmedia.widget.e.a() {
            public final void a() {
                d.this.f.g();
                d.this.e.a(d.this);
            }

            public final void c() {
                d.this.e.a();
            }

            public final void a(@NonNull String str) {
                d.this.e.a(d.this, str);
            }

            public final void a(@NonNull String str, boolean z) {
                d.this.f.a(str, z);
            }

            public final void a(@NonNull String str, @NonNull String str2) {
                d.this.e.a(str, str2);
            }

            public final void b() {
                d.this.e.f();
            }
        });
        eVar.setId(R.id.webView);
        this.h.addView(eVar, new LayoutParams(-1, -1));
        FrameLayout frameLayout = new FrameLayout(getContext());
        frameLayout.setBackgroundResource(R.color.smaato_sdk_richmedia_ui_semitransparent);
        frameLayout.setLayoutParams(new LayoutParams(-1, -1));
        frameLayout.setOnClickListener($$Lambda$d$V_A5NOLhAg5HgwI6B9wyvBgsiP8.INSTANCE);
        ProgressBar progressBar = new ProgressBar(getContext());
        LayoutParams layoutParams = new LayoutParams(-2, -2, 17);
        progressBar.setIndeterminateDrawable(getResources().getDrawable(R.drawable.smaato_sdk_richmedia_progress_bar));
        progressBar.setLayoutParams(layoutParams);
        frameLayout.addView(progressBar);
        this.g = frameLayout;
        this.g.setVisibility(8);
        this.h.addView(this.g);
        setLayoutParams(new LayoutParams(dpToPx, dpToPx2, 17));
        this.f.a(new BiConsumer(eVar) {
            private final /* synthetic */ e f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj, Object obj2) {
                d.this.a(this.f$1, (String) obj, (d) obj2);
            }
        });
        this.f.a_(new Consumer(eVar, aVar) {
            private final /* synthetic */ e f$1;
            private final /* synthetic */ a f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                d.this.a(this.f$1, this.f$2, (String) obj);
            }
        });
        this.f.b((Consumer<Whatever>) new Consumer(aVar) {
            private final /* synthetic */ a f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                d.this.a(this.f$1, (Whatever) obj);
            }
        });
        this.f.d(new Consumer(eVar) {
            private final /* synthetic */ e f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                d.this.a(this.f$1, (com.smaato.sdk.richmedia.mraid.presenter.d) obj);
            }
        });
        this.f.c((Consumer<Whatever>) new Consumer() {
            public final void accept(Object obj) {
                d.this.a((Whatever) obj);
            }
        });
        com.smaato.sdk.richmedia.mraid.presenter.a aVar3 = this.f;
        aVar.getClass();
        aVar3.b((BiConsumer<String, String>) new BiConsumer() {
            public final void accept(Object obj, Object obj2) {
                a.this.a((String) obj, (String) obj2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(e eVar, String str, com.smaato.sdk.richmedia.mraid.dataprovider.d dVar) {
        eVar.a();
        if (this.j == null) {
            if (!TextUtils.isEmpty(str) && URLUtil.isNetworkUrl(str)) {
                this.k = new e(getContext(), this.f3635a, this.d);
                this.k.a((com.smaato.sdk.richmedia.widget.e.a) new f(true) {

                    /* renamed from: a reason: collision with root package name */
                    private boolean f3636a;
                    private /* synthetic */ boolean b = true;

                    public final void a() {
                        if (this.f3636a) {
                            d.this.f.j();
                            return;
                        }
                        d.this.a((View) d.this.k, this.b);
                        d.this.e.a(d.this.k);
                    }

                    public final void a(@NonNull String str, @NonNull String str2) {
                        this.f3636a = true;
                        d.this.e.a(str, str2);
                    }

                    public final void b() {
                        d.this.f.j();
                    }
                });
                this.k.a(str);
                return;
            }
            a((View) this.h, false);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(e eVar, a aVar, String str) {
        eVar.a();
        aVar.a(this, str);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(a aVar, Whatever whatever) {
        aVar.c();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(e eVar, com.smaato.sdk.richmedia.mraid.presenter.d dVar) {
        eVar.a();
        if (this.i == null) {
            this.i = new c(this.f3635a, this.h, dVar.f3628a);
            this.i.a((com.smaato.sdk.richmedia.widget.c.a) new com.smaato.sdk.richmedia.widget.c.a() {
                public final void a(@NonNull ImageButton imageButton) {
                    d.this.f.i();
                    d.this.e.d();
                    d.this.e.a((View) imageButton);
                }

                public final void b(@NonNull ImageButton imageButton) {
                    d.this.f.h();
                    d.this.e.b(imageButton);
                }

                public final void a(@NonNull String str) {
                    d.this.f.c(str);
                }
            });
        }
        this.i.a(dVar.b);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Whatever whatever) {
        d();
    }

    @NonNull
    public static d a(@NonNull Logger logger, @NonNull Context context, @NonNull h hVar, @NonNull a aVar, @NonNull com.smaato.sdk.richmedia.util.d dVar, @NonNull e eVar, @NonNull com.smaato.sdk.richmedia.mraid.presenter.a aVar2) {
        d dVar2 = new d((Logger) Objects.requireNonNull(logger), (Context) Objects.requireNonNull(context), (h) Objects.requireNonNull(hVar), (a) Objects.requireNonNull(aVar), (com.smaato.sdk.richmedia.util.d) Objects.requireNonNull(dVar), (e) Objects.requireNonNull(eVar), (com.smaato.sdk.richmedia.mraid.presenter.a) Objects.requireNonNull(aVar2));
        return dVar2;
    }

    @MainThread
    public final void a() {
        Threads.ensureMainThread();
        this.b.a(this.c.a(), new com.smaato.sdk.richmedia.mraid.dataprovider.c.a(getContext().getPackageName(), this.c.getSomaApiContext().getApiAdRequest()).a());
    }

    @MainThread
    public final void a(boolean z) {
        Threads.ensureMainThread();
        this.g.setVisibility(z ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f.a(this);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f.e();
    }

    @MainThread
    public final void b() {
        Threads.ensureMainThread();
        d();
        Objects.onNotNull(this.k, $$Lambda$w4d5SW6dYE0MY70ALtxmi6Q37sQ.INSTANCE);
        this.f.f();
        Handler newUiHandler = Threads.newUiHandler();
        e eVar = this.b;
        eVar.getClass();
        newUiHandler.postDelayed(new Runnable() {
            public final void run() {
                e.this.destroy();
            }
        }, 1000);
    }

    @NonNull
    public final e c() {
        return this.b;
    }

    /* access modifiers changed from: private */
    public void a(@NonNull View view, final boolean z) {
        this.j = new b();
        this.j.a(view, (a) new a() {
            public final void a(@NonNull ImageButton imageButton) {
                d.this.f.k();
                d.this.e.b();
                d.this.e.a((View) imageButton);
            }

            public final void b(@NonNull ImageButton imageButton) {
                d.this.f.h();
                d.this.e.b(imageButton);
                if (z) {
                    d.this.e.a(d.this.b);
                }
            }
        });
    }

    private void d() {
        if ((this.i == null && this.j == null) ? false : true) {
            ViewUtils.removeFromParent(this.h);
            addView(this.h);
            c.a(this.h, new Runnable() {
                public final void run() {
                    d.this.e();
                }
            });
        }
        Objects.onNotNull(this.i, new Consumer() {
            public final void accept(Object obj) {
                d.this.a((c) obj);
            }
        });
        Objects.onNotNull(this.j, new Consumer() {
            public final void accept(Object obj) {
                d.this.a((b) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e() {
        this.f.l();
        this.e.e();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(c cVar) {
        cVar.a();
        this.i = null;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(b bVar) {
        bVar.a();
        this.j = null;
    }
}
