package com.smaato.sdk.richmedia.widget;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.R;

public final class a extends FrameLayout {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private FrameLayout f3632a = ((FrameLayout) findViewById(R.id.container));
    @NonNull
    private ImageButton b = ((ImageButton) findViewById(R.id.close));
    @Nullable
    private C0080a c;

    /* renamed from: com.smaato.sdk.richmedia.widget.a$a reason: collision with other inner class name */
    public interface C0080a {
        void onCloseClick();
    }

    public a(@NonNull Context context) {
        super(context);
        LayoutInflater.from(getContext()).inflate(R.layout.smaato_sdk_richmedia_layout_closable, this, true);
        this.b.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                a.this.b(view);
            }
        });
    }

    @NonNull
    public final ImageButton a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.f3632a.getChildCount() > 0 && getParent() != null;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull View view) {
        this.f3632a.addView(view, new LayoutParams(-1, -1));
    }

    public final void a(@Nullable C0080a aVar) {
        this.c = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@NonNull Rect rect, @NonNull Rect rect2) {
        Rect rect3 = new Rect();
        LayoutParams layoutParams = (LayoutParams) this.b.getLayoutParams();
        Gravity.apply(layoutParams.gravity, layoutParams.width, layoutParams.height, rect2, rect3);
        return rect.contains(rect3);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(View view) {
        Objects.onNotNull(this.c, $$Lambda$QdLEsWVLroYJv3RFkACj795kPI.INSTANCE);
    }
}
