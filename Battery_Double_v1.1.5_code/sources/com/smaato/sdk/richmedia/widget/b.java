package com.smaato.sdk.richmedia.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnShowListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.ViewUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.widget.a.C0080a;

final class b {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private Dialog f3633a;

    interface a {
        void a(@NonNull ImageButton imageButton);

        void b(@NonNull ImageButton imageButton);
    }

    b() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull View view, @NonNull a aVar) {
        Context context = view.getContext();
        a aVar2 = new a(context);
        aVar2.a((C0080a) new C0080a(aVar, aVar2) {
            private final /* synthetic */ a f$1;
            private final /* synthetic */ a f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onCloseClick() {
                b.this.b(this.f$1, this.f$2);
            }
        });
        ViewUtils.removeFromParent(view);
        aVar2.a(view);
        this.f3633a = new Dialog(context, 16973831);
        this.f3633a.setContentView(aVar2);
        this.f3633a.setCanceledOnTouchOutside(false);
        this.f3633a.setOnShowListener(new OnShowListener(aVar2) {
            private final /* synthetic */ a f$1;

            {
                this.f$1 = r2;
            }

            public final void onShow(DialogInterface dialogInterface) {
                a.this.a(this.f$1.a());
            }
        });
        this.f3633a.setOnKeyListener(new OnKeyListener(aVar, aVar2) {
            private final /* synthetic */ a f$1;
            private final /* synthetic */ a f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                return b.this.a(this.f$1, this.f$2, dialogInterface, i, keyEvent);
            }
        });
        this.f3633a.show();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(a aVar, a aVar2) {
        a(aVar, aVar2);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(a aVar, a aVar2, DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getAction() == 1) {
            a(aVar, aVar2);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        Objects.onNotNull(this.f3633a, new Consumer() {
            public final void accept(Object obj) {
                b.this.a((Dialog) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Dialog dialog) {
        dialog.dismiss();
        this.f3633a = null;
    }

    private static void a(@NonNull a aVar, @NonNull a aVar2) {
        aVar.b(aVar2.a());
    }
}
