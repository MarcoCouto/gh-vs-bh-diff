package com.smaato.sdk.richmedia.widget;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.ViewUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.widget.a.C0080a;
import com.smaato.sdk.richmedia.widget.c.a;

final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3634a;
    @NonNull
    private final Rect b;
    @NonNull
    private final View c;
    @NonNull
    private final a d;
    @Nullable
    private a e;

    public interface a {
        void a(@NonNull ImageButton imageButton);

        void a(@NonNull String str);

        void b(@NonNull ImageButton imageButton);
    }

    c(@NonNull Logger logger, @NonNull View view, @NonNull Rect rect) {
        this.f3634a = (Logger) Objects.requireNonNull(logger);
        this.c = (View) Objects.requireNonNull(view);
        this.b = (Rect) Objects.requireNonNull(rect);
        this.d = new a(view.getContext());
        this.d.a((C0080a) new C0080a() {
            public final void onCloseClick() {
                c.this.d();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(a aVar) {
        aVar.b(this.d.a());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d() {
        Objects.onNotNull(this.e, new Consumer() {
            public final void accept(Object obj) {
                c.this.c((a) obj);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Rect rect) {
        View rootView = ViewUtils.getRootView(this.c);
        if (!(rootView instanceof ViewGroup)) {
            a("Cannot find a root view for a resizable-view");
            return;
        }
        ViewGroup viewGroup = (ViewGroup) rootView;
        if (!this.d.a(this.b, rect)) {
            a("The close region cannot appear within the maximum allowed size");
            return;
        }
        if (!this.d.b()) {
            ViewUtils.removeFromParent(this.c);
            this.d.a(this.c);
            viewGroup.addView(this.d);
        }
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) this.d.getLayoutParams();
        marginLayoutParams.width = rect.width();
        marginLayoutParams.height = rect.height();
        marginLayoutParams.topMargin = rect.top;
        marginLayoutParams.leftMargin = rect.left;
        this.d.setLayoutParams(marginLayoutParams);
        com.smaato.sdk.richmedia.mraid.c.a(this.d, new Runnable() {
            public final void run() {
                c.this.c();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(a aVar) {
        aVar.a(this.d.a());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c() {
        Objects.onNotNull(this.e, new Consumer() {
            public final void accept(Object obj) {
                c.this.b((a) obj);
            }
        });
    }

    private void a(@NonNull String str) {
        this.f3634a.error(LogDomain.RICH_MEDIA, str, new Object[0]);
        Objects.onNotNull(this.e, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((a) obj).a(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        ViewUtils.removeFromParent(this.d);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        Threads.runOnNextUiFrame(new Runnable() {
            public final void run() {
                c.this.b();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable a aVar) {
        this.e = aVar;
    }
}
