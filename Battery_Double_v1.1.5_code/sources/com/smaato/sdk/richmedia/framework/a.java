package com.smaato.sdk.richmedia.framework;

import android.app.Application;
import android.view.OrientationEventListener;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.notifier.ChangeSender;

final class a extends OrientationEventListener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private ChangeSender<Whatever> f3596a;
    private int b;

    a(@NonNull Application application, @NonNull ChangeSender<Whatever> changeSender) {
        super(application);
        this.f3596a = (ChangeSender) Objects.requireNonNull(changeSender);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final ChangeSender<Whatever> a() {
        return this.f3596a;
    }

    public final void onOrientationChanged(int i) {
        if (i >= 0) {
            if (Math.abs(i - this.b) > 20) {
                this.b = i;
                this.f3596a.newValue(Whatever.INSTANCE);
            }
        }
    }
}
