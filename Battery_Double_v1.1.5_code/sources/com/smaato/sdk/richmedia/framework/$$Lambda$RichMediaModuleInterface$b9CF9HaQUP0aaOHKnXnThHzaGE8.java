package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$b9CF9HaQUP0aaOHKnXnThHzaGE8 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$b9CF9HaQUP0aaOHKnXnThHzaGE8 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$b9CF9HaQUP0aaOHKnXnThHzaGE8 INSTANCE = new $$Lambda$RichMediaModuleInterface$b9CF9HaQUP0aaOHKnXnThHzaGE8();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$b9CF9HaQUP0aaOHKnXnThHzaGE8() {
    }

    public final Object get(DiConstructor diConstructor) {
        return RichMediaModuleInterface.f(diConstructor);
    }
}
