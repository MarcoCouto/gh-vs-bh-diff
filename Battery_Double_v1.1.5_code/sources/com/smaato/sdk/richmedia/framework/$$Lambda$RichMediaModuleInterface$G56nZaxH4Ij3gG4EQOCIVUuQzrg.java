package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$G56nZaxH4Ij3gG4EQOCIVUuQzrg reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$G56nZaxH4Ij3gG4EQOCIVUuQzrg implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$G56nZaxH4Ij3gG4EQOCIVUuQzrg INSTANCE = new $$Lambda$RichMediaModuleInterface$G56nZaxH4Ij3gG4EQOCIVUuQzrg();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$G56nZaxH4Ij3gG4EQOCIVUuQzrg() {
    }

    public final Object get(DiConstructor diConstructor) {
        return RichMediaModuleInterface.k(diConstructor);
    }
}
