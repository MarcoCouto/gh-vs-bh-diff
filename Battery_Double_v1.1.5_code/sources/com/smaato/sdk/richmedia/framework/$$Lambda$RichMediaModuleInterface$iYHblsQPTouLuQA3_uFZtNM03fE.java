package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$iYHblsQPTouLuQA3_uFZtNM03fE reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$iYHblsQPTouLuQA3_uFZtNM03fE implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$iYHblsQPTouLuQA3_uFZtNM03fE INSTANCE = new $$Lambda$RichMediaModuleInterface$iYHblsQPTouLuQA3_uFZtNM03fE();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$iYHblsQPTouLuQA3_uFZtNM03fE() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(0.01d).visibilityTimeMillis(2000).build();
    }
}
