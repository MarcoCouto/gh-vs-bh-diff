package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$gjiiSh8r5pDCXmZAiAjEb35isdE reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$gjiiSh8r5pDCXmZAiAjEb35isdE implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$gjiiSh8r5pDCXmZAiAjEb35isdE INSTANCE = new $$Lambda$RichMediaModuleInterface$gjiiSh8r5pDCXmZAiAjEb35isdE();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$gjiiSh8r5pDCXmZAiAjEb35isdE() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(0.01d).visibilityTimeMillis(0).build();
    }
}
