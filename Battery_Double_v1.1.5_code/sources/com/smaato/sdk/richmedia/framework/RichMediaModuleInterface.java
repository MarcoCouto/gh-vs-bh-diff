package com.smaato.sdk.richmedia.framework;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.BuildConfig;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.util.PermissionChecker;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;
import com.smaato.sdk.richmedia.ad.c;
import com.smaato.sdk.richmedia.ad.g;
import com.smaato.sdk.richmedia.ad.k;
import com.smaato.sdk.richmedia.ad.tracker.b;
import com.smaato.sdk.richmedia.mraid.a;
import com.smaato.sdk.richmedia.mraid.dataprovider.h;
import com.smaato.sdk.richmedia.util.d;

public class RichMediaModuleInterface implements AdPresenterModuleInterface {
    @NonNull
    public String moduleDiName() {
        return "RichMediaModuleInterface";
    }

    @NonNull
    public String version() {
        return BuildConfig.VERSION_NAME;
    }

    public boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls) {
        return adFormat == AdFormat.RICH_MEDIA && (cls.isAssignableFrom(InterstitialAdPresenter.class) || cls.isAssignableFrom(BannerAdPresenter.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderPlugin k(DiConstructor diConstructor) {
        return new g((AdPresenterNameShaper) diConstructor.get(AdPresenterNameShaper.class), new NullableFunction() {
            public final Object apply(Object obj) {
                return RichMediaModuleInterface.a(DiConstructor.this, (String) obj);
            }
        });
    }

    @NonNull
    public ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory() {
        return $$Lambda$RichMediaModuleInterface$G56nZaxH4Ij3gG4EQOCIVUuQzrg.INSTANCE;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder a(DiConstructor diConstructor, String str) {
        return (AdPresenterBuilder) DiAdLayer.tryGetOrNull(diConstructor, str, AdPresenterBuilder.class);
    }

    @Nullable
    public DiRegistry moduleDiRegistry() {
        return DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                RichMediaModuleInterface.this.a((DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(a.class, $$Lambda$RichMediaModuleInterface$7fAvKZTijaJUUgTJLYeIpqZCLTE.INSTANCE);
        diRegistry.registerFactory(b.class, $$Lambda$RichMediaModuleInterface$5CcMEEt87IEMnut_TS0pRG4faDQ.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), d.class, $$Lambda$RichMediaModuleInterface$xtzWKPlHY4RzP3WVPzvipoErpfM.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), a.class, $$Lambda$RichMediaModuleInterface$taUcwyR8dRRQRWVZCLStkwKWw.INSTANCE);
        diRegistry.registerFactory(h.class, $$Lambda$RichMediaModuleInterface$b9CF9HaQUP0aaOHKnXnThHzaGE8.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), k.class, $$Lambda$RichMediaModuleInterface$XfsWYW3hQIH9DCv7ldS1iUk1A8o.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), VisibilityPrivateConfig.class, $$Lambda$RichMediaModuleInterface$gjiiSh8r5pDCXmZAiAjEb35isdE.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), b.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return RichMediaModuleInterface.this.c(diConstructor);
            }
        });
        diRegistry.registerFactory("RichMediaModuleInterfaceCLOSE_BUTTON_VISIBILITY_TRACKER_DI_NAME", VisibilityPrivateConfig.class, $$Lambda$RichMediaModuleInterface$iYHblsQPTouLuQA3_uFZtNM03fE.INSTANCE);
        diRegistry.registerFactory("RichMediaModuleInterfaceCLOSE_BUTTON_VISIBILITY_TRACKER_DI_NAME", b.class, $$Lambda$RichMediaModuleInterface$n1IMrPY6xRDo54ev71L8r7ldZtU.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a j(DiConstructor diConstructor) {
        return new a((Application) diConstructor.get(Application.class), ChangeSenderUtils.createDebounceChangeSender(Whatever.INSTANCE, 500));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b i(DiConstructor diConstructor) {
        return new b(DiLogLayer.getLoggerFrom(diConstructor), (a) diConstructor.get(a.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ d h(DiConstructor diConstructor) {
        return new d(DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a g(DiConstructor diConstructor) {
        a aVar = new a((PermissionChecker) diConstructor.get(PermissionChecker.class), (AppBackgroundAwareHandler) diConstructor.get(AppBackgroundAwareHandler.class), (b) diConstructor.get(b.class), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), DiLogLayer.getLoggerFrom(diConstructor), (LocationProvider) diConstructor.get(LocationProvider.class), (h) diConstructor.get(h.class));
        return aVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ h f(DiConstructor diConstructor) {
        return new h(h.b.LOADING);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ k e(DiConstructor diConstructor) {
        return new k(DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ b c(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get(moduleDiName(), VisibilityPrivateConfig.class);
        b bVar = new b(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class));
        return bVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b a(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get("RichMediaModuleInterfaceCLOSE_BUTTON_VISIBILITY_TRACKER_DI_NAME", VisibilityPrivateConfig.class);
        b bVar = new b(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class));
        return bVar;
    }

    @Nullable
    public DiRegistry moduleAdPresenterDiRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper) {
        return c.a(adPresenterNameShaper, moduleDiName(), "RichMediaModuleInterfaceCLOSE_BUTTON_VISIBILITY_TRACKER_DI_NAME");
    }

    @NonNull
    public ExpectedManifestEntries getExpectedManifestEntries() {
        return ExpectedManifestEntries.EMPTY;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("RichMediaModuleInterface{supportedFormat: ");
        sb.append(AdFormat.RICH_MEDIA);
        sb.append("}");
        return sb.toString();
    }
}
