package com.smaato.sdk.richmedia.framework;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.notifier.ChangeSender;

public final class b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3597a;
    @NonNull
    private final a b;

    b(@NonNull Logger logger, @NonNull a aVar) {
        this.f3597a = (Logger) Objects.requireNonNull(logger);
        this.b = (a) Objects.requireNonNull(aVar);
    }

    public final void a() {
        Threads.runOnUi(new Runnable() {
            public final void run() {
                b.this.d();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d() {
        if (this.b.canDetectOrientation()) {
            this.b.enable();
        } else {
            this.f3597a.error(LogDomain.MRAID, "This Android version cannot detect orientation changes", new Object[0]);
        }
    }

    public final void b() {
        a aVar = this.b;
        aVar.getClass();
        Threads.runOnUi(new Runnable() {
            public final void run() {
                a.this.disable();
            }
        });
    }

    @NonNull
    public final ChangeSender<Whatever> c() {
        return this.b.a();
    }
}
