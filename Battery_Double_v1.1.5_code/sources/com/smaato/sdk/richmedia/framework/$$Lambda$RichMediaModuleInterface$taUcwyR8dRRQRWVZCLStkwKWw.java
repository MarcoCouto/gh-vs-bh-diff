package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$taUcwyR8dRRQRW-VZCL-StkwKWw reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$taUcwyR8dRRQRWVZCLStkwKWw implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$taUcwyR8dRRQRWVZCLStkwKWw INSTANCE = new $$Lambda$RichMediaModuleInterface$taUcwyR8dRRQRWVZCLStkwKWw();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$taUcwyR8dRRQRWVZCLStkwKWw() {
    }

    public final Object get(DiConstructor diConstructor) {
        return RichMediaModuleInterface.g(diConstructor);
    }
}
