package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.List;

public final class h implements AdObject {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final SomaApiContext f3585a;
    @NonNull
    private final String b;
    private final int c;
    private final int d;
    @NonNull
    private final List<String> e;
    @NonNull
    private final List<String> f;
    @Nullable
    private final Object g;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private SomaApiContext f3586a;
        private String b;
        private int c;
        private int d;
        private List<String> e;
        private List<String> f;
        private Object g;

        @NonNull
        public final a a(@NonNull SomaApiContext somaApiContext) {
            this.f3586a = somaApiContext;
            return this;
        }

        @NonNull
        public final a a(@NonNull String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a a(int i) {
            this.c = i;
            return this;
        }

        @NonNull
        public final a b(int i) {
            this.d = i;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<String> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable Object obj) {
            this.g = obj;
            return this;
        }

        @NonNull
        public final h a() {
            ArrayList arrayList = new ArrayList();
            if (this.f3586a == null) {
                arrayList.add("somaApiContext");
            }
            if (this.b == null) {
                arrayList.add("content");
            }
            if (this.e == null) {
                arrayList.add("impressionTrackingUrls");
            }
            if (this.f == null) {
                arrayList.add("clickTrackingUrls");
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new IllegalStateException(sb.toString());
            } else if (this.e.isEmpty()) {
                throw new IllegalStateException("impressionTrackingUrls cannot be empty");
            } else if (!this.f.isEmpty()) {
                h hVar = new h(this.f3586a, this.b, this.c, this.d, this.e, this.f, this.g, 0);
                return hVar;
            } else {
                throw new IllegalStateException("clickTrackingUrls cannot be empty");
            }
        }
    }

    /* synthetic */ h(SomaApiContext somaApiContext, String str, int i, int i2, List list, List list2, Object obj, byte b2) {
        this(somaApiContext, str, i, i2, list, list2, obj);
    }

    private h(@NonNull SomaApiContext somaApiContext, @NonNull String str, int i, int i2, @NonNull List<String> list, @NonNull List<String> list2, @Nullable Object obj) {
        this.f3585a = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.b = (String) Objects.requireNonNull(str);
        this.c = i;
        this.d = i2;
        this.e = (List) Objects.requireNonNull(list);
        this.f = (List) Objects.requireNonNull(list2);
        this.g = obj;
    }

    @NonNull
    public final SomaApiContext getSomaApiContext() {
        return this.f3585a;
    }

    @NonNull
    public final String a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    @NonNull
    public final List<String> d() {
        return this.e;
    }

    @NonNull
    public final List<String> e() {
        return this.f;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("RichMediaAdObject{somaApiContext=");
        sb.append(this.f3585a);
        sb.append(", content='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", width=");
        sb.append(this.c);
        sb.append(", height=");
        sb.append(this.d);
        sb.append(", impressionTrackingUrls=");
        sb.append(this.e);
        sb.append(", clickTrackingUrls=");
        sb.append(this.f);
        sb.append(", extensions=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }
}
