package com.smaato.sdk.richmedia.ad.tracker;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.github.mikephil.charting.utils.Utils;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.PauseUnpauseListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import java.lang.ref.WeakReference;

public final class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private WeakReference<View> f3592a;
    private final double b;
    private final long c;
    @NonNull
    private final AppBackgroundAwareHandler d;
    @Nullable
    private c e;
    @NonNull
    private WeakReference<ViewTreeObserver> f = new WeakReference<>(null);
    /* access modifiers changed from: private */
    public boolean g = false;
    private boolean h = false;
    private boolean i = false;
    @NonNull
    private OnPreDrawListener j = new OnPreDrawListener() {
        public final boolean onPreDraw() {
            a.this.g = true;
            a.this.c();
            return true;
        }
    };

    @VisibleForTesting
    /* renamed from: com.smaato.sdk.richmedia.ad.tracker.a$a reason: collision with other inner class name */
    static class C0078a implements PauseUnpauseListener {
        @VisibleForTesting

        /* renamed from: a reason: collision with root package name */
        long f3594a;
        @VisibleForTesting
        boolean b = false;
        @VisibleForTesting
        long c = 0;
        @VisibleForTesting
        private long d = 0;

        C0078a(long j) {
            this.f3594a = j;
        }

        public final void onActionPaused() {
            this.d = SystemClock.uptimeMillis();
        }

        public final void onBeforeActionUnpaused() {
            long uptimeMillis = SystemClock.uptimeMillis() - this.d;
            this.d = 0;
            this.f3594a += uptimeMillis;
        }
    }

    a(@NonNull Logger logger, @NonNull View view, double d2, long j2, @NonNull c cVar, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler) {
        Objects.requireNonNull(logger);
        this.f3592a = new WeakReference<>(Objects.requireNonNull(view));
        if (d2 <= Utils.DOUBLE_EPSILON || d2 > 1.0d) {
            throw new IllegalArgumentException("visibilityRatio should be in range (0..1]");
        }
        this.b = d2;
        if (j2 >= 0) {
            this.c = j2;
            this.e = (c) Objects.requireNonNull(cVar);
            this.d = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
            View rootView = view.getRootView();
            if (rootView != null) {
                ViewTreeObserver viewTreeObserver = rootView.getViewTreeObserver();
                this.f = new WeakReference<>(viewTreeObserver);
                if (!viewTreeObserver.isAlive()) {
                    logger.error(LogDomain.AD, "Cannot start RichMediaVisibilityTracker due to no available root view", new Object[0]);
                } else {
                    viewTreeObserver.addOnPreDrawListener(this.j);
                    return;
                }
            } else {
                logger.error(LogDomain.AD, "Cannot start RichMediaVisibilityTracker due to no available root view", new Object[0]);
            }
            return;
        }
        throw new IllegalArgumentException("visibilityTimeInMillis should be bigger or equal to 0");
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.e != null && this.g && this.i && this.f3592a.get() != null && !this.h) {
            this.h = true;
            a(new C0078a(SystemClock.uptimeMillis()));
        }
    }

    @MainThread
    public final void a() {
        Threads.ensureMainThread();
        this.i = true;
        c();
    }

    private void a(@NonNull C0078a aVar) {
        this.d.postDelayed("rich-media visibility tracker", new Runnable(aVar) {
            private final /* synthetic */ C0078a f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                a.this.b(this.f$1);
            }
        }, 250, aVar);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(C0078a aVar) {
        View view = (View) this.f3592a.get();
        if (view != null) {
            long uptimeMillis = SystemClock.uptimeMillis();
            boolean a2 = d.a(view, this.b);
            if (aVar.b) {
                aVar.c += uptimeMillis - aVar.f3594a;
                if (aVar.c >= this.c) {
                    Objects.onNotNull(this.e, $$Lambda$7x2qUmnoqdud3mpBTHud5rXBWU.INSTANCE);
                    return;
                }
                aVar.f3594a = uptimeMillis;
                aVar.b = a2;
                a(aVar);
                return;
            }
            aVar.f3594a = uptimeMillis;
            aVar.b = a2;
            a(aVar);
        }
    }

    @MainThread
    public final void b() {
        Threads.ensureMainThread();
        this.d.stop();
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.f.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnPreDrawListener(this.j);
        }
        this.f3592a.clear();
        this.f.clear();
        this.e = null;
    }
}
