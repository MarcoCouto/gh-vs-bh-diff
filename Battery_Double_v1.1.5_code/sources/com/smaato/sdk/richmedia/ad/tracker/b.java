package com.smaato.sdk.richmedia.ad.tracker;

import android.view.View;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;

public class b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3595a;
    private final double b;
    private final long c;
    @NonNull
    private final AppBackgroundDetector d;

    public b(@NonNull Logger logger, double d2, long j, @NonNull AppBackgroundDetector appBackgroundDetector) {
        this.f3595a = (Logger) Objects.requireNonNull(logger);
        this.b = d2;
        this.c = j;
        this.d = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
    }

    @NonNull
    public final a a(@NonNull View view, @NonNull c cVar) {
        View view2 = view;
        a aVar = new a(this.f3595a, view2, this.b, this.c, cVar, new AppBackgroundAwareHandler(this.f3595a, Threads.newUiHandler(), this.d));
        return aVar;
    }
}
