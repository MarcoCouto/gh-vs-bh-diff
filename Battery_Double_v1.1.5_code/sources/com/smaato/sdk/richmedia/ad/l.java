package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.LatLng;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.util.Objects;

public class l {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final LatLng f3591a;
    public final float b;
    public final float c;

    private l(LatLng latLng, float f, float f2) {
        this.f3591a = latLng;
        this.b = f;
        this.c = f2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        l lVar = (l) obj;
        return this.f3591a != null ? this.f3591a.equals(lVar.f3591a) && this.b == lVar.b : lVar.f3591a == null && this.b == lVar.b;
    }

    public int hashCode() {
        return Objects.hash(this.f3591a, Float.valueOf(this.b));
    }

    @NonNull
    public static l a(LocationProvider locationProvider) {
        return new l(locationProvider.getLocationData(), locationProvider.getLocationAccuracy(), locationProvider.getTimeSinceLastLocationUpdate() / 1000.0f);
    }
}
