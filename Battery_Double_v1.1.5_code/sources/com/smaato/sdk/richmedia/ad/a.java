package com.smaato.sdk.richmedia.ad;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdInteractor.TtlListener;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.BannerAdPresenter.Listener;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.ad.tracker.a;
import com.smaato.sdk.richmedia.ad.tracker.b;
import com.smaato.sdk.richmedia.ad.tracker.c;
import com.smaato.sdk.richmedia.util.d;
import com.smaato.sdk.richmedia.widget.e;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

final class a implements BannerAdPresenter {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3570a;
    /* access modifiers changed from: private */
    @NonNull
    public final f b;
    @NonNull
    private final b c;
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<com.smaato.sdk.richmedia.ad.tracker.a> d = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public final AppBackgroundDetector e;
    @NonNull
    private final d f;
    @NonNull
    private final com.smaato.sdk.richmedia.mraid.a g;
    /* access modifiers changed from: private */
    @NonNull
    public final WebViewViewabilityTracker h;
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<com.smaato.sdk.richmedia.widget.d> i = new WeakReference<>(null);
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<Listener> j = new WeakReference<>(null);
    @NonNull
    private StateMachine.Listener<State> k;
    @NonNull
    private TtlListener l = new TtlListener() {
        public final void onTTLExpired(AdInteractor adInteractor) {
            a.this.a(adInteractor);
        }
    };

    /* renamed from: com.smaato.sdk.richmedia.ad.a$a reason: collision with other inner class name */
    final class C0077a implements com.smaato.sdk.richmedia.widget.d.a {
        private UrlResolveListener b;

        private C0077a() {
            this.b = new UrlResolveListener() {
                public final void onError() {
                    Threads.runOnUi(new Runnable(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$a$a$1$cJueGd0aUhPIN52pcD_0B09bnw4.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a() {
                    a.this.f3570a.error(LogDomain.AD, "The url seems to be invalid", new Object[0]);
                    Objects.onNotNull(a.this.j.get(), new Consumer(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$a$a$1$q3EXjo0mmVz13GW_qvhyvwF3ncA.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                    Objects.onNotNull(a.this.i.get(), $$Lambda$a$a$1$pVUaLx54KqbvGpheCpdRj579U.INSTANCE);
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Listener listener) {
                    listener.onAdError(a.this);
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    Threads.runOnUi(new Runnable(this, consumer) {
                        private final /* synthetic */ AnonymousClass1 f$0;
                        private final /* synthetic */ Consumer f$1;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$a$a$1$WpRi_PFmIdjvPC6Gs_N0-hvMByg.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Consumer consumer) {
                    Objects.onNotNull(a.this.i.get(), new Consumer(consumer) {
                        private final /* synthetic */ Consumer f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$a$a$1$_RDl1DqSwKvyEdXyNH6e6gI2HfU.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public static /* synthetic */ void a(Consumer consumer, com.smaato.sdk.richmedia.widget.d dVar) {
                    consumer.accept(dVar.getContext());
                    dVar.a(false);
                }
            };
        }

        /* synthetic */ C0077a(a aVar, byte b2) {
            this();
        }

        public final void a(@NonNull com.smaato.sdk.richmedia.widget.d dVar) {
            a.this.h.registerAdView(dVar.c());
            a.this.h.startTracking();
            Objects.onNotNull(a.this.d.get(), $$Lambda$2IOixLjhShDeZpLiQfrsdJVvkOk.INSTANCE);
        }

        public final void a() {
            if (a.this.e.isAppInBackground()) {
                a.this.f3570a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
            } else {
                a.this.b.onEvent(Event.CLICK);
            }
        }

        public final void a(@NonNull com.smaato.sdk.richmedia.widget.d dVar, @NonNull String str) {
            if (a.this.e.isAppInBackground()) {
                a.this.f3570a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
                return;
            }
            dVar.a(true);
            a.this.b.a(str, this.b);
        }

        public final void b() {
            if (a.this.e.isAppInBackground()) {
                a.this.f3570a.info(LogDomain.AD, "skipping expand event, because app is in background", new Object[0]);
            } else {
                Objects.onNotNull(a.this.j.get(), new Consumer() {
                    public final void accept(Object obj) {
                        C0077a.this.c((Listener) obj);
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void c(Listener listener) {
            listener.onAdExpanded(a.this);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b(Listener listener) {
            listener.onAdUnload(a.this);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void g() {
            Objects.onNotNull(a.this.j.get(), new Consumer() {
                public final void accept(Object obj) {
                    C0077a.this.b((Listener) obj);
                }
            });
        }

        public final void c() {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    C0077a.this.g();
                }
            });
        }

        public final void d() {
            if (a.this.e.isAppInBackground()) {
                a.this.f3570a.info(LogDomain.AD, "skipping resize event, because app is in background", new Object[0]);
            } else {
                Objects.onNotNull(a.this.j.get(), $$Lambda$cYYIRl5OnGn12VImG_S6TxguU.INSTANCE);
            }
        }

        public final void e() {
            Objects.onNotNull(a.this.j.get(), $$Lambda$DnClyV8LMK4oneag9X9lq8FTdY.INSTANCE);
        }

        public final void a(@NonNull String str, @Nullable String str2) {
            a.this.b.a(str, str2);
        }

        public final void a(@NonNull View view) {
            a.this.h.registerFriendlyObstruction(view);
        }

        public final void b(@NonNull View view) {
            a.this.h.removeFriendlyObstruction(view);
        }

        public final void a(@NonNull e eVar) {
            a.this.h.updateAdView(eVar);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Listener listener) {
            listener.onAdError(a.this);
        }

        public final void f() {
            Objects.onNotNull(a.this.j.get(), new Consumer() {
                public final void accept(Object obj) {
                    C0077a.this.a((Listener) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdInteractor adInteractor) {
        Objects.onNotNull(this.j.get(), new Consumer() {
            public final void accept(Object obj) {
                a.this.c((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onTTLExpired(this);
    }

    a(@NonNull Logger logger, @NonNull f fVar, @NonNull b bVar, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull d dVar, @NonNull com.smaato.sdk.richmedia.mraid.a aVar, @NonNull WebViewViewabilityTracker webViewViewabilityTracker) {
        this.f3570a = (Logger) Objects.requireNonNull(logger);
        this.b = (f) Objects.requireNonNull(fVar);
        this.c = (b) Objects.requireNonNull(bVar);
        this.e = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.f = (d) Objects.requireNonNull(dVar);
        this.g = (com.smaato.sdk.richmedia.mraid.a) Objects.requireNonNull(aVar);
        this.h = (WebViewViewabilityTracker) Objects.requireNonNull(webViewViewabilityTracker);
        this.k = new StateMachine.Listener(logger) {
            private final /* synthetic */ Logger f$1;

            {
                this.f$1 = r2;
            }

            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                a.this.a(this.f$1, (State) obj, (State) obj2, metadata);
            }
        };
        fVar.addStateListener(this.k);
        fVar.addTtlListener(this.l);
        fVar.a((com.smaato.sdk.richmedia.ad.f.a) new com.smaato.sdk.richmedia.ad.f.a() {
            public final void onImpressionTriggered() {
                a.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, State state, State state2, Metadata metadata) {
        switch (state2) {
            case CREATED:
            case IMPRESSED:
                return;
            case ON_SCREEN:
                Objects.onNotNull(this.i.get(), $$Lambda$C_rc7MrrbgUquxtxk2TeXB7f8Sg.INSTANCE);
                return;
            case CLICKED:
                Objects.onNotNull(this.j.get(), new Consumer() {
                    public final void accept(Object obj) {
                        a.this.b((Listener) obj);
                    }
                });
                return;
            case COMPLETE:
                return;
            case TO_BE_DELETED:
                return;
            default:
                logger.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onAdImpressed(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        Objects.onNotNull(this.j.get(), new Consumer() {
            public final void accept(Object obj) {
                a.this.a((Listener) obj);
            }
        });
    }

    public final void initialize() {
        this.b.onEvent(Event.INITIALISE);
    }

    @MainThread
    @NonNull
    public final AdContentView getAdContentView(@NonNull Context context) {
        Objects.onNotNull(this.i.get(), $$Lambda$BPax89SfCIeF4XSooRuSj78sPQ.INSTANCE);
        h hVar = (h) this.b.getAdObject();
        com.smaato.sdk.richmedia.widget.d a2 = this.g.a(context, hVar, (com.smaato.sdk.richmedia.widget.d.a) new C0077a(this, 0), this.f);
        a2.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(@NonNull View view) {
                a.this.b.onEvent(Event.ADDED_ON_SCREEN);
            }

            public final void onViewDetachedFromWindow(@NonNull View view) {
                a.this.b.onEvent(Event.REMOVED_FROM_SCREEN);
                view.removeOnAttachStateChangeListener(this);
            }
        });
        this.d.set(this.c.a(a2, new c() {
            public final void onVisibilityHappen() {
                a.this.a();
            }
        }));
        this.i = new WeakReference<>(a2);
        return a2;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        this.b.onEvent(Event.IMPRESSION);
    }

    public final void setListener(@Nullable Listener listener) {
        this.j = new WeakReference<>(listener);
    }

    @NonNull
    public final AdInteractor<? extends AdObject> getAdInteractor() {
        return this.b;
    }

    @NonNull
    public final String getPublisherId() {
        return this.b.getPublisherId();
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.b.getAdSpaceId();
    }

    @NonNull
    public final String getSessionId() {
        return this.b.getSessionId();
    }

    @Nullable
    public final String getCreativeId() {
        return this.b.getCreativeId();
    }

    public final boolean isValid() {
        return this.b.isValid();
    }

    @MainThread
    public final void destroy() {
        Threads.ensureMainThread();
        this.h.stopTracking();
        this.b.a();
        Objects.onNotNull(this.d.get(), new Consumer() {
            public final void accept(Object obj) {
                a.this.a((a) obj);
            }
        });
        Objects.onNotNull(this.i.get(), new Consumer() {
            public final void accept(Object obj) {
                a.this.a((com.smaato.sdk.richmedia.widget.d) obj);
            }
        });
        this.j.clear();
        if (!isValid()) {
            this.b.removeStateListener(this.k);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(com.smaato.sdk.richmedia.ad.tracker.a aVar) {
        this.d.set(null);
        aVar.b();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(com.smaato.sdk.richmedia.widget.d dVar) {
        this.i.clear();
        dVar.b();
    }
}
