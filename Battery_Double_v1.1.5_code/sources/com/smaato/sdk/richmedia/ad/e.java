package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.richmedia.ad.tracker.b;
import com.smaato.sdk.richmedia.mraid.a;
import com.smaato.sdk.richmedia.util.d;

final class e extends i<InterstitialAdPresenter> {
    e(@NonNull Logger logger, @NonNull k kVar, @NonNull b bVar, @NonNull b bVar2, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull d dVar, @NonNull a aVar, @NonNull Function<h, f> function, @NonNull WebViewViewabilityTracker webViewViewabilityTracker) {
        $$Lambda$e$Ax7A_IAAsNmX3G5XtAVq3YROEA4 r0 = new Function(bVar, bVar2, appBackgroundDetector, dVar, aVar, webViewViewabilityTracker) {
            private final /* synthetic */ b f$1;
            private final /* synthetic */ b f$2;
            private final /* synthetic */ AppBackgroundDetector f$3;
            private final /* synthetic */ d f$4;
            private final /* synthetic */ a f$5;
            private final /* synthetic */ WebViewViewabilityTracker f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            public final Object apply(Object obj) {
                return e.a(Logger.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (f) obj);
            }
        };
        k kVar2 = kVar;
        super(logger, kVar, function, r0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ InterstitialAdPresenter a(Logger logger, b bVar, b bVar2, AppBackgroundDetector appBackgroundDetector, d dVar, a aVar, WebViewViewabilityTracker webViewViewabilityTracker, f fVar) {
        d dVar2 = new d(logger, fVar, bVar, bVar2, appBackgroundDetector, dVar, aVar, webViewViewabilityTracker);
        return dVar2;
    }
}
