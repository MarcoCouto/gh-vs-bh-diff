package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.richmedia.ad.tracker.b;
import com.smaato.sdk.richmedia.mraid.a;
import com.smaato.sdk.richmedia.util.d;

final class b extends i<BannerAdPresenter> {
    b(@NonNull Logger logger, @NonNull k kVar, @NonNull com.smaato.sdk.richmedia.ad.tracker.b bVar, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull d dVar, @NonNull a aVar, @NonNull Function<h, f> function, @NonNull WebViewViewabilityTracker webViewViewabilityTracker) {
        $$Lambda$b$y5hW0FVxGngboH4zWjgwK63I6Cc r0 = new Function(bVar, appBackgroundDetector, dVar, aVar, webViewViewabilityTracker) {
            private final /* synthetic */ b f$1;
            private final /* synthetic */ AppBackgroundDetector f$2;
            private final /* synthetic */ d f$3;
            private final /* synthetic */ a f$4;
            private final /* synthetic */ WebViewViewabilityTracker f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            public final Object apply(Object obj) {
                return b.a(Logger.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, (f) obj);
            }
        };
        k kVar2 = kVar;
        Function<h, f> function2 = function;
        super(logger, kVar, function, r0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BannerAdPresenter a(Logger logger, com.smaato.sdk.richmedia.ad.tracker.b bVar, AppBackgroundDetector appBackgroundDetector, d dVar, a aVar, WebViewViewabilityTracker webViewViewabilityTracker, f fVar) {
        a aVar2 = new a(logger, fVar, bVar, appBackgroundDetector, dVar, aVar, webViewViewabilityTracker);
        return aVar2;
    }
}
