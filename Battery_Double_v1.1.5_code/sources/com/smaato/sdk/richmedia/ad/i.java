package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Error;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Listener;
import com.smaato.sdk.core.ad.AdPresenterBuilderException;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.richmedia.ad.h.a;
import java.io.UnsupportedEncodingException;

abstract class i<Presenter extends AdPresenter> implements AdPresenterBuilder {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3587a;
    @NonNull
    private final k b;
    @NonNull
    private final Function<h, f> c;
    @NonNull
    private final Function<f, Presenter> d;

    i(@NonNull Logger logger, @NonNull k kVar, @NonNull Function<h, f> function, @NonNull Function<f, Presenter> function2) {
        this.f3587a = (Logger) Objects.requireNonNull(logger);
        this.b = (k) Objects.requireNonNull(kVar);
        this.c = (Function) Objects.requireNonNull(function);
        this.d = (Function) Objects.requireNonNull(function2);
    }

    public void buildAdPresenter(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        Objects.requireNonNull(somaApiContext);
        h a2 = a(somaApiContext, listener);
        if (a2 != null) {
            this.f3587a.info(LogDomain.AD, "parsed RichMediaAdObject = %s", a2);
            listener.onAdPresenterBuildSuccess(this, (AdPresenter) this.d.apply((f) this.c.apply(a2)));
        }
    }

    @Nullable
    private h a(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        ApiAdResponse apiAdResponse = somaApiContext.getApiAdResponse();
        try {
            try {
                j a2 = this.b.a(new String(apiAdResponse.getBody(), apiAdResponse.getCharset()));
                try {
                    return new a().a(somaApiContext).a(a2.b()).b(a2.c()).a(a2.a()).b(a2.e()).a(a2.d()).a(a2.f()).a();
                } catch (Exception e) {
                    this.f3587a.error(LogDomain.AD, e, "Failed to build RichMediaAdObject", new Object[0]);
                    listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e));
                    return null;
                }
            } catch (a e2) {
                this.f3587a.error(LogDomain.AD, e2, "Invalid AdResponse: %s", apiAdResponse);
                listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e2));
                return null;
            }
        } catch (UnsupportedEncodingException e3) {
            this.f3587a.error(LogDomain.AD, e3, "Invalid AdResponse: %s. Cannot parse AdResponse with provided charset: %s", apiAdResponse, apiAdResponse.getCharset());
            listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e3));
            return null;
        }
    }
}
