package com.smaato.sdk.richmedia.ad;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.tracker.ImpressionDetector;
import com.smaato.sdk.core.tracker.ImpressionDetector.Callback;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Listener;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.violationreporter.AdQualityViolationReporter;
import java.util.concurrent.atomic.AtomicReference;

final class f extends AdInteractor<h> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3581a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final LinkResolver c;
    @NonNull
    private final AdQualityViolationReporter d;
    /* access modifiers changed from: private */
    @NonNull
    public AtomicReference<Task> e = new AtomicReference<>();
    @Nullable
    private a f;

    public interface a {
        void onImpressionTriggered();
    }

    f(@NonNull Logger logger, @NonNull h hVar, @NonNull BeaconTracker beaconTracker, @NonNull StateMachine<Event, State> stateMachine, @NonNull LinkResolver linkResolver, @NonNull AdQualityViolationReporter adQualityViolationReporter, @NonNull OneTimeActionFactory oneTimeActionFactory, @NonNull ImpressionDetector impressionDetector) {
        super(hVar, stateMachine, oneTimeActionFactory);
        this.f3581a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.c = (LinkResolver) Objects.requireNonNull(linkResolver);
        this.d = (AdQualityViolationReporter) Objects.requireNonNull(adQualityViolationReporter);
        stateMachine.addListener(new Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                f.this.a((State) obj, (State) obj2, metadata);
            }
        });
        stateMachine.addListener(impressionDetector.stateListener);
        $$Lambda$f$wzIDA9_xJsnTtai8zcIJ1_1YQ r0 = new Callback(impressionDetector, logger, beaconTracker, hVar) {
            private final /* synthetic */ ImpressionDetector f$1;
            private final /* synthetic */ Logger f$2;
            private final /* synthetic */ BeaconTracker f$3;
            private final /* synthetic */ h f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void onImpressionStateDetected() {
                f.this.a(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        impressionDetector.setOnImpressionStateDetectedCallback(r0);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(ImpressionDetector impressionDetector, Logger logger, BeaconTracker beaconTracker, h hVar) {
        impressionDetector.setOnImpressionStateDetectedCallback(null);
        logger.debug(LogDomain.AD, "Going to send impression beacons", new Object[0]);
        beaconTracker.trackBeaconUrls(hVar.d(), hVar.getSomaApiContext());
        Objects.onNotNull(this.f, $$Lambda$MZYuUbnmgMxXtleX0xq3IETes1c.INSTANCE);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull State state, @NonNull State state2, @Nullable Metadata metadata) {
        switch (state2) {
            case CREATED:
            case ON_SCREEN:
            case IMPRESSED:
                return;
            case CLICKED:
                this.f3581a.debug(LogDomain.AD, "event %s: going to send click beacons", state2);
                h hVar = (h) getAdObject();
                this.b.trackBeaconUrls(hVar.e(), hVar.getSomaApiContext());
                return;
            case COMPLETE:
            case TO_BE_DELETED:
                return;
            default:
                this.f3581a.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    public final boolean isValid() {
        return (this.stateMachine.getCurrentState() == State.TO_BE_DELETED || this.stateMachine.getCurrentState() == State.COMPLETE) ? false : true;
    }

    public final void a(@NonNull String str, @NonNull final UrlResolveListener urlResolveListener) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(urlResolveListener);
        if (this.e.get() == null) {
            Task handleClickThroughUrl = this.c.handleClickThroughUrl(((h) getAdObject()).getSomaApiContext(), str, new UrlResolveListener() {
                public final void onError() {
                    f.this.e.set(null);
                    urlResolveListener.onError();
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    f.this.e.set(null);
                    urlResolveListener.onSuccess(consumer);
                }
            });
            this.e.set(handleClickThroughUrl);
            handleClickThroughUrl.start();
        }
    }

    public final void a() {
        Objects.onNotNull(this.e.get(), $$Lambda$yaUpY0DGpVsMZlshO_jwvMPRY4g.INSTANCE);
        this.e.set(null);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str, @Nullable String str2) {
        this.d.reportAdViolation(str, ((h) getAdObject()).getSomaApiContext(), str2);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable a aVar) {
        this.f = aVar;
    }
}
