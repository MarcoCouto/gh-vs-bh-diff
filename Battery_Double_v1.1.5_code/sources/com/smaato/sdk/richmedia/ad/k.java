package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Jsons;
import com.smaato.sdk.core.util.Objects;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class k {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3590a;

    static class a extends Exception {
        a(String str, Throwable th) {
            super(str, th);
        }
    }

    public k(@NonNull Logger logger) {
        this.f3590a = (Logger) Objects.requireNonNull(logger);
    }

    @NonNull
    public final j a(@NonNull String str) throws a {
        com.smaato.sdk.richmedia.ad.j.a aVar = new com.smaato.sdk.richmedia.ad.j.a();
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject("richmedia");
            List stringList = Jsons.toStringList(jSONObject.getJSONArray("impressiontrackers"));
            List stringList2 = Jsons.toStringList(jSONObject.getJSONArray("clicktrackers"));
            JSONObject jSONObject2 = jSONObject.getJSONObject("mediadata");
            aVar.a(jSONObject2.getString("content")).a(Integer.parseInt(jSONObject2.getString("w"))).b(Integer.parseInt(jSONObject2.getString("h"))).a(stringList).b(stringList2);
            return aVar.a();
        } catch (NumberFormatException | JSONException e) {
            String format = String.format("Invalid JSON content: %s", new Object[]{str});
            this.f3590a.error(LogDomain.AD, e, format, new Object[0]);
            throw new a(format, e);
        } catch (Exception e2) {
            String str2 = "Cannot build RichMediaAdResponse due to validation error";
            this.f3590a.error(LogDomain.AD, e2, str2, new Object[0]);
            throw new a(str2, e2);
        }
    }
}
