package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.List;

final class j {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3588a;
    private final int b;
    private final int c;
    @NonNull
    private final List<String> d;
    @NonNull
    private final List<String> e;
    @Nullable
    private final Object f;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private String f3589a;
        private int b;
        private int c;
        private List<String> d;
        private List<String> e;

        @NonNull
        public final a a(@NonNull String str) {
            this.f3589a = str;
            return this;
        }

        @NonNull
        public final a a(int i) {
            this.b = i;
            return this;
        }

        @NonNull
        public final a b(int i) {
            this.c = i;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public final j a() {
            ArrayList arrayList = new ArrayList();
            if (this.f3589a == null) {
                arrayList.add("content");
            }
            if (this.d == null) {
                arrayList.add("impressionTrackingUrls");
            }
            if (this.e == null) {
                arrayList.add("clickTrackingUrls");
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new IllegalStateException(sb.toString());
            } else if (this.d.isEmpty()) {
                throw new IllegalStateException("impressionTrackingUrls cannot be empty");
            } else if (!this.e.isEmpty()) {
                j jVar = new j(this.f3589a, this.b, this.c, this.d, this.e, null, 0);
                return jVar;
            } else {
                throw new IllegalStateException("clickTrackingUrls cannot be empty");
            }
        }
    }

    /* synthetic */ j(String str, int i, int i2, List list, List list2, Object obj, byte b2) {
        this(str, i, i2, list, list2, obj);
    }

    private j(@NonNull String str, int i, int i2, @NonNull List<String> list, @NonNull List<String> list2, @Nullable Object obj) {
        this.f3588a = (String) Objects.requireNonNull(str);
        this.b = i;
        this.c = i2;
        this.d = (List) Objects.requireNonNull(list);
        this.e = (List) Objects.requireNonNull(list2);
        this.f = obj;
    }

    @NonNull
    public final String a() {
        return this.f3588a;
    }

    public final int b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    @NonNull
    public final List<String> d() {
        return this.d;
    }

    @NonNull
    public final List<String> e() {
        return this.e;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("RichMediaAdResponse{content='");
        sb.append(this.f3588a);
        sb.append('\'');
        sb.append(", width=");
        sb.append(this.b);
        sb.append(", height=");
        sb.append(this.c);
        sb.append(", impressionTrackingUrls=");
        sb.append(this.d);
        sb.append(", clickTrackingUrls=");
        sb.append(this.e);
        sb.append(", extensions=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }

    @Nullable
    public final Object f() {
        return this.f;
    }
}
