package com.smaato.sdk.richmedia.ad;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter.Listener;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.ad.tracker.a;
import com.smaato.sdk.richmedia.ad.tracker.b;
import com.smaato.sdk.richmedia.ad.tracker.c;
import com.smaato.sdk.richmedia.widget.d;
import com.smaato.sdk.richmedia.widget.e;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

final class d implements InterstitialAdPresenter {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3576a;
    /* access modifiers changed from: private */
    @NonNull
    public final f b;
    @NonNull
    private final b c;
    @NonNull
    private final b d;
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<com.smaato.sdk.richmedia.ad.tracker.a> e = new AtomicReference<>();
    @NonNull
    private final AtomicReference<com.smaato.sdk.richmedia.ad.tracker.a> f = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public final AppBackgroundDetector g;
    @NonNull
    private final com.smaato.sdk.richmedia.util.d h;
    @NonNull
    private final com.smaato.sdk.richmedia.mraid.a i;
    /* access modifiers changed from: private */
    @NonNull
    public final WebViewViewabilityTracker j;
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<Listener> k = new WeakReference<>(null);
    @NonNull
    private StateMachine.Listener<State> l;
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<com.smaato.sdk.richmedia.widget.d> m = new WeakReference<>(null);
    /* access modifiers changed from: private */
    @NonNull
    public final List<WeakReference<View>> n = Collections.synchronizedList(new ArrayList());

    final class a implements com.smaato.sdk.richmedia.widget.d.a {
        private UrlResolveListener b;

        public final void a(@NonNull e eVar) {
        }

        public final void b() {
        }

        public final void d() {
        }

        public final void e() {
        }

        private a() {
            this.b = new UrlResolveListener() {
                public final void onError() {
                    Threads.runOnUi(new Runnable(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$d$a$1$kCaasOqNmnUUQ__OhOtZ_Yq1_V8.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a() {
                    d.this.f3576a.error(LogDomain.AD, "The url seems to be invalid", new Object[0]);
                    Objects.onNotNull(d.this.m.get(), $$Lambda$d$a$1$SeoVR008hSlDRDtrJaDQz2vt3Ew.INSTANCE);
                    Objects.onNotNull(d.this.k.get(), new Consumer(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$d$a$1$EtA4MR06i6e93BOGdgmpP3N9a8U.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Listener listener) {
                    listener.onAdError(d.this);
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    Threads.runOnUi(new Runnable(this, consumer) {
                        private final /* synthetic */ AnonymousClass1 f$0;
                        private final /* synthetic */ Consumer f$1;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$d$a$1$oYrV8ImmL7rlk_KPTBkg6XYVYi4.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Consumer consumer) {
                    Objects.onNotNull(d.this.m.get(), new Consumer(consumer) {
                        private final /* synthetic */ Consumer f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$d$a$1$MWfrryg7wzOkYc7vgaDqg5p8DJ8.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public static /* synthetic */ void a(Consumer consumer, com.smaato.sdk.richmedia.widget.d dVar) {
                    consumer.accept(dVar.getContext());
                    dVar.a(false);
                }
            };
        }

        /* synthetic */ a(d dVar, byte b2) {
            this();
        }

        public final void a(@NonNull com.smaato.sdk.richmedia.widget.d dVar) {
            d.this.j.registerAdView(dVar.c());
            for (WeakReference weakReference : d.this.n) {
                Object obj = weakReference.get();
                WebViewViewabilityTracker e = d.this.j;
                e.getClass();
                Objects.onNotNull(obj, new Consumer() {
                    public final void accept(Object obj) {
                        WebViewViewabilityTracker.this.registerFriendlyObstruction((View) obj);
                    }
                });
            }
            Objects.onNotNull(d.this.e.get(), $$Lambda$2IOixLjhShDeZpLiQfrsdJVvkOk.INSTANCE);
        }

        public final void a() {
            if (d.this.g.isAppInBackground()) {
                d.this.f3576a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
            } else {
                d.this.b.onEvent(Event.CLICK);
            }
        }

        public final void a(@NonNull com.smaato.sdk.richmedia.widget.d dVar, @NonNull String str) {
            if (d.this.g.isAppInBackground()) {
                d.this.f3576a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
                return;
            }
            dVar.a(true);
            d.this.b.a(str, this.b);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b(Listener listener) {
            listener.onAdUnload(d.this);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void g() {
            Objects.onNotNull(d.this.k.get(), new Consumer() {
                public final void accept(Object obj) {
                    a.this.b((Listener) obj);
                }
            });
        }

        public final void c() {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    a.this.g();
                }
            });
        }

        public final void a(@NonNull String str, @Nullable String str2) {
            d.this.b.a(str, str2);
        }

        public final void a(@NonNull View view) {
            d.this.j.registerFriendlyObstruction(view);
        }

        public final void b(@NonNull View view) {
            d.this.j.removeFriendlyObstruction(view);
        }

        public final void f() {
            Objects.onNotNull(d.this.k.get(), new Consumer() {
                public final void accept(Object obj) {
                    a.this.a((Listener) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Listener listener) {
            listener.onAdError(d.this);
            listener.onClose(d.this);
        }
    }

    d(@NonNull Logger logger, @NonNull f fVar, @NonNull b bVar, @NonNull b bVar2, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull com.smaato.sdk.richmedia.util.d dVar, @NonNull com.smaato.sdk.richmedia.mraid.a aVar, @NonNull WebViewViewabilityTracker webViewViewabilityTracker) {
        this.f3576a = (Logger) Objects.requireNonNull(logger);
        this.b = (f) Objects.requireNonNull(fVar);
        this.c = (b) Objects.requireNonNull(bVar);
        this.d = (b) Objects.requireNonNull(bVar2);
        this.g = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.h = (com.smaato.sdk.richmedia.util.d) Objects.requireNonNull(dVar);
        this.i = (com.smaato.sdk.richmedia.mraid.a) Objects.requireNonNull(aVar);
        this.j = (WebViewViewabilityTracker) Objects.requireNonNull(webViewViewabilityTracker);
        this.l = new StateMachine.Listener(logger) {
            private final /* synthetic */ Logger f$1;

            {
                this.f$1 = r2;
            }

            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                d.this.a(this.f$1, (State) obj, (State) obj2, metadata);
            }
        };
        fVar.addStateListener(this.l);
        fVar.a((com.smaato.sdk.richmedia.ad.f.a) new com.smaato.sdk.richmedia.ad.f.a(webViewViewabilityTracker) {
            private final /* synthetic */ WebViewViewabilityTracker f$1;

            {
                this.f$1 = r2;
            }

            public final void onImpressionTriggered() {
                d.this.a(this.f$1);
            }
        });
        fVar.onEvent(Event.INITIALISE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, State state, State state2, Metadata metadata) {
        switch (state2) {
            case CREATED:
            case IMPRESSED:
                return;
            case ON_SCREEN:
                Objects.onNotNull(this.m.get(), $$Lambda$C_rc7MrrbgUquxtxk2TeXB7f8Sg.INSTANCE);
                Objects.onNotNull(this.f.get(), $$Lambda$2IOixLjhShDeZpLiQfrsdJVvkOk.INSTANCE);
                return;
            case CLICKED:
                Objects.onNotNull(this.k.get(), new Consumer() {
                    public final void accept(Object obj) {
                        d.this.b((Listener) obj);
                    }
                });
                return;
            case COMPLETE:
                return;
            default:
                logger.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(WebViewViewabilityTracker webViewViewabilityTracker) {
        Objects.onNotNull(this.k.get(), new Consumer(webViewViewabilityTracker) {
            private final /* synthetic */ WebViewViewabilityTracker f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                d.this.a(this.f$1, (Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(WebViewViewabilityTracker webViewViewabilityTracker, Listener listener) {
        listener.onAdImpressed(this);
        webViewViewabilityTracker.startTracking();
    }

    @MainThread
    @NonNull
    public final AdContentView getAdContentView(@NonNull Context context) {
        h hVar = (h) this.b.getAdObject();
        com.smaato.sdk.richmedia.widget.d b2 = this.i.b(context, hVar, new a(this, 0), this.h);
        b2.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(View view) {
                d.this.b.onEvent(Event.ADDED_ON_SCREEN);
            }

            public final void onViewDetachedFromWindow(View view) {
                d.this.b.onEvent(Event.REMOVED_FROM_SCREEN);
                view.removeOnAttachStateChangeListener(this);
            }
        });
        com.smaato.sdk.richmedia.ad.tracker.a a2 = this.c.a(b2, new c() {
            public final void onVisibilityHappen() {
                d.this.b();
            }
        });
        com.smaato.sdk.richmedia.ad.tracker.a a3 = this.d.a(b2, new c() {
            public final void onVisibilityHappen() {
                d.this.a();
            }
        });
        this.e.set(a2);
        this.f.set(a3);
        this.m = new WeakReference<>(b2);
        return b2;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        this.b.onEvent(Event.IMPRESSION);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        Objects.onNotNull(this.k.get(), $$Lambda$nk0D4u8wrNMCMfE3WzHxisIX_k.INSTANCE);
    }

    @NonNull
    public final AdInteractor<? extends AdObject> getAdInteractor() {
        return this.b;
    }

    @NonNull
    public final String getPublisherId() {
        return this.b.getPublisherId();
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.b.getAdSpaceId();
    }

    @NonNull
    public final String getSessionId() {
        return this.b.getSessionId();
    }

    @Nullable
    public final String getCreativeId() {
        return this.b.getCreativeId();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onClose(this);
    }

    public final void onCloseClicked() {
        Objects.onNotNull(this.k.get(), new Consumer() {
            public final void accept(Object obj) {
                d.this.a((Listener) obj);
            }
        });
    }

    public final void setListener(@Nullable Listener listener) {
        this.k = new WeakReference<>(listener);
    }

    public final void setFriendlyObstructionView(@NonNull View view) {
        this.n.add(new WeakReference(view));
    }

    public final boolean isValid() {
        return this.b.isValid();
    }

    @MainThread
    public final void destroy() {
        Threads.ensureMainThread();
        this.b.a();
        this.n.clear();
        this.j.stopTracking();
        Objects.onNotNull(this.e.get(), new Consumer() {
            public final void accept(Object obj) {
                d.this.b((a) obj);
            }
        });
        Objects.onNotNull(this.f.get(), new Consumer() {
            public final void accept(Object obj) {
                d.this.a((a) obj);
            }
        });
        Objects.onNotNull(this.m.get(), new Consumer() {
            public final void accept(Object obj) {
                d.this.a((d) obj);
            }
        });
        if (!isValid()) {
            this.b.removeStateListener(this.l);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(com.smaato.sdk.richmedia.ad.tracker.a aVar) {
        this.e.set(null);
        aVar.b();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(com.smaato.sdk.richmedia.ad.tracker.a aVar) {
        this.f.set(null);
        aVar.b();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(com.smaato.sdk.richmedia.widget.d dVar) {
        this.m.clear();
        dVar.b();
    }
}
