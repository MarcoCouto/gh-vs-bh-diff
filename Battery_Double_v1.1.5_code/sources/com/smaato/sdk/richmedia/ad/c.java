package com.smaato.sdk.richmedia.ad;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.media.AudioManager;
import android.net.Uri;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.google.android.exoplayer2.util.MimeTypes;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.analytics.Analytics;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.tracker.ImpressionDetector;
import com.smaato.sdk.core.util.Intents;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.PermissionChecker;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.violationreporter.AdQualityViolationReporter;
import com.smaato.sdk.richmedia.ad.tracker.b;
import com.smaato.sdk.richmedia.util.d;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class c {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final String f3575a;

    private interface a extends Function<h, f> {
    }

    @NonNull
    public static DiRegistry a(@NonNull AdPresenterNameShaper adPresenterNameShaper, @NonNull String str, @NonNull String str2) {
        Objects.requireNonNull(adPresenterNameShaper);
        Objects.requireNonNull(str);
        return DiRegistry.of(new Consumer(str, str2) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                c.a(AdPresenterNameShaper.this, this.f$1, this.f$2, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AdPresenterNameShaper adPresenterNameShaper, String str, String str2, DiRegistry diRegistry) {
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.RICH_MEDIA, InterstitialAdPresenter.class), AdPresenterBuilder.class, new ClassFactory(str, str2) {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final Object get(DiConstructor diConstructor) {
                return c.a(this.f$0, this.f$1, diConstructor);
            }
        });
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.RICH_MEDIA, BannerAdPresenter.class), AdPresenterBuilder.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return c.c(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory("RichMediaModuleInterfacebannerRichMedia", a.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return c.b(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory("RichMediaModuleInterfaceinterstitialRichMedia", a.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return c.a(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory(str, StateMachine.class, $$Lambda$c$0jacmfunWQd6HTJtjfD3PrDWQ.INSTANCE);
        diRegistry.registerFactory("RichMediaModuleInterfacebannerRichMedia", ImpressionDetector.class, $$Lambda$c$oZydlRpCOb2Wi6G3myKUBo8pI.INSTANCE);
        diRegistry.registerFactory("RichMediaModuleInterfaceinterstitialRichMedia", ImpressionDetector.class, $$Lambda$c$q1e15kYkAjTZNIxPujBzs3apm0.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder a(String str, String str2, DiConstructor diConstructor) {
        e eVar = new e(DiLogLayer.getLoggerFrom(diConstructor), (k) diConstructor.get(str, k.class), (b) diConstructor.get(str, b.class), (b) diConstructor.get(str2, b.class), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), (d) diConstructor.get(str, d.class), (com.smaato.sdk.richmedia.mraid.a) diConstructor.get(str, com.smaato.sdk.richmedia.mraid.a.class), (Function) diConstructor.get("RichMediaModuleInterfaceinterstitialRichMedia", a.class), ((Analytics) diConstructor.get(Analytics.class)).getWebViewTracker());
        return eVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder c(String str, DiConstructor diConstructor) {
        b bVar = new b(DiLogLayer.getLoggerFrom(diConstructor), (k) diConstructor.get(str, k.class), (b) diConstructor.get(str, b.class), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), (d) diConstructor.get(str, d.class), (com.smaato.sdk.richmedia.mraid.a) diConstructor.get(str, com.smaato.sdk.richmedia.mraid.a.class), (Function) diConstructor.get("RichMediaModuleInterfacebannerRichMedia", a.class), ((Analytics) diConstructor.get(Analytics.class)).getWebViewTracker());
        return bVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a b(String str, DiConstructor diConstructor) {
        return new a(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return c.b(DiConstructor.this, this.f$1, (h) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f b(DiConstructor diConstructor, String str, h hVar) {
        f fVar = new f((Logger) diConstructor.get(Logger.class), hVar, DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (StateMachine) diConstructor.get(str, StateMachine.class), (LinkResolver) diConstructor.get(LinkResolver.class), (AdQualityViolationReporter) diConstructor.get(AdQualityViolationReporter.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class), (ImpressionDetector) diConstructor.get("RichMediaModuleInterfacebannerRichMedia", ImpressionDetector.class));
        return fVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a a(String str, DiConstructor diConstructor) {
        return new a(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return c.a(DiConstructor.this, this.f$1, (h) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f a(DiConstructor diConstructor, String str, h hVar) {
        f fVar = new f((Logger) diConstructor.get(Logger.class), hVar, DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (StateMachine) diConstructor.get(str, StateMachine.class), (LinkResolver) diConstructor.get(LinkResolver.class), (AdQualityViolationReporter) diConstructor.get(AdQualityViolationReporter.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class), (ImpressionDetector) diConstructor.get("RichMediaModuleInterfaceinterstitialRichMedia", ImpressionDetector.class));
        return fVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ImpressionDetector b(DiConstructor diConstructor) {
        return new ImpressionDetector(State.CREATED);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ImpressionDetector a(DiConstructor diConstructor) {
        return new ImpressionDetector(State.IMPRESSED);
    }

    @NonNull
    public static List<String> a(Context context, WebView webView, PermissionChecker permissionChecker) {
        ArrayList arrayList = new ArrayList();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("sms:"));
        if (Intents.canHandleIntent(context, intent)) {
            arrayList.add("sms");
        }
        Intent intent2 = new Intent("android.intent.action.DIAL");
        intent2.setData(Uri.parse("tel:"));
        if (Intents.canHandleIntent(context, intent2)) {
            arrayList.add("tel");
        }
        if (com.smaato.sdk.richmedia.util.b.a(context, webView)) {
            arrayList.add(MRAIDNativeFeature.INLINE_VIDEO);
        }
        if (permissionChecker.checkPermission("android.permission.ACCESS_FINE_LOCATION") || permissionChecker.checkPermission("android.permission.ACCESS_COARSE_LOCATION")) {
            arrayList.add("location");
        }
        return arrayList;
    }

    @Nullable
    public static Float a(String str) {
        try {
            return Float.valueOf(Float.parseFloat(str));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @NonNull
    public static String a(String str, Object... objArr) {
        return String.format(Locale.US, str, objArr);
    }

    private c(String str) {
        this.f3575a = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Objects.equals(this.f3575a, ((c) obj).f3575a);
    }

    public int hashCode() {
        return Objects.hash(this.f3575a);
    }

    @NonNull
    public static c a(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager == null) {
            return new c(null);
        }
        return new c(String.format(Locale.US, "%.1f", new Object[]{Float.valueOf((((float) audioManager.getStreamVolume(3)) * 100.0f) / ((float) audioManager.getStreamMaxVolume(3)))}));
    }

    @NonNull
    public static Rect a(Context context, Rect rect) {
        return new Rect(UIUtils.pxToDp(context, (float) rect.left), UIUtils.pxToDp(context, (float) rect.top), UIUtils.pxToDp(context, (float) rect.right), UIUtils.pxToDp(context, (float) rect.bottom));
    }

    @NonNull
    public static Rect b(Context context, Rect rect) {
        return new Rect(UIUtils.dpToPx(context, (float) rect.left), UIUtils.dpToPx(context, (float) rect.top), UIUtils.dpToPx(context, (float) rect.right), UIUtils.dpToPx(context, (float) rect.bottom));
    }
}
