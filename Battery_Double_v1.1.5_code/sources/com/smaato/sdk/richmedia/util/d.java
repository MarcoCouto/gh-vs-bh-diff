package com.smaato.sdk.richmedia.util;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.dataprovider.c;
import java.util.regex.Pattern;

public final class d {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3631a;

    public d(@NonNull Logger logger) {
        this.f3631a = (Logger) Objects.requireNonNull(logger);
    }

    @NonNull
    public final String a(@NonNull String str, @NonNull Context context, @NonNull c cVar) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(context);
        Objects.requireNonNull(cVar);
        if (Pattern.compile("(?i)<(html|body|head)[^>]*>").matcher(str).find()) {
            this.f3631a.error(LogDomain.RICH_MEDIA, "Rich media HTML content has disallowed tag(s): html, head, or body.", new Object[0]);
            StringBuilder sb = new StringBuilder();
            sb.append(a(context, cVar));
            sb.append(a(context));
            sb.append(str);
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder("<!DOCTYPE html><html lang='en' style='height:100%;'><head><meta name='viewport' content='width=device-width,height=device-height,initial-scale=1.0'/><style>html {height:100%%;}body {margin:0;padding:0;min-height:100%%;}img {display:block;max-height:100vh;max-width:100vw;margin-left:auto;margin-right:auto;}</style>");
        sb2.append(a(context, cVar));
        sb2.append(a(context));
        sb2.append("</head><body>");
        sb2.append(str);
        sb2.append("</body></html>");
        return sb2.toString();
    }

    @NonNull
    private String a(@NonNull Context context, @NonNull c cVar) {
        StringBuilder sb = new StringBuilder("<script>");
        sb.append(a.a(context, this.f3631a, "mraid.js"));
        sb.append("</script><script>");
        sb.append(a(cVar));
        sb.append("</script>");
        return sb.toString();
    }

    @NonNull
    private String a(@NonNull Context context) {
        StringBuilder sb = new StringBuilder("<script>");
        sb.append(a.a(context, this.f3631a, "omsdk-v1.js"));
        sb.append("</script>");
        return sb.toString();
    }

    @NonNull
    private static String a(@NonNull c cVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("window.MRAID_ENV = {\n");
        sb.append(String.format("version:'%s',\n", new Object[]{"3.0"}));
        sb.append(String.format("sdk: '%s',\n", new Object[]{"SmaatoSDK Android"}));
        sb.append(String.format("sdkVersion: '%s',\n", new Object[]{cVar.f3610a}));
        sb.append(String.format("appId: '%s',\n", new Object[]{cVar.b}));
        Objects.onNotNull(cVar.c, new Consumer(sb) {
            private final /* synthetic */ StringBuilder f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.append(String.format("ifa: '%s',\n", new Object[]{(String) obj}));
            }
        });
        Objects.onNotNull(cVar.d, new Consumer(sb) {
            private final /* synthetic */ StringBuilder f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.append(String.format("limitAdTracking: %b,\n", new Object[]{(Boolean) obj}));
            }
        });
        Objects.onNotNull(cVar.e, new Consumer(sb) {
            private final /* synthetic */ StringBuilder f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                d.a(this.f$0, (Integer) obj);
            }
        });
        sb.append("};");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(StringBuilder sb, Integer num) {
        String str = "coppa: %b,\n";
        boolean z = true;
        Object[] objArr = new Object[1];
        if (num.intValue() != 1) {
            z = false;
        }
        objArr[0] = Boolean.valueOf(z);
        sb.append(String.format(str, objArr));
    }
}
