package com.smaato.sdk.richmedia.util;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings.System;
import android.view.View;
import android.view.Window;
import androidx.annotation.NonNull;

public final class b {

    public enum a {
        PORTRAIT,
        LANDSCAPE,
        UNKNOWN
    }

    private static boolean a(int i, int i2) {
        return (i & i2) != 0;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=android.webkit.WebView, code=android.view.View, for r4v0, types: [android.view.View, android.webkit.WebView] */
    public static boolean a(@NonNull Context context, @NonNull View view) {
        if (context instanceof Activity) {
            while (view != null) {
                if (view.isHardwareAccelerated() && !a(view.getLayerType(), 1)) {
                    if (!(view.getParent() instanceof View)) {
                        break;
                    }
                    view = (View) view.getParent();
                } else {
                    return false;
                }
            }
            Window window = ((Activity) context).getWindow();
            if (window != null) {
                return a(window.getAttributes().flags, 16777216);
            }
        }
        return false;
    }

    @NonNull
    public static a a(@NonNull Context context) {
        switch (context.getResources().getConfiguration().orientation) {
            case 1:
                return a.PORTRAIT;
            case 2:
                return a.LANDSCAPE;
            default:
                return a.UNKNOWN;
        }
    }

    public static int a(@NonNull a aVar) {
        switch (aVar) {
            case PORTRAIT:
                return 1;
            case LANDSCAPE:
                return 0;
            default:
                return -1;
        }
    }

    public static boolean b(@NonNull Context context) {
        if (c(context)) {
            return true;
        }
        if (!(context instanceof Activity)) {
            return false;
        }
        return c.b((Activity) context);
    }

    static boolean c(@NonNull Context context) {
        return System.getInt(context.getContentResolver(), "accelerometer_rotation", 0) == 0;
    }
}
