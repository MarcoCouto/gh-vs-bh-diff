package com.smaato.sdk.richmedia.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class a {
    public static boolean a(@NonNull Activity activity) {
        try {
            int i = activity.getPackageManager().getActivityInfo(new ComponentName(activity, activity.getClass()), 0).configChanges;
            boolean z = (i & 128) != 0;
            boolean z2 = (i & 1024) != 0;
            if (!z || !z2) {
                return true;
            }
            return false;
        } catch (NameNotFoundException unused) {
            return true;
        }
    }

    public static boolean b(@NonNull Activity activity) {
        return b.c(activity) || c.a(activity);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r1.close();
     */
    @NonNull
    static String a(Context context, Logger logger, String str) {
        BufferedReader bufferedReader;
        Objects.requireNonNull(context);
        Objects.requireNonNull(logger);
        Objects.requireNonNull(str);
        StringBuilder sb = new StringBuilder();
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(str), "UTF-8"));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append("\n");
            }
        } catch (IOException e) {
            logger.error(LogDomain.RICH_MEDIA, String.format("Could not read '%s' file from assets", new Object[]{str}), e);
        } catch (Throwable unused) {
        }
        return sb.toString();
        throw th;
    }
}
