package com.smaato.sdk.image.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.ApiAdRequestExtras;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.NullableFunction;

public final class e implements AdLoaderPlugin {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final AdPresenterNameShaper f3512a;
    @NonNull
    private final NullableFunction<String, AdPresenterBuilder> b;

    public final void addApiAdRequestExtras(@NonNull ApiAdRequestExtras apiAdRequestExtras, @NonNull Logger logger) {
    }

    public e(@NonNull AdPresenterNameShaper adPresenterNameShaper, @NonNull NullableFunction<String, AdPresenterBuilder> nullableFunction) {
        this.f3512a = (AdPresenterNameShaper) Objects.requireNonNull(adPresenterNameShaper);
        this.b = (NullableFunction) Objects.requireNonNull(nullableFunction);
    }

    @Nullable
    public final AdPresenterBuilder getAdPresenterBuilder(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls, @NonNull Logger logger) {
        return (AdPresenterBuilder) this.b.apply(this.f3512a.shapeName(adFormat, cls));
    }

    @Nullable
    public final AdFormat resolveAdFormatToServerAdFormat(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        if (adFormat == AdFormat.STATIC_IMAGE) {
            return AdFormat.STATIC_IMAGE;
        }
        return null;
    }
}
