package com.smaato.sdk.image.ad;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.memory.LeakProtection;
import java.io.InputStream;

final class b extends g<BannerAdPresenter> {
    b(@NonNull Logger logger, @NonNull Function<f, d> function, @NonNull j jVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull ResourceLoader<InputStream, Bitmap> resourceLoader, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull h hVar, @NonNull LeakProtection leakProtection) {
        super(logger, jVar, resourceLoader, hVar, function, new Function(visibilityTrackerCreator, appBackgroundDetector, leakProtection) {
            private final /* synthetic */ VisibilityTrackerCreator f$1;
            private final /* synthetic */ AppBackgroundDetector f$2;
            private final /* synthetic */ LeakProtection f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final Object apply(Object obj) {
                return b.a(Logger.this, this.f$1, this.f$2, this.f$3, (d) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BannerAdPresenter a(Logger logger, VisibilityTrackerCreator visibilityTrackerCreator, AppBackgroundDetector appBackgroundDetector, LeakProtection leakProtection, d dVar) {
        a aVar = new a(logger, dVar, visibilityTrackerCreator, appBackgroundDetector, leakProtection);
        return aVar;
    }
}
