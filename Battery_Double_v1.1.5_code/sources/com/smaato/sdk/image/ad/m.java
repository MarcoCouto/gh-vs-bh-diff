package com.smaato.sdk.image.ad;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.memory.LeakProtection;
import java.io.InputStream;

final class m extends g<InterstitialAdPresenter> {
    m(@NonNull Logger logger, @NonNull Function<f, d> function, @NonNull j jVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VisibilityTrackerCreator visibilityTrackerCreator2, @NonNull ResourceLoader<InputStream, Bitmap> resourceLoader, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull h hVar, @NonNull LeakProtection leakProtection) {
        Logger logger2 = logger;
        $$Lambda$m$8ScTf9zr6Vai3aCLuJoaQ3Kug3k r0 = new Function(visibilityTrackerCreator, visibilityTrackerCreator2, appBackgroundDetector, leakProtection) {
            private final /* synthetic */ VisibilityTrackerCreator f$1;
            private final /* synthetic */ VisibilityTrackerCreator f$2;
            private final /* synthetic */ AppBackgroundDetector f$3;
            private final /* synthetic */ LeakProtection f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final Object apply(Object obj) {
                return m.a(Logger.this, this.f$1, this.f$2, this.f$3, this.f$4, (d) obj);
            }
        };
        super(logger2, jVar, resourceLoader, hVar, function, r0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ InterstitialAdPresenter a(Logger logger, VisibilityTrackerCreator visibilityTrackerCreator, VisibilityTrackerCreator visibilityTrackerCreator2, AppBackgroundDetector appBackgroundDetector, LeakProtection leakProtection, d dVar) {
        l lVar = new l(logger, dVar, visibilityTrackerCreator, visibilityTrackerCreator2, appBackgroundDetector, leakProtection);
        return lVar;
    }
}
