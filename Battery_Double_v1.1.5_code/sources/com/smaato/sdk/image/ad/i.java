package com.smaato.sdk.image.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.List;

final class i {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3517a;
    private final int b;
    private final int c;
    @NonNull
    private final String d;
    @NonNull
    private final List<String> e;
    @NonNull
    private final List<String> f;
    @Nullable
    private final Object g;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private int f3518a;
        private int b;
        private String c;
        private List<String> d;
        private List<String> e;
        private String f;

        @NonNull
        public final a setImageUrl(@NonNull String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public final a setWidth(int i) {
            this.f3518a = i;
            return this;
        }

        @NonNull
        public final a setHeight(int i) {
            this.b = i;
            return this;
        }

        @NonNull
        public final a setClickUrl(@NonNull String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final a setImpressionTrackingUrls(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a setClickTrackingUrls(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public final i build() {
            ArrayList arrayList = new ArrayList();
            if (this.f == null) {
                arrayList.add("imageUrl");
            }
            if (this.c == null) {
                arrayList.add("clickUrl");
            }
            if (this.d == null) {
                arrayList.add("impressionTrackingUrls");
            }
            if (this.e == null) {
                arrayList.add("clickTrackingUrls");
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new IllegalStateException(sb.toString());
            } else if (this.d.isEmpty()) {
                throw new IllegalStateException("impressionTrackingUrls cannot be empty");
            } else if (!this.e.isEmpty()) {
                i iVar = new i(this.f, this.f3518a, this.b, this.c, this.d, this.e, null, 0);
                return iVar;
            } else {
                throw new IllegalStateException("clickTrackingUrls cannot be empty");
            }
        }
    }

    /* synthetic */ i(String str, int i, int i2, String str2, List list, List list2, Object obj, byte b2) {
        this(str, i, i2, str2, list, list2, obj);
    }

    private i(@NonNull String str, int i, int i2, @NonNull String str2, @NonNull List<String> list, @NonNull List<String> list2, @Nullable Object obj) {
        this.f3517a = (String) Objects.requireNonNull(str);
        this.b = i;
        this.c = i2;
        this.d = (String) Objects.requireNonNull(str2);
        this.e = (List) Objects.requireNonNull(list);
        this.f = (List) Objects.requireNonNull(list2);
        this.g = obj;
    }

    @NonNull
    public final String getImageUrl() {
        return this.f3517a;
    }

    public final int getWidth() {
        return this.b;
    }

    public final int getHeight() {
        return this.c;
    }

    @NonNull
    public final String getClickUrl() {
        return this.d;
    }

    @NonNull
    public final List<String> getImpressionTrackingUrls() {
        return this.e;
    }

    @NonNull
    public final List<String> getClickTrackingUrls() {
        return this.f;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ImageAdResponse{imageUrl='");
        sb.append(this.f3517a);
        sb.append('\'');
        sb.append(", width=");
        sb.append(this.b);
        sb.append(", height=");
        sb.append(this.c);
        sb.append(", clickUrl='");
        sb.append(this.d);
        sb.append('\'');
        sb.append(", impressionTrackingUrls=");
        sb.append(this.e);
        sb.append(", clickTrackingUrls=");
        sb.append(this.f);
        sb.append(", extensions=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }

    @Nullable
    public final Object getExtensions() {
        return this.g;
    }
}
