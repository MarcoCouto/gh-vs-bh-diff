package com.smaato.sdk.image.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Builder;
import java.util.Arrays;

public final class k {
    @NonNull
    public static StateMachine<Event, State> newInstance(@NonNull State state) {
        return new Builder().setInitialState(state).addTransition(Event.INITIALISE, Arrays.asList(new State[]{State.INIT, State.CREATED})).addTransition(Event.ADDED_ON_SCREEN, Arrays.asList(new State[]{State.CREATED, State.ON_SCREEN})).addLoopTransition(Event.ADDED_ON_SCREEN, State.ON_SCREEN).addTransition(Event.REMOVED_FROM_SCREEN, Arrays.asList(new State[]{State.ON_SCREEN, State.CREATED})).addTransition(Event.IMPRESSION, Arrays.asList(new State[]{State.ON_SCREEN, State.IMPRESSED})).addTransition(Event.CLICK, Arrays.asList(new State[]{State.ON_SCREEN, State.IMPRESSED, State.CLICKED, State.COMPLETE})).addTransition(Event.CLICK, Arrays.asList(new State[]{State.IMPRESSED, State.CLICKED, State.COMPLETE})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.INIT, State.TO_BE_DELETED})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.CREATED, State.TO_BE_DELETED})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.ON_SCREEN, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.INIT, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.CREATED, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.ON_SCREEN, State.TO_BE_DELETED})).build();
    }

    @NonNull
    public static StateMachine<Event, State> newInstance() {
        return newInstance(State.INIT);
    }
}
