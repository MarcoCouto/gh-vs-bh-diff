package com.smaato.sdk.image.ad;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnClickListener;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter.Listener;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTracker;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.tracker.VisibilityTrackerListener;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.memory.LeakProtection;
import com.smaato.sdk.image.ad.d.a;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

final class l implements InterstitialAdPresenter {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3520a;
    /* access modifiers changed from: private */
    @NonNull
    public final d b;
    @NonNull
    private final VisibilityTrackerCreator c;
    @NonNull
    private final VisibilityTrackerCreator d;
    /* access modifiers changed from: private */
    @NonNull
    public final AppBackgroundDetector e;
    @NonNull
    private final LeakProtection f;
    @NonNull
    private final AtomicReference<VisibilityTracker> g = new AtomicReference<>();
    @NonNull
    private final AtomicReference<VisibilityTracker> h = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<Listener> i = new WeakReference<>(null);
    @NonNull
    private StateMachine.Listener<State> j;

    public final void setFriendlyObstructionView(@NonNull View view) {
    }

    l(@NonNull Logger logger, @NonNull d dVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VisibilityTrackerCreator visibilityTrackerCreator2, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull LeakProtection leakProtection) {
        this.f3520a = (Logger) Objects.requireNonNull(logger);
        this.b = (d) Objects.requireNonNull(dVar);
        this.c = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.d = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator2);
        this.e = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.f = (LeakProtection) Objects.requireNonNull(leakProtection);
        this.j = new StateMachine.Listener(logger) {
            private final /* synthetic */ Logger f$1;

            {
                this.f$1 = r2;
            }

            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                l.this.a(this.f$1, (State) obj, (State) obj2, metadata);
            }
        };
        dVar.addStateListener(this.j);
        dVar.setOnImpressionTriggered(new a() {
            public final void onImpressionTriggered() {
                l.this.c();
            }
        });
        dVar.onEvent(Event.INITIALISE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, State state, State state2, Metadata metadata) {
        switch (state2) {
            case CREATED:
            case ON_SCREEN:
            case IMPRESSED:
                return;
            case CLICKED:
                Objects.onNotNull(this.i.get(), new Consumer() {
                    public final void accept(Object obj) {
                        l.this.c((Listener) obj);
                    }
                });
                return;
            case COMPLETE:
                return;
            case TO_BE_DELETED:
                return;
            default:
                logger.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdImpressed(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c() {
        Objects.onNotNull(this.i.get(), new Consumer() {
            public final void accept(Object obj) {
                l.this.b((Listener) obj);
            }
        });
    }

    @NonNull
    public final AdContentView getAdContentView(@NonNull Context context) {
        final AtomicReference atomicReference = new AtomicReference(null);
        com.smaato.sdk.image.ui.a create = com.smaato.sdk.image.ui.a.create(context, (f) this.b.getAdObject(), new OnClickListener() {
            private UrlResolveListener c = new UrlResolveListener() {
                public final void onError() {
                    Threads.runOnUi(new Runnable(this, atomicReference) {
                        private final /* synthetic */ AnonymousClass1 f$0;
                        private final /* synthetic */ AtomicReference f$1;

                        public final 
/*
Method generation error in method: com.smaato.sdk.image.ad.-$$Lambda$l$1$1$dG9i7c4eBSl6uHaTCe5_f3rqyF0.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.ClassGen.addInsnBody(ClassGen.java:435)
                        	at jadx.core.codegen.ClassGen.addField(ClassGen.java:376)
                        	at jadx.core.codegen.ClassGen.addFields(ClassGen.java:346)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:223)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(AtomicReference atomicReference) {
                    l.this.f3520a.error(LogDomain.AD, "The url seems to be invalid", new Object[0]);
                    Objects.onNotNull(l.this.i.get(), new Consumer(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.image.ad.-$$Lambda$l$1$1$EgmsN6zz3_aRozBaoNOc7KILzcA.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.ClassGen.addInsnBody(ClassGen.java:435)
                        	at jadx.core.codegen.ClassGen.addField(ClassGen.java:376)
                        	at jadx.core.codegen.ClassGen.addFields(ClassGen.java:346)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:223)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                    Objects.onNotNull(atomicReference.get(), $$Lambda$l$1$1$u43YmsPaubBN7u8gpdaCpGQXnaw.INSTANCE);
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Listener listener) {
                    listener.onAdError(l.this);
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    Threads.runOnUi(new Runnable(atomicReference, consumer) {
                        private final /* synthetic */ AtomicReference f$0;
                        private final /* synthetic */ Consumer f$1;

                        public final 
/*
Method generation error in method: com.smaato.sdk.image.ad.-$$Lambda$l$1$1$S-rgdi25hCsGz299_awd2K6xY1Y.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.ClassGen.addInsnBody(ClassGen.java:435)
                        	at jadx.core.codegen.ClassGen.addField(ClassGen.java:376)
                        	at jadx.core.codegen.ClassGen.addFields(ClassGen.java:346)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:223)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public static /* synthetic */ void a(Consumer consumer, com.smaato.sdk.image.ui.a aVar) {
                    consumer.accept(aVar.getContext());
                    aVar.showProgressIndicator(false);
                }
            };

            public final void onClick(View view) {
                if (l.this.e.isAppInBackground()) {
                    l.this.f3520a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
                    return;
                }
                ((com.smaato.sdk.image.ui.a) view).showProgressIndicator(true);
                l.this.b.resolveClickUrl(this.c);
                l.this.b.onEvent(Event.CLICK);
            }
        });
        atomicReference.set(create);
        this.g.set(this.c.createTracker(create, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                l.this.b();
            }
        }));
        this.h.set(this.d.createTracker(create, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                l.this.a();
            }
        }));
        create.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(View view) {
                l.this.b.onEvent(Event.ADDED_ON_SCREEN);
            }

            public final void onViewDetachedFromWindow(View view) {
                l.this.b.onEvent(Event.REMOVED_FROM_SCREEN);
                view.removeOnAttachStateChangeListener(this);
            }
        });
        return create;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        this.b.onEvent(Event.IMPRESSION);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        Objects.onNotNull(this.i.get(), $$Lambda$nk0D4u8wrNMCMfE3WzHxisIX_k.INSTANCE);
    }

    @NonNull
    public final AdInteractor<? extends AdObject> getAdInteractor() {
        return this.b;
    }

    @NonNull
    public final String getPublisherId() {
        return this.b.getPublisherId();
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.b.getAdSpaceId();
    }

    @NonNull
    public final String getSessionId() {
        return this.b.getSessionId();
    }

    @Nullable
    public final String getCreativeId() {
        return this.b.getCreativeId();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onClose(this);
    }

    public final void onCloseClicked() {
        Objects.onNotNull(this.i.get(), new Consumer() {
            public final void accept(Object obj) {
                l.this.a((Listener) obj);
            }
        });
    }

    public final void setListener(@Nullable Listener listener) {
        this.i = new WeakReference<>(listener);
    }

    public final boolean isValid() {
        return this.b.isValid();
    }

    @MainThread
    public final void destroy() {
        Threads.ensureMainThread();
        this.b.stopUrlResolving();
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                l.this.b((VisibilityTracker) obj);
            }
        });
        Objects.onNotNull(this.h.get(), new Consumer() {
            public final void accept(Object obj) {
                l.this.a((VisibilityTracker) obj);
            }
        });
        if (!isValid()) {
            this.b.removeStateListener(this.j);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(VisibilityTracker visibilityTracker) {
        this.g.set(null);
        visibilityTracker.destroy();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(VisibilityTracker visibilityTracker) {
        this.h.set(null);
        visibilityTracker.destroy();
    }
}
