package com.smaato.sdk.image.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.image.framework.-$$Lambda$ImageModuleInterface$h3XcgrRx-xVTbmDiT1NE4RNuy2k reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ImageModuleInterface$h3XcgrRxxVTbmDiT1NE4RNuy2k implements ClassFactory {
    public static final /* synthetic */ $$Lambda$ImageModuleInterface$h3XcgrRxxVTbmDiT1NE4RNuy2k INSTANCE = new $$Lambda$ImageModuleInterface$h3XcgrRxxVTbmDiT1NE4RNuy2k();

    private /* synthetic */ $$Lambda$ImageModuleInterface$h3XcgrRxxVTbmDiT1NE4RNuy2k() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(0.01d).visibilityTimeMillis(0).build();
    }
}
