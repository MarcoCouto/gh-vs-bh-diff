package com.smaato.sdk.image.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.image.framework.-$$Lambda$ImageModuleInterface$2ju-PZGuBl2t1Unw2B0x1BmL4h4 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ImageModuleInterface$2juPZGuBl2t1Unw2B0x1BmL4h4 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$ImageModuleInterface$2juPZGuBl2t1Unw2B0x1BmL4h4 INSTANCE = new $$Lambda$ImageModuleInterface$2juPZGuBl2t1Unw2B0x1BmL4h4();

    private /* synthetic */ $$Lambda$ImageModuleInterface$2juPZGuBl2t1Unw2B0x1BmL4h4() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newFixedThreadPool(2);
    }
}
