package com.smaato.sdk.image.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.image.framework.-$$Lambda$ImageModuleInterface$fJV9KzshywC49eXucpdps-vsBLI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ImageModuleInterface$fJV9KzshywC49eXucpdpsvsBLI implements ClassFactory {
    public static final /* synthetic */ $$Lambda$ImageModuleInterface$fJV9KzshywC49eXucpdpsvsBLI INSTANCE = new $$Lambda$ImageModuleInterface$fJV9KzshywC49eXucpdpsvsBLI();

    private /* synthetic */ $$Lambda$ImageModuleInterface$fJV9KzshywC49eXucpdpsvsBLI() {
    }

    public final Object get(DiConstructor diConstructor) {
        return ImageModuleInterface.m(diConstructor);
    }
}
