package com.smaato.sdk.image.resourceloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.ResourceTransformer;
import java.io.IOException;
import java.io.InputStream;

public class c implements ResourceTransformer<InputStream, Bitmap> {
    @NonNull
    public Bitmap transform(@NonNull InputStream inputStream) {
        Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
        try {
            inputStream.close();
        } catch (IOException unused) {
        }
        return decodeStream;
    }
}
