package com.smaato.sdk.video.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.video.vast.config.b;

/* renamed from: com.smaato.sdk.video.framework.-$$Lambda$VideoModuleInterface$DyoNZ4oNLV9G3SJqyvarVa5RBi0 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VideoModuleInterface$DyoNZ4oNLV9G3SJqyvarVa5RBi0 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$VideoModuleInterface$DyoNZ4oNLV9G3SJqyvarVa5RBi0 INSTANCE = new $$Lambda$VideoModuleInterface$DyoNZ4oNLV9G3SJqyvarVa5RBi0();

    private /* synthetic */ $$Lambda$VideoModuleInterface$DyoNZ4oNLV9G3SJqyvarVa5RBi0() {
    }

    public final Object get(DiConstructor diConstructor) {
        return diConstructor.get("WRAPPER_RESOLVER_CONFIG", b.class);
    }
}
