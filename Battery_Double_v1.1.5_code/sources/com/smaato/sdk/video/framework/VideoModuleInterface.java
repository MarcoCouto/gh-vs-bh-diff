package com.smaato.sdk.video.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.BuildConfig;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.CoreDiNames;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.resourceloader.DiResourceLoaderLayer;
import com.smaato.sdk.core.resourceloader.NetworkResourceStreamPreparationStrategy;
import com.smaato.sdk.core.resourceloader.PersistingStrategy;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.resourceloader.ResourceLoadingNetworkTaskCreator;
import com.smaato.sdk.core.resourceloader.ResourceTransformer;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.webview.BaseWebChromeClient;
import com.smaato.sdk.core.webview.BaseWebViewClient;
import com.smaato.sdk.video.ad.h;
import com.smaato.sdk.video.network.c;
import com.smaato.sdk.video.vast.build.aa;
import com.smaato.sdk.video.vast.build.ab;
import com.smaato.sdk.video.vast.build.ac;
import com.smaato.sdk.video.vast.build.e;
import com.smaato.sdk.video.vast.build.f;
import com.smaato.sdk.video.vast.build.g;
import com.smaato.sdk.video.vast.build.i;
import com.smaato.sdk.video.vast.build.j;
import com.smaato.sdk.video.vast.build.k;
import com.smaato.sdk.video.vast.build.l;
import com.smaato.sdk.video.vast.build.m;
import com.smaato.sdk.video.vast.build.n;
import com.smaato.sdk.video.vast.build.p;
import com.smaato.sdk.video.vast.build.q;
import com.smaato.sdk.video.vast.build.r;
import com.smaato.sdk.video.vast.build.t;
import com.smaato.sdk.video.vast.build.u;
import com.smaato.sdk.video.vast.build.v;
import com.smaato.sdk.video.vast.build.w;
import com.smaato.sdk.video.vast.build.y;
import com.smaato.sdk.video.vast.build.z;
import com.smaato.sdk.video.vast.config.a;
import com.smaato.sdk.video.vast.config.b;
import com.smaato.sdk.video.vast.parser.s;
import com.smaato.sdk.video.vast.parser.x;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.xmlpull.v1.XmlPullParser;

public class VideoModuleInterface implements AdPresenterModuleInterface {
    @NonNull
    public String moduleDiName() {
        return "VideoModuleInterface";
    }

    @NonNull
    public String version() {
        return BuildConfig.VERSION_NAME;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b R(DiConstructor diConstructor) {
        return new a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void k(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(com.smaato.sdk.video.network.b.class, $$Lambda$VideoModuleInterface$NghuwxSjzlEDNWucc07bA785mHU.INSTANCE);
        diRegistry.registerFactory(c.class, $$Lambda$VideoModuleInterface$kppwZHQcHhuS0RnodKVO1tC5alA.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.network.b Q(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.network.b(DiNetworkLayer.getNetworkActionsFrom(diConstructor), (c) diConstructor.get(c.class), Executors.newSingleThreadExecutor());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ c P(DiConstructor diConstructor) {
        return new c((Logger) diConstructor.get(Logger.class), (x) diConstructor.get(x.class), ErrorMapper.STANDARD);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void i(DiRegistry diRegistry) {
        diRegistry.registerFactory(com.smaato.sdk.video.vast.browser.a.class, $$Lambda$VideoModuleInterface$eMB5hjQcJf6iN1jQJLcuyNHOkA.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), BaseWebViewClient.class, $$Lambda$VideoModuleInterface$8sRLdFe0aviQ6fWWEjJIeeh1uk.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), BaseWebChromeClient.class, $$Lambda$VideoModuleInterface$RkAbIgs3HVQVqOxq7zUdlMJzoQ.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.browser.a O(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.browser.a((Logger) diConstructor.get(Logger.class), CoreDiNames.SOMA_API_URL, DiNetworkLayer.getUrlCreatorFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BaseWebViewClient N(DiConstructor diConstructor) {
        return new BaseWebViewClient();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BaseWebChromeClient M(DiConstructor diConstructor) {
        return new BaseWebChromeClient();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void h(DiRegistry diRegistry) {
        diRegistry.registerFactory(moduleDiName(), VisibilityPrivateConfig.class, $$Lambda$VideoModuleInterface$Xhpp1wqWZD2AqPz_EgxrTPbaso.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), VisibilityTrackerCreator.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return VideoModuleInterface.this.K(diConstructor);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ VisibilityTrackerCreator K(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get(moduleDiName(), VisibilityPrivateConfig.class);
        VisibilityTrackerCreator visibilityTrackerCreator = new VisibilityTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), moduleDiName());
        return visibilityTrackerCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void g(DiRegistry diRegistry) {
        diRegistry.registerFactory(com.smaato.sdk.video.vast.build.x.class, $$Lambda$VideoModuleInterface$dYsQL6fXSJZMlTjRUKrkZA5f4c.INSTANCE);
        diRegistry.registerFactory(m.class, $$Lambda$VideoModuleInterface$SR6k4MPaYEVQRbPBrV76R7VNiUg.INSTANCE);
        diRegistry.registerFactory(w.class, $$Lambda$VideoModuleInterface$LQiLV0TU4XkpP5sWhjrzl3TYdpQ.INSTANCE);
        diRegistry.registerFactory(y.class, $$Lambda$VideoModuleInterface$iQZi6_iB0ShM9XG72w0LZ3Wqbdc.INSTANCE);
        diRegistry.registerFactory(g.class, $$Lambda$VideoModuleInterface$VviQYEUj_0wvilTfrB36iECsFHw.INSTANCE);
        diRegistry.registerFactory(u.class, $$Lambda$VideoModuleInterface$AB4JmjUhMJOjjiA925nb9jVKtV0.INSTANCE);
        diRegistry.registerFactory(k.class, $$Lambda$VideoModuleInterface$VR2OGnXlY_r9Cy9aNLhBJH0AZw.INSTANCE);
        diRegistry.registerFactory(e.class, $$Lambda$VideoModuleInterface$h6IY3WxcLLOlyhDxduHUzpbeSc.INSTANCE);
        diRegistry.registerFactory(f.class, $$Lambda$VideoModuleInterface$T58ec1wDAeqOuiMFxun4N8yMYVw.INSTANCE);
        diRegistry.registerFactory(i.class, $$Lambda$VideoModuleInterface$BMcQPLtyeFJU151uCH0IxAbXTw.INSTANCE);
        diRegistry.registerFactory(j.class, $$Lambda$VideoModuleInterface$YnCvjk_2BOidrUP_SS8oN6u9Tuk.INSTANCE);
        diRegistry.registerFactory(l.class, $$Lambda$VideoModuleInterface$9cITJ4ytVuDtgnb2ONKWncZy4rc.INSTANCE);
        diRegistry.registerFactory(p.class, $$Lambda$VideoModuleInterface$hkiMFuwE7zuV2hnNJXozoa3xPHs.INSTANCE);
        diRegistry.registerFactory(q.class, $$Lambda$VideoModuleInterface$0Pq5rKzOhHn_l9Zv7yahmxN7JQg.INSTANCE);
        diRegistry.registerFactory(n.class, $$Lambda$VideoModuleInterface$Q3YWGhpHrO1YM89N6nMDSdV2nfM.INSTANCE);
        diRegistry.registerFactory(t.class, $$Lambda$VideoModuleInterface$kMvUNrt_ybmci0cYJwNq4hTbHjI.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.build.x J(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.build.x();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ m I(DiConstructor diConstructor) {
        return new m((com.smaato.sdk.video.vast.build.x) diConstructor.get(com.smaato.sdk.video.vast.build.x.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ w H(DiConstructor diConstructor) {
        return new w();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ y G(DiConstructor diConstructor) {
        return new y();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ g F(DiConstructor diConstructor) {
        return new g();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ u E(DiConstructor diConstructor) {
        return new u((m) diConstructor.get(m.class), (w) diConstructor.get(w.class), (g) diConstructor.get(g.class), (y) diConstructor.get(y.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ k D(DiConstructor diConstructor) {
        return new k();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ e C(DiConstructor diConstructor) {
        return new e();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f B(DiConstructor diConstructor) {
        return new f();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ i A(DiConstructor diConstructor) {
        return new i();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ j z(DiConstructor diConstructor) {
        return new j((i) diConstructor.get(i.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ l y(DiConstructor diConstructor) {
        return new l((j) diConstructor.get(j.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ p x(DiConstructor diConstructor) {
        return new p();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ q w(DiConstructor diConstructor) {
        q qVar = new q((k) diConstructor.get(k.class), (e) diConstructor.get(e.class), (f) diConstructor.get(f.class), (l) diConstructor.get(l.class), (p) diConstructor.get(p.class));
        return qVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ n v(DiConstructor diConstructor) {
        return new n((j) diConstructor.get(j.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ t u(DiConstructor diConstructor) {
        return new t((e) diConstructor.get(e.class), (f) diConstructor.get(f.class), (n) diConstructor.get(n.class), (p) diConstructor.get(p.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void f(DiRegistry diRegistry) {
        diRegistry.registerFactory(com.smaato.sdk.video.vast.build.a.class, $$Lambda$VideoModuleInterface$GZmcwi5PMBVZEPkhs5EeGSjOld4.INSTANCE);
        diRegistry.registerFactory(z.class, $$Lambda$VideoModuleInterface$H6IMWq99odgKx6dDXQ6jewqmVww.INSTANCE);
        diRegistry.registerFactory(r.class, $$Lambda$VideoModuleInterface$yisWhkftwnqj5ut6FV7QDwj9VXs.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.build.a t(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.build.a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ z s(DiConstructor diConstructor) {
        return new z();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ r r(DiConstructor diConstructor) {
        r rVar = new r((com.smaato.sdk.video.vast.build.a) diConstructor.get(com.smaato.sdk.video.vast.build.a.class), (z) diConstructor.get(z.class), (u) diConstructor.get(u.class), (q) diConstructor.get(q.class), (t) diConstructor.get(t.class));
        return rVar;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(DiRegistry diRegistry) {
        diRegistry.registerFactory(v.class, $$Lambda$VideoModuleInterface$1LAMPYpGTxfHZpvuYi0giLex9w.INSTANCE);
        diRegistry.registerFactory(x.class, $$Lambda$VideoModuleInterface$GiW_Ohb6Zsc17_SMZ4gMsrGdLF0.INSTANCE);
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$ZF7Tpxi8FbF07hhbjtNzWVXqiS0.INSTANCE));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ v q(DiConstructor diConstructor) {
        return new v((x) diConstructor.get(x.class), (ac) diConstructor.get(ac.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ x p(DiConstructor diConstructor) {
        return new x((com.smaato.sdk.video.vast.parser.r) diConstructor.get(com.smaato.sdk.video.vast.parser.r.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(DiRegistry diRegistry) {
        diRegistry.registerFactory(ac.class, $$Lambda$VideoModuleInterface$DyoNZ4oNLV9G3SJqyvarVa5RBi0.INSTANCE);
        diRegistry.registerFactory(aa.class, $$Lambda$VideoModuleInterface$TzCnuIem4KIVBxeq01Z1lP1T30.INSTANCE);
        diRegistry.registerFactory(ab.class, $$Lambda$VideoModuleInterface$KwBPr4Ytu9XAgAHSViGKYdwR4.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.video.vast.build.b.class, $$Lambda$VideoModuleInterface$EHWsw_bb_8lpzqBIEzqHSkks1F8.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ aa n(DiConstructor diConstructor) {
        return new aa((ab) diConstructor.get(ab.class), (com.smaato.sdk.video.network.b) diConstructor.get(com.smaato.sdk.video.network.b.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ab m(DiConstructor diConstructor) {
        return new ab();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.build.b l(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.build.b();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory("VideoModuleInterfaceVideoResource", ResourceLoader.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return VideoModuleInterface.this.k(diConstructor);
            }
        });
        diRegistry.registerSingletonFactory("VideoModuleInterfaceVIDEO_RESOURCE_LOADING_DEVICE_LOCAL_EXECUTOR", ExecutorService.class, $$Lambda$VideoModuleInterface$9dkBwjMCqZhEtPzjwUaxLZX4iQw.INSTANCE);
        diRegistry.registerFactory("VideoModuleInterfaceVideoResource", ResourceLoadingNetworkTaskCreator.class, $$Lambda$VideoModuleInterface$R7O5ssVd_z96kSedtRVTdj1u42c.INSTANCE);
        diRegistry.registerSingletonFactory("VideoModuleInterfaceVIDEO_RESOURCE_LOADING_NETWORK_EXECUTOR", ExecutorService.class, $$Lambda$VideoModuleInterface$lUPbJV6KNH2XBawmBnvHk7to9KI.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.video.resourceloader.a.class, $$Lambda$VideoModuleInterface$np9vEF9lecQFwvgfaQ12dQy4dbo.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.video.resourceloader.b.class, $$Lambda$VideoModuleInterface$uf3LXhz2dg9uIKtunTwU1xw8KGA.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.video.resourceloader.c.class, $$Lambda$VideoModuleInterface$g2cWRcyWX6XR8IOgbjoKsjVmig.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ ResourceLoader k(DiConstructor diConstructor) {
        ResourceLoader resourceLoader = new ResourceLoader(DiLogLayer.getLoggerFrom(diConstructor), (ResourceLoadingNetworkTaskCreator) diConstructor.get("VideoModuleInterfaceVideoResource", ResourceLoadingNetworkTaskCreator.class), (ExecutorService) diConstructor.get("VideoModuleInterfaceVIDEO_RESOURCE_LOADING_DEVICE_LOCAL_EXECUTOR", ExecutorService.class), (PersistingStrategy) diConstructor.get(com.smaato.sdk.video.resourceloader.b.class), (ResourceTransformer) diConstructor.get(com.smaato.sdk.video.resourceloader.c.class));
        return resourceLoader;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ResourceLoadingNetworkTaskCreator i(DiConstructor diConstructor) {
        ResourceLoadingNetworkTaskCreator resourceLoadingNetworkTaskCreator = new ResourceLoadingNetworkTaskCreator(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getNetworkActionsFrom(diConstructor), ErrorMapper.NETWORK_LAYER_EXCEPTION, (ExecutorService) diConstructor.get("VideoModuleInterfaceVIDEO_RESOURCE_LOADING_NETWORK_EXECUTOR", ExecutorService.class), (NetworkResourceStreamPreparationStrategy) diConstructor.get(com.smaato.sdk.video.resourceloader.a.class));
        return resourceLoadingNetworkTaskCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.resourceloader.a g(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.resourceloader.a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.resourceloader.b f(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.resourceloader.b(DiLogLayer.getLoggerFrom(diConstructor), "video/vast", DiResourceLoaderLayer.getBaseStoragePersistingStrategyFileUtils(diConstructor), DiResourceLoaderLayer.getMd5Digester(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.resourceloader.c e(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.resourceloader.c();
    }

    public boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls) {
        return adFormat == AdFormat.VIDEO && (cls.isAssignableFrom(RewardedAdPresenter.class) || cls.isAssignableFrom(InterstitialAdPresenter.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderPlugin d(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.ad.j((AdPresenterNameShaper) diConstructor.get(AdPresenterNameShaper.class), new NullableFunction() {
            public final Object apply(Object obj) {
                return VideoModuleInterface.a(DiConstructor.this, (String) obj);
            }
        });
    }

    @NonNull
    public ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory() {
        return $$Lambda$VideoModuleInterface$IFz342Ntqkta4dzYLc8X8oekP8.INSTANCE;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder a(DiConstructor diConstructor, String str) {
        return (AdPresenterBuilder) DiAdLayer.tryGetOrNull(diConstructor, str, AdPresenterBuilder.class);
    }

    @Nullable
    public DiRegistry moduleDiRegistry() {
        return DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.a((DiRegistry) obj);
            }
        });
    }

    @Nullable
    public DiRegistry moduleAdPresenterDiRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper) {
        return DiRegistry.of(new Consumer(adPresenterNameShaper) {
            private final /* synthetic */ AdPresenterNameShaper f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                VideoModuleInterface.this.a(this.f$1, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdPresenterNameShaper adPresenterNameShaper, DiRegistry diRegistry) {
        diRegistry.registerFactory(h.class, $$Lambda$VideoModuleInterface$YpvuEHwWeoh_1281eUN2VHi70RU.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.video.ad.e.class, $$Lambda$VideoModuleInterface$IsE0_RIN_msfWXcbpGbYWxz1jM.INSTANCE);
        diRegistry.addFrom(com.smaato.sdk.video.ad.a.a(adPresenterNameShaper, moduleDiName()));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ h b(DiConstructor diConstructor) {
        return new h((Logger) diConstructor.get(Logger.class), DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (com.smaato.sdk.video.vast.tracking.b) diConstructor.get(com.smaato.sdk.video.vast.tracking.b.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.ad.e a(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.ad.e();
    }

    @NonNull
    public ExpectedManifestEntries getExpectedManifestEntries() {
        return ExpectedManifestEntries.EMPTY;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VideoModuleInterface{supportedFormat: ");
        sb.append(AdFormat.VIDEO);
        sb.append("}");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$yAtHFQg2lyDJdhUU4YOwTSpn0L8.INSTANCE));
        diRegistry.registerFactory(moduleDiName(), XmlPullParser.class, $$Lambda$VideoModuleInterface$POBREcnOLuaZyLMxB8995JHted0.INSTANCE);
        diRegistry.registerSingletonFactory(com.smaato.sdk.video.vast.parser.r.class, new s());
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.j((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.b((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(com.smaato.sdk.video.vast.player.b.a());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(DiRegistry diRegistry) {
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.i((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$dBAWhnxLsCP7mZ6umRVDM_TAQZc.INSTANCE));
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$LWqXMoNNqu_DwCxOeg3h4fOeDG0.INSTANCE));
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.e((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$UaQgqN46Ya_kvTP7EwZogXTfRYc.INSTANCE));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void j(DiRegistry diRegistry) {
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.h((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.c((DiRegistry) obj);
            }
        }));
        diRegistry.registerFactory(moduleDiName(), StateMachine.class, new com.smaato.sdk.video.ad.n(State.INIT));
    }
}
