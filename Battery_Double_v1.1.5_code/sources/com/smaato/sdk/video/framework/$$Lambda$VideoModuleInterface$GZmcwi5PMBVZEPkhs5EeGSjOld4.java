package com.smaato.sdk.video.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.video.framework.-$$Lambda$VideoModuleInterface$GZmcwi5PMBVZEPkhs5EeGSjOld4 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VideoModuleInterface$GZmcwi5PMBVZEPkhs5EeGSjOld4 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$VideoModuleInterface$GZmcwi5PMBVZEPkhs5EeGSjOld4 INSTANCE = new $$Lambda$VideoModuleInterface$GZmcwi5PMBVZEPkhs5EeGSjOld4();

    private /* synthetic */ $$Lambda$VideoModuleInterface$GZmcwi5PMBVZEPkhs5EeGSjOld4() {
    }

    public final Object get(DiConstructor diConstructor) {
        return VideoModuleInterface.t(diConstructor);
    }
}
