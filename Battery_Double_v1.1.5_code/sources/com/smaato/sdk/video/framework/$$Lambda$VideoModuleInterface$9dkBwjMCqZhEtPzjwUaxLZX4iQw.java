package com.smaato.sdk.video.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.video.framework.-$$Lambda$VideoModuleInterface$9dkBwjMCqZhEtPzjwUaxLZX4iQw reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VideoModuleInterface$9dkBwjMCqZhEtPzjwUaxLZX4iQw implements ClassFactory {
    public static final /* synthetic */ $$Lambda$VideoModuleInterface$9dkBwjMCqZhEtPzjwUaxLZX4iQw INSTANCE = new $$Lambda$VideoModuleInterface$9dkBwjMCqZhEtPzjwUaxLZX4iQw();

    private /* synthetic */ $$Lambda$VideoModuleInterface$9dkBwjMCqZhEtPzjwUaxLZX4iQw() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newFixedThreadPool(2);
    }
}
