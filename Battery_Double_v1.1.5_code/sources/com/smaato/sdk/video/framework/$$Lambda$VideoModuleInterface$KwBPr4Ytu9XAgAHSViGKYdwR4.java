package com.smaato.sdk.video.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.video.framework.-$$Lambda$VideoModuleInterface$KwBPr4Ytu9XAgAHSV-iGKYdwR-4 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VideoModuleInterface$KwBPr4Ytu9XAgAHSViGKYdwR4 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$VideoModuleInterface$KwBPr4Ytu9XAgAHSViGKYdwR4 INSTANCE = new $$Lambda$VideoModuleInterface$KwBPr4Ytu9XAgAHSViGKYdwR4();

    private /* synthetic */ $$Lambda$VideoModuleInterface$KwBPr4Ytu9XAgAHSViGKYdwR4() {
    }

    public final Object get(DiConstructor diConstructor) {
        return VideoModuleInterface.m(diConstructor);
    }
}
