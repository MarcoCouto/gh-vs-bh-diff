package com.smaato.sdk.video.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.video.framework.-$$Lambda$VideoModuleInterface$Q3YWGhpHrO1YM89N6nMDSdV2nfM reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VideoModuleInterface$Q3YWGhpHrO1YM89N6nMDSdV2nfM implements ClassFactory {
    public static final /* synthetic */ $$Lambda$VideoModuleInterface$Q3YWGhpHrO1YM89N6nMDSdV2nfM INSTANCE = new $$Lambda$VideoModuleInterface$Q3YWGhpHrO1YM89N6nMDSdV2nfM();

    private /* synthetic */ $$Lambda$VideoModuleInterface$Q3YWGhpHrO1YM89N6nMDSdV2nfM() {
    }

    public final Object get(DiConstructor diConstructor) {
        return VideoModuleInterface.v(diConstructor);
    }
}
