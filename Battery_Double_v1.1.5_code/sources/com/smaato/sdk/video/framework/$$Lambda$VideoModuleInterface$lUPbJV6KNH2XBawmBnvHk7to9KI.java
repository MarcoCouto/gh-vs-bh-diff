package com.smaato.sdk.video.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.video.framework.-$$Lambda$VideoModuleInterface$lUPbJV6KNH2XBawmBnvHk7to9KI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VideoModuleInterface$lUPbJV6KNH2XBawmBnvHk7to9KI implements ClassFactory {
    public static final /* synthetic */ $$Lambda$VideoModuleInterface$lUPbJV6KNH2XBawmBnvHk7to9KI INSTANCE = new $$Lambda$VideoModuleInterface$lUPbJV6KNH2XBawmBnvHk7to9KI();

    private /* synthetic */ $$Lambda$VideoModuleInterface$lUPbJV6KNH2XBawmBnvHk7to9KI() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newFixedThreadPool(2);
    }
}
