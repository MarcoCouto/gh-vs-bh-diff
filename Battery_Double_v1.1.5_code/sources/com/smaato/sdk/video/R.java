package com.smaato.sdk.video;

public final class R {

    public static final class attr {
        public static final int smaato_sdk_video_cpb_background_progressbar_color = 2130968866;
        public static final int smaato_sdk_video_cpb_background_progressbar_width = 2130968867;
        public static final int smaato_sdk_video_cpb_label_font_size = 2130968868;
        public static final int smaato_sdk_video_cpb_progressbar_color = 2130968869;
        public static final int smaato_sdk_video_cpb_progressbar_width = 2130968870;

        private attr() {
        }
    }

    public static final class color {
        public static final int smaato_sdk_core_ui_ctrl_almost_white = 2131099828;
        public static final int smaato_sdk_core_ui_ctrl_black = 2131099829;
        public static final int smaato_sdk_core_ui_ctrl_grey = 2131099830;
        public static final int smaato_sdk_core_ui_semitransparent = 2131099831;
        public static final int smaato_sdk_video_blue = 2131099833;
        public static final int smaato_sdk_video_grey = 2131099834;

        private color() {
        }
    }

    public static final class dimen {
        public static final int smaato_sdk_core_activity_margin = 2131165384;
        public static final int smaato_sdk_video_default_background_stroke_width = 2131165385;
        public static final int smaato_sdk_video_default_stroke_width = 2131165386;
        public static final int smaato_sdk_video_progress_label_font_size = 2131165387;
        public static final int smaato_sdk_video_touch_target_minimum = 2131165388;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int smaato_sdk_core_back = 2131231150;
        public static final int smaato_sdk_core_back_disabled = 2131231151;
        public static final int smaato_sdk_core_background = 2131231152;
        public static final int smaato_sdk_core_browser_bottom_button_layout_bg = 2131231153;
        public static final int smaato_sdk_core_browser_progress_bar = 2131231154;
        public static final int smaato_sdk_core_browser_top_button_layout_bg = 2131231155;
        public static final int smaato_sdk_core_circle_close = 2131231156;
        public static final int smaato_sdk_core_close = 2131231157;
        public static final int smaato_sdk_core_forward = 2131231158;
        public static final int smaato_sdk_core_forward_disabled = 2131231159;
        public static final int smaato_sdk_core_ic_browser_background_selector = 2131231160;
        public static final int smaato_sdk_core_ic_browser_backward_selector = 2131231161;
        public static final int smaato_sdk_core_ic_browser_forward_selector = 2131231162;
        public static final int smaato_sdk_core_ic_browser_secure_connection = 2131231163;
        public static final int smaato_sdk_core_lock = 2131231164;
        public static final int smaato_sdk_core_open_in_browser = 2131231165;
        public static final int smaato_sdk_core_refresh = 2131231166;
        public static final int smaato_sdk_video_muted = 2131231168;
        public static final int smaato_sdk_video_skip = 2131231169;
        public static final int smaato_sdk_video_unmuted = 2131231170;

        private drawable() {
        }
    }

    public static final class id {
        public static final int btnBackward = 2131296342;
        public static final int btnClose = 2131296343;
        public static final int btnForward = 2131296344;
        public static final int btnLayoutBottom = 2131296345;
        public static final int btnLayoutTop = 2131296346;
        public static final int btnOpenExternal = 2131296347;
        public static final int btnRefresh = 2131296348;
        public static final int progressBar = 2131296587;
        public static final int smaato_sdk_video_companion_view_id = 2131296627;
        public static final int smaato_sdk_video_icon_view_id = 2131296628;
        public static final int smaato_sdk_video_mute_button = 2131296629;
        public static final int smaato_sdk_video_player_layout = 2131296630;
        public static final int smaato_sdk_video_player_surface_layout = 2131296631;
        public static final int smaato_sdk_video_skip_button = 2131296632;
        public static final int smaato_sdk_video_surface_holder_view_id = 2131296633;
        public static final int smaato_sdk_video_video_player_view_id = 2131296634;
        public static final int smaato_sdk_video_video_progress = 2131296635;
        public static final int tvHostname = 2131296696;
        public static final int webView = 2131296707;

        private id() {
        }
    }

    public static final class layout {
        public static final int smaato_sdk_core_activity_internal_browser = 2131427447;
        public static final int smaato_sdk_video_player_view = 2131427451;
        public static final int smaato_sdk_video_vast_video_player_view = 2131427452;

        private layout() {
        }
    }

    public static final class string {
        public static final int smaato_sdk_core_browser_hostname_content_description = 2131624221;
        public static final int smaato_sdk_core_btn_browser_backward_content_description = 2131624222;
        public static final int smaato_sdk_core_btn_browser_close_content_description = 2131624223;
        public static final int smaato_sdk_core_btn_browser_forward_content_description = 2131624224;
        public static final int smaato_sdk_core_btn_browser_open_content_description = 2131624225;
        public static final int smaato_sdk_core_btn_browser_refresh_content_description = 2131624226;
        public static final int smaato_sdk_core_fullscreen_dimension = 2131624227;
        public static final int smaato_sdk_core_no_external_browser_found = 2131624228;
        public static final int smaato_sdk_video_close_button_text = 2131624230;
        public static final int smaato_sdk_video_mute_button_text = 2131624231;
        public static final int smaato_sdk_video_skip_button_text = 2131624232;

        private string() {
        }
    }

    public static final class style {
        public static final int smaato_sdk_core_browserProgressBar = 2131689898;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] smaato_sdk_video_circular_progress_bar = {com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_background_progressbar_color, com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_background_progressbar_width, com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_label_font_size, com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_progressbar_color, com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_progressbar_width};
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_background_progressbar_color = 0;
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_background_progressbar_width = 1;
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_label_font_size = 2;
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_progressbar_color = 3;
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_progressbar_width = 4;

        private styleable() {
        }
    }

    private R() {
    }
}
