package com.smaato.sdk.video.ad;

import android.content.Context;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdInteractor.TtlListener;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine.Listener;
import com.smaato.sdk.video.vast.player.k;
import com.smaato.sdk.video.vast.player.k.a;

abstract class l implements AdPresenter {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3651a;
    @NonNull
    private final k b;
    /* access modifiers changed from: private */
    @NonNull
    public final i c;
    @NonNull
    private final a d = new a() {
        public final void a() {
            l.this.c.onEvent(Event.ADDED_ON_SCREEN);
        }

        public final void b() {
            l.this.c.addStateListener(new Listener() {
                public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                    AnonymousClass1.this.b((State) obj, (State) obj2, metadata);
                }
            });
            l.this.c.onEvent(Event.CLOSE);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b(State state, State state2, Metadata metadata) {
            if (state2 == State.TO_BE_DELETED) {
                l.this.e();
            }
        }

        public final void c() {
            l.this.c();
        }

        public final void d() {
            l.this.h();
        }

        public final void e() {
            l.this.c.onEvent(Event.IMPRESSION);
        }

        public final void f() {
            l.this.g();
        }

        public final void g() {
            l.this.c.addStateListener(new Listener() {
                public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                    AnonymousClass1.this.a((State) obj, (State) obj2, metadata);
                }
            });
            l.this.c.onEvent(Event.AD_ERROR);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(State state, State state2, Metadata metadata) {
            if (state2 == State.TO_BE_DELETED) {
                l.this.f();
            }
        }
    };
    @NonNull
    private final Listener<State> e = new Listener() {
        public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
            l.this.b((State) obj, (State) obj2, metadata);
        }
    };
    @NonNull
    private TtlListener f = new TtlListener() {
        public final void onTTLExpired(AdInteractor adInteractor) {
            l.this.a(adInteractor);
        }
    };

    /* access modifiers changed from: 0000 */
    public abstract void a();

    /* access modifiers changed from: 0000 */
    public abstract void b();

    /* access modifiers changed from: 0000 */
    public abstract void c();

    /* access modifiers changed from: 0000 */
    public abstract void d();

    /* access modifiers changed from: 0000 */
    public abstract void e();

    /* access modifiers changed from: 0000 */
    public abstract void f();

    /* access modifiers changed from: 0000 */
    public abstract void g();

    /* access modifiers changed from: 0000 */
    public abstract void h();

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdInteractor adInteractor) {
        d();
    }

    l(@NonNull Logger logger, @NonNull k kVar, @NonNull i iVar) {
        this.f3651a = (Logger) Objects.requireNonNull(logger);
        this.b = (k) Objects.requireNonNull(kVar);
        this.c = (i) Objects.requireNonNull(iVar);
        this.b.a(this.d);
        iVar.addStateListener(this.e);
        iVar.addTtlListener(this.f);
        iVar.onEvent(Event.INITIALISE);
    }

    @NonNull
    public AdInteractor<? extends AdObject> getAdInteractor() {
        return this.c;
    }

    @NonNull
    public AdContentView getAdContentView(@NonNull Context context) {
        return this.b.a(context);
    }

    @NonNull
    public String getPublisherId() {
        return this.c.getPublisherId();
    }

    @NonNull
    public String getAdSpaceId() {
        return this.c.getAdSpaceId();
    }

    @NonNull
    public String getSessionId() {
        return this.c.getSessionId();
    }

    @Nullable
    public String getCreativeId() {
        return this.c.getCreativeId();
    }

    @MainThread
    public void destroy() {
        this.c.removeStateListener(this.e);
        this.c.addStateListener(new Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                l.this.a((State) obj, (State) obj2, metadata);
            }
        });
        this.c.onEvent(Event.CLOSE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(State state, State state2, Metadata metadata) {
        if (state2 == State.TO_BE_DELETED) {
            e();
        }
    }

    public boolean isValid() {
        return this.c.isValid();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(State state, State state2, Metadata metadata) {
        switch (state2) {
            case CREATED:
                return;
            case ON_SCREEN:
                a();
                return;
            case IMPRESSED:
                b();
                return;
            case TO_BE_DELETED:
                return;
            default:
                StringBuilder sb = new StringBuilder("Unexpected state for RewardedVideoAdPresenter ");
                sb.append(state2);
                throw new IllegalStateException(sb.toString());
        }
    }
}
