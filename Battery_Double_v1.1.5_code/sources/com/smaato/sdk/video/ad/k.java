package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.util.Objects;

final class k implements AdObject {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final SomaApiContext f3650a;

    k(@NonNull SomaApiContext somaApiContext) {
        this.f3650a = (SomaApiContext) Objects.requireNonNull(somaApiContext);
    }

    @NonNull
    public final SomaApiContext getSomaApiContext() {
        return this.f3650a;
    }
}
