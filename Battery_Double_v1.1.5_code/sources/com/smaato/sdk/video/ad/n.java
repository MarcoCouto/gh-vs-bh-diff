package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Builder;
import java.util.Arrays;

public final class n implements ClassFactory<StateMachine> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private State f3657a;

    public n(@NonNull State state) {
        this.f3657a = state;
    }

    @NonNull
    public final /* synthetic */ Object get(@NonNull DiConstructor diConstructor) {
        return new Builder().setInitialState(this.f3657a).addTransition(Event.INITIALISE, Arrays.asList(new State[]{State.INIT, State.CREATED})).addTransition(Event.ADDED_ON_SCREEN, Arrays.asList(new State[]{State.CREATED, State.ON_SCREEN})).addTransition(Event.REMOVED_FROM_SCREEN, Arrays.asList(new State[]{State.ON_SCREEN, State.CREATED})).addTransition(Event.IMPRESSION, Arrays.asList(new State[]{State.ON_SCREEN, State.IMPRESSED})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.INIT, State.TO_BE_DELETED})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.CREATED, State.TO_BE_DELETED})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.ON_SCREEN, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.INIT, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.CREATED, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.ON_SCREEN, State.TO_BE_DELETED})).addTransition(Event.CLOSE, Arrays.asList(new State[]{State.ON_SCREEN, State.TO_BE_DELETED})).addTransition(Event.CLOSE, Arrays.asList(new State[]{State.IMPRESSED, State.TO_BE_DELETED})).addTransition(Event.CLOSE, Arrays.asList(new State[]{State.CLICKED, State.TO_BE_DELETED})).build();
    }
}
