package com.smaato.sdk.video.ad;

import android.content.Context;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter.Listener;
import com.smaato.sdk.core.ad.RewardedAdPresenter.OnCloseEnabledListener;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.player.k;
import java.lang.ref.WeakReference;

public final class f extends l implements RewardedAdPresenter {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final k f3646a;
    @NonNull
    private WeakReference<Listener> b = new WeakReference<>(null);
    @NonNull
    private WeakReference<OnCloseEnabledListener> c = new WeakReference<>(null);

    @MainThread
    public final /* bridge */ /* synthetic */ void destroy() {
        super.destroy();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ AdContentView getAdContentView(@NonNull Context context) {
        return super.getAdContentView(context);
    }

    @NonNull
    public final /* bridge */ /* synthetic */ AdInteractor getAdInteractor() {
        return super.getAdInteractor();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ String getAdSpaceId() {
        return super.getAdSpaceId();
    }

    @Nullable
    public final /* bridge */ /* synthetic */ String getCreativeId() {
        return super.getCreativeId();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ String getPublisherId() {
        return super.getPublisherId();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ String getSessionId() {
        return super.getSessionId();
    }

    public final /* bridge */ /* synthetic */ boolean isValid() {
        return super.isValid();
    }

    f(@NonNull Logger logger, @NonNull k kVar, @NonNull i iVar) {
        super(logger, kVar, iVar);
        this.f3646a = kVar;
    }

    public final void setListener(@Nullable Listener listener) {
        this.b = new WeakReference<>(listener);
    }

    public final void setOnCloseEnabledListener(@Nullable OnCloseEnabledListener onCloseEnabledListener) {
        this.c = new WeakReference<>(onCloseEnabledListener);
    }

    public final void onCloseClicked() {
        this.f3646a.a();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void g(Listener listener) {
        listener.onStart(this);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.g((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void f(Listener listener) {
        listener.onAdImpressed(this);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.f((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(Listener listener) {
        listener.onCompleted(this);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.e((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Listener listener) {
        listener.onTTLExpired(this);
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.d((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onClose(this);
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.c((Listener) obj);
            }
        });
        Objects.onNotNull(this.c.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.b((OnCloseEnabledListener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(OnCloseEnabledListener onCloseEnabledListener) {
        onCloseEnabledListener.onClose(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdError(this);
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.b((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.a((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(OnCloseEnabledListener onCloseEnabledListener) {
        onCloseEnabledListener.onCloseEnabled(this);
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        Objects.onNotNull(this.c.get(), new Consumer() {
            public final void accept(Object obj) {
                f.this.a((OnCloseEnabledListener) obj);
            }
        });
    }
}
