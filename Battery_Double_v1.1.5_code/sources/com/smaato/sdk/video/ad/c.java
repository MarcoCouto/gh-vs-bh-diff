package com.smaato.sdk.video.ad;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.build.r;
import com.smaato.sdk.video.vast.build.v;
import com.smaato.sdk.video.vast.player.l;

final class c extends m<InterstitialAdPresenter> {
    /* access modifiers changed from: protected */
    public final boolean a() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return true;
    }

    c(@NonNull Logger logger, @NonNull Function<k, i> function, @NonNull r rVar, @NonNull v vVar, @NonNull l lVar, @NonNull ResourceLoader<Uri, Uri> resourceLoader, @NonNull h hVar, @NonNull e eVar) {
        super(logger, rVar, vVar, lVar, resourceLoader, hVar, eVar, function, new Function() {
            public final Object apply(Object obj) {
                return c.a(Logger.this, (a) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ InterstitialAdPresenter a(Logger logger, a aVar) {
        return new b(logger, aVar.f3655a, aVar.b);
    }
}
