package com.smaato.sdk.video.ad;

import android.content.res.Resources;
import android.net.Uri;
import android.util.DisplayMetrics;
import androidx.annotation.NonNull;
import com.explorestack.iab.vast.VastError;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Error;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Listener;
import com.smaato.sdk.core.ad.AdPresenterBuilderErrorMapper;
import com.smaato.sdk.core.ad.AdPresenterBuilderException;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.resourceloader.ResourceLoaderException;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.build.o;
import com.smaato.sdk.video.vast.build.r;
import com.smaato.sdk.video.vast.build.s;
import com.smaato.sdk.video.vast.build.v;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.model.h;
import com.smaato.sdk.video.vast.model.n;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.player.k;
import com.smaato.sdk.video.vast.player.l;
import com.smaato.sdk.video.vast.tracking.f;
import java.io.ByteArrayInputStream;
import java.util.HashSet;

abstract class m<Presenter extends AdPresenter> implements AdPresenterBuilder {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3654a;
    @NonNull
    private final r b;
    @NonNull
    private final v c;
    @NonNull
    private final l d;
    @NonNull
    private final ResourceLoader<Uri, Uri> e;
    @NonNull
    private final h f;
    @NonNull
    private final e g;
    @NonNull
    private final Function<k, i> h;
    @NonNull
    private final Function<a, Presenter> i;

    static class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        final k f3655a;
        @NonNull
        final i b;

        a(@NonNull k kVar, @NonNull i iVar) {
            this.f3655a = kVar;
            this.b = iVar;
        }
    }

    final class b implements a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final SomaApiContext f3656a;
        @NonNull
        private final f b;
        @NonNull
        private final Listener c;

        /* synthetic */ b(m mVar, SomaApiContext somaApiContext, f fVar, Listener listener, byte b2) {
            this(somaApiContext, fVar, listener);
        }

        private b(SomaApiContext somaApiContext, @NonNull f fVar, @NonNull Listener listener) {
            this.f3656a = (SomaApiContext) Objects.requireNonNull(somaApiContext);
            this.b = (f) Objects.requireNonNull(fVar);
            this.c = (Listener) Objects.requireNonNull(listener);
        }

        public final void a(@NonNull aa aaVar) {
            m.this.a(aaVar, this.f3656a, this.b, (com.smaato.sdk.video.fi.b<Either<k, Exception>>) new com.smaato.sdk.video.fi.b() {
                public final void accept(Object obj) {
                    b.this.a((Either) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Either either) {
            m.this.a(either, this.f3656a, this.c);
        }

        public final void a(@NonNull Exception exc) {
            AdPresenterBuilderException adPresenterBuilderException;
            this.b.a(new com.smaato.sdk.video.vast.build.h.a(400).a());
            if (exc instanceof ResourceLoaderException) {
                adPresenterBuilderException = AdPresenterBuilderErrorMapper.mapError((ResourceLoaderException) exc);
            } else {
                adPresenterBuilderException = new AdPresenterBuilderException(Error.GENERIC, exc);
            }
            this.c.onAdPresenterBuildError(m.this, adPresenterBuilderException);
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean a();

    /* access modifiers changed from: protected */
    public abstract boolean b();

    m(@NonNull Logger logger, @NonNull r rVar, @NonNull v vVar, @NonNull l lVar, @NonNull ResourceLoader<Uri, Uri> resourceLoader, @NonNull h hVar, @NonNull e eVar, @NonNull Function<k, i> function, @NonNull Function<a, Presenter> function2) {
        this.f3654a = (Logger) Objects.requireNonNull(logger);
        this.b = (r) Objects.requireNonNull(rVar);
        this.c = (v) Objects.requireNonNull(vVar);
        this.d = (l) Objects.requireNonNull(lVar);
        this.e = (ResourceLoader) Objects.requireNonNull(resourceLoader);
        this.f = (h) Objects.requireNonNull(hVar);
        this.g = (e) Objects.requireNonNull(eVar);
        this.h = (Function) Objects.requireNonNull(function);
        this.i = (Function) Objects.requireNonNull(function2);
    }

    public void buildAdPresenter(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        Objects.requireNonNull(somaApiContext, "Parameter somaApiContext cannot be null for VideoAdPresenterBuilder::buildAdPresenter");
        ApiAdResponse apiAdResponse = somaApiContext.getApiAdResponse();
        this.c.a(this.f3654a, somaApiContext, new ByteArrayInputStream(apiAdResponse.getBody()), apiAdResponse.getCharset(), new com.smaato.sdk.video.fi.b(somaApiContext, listener) {
            private final /* synthetic */ SomaApiContext f$1;
            private final /* synthetic */ Listener f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                m.this.a(this.f$1, this.f$2, (o) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Either<k, Exception> either, @NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        Exception exc = (Exception) either.right();
        if (exc != null) {
            a(exc.getMessage(), listener);
            return;
        }
        listener.onAdPresenterBuildSuccess(this, (AdPresenter) this.i.apply(new a((k) Objects.requireNonNull(either.left()), (i) this.h.apply(new k(somaApiContext)))));
    }

    /* access modifiers changed from: private */
    public void a(@NonNull aa aaVar, @NonNull SomaApiContext somaApiContext, @NonNull f fVar, @NonNull com.smaato.sdk.video.fi.b<Either<k, Exception>> bVar) {
        this.d.a(this.f3654a, somaApiContext, aaVar, fVar, bVar, a(), b());
    }

    private void a(@NonNull String str, @NonNull Listener listener) {
        this.f3654a.error(LogDomain.VAST, str, new Object[0]);
        listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, new RuntimeException(str)));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(SomaApiContext somaApiContext, Listener listener, o oVar) {
        if (oVar.b == null) {
            a("Failed to build RewardedVideoAdPresenter: VAST parse result is empty", listener);
            return;
        }
        HashSet<Integer> hashSet = new HashSet<>(oVar.f3685a);
        Logger logger = this.f3654a;
        ad adVar = (ad) oVar.b;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        Integer width = somaApiContext.getApiAdRequest().getWidth();
        Integer height = somaApiContext.getApiAdRequest().getHeight();
        if (width == null) {
            width = Integer.valueOf(displayMetrics.widthPixels);
        }
        if (height == null) {
            height = Integer.valueOf(displayMetrics.heightPixels);
        }
        s a2 = this.b.a(logger, adVar, new com.smaato.sdk.video.vast.player.b(width.intValue(), height.intValue()));
        hashSet.addAll(a2.b);
        f a3 = this.f.a(somaApiContext, a2.f3690a);
        for (Integer intValue : hashSet) {
            a3.a(new com.smaato.sdk.video.vast.build.h.a(intValue.intValue()).a());
        }
        aa aaVar = a2.c;
        if (aaVar == null) {
            a("Failed to build RewardedVideoAdPresenter: Unable to pick proper VAST scenario to play", listener);
            return;
        }
        x xVar = aaVar.e;
        if (!(xVar.f > 0)) {
            a3.a(new com.smaato.sdk.video.vast.build.h.a(400).a());
            a("Failed to build VastAdPresenter: Invalid value of expected duration", listener);
            return;
        }
        n nVar = xVar.b;
        if (TextUtils.isEmpty(nVar.f3737a)) {
            a3.a(new com.smaato.sdk.video.vast.build.h.a(400).a());
            a("Failed to build VastAdPresenter: Empty URL of MediaFile", listener);
            return;
        }
        h hVar = nVar.m;
        if (hVar == h.PROGRESSIVE) {
            b bVar = new b(this, somaApiContext, a3, listener, 0);
            this.e.loadResource(nVar.f3737a, somaApiContext, e.a(aaVar, bVar));
        } else if (hVar == h.STREAMING) {
            a(aaVar, somaApiContext, a3, (com.smaato.sdk.video.fi.b<Either<k, Exception>>) new com.smaato.sdk.video.fi.b(somaApiContext, listener) {
                private final /* synthetic */ SomaApiContext f$1;
                private final /* synthetic */ Listener f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    m.this.a(this.f$1, this.f$2, (Either) obj);
                }
            });
        } else {
            a3.a(new com.smaato.sdk.video.vast.build.h.a(VastError.ERROR_CODE_ERROR_SHOWING).a());
            a("Failed to build RewardedVideoAdPresenter: Unknown delivery method", listener);
        }
    }
}
