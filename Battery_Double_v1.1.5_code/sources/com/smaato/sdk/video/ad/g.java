package com.smaato.sdk.video.ad;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.build.r;
import com.smaato.sdk.video.vast.build.v;
import com.smaato.sdk.video.vast.player.l;

public final class g extends m<f> {
    /* access modifiers changed from: protected */
    public final boolean a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return false;
    }

    public final /* bridge */ /* synthetic */ void buildAdPresenter(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        super.buildAdPresenter(somaApiContext, listener);
    }

    public g(@NonNull Logger logger, @NonNull Function<k, i> function, @NonNull r rVar, @NonNull v vVar, @NonNull l lVar, @NonNull ResourceLoader<Uri, Uri> resourceLoader, @NonNull h hVar, @NonNull e eVar) {
        super(logger, rVar, vVar, lVar, resourceLoader, hVar, eVar, function, new Function() {
            public final Object apply(Object obj) {
                return g.a(Logger.this, (a) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f a(Logger logger, a aVar) {
        return new f(logger, aVar.f3655a, aVar.b);
    }
}
