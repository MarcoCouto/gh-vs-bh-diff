package com.smaato.sdk.video.ad;

import android.content.Context;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter.Listener;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.player.k;
import java.lang.ref.WeakReference;

public final class b extends l implements InterstitialAdPresenter {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final k f3644a;
    @NonNull
    private WeakReference<Listener> b = new WeakReference<>(null);

    /* access modifiers changed from: 0000 */
    public final void a() {
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
    }

    public final void setFriendlyObstructionView(@NonNull View view) {
    }

    @MainThread
    public final /* bridge */ /* synthetic */ void destroy() {
        super.destroy();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ AdContentView getAdContentView(@NonNull Context context) {
        return super.getAdContentView(context);
    }

    @NonNull
    public final /* bridge */ /* synthetic */ AdInteractor getAdInteractor() {
        return super.getAdInteractor();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ String getAdSpaceId() {
        return super.getAdSpaceId();
    }

    @Nullable
    public final /* bridge */ /* synthetic */ String getCreativeId() {
        return super.getCreativeId();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ String getPublisherId() {
        return super.getPublisherId();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ String getSessionId() {
        return super.getSessionId();
    }

    public final /* bridge */ /* synthetic */ boolean isValid() {
        return super.isValid();
    }

    b(@NonNull Logger logger, @NonNull k kVar, @NonNull i iVar) {
        super(logger, kVar, iVar);
        this.f3644a = kVar;
    }

    @MainThread
    public final void onCloseClicked() {
        this.f3644a.a();
    }

    public final void setListener(@Nullable Listener listener) {
        this.b = new WeakReference<>(listener);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(Listener listener) {
        listener.onAdImpressed(this);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                b.this.e((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Listener listener) {
        listener.onTTLExpired(this);
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                b.this.d((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onClose(this);
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                b.this.c((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdError(this);
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                b.this.b((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                b.this.a((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        Objects.onNotNull(this.b.get(), $$Lambda$nk0D4u8wrNMCMfE3WzHxisIX_k.INSTANCE);
    }
}
