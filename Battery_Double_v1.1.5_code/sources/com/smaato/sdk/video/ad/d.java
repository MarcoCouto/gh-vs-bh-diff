package com.smaato.sdk.video.ad;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.ResourceLoader.Listener;
import com.smaato.sdk.core.resourceloader.ResourceLoaderException;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.aa;

public final class d implements Listener<Uri> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final aa f3645a;
    @NonNull
    private final a b;

    interface a {
        void a(@NonNull aa aaVar);

        void a(@NonNull Exception exc);
    }

    public final /* synthetic */ void onResourceLoaded(@NonNull Object obj) {
        try {
            this.b.a(this.f3645a.a().a(this.f3645a.e.a().a(new com.smaato.sdk.video.vast.model.n.a(this.f3645a.e.b).a(((Uri) obj).toString()).a()).a()).a());
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            a(e);
        }
    }

    d(@NonNull aa aaVar, @NonNull a aVar) {
        this.f3645a = (aa) Objects.requireNonNull(aaVar);
        this.b = (a) Objects.requireNonNull(aVar);
    }

    public final void onFailure(@NonNull ResourceLoaderException resourceLoaderException) {
        a(resourceLoaderException);
    }

    private void a(@NonNull Exception exc) {
        this.b.a(exc);
    }
}
