package com.smaato.sdk.video.ad;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.build.r;
import com.smaato.sdk.video.vast.build.v;
import com.smaato.sdk.video.vast.player.l;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class a {

    /* renamed from: com.smaato.sdk.video.ad.a$a reason: collision with other inner class name */
    private interface C0081a extends Function<k, i> {
    }

    @NonNull
    public static DiRegistry a(@NonNull AdPresenterNameShaper adPresenterNameShaper, @NonNull String str) {
        Objects.requireNonNull(adPresenterNameShaper);
        Objects.requireNonNull(str);
        return DiRegistry.of(new Consumer(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                a.a(AdPresenterNameShaper.this, this.f$1, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AdPresenterNameShaper adPresenterNameShaper, String str, DiRegistry diRegistry) {
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.VIDEO, RewardedAdPresenter.class), AdPresenterBuilder.class, $$Lambda$a$eiItnwrKELQEk4ljPldx6r1ZUk.INSTANCE);
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.VIDEO, InterstitialAdPresenter.class), AdPresenterBuilder.class, $$Lambda$a$GKtItYA6CHUSPdBMZS5F4XaIz8.INSTANCE);
        diRegistry.registerFactory(C0081a.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return a.a(this.f$0, diConstructor);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder c(DiConstructor diConstructor) {
        g gVar = new g((Logger) diConstructor.get(Logger.class), (Function) diConstructor.get(C0081a.class), (r) diConstructor.get(r.class), (v) diConstructor.get(v.class), (l) diConstructor.get(l.class), a(diConstructor), (h) diConstructor.get(h.class), (e) diConstructor.get(e.class));
        return gVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder b(DiConstructor diConstructor) {
        c cVar = new c((Logger) diConstructor.get(Logger.class), (Function) diConstructor.get(C0081a.class), (r) diConstructor.get(r.class), (v) diConstructor.get(v.class), (l) diConstructor.get(l.class), a(diConstructor), (h) diConstructor.get(h.class), (e) diConstructor.get(e.class));
        return cVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ C0081a a(String str, DiConstructor diConstructor) {
        return new C0081a(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return a.a(DiConstructor.this, this.f$1, (k) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ i a(DiConstructor diConstructor, String str, k kVar) {
        return new i((Logger) diConstructor.get(Logger.class), kVar, (StateMachine) diConstructor.get(str, StateMachine.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class));
    }

    @NonNull
    private static ResourceLoader<Uri, Uri> a(@NonNull DiConstructor diConstructor) {
        return (ResourceLoader) diConstructor.get("VideoModuleInterfaceVideoResource", ResourceLoader.class);
    }

    @NonNull
    public String a() {
        return String.valueOf(new Random().nextInt(90000000) + 10000000);
    }

    @NonNull
    public static <T> List<T> a(List<T> list, List<T> list2) {
        ArrayList arrayList = new ArrayList(list.size() + list2.size());
        arrayList.addAll(list);
        arrayList.addAll(list2);
        return arrayList;
    }

    @NonNull
    public static <T> T a(T t, String str) throws com.smaato.sdk.video.vast.exceptions.a {
        if (t != null) {
            return t;
        }
        throw new com.smaato.sdk.video.vast.exceptions.a(str);
    }

    @NonNull
    public static <T> List<T> a(List<T> list) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(new ArrayList(list));
    }
}
