package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;

final class i extends AdInteractor<k> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3648a;

    public i(@NonNull Logger logger, @NonNull k kVar, @NonNull StateMachine<Event, State> stateMachine, @NonNull OneTimeActionFactory oneTimeActionFactory) {
        super(kVar, stateMachine, oneTimeActionFactory);
        this.f3648a = (Logger) Objects.requireNonNull(logger);
        stateMachine.onEvent(Event.INITIALISE);
    }

    public final boolean isValid() {
        return this.stateMachine.getCurrentState() != State.TO_BE_DELETED;
    }
}
