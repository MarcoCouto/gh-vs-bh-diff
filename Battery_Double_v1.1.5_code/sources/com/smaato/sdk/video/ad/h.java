package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.video.vast.tracking.b;
import com.smaato.sdk.video.vast.tracking.f;
import java.util.Collection;

public class h {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3647a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final b c;

    public h(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull b bVar) {
        logger.getClass();
        this.f3647a = logger;
        beaconTracker.getClass();
        this.b = beaconTracker;
        bVar.getClass();
        this.c = bVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final f a(@NonNull SomaApiContext somaApiContext, @NonNull Collection<String> collection) {
        somaApiContext.getClass();
        collection.getClass();
        f fVar = new f(this.f3647a, this.b, somaApiContext, this.c, collection);
        return fVar;
    }
}
