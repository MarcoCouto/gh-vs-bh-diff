package com.smaato.sdk.video.network;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.network.exception.TaskCancelledException;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.TaskStepResult;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.parser.p;
import com.smaato.sdk.video.vast.parser.x;
import java.io.ByteArrayInputStream;

public class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3660a;
    @NonNull
    private final x b;
    @NonNull
    private final ErrorMapper<Error> c;

    public c(@NonNull Logger logger, @NonNull x xVar, @NonNull ErrorMapper<Error> errorMapper) {
        this.f3660a = logger;
        this.b = xVar;
        this.c = errorMapper;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Consumer<TaskStepResult<a, Exception>> a(@NonNull Task task, @NonNull Listener<p<ad>, Error> listener) {
        return new Consumer(listener, task) {
            private final /* synthetic */ Listener f$1;
            private final /* synthetic */ Task f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                c.this.a(this.f$1, this.f$2, (TaskStepResult) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener, Task task, TaskStepResult taskStepResult) {
        if (taskStepResult.success != null) {
            this.b.a(this.f3660a, new ByteArrayInputStream(((a) taskStepResult.success).f3658a), ((a) taskStepResult.success).b, new b(task) {
                private final /* synthetic */ Task f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    Listener.this.onSuccess(this.f$1, (p) obj);
                }
            });
        } else if (taskStepResult.isCancelled) {
            listener.onFailure(task, this.c.map(new TaskCancelledException()));
        } else if (taskStepResult.error != null) {
            listener.onFailure(task, this.c.map((Exception) taskStepResult.error));
        } else {
            this.f3660a.error(LogDomain.NETWORK, "Network Task finished in unexpected state: %s", taskStepResult);
            listener.onFailure(task, this.c.map(new Exception("Generic")));
        }
    }
}
