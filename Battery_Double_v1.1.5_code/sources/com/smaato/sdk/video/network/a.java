package com.smaato.sdk.video.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;

final class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final byte[] f3658a;
    @Nullable
    public final String b;

    a(@NonNull byte[] bArr, @Nullable String str) {
        Objects.requireNonNull(bArr, "Parameter responseBody cannot be null for BodyEncodingPair::new");
        this.f3658a = bArr;
        this.b = str;
    }
}
