package com.smaato.sdk.video.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.network.NetworkHttpRequest.Builder;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.network.execution.Executioner;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.parser.p;
import java.util.concurrent.ExecutorService;

public class b implements Executioner<String, p<ad>, Error> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ExecutorService f3659a;
    @NonNull
    private final c b;
    @NonNull
    private final NetworkActions c;

    public b(@NonNull NetworkActions networkActions, @NonNull c cVar, @NonNull ExecutorService executorService) {
        this.c = (NetworkActions) Objects.requireNonNull(networkActions, "Parameter networkActions cannot be null for StaticWrapperLoaderExecutioner::new");
        this.f3659a = (ExecutorService) Objects.requireNonNull(executorService, "Parameter executorService cannot be null for StaticWrapperLoaderExecutioner::new");
        this.b = (c) Objects.requireNonNull(cVar, "Parameter vastParsingConsumerFactory cannot be null for StaticWrapperLoaderExecutioner::new");
    }

    @NonNull
    /* renamed from: a */
    public final Task submitRequest(@NonNull String str, @Nullable SomaApiContext somaApiContext, @NonNull Listener<p<ad>, Error> listener) {
        Objects.requireNonNull(somaApiContext, "Cannot load Vast Wrapper without SomaApiContext");
        d dVar = new d(this.c, new Builder().setMethod(Method.GET).setUrl(str).build(), this.b, this.f3659a, somaApiContext, listener);
        return dVar;
    }
}
