package com.smaato.sdk.video.network;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.network.execution.NetworkTask;
import com.smaato.sdk.core.network.execution.TaskStepResult;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.parser.p;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.concurrent.ExecutorService;

final class d extends NetworkTask<a> {
    d(@NonNull NetworkActions networkActions, @NonNull NetworkRequest networkRequest, @NonNull c cVar, @NonNull ExecutorService executorService, @NonNull SomaApiContext somaApiContext, @NonNull Listener<p<ad>, Error> listener) {
        $$Lambda$d$Ph8J4eP9BPH36idaBTBQiqEFOc r0 = new Function(networkRequest, somaApiContext, cVar, listener) {
            private final /* synthetic */ NetworkRequest f$1;
            private final /* synthetic */ SomaApiContext f$2;
            private final /* synthetic */ c f$3;
            private final /* synthetic */ Listener f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final Object apply(Object obj) {
                return d.createRunnable((NetworkTask) obj, NetworkActions.this, this.f$1, this.f$2, $$Lambda$d$k0G2drAh4j9idziQOLLtaj7Uz5E.INSTANCE, this.f$3.a((NetworkTask) obj, this.f$4));
            }
        };
        super(executorService, r0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ TaskStepResult a(HttpURLConnection httpURLConnection) throws IOException {
        Throwable th;
        if (httpURLConnection.getResponseCode() != 200) {
            return TaskStepResult.error(new IOException());
        }
        InputStream inputStream = httpURLConnection.getInputStream();
        try {
            TaskStepResult success = TaskStepResult.success(new a(a(inputStream), httpURLConnection.getContentEncoding()));
            if (inputStream != null) {
                inputStream.close();
            }
            return success;
        } catch (Throwable unused) {
        }
        throw th;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0039, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x003d, code lost:
        if (r6 != null) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0043, code lost:
        r0.close();
     */
    private static byte[] a(@NonNull InputStream inputStream) throws IOException {
        Throwable th;
        Throwable th2;
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = bufferedInputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.close();
                    bufferedInputStream.close();
                    return byteArray;
                }
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            th = r2;
            th2 = th4;
        }
        throw th;
        throw th2;
        if (th != null) {
            try {
                byteArrayOutputStream.close();
            } catch (Throwable unused) {
            }
        } else {
            byteArrayOutputStream.close();
        }
        throw th2;
    }
}
