package com.smaato.sdk.video.utils;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;

public final class d implements Runnable {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Handler f3665a;
    @NonNull
    private final a b;
    private final long c;
    private boolean d;

    @FunctionalInterface
    public interface a {
        void doAction();
    }

    private d(@NonNull Handler handler, long j, @NonNull a aVar) {
        this.f3665a = (Handler) Objects.requireNonNull(handler);
        this.c = 50;
        this.b = (a) Objects.requireNonNull(aVar);
    }

    public d(@NonNull Handler handler, @NonNull a aVar) {
        this(handler, 50, aVar);
    }

    public final void a() {
        Threads.ensureHandlerThread(this.f3665a);
        if (!this.d) {
            this.f3665a.postDelayed(this, this.c);
            this.d = true;
        }
    }

    public final void b() {
        Threads.ensureHandlerThread(this.f3665a);
        if (this.d) {
            this.f3665a.removeCallbacks(this);
            this.d = false;
        }
    }

    public final void run() {
        Threads.ensureHandlerThread(this.f3665a);
        this.d = false;
        a();
        this.b.doAction();
    }
}
