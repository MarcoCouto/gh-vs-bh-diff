package com.smaato.sdk.video.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import androidx.annotation.NonNull;

public class a {

    /* renamed from: a reason: collision with root package name */
    private final long f3661a = 300;

    public a(long j) {
    }

    public final void a(@NonNull View view) {
        view.setAlpha(0.0f);
        view.setVisibility(0);
        view.animate().alpha(1.0f).setDuration(this.f3661a).start();
    }

    public void b(@NonNull final View view) {
        view.animate().alpha(0.0f).setDuration(this.f3661a).setListener(new AnimatorListenerAdapter() {
            public final void onAnimationEnd(Animator animator) {
                view.setVisibility(8);
            }
        }).start();
    }
}
