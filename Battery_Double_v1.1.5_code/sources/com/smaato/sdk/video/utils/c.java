package com.smaato.sdk.video.utils;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public final class c<Event, State> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Map<Event, List<? extends State>> f3663a;

    public static class a<Event, State> {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final Map<Event, List<? extends State>> f3664a = new HashMap();

        @NonNull
        public final a<Event, State> a(@NonNull Event event, @NonNull List<? extends State> list) {
            Objects.requireNonNull(event, "Parameter event can not be null");
            ArrayList<Object> arrayList = new ArrayList<>((Collection) Objects.requireNonNull(list, "Parameter states can not be null"));
            for (Object requireNonNull : arrayList) {
                Objects.requireNonNull(requireNonNull, "a state can not be null");
            }
            if (arrayList.size() <= new HashSet(list).size()) {
                this.f3664a.put(event, arrayList);
                return this;
            }
            throw new IllegalArgumentException("a states must consist of unique states");
        }

        @NonNull
        public final c<Event, State> a() {
            if (!this.f3664a.isEmpty()) {
                return new c<>(this.f3664a, 0);
            }
            throw new IllegalStateException("At least one valid event for states should be added.");
        }
    }

    /* synthetic */ c(Map map, byte b) {
        this(map);
    }

    private c(@NonNull Map<Event, List<? extends State>> map) {
        this.f3663a = map;
    }

    public final boolean a(@NonNull Event event, @NonNull State state) {
        List list = (List) this.f3663a.get(event);
        if (list == null) {
            return false;
        }
        return list.contains(state);
    }
}
