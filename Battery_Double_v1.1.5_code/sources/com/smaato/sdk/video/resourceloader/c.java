package com.smaato.sdk.video.resourceloader;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.ResourceTransformer;
import com.smaato.sdk.core.util.Objects;

public class c implements ResourceTransformer<Uri, Uri> {
    @NonNull
    public /* synthetic */ Object transform(@NonNull Object obj) {
        return (Uri) Objects.requireNonNull((Uri) obj);
    }
}
