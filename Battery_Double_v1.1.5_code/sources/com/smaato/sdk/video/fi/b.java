package com.smaato.sdk.video.fi;

import androidx.annotation.NonNull;

@FunctionalInterface
public interface b<T> {
    void accept(@NonNull T t);
}
