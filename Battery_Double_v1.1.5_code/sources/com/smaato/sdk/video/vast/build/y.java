package com.smaato.sdk.video.vast.build;

import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.ag;
import com.smaato.sdk.video.vast.model.ag.a;

public class y {
    @Nullable
    public static ag a(@Nullable ag agVar, @Nullable ag agVar2) {
        if (agVar == null) {
            return agVar2;
        }
        if (agVar2 == null) {
            return agVar;
        }
        return new a().a(agVar.d).a(com.smaato.sdk.video.ad.a.a(agVar.f3709a, agVar2.f3709a)).b(com.smaato.sdk.video.ad.a.a(agVar.b, agVar2.b)).c(com.smaato.sdk.video.ad.a.a(agVar.c, agVar2.c)).a();
    }
}
