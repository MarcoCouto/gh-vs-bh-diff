package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.VastError;
import com.smaato.sdk.video.vast.build.c.a;
import com.smaato.sdk.video.vast.build.compare.c;
import com.smaato.sdk.video.vast.build.compare.d;
import com.smaato.sdk.video.vast.model.n;
import com.smaato.sdk.video.vast.player.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class k {
    @NonNull
    static c a(@NonNull List<n> list, @NonNull b bVar) {
        if (list.isEmpty()) {
            return new a().a();
        }
        HashSet hashSet = new HashSet();
        ArrayList<n> arrayList = new ArrayList<>(list);
        Collections.sort(arrayList, new c(new d(bVar), new com.smaato.sdk.video.vast.build.compare.b(new com.smaato.sdk.video.vast.build.compare.a(bVar).a())));
        for (n nVar : arrayList) {
            if ("vpaid".equalsIgnoreCase(nVar.l)) {
                hashSet.add(Integer.valueOf(901));
            } else {
                String substring = nVar.f3737a.substring(nVar.f3737a.lastIndexOf(".") + 1);
                if ((nVar.c != null && nVar.c.matches("video/.*(?i)(mp4|3gp|mp2t|webm|mkv)")) || substring.matches("(?i)^(mp4|3gp|mp2t|webm|mkv)$")) {
                    return new a().a(nVar).a();
                }
            }
        }
        hashSet.add(Integer.valueOf(VastError.ERROR_CODE_BAD_FILE));
        return new a().a((Set<Integer>) hashSet).a();
    }
}
