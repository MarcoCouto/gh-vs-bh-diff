package com.smaato.sdk.video.vast.build.compare;

import android.support.graphics.drawable.PathInterpolatorCompat;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.player.b;

public final class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final b f3673a;

    public a(@NonNull b bVar) {
        this.f3673a = (b) Objects.requireNonNull(bVar, "configurationSettings can not be null in AverageBitratePicker");
    }

    public final int a() {
        int max = Math.max(this.f3673a.b, this.f3673a.f3787a);
        if (max <= e.LOW.d) {
            return e.LOW.e;
        }
        if (max <= e.MEDIUM.d) {
            return e.MEDIUM.e;
        }
        return max <= e.HIGH.d ? e.HIGH.e : PathInterpolatorCompat.MAX_NUM_POINTS;
    }
}
