package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.r;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.model.u.a;
import java.util.List;

public class g {
    @NonNull
    public final u a(@NonNull u uVar, @NonNull List<u> list) {
        Objects.requireNonNull(uVar, "Parameter companionScenario should not be null for VastCompanionScenarioMerger::merge");
        Objects.requireNonNull(list, "Parameter wrapperCompanionScenarios should not be null for VastCompanionScenarioMerger::merge");
        u uVar2 = uVar;
        for (u uVar3 : list) {
            r rVar = uVar.b.f3699a;
            r rVar2 = uVar3.b.f3699a;
            if (rVar.equals(rVar2) || rVar2.equals(r.f3745a)) {
                uVar2 = new a(uVar2).a(com.smaato.sdk.video.ad.a.a(uVar2.d, uVar3.d)).b(com.smaato.sdk.video.ad.a.a(uVar2.c, uVar3.c)).a();
            }
        }
        return uVar2;
    }
}
