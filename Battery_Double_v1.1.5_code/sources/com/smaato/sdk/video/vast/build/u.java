package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.model.aa.a;
import com.smaato.sdk.video.vast.model.r;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.model.z;
import com.smaato.sdk.video.vast.player.b;

public class u {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final m f3693a;
    @NonNull
    private final w b;
    @NonNull
    private final y c;
    @NonNull
    private final g d;

    public u(@NonNull m mVar, @NonNull w wVar, @NonNull g gVar, @NonNull y yVar) {
        this.f3693a = (m) Objects.requireNonNull(mVar, "Parameter vastMediaFileScenarioMerger should be null for VastScenarioWrapperMerger::new");
        this.b = (w) Objects.requireNonNull(wVar, "Parameter wrapperCompanionScenarioPicker should be null for VastScenarioWrapperMerger::new");
        this.d = (g) Objects.requireNonNull(gVar, "Parameter vastCompanionScenarioMerger should be null for VastScenarioWrapperMerger::new");
        this.c = (y) Objects.requireNonNull(yVar, "Parameter viewableImpressionMerger should be null for VastScenarioWrapperMerger::new");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final aa a(@NonNull aa aaVar, @NonNull z zVar, @NonNull b bVar) {
        com.smaato.sdk.video.vast.model.u uVar;
        a a2 = aaVar.a().b(com.smaato.sdk.video.ad.a.a(aaVar.f3697a, zVar.f3759a)).a(com.smaato.sdk.video.ad.a.a(aaVar.b, zVar.b)).c(com.smaato.sdk.video.ad.a.a(aaVar.c, zVar.c)).d(com.smaato.sdk.video.ad.a.a(aaVar.d, zVar.d)).a(y.a(aaVar.k, zVar.g));
        x xVar = aaVar.e;
        a2.a(this.f3693a.a(xVar, zVar.e));
        r rVar = xVar.f3755a.f3699a;
        com.smaato.sdk.video.vast.model.u uVar2 = aaVar.f;
        if (uVar2 == null) {
            uVar = w.a(rVar, zVar.f, bVar);
        } else {
            uVar = this.d.a(uVar2, zVar.f);
        }
        if (uVar != null) {
            a2.a(uVar);
        }
        return a2.a();
    }
}
