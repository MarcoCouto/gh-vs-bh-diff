package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.a;
import com.smaato.sdk.video.vast.model.ah;
import java.util.List;

public class z {
    @Nullable
    static d<ah> a(@NonNull List<a> list) {
        if (list.size() == 1) {
            a aVar = (a) list.get(0);
            if (aVar.c != null) {
                return new d<>(aVar, aVar.c);
            }
        } else {
            for (a aVar2 : list) {
                if (aVar2.c != null && aVar2.d == null) {
                    return new d<>(aVar2, aVar2.c);
                }
            }
        }
        return null;
    }
}
