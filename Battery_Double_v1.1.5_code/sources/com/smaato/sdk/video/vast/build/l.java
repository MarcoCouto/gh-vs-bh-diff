package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.ab;
import com.smaato.sdk.video.vast.model.m;
import com.smaato.sdk.video.vast.model.n;
import com.smaato.sdk.video.vast.model.w;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.model.x.a;
import com.smaato.sdk.video.vast.utils.c;

public class l {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final j f3682a;

    public l(@NonNull j jVar) {
        this.f3682a = (j) Objects.requireNonNull(jVar, "Parameter vastIconScenarioPicker should not be null for VastMediaFileScenarioMapper::new");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final x a(@NonNull Logger logger, @NonNull n nVar, @NonNull m mVar, @NonNull ab abVar) {
        Objects.requireNonNull(mVar);
        Objects.requireNonNull(nVar);
        Objects.requireNonNull(mVar);
        Objects.requireNonNull(abVar);
        w a2 = this.f3682a.a(logger, mVar.c);
        long a3 = c.a(mVar.e, logger);
        return new a().a(abVar).a(mVar.b).a(nVar).a(a2).a(mVar.g).a(mVar.f).b(c.b(mVar.d, a3, logger)).a(a3).a();
    }
}
