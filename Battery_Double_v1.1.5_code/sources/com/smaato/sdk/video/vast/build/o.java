package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Sets;
import java.util.Set;

public final class o<Result> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Set<Integer> f3685a;
    @Nullable
    public final Result b;

    public static class a<Result> {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private Result f3686a;
        @Nullable
        private Set<Integer> b;

        @NonNull
        public final a<Result> a(@Nullable Result result) {
            this.f3686a = result;
            return this;
        }

        @NonNull
        public final a<Result> a(@Nullable Set<Integer> set) {
            this.b = set;
            return this;
        }

        @NonNull
        public final o<Result> a() {
            if (this.f3686a != null || this.b != null) {
                return new o<>(Sets.toImmutableSet(this.b), this.f3686a, 0);
            }
            throw new IllegalStateException("VastResult should contain value or list of errors at least");
        }
    }

    /* synthetic */ o(Set set, Object obj, byte b2) {
        this(set, obj);
    }

    private o(@NonNull Set<Integer> set, @Nullable Result result) {
        this.f3685a = (Set) Objects.requireNonNull(set);
        this.b = result;
    }

    @NonNull
    public static <Result> o<Result> a(@NonNull Set<Integer> set) {
        return new o<>(set, null);
    }
}
