package com.smaato.sdk.video.vast.build.compare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.n;
import java.util.Comparator;

public final class c implements Comparator<n> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final d<n> f3675a;
    @NonNull
    private final b b;

    public final /* synthetic */ int compare(@Nullable Object obj, @Nullable Object obj2) {
        n nVar = (n) obj;
        n nVar2 = (n) obj2;
        int a2 = this.f3675a.compare(nVar, nVar2);
        return a2 == 0 ? this.b.compare(nVar, nVar2) : a2;
    }

    public c(@NonNull d<n> dVar, @NonNull b bVar) {
        this.f3675a = (d) Objects.requireNonNull(dVar, "sizeComparator can not be null in MediaFileComparator");
        this.b = (b) Objects.requireNonNull(bVar, "bitrateComparator cannot be null in MediaFileComparator");
    }
}
