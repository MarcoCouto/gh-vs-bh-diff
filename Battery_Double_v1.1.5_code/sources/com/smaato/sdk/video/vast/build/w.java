package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.compare.d;
import com.smaato.sdk.video.vast.model.r;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.player.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class w {
    @Nullable
    public static u a(@NonNull r rVar, @NonNull List<u> list, @NonNull b bVar) {
        ArrayList arrayList;
        ArrayList arrayList2;
        Objects.requireNonNull(rVar, "Parameter universalAdId should not be null for VastWrapperCompanionScenarioPicker::pickWrapperCompanionScenario");
        Objects.requireNonNull(list, "Parameter wrapperVastCompanionScenarios should not be null for VastWrapperCompanionScenarioPicker::pickWrapperCompanionScenario");
        Objects.requireNonNull(bVar, "Parameter vastConfigurationSettings should not be null for VastWrapperCompanionScenarioPicker::pickWrapperCompanionScenario");
        if (list.isEmpty()) {
            return null;
        }
        if (rVar.equals(r.f3745a)) {
            arrayList2 = new ArrayList(list);
        } else {
            arrayList = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (u uVar : list) {
                if (uVar.b.f3699a.equals(rVar)) {
                    arrayList.add(uVar);
                } else if (uVar.b.f3699a.equals(r.f3745a)) {
                    arrayList3.add(uVar);
                }
            }
            if (arrayList.isEmpty()) {
                if (arrayList3.isEmpty()) {
                    arrayList2 = new ArrayList(list);
                } else {
                    arrayList = arrayList3;
                }
            }
            Collections.sort(arrayList, new d(bVar));
            return (u) arrayList.get(0);
        }
        arrayList = arrayList2;
        Collections.sort(arrayList, new d(bVar));
        return (u) arrayList.get(0);
    }
}
