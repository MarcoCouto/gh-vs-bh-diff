package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.video.vast.model.a;
import com.smaato.sdk.video.vast.model.a.C0082a;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.model.ah;

class d<VastModel> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final VastModel f3678a;
    @NonNull
    private a b;

    /* access modifiers changed from: private */
    public static /* synthetic */ a a(a aVar, a aVar2, a aVar3) {
        return aVar3 == aVar ? aVar2 : aVar3;
    }

    @NonNull
    static ad a(@NonNull ad adVar, @NonNull ad adVar2, @NonNull d<ah> dVar) {
        if (adVar2.c.contains(dVar.b)) {
            return new ad.a(adVar2).a(Lists.map(adVar2.c, new NullableFunction(new C0082a(dVar.b).a(new ah.a((ah) dVar.f3678a).a(adVar).a()).a()) {
                private final /* synthetic */ a f$1;

                {
                    this.f$1 = r2;
                }

                public final Object apply(Object obj) {
                    return d.a(a.this, this.f$1, (a) obj);
                }
            })).a();
        }
        throw new IllegalArgumentException("parentVastTree parameter should contains same ad that passed in parentWrapperContainer. Wrong argument passed for WrapperMergeUtilsTest::mergeParsedResultWithParents");
    }

    d(a aVar, VastModel vastmodel) {
        Objects.requireNonNull(aVar, "Parameter ad cannot be null for AdContainer::new");
        Objects.requireNonNull(vastmodel, "Parameter model cannot be null for AdContainer::new");
        this.b = aVar;
        this.f3678a = vastmodel;
    }
}
