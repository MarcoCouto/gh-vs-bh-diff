package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.ab;
import com.smaato.sdk.video.vast.model.ah;
import com.smaato.sdk.video.vast.model.e;
import com.smaato.sdk.video.vast.model.g;
import com.smaato.sdk.video.vast.model.z;
import com.smaato.sdk.video.vast.model.z.a;
import com.smaato.sdk.video.vast.player.b;
import java.util.ArrayList;

public class t {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final e f3692a;
    @NonNull
    private final f b;
    @NonNull
    private final n c;
    @NonNull
    private final p d;

    public t(@NonNull e eVar, @NonNull f fVar, @NonNull n nVar, @NonNull p pVar) {
        this.f3692a = (e) Objects.requireNonNull(eVar, "Parameter vastCompanionPicker should be null for VastScenarioPicker::new");
        this.b = (f) Objects.requireNonNull(fVar, "Parameter vastCompanionScenarioMapper should be null for VastScenarioPicker::new");
        this.c = (n) Objects.requireNonNull(nVar, "Parameter vastMediaFileScenarioWrapperMapper should be null for VastScenarioPicker::new");
        this.d = (p) Objects.requireNonNull(pVar, "Parameter vastScenarioCreativeDataMapper should be null for VastScenarioPicker::new");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final z a(@NonNull Logger logger, @NonNull ah ahVar, @NonNull b bVar) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(bVar);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (g gVar : ahVar.d) {
            ab a2 = p.a(gVar);
            if (gVar.f != null) {
                arrayList.add(this.c.a(logger, gVar.f, a2));
            }
            if (gVar.g != null) {
                e a3 = e.a(gVar.g, bVar);
                if (a3 != null) {
                    arrayList2.add(f.a(logger, a3, a2));
                }
            }
        }
        return new a().a(ahVar.f).a(ahVar.c).d(ahVar.b).e(ahVar.e).a(ahVar.g).c(arrayList2).b(arrayList).a();
    }
}
