package com.smaato.sdk.video.vast.build;

import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.af;
import com.smaato.sdk.video.vast.model.af.a;

public class x {
    @Nullable
    public static af a(@Nullable af afVar, @Nullable af afVar2) {
        if (afVar == null) {
            return afVar2;
        }
        if (afVar2 == null) {
            return afVar;
        }
        return new a().a(afVar.c).a(com.smaato.sdk.video.ad.a.a(afVar.f3707a, afVar2.f3707a)).b(com.smaato.sdk.video.ad.a.a(afVar.b, afVar2.b)).a();
    }
}
