package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class h {

    /* renamed from: a reason: collision with root package name */
    public final int f3679a;
    @Nullable
    public final String b;
    public final long c;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private int f3680a;
        private long b;
        @Nullable
        private String c;

        public a(int i) {
            this.f3680a = i;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final a a(long j) {
            this.b = j;
            return this;
        }

        @NonNull
        public final h a() {
            h hVar = new h(this.f3680a, this.b, this.c, 0);
            return hVar;
        }
    }

    /* synthetic */ h(int i, long j, String str, byte b2) {
        this(i, j, str);
    }

    private h(int i, long j, @Nullable String str) {
        this.f3679a = i;
        this.c = j;
        this.b = str;
    }
}
