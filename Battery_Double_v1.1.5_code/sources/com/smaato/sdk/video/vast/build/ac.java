package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.VastError;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.build.o.a;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.model.ah;
import com.smaato.sdk.video.vast.parser.o;
import com.smaato.sdk.video.vast.parser.p;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ac {

    /* renamed from: a reason: collision with root package name */
    private final int f3670a;
    @NonNull
    private final aa b;
    @NonNull
    private final b c;
    @NonNull
    private final z d;

    public ac(int i, @NonNull aa aaVar, @NonNull b bVar, @NonNull z zVar) {
        if (i >= 0) {
            this.c = bVar;
            this.d = zVar;
            this.f3670a = i;
            this.b = aaVar;
            return;
        }
        throw new IllegalArgumentException("Cannot construct WrapperResolver: maxDepth can't be negative");
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull ad adVar, boolean z, int i, @NonNull b<o<ad>> bVar) {
        ad adVar2 = adVar;
        b<o<ad>> bVar2 = bVar;
        a a2 = new a().a(adVar);
        if (adVar2.c.isEmpty()) {
            a2.a(Collections.singleton(Integer.valueOf(VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD)));
            bVar2.accept(a2.a());
        } else if (b.a(adVar2.c)) {
            bVar2.accept(a2.a());
        } else if (!z) {
            a2.a(Collections.singleton(Integer.valueOf(VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD)));
            bVar2.accept(a2.a());
        } else {
            d a3 = z.a(adVar2.c);
            if (a3 == null) {
                a2.a(Collections.singleton(Integer.valueOf(VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD)));
                bVar2.accept(a2.a());
            } else if (i > this.f3670a) {
                a2.a(Collections.singleton(Integer.valueOf(302)));
                bVar2.accept(a2.a());
            } else {
                aa aaVar = this.b;
                ah ahVar = (ah) a3.f3678a;
                $$Lambda$ac$USMiZ5EAyFGV7QQY2XsctCmpfdE r0 = new b(logger, somaApiContext, a3, i, bVar, adVar) {
                    private final /* synthetic */ Logger f$1;
                    private final /* synthetic */ SomaApiContext f$2;
                    private final /* synthetic */ d f$3;
                    private final /* synthetic */ int f$4;
                    private final /* synthetic */ b f$5;
                    private final /* synthetic */ ad f$6;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                        this.f$4 = r5;
                        this.f$5 = r6;
                        this.f$6 = r7;
                    }

                    public final void accept(Object obj) {
                        ac.this.a(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (p) obj);
                    }
                };
                Logger logger2 = logger;
                SomaApiContext somaApiContext2 = somaApiContext;
                aaVar.a(logger, somaApiContext, ahVar, r0);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, SomaApiContext somaApiContext, d dVar, int i, b bVar, ad adVar, p pVar) {
        d dVar2 = dVar;
        p pVar2 = pVar;
        $$Lambda$ac$TcLOKvPrbUIsyPenv8ZCeXZKmyA r2 = new b(bVar, adVar, dVar) {
            private final /* synthetic */ b f$1;
            private final /* synthetic */ ad f$2;
            private final /* synthetic */ d f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                ac.this.a(this.f$1, this.f$2, this.f$3, (o) obj);
            }
        };
        a aVar = new a();
        HashSet hashSet = new HashSet();
        aVar.a((Set<Integer>) hashSet);
        if (!pVar2.f3776a.isEmpty()) {
            hashSet.addAll(Lists.map(pVar2.f3776a, $$Lambda$ac$DgLFnX9iVWXKuy9VWB6UQvXEOoE.INSTANCE));
            hashSet.remove(null);
        }
        if (pVar2.b == null) {
            if (!pVar2.f3776a.isEmpty()) {
                hashSet.add(Integer.valueOf(100));
            }
            r2.accept(aVar.a());
            return;
        }
        a(logger, somaApiContext, (ad) pVar2.b, ((ah) dVar2.f3678a).f3711a, i + 1, new b(hashSet, aVar, pVar2, r2) {
            private final /* synthetic */ Set f$0;
            private final /* synthetic */ a f$1;
            private final /* synthetic */ p f$2;
            private final /* synthetic */ b f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                ac.a(this.f$0, this.f$1, this.f$2, this.f$3, (o) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Integer a(o oVar) {
        Exception exc = oVar == null ? null : oVar.f3774a;
        if (exc instanceof com.smaato.sdk.video.vast.exceptions.wrapper.a) {
            return Integer.valueOf(VastError.ERROR_CODE_GENERAL_WRAPPER);
        }
        if (exc instanceof com.smaato.sdk.video.vast.exceptions.wrapper.b) {
            return Integer.valueOf(VastError.ERROR_CODE_BAD_URI);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Set set, a aVar, p pVar, b bVar, o oVar) {
        set.addAll(oVar.f3685a);
        if (oVar.b != null) {
            aVar.a(oVar.b);
        } else {
            aVar.a(pVar.b);
        }
        bVar.accept(aVar.a());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(b bVar, ad adVar, d dVar, o oVar) {
        a aVar = new a();
        HashSet hashSet = new HashSet(oVar.f3685a);
        aVar.a((Set<Integer>) hashSet);
        ad adVar2 = (ad) oVar.b;
        if (adVar2 == null) {
            hashSet.add(Integer.valueOf(VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD));
            aVar.a(adVar);
        } else {
            aVar.a(d.a(adVar2, adVar, dVar));
        }
        bVar.accept(aVar.a());
    }
}
