package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.s.a;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.model.ah;
import com.smaato.sdk.video.vast.model.k;
import com.smaato.sdk.video.vast.model.z;
import com.smaato.sdk.video.vast.player.b;
import java.util.HashSet;
import java.util.Set;

public class r {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f3689a;
    @NonNull
    private final z b;
    @NonNull
    private final u c;
    @NonNull
    private final q d;
    @NonNull
    private final t e;

    public r(@NonNull a aVar, @NonNull z zVar, @NonNull u uVar, @NonNull q qVar, @NonNull t tVar) {
        this.f3689a = (a) Objects.requireNonNull(aVar, "Parameter inLineAdContainerPicker should be null for VastScenarioPicker::new");
        this.b = (z) Objects.requireNonNull(zVar, "Parameter wrapperAdContainerPicker should be null for VastScenarioPicker::new");
        this.c = (u) Objects.requireNonNull(uVar, "Parameter vastScenarioWrapperMerger should be null for VastScenarioPicker::new");
        this.d = (q) Objects.requireNonNull(qVar, "Parameter vastScenarioMapper should be null for VastScenarioPicker::new");
        this.e = (t) Objects.requireNonNull(tVar, "Parameter vastScenarioWrapperMapper should be null for VastScenarioPicker::new");
    }

    @NonNull
    public final s a(@NonNull Logger logger, @NonNull ad adVar, @NonNull b bVar) {
        Objects.requireNonNull(logger, "Parameter logger should not be null for VastScenarioPicker::pickVastScenario");
        Objects.requireNonNull(adVar, "Parameter vastTree should not be null for VastScenarioPicker::pickVastScenario");
        Objects.requireNonNull(bVar, "Parameter vastConfigurationSettings should not be null for VastScenarioPicker::pickVastScenario");
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet(adVar.b);
        a b2 = new a().a((Set<Integer>) hashSet).b(hashSet2);
        if (adVar.c.isEmpty()) {
            return b2.a();
        }
        d a2 = a.a(adVar.c);
        if (a2 != null) {
            s a3 = this.d.a(logger, (k) a2.f3678a, bVar);
            hashSet.addAll(a3.b);
            hashSet2.addAll(a3.f3690a);
            return b2.a(a3.c).a();
        }
        d a4 = z.a(adVar.c);
        if (a4 != null) {
            ah ahVar = (ah) a4.f3678a;
            hashSet2.addAll(ahVar.e);
            if (ahVar.k != null) {
                z a5 = this.e.a(logger, ahVar, bVar);
                hashSet2.addAll(a5.d);
                s a6 = a(logger, ahVar.k, bVar);
                hashSet.addAll(a6.b);
                hashSet2.addAll(a6.f3690a);
                if (a6.c != null) {
                    b2.a(this.c.a(a6.c, a5, bVar));
                }
            }
        }
        return b2.a();
    }
}
