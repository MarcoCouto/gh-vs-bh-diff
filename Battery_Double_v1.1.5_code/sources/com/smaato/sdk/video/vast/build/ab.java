package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.video.vast.exceptions.wrapper.a;
import com.smaato.sdk.video.vast.exceptions.wrapper.b;

public class ab {
    @NonNull
    static Exception a(@NonNull Error error, @NonNull String str) {
        switch (error) {
            case TIMEOUT:
            case IO_ERROR:
            case IO_TOO_MANY_REDIRECTS:
            case NO_NETWORK_CONNECTION:
                return new b(str);
            default:
                return new a(str);
        }
    }
}
