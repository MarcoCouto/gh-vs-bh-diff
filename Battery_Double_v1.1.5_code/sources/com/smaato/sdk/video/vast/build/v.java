package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.build.o.a;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.parser.p;
import com.smaato.sdk.video.vast.parser.x;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class v {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final x f3694a;
    @NonNull
    private final ac b;

    public v(@NonNull x xVar, @NonNull ac acVar) {
        xVar.getClass();
        this.f3694a = xVar;
        acVar.getClass();
        this.b = acVar;
    }

    public final void a(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull InputStream inputStream, @Nullable String str, @NonNull b<o<ad>> bVar) {
        logger.getClass();
        somaApiContext.getClass();
        inputStream.getClass();
        this.f3694a.a(logger, inputStream, str, new b(bVar, logger, somaApiContext) {
            private final /* synthetic */ b f$1;
            private final /* synthetic */ Logger f$2;
            private final /* synthetic */ SomaApiContext f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                v.this.a(this.f$1, this.f$2, this.f$3, (p) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(b bVar, Logger logger, SomaApiContext somaApiContext, p pVar) {
        HashSet hashSet = new HashSet();
        ad adVar = (ad) pVar.b;
        if (adVar != null) {
            if (adVar.c.isEmpty() && !pVar.f3776a.isEmpty()) {
                bVar.accept(o.a(Collections.singleton(Integer.valueOf(100))));
            }
            this.b.a(logger, somaApiContext, adVar, true, 0, new b(hashSet, bVar) {
                private final /* synthetic */ Set f$0;
                private final /* synthetic */ b f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    v.a(this.f$0, this.f$1, (o) obj);
                }
            });
            return;
        }
        bVar.accept(o.a(Collections.singleton(Integer.valueOf(100))));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Set set, b bVar, o oVar) {
        a aVar = new a();
        set.addAll(oVar.f3685a);
        aVar.a(set).a(oVar.b);
        bVar.accept(aVar.a());
    }
}
