package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.model.a;
import java.util.List;

public class b {
    static boolean a(@NonNull List<a> list) {
        if (list.size() == 1) {
            return ((a) list.get(0)).b != null;
        }
        for (a aVar : list) {
            if (aVar.b != null && aVar.d == null) {
                return true;
            }
        }
        return false;
    }
}
