package com.smaato.sdk.video.vast.build.compare;

import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.n;
import java.util.Comparator;

public final class b implements Comparator<n> {

    /* renamed from: a reason: collision with root package name */
    private final int f3674a;

    public b(int i) {
        this.f3674a = i;
    }

    /* renamed from: a */
    public final int compare(@Nullable n nVar, @Nullable n nVar2) {
        if ((nVar == null) ^ (nVar2 == null)) {
            return nVar == null ? 1 : -1;
        }
        if (nVar == null) {
            return 0;
        }
        float f = 0.0f;
        float intValue = nVar.g == null ? 0.0f : (float) nVar.g.intValue();
        if (nVar2.g != null) {
            f = (float) nVar2.g.intValue();
        }
        return Float.compare(Math.abs(((float) this.f3674a) - intValue), Math.abs(((float) this.f3674a) - f));
    }
}
