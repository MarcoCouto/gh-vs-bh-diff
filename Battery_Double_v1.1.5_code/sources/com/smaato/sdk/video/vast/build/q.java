package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.compare.c;
import com.smaato.sdk.video.vast.build.compare.d;
import com.smaato.sdk.video.vast.model.ab;
import com.smaato.sdk.video.vast.model.e;
import com.smaato.sdk.video.vast.model.g;
import com.smaato.sdk.video.vast.model.k;
import com.smaato.sdk.video.vast.model.m;
import com.smaato.sdk.video.vast.model.n;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.player.b;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class q {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final k f3687a;
    @NonNull
    private final e b;
    @NonNull
    private final f c;
    @NonNull
    private final l d;
    @NonNull
    private final p e;

    static final class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final g f3688a;
        @NonNull
        public final m b;

        /* synthetic */ a(g gVar, m mVar, byte b2) {
            this(gVar, mVar);
        }

        private a(@NonNull g gVar, @NonNull m mVar) {
            this.f3688a = (g) Objects.requireNonNull(gVar);
            this.b = (m) Objects.requireNonNull(mVar);
        }
    }

    public q(@NonNull k kVar, @NonNull e eVar, @NonNull f fVar, @NonNull l lVar, @NonNull p pVar) {
        this.f3687a = (k) Objects.requireNonNull(kVar, "Parameter vastLinearMediaFilePicker should be null for VastScenarioPicker::new");
        this.b = (e) Objects.requireNonNull(eVar, "Parameter vastCompanionPicker should be null for VastScenarioPicker::new");
        this.c = (f) Objects.requireNonNull(fVar, "Parameter vastCompanionScenarioMapper should be null for VastScenarioPicker::new");
        this.d = (l) Objects.requireNonNull(lVar, "Parameter vastMediaFileScenarioMapper should be null for VastScenarioPicker::new");
        this.e = (p) Objects.requireNonNull(pVar, "Parameter vastScenarioCreativeDataMapper should be null for VastScenarioPicker::new");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final s a(@NonNull Logger logger, @NonNull k kVar, @NonNull b bVar) {
        boolean z;
        Objects.requireNonNull(logger);
        Objects.requireNonNull(bVar);
        HashSet hashSet = new HashSet();
        com.smaato.sdk.video.vast.build.s.a b2 = new com.smaato.sdk.video.vast.build.s.a().a((Set<Integer>) hashSet).b(new HashSet(kVar.e));
        TreeMap treeMap = new TreeMap(new c(new d(bVar), new com.smaato.sdk.video.vast.build.compare.b(new com.smaato.sdk.video.vast.build.compare.a(bVar).a())));
        HashSet hashSet2 = new HashSet();
        Iterator it = kVar.b.iterator();
        while (true) {
            z = false;
            if (!it.hasNext()) {
                break;
            }
            g gVar = (g) it.next();
            m mVar = gVar.f;
            if (mVar != null && !mVar.f3735a.isEmpty()) {
                c a2 = k.a(mVar.f3735a, bVar);
                if (a2.b != null) {
                    treeMap.put(a2.b, new a(gVar, mVar, 0));
                    break;
                }
                hashSet2.addAll(a2.f3671a);
            }
        }
        if (treeMap.isEmpty()) {
            if (hashSet2.isEmpty()) {
                hashSet.add(Integer.valueOf(400));
            } else {
                hashSet.addAll(hashSet2);
            }
            return b2.a();
        }
        Entry firstEntry = treeMap.firstEntry();
        g gVar2 = ((a) firstEntry.getValue()).f3688a;
        m mVar2 = ((a) firstEntry.getValue()).b;
        n nVar = (n) firstEntry.getKey();
        ab a3 = p.a(gVar2);
        x a4 = this.d.a(logger, nVar, mVar2, a3);
        u uVar = null;
        e a5 = gVar2.g != null ? e.a(gVar2.g, bVar) : null;
        if (a5 == null) {
            a5 = this.b.a(kVar.b, bVar);
        }
        if (a5 == null) {
            if (gVar2.g != null && !gVar2.g.f3721a.isEmpty()) {
                z = true;
            }
            if (z) {
                hashSet.add(Integer.valueOf(600));
            }
        } else {
            uVar = f.a(logger, a5, a3);
        }
        return b2.a(new com.smaato.sdk.video.vast.model.aa.a().a(kVar.f).a(kVar.g).a(kVar.c).c(kVar.i).c(kVar.d).b(kVar.h).d(kVar.e).b(kVar.f3731a).a(kVar.j).a(a4).a(uVar).a()).a();
    }
}
