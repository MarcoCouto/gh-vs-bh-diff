package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.compare.d;
import com.smaato.sdk.video.vast.model.f;
import com.smaato.sdk.video.vast.model.g;
import com.smaato.sdk.video.vast.player.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class e {
    @Nullable
    public static com.smaato.sdk.video.vast.model.e a(@NonNull f fVar, @NonNull b bVar) {
        Objects.requireNonNull(fVar, "Parameter companionAds should not be null for VastCompanionPicker::pickCompanion");
        Objects.requireNonNull(bVar, "Parameter vastConfigurationSettings should not be null for VastCompanionPicker::pickCompanion");
        ArrayList<com.smaato.sdk.video.vast.model.e> arrayList = new ArrayList<>(fVar.f3721a);
        Collections.sort(arrayList, new d(bVar));
        for (com.smaato.sdk.video.vast.model.e eVar : arrayList) {
            if (!eVar.c.isEmpty() || !eVar.d.isEmpty()) {
                return eVar;
            }
            if (!eVar.e.isEmpty()) {
                return eVar;
            }
        }
        return null;
    }

    @Nullable
    public final com.smaato.sdk.video.vast.model.e a(@NonNull List<g> list, @NonNull b bVar) {
        Objects.requireNonNull(list, "Parameter creatives should not be null for VastCompanionPicker::pickCompanion");
        Objects.requireNonNull(bVar, "Parameter vastConfigurationSettings should not be null for VastCompanionPicker::pickCompanion");
        ArrayList arrayList = new ArrayList();
        for (g gVar : list) {
            if (gVar.g != null) {
                com.smaato.sdk.video.vast.model.e a2 = a(gVar.g, bVar);
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        Collections.sort(arrayList, new d(bVar));
        return (com.smaato.sdk.video.vast.model.e) arrayList.get(0);
    }
}
