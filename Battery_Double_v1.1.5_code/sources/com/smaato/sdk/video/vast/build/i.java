package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.ac;
import com.smaato.sdk.video.vast.model.ac.a;
import com.smaato.sdk.video.vast.model.p;
import com.smaato.sdk.video.vast.model.w;
import com.smaato.sdk.video.vast.utils.c;

public class i {
    @Nullable
    static w a(@NonNull Logger logger, @NonNull com.smaato.sdk.video.vast.model.i iVar) {
        Objects.requireNonNull(logger);
        try {
            ac a2 = new a().a(iVar.b.isEmpty() ? null : (p) iVar.b.get(0)).b(iVar.d.isEmpty() ? null : (String) iVar.d.get(0)).a(iVar.c.isEmpty() ? null : (String) iVar.c.get(0)).a();
            long a3 = c.a(iVar.k, logger);
            return new w.a().a(a2).a(iVar.m).a(iVar.f3727a).a(iVar.f).b(iVar.g).b(iVar.h).c(iVar.i).c(iVar.l).a(c.b(iVar.j, a3, logger)).a(iVar.e).d(iVar.n).b(a3).a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            logger.error(LogDomain.VAST, e, "Cannot build VastIconScenario", new Object[0]);
            return null;
        }
    }
}
