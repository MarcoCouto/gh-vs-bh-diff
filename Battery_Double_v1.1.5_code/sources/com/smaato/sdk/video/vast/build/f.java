package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.ab;
import com.smaato.sdk.video.vast.model.ac;
import com.smaato.sdk.video.vast.model.e;
import com.smaato.sdk.video.vast.model.p;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.model.u.a;

public class f {
    @Nullable
    public static u a(@NonNull Logger logger, @NonNull e eVar, @NonNull ab abVar) {
        Objects.requireNonNull(logger);
        try {
            return new a().a(new ac.a().a(eVar.c.isEmpty() ? null : (p) eVar.c.get(0)).b(eVar.e.isEmpty() ? null : (String) eVar.e.get(0)).a(eVar.d.isEmpty() ? null : (String) eVar.d.get(0)).a()).a(abVar).a(eVar.f).a(eVar.q).c(eVar.o).d(eVar.p).e(eVar.n).a(eVar.g).b(eVar.h).d(eVar.j).c(eVar.i).f(eVar.l).e(eVar.k).g(eVar.m).a(eVar.b).b(eVar.r).b(eVar.f3719a).a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            logger.error(LogDomain.VAST, e, "Cannot build VastCompanionScenario", new Object[0]);
            return null;
        }
    }
}
