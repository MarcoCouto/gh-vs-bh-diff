package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.k;
import java.util.List;

public class a {
    @Nullable
    static d<k> a(@NonNull List<com.smaato.sdk.video.vast.model.a> list) {
        if (list.size() == 1) {
            com.smaato.sdk.video.vast.model.a aVar = (com.smaato.sdk.video.vast.model.a) list.get(0);
            if (aVar.b != null) {
                return new d<>(aVar, aVar.b);
            }
        } else {
            for (com.smaato.sdk.video.vast.model.a aVar2 : list) {
                if (aVar2.b != null && aVar2.d == null) {
                    return new d<>(aVar2, aVar2.b);
                }
            }
        }
        return null;
    }
}
