package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.video.vast.model.aa;
import java.util.Set;

public final class s {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Set<String> f3690a;
    @NonNull
    public final Set<Integer> b;
    @Nullable
    public final aa c;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private aa f3691a;
        @Nullable
        private Set<String> b;
        @Nullable
        private Set<Integer> c;

        @NonNull
        public final a a(@Nullable aa aaVar) {
            this.f3691a = aaVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable Set<Integer> set) {
            this.c = set;
            return this;
        }

        @NonNull
        public final a b(@Nullable Set<String> set) {
            this.b = set;
            return this;
        }

        @NonNull
        public final s a() {
            return new s(Sets.toImmutableSet(this.b), Sets.toImmutableSet(this.c), this.f3691a, 0);
        }
    }

    /* synthetic */ s(Set set, Set set2, aa aaVar, byte b2) {
        this(set, set2, aaVar);
    }

    private s(@NonNull Set<String> set, @NonNull Set<Integer> set2, @Nullable aa aaVar) {
        set.getClass();
        this.f3690a = set;
        set2.getClass();
        this.b = set2;
        this.c = aaVar;
    }
}
