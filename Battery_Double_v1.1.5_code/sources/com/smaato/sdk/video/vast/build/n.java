package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.ab;
import com.smaato.sdk.video.vast.model.m;
import com.smaato.sdk.video.vast.model.w;
import com.smaato.sdk.video.vast.model.y;
import com.smaato.sdk.video.vast.model.y.a;
import com.smaato.sdk.video.vast.utils.c;

public class n {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final j f3684a;

    public n(@NonNull j jVar) {
        this.f3684a = (j) Objects.requireNonNull(jVar, "Parameter vastIconScenarioPicker should not be null for VastMediaFileScenarioMapper::new");
    }

    @NonNull
    public final y a(@NonNull Logger logger, @NonNull m mVar, @NonNull ab abVar) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(abVar);
        w a2 = this.f3684a.a(logger, mVar.c);
        long a3 = c.a(mVar.e, logger);
        return new a().a(abVar).a(a2).a(mVar.b).a(mVar.g).a(mVar.f).b(c.b(mVar.d, a3, logger)).a(a3).a();
    }
}
