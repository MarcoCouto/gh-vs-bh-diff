package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.video.vast.model.n;
import java.util.Set;

public final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Set<Integer> f3671a;
    @Nullable
    public final n b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private n f3672a;
        @Nullable
        private Set<Integer> b;

        @NonNull
        public final a a(@Nullable n nVar) {
            this.f3672a = nVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable Set<Integer> set) {
            this.b = set;
            return this;
        }

        @NonNull
        public final c a() {
            return new c(Sets.toImmutableSet(this.b), this.f3672a, 0);
        }
    }

    /* synthetic */ c(Set set, n nVar, byte b2) {
        this(set, nVar);
    }

    private c(@NonNull Set<Integer> set, @Nullable n nVar) {
        set.getClass();
        this.f3671a = set;
        this.b = nVar;
    }
}
