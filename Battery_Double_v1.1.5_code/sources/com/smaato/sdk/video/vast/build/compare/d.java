package com.smaato.sdk.video.vast.build.compare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.o;
import com.smaato.sdk.video.vast.player.b;
import java.util.Comparator;

public final class d<T extends o> implements Comparator<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final b f3676a;

    public d(@NonNull b bVar) {
        this.f3676a = bVar;
    }

    /* renamed from: a */
    public final int compare(@Nullable T t, @Nullable T t2) {
        if (t == null && t2 != null) {
            return 1;
        }
        if (t2 == null && t != null) {
            return -1;
        }
        if (t2 == null) {
            return 0;
        }
        float f = 0.0f;
        float floatValue = t.b() == null ? 0.0f : t.b().floatValue();
        float floatValue2 = t.a() == null ? 0.0f : t.a().floatValue();
        float floatValue3 = t2.b() == null ? 0.0f : t2.b().floatValue();
        if (t2.a() != null) {
            f = t2.a().floatValue();
        }
        return Float.compare(Math.abs(((float) this.f3676a.f3787a) - floatValue) + Math.abs(((float) this.f3676a.b) - floatValue2), Math.abs(((float) this.f3676a.f3787a) - floatValue3) + Math.abs(((float) this.f3676a.b) - f));
    }
}
