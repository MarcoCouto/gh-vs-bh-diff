package com.smaato.sdk.video.vast.build;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.network.b;
import com.smaato.sdk.video.vast.exceptions.wrapper.a;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.model.ah;
import com.smaato.sdk.video.vast.parser.p;

public class aa {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ab f3667a;
    @NonNull
    private final b b;

    public aa(@NonNull ab abVar, @NonNull b bVar) {
        this.f3667a = (ab) Objects.requireNonNull(abVar);
        this.b = (b) Objects.requireNonNull(bVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull final ah ahVar, @NonNull final com.smaato.sdk.video.fi.b<p<ad>> bVar) {
        if (TextUtils.isEmpty(ahVar.j)) {
            bVar.accept(p.a("VastAdTagURI", new a("Cannot resolve wrapper: vastAdTagUri is missing")));
        } else {
            this.b.submitRequest(ahVar.j, somaApiContext, new Listener<p<ad>, Error>() {
                public final /* synthetic */ void onFailure(@NonNull Task task, @NonNull Object obj) {
                    Error error = (Error) obj;
                    String format = String.format("Failed to load Vast url: %s due to error: %s", new Object[]{ahVar.j, error});
                    logger.error(LogDomain.VAST, format, new Object[0]);
                    com.smaato.sdk.video.fi.b bVar = bVar;
                    String str = VastTagName.WRAPPER;
                    aa.this.f3667a;
                    bVar.accept(p.a(str, ab.a(error, format)));
                }

                public final /* synthetic */ void onSuccess(@NonNull Task task, @NonNull Object obj) {
                    bVar.accept((p) obj);
                }
            }).start();
        }
    }
}
