package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.i;
import com.smaato.sdk.video.vast.model.w;
import java.util.List;

public class j {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final i f3681a;

    public j(@NonNull i iVar) {
        this.f3681a = (i) Objects.requireNonNull(iVar);
    }

    @Nullable
    public final w a(@NonNull Logger logger, @NonNull List<i> list) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(list);
        w wVar = null;
        if (list.isEmpty()) {
            return null;
        }
        for (i iVar : list) {
            if (!iVar.b.isEmpty() || !iVar.c.isEmpty() || !iVar.d.isEmpty()) {
                wVar = i.a(logger, iVar);
                if (wVar != null) {
                    break;
                }
            }
        }
        return wVar;
    }
}
