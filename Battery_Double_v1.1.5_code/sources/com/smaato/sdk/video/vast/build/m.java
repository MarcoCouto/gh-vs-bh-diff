package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.ad.a;
import com.smaato.sdk.video.vast.model.r;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.model.y;
import java.util.List;

public class m {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private x f3683a;

    public m(@NonNull x xVar) {
        this.f3683a = (x) Objects.requireNonNull(xVar, "Parameter videoClicksMerger should not be null for VastMediaFileScenarioMerger::new");
    }

    @NonNull
    public final x a(@NonNull x xVar, @NonNull List<y> list) {
        Objects.requireNonNull(xVar, "Parameter mediaFileScenario should not be null for VastMediaFileScenarioMerger::merge");
        Objects.requireNonNull(list, "Parameter wrapperMediaFileScenarios should not be null for VastMediaFileScenarioMerger::merge");
        x xVar2 = xVar;
        for (y yVar : list) {
            r rVar = xVar.f3755a.f3699a;
            r rVar2 = yVar.f3757a.f3699a;
            if (rVar.equals(rVar2) || rVar2.equals(r.f3745a)) {
                xVar2 = xVar2.a().a(x.a(xVar2.e, yVar.c)).a(a.a(xVar2.c, yVar.b)).a(xVar2.h == null ? yVar.d : xVar2.h).a();
            }
        }
        return xVar2;
    }
}
