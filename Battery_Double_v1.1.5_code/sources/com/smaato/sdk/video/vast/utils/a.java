package com.smaato.sdk.video.vast.utils;

import android.text.TextUtils;
import android.webkit.URLUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.ac;
import com.smaato.sdk.video.vast.model.p;
import com.smaato.sdk.video.vast.model.p.b;
import java.util.Locale;

public class a {
    @Nullable
    public static String a(@NonNull ac acVar, int i, int i2) {
        String str;
        String str2;
        if (i == 0) {
            str = "100%";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(i);
            sb.append("px");
            str = sb.toString();
        }
        if (i2 == 0) {
            str2 = "100%";
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(i2);
            sb2.append("px");
            str2 = sb2.toString();
        }
        if (acVar.f3701a != null) {
            p pVar = acVar.f3701a;
            b bVar = pVar.b;
            if (URLUtil.isValidUrl(pVar.f3739a)) {
                switch (AnonymousClass1.f3828a[bVar.ordinal()]) {
                    case 1:
                        return String.format(Locale.US, "<html><head></head><body style=\"margin:0;padding:0;-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"><img src=\"%1$s\" width=\"100%%\" style=\"max-width:100%%;max-height:100%%;\" /></body></html>", new Object[]{pVar.f3739a});
                    case 2:
                        return String.format(Locale.US, "<script src=\"%1$s\"></script>", new Object[]{pVar.f3739a});
                    case 3:
                        return String.format(Locale.US, "<html><head></head><body style=\"margin:0;padding:0;-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">%1$s</body></html>", new Object[]{pVar.f3739a});
                }
            }
            return "";
        } else if (!TextUtils.isEmpty(acVar.c)) {
            return String.format(Locale.US, "<html><head></head><body style=\"margin:0;padding:0;-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">%1$s</body></html>", new Object[]{acVar.c});
        } else if (TextUtils.isEmpty(acVar.b)) {
            return null;
        } else {
            return String.format(Locale.US, "<iframe frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" style=\"border:0px;margin:0;padding:0\" width=\"%1$s\" height=\"%2$s\" src=\"%3$s\"></iframe>", new Object[]{str, str2, acVar.b});
        }
    }
}
