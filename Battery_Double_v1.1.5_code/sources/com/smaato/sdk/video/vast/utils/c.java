package com.smaato.sdk.video.vast.utils;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;

public final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private static final Pattern f3829a = Pattern.compile("^(\\d?\\d(\\.\\d*)?|100(?:\\.0*)?)%$");
    @NonNull
    private static final Pattern b = Pattern.compile("^(([01]?[0-9]|2[0-3])(:|\\.)[0-5][0-9](:|\\.)[0-5][0-9](:|\\.)\\d{1,3}$)");
    @NonNull
    private static final Pattern c = Pattern.compile("^(([01]?[0-9]|2[0-3])(:|\\.)[0-5][0-9](:|\\.)[0-5][0-9]$)");
    @NonNull
    private static final Pattern d = Pattern.compile("^([0-5][0-9](:|\\.)[0-5][0-9]$)");
    @NonNull
    private static final Pattern e = Pattern.compile("^([0-5][0-9]$)");

    public static int a(@Nullable String str, long j, @NonNull Logger logger) {
        if (j == 0) {
            return -1;
        }
        return (int) ((b(str, j, logger) * 100) / j);
    }

    public static long b(@Nullable String str, long j, @NonNull Logger logger) {
        if (!TextUtils.isEmpty(str)) {
            long b2 = b(str, logger);
            if (b2 >= 0) {
                return b2;
            }
            if (f3829a.matcher(str).matches()) {
                try {
                    Float valueOf = Float.valueOf(Float.parseFloat(str.replace("%", "")));
                    if (valueOf.floatValue() != 0.0f) {
                        return (long) Math.round(((float) (j / 100)) * valueOf.floatValue());
                    }
                    return -1;
                } catch (NumberFormatException unused) {
                    logger.debug(LogDomain.VAST, "Invalid baseOffsetInMs value: %s", str);
                    return -1;
                }
            }
        }
        return -1;
    }

    public static long a(@Nullable String str, @NonNull Logger logger) {
        if (!TextUtils.isEmpty(str)) {
            return b(str, logger);
        }
        return -1;
    }

    private static long a(@NonNull String str, @NonNull Logger logger, @NonNull String str2) {
        String replace = str.replace('.', ':');
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str2, Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return simpleDateFormat.parse(replace).getTime();
        } catch (ParseException unused) {
            logger.debug(LogDomain.VAST, "Received invalid duration parameter: %s", replace);
            return -1;
        }
    }

    private static long b(@NonNull String str, @NonNull Logger logger) {
        if (b.matcher(str).matches()) {
            return a(str, logger, "HH:mm:ss:SSS");
        }
        if (c.matcher(str).matches()) {
            return a(str, logger, "HH:mm:ss");
        }
        if (d.matcher(str).matches()) {
            return a(str, logger, "mm:ss");
        }
        if (e.matcher(str).matches()) {
            return a(str, logger, "ss");
        }
        return -1;
    }
}
