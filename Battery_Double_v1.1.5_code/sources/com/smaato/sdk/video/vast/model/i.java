package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class i {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<String> f3727a;
    @NonNull
    public final List<p> b;
    @NonNull
    public final List<String> c;
    @NonNull
    public final List<String> d;
    @Nullable
    public final String e;
    @Nullable
    public final Float f;
    @Nullable
    public final Float g;
    @Nullable
    public final String h;
    @Nullable
    public final String i;
    @Nullable
    public final String j;
    @Nullable
    public final String k;
    @Nullable
    public final Float l;
    @Nullable
    public final j m;
    @Nullable
    public final String n;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private List<String> f3728a;
        @Nullable
        private String b;
        @Nullable
        private Float c;
        @Nullable
        private Float d;
        @Nullable
        private String e;
        @Nullable
        private String f;
        @Nullable
        private String g;
        @Nullable
        private String h;
        @Nullable
        private Float i;
        @Nullable
        private j j;
        @Nullable
        private List<p> k;
        @Nullable
        private List<String> l;
        @Nullable
        private List<String> m;
        @Nullable
        private String n;

        @NonNull
        public final a a(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Float f2) {
            this.c = f2;
            return this;
        }

        @NonNull
        public final a b(@Nullable Float f2) {
            this.d = f2;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public final a d(@Nullable String str) {
            this.g = str;
            return this;
        }

        @NonNull
        public final a e(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable Float f2) {
            this.i = f2;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<String> list) {
            this.f3728a = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable j jVar) {
            this.j = jVar;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<p> list) {
            this.k = list;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<String> list) {
            this.l = list;
            return this;
        }

        @NonNull
        public final a d(@Nullable List<String> list) {
            this.m = list;
            return this;
        }

        @NonNull
        public final a f(@Nullable String str) {
            this.n = str;
            return this;
        }

        @NonNull
        public final i a() {
            this.f3728a = com.smaato.sdk.video.ad.a.a(this.f3728a);
            this.k = com.smaato.sdk.video.ad.a.a(this.k);
            this.l = com.smaato.sdk.video.ad.a.a(this.l);
            this.m = com.smaato.sdk.video.ad.a.a(this.m);
            i iVar = new i(this.f3728a, this.k, this.l, this.m, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.n);
            return iVar;
        }
    }

    i(@NonNull List<String> list, @NonNull List<p> list2, @NonNull List<String> list3, @NonNull List<String> list4, @Nullable String str, @Nullable Float f2, @Nullable Float f3, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable Float f4, @Nullable j jVar, @Nullable String str6) {
        this.e = str;
        this.f = f2;
        this.g = f3;
        this.h = str2;
        this.i = str3;
        this.j = str4;
        this.k = str5;
        this.l = f4;
        this.f3727a = list;
        this.m = jVar;
        this.b = list2;
        this.c = list3;
        this.d = list4;
        this.n = str6;
    }
}
