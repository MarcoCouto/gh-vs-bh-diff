package com.smaato.sdk.video.vast.model;

import android.support.v4.app.NotificationCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.smaato.sdk.core.util.Objects;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public enum v {
    START("start", true),
    FIRST_QUARTILE("firstQuartile", true),
    MID_POINT("midpoint", true),
    THIRD_QUARTILE("thirdQuartile", true),
    COMPLETE("complete", true),
    ACCEPT_INVITATION_LINEAR("acceptInvitationLinear", true),
    TIME_SPENT_VIEWING("timeSpentViewing", true),
    OTHER_AD_INTERACTION("otherAdInteraction", false),
    PROGRESS(NotificationCompat.CATEGORY_PROGRESS, false),
    CREATIVE_VIEW("creativeView", true),
    PAUSE(CampaignEx.JSON_NATIVE_VIDEO_PAUSE, false),
    RESUME(CampaignEx.JSON_NATIVE_VIDEO_RESUME, false),
    REWIND("rewind", false),
    SKIP("skip", false),
    MUTE("mute", false),
    UNMUTE("unmute", false),
    PLAYER_EXPAND("playerExpand", false),
    PLAYER_COLLAPSE("playerCollapse", false);
    
    @NonNull
    public static final Set<v> n = null;
    public final boolean o;
    @NonNull
    private String u;

    static {
        n = Collections.unmodifiableSet(new HashSet<v>() {
            {
                add(v.PROGRESS);
                add(v.TIME_SPENT_VIEWING);
                add(v.START);
                add(v.FIRST_QUARTILE);
                add(v.MID_POINT);
                add(v.THIRD_QUARTILE);
            }
        });
    }

    private v(String str, boolean z) {
        this.u = (String) Objects.requireNonNull(str);
        this.o = z;
    }

    @Nullable
    public static v a(@Nullable String str) {
        v[] values;
        for (v vVar : values()) {
            if (vVar.u.equalsIgnoreCase(str)) {
                return vVar;
            }
        }
        return null;
    }
}
