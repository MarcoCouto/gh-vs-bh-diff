package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class n implements o {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f3737a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;
    @Nullable
    public final Float d;
    @Nullable
    public final Float e;
    @Nullable
    public final String f;
    @Nullable
    public final Integer g;
    @Nullable
    public final Integer h;
    @Nullable
    public final Integer i;
    @Nullable
    public final Boolean j;
    @Nullable
    public final Boolean k;
    @Nullable
    public final String l;
    @Nullable
    public final h m;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3738a;
        @Nullable
        private String b;
        @Nullable
        private String c;
        @Nullable
        private Float d;
        @Nullable
        private Float e;
        @Nullable
        private String f;
        @Nullable
        private Integer g;
        @Nullable
        private Integer h;
        @Nullable
        private Integer i;
        @Nullable
        private Boolean j;
        @Nullable
        private Boolean k;
        @Nullable
        private String l;
        @Nullable
        private h m;

        public a() {
        }

        public a(@NonNull n nVar) {
            this.f3738a = nVar.f3737a;
            this.b = nVar.b;
            this.c = nVar.c;
            this.d = nVar.d;
            this.e = nVar.e;
            this.f = nVar.f;
            this.g = nVar.g;
            this.h = nVar.h;
            this.i = nVar.i;
            this.j = nVar.j;
            this.k = nVar.k;
            this.l = nVar.l;
            this.m = nVar.m;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.f3738a = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Float f2) {
            this.d = f2;
            return this;
        }

        @NonNull
        public final a b(@Nullable Float f2) {
            this.e = f2;
            return this;
        }

        @NonNull
        public final a d(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Integer num) {
            this.g = num;
            return this;
        }

        @NonNull
        public final a b(@Nullable Integer num) {
            this.h = num;
            return this;
        }

        @NonNull
        public final a c(@Nullable Integer num) {
            this.i = num;
            return this;
        }

        @NonNull
        public final a a(@Nullable Boolean bool) {
            this.j = bool;
            return this;
        }

        @NonNull
        public final a b(@Nullable Boolean bool) {
            this.k = bool;
            return this;
        }

        @NonNull
        public final a e(@Nullable String str) {
            this.l = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable h hVar) {
            this.m = hVar;
            return this;
        }

        @NonNull
        public final n a() throws com.smaato.sdk.video.vast.exceptions.a {
            com.smaato.sdk.video.ad.a.a(this.f3738a, "Cannot build MediaFile: uri is missing");
            n nVar = new n(this.f3738a, this.c, this.d, this.e, this.b, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m);
            return nVar;
        }
    }

    n(@NonNull String str, @Nullable String str2, @Nullable Float f2, @Nullable Float f3, @Nullable String str3, @Nullable String str4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool, @Nullable Boolean bool2, @Nullable String str5, @Nullable h hVar) {
        this.c = str2;
        this.d = f2;
        this.e = f3;
        this.b = str3;
        this.f = str4;
        this.f3737a = str;
        this.g = num;
        this.h = num2;
        this.i = num3;
        this.j = bool;
        this.k = bool2;
        this.l = str5;
        this.m = hVar;
    }

    @Nullable
    public final Float a() {
        return this.e;
    }

    @Nullable
    public final Float b() {
        return this.d;
    }
}
