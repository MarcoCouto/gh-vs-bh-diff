package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.List;

public final class x {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ab f3755a;
    @NonNull
    public final n b;
    @NonNull
    public final List<q> c;
    @Nullable
    public final b d;
    @Nullable
    public final af e;
    public final long f;
    public final long g;
    @Nullable
    public final w h;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private n f3756a;
        @Nullable
        private List<q> b;
        @Nullable
        private b c;
        @Nullable
        private af d;
        @Nullable
        private w e;
        @Nullable
        private ab f;
        private long g;
        private long h;

        /* synthetic */ a(x xVar, byte b2) {
            this(xVar);
        }

        public a() {
        }

        private a(@NonNull x xVar) {
            this.f3756a = xVar.b;
            this.b = xVar.c;
            this.f = xVar.f3755a;
            this.g = xVar.f;
            this.h = xVar.g;
            this.c = xVar.d;
            this.d = xVar.e;
            this.e = xVar.h;
        }

        @NonNull
        public final a a(@Nullable List<q> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable n nVar) {
            this.f3756a = nVar;
            return this;
        }

        @NonNull
        public final a a(long j) {
            this.g = j;
            return this;
        }

        @NonNull
        public final a a(@Nullable b bVar) {
            this.c = bVar;
            return this;
        }

        @NonNull
        public final a b(long j) {
            this.h = j;
            return this;
        }

        @NonNull
        public final a a(@Nullable af afVar) {
            this.d = afVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable w wVar) {
            this.e = wVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable ab abVar) {
            this.f = abVar;
            return this;
        }

        @NonNull
        public final x a() {
            Objects.requireNonNull(this.f, "Cannot build VastMediaFileScenario: vastScenarioCreativeData is missing");
            Objects.requireNonNull(this.f3756a, "Cannot build VastMediaFileScenario: mediaFile is missing");
            x xVar = new x(this.f3756a, com.smaato.sdk.video.ad.a.a(this.b), this.f, this.g, this.h, this.c, this.d, this.e, 0);
            return xVar;
        }
    }

    /* synthetic */ x(n nVar, List list, ab abVar, long j, long j2, b bVar, af afVar, w wVar, byte b2) {
        this(nVar, list, abVar, j, j2, bVar, afVar, wVar);
    }

    private x(@NonNull n nVar, @NonNull List<q> list, @NonNull ab abVar, long j, long j2, @Nullable b bVar, @Nullable af afVar, @Nullable w wVar) {
        this.b = (n) Objects.requireNonNull(nVar);
        this.c = (List) Objects.requireNonNull(list);
        this.f3755a = (ab) Objects.requireNonNull(abVar);
        this.f = j;
        this.g = j2;
        this.d = bVar;
        this.e = afVar;
        this.h = wVar;
    }

    public final a a() {
        return new a(this, 0);
    }
}
