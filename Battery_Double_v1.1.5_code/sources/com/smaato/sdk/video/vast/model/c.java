package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class c {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private String f3715a;
    @Nullable
    private String b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3716a;
        @Nullable
        private String b;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3716a = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final c a() {
            return new c(this.f3716a, this.b);
        }
    }

    c(@Nullable String str, @Nullable String str2) {
        this.f3715a = str;
        this.b = str2;
    }
}
