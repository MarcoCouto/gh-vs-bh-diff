package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class ag {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<String> f3709a;
    @NonNull
    public final List<String> b;
    @NonNull
    public final List<String> c;
    @Nullable
    public final String d;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3710a;
        @Nullable
        private List<String> b;
        @Nullable
        private List<String> c;
        @Nullable
        private List<String> d;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3710a = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<String> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<String> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final ag a() {
            this.b = com.smaato.sdk.video.ad.a.a(this.b);
            this.c = com.smaato.sdk.video.ad.a.a(this.c);
            this.d = com.smaato.sdk.video.ad.a.a(this.d);
            return new ag(this.b, this.c, this.d, this.f3710a);
        }
    }

    ag(@NonNull List<String> list, @NonNull List<String> list2, @NonNull List<String> list3, @Nullable String str) {
        this.d = str;
        this.f3709a = list;
        this.b = list2;
        this.c = list3;
    }
}
