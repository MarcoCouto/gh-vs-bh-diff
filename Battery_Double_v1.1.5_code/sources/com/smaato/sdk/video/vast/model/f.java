package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class f {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<e> f3721a;
    @Nullable
    private b b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private List<e> f3722a;
        @Nullable
        private b b;

        @NonNull
        public final a a(@Nullable List<e> list) {
            this.f3722a = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable b bVar) {
            this.b = bVar;
            return this;
        }

        @NonNull
        public final f a() throws com.smaato.sdk.video.vast.exceptions.a {
            List<e> list = this.f3722a;
            if (list != null && !list.isEmpty()) {
                return new f(com.smaato.sdk.video.ad.a.a(this.f3722a), this.b);
            }
            throw new com.smaato.sdk.video.vast.exceptions.a("Cannot build CompanionAds: companions are missing");
        }
    }

    public enum b {
        ALL,
        ANY,
        NONE;

        @Nullable
        public static b a(@Nullable String str) {
            b[] values;
            for (b bVar : values()) {
                if (bVar.name().equalsIgnoreCase(str)) {
                    return bVar;
                }
            }
            return null;
        }
    }

    f(@NonNull List<e> list, @Nullable b bVar) {
        this.f3721a = list;
        this.b = bVar;
    }
}
