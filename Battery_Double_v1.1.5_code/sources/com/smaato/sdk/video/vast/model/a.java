package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class a {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final String f3695a;
    @Nullable
    public final k b;
    @Nullable
    public final ah c;
    @Nullable
    public final Integer d;
    @Nullable
    public final Boolean e;

    /* renamed from: com.smaato.sdk.video.vast.model.a$a reason: collision with other inner class name */
    public static class C0082a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3696a;
        @Nullable
        private k b;
        @Nullable
        private ah c;
        @Nullable
        private Integer d;
        @Nullable
        private Boolean e;

        public C0082a() {
        }

        public C0082a(@NonNull a aVar) {
            this.b = aVar.b;
            this.c = aVar.c;
            this.f3696a = aVar.f3695a;
            this.d = aVar.d;
            this.e = aVar.e;
        }

        @NonNull
        public final C0082a a(@Nullable String str) {
            this.f3696a = str;
            return this;
        }

        @NonNull
        public final C0082a a(@Nullable k kVar) {
            this.b = kVar;
            return this;
        }

        @NonNull
        public final C0082a a(@Nullable ah ahVar) {
            this.c = ahVar;
            return this;
        }

        @NonNull
        public final C0082a a(@Nullable Integer num) {
            this.d = num;
            return this;
        }

        @NonNull
        public final C0082a a(@Nullable Boolean bool) {
            this.e = bool;
            return this;
        }

        @NonNull
        public final a a() {
            a aVar = new a(this.f3696a, this.b, this.c, this.d, this.e);
            return aVar;
        }
    }

    a(@Nullable String str, @Nullable k kVar, @Nullable ah ahVar, @Nullable Integer num, @Nullable Boolean bool) {
        this.b = kVar;
        this.c = ahVar;
        this.f3695a = str;
        this.d = num;
        this.e = bool;
    }
}
