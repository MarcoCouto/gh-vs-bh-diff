package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private String f3713a;
    @Nullable
    private Boolean b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3714a;
        @Nullable
        private Boolean b;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3714a = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Boolean bool) {
            this.b = bool;
            return this;
        }

        @NonNull
        public final b a() throws com.smaato.sdk.video.vast.exceptions.a {
            com.smaato.sdk.video.ad.a.a(this.f3714a, "Cannot build AdParameters: parameters are missing");
            return new b(this.f3714a, this.b);
        }
    }

    b(@NonNull String str, @Nullable Boolean bool) {
        this.f3713a = str;
        this.b = bool;
    }
}
