package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class j {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<s> f3729a;
    @Nullable
    public final String b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private List<s> f3730a;
        @Nullable
        private String b;

        @NonNull
        public final a a(@Nullable List<s> list) {
            this.f3730a = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final j a() {
            return new j(com.smaato.sdk.video.ad.a.a(this.f3730a), this.b);
        }
    }

    j(@NonNull List<s> list, @Nullable String str) {
        this.f3729a = list;
        this.b = str;
    }
}
