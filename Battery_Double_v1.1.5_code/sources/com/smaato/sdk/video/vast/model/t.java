package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public enum t {
    SMAATO_VIDEO_CLICK_TRACKING("smaato_sdk_vastVideoClickTracking", true),
    SMAATO_ICON_CLICK_TRACKING("smaato_sdk_vastIconClickTracking", true),
    SMAATO_COMPANION_CLICK_TRACKING("smaato_sdk_vastCompanionClickTracking", true),
    SMAATO_IMPRESSION("smaato_sdk_vastInlineImpression", true),
    SMAATO_VIEWABLE_IMPRESSION("smaato_sdk_vastInlineViewableImpression", true),
    SMAATO_ICON_VIEW_TRACKING("smaato_sdk_vastIconViewTracking", true);
    
    @NonNull
    private String g;
    private boolean h;

    private t(String str, boolean z) {
        this.g = (String) Objects.requireNonNull(str);
        this.h = true;
    }
}
