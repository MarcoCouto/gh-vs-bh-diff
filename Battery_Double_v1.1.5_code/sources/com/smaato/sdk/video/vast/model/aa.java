package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class aa {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<s> f3697a;
    @NonNull
    public final List<ae> b;
    @NonNull
    public final List<d> c;
    @NonNull
    public final List<String> d;
    @NonNull
    public final x e;
    @Nullable
    public final u f;
    @Nullable
    public final c g;
    @Nullable
    public final String h;
    @Nullable
    public final String i;
    @Nullable
    public final String j;
    @Nullable
    public final ag k;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private x f3698a;
        @Nullable
        private u b;
        @Nullable
        private List<ae> c;
        @Nullable
        private List<s> d;
        @Nullable
        private List<d> e;
        @Nullable
        private List<String> f;
        @Nullable
        private c g;
        @Nullable
        private String h;
        @Nullable
        private String i;
        @Nullable
        private String j;
        @Nullable
        private ag k;

        public a() {
        }

        public a(@NonNull aa aaVar) {
            this.d = aaVar.f3697a;
            this.c = aaVar.b;
            this.e = aaVar.c;
            this.f = aaVar.d;
            this.g = aaVar.g;
            this.h = aaVar.h;
            this.i = aaVar.i;
            this.j = aaVar.j;
            this.k = aaVar.k;
            this.f3698a = aaVar.e;
            this.b = aaVar.f;
        }

        @NonNull
        public final a a(@Nullable c cVar) {
            this.g = cVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.j = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable ag agVar) {
            this.k = agVar;
            return this;
        }

        @NonNull
        public final a a(@NonNull List<ae> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable x xVar) {
            this.f3698a = xVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable u uVar) {
            this.b = uVar;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<s> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<d> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public final a d(@Nullable List<String> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public final aa a() {
            List a2 = com.smaato.sdk.video.ad.a.a(this.d);
            List a3 = com.smaato.sdk.video.ad.a.a(this.c);
            List a4 = com.smaato.sdk.video.ad.a.a(this.e);
            List a5 = com.smaato.sdk.video.ad.a.a(this.f);
            x xVar = this.f3698a;
            xVar.getClass();
            aa aaVar = new aa(a2, a3, a4, a5, xVar, this.b, this.g, this.h, this.i, this.j, this.k, 0);
            return aaVar;
        }
    }

    /* synthetic */ aa(List list, List list2, List list3, List list4, x xVar, u uVar, c cVar, String str, String str2, String str3, ag agVar, byte b2) {
        this(list, list2, list3, list4, xVar, uVar, cVar, str, str2, str3, agVar);
    }

    private aa(@NonNull List<s> list, @NonNull List<ae> list2, @NonNull List<d> list3, @NonNull List<String> list4, @NonNull x xVar, @Nullable u uVar, @Nullable c cVar, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable ag agVar) {
        list.getClass();
        this.f3697a = list;
        list2.getClass();
        this.b = list2;
        list3.getClass();
        this.c = list3;
        list4.getClass();
        this.d = list4;
        xVar.getClass();
        this.e = xVar;
        this.f = uVar;
        this.g = cVar;
        this.h = str;
        this.i = str2;
        this.j = str3;
        this.k = agVar;
    }

    @NonNull
    public final a a() {
        return new a(this);
    }
}
