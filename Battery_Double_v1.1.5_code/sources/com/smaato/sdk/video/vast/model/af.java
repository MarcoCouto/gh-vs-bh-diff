package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class af {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<s> f3707a;
    @NonNull
    public final List<s> b;
    @Nullable
    public final s c;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private s f3708a;
        @Nullable
        private List<s> b;
        @Nullable
        private List<s> c;

        @NonNull
        public final a a(@Nullable s sVar) {
            this.f3708a = sVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<s> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<s> list) {
            this.c = list;
            return this;
        }

        public final af a() {
            this.b = com.smaato.sdk.video.ad.a.a(this.b);
            this.c = com.smaato.sdk.video.ad.a.a(this.c);
            return new af(this.b, this.c, this.f3708a);
        }
    }

    af(@NonNull List<s> list, @NonNull List<s> list2, @Nullable s sVar) {
        this.c = sVar;
        this.f3707a = list;
        this.b = list2;
    }
}
