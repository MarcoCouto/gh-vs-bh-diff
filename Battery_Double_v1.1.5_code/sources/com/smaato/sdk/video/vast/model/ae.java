package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class ae {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private List<l> f3705a;
    @Nullable
    private String b;
    @Nullable
    private ag c;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3706a;
        @Nullable
        private List<l> b;
        @Nullable
        private ag c;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3706a = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<l> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable ag agVar) {
            this.c = agVar;
            return this;
        }

        @NonNull
        public final ae a() {
            this.b = com.smaato.sdk.video.ad.a.a(this.b);
            return new ae(this.b, this.f3706a, this.c);
        }
    }

    ae(@NonNull List<l> list, @Nullable String str, @Nullable ag agVar) {
        this.b = str;
        this.f3705a = list;
        this.c = agVar;
    }
}
