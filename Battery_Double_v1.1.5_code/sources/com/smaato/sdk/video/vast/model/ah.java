package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class ah {

    /* renamed from: a reason: collision with root package name */
    public final boolean f3711a;
    @NonNull
    public final List<s> b;
    @NonNull
    public final List<ae> c;
    @NonNull
    public final List<g> d;
    @NonNull
    public final List<String> e;
    @Nullable
    public final c f;
    @Nullable
    public final ag g;
    @Nullable
    public final Boolean h;
    @Nullable
    public final Boolean i;
    @Nullable
    public final String j;
    @Nullable
    public final ad k;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private c f3712a;
        @Nullable
        private List<s> b;
        @Nullable
        private List<String> c;
        @Nullable
        private ag d;
        @Nullable
        private Boolean e;
        @Nullable
        private Boolean f;
        @Nullable
        private Boolean g;
        @Nullable
        private String h;
        @Nullable
        private List<ae> i;
        @Nullable
        private List<g> j;
        @Nullable
        private ad k;

        public a() {
        }

        public a(@NonNull ah ahVar) {
            this.e = Boolean.valueOf(ahVar.f3711a);
            this.f3712a = ahVar.f;
            this.b = ahVar.b;
            this.c = ahVar.e;
            this.d = ahVar.g;
            this.f = ahVar.h;
            this.g = ahVar.i;
            this.h = ahVar.j;
            this.i = ahVar.c;
            this.j = ahVar.d;
            this.k = ahVar.k;
        }

        @NonNull
        public final a a(@Nullable c cVar) {
            this.f3712a = cVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<s> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<String> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable ag agVar) {
            this.d = agVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable Boolean bool) {
            this.e = bool;
            return this;
        }

        @NonNull
        public final a b(@Nullable Boolean bool) {
            this.f = bool;
            return this;
        }

        @NonNull
        public final a c(@Nullable Boolean bool) {
            this.g = bool;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<ae> list) {
            this.i = list;
            return this;
        }

        @NonNull
        public final a d(@Nullable List<g> list) {
            this.j = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable ad adVar) {
            this.k = adVar;
            return this;
        }

        @NonNull
        public final ah a() {
            this.e = Boolean.valueOf(this.e == null ? true : this.e.booleanValue());
            this.b = com.smaato.sdk.video.ad.a.a(this.b);
            this.i = com.smaato.sdk.video.ad.a.a(this.i);
            this.j = com.smaato.sdk.video.ad.a.a(this.j);
            this.c = com.smaato.sdk.video.ad.a.a(this.c);
            ah ahVar = new ah(this.e.booleanValue(), this.b, this.i, this.j, this.c, this.f3712a, this.d, this.f, this.g, this.h, this.k);
            return ahVar;
        }
    }

    ah(boolean z, @NonNull List<s> list, @NonNull List<ae> list2, @NonNull List<g> list3, @NonNull List<String> list4, @Nullable c cVar, @Nullable ag agVar, @Nullable Boolean bool, @Nullable Boolean bool2, @Nullable String str, @Nullable ad adVar) {
        this.f3711a = z;
        this.f = cVar;
        this.b = list;
        this.e = list4;
        this.g = agVar;
        this.h = bool;
        this.i = bool2;
        this.j = str;
        this.c = list2;
        this.d = list3;
        this.k = adVar;
    }
}
