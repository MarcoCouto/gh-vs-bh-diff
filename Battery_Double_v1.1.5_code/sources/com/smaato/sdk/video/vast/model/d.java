package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class d {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private String f3717a;
    @Nullable
    private String b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3718a;
        @Nullable
        private String b;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3718a = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final d a() {
            return new d(this.f3718a, this.b);
        }
    }

    d(@Nullable String str, @Nullable String str2) {
        this.f3717a = str;
        this.b = str2;
    }
}
