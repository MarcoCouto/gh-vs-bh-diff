package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import java.util.regex.Pattern;

public final class p {
    /* access modifiers changed from: private */
    public static final Pattern c = Pattern.compile("(image/[^\\s;]+)");
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f3739a;
    @NonNull
    public final b b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3740a;
        @Nullable
        private b b;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3740a = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable b bVar) {
            this.b = bVar;
            return this;
        }

        @NonNull
        public final p a() throws com.smaato.sdk.video.vast.exceptions.a {
            b bVar = this.b;
            if (bVar == null) {
                bVar = b.UNKNOWN;
            }
            return new p((String) com.smaato.sdk.video.ad.a.a(this.f3740a, "Cannot build StaticResource: uri is missing"), bVar, 0);
        }
    }

    public enum b {
        JAVASCRIPT,
        IMAGE,
        UNKNOWN;

        @Nullable
        public static b a(@Nullable String str) {
            if (!TextUtils.isEmpty(str)) {
                if (p.c.matcher(str.trim()).find()) {
                    return IMAGE;
                }
                if (WebRequest.CONTENT_TYPE_JAVASCRIPT.equalsIgnoreCase(str)) {
                    return JAVASCRIPT;
                }
            }
            return null;
        }
    }

    /* synthetic */ p(String str, b bVar, byte b2) {
        this(str, bVar);
    }

    private p(@NonNull String str, @NonNull b bVar) {
        this.f3739a = (String) Objects.requireNonNull(str);
        this.b = (b) Objects.requireNonNull(bVar);
    }
}
