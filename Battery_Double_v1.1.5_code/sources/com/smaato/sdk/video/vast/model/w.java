package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class w {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ac f3753a;
    @NonNull
    public final List<String> b;
    @Nullable
    public final Float c;
    @Nullable
    public final Float d;
    @Nullable
    public final j e;
    public final long f;
    public final long g;
    @Nullable
    private String h;
    @Nullable
    private String i;
    @Nullable
    private String j;
    @Nullable
    private Float k;
    @Nullable
    private String l;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private ac f3754a;
        @Nullable
        private List<String> b;
        @Nullable
        private Float c;
        @Nullable
        private Float d;
        @Nullable
        private String e;
        @Nullable
        private String f;
        @Nullable
        private String g;
        @Nullable
        private Float h;
        @Nullable
        private j i;
        @Nullable
        private String j;
        private long k;
        private long l;

        @NonNull
        public final a a(@Nullable ac acVar) {
            this.f3754a = acVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<String> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable Float f2) {
            this.c = f2;
            return this;
        }

        @NonNull
        public final a b(@Nullable Float f2) {
            this.d = f2;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.g = str;
            return this;
        }

        @NonNull
        public final a a(long j2) {
            this.k = j2;
            return this;
        }

        @NonNull
        public final a c(@Nullable Float f2) {
            this.h = f2;
            return this;
        }

        @NonNull
        public final a a(@Nullable j jVar) {
            this.i = jVar;
            return this;
        }

        @NonNull
        public final a d(@Nullable String str) {
            this.j = str;
            return this;
        }

        @NonNull
        public final a b(long j2) {
            this.l = j2;
            return this;
        }

        @NonNull
        public final w a() throws com.smaato.sdk.video.vast.exceptions.a {
            com.smaato.sdk.video.ad.a.a(this.f3754a, "Cannot build VastIconScenario: resourceData is missing");
            w wVar = new w(this.f3754a, com.smaato.sdk.video.ad.a.a(this.b), this.c, this.d, this.e, this.f, this.g, this.k, this.l, this.h, this.i, this.j, 0);
            return wVar;
        }
    }

    /* synthetic */ w(ac acVar, List list, Float f2, Float f3, String str, String str2, String str3, long j2, long j3, Float f4, j jVar, String str4, byte b2) {
        this(acVar, list, f2, f3, str, str2, str3, j2, j3, f4, jVar, str4);
    }

    private w(@NonNull ac acVar, @NonNull List<String> list, @Nullable Float f2, @Nullable Float f3, @Nullable String str, @Nullable String str2, @Nullable String str3, long j2, long j3, @Nullable Float f4, @Nullable j jVar, @Nullable String str4) {
        this.b = list;
        this.f3753a = acVar;
        this.h = str;
        this.c = f2;
        this.d = f3;
        this.i = str2;
        this.j = str3;
        this.f = j2;
        this.g = j3;
        this.k = f4;
        this.e = jVar;
        this.l = str4;
    }
}
