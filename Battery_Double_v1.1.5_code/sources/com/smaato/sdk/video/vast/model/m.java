package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class m {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<n> f3735a;
    @NonNull
    public final List<q> b;
    @NonNull
    public final List<i> c;
    @Nullable
    public final String d;
    @Nullable
    public final String e;
    @Nullable
    public final b f;
    @Nullable
    public final af g;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3736a;
        @Nullable
        private String b;
        @Nullable
        private b c;
        @Nullable
        private List<n> d;
        @Nullable
        private af e;
        @Nullable
        private List<q> f;
        @Nullable
        private List<i> g;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3736a = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable b bVar) {
            this.c = bVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<n> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable af afVar) {
            this.e = afVar;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<q> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<i> list) {
            this.g = list;
            return this;
        }

        @NonNull
        public final m a() {
            m mVar = new m(com.smaato.sdk.video.ad.a.a(this.d), com.smaato.sdk.video.ad.a.a(this.f), com.smaato.sdk.video.ad.a.a(this.g), this.c, this.b, this.f3736a, this.e);
            return mVar;
        }
    }

    m(@NonNull List<n> list, @NonNull List<q> list2, @NonNull List<i> list3, @Nullable b bVar, @Nullable String str, @Nullable String str2, @Nullable af afVar) {
        this.f = bVar;
        this.e = str;
        this.d = str2;
        this.f3735a = list;
        this.g = afVar;
        this.b = list2;
        this.c = list3;
    }
}
