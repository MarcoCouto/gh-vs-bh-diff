package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class ad {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final String f3703a;
    @NonNull
    public final List<String> b;
    @NonNull
    public final List<a> c;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3704a;
        @Nullable
        private List<String> b;
        @Nullable
        private List<a> c;

        public a() {
        }

        public a(@NonNull ad adVar) {
            this.f3704a = adVar.f3703a;
            this.b = adVar.b;
            this.c = adVar.c;
        }

        @NonNull
        public final a a(@Nullable List<a> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<String> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.f3704a = str;
            return this;
        }

        @NonNull
        public final ad a() {
            return new ad(com.smaato.sdk.video.ad.a.a(this.c), com.smaato.sdk.video.ad.a.a(this.b), this.f3704a);
        }
    }

    ad(@NonNull List<a> list, @NonNull List<String> list2, @Nullable String str) {
        list.getClass();
        this.c = list;
        list2.getClass();
        this.b = list2;
        this.f3703a = str;
    }
}
