package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class k {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<s> f3731a;
    @NonNull
    public final List<g> b;
    @NonNull
    public final List<ae> c;
    @NonNull
    public final List<d> d;
    @NonNull
    public final List<String> e;
    @Nullable
    public final c f;
    @Nullable
    public final String g;
    @Nullable
    public final String h;
    @Nullable
    public final String i;
    @Nullable
    public final ag j;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private c f3732a;
        @Nullable
        private String b;
        @Nullable
        private List<s> c;
        @Nullable
        private List<d> d;
        @Nullable
        private String e;
        @Nullable
        private String f;
        @Nullable
        private List<String> g;
        @Nullable
        private ag h;
        @Nullable
        private List<g> i;
        @Nullable
        private List<ae> j;

        @NonNull
        public final a a(@Nullable c cVar) {
            this.f3732a = cVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<s> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<d> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<String> list) {
            this.g = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable ag agVar) {
            this.h = agVar;
            return this;
        }

        @NonNull
        public final a d(@Nullable List<g> list) {
            this.i = list;
            return this;
        }

        @NonNull
        public final a e(@Nullable List<ae> list) {
            this.j = list;
            return this;
        }

        @NonNull
        public final k a() {
            this.c = com.smaato.sdk.video.ad.a.a(this.c);
            this.i = com.smaato.sdk.video.ad.a.a(this.i);
            this.j = com.smaato.sdk.video.ad.a.a(this.j);
            this.d = com.smaato.sdk.video.ad.a.a(this.d);
            this.g = com.smaato.sdk.video.ad.a.a(this.g);
            k kVar = new k(this.c, this.i, this.j, this.d, this.g, this.f3732a, this.b, this.e, this.f, this.h);
            return kVar;
        }
    }

    public k(@NonNull List<s> list, @NonNull List<g> list2, @NonNull List<ae> list3, @NonNull List<d> list4, @NonNull List<String> list5, @Nullable c cVar, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable ag agVar) {
        this.f = cVar;
        this.g = str;
        this.f3731a = list;
        this.d = list4;
        this.h = str2;
        this.i = str3;
        this.e = list5;
        this.j = agVar;
        this.b = list2;
        this.c = list3;
    }
}
