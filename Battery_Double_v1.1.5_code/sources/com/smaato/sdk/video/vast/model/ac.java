package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class ac {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final p f3701a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private p f3702a;
        @Nullable
        private String b;
        @Nullable
        private String c;

        @NonNull
        public final a a(@Nullable p pVar) {
            this.f3702a = pVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final ac a() throws com.smaato.sdk.video.vast.exceptions.a {
            if (this.f3702a != null || this.b != null || this.c != null) {
                return new ac(this.f3702a, this.b, this.c, 0);
            }
            throw new com.smaato.sdk.video.vast.exceptions.a("Cannot build VastScenarioResourceData: staticResources, iFrameResources and htmlResources are missing");
        }
    }

    /* synthetic */ ac(p pVar, String str, String str2, byte b2) {
        this(pVar, str, str2);
    }

    private ac(@Nullable p pVar, @Nullable String str, @Nullable String str2) {
        this.f3701a = pVar;
        this.b = str;
        this.c = str2;
    }
}
