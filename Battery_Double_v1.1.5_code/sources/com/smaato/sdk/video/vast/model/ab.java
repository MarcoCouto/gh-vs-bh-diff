package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class ab {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final r f3699a;
    @Nullable
    private String b;
    @Nullable
    private String c;
    @Nullable
    private Integer d;
    @Nullable
    private String e;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private r f3700a;
        @Nullable
        private String b;
        @Nullable
        private String c;
        @Nullable
        private Integer d;
        @Nullable
        private String e;

        @NonNull
        public final a a(@Nullable r rVar) {
            this.f3700a = rVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Integer num) {
            this.d = num;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public final ab a() {
            if (this.f3700a == null) {
                this.f3700a = r.f3745a;
            }
            ab abVar = new ab(this.f3700a, this.b, this.c, this.d, this.e, 0);
            return abVar;
        }
    }

    /* synthetic */ ab(r rVar, String str, String str2, Integer num, String str3, byte b2) {
        this(rVar, str, str2, num, str3);
    }

    private ab(@NonNull r rVar, @Nullable String str, @Nullable String str2, @Nullable Integer num, @Nullable String str3) {
        this.f3699a = rVar;
        this.b = str;
        this.c = str2;
        this.d = num;
        this.e = str3;
    }
}
