package com.smaato.sdk.video.vast.model;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class r {

    /* renamed from: a reason: collision with root package name */
    public static final r f3745a = new r("unknown", "unknown", "unknown");
    @NonNull
    private String b;
    @NonNull
    private String c;
    @NonNull
    private String d;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3746a;
        @Nullable
        private String b;
        @Nullable
        private String c;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3746a = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final r a() {
            if (TextUtils.isEmpty(this.c) && TextUtils.isEmpty(this.b) && TextUtils.isEmpty(this.f3746a)) {
                return r.f3745a;
            }
            if (TextUtils.isEmpty(this.f3746a)) {
                this.f3746a = "unknown";
            }
            if (TextUtils.isEmpty(this.b)) {
                this.b = "unknown";
            }
            if (TextUtils.isEmpty(this.c)) {
                this.c = "unknown";
            }
            return new r(this.f3746a, this.b, this.c);
        }
    }

    public r(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        r rVar = (r) obj;
        if (this.b.equals(rVar.b) && this.c.equals(rVar.c)) {
            return this.d.equals(rVar.d);
        }
        return false;
    }

    public final int hashCode() {
        return (((this.b.hashCode() * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }
}
