package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.List;

public final class y {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ab f3757a;
    @NonNull
    public final List<q> b;
    @Nullable
    public final af c;
    @Nullable
    public final w d;
    @Nullable
    private n e;
    @Nullable
    private b f;
    private long g;
    private long h;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private List<q> f3758a;
        @Nullable
        private b b;
        @Nullable
        private af c;
        @Nullable
        private w d;
        @Nullable
        private ab e;
        private long f;
        private long g;

        @NonNull
        public final a a(@Nullable List<q> list) {
            this.f3758a = list;
            return this;
        }

        @NonNull
        public final a a(long j) {
            this.f = j;
            return this;
        }

        @NonNull
        public final a a(@Nullable b bVar) {
            this.b = bVar;
            return this;
        }

        @NonNull
        public final a b(long j) {
            this.g = j;
            return this;
        }

        @NonNull
        public final a a(@Nullable af afVar) {
            this.c = afVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable w wVar) {
            this.d = wVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable ab abVar) {
            this.e = abVar;
            return this;
        }

        @NonNull
        public final y a() {
            Objects.requireNonNull(this.e, "Cannot build VastMediaFileScenario: vastScenarioCreativeData is missing");
            y yVar = new y(com.smaato.sdk.video.ad.a.a(this.f3758a), this.e, null, this.f, this.g, this.b, this.c, this.d, 0);
            return yVar;
        }
    }

    /* synthetic */ y(List list, ab abVar, n nVar, long j, long j2, b bVar, af afVar, w wVar, byte b2) {
        this(list, abVar, nVar, j, j2, bVar, afVar, wVar);
    }

    private y(@NonNull List<q> list, @NonNull ab abVar, @Nullable n nVar, long j, long j2, @Nullable b bVar, @Nullable af afVar, @Nullable w wVar) {
        this.e = nVar;
        this.f3757a = abVar;
        this.g = j;
        this.h = j2;
        this.b = list;
        this.f = bVar;
        this.c = afVar;
        this.d = wVar;
    }
}
