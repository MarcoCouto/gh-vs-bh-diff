package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.TextUtils;

public final class q {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f3742a;
    @NonNull
    public final v b;
    @Nullable
    public final String c;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3744a;
        @Nullable
        private v b;
        @Nullable
        private String c;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3744a = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable v vVar) {
            this.b = vVar;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final q a() throws com.smaato.sdk.video.vast.exceptions.a {
            String str;
            com.smaato.sdk.video.ad.a.a(this.b, "Cannot build Tracking: event is missing");
            com.smaato.sdk.video.ad.a.a(this.f3744a, "Cannot build Tracking: url is missing");
            if (TextUtils.isEmpty(this.c)) {
                switch (this.b) {
                    case THIRD_QUARTILE:
                        str = "75%";
                        break;
                    case MID_POINT:
                        str = "50%";
                        break;
                    case FIRST_QUARTILE:
                        str = "25%";
                        break;
                    case START:
                        str = "0%";
                        break;
                    default:
                        str = null;
                        break;
                }
            } else {
                str = this.c;
            }
            this.c = str;
            return new q(this.b, this.f3744a, this.c);
        }
    }

    q(@NonNull v vVar, @NonNull String str, @Nullable String str2) {
        this.b = vVar;
        this.f3742a = str;
        this.c = str2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        q qVar = (q) obj;
        if (!this.f3742a.equals(qVar.f3742a) || this.b != qVar.b) {
            return false;
        }
        if (this.c != null) {
            return this.c.equals(qVar.c);
        }
        return qVar.c == null;
    }

    public final int hashCode() {
        return (((this.f3742a.hashCode() * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0);
    }
}
