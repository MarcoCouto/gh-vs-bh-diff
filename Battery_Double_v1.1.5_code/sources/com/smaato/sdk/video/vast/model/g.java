package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class g {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final r f3724a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;
    @Nullable
    public final Integer d;
    @Nullable
    public final String e;
    @Nullable
    public final m f;
    @Nullable
    public final f g;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3725a;
        @Nullable
        private String b;
        @Nullable
        private Integer c;
        @Nullable
        private String d;
        @Nullable
        private r e;
        @Nullable
        private m f;
        @Nullable
        private f g;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3725a = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Integer num) {
            this.c = num;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.d = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable m mVar) {
            this.f = mVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable f fVar) {
            this.g = fVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable r rVar) {
            this.e = rVar;
            return this;
        }

        @NonNull
        public final g a() {
            if (this.e == null) {
                this.e = r.f3745a;
            }
            g gVar = new g(this.e, this.f3725a, this.b, this.c, this.d, this.f, this.g);
            return gVar;
        }
    }

    g(@NonNull r rVar, @Nullable String str, @Nullable String str2, @Nullable Integer num, @Nullable String str3, @Nullable m mVar, @Nullable f fVar) {
        this.b = str;
        this.c = str2;
        this.d = num;
        this.e = str3;
        this.f3724a = rVar;
        this.f = mVar;
        this.g = fVar;
    }
}
