package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class s {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f3747a;
    @Nullable
    private String b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3748a;
        @Nullable
        private String b;

        @NonNull
        public final a a(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.f3748a = str;
            return this;
        }

        @NonNull
        public final s a() throws com.smaato.sdk.video.vast.exceptions.a {
            com.smaato.sdk.video.ad.a.a(this.f3748a, "Cannot build VastBeacon: uri is missing");
            return new s(this.f3748a, this.b);
        }
    }

    s(@NonNull String str, @Nullable String str2) {
        this.f3747a = str;
        this.b = str2;
    }
}
