package com.smaato.sdk.video.vast.model;

import androidx.annotation.Nullable;

public enum h {
    PROGRESSIVE,
    STREAMING;

    @Nullable
    public static h a(@Nullable String str) {
        h[] values;
        for (h hVar : values()) {
            if (hVar.name().equalsIgnoreCase(str)) {
                return hVar;
            }
        }
        return null;
    }
}
