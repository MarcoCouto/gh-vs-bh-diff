package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class z {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<s> f3759a;
    @NonNull
    public final List<ae> b;
    @NonNull
    public final List<d> c;
    @NonNull
    public final List<String> d;
    @NonNull
    public final List<y> e;
    @NonNull
    public final List<u> f;
    @Nullable
    public final ag g;
    @Nullable
    private c h;
    @Nullable
    private String i;
    @Nullable
    private String j;
    @Nullable
    private String k;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private List<y> f3760a;
        @Nullable
        private List<u> b;
        @Nullable
        private List<ae> c;
        @Nullable
        private List<s> d;
        @Nullable
        private List<String> e;
        @Nullable
        private c f;
        @Nullable
        private ag g;

        @NonNull
        public final a a(@Nullable c cVar) {
            this.f = cVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable ag agVar) {
            this.g = agVar;
            return this;
        }

        @NonNull
        public final a a(@NonNull List<ae> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<y> list) {
            this.f3760a = list;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<u> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a d(@Nullable List<s> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a e(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public final z a() {
            z zVar = new z(com.smaato.sdk.video.ad.a.a(this.d), com.smaato.sdk.video.ad.a.a(this.c), com.smaato.sdk.video.ad.a.a(null), com.smaato.sdk.video.ad.a.a(this.e), com.smaato.sdk.video.ad.a.a(this.f3760a), com.smaato.sdk.video.ad.a.a(this.b), this.f, null, null, null, this.g, 0);
            return zVar;
        }
    }

    /* synthetic */ z(List list, List list2, List list3, List list4, List list5, List list6, c cVar, String str, String str2, String str3, ag agVar, byte b2) {
        this(list, list2, list3, list4, list5, list6, cVar, str, str2, str3, agVar);
    }

    private z(@NonNull List<s> list, @NonNull List<ae> list2, @NonNull List<d> list3, @NonNull List<String> list4, @NonNull List<y> list5, @NonNull List<u> list6, @Nullable c cVar, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable ag agVar) {
        list.getClass();
        this.f3759a = list;
        list2.getClass();
        this.b = list2;
        list3.getClass();
        this.c = list3;
        list4.getClass();
        this.d = list4;
        list5.getClass();
        this.e = list5;
        list6.getClass();
        this.f = list6;
        this.h = cVar;
        this.i = str;
        this.j = str2;
        this.k = str3;
        this.g = agVar;
    }
}
