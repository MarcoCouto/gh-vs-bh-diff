package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class e implements o {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<s> f3719a;
    @NonNull
    public final List<q> b;
    @NonNull
    public final List<p> c;
    @NonNull
    public final List<String> d;
    @NonNull
    public final List<String> e;
    @Nullable
    public final String f;
    @Nullable
    public final Float g;
    @Nullable
    public final Float h;
    @Nullable
    public final Float i;
    @Nullable
    public final Float j;
    @Nullable
    public final Float k;
    @Nullable
    public final Float l;
    @Nullable
    public final Float m;
    @Nullable
    public final String n;
    @Nullable
    public final String o;
    @Nullable
    public final String p;
    @Nullable
    public final b q;
    @Nullable
    public final String r;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3720a;
        @Nullable
        private Float b;
        @Nullable
        private Float c;
        @Nullable
        private Float d;
        @Nullable
        private Float e;
        @Nullable
        private Float f;
        @Nullable
        private Float g;
        @Nullable
        private String h;
        @Nullable
        private String i;
        @Nullable
        private Float j;
        @Nullable
        private String k;
        @Nullable
        private b l;
        @Nullable
        private List<p> m;
        @Nullable
        private List<String> n;
        @Nullable
        private List<String> o;
        @Nullable
        private String p;
        @Nullable
        private List<s> q;
        @Nullable
        private List<q> r;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3720a = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Float f2) {
            this.b = f2;
            return this;
        }

        @NonNull
        public final a b(@Nullable Float f2) {
            this.c = f2;
            return this;
        }

        @NonNull
        public final a c(@Nullable Float f2) {
            this.d = f2;
            return this;
        }

        @NonNull
        public final a d(@Nullable Float f2) {
            this.e = f2;
            return this;
        }

        @NonNull
        public final a e(@Nullable Float f2) {
            this.f = f2;
            return this;
        }

        @NonNull
        public final a f(@Nullable Float f2) {
            this.g = f2;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public final a g(@Nullable Float f2) {
            this.j = f2;
            return this;
        }

        @NonNull
        public final a d(@Nullable String str) {
            this.k = str;
            return this;
        }

        @NonNull
        public final a e(@Nullable String str) {
            this.p = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable b bVar) {
            this.l = bVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<p> list) {
            this.m = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<String> list) {
            this.n = list;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<String> list) {
            this.o = list;
            return this;
        }

        @NonNull
        public final a d(@Nullable List<s> list) {
            this.q = list;
            return this;
        }

        @NonNull
        public final a e(@Nullable List<q> list) {
            this.r = list;
            return this;
        }

        @NonNull
        public final e a() {
            this.q = com.smaato.sdk.video.ad.a.a(this.q);
            this.r = com.smaato.sdk.video.ad.a.a(this.r);
            this.m = com.smaato.sdk.video.ad.a.a(this.m);
            this.n = com.smaato.sdk.video.ad.a.a(this.n);
            this.o = com.smaato.sdk.video.ad.a.a(this.o);
            e eVar = r2;
            e eVar2 = new e(this.q, this.r, this.m, this.n, this.o, this.f3720a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.p, this.l);
            return eVar;
        }
    }

    e(@NonNull List<s> list, @NonNull List<q> list2, @NonNull List<p> list3, @NonNull List<String> list4, @NonNull List<String> list5, @Nullable String str, @Nullable Float f2, @Nullable Float f3, @Nullable Float f4, @Nullable Float f5, @Nullable Float f6, @Nullable Float f7, @Nullable String str2, @Nullable String str3, @Nullable Float f8, @Nullable String str4, @Nullable String str5, @Nullable b bVar) {
        this.f = str;
        this.g = f2;
        this.h = f3;
        this.i = f4;
        this.j = f5;
        this.k = f6;
        this.l = f7;
        this.n = str2;
        this.o = str3;
        this.m = f8;
        this.p = str4;
        this.r = str5;
        this.q = bVar;
        this.c = list3;
        this.d = list4;
        this.e = list5;
        this.f3719a = list;
        this.b = list2;
    }

    @Nullable
    public final Float a() {
        return this.h;
    }

    @Nullable
    public final Float b() {
        return this.g;
    }
}
