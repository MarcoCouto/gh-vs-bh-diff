package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class l {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private String f3733a;
    @Nullable
    private String b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3734a;
        @Nullable
        private String b;

        @NonNull
        public final a a(@NonNull String str) {
            this.f3734a = str;
            return this;
        }

        @NonNull
        public final a b(@NonNull String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final l a() throws com.smaato.sdk.video.vast.exceptions.a {
            com.smaato.sdk.video.ad.a.a(this.f3734a, "Cannot build JavaScriptResource: uri is missing");
            return new l(this.f3734a, this.b);
        }
    }

    public l(@NonNull String str, @Nullable String str2) {
        this.f3733a = str;
        this.b = str2;
    }
}
