package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.List;

public final class u implements o {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ac f3750a;
    @NonNull
    public final ab b;
    @NonNull
    public final List<s> c;
    @NonNull
    public final List<q> d;
    @Nullable
    public final String e;
    @Nullable
    public final Float f;
    @Nullable
    public final Float g;
    @Nullable
    public final Float h;
    @Nullable
    public final Float i;
    @Nullable
    public final Float j;
    @Nullable
    public final Float k;
    @Nullable
    public final Float l;
    @Nullable
    public final String m;
    @Nullable
    public final String n;
    @Nullable
    public final String o;
    @Nullable
    public final b p;
    @Nullable
    public final String q;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private ac f3751a;
        @Nullable
        private List<s> b;
        @Nullable
        private String c;
        @Nullable
        private List<q> d;
        @Nullable
        private String e;
        @Nullable
        private Float f;
        @Nullable
        private Float g;
        @Nullable
        private Float h;
        @Nullable
        private Float i;
        @Nullable
        private Float j;
        @Nullable
        private Float k;
        @Nullable
        private Float l;
        @Nullable
        private String m;
        @Nullable
        private String n;
        @Nullable
        private String o;
        @Nullable
        private b p;
        @Nullable
        private ab q;

        public a() {
        }

        public a(@NonNull u uVar) {
            this.f3751a = uVar.f3750a;
            this.b = uVar.c;
            this.c = uVar.q;
            this.d = uVar.d;
            this.e = uVar.e;
            this.f = uVar.f;
            this.g = uVar.g;
            this.h = uVar.h;
            this.i = uVar.i;
            this.j = uVar.j;
            this.k = uVar.k;
            this.l = uVar.l;
            this.m = uVar.m;
            this.n = uVar.n;
            this.o = uVar.o;
            this.p = uVar.p;
            this.q = uVar.b;
        }

        @NonNull
        public final a a(@Nullable ac acVar) {
            this.f3751a = acVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable Float f2) {
            this.f = f2;
            return this;
        }

        @NonNull
        public final a b(@Nullable Float f2) {
            this.g = f2;
            return this;
        }

        @NonNull
        public final a a(@Nullable List<q> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<s> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final a c(@Nullable Float f2) {
            this.h = f2;
            return this;
        }

        @NonNull
        public final a d(@Nullable Float f2) {
            this.i = f2;
            return this;
        }

        @NonNull
        public final a e(@Nullable Float f2) {
            this.j = f2;
            return this;
        }

        @NonNull
        public final a f(@Nullable Float f2) {
            this.k = f2;
            return this;
        }

        @NonNull
        public final a g(@Nullable Float f2) {
            this.l = f2;
            return this;
        }

        @NonNull
        public final a c(@Nullable String str) {
            this.n = str;
            return this;
        }

        @NonNull
        public final a d(@Nullable String str) {
            this.o = str;
            return this;
        }

        @NonNull
        public final a e(@Nullable String str) {
            this.m = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable b bVar) {
            this.p = bVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable ab abVar) {
            this.q = abVar;
            return this;
        }

        @NonNull
        public final u a() {
            Objects.requireNonNull(this.f3751a, "Cannot build VastCompanionScenario: resourceData is missing");
            Objects.requireNonNull(this.q, "Cannot build VastMediaFileScenario: vastScenarioCreativeData is missing");
            u uVar = new u(this.f3751a, this.q, com.smaato.sdk.video.ad.a.a(this.b), com.smaato.sdk.video.ad.a.a(this.d), this.c, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, 0);
            return uVar;
        }
    }

    /* synthetic */ u(ac acVar, ab abVar, List list, List list2, String str, String str2, Float f2, Float f3, Float f4, Float f5, Float f6, Float f7, Float f8, String str3, String str4, String str5, b bVar, byte b2) {
        this(acVar, abVar, list, list2, str, str2, f2, f3, f4, f5, f6, f7, f8, str3, str4, str5, bVar);
    }

    private u(@NonNull ac acVar, @NonNull ab abVar, @NonNull List<s> list, @NonNull List<q> list2, @Nullable String str, @Nullable String str2, @Nullable Float f2, @Nullable Float f3, @Nullable Float f4, @Nullable Float f5, @Nullable Float f6, @Nullable Float f7, @Nullable Float f8, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable b bVar) {
        this.f3750a = acVar;
        this.b = abVar;
        List<s> list3 = list;
        this.c = new ArrayList(list);
        this.q = str;
        this.d = list2;
        this.e = str2;
        this.f = f2;
        this.g = f3;
        this.h = f4;
        this.i = f5;
        this.j = f6;
        this.k = f7;
        this.l = f8;
        this.m = str3;
        this.n = str4;
        this.o = str5;
        this.p = bVar;
    }

    @Nullable
    public final Float a() {
        return this.g;
    }

    @Nullable
    public final Float b() {
        return this.f;
    }
}
