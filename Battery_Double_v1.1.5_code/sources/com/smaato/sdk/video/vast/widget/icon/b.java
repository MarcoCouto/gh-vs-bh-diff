package com.smaato.sdk.video.vast.widget.icon;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.w;
import com.smaato.sdk.video.vast.utils.a;
import com.smaato.sdk.video.vast.widget.element.VastElementView;
import com.smaato.sdk.video.vast.widget.element.d;
import com.smaato.sdk.video.vast.widget.element.e;
import com.smaato.sdk.video.vast.widget.element.f;

final class b implements f {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3841a;
    @NonNull
    private final w b;
    @NonNull
    private final a c;

    b(@NonNull Logger logger, @NonNull w wVar, @NonNull a aVar) {
        this.f3841a = (Logger) Objects.requireNonNull(logger);
        this.b = (w) Objects.requireNonNull(wVar);
        this.c = (a) Objects.requireNonNull(aVar);
    }

    public final void a(@NonNull VastElementView vastElementView, @NonNull Consumer<d> consumer) {
        float f = Resources.getSystem().getDisplayMetrics().density;
        float max = Math.max(Math.min(UIUtils.getNormalizedSize(this.b.c), 50.0f), 12.0f);
        float max2 = Math.max(Math.min(UIUtils.getNormalizedSize(this.b.d), 50.0f), 12.0f);
        int dpToPx = UIUtils.dpToPx(max, f);
        int dpToPx2 = UIUtils.dpToPx(max2, f);
        String a2 = a.a(this.b.f3753a, dpToPx, dpToPx2);
        if (TextUtils.isEmpty(a2)) {
            consumer.accept(new e(String.format("Error while preparing Icon. Unable to convert Icon resource: %s", new Object[]{a2})));
            return;
        }
        vastElementView.a(a2);
        vastElementView.a(dpToPx, dpToPx2);
    }
}
