package com.smaato.sdk.video.vast.widget;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout.LayoutParams;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.diinjection.Inject;
import com.smaato.sdk.video.R;
import com.smaato.sdk.video.vast.player.x;
import com.smaato.sdk.video.vast.widget.element.VastElementView;

public final class e extends AdContentView {
    @Inject
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private f f3836a;
    @NonNull
    private x b;
    @NonNull
    private VastElementView c = ((VastElementView) findViewById(R.id.smaato_sdk_video_icon_view_id));
    @NonNull
    private VastElementView d = ((VastElementView) findViewById(R.id.smaato_sdk_video_companion_view_id));

    public e(@NonNull Context context) {
        super(context);
        AndroidsInjector.inject((View) this);
        inflate(context, R.layout.smaato_sdk_video_vast_video_player_view, this);
        this.b = this.f3836a.a(context);
        this.b.setId(R.id.smaato_sdk_video_video_player_view_id);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.smaato_sdk_video_player_layout);
        frameLayout.removeAllViews();
        frameLayout.addView(this.b, new LayoutParams(-1, -1));
    }

    @NonNull
    public final VastElementView a() {
        return this.c;
    }

    @NonNull
    public final VastElementView b() {
        return this.d;
    }

    @NonNull
    public final x c() {
        return this.b;
    }

    public final void d() {
        Threads.runOnUi(new Runnable() {
            public final void run() {
                e.this.g();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void g() {
        this.d.setVisibility(0);
        this.d.requestFocus();
    }

    public final void e() {
        Threads.runOnUi(new Runnable() {
            public final void run() {
                e.this.f();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void f() {
        Objects.onNotNull(this.b, $$Lambda$e$IJr0pVr0JibwoNYYz4m3VQ2fIpc.INSTANCE);
    }
}
