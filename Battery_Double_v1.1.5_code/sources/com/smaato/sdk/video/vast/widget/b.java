package com.smaato.sdk.video.vast.widget;

import android.content.Context;
import android.view.SurfaceView;
import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.player.x;

public final class b extends x {
    public b(@NonNull Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final d a(@NonNull Context context) {
        return new a(new SurfaceView(context));
    }
}
