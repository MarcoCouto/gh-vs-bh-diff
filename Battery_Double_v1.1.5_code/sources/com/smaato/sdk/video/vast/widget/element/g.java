package com.smaato.sdk.video.vast.widget.element;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface g {

    public interface a {
        void a(int i);

        void a(@Nullable String str);

        void d();

        void e();
    }

    void a();

    void a(@NonNull VastElementView vastElementView);

    void a(@NonNull d dVar);

    void a(@Nullable a aVar);

    void a(@Nullable String str);

    void b();

    boolean b(@NonNull String str);

    void c();

    void d();

    void e();
}
