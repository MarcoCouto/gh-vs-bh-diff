package com.smaato.sdk.video.vast.widget;

import android.graphics.Rect;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.widget.d.a;
import com.smaato.sdk.video.vast.widget.d.b;
import com.smaato.sdk.video.vast.widget.d.c;

public final class a implements d {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final SurfaceView f3831a;
    /* access modifiers changed from: private */
    @Nullable
    public com.smaato.sdk.video.vast.widget.d.a b;
    /* access modifiers changed from: private */
    @Nullable
    public b c;
    /* access modifiers changed from: private */
    @Nullable
    public c d;

    a(@NonNull SurfaceView surfaceView) {
        this.f3831a = surfaceView;
        surfaceView.getHolder().addCallback(new Callback() {
            public final void surfaceCreated(SurfaceHolder surfaceHolder) {
                Objects.onNotNull(a.this.b, new Consumer(surfaceHolder) {
                    private final /* synthetic */ SurfaceHolder f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        AnonymousClass1.a(this.f$0, (a) obj);
                    }
                });
            }

            /* access modifiers changed from: private */
            public static /* synthetic */ void a(SurfaceHolder surfaceHolder, com.smaato.sdk.video.vast.widget.d.a aVar) {
                Surface surface = surfaceHolder.getSurface();
                if (surface != null) {
                    Rect surfaceFrame = surfaceHolder.getSurfaceFrame();
                    aVar.onSurfaceAvailable(surface, surfaceFrame.width(), surfaceFrame.height());
                }
            }

            public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
                Objects.onNotNull(a.this.c, new Consumer(surfaceHolder, i2, i3) {
                    private final /* synthetic */ SurfaceHolder f$0;
                    private final /* synthetic */ int f$1;
                    private final /* synthetic */ int f$2;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    public final void accept(Object obj) {
                        AnonymousClass1.a(this.f$0, this.f$1, this.f$2, (b) obj);
                    }
                });
            }

            /* access modifiers changed from: private */
            public static /* synthetic */ void a(SurfaceHolder surfaceHolder, int i, int i2, b bVar) {
                Surface surface = surfaceHolder.getSurface();
                if (surface != null) {
                    bVar.onSurfaceChanged(surface, i, i2);
                }
            }

            public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                Objects.onNotNull(a.this.d, new Consumer(surfaceHolder) {
                    private final /* synthetic */ SurfaceHolder f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        AnonymousClass1.a(this.f$0, (c) obj);
                    }
                });
            }

            /* access modifiers changed from: private */
            public static /* synthetic */ void a(SurfaceHolder surfaceHolder, c cVar) {
                Surface surface = surfaceHolder.getSurface();
                if (surface != null) {
                    cVar.onSurfaceDestroyed(surface);
                }
            }
        });
    }

    @NonNull
    public final View a() {
        return this.f3831a;
    }

    public final void a(@Nullable com.smaato.sdk.video.vast.widget.d.a aVar) {
        this.b = aVar;
    }

    public final void a(@Nullable b bVar) {
        this.c = bVar;
    }

    public final void a(@Nullable c cVar) {
        this.d = cVar;
    }
}
