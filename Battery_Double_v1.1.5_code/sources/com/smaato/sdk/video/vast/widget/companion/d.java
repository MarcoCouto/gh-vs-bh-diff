package com.smaato.sdk.video.vast.widget.companion;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.video.vast.browser.a;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.widget.element.c;
import com.smaato.sdk.video.vast.widget.element.f;
import com.smaato.sdk.video.vast.widget.element.h;

public final class d extends h {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final u f3835a;

    d(@NonNull Logger logger, @NonNull f fVar, @NonNull a aVar, @NonNull SomaApiContext somaApiContext, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull c cVar, @NonNull u uVar) {
        super(logger, fVar, aVar, somaApiContext, visibilityTrackerCreator, cVar);
        this.f3835a = (u) Objects.requireNonNull(uVar);
    }

    public final void a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            str = this.f3835a.q;
        }
        super.a(str);
    }
}
