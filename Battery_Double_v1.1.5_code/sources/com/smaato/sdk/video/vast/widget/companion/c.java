package com.smaato.sdk.video.vast.widget.companion;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.utils.a;
import com.smaato.sdk.video.vast.widget.element.g;

public final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f3834a;
    @NonNull
    private final VisibilityTrackerCreator b;
    @NonNull
    private final com.smaato.sdk.video.vast.browser.a c;
    @NonNull
    private final a d;

    public c(@NonNull a aVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull com.smaato.sdk.video.vast.browser.a aVar2, @NonNull a aVar3) {
        this.f3834a = (a) Objects.requireNonNull(aVar);
        this.b = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.c = (com.smaato.sdk.video.vast.browser.a) Objects.requireNonNull(aVar2);
        this.d = (a) Objects.requireNonNull(aVar3);
    }

    @NonNull
    public final g a(@NonNull Logger logger, @NonNull aa aaVar, @NonNull SomaApiContext somaApiContext) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(somaApiContext);
        u uVar = aaVar.f;
        if (uVar == null) {
            return new com.smaato.sdk.video.vast.widget.element.a();
        }
        d dVar = new d(logger, new b(logger, uVar, this.f3834a), this.c, somaApiContext, this.b, this.d, uVar);
        return dVar;
    }
}
