package com.smaato.sdk.video.vast.widget.element;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.webview.BaseWebView;
import com.smaato.sdk.core.webview.BaseWebViewClient;
import com.smaato.sdk.core.webview.BaseWebViewClient.WebViewClientCallback;

@SuppressLint({"SetJavaScriptEnabled"})
public class VastElementView extends BaseWebView {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Handler f3837a = Threads.newUiHandler();
    @NonNull
    private final BaseWebViewClient b = new BaseWebViewClient();
    /* access modifiers changed from: private */
    @Nullable
    public g c;
    /* access modifiers changed from: private */
    @Nullable
    public Runnable d;
    /* access modifiers changed from: private */
    public boolean e = false;
    @NonNull
    private final OnClickListener f = new OnClickListener() {
        public final void onClick(View view) {
            VastElementView.this.a(view);
        }
    };
    private boolean g = false;
    @NonNull
    private final WebViewClientCallback h = new WebViewClientCallback() {
        public final void onPageStartedLoading(@NonNull String str) {
        }

        public final boolean shouldOverrideUrlLoading(@NonNull String str) {
            if (!VastElementView.this.e) {
                return VastElementView.this.c == null || !VastElementView.this.c.b(str);
            }
            if (VastElementView.this.d != null) {
                VastElementView.this.f3837a.removeCallbacks(VastElementView.this.d);
                VastElementView.this.d = null;
            }
            VastElementView.this.b(str);
            VastElementView.this.e = false;
            return true;
        }

        public final void onPageFinishedLoading(@NonNull String str) {
            VastElementView.this.a();
        }

        public final void onHttpError(@NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse) {
            VastElementView.this.c(String.format("VastElementView WebViewClientHTTP HTTP Error. Request: %s; Error Response: %s", new Object[]{webResourceRequest, webResourceResponse}));
        }

        public final void onGeneralError(int i, @NonNull String str, @NonNull String str2) {
            VastElementView.this.c(String.format("VastElementView WebViewClientHTTP General Error. code: %s; description: %s; url: %s", new Object[]{Integer.valueOf(i), str, str2}));
        }

        public final void onRenderProcessGone() {
            Objects.onNotNull(VastElementView.this.c, $$Lambda$la6kOmj0RDF6wtL4dg4eWAlPwGM.INSTANCE);
        }
    };

    /* access modifiers changed from: private */
    public /* synthetic */ void a(View view) {
        this.e = true;
        if (this.d == null) {
            this.d = new Runnable() {
                public final void run() {
                    VastElementView.this.d();
                }
            };
            this.f3837a.postDelayed(this.d, 100);
        }
    }

    public VastElementView(@NonNull Context context) {
        super(context);
        b();
    }

    public VastElementView(@NonNull Context context, @NonNull AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    private void b() {
        c();
        getSettings().setJavaScriptEnabled(true);
        this.b.setWebViewClientCallback(this.h);
        setWebViewClient(this.b);
        setBackgroundColor(0);
        setOnTouchListener(new i(this.f));
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                VastElementView.this.b((g) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(g gVar) {
        gVar.a(this);
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        Objects.onNotNull(this.c, $$Lambda$8RV3K80G31qgv_EB4RDOLDNlbU.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Objects.onNotNull(this.c, $$Lambda$SIMMp50XgsIZIhW_vCbHatWC8UM.INSTANCE);
        this.g = false;
    }

    public final void a(int i, int i2) {
        Threads.runOnUi(new Runnable(i, i2) {
            private final /* synthetic */ int f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                VastElementView.this.b(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(int i, int i2) {
        LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d() {
        b((String) null);
        this.d = null;
    }

    public final void a(@NonNull g gVar) {
        this.c = gVar;
    }

    public final void a(@NonNull String str) {
        Threads.runOnUi(new Runnable(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                VastElementView.this.d(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(String str) {
        if (!this.g) {
            Objects.onNotNull(this.c, $$Lambda$ehwPenEcWKFhBid193s5VY6_w.INSTANCE);
            loadData(str, "text/html;charset=utf-8", "UTF-8");
        }
    }

    /* access modifiers changed from: protected */
    public final void b(@Nullable String str) {
        Objects.onNotNull(this.c, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((g) obj).a(this.f$0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!this.g) {
            this.g = true;
            Objects.onNotNull(this.c, $$Lambda$lUziYrnNsDbg1k2Y4osGl8F56c.INSTANCE);
        }
    }

    /* access modifiers changed from: protected */
    public final void c(@NonNull String str) {
        Objects.onNotNull(this.c, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((g) obj).a((d) new e(this.f$0));
            }
        });
    }

    private void c() {
        setHorizontalScrollBarEnabled(false);
        setHorizontalScrollbarOverlay(false);
        setVerticalScrollBarEnabled(false);
        setVerticalScrollbarOverlay(false);
        getSettings().setSupportZoom(false);
        setScrollBarStyle(0);
    }
}
