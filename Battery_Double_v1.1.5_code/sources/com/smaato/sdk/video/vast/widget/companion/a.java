package com.smaato.sdk.video.vast.widget.companion;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.VastError;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.smaato.sdk.video.vast.widget.element.c;
import com.smaato.sdk.video.vast.widget.element.d;
import com.smaato.sdk.video.vast.widget.element.e;

public final class a implements c {
    public final int a(@NonNull d dVar) {
        return dVar instanceof e ? IronSourceError.ERROR_BN_LOAD_WHILE_LONG_INITIATION : VastError.ERROR_CODE_UNKNOWN;
    }
}
