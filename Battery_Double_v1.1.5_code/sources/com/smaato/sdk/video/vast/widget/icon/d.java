package com.smaato.sdk.video.vast.widget.icon;

import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeAction.Listener;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.utils.a;
import com.smaato.sdk.video.vast.model.j;
import com.smaato.sdk.video.vast.model.w;
import com.smaato.sdk.video.vast.widget.element.VastElementView;
import com.smaato.sdk.video.vast.widget.element.c;
import com.smaato.sdk.video.vast.widget.element.f;
import com.smaato.sdk.video.vast.widget.element.h;

final class d extends h {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final w f3843a;
    @NonNull
    private final OneTimeActionFactory b;
    @NonNull
    private final a c;
    private final long d;
    private long e;

    d(@NonNull Logger logger, @NonNull f fVar, @NonNull com.smaato.sdk.video.vast.browser.a aVar, @NonNull SomaApiContext somaApiContext, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull c cVar, @NonNull w wVar, @NonNull OneTimeActionFactory oneTimeActionFactory, @NonNull a aVar2, long j) {
        super(logger, fVar, aVar, somaApiContext, visibilityTrackerCreator, cVar);
        this.f3843a = (w) Objects.requireNonNull(wVar);
        this.b = (OneTimeActionFactory) Objects.requireNonNull(oneTimeActionFactory);
        this.c = (a) Objects.requireNonNull(aVar2);
        this.d = j;
    }

    public final void a() {
        this.e = SystemClock.uptimeMillis();
    }

    public final void a(@Nullable String str) {
        String str2;
        j jVar = this.f3843a.e;
        if (jVar == null) {
            str2 = null;
        } else {
            str2 = jVar.b;
        }
        super.a(str2);
    }

    public final void b() {
        super.b();
        long uptimeMillis = SystemClock.uptimeMillis() - this.e;
        this.b.createOneTimeAction(new Listener(uptimeMillis) {
            private final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            public final void doAction() {
                d.this.a(this.f$1);
            }
        }).start(Math.max(this.f3843a.f - uptimeMillis, 0));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(long j) {
        Objects.onNotNull(f(), new Consumer(j) {
            private final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                d.this.a(this.f$1, (VastElementView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(long j, VastElementView vastElementView) {
        this.c.a(vastElementView);
        long j2 = this.f3843a.g;
        if (((float) j2) <= 0.0f) {
            j2 = this.d - j;
        }
        if (((float) j2) > 0.0f) {
            this.b.createOneTimeAction(new Listener() {
                public final void doAction() {
                    d.this.g();
                }
            }).start(j2);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void g() {
        VastElementView f = f();
        a aVar = this.c;
        aVar.getClass();
        Objects.onNotNull(f, new Consumer() {
            public final void accept(Object obj) {
                a.this.b((VastElementView) obj);
            }
        });
    }
}
