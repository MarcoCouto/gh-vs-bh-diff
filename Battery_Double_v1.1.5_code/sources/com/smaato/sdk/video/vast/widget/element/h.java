package com.smaato.sdk.video.vast.widget.element;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTracker;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.tracker.VisibilityTrackerListener;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.browser.a;
import java.lang.ref.WeakReference;

public class h implements g {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f3839a;
    @NonNull
    private final SomaApiContext b;
    @NonNull
    private final VisibilityTrackerCreator c;
    @NonNull
    private final f d;
    @NonNull
    private final c e;
    @NonNull
    private final Logger f;
    @NonNull
    private WeakReference<VastElementView> g = new WeakReference<>(null);
    @Nullable
    private VisibilityTracker h;
    @Nullable
    private g.a i;

    public void a() {
    }

    public h(@NonNull Logger logger, @NonNull f fVar, @NonNull a aVar, @NonNull SomaApiContext somaApiContext, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull c cVar) {
        this.f = (Logger) Objects.requireNonNull(logger);
        this.d = (f) Objects.requireNonNull(fVar);
        this.f3839a = (a) Objects.requireNonNull(aVar);
        this.b = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.c = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.e = (c) Objects.requireNonNull(cVar);
    }

    public final boolean b(@NonNull String str) {
        if (this.f3839a.a(this.b, str)) {
            return true;
        }
        a((d) new b());
        return false;
    }

    @CallSuper
    public void b() {
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                h.this.c((VastElementView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(VastElementView vastElementView) {
        this.h = this.c.createTracker(vastElementView, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                h.this.g();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void g() {
        Objects.onNotNull(this.i, $$Lambda$UKL8Tg5REEtUCS6CxBW343cZqgc.INSTANCE);
    }

    public void c() {
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                h.this.b((VastElementView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(VastElementView vastElementView) {
        this.d.a(vastElementView, new Consumer() {
            public final void accept(Object obj) {
                h.this.a((d) obj);
            }
        });
    }

    @CallSuper
    public final void a(@NonNull VastElementView vastElementView) {
        this.g = new WeakReference<>(vastElementView);
        this.d.a(vastElementView, new Consumer() {
            public final void accept(Object obj) {
                h.this.a((d) obj);
            }
        });
    }

    @CallSuper
    public void d() {
        this.g.clear();
        Objects.onNotNull(this.h, $$Lambda$180cT8NvrkJZ64YGAeZod3HsJqI.INSTANCE);
    }

    public void a(@Nullable String str) {
        Objects.onNotNull(this.i, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((g.a) obj).a(this.f$0);
            }
        });
    }

    public void a(@NonNull d dVar) {
        this.f.debug(LogDomain.VAST, String.format("VastElement error: %s", new Object[]{dVar.toString()}), new Object[0]);
        Objects.onNotNull(this.i, new Consumer(dVar) {
            private final /* synthetic */ d f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                h.this.a(this.f$1, (g.a) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(d dVar, g.a aVar) {
        aVar.a(this.e.a(dVar));
    }

    /* access modifiers changed from: protected */
    @Nullable
    public final VastElementView f() {
        return (VastElementView) this.g.get();
    }

    public final void a(@Nullable g.a aVar) {
        this.i = aVar;
    }

    public void e() {
        Objects.onNotNull(this.i, $$Lambda$P9IOhtUspqj9k98Q2JowMTdxY.INSTANCE);
    }
}
