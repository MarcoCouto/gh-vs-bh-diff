package com.smaato.sdk.video.vast.widget;

import android.view.Surface;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface d {

    public interface a {
        void onSurfaceAvailable(@NonNull Surface surface, int i, int i2);
    }

    public interface b {
        void onSurfaceChanged(@NonNull Surface surface, int i, int i2);
    }

    public interface c {
        void onSurfaceDestroyed(@NonNull Surface surface);
    }

    @NonNull
    View a();

    void a(@Nullable a aVar);

    void a(@Nullable b bVar);

    void a(@Nullable c cVar);
}
