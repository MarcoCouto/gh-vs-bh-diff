package com.smaato.sdk.video.vast.widget.icon;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.video.vast.model.w;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.utils.a;
import com.smaato.sdk.video.vast.widget.element.g;

public final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f3842a;
    @NonNull
    private final VisibilityTrackerCreator b;
    @NonNull
    private final com.smaato.sdk.video.vast.browser.a c;
    @NonNull
    private final OneTimeActionFactory d;
    @NonNull
    private final com.smaato.sdk.video.utils.a e;
    @NonNull
    private final a f;

    public c(@NonNull a aVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull com.smaato.sdk.video.vast.browser.a aVar2, @NonNull OneTimeActionFactory oneTimeActionFactory, @NonNull com.smaato.sdk.video.utils.a aVar3, @NonNull a aVar4) {
        this.f3842a = (a) Objects.requireNonNull(aVar);
        this.b = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.c = (com.smaato.sdk.video.vast.browser.a) Objects.requireNonNull(aVar2);
        this.d = (OneTimeActionFactory) Objects.requireNonNull(oneTimeActionFactory);
        this.e = (com.smaato.sdk.video.utils.a) Objects.requireNonNull(aVar3);
        this.f = (a) Objects.requireNonNull(aVar4);
    }

    @NonNull
    public final g a(@NonNull Logger logger, @NonNull x xVar, @NonNull SomaApiContext somaApiContext) {
        x xVar2 = xVar;
        w wVar = xVar2.h;
        if (wVar == null) {
            return new com.smaato.sdk.video.vast.widget.element.a();
        }
        Logger logger2 = logger;
        Logger logger3 = logger;
        d dVar = new d(logger3, new b(logger, wVar, this.f3842a), this.c, somaApiContext, this.b, this.f, wVar, this.d, this.e, xVar2.f);
        return dVar;
    }
}
