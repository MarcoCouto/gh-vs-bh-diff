package com.smaato.sdk.video.vast.widget.element;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import androidx.annotation.NonNull;

final class i implements OnTouchListener {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final OnClickListener f3840a;
    private int b;
    private float c;
    private float d;

    i(@NonNull OnClickListener onClickListener) {
        this.f3840a = onClickListener;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final boolean onTouch(@NonNull View view, @NonNull MotionEvent motionEvent) {
        int i = 1;
        switch (motionEvent.getAction()) {
            case 0:
                this.c = motionEvent.getX();
                this.d = motionEvent.getY();
                if (this.b != 0) {
                    i = -1;
                }
                this.b = i;
                break;
            case 1:
                if (this.b == 2) {
                    this.b = 0;
                    break;
                } else {
                    this.b = 0;
                    this.f3840a.onClick(view);
                    break;
                }
            case 2:
                float x = motionEvent.getX() - this.c;
                float y = motionEvent.getY() - this.d;
                if (Math.sqrt((double) ((x * x) + (y * y))) >= 5.0d) {
                    if (this.b != 1 && this.b != 2) {
                        this.b = -1;
                        break;
                    } else {
                        this.b = 2;
                        break;
                    }
                }
                break;
            default:
                this.b = -1;
                break;
        }
        return false;
    }
}
