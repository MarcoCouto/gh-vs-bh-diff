package com.smaato.sdk.video.vast.widget.companion;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.ac;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.utils.a;
import com.smaato.sdk.video.vast.widget.element.VastElementView;
import com.smaato.sdk.video.vast.widget.element.d;
import com.smaato.sdk.video.vast.widget.element.e;
import com.smaato.sdk.video.vast.widget.element.f;

final class b implements f {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3833a;
    @NonNull
    private final u b;
    @NonNull
    private final a c;

    b(@NonNull Logger logger, @NonNull u uVar, @NonNull a aVar) {
        this.f3833a = (Logger) Objects.requireNonNull(logger);
        this.b = (u) Objects.requireNonNull(uVar);
        this.c = (a) Objects.requireNonNull(aVar);
    }

    public final void a(@NonNull VastElementView vastElementView, @NonNull Consumer<d> consumer) {
        u uVar = this.b;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i = displayMetrics.widthPixels;
        int i2 = displayMetrics.heightPixels;
        float f = displayMetrics.density;
        float dpToPx = (float) UIUtils.dpToPx(UIUtils.getNormalizedSize(uVar.f), f);
        float dpToPx2 = (float) UIUtils.dpToPx(UIUtils.getNormalizedSize(uVar.g), f);
        if (dpToPx <= 0.0f) {
            dpToPx = (float) i;
        }
        if (dpToPx2 <= 0.0f) {
            dpToPx2 = (float) i2;
        }
        float f2 = (float) i;
        if (dpToPx > f2) {
            dpToPx2 = (dpToPx2 / dpToPx) * f2;
        } else {
            f2 = dpToPx;
        }
        float f3 = (float) i2;
        if (dpToPx2 > f3) {
            f2 = (f2 / dpToPx2) * f3;
            dpToPx2 = f3;
        }
        Size size = new Size((int) f2, (int) dpToPx2);
        ac acVar = this.b.f3750a;
        String a2 = a.a(acVar, size.width, size.height);
        if (TextUtils.isEmpty(a2)) {
            consumer.accept(new e(String.format("Error while preparing Companion. Unable to convert Companion resource: %s", new Object[]{acVar})));
            return;
        }
        vastElementView.a(a2);
        vastElementView.a(size.width, size.height);
    }
}
