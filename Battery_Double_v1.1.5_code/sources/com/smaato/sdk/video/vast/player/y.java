package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.model.x;

public final class y {

    /* renamed from: a reason: collision with root package name */
    final long f3815a;
    final long b;
    final boolean c;

    private y(long j, long j2, boolean z) {
        this.f3815a = j;
        this.b = j2;
        this.c = z;
    }

    @NonNull
    public static y a(@NonNull x xVar, long j, long j2, boolean z) {
        long j3 = xVar.f;
        y yVar = new y(j3, j, j3 > j2 && z);
        return yVar;
    }
}
