package com.smaato.sdk.video.vast.player;

public enum d {
    IDLE,
    INITIALIZED,
    PREPARING,
    PREPARED,
    STARTED,
    RESUMED,
    PAUSED,
    STOPPED,
    PLAYBACK_COMPLETED,
    END,
    ERROR
}
