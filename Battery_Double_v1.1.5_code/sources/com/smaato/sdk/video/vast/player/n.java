package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.tracking.e;
import com.smaato.sdk.video.vast.tracking.f;
import com.smaato.sdk.video.vast.tracking.h;

class n {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final LinkResolver f3800a;
    @NonNull
    private final h b;
    @NonNull
    private final e c;

    n(@NonNull LinkResolver linkResolver, @NonNull h hVar, @NonNull e eVar) {
        this.f3800a = (LinkResolver) Objects.requireNonNull(linkResolver);
        this.b = (h) Objects.requireNonNull(hVar);
        this.c = (e) Objects.requireNonNull(eVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final m a(@NonNull Logger logger, @NonNull aa aaVar, @NonNull SomaApiContext somaApiContext, @NonNull f fVar, boolean z) {
        a aVar = new a(logger, somaApiContext, this.f3800a, aaVar.e.e);
        Logger logger2 = logger;
        aa aaVar2 = aaVar;
        f fVar2 = fVar;
        m mVar = new m(logger2, aaVar2, fVar2, this.b.a(aaVar, somaApiContext), this.c.a(aaVar, somaApiContext), aVar, z);
        return mVar;
    }
}
