package com.smaato.sdk.video.vast.player.system;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Builder;
import com.smaato.sdk.video.vast.player.d;
import java.util.Arrays;

public class e {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final d f3809a;

    public e(@NonNull d dVar) {
        this.f3809a = dVar;
    }

    @NonNull
    public final StateMachine<com.smaato.sdk.video.vast.player.e, d> a() {
        Builder builder = new Builder();
        builder.setInitialState(this.f3809a).addTransition(com.smaato.sdk.video.vast.player.e.SET_DATA_SOURCE, Arrays.asList(new d[]{d.IDLE, d.INITIALIZED})).addTransition(com.smaato.sdk.video.vast.player.e.PREPARE_ASYNC, Arrays.asList(new d[]{d.INITIALIZED, d.PREPARING})).addTransition(com.smaato.sdk.video.vast.player.e.PREPARE_ASYNC, Arrays.asList(new d[]{d.STOPPED, d.PREPARING})).addTransition(com.smaato.sdk.video.vast.player.e.ON_PREPARED, Arrays.asList(new d[]{d.PREPARING, d.PREPARED})).addTransition(com.smaato.sdk.video.vast.player.e.START, Arrays.asList(new d[]{d.PREPARED, d.STARTED})).addTransition(com.smaato.sdk.video.vast.player.e.STOP, Arrays.asList(new d[]{d.PREPARED, d.STOPPED}));
        builder.addTransition(com.smaato.sdk.video.vast.player.e.START, Arrays.asList(new d[]{d.PLAYBACK_COMPLETED, d.STARTED})).addTransition(com.smaato.sdk.video.vast.player.e.STOP, Arrays.asList(new d[]{d.PLAYBACK_COMPLETED, d.STOPPED}));
        builder.addTransition(com.smaato.sdk.video.vast.player.e.STOP, Arrays.asList(new d[]{d.STARTED, d.STOPPED})).addTransition(com.smaato.sdk.video.vast.player.e.PAUSE, Arrays.asList(new d[]{d.STARTED, d.PAUSED})).addTransition(com.smaato.sdk.video.vast.player.e.ON_COMPLETE, Arrays.asList(new d[]{d.STARTED, d.PLAYBACK_COMPLETED}));
        builder.addTransition(com.smaato.sdk.video.vast.player.e.STOP, Arrays.asList(new d[]{d.RESUMED, d.STOPPED})).addTransition(com.smaato.sdk.video.vast.player.e.PAUSE, Arrays.asList(new d[]{d.RESUMED, d.PAUSED})).addTransition(com.smaato.sdk.video.vast.player.e.ON_COMPLETE, Arrays.asList(new d[]{d.RESUMED, d.PLAYBACK_COMPLETED}));
        builder.addTransition(com.smaato.sdk.video.vast.player.e.STOP, Arrays.asList(new d[]{d.PAUSED, d.STOPPED})).addTransition(com.smaato.sdk.video.vast.player.e.START, Arrays.asList(new d[]{d.PAUSED, d.RESUMED}));
        builder.addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.IDLE, d.ERROR})).addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.INITIALIZED, d.ERROR})).addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.PREPARING, d.ERROR})).addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.PREPARED, d.ERROR})).addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.STARTED, d.ERROR})).addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.RESUMED, d.ERROR})).addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.PAUSED, d.ERROR})).addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.STOPPED, d.ERROR})).addTransition(com.smaato.sdk.video.vast.player.e.ON_ERROR, Arrays.asList(new d[]{d.PLAYBACK_COMPLETED, d.ERROR}));
        builder.addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.ERROR, d.IDLE})).addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.INITIALIZED, d.IDLE})).addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.PREPARED, d.IDLE})).addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.PREPARING, d.IDLE})).addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.STARTED, d.IDLE})).addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.RESUMED, d.IDLE})).addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.STOPPED, d.IDLE})).addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.PAUSED, d.IDLE})).addTransition(com.smaato.sdk.video.vast.player.e.RESET, Arrays.asList(new d[]{d.PLAYBACK_COMPLETED, d.IDLE}));
        builder.addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.IDLE, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.ERROR, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.INITIALIZED, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.PREPARED, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.PREPARING, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.STARTED, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.RESUMED, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.STOPPED, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.PAUSED, d.END})).addTransition(com.smaato.sdk.video.vast.player.e.RELEASE, Arrays.asList(new d[]{d.PLAYBACK_COMPLETED, d.END}));
        return builder.build();
    }
}
