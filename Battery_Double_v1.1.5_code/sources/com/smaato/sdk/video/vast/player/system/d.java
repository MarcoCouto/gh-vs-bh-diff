package com.smaato.sdk.video.vast.player.system;

import android.content.Context;
import android.media.MediaPlayer;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.utils.c;
import com.smaato.sdk.video.vast.player.e;
import com.smaato.sdk.video.vast.player.s;
import com.smaato.sdk.video.vast.player.t;

public final class d implements t {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f3808a;
    @NonNull
    private final e b;
    @NonNull
    private final c<com.smaato.sdk.video.vast.player.c, com.smaato.sdk.video.vast.player.d> c;
    @NonNull
    private final c<e, com.smaato.sdk.video.vast.player.d> d;

    public d(@NonNull Context context, @NonNull e eVar, @NonNull c<com.smaato.sdk.video.vast.player.c, com.smaato.sdk.video.vast.player.d> cVar, @NonNull c<e, com.smaato.sdk.video.vast.player.d> cVar2) {
        this.f3808a = (Context) Objects.requireNonNull(context);
        this.b = (e) Objects.requireNonNull(eVar);
        this.c = (c) Objects.requireNonNull(cVar);
        this.d = (c) Objects.requireNonNull(cVar2);
    }

    @NonNull
    public final s a(@NonNull Logger logger) {
        Objects.requireNonNull(logger);
        b bVar = new b(this.f3808a, new MediaPlayer(), this.b.a(), this.c, this.d, logger);
        return bVar;
    }
}
