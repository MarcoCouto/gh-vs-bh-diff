package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Builder;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.model.u;
import java.util.Arrays;

public class q {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final j f3805a;

    q(@NonNull j jVar) {
        this.f3805a = (j) Objects.requireNonNull(jVar);
    }

    @NonNull
    public final StateMachine<i, j> a(@NonNull aa aaVar) {
        u uVar = aaVar.f;
        Builder builder = new Builder();
        j jVar = uVar == null ? j.CLOSE_PLAYER : j.SHOW_COMPANION;
        builder.setInitialState(this.f3805a).addTransition(i.ERROR, Arrays.asList(new j[]{j.SHOW_VIDEO, j.CLOSE_PLAYER})).addTransition(i.ERROR, Arrays.asList(new j[]{j.SHOW_COMPANION, j.CLOSE_PLAYER})).addTransition(i.CLICKED, Arrays.asList(new j[]{j.SHOW_VIDEO, j.CLOSE_PLAYER})).addTransition(i.CLICKED, Arrays.asList(new j[]{j.SHOW_COMPANION, j.CLOSE_PLAYER})).addTransition(i.VIDEO_COMPLETED, Arrays.asList(new j[]{j.SHOW_VIDEO, jVar})).addTransition(i.VIDEO_SKIPPED, Arrays.asList(new j[]{j.SHOW_VIDEO, jVar})).addTransition(i.CLOSE_BUTTON_CLICKED, Arrays.asList(new j[]{j.SHOW_VIDEO, j.CLOSE_PLAYER})).addTransition(i.CLOSE_BUTTON_CLICKED, Arrays.asList(new j[]{j.SHOW_COMPANION, j.CLOSE_PLAYER}));
        return builder.build();
    }
}
