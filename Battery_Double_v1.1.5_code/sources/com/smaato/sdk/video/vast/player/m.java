package com.smaato.sdk.video.vast.player;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.model.t;
import com.smaato.sdk.video.vast.model.v;
import com.smaato.sdk.video.vast.player.a.C0083a;
import com.smaato.sdk.video.vast.tracking.d;
import com.smaato.sdk.video.vast.tracking.f;
import com.smaato.sdk.video.vast.tracking.g;
import java.util.concurrent.atomic.AtomicReference;

final class m {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3798a;
    @NonNull
    private final d b;
    @NonNull
    private final aa c;
    @NonNull
    private final g d;
    @NonNull
    private final f e;
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<com.smaato.sdk.video.vast.player.k.a> f = new AtomicReference<>();
    @NonNull
    private final a g;
    private final boolean h;
    private long i;

    final class a implements C0083a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final C0083a f3799a;

        /* synthetic */ a(m mVar, C0083a aVar, byte b2) {
            this(aVar);
        }

        private a(C0083a aVar) {
            this.f3799a = aVar;
        }

        public final void onUrlResolved(@NonNull Consumer<Context> consumer) {
            this.f3799a.onUrlResolved(consumer);
            Objects.onNotNull(m.this.f.get(), $$Lambda$NQQClBa0aMCZD3BAeRqswID8weg.INSTANCE);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
    }

    m(@NonNull Logger logger, @NonNull aa aaVar, @NonNull f fVar, @NonNull g gVar, @NonNull d dVar, @NonNull a aVar, boolean z) {
        this.f3798a = (Logger) Objects.requireNonNull(logger);
        this.c = (aa) Objects.requireNonNull(aaVar);
        this.e = (f) Objects.requireNonNull(fVar);
        this.d = (g) Objects.requireNonNull(gVar);
        this.b = (d) Objects.requireNonNull(dVar);
        this.g = (a) Objects.requireNonNull(aVar);
        this.h = z;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull com.smaato.sdk.video.vast.player.k.a aVar) {
        this.f.set(aVar);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        Objects.onNotNull(this.f.get(), $$Lambda$yMP3ZbB381CSuk1ReOlqczYFHA.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        Objects.onNotNull(this.f.get(), $$Lambda$Pjdidl42uWVcOzn8y4I1Pi7tgOk.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        Objects.onNotNull(this.f.get(), $$Lambda$cvyrvCqkPBK5SoPh8pewDRR3fk.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        a(t.SMAATO_VIEWABLE_IMPRESSION);
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        this.d.a(v.SKIP, this.i);
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        this.d.a(v.MUTE, this.i);
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        this.d.a(v.UNMUTE, this.i);
    }

    /* access modifiers changed from: 0000 */
    public final void i() {
        Objects.onNotNull(this.f.get(), $$Lambda$PZQ8LGABgtK4GYfQ7_CEUw0mDg.INSTANCE);
        this.d.a(v.COMPLETE, this.i);
    }

    /* access modifiers changed from: 0000 */
    public final void j() {
        this.d.a(v.PAUSE, this.i);
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j, long j2) {
        this.i = j;
        this.d.a(j, j2);
        if (((float) j) / ((float) j2) >= 0.01f) {
            a(t.SMAATO_IMPRESSION);
            Objects.onNotNull(this.f.get(), $$Lambda$wYeYkSnvPuR06isr1dWwetk7CY8.INSTANCE);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void k() {
        this.d.a(v.RESUME, this.i);
    }

    /* access modifiers changed from: 0000 */
    public final void l() {
        this.d.a(v.CREATIVE_VIEW, this.i);
        Objects.onNotNull(this.f.get(), $$Lambda$gvQ2nSXmqTUEDOvMe7bUiM_xxOM.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void m() {
        a(t.SMAATO_ICON_VIEW_TRACKING);
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2) {
        d(i2);
    }

    /* access modifiers changed from: 0000 */
    public final void b(int i2) {
        d(i2);
    }

    /* access modifiers changed from: 0000 */
    public final void c(int i2) {
        d(i2);
    }

    private void d(int i2) {
        this.e.a(new com.smaato.sdk.video.vast.build.h.a(i2).a(this.i).a(this.c.e.b.f3737a).a());
    }

    private void a(@NonNull t tVar) {
        this.b.a(tVar, new com.smaato.sdk.video.vast.tracking.a(this.c.e.b.f3737a, Long.valueOf(this.i)));
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull C0083a aVar) {
        if (this.h) {
            a(t.SMAATO_VIDEO_CLICK_TRACKING);
            this.g.a(null, new a(this, aVar, 0));
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable String str, @NonNull C0083a aVar) {
        a(t.SMAATO_COMPANION_CLICK_TRACKING);
        this.g.a(str, new a(this, aVar, 0));
    }

    /* access modifiers changed from: 0000 */
    public final void b(@Nullable String str, @NonNull C0083a aVar) {
        a(t.SMAATO_ICON_CLICK_TRACKING);
        this.g.a(str, aVar);
    }
}
