package com.smaato.sdk.video.vast.player;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.widget.e;

public final class k {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final o f3795a;
    @NonNull
    private final r b;

    public interface a {
        void a();

        void b();

        void c();

        void d();

        void e();

        void f();

        void g();
    }

    k(@NonNull o oVar, @NonNull r rVar) {
        this.f3795a = (o) Objects.requireNonNull(oVar);
        this.b = (r) Objects.requireNonNull(rVar);
    }

    @NonNull
    public final AdContentView a(@NonNull Context context) {
        Objects.requireNonNull(context);
        final e a2 = r.a(context);
        a2.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(@NonNull View view) {
                k.this.f3795a.a(a2);
            }

            public final void onViewDetachedFromWindow(@NonNull View view) {
                view.removeOnAttachStateChangeListener(this);
                k.this.f3795a.c();
            }
        });
        return a2;
    }

    public final void a(@NonNull a aVar) {
        this.f3795a.a().a(aVar);
    }

    public final void a() {
        this.f3795a.b();
    }
}
