package com.smaato.sdk.video.vast.player;

import android.view.Surface;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.tracker.VisibilityTracker;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.tracker.VisibilityTrackerListener;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.utils.d;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.player.s.b;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

class v implements com.smaato.sdk.video.vast.player.s.a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final s f3812a;
    @NonNull
    private final x b;
    @NonNull
    private final z c;
    @NonNull
    private final VisibilityTrackerCreator d;
    @NonNull
    private final g e;
    @NonNull
    private final d f;
    @NonNull
    private final AtomicReference<VisibilityTracker> g;
    @Nullable
    private a h;
    @NonNull
    private final com.smaato.sdk.video.vast.player.s.a i = this;
    @NonNull
    private WeakReference<x> j = new WeakReference<>(null);
    private long k;

    interface a {
        void a(long j, long j2);

        void b(int i);

        void f();

        void g();

        void h();

        void i();

        void j();

        void k();

        void l();

        void m();

        void n();
    }

    v(@NonNull s sVar, @NonNull x xVar, @NonNull z zVar, @NonNull g gVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull f fVar) {
        this.f3812a = (s) Objects.requireNonNull(sVar);
        this.b = (x) Objects.requireNonNull(xVar);
        this.c = (z) Objects.requireNonNull(zVar);
        this.e = (g) Objects.requireNonNull(gVar);
        this.d = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.f = (d) Objects.requireNonNull(fVar.a(new com.smaato.sdk.video.utils.d.a() {
            public final void doAction() {
                v.this.n();
            }
        }));
        this.g = new AtomicReference<>();
        sVar.a(this.i);
        sVar.a((b) new b() {
            public final void onVolumeChanged(float f) {
                v.this.a(f);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(float f2) {
        boolean z = f2 == 0.0f;
        Objects.onNotNull(this.j.get(), new Consumer(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((x) obj).a(this.f$0);
            }
        });
        Objects.onNotNull(this.h, new Consumer(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                v.a(this.f$0, (a) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(boolean z, a aVar) {
        if (z) {
            aVar.j();
        } else {
            aVar.k();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable a aVar) {
        this.h = aVar;
    }

    /* access modifiers changed from: private */
    public void n() {
        long g2 = this.f3812a.g();
        if (g2 != this.k) {
            this.k = g2;
            long j2 = this.k;
            long h2 = this.f3812a.h();
            Objects.onNotNull(this.h, new Consumer(j2, h2) {
                private final /* synthetic */ long f$0;
                private final /* synthetic */ long f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r3;
                }

                public final void accept(Object obj) {
                    ((a) obj).a(this.f$0, this.f$1);
                }
            });
            Object obj = this.j.get();
            $$Lambda$v$Fg6d_FrubUguPeZ_nbLQA8h2_Y r5 = new Consumer(j2, h2) {
                private final /* synthetic */ long f$1;
                private final /* synthetic */ long f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r4;
                }

                public final void accept(Object obj) {
                    v.this.a(this.f$1, this.f$2, (x) obj);
                }
            };
            Objects.onNotNull(obj, r5);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull x xVar) {
        this.j = new WeakReference<>(xVar);
        xVar.a(this);
        xVar.a(this.f3812a.f() == 0.0f);
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        this.j.clear();
        o();
    }

    /* access modifiers changed from: 0000 */
    public final void i() {
        this.j.clear();
        o();
        this.f3812a.c();
        this.f3812a.e();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Surface surface) {
        Objects.onNotNull(this.j.get(), new Consumer() {
            public final void accept(Object obj) {
                v.this.b((x) obj);
            }
        });
        this.f3812a.a(surface);
        this.f3812a.a();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(x xVar) {
        this.g.set(this.d.createTracker(xVar, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                v.this.p();
            }
        }));
    }

    /* access modifiers changed from: 0000 */
    public final void j() {
        o();
        this.f3812a.a((Surface) null);
        this.f3812a.b();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull x xVar, int i2, int i3) {
        this.c.a(xVar, i2, i3);
    }

    /* access modifiers changed from: 0000 */
    public final void k() {
        Objects.onNotNull(this.h, $$Lambda$uGBNf2_iV4xMQfRSj5jVWGeNcBk.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void l() {
        Objects.onNotNull(this.h, $$Lambda$H3fay7CyfTWrKt5C92deE3f2FQ.INSTANCE);
        i();
    }

    /* access modifiers changed from: 0000 */
    public final void m() {
        float f2 = 0.0f;
        boolean z = this.f3812a.f() == 0.0f;
        s sVar = this.f3812a;
        if (z) {
            f2 = 1.0f;
        }
        sVar.a(f2);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void p() {
        Objects.onNotNull(this.h, $$Lambda$t1qwL4YbprNzGNNZGK8zLDrwkQ4.INSTANCE);
    }

    private void o() {
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                v.this.a((VisibilityTracker) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(VisibilityTracker visibilityTracker) {
        visibilityTracker.destroy();
        this.g.set(null);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(long j2, long j3, x xVar) {
        xVar.a(j2, j3);
        this.e.a(j2, xVar);
    }

    public void a() {
        this.f.a();
        Objects.onNotNull(this.h, $$Lambda$tcLeTpxGBWZDW5dM4cauyOaCIKY.INSTANCE);
    }

    public void b() {
        this.f.a();
        Objects.onNotNull(this.h, $$Lambda$_kXKAnFcbDVWH44mXAK9ncFauXQ.INSTANCE);
    }

    public void c() {
        Objects.onNotNull(this.h, $$Lambda$PslJeKfmDWYIq0sSUo8CzytxFDs.INSTANCE);
        this.f.b();
    }

    public void d() {
        this.f.b();
    }

    public void e() {
        Objects.onNotNull(this.h, $$Lambda$UukvgkKKipFg8yi4X0EGU9zPo.INSTANCE);
        this.f.b();
    }

    public void f() {
        this.f.b();
    }

    public void g() {
        Objects.onNotNull(this.h, $$Lambda$v$DbTnW0QzNUzq1LzR8NuYRZwSm0.INSTANCE);
        this.f.b();
    }
}
