package com.smaato.sdk.video.vast.player;

public enum c {
    SET_SURFACE,
    GET_CURRENT_POSITION,
    GET_DURATION,
    IS_PLAYING,
    SEEK_TO,
    SET_VOLUME
}
