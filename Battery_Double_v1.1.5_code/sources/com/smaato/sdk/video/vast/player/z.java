package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Size;

public final class z {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Size f3816a;

    z(@NonNull Size size) {
        this.f3816a = size;
    }

    public final void a(@NonNull x xVar, int i, int i2) {
        float f = (float) i;
        float f2 = (float) i2;
        if (f / f2 > ((float) this.f3816a.width) / ((float) this.f3816a.height)) {
            i = Math.round(((float) this.f3816a.width) * (f2 / ((float) this.f3816a.height)));
        } else {
            i2 = Math.round(((float) this.f3816a.height) * (f / ((float) this.f3816a.width)));
        }
        xVar.a(i, i2);
    }
}
