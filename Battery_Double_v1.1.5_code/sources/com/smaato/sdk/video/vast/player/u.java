package com.smaato.sdk.video.vast.player;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.n;
import com.smaato.sdk.video.vast.player.exception.f;
import com.smaato.sdk.video.vast.player.s.c;

public class u {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final t f3810a;

    public u(@NonNull t tVar) {
        this.f3810a = (t) Objects.requireNonNull(tVar);
    }

    public final void a(@NonNull final Logger logger, @NonNull n nVar, @NonNull final b<Either<s, Exception>> bVar) {
        Objects.requireNonNull(logger);
        final Uri parse = Uri.parse(nVar.f3737a);
        s a2 = this.f3810a.a(logger);
        a2.a((c) new c() {
            public final void b(@NonNull s sVar) {
                logger.debug(LogDomain.VAST, "VAST VideoPlayer initialised. Preparing...", new Object[0]);
                sVar.d();
            }

            public final void a(@NonNull s sVar) {
                logger.debug(LogDomain.VAST, "VAST VideoPlayer prepared with DataSource: %s", parse);
                sVar.a((c) null);
                bVar.accept(Either.left(sVar));
            }

            public final void a(@NonNull s sVar, @NonNull f fVar) {
                logger.error(LogDomain.VAST, String.format("Unable to prepare VAST VideoPlayer with DataSource: %s", new Object[]{parse}), new Object[0]);
                sVar.a((c) null);
                bVar.accept(Either.right(fVar));
            }
        });
        logger.debug(LogDomain.VAST, "Initialising VAST VideoPlayer with DataSource: %s", parse);
        a2.a(parse.toString());
    }
}
