package com.smaato.sdk.video.vast.player;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.view.Surface;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.R;
import com.smaato.sdk.video.vast.widget.CircularProgressBar;
import com.smaato.sdk.video.vast.widget.d;
import com.smaato.sdk.video.vast.widget.d.a;
import com.smaato.sdk.video.vast.widget.d.b;
import com.smaato.sdk.video.vast.widget.d.c;

public abstract class x extends FrameLayout {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private ImageButton f3814a;
    @NonNull
    private ImageButton b;
    @NonNull
    private CircularProgressBar c = ((CircularProgressBar) findViewById(R.id.smaato_sdk_video_video_progress));
    @NonNull
    private View d;
    @Nullable
    private v e;

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Surface surface, int i, int i2, v vVar) {
    }

    /* access modifiers changed from: protected */
    @NonNull
    public abstract d a(@NonNull Context context);

    protected x(Context context) {
        super(context);
        inflate(context, R.layout.smaato_sdk_video_player_view, this);
        d a2 = a(context);
        a2.a((a) new a() {
            public final void onSurfaceAvailable(Surface surface, int i, int i2) {
                x.this.a(surface, i, i2);
            }
        });
        a2.a((b) new b() {
            public final void onSurfaceChanged(Surface surface, int i, int i2) {
                x.this.b(surface, i, i2);
            }
        });
        a2.a((c) new c() {
            public final void onSurfaceDestroyed(Surface surface) {
                x.this.a(surface);
            }
        });
        View a3 = a2.a();
        a3.setId(R.id.smaato_sdk_video_surface_holder_view_id);
        a3.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                x.this.a(view);
            }
        });
        ((FrameLayout) findViewById(R.id.smaato_sdk_video_player_surface_layout)).addView(a3, new LayoutParams(-1, -1));
        this.d = a3;
        ImageButton imageButton = (ImageButton) findViewById(R.id.smaato_sdk_video_skip_button);
        imageButton.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                x.this.c(view);
            }
        });
        this.f3814a = imageButton;
        ImageButton imageButton2 = (ImageButton) findViewById(R.id.smaato_sdk_video_mute_button);
        imageButton2.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                x.this.b(view);
            }
        });
        this.b = imageButton2;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable v vVar) {
        Threads.ensureMainThread();
        this.e = vVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        setMeasuredDimension(size, size2);
        if (size > 0 && size2 > 0) {
            Objects.onNotNull(this.e, new Consumer(size, size2) {
                private final /* synthetic */ int f$1;
                private final /* synthetic */ int f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    x.this.a(this.f$1, this.f$2, (v) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(int i, int i2, v vVar) {
        vVar.a(this, i, i2);
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        Threads.runOnUi(new Runnable(z) {
            private final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                x.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(boolean z) {
        this.b.setImageResource(z ? R.drawable.smaato_sdk_video_muted : R.drawable.smaato_sdk_video_unmuted);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Objects.onNotNull(this.e, $$Lambda$kOj7u0fGGCrcdPcGI8wvmJEtHhA.INSTANCE);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull View view) {
        Objects.onNotNull(this.e, $$Lambda$KhRbTJe2QFX2VZVn1gzE5_n3hM.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i, int i2) {
        Threads.runOnUi(new Runnable(i, i2) {
            private final /* synthetic */ int f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                x.this.b(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(int i, int i2) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.d.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        layoutParams.gravity = 17;
        this.d.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(View view) {
        Objects.onNotNull(this.e, $$Lambda$5Mm9WCzL_SROKiK62dsNkDKOKCc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(View view) {
        Objects.onNotNull(this.e, $$Lambda$PeHe2OZqcTaV4WMQSfNOymrcLU.INSTANCE);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Surface surface, int i, int i2) {
        Objects.onNotNull(this.e, new Consumer(surface) {
            private final /* synthetic */ Surface f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((v) obj).a(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull Surface surface, int i, int i2) {
        Objects.onNotNull(this.e, new Consumer(surface, i, i2) {
            private final /* synthetic */ Surface f$0;
            private final /* synthetic */ int f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                x.a(this.f$0, this.f$1, this.f$2, (v) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Surface surface) {
        Objects.onNotNull(this.e, new Consumer(surface) {
            private final /* synthetic */ Surface f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((v) obj).j();
            }
        });
        surface.release();
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j, long j2) {
        long j3 = j;
        long j4 = j2;
        $$Lambda$x$tfXZexiocY2S3PwpNStgEwEdYlg r1 = new Runnable(j3, j4, String.valueOf(((int) (j2 / 1000)) - ((int) (j / 1000)))) {
            private final /* synthetic */ long f$1;
            private final /* synthetic */ long f$2;
            private final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r4;
                this.f$3 = r6;
            }

            public final void run() {
                x.this.a(this.f$1, this.f$2, this.f$3);
            }
        };
        Threads.runOnUi(r1);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(long j, long j2, String str) {
        this.c.a((float) j, (float) j2, str);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        Threads.runOnUi(new Runnable() {
            public final void run() {
                x.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        if (!(this.f3814a.getVisibility() == 0)) {
            this.f3814a.setAlpha(0.0f);
            this.f3814a.setVisibility(0);
            this.f3814a.animate().alpha(1.0f).setDuration(300).start();
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (!(VERSION.SDK_INT >= 18 && isInLayout())) {
            requestLayout();
        }
    }
}
