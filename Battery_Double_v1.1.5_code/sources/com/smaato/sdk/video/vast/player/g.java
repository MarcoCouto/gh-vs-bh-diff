package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;

public abstract class g {

    static class a extends g {
        public final void a(long j, @NonNull x xVar) {
        }

        private a() {
        }

        /* synthetic */ a(byte b) {
            this();
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(long j, @NonNull x xVar);
}
