package com.smaato.sdk.video.vast.player;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.utils.d;
import com.smaato.sdk.video.utils.d.a;

public class f {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Handler f3791a;

    public f(@NonNull Handler handler) {
        this.f3791a = (Handler) Objects.requireNonNull(handler);
    }

    @NonNull
    public final d a(@NonNull a aVar) {
        return new d(this.f3791a, aVar);
    }
}
