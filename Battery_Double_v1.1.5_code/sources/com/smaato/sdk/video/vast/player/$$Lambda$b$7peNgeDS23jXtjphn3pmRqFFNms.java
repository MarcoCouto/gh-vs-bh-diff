package com.smaato.sdk.video.vast.player;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.video.vast.player.-$$Lambda$b$7peNgeDS23jXtjphn3pmRqFFNms reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$b$7peNgeDS23jXtjphn3pmRqFFNms implements ClassFactory {
    public static final /* synthetic */ $$Lambda$b$7peNgeDS23jXtjphn3pmRqFFNms INSTANCE = new $$Lambda$b$7peNgeDS23jXtjphn3pmRqFFNms();

    private /* synthetic */ $$Lambda$b$7peNgeDS23jXtjphn3pmRqFFNms() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(1.0d).visibilityTimeMillis(100).build();
    }
}
