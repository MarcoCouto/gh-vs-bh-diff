package com.smaato.sdk.video.vast.player.system;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.video.utils.c.a;
import com.smaato.sdk.video.vast.player.d;
import java.util.Arrays;

public final class c implements ClassFactory<com.smaato.sdk.video.utils.c> {
    @NonNull
    public final /* synthetic */ Object get(@NonNull DiConstructor diConstructor) {
        a aVar = new a();
        aVar.a(com.smaato.sdk.video.vast.player.c.SET_SURFACE, Arrays.asList(new d[]{d.INITIALIZED, d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.STOPPED, d.PLAYBACK_COMPLETED})).a(com.smaato.sdk.video.vast.player.c.GET_CURRENT_POSITION, Arrays.asList(new d[]{d.IDLE, d.INITIALIZED, d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.STOPPED, d.PLAYBACK_COMPLETED})).a(com.smaato.sdk.video.vast.player.c.GET_DURATION, Arrays.asList(new d[]{d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.STOPPED, d.PLAYBACK_COMPLETED})).a(com.smaato.sdk.video.vast.player.c.IS_PLAYING, Arrays.asList(new d[]{d.IDLE, d.INITIALIZED, d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.STOPPED, d.PLAYBACK_COMPLETED})).a(com.smaato.sdk.video.vast.player.c.SEEK_TO, Arrays.asList(new d[]{d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.PLAYBACK_COMPLETED})).a(com.smaato.sdk.video.vast.player.c.SET_VOLUME, Arrays.asList(new d[]{d.IDLE, d.INITIALIZED, d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.STOPPED, d.PLAYBACK_COMPLETED}));
        return aVar.a();
    }
}
