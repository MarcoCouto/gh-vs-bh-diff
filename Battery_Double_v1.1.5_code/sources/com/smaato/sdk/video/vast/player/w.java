package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.VastError;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.build.h.a;
import com.smaato.sdk.video.vast.model.n;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.player.exception.e;
import com.smaato.sdk.video.vast.tracking.f;

class w {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final VisibilityTrackerCreator f3813a;
    @NonNull
    private final u b;
    @NonNull
    private final f c;
    private final boolean d = true;
    private final long e = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
    private final long f = MTGInterstitialActivity.WEB_LOAD_TIME;

    w(@NonNull u uVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull f fVar, boolean z, long j, long j2) {
        this.b = (u) Objects.requireNonNull(uVar);
        this.f3813a = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.c = (f) Objects.requireNonNull(fVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Logger logger, @NonNull x xVar, @NonNull f fVar, @NonNull b<Either<v, Exception>> bVar, boolean z) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(bVar);
        u uVar = this.b;
        n nVar = xVar.b;
        $$Lambda$w$a5WVJBJeiPEVMnRT9xQvxNhqeBg r2 = new b(xVar, fVar, bVar, z) {
            private final /* synthetic */ x f$1;
            private final /* synthetic */ f f$2;
            private final /* synthetic */ b f$3;
            private final /* synthetic */ boolean f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void accept(Object obj) {
                w.this.a(this.f$1, this.f$2, this.f$3, this.f$4, (Either) obj);
            }
        };
        uVar.a(logger, nVar, r2);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b0  */
    public /* synthetic */ void a(x xVar, f fVar, b bVar, boolean z, Either either) {
        g gVar;
        a aVar;
        x xVar2 = xVar;
        f fVar2 = fVar;
        b bVar2 = bVar;
        n nVar = xVar2.b;
        Exception exc = (Exception) either.right();
        if (exc == null) {
            s sVar = (s) Objects.requireNonNull(either.left());
            if (Math.abs(sVar.h() - xVar2.f) > 3000) {
                fVar.a(new a(VastError.ERROR_CODE_DURATION).a(nVar.f3737a).a());
                bVar2.accept(Either.right(new Exception("Video player expecting different duration")));
                return;
            }
            int round = nVar.d == null ? 0 : Math.round(nVar.d.floatValue());
            int round2 = nVar.e == null ? 0 : Math.round(nVar.e.floatValue());
            if (round == 0 || round2 == 0) {
                round = 16;
                round2 = 9;
            }
            z zVar = new z(new Size(round, round2));
            y a2 = y.a(xVar, this.e, this.f, z);
            if (a2 == null) {
                aVar = new a(0);
            } else if (a2.c) {
                gVar = new h(a2.b, a2.f3815a);
                sVar.a(!this.d ? 0.0f : 1.0f);
                v vVar = new v(sVar, xVar, zVar, gVar, this.f3813a, this.c);
                bVar2.accept(Either.left(vVar));
                return;
            } else {
                aVar = new a(0);
            }
            gVar = aVar;
            sVar.a(!this.d ? 0.0f : 1.0f);
            v vVar2 = new v(sVar, xVar, zVar, gVar, this.f3813a, this.c);
            bVar2.accept(Either.left(vVar2));
            return;
        }
        try {
            throw exc;
        } catch (com.smaato.sdk.video.vast.player.exception.b | e unused) {
            fVar.a(new a(VastError.ERROR_CODE_ERROR_SHOWING).a(nVar.f3737a).a());
        } catch (Exception unused2) {
            fVar.a(new a(400).a(nVar.f3737a).a());
        }
        bVar2.accept(Either.right(exc));
    }
}
