package com.smaato.sdk.video.vast.player;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.af;
import com.smaato.sdk.video.vast.model.s;
import java.util.concurrent.atomic.AtomicReference;

class a {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final af f3785a;
    @NonNull
    private final LinkResolver b;
    @NonNull
    private final SomaApiContext c;
    /* access modifiers changed from: private */
    @NonNull
    public final Logger d;
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<Task> e = new AtomicReference<>();

    /* renamed from: com.smaato.sdk.video.vast.player.a$a reason: collision with other inner class name */
    public interface C0083a {
        void onUrlResolved(@NonNull Consumer<Context> consumer);
    }

    a(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull LinkResolver linkResolver, @Nullable af afVar) {
        this.b = (LinkResolver) Objects.requireNonNull(linkResolver);
        this.c = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.d = (Logger) Objects.requireNonNull(logger);
        this.f3785a = afVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable final String str, @NonNull final C0083a aVar) {
        if (TextUtils.isEmpty(str)) {
            s sVar = this.f3785a == null ? null : this.f3785a.c;
            if (sVar == null) {
                str = null;
            } else {
                str = sVar.f3747a;
            }
        }
        if (TextUtils.isEmpty(str)) {
            this.d.error(LogDomain.VAST, "Cannot handle click due to a missing URL", new Object[0]);
            return;
        }
        if (this.e.get() == null) {
            Task handleClickThroughUrl = this.b.handleClickThroughUrl(this.c, str, new UrlResolveListener() {
                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    aVar.onUrlResolved(consumer);
                    a.this.e.set(null);
                }

                public final void onError() {
                    Logger b2 = a.this.d;
                    LogDomain logDomain = LogDomain.VAST;
                    StringBuilder sb = new StringBuilder("Seems to be an invalid URL: ");
                    sb.append(str);
                    b2.error(logDomain, sb.toString(), new Object[0]);
                    a.this.e.set(null);
                }
            });
            this.e.set(handleClickThroughUrl);
            handleClickThroughUrl.start();
        }
    }
}
