package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;

public final class h extends g {

    /* renamed from: a reason: collision with root package name */
    private final long f3792a;
    private final long b;

    h(long j, long j2) {
        this.f3792a = j;
        this.b = j2;
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j, @NonNull x xVar) {
        if (j >= this.f3792a && j < this.b) {
            xVar.a();
        }
    }
}
