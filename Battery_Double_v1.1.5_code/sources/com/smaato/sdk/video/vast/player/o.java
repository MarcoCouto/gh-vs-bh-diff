package com.smaato.sdk.video.vast.player;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTracker;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.tracker.VisibilityTrackerListener;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Listener;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.player.a.C0083a;
import com.smaato.sdk.video.vast.widget.e;
import com.smaato.sdk.video.vast.widget.element.g;
import com.smaato.sdk.video.vast.widget.element.g.a;
import java.lang.ref.WeakReference;

public class o implements a, a {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3801a;
    /* access modifiers changed from: private */
    @NonNull
    public final m b;
    @NonNull
    private final VisibilityTrackerCreator c;
    @NonNull
    private final v d;
    @NonNull
    private final g e;
    @NonNull
    private final g f;
    @NonNull
    private final StateMachine<i, j> g;
    @NonNull
    private WeakReference<e> h = new WeakReference<>(null);
    @NonNull
    private final a i = this;
    @NonNull
    private final a j = this;
    @Nullable
    private VisibilityTracker k;
    /* access modifiers changed from: private */
    public boolean l;
    @NonNull
    private final Listener<j> m = new Listener() {
        public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
            o.this.a((j) obj, (j) obj2, metadata);
        }
    };
    @NonNull
    private final a n = new a() {
        /* access modifiers changed from: private */
        public /* synthetic */ void a(Consumer consumer) {
            o.a(o.this, consumer);
        }

        public final void a(@Nullable String str) {
            o.this.b.a(str, (C0083a) new C0083a() {
                public final void onUrlResolved(Consumer consumer) {
                    AnonymousClass1.this.a(consumer);
                }
            });
        }

        public final void d() {
            o.this.f3801a.debug(LogDomain.VAST, "onCompanionRendered", new Object[0]);
            o.this.b.l();
        }

        public final void a(int i) {
            o.this.f3801a.debug(LogDomain.VAST, "onCompanionError", new Object[0]);
            o.this.b.c(i);
            o.this.l = true;
        }

        public final void e() {
            o.this.b.b();
            o.this.s();
        }
    };

    /* access modifiers changed from: private */
    public /* synthetic */ void a(j jVar, j jVar2, Metadata metadata) {
        a(jVar2);
    }

    o(@NonNull Logger logger, @NonNull m mVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull g gVar, @NonNull g gVar2, @NonNull v vVar, @NonNull StateMachine<i, j> stateMachine) {
        this.f3801a = (Logger) Objects.requireNonNull(logger);
        this.b = (m) Objects.requireNonNull(mVar);
        this.c = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.f = (g) Objects.requireNonNull(gVar);
        this.e = (g) Objects.requireNonNull(gVar2);
        this.d = (v) Objects.requireNonNull(vVar);
        this.g = (StateMachine) Objects.requireNonNull(stateMachine);
        this.d.a(this.j);
        this.f.a(this.n);
        this.e.a(this.i);
        this.g.addListener(this.m);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void t() {
        this.g.onEvent(i.CLICKED);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void c(@NonNull Consumer<Context> consumer) {
        Threads.runOnUi(new Runnable(consumer) {
            private final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                o.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Consumer consumer) {
        Objects.onNotNull(this.h.get(), new Consumer() {
            public final void accept(Object obj) {
                Consumer.this.accept(((e) obj).getContext());
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final m a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull e eVar) {
        c();
        this.h = new WeakReference<>(eVar);
        eVar.a().a(this.e);
        eVar.b().a(this.f);
        VisibilityTrackerCreator visibilityTrackerCreator = this.c;
        m mVar = this.b;
        mVar.getClass();
        this.k = visibilityTrackerCreator.createTracker(eVar, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                m.this.a();
            }
        });
        a((j) this.g.getCurrentState());
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.g.onEvent(i.CLOSE_BUTTON_CLICKED);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        r();
        Objects.onNotNull(this.h.get(), new Consumer() {
            public final void accept(Object obj) {
                o.this.b((e) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(e eVar) {
        this.h.clear();
    }

    private void a(@NonNull j jVar) {
        if (!this.l || jVar != j.SHOW_COMPANION) {
            switch (jVar) {
                case SHOW_VIDEO:
                    q();
                    return;
                case SHOW_COMPANION:
                    p();
                    return;
                case CLOSE_PLAYER:
                    s();
                    return;
                default:
                    Logger logger = this.f3801a;
                    LogDomain logDomain = LogDomain.VAST;
                    StringBuilder sb = new StringBuilder("Unknown state for VastVideoPlayer: ");
                    sb.append(jVar);
                    logger.error(logDomain, sb.toString(), new Object[0]);
                    s();
                    return;
            }
        } else {
            s();
        }
    }

    private void o() {
        this.d.i();
        c();
    }

    private void p() {
        e eVar = (e) this.h.get();
        if (eVar != null) {
            eVar.e();
            eVar.d();
        }
    }

    private void q() {
        x xVar;
        e eVar = (e) this.h.get();
        if (eVar == null) {
            xVar = null;
        } else {
            xVar = eVar.c();
        }
        v vVar = this.d;
        vVar.getClass();
        Objects.onNotNull(xVar, new Consumer() {
            public final void accept(Object obj) {
                v.this.a((x) obj);
            }
        });
    }

    private void r() {
        if (this.k != null) {
            this.k.destroy();
        }
    }

    /* access modifiers changed from: private */
    public void s() {
        this.b.c();
        o();
    }

    static /* synthetic */ void a(o oVar, Consumer consumer) {
        oVar.c(consumer);
        Threads.runOnUi(new Runnable() {
            public final void run() {
                o.this.t();
            }
        });
    }

    public void a(String str) {
        this.b.b(str, new C0083a() {
            public final void onUrlResolved(Consumer consumer) {
                o.this.c(consumer);
            }
        });
    }

    public void d() {
        this.f3801a.debug(LogDomain.VAST, "onIconRendered", new Object[0]);
        this.b.m();
    }

    public void a(int i2) {
        this.f3801a.debug(LogDomain.VAST, "onIconError", new Object[0]);
        this.b.b(i2);
    }

    public void e() {
        this.b.b();
        s();
    }

    public void f() {
        this.f3801a.info(LogDomain.VAST, "VAST video has started", new Object[0]);
        this.b.d();
    }

    public void g() {
        this.f3801a.debug(LogDomain.VAST, "onVideoImpression", new Object[0]);
        this.b.e();
    }

    public void a(long j2, long j3) {
        this.b.a(j2, j3);
    }

    public void h() {
        this.b.a((C0083a) new C0083a() {
            public final void onUrlResolved(Consumer consumer) {
                o.this.d(consumer);
            }
        });
    }

    public void i() {
        this.f3801a.debug(LogDomain.VAST, "onVideoSkipped", new Object[0]);
        this.b.f();
        this.g.onEvent(i.VIDEO_SKIPPED);
    }

    public void j() {
        this.f3801a.debug(LogDomain.VAST, "onMuteClicked", new Object[0]);
        this.b.g();
    }

    public void k() {
        this.f3801a.debug(LogDomain.VAST, "onUnmuteClicked", new Object[0]);
        this.b.h();
    }

    public void l() {
        this.f3801a.debug(LogDomain.VAST, "onVideoCompleted", new Object[0]);
        this.b.i();
        this.g.onEvent(i.VIDEO_COMPLETED);
    }

    public void b(int i2) {
        this.f3801a.error(LogDomain.VAST, "onVideoError", new Object[0]);
        this.b.a(400);
        this.g.onEvent(i.ERROR);
    }

    public void m() {
        this.f3801a.debug(LogDomain.VAST, "onVideoPaused", new Object[0]);
        this.b.j();
    }

    public void n() {
        this.f3801a.debug(LogDomain.VAST, "onVideoResumed", new Object[0]);
        this.b.k();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Consumer consumer) {
        a(this, consumer);
    }
}
