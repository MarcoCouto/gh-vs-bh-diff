package com.smaato.sdk.video.vast.player.system;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.video.utils.c;
import com.smaato.sdk.video.utils.c.a;
import com.smaato.sdk.video.vast.player.d;
import com.smaato.sdk.video.vast.player.e;
import java.util.Arrays;
import java.util.Collections;

public final class f implements ClassFactory<c> {
    @NonNull
    public final /* synthetic */ Object get(@NonNull DiConstructor diConstructor) {
        a aVar = new a();
        aVar.a(e.SET_DATA_SOURCE, Collections.singletonList(d.IDLE)).a(e.PREPARE_ASYNC, Arrays.asList(new d[]{d.INITIALIZED, d.STOPPED})).a(e.ON_PREPARED, Collections.singletonList(d.PREPARING)).a(e.PAUSE, Arrays.asList(new d[]{d.STARTED, d.RESUMED})).a(e.START, Arrays.asList(new d[]{d.PREPARED, d.PAUSED, d.PLAYBACK_COMPLETED})).a(e.STOP, Arrays.asList(new d[]{d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.PLAYBACK_COMPLETED})).a(e.ON_COMPLETE, Arrays.asList(new d[]{d.STARTED, d.RESUMED}));
        aVar.a(e.RELEASE, Arrays.asList(new d[]{d.IDLE, d.INITIALIZED, d.PREPARING, d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.STOPPED, d.PLAYBACK_COMPLETED, d.ERROR}));
        aVar.a(e.RESET, Arrays.asList(new d[]{d.INITIALIZED, d.PREPARING, d.PREPARED, d.STARTED, d.RESUMED, d.PAUSED, d.STOPPED, d.PLAYBACK_COMPLETED, d.ERROR}));
        return aVar.a();
    }
}
