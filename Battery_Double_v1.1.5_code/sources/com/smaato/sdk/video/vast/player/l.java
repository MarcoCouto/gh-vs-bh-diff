package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.tracking.f;

public class l {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final r f3797a;
    @NonNull
    private final n b;
    @NonNull
    private final p c;

    l(@NonNull r rVar, @NonNull n nVar, @NonNull p pVar) {
        this.f3797a = (r) Objects.requireNonNull(rVar);
        this.b = (n) Objects.requireNonNull(nVar);
        this.c = (p) Objects.requireNonNull(pVar);
    }

    public final void a(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull aa aaVar, @NonNull f fVar, @NonNull b<Either<k, Exception>> bVar, boolean z, boolean z2) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(somaApiContext);
        Objects.requireNonNull(aaVar);
        Objects.requireNonNull(fVar);
        Objects.requireNonNull(bVar);
        SomaApiContext somaApiContext2 = somaApiContext;
        Logger logger2 = logger;
        b<Either<k, Exception>> bVar2 = bVar;
        Logger logger3 = logger;
        aa aaVar2 = aaVar;
        this.c.a(logger3, somaApiContext2, aaVar2, this.b.a(logger, aaVar, somaApiContext2, fVar, z2), fVar, new b(logger, bVar) {
            private final /* synthetic */ Logger f$1;
            private final /* synthetic */ b f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                l.this.a(this.f$1, this.f$2, (Either) obj);
            }
        }, z);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, b bVar, Either either) {
        Exception exc = (Exception) either.right();
        if (exc != null) {
            bVar.accept(Either.right(exc));
        } else {
            bVar.accept(Either.left(new k((o) Objects.requireNonNull(either.left()), this.f3797a)));
        }
    }
}
