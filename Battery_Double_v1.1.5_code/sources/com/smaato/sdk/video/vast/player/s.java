package com.smaato.sdk.video.vast.player;

import android.view.Surface;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.player.exception.f;

public interface s {

    public interface a {
        void a();

        void b();

        void c();

        void d();

        void e();

        void f();

        void g();
    }

    public interface b {
        void onVolumeChanged(float f);
    }

    public interface c {
        void a(@NonNull s sVar);

        void a(@NonNull s sVar, @NonNull f fVar);

        void b(@NonNull s sVar);
    }

    void a();

    void a(@FloatRange(from = 0.0d, to = 1.0d) float f);

    void a(@Nullable Surface surface);

    void a(@Nullable a aVar);

    void a(@Nullable b bVar);

    void a(@Nullable c cVar);

    void a(@NonNull String str);

    void b();

    void c();

    void d();

    void e();

    @FloatRange(from = 0.0d, to = 1.0d)
    float f();

    long g();

    long h();
}
