package com.smaato.sdk.video.vast.player.system;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.view.Surface;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Metadata.Builder;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Listener;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.utils.c;
import com.smaato.sdk.video.vast.player.d;
import com.smaato.sdk.video.vast.player.e;
import com.smaato.sdk.video.vast.player.exception.f;
import com.smaato.sdk.video.vast.player.s;
import com.smaato.sdk.video.vast.player.s.a;
import com.smaato.sdk.video.vast.player.s.b;
import java.io.IOException;

public final class b implements s {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final MediaPlayer f3806a;
    @NonNull
    private final StateMachine<e, d> b;
    @NonNull
    private final c<com.smaato.sdk.video.vast.player.c, d> c;
    @NonNull
    private final c<e, d> d;
    @NonNull
    private final Logger e;
    @NonNull
    private Context f;
    @Nullable
    private s.c g;
    @Nullable
    private a h;
    @Nullable
    private com.smaato.sdk.video.vast.player.s.b i;
    private float j = -1.0f;

    b(@NonNull Context context, @NonNull MediaPlayer mediaPlayer, @NonNull StateMachine<e, d> stateMachine, @NonNull c<com.smaato.sdk.video.vast.player.c, d> cVar, @NonNull c<e, d> cVar2, @NonNull Logger logger) {
        this.f = (Context) Objects.requireNonNull(context, "Parameter context should not be null for SystemMediaPlayer::new");
        this.f3806a = (MediaPlayer) Objects.requireNonNull(mediaPlayer, "Parameter mediaPlayer should not be null for SystemMediaPlayer::new");
        this.b = (StateMachine) Objects.requireNonNull(stateMachine, "Parameter mediaPlayerStatMachine should not be null for SystemMediaPlayer::new");
        this.c = (c) Objects.requireNonNull(cVar, "Parameter mediaPlayerActionsValidator should not be null for SystemMediaPlayer::new");
        this.d = (c) Objects.requireNonNull(cVar2, "Parameter mediaPlayerTransitionsValidator should not be null for SystemMediaPlayer::new");
        this.e = (Logger) Objects.requireNonNull(logger, "Parameter logger should not be null for SystemMediaPlayer::new");
        mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
            public final void onCompletion(MediaPlayer mediaPlayer) {
                b.this.b(mediaPlayer);
            }
        });
        mediaPlayer.setOnErrorListener(new OnErrorListener() {
            public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                return b.this.b(mediaPlayer, i, i2);
            }
        });
        mediaPlayer.setOnInfoListener(new OnInfoListener() {
            public final boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                return b.this.a(mediaPlayer, i, i2);
            }
        });
        mediaPlayer.setOnPreparedListener(new OnPreparedListener() {
            public final void onPrepared(MediaPlayer mediaPlayer) {
                b.this.c(mediaPlayer);
            }
        });
        mediaPlayer.setOnSeekCompleteListener(new OnSeekCompleteListener() {
            public final void onSeekComplete(MediaPlayer mediaPlayer) {
                b.this.a(mediaPlayer);
            }
        });
        stateMachine.addListener(new Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                b.this.a((d) obj, (d) obj2, metadata);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull MediaPlayer mediaPlayer) {
        a(com.smaato.sdk.video.vast.player.c.SEEK_TO);
    }

    /* access modifiers changed from: private */
    public void b(@NonNull MediaPlayer mediaPlayer) {
        if (a(e.ON_COMPLETE)) {
            this.b.onEvent(e.ON_COMPLETE);
        } else {
            this.b.onEvent(e.ON_ERROR);
        }
    }

    /* access modifiers changed from: private */
    public void c(@NonNull MediaPlayer mediaPlayer) {
        if (a(e.ON_PREPARED)) {
            this.b.onEvent(e.ON_PREPARED);
        } else {
            this.b.onEvent(e.ON_ERROR);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(@NonNull MediaPlayer mediaPlayer, int i2, int i3) {
        this.e.info(LogDomain.VAST, "MediaPlayer Info: [what: %d, extra: %d]; For more details check android.media.MediaPlayer info codes", Integer.valueOf(i2), Integer.valueOf(i3));
        return false;
    }

    /* access modifiers changed from: private */
    public boolean b(@NonNull MediaPlayer mediaPlayer, int i2, int i3) {
        this.e.error(LogDomain.VAST, "MediaPlayer Error: [what: %d, extra: %d]; For more details check android.media.MediaPlayer error codes", Integer.valueOf(i2), Integer.valueOf(i3));
        this.b.onEvent(e.ON_ERROR, new Builder().putInt("what", i2).putInt("extra", i3).build());
        return true;
    }

    /* access modifiers changed from: private */
    public void a(@NonNull d dVar, @NonNull d dVar2, @Nullable Metadata metadata) {
        switch (dVar2) {
            case IDLE:
                if (this.h != null) {
                    this.h.f();
                    return;
                }
                break;
            case INITIALIZED:
                if (this.g != null) {
                    this.g.b(this);
                    return;
                }
                break;
            case PREPARING:
                if (this.g != null) {
                    return;
                }
                break;
            case PREPARED:
                if (this.g != null) {
                    this.g.a(this);
                    return;
                }
                break;
            case STARTED:
                if (this.h != null) {
                    this.h.a();
                    return;
                }
                break;
            case RESUMED:
                if (this.h != null) {
                    this.h.b();
                    return;
                }
                break;
            case PAUSED:
                if (this.h != null) {
                    this.h.c();
                    return;
                }
                break;
            case STOPPED:
                if (this.h != null) {
                    this.h.d();
                    return;
                }
                break;
            case PLAYBACK_COMPLETED:
                if (this.h != null) {
                    this.h.e();
                    return;
                }
                break;
            case ERROR:
                f a2 = a.a(metadata);
                if (this.h != null) {
                    this.h.g();
                }
                if (this.g != null) {
                    this.g.a(this, a2);
                    return;
                }
                break;
            case END:
                if (this.h != null) {
                    return;
                }
                break;
            default:
                throw new IllegalArgumentException(String.format("Unexpected MediaPlayerState: %s", new Object[]{dVar2}));
        }
    }

    public final void a(@NonNull String str) {
        if (a(e.SET_DATA_SOURCE)) {
            try {
                this.f3806a.setDataSource(str);
                this.b.onEvent(e.SET_DATA_SOURCE);
            } catch (IOException | IllegalArgumentException | SecurityException e2) {
                this.e.error(LogDomain.VAST, "Unable to set DataSource path:[%s] to MediaPlayer. Exception %s", str, e2);
                this.b.onEvent(e.ON_ERROR);
            }
        }
    }

    public final void d() {
        if (a(e.PREPARE_ASYNC)) {
            try {
                this.b.onEvent(e.PREPARE_ASYNC);
                this.f3806a.prepare();
            } catch (IOException e2) {
                this.e.error(LogDomain.VAST, "Unable to prepare DataSource for MediaPlayer. Exception %s", e2);
                this.b.onEvent(e.ON_ERROR);
            }
        }
    }

    public final void a() {
        if (a(e.START)) {
            this.f3806a.start();
            this.b.onEvent(e.START);
        }
    }

    public final void b() {
        if (a(e.PAUSE)) {
            this.f3806a.pause();
            this.b.onEvent(e.PAUSE);
        }
    }

    public final void c() {
        if (a(e.STOP)) {
            this.f3806a.stop();
            this.b.onEvent(e.STOP);
        }
    }

    public final void e() {
        if (a(e.RELEASE)) {
            this.f3806a.release();
            this.f3806a.setOnCompletionListener(null);
            this.f3806a.setOnErrorListener(null);
            this.f3806a.setOnInfoListener(null);
            this.f3806a.setOnPreparedListener(null);
            this.f3806a.setOnSeekCompleteListener(null);
            this.i = null;
            this.b.onEvent(e.RELEASE);
            this.b.deleteListeners();
        }
    }

    public final void a(@Nullable s.c cVar) {
        this.g = cVar;
    }

    public final void a(@Nullable a aVar) {
        this.h = aVar;
    }

    public final void a(@Nullable Surface surface) {
        if (a(com.smaato.sdk.video.vast.player.c.SET_SURFACE)) {
            this.f3806a.setSurface(surface);
        }
    }

    public final void a(@Nullable com.smaato.sdk.video.vast.player.s.b bVar) {
        this.i = bVar;
    }

    public final void a(@FloatRange(from = 0.0d, to = 1.0d) float f2) {
        if ((Math.abs(f2 - this.j) > 0.0f) && a(com.smaato.sdk.video.vast.player.c.SET_VOLUME)) {
            this.f3806a.setVolume(f2, f2);
            this.j = f2;
            Objects.onNotNull(this.i, new Consumer() {
                public final void accept(Object obj) {
                    b.this.b((b) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(com.smaato.sdk.video.vast.player.s.b bVar) {
        bVar.onVolumeChanged(this.j);
    }

    public final float f() {
        return this.j;
    }

    public final long g() {
        if (a(com.smaato.sdk.video.vast.player.c.GET_CURRENT_POSITION)) {
            return (long) this.f3806a.getCurrentPosition();
        }
        return 0;
    }

    public final long h() {
        if (a(com.smaato.sdk.video.vast.player.c.GET_DURATION)) {
            return (long) this.f3806a.getDuration();
        }
        return 0;
    }

    private boolean a(@NonNull com.smaato.sdk.video.vast.player.c cVar) {
        d dVar = (d) this.b.getCurrentState();
        if (this.c.a(cVar, dVar)) {
            return true;
        }
        this.e.error(LogDomain.VAST, "Invalid MediaPlayer state: %s, for action: %s ", dVar, cVar);
        return false;
    }

    private boolean a(@NonNull e eVar) {
        d dVar = (d) this.b.getCurrentState();
        if (this.d.a(eVar, dVar)) {
            return true;
        }
        this.e.error(LogDomain.VAST, "Invalid MediaPlayer state: %s, for transition: %s ", dVar, eVar);
        return false;
    }
}
