package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.model.x;
import com.smaato.sdk.video.vast.tracking.f;
import com.smaato.sdk.video.vast.widget.companion.c;
import com.smaato.sdk.video.vast.widget.element.g;

class p {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final VisibilityTrackerCreator f3804a;
    @NonNull
    private final w b;
    @NonNull
    private final c c;
    @NonNull
    private final com.smaato.sdk.video.vast.widget.icon.c d;
    @NonNull
    private final q e;

    p(@NonNull w wVar, @NonNull c cVar, @NonNull com.smaato.sdk.video.vast.widget.icon.c cVar2, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull q qVar) {
        this.b = (w) Objects.requireNonNull(wVar);
        this.c = (c) Objects.requireNonNull(cVar);
        this.d = (com.smaato.sdk.video.vast.widget.icon.c) Objects.requireNonNull(cVar2);
        this.f3804a = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.e = (q) Objects.requireNonNull(qVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull aa aaVar, @NonNull m mVar, @NonNull f fVar, @NonNull b<Either<o, Exception>> bVar, boolean z) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(somaApiContext);
        Objects.requireNonNull(mVar);
        Objects.requireNonNull(bVar);
        w wVar = this.b;
        aa aaVar2 = aaVar;
        x xVar = aaVar2.e;
        $$Lambda$p$EsWM0XkCbx2olvYcvmD950Nb40 r0 = new b(logger, somaApiContext, aaVar2, mVar, bVar) {
            private final /* synthetic */ Logger f$1;
            private final /* synthetic */ SomaApiContext f$2;
            private final /* synthetic */ aa f$3;
            private final /* synthetic */ m f$4;
            private final /* synthetic */ b f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            public final void accept(Object obj) {
                p.this.a(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, (Either) obj);
            }
        };
        wVar.a(logger, xVar, fVar, (b<Either<v, Exception>>) r0, z);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, SomaApiContext somaApiContext, aa aaVar, m mVar, b bVar, Either either) {
        Exception exc = (Exception) either.right();
        if (exc != null) {
            bVar.accept(Either.right(exc));
            return;
        }
        v vVar = (v) Objects.requireNonNull(either.left());
        g a2 = this.d.a(logger, aaVar.e, somaApiContext);
        Logger logger2 = logger;
        m mVar2 = mVar;
        o oVar = new o(logger2, mVar2, this.f3804a, this.c.a(logger, aaVar, somaApiContext), a2, vVar, this.e.a(aaVar));
        bVar.accept(Either.left(oVar));
    }
}
