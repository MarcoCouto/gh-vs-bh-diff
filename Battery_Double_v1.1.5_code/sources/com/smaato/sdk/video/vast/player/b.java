package com.smaato.sdk.video.vast.player;

import android.app.Application;
import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.ad.a;
import com.smaato.sdk.video.vast.parser.ae;
import com.smaato.sdk.video.vast.player.system.d;
import com.smaato.sdk.video.vast.tracking.e;
import com.smaato.sdk.video.vast.tracking.h;
import com.smaato.sdk.video.vast.widget.c;
import com.smaato.sdk.video.vast.widget.f;

public class b {

    /* renamed from: a reason: collision with root package name */
    public final int f3787a;
    public final int b;

    @NonNull
    public static DiRegistry a() {
        return DiRegistry.of($$Lambda$b$JwltmvpKfheiolwQszgKpeIGwLg.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void f(DiRegistry diRegistry) {
        diRegistry.registerFactory(com.smaato.sdk.video.vast.tracking.b.class, $$Lambda$b$Xmoo99VpMF2GxLZANFs6ELZMMM.INSTANCE);
        diRegistry.registerFactory(h.class, $$Lambda$b$axN33QASEStiQPlw6a5swEcKrs.INSTANCE);
        diRegistry.registerFactory(e.class, $$Lambda$b$bzpn3eX1SICzywhkIe3e4XN73k.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.tracking.b u(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.tracking.b(new ae(), new com.smaato.sdk.video.utils.b(), new a());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ h t(DiConstructor diConstructor) {
        return new h(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (com.smaato.sdk.video.vast.tracking.b) diConstructor.get(com.smaato.sdk.video.vast.tracking.b.class), DiNetworkLayer.getNetworkingExecutorServiceFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ e s(DiConstructor diConstructor) {
        return new e(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (com.smaato.sdk.video.vast.tracking.b) diConstructor.get(com.smaato.sdk.video.vast.tracking.b.class), DiNetworkLayer.getNetworkingExecutorServiceFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void e(DiRegistry diRegistry) {
        diRegistry.registerFactory(l.class, $$Lambda$b$B73WwzL7qh135WXrUFLY7PSd8Rw.INSTANCE);
        diRegistry.registerFactory(r.class, $$Lambda$b$ujZfne8SKIJf0P9sV4A93xXMnHI.INSTANCE);
        diRegistry.registerFactory(f.class, $$Lambda$b$oq2zT0Df4ZazHQWRcMzS7zLNQek.INSTANCE);
        diRegistry.registerFactory(n.class, $$Lambda$b$IMsSCKFVyyXbd9Z01ylUQq4XnKE.INSTANCE);
        diRegistry.registerFactory(p.class, $$Lambda$b$ukLpeirggcyac0EfjKFkTW74kA4.INSTANCE);
        diRegistry.registerFactory(q.class, $$Lambda$b$U7IPYNcINKqQghHz0ch923yzYYQ.INSTANCE);
        diRegistry.registerFactory(w.class, $$Lambda$b$XN5ZHD2cUEyt3voVf0S2EQl0G4g.INSTANCE);
        diRegistry.registerFactory(u.class, $$Lambda$b$EdnLWOccJ0UyvrT6L0qFTzGRgxc.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.video.vast.utils.a.class, $$Lambda$b$bL46vFyF8eIZrLjkJVtEpOjS9To.INSTANCE);
        diRegistry.addFrom(DiRegistry.of($$Lambda$b$PYrMbC8sa8tfnWIfBB90nrJaWpo.INSTANCE));
        diRegistry.addFrom(DiRegistry.of($$Lambda$b$wRd2ccPka1VHuoKYBQ_aYjUDWSs.INSTANCE));
        diRegistry.addFrom(DiRegistry.of($$Lambda$b$A6gGJPGGIZNGo1zlNaNj4PfLPks.INSTANCE));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ l r(DiConstructor diConstructor) {
        return new l((r) diConstructor.get(r.class), (n) diConstructor.get(n.class), (p) diConstructor.get(p.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ r q(DiConstructor diConstructor) {
        return new r();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f p(DiConstructor diConstructor) {
        return new c();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ n o(DiConstructor diConstructor) {
        return new n((LinkResolver) diConstructor.get(LinkResolver.class), (h) diConstructor.get(h.class), (e) diConstructor.get(e.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ p n(DiConstructor diConstructor) {
        p pVar = new p((w) diConstructor.get(w.class), (com.smaato.sdk.video.vast.widget.companion.c) diConstructor.get(com.smaato.sdk.video.vast.widget.companion.c.class), (com.smaato.sdk.video.vast.widget.icon.c) diConstructor.get(com.smaato.sdk.video.vast.widget.icon.c.class), (VisibilityTrackerCreator) diConstructor.get("VideoModuleInterface", VisibilityTrackerCreator.class), (q) diConstructor.get(q.class));
        return pVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ q m(DiConstructor diConstructor) {
        return new q(j.SHOW_VIDEO);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ w l(DiConstructor diConstructor) {
        w wVar = new w((u) diConstructor.get(u.class), (VisibilityTrackerCreator) diConstructor.get("VideoModuleInterface", VisibilityTrackerCreator.class), (f) diConstructor.get(f.class), true, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, MTGInterstitialActivity.WEB_LOAD_TIME);
        return wVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ u k(DiConstructor diConstructor) {
        return new u((t) diConstructor.get(t.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.utils.a j(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.utils.a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(DiRegistry diRegistry) {
        diRegistry.registerFactory(com.smaato.sdk.video.vast.widget.companion.a.class, $$Lambda$b$zkViNC8iNnvqvgr_xEu2yg2sSi0.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.video.vast.widget.companion.c.class, $$Lambda$b$MWJsRzsbBcerfrrgLQSjiQNjNd4.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.widget.companion.a i(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.widget.companion.a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.widget.companion.c h(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.widget.companion.c((com.smaato.sdk.video.vast.utils.a) diConstructor.get(com.smaato.sdk.video.vast.utils.a.class), (VisibilityTrackerCreator) diConstructor.get("VAST_ELEMENT_VISIBILITY", VisibilityTrackerCreator.class), (com.smaato.sdk.video.vast.browser.a) diConstructor.get(com.smaato.sdk.video.vast.browser.a.class), (com.smaato.sdk.video.vast.widget.companion.a) diConstructor.get(com.smaato.sdk.video.vast.widget.companion.a.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(DiRegistry diRegistry) {
        diRegistry.registerFactory(com.smaato.sdk.video.vast.widget.icon.a.class, $$Lambda$b$H3YkZP0QPmjVjf7pq7aB9UBhlRk.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.video.vast.widget.icon.c.class, $$Lambda$b$41RcdrIoyIjboRaAiAvdM73D7c.INSTANCE);
        diRegistry.registerFactory("ICON_ANIMATION_HELPER", com.smaato.sdk.video.utils.a.class, $$Lambda$b$YHGoxbp9qx93JMT9Xe2duZHvaPQ.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.widget.icon.a g(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.widget.icon.a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.widget.icon.c f(DiConstructor diConstructor) {
        com.smaato.sdk.video.vast.widget.icon.c cVar = new com.smaato.sdk.video.vast.widget.icon.c((com.smaato.sdk.video.vast.utils.a) diConstructor.get(com.smaato.sdk.video.vast.utils.a.class), (VisibilityTrackerCreator) diConstructor.get("VAST_ELEMENT_VISIBILITY", VisibilityTrackerCreator.class), (com.smaato.sdk.video.vast.browser.a) diConstructor.get(com.smaato.sdk.video.vast.browser.a.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class), (com.smaato.sdk.video.utils.a) diConstructor.get("ICON_ANIMATION_HELPER", com.smaato.sdk.video.utils.a.class), (com.smaato.sdk.video.vast.widget.icon.a) diConstructor.get(com.smaato.sdk.video.vast.widget.icon.a.class));
        return cVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.utils.a e(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.utils.a(300);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f d(DiConstructor diConstructor) {
        return new f(Threads.newUiHandler());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory("VAST_ELEMENT_VISIBILITY", VisibilityPrivateConfig.class, $$Lambda$b$7peNgeDS23jXtjphn3pmRqFFNms.INSTANCE);
        diRegistry.registerFactory("VAST_ELEMENT_VISIBILITY", VisibilityTrackerCreator.class, $$Lambda$b$JwL3bkBTTRJ_YpnbIL2sexvK41s.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VisibilityTrackerCreator b(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get("VAST_ELEMENT_VISIBILITY", VisibilityPrivateConfig.class);
        VisibilityTrackerCreator visibilityTrackerCreator = new VisibilityTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), "VAST_ELEMENT_VISIBILITY");
        return visibilityTrackerCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(String str, String str2, DiRegistry diRegistry) {
        diRegistry.registerFactory(t.class, new ClassFactory(str, str2) {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final Object get(DiConstructor diConstructor) {
                return b.a(this.f$0, this.f$1, diConstructor);
            }
        });
        diRegistry.registerFactory(com.smaato.sdk.video.vast.player.system.e.class, $$Lambda$b$fYs7y_mh90vUnNsxfvyM7BFOZv8.INSTANCE);
        diRegistry.registerFactory(str, com.smaato.sdk.video.utils.c.class, new com.smaato.sdk.video.vast.player.system.c());
        diRegistry.registerFactory(str2, com.smaato.sdk.video.utils.c.class, new com.smaato.sdk.video.vast.player.system.f());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ t a(String str, String str2, DiConstructor diConstructor) {
        return new d((Context) diConstructor.get(Application.class), (com.smaato.sdk.video.vast.player.system.e) diConstructor.get(com.smaato.sdk.video.vast.player.system.e.class), (com.smaato.sdk.video.utils.c) diConstructor.get(str, com.smaato.sdk.video.utils.c.class), (com.smaato.sdk.video.utils.c) diConstructor.get(str2, com.smaato.sdk.video.utils.c.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.video.vast.player.system.e a(DiConstructor diConstructor) {
        return new com.smaato.sdk.video.vast.player.system.e(d.IDLE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void g(DiRegistry diRegistry) {
        diRegistry.addFrom(DiRegistry.of($$Lambda$b$TmeIE3iOjr69qGEESwgcFMIjGZU.INSTANCE));
        diRegistry.addFrom(DiRegistry.of($$Lambda$b$FrfMqkcE1HGBfEg718C9OF_upI.INSTANCE));
        diRegistry.addFrom(DiRegistry.of(new Consumer("VideoModuleInterfaceSystemMediaPlayerActionValidator", "VideoModuleInterfaceSystemMediaPlayerTransitionValidator") {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                b.a(this.f$0, this.f$1, (DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of($$Lambda$b$S_7L0nME_YGWnMRO0UNAtR78N8.INSTANCE));
    }

    public b(int i, int i2) {
        this.f3787a = i;
        this.b = i2;
    }
}
