package com.smaato.sdk.video.vast.player.system;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.video.vast.player.exception.b;
import com.smaato.sdk.video.vast.player.exception.c;
import com.smaato.sdk.video.vast.player.exception.d;
import com.smaato.sdk.video.vast.player.exception.e;
import com.smaato.sdk.video.vast.player.exception.f;

final class a {
    @NonNull
    static f a(@Nullable Metadata metadata) {
        Integer num = null;
        Integer num2 = metadata == null ? null : metadata.getInt("what");
        if (metadata != null) {
            num = metadata.getInt("extra");
        }
        if (num2 == null) {
            return new d();
        }
        if (num2.intValue() != 1) {
            return new d();
        }
        if (num == null) {
            return new d();
        }
        int intValue = num.intValue();
        if (intValue == -1010) {
            return new e();
        }
        if (intValue == -1007) {
            return new b();
        }
        if (intValue == -1004) {
            return new com.smaato.sdk.video.vast.player.exception.a();
        }
        if (intValue != -110) {
            return new d();
        }
        return new c();
    }
}
