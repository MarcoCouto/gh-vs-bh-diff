package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.ad.a;
import com.smaato.sdk.video.vast.build.h;
import com.smaato.sdk.video.vast.model.q;
import com.smaato.sdk.video.vast.parser.ae;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ae f3818a;
    @NonNull
    private final com.smaato.sdk.video.utils.b b;
    @NonNull
    private final a c;

    public b(@NonNull ae aeVar, @NonNull com.smaato.sdk.video.utils.b bVar, @NonNull a aVar) {
        this.f3818a = (ae) Objects.requireNonNull(aeVar, "Parameter uriUtils cannot be null for MacroInjector");
        this.b = (com.smaato.sdk.video.utils.b) Objects.requireNonNull(bVar, "Parameter dateFormatUtils cannot be null for MacroInjector");
        this.c = (a) Objects.requireNonNull(aVar, "Parameter randomUtils cannot be null for MacroInjector");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String a(@NonNull String str, @NonNull h hVar) {
        return str.replace("[ERRORCODE]", ae.b(String.valueOf(hVar.f3679a))).replace("[CONTENTPLAYHEAD]", ae.b(com.smaato.sdk.video.utils.b.a(hVar.c))).replace("[TIMESTAMP]", ae.b(this.b.a())).replace("[CACHEBUSTING]", ae.b(this.c.a())).replace("[ASSETURI]", hVar.b != null ? ae.b(hVar.b) : "");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String a(@NonNull q qVar, @NonNull a aVar) {
        return qVar.f3742a.replace("[CONTENTPLAYHEAD]", ae.b(com.smaato.sdk.video.utils.b.a(aVar.f3817a.longValue()))).replace("[TIMESTAMP]", ae.b(this.b.a())).replace("[CACHEBUSTING]", ae.b(this.c.a())).replace("[ASSETURI]", ae.b(aVar.b));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Set<String> a(@NonNull Collection<String> collection, @NonNull a aVar) {
        HashSet hashSet = new HashSet(collection.size());
        for (String replace : collection) {
            hashSet.add(replace.replace("[CONTENTPLAYHEAD]", ae.b(com.smaato.sdk.video.utils.b.a(aVar.f3817a.longValue()))).replace("[TIMESTAMP]", ae.b(this.b.a())).replace("[CACHEBUSTING]", ae.b(this.c.a())).replace("[ASSETURI]", ae.b(aVar.b)));
        }
        return hashSet;
    }
}
