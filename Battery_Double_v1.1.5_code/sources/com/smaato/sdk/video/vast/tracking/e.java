package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Iterables;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.model.s;
import com.smaato.sdk.video.vast.model.t;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.model.w;
import com.smaato.sdk.video.vast.model.x;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

public class e {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3822a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final b c;
    @NonNull
    private final ExecutorService d;
    @NonNull
    private final Function<s, String> e = $$Lambda$e$zWbKI5wI30oIy4NBhOof2TR0QBA.INSTANCE;

    public e(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull b bVar, @NonNull ExecutorService executorService) {
        this.f3822a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.c = (b) Objects.requireNonNull(bVar);
        this.d = (ExecutorService) Objects.requireNonNull(executorService);
    }

    @NonNull
    public final d a(@NonNull aa aaVar, @NonNull SomaApiContext somaApiContext) {
        Logger logger = this.f3822a;
        BeaconTracker beaconTracker = this.b;
        b bVar = this.c;
        HashMap hashMap = new HashMap();
        a(hashMap, t.SMAATO_IMPRESSION, Iterables.map(aaVar.f3697a, this.e));
        if (aaVar.k != null) {
            a(hashMap, t.SMAATO_VIEWABLE_IMPRESSION, aaVar.k.f3709a);
        }
        x xVar = aaVar.e;
        if (xVar.e != null) {
            a(hashMap, t.SMAATO_VIDEO_CLICK_TRACKING, Iterables.map(xVar.e.f3707a, this.e));
        }
        w wVar = xVar.h;
        if (wVar != null) {
            if (wVar.e != null) {
                a(hashMap, t.SMAATO_ICON_CLICK_TRACKING, Iterables.map(wVar.e.f3729a, this.e));
            }
            a(hashMap, t.SMAATO_ICON_VIEW_TRACKING, wVar.b);
        }
        u uVar = aaVar.f;
        if (uVar != null) {
            a(hashMap, t.SMAATO_COMPANION_CLICK_TRACKING, Iterables.map(uVar.c, this.e));
        }
        d dVar = new d(logger, somaApiContext, beaconTracker, bVar, new i(Collections.unmodifiableMap(hashMap)), this.d);
        return dVar;
    }

    private static void a(@NonNull Map<t, Set<String>> map, @NonNull t tVar, @NonNull Iterable<String> iterable) {
        if (!map.containsKey(tVar)) {
            map.put(tVar, Collections.unmodifiableSet(Sets.toSet(iterable)));
        } else {
            throw new IllegalArgumentException(String.format("beaconsEventsMap already contains %s event", new Object[]{tVar}));
        }
    }
}
