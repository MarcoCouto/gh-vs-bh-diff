package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    final Long f3817a;
    @NonNull
    final String b;

    public a(@NonNull String str, @NonNull Long l) {
        this.b = str;
        this.f3817a = l;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        if (!this.b.equals(aVar.b)) {
            return false;
        }
        return this.f3817a.equals(aVar.f3817a);
    }

    public final int hashCode() {
        return (this.b.hashCode() * 31) + this.f3817a.hashCode();
    }
}
