package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.aa;
import com.smaato.sdk.video.vast.model.q;
import com.smaato.sdk.video.vast.model.u;
import com.smaato.sdk.video.vast.model.v;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class h {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3826a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final b c;
    @NonNull
    private final ExecutorService d;

    public h(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull b bVar, @NonNull ExecutorService executorService) {
        this.f3826a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.c = (b) Objects.requireNonNull(bVar);
        this.d = (ExecutorService) Objects.requireNonNull(executorService);
    }

    private static void a(@NonNull Map<v, LinkedList<q>> map, @NonNull List<q> list) {
        for (q qVar : list) {
            if (!map.containsKey(qVar.b)) {
                map.put(qVar.b, new LinkedList());
            }
            Objects.onNotNull(map.get(qVar.b), new Consumer() {
                public final void accept(Object obj) {
                    ((LinkedList) obj).add(q.this);
                }
            });
        }
    }

    @NonNull
    public final g a(@NonNull aa aaVar, @NonNull SomaApiContext somaApiContext) {
        HashMap hashMap = new HashMap();
        a((Map<v, LinkedList<q>>) hashMap, aaVar.e.c);
        u uVar = aaVar.f;
        if (uVar != null) {
            a((Map<v, LinkedList<q>>) hashMap, uVar.d);
        }
        Map unmodifiableMap = Collections.unmodifiableMap(hashMap);
        g gVar = new g(this.f3826a, this.b, unmodifiableMap, c.a(unmodifiableMap, aaVar.e.f, this.f3826a), this.c, somaApiContext, this.d, aaVar.e.b.f3737a);
        return gVar;
    }
}
