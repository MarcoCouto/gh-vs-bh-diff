package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.t;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class i {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Map<t, Collection<String>> f3827a;
    @NonNull
    private final Set<t> b = Collections.synchronizedSet(new HashSet());

    i(@NonNull Map<t, Collection<String>> map) {
        this.f3827a = new HashMap((Map) Objects.requireNonNull(map));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Set<String> a(@NonNull t tVar) {
        if (this.b.contains(tVar)) {
            return Collections.emptySet();
        }
        if (this.f3827a.containsKey(tVar)) {
            Collection collection = (Collection) this.f3827a.get(tVar);
            if (collection != null) {
                return Collections.unmodifiableSet(new HashSet(collection));
            }
        }
        return Collections.emptySet();
    }

    /* access modifiers changed from: 0000 */
    public final void b(@NonNull t tVar) {
        this.b.add(tVar);
    }

    /* access modifiers changed from: 0000 */
    public final boolean c(@NonNull t tVar) {
        return this.b.contains(tVar);
    }
}
