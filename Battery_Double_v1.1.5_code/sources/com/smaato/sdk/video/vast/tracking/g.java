package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.video.vast.model.q;
import com.smaato.sdk.video.vast.model.v;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

public final class g {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3824a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final Map<v, List<q>> c;
    @NonNull
    private final Set<v> d = Collections.synchronizedSet(new HashSet());
    @NonNull
    private final c e;
    @NonNull
    private final String f;
    @NonNull
    private final b g;
    @NonNull
    private final SomaApiContext h;
    @NonNull
    private final ExecutorService i;

    g(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull Map<v, List<q>> map, @NonNull c cVar, @NonNull b bVar, @NonNull SomaApiContext somaApiContext, @NonNull ExecutorService executorService, @NonNull String str) {
        this.f3824a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.g = (b) Objects.requireNonNull(bVar);
        this.h = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.c = (Map) Objects.requireNonNull(map);
        this.i = (ExecutorService) Objects.requireNonNull(executorService);
        this.e = (c) Objects.requireNonNull(cVar);
        this.f = (String) Objects.requireNonNull(str);
    }

    public final void a(long j, long j2) {
        Objects.onNotNull(this.e.a(j, j2), new Consumer(j) {
            private final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                g.this.a(this.f$1, (Set) obj);
            }
        });
    }

    public final void a(@NonNull v vVar, long j) {
        Objects.onNotNull(this.c.get(vVar), new Consumer(j) {
            private final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                g.this.a(this.f$1, (List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Collection<q> collection, long j) {
        Set<q> retainToSet = Sets.retainToSet(collection, new Predicate() {
            public final boolean test(Object obj) {
                return g.this.a((q) obj);
            }
        });
        if (!retainToSet.isEmpty()) {
            for (q qVar : retainToSet) {
                this.e.a(qVar);
                this.d.add(qVar.b);
            }
            this.i.execute(new Runnable(retainToSet, j) {
                private final /* synthetic */ Set f$1;
                private final /* synthetic */ long f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    g.this.a(this.f$1, this.f$2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(q qVar) {
        return !qVar.b.o || !this.d.contains(qVar.b);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Set, code=java.util.Set<com.smaato.sdk.video.vast.model.q>, for r7v0, types: [java.util.Collection, java.util.Set, java.util.Set<com.smaato.sdk.video.vast.model.q>] */
    public /* synthetic */ void a(Set<q> set, long j) {
        for (final q qVar : set) {
            Threads.ensureNotMainThread();
            this.b.trackBeaconUrl(this.g.a(qVar, new a(this.f, Long.valueOf(j))), this.h, new Listener<Whatever, Exception>() {
                public final /* synthetic */ void onFailure(@NonNull Task task, @NonNull Object obj) {
                    g.this.f3824a.error(LogDomain.VAST, (Exception) obj, "Tracking Vast event failed with exception: %s", qVar.b);
                }

                public final /* synthetic */ void onSuccess(@NonNull Task task, @NonNull Object obj) {
                    g.this.f3824a.info(LogDomain.VAST, "Vast event was tracked successfully %s", qVar.b);
                }
            });
        }
    }
}
