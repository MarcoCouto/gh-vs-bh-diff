package com.smaato.sdk.video.vast.tracking;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.video.vast.model.q;
import com.smaato.sdk.video.vast.model.v;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final SparseArray<Set<q>> f3819a;
    @NonNull
    private final Set<q> b = Collections.synchronizedSet(new HashSet());

    private c(@NonNull SparseArray<Set<q>> sparseArray) {
        this.f3819a = sparseArray;
    }

    @NonNull
    static c a(@NonNull Map<v, List<q>> map, long j, @NonNull Logger logger) {
        SparseArray sparseArray = new SparseArray();
        for (v vVar : v.n) {
            Objects.onNotNull(map.get(vVar), new Consumer(j, logger, sparseArray) {
                private final /* synthetic */ long f$0;
                private final /* synthetic */ Logger f$1;
                private final /* synthetic */ SparseArray f$2;

                {
                    this.f$0 = r1;
                    this.f$1 = r3;
                    this.f$2 = r4;
                }

                public final void accept(Object obj) {
                    c.a(this.f$0, this.f$1, this.f$2, (List) obj);
                }
            });
        }
        return new c(sparseArray);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.smaato.sdk.video.vast.model.q>, for r7v0, types: [java.util.List, java.util.List<com.smaato.sdk.video.vast.model.q>] */
    public static /* synthetic */ void a(long j, Logger logger, SparseArray sparseArray, List<q> list) {
        for (q qVar : list) {
            int a2 = com.smaato.sdk.video.vast.utils.c.a(qVar.c, j, logger);
            if (a2 >= 0) {
                Set set = (Set) sparseArray.get(a2);
                if (set != null) {
                    set.add(qVar);
                } else {
                    HashSet hashSet = new HashSet();
                    hashSet.add(qVar);
                    sparseArray.append(a2, hashSet);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Set<q> a(long j, long j2) {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < this.f3819a.size(); i++) {
            if (((long) this.f3819a.keyAt(i)) <= (100 * j) / j2) {
                hashSet.addAll(Sets.retainToSet((Collection) this.f3819a.valueAt(i), new Predicate() {
                    public final boolean test(Object obj) {
                        return c.this.b((q) obj);
                    }
                }));
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean b(q qVar) {
        return !this.b.contains(qVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull q qVar) {
        this.b.add(qVar);
    }
}
