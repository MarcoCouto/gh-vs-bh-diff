package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.video.vast.model.t;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ExecutorService;

public final class d {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3820a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final i c;
    @NonNull
    private final SomaApiContext d;
    @NonNull
    private final b e;
    @NonNull
    private final ExecutorService f;

    d(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull BeaconTracker beaconTracker, @NonNull b bVar, @NonNull i iVar, @NonNull ExecutorService executorService) {
        this.f3820a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.e = (b) Objects.requireNonNull(bVar);
        this.c = (i) Objects.requireNonNull(iVar);
        this.d = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.f = (ExecutorService) Objects.requireNonNull(executorService);
    }

    public final void a(@NonNull t tVar, @NonNull a aVar) {
        if (!this.c.c(tVar)) {
            if (this.f.isShutdown()) {
                this.f3820a.error(LogDomain.VAST, "Attempt to trigger event: %s on a already shutdown beacon tracker", tVar);
                return;
            }
            this.f.execute(new Runnable(tVar, aVar) {
                private final /* synthetic */ t f$1;
                private final /* synthetic */ a f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    d.this.b(this.f$1, this.f$2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(final t tVar, a aVar) {
        Set set;
        Set a2 = this.c.a(tVar);
        if (!a2.isEmpty()) {
            set = Collections.unmodifiableSet(this.e.a((Collection<String>) a2, aVar));
        } else {
            set = Collections.emptySet();
        }
        if (!set.isEmpty()) {
            this.c.b(tVar);
            this.b.trackBeaconUrls(set, this.d, new Listener<Whatever, Exception>() {
                public final /* synthetic */ void onFailure(@NonNull Task task, @NonNull Object obj) {
                    d.this.f3820a.error(LogDomain.VAST, (Exception) obj, "Tracking Vast beacon failed with exception: %s", tVar);
                }

                public final /* synthetic */ void onSuccess(@NonNull Task task, @NonNull Object obj) {
                    d.this.f3820a.info(LogDomain.VAST, "Vast beacon was tracked successfully %s", tVar);
                }
            });
        }
    }
}
