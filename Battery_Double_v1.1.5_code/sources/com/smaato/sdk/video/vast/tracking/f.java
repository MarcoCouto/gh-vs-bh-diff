package com.smaato.sdk.video.vast.tracking;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.h;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class f {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Logger f3823a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final Set<String> c;
    @NonNull
    private final SomaApiContext d;
    @NonNull
    private final b e;

    public f(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull SomaApiContext somaApiContext, @NonNull b bVar, @NonNull Collection<String> collection) {
        this.f3823a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.d = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.e = (b) Objects.requireNonNull(bVar);
        this.c = new HashSet((Collection) Objects.requireNonNull(collection));
    }

    public final void a(@NonNull h hVar) {
        if (this.c.isEmpty()) {
            this.f3823a.info(LogDomain.VAST, "Wanted to track VastError [%d], but no beacon URLs available", Integer.valueOf(hVar.f3679a));
            return;
        }
        this.f3823a.info(LogDomain.VAST, "Tracking VastError [%d]", Integer.valueOf(hVar.f3679a));
        for (String str : this.c) {
            if (!TextUtils.isEmpty(str)) {
                this.b.trackBeaconUrl(this.e.a(str, hVar), this.d);
            }
        }
    }
}
