package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.af;
import com.smaato.sdk.video.vast.model.af.a;
import com.smaato.sdk.video.vast.model.s;
import java.util.ArrayList;
import java.util.List;

public final class aa implements ad<af> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3761a = {VastTagName.CLICK_THROUGH, VastTagName.CLICK_TRACKING, VastTagName.CUSTOM_CLICK};

    @NonNull
    public final p<af> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        aVar.a((List<s>) arrayList2);
        ArrayList arrayList3 = new ArrayList();
        aVar.b(arrayList3);
        String[] strArr = f3761a;
        $$Lambda$aa$xmhcJSIRcuX5lNJfFTDvOK7i4 r0 = new Consumer(aVar, arrayList, arrayList2, arrayList3) {
            private final /* synthetic */ a f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void accept(Object obj) {
                aa.a(r.this, this.f$1, this.f$2, this.f$3, this.f$4, (String) obj);
            }
        };
        rVar.a(strArr, (Consumer<String>) r0, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.VIDEO_CLICKS, new Exception("Unable to parse tags in CompanionAds", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, a aVar, List list, List list2, List list3, String str) {
        if (str.equalsIgnoreCase(VastTagName.CLICK_THROUGH)) {
            rVar.a(VastTagName.CLICK_THROUGH, (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    aa.a(a.this, this.f$1, (p) obj);
                }
            });
        } else if (str.equalsIgnoreCase(VastTagName.CLICK_TRACKING)) {
            rVar.a(VastTagName.CLICK_TRACKING, (b<p<Result>>) new b(list2, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    aa.b(this.f$0, this.f$1, (p) obj);
                }
            });
        } else {
            if (str.equalsIgnoreCase(VastTagName.CUSTOM_CLICK)) {
                rVar.a(VastTagName.CUSTOM_CLICK, (b<p<Result>>) new b(list3, list) {
                    private final /* synthetic */ List f$0;
                    private final /* synthetic */ List f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        aa.a(this.f$0, this.f$1, (p) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(a aVar, List list, p pVar) {
        aVar.a((s) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(List list, List list2, p pVar) {
        Result result = pVar.b;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((s) obj);
            }
        });
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        Result result = pVar.b;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((s) obj);
            }
        });
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
