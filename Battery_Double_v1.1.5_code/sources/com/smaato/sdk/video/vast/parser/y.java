package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.ad;
import com.smaato.sdk.video.vast.model.ad.a;
import java.util.ArrayList;
import java.util.List;

public final class y implements ad<ad> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3783a = {VastTagName.AD, "Error"};

    @NonNull
    public final p<ad> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        aVar.a((List<com.smaato.sdk.video.vast.model.a>) arrayList2);
        ArrayList arrayList3 = new ArrayList();
        aVar.b(arrayList3);
        aVar.getClass();
        $$Lambda$cr3z9y25XuUWpKcD9RbO2pfug5A r5 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList.getClass();
        rVar.a("version", (Consumer<String>) r5, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a(f3783a, (Consumer<String>) new Consumer(arrayList3, arrayList, arrayList2) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                y.a(r.this, this.f$1, this.f$2, this.f$3, (String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.VAST, new Exception("Unable to parse tags in Vast", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, List list, List list2, List list3, String str) {
        if (str.equalsIgnoreCase("Error")) {
            rVar.a((Consumer<String>) new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    y.a(this.f$0, (String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("Error", new Exception("Unable to parse Error in Vast", (Exception) obj)));
                }
            });
            return;
        }
        if (str.equalsIgnoreCase(VastTagName.AD)) {
            rVar.a(VastTagName.AD, (b<p<Result>>) new b(list3, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    y.a(this.f$0, this.f$1, (p) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, String str) {
        list.getClass();
        Objects.onNotNull(str, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((String) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        list.add(pVar.b);
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
