package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.a;
import com.smaato.sdk.video.vast.model.p;
import com.smaato.sdk.video.vast.model.p.b;
import java.util.ArrayList;
import java.util.List;

public final class t implements ad<p> {

    /* renamed from: a reason: collision with root package name */
    private static final a<String, b> f3779a = $$Lambda$t$vyH5d3dxr8mUgPsbgusBB3IZhis.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ b a(String str) throws Exception {
        return (b) Objects.requireNonNull(b.a(str));
    }

    @NonNull
    public final p<p> a(@NonNull r rVar) {
        p pVar;
        p.a aVar = new p.a();
        ArrayList arrayList = new ArrayList();
        String str = VastAttributes.CREATIVE_TYPE;
        a<String, b> aVar2 = f3779a;
        aVar.getClass();
        $$Lambda$lcoTuCEqGBDR_84iYBY4MB6NfI r4 = new Consumer() {
            public final void accept(Object obj) {
                p.a.this.a((b) obj);
            }
        };
        arrayList.getClass();
        r a2 = rVar.a(str, aVar2, r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        a2.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                p.a.this.a((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.STATIC_RESOURCE, new Exception("Unable to parse StaticResource uri", (Exception) obj)));
            }
        });
        try {
            pVar = aVar.a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            arrayList.add(o.a(VastTagName.STATIC_RESOURCE, e));
            pVar = null;
        }
        return new p.a().a(pVar).a((List<o>) arrayList).a();
    }
}
