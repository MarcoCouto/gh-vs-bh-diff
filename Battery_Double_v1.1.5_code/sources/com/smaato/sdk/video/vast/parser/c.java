package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.c.a;
import java.util.ArrayList;
import java.util.List;

public final class c implements ad<com.smaato.sdk.video.vast.model.c> {
    @NonNull
    public final p<com.smaato.sdk.video.vast.model.c> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        aVar.getClass();
        $$Lambda$weKheOIQRQuiyq6a9ME6em9ws r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = rVar.a("version", (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        a2.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.AD_SYSTEM, new Exception("Unable to parse AdServer name value", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }
}
