package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.ag;
import com.smaato.sdk.video.vast.model.ah;
import com.smaato.sdk.video.vast.model.ah.a;
import com.smaato.sdk.video.vast.model.c;
import com.smaato.sdk.video.vast.model.s;
import java.util.ArrayList;
import java.util.List;

public final class ac implements ad<ah> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3763a = {VastTagName.IMPRESSION, "VastAdTagURI", VastTagName.AD_SYSTEM, "Error", "ViewableImpression", "AdVerifications", VastTagName.CREATIVES};

    @NonNull
    public final p<ah> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        String str = VastAttributes.FOLLOW_ADDITIONAL_WRAPPERS;
        aVar.getClass();
        $$Lambda$KdCdgMomVVQtlEKc9dLCLhb80KU r1 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((Boolean) obj);
            }
        };
        arrayList.getClass();
        r d = rVar.d(str, r1, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str2 = VastAttributes.ALLOW_MULTIPLE_ADS;
        aVar.getClass();
        $$Lambda$_sOVeJ5pTVxaEFidABwSGXfyfE r2 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((Boolean) obj);
            }
        };
        arrayList.getClass();
        r d2 = d.d(str2, r2, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str3 = VastAttributes.FALLBACK_ON_NO_AD;
        aVar.getClass();
        $$Lambda$POVh7h2cwO_G4Rzlg1NArv_e2t8 r22 = new Consumer() {
            public final void accept(Object obj) {
                a.this.c((Boolean) obj);
            }
        };
        arrayList.getClass();
        r d3 = d2.d(str3, r22, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String[] strArr = f3763a;
        ArrayList arrayList2 = new ArrayList();
        aVar.a((List<s>) arrayList2);
        ArrayList arrayList3 = new ArrayList();
        aVar.b((List<String>) arrayList3);
        $$Lambda$ac$T__9nL0A8UaH1zaWMJjuzVAf9ew r0 = new Consumer(arrayList2, arrayList, aVar, arrayList3) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ a f$3;
            private final /* synthetic */ List f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void accept(Object obj) {
                ac.a(r.this, this.f$1, this.f$2, this.f$3, this.f$4, (String) obj);
            }
        };
        d3.a(strArr, (Consumer<String>) r0, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.WRAPPER, new Exception("Unable to parse tags in Wrapper", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, List list, List list2, a aVar, List list3, String str) {
        if (str.equalsIgnoreCase(VastTagName.IMPRESSION)) {
            rVar.a(VastTagName.IMPRESSION, (b<p<Result>>) new b(list, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    ac.a(this.f$0, this.f$1, (p) obj);
                }
            });
        } else if (str.equalsIgnoreCase("VastAdTagURI")) {
            aVar.getClass();
            rVar.a((Consumer<String>) new Consumer() {
                public final void accept(Object obj) {
                    a.this.a((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("VastAdTagURI", new Exception("Unable to parse VastAdTagUri in Wrapper", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase(VastTagName.AD_SYSTEM)) {
            rVar.a(VastTagName.AD_SYSTEM, (b<p<Result>>) new b(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    ac.d(a.this, this.f$1, (p) obj);
                }
            });
        } else if (str.equalsIgnoreCase("Error")) {
            list3.getClass();
            rVar.a((Consumer<String>) new Consumer(list3) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("Error", new Exception("Unable to parse Error in Wrapper", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase("ViewableImpression")) {
            rVar.a("ViewableImpression", (b<p<Result>>) new b(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    ac.c(a.this, this.f$1, (p) obj);
                }
            });
        } else if (str.equalsIgnoreCase("AdVerifications")) {
            rVar.a("AdVerifications", (b<p<Result>>) new b(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    ac.b(a.this, this.f$1, (p) obj);
                }
            });
        } else {
            if (str.equalsIgnoreCase(VastTagName.CREATIVES)) {
                rVar.a(VastTagName.CREATIVES, (b<p<Result>>) new b(list2) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        ac.a(a.this, this.f$1, (p) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        Result result = pVar.b;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((s) obj);
            }
        });
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(a aVar, List list, p pVar) {
        aVar.a((c) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(a aVar, List list, p pVar) {
        aVar.a((ag) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(a aVar, List list, p pVar) {
        Result result = pVar.b;
        aVar.getClass();
        Objects.onNotNull(result, new Consumer() {
            public final void accept(Object obj) {
                a.this.c((List) obj);
            }
        });
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(a aVar, List list, p pVar) {
        Result result = pVar.b;
        aVar.getClass();
        Objects.onNotNull(result, new Consumer() {
            public final void accept(Object obj) {
                a.this.d((List) obj);
            }
        });
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
