package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.util.Objects;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;

public final class s implements ClassFactory<r> {
    @NonNull
    public final /* synthetic */ Object get(DiConstructor diConstructor) {
        XmlPullParser xmlPullParser = (XmlPullParser) diConstructor.get("VideoModuleInterface", XmlPullParser.class);
        Objects.requireNonNull(xmlPullParser, "XML pull parser shouldn't be null");
        r rVar = new r(xmlPullParser, new HashMap());
        rVar.a(VastTagName.VAST, (ad<T>) new y<T>());
        rVar.a(VastTagName.AD_PARAMETERS, (ad<T>) new a<T>());
        rVar.a(VastTagName.AD, (ad<T>) new b<T>());
        rVar.a(VastTagName.AD_SYSTEM, (ad<T>) new c<T>());
        rVar.a("Category", (ad<T>) new e<T>());
        rVar.a(VastTagName.COMPANION_ADS, (ad<T>) new f<T>());
        rVar.a(VastTagName.COMPANION, (ad<T>) new g<T>());
        rVar.a(VastTagName.CREATIVE, (ad<T>) new h<T>());
        rVar.a("IconClicks", (ad<T>) new i<T>());
        rVar.a("Icon", (ad<T>) new j<T>());
        rVar.a(VastTagName.IN_LINE, (ad<T>) new k<T>());
        rVar.a("JavaScriptResource", (ad<T>) new l<T>());
        rVar.a(VastTagName.LINEAR, (ad<T>) new m<T>());
        rVar.a(VastTagName.MEDIA_FILE, (ad<T>) new n<T>());
        rVar.a(VastTagName.STATIC_RESOURCE, (ad<T>) new t<T>());
        rVar.a(VastTagName.TRACKING, (ad<T>) new u<T>());
        rVar.a("UniversalAdId", (ad<T>) new v<T>());
        rVar.a("Verification", (ad<T>) new z<T>());
        rVar.a(VastTagName.VIDEO_CLICKS, (ad<T>) new aa<T>());
        rVar.a("ViewableImpression", (ad<T>) new ab<T>());
        rVar.a(VastTagName.WRAPPER, (ad<T>) new ac<T>());
        rVar.a(VastTagName.IMPRESSION, (ad<T>) new w<T>(VastTagName.IMPRESSION));
        rVar.a(VastTagName.CLICK_THROUGH, (ad<T>) new w<T>(VastTagName.CLICK_THROUGH));
        rVar.a(VastTagName.CLICK_TRACKING, (ad<T>) new w<T>(VastTagName.CLICK_TRACKING));
        rVar.a(VastTagName.CUSTOM_CLICK, (ad<T>) new w<T>(VastTagName.CUSTOM_CLICK));
        rVar.a("IconClickTracking", (ad<T>) new w<T>("IconClickTracking"));
        rVar.a(VastTagName.COMPANION_CLICK_TRACKING, (ad<T>) new w<T>(VastTagName.COMPANION_CLICK_TRACKING));
        rVar.a("AdVerifications", (ad<T>) new d<T>("AdVerifications", "Verification"));
        rVar.a(VastTagName.CREATIVES, (ad<T>) new d<T>(VastTagName.CREATIVES, VastTagName.CREATIVE));
        rVar.a(VastTagName.MEDIA_FILES, (ad<T>) new d<T>(VastTagName.MEDIA_FILES, VastTagName.MEDIA_FILE));
        rVar.a("Icons", (ad<T>) new d<T>("Icons", "Icon"));
        rVar.a(VastTagName.TRACKING_EVENTS, (ad<T>) new d<T>(VastTagName.TRACKING_EVENTS, VastTagName.TRACKING));
        return rVar;
    }
}
