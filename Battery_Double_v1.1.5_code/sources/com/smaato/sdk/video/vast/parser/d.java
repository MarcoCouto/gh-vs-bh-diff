package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.parser.p.a;
import java.util.ArrayList;
import java.util.List;

public final class d<Result> implements ad<List<Result>> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3765a;
    @NonNull
    private final String b;

    public d(@NonNull String str, @NonNull String str2) {
        this.f3765a = str;
        this.b = str2;
    }

    @NonNull
    public final p<List<Result>> a(@NonNull r rVar) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        rVar.a(new String[]{this.b}, (Consumer<String>) new Consumer(rVar, arrayList, arrayList2) {
            private final /* synthetic */ r f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                d.this.a(this.f$1, this.f$2, this.f$3, (String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList2) {
            private final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                d.this.a(this.f$1, (Exception) obj);
            }
        });
        return new a().a(arrayList).a((List<o>) arrayList2).a();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(r rVar, List list, List list2, String str) {
        rVar.a(this.b, (b<p<Result>>) new b(list, list2) {
            private final /* synthetic */ List f$0;
            private final /* synthetic */ List f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                d.a(this.f$0, this.f$1, (p) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        Result result = pVar.b;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(obj);
            }
        });
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(List list, Exception exc) {
        String str = this.f3765a;
        StringBuilder sb = new StringBuilder("Unable to parse ");
        sb.append(this.b);
        sb.append(" elements in ");
        sb.append(this.f3765a);
        list.add(o.a(str, new Exception(sb.toString(), exc)));
    }
}
