package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.b;
import com.smaato.sdk.video.vast.model.b.a;
import java.util.ArrayList;
import java.util.List;

public class a implements ad<b> {
    @NonNull
    public final p<b> a(@NonNull r rVar) {
        b bVar;
        com.smaato.sdk.video.vast.model.b.a aVar = new com.smaato.sdk.video.vast.model.b.a();
        ArrayList arrayList = new ArrayList();
        aVar.getClass();
        $$Lambda$Hf2Ka7JNngpSt2L2MfutkYGcIsI r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((Boolean) obj);
            }
        };
        arrayList.getClass();
        r d = rVar.d("xmlEncoded", r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        d.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.AD_PARAMETERS, new Exception("Unable to parse AdParameters body", (Exception) obj)));
            }
        });
        try {
            bVar = aVar.a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            arrayList.add(o.a(VastTagName.AD_PARAMETERS, e));
            bVar = null;
        }
        return new com.smaato.sdk.video.vast.parser.p.a().a(bVar).a((List<o>) arrayList).a();
    }
}
