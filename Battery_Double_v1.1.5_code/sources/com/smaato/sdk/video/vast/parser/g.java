package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.e;
import com.smaato.sdk.video.vast.model.e.a;
import com.smaato.sdk.video.vast.model.p;
import com.smaato.sdk.video.vast.model.q;
import com.smaato.sdk.video.vast.model.s;
import java.util.ArrayList;
import java.util.List;

public final class g implements ad<e> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3767a = {VastTagName.STATIC_RESOURCE, VastTagName.I_FRAME_RESOURCE, VastTagName.HTML_RESOURCE, "AltText", VastTagName.COMPANION_CLICK_THROUGH, VastTagName.COMPANION_CLICK_TRACKING, VastTagName.TRACKING_EVENTS, VastTagName.AD_PARAMETERS};

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, List list, List list2, List list3, List list4, a aVar, List list5, String str) {
        if (VastTagName.STATIC_RESOURCE.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.STATIC_RESOURCE, (b<p<Result>>) new b(list, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    g.b(this.f$0, this.f$1, (p) obj);
                }
            });
        } else if (VastTagName.I_FRAME_RESOURCE.equalsIgnoreCase(str)) {
            list3.getClass();
            rVar.a((Consumer<String>) new Consumer(list3) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a(VastTagName.I_FRAME_RESOURCE, new Exception("Unable to parse IFrameResource", (Exception) obj)));
                }
            });
        } else if (VastTagName.HTML_RESOURCE.equalsIgnoreCase(str)) {
            list4.getClass();
            rVar.a((Consumer<String>) new Consumer(list4) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a(VastTagName.HTML_RESOURCE, new Exception("Unable to parse HtmlResource", (Exception) obj)));
                }
            });
        } else if ("AltText".equalsIgnoreCase(str)) {
            aVar.getClass();
            rVar.a((Consumer<String>) new Consumer() {
                public final void accept(Object obj) {
                    a.this.d((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("AltText", new Exception("Unable to parse AltText", (Exception) obj)));
                }
            });
        } else if (VastTagName.AD_PARAMETERS.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.AD_PARAMETERS, (b<p<Result>>) new b(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    g.a(a.this, this.f$1, (p) obj);
                }
            });
        } else if (VastTagName.COMPANION_CLICK_THROUGH.equalsIgnoreCase(str)) {
            aVar.getClass();
            rVar.a((Consumer<String>) new Consumer() {
                public final void accept(Object obj) {
                    a.this.e((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a(VastTagName.COMPANION_CLICK_THROUGH, new Exception("Unable to parse CompanionClickThrough", (Exception) obj)));
                }
            });
        } else if (VastTagName.COMPANION_CLICK_TRACKING.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.COMPANION_CLICK_TRACKING, (b<p<Result>>) new b(list5, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    g.a(this.f$0, this.f$1, (p) obj);
                }
            });
        } else {
            if (VastTagName.TRACKING_EVENTS.equalsIgnoreCase(str)) {
                rVar.a(VastTagName.TRACKING_EVENTS, (b<p<Result>>) new b(list2) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        g.b(a.this, this.f$1, (p) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(List list, List list2, p pVar) {
        Result result = pVar.b;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((p) obj);
            }
        });
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(a aVar, List list, p pVar) {
        Result result = pVar.b;
        aVar.getClass();
        Objects.onNotNull(result, new Consumer() {
            public final void accept(Object obj) {
                a.this.e((List) obj);
            }
        });
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        Result result = pVar.b;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((s) obj);
            }
        });
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(a aVar, List list, p pVar) {
        aVar.a((com.smaato.sdk.video.vast.model.b) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    @NonNull
    public final p<e> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        aVar.e((List<q>) arrayList2);
        aVar.d((List<s>) arrayList3);
        aVar.getClass();
        $$Lambda$h0WgkwMD6h5ZRx1t8bkOWc_ISlo r1 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$f1TNrGBrN6bwstkAgoBn3jRsMXc r2 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((Float) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$Xruri7Y1xIgdQ8IEHKIPXGHIfs r22 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((Float) obj);
            }
        };
        arrayList.getClass();
        r c = rVar.a("id", (Consumer<String>) r1, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).c("width", r2, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).c("height", r22, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str = VastAttributes.ASSET_WIDTH;
        aVar.getClass();
        $$Lambda$2VQORvuOCfEwwiNIrtB0GNkH0ZU r23 = new Consumer() {
            public final void accept(Object obj) {
                a.this.c((Float) obj);
            }
        };
        arrayList.getClass();
        r c2 = c.c(str, r23, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str2 = VastAttributes.ASSET_HEIGHT;
        aVar.getClass();
        $$Lambda$Stv6SG2oBQ3EsGMh8kVrzstoUmY r24 = new Consumer() {
            public final void accept(Object obj) {
                a.this.d((Float) obj);
            }
        };
        arrayList.getClass();
        r c3 = c2.c(str2, r24, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str3 = VastAttributes.EXPANDED_WIDTH;
        aVar.getClass();
        $$Lambda$Sj54W74PBX8VInMnl_0BbrJX5Xk r25 = new Consumer() {
            public final void accept(Object obj) {
                a.this.e((Float) obj);
            }
        };
        arrayList.getClass();
        r c4 = c3.c(str3, r25, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str4 = VastAttributes.EXPANDED_HEIGHT;
        aVar.getClass();
        $$Lambda$RWGtqu8p2Cyc_3NY3fez6umKc r26 = new Consumer() {
            public final void accept(Object obj) {
                a.this.f((Float) obj);
            }
        };
        arrayList.getClass();
        r c5 = c4.c(str4, r26, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str5 = VastAttributes.API_FRAMEWORK;
        aVar.getClass();
        $$Lambda$iVgPqWJAvR57lMP7V6noHYAUFqc r27 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = c5.a(str5, (Consumer<String>) r27, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str6 = VastAttributes.AD_SLOT_ID;
        aVar.getClass();
        $$Lambda$XBDMOOwBE9pFmGVECVuLiKQrQB4 r28 = new Consumer() {
            public final void accept(Object obj) {
                a.this.c((String) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$aNQtDZlDyUUjBiWYDDC14fDwxA r29 = new Consumer() {
            public final void accept(Object obj) {
                a.this.g((Float) obj);
            }
        };
        arrayList.getClass();
        a2.a(str6, (Consumer<String>) r28, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).c("pxratio", r29, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String[] strArr = f3767a;
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        ArrayList arrayList7 = new ArrayList();
        aVar.d((List<s>) arrayList4);
        aVar.a((List<p>) arrayList5);
        aVar.b((List<String>) arrayList6);
        aVar.c((List<String>) arrayList7);
        $$Lambda$g$_jjwSpESzVXYwIPM_w9iS9E_Xo r0 = new Consumer(arrayList5, arrayList, arrayList6, arrayList7, aVar, arrayList4) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;
            private final /* synthetic */ a f$5;
            private final /* synthetic */ List f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            public final void accept(Object obj) {
                g.a(r.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (String) obj);
            }
        };
        rVar.a(strArr, (Consumer<String>) r0, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.COMPANION, new Exception("Unable to parse tags in Companion", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }
}
