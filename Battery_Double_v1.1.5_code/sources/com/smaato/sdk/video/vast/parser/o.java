package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class o {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final Exception f3774a;
    @NonNull
    private String b;
    @Nullable
    private List<o> c;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f3775a;
        @Nullable
        private Exception b;

        @NonNull
        public final a a(@Nullable String str) {
            this.f3775a = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Exception exc) {
            this.b = exc;
            return this;
        }

        @NonNull
        public final o a() {
            this.f3775a.getClass();
            return new o(this.f3775a, this.b, null);
        }
    }

    public o(@NonNull String str, @Nullable Exception exc, @Nullable List<o> list) {
        this.b = str;
        this.f3774a = exc;
        this.c = list;
    }

    @NonNull
    public static o a(@NonNull String str, @Nullable Exception exc) {
        return new a().a(str).a(exc).a();
    }
}
