package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.a;
import com.smaato.sdk.video.vast.model.e;
import com.smaato.sdk.video.vast.model.f;
import com.smaato.sdk.video.vast.model.f.b;
import java.util.ArrayList;
import java.util.List;

public final class f implements ad<com.smaato.sdk.video.vast.model.f> {

    /* renamed from: a reason: collision with root package name */
    private static final a<String, b> f3766a = $$Lambda$f$ZliTXFuU7Jq0SdYwA3IukcH5q8U.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ b a(String str) throws Exception {
        return (b) Objects.requireNonNull(b.a(str));
    }

    @NonNull
    public final p<com.smaato.sdk.video.vast.model.f> a(@NonNull r rVar) {
        com.smaato.sdk.video.vast.model.f fVar;
        com.smaato.sdk.video.vast.model.f.a aVar = new com.smaato.sdk.video.vast.model.f.a();
        ArrayList arrayList = new ArrayList();
        aVar.a((List<e>) arrayList);
        ArrayList arrayList2 = new ArrayList();
        String str = VastAttributes.REQUIRED;
        a<String, b> aVar2 = f3766a;
        aVar.getClass();
        $$Lambda$QGmrJ9dlc3yYdxAGiLb7KuLfPU r5 = new Consumer() {
            public final void accept(Object obj) {
                f.a.this.a((b) obj);
            }
        };
        arrayList2.getClass();
        rVar.a(str, aVar2, r5, new Consumer(arrayList2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a(new String[]{VastTagName.COMPANION}, (Consumer<String>) new Consumer(arrayList, arrayList2) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                r.this.a(VastTagName.COMPANION, (com.smaato.sdk.video.fi.b<p<Result>>) new com.smaato.sdk.video.fi.b(this.f$1, this.f$2) {
                    private final /* synthetic */ List f$0;
                    private final /* synthetic */ List f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        f.a(this.f$0, this.f$1, (p) obj);
                    }
                });
            }
        }, (Consumer<Exception>) new Consumer(arrayList2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.COMPANION_ADS, new Exception("Unable to parse tags in CompanionAds", (Exception) obj)));
            }
        });
        try {
            fVar = aVar.a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            arrayList2.add(o.a(VastTagName.COMPANION_ADS, e));
            fVar = null;
        }
        return new p.a().a(fVar).a((List<o>) arrayList2).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        Result result = pVar.b;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((e) obj);
            }
        });
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
