package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.ad;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParserException;

public class x {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final r f3782a;

    public x(@NonNull r rVar) {
        this.f3782a = (r) Objects.requireNonNull(rVar, "Parameter xmlPullParser cannot be null for VastResponseParser::new");
    }

    public final void a(@NonNull Logger logger, @NonNull InputStream inputStream, @Nullable String str, @NonNull b<p<ad>> bVar) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(inputStream);
        Objects.requireNonNull(bVar);
        try {
            this.f3782a.a(inputStream, str).a(VastTagName.VAST, bVar);
        } catch (IOException | XmlPullParserException e) {
            bVar.accept(p.a(VastTagName.VAST, e));
        }
    }
}
