package com.smaato.sdk.video.vast.parser;

import android.util.Xml;
import androidx.annotation.NonNull;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ae {
    public static boolean a(@NonNull String str) {
        try {
            return Xml.findEncodingByName(str) != null;
        } catch (UnsupportedEncodingException unused) {
            return false;
        }
    }

    @NonNull
    public static String b(String str) {
        String str2 = "";
        if (str == null) {
            return str2;
        }
        try {
            return URLEncoder.encode(str, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!").replaceAll("\\%27", "'").replaceAll("\\%28", "(").replaceAll("\\%29", ")").replaceAll("\\%7E", "~");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Unsupported encoding", e);
        }
    }
}
