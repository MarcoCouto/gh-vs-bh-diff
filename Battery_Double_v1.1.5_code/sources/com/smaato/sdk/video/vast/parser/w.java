package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.s;
import com.smaato.sdk.video.vast.model.s.a;
import java.util.ArrayList;
import java.util.List;

public final class w implements ad<s> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3781a;

    public w(@NonNull String str) {
        this.f3781a = str;
    }

    @NonNull
    public final p<s> a(@NonNull r rVar) {
        s sVar;
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        aVar.getClass();
        $$Lambda$9be1OttpLeyCXagkG8WlUJRyNo r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = rVar.a("id", (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        a2.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                a.this.b((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                w.this.a(this.f$1, (Exception) obj);
            }
        });
        try {
            sVar = aVar.a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            arrayList.add(o.a(this.f3781a, e));
            sVar = null;
        }
        return new p.a().a(sVar).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(List list, Exception exc) {
        list.add(o.a(this.f3781a, new Exception("Unable to parse UniversalAdId value", exc)));
    }
}
