package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.a;
import com.smaato.sdk.video.vast.model.h;
import com.smaato.sdk.video.vast.model.n;
import java.util.ArrayList;
import java.util.List;

public final class n implements ad<com.smaato.sdk.video.vast.model.n> {

    /* renamed from: a reason: collision with root package name */
    private static final a<String, h> f3773a = $$Lambda$n$HNNI_GtJu0v7gytZf79udAgnYQ.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ h a(String str) throws Exception {
        return (h) Objects.requireNonNull(h.a(str));
    }

    @NonNull
    public final p<com.smaato.sdk.video.vast.model.n> a(@NonNull r rVar) {
        com.smaato.sdk.video.vast.model.n nVar;
        com.smaato.sdk.video.vast.model.n.a aVar = new com.smaato.sdk.video.vast.model.n.a();
        ArrayList arrayList = new ArrayList();
        aVar.getClass();
        $$Lambda$mSZHwZV764y1B3bRg4upHJ9AD0Y r3 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.b((String) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$tr869jYCsBdX_KaGQKhNXjMhIZ8 r32 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.c((String) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$sOpvkXfzVL1ORE6ysmRh9eQLrzk r33 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.a((Float) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$lTpMKoOapp_Q4c9DV3kbo_aG9dI r34 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.b((Float) obj);
            }
        };
        arrayList.getClass();
        r c = rVar.a("id", (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a("type", (Consumer<String>) r32, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).c("width", r33, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).c("height", r34, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str = VastAttributes.CODEC;
        aVar.getClass();
        $$Lambda$MxWSEt8pbGD4SAWz10xLAZosJbA r35 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.d((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = c.a(str, (Consumer<String>) r35, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str2 = VastAttributes.BITRATE;
        aVar.getClass();
        $$Lambda$DIU9Zh0O_n_pae3gbJdvY6bF34 r36 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.a((Integer) obj);
            }
        };
        arrayList.getClass();
        r b = a2.b(str2, r36, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str3 = VastAttributes.MIN_BITRATE;
        aVar.getClass();
        $$Lambda$3l_eIKfVZicEYj1gXXduTFo_xQ r37 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.b((Integer) obj);
            }
        };
        arrayList.getClass();
        r b2 = b.b(str3, r37, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str4 = VastAttributes.MAX_BITRATE;
        aVar.getClass();
        $$Lambda$_5YnFCqLYbXt3cmK21qi28QFd2g r38 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.c((Integer) obj);
            }
        };
        arrayList.getClass();
        r b3 = b2.b(str4, r38, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str5 = VastAttributes.SCALABLE;
        aVar.getClass();
        $$Lambda$MdYQ35LwZgmCa8a7WEAxvyu3LU r39 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.a((Boolean) obj);
            }
        };
        arrayList.getClass();
        r d = b3.d(str5, r39, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str6 = VastAttributes.MAINTAIN_ASPECT_RATION;
        aVar.getClass();
        $$Lambda$CEXToEx6g9n9DWhlRzv94_WLpE0 r310 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.b((Boolean) obj);
            }
        };
        arrayList.getClass();
        r d2 = d.d(str6, r310, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str7 = VastAttributes.API_FRAMEWORK;
        aVar.getClass();
        $$Lambda$QQrI8cgOQb7RHlIRS7h2v3KvbM r311 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.e((String) obj);
            }
        };
        arrayList.getClass();
        r a3 = d2.a(str7, (Consumer<String>) r311, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str8 = VastAttributes.DELIVERY;
        a<String, h> aVar2 = f3773a;
        aVar.getClass();
        $$Lambda$K9M0ctvV1mbjUPQtOasRC_neCYs r4 = new Consumer() {
            public final void accept(Object obj) {
                n.a.this.a((h) obj);
            }
        };
        arrayList.getClass();
        r a4 = a3.a(str8, aVar2, r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        a4.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                n.a.this.a((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a("url", new Exception("Unable to parse URL value", (Exception) obj)));
            }
        });
        try {
            nVar = aVar.a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            arrayList.add(o.a(VastTagName.MEDIA_FILE, e));
            nVar = null;
        }
        return new p.a().a(nVar).a((List<o>) arrayList).a();
    }
}
