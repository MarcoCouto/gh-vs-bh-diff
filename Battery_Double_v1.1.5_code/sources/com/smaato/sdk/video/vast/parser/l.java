package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.facebook.share.internal.ShareConstants;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.l.a;
import java.util.ArrayList;
import java.util.List;

public final class l implements ad<com.smaato.sdk.video.vast.model.l> {
    @NonNull
    public final p<com.smaato.sdk.video.vast.model.l> a(@NonNull r rVar) {
        com.smaato.sdk.video.vast.model.l lVar;
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        String str = VastAttributes.API_FRAMEWORK;
        aVar.getClass();
        $$Lambda$PaMmI1BKzjUV3M_RvjRvrGcZWxs r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = rVar.a(str, (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        a2.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(ShareConstants.MEDIA_URI, new Exception("Unable to parse URI value", (Exception) obj)));
            }
        });
        try {
            lVar = aVar.a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            arrayList.add(o.a("JavaScriptResource", e));
            lVar = null;
        }
        return new p.a().a(lVar).a((List<o>) arrayList).a();
    }
}
