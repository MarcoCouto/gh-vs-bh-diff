package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.ag;
import com.smaato.sdk.video.vast.model.ag.a;
import java.util.ArrayList;
import java.util.List;

public final class ab implements ad<ag> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3762a = {"Viewable", "NotViewable", "ViewUndetermined"};

    @NonNull
    public final p<ag> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        aVar.a((List<String>) arrayList);
        aVar.b(arrayList2);
        aVar.c(arrayList3);
        aVar.getClass();
        $$Lambda$A_yHxFoB1Apk4K9qn51A6102AAQ r2 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList4.getClass();
        r a2 = rVar.a("id", (Consumer<String>) r2, (Consumer<o>) new Consumer(arrayList4) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String[] strArr = f3762a;
        $$Lambda$ab$aujMkRRz5a8D6XOzVvme8f0hVko r1 = new Consumer(arrayList, arrayList4, arrayList2, arrayList3) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void accept(Object obj) {
                ab.a(r.this, this.f$1, this.f$2, this.f$3, this.f$4, (String) obj);
            }
        };
        a2.a(strArr, (Consumer<String>) r1, (Consumer<Exception>) new Consumer(arrayList4) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a("ViewableImpression", new Exception("Unable to parse tags in ViewableImpression")));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList4).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, List list, List list2, List list3, List list4, String str) {
        if ("Viewable".equalsIgnoreCase(str)) {
            list.getClass();
            rVar.a((Consumer<String>) new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("Viewable", new Exception("Unable to parse ViewableImpression value", (Exception) obj)));
                }
            });
        } else if ("NotViewable".equalsIgnoreCase(str)) {
            list3.getClass();
            rVar.a((Consumer<String>) new Consumer(list3) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("NotViewable", new Exception("Unable to parse NotViewableImpression value", (Exception) obj)));
                }
            });
        } else {
            if ("ViewUndetermined".equalsIgnoreCase(str)) {
                list4.getClass();
                rVar.a((Consumer<String>) new Consumer(list4) {
                    private final /* synthetic */ List f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        this.f$0.add((String) obj);
                    }
                }, (Consumer<Exception>) new Consumer(list2) {
                    private final /* synthetic */ List f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        this.f$0.add(o.a("ViewUndetermined", new Exception("Unable to parse ViewUndetermined value", (Exception) obj)));
                    }
                });
            }
        }
    }
}
