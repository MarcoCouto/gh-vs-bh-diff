package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.r;
import com.smaato.sdk.video.vast.model.r.a;
import java.util.ArrayList;
import java.util.List;

public final class v implements ad<r> {
    @NonNull
    public final p<r> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        aVar.getClass();
        $$Lambda$fGNedvdBR3l7PqXFcJXXQs46JN0 r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$_cefOrZSTFtSsDg5oxNq1Bv8wsQ r32 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = rVar.a("idRegistry", (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a("idValue", (Consumer<String>) r32, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        a2.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                a.this.c((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a("UniversalAdId", new Exception("Unable to parse UniversalAdId value", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }
}
