package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.i;
import com.smaato.sdk.video.vast.model.i.a;
import com.smaato.sdk.video.vast.model.p;
import java.util.ArrayList;
import java.util.List;

public final class j implements ad<i> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3770a = {VastTagName.STATIC_RESOURCE, VastTagName.I_FRAME_RESOURCE, VastTagName.HTML_RESOURCE, "IconClicks", "IconViewTracking"};

    @NonNull
    public final p<i> a(@NonNull r rVar) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        a aVar = new a();
        aVar.a((List<String>) arrayList);
        aVar.b((List<p>) arrayList2);
        aVar.c((List<String>) arrayList3);
        aVar.d((List<String>) arrayList4);
        ArrayList arrayList5 = new ArrayList();
        aVar.getClass();
        $$Lambda$6uwdPmGSoQyxcWHmLT8CH7SWI_8 r1 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList5.getClass();
        aVar.getClass();
        $$Lambda$DwzVAN77emdgbWJZplWFiRahV4 r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((Float) obj);
            }
        };
        arrayList5.getClass();
        aVar.getClass();
        $$Lambda$Zvu6yEJbSXbboI8WKvXZKtl4sbc r32 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((Float) obj);
            }
        };
        arrayList5.getClass();
        aVar.getClass();
        $$Lambda$XEfyplTjj3Gi8UAXWKmewlamyT0 r33 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((String) obj);
            }
        };
        arrayList5.getClass();
        aVar.getClass();
        $$Lambda$DFIbRxAMaXqR_PYsorT4171ZUso r34 = new Consumer() {
            public final void accept(Object obj) {
                a.this.c((String) obj);
            }
        };
        arrayList5.getClass();
        r a2 = rVar.a("program", (Consumer<String>) r1, (Consumer<o>) new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).c("width", r3, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).c("height", r32, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a("xPosition", (Consumer<String>) r33, (Consumer<o>) new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a("yPosition", (Consumer<String>) r34, (Consumer<o>) new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str = IronSourceConstants.EVENTS_DURATION;
        aVar.getClass();
        $$Lambda$hj_YQn5vMhAC889JYh7E1y4m5c r35 = new Consumer() {
            public final void accept(Object obj) {
                a.this.e((String) obj);
            }
        };
        arrayList5.getClass();
        aVar.getClass();
        $$Lambda$zkzwfx0uEnB6GwALS5CrnTLUwo r36 = new Consumer() {
            public final void accept(Object obj) {
                a.this.d((String) obj);
            }
        };
        arrayList5.getClass();
        r a3 = a2.a(str, (Consumer<String>) r35, (Consumer<o>) new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a("offset", (Consumer<String>) r36, (Consumer<o>) new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str2 = VastAttributes.API_FRAMEWORK;
        aVar.getClass();
        $$Lambda$ydwvxSmh8_49CbNhKVG7zSltwko r37 = new Consumer() {
            public final void accept(Object obj) {
                a.this.f((String) obj);
            }
        };
        arrayList5.getClass();
        aVar.getClass();
        $$Lambda$ZyTPBpPHo8l4sPN6gCevs_OWdQc r38 = new Consumer() {
            public final void accept(Object obj) {
                a.this.c((Float) obj);
            }
        };
        arrayList5.getClass();
        a3.a(str2, (Consumer<String>) r37, (Consumer<o>) new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).c("pxratio", r38, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String[] strArr = f3770a;
        $$Lambda$j$q7ZknmMX4wGWHgRtoBwWzNXy8kk r0 = new Consumer(arrayList2, arrayList5, arrayList3, arrayList4, arrayList, aVar) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;
            private final /* synthetic */ List f$5;
            private final /* synthetic */ a f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            public final void accept(Object obj) {
                j.a(r.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (String) obj);
            }
        };
        rVar.a(strArr, (Consumer<String>) r0, (Consumer<Exception>) new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a("Icon", new Exception("Unable to parse URL value", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList5).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, List list, List list2, List list3, List list4, List list5, a aVar, String str) {
        if (str.equalsIgnoreCase(VastTagName.STATIC_RESOURCE)) {
            rVar.a(VastTagName.STATIC_RESOURCE, (b<p<Result>>) new b(list, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    j.a(this.f$0, this.f$1, (p) obj);
                }
            });
        } else if (str.equalsIgnoreCase(VastTagName.I_FRAME_RESOURCE)) {
            list3.getClass();
            rVar.a((Consumer<String>) new Consumer(list3) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a(VastTagName.I_FRAME_RESOURCE, new Exception("Unable to parse IFrameResource value", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase(VastTagName.HTML_RESOURCE)) {
            list4.getClass();
            rVar.a((Consumer<String>) new Consumer(list4) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a(VastTagName.HTML_RESOURCE, new Exception("Unable to parse HTMLResource value", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase("IconViewTracking")) {
            list5.getClass();
            rVar.a((Consumer<String>) new Consumer(list5) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("IconViewTracking", new Exception("Unable to parse IconViewTracking value", (Exception) obj)));
                }
            });
        } else {
            if (str.equalsIgnoreCase("IconClicks")) {
                rVar.a("IconClicks", (b<p<Result>>) new b(list2) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        j.a(a.this, this.f$1, (p) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        if (pVar.b != null) {
            list.add(pVar.b);
        }
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(a aVar, List list, p pVar) {
        aVar.a((com.smaato.sdk.video.vast.model.j) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
