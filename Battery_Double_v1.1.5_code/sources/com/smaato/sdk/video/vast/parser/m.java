package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.af;
import com.smaato.sdk.video.vast.model.m.a;
import java.util.ArrayList;
import java.util.List;

public final class m implements ad<com.smaato.sdk.video.vast.model.m> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3772a = {VastTagName.DURATION, VastTagName.AD_PARAMETERS, VastTagName.MEDIA_FILES, VastTagName.VIDEO_CLICKS, VastTagName.TRACKING_EVENTS, "Icons"};

    @NonNull
    public final p<com.smaato.sdk.video.vast.model.m> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        String str = VastAttributes.SKIP_OFFSET;
        aVar.getClass();
        $$Lambda$KjnlYoImeEB8FRspa8LjD9jHzUs r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList.getClass();
        rVar.a(str, (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a(f3772a, (Consumer<String>) new Consumer(aVar, arrayList) {
            private final /* synthetic */ a f$1;
            private final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                m.a(r.this, this.f$1, this.f$2, (String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.LINEAR, new Exception("Unable to parse tags in Linear")));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, a aVar, List list, String str) {
        if (VastTagName.DURATION.equalsIgnoreCase(str)) {
            aVar.getClass();
            rVar.a((Consumer<String>) new Consumer() {
                public final void accept(Object obj) {
                    a.this.b((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a(VastTagName.DURATION, new Exception("Unable to parse Duration value", (Exception) obj)));
                }
            });
        } else if (VastTagName.AD_PARAMETERS.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.AD_PARAMETERS, (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    m.e(a.this, this.f$1, (p) obj);
                }
            });
        } else if (VastTagName.MEDIA_FILES.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.MEDIA_FILES, (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    m.d(a.this, this.f$1, (p) obj);
                }
            });
        } else if (VastTagName.VIDEO_CLICKS.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.VIDEO_CLICKS, (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    m.c(a.this, this.f$1, (p) obj);
                }
            });
        } else if (VastTagName.TRACKING_EVENTS.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.TRACKING_EVENTS, (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    m.b(a.this, this.f$1, (p) obj);
                }
            });
        } else {
            if ("Icons".equalsIgnoreCase(str)) {
                rVar.a("Icons", (b<p<Result>>) new b(list) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        m.a(a.this, this.f$1, (p) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void e(a aVar, List list, p pVar) {
        aVar.a((com.smaato.sdk.video.vast.model.b) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(a aVar, List list, p pVar) {
        aVar.a((List) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(a aVar, List list, p pVar) {
        aVar.a((af) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(a aVar, List list, p pVar) {
        aVar.b((List) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(a aVar, List list, p pVar) {
        aVar.c((List) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
