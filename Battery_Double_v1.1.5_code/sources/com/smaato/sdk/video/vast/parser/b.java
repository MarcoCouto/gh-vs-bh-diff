package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.a;
import com.smaato.sdk.video.vast.model.a.C0082a;
import com.smaato.sdk.video.vast.model.ah;
import com.smaato.sdk.video.vast.model.k;
import java.util.ArrayList;
import java.util.List;

public final class b implements ad<a> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3764a = {VastTagName.IN_LINE, VastTagName.WRAPPER};

    @NonNull
    public final p<a> a(@NonNull r rVar) {
        C0082a aVar = new C0082a();
        ArrayList arrayList = new ArrayList();
        aVar.getClass();
        $$Lambda$eSfhdVzQQBvhx7JsaOfJVrzrPCA r3 = new Consumer() {
            public final void accept(Object obj) {
                C0082a.this.a((String) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$M5HPXT4nT6I0djOOOj45eWDBoQ r4 = new Consumer() {
            public final void accept(Object obj) {
                C0082a.this.a((Integer) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$jH57PAUjnyKODWm5U8_68TofvrU r42 = new Consumer() {
            public final void accept(Object obj) {
                C0082a.this.a((Boolean) obj);
            }
        };
        arrayList.getClass();
        rVar.a("id", (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).b("sequence", r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).d("conditionalAd", r42, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a(f3764a, (Consumer<String>) new Consumer(aVar, arrayList) {
            private final /* synthetic */ C0082a f$1;
            private final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                b.a(r.this, this.f$1, this.f$2, (String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.AD, new Exception("Unable to parse tags in Ad", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, C0082a aVar, List list, String str) {
        if (str.equalsIgnoreCase(VastTagName.IN_LINE)) {
            rVar.a(VastTagName.IN_LINE, (com.smaato.sdk.video.fi.b<p<Result>>) new com.smaato.sdk.video.fi.b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    b.b(C0082a.this, this.f$1, (p) obj);
                }
            });
            return;
        }
        if (str.equalsIgnoreCase(VastTagName.WRAPPER)) {
            rVar.a(VastTagName.WRAPPER, (com.smaato.sdk.video.fi.b<p<Result>>) new com.smaato.sdk.video.fi.b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    b.a(C0082a.this, this.f$1, (p) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(C0082a aVar, List list, p pVar) {
        aVar.a((k) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(C0082a aVar, List list, p pVar) {
        aVar.a((ah) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
