package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.a;
import com.smaato.sdk.video.vast.model.q;
import com.smaato.sdk.video.vast.model.v;
import java.util.ArrayList;
import java.util.List;

public final class u implements ad<q> {

    /* renamed from: a reason: collision with root package name */
    private static final a<String, v> f3780a = $$Lambda$u$o0rFayQ6lswRDs1WwFfOHqn1sOg.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ v a(String str) throws Exception {
        return (v) Objects.requireNonNull(v.a(str));
    }

    @NonNull
    public final p<q> a(@NonNull r rVar) {
        q qVar;
        q.a aVar = new q.a();
        ArrayList arrayList = new ArrayList();
        a<String, v> aVar2 = f3780a;
        aVar.getClass();
        $$Lambda$bBQ3oEuchQ4KgjSaO6V8vVVVUJA r4 = new Consumer() {
            public final void accept(Object obj) {
                q.a.this.a((v) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$BGy8c54XVw0eG4rT9zyYD42vyHs r3 = new Consumer() {
            public final void accept(Object obj) {
                q.a.this.b((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = rVar.a("event", aVar2, r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a("offset", (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        a2.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                q.a.this.a((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a("url", new Exception("Unable to parse URL value", (Exception) obj)));
            }
        });
        try {
            qVar = aVar.a();
        } catch (com.smaato.sdk.video.vast.exceptions.a e) {
            arrayList.add(o.a(VastTagName.TRACKING, e));
            qVar = null;
        }
        return new p.a().a(qVar).a((List<o>) arrayList).a();
    }
}
