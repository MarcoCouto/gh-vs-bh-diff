package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.j;
import com.smaato.sdk.video.vast.model.j.a;
import com.smaato.sdk.video.vast.model.s;
import java.util.ArrayList;
import java.util.List;

public final class i implements ad<j> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3769a = {"IconClickThrough", "IconClickTracking"};

    @NonNull
    public final p<j> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        aVar.a((List<s>) arrayList2);
        rVar.a(f3769a, (Consumer<String>) new Consumer(aVar, arrayList, arrayList2) {
            private final /* synthetic */ a f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                i.a(r.this, this.f$1, this.f$2, this.f$3, (String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a("IconClicks", new Exception("Unable to parse IconClicks value", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, a aVar, List list, List list2, String str) {
        if (str.equalsIgnoreCase("IconClickThrough")) {
            aVar.getClass();
            rVar.a((Consumer<String>) new Consumer() {
                public final void accept(Object obj) {
                    a.this.a((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("IconClickThrough", new Exception("Unable to parse IconClickThrough value", (Exception) obj)));
                }
            });
            return;
        }
        if (str.equalsIgnoreCase("IconClickTracking")) {
            rVar.a("IconClickTracking", (b<p<Result>>) new b(list2, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    i.a(this.f$0, this.f$1, (p) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        Result result = pVar.b;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((s) obj);
            }
        });
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
