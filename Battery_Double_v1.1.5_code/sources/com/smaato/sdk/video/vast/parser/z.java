package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.ae;
import com.smaato.sdk.video.vast.model.ae.a;
import com.smaato.sdk.video.vast.model.ag;
import com.smaato.sdk.video.vast.model.l;
import java.util.ArrayList;
import java.util.List;

public final class z implements ad<ae> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3784a = {"JavaScriptResource", "ViewableImpression"};

    @NonNull
    public final p<ae> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        aVar.a((List<l>) arrayList);
        aVar.getClass();
        $$Lambda$J0PNr9WVBY_Osrixl9xnSp_5hc4 r4 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList2.getClass();
        rVar.a("vendor", (Consumer<String>) r4, (Consumer<o>) new Consumer(arrayList2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a(f3784a, (Consumer<String>) new Consumer(arrayList, arrayList2, aVar) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ a f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                z.a(r.this, this.f$1, this.f$2, this.f$3, (String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a("Verification", new Exception("Unable to parse tags in Verification", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList2).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, List list, List list2, a aVar, String str) {
        if ("JavaScriptResource".equalsIgnoreCase(str)) {
            rVar.a("JavaScriptResource", (b<p<Result>>) new b(list, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    Objects.onNotNull((p) obj, new Consumer(this.f$0, this.f$1) {
                        private final /* synthetic */ List f$0;
                        private final /* synthetic */ List f$1;

                        {
                            this.f$0 = r1;
                            this.f$1 = r2;
                        }

                        public final void accept(Object obj) {
                            z.b(this.f$0, this.f$1, (p) obj);
                        }
                    });
                }
            });
            return;
        }
        if ("ViewableImpression".equalsIgnoreCase(str)) {
            rVar.a("ViewableImpression", (b<p<Result>>) new b(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    Objects.onNotNull((p) obj, new Consumer(this.f$1) {
                        private final /* synthetic */ List f$1;

                        {
                            this.f$1 = r2;
                        }

                        public final void accept(Object obj) {
                            z.b(a.this, this.f$1, (p) obj);
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(List list, List list2, p pVar) {
        list.add(pVar.b);
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(a aVar, List list, p pVar) {
        aVar.a((ag) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
