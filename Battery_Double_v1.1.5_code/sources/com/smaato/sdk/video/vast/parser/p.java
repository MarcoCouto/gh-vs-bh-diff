package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.collections.Lists;
import java.util.Collections;
import java.util.List;

public final class p<Result> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<o> f3776a;
    @Nullable
    public final Result b;

    public static class a<Result> {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private Result f3777a;
        @Nullable
        private List<o> b;

        @NonNull
        public final a<Result> a(@Nullable Result result) {
            this.f3777a = result;
            return this;
        }

        @NonNull
        public final a<Result> a(@Nullable List<o> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public final p<Result> a() {
            if (this.f3777a != null || this.b != null) {
                return new p<>(Lists.toImmutableList(this.b), this.f3777a, 0);
            }
            throw new IllegalStateException("ParseResult should contain value or list of errors at least");
        }
    }

    /* synthetic */ p(List list, Object obj, byte b2) {
        this(list, obj);
    }

    private p(@NonNull List<o> list, @Nullable Result result) {
        this.f3776a = list;
        this.b = result;
    }

    @NonNull
    public static <Result> p<Result> a(@NonNull String str, @Nullable Exception exc) {
        return new p<>(Collections.singletonList(o.a(str, exc)), null);
    }
}
