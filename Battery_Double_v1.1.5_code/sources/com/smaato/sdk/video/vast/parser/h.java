package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.f;
import com.smaato.sdk.video.vast.model.g;
import com.smaato.sdk.video.vast.model.g.a;
import com.smaato.sdk.video.vast.model.m;
import com.smaato.sdk.video.vast.model.r;
import java.util.ArrayList;
import java.util.List;

public final class h implements ad<g> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3768a = {"UniversalAdId", VastTagName.COMPANION_ADS, VastTagName.LINEAR};

    @NonNull
    public final p<g> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        aVar.getClass();
        $$Lambda$8SQD0YGGsg7MGUWNSblhouyWiXk r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$1b4rx9eGCIkb4fVtUcJbShBXXo r4 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((Integer) obj);
            }
        };
        arrayList.getClass();
        aVar.getClass();
        $$Lambda$LAWs2rvUvmBWVrSbmphgkffgDyo r42 = new Consumer() {
            public final void accept(Object obj) {
                a.this.b((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = rVar.a("id", (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).b("sequence", r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a("adId", (Consumer<String>) r42, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        String str = VastAttributes.API_FRAMEWORK;
        aVar.getClass();
        $$Lambda$avTWJKCmKhLhF4eLBWSWcPGeH5Q r43 = new Consumer() {
            public final void accept(Object obj) {
                a.this.c((String) obj);
            }
        };
        arrayList.getClass();
        a2.a(str, (Consumer<String>) r43, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        }).a(f3768a, (Consumer<String>) new Consumer(aVar, arrayList) {
            private final /* synthetic */ a f$1;
            private final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                h.a(r.this, this.f$1, this.f$2, (String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.CREATIVE, new Exception("Unable to parse tags in Creative", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(r rVar, a aVar, List list, String str) {
        if (str.equalsIgnoreCase("UniversalAdId")) {
            rVar.a("UniversalAdId", (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    h.c(a.this, this.f$1, (p) obj);
                }
            });
        } else if (str.equalsIgnoreCase(VastTagName.COMPANION_ADS)) {
            rVar.a(VastTagName.COMPANION_ADS, (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    h.b(a.this, this.f$1, (p) obj);
                }
            });
        } else {
            if (str.equalsIgnoreCase(VastTagName.LINEAR)) {
                rVar.a(VastTagName.LINEAR, (b<p<Result>>) new b(list) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        h.a(a.this, this.f$1, (p) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(a aVar, List list, p pVar) {
        aVar.a((r) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(a aVar, List list, p pVar) {
        aVar.a((f) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(a aVar, List list, p pVar) {
        aVar.a((m) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
