package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.d;
import com.smaato.sdk.video.vast.model.d.a;
import java.util.ArrayList;
import java.util.List;

public final class e implements ad<d> {
    @NonNull
    public final p<d> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        aVar.getClass();
        $$Lambda$aHzfhCg8bY_rRaNw1Oqwr99egFI r3 = new Consumer() {
            public final void accept(Object obj) {
                a.this.a((String) obj);
            }
        };
        arrayList.getClass();
        r a2 = rVar.a("authority", (Consumer<String>) r3, (Consumer<o>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((o) obj);
            }
        });
        aVar.getClass();
        a2.a((Consumer<String>) new Consumer() {
            public final void accept(Object obj) {
                a.this.b((String) obj);
            }
        }, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a("categories code", new Exception("Unable to parse Category code value", (Exception) obj)));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }
}
