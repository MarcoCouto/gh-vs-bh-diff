package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastTagName;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.b;
import com.smaato.sdk.video.vast.model.ag;
import com.smaato.sdk.video.vast.model.c;
import com.smaato.sdk.video.vast.model.d;
import com.smaato.sdk.video.vast.model.k.a;
import com.smaato.sdk.video.vast.model.s;
import java.util.ArrayList;
import java.util.List;

public final class k implements ad<com.smaato.sdk.video.vast.model.k> {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3771a = {VastTagName.AD_SYSTEM, "AdTitle", VastTagName.IMPRESSION, "Category", "Description", "Advertiser", "Error", "ViewableImpression", "AdVerifications", VastTagName.CREATIVES};

    @NonNull
    public final p<com.smaato.sdk.video.vast.model.k> a(@NonNull r rVar) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        String[] strArr = f3771a;
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        aVar.a((List<s>) arrayList2);
        aVar.b((List<d>) arrayList3);
        aVar.c((List<String>) arrayList4);
        $$Lambda$k$tqw2nwnTEEdSkaXVWUHZRknxcg r0 = new Consumer(rVar, aVar, arrayList, arrayList2, arrayList3, arrayList4) {
            private final /* synthetic */ r f$1;
            private final /* synthetic */ a f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;
            private final /* synthetic */ List f$5;
            private final /* synthetic */ List f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            public final void accept(Object obj) {
                k.this.a(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (String) obj);
            }
        };
        rVar.a(strArr, (Consumer<String>) r0, (Consumer<Exception>) new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(o.a(VastTagName.IN_LINE, new Exception("Unable to parse tags in InLine")));
            }
        });
        return new p.a().a(aVar.a()).a((List<o>) arrayList).a();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(r rVar, a aVar, List list, List list2, List list3, List list4, String str) {
        if (VastTagName.AD_SYSTEM.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.AD_SYSTEM, (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    k.a(a.this, this.f$1, (p) obj);
                }
            });
        } else if ("AdTitle".equalsIgnoreCase(str)) {
            aVar.getClass();
            rVar.a((Consumer<String>) new Consumer() {
                public final void accept(Object obj) {
                    a.this.a((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("AdTitle", new Exception("Unable to parse AdTitle value", (Exception) obj)));
                }
            });
        } else if (VastTagName.IMPRESSION.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.IMPRESSION, (b<p<Result>>) new b(list2, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    k.a(this.f$0, this.f$1, (p) obj);
                }
            });
        } else if ("Category".equalsIgnoreCase(str)) {
            rVar.a("Category", (b<p<Result>>) new b(list3, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    k.b(this.f$0, this.f$1, (p) obj);
                }
            });
        } else if ("Description".equalsIgnoreCase(str)) {
            aVar.getClass();
            rVar.a((Consumer<String>) new Consumer() {
                public final void accept(Object obj) {
                    a.this.b((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("Description", new Exception("Unable to parse Description value", (Exception) obj)));
                }
            });
        } else if ("Advertiser".equalsIgnoreCase(str)) {
            aVar.getClass();
            rVar.a((Consumer<String>) new Consumer() {
                public final void accept(Object obj) {
                    a.this.c((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("Advertiser", new Exception("Unable to parse Advertiser value", (Exception) obj)));
                }
            });
        } else if ("Error".equalsIgnoreCase(str)) {
            list4.getClass();
            rVar.a((Consumer<String>) new Consumer(list4) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, (Consumer<Exception>) new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(o.a("Error", new Exception("Unable to parse Error value", (Exception) obj)));
                }
            });
        } else if ("ViewableImpression".equalsIgnoreCase(str)) {
            rVar.a("ViewableImpression", (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    k.b(a.this, this.f$1, (p) obj);
                }
            });
        } else if (VastTagName.CREATIVES.equalsIgnoreCase(str)) {
            rVar.a(VastTagName.CREATIVES, (b<p<Result>>) new b(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    k.c(a.this, this.f$1, (p) obj);
                }
            });
        } else {
            if ("AdVerifications".equalsIgnoreCase(str)) {
                rVar.a("AdVerifications", (b<p<Result>>) new b(list) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        k.d(a.this, this.f$1, (p) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(a aVar, List list, p pVar) {
        aVar.e((List) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(a aVar, List list, p pVar) {
        aVar.d((List) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(a aVar, List list, p pVar) {
        aVar.a((ag) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(List list, List list2, p pVar) {
        if (pVar.b != null) {
            list.add(pVar.b);
        }
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, p pVar) {
        if (pVar.b != null) {
            list.add(pVar.b);
        }
        List<o> list3 = pVar.f3776a;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(a aVar, List list, p pVar) {
        aVar.a((c) pVar.b);
        List<o> list2 = pVar.f3776a;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
