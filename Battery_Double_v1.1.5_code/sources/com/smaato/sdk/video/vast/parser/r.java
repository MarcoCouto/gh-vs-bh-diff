package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.video.fi.a;
import com.smaato.sdk.video.fi.b;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class r {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private static final a<String, Boolean> f3778a = $$Lambda$r$aI2POe582bT9huDX04xlVBqNbJk.INSTANCE;
    @NonNull
    private final Map<String, ad> b;
    @NonNull
    private final XmlPullParser c;

    /* access modifiers changed from: private */
    public static /* synthetic */ String a(String str) throws Exception {
        return str;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Boolean b(String str) throws Exception {
        if (str.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        }
        if (str.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }
        StringBuilder sb = new StringBuilder("Cannot convert: \"");
        sb.append(str);
        sb.append("\" to boolean");
        throw new q(sb.toString());
    }

    public r(@NonNull XmlPullParser xmlPullParser, @NonNull Map<String, ad> map) {
        this.c = (XmlPullParser) Objects.requireNonNull(xmlPullParser);
        this.b = (Map) Objects.requireNonNull(map);
    }

    public final <T> void a(@NonNull String str, @NonNull ad<T> adVar) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(adVar);
        this.b.put(str, adVar);
    }

    @NonNull
    public final r a(@NonNull InputStream inputStream, @Nullable String str) throws XmlPullParserException, IOException {
        Objects.requireNonNull(inputStream);
        if (TextUtils.isEmpty(str) || !ae.a(str)) {
            str = null;
        }
        this.c.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        this.c.setInput(inputStream, str);
        this.c.nextTag();
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        r2 = new java.lang.StringBuilder("Exception while parsing ");
        r2.append(r5);
        r6.accept(com.smaato.sdk.video.vast.parser.p.a(r5, new java.lang.Exception(r2.toString(), r0)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0030, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:2:0x000d, B:7:0x0036] */
    @NonNull
    public final <Result> r a(@NonNull String str, @NonNull b<p<Result>> bVar) {
        Objects.requireNonNull(str);
        ad adVar = (ad) this.b.get(str);
        if (adVar == null) {
            StringBuilder sb = new StringBuilder("XmlClassParser for ");
            sb.append(str);
            sb.append(" is not found");
            bVar.accept(p.a(str, new NullPointerException(sb.toString())));
            a();
        } else {
            bVar.accept(adVar.a(this));
        }
        return this;
    }

    @NonNull
    public final r a(@NonNull Consumer<String> consumer, @NonNull Consumer<Exception> consumer2) {
        try {
            Object obj = null;
            if (this.c.next() == 4) {
                String text = this.c.getText();
                if (text != null) {
                    obj = text.trim();
                }
                this.c.nextTag();
            }
            consumer.accept(obj);
        } catch (IOException | XmlPullParserException e) {
            consumer2.accept(e);
        }
        return this;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003d A[Catch:{ IOException | XmlPullParserException -> 0x004d }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041 A[Catch:{ IOException | XmlPullParserException -> 0x004d }] */
    @NonNull
    public final r a(@NonNull String[] strArr, @NonNull Consumer<String> consumer, @NonNull Consumer<Exception> consumer2) {
        while (this.c.next() != 3) {
            try {
                boolean z = true;
                if (this.c.getEventType() == 1) {
                    throw new XmlPullParserException("XML END tag is missing");
                } else if (this.c.getEventType() == 2) {
                    String name = this.c.getName();
                    if (!TextUtils.isEmpty(name)) {
                        List asList = Arrays.asList(strArr);
                        name.getClass();
                        if (Lists.any(asList, new Predicate(name) {
                            private final /* synthetic */ String f$0;

                            {
                                this.f$0 = r1;
                            }

                            public final boolean test(Object obj) {
                                return this.f$0.equalsIgnoreCase((String) obj);
                            }
                        })) {
                            if (!z) {
                                consumer.accept(name);
                            } else {
                                a();
                            }
                        }
                    }
                    z = false;
                    if (!z) {
                    }
                }
            } catch (IOException | XmlPullParserException e) {
                consumer2.accept(e);
            }
        }
        return this;
    }

    @NonNull
    public final r a(@NonNull String str, @NonNull Consumer<String> consumer, @NonNull Consumer<o> consumer2) {
        return a(str, $$Lambda$r$kzJAaPfEszegH93QV6rDsVO90Q.INSTANCE, consumer, consumer2);
    }

    @NonNull
    public final r b(@NonNull String str, @NonNull Consumer<Integer> consumer, @NonNull Consumer<o> consumer2) {
        return a(str, $$Lambda$CaySRTa8WXoQ0kRQ5FU8eANLFH8.INSTANCE, consumer, consumer2);
    }

    @NonNull
    public final r c(@NonNull String str, @NonNull Consumer<Float> consumer, @NonNull Consumer<o> consumer2) {
        return a(str, $$Lambda$M31ywJDQELNwlOBXKjKBvxA7xo.INSTANCE, consumer, consumer2);
    }

    @NonNull
    public final r d(@NonNull String str, @NonNull Consumer<Boolean> consumer, @NonNull Consumer<o> consumer2) {
        return a(str, f3778a, consumer, consumer2);
    }

    @NonNull
    public final <Result> r a(@NonNull String str, @NonNull a<String, Result> aVar, @NonNull Consumer<Result> consumer, @NonNull Consumer<o> consumer2) {
        int attributeCount = this.c.getAttributeCount();
        Object obj = null;
        for (int i = 0; i < attributeCount; i++) {
            String attributeName = this.c.getAttributeName(i);
            if (str.equalsIgnoreCase(attributeName)) {
                obj = this.c.getAttributeValue(null, attributeName);
            }
        }
        if (obj != null) {
            try {
                consumer.accept(aVar.apply(obj));
            } catch (Exception e) {
                Objects.onNotNull(consumer2, new Consumer(str, e) {
                    private final /* synthetic */ String f$0;
                    private final /* synthetic */ Exception f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        ((Consumer) obj).accept(o.a(this.f$0, this.f$1));
                    }
                });
            }
        } else {
            StringBuilder sb = new StringBuilder("No attribute found for name: ");
            sb.append(str);
            Objects.onNotNull(consumer2, new Consumer(str, sb.toString()) {
                private final /* synthetic */ String f$0;
                private final /* synthetic */ String f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    ((Consumer) obj).accept(o.a(this.f$0, new com.smaato.sdk.video.vast.exceptions.a(this.f$1)));
                }
            });
        }
        return this;
    }

    private void a() throws XmlPullParserException, IOException {
        if (this.c.getEventType() == 2) {
            b();
            return;
        }
        throw new IllegalStateException();
    }

    private void b() throws XmlPullParserException, IOException {
        int i = 1;
        while (i != 0) {
            switch (this.c.next()) {
                case 1:
                    if (i <= 0) {
                        break;
                    } else {
                        throw new XmlPullParserException("XML END tag is missing");
                    }
                case 2:
                    i++;
                    break;
                case 3:
                    i--;
                    break;
            }
        }
    }
}
