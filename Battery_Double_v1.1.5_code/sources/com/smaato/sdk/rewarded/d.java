package com.smaato.sdk.rewarded;

import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter.Listener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.rewarded.repository.c;
import com.smaato.sdk.rewarded.repository.e;
import com.smaato.sdk.rewarded.widget.RewardedInterstitialAdActivity;

final class d extends RewardedInterstitialAd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f3554a;
    /* access modifiers changed from: private */
    @NonNull
    public final EventListener b;
    @NonNull
    private final c c;
    @NonNull
    private final RewardedAdPresenter d;
    @NonNull
    private final Logger e;
    /* access modifiers changed from: private */
    @NonNull
    public final Handler f;
    @NonNull
    private final Listener g = new Listener() {
        public final /* bridge */ /* synthetic */ void onAdImpressed(@NonNull AdPresenter adPresenter) {
        }

        public final void onStart(@NonNull RewardedAdPresenter rewardedAdPresenter) {
            Threads.ensureInvokedOnHandlerThread(d.this.f, new Runnable() {
                public final void run() {
                    AnonymousClass1.this.f();
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void f() {
            d.this.b.onAdStarted(d.this);
        }

        public final void onClose(@NonNull RewardedAdPresenter rewardedAdPresenter) {
            Threads.ensureInvokedOnHandlerThread(d.this.f, new Runnable() {
                public final void run() {
                    AnonymousClass1.this.e();
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void e() {
            d.this.b.onAdClosed(d.this);
        }

        public final void onCompleted(@NonNull RewardedAdPresenter rewardedAdPresenter) {
            Threads.ensureInvokedOnHandlerThread(d.this.f, new Runnable() {
                public final void run() {
                    AnonymousClass1.this.d();
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void d() {
            d.this.b.onAdReward(d.this);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void c() {
            d.this.b.onAdTTLExpired(d.this);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b() {
            d.this.b.onAdClicked(d.this);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a() {
            d.this.b.onAdError(d.this, RewardedError.INTERNAL_ERROR);
        }

        public final /* synthetic */ void onTTLExpired(@NonNull AdPresenter adPresenter) {
            Threads.ensureInvokedOnHandlerThread(d.this.f, new Runnable() {
                public final void run() {
                    AnonymousClass1.this.c();
                }
            });
        }

        public final /* synthetic */ void onAdError(@NonNull AdPresenter adPresenter) {
            Threads.ensureInvokedOnHandlerThread(d.this.f, new Runnable() {
                public final void run() {
                    AnonymousClass1.this.a();
                }
            });
        }

        public final /* synthetic */ void onAdClicked(@NonNull AdPresenter adPresenter) {
            Threads.ensureInvokedOnHandlerThread(d.this.f, new Runnable() {
                public final void run() {
                    AnonymousClass1.this.b();
                }
            });
        }
    };
    private boolean h = false;

    d(@NonNull Context context, @NonNull Handler handler, @NonNull Logger logger, @NonNull RewardedAdPresenter rewardedAdPresenter, @NonNull EventListener eventListener, @NonNull c cVar) {
        this.f3554a = (Context) Objects.requireNonNull(context);
        this.f = (Handler) Objects.requireNonNull(handler);
        this.e = (Logger) Objects.requireNonNull(logger);
        this.d = (RewardedAdPresenter) Objects.requireNonNull(rewardedAdPresenter);
        this.b = (EventListener) Objects.requireNonNull(eventListener);
        this.c = (c) Objects.requireNonNull(cVar);
        rewardedAdPresenter.setListener(this.g);
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.d.getAdSpaceId();
    }

    @NonNull
    public final String getSessionId() {
        return this.d.getSessionId();
    }

    @Nullable
    public final String getCreativeId() {
        return this.d.getCreativeId();
    }

    public final void setCloseButtonEnabled(boolean z) {
        Threads.runOnHandlerThreadBlocking(this.f, (Supplier<T>) new Supplier(z) {
            private final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            public final Object get() {
                return d.this.a(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Boolean a(boolean z) {
        this.h = z;
        return Boolean.valueOf(z);
    }

    public final boolean isAvailableForPresentation() {
        Handler handler = this.f;
        RewardedAdPresenter rewardedAdPresenter = this.d;
        rewardedAdPresenter.getClass();
        return ((Boolean) Threads.runOnHandlerThreadBlocking(handler, (Supplier<T>) new Supplier() {
            public final Object get() {
                return Boolean.valueOf(RewardedAdPresenter.this.isValid());
            }
        })).booleanValue();
    }

    /* access modifiers changed from: protected */
    public final void showAdInternal() {
        Threads.runOnHandlerThreadBlocking(this.f, (Runnable) new Runnable() {
            public final void run() {
                d.this.a();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        if (this.d.isValid()) {
            String uniqueKeyForType = new e(this.d.getPublisherId(), getAdSpaceId()).getUniqueKeyForType();
            this.c.a(this.d, uniqueKeyForType);
            RewardedInterstitialAdActivity.start(this.f3554a, uniqueKeyForType, this.h);
            return;
        }
        this.e.debug(LogDomain.REWARDED, "RewardedInterstitialAd is already shown on screen or expired. Please request new one.", new Object[0]);
    }
}
