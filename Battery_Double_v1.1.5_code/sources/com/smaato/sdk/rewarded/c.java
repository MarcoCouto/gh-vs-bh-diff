package com.smaato.sdk.rewarded;

import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;

final class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final com.smaato.sdk.rewarded.repository.c f3553a;

    c(@NonNull com.smaato.sdk.rewarded.repository.c cVar) {
        this.f3553a = (com.smaato.sdk.rewarded.repository.c) Objects.requireNonNull(cVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final RewardedInterstitialAd a(@NonNull Context context, @NonNull Handler handler, @NonNull Logger logger, @NonNull RewardedAdPresenter rewardedAdPresenter, @NonNull EventListener eventListener) {
        d dVar = new d(context, handler, logger, rewardedAdPresenter, eventListener, this.f3553a);
        return dVar;
    }
}
