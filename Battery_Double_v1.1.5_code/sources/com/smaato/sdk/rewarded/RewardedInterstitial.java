package com.smaato.sdk.rewarded;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.diinjection.Inject;

public final class RewardedInterstitial {

    /* renamed from: a reason: collision with root package name */
    private static final String f3550a = "RewardedInterstitial";
    @Inject
    @Nullable
    private static volatile e b;
    @Nullable
    private static String c;
    @Nullable
    private static String d;
    @Nullable
    private static String e;

    private RewardedInterstitial() {
    }

    @Nullable
    private static e a() {
        if (b == null) {
            synchronized (RewardedInterstitial.class) {
                if (b == null) {
                    AndroidsInjector.injectStatic(RewardedInterstitial.class);
                }
            }
        }
        return b;
    }

    public static void loadAd(@NonNull String str, @NonNull EventListener eventListener) {
        e a2 = a();
        if (a2 == null) {
            Log.e(f3550a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
            return;
        }
        String publisherId = SmaatoSdk.getPublisherId();
        if (TextUtils.isEmpty(publisherId)) {
            Log.e(f3550a, "Failed to proceed with RewardedInterstitial::loadAd. Missing required parameter: publisherId");
            Threads.runOnUi(new Runnable(publisherId, str) {
                private final /* synthetic */ String f$1;
                private final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    EventListener.this.onAdFailedToLoad(new RewardedRequestError(RewardedError.INVALID_REQUEST, this.f$1, this.f$2));
                }
            });
            return;
        }
        a2.a(publisherId, str, eventListener, null, c, d, e);
    }

    @Nullable
    public static KeyValuePairs getKeyValuePairs() {
        e a2 = a();
        if (a2 != null) {
            return a2.a();
        }
        Log.e(f3550a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
        return null;
    }

    public static void setKeyValuePairs(@Nullable KeyValuePairs keyValuePairs) {
        e a2 = a();
        if (a2 == null) {
            Log.e(f3550a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
        } else {
            a2.a(keyValuePairs);
        }
    }

    public static void setMediationNetworkName(@Nullable String str) {
        c = str;
    }

    public static void setMediationNetworkSDKVersion(@Nullable String str) {
        d = str;
    }

    public static void setMediationAdapterVersion(@Nullable String str) {
        e = str;
    }
}
