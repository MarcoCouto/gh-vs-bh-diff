package com.smaato.sdk.rewarded.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.ApiAdRequestExtras;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.api.AdRequestExtrasProvider;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.NullableFunction;

public final class a implements AdLoaderPlugin {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Iterable<? extends AdPresenterModuleInterface> f3559a;
    @NonNull
    private final AdRequestExtrasProvider b;
    @NonNull
    private final String c;
    private final boolean d = a(AdFormat.VIDEO);
    @NonNull
    private NullableFunction<AdPresenterModuleInterface, AdLoaderPlugin> e;

    a(@NonNull Iterable<? extends AdPresenterModuleInterface> iterable, @NonNull NullableFunction<AdPresenterModuleInterface, AdLoaderPlugin> nullableFunction, @NonNull AdRequestExtrasProvider adRequestExtrasProvider, @NonNull String str) {
        this.f3559a = (Iterable) Objects.requireNonNull(iterable);
        this.e = (NullableFunction) Objects.requireNonNull(nullableFunction);
        this.b = (AdRequestExtrasProvider) Objects.requireNonNull(adRequestExtrasProvider);
        this.c = (String) Objects.requireNonNull(str);
    }

    @Nullable
    public final AdPresenterBuilder getAdPresenterBuilder(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls, @NonNull Logger logger) {
        if (!RewardedAdPresenter.class.isAssignableFrom(cls)) {
            return null;
        }
        for (AdPresenterModuleInterface adPresenterModuleInterface : this.f3559a) {
            if (adPresenterModuleInterface.isFormatSupported(adFormat, RewardedAdPresenter.class)) {
                AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.e.apply(adPresenterModuleInterface);
                if (adLoaderPlugin != null) {
                    AdPresenterBuilder adPresenterBuilder = adLoaderPlugin.getAdPresenterBuilder(adFormat, cls, logger);
                    if (adPresenterBuilder != null) {
                        return adPresenterBuilder;
                    }
                } else {
                    continue;
                }
            }
        }
        return null;
    }

    public final void addApiAdRequestExtras(@NonNull ApiAdRequestExtras apiAdRequestExtras, @NonNull Logger logger) {
        this.b.addApiAdRequestExtras(apiAdRequestExtras);
        AdFormat adFormat = apiAdRequestExtras.adFormat();
        for (AdPresenterModuleInterface adPresenterModuleInterface : this.f3559a) {
            if (adPresenterModuleInterface.isFormatSupported(adFormat, RewardedAdPresenter.class)) {
                AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.e.apply(adPresenterModuleInterface);
                if (adLoaderPlugin != null) {
                    adLoaderPlugin.addApiAdRequestExtras(apiAdRequestExtras, logger);
                }
            }
        }
    }

    @Nullable
    public final AdFormat resolveAdFormatToServerAdFormat(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        if (adFormat != AdFormat.VIDEO) {
            for (AdPresenterModuleInterface adPresenterModuleInterface : this.f3559a) {
                if (adPresenterModuleInterface.isFormatSupported(adFormat, RewardedAdPresenter.class)) {
                    AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.e.apply(adPresenterModuleInterface);
                    if (adLoaderPlugin != null) {
                        return adLoaderPlugin.resolveAdFormatToServerAdFormat(adFormat, logger);
                    }
                }
            }
            return null;
        } else if (this.d) {
            return AdFormat.VIDEO;
        } else {
            logger.error(LogDomain.FRAMEWORK, this.c, new Object[0]);
            return null;
        }
    }

    private boolean a(@NonNull AdFormat adFormat) {
        for (AdPresenterModuleInterface isFormatSupported : this.f3559a) {
            if (isFormatSupported.isFormatSupported(adFormat, RewardedAdPresenter.class)) {
                return true;
            }
        }
        return false;
    }
}
