package com.smaato.sdk.rewarded.framework;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.rewarded.widget.RewardedInterstitialAdActivity;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

final class b {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    static final Set<String> f3560a = Collections.emptySet();
    @NonNull
    static final Set<String> b = Collections.emptySet();
    @NonNull
    static final Set<String> c = Collections.unmodifiableSet(new HashSet(Lists.of((T[]) new String[]{RewardedInterstitialAdActivity.class.getName()})));
}
