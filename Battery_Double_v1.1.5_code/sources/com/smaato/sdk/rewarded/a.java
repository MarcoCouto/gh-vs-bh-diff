package com.smaato.sdk.rewarded;

import android.app.Application;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.api.AdRequestExtrasProvider;
import com.smaato.sdk.core.api.VideoTypeAdRequestExtrasProvider;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.DiAdRepository.Provider;
import com.smaato.sdk.rewarded.repository.b;
import com.smaato.sdk.rewarded.repository.c;

public final class a {
    @NonNull
    public static DiRegistry a() {
        return DiRegistry.of($$Lambda$a$MZExo5uYYRIWOH9LVsR2c5mfSQ.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(e.class, $$Lambda$a$rE3O2Dz_x_yFSYow2vpLKtGAcc0.INSTANCE);
        diRegistry.registerSingletonFactory("RewardedAdModuleInterface", SharedKeyValuePairsHolder.class, $$Lambda$a$vF6lML15unM9dttAyzMyTkJvzac.INSTANCE);
        diRegistry.registerFactory(b.class, $$Lambda$a$DyMIoRLeQbMP0mXIgs_FgM3EeFA.INSTANCE);
        diRegistry.registerFactory(c.class, $$Lambda$a$awhXmGrA0kBnmAW24kblR2ozeiM.INSTANCE);
        diRegistry.registerFactory("RewardedAdModuleInterface", AdRequestExtrasProvider.class, $$Lambda$a$8DrI60kgkmvFtxdv9GWKToZy6bo.INSTANCE);
        diRegistry.addFrom(b.a());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ e e(DiConstructor diConstructor) {
        e eVar = new e((c) diConstructor.get(c.class), (AdRepository) ((Provider) diConstructor.get(Provider.class)).apply("RewardedAdModuleInterface"), (b) diConstructor.get(b.class), DiAdLayer.getUserInfoFactoryFrom(diConstructor), (SharedKeyValuePairsHolder) diConstructor.get("RewardedAdModuleInterface", SharedKeyValuePairsHolder.class), DiAdLayer.getFullscreenAdDimensionMapperFrom(diConstructor), (Application) diConstructor.get(Application.class), DiLogLayer.getLoggerFrom(diConstructor));
        return eVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SharedKeyValuePairsHolder d(DiConstructor diConstructor) {
        return new SharedKeyValuePairsHolder();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b c(DiConstructor diConstructor) {
        return new b();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ c b(DiConstructor diConstructor) {
        return new c((c) diConstructor.get(c.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdRequestExtrasProvider a(DiConstructor diConstructor) {
        return new VideoTypeAdRequestExtrasProvider("rewarded");
    }
}
