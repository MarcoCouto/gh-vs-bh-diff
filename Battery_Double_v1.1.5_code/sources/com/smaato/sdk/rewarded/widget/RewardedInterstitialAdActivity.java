package com.smaato.sdk.rewarded.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter.OnCloseEnabledListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.diinjection.Inject;
import com.smaato.sdk.rewarded.R;
import com.smaato.sdk.rewarded.repository.c;

public class RewardedInterstitialAdActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    private static final String f3568a = "extra_retained_cache_key";
    private static final String b = "extra_enable_close_button";
    private static final String c = "com.smaato.sdk.rewarded.widget.RewardedInterstitialAdActivity";
    @Nullable
    private RewardedAdPresenter d;
    @Inject
    @Nullable
    private c e;
    @Inject
    @Nullable
    private Logger f;
    /* access modifiers changed from: private */
    public boolean g;
    @NonNull
    private final OnCloseEnabledListener h = new OnCloseEnabledListener() {
        public final void onCloseEnabled(@NonNull RewardedAdPresenter rewardedAdPresenter) {
            RewardedInterstitialAdActivity.this.findViewById(R.id.smaato_sdk_rewarded_ads_close).setVisibility(0);
            RewardedInterstitialAdActivity.this.g = true;
        }

        public final void onClose(@NonNull RewardedAdPresenter rewardedAdPresenter) {
            RewardedInterstitialAdActivity.this.finish();
        }
    };

    public static void start(@NonNull Context context, @NonNull String str, boolean z) {
        Objects.requireNonNull(str);
        context.startActivity(new Intent(context, RewardedInterstitialAdActivity.class).addFlags(268435456).putExtra(f3568a, str).putExtra(b, z));
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        AndroidsInjector.inject((Activity) this);
        if (this.e == null) {
            a("SmaatoSdk is not initialised.");
            return;
        }
        this.g = getIntent().getBooleanExtra(b, false);
        this.d = a(this.e);
        if (this.d != null) {
            a(this.d);
        } else {
            a("Video Ad is not available for presentation");
        }
    }

    @Nullable
    private RewardedAdPresenter a(@NonNull c cVar) {
        String stringExtra = getIntent().getStringExtra(f3568a);
        if (TextUtils.isEmpty(stringExtra)) {
            return null;
        }
        return cVar.a(stringExtra);
    }

    private void a(@NonNull RewardedAdPresenter rewardedAdPresenter) {
        AdContentView adContentView = rewardedAdPresenter.getAdContentView(this);
        if (adContentView != null) {
            a(adContentView);
            rewardedAdPresenter.setOnCloseEnabledListener(this.h);
            return;
        }
        a("Video Ad is not available to provide View for presentation");
    }

    private void a(@Nullable AdContentView adContentView) {
        setContentView(R.layout.smaato_sdk_rewarded_ads_activity);
        ((FrameLayout) findViewById(R.id.smaato_sdk_rewarded_ads_content)).addView(adContentView);
        View findViewById = findViewById(R.id.smaato_sdk_rewarded_ads_close);
        findViewById.setVisibility(this.g ? 0 : 8);
        findViewById.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                RewardedInterstitialAdActivity.this.a(view);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(View view) {
        a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (!isChangingConfigurations() && this.d != null) {
            this.d.destroy();
        }
        super.onDestroy();
    }

    private void a(@NonNull String str) {
        if (this.d != null) {
            this.d.destroy();
        }
        b(str);
        finish();
    }

    private void b(@NonNull String str) {
        if (this.f != null) {
            this.f.error(LogDomain.WIDGET, str, new Object[0]);
        } else {
            Log.e(c, str);
        }
    }

    public void onBackPressed() {
        if (this.g) {
            a();
        }
    }

    private void a() {
        Objects.onNotNull(this.d, $$Lambda$MyDgG483o8NnGe00daRutl3T5ds.INSTANCE);
        finish();
    }
}
