package com.smaato.sdk.rewarded;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class RewardedInterstitialAd {
    @NonNull
    public abstract String getAdSpaceId();

    @Nullable
    public abstract String getCreativeId();

    @NonNull
    public abstract String getSessionId();

    public abstract boolean isAvailableForPresentation();

    public abstract void setCloseButtonEnabled(boolean z);

    /* access modifiers changed from: protected */
    public abstract void showAdInternal();

    public final void showAd() {
        showAdInternal();
    }
}
