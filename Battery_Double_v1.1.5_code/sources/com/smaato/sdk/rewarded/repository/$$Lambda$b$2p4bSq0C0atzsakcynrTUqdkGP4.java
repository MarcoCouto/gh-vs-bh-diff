package com.smaato.sdk.rewarded.repository;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.rewarded.repository.-$$Lambda$b$2p4bSq0C0atzsakcynrTUqdkGP4 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$b$2p4bSq0C0atzsakcynrTUqdkGP4 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$b$2p4bSq0C0atzsakcynrTUqdkGP4 INSTANCE = new $$Lambda$b$2p4bSq0C0atzsakcynrTUqdkGP4();

    private /* synthetic */ $$Lambda$b$2p4bSq0C0atzsakcynrTUqdkGP4() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newSingleThreadExecutor();
    }
}
