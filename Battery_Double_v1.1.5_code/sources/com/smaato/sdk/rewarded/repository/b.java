package com.smaato.sdk.rewarded.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.config.Configuration;
import com.smaato.sdk.core.config.ConfigurationRepository;
import com.smaato.sdk.core.config.DiConfiguration.Provider;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.repository.AdLoadersRegistry;
import com.smaato.sdk.core.repository.AdPresenterCache;
import com.smaato.sdk.core.repository.MultipleAdLoadersRegistry;
import com.smaato.sdk.core.repository.MultipleAdPresenterCache;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private static final State f3562a = State.IMPRESSED;

    @NonNull
    public static DiRegistry a() {
        return DiRegistry.of($$Lambda$b$sjUNn6oP_SszxBRywbZoyjmwjE.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(c.class, $$Lambda$b$2AjCypfLcPc1N7Lmc53NncmJKvk.INSTANCE);
        diRegistry.registerFactory("RewardedAdModuleInterface.RewardedAdLoadExecutioner", ExecutorService.class, $$Lambda$b$2p4bSq0C0atzsakcynrTUqdkGP4.INSTANCE);
        diRegistry.registerSingletonFactory("RewardedAdModuleInterface", AdPresenterCache.class, $$Lambda$b$nAnrUIGEB8WATQWeOCrM7kLUrrc.INSTANCE);
        diRegistry.registerSingletonFactory("RewardedAdModuleInterface", AdLoadersRegistry.class, $$Lambda$b$66W80NqKi308nAVQokYjI8tnXdg.INSTANCE);
        diRegistry.registerSingletonFactory("RewardedAdModuleInterface", ConfigurationRepository.class, $$Lambda$b$8qYmYoGypMutMi5SxBumrdLh4Ck.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ c e(DiConstructor diConstructor) {
        return new c(new HashMap());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterCache c(DiConstructor diConstructor) {
        return new MultipleAdPresenterCache((ConfigurationRepository) diConstructor.get("RewardedAdModuleInterface", ConfigurationRepository.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoadersRegistry b(DiConstructor diConstructor) {
        return new MultipleAdLoadersRegistry((ConfigurationRepository) diConstructor.get("RewardedAdModuleInterface", ConfigurationRepository.class), DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConfigurationRepository a(DiConstructor diConstructor) {
        return (ConfigurationRepository) ((Provider) diConstructor.get(Provider.class)).apply(new Configuration(1, f3562a));
    }
}
