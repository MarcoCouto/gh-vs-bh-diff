package com.smaato.sdk.rewarded.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.repository.AdTypeStrategy;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import io.fabric.sdk.android.services.events.EventsFilesManager;

public final class e implements AdTypeStrategy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3567a;
    @NonNull
    private final String b;

    public e(@NonNull String str, @NonNull String str2) {
        this.f3567a = (String) Objects.requireNonNull(str);
        this.b = (String) Objects.requireNonNull(str2);
    }

    @NonNull
    public final String getUniqueKeyForType() {
        return Joiner.join((CharSequence) EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, this.f3567a, this.b);
    }

    @NonNull
    public final Class<? extends AdPresenter> getAdPresenterClass() {
        return RewardedAdPresenter.class;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        if (!this.f3567a.equals(eVar.f3567a)) {
            return false;
        }
        return this.b.equals(eVar.b);
    }

    public final int hashCode() {
        return (this.f3567a.hashCode() * 31) + this.b.hashCode();
    }
}
