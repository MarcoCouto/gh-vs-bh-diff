package com.smaato.sdk.rewarded.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.util.Objects;
import java.util.Map;

public class c {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Map<String, a<RewardedAdPresenter>> f3563a;

    c(@NonNull Map<String, a<RewardedAdPresenter>> map) {
        this.f3563a = (Map) Objects.requireNonNull(map);
    }

    @Nullable
    public final synchronized RewardedAdPresenter a(@NonNull String str) {
        a aVar = (a) this.f3563a.get(str);
        if (aVar == null) {
            return null;
        }
        return (RewardedAdPresenter) aVar.a();
    }

    @NonNull
    public final synchronized String a(@NonNull RewardedAdPresenter rewardedAdPresenter, @NonNull final String str) {
        AnonymousClass1 r0 = new d(rewardedAdPresenter) {
            public final void a(@NonNull RewardedAdPresenter rewardedAdPresenter) {
                rewardedAdPresenter.getAdInteractor().removeStateListener(this);
                c.this.b(str);
            }
        };
        rewardedAdPresenter.getAdInteractor().addStateListener(r0);
        this.f3563a.put(str, new a(rewardedAdPresenter, r0));
        return str;
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void b(@NonNull String str) {
        this.f3563a.remove(str);
    }
}
