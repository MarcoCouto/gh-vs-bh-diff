package com.smaato.sdk.rewarded.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine.Listener;

public abstract class d implements Listener<State> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final RewardedAdPresenter f3565a;

    /* access modifiers changed from: 0000 */
    public abstract void a(@NonNull RewardedAdPresenter rewardedAdPresenter);

    public /* synthetic */ void onStateChanged(@NonNull Object obj, @NonNull Object obj2, @Nullable Metadata metadata) {
        State state = (State) obj2;
        switch (state) {
            case INIT:
            case CREATED:
            case ON_SCREEN:
            case IMPRESSED:
                return;
            case TO_BE_DELETED:
                a(this.f3565a);
                return;
            default:
                throw new IllegalArgumentException(String.format("Unexpected AdStateMachine.State: %s", new Object[]{state}));
        }
    }

    d(@NonNull RewardedAdPresenter rewardedAdPresenter) {
        this.f3565a = (RewardedAdPresenter) Objects.requireNonNull(rewardedAdPresenter);
    }
}
