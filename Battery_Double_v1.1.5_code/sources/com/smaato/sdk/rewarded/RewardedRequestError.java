package com.smaato.sdk.rewarded;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;

public final class RewardedRequestError {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f3551a;
    @Nullable
    private final String b;
    @NonNull
    private final RewardedError c;

    RewardedRequestError(@NonNull RewardedError rewardedError, @Nullable String str, @Nullable String str2) {
        this.c = (RewardedError) Objects.requireNonNull(rewardedError);
        this.f3551a = str;
        this.b = str2;
    }

    @Nullable
    public final String getPublisherId() {
        return this.f3551a;
    }

    @Nullable
    public final String getAdSpaceId() {
        return this.b;
    }

    @NonNull
    public final RewardedError getRewardedError() {
        return this.c;
    }
}
