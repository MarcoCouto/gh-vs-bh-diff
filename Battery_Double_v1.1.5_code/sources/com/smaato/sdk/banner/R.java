package com.smaato.sdk.banner;

public final class R {

    public static final class color {
        public static final int smaato_sdk_core_ui_ctrl_almost_white = 2131099828;
        public static final int smaato_sdk_core_ui_ctrl_black = 2131099829;
        public static final int smaato_sdk_core_ui_ctrl_grey = 2131099830;
        public static final int smaato_sdk_core_ui_semitransparent = 2131099831;
        public static final int smaato_sdk_richmedia_ui_semitransparent = 2131099832;

        private color() {
        }
    }

    public static final class dimen {
        public static final int smaato_sdk_core_activity_margin = 2131165384;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int smaato_sdk_core_back = 2131231150;
        public static final int smaato_sdk_core_back_disabled = 2131231151;
        public static final int smaato_sdk_core_background = 2131231152;
        public static final int smaato_sdk_core_browser_bottom_button_layout_bg = 2131231153;
        public static final int smaato_sdk_core_browser_progress_bar = 2131231154;
        public static final int smaato_sdk_core_browser_top_button_layout_bg = 2131231155;
        public static final int smaato_sdk_core_circle_close = 2131231156;
        public static final int smaato_sdk_core_close = 2131231157;
        public static final int smaato_sdk_core_forward = 2131231158;
        public static final int smaato_sdk_core_forward_disabled = 2131231159;
        public static final int smaato_sdk_core_ic_browser_background_selector = 2131231160;
        public static final int smaato_sdk_core_ic_browser_backward_selector = 2131231161;
        public static final int smaato_sdk_core_ic_browser_forward_selector = 2131231162;
        public static final int smaato_sdk_core_ic_browser_secure_connection = 2131231163;
        public static final int smaato_sdk_core_lock = 2131231164;
        public static final int smaato_sdk_core_open_in_browser = 2131231165;
        public static final int smaato_sdk_core_refresh = 2131231166;
        public static final int smaato_sdk_richmedia_progress_bar = 2131231167;

        private drawable() {
        }
    }

    public static final class id {
        public static final int btnBackward = 2131296342;
        public static final int btnClose = 2131296343;
        public static final int btnForward = 2131296344;
        public static final int btnLayoutBottom = 2131296345;
        public static final int btnLayoutTop = 2131296346;
        public static final int btnOpenExternal = 2131296347;
        public static final int btnRefresh = 2131296348;
        public static final int close = 2131296366;
        public static final int container = 2131296379;
        public static final int progressBar = 2131296587;
        public static final int tvHostname = 2131296696;
        public static final int webView = 2131296707;

        private id() {
        }
    }

    public static final class layout {
        public static final int smaato_sdk_core_activity_internal_browser = 2131427447;
        public static final int smaato_sdk_richmedia_layout_closable = 2131427450;

        private layout() {
        }
    }

    public static final class string {
        public static final int smaato_sdk_core_browser_hostname_content_description = 2131624221;
        public static final int smaato_sdk_core_btn_browser_backward_content_description = 2131624222;
        public static final int smaato_sdk_core_btn_browser_close_content_description = 2131624223;
        public static final int smaato_sdk_core_btn_browser_forward_content_description = 2131624224;
        public static final int smaato_sdk_core_btn_browser_open_content_description = 2131624225;
        public static final int smaato_sdk_core_btn_browser_refresh_content_description = 2131624226;
        public static final int smaato_sdk_core_fullscreen_dimension = 2131624227;
        public static final int smaato_sdk_core_no_external_browser_found = 2131624228;
        public static final int smaato_sdk_richmedia_collapse_mraid_ad = 2131624229;

        private string() {
        }
    }

    public static final class style {
        public static final int smaato_sdk_core_browserProgressBar = 2131689898;

        private style() {
        }
    }

    private R() {
    }
}
