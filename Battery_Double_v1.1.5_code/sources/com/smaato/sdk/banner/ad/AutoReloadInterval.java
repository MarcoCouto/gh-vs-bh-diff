package com.smaato.sdk.banner.ad;

import com.google.android.exoplayer2.extractor.ts.PsExtractor;

public enum AutoReloadInterval {
    DISABLED(0),
    DEFAULT(60),
    VERY_SHORT(10),
    SHORT(30),
    NORMAL(60),
    LONG(120),
    VERY_LONG(PsExtractor.VIDEO_STREAM_MASK);
    

    /* renamed from: a reason: collision with root package name */
    private final int f3262a;

    private AutoReloadInterval(int i) {
        this.f3262a = i;
    }

    public final int getSeconds() {
        return this.f3262a;
    }
}
