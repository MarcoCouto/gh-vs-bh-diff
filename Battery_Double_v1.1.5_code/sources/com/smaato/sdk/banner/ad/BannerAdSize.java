package com.smaato.sdk.banner.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdDimension;
import com.smaato.sdk.core.util.Objects;

public enum BannerAdSize {
    XX_LARGE_320x50(AdDimension.XX_LARGE),
    MEDIUM_RECTANGLE_300x250(AdDimension.MEDIUM_RECTANGLE),
    LEADERBOARD_728x90(AdDimension.LEADERBOARD),
    SKYSCRAPER_120x600(AdDimension.SKYSCRAPER);
    
    @NonNull
    public final AdDimension adDimension;

    private BannerAdSize(AdDimension adDimension2) {
        this.adDimension = (AdDimension) Objects.requireNonNull(adDimension2);
    }
}
