package com.smaato.sdk.banner.ad;

import com.smaato.sdk.core.ad.AutoReloadConfig;

public class BannerAutoReloadConfig implements AutoReloadConfig {

    /* renamed from: a reason: collision with root package name */
    private static final int f3264a = 10;
    private static final int b = 240;
    private static final int c = AutoReloadInterval.DEFAULT.getSeconds();

    public int maxInterval() {
        return 240;
    }

    public int minInterval() {
        return 10;
    }

    public int defaultInterval() {
        return c;
    }
}
