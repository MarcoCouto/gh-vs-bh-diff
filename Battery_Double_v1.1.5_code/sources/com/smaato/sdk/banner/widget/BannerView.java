package com.smaato.sdk.banner.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.banner.ad.AutoReloadInterval;
import com.smaato.sdk.banner.ad.BannerAdSize;
import com.smaato.sdk.banner.widget.BannerView.EventListener;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.diinjection.Inject;
import com.smaato.sdk.core.util.fi.Consumer;

public class BannerView extends FrameLayout {

    /* renamed from: a reason: collision with root package name */
    private static final String f3268a = "BannerView";
    @NonNull
    private final Handler b = Threads.newUiHandler();
    /* access modifiers changed from: private */
    @Inject
    public c c;
    @Inject
    private Logger d;
    /* access modifiers changed from: private */
    public boolean e = false;
    @Nullable
    private EventListener f;
    @Nullable
    private String g;
    @Nullable
    private String h;
    @Nullable
    private String i;

    public interface EventListener {
        void onAdClicked(@NonNull BannerView bannerView);

        void onAdFailedToLoad(@NonNull BannerView bannerView, @NonNull BannerError bannerError);

        void onAdImpression(@NonNull BannerView bannerView);

        void onAdLoaded(@NonNull BannerView bannerView);

        void onAdTTLExpired(@NonNull BannerView bannerView);
    }

    public BannerView(@NonNull Context context) {
        super(context);
        e();
    }

    public BannerView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        e();
    }

    public BannerView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        e();
    }

    @TargetApi(21)
    public BannerView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        e();
    }

    private void e() {
        AndroidsInjector.inject((View) this);
        if (this.c == null) {
            Log.e(f3268a, "SmaatoSdk is not initialized.");
        } else {
            this.c.a(this);
        }
    }

    public void setEventListener(@Nullable EventListener eventListener) {
        this.f = eventListener;
    }

    @Nullable
    public String getSessionId() {
        if (f()) {
            return this.c.c();
        }
        return null;
    }

    @Nullable
    public String getCreativeId() {
        if (f()) {
            return this.c.d();
        }
        return null;
    }

    @Nullable
    public String getAdSpaceId() {
        if (f()) {
            b a2 = this.c.a();
            if (a2 != null) {
                return a2.b;
            }
        }
        return null;
    }

    @Nullable
    public BannerAdSize getBannerAdSize() {
        if (f()) {
            b a2 = this.c.a();
            if (a2 != null) {
                return a2.c;
            }
        }
        return null;
    }

    public void loadAd(@NonNull String str, @Nullable BannerAdSize bannerAdSize) {
        if (f()) {
            b bVar = new b(SmaatoSdk.getPublisherId(), str, bannerAdSize, this.g, this.h, this.i);
            Objects.onNotNull(this.c, new Consumer() {
                public final void accept(Object obj) {
                    ((c) obj).a(b.this);
                }
            });
        }
    }

    public void setMediationNetworkName(@Nullable String str) {
        this.g = str;
    }

    public void setMediationNetworkSDKVersion(@Nullable String str) {
        this.h = str;
    }

    public void setMediationAdapterVersion(@Nullable String str) {
        this.i = str;
    }

    @Nullable
    public KeyValuePairs getKeyValuePairs() {
        if (!f()) {
            return null;
        }
        return this.c.g();
    }

    public void setKeyValuePairs(@Nullable KeyValuePairs keyValuePairs) {
        if (f()) {
            this.c.a(keyValuePairs);
        }
    }

    private boolean f() {
        if (this.c != null) {
            return true;
        }
        Log.e(f3268a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
        return false;
    }

    @Nullable
    public AutoReloadInterval getAutoReloadInterval() {
        AutoReloadInterval[] values;
        if (f()) {
            int b2 = this.c.b();
            for (AutoReloadInterval autoReloadInterval : AutoReloadInterval.values()) {
                if (autoReloadInterval.getSeconds() == b2) {
                    return autoReloadInterval;
                }
            }
        }
        return null;
    }

    public void setAutoReloadInterval(@NonNull AutoReloadInterval autoReloadInterval) {
        if (autoReloadInterval == null) {
            Log.e(f3268a, "bannerAutoReloadInterval can not be null");
            return;
        }
        if (f()) {
            Objects.onNotNull(this.c, new Consumer() {
                public final void accept(Object obj) {
                    ((c) obj).a(AutoReloadInterval.this.getSeconds());
                }
            });
        }
    }

    public void destroy() {
        Objects.onNotNull(this.c, $$Lambda$T8QDVp1l2p5bqRDK73npA1L4fKY.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.e) {
            removeAllViews();
            Objects.onNotNull(this.c, $$Lambda$BXF86NVpU8XqwsdUULOTUozwgH4.INSTANCE);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        Threads.ensureInvokedOnHandlerThread(this.b, new Runnable() {
            public final void run() {
                BannerView.this.j();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(EventListener eventListener) {
        eventListener.onAdLoaded(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void j() {
        Objects.onNotNull(this.f, new Consumer() {
            public final void accept(Object obj) {
                BannerView.this.d((EventListener) obj);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        Threads.ensureInvokedOnHandlerThread(this.b, new Runnable() {
            public final void run() {
                BannerView.this.i();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(EventListener eventListener) {
        eventListener.onAdClicked(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void i() {
        Objects.onNotNull(this.f, new Consumer() {
            public final void accept(Object obj) {
                BannerView.this.c((EventListener) obj);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull BannerError bannerError) {
        Threads.ensureInvokedOnHandlerThread(this.b, new Runnable(bannerError) {
            private final /* synthetic */ BannerError f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                BannerView.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(BannerError bannerError) {
        this.d.error(LogDomain.WIDGET, "Error loading ad. %s", bannerError);
        Objects.onNotNull(this.f, new Consumer(bannerError) {
            private final /* synthetic */ BannerError f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                BannerView.this.a(this.f$1, (EventListener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(BannerError bannerError, EventListener eventListener) {
        eventListener.onAdFailedToLoad(this, bannerError);
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        Threads.ensureInvokedOnHandlerThread(this.b, new Runnable() {
            public final void run() {
                BannerView.this.h();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(EventListener eventListener) {
        eventListener.onAdImpression(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void h() {
        Objects.onNotNull(this.f, new Consumer() {
            public final void accept(Object obj) {
                BannerView.this.b((EventListener) obj);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        Threads.ensureInvokedOnHandlerThread(this.b, new Runnable() {
            public final void run() {
                BannerView.this.g();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(EventListener eventListener) {
        eventListener.onAdTTLExpired(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void g() {
        Objects.onNotNull(this.f, new Consumer() {
            public final void accept(Object obj) {
                BannerView.this.a((EventListener) obj);
            }
        });
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        Objects.onNotNull(this.c, new Consumer(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((c) obj).a(this.f$0);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull AdPresenter adPresenter) {
        this.b.post(new Runnable(adPresenter) {
            private final /* synthetic */ AdPresenter f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                BannerView.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(AdPresenter adPresenter) {
        removeAllViews();
        final AdContentView adContentView = adPresenter.getAdContentView(getContext());
        if (adContentView != null) {
            adContentView.setVisibility(4);
            addView(adContentView);
            getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
                public final boolean onPreDraw() {
                    BannerView.this.getViewTreeObserver().removeOnPreDrawListener(this);
                    int measuredWidth = BannerView.this.getMeasuredWidth();
                    int measuredHeight = BannerView.this.getMeasuredHeight();
                    LayoutParams layoutParams = adContentView.getLayoutParams();
                    int i = layoutParams.width;
                    int i2 = layoutParams.height;
                    if (i > measuredWidth || i2 > measuredHeight) {
                        BannerView.this.removeAllViews();
                        Objects.onNotNull(BannerView.this.c, new Consumer(i, i2, measuredWidth, measuredHeight) {
                            private final /* synthetic */ int f$0;
                            private final /* synthetic */ int f$1;
                            private final /* synthetic */ int f$2;
                            private final /* synthetic */ int f$3;

                            {
                                this.f$0 = r1;
                                this.f$1 = r2;
                                this.f$2 = r3;
                                this.f$3 = r4;
                            }

                            public final void accept(Object obj) {
                                ((c) obj).a(this.f$0, this.f$1, this.f$2, this.f$3);
                            }
                        });
                    } else {
                        BannerView.this.e = true;
                        adContentView.setVisibility(0);
                        BannerView.this.setWillNotDraw(false);
                    }
                    return true;
                }
            });
        }
    }
}
