package com.smaato.sdk.banner.widget;

import androidx.annotation.NonNull;

public enum BannerError {
    NO_AD_AVAILABLE("No ad is currently available matching the requested parameters. Please try again later."),
    INVALID_REQUEST("Invalid ad request (e.g. PublisherId or AdSpaceId is incorrect)."),
    NETWORK_ERROR("The ad request has not been completed due to a network connectivity issue."),
    INTERNAL_ERROR("An internal error happened (e.g. the ad server responded with an invalid response)."),
    CREATIVE_RESOURCE_EXPIRED("A creative resource's TTL expired."),
    AD_UNLOADED("RichMedia ad requested to be unloaded.");
    
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private String f3267a;

    private BannerError(String str) {
        this.f3267a = str;
    }

    public final String toString() {
        return String.format("[%s]: %s", new Object[]{name(), this.f3267a});
    }
}
