package com.smaato.sdk.banner.widget;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.banner.ad.BannerAdSize;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.repository.AdTypeStrategy;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.TextUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.Serializable;
import java.util.Arrays;

final class a implements AdTypeStrategy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3270a;
    @NonNull
    private final String b;
    @Nullable
    private final BannerAdSize c;

    private a(@NonNull String str, @NonNull String str2, @Nullable BannerAdSize bannerAdSize) {
        this.f3270a = str;
        this.b = str2;
        this.c = bannerAdSize;
    }

    @NonNull
    public final Class<? extends AdPresenter> getAdPresenterClass() {
        return BannerAdPresenter.class;
    }

    @Nullable
    static a a(@NonNull String str, @NonNull String str2, @Nullable BannerAdSize bannerAdSize) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return null;
        }
        return new a(str, str2, bannerAdSize);
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return this.f3270a.equals(aVar.f3270a) && this.b.equals(aVar.b) && this.c == aVar.c;
    }

    public final int hashCode() {
        return (((this.f3270a.hashCode() * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0);
    }

    @NonNull
    public final String getUniqueKeyForType() {
        return Joiner.join((CharSequence) EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, (Iterable) Arrays.asList(this.c == null ? new String[]{this.f3270a, this.b} : new Serializable[]{this.f3270a, this.b, Integer.valueOf(this.c.ordinal())}));
    }
}
