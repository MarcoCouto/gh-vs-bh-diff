package com.smaato.sdk.banner.widget;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.banner.ad.BannerAdSize;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdRequest;
import com.smaato.sdk.core.ad.AdSettings;
import com.smaato.sdk.core.ad.AdSettings.Builder;
import com.smaato.sdk.core.ad.AutoReloadPolicy;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.ad.UserInfo;
import com.smaato.sdk.core.ad.UserInfoSupplier;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkStateMonitor;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.AdTypeStrategy;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.NullableSupplier;
import com.smaato.sdk.core.util.memory.LeakProtection;
import com.smaato.sdk.core.util.notifier.ChangeNotifier.Listener;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

public final class c {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Logger f3272a;
    /* access modifiers changed from: private */
    @NonNull
    public final AutoReloadPolicy b;
    @NonNull
    private final AppBackgroundAwareHandler c;
    @NonNull
    private final UserInfoSupplier d;
    @NonNull
    private final NetworkStateMonitor e;
    @NonNull
    private final LeakProtection f;
    @NonNull
    private final AdRepository g;
    @NonNull
    private final SharedKeyValuePairsHolder h;
    @NonNull
    private AtomicReference<Runnable> i = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public AtomicReference<Runnable> j = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<AdPresenter> k = new WeakReference<>(null);
    @Nullable
    private Listener<Boolean> l;
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<BannerView> m = new WeakReference<>(null);
    /* access modifiers changed from: private */
    @Nullable
    public b n;
    /* access modifiers changed from: private */
    @Nullable
    public a o;
    @NonNull
    private final AdRepository.Listener p = new AdRepository.Listener() {
        public final void onAdLoadSuccess(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdPresenter adPresenter) {
            if (adPresenter instanceof BannerAdPresenter) {
                Threads.runOnUi(new Runnable((BannerAdPresenter) adPresenter) {
                    private final /* synthetic */ BannerAdPresenter f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void run() {
                        AnonymousClass1.this.a(this.f$1);
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(BannerAdPresenter bannerAdPresenter) {
            Objects.onNotNull(c.this.m.get(), $$Lambda$yqHc2JzDvjC4zMGnrUn3K26B4Yg.INSTANCE);
            c.this.k = new WeakReference(bannerAdPresenter);
            c.this.o = new a(c.this, 0);
            bannerAdPresenter.setListener(c.this.o);
            bannerAdPresenter.initialize();
            Objects.onNotNull(c.this.m.get(), new Consumer() {
                public final void accept(Object obj) {
                    ((BannerView) obj).a((AdPresenter) BannerAdPresenter.this);
                }
            });
        }

        public final void onAdLoadError(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdLoaderException adLoaderException) {
            Threads.runOnUi(new Runnable(adLoaderException) {
                private final /* synthetic */ AdLoaderException f$1;

                {
                    this.f$1 = r2;
                }

                public final void run() {
                    AnonymousClass1.this.a(this.f$1);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(AdLoaderException adLoaderException) {
            BannerError bannerError;
            Error errorType = adLoaderException.getErrorType();
            if (errorType == Error.CANCELLED) {
                c.this.f3272a.info(LogDomain.WIDGET, "Ignoring AdLoader adLoaderException: %s", adLoaderException);
                return;
            }
            Objects.requireNonNull(errorType, "Parameter adLoaderError cannot be null for BannerErrorMapperUtil::map");
            switch (AnonymousClass1.f3271a[errorType.ordinal()]) {
                case 1:
                    bannerError = BannerError.NO_AD_AVAILABLE;
                    break;
                case 2:
                    bannerError = BannerError.INVALID_REQUEST;
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    bannerError = BannerError.INTERNAL_ERROR;
                    break;
                case 9:
                case 10:
                    bannerError = BannerError.NETWORK_ERROR;
                    break;
                case 11:
                    bannerError = BannerError.CREATIVE_RESOURCE_EXPIRED;
                    break;
                default:
                    bannerError = BannerError.INTERNAL_ERROR;
                    break;
            }
            Objects.onNotNull(c.this.m.get(), new Consumer() {
                public final void accept(Object obj) {
                    ((BannerView) obj).a(BannerError.this);
                }
            });
            c.a(c.this, errorType);
        }
    };
    /* access modifiers changed from: private */
    @NonNull
    public final Runnable q = new Runnable() {
        public final void run() {
            c.this.a(c.this.n);
        }
    };

    private class a implements BannerAdPresenter.Listener {
        private a() {
        }

        /* synthetic */ a(c cVar, byte b) {
            this();
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void e() {
            Objects.onNotNull(c.this.m.get(), $$Lambda$6pvaFapotc797IUO_tpZnMhOAjg.INSTANCE);
            c.this.a(c.this.q);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void d() {
            Objects.onNotNull(c.this.m.get(), $$Lambda$8iLCwlChtkJahuWHP31sIuBDG6g.INSTANCE);
            if (c.this.b.isAutoReloadEnabled()) {
                c.this.j.set(c.this.q);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void onAdError(@NonNull BannerAdPresenter bannerAdPresenter) {
            Threads.runOnUi(new Runnable(bannerAdPresenter) {
                private final /* synthetic */ BannerAdPresenter f$1;

                {
                    this.f$1 = r2;
                }

                public final void run() {
                    a.this.b(this.f$1);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b(BannerAdPresenter bannerAdPresenter) {
            Objects.onNotNull(c.this.k.get(), new Consumer(bannerAdPresenter) {
                private final /* synthetic */ BannerAdPresenter f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    a.this.a(this.f$1, (AdPresenter) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(BannerAdPresenter bannerAdPresenter, AdPresenter adPresenter) {
            if (adPresenter == bannerAdPresenter) {
                Objects.onNotNull(c.this.n, new Consumer() {
                    public final void accept(Object obj) {
                        c.this.a((b) obj);
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void c() {
            Objects.onNotNull(c.this.k.get(), new Consumer() {
                public final void accept(Object obj) {
                    a.this.a((AdPresenter) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(AdPresenter adPresenter) {
            Objects.onNotNull(c.this.m.get(), $$Lambda$o12ZmfRSmYPWpsUJeY1GpnbVwM.INSTANCE);
        }

        public final void onAdExpanded(@NonNull BannerAdPresenter bannerAdPresenter) {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    c.this.i();
                }
            });
        }

        public final void onAdUnload(@NonNull BannerAdPresenter bannerAdPresenter) {
            if (TextUtils.isEmpty(c.this.n == null ? null : c.this.n.d)) {
                onAdError(bannerAdPresenter);
            } else {
                Threads.runOnUi(new Runnable() {
                    public final void run() {
                        a.this.b();
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b() {
            Objects.onNotNull(c.this.m.get(), $$Lambda$c$a$MsWtqfTWdpvzDweXPKtaGgFF53k.INSTANCE);
        }

        public final void onAdClosed() {
            c.this.a(c.this.q);
        }

        public final void onAdResized() {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    a.this.a();
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a() {
            c.this.j.set(null);
            c.this.i();
        }

        public final /* synthetic */ void onTTLExpired(@NonNull AdPresenter adPresenter) {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    a.this.c();
                }
            });
        }

        public final /* synthetic */ void onAdClicked(@NonNull AdPresenter adPresenter) {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    a.this.d();
                }
            });
        }

        public final /* synthetic */ void onAdImpressed(@NonNull AdPresenter adPresenter) {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    a.this.e();
                }
            });
        }
    }

    static final class b {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        final String f3277a;
        @Nullable
        final String b;
        @Nullable
        final BannerAdSize c;
        @Nullable
        final String d;
        @Nullable
        final String e;
        @Nullable
        final String f;

        b(@Nullable String str, @Nullable String str2, @Nullable BannerAdSize bannerAdSize, @Nullable String str3, @Nullable String str4, @Nullable String str5) {
            this.f3277a = str;
            this.b = str2;
            this.c = bannerAdSize;
            this.d = str3;
            this.e = str4;
            this.f = str5;
        }
    }

    public c(@NonNull Logger logger, @NonNull AutoReloadPolicy autoReloadPolicy, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler, @NonNull UserInfoSupplier userInfoSupplier, @NonNull NetworkStateMonitor networkStateMonitor, @NonNull LeakProtection leakProtection, @NonNull AdRepository adRepository, @NonNull SharedKeyValuePairsHolder sharedKeyValuePairsHolder) {
        this.f3272a = (Logger) Objects.requireNonNull(logger);
        this.b = (AutoReloadPolicy) Objects.requireNonNull(autoReloadPolicy);
        this.c = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
        this.d = (UserInfoSupplier) Objects.requireNonNull(userInfoSupplier);
        this.e = (NetworkStateMonitor) Objects.requireNonNull(networkStateMonitor);
        this.f = (LeakProtection) Objects.requireNonNull(leakProtection);
        this.g = (AdRepository) Objects.requireNonNull(adRepository);
        this.h = (SharedKeyValuePairsHolder) Objects.requireNonNull(sharedKeyValuePairsHolder);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ b o() {
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final b a() {
        return (b) Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier() {
            public final Object get() {
                return c.this.o();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull BannerView bannerView) {
        Objects.requireNonNull(bannerView, "Parameter bannerView cannot be null for BannerViewLoader::setView");
        Threads.runOnUiBlocking((Runnable) new Runnable(bannerView) {
            private final /* synthetic */ BannerView f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                c.this.e(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(BannerView bannerView) {
        this.m = new WeakReference<>(bannerView);
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        Threads.runOnUiBlocking((Runnable) new Runnable(z) {
            private final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                c.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(boolean z) {
        if (z) {
            Runnable runnable = (Runnable) this.j.get();
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull b bVar) {
        Objects.requireNonNull(bVar);
        Threads.runOnUiBlocking((Runnable) new Runnable(bVar, this.h.getKeyValuePairs()) {
            private final /* synthetic */ b f$1;
            private final /* synthetic */ KeyValuePairs f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                c.this.a(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(b bVar, KeyValuePairs keyValuePairs) {
        this.n = bVar;
        try {
            AdSettings build = new Builder().setPublisherId(bVar.f3277a).setAdSpaceId(bVar.b).setAdFormat(AdFormat.DISPLAY).setAdDimension(bVar.c != null ? bVar.c.adDimension : null).setMediationNetworkName(bVar.d).setMediationNetworkSDKVersion(bVar.e).setMediationAdapterVersion(bVar.f).build();
            UserInfo userInfo = this.d.get();
            h();
            AdRequest build2 = new AdRequest.Builder().setAdSettings(build).setUserInfo(userInfo).setKeyValuePairs(keyValuePairs).build();
            a a2 = a.a(build.getPublisherId(), build.getAdSpaceId(), bVar.c);
            if (a2 == null) {
                this.f3272a.error(LogDomain.WIDGET, "publisherId or adSpaceId are missing", new Object[0]);
                Objects.onNotNull(this.m.get(), $$Lambda$c$8d5WOTcjLINio1grb_4lRABYwLc.INSTANCE);
                return;
            }
            this.g.loadAd(a2, build2, this.p);
        } catch (Exception e2) {
            this.f3272a.error(LogDomain.WIDGET, e2.getMessage(), new Object[0]);
            Objects.onNotNull(this.m.get(), $$Lambda$c$yT3nhtfwjnV5qFijDY0Im9OH9AQ.INSTANCE);
        }
    }

    private void h() {
        this.j.set(null);
        this.c.stop();
        this.e.getNetworkStateChangeNotifier().removeListener(this.l);
        this.l = null;
        i();
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Runnable runnable) {
        this.i.set(runnable);
        this.b.startWithAction(runnable);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Integer n() {
        return Integer.valueOf(this.b.getAutoReloadInterval());
    }

    /* access modifiers changed from: 0000 */
    public final int b() {
        return ((Integer) Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier() {
            public final Object get() {
                return c.this.n();
            }
        })).intValue();
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2) {
        Threads.runOnUiBlocking((Runnable) new Runnable(i2) {
            private final /* synthetic */ int f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                c.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(int i2) {
        this.b.setAutoReloadInterval(i2);
        this.b.startWithAction((Runnable) this.i.get());
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String c() {
        return (String) Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier() {
            public final Object get() {
                return c.this.m();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ String m() {
        AdPresenter adPresenter = (AdPresenter) this.k.get();
        if (adPresenter == null) {
            return null;
        }
        return adPresenter.getSessionId();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String d() {
        return (String) Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier() {
            public final Object get() {
                return c.this.l();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ String l() {
        AdPresenter adPresenter = (AdPresenter) this.k.get();
        if (adPresenter == null) {
            return null;
        }
        return adPresenter.getCreativeId();
    }

    /* access modifiers changed from: private */
    public void i() {
        this.i.set(null);
        this.b.stopTimer();
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        Threads.runOnUiBlocking((Runnable) new Runnable() {
            public final void run() {
                c.this.k();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void k() {
        h();
        Objects.onNotNull(this.k.get(), $$Lambda$LQpepmv_r2DaOeTXvoA1FmnP5I.INSTANCE);
        Objects.onNotNull(this.m.get(), new Consumer() {
            public final void accept(Object obj) {
                c.this.b((BannerView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(BannerView bannerView) {
        bannerView.removeAllViews();
        this.m.clear();
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        Threads.runOnUiBlocking((Runnable) new Runnable() {
            public final void run() {
                c.this.j();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void j() {
        this.f3272a.error(LogDomain.WIDGET, "You cannot resize BannerView AFTER ad was loaded", new Object[0]);
        i();
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2, int i3, int i4, int i5) {
        $$Lambda$c$mJm_SVZR2kJxKOLomOEv_ejnRlw r0 = new Runnable(i2, i3, i4, i5) {
            private final /* synthetic */ int f$1;
            private final /* synthetic */ int f$2;
            private final /* synthetic */ int f$3;
            private final /* synthetic */ int f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void run() {
                c.this.b(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        Threads.runOnUiBlocking((Runnable) r0);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(int i2, int i3, int i4, int i5) {
        i();
        this.f3272a.error(LogDomain.WIDGET, "Content size[%d x %d] is bigger than BannerView [%d x %d]", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Boolean bool) {
        if (bool.booleanValue()) {
            a(this.n);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final KeyValuePairs g() {
        return this.h.getKeyValuePairs();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable KeyValuePairs keyValuePairs) {
        this.h.setKeyValuePairs(keyValuePairs);
    }

    static /* synthetic */ void a(c cVar, Error error) {
        switch (error) {
            case PRESENTER_BUILDER_GENERIC:
            case INVALID_RESPONSE:
            case NO_AD:
            case API:
            case CREATIVE_RESOURCE_EXPIRED:
                cVar.a(cVar.q);
                return;
            case NETWORK:
            case NO_CONNECTION:
                if (cVar.e.isOnline()) {
                    cVar.c.postDelayed("Auto-retry", cVar.q, 30000, null);
                    return;
                }
                cVar.l = new Listener() {
                    public final void onNextValue(Object obj) {
                        c.this.a((Boolean) obj);
                    }
                };
                cVar.e.getNetworkStateChangeNotifier().addListener(cVar.l);
                return;
            case BAD_REQUEST:
            case CONFIGURATION_ERROR:
            case INTERNAL:
            case CANCELLED:
            case TTL_EXPIRED:
                return;
            default:
                cVar.f3272a.warning(LogDomain.WIDGET, "unexpected errorType %s", error);
                return;
        }
    }
}
