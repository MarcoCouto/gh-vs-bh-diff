package com.smaato.sdk.banner.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.banner.ad.BannerAutoReloadConfig;
import com.smaato.sdk.banner.ad.a;
import com.smaato.sdk.banner.widget.c;
import com.smaato.sdk.core.BuildConfig;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.AutoReloadConfig;
import com.smaato.sdk.core.ad.AutoReloadPolicy;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.config.Configuration;
import com.smaato.sdk.core.config.ConfigurationRepository;
import com.smaato.sdk.core.config.DiConfiguration;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.ModuleInterface;
import com.smaato.sdk.core.init.AdPresenterModuleInterfaceUtils;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.repository.AdLoadersRegistry;
import com.smaato.sdk.core.repository.AdPresenterCache;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.DiAdRepository.Provider;
import com.smaato.sdk.core.repository.MultipleAdLoadersRegistry;
import com.smaato.sdk.core.repository.MultipleAdPresenterCache;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.FunctionUtils;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.core.util.memory.LeakProtection;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

public class BannerModuleInterface implements ModuleInterface {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    private static final List<AdFormat> f3266a = Lists.of((T[]) new AdFormat[]{AdFormat.STATIC_IMAGE, AdFormat.RICH_MEDIA});
    private static final State b = State.IMPRESSED;
    @Nullable
    private volatile List<AdPresenterModuleInterface> c;
    private boolean d;
    @Nullable
    private String e;

    @NonNull
    public String moduleDiName() {
        return "BannerModuleInterface";
    }

    @NonNull
    public String version() {
        return BuildConfig.VERSION_NAME;
    }

    public synchronized void init(@NonNull ClassLoader classLoader) {
        a((Iterable<AdPresenterModuleInterface>) ServiceLoader.load(AdPresenterModuleInterface.class, classLoader));
    }

    public boolean isInitialised() {
        return this.d;
    }

    @NonNull
    public Class<? extends AdPresenter> getSupportedAdPresenterInterface() {
        return BannerAdPresenter.class;
    }

    @VisibleForTesting
    private synchronized void a(@NonNull Iterable<AdPresenterModuleInterface> iterable) {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = AdPresenterModuleInterfaceUtils.getValidModuleInterfaces(BuildConfig.VERSION_NAME, iterable);
                    Map mapWithOrder = Maps.toMapWithOrder(f3266a, FunctionUtils.identity(), new Function() {
                        public final Object apply(Object obj) {
                            return Boolean.valueOf(BannerModuleInterface.this.a((AdFormat) obj));
                        }
                    });
                    this.d = Lists.any(mapWithOrder.values(), $$Lambda$orGLThciAnXWGWqy4r8Z91E5vck.INSTANCE);
                    if (!this.d) {
                        this.e = String.format("In order to show ads of %s format at least one of %s modules should be added to your project configuration. Missing module(s): %s", new Object[]{AdFormat.DISPLAY, f3266a, Maps.filteredKeys(mapWithOrder, $$Lambda$BannerModuleInterface$6P9CCtULNso55b184F5fHoPnHwY.INSTANCE)});
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean a(Boolean bool) {
        return !bool.booleanValue();
    }

    @NonNull
    private List<AdPresenterModuleInterface> a() {
        List<AdPresenterModuleInterface> list = this.c;
        if (list != null) {
            return list;
        }
        throw new IllegalStateException("init() method should have been called first for this module: smaato-sdk-banner");
    }

    public boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        if (adFormat != AdFormat.DISPLAY) {
            return a(adFormat);
        }
        if (!this.d) {
            logger.error(LogDomain.FRAMEWORK, this.e, new Object[0]);
        }
        return this.d;
    }

    /* access modifiers changed from: private */
    public boolean a(@NonNull AdFormat adFormat) {
        return Lists.any(a(), new Predicate() {
            public final boolean test(Object obj) {
                return ((AdPresenterModuleInterface) obj).isFormatSupported(AdFormat.this, BannerAdPresenter.class);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ AdLoaderPlugin h(DiConstructor diConstructor) {
        return new a(a(), new NullableFunction() {
            public final Object apply(Object obj) {
                return BannerModuleInterface.a(DiConstructor.this, (AdPresenterModuleInterface) obj);
            }
        }, f3266a, this.e);
    }

    @NonNull
    public ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory() {
        return new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return BannerModuleInterface.this.h(diConstructor);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderPlugin a(DiConstructor diConstructor, AdPresenterModuleInterface adPresenterModuleInterface) {
        return (AdLoaderPlugin) DiAdLayer.tryGetOrNull(diConstructor, adPresenterModuleInterface.moduleDiName(), AdLoaderPlugin.class);
    }

    @Nullable
    public DiRegistry moduleDiRegistry() {
        return DiRegistry.of($$Lambda$BannerModuleInterface$6m8EaMV5mbRwLDOOyiJeKkXlWc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory("AUTO_RELOAD_CONFIG_NAME", AutoReloadConfig.class, $$Lambda$BannerModuleInterface$dMhPyQHZaUj5OqGnLnFz0JNzHLg.INSTANCE);
        diRegistry.registerFactory(c.class, $$Lambda$BannerModuleInterface$m_63H5Qv_KFXDaSdd3v7_0tVk.INSTANCE);
        diRegistry.registerSingletonFactory("BannerModuleInterface", SharedKeyValuePairsHolder.class, $$Lambda$BannerModuleInterface$mVN9ArIssUIpWKnIEqBeEIB6A.INSTANCE);
        diRegistry.registerFactory("BANNER_AUTO_RELOAD_CONFIG", AutoReloadPolicy.class, $$Lambda$BannerModuleInterface$39nGajAOVMYrsgAQx_pGTWMBz4o.INSTANCE);
        diRegistry.registerSingletonFactory("BannerModuleInterface", AdPresenterCache.class, $$Lambda$BannerModuleInterface$FhLOKzBLzpuV2CEyqpiq6TwrnEc.INSTANCE);
        diRegistry.registerSingletonFactory("BannerModuleInterface", AdLoadersRegistry.class, $$Lambda$BannerModuleInterface$u20WJTVLsc3d_piAX2aGqMoa_Fk.INSTANCE);
        diRegistry.registerSingletonFactory("BannerModuleInterface", ConfigurationRepository.class, $$Lambda$BannerModuleInterface$ffbnqwdWYr8XrAHSXdlDedI9rCc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AutoReloadConfig g(DiConstructor diConstructor) {
        return new BannerAutoReloadConfig();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ c f(DiConstructor diConstructor) {
        c cVar = new c(DiLogLayer.getLoggerFrom(diConstructor), (AutoReloadPolicy) diConstructor.get("BANNER_AUTO_RELOAD_CONFIG", AutoReloadPolicy.class), (AppBackgroundAwareHandler) diConstructor.get(AppBackgroundAwareHandler.class), DiAdLayer.getUserInfoFactoryFrom(diConstructor), DiNetworkLayer.getNetworkStateMonitorFrom(diConstructor), (LeakProtection) diConstructor.get(LeakProtection.class), (AdRepository) ((Provider) diConstructor.get(Provider.class)).apply("BannerModuleInterface"), (SharedKeyValuePairsHolder) diConstructor.get("BannerModuleInterface", SharedKeyValuePairsHolder.class));
        return cVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SharedKeyValuePairsHolder e(DiConstructor diConstructor) {
        return new SharedKeyValuePairsHolder();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AutoReloadPolicy d(DiConstructor diConstructor) {
        return new AutoReloadPolicy(DiLogLayer.getLoggerFrom(diConstructor), (AutoReloadConfig) diConstructor.get("AUTO_RELOAD_CONFIG_NAME", AutoReloadConfig.class), (AppBackgroundAwareHandler) diConstructor.get(AppBackgroundAwareHandler.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterCache c(DiConstructor diConstructor) {
        return new MultipleAdPresenterCache((ConfigurationRepository) diConstructor.get("BannerModuleInterface", ConfigurationRepository.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoadersRegistry b(DiConstructor diConstructor) {
        return new MultipleAdLoadersRegistry((ConfigurationRepository) diConstructor.get("BannerModuleInterface", ConfigurationRepository.class), DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConfigurationRepository a(DiConstructor diConstructor) {
        return (ConfigurationRepository) ((DiConfiguration.Provider) diConstructor.get(DiConfiguration.Provider.class)).apply(new Configuration(20, b));
    }

    @NonNull
    public ExpectedManifestEntries getExpectedManifestEntries() {
        return ExpectedManifestEntries.EMPTY;
    }

    @NonNull
    public String toString() {
        StringBuilder sb = new StringBuilder("BannerModuleInterface{supportedFormat: ");
        sb.append(AdFormat.DISPLAY);
        sb.append("}");
        return sb.toString();
    }
}
