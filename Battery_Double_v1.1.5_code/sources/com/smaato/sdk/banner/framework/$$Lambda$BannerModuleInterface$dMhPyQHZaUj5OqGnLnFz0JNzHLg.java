package com.smaato.sdk.banner.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.banner.framework.-$$Lambda$BannerModuleInterface$dMhPyQHZaUj5OqGnLnFz0JNzHLg reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$BannerModuleInterface$dMhPyQHZaUj5OqGnLnFz0JNzHLg implements ClassFactory {
    public static final /* synthetic */ $$Lambda$BannerModuleInterface$dMhPyQHZaUj5OqGnLnFz0JNzHLg INSTANCE = new $$Lambda$BannerModuleInterface$dMhPyQHZaUj5OqGnLnFz0JNzHLg();

    private /* synthetic */ $$Lambda$BannerModuleInterface$dMhPyQHZaUj5OqGnLnFz0JNzHLg() {
    }

    public final Object get(DiConstructor diConstructor) {
        return BannerModuleInterface.g(diConstructor);
    }
}
