package com.smaato.sdk.interstitial;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdRequest;
import com.smaato.sdk.core.ad.AdSettings;
import com.smaato.sdk.core.ad.AdSettings.Builder;
import com.smaato.sdk.core.ad.FullscreenAdDimensionMapper;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.ad.UserInfoSupplier;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.AdRepository.Listener;
import com.smaato.sdk.core.repository.AdTypeStrategy;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.UIUtils;

final class h {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final AdRepository f3544a;
    /* access modifiers changed from: private */
    @NonNull
    public final g b;
    @NonNull
    private final Application c;
    @NonNull
    private final UserInfoSupplier d;
    @NonNull
    private final SharedKeyValuePairsHolder e;
    @NonNull
    private final FullscreenAdDimensionMapper f;

    static {
        h.class.getSimpleName();
    }

    h(@NonNull AdRepository adRepository, @NonNull g gVar, @NonNull Application application, @NonNull UserInfoSupplier userInfoSupplier, @NonNull SharedKeyValuePairsHolder sharedKeyValuePairsHolder, @NonNull FullscreenAdDimensionMapper fullscreenAdDimensionMapper) {
        this.f3544a = (AdRepository) Objects.requireNonNull(adRepository);
        this.b = (g) Objects.requireNonNull(gVar);
        this.c = (Application) Objects.requireNonNull(application);
        this.d = (UserInfoSupplier) Objects.requireNonNull(userInfoSupplier);
        this.e = (SharedKeyValuePairsHolder) Objects.requireNonNull(sharedKeyValuePairsHolder);
        this.f = (FullscreenAdDimensionMapper) Objects.requireNonNull(fullscreenAdDimensionMapper);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final KeyValuePairs a() {
        return this.e.getKeyValuePairs();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable KeyValuePairs keyValuePairs) {
        this.e.setKeyValuePairs(keyValuePairs);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str, @NonNull String str2, @NonNull EventListener eventListener, @NonNull AdFormat adFormat, @Nullable String str3, @Nullable String str4, @Nullable String str5) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(str2);
        Objects.requireNonNull(eventListener);
        Objects.requireNonNull(adFormat);
        $$Lambda$h$JP6W7Ce7GxZEQnlN77FZnoDMF4 r0 = new Runnable(str, str2, adFormat, str3, str4, str5, eventListener) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ String f$2;
            private final /* synthetic */ AdFormat f$3;
            private final /* synthetic */ String f$4;
            private final /* synthetic */ String f$5;
            private final /* synthetic */ String f$6;
            private final /* synthetic */ EventListener f$7;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
            }

            public final void run() {
                h.this.a(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7);
            }
        };
        Threads.runOnUi(r0);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(final String str, final String str2, AdFormat adFormat, String str3, String str4, String str5, EventListener eventListener) {
        try {
            AdSettings build = new Builder().setPublisherId(str).setAdSpaceId(str2).setAdFormat(adFormat).setWidth(UIUtils.getDisplayWidthInDp()).setHeight(UIUtils.getDisplayHeightInDp()).setMediationNetworkName(str3).setMediationNetworkSDKVersion(str4).setMediationAdapterVersion(str5).setAdDimension(this.f.getDimension(this.c.getString(R.string.smaato_sdk_core_fullscreen_dimension))).build();
            e eVar = new e(str, str2);
            this.b.a(eVar.getUniqueKeyForType(), eventListener);
            this.f3544a.loadAd(eVar, new AdRequest.Builder().setAdSettings(build).setUserInfo(this.d.get()).setKeyValuePairs(this.e.getKeyValuePairs()).build(), new Listener() {
                public final void onAdLoadSuccess(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdPresenter adPresenter) {
                    h.this.b.a(adPresenter, adTypeStrategy.getUniqueKeyForType());
                }

                public final void onAdLoadError(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdLoaderException adLoaderException) {
                    InterstitialError interstitialError;
                    Error errorType = adLoaderException.getErrorType();
                    Objects.requireNonNull(errorType);
                    switch (AnonymousClass1.f3540a[errorType.ordinal()]) {
                        case 1:
                            interstitialError = InterstitialError.NO_AD_AVAILABLE;
                            break;
                        case 2:
                            interstitialError = InterstitialError.INVALID_REQUEST;
                            break;
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            interstitialError = InterstitialError.INTERNAL_ERROR;
                            break;
                        case 10:
                        case 11:
                            interstitialError = InterstitialError.NETWORK_ERROR;
                            break;
                        case 12:
                            interstitialError = InterstitialError.CREATIVE_RESOURCE_EXPIRED;
                            break;
                        default:
                            throw new IllegalArgumentException(String.format("Unexpected %s: %s", new Object[]{Error.class.getSimpleName(), errorType}));
                    }
                    h.this.b.a(new InterstitialRequestError(interstitialError, str, str2), adTypeStrategy.getUniqueKeyForType());
                }
            });
        } catch (Exception unused) {
            eventListener.onAdFailedToLoad(new InterstitialRequestError(InterstitialError.INVALID_REQUEST, str, str2));
        }
    }
}
