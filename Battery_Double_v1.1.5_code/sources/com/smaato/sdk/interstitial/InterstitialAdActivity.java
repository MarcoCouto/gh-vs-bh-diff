package com.smaato.sdk.interstitial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter.Listener;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.diinjection.Inject;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.UUID;

public class InterstitialAdActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    private static final String f3528a = "KEY_PRESENTER_UUID";
    private static final String b = "KEY_INTERSTITIAL_IDENTIFIER";
    private static final String c = "KEY_BACKGROUND_COLOR";
    /* access modifiers changed from: private */
    public static final String d = "com.smaato.sdk.interstitial.InterstitialAdActivity";
    @Inject
    @Nullable
    private d e;
    /* access modifiers changed from: private */
    @Inject
    @NonNull
    public g f;
    @Nullable
    private InterstitialAdPresenter g;
    @Nullable
    private Listener h;
    /* access modifiers changed from: private */
    @NonNull
    public String i;
    @NonNull
    private UUID j;
    /* access modifiers changed from: private */
    @NonNull
    public FrameLayout k;
    /* access modifiers changed from: private */
    @NonNull
    public ImageButton l;
    /* access modifiers changed from: private */
    public boolean m;

    @NonNull
    public static Intent createIntent(@NonNull Activity activity, @NonNull UUID uuid, @NonNull String str, @ColorInt int i2) {
        Objects.requireNonNull(activity);
        Objects.requireNonNull(uuid);
        Objects.requireNonNull(str);
        return new Intent(activity, InterstitialAdActivity.class).putExtra(f3528a, uuid).putExtra(b, str).putExtra(c, i2);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(@NonNull Intent intent) {
        Objects.onNotNull(this.e, new Consumer((UUID) getIntent().getSerializableExtra(f3528a)) {
            private final /* synthetic */ UUID f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((d) obj).b(this.f$0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        AndroidsInjector.inject((Activity) this);
        if (this.e == null || this.f == null) {
            Log.e(d, "SmaatoSdk is not initialized.");
            finish();
            return;
        }
        this.j = (UUID) getIntent().getSerializableExtra(f3528a);
        this.i = getIntent().getStringExtra(b);
        this.g = this.e.a(this.j);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        if (this.g == null) {
            finish();
            this.f.a(this.i, new a(C0076a.ERROR, InterstitialError.INTERNAL_ERROR));
            return;
        }
        this.h = a(this.i);
        this.g.setListener(this.h);
        a(this.g.getAdContentView(this));
        this.f.a(this.i, C0076a.OPEN);
    }

    @NonNull
    private Listener a(@NonNull final String str) {
        return new Listener() {
            public final /* bridge */ /* synthetic */ void onTTLExpired(@NonNull AdPresenter adPresenter) {
            }

            public final void onClose(@NonNull InterstitialAdPresenter interstitialAdPresenter) {
                InterstitialAdActivity.this.finish();
            }

            public final void onShowCloseButton() {
                InterstitialAdActivity.this.l.setVisibility(0);
                InterstitialAdActivity.this.m = true;
            }

            public final void onAdUnload(@NonNull InterstitialAdPresenter interstitialAdPresenter) {
                Log.i(InterstitialAdActivity.d, "Ad requested to be unloaded.");
                InterstitialAdActivity.this.f.a(str, new a(C0076a.ERROR, InterstitialError.AD_UNLOADED));
                InterstitialAdActivity.this.finish();
            }

            public final /* synthetic */ void onAdError(@NonNull AdPresenter adPresenter) {
                InterstitialAdActivity.this.f.a(str, new a(C0076a.ERROR, InterstitialError.INTERNAL_ERROR));
            }

            public final /* synthetic */ void onAdClicked(@NonNull AdPresenter adPresenter) {
                InterstitialAdActivity.this.f.a(str, C0076a.CLICK);
            }

            public final /* synthetic */ void onAdImpressed(@NonNull AdPresenter adPresenter) {
                InterstitialAdActivity.this.f.a(str, C0076a.IMPRESS);
            }
        };
    }

    private void a(@Nullable final AdContentView adContentView) {
        setContentView(R.layout.smaato_sdk_interstitial_activity);
        this.l = (ImageButton) findViewById(R.id.smaato_sdk_interstitial_close);
        Objects.onNotNull(this.g, new Consumer() {
            public final void accept(Object obj) {
                InterstitialAdActivity.this.b((InterstitialAdPresenter) obj);
            }
        });
        findViewById(16908290).setBackgroundColor(getIntent().getIntExtra(c, ViewCompat.MEASURED_STATE_MASK));
        this.k = (FrameLayout) findViewById(R.id.smaato_sdk_interstitial_content);
        this.k.addView(adContentView);
        this.l.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                InterstitialAdActivity.this.a(view);
            }
        });
        this.k.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public final void onGlobalLayout() {
                InterstitialAdActivity.this.k.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                if (adContentView == null) {
                    InterstitialAdActivity.this.finish();
                    InterstitialAdActivity.this.f.a(InterstitialAdActivity.this.i, new a(C0076a.ERROR, InterstitialError.INTERNAL_ERROR));
                    return;
                }
                float a2 = InterstitialAdActivity.this.a(InterstitialAdActivity.this.k, adContentView);
                adContentView.setScaleX(a2);
                adContentView.setScaleY(a2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(InterstitialAdPresenter interstitialAdPresenter) {
        interstitialAdPresenter.setFriendlyObstructionView(this.l);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(View view) {
        b();
    }

    /* access modifiers changed from: private */
    public float a(@NonNull FrameLayout frameLayout, @NonNull AdContentView adContentView) {
        float width = ((float) frameLayout.getWidth()) / ((float) adContentView.getWidth());
        float height = ((float) frameLayout.getHeight()) / ((float) adContentView.getHeight());
        return width < height ? width : height;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            Objects.onNotNull(this.e, new Consumer() {
                public final void accept(Object obj) {
                    InterstitialAdActivity.this.a((d) obj);
                }
            });
            Objects.onNotNull(this.g, $$Lambda$InterstitialAdActivity$PZNgSfP5EjDguD1yo8NkwTIaw6Q.INSTANCE);
            this.f.a(this.i, C0076a.CLOSE);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(d dVar) {
        dVar.b(this.j);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(InterstitialAdPresenter interstitialAdPresenter) {
        if (!interstitialAdPresenter.isValid()) {
            interstitialAdPresenter.destroy();
        }
    }

    public void onBackPressed() {
        if (this.m) {
            super.onBackPressed();
            b();
        }
    }

    private void b() {
        Objects.onNotNull(this.g, $$Lambda$txm5DTbYXLiKFmFQmU_1Fi3AAck.INSTANCE);
    }
}
