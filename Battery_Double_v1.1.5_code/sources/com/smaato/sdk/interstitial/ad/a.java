package com.smaato.sdk.interstitial.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.ApiAdRequestExtras;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.api.AdRequestExtrasProvider;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.util.fi.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class a implements AdLoaderPlugin {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Iterable<? extends AdPresenterModuleInterface> f3535a;
    @NonNull
    private final ArrayList<AdFormat> b = new ArrayList<>();
    @NonNull
    private final AdRequestExtrasProvider c;
    @NonNull
    private final NullableFunction<AdPresenterModuleInterface, AdLoaderPlugin> d;
    @NonNull
    private final NullableFunction<Collection<AdFormat>, AdFormat> e;
    private final boolean f;
    private final boolean g;
    @Nullable
    private final String h;

    public a(@NonNull Iterable<? extends AdPresenterModuleInterface> iterable, @NonNull NullableFunction<AdPresenterModuleInterface, AdLoaderPlugin> nullableFunction, @NonNull NullableFunction<Collection<AdFormat>, AdFormat> nullableFunction2, @NonNull AdRequestExtrasProvider adRequestExtrasProvider, @NonNull List<AdFormat> list, @Nullable String str) {
        this.f3535a = (Iterable) Objects.requireNonNull(iterable);
        this.d = (NullableFunction) Objects.requireNonNull(nullableFunction);
        this.c = (AdRequestExtrasProvider) Objects.requireNonNull(adRequestExtrasProvider);
        this.e = (NullableFunction) Objects.requireNonNull(nullableFunction2);
        this.b.addAll((Collection) Objects.requireNonNull(list));
        this.f = Lists.any(this.b, new Predicate() {
            public final boolean test(Object obj) {
                return a.this.a((AdFormat) obj);
            }
        });
        this.g = Lists.all(this.b, new Predicate() {
            public final boolean test(Object obj) {
                return a.this.a((AdFormat) obj);
            }
        });
        this.h = str;
    }

    @Nullable
    public final AdPresenterBuilder getAdPresenterBuilder(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls, @NonNull Logger logger) {
        if (!InterstitialAdPresenter.class.isAssignableFrom(cls)) {
            return null;
        }
        for (AdPresenterModuleInterface adPresenterModuleInterface : this.f3535a) {
            if (adPresenterModuleInterface.isFormatSupported(adFormat, InterstitialAdPresenter.class)) {
                AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.d.apply(adPresenterModuleInterface);
                if (adLoaderPlugin != null) {
                    AdPresenterBuilder adPresenterBuilder = adLoaderPlugin.getAdPresenterBuilder(adFormat, cls, logger);
                    if (adPresenterBuilder != null) {
                        return adPresenterBuilder;
                    }
                } else {
                    continue;
                }
            }
        }
        return null;
    }

    public final void addApiAdRequestExtras(@NonNull ApiAdRequestExtras apiAdRequestExtras, @NonNull Logger logger) {
        this.c.addApiAdRequestExtras(apiAdRequestExtras);
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            AdFormat adFormat = (AdFormat) it.next();
            for (AdPresenterModuleInterface adPresenterModuleInterface : this.f3535a) {
                if (adPresenterModuleInterface.isFormatSupported(adFormat, InterstitialAdPresenter.class)) {
                    AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.d.apply(adPresenterModuleInterface);
                    if (adLoaderPlugin != null) {
                        adLoaderPlugin.addApiAdRequestExtras(apiAdRequestExtras, logger);
                    }
                }
            }
        }
    }

    @Nullable
    public final AdFormat resolveAdFormatToServerAdFormat(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        if (adFormat != AdFormat.INTERSTITIAL) {
            return a(adFormat, logger);
        }
        if (!this.f) {
            logger.error(LogDomain.FRAMEWORK, this.h, new Object[0]);
            return null;
        } else if (this.g) {
            return AdFormat.INTERSTITIAL;
        } else {
            ArrayList arrayList = new ArrayList();
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                AdFormat a2 = a((AdFormat) it.next(), logger);
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
            return (AdFormat) this.e.apply(arrayList);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(@NonNull AdFormat adFormat) {
        for (AdPresenterModuleInterface isFormatSupported : this.f3535a) {
            if (isFormatSupported.isFormatSupported(adFormat, InterstitialAdPresenter.class)) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    private AdFormat a(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        for (AdPresenterModuleInterface adPresenterModuleInterface : this.f3535a) {
            if (adPresenterModuleInterface.isFormatSupported(adFormat, InterstitialAdPresenter.class)) {
                AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.d.apply(adPresenterModuleInterface);
                if (adLoaderPlugin != null) {
                    return adLoaderPlugin.resolveAdFormatToServerAdFormat(adFormat, logger);
                }
            }
        }
        return null;
    }
}
