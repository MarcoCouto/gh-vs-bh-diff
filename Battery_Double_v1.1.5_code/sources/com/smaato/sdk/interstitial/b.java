package com.smaato.sdk.interstitial;

import android.app.Application;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.FullscreenAdDimensionMapper;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.ad.UserInfoSupplier;
import com.smaato.sdk.core.api.AdRequestExtrasProvider;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.core.api.VideoTypeAdRequestExtrasProvider;
import com.smaato.sdk.core.config.Configuration;
import com.smaato.sdk.core.config.ConfigurationRepository;
import com.smaato.sdk.core.config.DiConfiguration;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.repository.AdLoadersRegistry;
import com.smaato.sdk.core.repository.AdPresenterCache;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.DiAdRepository.Provider;
import com.smaato.sdk.core.repository.MultipleAdLoadersRegistry;
import com.smaato.sdk.core.repository.MultipleAdPresenterCache;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.HashMap;
import java.util.WeakHashMap;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private static final State f3536a = State.IMPRESSED;

    @NonNull
    public static DiRegistry a(@NonNull String str) {
        Objects.requireNonNull(str);
        return DiRegistry.of(new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                b.a(this.f$0, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(String str, DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(h.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return b.c(this.f$0, diConstructor);
            }
        });
        diRegistry.registerSingletonFactory(str, SharedKeyValuePairsHolder.class, $$Lambda$b$JI9oe9iJoyKFEaOIF2UdqE9VA1U.INSTANCE);
        diRegistry.registerSingletonFactory(g.class, $$Lambda$b$2kc7dN1hJYnVLauBDrY0thqE2Z8.INSTANCE);
        diRegistry.registerSingletonFactory(d.class, $$Lambda$b$Y9o2uwncaXICi4vnG4SG5sDWJys.INSTANCE);
        diRegistry.registerFactory(str, AdRequestExtrasProvider.class, $$Lambda$b$N25TvStB5WoWTmEWwDFQeYG7JWc.INSTANCE);
        diRegistry.registerSingletonFactory(str, AdPresenterCache.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return b.b(this.f$0, diConstructor);
            }
        });
        diRegistry.registerSingletonFactory(str, AdLoadersRegistry.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return b.a(this.f$0, diConstructor);
            }
        });
        diRegistry.registerSingletonFactory(str, ConfigurationRepository.class, $$Lambda$b$38FmffEaRZyRbUD0sOlU7FVhfBI.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ h c(String str, DiConstructor diConstructor) {
        h hVar = new h((AdRepository) ((Provider) diConstructor.get(Provider.class)).apply(str), (g) diConstructor.get(g.class), (Application) diConstructor.get(Application.class), (UserInfoSupplier) diConstructor.get(UserInfoSupplier.class), (SharedKeyValuePairsHolder) diConstructor.get(str, SharedKeyValuePairsHolder.class), (FullscreenAdDimensionMapper) diConstructor.get(FullscreenAdDimensionMapper.class));
        return hVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SharedKeyValuePairsHolder e(DiConstructor diConstructor) {
        return new SharedKeyValuePairsHolder();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ g d(DiConstructor diConstructor) {
        g gVar = new g(new HashMap(), new HashMap(), new WeakHashMap(), (d) diConstructor.get(d.class), $$Lambda$Wd7bUiJ6rzarh6euqzITU7sSM.INSTANCE);
        return gVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ d c(DiConstructor diConstructor) {
        return new d();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdRequestExtrasProvider b(DiConstructor diConstructor) {
        return new VideoTypeAdRequestExtrasProvider(VideoType.INTERSTITIAL);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterCache b(String str, DiConstructor diConstructor) {
        return new MultipleAdPresenterCache((ConfigurationRepository) diConstructor.get(str, ConfigurationRepository.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoadersRegistry a(String str, DiConstructor diConstructor) {
        return new MultipleAdLoadersRegistry((ConfigurationRepository) diConstructor.get(str, ConfigurationRepository.class), DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConfigurationRepository a(DiConstructor diConstructor) {
        return (ConfigurationRepository) ((DiConfiguration.Provider) diConstructor.get(DiConfiguration.Provider.class)).apply(new Configuration(1, f3536a));
    }
}
