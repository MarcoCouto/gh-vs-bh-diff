package com.smaato.sdk.interstitial;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdInteractor.TtlListener;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.util.Intents;
import com.smaato.sdk.core.util.Objects;
import java.util.UUID;

final class c extends InterstitialAd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final UUID f3537a;
    @NonNull
    private final String b;
    @NonNull
    private final InterstitialAdPresenter c;
    @NonNull
    private final TtlListener d;
    @NonNull
    private final EventListener e;
    @NonNull
    private final d f;

    c(@NonNull UUID uuid, @NonNull String str, @NonNull InterstitialAdPresenter interstitialAdPresenter, @NonNull d dVar, @NonNull EventListener eventListener) {
        this.f3537a = (UUID) Objects.requireNonNull(uuid);
        this.b = (String) Objects.requireNonNull(str);
        this.c = (InterstitialAdPresenter) Objects.requireNonNull(interstitialAdPresenter);
        this.f = (d) Objects.requireNonNull(dVar);
        this.e = (EventListener) Objects.requireNonNull(eventListener);
        this.d = new TtlListener(eventListener) {
            private final /* synthetic */ EventListener f$1;

            {
                this.f$1 = r2;
            }

            public final void onTTLExpired(AdInteractor adInteractor) {
                c.this.a(this.f$1, adInteractor);
            }
        };
        interstitialAdPresenter.getAdInteractor().addTtlListener(this.d);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(EventListener eventListener, AdInteractor adInteractor) {
        eventListener.onAdTTLExpired(this);
    }

    /* access modifiers changed from: protected */
    public final void showAdInternal(@NonNull Activity activity) {
        if (this.c.isValid()) {
            this.f.a(this.f3537a, this.c);
            Intents.startIntent(activity, InterstitialAdActivity.createIntent(activity, this.f3537a, this.b, this.backgroundColor));
            return;
        }
        this.e.onAdError(this, InterstitialError.CREATIVE_RESOURCE_EXPIRED);
    }

    @NonNull
    public final String getSessionId() {
        return this.c.getSessionId();
    }

    @Nullable
    public final String getCreativeId() {
        return this.c.getCreativeId();
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.c.getAdSpaceId();
    }

    public final boolean isAvailableForPresentation() {
        return this.c.isValid();
    }
}
