package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.repository.AdTypeStrategy;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import io.fabric.sdk.android.services.events.EventsFilesManager;

final class e implements AdTypeStrategy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f3539a;
    @NonNull
    private final String b;

    e(@NonNull String str, @NonNull String str2) {
        this.f3539a = (String) Objects.requireNonNull(str);
        this.b = (String) Objects.requireNonNull(str2);
    }

    @NonNull
    public final String getUniqueKeyForType() {
        return Joiner.join((CharSequence) EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, this.f3539a, this.b);
    }

    @NonNull
    public final Class<? extends AdPresenter> getAdPresenterClass() {
        return InterstitialAdPresenter.class;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        if (!this.f3539a.equals(eVar.f3539a)) {
            return false;
        }
        return this.b.equals(eVar.b);
    }

    public final int hashCode() {
        return (this.f3539a.hashCode() * 31) + this.b.hashCode();
    }
}
