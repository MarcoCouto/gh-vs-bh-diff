package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public final class a {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final C0076a f3533a;
    @NonNull
    private final Object b;

    /* renamed from: com.smaato.sdk.interstitial.a$a reason: collision with other inner class name */
    enum C0076a {
        LOADED,
        OPEN,
        IMPRESS,
        CLICK,
        CLOSE,
        ERROR,
        TTL_EXPIRED
    }

    a(@NonNull C0076a aVar, @NonNull Object obj) {
        this.f3533a = (C0076a) Objects.requireNonNull(aVar);
        this.b = Objects.requireNonNull(obj);
    }

    @NonNull
    public final C0076a a() {
        return this.f3533a;
    }

    @NonNull
    public final Object b() {
        return this.b;
    }
}
