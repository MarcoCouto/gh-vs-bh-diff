package com.smaato.sdk.interstitial;

import com.smaato.sdk.core.ad.AdLoader.Error;

final class f {

    /* renamed from: com.smaato.sdk.interstitial.f$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f3540a = new int[Error.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|(3:23|24|26)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|26) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            f3540a[Error.NO_AD.ordinal()] = 1;
            f3540a[Error.BAD_REQUEST.ordinal()] = 2;
            f3540a[Error.PRESENTER_BUILDER_GENERIC.ordinal()] = 3;
            f3540a[Error.INVALID_RESPONSE.ordinal()] = 4;
            f3540a[Error.API.ordinal()] = 5;
            f3540a[Error.CONFIGURATION_ERROR.ordinal()] = 6;
            f3540a[Error.INTERNAL.ordinal()] = 7;
            f3540a[Error.CANCELLED.ordinal()] = 8;
            f3540a[Error.TTL_EXPIRED.ordinal()] = 9;
            f3540a[Error.NETWORK.ordinal()] = 10;
            f3540a[Error.NO_CONNECTION.ordinal()] = 11;
            try {
                f3540a[Error.CREATIVE_RESOURCE_EXPIRED.ordinal()] = 12;
            } catch (NoSuchFieldError unused) {
            }
        }
    }
}
