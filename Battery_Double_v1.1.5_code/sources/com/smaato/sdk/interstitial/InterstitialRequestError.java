package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;

public class InterstitialRequestError {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f3532a;
    @Nullable
    private final String b;
    @NonNull
    private final InterstitialError c;

    InterstitialRequestError(@NonNull InterstitialError interstitialError, @Nullable String str, @Nullable String str2) {
        this.c = (InterstitialError) Objects.requireNonNull(interstitialError);
        this.f3532a = str;
        this.b = str2;
    }

    @Nullable
    public String getPublisherId() {
        return this.f3532a;
    }

    @Nullable
    public String getAdSpaceId() {
        return this.b;
    }

    @NonNull
    public InterstitialError getInterstitialError() {
        return this.c;
    }
}
