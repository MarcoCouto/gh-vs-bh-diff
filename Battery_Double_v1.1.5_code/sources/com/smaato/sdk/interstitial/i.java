package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.CollectionUtils;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.NullableFunction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class i implements NullableFunction<Collection<AdFormat>, AdFormat> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private static final List<AdFormat> f3546a = Lists.of((T[]) new AdFormat[]{AdFormat.STATIC_IMAGE, AdFormat.RICH_MEDIA});
    @NonNull
    private static final List<AdFormat> b = Lists.of((T[]) new AdFormat[]{AdFormat.STATIC_IMAGE, AdFormat.VIDEO});
    private final List<AdFormat> c;

    @Nullable
    public final /* synthetic */ Object apply(@Nullable Object obj) {
        Collection collection = (Collection) obj;
        if (collection == null || collection.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.c);
        if (!arrayList.retainAll(collection)) {
            return AdFormat.INTERSTITIAL;
        }
        if (arrayList.size() == 1) {
            return (AdFormat) arrayList.get(0);
        }
        if (CollectionUtils.equalsByElements(f3546a, arrayList)) {
            return AdFormat.DISPLAY;
        }
        if (CollectionUtils.equalsByElements(b, arrayList)) {
            return AdFormat.VIDEO;
        }
        return null;
    }

    public i(@NonNull Collection<AdFormat> collection) {
        this.c = Lists.toImmutableList((Collection) Objects.requireNonNull(collection));
    }
}
