package com.explorestack.protobuf;

import com.explorestack.protobuf.UnknownFieldSet.Builder;
import com.explorestack.protobuf.UnknownFieldSet.Field;
import java.io.IOException;

class UnknownFieldSetSchema extends UnknownFieldSchema<UnknownFieldSet, Builder> {
    private final boolean proto3;

    /* access modifiers changed from: 0000 */
    public void makeImmutable(Object obj) {
    }

    public UnknownFieldSetSchema(boolean z) {
        this.proto3 = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean shouldDiscardUnknownFields(Reader reader) {
        return reader.shouldDiscardUnknownFields();
    }

    /* access modifiers changed from: 0000 */
    public Builder newBuilder() {
        return UnknownFieldSet.newBuilder();
    }

    /* access modifiers changed from: 0000 */
    public void addVarint(Builder builder, int i, long j) {
        builder.mergeField(i, Field.newBuilder().addVarint(j).build());
    }

    /* access modifiers changed from: 0000 */
    public void addFixed32(Builder builder, int i, int i2) {
        builder.mergeField(i, Field.newBuilder().addFixed32(i2).build());
    }

    /* access modifiers changed from: 0000 */
    public void addFixed64(Builder builder, int i, long j) {
        builder.mergeField(i, Field.newBuilder().addFixed64(j).build());
    }

    /* access modifiers changed from: 0000 */
    public void addLengthDelimited(Builder builder, int i, ByteString byteString) {
        builder.mergeField(i, Field.newBuilder().addLengthDelimited(byteString).build());
    }

    /* access modifiers changed from: 0000 */
    public void addGroup(Builder builder, int i, UnknownFieldSet unknownFieldSet) {
        builder.mergeField(i, Field.newBuilder().addGroup(unknownFieldSet).build());
    }

    /* access modifiers changed from: 0000 */
    public UnknownFieldSet toImmutable(Builder builder) {
        return builder.build();
    }

    /* access modifiers changed from: 0000 */
    public void writeTo(UnknownFieldSet unknownFieldSet, Writer writer) throws IOException {
        unknownFieldSet.writeTo(writer);
    }

    /* access modifiers changed from: 0000 */
    public void writeAsMessageSetTo(UnknownFieldSet unknownFieldSet, Writer writer) throws IOException {
        unknownFieldSet.writeAsMessageSetTo(writer);
    }

    /* access modifiers changed from: 0000 */
    public UnknownFieldSet getFromMessage(Object obj) {
        return ((GeneratedMessageV3) obj).unknownFields;
    }

    /* access modifiers changed from: 0000 */
    public void setToMessage(Object obj, UnknownFieldSet unknownFieldSet) {
        ((GeneratedMessageV3) obj).unknownFields = unknownFieldSet;
    }

    /* access modifiers changed from: 0000 */
    public Builder getBuilderFromMessage(Object obj) {
        return ((GeneratedMessageV3) obj).unknownFields.toBuilder();
    }

    /* access modifiers changed from: 0000 */
    public void setBuilderToMessage(Object obj, Builder builder) {
        ((GeneratedMessageV3) obj).unknownFields = builder.build();
    }

    /* access modifiers changed from: 0000 */
    public UnknownFieldSet merge(UnknownFieldSet unknownFieldSet, UnknownFieldSet unknownFieldSet2) {
        return unknownFieldSet.toBuilder().mergeFrom(unknownFieldSet2).build();
    }

    /* access modifiers changed from: 0000 */
    public int getSerializedSize(UnknownFieldSet unknownFieldSet) {
        return unknownFieldSet.getSerializedSize();
    }

    /* access modifiers changed from: 0000 */
    public int getSerializedSizeAsMessageSet(UnknownFieldSet unknownFieldSet) {
        return unknownFieldSet.getSerializedSizeAsMessageSet();
    }
}
