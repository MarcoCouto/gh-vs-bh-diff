package com.explorestack.protobuf;

import java.io.IOException;

abstract class UnknownFieldSchema<T, B> {
    /* access modifiers changed from: 0000 */
    public abstract void addFixed32(B b, int i, int i2);

    /* access modifiers changed from: 0000 */
    public abstract void addFixed64(B b, int i, long j);

    /* access modifiers changed from: 0000 */
    public abstract void addGroup(B b, int i, T t);

    /* access modifiers changed from: 0000 */
    public abstract void addLengthDelimited(B b, int i, ByteString byteString);

    /* access modifiers changed from: 0000 */
    public abstract void addVarint(B b, int i, long j);

    /* access modifiers changed from: 0000 */
    public abstract B getBuilderFromMessage(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract T getFromMessage(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract int getSerializedSize(T t);

    /* access modifiers changed from: 0000 */
    public abstract int getSerializedSizeAsMessageSet(T t);

    /* access modifiers changed from: 0000 */
    public abstract void makeImmutable(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract T merge(T t, T t2);

    /* access modifiers changed from: 0000 */
    public abstract B newBuilder();

    /* access modifiers changed from: 0000 */
    public abstract void setBuilderToMessage(Object obj, B b);

    /* access modifiers changed from: 0000 */
    public abstract void setToMessage(Object obj, T t);

    /* access modifiers changed from: 0000 */
    public abstract boolean shouldDiscardUnknownFields(Reader reader);

    /* access modifiers changed from: 0000 */
    public abstract T toImmutable(B b);

    /* access modifiers changed from: 0000 */
    public abstract void writeAsMessageSetTo(T t, Writer writer) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void writeTo(T t, Writer writer) throws IOException;

    UnknownFieldSchema() {
    }

    /* access modifiers changed from: 0000 */
    public final boolean mergeOneFieldFrom(B b, Reader reader) throws IOException {
        int tag = reader.getTag();
        int tagFieldNumber = WireFormat.getTagFieldNumber(tag);
        switch (WireFormat.getTagWireType(tag)) {
            case 0:
                addVarint(b, tagFieldNumber, reader.readInt64());
                return true;
            case 1:
                addFixed64(b, tagFieldNumber, reader.readFixed64());
                return true;
            case 2:
                addLengthDelimited(b, tagFieldNumber, reader.readBytes());
                return true;
            case 3:
                Object newBuilder = newBuilder();
                int makeTag = WireFormat.makeTag(tagFieldNumber, 4);
                mergeFrom(newBuilder, reader);
                if (makeTag == reader.getTag()) {
                    addGroup(b, tagFieldNumber, toImmutable(newBuilder));
                    return true;
                }
                throw InvalidProtocolBufferException.invalidEndTag();
            case 4:
                return false;
            case 5:
                addFixed32(b, tagFieldNumber, reader.readFixed32());
                return true;
            default:
                throw InvalidProtocolBufferException.invalidWireType();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void mergeFrom(B b, Reader reader) throws IOException {
        while (reader.getFieldNumber() != Integer.MAX_VALUE) {
            if (!mergeOneFieldFrom(b, reader)) {
                return;
            }
        }
    }
}
