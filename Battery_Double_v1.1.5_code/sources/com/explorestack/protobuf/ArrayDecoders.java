package com.explorestack.protobuf;

import com.explorestack.protobuf.GeneratedMessageLite.ExtendableMessage;
import com.explorestack.protobuf.GeneratedMessageLite.GeneratedExtension;
import com.explorestack.protobuf.Internal.ProtobufList;
import com.explorestack.protobuf.WireFormat.FieldType;
import java.io.IOException;
import java.util.List;

final class ArrayDecoders {

    static final class Registers {
        public final ExtensionRegistryLite extensionRegistry;
        public int int1;
        public long long1;
        public Object object1;

        Registers() {
            this.extensionRegistry = ExtensionRegistryLite.getEmptyRegistry();
        }

        Registers(ExtensionRegistryLite extensionRegistryLite) {
            if (extensionRegistryLite != null) {
                this.extensionRegistry = extensionRegistryLite;
                return;
            }
            throw new NullPointerException();
        }
    }

    ArrayDecoders() {
    }

    static int decodeVarint32(byte[] bArr, int i, Registers registers) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return decodeVarint32(b, bArr, i2, registers);
        }
        registers.int1 = b;
        return i2;
    }

    static int decodeVarint32(int i, byte[] bArr, int i2, Registers registers) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            registers.int1 = i3 | (b << 7);
            return i4;
        }
        int i5 = i3 | ((b & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            registers.int1 = i5 | (b2 << 14);
            return i6;
        }
        int i7 = i5 | ((b2 & Byte.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            registers.int1 = i7 | (b3 << 21);
            return i8;
        }
        int i9 = i7 | ((b3 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            registers.int1 = i9 | (b4 << 28);
            return i10;
        }
        int i11 = i9 | ((b4 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] < 0) {
                i10 = i12;
            } else {
                registers.int1 = i11;
                return i12;
            }
        }
    }

    static int decodeVarint64(byte[] bArr, int i, Registers registers) {
        int i2 = i + 1;
        long j = (long) bArr[i];
        if (j < 0) {
            return decodeVarint64(j, bArr, i2, registers);
        }
        registers.long1 = j;
        return i2;
    }

    static int decodeVarint64(long j, byte[] bArr, int i, Registers registers) {
        long j2 = j & 127;
        int i2 = i + 1;
        byte b = bArr[i];
        long j3 = j2 | (((long) (b & Byte.MAX_VALUE)) << 7);
        int i3 = 7;
        while (b < 0) {
            int i4 = i2 + 1;
            b = bArr[i2];
            i3 += 7;
            j3 |= ((long) (b & Byte.MAX_VALUE)) << i3;
            i2 = i4;
        }
        registers.long1 = j3;
        return i2;
    }

    static int decodeFixed32(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    static long decodeFixed64(byte[] bArr, int i) {
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    static double decodeDouble(byte[] bArr, int i) {
        return Double.longBitsToDouble(decodeFixed64(bArr, i));
    }

    static float decodeFloat(byte[] bArr, int i) {
        return Float.intBitsToFloat(decodeFixed32(bArr, i));
    }

    static int decodeString(byte[] bArr, int i, Registers registers) throws InvalidProtocolBufferException {
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1;
        if (i2 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        } else if (i2 == 0) {
            registers.object1 = "";
            return decodeVarint32;
        } else {
            registers.object1 = new String(bArr, decodeVarint32, i2, Internal.UTF_8);
            return decodeVarint32 + i2;
        }
    }

    static int decodeStringRequireUtf8(byte[] bArr, int i, Registers registers) throws InvalidProtocolBufferException {
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1;
        if (i2 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        } else if (i2 == 0) {
            registers.object1 = "";
            return decodeVarint32;
        } else {
            registers.object1 = Utf8.decodeUtf8(bArr, decodeVarint32, i2);
            return decodeVarint32 + i2;
        }
    }

    static int decodeBytes(byte[] bArr, int i, Registers registers) throws InvalidProtocolBufferException {
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1;
        if (i2 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        } else if (i2 > bArr.length - decodeVarint32) {
            throw InvalidProtocolBufferException.truncatedMessage();
        } else if (i2 == 0) {
            registers.object1 = ByteString.EMPTY;
            return decodeVarint32;
        } else {
            registers.object1 = ByteString.copyFrom(bArr, decodeVarint32, i2);
            return decodeVarint32 + i2;
        }
    }

    /* JADX WARNING: type inference failed for: r8v2, types: [int] */
    /* JADX WARNING: type inference failed for: r8v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    static int decodeMessageField(Schema schema, byte[] bArr, int i, int i2, Registers registers) throws IOException {
        int i3 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            i3 = decodeVarint32(b, bArr, i3, registers);
            b = registers.int1;
        }
        int i4 = i3;
        if (b < 0 || b > i2 - i4) {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        Object newInstance = schema.newInstance();
        int i5 = b + i4;
        schema.mergeFrom(newInstance, bArr, i4, i5, registers);
        schema.makeImmutable(newInstance);
        registers.object1 = newInstance;
        return i5;
    }

    static int decodeGroupField(Schema schema, byte[] bArr, int i, int i2, int i3, Registers registers) throws IOException {
        MessageSchema messageSchema = (MessageSchema) schema;
        Object newInstance = messageSchema.newInstance();
        int parseProto2Message = messageSchema.parseProto2Message(newInstance, bArr, i, i2, i3, registers);
        messageSchema.makeImmutable(newInstance);
        registers.object1 = newInstance;
        return parseProto2Message;
    }

    static int decodeVarint32List(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        IntArrayList intArrayList = (IntArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i2, registers);
        intArrayList.addInt(registers.int1);
        while (decodeVarint32 < i3) {
            int decodeVarint322 = decodeVarint32(bArr, decodeVarint32, registers);
            if (i != registers.int1) {
                break;
            }
            decodeVarint32 = decodeVarint32(bArr, decodeVarint322, registers);
            intArrayList.addInt(registers.int1);
        }
        return decodeVarint32;
    }

    static int decodeVarint64List(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        LongArrayList longArrayList = (LongArrayList) protobufList;
        int decodeVarint64 = decodeVarint64(bArr, i2, registers);
        longArrayList.addLong(registers.long1);
        while (decodeVarint64 < i3) {
            int decodeVarint32 = decodeVarint32(bArr, decodeVarint64, registers);
            if (i != registers.int1) {
                break;
            }
            decodeVarint64 = decodeVarint64(bArr, decodeVarint32, registers);
            longArrayList.addLong(registers.long1);
        }
        return decodeVarint64;
    }

    static int decodeFixed32List(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        IntArrayList intArrayList = (IntArrayList) protobufList;
        intArrayList.addInt(decodeFixed32(bArr, i2));
        int i4 = i2 + 4;
        while (i4 < i3) {
            int decodeVarint32 = decodeVarint32(bArr, i4, registers);
            if (i != registers.int1) {
                break;
            }
            intArrayList.addInt(decodeFixed32(bArr, decodeVarint32));
            i4 = decodeVarint32 + 4;
        }
        return i4;
    }

    static int decodeFixed64List(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        LongArrayList longArrayList = (LongArrayList) protobufList;
        longArrayList.addLong(decodeFixed64(bArr, i2));
        int i4 = i2 + 8;
        while (i4 < i3) {
            int decodeVarint32 = decodeVarint32(bArr, i4, registers);
            if (i != registers.int1) {
                break;
            }
            longArrayList.addLong(decodeFixed64(bArr, decodeVarint32));
            i4 = decodeVarint32 + 8;
        }
        return i4;
    }

    static int decodeFloatList(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        FloatArrayList floatArrayList = (FloatArrayList) protobufList;
        floatArrayList.addFloat(decodeFloat(bArr, i2));
        int i4 = i2 + 4;
        while (i4 < i3) {
            int decodeVarint32 = decodeVarint32(bArr, i4, registers);
            if (i != registers.int1) {
                break;
            }
            floatArrayList.addFloat(decodeFloat(bArr, decodeVarint32));
            i4 = decodeVarint32 + 4;
        }
        return i4;
    }

    static int decodeDoubleList(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        DoubleArrayList doubleArrayList = (DoubleArrayList) protobufList;
        doubleArrayList.addDouble(decodeDouble(bArr, i2));
        int i4 = i2 + 8;
        while (i4 < i3) {
            int decodeVarint32 = decodeVarint32(bArr, i4, registers);
            if (i != registers.int1) {
                break;
            }
            doubleArrayList.addDouble(decodeDouble(bArr, decodeVarint32));
            i4 = decodeVarint32 + 8;
        }
        return i4;
    }

    static int decodeBoolList(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        BooleanArrayList booleanArrayList = (BooleanArrayList) protobufList;
        int decodeVarint64 = decodeVarint64(bArr, i2, registers);
        booleanArrayList.addBoolean(registers.long1 != 0);
        while (decodeVarint64 < i3) {
            int decodeVarint32 = decodeVarint32(bArr, decodeVarint64, registers);
            if (i != registers.int1) {
                break;
            }
            decodeVarint64 = decodeVarint64(bArr, decodeVarint32, registers);
            booleanArrayList.addBoolean(registers.long1 != 0);
        }
        return decodeVarint64;
    }

    static int decodeSInt32List(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        IntArrayList intArrayList = (IntArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i2, registers);
        intArrayList.addInt(CodedInputStream.decodeZigZag32(registers.int1));
        while (decodeVarint32 < i3) {
            int decodeVarint322 = decodeVarint32(bArr, decodeVarint32, registers);
            if (i != registers.int1) {
                break;
            }
            decodeVarint32 = decodeVarint32(bArr, decodeVarint322, registers);
            intArrayList.addInt(CodedInputStream.decodeZigZag32(registers.int1));
        }
        return decodeVarint32;
    }

    static int decodeSInt64List(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) {
        LongArrayList longArrayList = (LongArrayList) protobufList;
        int decodeVarint64 = decodeVarint64(bArr, i2, registers);
        longArrayList.addLong(CodedInputStream.decodeZigZag64(registers.long1));
        while (decodeVarint64 < i3) {
            int decodeVarint32 = decodeVarint32(bArr, decodeVarint64, registers);
            if (i != registers.int1) {
                break;
            }
            decodeVarint64 = decodeVarint64(bArr, decodeVarint32, registers);
            longArrayList.addLong(CodedInputStream.decodeZigZag64(registers.long1));
        }
        return decodeVarint64;
    }

    static int decodePackedVarint32List(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        IntArrayList intArrayList = (IntArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            decodeVarint32 = decodeVarint32(bArr, decodeVarint32, registers);
            intArrayList.addInt(registers.int1);
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodePackedVarint64List(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        LongArrayList longArrayList = (LongArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            decodeVarint32 = decodeVarint64(bArr, decodeVarint32, registers);
            longArrayList.addLong(registers.long1);
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodePackedFixed32List(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        IntArrayList intArrayList = (IntArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            intArrayList.addInt(decodeFixed32(bArr, decodeVarint32));
            decodeVarint32 += 4;
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodePackedFixed64List(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        LongArrayList longArrayList = (LongArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            longArrayList.addLong(decodeFixed64(bArr, decodeVarint32));
            decodeVarint32 += 8;
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodePackedFloatList(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        FloatArrayList floatArrayList = (FloatArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            floatArrayList.addFloat(decodeFloat(bArr, decodeVarint32));
            decodeVarint32 += 4;
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodePackedDoubleList(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        DoubleArrayList doubleArrayList = (DoubleArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            doubleArrayList.addDouble(decodeDouble(bArr, decodeVarint32));
            decodeVarint32 += 8;
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodePackedBoolList(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        BooleanArrayList booleanArrayList = (BooleanArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            decodeVarint32 = decodeVarint64(bArr, decodeVarint32, registers);
            booleanArrayList.addBoolean(registers.long1 != 0);
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodePackedSInt32List(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        IntArrayList intArrayList = (IntArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            decodeVarint32 = decodeVarint32(bArr, decodeVarint32, registers);
            intArrayList.addInt(CodedInputStream.decodeZigZag32(registers.int1));
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodePackedSInt64List(byte[] bArr, int i, ProtobufList<?> protobufList, Registers registers) throws IOException {
        LongArrayList longArrayList = (LongArrayList) protobufList;
        int decodeVarint32 = decodeVarint32(bArr, i, registers);
        int i2 = registers.int1 + decodeVarint32;
        while (decodeVarint32 < i2) {
            decodeVarint32 = decodeVarint64(bArr, decodeVarint32, registers);
            longArrayList.addLong(CodedInputStream.decodeZigZag64(registers.long1));
        }
        if (decodeVarint32 == i2) {
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }

    static int decodeStringList(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) throws InvalidProtocolBufferException {
        int i4;
        int decodeVarint32 = decodeVarint32(bArr, i2, registers);
        int i5 = registers.int1;
        if (i5 >= 0) {
            if (i5 == 0) {
                protobufList.add("");
            } else {
                protobufList.add(new String(bArr, decodeVarint32, i5, Internal.UTF_8));
                decodeVarint32 += i5;
            }
            while (i4 < i3) {
                int decodeVarint322 = decodeVarint32(bArr, i4, registers);
                if (i != registers.int1) {
                    break;
                }
                i4 = decodeVarint32(bArr, decodeVarint322, registers);
                int i6 = registers.int1;
                if (i6 < 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                } else if (i6 == 0) {
                    protobufList.add("");
                } else {
                    protobufList.add(new String(bArr, i4, i6, Internal.UTF_8));
                    i4 += i6;
                }
            }
            return i4;
        }
        throw InvalidProtocolBufferException.negativeSize();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        r1 = r6 + r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0044, code lost:
        if (com.explorestack.protobuf.Utf8.isValidUtf8(r5, r6, r1) == false) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        r8.add(new java.lang.String(r5, r6, r0, com.explorestack.protobuf.Internal.UTF_8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0055, code lost:
        throw com.explorestack.protobuf.InvalidProtocolBufferException.invalidUtf8();
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b A[EDGE_INSN: B:27:0x005b->B:22:0x005b ?: BREAK  
EDGE_INSN: B:27:0x005b->B:22:0x005b ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025  */
    static int decodeStringListRequireUtf8(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) throws InvalidProtocolBufferException {
        int i4;
        int decodeVarint32 = decodeVarint32(bArr, i2, registers);
        int i5 = registers.int1;
        if (i5 >= 0) {
            if (i5 == 0) {
                protobufList.add("");
                while (true) {
                    if (decodeVarint32 < i3) {
                        break;
                    }
                    int decodeVarint322 = decodeVarint32(bArr, decodeVarint32, registers);
                    if (i != registers.int1) {
                        break;
                    }
                    decodeVarint32 = decodeVarint32(bArr, decodeVarint322, registers);
                    int i6 = registers.int1;
                    if (i6 >= 0) {
                        if (i6 != 0) {
                            break;
                        }
                        protobufList.add("");
                        protobufList.add("");
                    }
                    throw InvalidProtocolBufferException.negativeSize();
                    break;
                    break;
                }
                return decodeVarint32;
            }
            i4 = decodeVarint32 + i5;
            if (Utf8.isValidUtf8(bArr, decodeVarint32, i4)) {
                protobufList.add(new String(bArr, decodeVarint32, i5, Internal.UTF_8));
            } else {
                throw InvalidProtocolBufferException.invalidUtf8();
            }
            decodeVarint32 = i4;
            while (true) {
                if (decodeVarint32 < i3) {
                }
                protobufList.add("");
            }
            return decodeVarint32;
        }
        throw InvalidProtocolBufferException.negativeSize();
    }

    static int decodeBytesList(int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) throws InvalidProtocolBufferException {
        int i4;
        int decodeVarint32 = decodeVarint32(bArr, i2, registers);
        int i5 = registers.int1;
        if (i5 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        } else if (i5 <= bArr.length - decodeVarint32) {
            if (i5 == 0) {
                protobufList.add(ByteString.EMPTY);
            } else {
                protobufList.add(ByteString.copyFrom(bArr, decodeVarint32, i5));
                decodeVarint32 += i5;
            }
            while (i4 < i3) {
                int decodeVarint322 = decodeVarint32(bArr, i4, registers);
                if (i != registers.int1) {
                    break;
                }
                i4 = decodeVarint32(bArr, decodeVarint322, registers);
                int i6 = registers.int1;
                if (i6 < 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                } else if (i6 > bArr.length - i4) {
                    throw InvalidProtocolBufferException.truncatedMessage();
                } else if (i6 == 0) {
                    protobufList.add(ByteString.EMPTY);
                } else {
                    protobufList.add(ByteString.copyFrom(bArr, i4, i6));
                    i4 += i6;
                }
            }
            return i4;
        } else {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
    }

    static int decodeMessageList(Schema<?> schema, int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) throws IOException {
        int decodeMessageField = decodeMessageField(schema, bArr, i2, i3, registers);
        protobufList.add(registers.object1);
        while (decodeMessageField < i3) {
            int decodeVarint32 = decodeVarint32(bArr, decodeMessageField, registers);
            if (i != registers.int1) {
                break;
            }
            decodeMessageField = decodeMessageField(schema, bArr, decodeVarint32, i3, registers);
            protobufList.add(registers.object1);
        }
        return decodeMessageField;
    }

    static int decodeGroupList(Schema schema, int i, byte[] bArr, int i2, int i3, ProtobufList<?> protobufList, Registers registers) throws IOException {
        int i4 = (i & -8) | 4;
        int decodeGroupField = decodeGroupField(schema, bArr, i2, i3, i4, registers);
        protobufList.add(registers.object1);
        while (decodeGroupField < i3) {
            int decodeVarint32 = decodeVarint32(bArr, decodeGroupField, registers);
            if (i != registers.int1) {
                break;
            }
            decodeGroupField = decodeGroupField(schema, bArr, decodeVarint32, i3, i4, registers);
            protobufList.add(registers.object1);
        }
        return decodeGroupField;
    }

    static int decodeExtensionOrUnknownField(int i, byte[] bArr, int i2, int i3, Object obj, MessageLite messageLite, UnknownFieldSchema<UnknownFieldSetLite, UnknownFieldSetLite> unknownFieldSchema, Registers registers) throws IOException {
        GeneratedExtension findLiteExtensionByNumber = registers.extensionRegistry.findLiteExtensionByNumber(messageLite, i >>> 3);
        if (findLiteExtensionByNumber == null) {
            return decodeUnknownField(i, bArr, i2, i3, MessageSchema.getMutableUnknownFields(obj), registers);
        }
        ExtendableMessage extendableMessage = (ExtendableMessage) obj;
        extendableMessage.ensureExtensionsAreMutable();
        return decodeExtension(i, bArr, i2, i3, extendableMessage, findLiteExtensionByNumber, unknownFieldSchema, registers);
    }

    static int decodeExtension(int i, byte[] bArr, int i2, int i3, ExtendableMessage<?, ?> extendableMessage, GeneratedExtension<?, ?> generatedExtension, UnknownFieldSchema<UnknownFieldSetLite, UnknownFieldSetLite> unknownFieldSchema, Registers registers) throws IOException {
        int i4;
        FieldSet<ExtensionDescriptor> fieldSet = extendableMessage.extensions;
        int i5 = i >>> 3;
        Object obj = null;
        if (!generatedExtension.descriptor.isRepeated() || !generatedExtension.descriptor.isPacked()) {
            if (generatedExtension.getLiteType() != FieldType.ENUM) {
                switch (generatedExtension.getLiteType()) {
                    case DOUBLE:
                        obj = Double.valueOf(decodeDouble(bArr, i2));
                        i2 += 8;
                        break;
                    case FLOAT:
                        obj = Float.valueOf(decodeFloat(bArr, i2));
                        i2 += 4;
                        break;
                    case INT64:
                    case UINT64:
                        i2 = decodeVarint64(bArr, i2, registers);
                        obj = Long.valueOf(registers.long1);
                        break;
                    case INT32:
                    case UINT32:
                        i2 = decodeVarint32(bArr, i2, registers);
                        obj = Integer.valueOf(registers.int1);
                        break;
                    case FIXED64:
                    case SFIXED64:
                        obj = Long.valueOf(decodeFixed64(bArr, i2));
                        i2 += 8;
                        break;
                    case FIXED32:
                    case SFIXED32:
                        obj = Integer.valueOf(decodeFixed32(bArr, i2));
                        i2 += 4;
                        break;
                    case BOOL:
                        i2 = decodeVarint64(bArr, i2, registers);
                        obj = Boolean.valueOf(registers.long1 != 0);
                        break;
                    case SINT32:
                        i2 = decodeVarint32(bArr, i2, registers);
                        obj = Integer.valueOf(CodedInputStream.decodeZigZag32(registers.int1));
                        break;
                    case SINT64:
                        i2 = decodeVarint64(bArr, i2, registers);
                        obj = Long.valueOf(CodedInputStream.decodeZigZag64(registers.long1));
                        break;
                    case ENUM:
                        throw new IllegalStateException("Shouldn't reach here.");
                    case BYTES:
                        i2 = decodeBytes(bArr, i2, registers);
                        obj = registers.object1;
                        break;
                    case STRING:
                        i2 = decodeString(bArr, i2, registers);
                        obj = registers.object1;
                        break;
                    case GROUP:
                        i2 = decodeGroupField(Protobuf.getInstance().schemaFor(generatedExtension.getMessageDefaultInstance().getClass()), bArr, i2, i3, (i5 << 3) | 4, registers);
                        obj = registers.object1;
                        break;
                    case MESSAGE:
                        i2 = decodeMessageField(Protobuf.getInstance().schemaFor(generatedExtension.getMessageDefaultInstance().getClass()), bArr, i2, i3, registers);
                        obj = registers.object1;
                        break;
                }
            } else {
                i2 = decodeVarint32(bArr, i2, registers);
                if (generatedExtension.descriptor.getEnumType().findValueByNumber(registers.int1) == null) {
                    UnknownFieldSetLite unknownFieldSetLite = extendableMessage.unknownFields;
                    if (unknownFieldSetLite == UnknownFieldSetLite.getDefaultInstance()) {
                        unknownFieldSetLite = UnknownFieldSetLite.newInstance();
                        extendableMessage.unknownFields = unknownFieldSetLite;
                    }
                    SchemaUtil.storeUnknownEnum(i5, registers.int1, unknownFieldSetLite, unknownFieldSchema);
                    return i2;
                }
                obj = Integer.valueOf(registers.int1);
            }
            i4 = i2;
            if (generatedExtension.isRepeated()) {
                fieldSet.addRepeatedField(generatedExtension.descriptor, obj);
            } else {
                switch (generatedExtension.getLiteType()) {
                    case GROUP:
                    case MESSAGE:
                        Object field = fieldSet.getField(generatedExtension.descriptor);
                        if (field != null) {
                            obj = Internal.mergeMessage(field, obj);
                            break;
                        }
                        break;
                }
                fieldSet.setField(generatedExtension.descriptor, obj);
            }
        } else {
            switch (generatedExtension.getLiteType()) {
                case DOUBLE:
                    DoubleArrayList doubleArrayList = new DoubleArrayList();
                    i4 = decodePackedDoubleList(bArr, i2, doubleArrayList, registers);
                    fieldSet.setField(generatedExtension.descriptor, doubleArrayList);
                    break;
                case FLOAT:
                    FloatArrayList floatArrayList = new FloatArrayList();
                    i4 = decodePackedFloatList(bArr, i2, floatArrayList, registers);
                    fieldSet.setField(generatedExtension.descriptor, floatArrayList);
                    break;
                case INT64:
                case UINT64:
                    LongArrayList longArrayList = new LongArrayList();
                    i4 = decodePackedVarint64List(bArr, i2, longArrayList, registers);
                    fieldSet.setField(generatedExtension.descriptor, longArrayList);
                    break;
                case INT32:
                case UINT32:
                    IntArrayList intArrayList = new IntArrayList();
                    i4 = decodePackedVarint32List(bArr, i2, intArrayList, registers);
                    fieldSet.setField(generatedExtension.descriptor, intArrayList);
                    break;
                case FIXED64:
                case SFIXED64:
                    LongArrayList longArrayList2 = new LongArrayList();
                    i4 = decodePackedFixed64List(bArr, i2, longArrayList2, registers);
                    fieldSet.setField(generatedExtension.descriptor, longArrayList2);
                    break;
                case FIXED32:
                case SFIXED32:
                    IntArrayList intArrayList2 = new IntArrayList();
                    i4 = decodePackedFixed32List(bArr, i2, intArrayList2, registers);
                    fieldSet.setField(generatedExtension.descriptor, intArrayList2);
                    break;
                case BOOL:
                    BooleanArrayList booleanArrayList = new BooleanArrayList();
                    i4 = decodePackedBoolList(bArr, i2, booleanArrayList, registers);
                    fieldSet.setField(generatedExtension.descriptor, booleanArrayList);
                    break;
                case SINT32:
                    IntArrayList intArrayList3 = new IntArrayList();
                    i4 = decodePackedSInt32List(bArr, i2, intArrayList3, registers);
                    fieldSet.setField(generatedExtension.descriptor, intArrayList3);
                    break;
                case SINT64:
                    LongArrayList longArrayList3 = new LongArrayList();
                    i4 = decodePackedSInt64List(bArr, i2, longArrayList3, registers);
                    fieldSet.setField(generatedExtension.descriptor, longArrayList3);
                    break;
                case ENUM:
                    IntArrayList intArrayList4 = new IntArrayList();
                    i4 = decodePackedVarint32List(bArr, i2, intArrayList4, registers);
                    UnknownFieldSetLite unknownFieldSetLite2 = extendableMessage.unknownFields;
                    if (unknownFieldSetLite2 == UnknownFieldSetLite.getDefaultInstance()) {
                        unknownFieldSetLite2 = null;
                    }
                    UnknownFieldSetLite unknownFieldSetLite3 = (UnknownFieldSetLite) SchemaUtil.filterUnknownEnumList(i5, (List<Integer>) intArrayList4, generatedExtension.descriptor.getEnumType(), unknownFieldSetLite2, unknownFieldSchema);
                    if (unknownFieldSetLite3 != null) {
                        extendableMessage.unknownFields = unknownFieldSetLite3;
                    }
                    fieldSet.setField(generatedExtension.descriptor, intArrayList4);
                    break;
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("Type cannot be packed: ");
                    sb.append(generatedExtension.descriptor.getLiteType());
                    throw new IllegalStateException(sb.toString());
            }
        }
        return i4;
    }

    static int decodeUnknownField(int i, byte[] bArr, int i2, int i3, UnknownFieldSetLite unknownFieldSetLite, Registers registers) throws InvalidProtocolBufferException {
        if (WireFormat.getTagFieldNumber(i) != 0) {
            int tagWireType = WireFormat.getTagWireType(i);
            if (tagWireType != 5) {
                switch (tagWireType) {
                    case 0:
                        int decodeVarint64 = decodeVarint64(bArr, i2, registers);
                        unknownFieldSetLite.storeField(i, Long.valueOf(registers.long1));
                        return decodeVarint64;
                    case 1:
                        unknownFieldSetLite.storeField(i, Long.valueOf(decodeFixed64(bArr, i2)));
                        return i2 + 8;
                    case 2:
                        int decodeVarint32 = decodeVarint32(bArr, i2, registers);
                        int i4 = registers.int1;
                        if (i4 < 0) {
                            throw InvalidProtocolBufferException.negativeSize();
                        } else if (i4 <= bArr.length - decodeVarint32) {
                            if (i4 == 0) {
                                unknownFieldSetLite.storeField(i, ByteString.EMPTY);
                            } else {
                                unknownFieldSetLite.storeField(i, ByteString.copyFrom(bArr, decodeVarint32, i4));
                            }
                            return decodeVarint32 + i4;
                        } else {
                            throw InvalidProtocolBufferException.truncatedMessage();
                        }
                    case 3:
                        UnknownFieldSetLite newInstance = UnknownFieldSetLite.newInstance();
                        int i5 = (i & -8) | 4;
                        int i6 = 0;
                        while (true) {
                            if (i2 < i3) {
                                int decodeVarint322 = decodeVarint32(bArr, i2, registers);
                                int i7 = registers.int1;
                                if (i7 == i5) {
                                    i6 = i7;
                                    i2 = decodeVarint322;
                                } else {
                                    i6 = i7;
                                    i2 = decodeUnknownField(i7, bArr, decodeVarint322, i3, newInstance, registers);
                                }
                            }
                        }
                        if (i2 > i3 || i6 != i5) {
                            throw InvalidProtocolBufferException.parseFailure();
                        }
                        unknownFieldSetLite.storeField(i, newInstance);
                        return i2;
                    default:
                        throw InvalidProtocolBufferException.invalidTag();
                }
            } else {
                unknownFieldSetLite.storeField(i, Integer.valueOf(decodeFixed32(bArr, i2)));
                return i2 + 4;
            }
        } else {
            throw InvalidProtocolBufferException.invalidTag();
        }
    }

    static int skipField(int i, byte[] bArr, int i2, int i3, Registers registers) throws InvalidProtocolBufferException {
        if (WireFormat.getTagFieldNumber(i) != 0) {
            int tagWireType = WireFormat.getTagWireType(i);
            if (tagWireType == 5) {
                return i2 + 4;
            }
            switch (tagWireType) {
                case 0:
                    return decodeVarint64(bArr, i2, registers);
                case 1:
                    return i2 + 8;
                case 2:
                    return decodeVarint32(bArr, i2, registers) + registers.int1;
                case 3:
                    int i4 = (i & -8) | 4;
                    int i5 = 0;
                    while (i2 < i3) {
                        i2 = decodeVarint32(bArr, i2, registers);
                        i5 = registers.int1;
                        if (i5 != i4) {
                            i2 = skipField(i5, bArr, i2, i3, registers);
                        } else if (i2 > i3 && i5 == i4) {
                            return i2;
                        } else {
                            throw InvalidProtocolBufferException.parseFailure();
                        }
                    }
                    if (i2 > i3) {
                    }
                    throw InvalidProtocolBufferException.parseFailure();
                default:
                    throw InvalidProtocolBufferException.invalidTag();
            }
        } else {
            throw InvalidProtocolBufferException.invalidTag();
        }
    }
}
