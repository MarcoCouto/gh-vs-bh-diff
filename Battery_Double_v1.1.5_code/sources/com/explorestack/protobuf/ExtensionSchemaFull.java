package com.explorestack.protobuf;

import com.explorestack.protobuf.Descriptors.EnumValueDescriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.ExtensionRegistry.ExtensionInfo;
import com.explorestack.protobuf.GeneratedMessageV3.ExtendableMessage;
import com.explorestack.protobuf.WireFormat.FieldType;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

final class ExtensionSchemaFull extends ExtensionSchema<FieldDescriptor> {
    private static final long EXTENSION_FIELD_OFFSET = getExtensionsFieldOffset();

    ExtensionSchemaFull() {
    }

    private static <T> long getExtensionsFieldOffset() {
        try {
            return UnsafeUtil.objectFieldOffset(ExtendableMessage.class.getDeclaredField("extensions"));
        } catch (Throwable unused) {
            throw new IllegalStateException("Unable to lookup extension field offset");
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean hasExtensions(MessageLite messageLite) {
        return messageLite instanceof ExtendableMessage;
    }

    public FieldSet<FieldDescriptor> getExtensions(Object obj) {
        return (FieldSet) UnsafeUtil.getObject(obj, EXTENSION_FIELD_OFFSET);
    }

    /* access modifiers changed from: 0000 */
    public void setExtensions(Object obj, FieldSet<FieldDescriptor> fieldSet) {
        UnsafeUtil.putObject(obj, EXTENSION_FIELD_OFFSET, (Object) fieldSet);
    }

    /* access modifiers changed from: 0000 */
    public FieldSet<FieldDescriptor> getMutableExtensions(Object obj) {
        FieldSet<FieldDescriptor> extensions = getExtensions(obj);
        if (!extensions.isImmutable()) {
            return extensions;
        }
        FieldSet<FieldDescriptor> clone = extensions.clone();
        setExtensions(obj, clone);
        return clone;
    }

    /* access modifiers changed from: 0000 */
    public void makeImmutable(Object obj) {
        getExtensions(obj).makeImmutable();
    }

    /* access modifiers changed from: 0000 */
    public <UT, UB> UB parseExtension(Reader reader, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<FieldDescriptor> fieldSet, UB ub, UnknownFieldSchema<UT, UB> unknownFieldSchema) throws IOException {
        ArrayList arrayList;
        ExtensionInfo extensionInfo = (ExtensionInfo) obj;
        int number = extensionInfo.descriptor.getNumber();
        if (!extensionInfo.descriptor.isRepeated() || !extensionInfo.descriptor.isPacked()) {
            Object obj2 = null;
            if (extensionInfo.descriptor.getLiteType() != FieldType.ENUM) {
                switch (extensionInfo.descriptor.getLiteType()) {
                    case DOUBLE:
                        obj2 = Double.valueOf(reader.readDouble());
                        break;
                    case FLOAT:
                        obj2 = Float.valueOf(reader.readFloat());
                        break;
                    case INT64:
                        obj2 = Long.valueOf(reader.readInt64());
                        break;
                    case UINT64:
                        obj2 = Long.valueOf(reader.readUInt64());
                        break;
                    case INT32:
                        obj2 = Integer.valueOf(reader.readInt32());
                        break;
                    case FIXED64:
                        obj2 = Long.valueOf(reader.readFixed64());
                        break;
                    case FIXED32:
                        obj2 = Integer.valueOf(reader.readFixed32());
                        break;
                    case BOOL:
                        obj2 = Boolean.valueOf(reader.readBool());
                        break;
                    case UINT32:
                        obj2 = Integer.valueOf(reader.readUInt32());
                        break;
                    case SFIXED32:
                        obj2 = Integer.valueOf(reader.readSFixed32());
                        break;
                    case SFIXED64:
                        obj2 = Long.valueOf(reader.readSFixed64());
                        break;
                    case SINT32:
                        obj2 = Integer.valueOf(reader.readSInt32());
                        break;
                    case SINT64:
                        obj2 = Long.valueOf(reader.readSInt64());
                        break;
                    case ENUM:
                        throw new IllegalStateException("Shouldn't reach here.");
                    case BYTES:
                        obj2 = reader.readBytes();
                        break;
                    case STRING:
                        obj2 = reader.readString();
                        break;
                    case GROUP:
                        obj2 = reader.readGroup(extensionInfo.defaultInstance.getClass(), extensionRegistryLite);
                        break;
                    case MESSAGE:
                        obj2 = reader.readMessage(extensionInfo.defaultInstance.getClass(), extensionRegistryLite);
                        break;
                }
            } else {
                int readInt32 = reader.readInt32();
                obj2 = extensionInfo.descriptor.getEnumType().findValueByNumber(readInt32);
                if (obj2 == null) {
                    return SchemaUtil.storeUnknownEnum(number, readInt32, ub, unknownFieldSchema);
                }
            }
            if (extensionInfo.descriptor.isRepeated()) {
                fieldSet.addRepeatedField(extensionInfo.descriptor, obj2);
            } else {
                switch (extensionInfo.descriptor.getLiteType()) {
                    case GROUP:
                    case MESSAGE:
                        Object field = fieldSet.getField(extensionInfo.descriptor);
                        if (field != null) {
                            obj2 = Internal.mergeMessage(field, obj2);
                            break;
                        }
                        break;
                }
                fieldSet.setField(extensionInfo.descriptor, obj2);
            }
        } else {
            switch (extensionInfo.descriptor.getLiteType()) {
                case DOUBLE:
                    arrayList = new ArrayList();
                    reader.readDoubleList(arrayList);
                    break;
                case FLOAT:
                    arrayList = new ArrayList();
                    reader.readFloatList(arrayList);
                    break;
                case INT64:
                    arrayList = new ArrayList();
                    reader.readInt64List(arrayList);
                    break;
                case UINT64:
                    arrayList = new ArrayList();
                    reader.readUInt64List(arrayList);
                    break;
                case INT32:
                    arrayList = new ArrayList();
                    reader.readInt32List(arrayList);
                    break;
                case FIXED64:
                    arrayList = new ArrayList();
                    reader.readFixed64List(arrayList);
                    break;
                case FIXED32:
                    arrayList = new ArrayList();
                    reader.readFixed32List(arrayList);
                    break;
                case BOOL:
                    arrayList = new ArrayList();
                    reader.readBoolList(arrayList);
                    break;
                case UINT32:
                    arrayList = new ArrayList();
                    reader.readUInt32List(arrayList);
                    break;
                case SFIXED32:
                    arrayList = new ArrayList();
                    reader.readSFixed32List(arrayList);
                    break;
                case SFIXED64:
                    arrayList = new ArrayList();
                    reader.readSFixed64List(arrayList);
                    break;
                case SINT32:
                    arrayList = new ArrayList();
                    reader.readSInt32List(arrayList);
                    break;
                case SINT64:
                    arrayList = new ArrayList();
                    reader.readSInt64List(arrayList);
                    break;
                case ENUM:
                    ArrayList<Integer> arrayList2 = new ArrayList<>();
                    reader.readEnumList(arrayList2);
                    ArrayList arrayList3 = new ArrayList();
                    for (Integer intValue : arrayList2) {
                        int intValue2 = intValue.intValue();
                        EnumValueDescriptor findValueByNumber = extensionInfo.descriptor.getEnumType().findValueByNumber(intValue2);
                        if (findValueByNumber != null) {
                            arrayList3.add(findValueByNumber);
                        } else {
                            ub = SchemaUtil.storeUnknownEnum(number, intValue2, ub, unknownFieldSchema);
                        }
                    }
                    arrayList = arrayList3;
                    break;
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("Type cannot be packed: ");
                    sb.append(extensionInfo.descriptor.getLiteType());
                    throw new IllegalStateException(sb.toString());
            }
            fieldSet.setField(extensionInfo.descriptor, arrayList);
        }
        return ub;
    }

    /* access modifiers changed from: 0000 */
    public int extensionNumber(Entry<?, ?> entry) {
        return ((FieldDescriptor) entry.getKey()).getNumber();
    }

    /* access modifiers changed from: 0000 */
    public void serializeExtension(Writer writer, Entry<?, ?> entry) throws IOException {
        FieldDescriptor fieldDescriptor = (FieldDescriptor) entry.getKey();
        if (fieldDescriptor.isRepeated()) {
            switch (fieldDescriptor.getLiteType()) {
                case DOUBLE:
                    SchemaUtil.writeDoubleList(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case FLOAT:
                    SchemaUtil.writeFloatList(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case INT64:
                    SchemaUtil.writeInt64List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case UINT64:
                    SchemaUtil.writeUInt64List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case INT32:
                    SchemaUtil.writeInt32List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case FIXED64:
                    SchemaUtil.writeFixed64List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case FIXED32:
                    SchemaUtil.writeFixed32List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case BOOL:
                    SchemaUtil.writeBoolList(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case UINT32:
                    SchemaUtil.writeUInt32List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case SFIXED32:
                    SchemaUtil.writeSFixed32List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case SFIXED64:
                    SchemaUtil.writeSFixed64List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case SINT32:
                    SchemaUtil.writeSInt32List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case SINT64:
                    SchemaUtil.writeSInt64List(fieldDescriptor.getNumber(), (List) entry.getValue(), writer, fieldDescriptor.isPacked());
                    return;
                case ENUM:
                    List<EnumValueDescriptor> list = (List) entry.getValue();
                    ArrayList arrayList = new ArrayList();
                    for (EnumValueDescriptor number : list) {
                        arrayList.add(Integer.valueOf(number.getNumber()));
                    }
                    SchemaUtil.writeInt32List(fieldDescriptor.getNumber(), arrayList, writer, fieldDescriptor.isPacked());
                    return;
                case BYTES:
                    SchemaUtil.writeBytesList(fieldDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                case STRING:
                    SchemaUtil.writeStringList(fieldDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                case GROUP:
                    SchemaUtil.writeGroupList(fieldDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                case MESSAGE:
                    SchemaUtil.writeMessageList(fieldDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                default:
                    return;
            }
        } else {
            switch (fieldDescriptor.getLiteType()) {
                case DOUBLE:
                    writer.writeDouble(fieldDescriptor.getNumber(), ((Double) entry.getValue()).doubleValue());
                    return;
                case FLOAT:
                    writer.writeFloat(fieldDescriptor.getNumber(), ((Float) entry.getValue()).floatValue());
                    return;
                case INT64:
                    writer.writeInt64(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case UINT64:
                    writer.writeUInt64(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case INT32:
                    writer.writeInt32(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case FIXED64:
                    writer.writeFixed64(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case FIXED32:
                    writer.writeFixed32(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case BOOL:
                    writer.writeBool(fieldDescriptor.getNumber(), ((Boolean) entry.getValue()).booleanValue());
                    return;
                case UINT32:
                    writer.writeUInt32(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case SFIXED32:
                    writer.writeSFixed32(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case SFIXED64:
                    writer.writeSFixed64(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case SINT32:
                    writer.writeSInt32(fieldDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case SINT64:
                    writer.writeSInt64(fieldDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case ENUM:
                    writer.writeInt32(fieldDescriptor.getNumber(), ((EnumValueDescriptor) entry.getValue()).getNumber());
                    return;
                case BYTES:
                    writer.writeBytes(fieldDescriptor.getNumber(), (ByteString) entry.getValue());
                    return;
                case STRING:
                    writer.writeString(fieldDescriptor.getNumber(), (String) entry.getValue());
                    return;
                case GROUP:
                    writer.writeGroup(fieldDescriptor.getNumber(), entry.getValue());
                    return;
                case MESSAGE:
                    writer.writeMessage(fieldDescriptor.getNumber(), entry.getValue());
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public Object findExtensionByNumber(ExtensionRegistryLite extensionRegistryLite, MessageLite messageLite, int i) {
        return ((ExtensionRegistry) extensionRegistryLite).findExtensionByNumber(((Message) messageLite).getDescriptorForType(), i);
    }

    /* access modifiers changed from: 0000 */
    public void parseLengthPrefixedMessageSetItem(Reader reader, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<FieldDescriptor> fieldSet) throws IOException {
        ExtensionInfo extensionInfo = (ExtensionInfo) obj;
        if (ExtensionRegistryLite.isEagerlyParseMessageSets()) {
            fieldSet.setField(extensionInfo.descriptor, reader.readMessage(extensionInfo.defaultInstance.getClass(), extensionRegistryLite));
            return;
        }
        fieldSet.setField(extensionInfo.descriptor, new LazyField(extensionInfo.defaultInstance, extensionRegistryLite, reader.readBytes()));
    }

    /* access modifiers changed from: 0000 */
    public void parseMessageSetItem(ByteString byteString, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<FieldDescriptor> fieldSet) throws IOException {
        ExtensionInfo extensionInfo = (ExtensionInfo) obj;
        Message buildPartial = extensionInfo.defaultInstance.newBuilderForType().buildPartial();
        if (ExtensionRegistryLite.isEagerlyParseMessageSets()) {
            BinaryReader newInstance = BinaryReader.newInstance(ByteBuffer.wrap(byteString.toByteArray()), true);
            Protobuf.getInstance().mergeFrom(buildPartial, newInstance, extensionRegistryLite);
            fieldSet.setField(extensionInfo.descriptor, buildPartial);
            if (newInstance.getFieldNumber() != Integer.MAX_VALUE) {
                throw InvalidProtocolBufferException.invalidEndTag();
            }
            return;
        }
        fieldSet.setField(extensionInfo.descriptor, new LazyField(extensionInfo.defaultInstance, extensionRegistryLite, byteString));
    }
}
