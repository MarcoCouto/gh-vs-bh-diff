package com.explorestack.protobuf;

import com.explorestack.protobuf.GeneratedMessageLite.MethodToInvoke;

final class NewInstanceSchemaLite implements NewInstanceSchema {
    NewInstanceSchemaLite() {
    }

    public Object newInstance(Object obj) {
        return ((GeneratedMessageLite) obj).dynamicMethod(MethodToInvoke.NEW_MUTABLE_INSTANCE);
    }
}
