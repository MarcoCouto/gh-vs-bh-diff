package com.explorestack.protobuf;

import com.explorestack.protobuf.Descriptors.EnumDescriptor;
import com.explorestack.protobuf.Descriptors.EnumValueDescriptor;
import com.explorestack.protobuf.Internal.EnumLite;

public interface ProtocolMessageEnum extends EnumLite {
    EnumDescriptor getDescriptorForType();

    int getNumber();

    EnumValueDescriptor getValueDescriptor();
}
