package com.explorestack.protobuf;

import com.explorestack.protobuf.FieldSet.FieldDescriptorLite;
import com.explorestack.protobuf.Internal.EnumVerifier;
import com.explorestack.protobuf.Internal.ProtobufList;
import com.explorestack.protobuf.WireFormat.FieldType;
import com.explorestack.protobuf.WireFormat.JavaType;
import com.explorestack.protobuf.Writer.FieldOrder;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import sun.misc.Unsafe;

final class MessageSchema<T> implements Schema<T> {
    private static final int[] EMPTY_INT_ARRAY = new int[0];
    private static final int ENFORCE_UTF8_MASK = 536870912;
    private static final int FIELD_TYPE_MASK = 267386880;
    private static final int INTS_PER_FIELD = 3;
    private static final int OFFSET_BITS = 20;
    private static final int OFFSET_MASK = 1048575;
    static final int ONEOF_TYPE_OFFSET = 51;
    private static final int REQUIRED_MASK = 268435456;
    private static final Unsafe UNSAFE = UnsafeUtil.getUnsafe();
    private final int[] buffer;
    private final int checkInitializedCount;
    private final MessageLite defaultInstance;
    private final ExtensionSchema<?> extensionSchema;
    private final boolean hasExtensions;
    private final int[] intArray;
    private final ListFieldSchema listFieldSchema;
    private final boolean lite;
    private final MapFieldSchema mapFieldSchema;
    private final int maxFieldNumber;
    private final int minFieldNumber;
    private final NewInstanceSchema newInstanceSchema;
    private final Object[] objects;
    private final boolean proto3;
    private final int repeatedFieldOffsetStart;
    private final UnknownFieldSchema<?, ?> unknownFieldSchema;
    private final boolean useCachedSizeField;

    private static boolean isEnforceUtf8(int i) {
        return (i & ENFORCE_UTF8_MASK) != 0;
    }

    private static boolean isRequired(int i) {
        return (i & REQUIRED_MASK) != 0;
    }

    private static long offset(int i) {
        return (long) (i & OFFSET_MASK);
    }

    private static int type(int i) {
        return (i & FIELD_TYPE_MASK) >>> 20;
    }

    private MessageSchema(int[] iArr, Object[] objArr, int i, int i2, MessageLite messageLite, boolean z, boolean z2, int[] iArr2, int i3, int i4, NewInstanceSchema newInstanceSchema2, ListFieldSchema listFieldSchema2, UnknownFieldSchema<?, ?> unknownFieldSchema2, ExtensionSchema<?> extensionSchema2, MapFieldSchema mapFieldSchema2) {
        this.buffer = iArr;
        this.objects = objArr;
        this.minFieldNumber = i;
        this.maxFieldNumber = i2;
        this.lite = messageLite instanceof GeneratedMessageLite;
        this.proto3 = z;
        this.hasExtensions = extensionSchema2 != null && extensionSchema2.hasExtensions(messageLite);
        this.useCachedSizeField = z2;
        this.intArray = iArr2;
        this.checkInitializedCount = i3;
        this.repeatedFieldOffsetStart = i4;
        this.newInstanceSchema = newInstanceSchema2;
        this.listFieldSchema = listFieldSchema2;
        this.unknownFieldSchema = unknownFieldSchema2;
        this.extensionSchema = extensionSchema2;
        this.defaultInstance = messageLite;
        this.mapFieldSchema = mapFieldSchema2;
    }

    static <T> MessageSchema<T> newSchema(Class<T> cls, MessageInfo messageInfo, NewInstanceSchema newInstanceSchema2, ListFieldSchema listFieldSchema2, UnknownFieldSchema<?, ?> unknownFieldSchema2, ExtensionSchema<?> extensionSchema2, MapFieldSchema mapFieldSchema2) {
        if (messageInfo instanceof RawMessageInfo) {
            return newSchemaForRawMessageInfo((RawMessageInfo) messageInfo, newInstanceSchema2, listFieldSchema2, unknownFieldSchema2, extensionSchema2, mapFieldSchema2);
        }
        return newSchemaForMessageInfo((StructuralMessageInfo) messageInfo, newInstanceSchema2, listFieldSchema2, unknownFieldSchema2, extensionSchema2, mapFieldSchema2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:166:0x0361  */
    static <T> MessageSchema<T> newSchemaForRawMessageInfo(RawMessageInfo rawMessageInfo, NewInstanceSchema newInstanceSchema2, ListFieldSchema listFieldSchema2, UnknownFieldSchema<?, ?> unknownFieldSchema2, ExtensionSchema<?> extensionSchema2, MapFieldSchema mapFieldSchema2) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int[] iArr;
        int i7;
        int i8;
        char c;
        int i9;
        int i10;
        Class cls;
        char c2;
        int i11;
        boolean z;
        int i12;
        int i13;
        int i14;
        int i15;
        char c3;
        Field reflectField;
        int i16;
        char charAt;
        int i17;
        int i18;
        Field reflectField2;
        Field reflectField3;
        char charAt2;
        int i19;
        char charAt3;
        int i20;
        char charAt4;
        int i21;
        int i22;
        int i23;
        int i24;
        int i25;
        int i26;
        char charAt5;
        int i27;
        char charAt6;
        int i28;
        char charAt7;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        boolean z2 = rawMessageInfo.getSyntax() == ProtoSyntax.PROTO3;
        String stringInfo = rawMessageInfo.getStringInfo();
        int length = stringInfo.length();
        char charAt15 = stringInfo.charAt(0);
        char c4 = 55296;
        if (charAt15 >= 55296) {
            char c5 = charAt15 & 8191;
            int i29 = 1;
            int i30 = 13;
            while (true) {
                i = i29 + 1;
                charAt14 = stringInfo.charAt(i29);
                if (charAt14 < 55296) {
                    break;
                }
                c5 |= (charAt14 & 8191) << i30;
                i30 += 13;
                i29 = i;
            }
            charAt15 = (charAt14 << i30) | c5;
        } else {
            i = 1;
        }
        int i31 = i + 1;
        char charAt16 = stringInfo.charAt(i);
        if (charAt16 >= 55296) {
            char c6 = charAt16 & 8191;
            int i32 = 13;
            while (true) {
                i2 = i31 + 1;
                charAt13 = stringInfo.charAt(i31);
                if (charAt13 < 55296) {
                    break;
                }
                c6 |= (charAt13 & 8191) << i32;
                i32 += 13;
                i31 = i2;
            }
            charAt16 = c6 | (charAt13 << i32);
        } else {
            i2 = i31;
        }
        if (charAt16 == 0) {
            iArr = EMPTY_INT_ARRAY;
            c = 0;
            i8 = 0;
            i7 = 0;
            i6 = 0;
            i5 = 0;
            i4 = 0;
            i3 = 0;
        } else {
            int i33 = i2 + 1;
            char charAt17 = stringInfo.charAt(i2);
            if (charAt17 >= 55296) {
                char c7 = charAt17 & 8191;
                int i34 = 13;
                while (true) {
                    i21 = i33 + 1;
                    charAt12 = stringInfo.charAt(i33);
                    if (charAt12 < 55296) {
                        break;
                    }
                    c7 |= (charAt12 & 8191) << i34;
                    i34 += 13;
                    i33 = i21;
                }
                c = (charAt12 << i34) | c7;
            } else {
                i21 = i33;
                c = charAt17;
            }
            int i35 = i21 + 1;
            char charAt18 = stringInfo.charAt(i21);
            if (charAt18 >= 55296) {
                char c8 = charAt18 & 8191;
                int i36 = 13;
                while (true) {
                    i22 = i35 + 1;
                    charAt11 = stringInfo.charAt(i35);
                    if (charAt11 < 55296) {
                        break;
                    }
                    c8 |= (charAt11 & 8191) << i36;
                    i36 += 13;
                    i35 = i22;
                }
                charAt18 = c8 | (charAt11 << i36);
            } else {
                i22 = i35;
            }
            int i37 = i22 + 1;
            int charAt19 = stringInfo.charAt(i22);
            if (charAt19 >= 55296) {
                int i38 = charAt19 & 8191;
                int i39 = 13;
                while (true) {
                    i23 = i37 + 1;
                    charAt10 = stringInfo.charAt(i37);
                    if (charAt10 < 55296) {
                        break;
                    }
                    i38 |= (charAt10 & 8191) << i39;
                    i39 += 13;
                    i37 = i23;
                }
                charAt19 = (charAt10 << i39) | i38;
            } else {
                i23 = i37;
            }
            int i40 = i23 + 1;
            int charAt20 = stringInfo.charAt(i23);
            if (charAt20 >= 55296) {
                int i41 = charAt20 & 8191;
                int i42 = 13;
                while (true) {
                    i24 = i40 + 1;
                    charAt9 = stringInfo.charAt(i40);
                    if (charAt9 < 55296) {
                        break;
                    }
                    i41 |= (charAt9 & 8191) << i42;
                    i42 += 13;
                    i40 = i24;
                }
                charAt20 = (charAt9 << i42) | i41;
            } else {
                i24 = i40;
            }
            int i43 = i24 + 1;
            i6 = stringInfo.charAt(i24);
            if (i6 >= 55296) {
                int i44 = i6 & 8191;
                int i45 = 13;
                while (true) {
                    i25 = i43 + 1;
                    charAt8 = stringInfo.charAt(i43);
                    if (charAt8 < 55296) {
                        break;
                    }
                    i44 |= (charAt8 & 8191) << i45;
                    i45 += 13;
                    i43 = i25;
                }
                i6 = (charAt8 << i45) | i44;
            } else {
                i25 = i43;
            }
            int i46 = i25 + 1;
            i5 = stringInfo.charAt(i25);
            if (i5 >= 55296) {
                int i47 = i5 & 8191;
                int i48 = 13;
                while (true) {
                    i28 = i46 + 1;
                    charAt7 = stringInfo.charAt(i46);
                    if (charAt7 < 55296) {
                        break;
                    }
                    i47 |= (charAt7 & 8191) << i48;
                    i48 += 13;
                    i46 = i28;
                }
                i5 = (charAt7 << i48) | i47;
                i46 = i28;
            }
            int i49 = i46 + 1;
            char charAt21 = stringInfo.charAt(i46);
            if (charAt21 >= 55296) {
                char c9 = charAt21 & 8191;
                int i50 = 13;
                while (true) {
                    i27 = i49 + 1;
                    charAt6 = stringInfo.charAt(i49);
                    if (charAt6 < 55296) {
                        break;
                    }
                    c9 |= (charAt6 & 8191) << i50;
                    i50 += 13;
                    i49 = i27;
                }
                charAt21 = c9 | (charAt6 << i50);
                i49 = i27;
            }
            int i51 = i49 + 1;
            i4 = stringInfo.charAt(i49);
            if (i4 >= 55296) {
                int i52 = 13;
                int i53 = i51;
                int i54 = i4 & 8191;
                int i55 = i53;
                while (true) {
                    i26 = i55 + 1;
                    charAt5 = stringInfo.charAt(i55);
                    if (charAt5 < 55296) {
                        break;
                    }
                    i54 |= (charAt5 & 8191) << i52;
                    i52 += 13;
                    i55 = i26;
                }
                i4 = i54 | (charAt5 << i52);
                i51 = i26;
            }
            i7 = (c * 2) + charAt18;
            int i56 = charAt20;
            iArr = new int[(i4 + i5 + charAt21)];
            i8 = charAt19;
            i2 = i51;
            i3 = i56;
        }
        Unsafe unsafe = UNSAFE;
        Object[] objects2 = rawMessageInfo.getObjects();
        Class cls2 = rawMessageInfo.getDefaultInstance().getClass();
        int[] iArr2 = new int[(i6 * 3)];
        Object[] objArr = new Object[(i6 * 2)];
        int i57 = i5 + i4;
        int i58 = i7;
        int i59 = i57;
        int i60 = i4;
        int i61 = 0;
        int i62 = 0;
        while (i2 < length) {
            int i63 = i2 + 1;
            int charAt22 = stringInfo.charAt(i2);
            if (charAt22 >= c4) {
                int i64 = 13;
                int i65 = i63;
                int i66 = charAt22 & 8191;
                int i67 = i65;
                while (true) {
                    i20 = i67 + 1;
                    charAt4 = stringInfo.charAt(i67);
                    if (charAt4 < c4) {
                        break;
                    }
                    i66 |= (charAt4 & 8191) << i64;
                    i64 += 13;
                    i67 = i20;
                }
                charAt22 = i66 | (charAt4 << i64);
                i9 = i20;
            } else {
                i9 = i63;
            }
            int i68 = i9 + 1;
            char charAt23 = stringInfo.charAt(i9);
            int i69 = length;
            char c10 = 55296;
            if (charAt23 >= 55296) {
                int i70 = 13;
                int i71 = i68;
                int i72 = charAt23 & 8191;
                int i73 = i71;
                while (true) {
                    i19 = i73 + 1;
                    charAt3 = stringInfo.charAt(i73);
                    if (charAt3 < c10) {
                        break;
                    }
                    i72 |= (charAt3 & 8191) << i70;
                    i70 += 13;
                    i73 = i19;
                    c10 = 55296;
                }
                charAt23 = i72 | (charAt3 << i70);
                i10 = i19;
            } else {
                i10 = i68;
            }
            int i74 = i57;
            char c11 = charAt23 & 255;
            int i75 = i4;
            if ((charAt23 & 1024) != 0) {
                int i76 = i61 + 1;
                iArr[i61] = i62;
                i61 = i76;
            }
            int i77 = i61;
            if (c11 >= '3') {
                int i78 = i10 + 1;
                char charAt24 = stringInfo.charAt(i10);
                char c12 = 55296;
                if (charAt24 >= 55296) {
                    char c13 = charAt24 & 8191;
                    int i79 = 13;
                    while (true) {
                        i17 = i78 + 1;
                        charAt2 = stringInfo.charAt(i78);
                        if (charAt2 < c12) {
                            break;
                        }
                        c13 |= (charAt2 & 8191) << i79;
                        i79 += 13;
                        i78 = i17;
                        c12 = 55296;
                    }
                    charAt24 = c13 | (charAt2 << i79);
                } else {
                    i17 = i78;
                }
                int i80 = c11 - '3';
                if (i80 == 9 || i80 == 17) {
                    i18 = i58 + 1;
                    objArr[((i62 / 3) * 2) + 1] = objects2[i58];
                } else if (i80 == 12 && (charAt15 & 1) == 1) {
                    i18 = i58 + 1;
                    objArr[((i62 / 3) * 2) + 1] = objects2[i58];
                } else {
                    i18 = i58;
                }
                int i81 = charAt24 * 2;
                Object obj = objects2[i81];
                int i82 = i18;
                if (obj instanceof Field) {
                    reflectField2 = (Field) obj;
                } else {
                    reflectField2 = reflectField(cls2, (String) obj);
                    objects2[i81] = reflectField2;
                }
                z = z2;
                int objectFieldOffset = (int) unsafe.objectFieldOffset(reflectField2);
                int i83 = i81 + 1;
                Object obj2 = objects2[i83];
                if (obj2 instanceof Field) {
                    reflectField3 = (Field) obj2;
                } else {
                    reflectField3 = reflectField(cls2, (String) obj2);
                    objects2[i83] = reflectField3;
                }
                int objectFieldOffset2 = (int) unsafe.objectFieldOffset(reflectField3);
                c2 = charAt15;
                cls = cls2;
                i11 = i8;
                i12 = i17;
                i58 = i82;
                i13 = objectFieldOffset;
                i14 = objectFieldOffset2;
                i15 = 0;
            } else {
                z = z2;
                int i84 = i58 + 1;
                Field reflectField4 = reflectField(cls2, (String) objects2[i58]);
                if (c11 == 9 || c11 == 17) {
                    i11 = i8;
                    c3 = 1;
                    objArr[((i62 / 3) * 2) + 1] = reflectField4.getType();
                } else if (c11 == 27 || c11 == '1') {
                    i11 = i8;
                    c3 = 1;
                    i58 = i84 + 1;
                    objArr[((i62 / 3) * 2) + 1] = objects2[i84];
                    i13 = (int) unsafe.objectFieldOffset(reflectField4);
                    if ((charAt15 & 1) == c3 || c11 > 17) {
                        c2 = charAt15;
                        cls = cls2;
                        i12 = i10;
                        i15 = 0;
                        i14 = 0;
                    } else {
                        i12 = i10 + 1;
                        char charAt25 = stringInfo.charAt(i10);
                        if (charAt25 >= 55296) {
                            char c14 = charAt25 & 8191;
                            int i85 = 13;
                            while (true) {
                                i16 = i12 + 1;
                                charAt = stringInfo.charAt(i12);
                                if (charAt < 55296) {
                                    break;
                                }
                                c14 |= (charAt & 8191) << i85;
                                i85 += 13;
                                i12 = i16;
                            }
                            charAt25 = c14 | (charAt << i85);
                            i12 = i16;
                        }
                        int i86 = (c * 2) + (charAt25 / ' ');
                        Object obj3 = objects2[i86];
                        if (obj3 instanceof Field) {
                            reflectField = (Field) obj3;
                        } else {
                            reflectField = reflectField(cls2, (String) obj3);
                            objects2[i86] = reflectField;
                        }
                        c2 = charAt15;
                        cls = cls2;
                        i14 = (int) unsafe.objectFieldOffset(reflectField);
                        i15 = charAt25 % ' ';
                    }
                    if (c11 >= 18 && c11 <= '1') {
                        int i87 = i59 + 1;
                        iArr[i59] = i13;
                        i59 = i87;
                    }
                } else if (c11 == 12 || c11 == 30 || c11 == ',') {
                    i11 = i8;
                    c3 = 1;
                    if ((charAt15 & 1) == 1) {
                        i58 = i84 + 1;
                        objArr[((i62 / 3) * 2) + 1] = objects2[i84];
                        i13 = (int) unsafe.objectFieldOffset(reflectField4);
                        if ((charAt15 & 1) == c3) {
                        }
                        c2 = charAt15;
                        cls = cls2;
                        i12 = i10;
                        i15 = 0;
                        i14 = 0;
                        int i872 = i59 + 1;
                        iArr[i59] = i13;
                        i59 = i872;
                    }
                } else if (c11 == '2') {
                    int i88 = i60 + 1;
                    iArr[i60] = i62;
                    int i89 = (i62 / 3) * 2;
                    int i90 = i84 + 1;
                    objArr[i89] = objects2[i84];
                    if ((charAt23 & 2048) != 0) {
                        int i91 = i90 + 1;
                        objArr[i89 + 1] = objects2[i90];
                        i11 = i8;
                        i58 = i91;
                        i60 = i88;
                        c3 = 1;
                    } else {
                        i11 = i8;
                        i58 = i90;
                        c3 = 1;
                        i60 = i88;
                    }
                    i13 = (int) unsafe.objectFieldOffset(reflectField4);
                    if ((charAt15 & 1) == c3) {
                    }
                    c2 = charAt15;
                    cls = cls2;
                    i12 = i10;
                    i15 = 0;
                    i14 = 0;
                    int i8722 = i59 + 1;
                    iArr[i59] = i13;
                    i59 = i8722;
                } else {
                    i11 = i8;
                    c3 = 1;
                }
                i58 = i84;
                i13 = (int) unsafe.objectFieldOffset(reflectField4);
                if ((charAt15 & 1) == c3) {
                }
                c2 = charAt15;
                cls = cls2;
                i12 = i10;
                i15 = 0;
                i14 = 0;
                int i87222 = i59 + 1;
                iArr[i59] = i13;
                i59 = i87222;
            }
            int i92 = i62 + 1;
            iArr2[i62] = charAt22;
            int i93 = i92 + 1;
            iArr2[i92] = ((charAt23 & 256) != 0 ? REQUIRED_MASK : 0) | ((charAt23 & 512) != 0 ? ENFORCE_UTF8_MASK : 0) | (c11 << 20) | i13;
            i62 = i93 + 1;
            iArr2[i93] = (i15 << 20) | i14;
            i2 = i12;
            length = i69;
            i57 = i74;
            i4 = i75;
            i61 = i77;
            z2 = z;
            i8 = i11;
            charAt15 = c2;
            cls2 = cls;
            c4 = 55296;
        }
        boolean z3 = z2;
        int i94 = i57;
        MessageSchema messageSchema = new MessageSchema(iArr2, objArr, i8, i3, rawMessageInfo.getDefaultInstance(), z2, false, iArr, i4, i57, newInstanceSchema2, listFieldSchema2, unknownFieldSchema2, extensionSchema2, mapFieldSchema2);
        return messageSchema;
    }

    private static Field reflectField(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(cls.getName());
            sb.append(" not found. Known fields are ");
            sb.append(Arrays.toString(declaredFields));
            throw new RuntimeException(sb.toString());
        }
    }

    static <T> MessageSchema<T> newSchemaForMessageInfo(StructuralMessageInfo structuralMessageInfo, NewInstanceSchema newInstanceSchema2, ListFieldSchema listFieldSchema2, UnknownFieldSchema<?, ?> unknownFieldSchema2, ExtensionSchema<?> extensionSchema2, MapFieldSchema mapFieldSchema2) {
        int i;
        int i2;
        int i3;
        boolean z = structuralMessageInfo.getSyntax() == ProtoSyntax.PROTO3;
        FieldInfo[] fields = structuralMessageInfo.getFields();
        if (fields.length == 0) {
            i2 = 0;
            i = 0;
        } else {
            i2 = fields[0].getFieldNumber();
            i = fields[fields.length - 1].getFieldNumber();
        }
        int length = fields.length;
        int[] iArr = new int[(length * 3)];
        Object[] objArr = new Object[(length * 2)];
        int i4 = 0;
        int i5 = 0;
        for (FieldInfo fieldInfo : fields) {
            if (fieldInfo.getType() == FieldType.MAP) {
                i4++;
            } else if (fieldInfo.getType().id() >= 18 && fieldInfo.getType().id() <= 49) {
                i5++;
            }
        }
        int[] iArr2 = null;
        int[] iArr3 = i4 > 0 ? new int[i4] : null;
        if (i5 > 0) {
            iArr2 = new int[i5];
        }
        int[] checkInitialized = structuralMessageInfo.getCheckInitialized();
        if (checkInitialized == null) {
            checkInitialized = EMPTY_INT_ARRAY;
        }
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        while (i6 < fields.length) {
            FieldInfo fieldInfo2 = fields[i6];
            int fieldNumber = fieldInfo2.getFieldNumber();
            storeFieldData(fieldInfo2, iArr, i7, z, objArr);
            if (i8 < checkInitialized.length && checkInitialized[i8] == fieldNumber) {
                int i11 = i8 + 1;
                checkInitialized[i8] = i7;
                i8 = i11;
            }
            if (fieldInfo2.getType() == FieldType.MAP) {
                int i12 = i9 + 1;
                iArr3[i9] = i7;
                i9 = i12;
            } else if (fieldInfo2.getType().id() >= 18 && fieldInfo2.getType().id() <= 49) {
                int i13 = i10 + 1;
                i3 = i7;
                iArr2[i10] = (int) UnsafeUtil.objectFieldOffset(fieldInfo2.getField());
                i10 = i13;
                i6++;
                i7 = i3 + 3;
            }
            i3 = i7;
            i6++;
            i7 = i3 + 3;
        }
        if (iArr3 == null) {
            iArr3 = EMPTY_INT_ARRAY;
        }
        if (iArr2 == null) {
            iArr2 = EMPTY_INT_ARRAY;
        }
        int[] iArr4 = new int[(checkInitialized.length + iArr3.length + iArr2.length)];
        System.arraycopy(checkInitialized, 0, iArr4, 0, checkInitialized.length);
        System.arraycopy(iArr3, 0, iArr4, checkInitialized.length, iArr3.length);
        System.arraycopy(iArr2, 0, iArr4, checkInitialized.length + iArr3.length, iArr2.length);
        MessageSchema messageSchema = new MessageSchema(iArr, objArr, i2, i, structuralMessageInfo.getDefaultInstance(), z, true, iArr4, checkInitialized.length, checkInitialized.length + iArr3.length, newInstanceSchema2, listFieldSchema2, unknownFieldSchema2, extensionSchema2, mapFieldSchema2);
        return messageSchema;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bc  */
    private static void storeFieldData(FieldInfo fieldInfo, int[] iArr, int i, boolean z, Object[] objArr) {
        int i2;
        int i3;
        int i4;
        int i5;
        OneofInfo oneof = fieldInfo.getOneof();
        int i6 = 0;
        if (oneof != null) {
            i4 = (int) UnsafeUtil.objectFieldOffset(oneof.getValueField());
            i3 = fieldInfo.getType().id() + 51;
            i2 = (int) UnsafeUtil.objectFieldOffset(oneof.getCaseField());
        } else {
            FieldType type = fieldInfo.getType();
            i4 = (int) UnsafeUtil.objectFieldOffset(fieldInfo.getField());
            i3 = type.id();
            if (!z && !type.isList() && !type.isMap()) {
                i2 = (int) UnsafeUtil.objectFieldOffset(fieldInfo.getPresenceField());
                i5 = Integer.numberOfTrailingZeros(fieldInfo.getPresenceMask());
                iArr[i] = fieldInfo.getFieldNumber();
                int i7 = i + 1;
                int i8 = !fieldInfo.isEnforceUtf8() ? ENFORCE_UTF8_MASK : 0;
                if (fieldInfo.isRequired()) {
                    i6 = REQUIRED_MASK;
                }
                iArr[i7] = i6 | i8 | (i3 << 20) | i4;
                iArr[i + 2] = i2 | (i5 << 20);
                Class messageFieldClass = fieldInfo.getMessageFieldClass();
                if (fieldInfo.getMapDefaultEntry() == null) {
                    int i9 = (i / 3) * 2;
                    objArr[i9] = fieldInfo.getMapDefaultEntry();
                    if (messageFieldClass != null) {
                        objArr[i9 + 1] = messageFieldClass;
                        return;
                    } else if (fieldInfo.getEnumVerifier() != null) {
                        objArr[i9 + 1] = fieldInfo.getEnumVerifier();
                        return;
                    } else {
                        return;
                    }
                } else if (messageFieldClass != null) {
                    objArr[((i / 3) * 2) + 1] = messageFieldClass;
                    return;
                } else if (fieldInfo.getEnumVerifier() != null) {
                    objArr[((i / 3) * 2) + 1] = fieldInfo.getEnumVerifier();
                    return;
                } else {
                    return;
                }
            } else if (fieldInfo.getCachedSizeField() == null) {
                i2 = 0;
            } else {
                i2 = (int) UnsafeUtil.objectFieldOffset(fieldInfo.getCachedSizeField());
            }
        }
        i5 = 0;
        iArr[i] = fieldInfo.getFieldNumber();
        int i72 = i + 1;
        if (!fieldInfo.isEnforceUtf8()) {
        }
        if (fieldInfo.isRequired()) {
        }
        iArr[i72] = i6 | i8 | (i3 << 20) | i4;
        iArr[i + 2] = i2 | (i5 << 20);
        Class messageFieldClass2 = fieldInfo.getMessageFieldClass();
        if (fieldInfo.getMapDefaultEntry() == null) {
        }
    }

    public T newInstance() {
        return this.newInstanceSchema.newInstance(this.defaultInstance);
    }

    public boolean equals(T t, T t2) {
        int length = this.buffer.length;
        for (int i = 0; i < length; i += 3) {
            if (!equals(t, t2, i)) {
                return false;
            }
        }
        if (!this.unknownFieldSchema.getFromMessage(t).equals(this.unknownFieldSchema.getFromMessage(t2))) {
            return false;
        }
        if (this.hasExtensions) {
            return this.extensionSchema.getExtensions(t).equals(this.extensionSchema.getExtensions(t2));
        }
        return true;
    }

    private boolean equals(T t, T t2, int i) {
        int typeAndOffsetAt = typeAndOffsetAt(i);
        long offset = offset(typeAndOffsetAt);
        boolean z = false;
        switch (type(typeAndOffsetAt)) {
            case 0:
                if (arePresentForEquals(t, t2, i) && Double.doubleToLongBits(UnsafeUtil.getDouble((Object) t, offset)) == Double.doubleToLongBits(UnsafeUtil.getDouble((Object) t2, offset))) {
                    z = true;
                }
                return z;
            case 1:
                if (arePresentForEquals(t, t2, i) && Float.floatToIntBits(UnsafeUtil.getFloat((Object) t, offset)) == Float.floatToIntBits(UnsafeUtil.getFloat((Object) t2, offset))) {
                    z = true;
                }
                return z;
            case 2:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getLong((Object) t, offset) == UnsafeUtil.getLong((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 3:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getLong((Object) t, offset) == UnsafeUtil.getLong((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 4:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getInt((Object) t, offset) == UnsafeUtil.getInt((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 5:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getLong((Object) t, offset) == UnsafeUtil.getLong((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 6:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getInt((Object) t, offset) == UnsafeUtil.getInt((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 7:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getBoolean((Object) t, offset) == UnsafeUtil.getBoolean((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 8:
                if (arePresentForEquals(t, t2, i) && SchemaUtil.safeEquals(UnsafeUtil.getObject((Object) t, offset), UnsafeUtil.getObject((Object) t2, offset))) {
                    z = true;
                }
                return z;
            case 9:
                if (arePresentForEquals(t, t2, i) && SchemaUtil.safeEquals(UnsafeUtil.getObject((Object) t, offset), UnsafeUtil.getObject((Object) t2, offset))) {
                    z = true;
                }
                return z;
            case 10:
                if (arePresentForEquals(t, t2, i) && SchemaUtil.safeEquals(UnsafeUtil.getObject((Object) t, offset), UnsafeUtil.getObject((Object) t2, offset))) {
                    z = true;
                }
                return z;
            case 11:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getInt((Object) t, offset) == UnsafeUtil.getInt((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 12:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getInt((Object) t, offset) == UnsafeUtil.getInt((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 13:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getInt((Object) t, offset) == UnsafeUtil.getInt((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 14:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getLong((Object) t, offset) == UnsafeUtil.getLong((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 15:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getInt((Object) t, offset) == UnsafeUtil.getInt((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 16:
                if (arePresentForEquals(t, t2, i) && UnsafeUtil.getLong((Object) t, offset) == UnsafeUtil.getLong((Object) t2, offset)) {
                    z = true;
                }
                return z;
            case 17:
                if (arePresentForEquals(t, t2, i) && SchemaUtil.safeEquals(UnsafeUtil.getObject((Object) t, offset), UnsafeUtil.getObject((Object) t2, offset))) {
                    z = true;
                }
                return z;
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
                return SchemaUtil.safeEquals(UnsafeUtil.getObject((Object) t, offset), UnsafeUtil.getObject((Object) t2, offset));
            case 50:
                return SchemaUtil.safeEquals(UnsafeUtil.getObject((Object) t, offset), UnsafeUtil.getObject((Object) t2, offset));
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
                if (isOneofCaseEqual(t, t2, i) && SchemaUtil.safeEquals(UnsafeUtil.getObject((Object) t, offset), UnsafeUtil.getObject((Object) t2, offset))) {
                    z = true;
                }
                return z;
            default:
                return true;
        }
    }

    public int hashCode(T t) {
        int length = this.buffer.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2 += 3) {
            int typeAndOffsetAt = typeAndOffsetAt(i2);
            int numberAt = numberAt(i2);
            long offset = offset(typeAndOffsetAt);
            int i3 = 37;
            switch (type(typeAndOffsetAt)) {
                case 0:
                    i = (i * 53) + Internal.hashLong(Double.doubleToLongBits(UnsafeUtil.getDouble((Object) t, offset)));
                    break;
                case 1:
                    i = (i * 53) + Float.floatToIntBits(UnsafeUtil.getFloat((Object) t, offset));
                    break;
                case 2:
                    i = (i * 53) + Internal.hashLong(UnsafeUtil.getLong((Object) t, offset));
                    break;
                case 3:
                    i = (i * 53) + Internal.hashLong(UnsafeUtil.getLong((Object) t, offset));
                    break;
                case 4:
                    i = (i * 53) + UnsafeUtil.getInt((Object) t, offset);
                    break;
                case 5:
                    i = (i * 53) + Internal.hashLong(UnsafeUtil.getLong((Object) t, offset));
                    break;
                case 6:
                    i = (i * 53) + UnsafeUtil.getInt((Object) t, offset);
                    break;
                case 7:
                    i = (i * 53) + Internal.hashBoolean(UnsafeUtil.getBoolean((Object) t, offset));
                    break;
                case 8:
                    i = (i * 53) + ((String) UnsafeUtil.getObject((Object) t, offset)).hashCode();
                    break;
                case 9:
                    Object object = UnsafeUtil.getObject((Object) t, offset);
                    if (object != null) {
                        i3 = object.hashCode();
                    }
                    i = (i * 53) + i3;
                    break;
                case 10:
                    i = (i * 53) + UnsafeUtil.getObject((Object) t, offset).hashCode();
                    break;
                case 11:
                    i = (i * 53) + UnsafeUtil.getInt((Object) t, offset);
                    break;
                case 12:
                    i = (i * 53) + UnsafeUtil.getInt((Object) t, offset);
                    break;
                case 13:
                    i = (i * 53) + UnsafeUtil.getInt((Object) t, offset);
                    break;
                case 14:
                    i = (i * 53) + Internal.hashLong(UnsafeUtil.getLong((Object) t, offset));
                    break;
                case 15:
                    i = (i * 53) + UnsafeUtil.getInt((Object) t, offset);
                    break;
                case 16:
                    i = (i * 53) + Internal.hashLong(UnsafeUtil.getLong((Object) t, offset));
                    break;
                case 17:
                    Object object2 = UnsafeUtil.getObject((Object) t, offset);
                    if (object2 != null) {
                        i3 = object2.hashCode();
                    }
                    i = (i * 53) + i3;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i = (i * 53) + UnsafeUtil.getObject((Object) t, offset).hashCode();
                    break;
                case 50:
                    i = (i * 53) + UnsafeUtil.getObject((Object) t, offset).hashCode();
                    break;
                case 51:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Internal.hashLong(Double.doubleToLongBits(oneofDoubleAt(t, offset)));
                        break;
                    }
                case 52:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Float.floatToIntBits(oneofFloatAt(t, offset));
                        break;
                    }
                case 53:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Internal.hashLong(oneofLongAt(t, offset));
                        break;
                    }
                case 54:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Internal.hashLong(oneofLongAt(t, offset));
                        break;
                    }
                case 55:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + oneofIntAt(t, offset);
                        break;
                    }
                case 56:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Internal.hashLong(oneofLongAt(t, offset));
                        break;
                    }
                case 57:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + oneofIntAt(t, offset);
                        break;
                    }
                case 58:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Internal.hashBoolean(oneofBooleanAt(t, offset));
                        break;
                    }
                case 59:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + ((String) UnsafeUtil.getObject((Object) t, offset)).hashCode();
                        break;
                    }
                case 60:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + UnsafeUtil.getObject((Object) t, offset).hashCode();
                        break;
                    }
                case 61:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + UnsafeUtil.getObject((Object) t, offset).hashCode();
                        break;
                    }
                case 62:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + oneofIntAt(t, offset);
                        break;
                    }
                case 63:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + oneofIntAt(t, offset);
                        break;
                    }
                case 64:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + oneofIntAt(t, offset);
                        break;
                    }
                case 65:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Internal.hashLong(oneofLongAt(t, offset));
                        break;
                    }
                case 66:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + oneofIntAt(t, offset);
                        break;
                    }
                case 67:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Internal.hashLong(oneofLongAt(t, offset));
                        break;
                    }
                case 68:
                    if (!isOneofPresent(t, numberAt, i2)) {
                        break;
                    } else {
                        i = (i * 53) + UnsafeUtil.getObject((Object) t, offset).hashCode();
                        break;
                    }
            }
        }
        int hashCode = (i * 53) + this.unknownFieldSchema.getFromMessage(t).hashCode();
        return this.hasExtensions ? (hashCode * 53) + this.extensionSchema.getExtensions(t).hashCode() : hashCode;
    }

    public void mergeFrom(T t, T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.buffer.length; i += 3) {
                mergeSingleField(t, t2, i);
            }
            if (!this.proto3) {
                SchemaUtil.mergeUnknownFields(this.unknownFieldSchema, t, t2);
                if (this.hasExtensions) {
                    SchemaUtil.mergeExtensions(this.extensionSchema, t, t2);
                    return;
                }
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    private void mergeSingleField(T t, T t2, int i) {
        int typeAndOffsetAt = typeAndOffsetAt(i);
        long offset = offset(typeAndOffsetAt);
        int numberAt = numberAt(i);
        switch (type(typeAndOffsetAt)) {
            case 0:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putDouble((Object) t, offset, UnsafeUtil.getDouble((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 1:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putFloat((Object) t, offset, UnsafeUtil.getFloat((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 2:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putLong((Object) t, offset, UnsafeUtil.getLong((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 3:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putLong((Object) t, offset, UnsafeUtil.getLong((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 4:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putInt((Object) t, offset, UnsafeUtil.getInt((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 5:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putLong((Object) t, offset, UnsafeUtil.getLong((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 6:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putInt((Object) t, offset, UnsafeUtil.getInt((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 7:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putBoolean((Object) t, offset, UnsafeUtil.getBoolean((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 8:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putObject((Object) t, offset, UnsafeUtil.getObject((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 9:
                mergeMessage(t, t2, i);
                return;
            case 10:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putObject((Object) t, offset, UnsafeUtil.getObject((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 11:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putInt((Object) t, offset, UnsafeUtil.getInt((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 12:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putInt((Object) t, offset, UnsafeUtil.getInt((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 13:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putInt((Object) t, offset, UnsafeUtil.getInt((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 14:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putLong((Object) t, offset, UnsafeUtil.getLong((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 15:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putInt((Object) t, offset, UnsafeUtil.getInt((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 16:
                if (isFieldPresent(t2, i)) {
                    UnsafeUtil.putLong((Object) t, offset, UnsafeUtil.getLong((Object) t2, offset));
                    setFieldPresent(t, i);
                    return;
                }
                return;
            case 17:
                mergeMessage(t, t2, i);
                return;
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
                this.listFieldSchema.mergeListsAt(t, t2, offset);
                return;
            case 50:
                SchemaUtil.mergeMap(this.mapFieldSchema, t, t2, offset);
                return;
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
                if (isOneofPresent(t2, numberAt, i)) {
                    UnsafeUtil.putObject((Object) t, offset, UnsafeUtil.getObject((Object) t2, offset));
                    setOneofPresent(t, numberAt, i);
                    return;
                }
                return;
            case 60:
                mergeOneofMessage(t, t2, i);
                return;
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
                if (isOneofPresent(t2, numberAt, i)) {
                    UnsafeUtil.putObject((Object) t, offset, UnsafeUtil.getObject((Object) t2, offset));
                    setOneofPresent(t, numberAt, i);
                    return;
                }
                return;
            case 68:
                mergeOneofMessage(t, t2, i);
                return;
            default:
                return;
        }
    }

    private void mergeMessage(T t, T t2, int i) {
        long offset = offset(typeAndOffsetAt(i));
        if (isFieldPresent(t2, i)) {
            Object object = UnsafeUtil.getObject((Object) t, offset);
            Object object2 = UnsafeUtil.getObject((Object) t2, offset);
            if (object != null && object2 != null) {
                UnsafeUtil.putObject((Object) t, offset, Internal.mergeMessage(object, object2));
                setFieldPresent(t, i);
            } else if (object2 != null) {
                UnsafeUtil.putObject((Object) t, offset, object2);
                setFieldPresent(t, i);
            }
        }
    }

    private void mergeOneofMessage(T t, T t2, int i) {
        int typeAndOffsetAt = typeAndOffsetAt(i);
        int numberAt = numberAt(i);
        long offset = offset(typeAndOffsetAt);
        if (isOneofPresent(t2, numberAt, i)) {
            Object object = UnsafeUtil.getObject((Object) t, offset);
            Object object2 = UnsafeUtil.getObject((Object) t2, offset);
            if (object != null && object2 != null) {
                UnsafeUtil.putObject((Object) t, offset, Internal.mergeMessage(object, object2));
                setOneofPresent(t, numberAt, i);
            } else if (object2 != null) {
                UnsafeUtil.putObject((Object) t, offset, object2);
                setOneofPresent(t, numberAt, i);
            }
        }
    }

    public int getSerializedSize(T t) {
        return this.proto3 ? getSerializedSizeProto3(t) : getSerializedSizeProto2(t);
    }

    private int getSerializedSizeProto2(T t) {
        int i;
        int i2;
        int i3;
        T t2 = t;
        Unsafe unsafe = UNSAFE;
        int i4 = 0;
        int i5 = -1;
        int i6 = 0;
        for (int i7 = 0; i7 < this.buffer.length; i7 = i3 + 3) {
            int typeAndOffsetAt = typeAndOffsetAt(i7);
            int numberAt = numberAt(i7);
            int type = type(typeAndOffsetAt);
            if (type <= 17) {
                i2 = this.buffer[i7 + 2];
                int i8 = OFFSET_MASK & i2;
                int i9 = 1 << (i2 >>> 20);
                if (i8 != i5) {
                    i6 = unsafe.getInt(t2, (long) i8);
                    i5 = i8;
                }
                i = i9;
            } else {
                i2 = (!this.useCachedSizeField || type < FieldType.DOUBLE_LIST_PACKED.id() || type > FieldType.SINT64_LIST_PACKED.id()) ? 0 : this.buffer[i7 + 2] & OFFSET_MASK;
                i = 0;
            }
            long offset = offset(typeAndOffsetAt);
            int i10 = i7;
            switch (type) {
                case 0:
                    i3 = i10;
                    if ((i6 & i) == 0) {
                        break;
                    } else {
                        i4 += CodedOutputStream.computeDoubleSize(numberAt, Utils.DOUBLE_EPSILON);
                        continue;
                    }
                case 1:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeFloatSize(numberAt, 0.0f);
                        break;
                    } else {
                        continue;
                    }
                case 2:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeInt64Size(numberAt, unsafe.getLong(t2, offset));
                        break;
                    } else {
                        continue;
                    }
                case 3:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeUInt64Size(numberAt, unsafe.getLong(t2, offset));
                        break;
                    } else {
                        continue;
                    }
                case 4:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeInt32Size(numberAt, unsafe.getInt(t2, offset));
                        break;
                    } else {
                        continue;
                    }
                case 5:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeFixed64Size(numberAt, 0);
                        break;
                    } else {
                        continue;
                    }
                case 6:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeFixed32Size(numberAt, 0);
                        continue;
                    }
                case 7:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeBoolSize(numberAt, true);
                    }
                    break;
                case 8:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        Object object = unsafe.getObject(t2, offset);
                        if (object instanceof ByteString) {
                            i4 += CodedOutputStream.computeBytesSize(numberAt, (ByteString) object);
                        } else {
                            i4 += CodedOutputStream.computeStringSize(numberAt, (String) object);
                        }
                    }
                    break;
                case 9:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += SchemaUtil.computeSizeMessage(numberAt, unsafe.getObject(t2, offset), getMessageFieldSchema(i3));
                    }
                    break;
                case 10:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeBytesSize(numberAt, (ByteString) unsafe.getObject(t2, offset));
                    }
                    break;
                case 11:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeUInt32Size(numberAt, unsafe.getInt(t2, offset));
                    }
                    break;
                case 12:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeEnumSize(numberAt, unsafe.getInt(t2, offset));
                    }
                    break;
                case 13:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeSFixed32Size(numberAt, 0);
                    }
                    break;
                case 14:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeSFixed64Size(numberAt, 0);
                    }
                    break;
                case 15:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeSInt32Size(numberAt, unsafe.getInt(t2, offset));
                    }
                    break;
                case 16:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeSInt64Size(numberAt, unsafe.getLong(t2, offset));
                    }
                    break;
                case 17:
                    i3 = i10;
                    if ((i6 & i) != 0) {
                        i4 += CodedOutputStream.computeGroupSize(numberAt, (MessageLite) unsafe.getObject(t2, offset), getMessageFieldSchema(i3));
                    }
                    break;
                case 18:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeFixed64List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 19:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeFixed32List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 20:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeInt64List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 21:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeUInt64List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 22:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeInt32List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 23:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeFixed64List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 24:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeFixed32List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 25:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeBoolList(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 26:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeStringList(numberAt, (List) unsafe.getObject(t2, offset));
                    break;
                case 27:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeMessageList(numberAt, (List) unsafe.getObject(t2, offset), getMessageFieldSchema(i3));
                    break;
                case 28:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeByteStringList(numberAt, (List) unsafe.getObject(t2, offset));
                    break;
                case 29:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeUInt32List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 30:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeEnumList(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 31:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeFixed32List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 32:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeFixed64List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 33:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeSInt32List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 34:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeSInt64List(numberAt, (List) unsafe.getObject(t2, offset), false);
                    break;
                case 35:
                    i3 = i10;
                    int computeSizeFixed64ListNoTag = SchemaUtil.computeSizeFixed64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed64ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeFixed64ListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed64ListNoTag) + computeSizeFixed64ListNoTag;
                    }
                    break;
                case 36:
                    i3 = i10;
                    int computeSizeFixed32ListNoTag = SchemaUtil.computeSizeFixed32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed32ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeFixed32ListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed32ListNoTag) + computeSizeFixed32ListNoTag;
                    }
                    break;
                case 37:
                    i3 = i10;
                    int computeSizeInt64ListNoTag = SchemaUtil.computeSizeInt64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeInt64ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeInt64ListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeInt64ListNoTag) + computeSizeInt64ListNoTag;
                    }
                    break;
                case 38:
                    i3 = i10;
                    int computeSizeUInt64ListNoTag = SchemaUtil.computeSizeUInt64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeUInt64ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeUInt64ListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeUInt64ListNoTag) + computeSizeUInt64ListNoTag;
                    }
                    break;
                case 39:
                    i3 = i10;
                    int computeSizeInt32ListNoTag = SchemaUtil.computeSizeInt32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeInt32ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeInt32ListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeInt32ListNoTag) + computeSizeInt32ListNoTag;
                    }
                    break;
                case 40:
                    i3 = i10;
                    int computeSizeFixed64ListNoTag2 = SchemaUtil.computeSizeFixed64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed64ListNoTag2 > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeFixed64ListNoTag2);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed64ListNoTag2) + computeSizeFixed64ListNoTag2;
                    }
                    break;
                case 41:
                    i3 = i10;
                    int computeSizeFixed32ListNoTag2 = SchemaUtil.computeSizeFixed32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed32ListNoTag2 > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeFixed32ListNoTag2);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed32ListNoTag2) + computeSizeFixed32ListNoTag2;
                    }
                    break;
                case 42:
                    i3 = i10;
                    int computeSizeBoolListNoTag = SchemaUtil.computeSizeBoolListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeBoolListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeBoolListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeBoolListNoTag) + computeSizeBoolListNoTag;
                    }
                    break;
                case 43:
                    i3 = i10;
                    int computeSizeUInt32ListNoTag = SchemaUtil.computeSizeUInt32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeUInt32ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeUInt32ListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeUInt32ListNoTag) + computeSizeUInt32ListNoTag;
                    }
                    break;
                case 44:
                    i3 = i10;
                    int computeSizeEnumListNoTag = SchemaUtil.computeSizeEnumListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeEnumListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeEnumListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeEnumListNoTag) + computeSizeEnumListNoTag;
                    }
                    break;
                case 45:
                    i3 = i10;
                    int computeSizeFixed32ListNoTag3 = SchemaUtil.computeSizeFixed32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed32ListNoTag3 > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeFixed32ListNoTag3);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed32ListNoTag3) + computeSizeFixed32ListNoTag3;
                    }
                    break;
                case 46:
                    i3 = i10;
                    int computeSizeFixed64ListNoTag3 = SchemaUtil.computeSizeFixed64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed64ListNoTag3 > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeFixed64ListNoTag3);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed64ListNoTag3) + computeSizeFixed64ListNoTag3;
                    }
                    break;
                case 47:
                    i3 = i10;
                    int computeSizeSInt32ListNoTag = SchemaUtil.computeSizeSInt32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeSInt32ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeSInt32ListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeSInt32ListNoTag) + computeSizeSInt32ListNoTag;
                    }
                    break;
                case 48:
                    i3 = i10;
                    int computeSizeSInt64ListNoTag = SchemaUtil.computeSizeSInt64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeSInt64ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i2, computeSizeSInt64ListNoTag);
                        }
                        i4 += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeSInt64ListNoTag) + computeSizeSInt64ListNoTag;
                    }
                    break;
                case 49:
                    i3 = i10;
                    i4 += SchemaUtil.computeSizeGroupList(numberAt, (List) unsafe.getObject(t2, offset), getMessageFieldSchema(i3));
                    break;
                case 50:
                    i3 = i10;
                    i4 += this.mapFieldSchema.getSerializedSize(numberAt, unsafe.getObject(t2, offset), getMapFieldDefaultEntry(i3));
                    break;
                case 51:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeDoubleSize(numberAt, Utils.DOUBLE_EPSILON);
                    }
                    break;
                case 52:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeFloatSize(numberAt, 0.0f);
                    }
                    break;
                case 53:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeInt64Size(numberAt, oneofLongAt(t2, offset));
                    }
                    break;
                case 54:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeUInt64Size(numberAt, oneofLongAt(t2, offset));
                    }
                    break;
                case 55:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeInt32Size(numberAt, oneofIntAt(t2, offset));
                    }
                    break;
                case 56:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeFixed64Size(numberAt, 0);
                    }
                    break;
                case 57:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeFixed32Size(numberAt, 0);
                    }
                    break;
                case 58:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeBoolSize(numberAt, true);
                    }
                    break;
                case 59:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        Object object2 = unsafe.getObject(t2, offset);
                        if (object2 instanceof ByteString) {
                            i4 += CodedOutputStream.computeBytesSize(numberAt, (ByteString) object2);
                        } else {
                            i4 += CodedOutputStream.computeStringSize(numberAt, (String) object2);
                        }
                    }
                    break;
                case 60:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += SchemaUtil.computeSizeMessage(numberAt, unsafe.getObject(t2, offset), getMessageFieldSchema(i3));
                    }
                    break;
                case 61:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeBytesSize(numberAt, (ByteString) unsafe.getObject(t2, offset));
                    }
                    break;
                case 62:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeUInt32Size(numberAt, oneofIntAt(t2, offset));
                    }
                    break;
                case 63:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeEnumSize(numberAt, oneofIntAt(t2, offset));
                    }
                    break;
                case 64:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeSFixed32Size(numberAt, 0);
                    }
                    break;
                case 65:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeSFixed64Size(numberAt, 0);
                    }
                    break;
                case 66:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeSInt32Size(numberAt, oneofIntAt(t2, offset));
                    }
                    break;
                case 67:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeSInt64Size(numberAt, oneofLongAt(t2, offset));
                    }
                    break;
                case 68:
                    i3 = i10;
                    if (isOneofPresent(t2, numberAt, i3)) {
                        i4 += CodedOutputStream.computeGroupSize(numberAt, (MessageLite) unsafe.getObject(t2, offset), getMessageFieldSchema(i3));
                    }
                    break;
                default:
                    i3 = i10;
            }
        }
        int unknownFieldsSerializedSize = i4 + getUnknownFieldsSerializedSize(this.unknownFieldSchema, t2);
        return this.hasExtensions ? unknownFieldsSerializedSize + this.extensionSchema.getExtensions(t2).getSerializedSize() : unknownFieldsSerializedSize;
    }

    private int getSerializedSizeProto3(T t) {
        T t2 = t;
        Unsafe unsafe = UNSAFE;
        int i = 0;
        for (int i2 = 0; i2 < this.buffer.length; i2 += 3) {
            int typeAndOffsetAt = typeAndOffsetAt(i2);
            int type = type(typeAndOffsetAt);
            int numberAt = numberAt(i2);
            long offset = offset(typeAndOffsetAt);
            int i3 = (type < FieldType.DOUBLE_LIST_PACKED.id() || type > FieldType.SINT64_LIST_PACKED.id()) ? 0 : this.buffer[i2 + 2] & OFFSET_MASK;
            switch (type) {
                case 0:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeDoubleSize(numberAt, Utils.DOUBLE_EPSILON);
                        break;
                    }
                case 1:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeFloatSize(numberAt, 0.0f);
                        break;
                    }
                case 2:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeInt64Size(numberAt, UnsafeUtil.getLong((Object) t2, offset));
                        break;
                    }
                case 3:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeUInt64Size(numberAt, UnsafeUtil.getLong((Object) t2, offset));
                        break;
                    }
                case 4:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeInt32Size(numberAt, UnsafeUtil.getInt((Object) t2, offset));
                        break;
                    }
                case 5:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeFixed64Size(numberAt, 0);
                        break;
                    }
                case 6:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeFixed32Size(numberAt, 0);
                        break;
                    }
                case 7:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeBoolSize(numberAt, true);
                        break;
                    }
                case 8:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        Object object = UnsafeUtil.getObject((Object) t2, offset);
                        if (!(object instanceof ByteString)) {
                            i += CodedOutputStream.computeStringSize(numberAt, (String) object);
                            break;
                        } else {
                            i += CodedOutputStream.computeBytesSize(numberAt, (ByteString) object);
                            break;
                        }
                    }
                case 9:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += SchemaUtil.computeSizeMessage(numberAt, UnsafeUtil.getObject((Object) t2, offset), getMessageFieldSchema(i2));
                        break;
                    }
                case 10:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeBytesSize(numberAt, (ByteString) UnsafeUtil.getObject((Object) t2, offset));
                        break;
                    }
                case 11:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeUInt32Size(numberAt, UnsafeUtil.getInt((Object) t2, offset));
                        break;
                    }
                case 12:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeEnumSize(numberAt, UnsafeUtil.getInt((Object) t2, offset));
                        break;
                    }
                case 13:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeSFixed32Size(numberAt, 0);
                        break;
                    }
                case 14:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeSFixed64Size(numberAt, 0);
                        break;
                    }
                case 15:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeSInt32Size(numberAt, UnsafeUtil.getInt((Object) t2, offset));
                        break;
                    }
                case 16:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeSInt64Size(numberAt, UnsafeUtil.getLong((Object) t2, offset));
                        break;
                    }
                case 17:
                    if (!isFieldPresent(t2, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeGroupSize(numberAt, (MessageLite) UnsafeUtil.getObject((Object) t2, offset), getMessageFieldSchema(i2));
                        break;
                    }
                case 18:
                    i += SchemaUtil.computeSizeFixed64List(numberAt, listAt(t2, offset), false);
                    break;
                case 19:
                    i += SchemaUtil.computeSizeFixed32List(numberAt, listAt(t2, offset), false);
                    break;
                case 20:
                    i += SchemaUtil.computeSizeInt64List(numberAt, listAt(t2, offset), false);
                    break;
                case 21:
                    i += SchemaUtil.computeSizeUInt64List(numberAt, listAt(t2, offset), false);
                    break;
                case 22:
                    i += SchemaUtil.computeSizeInt32List(numberAt, listAt(t2, offset), false);
                    break;
                case 23:
                    i += SchemaUtil.computeSizeFixed64List(numberAt, listAt(t2, offset), false);
                    break;
                case 24:
                    i += SchemaUtil.computeSizeFixed32List(numberAt, listAt(t2, offset), false);
                    break;
                case 25:
                    i += SchemaUtil.computeSizeBoolList(numberAt, listAt(t2, offset), false);
                    break;
                case 26:
                    i += SchemaUtil.computeSizeStringList(numberAt, listAt(t2, offset));
                    break;
                case 27:
                    i += SchemaUtil.computeSizeMessageList(numberAt, listAt(t2, offset), getMessageFieldSchema(i2));
                    break;
                case 28:
                    i += SchemaUtil.computeSizeByteStringList(numberAt, listAt(t2, offset));
                    break;
                case 29:
                    i += SchemaUtil.computeSizeUInt32List(numberAt, listAt(t2, offset), false);
                    break;
                case 30:
                    i += SchemaUtil.computeSizeEnumList(numberAt, listAt(t2, offset), false);
                    break;
                case 31:
                    i += SchemaUtil.computeSizeFixed32List(numberAt, listAt(t2, offset), false);
                    break;
                case 32:
                    i += SchemaUtil.computeSizeFixed64List(numberAt, listAt(t2, offset), false);
                    break;
                case 33:
                    i += SchemaUtil.computeSizeSInt32List(numberAt, listAt(t2, offset), false);
                    break;
                case 34:
                    i += SchemaUtil.computeSizeSInt64List(numberAt, listAt(t2, offset), false);
                    break;
                case 35:
                    int computeSizeFixed64ListNoTag = SchemaUtil.computeSizeFixed64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed64ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeFixed64ListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed64ListNoTag) + computeSizeFixed64ListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 36:
                    int computeSizeFixed32ListNoTag = SchemaUtil.computeSizeFixed32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed32ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeFixed32ListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed32ListNoTag) + computeSizeFixed32ListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 37:
                    int computeSizeInt64ListNoTag = SchemaUtil.computeSizeInt64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeInt64ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeInt64ListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeInt64ListNoTag) + computeSizeInt64ListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 38:
                    int computeSizeUInt64ListNoTag = SchemaUtil.computeSizeUInt64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeUInt64ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeUInt64ListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeUInt64ListNoTag) + computeSizeUInt64ListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 39:
                    int computeSizeInt32ListNoTag = SchemaUtil.computeSizeInt32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeInt32ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeInt32ListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeInt32ListNoTag) + computeSizeInt32ListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 40:
                    int computeSizeFixed64ListNoTag2 = SchemaUtil.computeSizeFixed64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed64ListNoTag2 > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeFixed64ListNoTag2);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed64ListNoTag2) + computeSizeFixed64ListNoTag2;
                        break;
                    } else {
                        break;
                    }
                case 41:
                    int computeSizeFixed32ListNoTag2 = SchemaUtil.computeSizeFixed32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed32ListNoTag2 > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeFixed32ListNoTag2);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed32ListNoTag2) + computeSizeFixed32ListNoTag2;
                        break;
                    } else {
                        break;
                    }
                case 42:
                    int computeSizeBoolListNoTag = SchemaUtil.computeSizeBoolListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeBoolListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeBoolListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeBoolListNoTag) + computeSizeBoolListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 43:
                    int computeSizeUInt32ListNoTag = SchemaUtil.computeSizeUInt32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeUInt32ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeUInt32ListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeUInt32ListNoTag) + computeSizeUInt32ListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 44:
                    int computeSizeEnumListNoTag = SchemaUtil.computeSizeEnumListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeEnumListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeEnumListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeEnumListNoTag) + computeSizeEnumListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 45:
                    int computeSizeFixed32ListNoTag3 = SchemaUtil.computeSizeFixed32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed32ListNoTag3 > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeFixed32ListNoTag3);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed32ListNoTag3) + computeSizeFixed32ListNoTag3;
                        break;
                    } else {
                        break;
                    }
                case 46:
                    int computeSizeFixed64ListNoTag3 = SchemaUtil.computeSizeFixed64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeFixed64ListNoTag3 > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeFixed64ListNoTag3);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeFixed64ListNoTag3) + computeSizeFixed64ListNoTag3;
                        break;
                    } else {
                        break;
                    }
                case 47:
                    int computeSizeSInt32ListNoTag = SchemaUtil.computeSizeSInt32ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeSInt32ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeSInt32ListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeSInt32ListNoTag) + computeSizeSInt32ListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 48:
                    int computeSizeSInt64ListNoTag = SchemaUtil.computeSizeSInt64ListNoTag((List) unsafe.getObject(t2, offset));
                    if (computeSizeSInt64ListNoTag > 0) {
                        if (this.useCachedSizeField) {
                            unsafe.putInt(t2, (long) i3, computeSizeSInt64ListNoTag);
                        }
                        i += CodedOutputStream.computeTagSize(numberAt) + CodedOutputStream.computeUInt32SizeNoTag(computeSizeSInt64ListNoTag) + computeSizeSInt64ListNoTag;
                        break;
                    } else {
                        break;
                    }
                case 49:
                    i += SchemaUtil.computeSizeGroupList(numberAt, listAt(t2, offset), getMessageFieldSchema(i2));
                    break;
                case 50:
                    i += this.mapFieldSchema.getSerializedSize(numberAt, UnsafeUtil.getObject((Object) t2, offset), getMapFieldDefaultEntry(i2));
                    break;
                case 51:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeDoubleSize(numberAt, Utils.DOUBLE_EPSILON);
                        break;
                    }
                case 52:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeFloatSize(numberAt, 0.0f);
                        break;
                    }
                case 53:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeInt64Size(numberAt, oneofLongAt(t2, offset));
                        break;
                    }
                case 54:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeUInt64Size(numberAt, oneofLongAt(t2, offset));
                        break;
                    }
                case 55:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeInt32Size(numberAt, oneofIntAt(t2, offset));
                        break;
                    }
                case 56:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeFixed64Size(numberAt, 0);
                        break;
                    }
                case 57:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeFixed32Size(numberAt, 0);
                        break;
                    }
                case 58:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeBoolSize(numberAt, true);
                        break;
                    }
                case 59:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        Object object2 = UnsafeUtil.getObject((Object) t2, offset);
                        if (!(object2 instanceof ByteString)) {
                            i += CodedOutputStream.computeStringSize(numberAt, (String) object2);
                            break;
                        } else {
                            i += CodedOutputStream.computeBytesSize(numberAt, (ByteString) object2);
                            break;
                        }
                    }
                case 60:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += SchemaUtil.computeSizeMessage(numberAt, UnsafeUtil.getObject((Object) t2, offset), getMessageFieldSchema(i2));
                        break;
                    }
                case 61:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeBytesSize(numberAt, (ByteString) UnsafeUtil.getObject((Object) t2, offset));
                        break;
                    }
                case 62:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeUInt32Size(numberAt, oneofIntAt(t2, offset));
                        break;
                    }
                case 63:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeEnumSize(numberAt, oneofIntAt(t2, offset));
                        break;
                    }
                case 64:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeSFixed32Size(numberAt, 0);
                        break;
                    }
                case 65:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeSFixed64Size(numberAt, 0);
                        break;
                    }
                case 66:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeSInt32Size(numberAt, oneofIntAt(t2, offset));
                        break;
                    }
                case 67:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeSInt64Size(numberAt, oneofLongAt(t2, offset));
                        break;
                    }
                case 68:
                    if (!isOneofPresent(t2, numberAt, i2)) {
                        break;
                    } else {
                        i += CodedOutputStream.computeGroupSize(numberAt, (MessageLite) UnsafeUtil.getObject((Object) t2, offset), getMessageFieldSchema(i2));
                        break;
                    }
            }
        }
        return i + getUnknownFieldsSerializedSize(this.unknownFieldSchema, t2);
    }

    private <UT, UB> int getUnknownFieldsSerializedSize(UnknownFieldSchema<UT, UB> unknownFieldSchema2, T t) {
        return unknownFieldSchema2.getSerializedSize(unknownFieldSchema2.getFromMessage(t));
    }

    private static List<?> listAt(Object obj, long j) {
        return (List) UnsafeUtil.getObject(obj, j);
    }

    public void writeTo(T t, Writer writer) throws IOException {
        if (writer.fieldOrder() == FieldOrder.DESCENDING) {
            writeFieldsInDescendingOrder(t, writer);
        } else if (this.proto3) {
            writeFieldsInAscendingOrderProto3(t, writer);
        } else {
            writeFieldsInAscendingOrderProto2(t, writer);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:172:0x0527  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    private void writeFieldsInAscendingOrderProto2(T t, Writer writer) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        Entry entry2;
        int i;
        int i2;
        int i3;
        int i4;
        T t2 = t;
        Writer writer2 = writer;
        if (this.hasExtensions) {
            FieldSet extensions = this.extensionSchema.getExtensions(t2);
            if (!extensions.isEmpty()) {
                it = extensions.iterator();
                entry = (Entry) it.next();
                int i5 = -1;
                length = this.buffer.length;
                Unsafe unsafe = UNSAFE;
                entry2 = entry;
                int i6 = 0;
                for (i = 0; i < length; i = i4 + 3) {
                    int typeAndOffsetAt = typeAndOffsetAt(i);
                    int numberAt = numberAt(i);
                    int type = type(typeAndOffsetAt);
                    if (this.proto3 || type > 17) {
                        i2 = i;
                        i3 = 0;
                    } else {
                        int i7 = this.buffer[i + 2];
                        int i8 = i7 & OFFSET_MASK;
                        if (i8 != i5) {
                            i2 = i;
                            i6 = unsafe.getInt(t2, (long) i8);
                            i5 = i8;
                        } else {
                            i2 = i;
                        }
                        i3 = 1 << (i7 >>> 20);
                    }
                    while (entry2 != null && this.extensionSchema.extensionNumber(entry2) <= numberAt) {
                        this.extensionSchema.serializeExtension(writer2, entry2);
                        entry2 = it.hasNext() ? (Entry) it.next() : null;
                    }
                    long offset = offset(typeAndOffsetAt);
                    switch (type) {
                        case 0:
                            i4 = i2;
                            if ((i3 & i6) == 0) {
                                break;
                            } else {
                                writer2.writeDouble(numberAt, doubleAt(t2, offset));
                                continue;
                            }
                        case 1:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeFloat(numberAt, floatAt(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 2:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeInt64(numberAt, unsafe.getLong(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 3:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeUInt64(numberAt, unsafe.getLong(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 4:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeInt32(numberAt, unsafe.getInt(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 5:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeFixed64(numberAt, unsafe.getLong(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 6:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeFixed32(numberAt, unsafe.getInt(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 7:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeBool(numberAt, booleanAt(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 8:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writeString(numberAt, unsafe.getObject(t2, offset), writer2);
                                break;
                            } else {
                                continue;
                            }
                        case 9:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeMessage(numberAt, unsafe.getObject(t2, offset), getMessageFieldSchema(i4));
                                break;
                            } else {
                                continue;
                            }
                        case 10:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeBytes(numberAt, (ByteString) unsafe.getObject(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 11:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeUInt32(numberAt, unsafe.getInt(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 12:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeEnum(numberAt, unsafe.getInt(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 13:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeSFixed32(numberAt, unsafe.getInt(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 14:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeSFixed64(numberAt, unsafe.getLong(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 15:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeSInt32(numberAt, unsafe.getInt(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 16:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeSInt64(numberAt, unsafe.getLong(t2, offset));
                                break;
                            } else {
                                continue;
                            }
                        case 17:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                writer2.writeGroup(numberAt, unsafe.getObject(t2, offset), getMessageFieldSchema(i4));
                                break;
                            } else {
                                continue;
                            }
                        case 18:
                            i4 = i2;
                            SchemaUtil.writeDoubleList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            continue;
                        case 19:
                            i4 = i2;
                            SchemaUtil.writeFloatList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            continue;
                        case 20:
                            i4 = i2;
                            SchemaUtil.writeInt64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            continue;
                        case 21:
                            i4 = i2;
                            SchemaUtil.writeUInt64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            continue;
                        case 22:
                            i4 = i2;
                            SchemaUtil.writeInt32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            continue;
                        case 23:
                            i4 = i2;
                            SchemaUtil.writeFixed64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            continue;
                        case 24:
                            i4 = i2;
                            SchemaUtil.writeFixed32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            continue;
                        case 25:
                            i4 = i2;
                            SchemaUtil.writeBoolList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            continue;
                        case 26:
                            i4 = i2;
                            SchemaUtil.writeStringList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2);
                            break;
                        case 27:
                            i4 = i2;
                            SchemaUtil.writeMessageList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, getMessageFieldSchema(i4));
                            break;
                        case 28:
                            i4 = i2;
                            SchemaUtil.writeBytesList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2);
                            break;
                        case 29:
                            i4 = i2;
                            SchemaUtil.writeUInt32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            break;
                        case 30:
                            i4 = i2;
                            SchemaUtil.writeEnumList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            break;
                        case 31:
                            i4 = i2;
                            SchemaUtil.writeSFixed32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            break;
                        case 32:
                            i4 = i2;
                            SchemaUtil.writeSFixed64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            break;
                        case 33:
                            i4 = i2;
                            SchemaUtil.writeSInt32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            break;
                        case 34:
                            i4 = i2;
                            SchemaUtil.writeSInt64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, false);
                            break;
                        case 35:
                            i4 = i2;
                            SchemaUtil.writeDoubleList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 36:
                            i4 = i2;
                            SchemaUtil.writeFloatList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 37:
                            i4 = i2;
                            SchemaUtil.writeInt64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 38:
                            i4 = i2;
                            SchemaUtil.writeUInt64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 39:
                            i4 = i2;
                            SchemaUtil.writeInt32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 40:
                            i4 = i2;
                            SchemaUtil.writeFixed64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 41:
                            i4 = i2;
                            SchemaUtil.writeFixed32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 42:
                            i4 = i2;
                            SchemaUtil.writeBoolList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 43:
                            i4 = i2;
                            SchemaUtil.writeUInt32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 44:
                            i4 = i2;
                            SchemaUtil.writeEnumList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 45:
                            i4 = i2;
                            SchemaUtil.writeSFixed32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 46:
                            i4 = i2;
                            SchemaUtil.writeSFixed64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 47:
                            i4 = i2;
                            SchemaUtil.writeSInt32List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 48:
                            i4 = i2;
                            SchemaUtil.writeSInt64List(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, true);
                            break;
                        case 49:
                            i4 = i2;
                            SchemaUtil.writeGroupList(numberAt(i4), (List) unsafe.getObject(t2, offset), writer2, getMessageFieldSchema(i4));
                            break;
                        case 50:
                            i4 = i2;
                            writeMapHelper(writer2, numberAt, unsafe.getObject(t2, offset), i4);
                            break;
                        case 51:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeDouble(numberAt, oneofDoubleAt(t2, offset));
                                break;
                            }
                            break;
                        case 52:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeFloat(numberAt, oneofFloatAt(t2, offset));
                                break;
                            }
                            break;
                        case 53:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeInt64(numberAt, oneofLongAt(t2, offset));
                                break;
                            }
                            break;
                        case 54:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeUInt64(numberAt, oneofLongAt(t2, offset));
                                break;
                            }
                            break;
                        case 55:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeInt32(numberAt, oneofIntAt(t2, offset));
                                break;
                            }
                            break;
                        case 56:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeFixed64(numberAt, oneofLongAt(t2, offset));
                                break;
                            }
                            break;
                        case 57:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeFixed32(numberAt, oneofIntAt(t2, offset));
                                break;
                            }
                            break;
                        case 58:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeBool(numberAt, oneofBooleanAt(t2, offset));
                                break;
                            }
                            break;
                        case 59:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writeString(numberAt, unsafe.getObject(t2, offset), writer2);
                                break;
                            }
                            break;
                        case 60:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeMessage(numberAt, unsafe.getObject(t2, offset), getMessageFieldSchema(i4));
                                break;
                            }
                            break;
                        case 61:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeBytes(numberAt, (ByteString) unsafe.getObject(t2, offset));
                                break;
                            }
                            break;
                        case 62:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeUInt32(numberAt, oneofIntAt(t2, offset));
                                break;
                            }
                            break;
                        case 63:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeEnum(numberAt, oneofIntAt(t2, offset));
                                break;
                            }
                            break;
                        case 64:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeSFixed32(numberAt, oneofIntAt(t2, offset));
                                break;
                            }
                            break;
                        case 65:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeSFixed64(numberAt, oneofLongAt(t2, offset));
                                break;
                            }
                            break;
                        case 66:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeSInt32(numberAt, oneofIntAt(t2, offset));
                                break;
                            }
                            break;
                        case 67:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeSInt64(numberAt, oneofLongAt(t2, offset));
                                break;
                            }
                            break;
                        case 68:
                            i4 = i2;
                            if (isOneofPresent(t2, numberAt, i4)) {
                                writer2.writeGroup(numberAt, unsafe.getObject(t2, offset), getMessageFieldSchema(i4));
                                break;
                            }
                            break;
                        default:
                            i4 = i2;
                            break;
                    }
                }
                while (entry2 != null) {
                    this.extensionSchema.serializeExtension(writer2, entry2);
                    entry2 = it.hasNext() ? (Entry) it.next() : null;
                }
                writeUnknownInMessageTo(this.unknownFieldSchema, t2, writer2);
            }
        }
        it = null;
        entry = null;
        int i52 = -1;
        length = this.buffer.length;
        Unsafe unsafe2 = UNSAFE;
        entry2 = entry;
        int i62 = 0;
        while (i < length) {
        }
        while (entry2 != null) {
        }
        writeUnknownInMessageTo(this.unknownFieldSchema, t2, writer2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:161:0x0589  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    private void writeFieldsInAscendingOrderProto3(T t, Writer writer) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        Entry entry2;
        int i;
        if (this.hasExtensions) {
            FieldSet extensions = this.extensionSchema.getExtensions(t);
            if (!extensions.isEmpty()) {
                it = extensions.iterator();
                entry = (Entry) it.next();
                length = this.buffer.length;
                entry2 = entry;
                for (i = 0; i < length; i += 3) {
                    int typeAndOffsetAt = typeAndOffsetAt(i);
                    int numberAt = numberAt(i);
                    while (entry2 != null && this.extensionSchema.extensionNumber(entry2) <= numberAt) {
                        this.extensionSchema.serializeExtension(writer, entry2);
                        entry2 = it.hasNext() ? (Entry) it.next() : null;
                    }
                    switch (type(typeAndOffsetAt)) {
                        case 0:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeDouble(numberAt, doubleAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 1:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeFloat(numberAt, floatAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 2:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeInt64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 3:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeUInt64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 4:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeInt32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 5:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeFixed64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 6:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeFixed32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 7:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeBool(numberAt, booleanAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 8:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writeString(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer);
                                break;
                            }
                        case 9:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeMessage(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), getMessageFieldSchema(i));
                                break;
                            }
                        case 10:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeBytes(numberAt, (ByteString) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 11:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeUInt32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 12:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeEnum(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 13:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeSFixed32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 14:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeSFixed64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 15:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeSInt32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 16:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeSInt64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 17:
                            if (!isFieldPresent(t, i)) {
                                break;
                            } else {
                                writer.writeGroup(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), getMessageFieldSchema(i));
                                break;
                            }
                        case 18:
                            SchemaUtil.writeDoubleList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 19:
                            SchemaUtil.writeFloatList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 20:
                            SchemaUtil.writeInt64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 21:
                            SchemaUtil.writeUInt64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 22:
                            SchemaUtil.writeInt32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 23:
                            SchemaUtil.writeFixed64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 24:
                            SchemaUtil.writeFixed32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 25:
                            SchemaUtil.writeBoolList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 26:
                            SchemaUtil.writeStringList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer);
                            break;
                        case 27:
                            SchemaUtil.writeMessageList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, getMessageFieldSchema(i));
                            break;
                        case 28:
                            SchemaUtil.writeBytesList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer);
                            break;
                        case 29:
                            SchemaUtil.writeUInt32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 30:
                            SchemaUtil.writeEnumList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 31:
                            SchemaUtil.writeSFixed32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 32:
                            SchemaUtil.writeSFixed64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 33:
                            SchemaUtil.writeSInt32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 34:
                            SchemaUtil.writeSInt64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 35:
                            SchemaUtil.writeDoubleList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 36:
                            SchemaUtil.writeFloatList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 37:
                            SchemaUtil.writeInt64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 38:
                            SchemaUtil.writeUInt64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 39:
                            SchemaUtil.writeInt32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 40:
                            SchemaUtil.writeFixed64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 41:
                            SchemaUtil.writeFixed32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 42:
                            SchemaUtil.writeBoolList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 43:
                            SchemaUtil.writeUInt32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 44:
                            SchemaUtil.writeEnumList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 45:
                            SchemaUtil.writeSFixed32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 46:
                            SchemaUtil.writeSFixed64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 47:
                            SchemaUtil.writeSInt32List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 48:
                            SchemaUtil.writeSInt64List(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 49:
                            SchemaUtil.writeGroupList(numberAt(i), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, getMessageFieldSchema(i));
                            break;
                        case 50:
                            writeMapHelper(writer, numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), i);
                            break;
                        case 51:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeDouble(numberAt, oneofDoubleAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 52:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeFloat(numberAt, oneofFloatAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 53:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeInt64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 54:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeUInt64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 55:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeInt32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 56:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeFixed64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 57:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeFixed32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 58:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeBool(numberAt, oneofBooleanAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 59:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writeString(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer);
                                break;
                            }
                        case 60:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeMessage(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), getMessageFieldSchema(i));
                                break;
                            }
                        case 61:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeBytes(numberAt, (ByteString) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 62:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeUInt32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 63:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeEnum(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 64:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeSFixed32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 65:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeSFixed64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 66:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeSInt32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 67:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeSInt64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 68:
                            if (!isOneofPresent(t, numberAt, i)) {
                                break;
                            } else {
                                writer.writeGroup(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), getMessageFieldSchema(i));
                                break;
                            }
                    }
                }
                while (entry2 != null) {
                    this.extensionSchema.serializeExtension(writer, entry2);
                    entry2 = it.hasNext() ? (Entry) it.next() : null;
                }
                writeUnknownInMessageTo(this.unknownFieldSchema, t, writer);
            }
        }
        it = null;
        entry = null;
        length = this.buffer.length;
        entry2 = entry;
        while (i < length) {
        }
        while (entry2 != null) {
        }
        writeUnknownInMessageTo(this.unknownFieldSchema, t, writer);
    }

    /* JADX WARNING: Removed duplicated region for block: B:161:0x058e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    private void writeFieldsInDescendingOrder(T t, Writer writer) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        writeUnknownInMessageTo(this.unknownFieldSchema, t, writer);
        if (this.hasExtensions) {
            FieldSet extensions = this.extensionSchema.getExtensions(t);
            if (!extensions.isEmpty()) {
                it = extensions.descendingIterator();
                entry = (Entry) it.next();
                for (length = this.buffer.length - 3; length >= 0; length -= 3) {
                    int typeAndOffsetAt = typeAndOffsetAt(length);
                    int numberAt = numberAt(length);
                    while (entry != null && this.extensionSchema.extensionNumber(entry) > numberAt) {
                        this.extensionSchema.serializeExtension(writer, entry);
                        entry = it.hasNext() ? (Entry) it.next() : null;
                    }
                    switch (type(typeAndOffsetAt)) {
                        case 0:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeDouble(numberAt, doubleAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 1:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeFloat(numberAt, floatAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 2:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeInt64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 3:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeUInt64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 4:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeInt32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 5:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeFixed64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 6:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeFixed32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 7:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeBool(numberAt, booleanAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 8:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writeString(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer);
                                break;
                            }
                        case 9:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeMessage(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), getMessageFieldSchema(length));
                                break;
                            }
                        case 10:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeBytes(numberAt, (ByteString) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 11:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeUInt32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 12:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeEnum(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 13:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeSFixed32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 14:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeSFixed64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 15:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeSInt32(numberAt, intAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 16:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeSInt64(numberAt, longAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 17:
                            if (!isFieldPresent(t, length)) {
                                break;
                            } else {
                                writer.writeGroup(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), getMessageFieldSchema(length));
                                break;
                            }
                        case 18:
                            SchemaUtil.writeDoubleList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 19:
                            SchemaUtil.writeFloatList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 20:
                            SchemaUtil.writeInt64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 21:
                            SchemaUtil.writeUInt64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 22:
                            SchemaUtil.writeInt32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 23:
                            SchemaUtil.writeFixed64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 24:
                            SchemaUtil.writeFixed32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 25:
                            SchemaUtil.writeBoolList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 26:
                            SchemaUtil.writeStringList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer);
                            break;
                        case 27:
                            SchemaUtil.writeMessageList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, getMessageFieldSchema(length));
                            break;
                        case 28:
                            SchemaUtil.writeBytesList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer);
                            break;
                        case 29:
                            SchemaUtil.writeUInt32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 30:
                            SchemaUtil.writeEnumList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 31:
                            SchemaUtil.writeSFixed32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 32:
                            SchemaUtil.writeSFixed64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 33:
                            SchemaUtil.writeSInt32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 34:
                            SchemaUtil.writeSInt64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, false);
                            break;
                        case 35:
                            SchemaUtil.writeDoubleList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 36:
                            SchemaUtil.writeFloatList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 37:
                            SchemaUtil.writeInt64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 38:
                            SchemaUtil.writeUInt64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 39:
                            SchemaUtil.writeInt32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 40:
                            SchemaUtil.writeFixed64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 41:
                            SchemaUtil.writeFixed32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 42:
                            SchemaUtil.writeBoolList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 43:
                            SchemaUtil.writeUInt32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 44:
                            SchemaUtil.writeEnumList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 45:
                            SchemaUtil.writeSFixed32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 46:
                            SchemaUtil.writeSFixed64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 47:
                            SchemaUtil.writeSInt32List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 48:
                            SchemaUtil.writeSInt64List(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, true);
                            break;
                        case 49:
                            SchemaUtil.writeGroupList(numberAt(length), (List) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer, getMessageFieldSchema(length));
                            break;
                        case 50:
                            writeMapHelper(writer, numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), length);
                            break;
                        case 51:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeDouble(numberAt, oneofDoubleAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 52:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeFloat(numberAt, oneofFloatAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 53:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeInt64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 54:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeUInt64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 55:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeInt32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 56:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeFixed64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 57:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeFixed32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 58:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeBool(numberAt, oneofBooleanAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 59:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writeString(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), writer);
                                break;
                            }
                        case 60:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeMessage(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), getMessageFieldSchema(length));
                                break;
                            }
                        case 61:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeBytes(numberAt, (ByteString) UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 62:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeUInt32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 63:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeEnum(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 64:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeSFixed32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 65:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeSFixed64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 66:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeSInt32(numberAt, oneofIntAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 67:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeSInt64(numberAt, oneofLongAt(t, offset(typeAndOffsetAt)));
                                break;
                            }
                        case 68:
                            if (!isOneofPresent(t, numberAt, length)) {
                                break;
                            } else {
                                writer.writeGroup(numberAt, UnsafeUtil.getObject((Object) t, offset(typeAndOffsetAt)), getMessageFieldSchema(length));
                                break;
                            }
                    }
                }
                while (entry != null) {
                    this.extensionSchema.serializeExtension(writer, entry);
                    entry = it.hasNext() ? (Entry) it.next() : null;
                }
            }
        }
        it = null;
        entry = null;
        while (length >= 0) {
        }
        while (entry != null) {
        }
    }

    private <K, V> void writeMapHelper(Writer writer, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            writer.writeMap(i, this.mapFieldSchema.forMapMetadata(getMapFieldDefaultEntry(i2)), this.mapFieldSchema.forMapData(obj));
        }
    }

    private <UT, UB> void writeUnknownInMessageTo(UnknownFieldSchema<UT, UB> unknownFieldSchema2, T t, Writer writer) throws IOException {
        unknownFieldSchema2.writeTo(unknownFieldSchema2.getFromMessage(t), writer);
    }

    public void mergeFrom(T t, Reader reader, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        if (extensionRegistryLite != null) {
            mergeFromHelper(this.unknownFieldSchema, this.extensionSchema, t, reader, extensionRegistryLite);
            return;
        }
        throw new NullPointerException();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:149|150|(2:152|(6:186|154|(2:157|155)|196|(1:159)|160)(1:191))(3:(2:162|163)|164|(6:187|166|(2:169|167)|197|(1:171)|172)(1:192))) */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x05ca, code lost:
        if (r9.shouldDiscardUnknownFields(r0) != false) goto L_0x05cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x05d0, code lost:
        if (r20.skipField() == false) goto L_0x05d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x05d2, code lost:
        r0 = r8.checkInitializedCount;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x05d6, code lost:
        if (r0 < r8.repeatedFieldOffsetStart) goto L_0x05d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x05d8, code lost:
        r13 = filterMapUnknownEnumValues(r10, r8.intArray[r0], r13, r9);
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x05e3, code lost:
        if (r13 != null) goto L_0x05e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x05e5, code lost:
        r9.setBuilderToMessage(r10, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x05e8, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x05e9, code lost:
        if (r13 == null) goto L_0x05eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:?, code lost:
        r13 = r9.getBuilderFromMessage(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x05f4, code lost:
        if (r9.mergeOneFieldFrom(r13, r0) == false) goto L_0x05f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x05f6, code lost:
        r0 = r8.checkInitializedCount;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x05fa, code lost:
        if (r0 < r8.repeatedFieldOffsetStart) goto L_0x05fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x05fc, code lost:
        r13 = filterMapUnknownEnumValues(r10, r8.intArray[r0], r13, r9);
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0607, code lost:
        if (r13 != null) goto L_0x0609;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0609, code lost:
        r9.setBuilderToMessage(r10, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x060c, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:149:0x05c6 */
    private <UT, UB, ET extends FieldDescriptorLite<ET>> void mergeFromHelper(UnknownFieldSchema<UT, UB> unknownFieldSchema2, ExtensionSchema<ET> extensionSchema2, T t, Reader reader, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        Object obj;
        Object obj2;
        UnknownFieldSchema<UT, UB> unknownFieldSchema3 = unknownFieldSchema2;
        T t2 = t;
        Reader reader2 = reader;
        ExtensionRegistryLite extensionRegistryLite2 = extensionRegistryLite;
        Object obj3 = null;
        FieldSet fieldSet = null;
        while (true) {
            int fieldNumber = reader.getFieldNumber();
            int positionForFieldNumber = positionForFieldNumber(fieldNumber);
            if (positionForFieldNumber >= 0) {
                ExtensionSchema<ET> extensionSchema3 = extensionSchema2;
                int typeAndOffsetAt = typeAndOffsetAt(positionForFieldNumber);
                try {
                    switch (type(typeAndOffsetAt)) {
                        case 0:
                            UnsafeUtil.putDouble((Object) t2, offset(typeAndOffsetAt), reader.readDouble());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 1:
                            UnsafeUtil.putFloat((Object) t2, offset(typeAndOffsetAt), reader.readFloat());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 2:
                            UnsafeUtil.putLong((Object) t2, offset(typeAndOffsetAt), reader.readInt64());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 3:
                            UnsafeUtil.putLong((Object) t2, offset(typeAndOffsetAt), reader.readUInt64());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 4:
                            UnsafeUtil.putInt((Object) t2, offset(typeAndOffsetAt), reader.readInt32());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 5:
                            UnsafeUtil.putLong((Object) t2, offset(typeAndOffsetAt), reader.readFixed64());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 6:
                            UnsafeUtil.putInt((Object) t2, offset(typeAndOffsetAt), reader.readFixed32());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 7:
                            UnsafeUtil.putBoolean((Object) t2, offset(typeAndOffsetAt), reader.readBool());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 8:
                            readString(t2, typeAndOffsetAt, reader2);
                            setFieldPresent(t2, positionForFieldNumber);
                        case 9:
                            if (isFieldPresent(t2, positionForFieldNumber)) {
                                UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), Internal.mergeMessage(UnsafeUtil.getObject((Object) t2, offset(typeAndOffsetAt)), reader2.readMessageBySchemaWithCheck(getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite2)));
                            } else {
                                UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), reader2.readMessageBySchemaWithCheck(getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite2));
                                setFieldPresent(t2, positionForFieldNumber);
                            }
                        case 10:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) reader.readBytes());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 11:
                            UnsafeUtil.putInt((Object) t2, offset(typeAndOffsetAt), reader.readUInt32());
                            setFieldPresent(t2, positionForFieldNumber);
                        case 12:
                            int readEnum = reader.readEnum();
                            EnumVerifier enumFieldVerifier = getEnumFieldVerifier(positionForFieldNumber);
                            if (enumFieldVerifier != null) {
                                if (!enumFieldVerifier.isInRange(readEnum)) {
                                    obj = SchemaUtil.storeUnknownEnum(fieldNumber, readEnum, obj3, unknownFieldSchema3);
                                    break;
                                }
                            }
                            UnsafeUtil.putInt((Object) t2, offset(typeAndOffsetAt), readEnum);
                            setFieldPresent(t2, positionForFieldNumber);
                        case 13:
                            UnsafeUtil.putInt((Object) t2, offset(typeAndOffsetAt), reader.readSFixed32());
                            setFieldPresent(t2, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 14:
                            UnsafeUtil.putLong((Object) t2, offset(typeAndOffsetAt), reader.readSFixed64());
                            setFieldPresent(t2, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 15:
                            UnsafeUtil.putInt((Object) t2, offset(typeAndOffsetAt), reader.readSInt32());
                            setFieldPresent(t2, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 16:
                            UnsafeUtil.putLong((Object) t2, offset(typeAndOffsetAt), reader.readSInt64());
                            setFieldPresent(t2, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 17:
                            if (isFieldPresent(t2, positionForFieldNumber)) {
                                UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), Internal.mergeMessage(UnsafeUtil.getObject((Object) t2, offset(typeAndOffsetAt)), reader2.readGroupBySchemaWithCheck(getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite2)));
                            } else {
                                UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), reader2.readGroupBySchemaWithCheck(getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite2));
                                setFieldPresent(t2, positionForFieldNumber);
                            }
                            obj = obj3;
                            break;
                        case 18:
                            reader2.readDoubleList(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 19:
                            reader2.readFloatList(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 20:
                            reader2.readInt64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 21:
                            reader2.readUInt64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 22:
                            reader2.readInt32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 23:
                            reader2.readFixed64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 24:
                            reader2.readFixed32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 25:
                            reader2.readBoolList(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 26:
                            readStringList(t2, typeAndOffsetAt, reader2);
                            obj = obj3;
                            break;
                        case 27:
                            readMessageList(t, typeAndOffsetAt, reader, getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite);
                            obj = obj3;
                            break;
                        case 28:
                            reader2.readBytesList(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 29:
                            reader2.readUInt32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 30:
                            List mutableListAt = this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt));
                            reader2.readEnumList(mutableListAt);
                            obj = SchemaUtil.filterUnknownEnumList(fieldNumber, mutableListAt, getEnumFieldVerifier(positionForFieldNumber), obj3, unknownFieldSchema3);
                            break;
                        case 31:
                            reader2.readSFixed32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 32:
                            reader2.readSFixed64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 33:
                            reader2.readSInt32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 34:
                            reader2.readSInt64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 35:
                            reader2.readDoubleList(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 36:
                            reader2.readFloatList(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 37:
                            reader2.readInt64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 38:
                            reader2.readUInt64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 39:
                            reader2.readInt32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 40:
                            reader2.readFixed64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 41:
                            reader2.readFixed32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 42:
                            reader2.readBoolList(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 43:
                            reader2.readUInt32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 44:
                            List mutableListAt2 = this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt));
                            reader2.readEnumList(mutableListAt2);
                            obj = SchemaUtil.filterUnknownEnumList(fieldNumber, mutableListAt2, getEnumFieldVerifier(positionForFieldNumber), obj3, unknownFieldSchema3);
                            break;
                        case 45:
                            reader2.readSFixed32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 46:
                            reader2.readSFixed64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 47:
                            reader2.readSInt32List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 48:
                            reader2.readSInt64List(this.listFieldSchema.mutableListAt(t2, offset(typeAndOffsetAt)));
                            obj = obj3;
                            break;
                        case 49:
                            readGroupList(t, offset(typeAndOffsetAt), reader, getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite);
                            obj = obj3;
                            break;
                        case 50:
                            mergeMap(t, positionForFieldNumber, getMapFieldDefaultEntry(positionForFieldNumber), extensionRegistryLite, reader);
                            obj = obj3;
                            break;
                        case 51:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Double.valueOf(reader.readDouble()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 52:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Float.valueOf(reader.readFloat()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 53:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Long.valueOf(reader.readInt64()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 54:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Long.valueOf(reader.readUInt64()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 55:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Integer.valueOf(reader.readInt32()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 56:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Long.valueOf(reader.readFixed64()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 57:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Integer.valueOf(reader.readFixed32()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 58:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Boolean.valueOf(reader.readBool()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 59:
                            readString(t2, typeAndOffsetAt, reader2);
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 60:
                            if (isOneofPresent(t2, fieldNumber, positionForFieldNumber)) {
                                UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), Internal.mergeMessage(UnsafeUtil.getObject((Object) t2, offset(typeAndOffsetAt)), reader2.readMessageBySchemaWithCheck(getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite2)));
                            } else {
                                UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), reader2.readMessageBySchemaWithCheck(getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite2));
                                setFieldPresent(t2, positionForFieldNumber);
                            }
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 61:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) reader.readBytes());
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 62:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Integer.valueOf(reader.readUInt32()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 63:
                            int readEnum2 = reader.readEnum();
                            EnumVerifier enumFieldVerifier2 = getEnumFieldVerifier(positionForFieldNumber);
                            if (enumFieldVerifier2 != null) {
                                if (!enumFieldVerifier2.isInRange(readEnum2)) {
                                    obj = SchemaUtil.storeUnknownEnum(fieldNumber, readEnum2, obj3, unknownFieldSchema3);
                                    break;
                                }
                            }
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Integer.valueOf(readEnum2));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                        case 64:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Integer.valueOf(reader.readSFixed32()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 65:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Long.valueOf(reader.readSFixed64()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 66:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Integer.valueOf(reader.readSInt32()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 67:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), (Object) Long.valueOf(reader.readSInt64()));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        case 68:
                            UnsafeUtil.putObject((Object) t2, offset(typeAndOffsetAt), reader2.readGroupBySchemaWithCheck(getMessageFieldSchema(positionForFieldNumber), extensionRegistryLite2));
                            setOneofPresent(t2, fieldNumber, positionForFieldNumber);
                            obj = obj3;
                            break;
                        default:
                            if (obj3 == null) {
                                obj3 = unknownFieldSchema2.newBuilder();
                            }
                            if (!unknownFieldSchema3.mergeOneFieldFrom(obj3, reader2)) {
                                if (obj3 != null) {
                                    unknownFieldSchema3.setBuilderToMessage(t2, obj3);
                                }
                                return;
                            }
                    }
                    obj = obj3;
                } finally {
                    for (int i = this.checkInitializedCount; i < this.repeatedFieldOffsetStart; i++) {
                        obj3 = filterMapUnknownEnumValues(t2, this.intArray[i], obj3, unknownFieldSchema3);
                    }
                    if (obj3 != null) {
                        unknownFieldSchema3.setBuilderToMessage(t2, obj3);
                    }
                }
            } else if (fieldNumber == Integer.MAX_VALUE) {
                for (int i2 = this.checkInitializedCount; i2 < this.repeatedFieldOffsetStart; i2++) {
                    obj3 = filterMapUnknownEnumValues(t2, this.intArray[i2], obj3, unknownFieldSchema3);
                }
                if (obj3 != null) {
                    unknownFieldSchema3.setBuilderToMessage(t2, obj3);
                }
                return;
            } else {
                if (!this.hasExtensions) {
                    ExtensionSchema<ET> extensionSchema4 = extensionSchema2;
                    obj2 = null;
                } else {
                    obj2 = extensionSchema2.findExtensionByNumber(extensionRegistryLite2, this.defaultInstance, fieldNumber);
                }
                if (obj2 != null) {
                    if (fieldSet == null) {
                        fieldSet = extensionSchema2.getMutableExtensions(t);
                    }
                    obj = extensionSchema2.parseExtension(reader, obj2, extensionRegistryLite, fieldSet, obj3, unknownFieldSchema2);
                } else if (!unknownFieldSchema3.shouldDiscardUnknownFields(reader2)) {
                    if (obj3 == null) {
                        obj3 = unknownFieldSchema3.getBuilderFromMessage(t2);
                    }
                    if (unknownFieldSchema3.mergeOneFieldFrom(obj3, reader2)) {
                    }
                } else if (reader.skipField()) {
                }
            }
            obj3 = obj;
        }
        for (int i3 = this.checkInitializedCount; i3 < this.repeatedFieldOffsetStart; i3++) {
            obj3 = filterMapUnknownEnumValues(t2, this.intArray[i3], obj3, unknownFieldSchema3);
        }
        if (obj3 != null) {
            unknownFieldSchema3.setBuilderToMessage(t2, obj3);
        }
    }

    static UnknownFieldSetLite getMutableUnknownFields(Object obj) {
        GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite) obj;
        UnknownFieldSetLite unknownFieldSetLite = generatedMessageLite.unknownFields;
        if (unknownFieldSetLite != UnknownFieldSetLite.getDefaultInstance()) {
            return unknownFieldSetLite;
        }
        UnknownFieldSetLite newInstance = UnknownFieldSetLite.newInstance();
        generatedMessageLite.unknownFields = newInstance;
        return newInstance;
    }

    private int decodeMapEntryValue(byte[] bArr, int i, int i2, FieldType fieldType, Class<?> cls, Registers registers) throws IOException {
        switch (fieldType) {
            case BOOL:
                int decodeVarint64 = ArrayDecoders.decodeVarint64(bArr, i, registers);
                registers.object1 = Boolean.valueOf(registers.long1 != 0);
                return decodeVarint64;
            case BYTES:
                return ArrayDecoders.decodeBytes(bArr, i, registers);
            case DOUBLE:
                registers.object1 = Double.valueOf(ArrayDecoders.decodeDouble(bArr, i));
                return i + 8;
            case FIXED32:
            case SFIXED32:
                registers.object1 = Integer.valueOf(ArrayDecoders.decodeFixed32(bArr, i));
                return i + 4;
            case FIXED64:
            case SFIXED64:
                registers.object1 = Long.valueOf(ArrayDecoders.decodeFixed64(bArr, i));
                return i + 8;
            case FLOAT:
                registers.object1 = Float.valueOf(ArrayDecoders.decodeFloat(bArr, i));
                return i + 4;
            case ENUM:
            case INT32:
            case UINT32:
                int decodeVarint32 = ArrayDecoders.decodeVarint32(bArr, i, registers);
                registers.object1 = Integer.valueOf(registers.int1);
                return decodeVarint32;
            case INT64:
            case UINT64:
                int decodeVarint642 = ArrayDecoders.decodeVarint64(bArr, i, registers);
                registers.object1 = Long.valueOf(registers.long1);
                return decodeVarint642;
            case MESSAGE:
                return ArrayDecoders.decodeMessageField(Protobuf.getInstance().schemaFor(cls), bArr, i, i2, registers);
            case SINT32:
                int decodeVarint322 = ArrayDecoders.decodeVarint32(bArr, i, registers);
                registers.object1 = Integer.valueOf(CodedInputStream.decodeZigZag32(registers.int1));
                return decodeVarint322;
            case SINT64:
                int decodeVarint643 = ArrayDecoders.decodeVarint64(bArr, i, registers);
                registers.object1 = Long.valueOf(CodedInputStream.decodeZigZag64(registers.long1));
                return decodeVarint643;
            case STRING:
                return ArrayDecoders.decodeStringRequireUtf8(bArr, i, registers);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    /* JADX WARNING: type inference failed for: r7v0, types: [byte[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    private <K, V> int decodeMapEntry(byte[] bArr, int i, int i2, Metadata<K, V> metadata, Map<K, V> map, Registers registers) throws IOException {
        int i3;
        int i4;
        ?[OBJECT, ARRAY] AOBJECT__ARRAY = bArr;
        int i5 = i2;
        Metadata<K, V> metadata2 = metadata;
        Registers registers2 = registers;
        int decodeVarint32 = ArrayDecoders.decodeVarint32(bArr, i, registers2);
        int i6 = registers2.int1;
        if (i6 < 0 || i6 > i5 - decodeVarint32) {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        int i7 = decodeVarint32 + i6;
        K k = metadata2.defaultKey;
        V v = metadata2.defaultValue;
        while (decodeVarint32 < i7) {
            int i8 = decodeVarint32 + 1;
            byte b = AOBJECT__ARRAY[decodeVarint32];
            if (b < 0) {
                i3 = ArrayDecoders.decodeVarint32(b, bArr, i8, registers2);
                i4 = registers2.int1;
            } else {
                i3 = i8;
                i4 = b;
            }
            int i9 = i4 & 7;
            switch (i4 >>> 3) {
                case 1:
                    if (i9 == metadata2.keyType.getWireType()) {
                        decodeVarint32 = decodeMapEntryValue(bArr, i3, i2, metadata2.keyType, null, registers);
                        k = registers2.object1;
                        break;
                    }
                case 2:
                    if (i9 == metadata2.valueType.getWireType()) {
                        decodeVarint32 = decodeMapEntryValue(bArr, i3, i2, metadata2.valueType, metadata2.defaultValue.getClass(), registers);
                        v = registers2.object1;
                        break;
                    }
                default:
                    decodeVarint32 = ArrayDecoders.skipField(i4, bArr, i3, i5, registers2);
                    break;
            }
        }
        if (decodeVarint32 == i7) {
            map.put(k, v);
            return i7;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }

    private int parseRepeatedField(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, long j, int i7, long j2, Registers registers) throws IOException {
        int i8;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i9 = i;
        int i10 = i5;
        int i11 = i6;
        long j3 = j2;
        Registers registers2 = registers;
        ProtobufList protobufList = (ProtobufList) UNSAFE.getObject(t, j3);
        if (!protobufList.isModifiable()) {
            int size = protobufList.size();
            protobufList = protobufList.mutableCopyWithCapacity(size == 0 ? 10 : size * 2);
            UNSAFE.putObject(t, j3, protobufList);
        }
        switch (i7) {
            case 18:
            case 35:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedDoubleList(bArr, i, protobufList, registers2);
                }
                if (i10 == 1) {
                    return ArrayDecoders.decodeDoubleList(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 19:
            case 36:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedFloatList(bArr, i, protobufList, registers2);
                }
                if (i10 == 5) {
                    return ArrayDecoders.decodeFloatList(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 20:
            case 21:
            case 37:
            case 38:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedVarint64List(bArr, i, protobufList, registers2);
                }
                if (i10 == 0) {
                    return ArrayDecoders.decodeVarint64List(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 22:
            case 29:
            case 39:
            case 43:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedVarint32List(bArr, i, protobufList, registers2);
                }
                if (i10 == 0) {
                    return ArrayDecoders.decodeVarint32List(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 23:
            case 32:
            case 40:
            case 46:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedFixed64List(bArr, i, protobufList, registers2);
                }
                if (i10 == 1) {
                    return ArrayDecoders.decodeFixed64List(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 24:
            case 31:
            case 41:
            case 45:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedFixed32List(bArr, i, protobufList, registers2);
                }
                if (i10 == 5) {
                    return ArrayDecoders.decodeFixed32List(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 25:
            case 42:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedBoolList(bArr, i, protobufList, registers2);
                }
                if (i10 == 0) {
                    return ArrayDecoders.decodeBoolList(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 26:
                if (i10 == 2) {
                    if ((j & 536870912) == 0) {
                        return ArrayDecoders.decodeStringList(i3, bArr, i, i2, protobufList, registers);
                    }
                    return ArrayDecoders.decodeStringListRequireUtf8(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 27:
                if (i10 == 2) {
                    return ArrayDecoders.decodeMessageList(getMessageFieldSchema(i11), i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 28:
                if (i10 == 2) {
                    return ArrayDecoders.decodeBytesList(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 30:
            case 44:
                if (i10 == 2) {
                    i8 = ArrayDecoders.decodePackedVarint32List(bArr, i, protobufList, registers2);
                } else if (i10 == 0) {
                    i8 = ArrayDecoders.decodeVarint32List(i3, bArr, i, i2, protobufList, registers);
                }
                GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite) t2;
                UnknownFieldSetLite unknownFieldSetLite = generatedMessageLite.unknownFields;
                if (unknownFieldSetLite == UnknownFieldSetLite.getDefaultInstance()) {
                    unknownFieldSetLite = null;
                }
                UnknownFieldSetLite unknownFieldSetLite2 = (UnknownFieldSetLite) SchemaUtil.filterUnknownEnumList(i4, (List<Integer>) protobufList, getEnumFieldVerifier(i11), unknownFieldSetLite, this.unknownFieldSchema);
                if (unknownFieldSetLite2 != null) {
                    generatedMessageLite.unknownFields = unknownFieldSetLite2;
                }
                return i8;
            case 33:
            case 47:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedSInt32List(bArr, i, protobufList, registers2);
                }
                if (i10 == 0) {
                    return ArrayDecoders.decodeSInt32List(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 34:
            case 48:
                if (i10 == 2) {
                    return ArrayDecoders.decodePackedSInt64List(bArr, i, protobufList, registers2);
                }
                if (i10 == 0) {
                    return ArrayDecoders.decodeSInt64List(i3, bArr, i, i2, protobufList, registers);
                }
                break;
            case 49:
                if (i10 == 3) {
                    return ArrayDecoders.decodeGroupList(getMessageFieldSchema(i11), i3, bArr, i, i2, protobufList, registers);
                }
                break;
        }
        return i9;
    }

    private <K, V> int parseMapField(T t, byte[] bArr, int i, int i2, int i3, long j, Registers registers) throws IOException {
        T t2 = t;
        long j2 = j;
        Unsafe unsafe = UNSAFE;
        int i4 = i3;
        Object mapFieldDefaultEntry = getMapFieldDefaultEntry(i3);
        Object object = unsafe.getObject(t, j2);
        if (this.mapFieldSchema.isImmutable(object)) {
            Object newMapField = this.mapFieldSchema.newMapField(mapFieldDefaultEntry);
            this.mapFieldSchema.mergeFrom(newMapField, object);
            unsafe.putObject(t, j2, newMapField);
            object = newMapField;
        }
        return decodeMapEntry(bArr, i, i2, this.mapFieldSchema.forMapMetadata(mapFieldDefaultEntry), this.mapFieldSchema.forMutableMapData(object), registers);
    }

    private int parseOneofField(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, Registers registers) throws IOException {
        T t2 = t;
        byte[] bArr2 = bArr;
        int i9 = i;
        int i10 = i3;
        int i11 = i4;
        int i12 = i5;
        long j2 = j;
        int i13 = i8;
        Registers registers2 = registers;
        Unsafe unsafe = UNSAFE;
        long j3 = (long) (this.buffer[i13 + 2] & OFFSET_MASK);
        switch (i7) {
            case 51:
                if (i12 == 1) {
                    unsafe.putObject(t2, j2, Double.valueOf(ArrayDecoders.decodeDouble(bArr, i)));
                    int i14 = i9 + 8;
                    unsafe.putInt(t2, j3, i11);
                    return i14;
                }
                break;
            case 52:
                if (i12 == 5) {
                    unsafe.putObject(t2, j2, Float.valueOf(ArrayDecoders.decodeFloat(bArr, i)));
                    int i15 = i9 + 4;
                    unsafe.putInt(t2, j3, i11);
                    return i15;
                }
                break;
            case 53:
            case 54:
                if (i12 == 0) {
                    int decodeVarint64 = ArrayDecoders.decodeVarint64(bArr2, i9, registers2);
                    unsafe.putObject(t2, j2, Long.valueOf(registers2.long1));
                    unsafe.putInt(t2, j3, i11);
                    return decodeVarint64;
                }
                break;
            case 55:
            case 62:
                if (i12 == 0) {
                    int decodeVarint32 = ArrayDecoders.decodeVarint32(bArr2, i9, registers2);
                    unsafe.putObject(t2, j2, Integer.valueOf(registers2.int1));
                    unsafe.putInt(t2, j3, i11);
                    return decodeVarint32;
                }
                break;
            case 56:
            case 65:
                if (i12 == 1) {
                    unsafe.putObject(t2, j2, Long.valueOf(ArrayDecoders.decodeFixed64(bArr, i)));
                    int i16 = i9 + 8;
                    unsafe.putInt(t2, j3, i11);
                    return i16;
                }
                break;
            case 57:
            case 64:
                if (i12 == 5) {
                    unsafe.putObject(t2, j2, Integer.valueOf(ArrayDecoders.decodeFixed32(bArr, i)));
                    int i17 = i9 + 4;
                    unsafe.putInt(t2, j3, i11);
                    return i17;
                }
                break;
            case 58:
                if (i12 == 0) {
                    int decodeVarint642 = ArrayDecoders.decodeVarint64(bArr2, i9, registers2);
                    unsafe.putObject(t2, j2, Boolean.valueOf(registers2.long1 != 0));
                    unsafe.putInt(t2, j3, i11);
                    return decodeVarint642;
                }
                break;
            case 59:
                if (i12 == 2) {
                    int decodeVarint322 = ArrayDecoders.decodeVarint32(bArr2, i9, registers2);
                    int i18 = registers2.int1;
                    if (i18 == 0) {
                        unsafe.putObject(t2, j2, "");
                    } else if ((i6 & ENFORCE_UTF8_MASK) == 0 || Utf8.isValidUtf8(bArr2, decodeVarint322, decodeVarint322 + i18)) {
                        unsafe.putObject(t2, j2, new String(bArr2, decodeVarint322, i18, Internal.UTF_8));
                        decodeVarint322 += i18;
                    } else {
                        throw InvalidProtocolBufferException.invalidUtf8();
                    }
                    unsafe.putInt(t2, j3, i11);
                    return decodeVarint322;
                }
                break;
            case 60:
                if (i12 == 2) {
                    int decodeMessageField = ArrayDecoders.decodeMessageField(getMessageFieldSchema(i13), bArr2, i9, i2, registers2);
                    Object object = unsafe.getInt(t2, j3) == i11 ? unsafe.getObject(t2, j2) : null;
                    if (object == null) {
                        unsafe.putObject(t2, j2, registers2.object1);
                    } else {
                        unsafe.putObject(t2, j2, Internal.mergeMessage(object, registers2.object1));
                    }
                    unsafe.putInt(t2, j3, i11);
                    return decodeMessageField;
                }
                break;
            case 61:
                if (i12 == 2) {
                    int decodeBytes = ArrayDecoders.decodeBytes(bArr2, i9, registers2);
                    unsafe.putObject(t2, j2, registers2.object1);
                    unsafe.putInt(t2, j3, i11);
                    return decodeBytes;
                }
                break;
            case 63:
                if (i12 == 0) {
                    int decodeVarint323 = ArrayDecoders.decodeVarint32(bArr2, i9, registers2);
                    int i19 = registers2.int1;
                    EnumVerifier enumFieldVerifier = getEnumFieldVerifier(i13);
                    if (enumFieldVerifier == null || enumFieldVerifier.isInRange(i19)) {
                        unsafe.putObject(t2, j2, Integer.valueOf(i19));
                        unsafe.putInt(t2, j3, i11);
                    } else {
                        getMutableUnknownFields(t).storeField(i10, Long.valueOf((long) i19));
                    }
                    return decodeVarint323;
                }
                break;
            case 66:
                if (i12 == 0) {
                    int decodeVarint324 = ArrayDecoders.decodeVarint32(bArr2, i9, registers2);
                    unsafe.putObject(t2, j2, Integer.valueOf(CodedInputStream.decodeZigZag32(registers2.int1)));
                    unsafe.putInt(t2, j3, i11);
                    return decodeVarint324;
                }
                break;
            case 67:
                if (i12 == 0) {
                    int decodeVarint643 = ArrayDecoders.decodeVarint64(bArr2, i9, registers2);
                    unsafe.putObject(t2, j2, Long.valueOf(CodedInputStream.decodeZigZag64(registers2.long1)));
                    unsafe.putInt(t2, j3, i11);
                    return decodeVarint643;
                }
                break;
            case 68:
                if (i12 == 3) {
                    int decodeGroupField = ArrayDecoders.decodeGroupField(getMessageFieldSchema(i13), bArr, i, i2, (i10 & -8) | 4, registers);
                    Object object2 = unsafe.getInt(t2, j3) == i11 ? unsafe.getObject(t2, j2) : null;
                    if (object2 == null) {
                        unsafe.putObject(t2, j2, registers2.object1);
                    } else {
                        unsafe.putObject(t2, j2, Internal.mergeMessage(object2, registers2.object1));
                    }
                    unsafe.putInt(t2, j3, i11);
                    return decodeGroupField;
                }
                break;
        }
        return i9;
    }

    private Schema getMessageFieldSchema(int i) {
        int i2 = (i / 3) * 2;
        Schema schema = (Schema) this.objects[i2];
        if (schema != null) {
            return schema;
        }
        Schema schemaFor = Protobuf.getInstance().schemaFor((Class) this.objects[i2 + 1]);
        this.objects[i2] = schemaFor;
        return schemaFor;
    }

    private Object getMapFieldDefaultEntry(int i) {
        return this.objects[(i / 3) * 2];
    }

    private EnumVerifier getEnumFieldVerifier(int i) {
        return (EnumVerifier) this.objects[((i / 3) * 2) + 1];
    }

    /* JADX WARNING: type inference failed for: r32v0, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v0 */
    /* JADX WARNING: type inference failed for: r12v1, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r0v11, types: [byte, int] */
    /* JADX WARNING: type inference failed for: r5v3, types: [int] */
    /* JADX WARNING: type inference failed for: r12v2 */
    /* JADX WARNING: type inference failed for: r3v5 */
    /* JADX WARNING: type inference failed for: r8v1 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r3v6 */
    /* JADX WARNING: type inference failed for: r0v17, types: [int] */
    /* JADX WARNING: type inference failed for: r1v13, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r0v20, types: [int] */
    /* JADX WARNING: type inference failed for: r1v15, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r3v9 */
    /* JADX WARNING: type inference failed for: r20v2 */
    /* JADX WARNING: type inference failed for: r8v3 */
    /* JADX WARNING: type inference failed for: r13v4 */
    /* JADX WARNING: type inference failed for: r12v4 */
    /* JADX WARNING: type inference failed for: r29v0 */
    /* JADX WARNING: type inference failed for: r8v4 */
    /* JADX WARNING: type inference failed for: r29v1 */
    /* JADX WARNING: type inference failed for: r29v2 */
    /* JADX WARNING: type inference failed for: r2v12, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v8, types: [int] */
    /* JADX WARNING: type inference failed for: r2v13, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r29v3 */
    /* JADX WARNING: type inference failed for: r29v4 */
    /* JADX WARNING: type inference failed for: r12v6 */
    /* JADX WARNING: type inference failed for: r3v12 */
    /* JADX WARNING: type inference failed for: r2v16, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v11, types: [int] */
    /* JADX WARNING: type inference failed for: r29v5 */
    /* JADX WARNING: type inference failed for: r29v6 */
    /* JADX WARNING: type inference failed for: r1v29, types: [int] */
    /* JADX WARNING: type inference failed for: r2v17, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r3v15 */
    /* JADX WARNING: type inference failed for: r12v8 */
    /* JADX WARNING: type inference failed for: r3v16 */
    /* JADX WARNING: type inference failed for: r12v9 */
    /* JADX WARNING: type inference failed for: r3v17 */
    /* JADX WARNING: type inference failed for: r13v8 */
    /* JADX WARNING: type inference failed for: r8v10 */
    /* JADX WARNING: type inference failed for: r13v9 */
    /* JADX WARNING: type inference failed for: r12v10 */
    /* JADX WARNING: type inference failed for: r3v18 */
    /* JADX WARNING: type inference failed for: r13v10 */
    /* JADX WARNING: type inference failed for: r12v11 */
    /* JADX WARNING: type inference failed for: r13v11 */
    /* JADX WARNING: type inference failed for: r12v12, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v12 */
    /* JADX WARNING: type inference failed for: r12v13, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v13 */
    /* JADX WARNING: type inference failed for: r12v14 */
    /* JADX WARNING: type inference failed for: r13v14 */
    /* JADX WARNING: type inference failed for: r12v15, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v15 */
    /* JADX WARNING: type inference failed for: r12v16, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v16 */
    /* JADX WARNING: type inference failed for: r13v17 */
    /* JADX WARNING: type inference failed for: r12v17, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v18 */
    /* JADX WARNING: type inference failed for: r12v18 */
    /* JADX WARNING: type inference failed for: r3v19 */
    /* JADX WARNING: type inference failed for: r13v20 */
    /* JADX WARNING: type inference failed for: r12v19, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v21 */
    /* JADX WARNING: type inference failed for: r12v20, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v22 */
    /* JADX WARNING: type inference failed for: r12v21, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v23 */
    /* JADX WARNING: type inference failed for: r12v22, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v24 */
    /* JADX WARNING: type inference failed for: r13v25 */
    /* JADX WARNING: type inference failed for: r12v23, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v26, types: [int] */
    /* JADX WARNING: type inference failed for: r12v24, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v27 */
    /* JADX WARNING: type inference failed for: r12v25, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v28 */
    /* JADX WARNING: type inference failed for: r12v26 */
    /* JADX WARNING: type inference failed for: r12v27, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v29 */
    /* JADX WARNING: type inference failed for: r1v64, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v30 */
    /* JADX WARNING: type inference failed for: r3v22 */
    /* JADX WARNING: type inference failed for: r12v28 */
    /* JADX WARNING: type inference failed for: r13v31 */
    /* JADX WARNING: type inference failed for: r13v32 */
    /* JADX WARNING: type inference failed for: r12v29 */
    /* JADX WARNING: type inference failed for: r13v33 */
    /* JADX WARNING: type inference failed for: r8v34 */
    /* JADX WARNING: type inference failed for: r5v18 */
    /* JADX WARNING: type inference failed for: r3v23, types: [int] */
    /* JADX WARNING: type inference failed for: r5v19 */
    /* JADX WARNING: type inference failed for: r12v30 */
    /* JADX WARNING: type inference failed for: r3v24 */
    /* JADX WARNING: type inference failed for: r8v35 */
    /* JADX WARNING: type inference failed for: r29v7 */
    /* JADX WARNING: type inference failed for: r29v8 */
    /* JADX WARNING: type inference failed for: r12v31 */
    /* JADX WARNING: type inference failed for: r3v25 */
    /* JADX WARNING: type inference failed for: r12v32 */
    /* JADX WARNING: type inference failed for: r3v26 */
    /* JADX WARNING: type inference failed for: r12v33 */
    /* JADX WARNING: type inference failed for: r13v34 */
    /* JADX WARNING: type inference failed for: r12v34 */
    /* JADX WARNING: type inference failed for: r12v35 */
    /* JADX WARNING: type inference failed for: r12v36 */
    /* JADX WARNING: type inference failed for: r13v35 */
    /* JADX WARNING: type inference failed for: r12v37 */
    /* JADX WARNING: type inference failed for: r12v38 */
    /* JADX WARNING: type inference failed for: r12v39 */
    /* JADX WARNING: type inference failed for: r13v36 */
    /* JADX WARNING: type inference failed for: r12v40 */
    /* JADX WARNING: type inference failed for: r12v41 */
    /* JADX WARNING: type inference failed for: r12v42 */
    /* JADX WARNING: type inference failed for: r12v43 */
    /* JADX WARNING: type inference failed for: r12v44 */
    /* JADX WARNING: type inference failed for: r13v37 */
    /* JADX WARNING: type inference failed for: r13v38 */
    /* JADX WARNING: type inference failed for: r13v39 */
    /* JADX WARNING: type inference failed for: r12v45 */
    /* JADX WARNING: type inference failed for: r12v46 */
    /* JADX WARNING: type inference failed for: r13v40 */
    /* JADX WARNING: type inference failed for: r12v47 */
    /* JADX WARNING: type inference failed for: r12v48 */
    /* JADX WARNING: type inference failed for: r12v49 */
    /* JADX WARNING: type inference failed for: r12v50 */
    /* JADX WARNING: type inference failed for: r13v41 */
    /* JADX WARNING: type inference failed for: r13v42 */
    /* JADX WARNING: type inference failed for: r12v51 */
    /* JADX WARNING: type inference failed for: r13v43 */
    /* JADX WARNING: type inference failed for: r13v44 */
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x02ba, code lost:
        r20 = r6;
        r25 = r7;
        r18 = r8;
        r28 = r10;
        r2 = r11;
        r8 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0358, code lost:
        if (r0 != r15) goto L_0x035a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0374, code lost:
        r2 = r0;
        r29 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x03a3, code lost:
        if (r0 != r15) goto L_0x035a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x03c6, code lost:
        if (r0 != r15) goto L_0x035a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0017, code lost:
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0099, code lost:
        r12 = r32;
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x009b, code lost:
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0173, code lost:
        r11 = r4;
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0214, code lost:
        r6 = r6 | r18;
        r2 = r8;
        r3 = r13;
        r1 = r17;
        r13 = r11;
        r11 = r35;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0240, code lost:
        r11 = r4;
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0279, code lost:
        r2 = r8;
        r0 = r11;
        r13 = r13;
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x02b0, code lost:
        r2 = r8;
        r13 = r13;
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x02b1, code lost:
        r3 = r13;
        r1 = r17;
        r12 = r12;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=null, for r0v11, types: [byte, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte[], code=null, for r32v0, types: [byte[]] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r12v2
  assigns: []
  uses: []
  mth insns count: 533
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 57 */
    public int parseProto2Message(T t, byte[] r32, int i, int i2, int i3, Registers registers) throws IOException {
        Unsafe unsafe;
        int i4;
        MessageSchema messageSchema;
        int i5;
        int i6;
        int i7;
        int i8;
        T t2;
        ? r5;
        int i9;
        int positionForFieldNumber;
        ? r12;
        ? r3;
        int i10;
        int i11;
        int i12;
        int i13;
        ? r8;
        int i14;
        Registers registers2;
        int decodeUnknownField;
        ? r122;
        ? r29;
        int i15;
        int parseOneofField;
        int decodeMessageList;
        int i16;
        ? r123;
        ? r33;
        int i17;
        ? r13;
        int i18;
        int i19;
        ? r124;
        ? r125;
        int i20;
        ? r126;
        int i21;
        ? r132;
        MessageSchema messageSchema2 = this;
        T t3 = t;
        ? r127 = r32;
        int i22 = i2;
        int i23 = i3;
        Registers registers3 = registers;
        Unsafe unsafe2 = UNSAFE;
        int i24 = i;
        int i25 = -1;
        int i26 = 0;
        ? r34 = 0;
        int i27 = 0;
        int i28 = -1;
        while (true) {
            if (i24 < i22) {
                int i29 = i24 + 1;
                ? r0 = r127[i24];
                if (r0 < 0) {
                    i9 = ArrayDecoders.decodeVarint32(r0, r127, i29, registers3);
                    r5 = registers3.int1;
                } else {
                    r5 = r0;
                    i9 = i29;
                }
                int i30 = r5 >>> 3;
                int i31 = r5 & 7;
                if (i30 > i25) {
                    positionForFieldNumber = messageSchema2.positionForFieldNumber(i30, i26 / 3);
                } else {
                    positionForFieldNumber = messageSchema2.positionForFieldNumber(i30);
                }
                int i32 = positionForFieldNumber;
                if (i32 == -1) {
                    i13 = i30;
                    i5 = i9;
                    r8 = r5;
                    i11 = i27;
                    i10 = i28;
                    unsafe = unsafe2;
                    i14 = i23;
                    i12 = 0;
                } else {
                    int i33 = messageSchema2.buffer[i32 + 1];
                    int type = type(i33);
                    long offset = offset(i33);
                    ? r20 = r5;
                    int i34 = i33;
                    if (type <= 17) {
                        int i35 = messageSchema2.buffer[i32 + 2];
                        int i36 = 1 << (i35 >>> 20);
                        int i37 = i35 & OFFSET_MASK;
                        if (i37 != i28) {
                            if (i28 != -1) {
                                i17 = i32;
                                unsafe2.putInt(t3, (long) i28, i27);
                            } else {
                                i17 = i32;
                            }
                            i27 = unsafe2.getInt(t3, (long) i37);
                            i28 = i37;
                        } else {
                            i17 = i32;
                        }
                        switch (type) {
                            case 0:
                                i13 = i30;
                                long j = offset;
                                r13 = r20;
                                i19 = i17;
                                ? r128 = r32;
                                i18 = i9;
                                if (i31 == 1) {
                                    UnsafeUtil.putDouble((Object) t3, j, ArrayDecoders.decodeDouble(r128, i18));
                                    decodeMessageList = i18 + 8;
                                    i27 |= i36;
                                    r124 = r128;
                                    break;
                                }
                                break;
                            case 1:
                                i13 = i30;
                                long j2 = offset;
                                r13 = r20;
                                i19 = i17;
                                ? r129 = r32;
                                i18 = i9;
                                if (i31 == 5) {
                                    UnsafeUtil.putFloat((Object) t3, j2, ArrayDecoders.decodeFloat(r129, i18));
                                    decodeMessageList = i18 + 4;
                                    i27 |= i36;
                                    r124 = r129;
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                i13 = i30;
                                long j3 = offset;
                                r13 = r20;
                                i19 = i17;
                                ? r1210 = r32;
                                i18 = i9;
                                if (i31 == 0) {
                                    i20 = ArrayDecoders.decodeVarint64(r1210, i18, registers3);
                                    unsafe2.putLong(t, j3, registers3.long1);
                                    i27 |= i36;
                                    r125 = r1210;
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                i13 = i30;
                                long j4 = offset;
                                r13 = r20;
                                i19 = i17;
                                ? r1211 = r32;
                                i18 = i9;
                                if (i31 == 0) {
                                    decodeMessageList = ArrayDecoders.decodeVarint32(r1211, i18, registers3);
                                    unsafe2.putInt(t3, j4, registers3.int1);
                                    i27 |= i36;
                                    r124 = r1211;
                                    break;
                                }
                                break;
                            case 5:
                            case 14:
                                i13 = i30;
                                long j5 = offset;
                                r13 = r20;
                                i19 = i17;
                                int i38 = i2;
                                ? r1212 = r32;
                                if (i31 == 1) {
                                    int i39 = i9;
                                    unsafe2.putLong(t, j5, ArrayDecoders.decodeFixed64(r1212, i9));
                                    decodeMessageList = i39 + 8;
                                    i27 |= i36;
                                    r124 = r1212;
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                i13 = i30;
                                long j6 = offset;
                                r13 = r20;
                                i19 = i17;
                                i21 = i2;
                                ? r1213 = r32;
                                if (i31 == 5) {
                                    unsafe2.putInt(t3, j6, ArrayDecoders.decodeFixed32(r1213, i9));
                                    i24 = i9 + 4;
                                    r126 = r1213;
                                    break;
                                }
                                break;
                            case 7:
                                i13 = i30;
                                long j7 = offset;
                                r13 = r20;
                                i19 = i17;
                                i21 = i2;
                                ? r1214 = r32;
                                if (i31 == 0) {
                                    i24 = ArrayDecoders.decodeVarint64(r1214, i9, registers3);
                                    UnsafeUtil.putBoolean((Object) t3, j7, registers3.long1 != 0);
                                    r126 = r1214;
                                    break;
                                }
                                break;
                            case 8:
                                i13 = i30;
                                long j8 = offset;
                                r13 = r20;
                                i19 = i17;
                                i21 = i2;
                                ? r1215 = r32;
                                if (i31 == 2) {
                                    if ((i34 & ENFORCE_UTF8_MASK) == 0) {
                                        i24 = ArrayDecoders.decodeString(r1215, i9, registers3);
                                    } else {
                                        i24 = ArrayDecoders.decodeStringRequireUtf8(r1215, i9, registers3);
                                    }
                                    unsafe2.putObject(t3, j8, registers3.object1);
                                    r126 = r1215;
                                    break;
                                }
                                break;
                            case 9:
                                i13 = i30;
                                long j9 = offset;
                                ? r133 = r20;
                                i19 = i17;
                                ? r1216 = r32;
                                if (i31 != 2) {
                                    int i40 = i2;
                                    r13 = r133;
                                    break;
                                } else {
                                    i21 = i2;
                                    i24 = ArrayDecoders.decodeMessageField(messageSchema2.getMessageFieldSchema(i19), r1216, i9, i21, registers3);
                                    if ((i27 & i36) != 0) {
                                        unsafe2.putObject(t3, j9, Internal.mergeMessage(unsafe2.getObject(t3, j9), registers3.object1));
                                        r13 = r133;
                                        r126 = r1216;
                                        break;
                                    } else {
                                        unsafe2.putObject(t3, j9, registers3.object1);
                                        r13 = r133;
                                        r126 = r1216;
                                        break;
                                    }
                                }
                            case 10:
                                i13 = i30;
                                long j10 = offset;
                                r13 = r20;
                                i19 = i17;
                                ? r1217 = r32;
                                if (i31 == 2) {
                                    decodeMessageList = ArrayDecoders.decodeBytes(r1217, i9, registers3);
                                    unsafe2.putObject(t3, j10, registers3.object1);
                                    i27 |= i36;
                                    r124 = r1217;
                                    break;
                                }
                                break;
                            case 12:
                                i13 = i30;
                                long j11 = offset;
                                r13 = r20;
                                i19 = i17;
                                ? r1218 = r32;
                                if (i31 == 0) {
                                    decodeMessageList = ArrayDecoders.decodeVarint32(r1218, i9, registers3);
                                    int i41 = registers3.int1;
                                    EnumVerifier enumFieldVerifier = messageSchema2.getEnumFieldVerifier(i19);
                                    if (enumFieldVerifier != null && !enumFieldVerifier.isInRange(i41)) {
                                        getMutableUnknownFields(t).storeField(r13, Long.valueOf((long) i41));
                                        r124 = r1218;
                                        break;
                                    } else {
                                        unsafe2.putInt(t3, j11, i41);
                                        i27 |= i36;
                                        r124 = r1218;
                                        break;
                                    }
                                }
                                break;
                            case 15:
                                i13 = i30;
                                long j12 = offset;
                                r13 = r20;
                                i19 = i17;
                                ? r1219 = r32;
                                if (i31 == 0) {
                                    decodeMessageList = ArrayDecoders.decodeVarint32(r1219, i9, registers3);
                                    unsafe2.putInt(t3, j12, CodedInputStream.decodeZigZag32(registers3.int1));
                                    i27 |= i36;
                                    r124 = r1219;
                                    break;
                                }
                                break;
                            case 16:
                                i13 = i30;
                                ? r134 = r20;
                                i19 = i17;
                                if (i31 != 0) {
                                    ? r1220 = r32;
                                    r13 = r134;
                                    break;
                                } else {
                                    long j13 = offset;
                                    ? r1221 = r32;
                                    i20 = ArrayDecoders.decodeVarint64(r1221, i9, registers3);
                                    unsafe2.putLong(t, j13, CodedInputStream.decodeZigZag64(registers3.long1));
                                    i27 |= i36;
                                    r13 = r134;
                                    r125 = r1221;
                                    break;
                                }
                            case 17:
                                if (i31 != 3) {
                                    i13 = i30;
                                    r132 = r20;
                                    i19 = i17;
                                    i18 = i9;
                                    break;
                                } else {
                                    int i42 = i17;
                                    int i43 = i42;
                                    int i44 = i30;
                                    ? r135 = r20;
                                    decodeMessageList = ArrayDecoders.decodeGroupField(messageSchema2.getMessageFieldSchema(i42), r32, i9, i2, (i30 << 3) | 4, registers);
                                    if ((i27 & i36) == 0) {
                                        unsafe2.putObject(t3, offset, registers3.object1);
                                    } else {
                                        unsafe2.putObject(t3, offset, Internal.mergeMessage(unsafe2.getObject(t3, offset), registers3.object1));
                                    }
                                    i27 |= i36;
                                    i26 = i43;
                                    r33 = r135;
                                    i25 = i44;
                                    i23 = i3;
                                    r123 = r32;
                                    break;
                                }
                            default:
                                i13 = i30;
                                i18 = i9;
                                r132 = r20;
                                i19 = i17;
                                break;
                        }
                    } else {
                        int i45 = i32;
                        i13 = i30;
                        long j14 = offset;
                        ? r136 = r20;
                        r122 = r32;
                        int i46 = i9;
                        if (type != 27) {
                            i12 = i45;
                            i11 = i27;
                            if (type <= 49) {
                                i10 = i28;
                                unsafe = unsafe2;
                                int i47 = i46;
                                r29 = r136;
                                parseOneofField = parseRepeatedField(t, r32, i46, i2, r136, i13, i31, i12, (long) i34, type, j14, registers);
                            } else {
                                int i48 = i31;
                                long j15 = j14;
                                i10 = i28;
                                unsafe = unsafe2;
                                i15 = i46;
                                r29 = r136;
                                int i49 = i34;
                                int i50 = type;
                                if (i50 != 50) {
                                    parseOneofField = parseOneofField(t, r32, i15, i2, r29, i13, i48, i49, i50, j15, i12, registers);
                                } else if (i48 == 2) {
                                    parseOneofField = parseMapField(t, r32, i15, i2, i12, j15, registers);
                                }
                            }
                            r12 = r32;
                            i25 = i13;
                            i26 = i12;
                            i27 = i11;
                            i28 = i10;
                            unsafe2 = unsafe;
                            r3 = r29;
                            registers3 = registers;
                            i23 = i3;
                            i22 = i2;
                            t3 = t;
                            messageSchema2 = this;
                            r127 = r12;
                            r34 = r3;
                        } else if (i31 == 2) {
                            ProtobufList protobufList = (ProtobufList) unsafe2.getObject(t3, j14);
                            if (!protobufList.isModifiable()) {
                                int size = protobufList.size();
                                protobufList = protobufList.mutableCopyWithCapacity(size == 0 ? 10 : size * 2);
                                unsafe2.putObject(t3, j14, protobufList);
                            }
                            int i51 = i45;
                            int i52 = i27;
                            decodeMessageList = ArrayDecoders.decodeMessageList(messageSchema2.getMessageFieldSchema(i45), r136, r32, i46, i2, protobufList, registers);
                            i16 = r136;
                            i25 = i13;
                            i26 = i51;
                            i27 = i52;
                        } else {
                            i12 = i45;
                            i11 = i27;
                            i10 = i28;
                            unsafe = unsafe2;
                            i15 = i46;
                            r29 = r136;
                        }
                        i5 = i15;
                        ? r292 = r29;
                        ? r82 = r292;
                        i14 = i3;
                        r8 = r82;
                    }
                    i23 = i3;
                    r123 = r122;
                    r33 = i16;
                    i22 = i2;
                    r12 = r123;
                    r3 = r33;
                    r127 = r12;
                    r34 = r3;
                }
                if (r8 != i14 || i14 == 0) {
                    int i53 = i14;
                    if (this.hasExtensions) {
                        registers2 = registers;
                        if (registers2.extensionRegistry != ExtensionRegistryLite.getEmptyRegistry()) {
                            decodeUnknownField = ArrayDecoders.decodeExtensionOrUnknownField(r8, r32, i5, i2, t, this.defaultInstance, this.unknownFieldSchema, registers);
                            r12 = r32;
                            r3 = r8;
                            messageSchema2 = this;
                            registers3 = registers2;
                            i25 = i13;
                            i26 = i12;
                            i27 = i11;
                            i28 = i10;
                            i22 = i2;
                            t3 = t;
                            i23 = i53;
                            unsafe2 = unsafe;
                            r127 = r12;
                            r34 = r3;
                        }
                    } else {
                        registers2 = registers;
                    }
                    decodeUnknownField = ArrayDecoders.decodeUnknownField(r8, r32, i5, i2, getMutableUnknownFields(t), registers);
                    r12 = r32;
                    r3 = r8;
                    messageSchema2 = this;
                    registers3 = registers2;
                    i25 = i13;
                    i26 = i12;
                    i27 = i11;
                    i28 = i10;
                    i22 = i2;
                    t3 = t;
                    i23 = i53;
                    unsafe2 = unsafe;
                    r127 = r12;
                    r34 = r3;
                } else {
                    i4 = i14;
                    r34 = r8;
                    i6 = i11;
                    i7 = i10;
                    i8 = -1;
                    messageSchema = this;
                }
            } else {
                unsafe = unsafe2;
                i4 = i23;
                messageSchema = messageSchema2;
                i5 = i24;
                i6 = i27;
                i7 = i28;
                i8 = -1;
            }
        }
        if (i7 != i8) {
            long j16 = (long) i7;
            t2 = t;
            unsafe.putInt(t2, j16, i6);
        } else {
            t2 = t;
        }
        UnknownFieldSetLite unknownFieldSetLite = null;
        for (int i54 = messageSchema.checkInitializedCount; i54 < messageSchema.repeatedFieldOffsetStart; i54++) {
            unknownFieldSetLite = (UnknownFieldSetLite) messageSchema.filterMapUnknownEnumValues(t2, messageSchema.intArray[i54], unknownFieldSetLite, messageSchema.unknownFieldSchema);
        }
        if (unknownFieldSetLite != null) {
            messageSchema.unknownFieldSchema.setBuilderToMessage(t2, unknownFieldSetLite);
        }
        if (i4 == 0) {
            if (i5 != i2) {
                throw InvalidProtocolBufferException.parseFailure();
            }
        } else if (i5 > i2 || r34 != i4) {
            throw InvalidProtocolBufferException.parseFailure();
        }
        return i5;
    }

    /* JADX WARNING: type inference failed for: r30v0, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v0 */
    /* JADX WARNING: type inference failed for: r12v1, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r0v3, types: [byte, int] */
    /* JADX WARNING: type inference failed for: r17v0, types: [int] */
    /* JADX WARNING: type inference failed for: r12v2 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r0v8, types: [int] */
    /* JADX WARNING: type inference failed for: r1v5, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r2v6, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v2, types: [int] */
    /* JADX WARNING: type inference failed for: r2v7, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r2v9, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v4, types: [int] */
    /* JADX WARNING: type inference failed for: r1v11, types: [int] */
    /* JADX WARNING: type inference failed for: r2v10, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r17v1 */
    /* JADX WARNING: type inference failed for: r3v12, types: [int] */
    /* JADX WARNING: type inference failed for: r17v2 */
    /* JADX WARNING: type inference failed for: r12v6 */
    /* JADX WARNING: type inference failed for: r12v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x010f, code lost:
        r2 = r4;
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0143, code lost:
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0161, code lost:
        r1 = r7;
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0166, code lost:
        r25 = r7;
        r15 = r8;
        r18 = r9;
        r19 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01de, code lost:
        if (r0 != r15) goto L_0x0240;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x020e, code lost:
        if (r0 != r15) goto L_0x0240;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x022d, code lost:
        if (r0 != r15) goto L_0x0240;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=null, for r0v3, types: [byte, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte[], code=null, for r30v0, types: [byte[]] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r12v2
  assigns: []
  uses: []
  mth insns count: 258
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 15 */
    private int parseProto3Message(T t, byte[] r30, int i, int i2, Registers registers) throws IOException {
        ? r17;
        int i3;
        int positionForFieldNumber;
        ? r12;
        int i4;
        int i5;
        Unsafe unsafe;
        int i6;
        int decodeUnknownField;
        int i7;
        int i8;
        int i9;
        int i10;
        MessageSchema messageSchema = this;
        T t2 = t;
        ? r122 = r30;
        int i11 = i2;
        Registers registers2 = registers;
        Unsafe unsafe2 = UNSAFE;
        int i12 = -1;
        int i13 = i;
        int i14 = -1;
        int i15 = 0;
        while (i13 < i11) {
            int i16 = i13 + 1;
            ? r0 = r122[i13];
            if (r0 < 0) {
                i3 = ArrayDecoders.decodeVarint32(r0, r122, i16, registers2);
                r17 = registers2.int1;
            } else {
                r17 = r0;
                i3 = i16;
            }
            int i17 = r17 >>> 3;
            int i18 = r17 & 7;
            if (i17 > i14) {
                positionForFieldNumber = messageSchema.positionForFieldNumber(i17, i15 / 3);
            } else {
                positionForFieldNumber = messageSchema.positionForFieldNumber(i17);
            }
            int i19 = positionForFieldNumber;
            if (i19 == i12) {
                i4 = i17;
                i6 = i3;
                unsafe = unsafe2;
                i5 = 0;
            } else {
                int i20 = messageSchema.buffer[i19 + 1];
                int type = type(i20);
                long offset = offset(i20);
                if (type <= 17) {
                    boolean z = true;
                    switch (type) {
                        case 0:
                            long j = offset;
                            i8 = i19;
                            if (i18 == 1) {
                                UnsafeUtil.putDouble((Object) t2, j, ArrayDecoders.decodeDouble(r122, i3));
                                i9 = i3 + 8;
                                break;
                            }
                            break;
                        case 1:
                            long j2 = offset;
                            i8 = i19;
                            if (i18 == 5) {
                                UnsafeUtil.putFloat((Object) t2, j2, ArrayDecoders.decodeFloat(r122, i3));
                                i9 = i3 + 4;
                                break;
                            }
                            break;
                        case 2:
                        case 3:
                            long j3 = offset;
                            i8 = i19;
                            if (i18 == 0) {
                                i10 = ArrayDecoders.decodeVarint64(r122, i3, registers2);
                                unsafe2.putLong(t, j3, registers2.long1);
                                break;
                            }
                            break;
                        case 4:
                        case 11:
                            long j4 = offset;
                            i8 = i19;
                            if (i18 == 0) {
                                i9 = ArrayDecoders.decodeVarint32(r122, i3, registers2);
                                unsafe2.putInt(t2, j4, registers2.int1);
                                break;
                            }
                            break;
                        case 5:
                        case 14:
                            long j5 = offset;
                            if (i18 == 1) {
                                i8 = i19;
                                unsafe2.putLong(t, j5, ArrayDecoders.decodeFixed64(r122, i3));
                                i9 = i3 + 8;
                                break;
                            }
                            break;
                        case 6:
                        case 13:
                            long j6 = offset;
                            if (i18 == 5) {
                                unsafe2.putInt(t2, j6, ArrayDecoders.decodeFixed32(r122, i3));
                                i13 = i3 + 4;
                                break;
                            }
                            break;
                        case 7:
                            long j7 = offset;
                            if (i18 == 0) {
                                int decodeVarint64 = ArrayDecoders.decodeVarint64(r122, i3, registers2);
                                if (registers2.long1 == 0) {
                                    z = false;
                                }
                                UnsafeUtil.putBoolean((Object) t2, j7, z);
                                i13 = decodeVarint64;
                                break;
                            }
                            break;
                        case 8:
                            long j8 = offset;
                            if (i18 == 2) {
                                if ((ENFORCE_UTF8_MASK & i20) == 0) {
                                    i13 = ArrayDecoders.decodeString(r122, i3, registers2);
                                } else {
                                    i13 = ArrayDecoders.decodeStringRequireUtf8(r122, i3, registers2);
                                }
                                unsafe2.putObject(t2, j8, registers2.object1);
                                break;
                            }
                            break;
                        case 9:
                            long j9 = offset;
                            if (i18 == 2) {
                                i13 = ArrayDecoders.decodeMessageField(messageSchema.getMessageFieldSchema(i19), r122, i3, i11, registers2);
                                Object object = unsafe2.getObject(t2, j9);
                                if (object != null) {
                                    unsafe2.putObject(t2, j9, Internal.mergeMessage(object, registers2.object1));
                                    break;
                                } else {
                                    unsafe2.putObject(t2, j9, registers2.object1);
                                    break;
                                }
                            }
                            break;
                        case 10:
                            long j10 = offset;
                            if (i18 == 2) {
                                i13 = ArrayDecoders.decodeBytes(r122, i3, registers2);
                                unsafe2.putObject(t2, j10, registers2.object1);
                                break;
                            }
                            break;
                        case 12:
                            long j11 = offset;
                            i8 = i19;
                            if (i18 == 0) {
                                i9 = ArrayDecoders.decodeVarint32(r122, i3, registers2);
                                unsafe2.putInt(t2, j11, registers2.int1);
                                break;
                            }
                            break;
                        case 15:
                            long j12 = offset;
                            i8 = i19;
                            if (i18 == 0) {
                                i9 = ArrayDecoders.decodeVarint32(r122, i3, registers2);
                                unsafe2.putInt(t2, j12, CodedInputStream.decodeZigZag32(registers2.int1));
                                break;
                            }
                            break;
                        case 16:
                            if (i18 == 0) {
                                i10 = ArrayDecoders.decodeVarint64(r122, i3, registers2);
                                i8 = i19;
                                unsafe2.putLong(t, offset, CodedInputStream.decodeZigZag64(registers2.long1));
                                break;
                            }
                            break;
                    }
                } else if (type != 27) {
                    i5 = i19;
                    if (type <= 49) {
                        long j13 = (long) i20;
                        int i21 = i18;
                        i4 = i17;
                        int i22 = i3;
                        unsafe = unsafe2;
                        decodeUnknownField = parseRepeatedField(t, r30, i3, i2, r17, i17, i21, i5, j13, type, offset, registers);
                    } else {
                        long j14 = offset;
                        int i23 = i18;
                        i4 = i17;
                        i7 = i3;
                        unsafe = unsafe2;
                        int i24 = type;
                        if (i24 == 50) {
                            if (i23 == 2) {
                                decodeUnknownField = parseMapField(t, r30, i7, i2, i5, j14, registers);
                            }
                            i6 = i7;
                        } else {
                            decodeUnknownField = parseOneofField(t, r30, i7, i2, r17, i4, i23, i20, i24, j14, i5, registers);
                        }
                    }
                    i6 = decodeUnknownField;
                } else if (i18 == 2) {
                    ProtobufList protobufList = (ProtobufList) unsafe2.getObject(t2, offset);
                    if (!protobufList.isModifiable()) {
                        int size = protobufList.size();
                        protobufList = protobufList.mutableCopyWithCapacity(size == 0 ? 10 : size * 2);
                        unsafe2.putObject(t2, offset, protobufList);
                    }
                    int i25 = i19;
                    i13 = ArrayDecoders.decodeMessageList(messageSchema.getMessageFieldSchema(i19), r17, r30, i3, i2, protobufList, registers);
                    i14 = i17;
                    i15 = i25;
                    i12 = -1;
                    r12 = r122;
                    r122 = r12;
                }
                i5 = i19;
                i4 = i17;
                i7 = i3;
                unsafe = unsafe2;
                i6 = i7;
            }
            decodeUnknownField = ArrayDecoders.decodeUnknownField(r17, r30, i6, i2, getMutableUnknownFields(t), registers);
            t2 = t;
            r12 = r30;
            registers2 = registers;
            unsafe2 = unsafe;
            i15 = i5;
            i14 = i4;
            i12 = -1;
            i11 = i2;
            messageSchema = this;
            r122 = r12;
        }
        if (i13 == i11) {
            return i13;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }

    public void mergeFrom(T t, byte[] bArr, int i, int i2, Registers registers) throws IOException {
        if (this.proto3) {
            parseProto3Message(t, bArr, i, i2, registers);
        } else {
            parseProto2Message(t, bArr, i, i2, 0, registers);
        }
    }

    public void makeImmutable(T t) {
        for (int i = this.checkInitializedCount; i < this.repeatedFieldOffsetStart; i++) {
            long offset = offset(typeAndOffsetAt(this.intArray[i]));
            Object object = UnsafeUtil.getObject((Object) t, offset);
            if (object != null) {
                UnsafeUtil.putObject((Object) t, offset, this.mapFieldSchema.toImmutable(object));
            }
        }
        int length = this.intArray.length;
        for (int i2 = this.repeatedFieldOffsetStart; i2 < length; i2++) {
            this.listFieldSchema.makeImmutableListAt(t, (long) this.intArray[i2]);
        }
        this.unknownFieldSchema.makeImmutable(t);
        if (this.hasExtensions) {
            this.extensionSchema.makeImmutable(t);
        }
    }

    private final <K, V> void mergeMap(Object obj, int i, Object obj2, ExtensionRegistryLite extensionRegistryLite, Reader reader) throws IOException {
        long offset = offset(typeAndOffsetAt(i));
        Object object = UnsafeUtil.getObject(obj, offset);
        if (object == null) {
            object = this.mapFieldSchema.newMapField(obj2);
            UnsafeUtil.putObject(obj, offset, object);
        } else if (this.mapFieldSchema.isImmutable(object)) {
            Object newMapField = this.mapFieldSchema.newMapField(obj2);
            this.mapFieldSchema.mergeFrom(newMapField, object);
            UnsafeUtil.putObject(obj, offset, newMapField);
            object = newMapField;
        }
        reader.readMap(this.mapFieldSchema.forMutableMapData(object), this.mapFieldSchema.forMapMetadata(obj2), extensionRegistryLite);
    }

    private final <UT, UB> UB filterMapUnknownEnumValues(Object obj, int i, UB ub, UnknownFieldSchema<UT, UB> unknownFieldSchema2) {
        int numberAt = numberAt(i);
        Object object = UnsafeUtil.getObject(obj, offset(typeAndOffsetAt(i)));
        if (object == null) {
            return ub;
        }
        EnumVerifier enumFieldVerifier = getEnumFieldVerifier(i);
        if (enumFieldVerifier == null) {
            return ub;
        }
        return filterUnknownEnumMap(i, numberAt, this.mapFieldSchema.forMutableMapData(object), enumFieldVerifier, ub, unknownFieldSchema2);
    }

    private final <K, V, UT, UB> UB filterUnknownEnumMap(int i, int i2, Map<K, V> map, EnumVerifier enumVerifier, UB ub, UnknownFieldSchema<UT, UB> unknownFieldSchema2) {
        Metadata forMapMetadata = this.mapFieldSchema.forMapMetadata(getMapFieldDefaultEntry(i));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (!enumVerifier.isInRange(((Integer) entry.getValue()).intValue())) {
                if (ub == null) {
                    ub = unknownFieldSchema2.newBuilder();
                }
                CodedBuilder newCodedBuilder = ByteString.newCodedBuilder(MapEntryLite.computeSerializedSize(forMapMetadata, entry.getKey(), entry.getValue()));
                try {
                    MapEntryLite.writeTo(newCodedBuilder.getCodedOutput(), forMapMetadata, entry.getKey(), entry.getValue());
                    unknownFieldSchema2.addLengthDelimited(ub, i2, newCodedBuilder.build());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x008d, code lost:
        continue;
     */
    public final boolean isInitialized(T t) {
        int i;
        int i2 = -1;
        int i3 = 0;
        for (int i4 = 0; i4 < this.checkInitializedCount; i4++) {
            int i5 = this.intArray[i4];
            int numberAt = numberAt(i5);
            int typeAndOffsetAt = typeAndOffsetAt(i5);
            if (!this.proto3) {
                int i6 = this.buffer[i5 + 2];
                int i7 = OFFSET_MASK & i6;
                i = 1 << (i6 >>> 20);
                if (i7 != i2) {
                    i3 = UNSAFE.getInt(t, (long) i7);
                    i2 = i7;
                }
            } else {
                i = 0;
            }
            if (isRequired(typeAndOffsetAt) && !isFieldPresent(t, i5, i3, i)) {
                return false;
            }
            int type = type(typeAndOffsetAt);
            if (type != 9 && type != 17) {
                if (type != 27) {
                    if (type != 60 && type != 68) {
                        switch (type) {
                            case 49:
                                break;
                            case 50:
                                if (!isMapInitialized(t, typeAndOffsetAt, i5)) {
                                    return false;
                                }
                                continue;
                        }
                    } else if (isOneofPresent(t, numberAt, i5) && !isInitialized(t, typeAndOffsetAt, getMessageFieldSchema(i5))) {
                        return false;
                    }
                }
                if (!isListInitialized(t, typeAndOffsetAt, i5)) {
                    return false;
                }
            } else if (isFieldPresent(t, i5, i3, i) && !isInitialized(t, typeAndOffsetAt, getMessageFieldSchema(i5))) {
                return false;
            }
        }
        return !this.hasExtensions || this.extensionSchema.getExtensions(t).isInitialized();
    }

    private static boolean isInitialized(Object obj, int i, Schema schema) {
        return schema.isInitialized(UnsafeUtil.getObject(obj, offset(i)));
    }

    private <N> boolean isListInitialized(Object obj, int i, int i2) {
        List list = (List) UnsafeUtil.getObject(obj, offset(i));
        if (list.isEmpty()) {
            return true;
        }
        Schema messageFieldSchema = getMessageFieldSchema(i2);
        for (int i3 = 0; i3 < list.size(); i3++) {
            if (!messageFieldSchema.isInitialized(list.get(i3))) {
                return false;
            }
        }
        return true;
    }

    private boolean isMapInitialized(T t, int i, int i2) {
        Map forMapData = this.mapFieldSchema.forMapData(UnsafeUtil.getObject((Object) t, offset(i)));
        if (forMapData.isEmpty()) {
            return true;
        }
        if (this.mapFieldSchema.forMapMetadata(getMapFieldDefaultEntry(i2)).valueType.getJavaType() != JavaType.MESSAGE) {
            return true;
        }
        Schema schema = null;
        for (Object next : forMapData.values()) {
            if (schema == null) {
                schema = Protobuf.getInstance().schemaFor(next.getClass());
            }
            if (!schema.isInitialized(next)) {
                return false;
            }
        }
        return true;
    }

    private void writeString(int i, Object obj, Writer writer) throws IOException {
        if (obj instanceof String) {
            writer.writeString(i, (String) obj);
        } else {
            writer.writeBytes(i, (ByteString) obj);
        }
    }

    private void readString(Object obj, int i, Reader reader) throws IOException {
        if (isEnforceUtf8(i)) {
            UnsafeUtil.putObject(obj, offset(i), (Object) reader.readStringRequireUtf8());
        } else if (this.lite) {
            UnsafeUtil.putObject(obj, offset(i), (Object) reader.readString());
        } else {
            UnsafeUtil.putObject(obj, offset(i), (Object) reader.readBytes());
        }
    }

    private void readStringList(Object obj, int i, Reader reader) throws IOException {
        if (isEnforceUtf8(i)) {
            reader.readStringListRequireUtf8(this.listFieldSchema.mutableListAt(obj, offset(i)));
        } else {
            reader.readStringList(this.listFieldSchema.mutableListAt(obj, offset(i)));
        }
    }

    private <E> void readMessageList(Object obj, int i, Reader reader, Schema<E> schema, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        reader.readMessageList(this.listFieldSchema.mutableListAt(obj, offset(i)), schema, extensionRegistryLite);
    }

    private <E> void readGroupList(Object obj, long j, Reader reader, Schema<E> schema, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        reader.readGroupList(this.listFieldSchema.mutableListAt(obj, j), schema, extensionRegistryLite);
    }

    private int numberAt(int i) {
        return this.buffer[i];
    }

    private int typeAndOffsetAt(int i) {
        return this.buffer[i + 1];
    }

    private int presenceMaskAndOffsetAt(int i) {
        return this.buffer[i + 2];
    }

    private static <T> double doubleAt(T t, long j) {
        return UnsafeUtil.getDouble((Object) t, j);
    }

    private static <T> float floatAt(T t, long j) {
        return UnsafeUtil.getFloat((Object) t, j);
    }

    private static <T> int intAt(T t, long j) {
        return UnsafeUtil.getInt((Object) t, j);
    }

    private static <T> long longAt(T t, long j) {
        return UnsafeUtil.getLong((Object) t, j);
    }

    private static <T> boolean booleanAt(T t, long j) {
        return UnsafeUtil.getBoolean((Object) t, j);
    }

    private static <T> double oneofDoubleAt(T t, long j) {
        return ((Double) UnsafeUtil.getObject((Object) t, j)).doubleValue();
    }

    private static <T> float oneofFloatAt(T t, long j) {
        return ((Float) UnsafeUtil.getObject((Object) t, j)).floatValue();
    }

    private static <T> int oneofIntAt(T t, long j) {
        return ((Integer) UnsafeUtil.getObject((Object) t, j)).intValue();
    }

    private static <T> long oneofLongAt(T t, long j) {
        return ((Long) UnsafeUtil.getObject((Object) t, j)).longValue();
    }

    private static <T> boolean oneofBooleanAt(T t, long j) {
        return ((Boolean) UnsafeUtil.getObject((Object) t, j)).booleanValue();
    }

    private boolean arePresentForEquals(T t, T t2, int i) {
        return isFieldPresent(t, i) == isFieldPresent(t2, i);
    }

    private boolean isFieldPresent(T t, int i, int i2, int i3) {
        if (this.proto3) {
            return isFieldPresent(t, i);
        }
        return (i2 & i3) != 0;
    }

    private boolean isFieldPresent(T t, int i) {
        boolean z = false;
        if (this.proto3) {
            int typeAndOffsetAt = typeAndOffsetAt(i);
            long offset = offset(typeAndOffsetAt);
            switch (type(typeAndOffsetAt)) {
                case 0:
                    if (UnsafeUtil.getDouble((Object) t, offset) != Utils.DOUBLE_EPSILON) {
                        z = true;
                    }
                    return z;
                case 1:
                    if (UnsafeUtil.getFloat((Object) t, offset) != 0.0f) {
                        z = true;
                    }
                    return z;
                case 2:
                    if (UnsafeUtil.getLong((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 3:
                    if (UnsafeUtil.getLong((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 4:
                    if (UnsafeUtil.getInt((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 5:
                    if (UnsafeUtil.getLong((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 6:
                    if (UnsafeUtil.getInt((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 7:
                    return UnsafeUtil.getBoolean((Object) t, offset);
                case 8:
                    Object object = UnsafeUtil.getObject((Object) t, offset);
                    if (object instanceof String) {
                        return !((String) object).isEmpty();
                    }
                    if (object instanceof ByteString) {
                        return !ByteString.EMPTY.equals(object);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    if (UnsafeUtil.getObject((Object) t, offset) != null) {
                        z = true;
                    }
                    return z;
                case 10:
                    return !ByteString.EMPTY.equals(UnsafeUtil.getObject((Object) t, offset));
                case 11:
                    if (UnsafeUtil.getInt((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 12:
                    if (UnsafeUtil.getInt((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 13:
                    if (UnsafeUtil.getInt((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 14:
                    if (UnsafeUtil.getLong((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 15:
                    if (UnsafeUtil.getInt((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 16:
                    if (UnsafeUtil.getLong((Object) t, offset) != 0) {
                        z = true;
                    }
                    return z;
                case 17:
                    if (UnsafeUtil.getObject((Object) t, offset) != null) {
                        z = true;
                    }
                    return z;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int presenceMaskAndOffsetAt = presenceMaskAndOffsetAt(i);
            if ((UnsafeUtil.getInt((Object) t, (long) (presenceMaskAndOffsetAt & OFFSET_MASK)) & (1 << (presenceMaskAndOffsetAt >>> 20))) != 0) {
                z = true;
            }
            return z;
        }
    }

    private void setFieldPresent(T t, int i) {
        if (!this.proto3) {
            int presenceMaskAndOffsetAt = presenceMaskAndOffsetAt(i);
            long j = (long) (presenceMaskAndOffsetAt & OFFSET_MASK);
            UnsafeUtil.putInt((Object) t, j, UnsafeUtil.getInt((Object) t, j) | (1 << (presenceMaskAndOffsetAt >>> 20)));
        }
    }

    private boolean isOneofPresent(T t, int i, int i2) {
        return UnsafeUtil.getInt((Object) t, (long) (presenceMaskAndOffsetAt(i2) & OFFSET_MASK)) == i;
    }

    private boolean isOneofCaseEqual(T t, T t2, int i) {
        long presenceMaskAndOffsetAt = (long) (presenceMaskAndOffsetAt(i) & OFFSET_MASK);
        return UnsafeUtil.getInt((Object) t, presenceMaskAndOffsetAt) == UnsafeUtil.getInt((Object) t2, presenceMaskAndOffsetAt);
    }

    private void setOneofPresent(T t, int i, int i2) {
        UnsafeUtil.putInt((Object) t, (long) (presenceMaskAndOffsetAt(i2) & OFFSET_MASK), i);
    }

    private int positionForFieldNumber(int i) {
        if (i < this.minFieldNumber || i > this.maxFieldNumber) {
            return -1;
        }
        return slowPositionForFieldNumber(i, 0);
    }

    private int positionForFieldNumber(int i, int i2) {
        if (i < this.minFieldNumber || i > this.maxFieldNumber) {
            return -1;
        }
        return slowPositionForFieldNumber(i, i2);
    }

    private int slowPositionForFieldNumber(int i, int i2) {
        int length = (this.buffer.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int numberAt = numberAt(i4);
            if (i == numberAt) {
                return i4;
            }
            if (i < numberAt) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }

    /* access modifiers changed from: 0000 */
    public int getSchemaSize() {
        return this.buffer.length * 3;
    }
}
