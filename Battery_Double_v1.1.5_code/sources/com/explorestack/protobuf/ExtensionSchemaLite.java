package com.explorestack.protobuf;

import com.explorestack.protobuf.GeneratedMessageLite.ExtendableMessage;
import com.explorestack.protobuf.GeneratedMessageLite.GeneratedExtension;
import com.explorestack.protobuf.WireFormat.FieldType;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

final class ExtensionSchemaLite extends ExtensionSchema<ExtensionDescriptor> {
    ExtensionSchemaLite() {
    }

    /* access modifiers changed from: 0000 */
    public boolean hasExtensions(MessageLite messageLite) {
        return messageLite instanceof ExtendableMessage;
    }

    /* access modifiers changed from: 0000 */
    public FieldSet<ExtensionDescriptor> getExtensions(Object obj) {
        return ((ExtendableMessage) obj).extensions;
    }

    /* access modifiers changed from: 0000 */
    public void setExtensions(Object obj, FieldSet<ExtensionDescriptor> fieldSet) {
        ((ExtendableMessage) obj).extensions = fieldSet;
    }

    /* access modifiers changed from: 0000 */
    public FieldSet<ExtensionDescriptor> getMutableExtensions(Object obj) {
        return ((ExtendableMessage) obj).ensureExtensionsAreMutable();
    }

    /* access modifiers changed from: 0000 */
    public void makeImmutable(Object obj) {
        getExtensions(obj).makeImmutable();
    }

    /* access modifiers changed from: 0000 */
    public <UT, UB> UB parseExtension(Reader reader, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<ExtensionDescriptor> fieldSet, UB ub, UnknownFieldSchema<UT, UB> unknownFieldSchema) throws IOException {
        ArrayList arrayList;
        GeneratedExtension generatedExtension = (GeneratedExtension) obj;
        int number = generatedExtension.getNumber();
        if (!generatedExtension.descriptor.isRepeated() || !generatedExtension.descriptor.isPacked()) {
            Object obj2 = null;
            if (generatedExtension.getLiteType() != FieldType.ENUM) {
                switch (generatedExtension.getLiteType()) {
                    case DOUBLE:
                        obj2 = Double.valueOf(reader.readDouble());
                        break;
                    case FLOAT:
                        obj2 = Float.valueOf(reader.readFloat());
                        break;
                    case INT64:
                        obj2 = Long.valueOf(reader.readInt64());
                        break;
                    case UINT64:
                        obj2 = Long.valueOf(reader.readUInt64());
                        break;
                    case INT32:
                        obj2 = Integer.valueOf(reader.readInt32());
                        break;
                    case FIXED64:
                        obj2 = Long.valueOf(reader.readFixed64());
                        break;
                    case FIXED32:
                        obj2 = Integer.valueOf(reader.readFixed32());
                        break;
                    case BOOL:
                        obj2 = Boolean.valueOf(reader.readBool());
                        break;
                    case UINT32:
                        obj2 = Integer.valueOf(reader.readUInt32());
                        break;
                    case SFIXED32:
                        obj2 = Integer.valueOf(reader.readSFixed32());
                        break;
                    case SFIXED64:
                        obj2 = Long.valueOf(reader.readSFixed64());
                        break;
                    case SINT32:
                        obj2 = Integer.valueOf(reader.readSInt32());
                        break;
                    case SINT64:
                        obj2 = Long.valueOf(reader.readSInt64());
                        break;
                    case ENUM:
                        throw new IllegalStateException("Shouldn't reach here.");
                    case BYTES:
                        obj2 = reader.readBytes();
                        break;
                    case STRING:
                        obj2 = reader.readString();
                        break;
                    case GROUP:
                        obj2 = reader.readGroup(generatedExtension.getMessageDefaultInstance().getClass(), extensionRegistryLite);
                        break;
                    case MESSAGE:
                        obj2 = reader.readMessage(generatedExtension.getMessageDefaultInstance().getClass(), extensionRegistryLite);
                        break;
                }
            } else {
                int readInt32 = reader.readInt32();
                if (generatedExtension.descriptor.getEnumType().findValueByNumber(readInt32) == null) {
                    return SchemaUtil.storeUnknownEnum(number, readInt32, ub, unknownFieldSchema);
                }
                obj2 = Integer.valueOf(readInt32);
            }
            if (generatedExtension.isRepeated()) {
                fieldSet.addRepeatedField(generatedExtension.descriptor, obj2);
            } else {
                switch (generatedExtension.getLiteType()) {
                    case GROUP:
                    case MESSAGE:
                        Object field = fieldSet.getField(generatedExtension.descriptor);
                        if (field != null) {
                            obj2 = Internal.mergeMessage(field, obj2);
                            break;
                        }
                        break;
                }
                fieldSet.setField(generatedExtension.descriptor, obj2);
            }
        } else {
            switch (generatedExtension.getLiteType()) {
                case DOUBLE:
                    arrayList = new ArrayList();
                    reader.readDoubleList(arrayList);
                    break;
                case FLOAT:
                    arrayList = new ArrayList();
                    reader.readFloatList(arrayList);
                    break;
                case INT64:
                    arrayList = new ArrayList();
                    reader.readInt64List(arrayList);
                    break;
                case UINT64:
                    arrayList = new ArrayList();
                    reader.readUInt64List(arrayList);
                    break;
                case INT32:
                    arrayList = new ArrayList();
                    reader.readInt32List(arrayList);
                    break;
                case FIXED64:
                    arrayList = new ArrayList();
                    reader.readFixed64List(arrayList);
                    break;
                case FIXED32:
                    arrayList = new ArrayList();
                    reader.readFixed32List(arrayList);
                    break;
                case BOOL:
                    arrayList = new ArrayList();
                    reader.readBoolList(arrayList);
                    break;
                case UINT32:
                    arrayList = new ArrayList();
                    reader.readUInt32List(arrayList);
                    break;
                case SFIXED32:
                    arrayList = new ArrayList();
                    reader.readSFixed32List(arrayList);
                    break;
                case SFIXED64:
                    arrayList = new ArrayList();
                    reader.readSFixed64List(arrayList);
                    break;
                case SINT32:
                    arrayList = new ArrayList();
                    reader.readSInt32List(arrayList);
                    break;
                case SINT64:
                    arrayList = new ArrayList();
                    reader.readSInt64List(arrayList);
                    break;
                case ENUM:
                    arrayList = new ArrayList();
                    reader.readEnumList(arrayList);
                    ub = SchemaUtil.filterUnknownEnumList(number, (List<Integer>) arrayList, generatedExtension.descriptor.getEnumType(), ub, unknownFieldSchema);
                    break;
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("Type cannot be packed: ");
                    sb.append(generatedExtension.descriptor.getLiteType());
                    throw new IllegalStateException(sb.toString());
            }
            fieldSet.setField(generatedExtension.descriptor, arrayList);
        }
        return ub;
    }

    /* access modifiers changed from: 0000 */
    public int extensionNumber(Entry<?, ?> entry) {
        return ((ExtensionDescriptor) entry.getKey()).getNumber();
    }

    /* access modifiers changed from: 0000 */
    public void serializeExtension(Writer writer, Entry<?, ?> entry) throws IOException {
        ExtensionDescriptor extensionDescriptor = (ExtensionDescriptor) entry.getKey();
        if (extensionDescriptor.isRepeated()) {
            switch (extensionDescriptor.getLiteType()) {
                case DOUBLE:
                    SchemaUtil.writeDoubleList(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case FLOAT:
                    SchemaUtil.writeFloatList(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case INT64:
                    SchemaUtil.writeInt64List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case UINT64:
                    SchemaUtil.writeUInt64List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case INT32:
                    SchemaUtil.writeInt32List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case FIXED64:
                    SchemaUtil.writeFixed64List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case FIXED32:
                    SchemaUtil.writeFixed32List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case BOOL:
                    SchemaUtil.writeBoolList(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case UINT32:
                    SchemaUtil.writeUInt32List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case SFIXED32:
                    SchemaUtil.writeSFixed32List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case SFIXED64:
                    SchemaUtil.writeSFixed64List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case SINT32:
                    SchemaUtil.writeSInt32List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case SINT64:
                    SchemaUtil.writeSInt64List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case ENUM:
                    SchemaUtil.writeInt32List(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, extensionDescriptor.isPacked());
                    return;
                case BYTES:
                    SchemaUtil.writeBytesList(extensionDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                case STRING:
                    SchemaUtil.writeStringList(extensionDescriptor.getNumber(), (List) entry.getValue(), writer);
                    return;
                case GROUP:
                    List list = (List) entry.getValue();
                    if (list != null && !list.isEmpty()) {
                        SchemaUtil.writeGroupList(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, Protobuf.getInstance().schemaFor(list.get(0).getClass()));
                        return;
                    }
                    return;
                case MESSAGE:
                    List list2 = (List) entry.getValue();
                    if (list2 != null && !list2.isEmpty()) {
                        SchemaUtil.writeMessageList(extensionDescriptor.getNumber(), (List) entry.getValue(), writer, Protobuf.getInstance().schemaFor(list2.get(0).getClass()));
                        return;
                    }
                    return;
                default:
                    return;
            }
        } else {
            switch (extensionDescriptor.getLiteType()) {
                case DOUBLE:
                    writer.writeDouble(extensionDescriptor.getNumber(), ((Double) entry.getValue()).doubleValue());
                    return;
                case FLOAT:
                    writer.writeFloat(extensionDescriptor.getNumber(), ((Float) entry.getValue()).floatValue());
                    return;
                case INT64:
                    writer.writeInt64(extensionDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case UINT64:
                    writer.writeUInt64(extensionDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case INT32:
                    writer.writeInt32(extensionDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case FIXED64:
                    writer.writeFixed64(extensionDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case FIXED32:
                    writer.writeFixed32(extensionDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case BOOL:
                    writer.writeBool(extensionDescriptor.getNumber(), ((Boolean) entry.getValue()).booleanValue());
                    return;
                case UINT32:
                    writer.writeUInt32(extensionDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case SFIXED32:
                    writer.writeSFixed32(extensionDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case SFIXED64:
                    writer.writeSFixed64(extensionDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case SINT32:
                    writer.writeSInt32(extensionDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case SINT64:
                    writer.writeSInt64(extensionDescriptor.getNumber(), ((Long) entry.getValue()).longValue());
                    return;
                case ENUM:
                    writer.writeInt32(extensionDescriptor.getNumber(), ((Integer) entry.getValue()).intValue());
                    return;
                case BYTES:
                    writer.writeBytes(extensionDescriptor.getNumber(), (ByteString) entry.getValue());
                    return;
                case STRING:
                    writer.writeString(extensionDescriptor.getNumber(), (String) entry.getValue());
                    return;
                case GROUP:
                    writer.writeGroup(extensionDescriptor.getNumber(), entry.getValue(), Protobuf.getInstance().schemaFor(entry.getValue().getClass()));
                    return;
                case MESSAGE:
                    writer.writeMessage(extensionDescriptor.getNumber(), entry.getValue(), Protobuf.getInstance().schemaFor(entry.getValue().getClass()));
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public Object findExtensionByNumber(ExtensionRegistryLite extensionRegistryLite, MessageLite messageLite, int i) {
        return extensionRegistryLite.findLiteExtensionByNumber(messageLite, i);
    }

    /* access modifiers changed from: 0000 */
    public void parseLengthPrefixedMessageSetItem(Reader reader, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<ExtensionDescriptor> fieldSet) throws IOException {
        GeneratedExtension generatedExtension = (GeneratedExtension) obj;
        fieldSet.setField(generatedExtension.descriptor, reader.readMessage(generatedExtension.getMessageDefaultInstance().getClass(), extensionRegistryLite));
    }

    /* access modifiers changed from: 0000 */
    public void parseMessageSetItem(ByteString byteString, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<ExtensionDescriptor> fieldSet) throws IOException {
        GeneratedExtension generatedExtension = (GeneratedExtension) obj;
        MessageLite buildPartial = generatedExtension.getMessageDefaultInstance().newBuilderForType().buildPartial();
        BinaryReader newInstance = BinaryReader.newInstance(ByteBuffer.wrap(byteString.toByteArray()), true);
        Protobuf.getInstance().mergeFrom(buildPartial, newInstance, extensionRegistryLite);
        fieldSet.setField(generatedExtension.descriptor, buildPartial);
        if (newInstance.getFieldNumber() != Integer.MAX_VALUE) {
            throw InvalidProtocolBufferException.invalidEndTag();
        }
    }
}
