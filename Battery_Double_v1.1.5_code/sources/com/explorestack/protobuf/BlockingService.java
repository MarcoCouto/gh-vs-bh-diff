package com.explorestack.protobuf;

import com.explorestack.protobuf.Descriptors.MethodDescriptor;
import com.explorestack.protobuf.Descriptors.ServiceDescriptor;

public interface BlockingService {
    Message callBlockingMethod(MethodDescriptor methodDescriptor, RpcController rpcController, Message message) throws ServiceException;

    ServiceDescriptor getDescriptorForType();

    Message getRequestPrototype(MethodDescriptor methodDescriptor);

    Message getResponsePrototype(MethodDescriptor methodDescriptor);
}
