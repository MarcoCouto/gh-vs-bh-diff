package com.explorestack.protobuf;

import com.explorestack.protobuf.Internal.EnumVerifier;
import java.lang.reflect.Field;

final class FieldInfo implements Comparable<FieldInfo> {
    private final Field cachedSizeField;
    private final boolean enforceUtf8;
    private final EnumVerifier enumVerifier;
    private final Field field;
    private final int fieldNumber;
    private final Object mapDefaultEntry;
    private final Class<?> messageClass;
    private final OneofInfo oneof;
    private final Class<?> oneofStoredType;
    private final Field presenceField;
    private final int presenceMask;
    private final boolean required;
    private final FieldType type;

    public static final class Builder {
        private Field cachedSizeField;
        private boolean enforceUtf8;
        private EnumVerifier enumVerifier;
        private Field field;
        private int fieldNumber;
        private Object mapDefaultEntry;
        private OneofInfo oneof;
        private Class<?> oneofStoredType;
        private Field presenceField;
        private int presenceMask;
        private boolean required;
        private FieldType type;

        private Builder() {
        }

        public Builder withField(Field field2) {
            if (this.oneof == null) {
                this.field = field2;
                return this;
            }
            throw new IllegalStateException("Cannot set field when building a oneof.");
        }

        public Builder withType(FieldType fieldType) {
            this.type = fieldType;
            return this;
        }

        public Builder withFieldNumber(int i) {
            this.fieldNumber = i;
            return this;
        }

        public Builder withPresence(Field field2, int i) {
            this.presenceField = (Field) Internal.checkNotNull(field2, "presenceField");
            this.presenceMask = i;
            return this;
        }

        public Builder withOneof(OneofInfo oneofInfo, Class<?> cls) {
            if (this.field == null && this.presenceField == null) {
                this.oneof = oneofInfo;
                this.oneofStoredType = cls;
                return this;
            }
            throw new IllegalStateException("Cannot set oneof when field or presenceField have been provided");
        }

        public Builder withRequired(boolean z) {
            this.required = z;
            return this;
        }

        public Builder withMapDefaultEntry(Object obj) {
            this.mapDefaultEntry = obj;
            return this;
        }

        public Builder withEnforceUtf8(boolean z) {
            this.enforceUtf8 = z;
            return this;
        }

        public Builder withEnumVerifier(EnumVerifier enumVerifier2) {
            this.enumVerifier = enumVerifier2;
            return this;
        }

        public Builder withCachedSizeField(Field field2) {
            this.cachedSizeField = field2;
            return this;
        }

        public FieldInfo build() {
            if (this.oneof != null) {
                return FieldInfo.forOneofMemberField(this.fieldNumber, this.type, this.oneof, this.oneofStoredType, this.enforceUtf8, this.enumVerifier);
            }
            if (this.mapDefaultEntry != null) {
                return FieldInfo.forMapField(this.field, this.fieldNumber, this.mapDefaultEntry, this.enumVerifier);
            }
            if (this.presenceField != null) {
                if (this.required) {
                    return FieldInfo.forProto2RequiredField(this.field, this.fieldNumber, this.type, this.presenceField, this.presenceMask, this.enforceUtf8, this.enumVerifier);
                }
                return FieldInfo.forProto2OptionalField(this.field, this.fieldNumber, this.type, this.presenceField, this.presenceMask, this.enforceUtf8, this.enumVerifier);
            } else if (this.enumVerifier != null) {
                if (this.cachedSizeField == null) {
                    return FieldInfo.forFieldWithEnumVerifier(this.field, this.fieldNumber, this.type, this.enumVerifier);
                }
                return FieldInfo.forPackedFieldWithEnumVerifier(this.field, this.fieldNumber, this.type, this.enumVerifier, this.cachedSizeField);
            } else if (this.cachedSizeField == null) {
                return FieldInfo.forField(this.field, this.fieldNumber, this.type, this.enforceUtf8);
            } else {
                return FieldInfo.forPackedField(this.field, this.fieldNumber, this.type, this.cachedSizeField);
            }
        }
    }

    private static boolean isExactlyOneBitSet(int i) {
        return i != 0 && (i & (i + -1)) == 0;
    }

    public static FieldInfo forField(Field field2, int i, FieldType fieldType, boolean z) {
        FieldType fieldType2 = fieldType;
        checkFieldNumber(i);
        Field field3 = field2;
        Internal.checkNotNull(field2, "field");
        Internal.checkNotNull(fieldType2, "fieldType");
        if (fieldType2 == FieldType.MESSAGE_LIST || fieldType2 == FieldType.GROUP_LIST) {
            throw new IllegalStateException("Shouldn't be called for repeated message fields.");
        }
        FieldInfo fieldInfo = new FieldInfo(field2, i, fieldType, null, null, 0, false, z, null, null, null, null, null);
        return fieldInfo;
    }

    public static FieldInfo forPackedField(Field field2, int i, FieldType fieldType, Field field3) {
        FieldType fieldType2 = fieldType;
        checkFieldNumber(i);
        Field field4 = field2;
        Internal.checkNotNull(field2, "field");
        Internal.checkNotNull(fieldType2, "fieldType");
        if (fieldType2 == FieldType.MESSAGE_LIST || fieldType2 == FieldType.GROUP_LIST) {
            throw new IllegalStateException("Shouldn't be called for repeated message fields.");
        }
        FieldInfo fieldInfo = new FieldInfo(field2, i, fieldType, null, null, 0, false, false, null, null, null, null, field3);
        return fieldInfo;
    }

    public static FieldInfo forRepeatedMessageField(Field field2, int i, FieldType fieldType, Class<?> cls) {
        checkFieldNumber(i);
        Field field3 = field2;
        Internal.checkNotNull(field2, "field");
        FieldType fieldType2 = fieldType;
        Internal.checkNotNull(fieldType2, "fieldType");
        Class<?> cls2 = cls;
        Internal.checkNotNull(cls2, "messageClass");
        FieldInfo fieldInfo = new FieldInfo(field3, i, fieldType2, cls2, null, 0, false, false, null, null, null, null, null);
        return fieldInfo;
    }

    public static FieldInfo forFieldWithEnumVerifier(Field field2, int i, FieldType fieldType, EnumVerifier enumVerifier2) {
        checkFieldNumber(i);
        Field field3 = field2;
        Internal.checkNotNull(field2, "field");
        FieldInfo fieldInfo = new FieldInfo(field3, i, fieldType, null, null, 0, false, false, null, null, null, enumVerifier2, null);
        return fieldInfo;
    }

    public static FieldInfo forPackedFieldWithEnumVerifier(Field field2, int i, FieldType fieldType, EnumVerifier enumVerifier2, Field field3) {
        checkFieldNumber(i);
        Field field4 = field2;
        Internal.checkNotNull(field2, "field");
        FieldInfo fieldInfo = new FieldInfo(field4, i, fieldType, null, null, 0, false, false, null, null, null, enumVerifier2, field3);
        return fieldInfo;
    }

    public static FieldInfo forProto2OptionalField(Field field2, int i, FieldType fieldType, Field field3, int i2, boolean z, EnumVerifier enumVerifier2) {
        Field field4 = field3;
        checkFieldNumber(i);
        Field field5 = field2;
        Internal.checkNotNull(field2, "field");
        Internal.checkNotNull(fieldType, "fieldType");
        Internal.checkNotNull(field4, "presenceField");
        if (field4 == null || isExactlyOneBitSet(i2)) {
            int i3 = i2;
            FieldInfo fieldInfo = new FieldInfo(field2, i, fieldType, null, field3, i2, false, z, null, null, null, enumVerifier2, null);
            return fieldInfo;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("presenceMask must have exactly one bit set: ");
        sb.append(i2);
        throw new IllegalArgumentException(sb.toString());
    }

    public static FieldInfo forOneofMemberField(int i, FieldType fieldType, OneofInfo oneofInfo, Class<?> cls, boolean z, EnumVerifier enumVerifier2) {
        FieldType fieldType2 = fieldType;
        checkFieldNumber(i);
        Internal.checkNotNull(fieldType2, "fieldType");
        Internal.checkNotNull(oneofInfo, "oneof");
        Internal.checkNotNull(cls, "oneofStoredType");
        if (fieldType.isScalar()) {
            FieldInfo fieldInfo = new FieldInfo(null, i, fieldType, null, null, 0, false, z, oneofInfo, cls, null, enumVerifier2, null);
            return fieldInfo;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Oneof is only supported for scalar fields. Field ");
        int i2 = i;
        sb.append(i);
        sb.append(" is of type ");
        sb.append(fieldType2);
        throw new IllegalArgumentException(sb.toString());
    }

    private static void checkFieldNumber(int i) {
        if (i <= 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("fieldNumber must be positive: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public static FieldInfo forProto2RequiredField(Field field2, int i, FieldType fieldType, Field field3, int i2, boolean z, EnumVerifier enumVerifier2) {
        Field field4 = field3;
        checkFieldNumber(i);
        Field field5 = field2;
        Internal.checkNotNull(field2, "field");
        Internal.checkNotNull(fieldType, "fieldType");
        Internal.checkNotNull(field4, "presenceField");
        if (field4 == null || isExactlyOneBitSet(i2)) {
            int i3 = i2;
            FieldInfo fieldInfo = new FieldInfo(field2, i, fieldType, null, field3, i2, true, z, null, null, null, enumVerifier2, null);
            return fieldInfo;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("presenceMask must have exactly one bit set: ");
        sb.append(i2);
        throw new IllegalArgumentException(sb.toString());
    }

    public static FieldInfo forMapField(Field field2, int i, Object obj, EnumVerifier enumVerifier2) {
        Object obj2 = obj;
        Internal.checkNotNull(obj2, "mapDefaultEntry");
        checkFieldNumber(i);
        Field field3 = field2;
        Internal.checkNotNull(field2, "field");
        FieldInfo fieldInfo = new FieldInfo(field3, i, FieldType.MAP, null, null, 0, false, true, null, null, obj2, enumVerifier2, null);
        return fieldInfo;
    }

    private FieldInfo(Field field2, int i, FieldType fieldType, Class<?> cls, Field field3, int i2, boolean z, boolean z2, OneofInfo oneofInfo, Class<?> cls2, Object obj, EnumVerifier enumVerifier2, Field field4) {
        this.field = field2;
        this.type = fieldType;
        this.messageClass = cls;
        this.fieldNumber = i;
        this.presenceField = field3;
        this.presenceMask = i2;
        this.required = z;
        this.enforceUtf8 = z2;
        this.oneof = oneofInfo;
        this.oneofStoredType = cls2;
        this.mapDefaultEntry = obj;
        this.enumVerifier = enumVerifier2;
        this.cachedSizeField = field4;
    }

    public int getFieldNumber() {
        return this.fieldNumber;
    }

    public Field getField() {
        return this.field;
    }

    public FieldType getType() {
        return this.type;
    }

    public OneofInfo getOneof() {
        return this.oneof;
    }

    public Class<?> getOneofStoredType() {
        return this.oneofStoredType;
    }

    public EnumVerifier getEnumVerifier() {
        return this.enumVerifier;
    }

    public int compareTo(FieldInfo fieldInfo) {
        return this.fieldNumber - fieldInfo.fieldNumber;
    }

    public Class<?> getListElementType() {
        return this.messageClass;
    }

    public Field getPresenceField() {
        return this.presenceField;
    }

    public Object getMapDefaultEntry() {
        return this.mapDefaultEntry;
    }

    public int getPresenceMask() {
        return this.presenceMask;
    }

    public boolean isRequired() {
        return this.required;
    }

    public boolean isEnforceUtf8() {
        return this.enforceUtf8;
    }

    public Field getCachedSizeField() {
        return this.cachedSizeField;
    }

    public Class<?> getMessageFieldClass() {
        switch (this.type) {
            case MESSAGE:
            case GROUP:
                return this.field != null ? this.field.getType() : this.oneofStoredType;
            case MESSAGE_LIST:
            case GROUP_LIST:
                return this.messageClass;
            default:
                return null;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
