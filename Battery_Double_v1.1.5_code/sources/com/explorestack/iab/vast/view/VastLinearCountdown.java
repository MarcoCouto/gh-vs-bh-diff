package com.explorestack.iab.vast.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.VisibleForTesting;
import android.util.AttributeSet;
import android.view.View;
import com.explorestack.iab.utils.Assets;

public class VastLinearCountdown extends View {
    private final Runnable invalidateRunnable = new Runnable() {
        public void run() {
            VastLinearCountdown.this.invalidate();
        }
    };
    @VisibleForTesting
    int lineColor = Assets.mainAssetsColor;
    private float lineLength;
    private float lineWidth;
    private Paint paint = new Paint(7);
    private float percent;

    public VastLinearCountdown(Context context) {
        super(context);
        initializeAttributes();
    }

    public VastLinearCountdown(Context context, int i) {
        super(context);
        this.lineColor = i;
        initializeAttributes();
    }

    public VastLinearCountdown(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initializeAttributes();
    }

    private void initializeAttributes() {
        this.percent = 0.0f;
        this.lineWidth = 15.0f;
    }

    public void setLineColor(int i) {
        this.lineColor = i;
    }

    public void setLineWidth(float f) {
        this.lineWidth = f;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.lineLength = (float) i;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.paint.setStrokeWidth(this.lineWidth);
        this.paint.setColor(this.lineColor);
        drawLineLoading(this.paint, canvas);
    }

    private void drawLineLoading(Paint paint2, Canvas canvas) {
        Canvas canvas2 = canvas;
        canvas2.drawLine(0.0f, ((float) getMeasuredHeight()) / 2.0f, (this.lineLength * this.percent) / 100.0f, ((float) getMeasuredHeight()) / 2.0f, paint2);
    }

    public void changePercentage(float f) {
        this.percent = f;
        post(this.invalidateRunnable);
    }
}
