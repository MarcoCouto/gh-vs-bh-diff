package com.explorestack.iab.vast.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.support.annotation.VisibleForTesting;
import android.util.AttributeSet;
import android.view.View;
import com.explorestack.iab.utils.Assets;

public class CircleCountdownView extends View {
    @VisibleForTesting
    int arcLoadingBackgroundColor = Assets.backgroundColor;
    private float arcLoadingStartAngle;
    private float arcLoadingStrokeWidth;
    private Bitmap bitmap;
    private float circleCenterPointX;
    private float circleCenterPointY;
    @VisibleForTesting
    int innerSize;
    @VisibleForTesting
    int mainColor = Assets.mainAssetsColor;
    private int paddingInContainer;
    private int percent;
    private int remainingTime;
    @VisibleForTesting
    Bitmap resource;

    public CircleCountdownView(Context context) {
        super(context);
        initializeAttributes();
    }

    public CircleCountdownView(Context context, int i, int i2) {
        super(context);
        this.mainColor = i;
        this.arcLoadingBackgroundColor = i2;
        initializeAttributes();
    }

    public CircleCountdownView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initializeAttributes();
    }

    public void setMainColor(int i) {
        this.mainColor = i;
    }

    public void setArcBackgroundColor(int i) {
        this.arcLoadingBackgroundColor = i;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.circleCenterPointX = ((float) i) / 2.0f;
        this.circleCenterPointY = ((float) i2) / 2.0f;
        this.paddingInContainer = Math.max(i, i2) / 4;
        double d = (double) (this.circleCenterPointX - ((float) this.paddingInContainer));
        double sqrt = Math.sqrt(2.0d);
        Double.isNaN(d);
        this.innerSize = (int) (d * sqrt);
        if (this.resource != null && this.innerSize > 0) {
            this.bitmap = Bitmap.createScaledBitmap(this.resource, this.innerSize, this.innerSize, true);
            fillBitmap();
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.remainingTime != 0 || this.bitmap != null) {
            drawArcLoading(canvas);
            if (this.bitmap != null) {
                drawBitmap(canvas);
            } else {
                drawText(canvas);
            }
        }
    }

    private void initializeAttributes() {
        setLayerType(1, null);
        this.circleCenterPointX = 54.0f;
        this.circleCenterPointY = 54.0f;
        this.arcLoadingStrokeWidth = 5.0f;
        this.percent = 100;
        this.arcLoadingStartAngle = 270.0f;
    }

    private void drawArcLoading(Canvas canvas) {
        float f = 360.0f - (((float) (this.percent * 360)) * 0.01f);
        RectF rectF = new RectF((float) this.paddingInContainer, (float) this.paddingInContainer, (this.circleCenterPointX * 2.0f) - ((float) this.paddingInContainer), (this.circleCenterPointX * 2.0f) - ((float) this.paddingInContainer));
        Paint paint = new Paint(7);
        paint.setColor(this.arcLoadingBackgroundColor);
        paint.setStyle(Style.FILL);
        canvas.drawArc(rectF, 0.0f, 360.0f, false, paint);
        paint.setColor(this.mainColor);
        paint.setStyle(Style.STROKE);
        paint.setStrokeWidth(this.arcLoadingStrokeWidth);
        canvas.drawArc(rectF, this.arcLoadingStartAngle, f, false, paint);
    }

    private void drawBitmap(Canvas canvas) {
        int i = (int) (this.circleCenterPointX - ((float) (this.innerSize / 2)));
        int i2 = (int) (this.circleCenterPointY - ((float) (this.innerSize / 2)));
        Paint paint = new Paint(7);
        paint.setStyle(Style.FILL);
        paint.setAntiAlias(true);
        canvas.drawBitmap(this.bitmap, (float) i, (float) i2, paint);
    }

    private void drawText(Canvas canvas) {
        String valueOf = String.valueOf(this.remainingTime);
        Paint paint = new Paint(7);
        paint.setColor(this.mainColor);
        paint.setTextSize((float) this.innerSize);
        paint.setStyle(Style.FILL);
        paint.setAntiAlias(true);
        paint.setTextAlign(Align.CENTER);
        canvas.drawText(valueOf, (float) (getMeasuredWidth() / 2), (float) ((int) (((float) (getMeasuredHeight() / 2)) - ((paint.descent() + paint.ascent()) / 2.0f))), paint);
    }

    public void changePercentage(int i, int i2) {
        if (this.bitmap == null || i == 100) {
            this.percent = i;
            this.remainingTime = i2;
            postInvalidate();
        }
    }

    public void setImage(Bitmap bitmap2) {
        if (bitmap2 == null || this.innerSize <= 0) {
            this.resource = bitmap2;
            this.bitmap = bitmap2;
        } else {
            this.resource = bitmap2;
            this.bitmap = Bitmap.createScaledBitmap(bitmap2, this.innerSize, this.innerSize, true);
            fillBitmap();
            this.percent = 100;
        }
        postInvalidate();
    }

    private void fillBitmap() {
        Paint paint = new Paint();
        paint.setColorFilter(new LightingColorFilter(this.mainColor, 0));
        new Canvas(this.bitmap).drawBitmap(this.bitmap, 0.0f, 0.0f, paint);
    }
}
