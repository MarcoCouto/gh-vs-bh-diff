package com.explorestack.iab.vast.activity;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.explorestack.iab.mraid.MRAIDInterstitial;
import com.explorestack.iab.mraid.MRAIDInterstitialListener;
import com.explorestack.iab.mraid.MRAIDNativeFeatureListener;
import com.explorestack.iab.utils.Assets;
import com.explorestack.iab.utils.CircularProgressBar;
import com.explorestack.iab.utils.Utils;
import com.explorestack.iab.vast.TrackingEvent;
import com.explorestack.iab.vast.VastClickCallback;
import com.explorestack.iab.vast.VastError;
import com.explorestack.iab.vast.VastExtension;
import com.explorestack.iab.vast.VastHelper;
import com.explorestack.iab.vast.VastHelper.OnScreenStateChangeListener;
import com.explorestack.iab.vast.VastLog;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.VideoType;
import com.explorestack.iab.vast.processor.VastAd;
import com.explorestack.iab.vast.tags.AppodealExtensionTag;
import com.explorestack.iab.vast.tags.CompanionTag;
import com.explorestack.iab.vast.view.CircleCountdownView;
import com.explorestack.iab.vast.view.VastLinearCountdown;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class VastView extends FrameLayout implements VastClickCallback {
    private static final int DEFAULT_VIDEO_SKIP_OFFSET = 5;
    @VisibleForTesting
    static final String LEARN_MORE_TEXT = "Learn more";
    private static final int MAX_PROGRESS_TRACKING_POINTS = 20;
    private static final int PROGRESS_SIZE_DP = 3;
    private static final long TIMER_INTERVAL = 16;
    private static final Pair<Integer, Integer> bannerAlignment = Pair.create(Integer.valueOf(14), Integer.valueOf(12));
    private static final Pair<Integer, Integer> closeViewAlignment = Pair.create(Integer.valueOf(11), Integer.valueOf(10));
    private static final Pair<Integer, Integer> ctaViewAlignment = Pair.create(Integer.valueOf(12), Integer.valueOf(11));
    private static final Pair<Integer, Integer> muteViewAlignment = Pair.create(Integer.valueOf(9), Integer.valueOf(10));
    private static final Pair<Integer, Integer> repeatViewAlignment = Pair.create(Integer.valueOf(9), Integer.valueOf(15));
    /* access modifiers changed from: private */
    public final String TAG;
    private int assetsBackgroundColor;
    private int assetsColor;
    @Nullable
    @VisibleForTesting
    CompanionTag bannerTag;
    private final OnTouchListener bannerTouchListener;
    @Nullable
    @VisibleForTesting
    WebView bannerView;
    private final WebChromeClient bannerWebChromeClient;
    private final WebViewClient bannerWebViewClient;
    @VisibleForTesting
    @NonNull
    CircleCountdownView closeView;
    @Nullable
    @VisibleForTesting
    MRAIDInterstitial companionInterstitial;
    private int companionOrientation;
    @Nullable
    @VisibleForTesting
    CompanionTag companionTag;
    @Nullable
    @VisibleForTesting
    ImageView companionView;
    @Nullable
    @VisibleForTesting
    TextView ctaView;
    @Nullable
    private MediaFrameRetriever currentMediaFrameRetriever;
    @Nullable
    @VisibleForTesting
    Surface currentSurface;
    /* access modifiers changed from: private */
    public boolean isMediaPlayerPrepared;
    /* access modifiers changed from: private */
    public boolean isOverlayHidePending;
    private boolean isReceivePlaybackError;
    private boolean isShowingCompanion;
    /* access modifiers changed from: private */
    public boolean isTextureCreated;
    /* access modifiers changed from: private */
    public boolean isWaitingSurface;
    private boolean isWaitingWindowFocus;
    private boolean isWindowFocused;
    @Nullable
    private VastViewListener listener;
    @Nullable
    @VisibleForTesting
    MediaPlayer mediaPlayer;
    @Nullable
    @VisibleForTesting
    CircleCountdownView muteView;
    /* access modifiers changed from: private */
    public final AnimatorListener overlayAnimatorListener;
    private final Runnable overlayHideRunnable;
    @VisibleForTesting
    @NonNull
    RelativeLayout overlayView;
    private final Runnable playbackTrackingRunnable;
    private final OnCompletionListener playerCompletionListener;
    private final OnErrorListener playerErrorListener;
    private final OnPreparedListener playerPreparedListener;
    private final OnVideoSizeChangedListener playerVideoSizeChangedListener;
    /* access modifiers changed from: private */
    public float previousProgressPercent;
    @VisibleForTesting
    @NonNull
    CircularProgressBar progressBar;
    /* access modifiers changed from: private */
    public int progressTrackingErrorCount;
    /* access modifiers changed from: private */
    public final TimeUpdateHandler progressUpdateHandler;
    @Nullable
    @VisibleForTesting
    VastLinearCountdown progressView;
    /* access modifiers changed from: private */
    public final TimeUpdateHandler quartileUpdateHandler;
    @Nullable
    @VisibleForTesting
    CircleCountdownView repeatView;
    private OnScreenStateChangeListener screenStateChangeListener;
    private final Runnable sizeChangeRunnable;
    /* access modifiers changed from: private */
    public final TimeUpdateHandler skipTimeUpdateHandler;
    private final SurfaceTextureListener textureListener;
    @VisibleForTesting
    @NonNull
    TextureView textureView;
    private int topPanelHeight;
    /* access modifiers changed from: private */
    public final List<View> touchedWebViews;
    @Nullable
    @VisibleForTesting
    VastRequest vastRequest;
    /* access modifiers changed from: private */
    public int videoHeight;
    private int videoOrientation;
    /* access modifiers changed from: private */
    public final LinkedList<Integer> videoProgressTracker;
    /* access modifiers changed from: private */
    public int videoWidth;
    @VisibleForTesting
    @NonNull
    VastViewState viewState;

    private final class CompanionInterstitialListener implements MRAIDInterstitialListener, MRAIDNativeFeatureListener {
        public void mraidInterstitialNoFill(MRAIDInterstitial mRAIDInterstitial) {
        }

        public void mraidInterstitialShow(MRAIDInterstitial mRAIDInterstitial) {
        }

        public void mraidNativeFeatureCallTel(String str) {
        }

        public void mraidNativeFeatureCreateCalendarEvent(String str) {
        }

        public void mraidNativeFeaturePlayVideo(String str) {
        }

        public void mraidNativeFeatureSendSms(String str) {
        }

        public void mraidNativeFeatureStorePicture(String str) {
        }

        private CompanionInterstitialListener() {
        }

        public void mraidInterstitialLoaded(MRAIDInterstitial mRAIDInterstitial) {
            if (VastView.this.viewState.isCompanionShown) {
                mRAIDInterstitial.show(VastView.this.getId());
            }
        }

        public void mraidInterstitialHide(MRAIDInterstitial mRAIDInterstitial) {
            VastView.this.handleCompanionClose();
        }

        public void mraidNativeFeatureOpenBrowser(String str, WebView webView) {
            VastView.this.processClickThroughEvent(VastView.this.companionTag, str);
        }
    }

    private static abstract class MediaFrameRetriever extends Thread {
        private static final String TAG = "MediaFrameRetriever";
        private WeakReference<Context> contextReference;
        private Uri fileUri;
        private boolean isCanceled;
        private String networkUrl;
        /* access modifiers changed from: private */
        public Bitmap result;

        /* access modifiers changed from: 0000 */
        public abstract void onObtained(@Nullable Bitmap bitmap);

        MediaFrameRetriever(@NonNull Context context, @Nullable Uri uri, @Nullable String str) {
            this.contextReference = new WeakReference<>(context);
            this.fileUri = uri;
            this.networkUrl = str;
            if (str != null || (uri != null && !TextUtils.isEmpty(uri.getPath()) && new File(uri.getPath()).exists())) {
                start();
            } else {
                onObtained(null);
            }
        }

        public void run() {
            Context context = (Context) this.contextReference.get();
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            if (context != null) {
                try {
                    if (this.fileUri != null) {
                        mediaMetadataRetriever.setDataSource(context, this.fileUri);
                    } else if (this.networkUrl != null) {
                        mediaMetadataRetriever.setDataSource(this.networkUrl, new HashMap());
                    }
                    this.result = mediaMetadataRetriever.getFrameAtTime((Long.parseLong(mediaMetadataRetriever.extractMetadata(9)) / 2) * 1000, 2);
                } catch (Exception e) {
                    VastLog.e(TAG, e.getMessage());
                }
            }
            mediaMetadataRetriever.release();
            if (!this.isCanceled) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        MediaFrameRetriever.this.onObtained(MediaFrameRetriever.this.result);
                    }
                });
            }
        }

        /* access modifiers changed from: 0000 */
        public void cancel() {
            this.isCanceled = true;
        }
    }

    static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        VastRequest vastRequest;
        VastViewState vastViewState;

        SavedState(Parcel parcel) {
            super(parcel);
            this.vastViewState = (VastViewState) parcel.readParcelable(VastViewState.class.getClassLoader());
            this.vastRequest = (VastRequest) parcel.readParcelable(VastRequest.class.getClassLoader());
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.vastViewState, 0);
            parcel.writeParcelable(this.vastRequest, 0);
        }
    }

    private interface TimeUpdateHandler {
        void update(int i, int i2, float f);
    }

    public interface VastViewListener {
        void onClick(@NonNull VastView vastView, @NonNull VastRequest vastRequest, @NonNull VastClickCallback vastClickCallback, @Nullable String str);

        void onComplete(@NonNull VastView vastView, @NonNull VastRequest vastRequest);

        void onError(@NonNull VastView vastView, @Nullable VastRequest vastRequest, int i);

        void onFinish(@NonNull VastView vastView, @NonNull VastRequest vastRequest, boolean z);

        void onOrientationRequested(@NonNull VastView vastView, @NonNull VastRequest vastRequest, int i);

        void onShown(@NonNull VastView vastView, @NonNull VastRequest vastRequest);
    }

    @VisibleForTesting
    static class VastViewState implements Parcelable {
        public static final Creator<VastViewState> CREATOR = new Creator<VastViewState>() {
            public VastViewState createFromParcel(Parcel parcel) {
                return new VastViewState(parcel);
            }

            public VastViewState[] newArray(int i) {
                return new VastViewState[i];
            }
        };
        int currentQuartile;
        int currentVideoPosition;
        boolean isCompanionShown;
        boolean isCompleted;
        boolean isImpressionProcessed;
        boolean isMuted;
        boolean isPaused;
        boolean isSkipEnabled;
        int skipTime;

        public int describeContents() {
            return 0;
        }

        VastViewState() {
            this.skipTime = 5;
            this.currentQuartile = 0;
            this.currentVideoPosition = 0;
            this.isMuted = false;
            this.isPaused = false;
            this.isCompleted = false;
            this.isSkipEnabled = false;
            this.isCompanionShown = false;
            this.isImpressionProcessed = false;
        }

        VastViewState(Parcel parcel) {
            this.skipTime = 5;
            boolean z = false;
            this.currentQuartile = 0;
            this.currentVideoPosition = 0;
            this.isMuted = false;
            this.isPaused = false;
            this.isCompleted = false;
            this.isSkipEnabled = false;
            this.isCompanionShown = false;
            this.isImpressionProcessed = false;
            this.skipTime = parcel.readInt();
            this.currentQuartile = parcel.readInt();
            this.currentVideoPosition = parcel.readInt();
            this.isMuted = parcel.readByte() != 0;
            this.isPaused = parcel.readByte() != 0;
            this.isCompleted = parcel.readByte() != 0;
            this.isSkipEnabled = parcel.readByte() != 0;
            this.isCompanionShown = parcel.readByte() != 0;
            if (parcel.readByte() != 0) {
                z = true;
            }
            this.isImpressionProcessed = z;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.skipTime);
            parcel.writeInt(this.currentQuartile);
            parcel.writeInt(this.currentVideoPosition);
            parcel.writeByte(this.isMuted ? (byte) 1 : 0);
            parcel.writeByte(this.isPaused ? (byte) 1 : 0);
            parcel.writeByte(this.isCompleted ? (byte) 1 : 0);
            parcel.writeByte(this.isSkipEnabled ? (byte) 1 : 0);
            parcel.writeByte(this.isCompanionShown ? (byte) 1 : 0);
            parcel.writeByte(this.isImpressionProcessed ? (byte) 1 : 0);
        }
    }

    public VastView(@NonNull Context context) {
        this(context, null);
    }

    public VastView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public VastView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        StringBuilder sb = new StringBuilder();
        sb.append("VASTView-");
        sb.append(Integer.toHexString(hashCode()));
        this.TAG = sb.toString();
        this.viewState = new VastViewState();
        this.assetsColor = Assets.mainAssetsColor;
        this.assetsBackgroundColor = Assets.backgroundColor;
        this.videoOrientation = 2;
        this.companionOrientation = 2;
        this.isWindowFocused = false;
        this.isWaitingWindowFocus = false;
        this.isTextureCreated = false;
        this.isWaitingSurface = false;
        this.isShowingCompanion = false;
        this.isMediaPlayerPrepared = false;
        this.isReceivePlaybackError = false;
        this.touchedWebViews = new ArrayList();
        this.sizeChangeRunnable = new Runnable() {
            public void run() {
                if (VastView.this.isPlaybackStarted()) {
                    VastView.this.configureVideoSurface();
                }
            }
        };
        this.isOverlayHidePending = false;
        this.overlayHideRunnable = new Runnable() {
            public void run() {
                VastView.this.isOverlayHidePending = false;
                if (VastView.this.overlayView.getVisibility() == 0 && !VastView.this.viewState.isCompanionShown) {
                    ViewPropertyAnimator listener = VastView.this.overlayView.animate().alpha(0.0f).setDuration(400).setListener(VastView.this.overlayAnimatorListener);
                    if (VERSION.SDK_INT >= 16) {
                        listener.withLayer();
                    }
                }
            }
        };
        this.overlayAnimatorListener = new AnimatorListenerAdapter() {
            public void onAnimationCancel(Animator animator) {
                super.onAnimationCancel(animator);
                animator.removeAllListeners();
            }

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                if (!VastView.this.viewState.isCompanionShown) {
                    VastView.this.overlayView.setVisibility(8);
                } else {
                    VastView.this.overlayView.setAlpha(1.0f);
                }
            }
        };
        this.playbackTrackingRunnable = new Runnable() {
            public void run() {
                try {
                    if (VastView.this.isPlaybackStarted() && VastView.this.mediaPlayer.isPlaying()) {
                        int duration = VastView.this.mediaPlayer.getDuration();
                        int currentPosition = VastView.this.mediaPlayer.getCurrentPosition();
                        if (currentPosition > 0) {
                            float f = (((float) currentPosition) * 100.0f) / ((float) duration);
                            VastView.this.skipTimeUpdateHandler.update(duration, currentPosition, f);
                            VastView.this.quartileUpdateHandler.update(duration, currentPosition, f);
                            VastView.this.progressUpdateHandler.update(duration, currentPosition, f);
                            if (f > 105.0f) {
                                VastLog.e(VastView.this.TAG, "Playback tracking: video hang detected");
                                VastView.this.handleComplete();
                            }
                        }
                    }
                } catch (Exception e) {
                    String access$1200 = VastView.this.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Playback tracking exception: ");
                    sb.append(e.getMessage());
                    VastLog.e(access$1200, sb.toString());
                }
                VastView.this.postDelayed(this, 16);
            }
        };
        this.skipTimeUpdateHandler = new TimeUpdateHandler() {
            public void update(int i, int i2, float f) {
                if (!VastView.this.viewState.isSkipEnabled && VastView.this.viewState.skipTime != 0 && VastView.this.vastRequest.getVideoType() == VideoType.NonRewarded) {
                    int i3 = (VastView.this.viewState.skipTime * 1000) - i2;
                    int i4 = (int) ((((float) i2) * 100.0f) / ((float) (VastView.this.viewState.skipTime * 1000)));
                    String access$1200 = VastView.this.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Skip percent: ");
                    sb.append(i4);
                    VastLog.d(access$1200, sb.toString());
                    if (i4 < 100) {
                        VastView.this.closeView.setImage(null);
                        CircleCountdownView circleCountdownView = VastView.this.closeView;
                        double d = (double) i3;
                        Double.isNaN(d);
                        circleCountdownView.changePercentage(i4, (int) Math.ceil(d / 1000.0d));
                    }
                    if (i3 <= 0) {
                        VastView.this.viewState.skipTime = 0;
                        VastView.this.viewState.isSkipEnabled = true;
                        VastView.this.closeView.setImage(Assets.getBitmapFromBase64(Assets.close));
                        VastView.this.setCloseViewVisibility(true);
                    }
                }
            }
        };
        this.quartileUpdateHandler = new TimeUpdateHandler() {
            public void update(int i, int i2, float f) {
                if (!VastView.this.viewState.isCompleted || VastView.this.viewState.currentQuartile != 3) {
                    if (VastView.this.vastRequest.getMaxDurationMillis() > 0 && i2 > VastView.this.vastRequest.getMaxDurationMillis() && VastView.this.vastRequest.getVideoType() == VideoType.Rewarded) {
                        VastView.this.closeView.changePercentage(100, 0);
                        VastView.this.setCloseViewVisibility(true);
                        VastView.this.viewState.isSkipEnabled = true;
                    }
                    if (f > ((float) VastView.this.viewState.currentQuartile) * 25.0f) {
                        if (VastView.this.viewState.currentQuartile == 3) {
                            String access$1200 = VastView.this.TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Video at third quartile: (");
                            sb.append(f);
                            sb.append("%)");
                            VastLog.d(access$1200, sb.toString());
                            VastView.this.trackEvent(TrackingEvent.thirdQuartile);
                        } else if (VastView.this.viewState.currentQuartile == 0) {
                            String access$12002 = VastView.this.TAG;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Video at start: (");
                            sb2.append(f);
                            sb2.append("%)");
                            VastLog.d(access$12002, sb2.toString());
                            VastView.this.trackEvent(TrackingEvent.start);
                        } else if (VastView.this.viewState.currentQuartile == 1) {
                            String access$12003 = VastView.this.TAG;
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Video at first quartile: (");
                            sb3.append(f);
                            sb3.append("%)");
                            VastLog.d(access$12003, sb3.toString());
                            VastView.this.trackEvent(TrackingEvent.firstQuartile);
                        } else if (VastView.this.viewState.currentQuartile == 2) {
                            String access$12004 = VastView.this.TAG;
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("Video at midpoint: (");
                            sb4.append(f);
                            sb4.append("%)");
                            VastLog.d(access$12004, sb4.toString());
                            VastView.this.trackEvent(TrackingEvent.midpoint);
                        }
                        VastView.this.viewState.currentQuartile++;
                    }
                }
            }
        };
        this.videoProgressTracker = new LinkedList<>();
        this.progressTrackingErrorCount = 0;
        this.previousProgressPercent = 0.0f;
        this.progressUpdateHandler = new TimeUpdateHandler() {
            public void update(int i, int i2, float f) {
                if (VastView.this.videoProgressTracker.size() == 2 && ((Integer) VastView.this.videoProgressTracker.getFirst()).intValue() > ((Integer) VastView.this.videoProgressTracker.getLast()).intValue()) {
                    VastLog.e(VastView.this.TAG, "Playing progressing error: seek");
                    VastView.this.videoProgressTracker.removeFirst();
                }
                if (VastView.this.videoProgressTracker.size() == 19) {
                    int intValue = ((Integer) VastView.this.videoProgressTracker.getFirst()).intValue();
                    int intValue2 = ((Integer) VastView.this.videoProgressTracker.getLast()).intValue();
                    VastLog.d(VastView.this.TAG, String.format(Locale.ENGLISH, "Playing progressing position: last=%d, first=%d)", new Object[]{Integer.valueOf(intValue2), Integer.valueOf(intValue)}));
                    if (intValue2 > intValue) {
                        VastView.this.videoProgressTracker.removeFirst();
                    } else {
                        VastView.this.progressTrackingErrorCount = VastView.this.progressTrackingErrorCount + 1;
                        if (VastView.this.progressTrackingErrorCount >= 3) {
                            VastLog.e(VastView.this.TAG, "Playing progressing error: video hang detected");
                            VastView.this.handlePlaybackError();
                            return;
                        }
                    }
                }
                try {
                    VastView.this.videoProgressTracker.addLast(Integer.valueOf(i2));
                    AppodealExtensionTag appodealExtension = VastView.this.vastRequest != null ? VastView.this.vastRequest.getVastAd().getAppodealExtension() : null;
                    if (i != 0 && i2 > 0 && (appodealExtension == null || appodealExtension.isShowProgress())) {
                        String access$1200 = VastView.this.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Playing progressing percent: ");
                        sb.append(f);
                        VastLog.d(access$1200, sb.toString());
                        if (VastView.this.previousProgressPercent < f) {
                            VastView.this.previousProgressPercent = f;
                            VastView.this.progressView.changePercentage(f);
                            VastView.this.progressView.setVisibility(0);
                        }
                    }
                } catch (Exception unused) {
                }
            }
        };
        this.textureListener = new SurfaceTextureListener() {
            public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
            }

            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
                VastLog.d(VastView.this.TAG, "onSurfaceTextureAvailable");
                VastView.this.currentSurface = new Surface(surfaceTexture);
                VastView.this.isTextureCreated = true;
                if (VastView.this.isWaitingSurface) {
                    VastView.this.isWaitingSurface = false;
                    VastView.this.startPlayback("onSurfaceTextureAvailable");
                } else if (VastView.this.isPlaybackStarted()) {
                    VastView.this.mediaPlayer.setSurface(VastView.this.currentSurface);
                    VastView.this.resumePlayback();
                }
            }

            public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
                String access$1200 = VastView.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("onSurfaceTextureSizeChanged: ");
                sb.append(i);
                sb.append("/");
                sb.append(i2);
                VastLog.d(access$1200, sb.toString());
            }

            public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                VastLog.d(VastView.this.TAG, "onSurfaceTextureDestroyed");
                VastView.this.currentSurface = null;
                VastView.this.isTextureCreated = false;
                if (VastView.this.isPlaybackStarted()) {
                    VastView.this.mediaPlayer.setSurface(null);
                    VastView.this.pausePlayback();
                }
                return false;
            }
        };
        this.playerCompletionListener = new OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                VastLog.d(VastView.this.TAG, "MediaPlayer - onCompletion");
                VastView.this.handleComplete();
            }
        };
        this.playerErrorListener = new OnErrorListener() {
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                String access$1200 = VastView.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("MediaPlayer - onError: what=");
                sb.append(i);
                sb.append(", extra=");
                sb.append(i2);
                VastLog.d(access$1200, sb.toString());
                VastView.this.handlePlaybackError();
                return true;
            }
        };
        this.playerPreparedListener = new OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                VastLog.d(VastView.this.TAG, "MediaPlayer - onPrepared");
                if (!VastView.this.viewState.isCompanionShown) {
                    VastView.this.trackEvent(TrackingEvent.creativeView);
                    VastView.this.trackEvent(TrackingEvent.fullscreen);
                    VastView.this.syncOverlayView();
                    VastView.this.setProgressBarVisibility(false);
                    VastView.this.isMediaPlayerPrepared = true;
                    if (!VastView.this.viewState.isPaused) {
                        mediaPlayer.start();
                        VastView.this.startPlaybackTracking();
                    }
                    VastView.this.syncMuteView();
                    if (VastView.this.viewState.currentVideoPosition > 0) {
                        mediaPlayer.seekTo(VastView.this.viewState.currentVideoPosition);
                        VastView.this.trackEvent(TrackingEvent.resume);
                    }
                    if (!VastView.this.viewState.isImpressionProcessed) {
                        VastView.this.handleImpressions();
                    }
                }
            }
        };
        this.playerVideoSizeChangedListener = new OnVideoSizeChangedListener() {
            public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
                VastLog.d(VastView.this.TAG, "onVideoSizeChanged");
                VastView.this.videoWidth = i;
                VastView.this.videoHeight = i2;
                VastView.this.configureVideoSurface();
            }
        };
        this.screenStateChangeListener = new OnScreenStateChangeListener() {
            public void onScreenStateChange(boolean z) {
                VastView.this.syncPlayback();
            }
        };
        this.bannerTouchListener = new OnTouchListener() {
            @SuppressLint({"ClickableViewAccessibility"})
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                    case 1:
                        VastView.this.touchedWebViews.add(view);
                        if (!view.hasFocus()) {
                            view.requestFocus();
                            break;
                        }
                        break;
                }
                return false;
            }
        };
        this.bannerWebChromeClient = new WebChromeClient() {
            public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                VastLog.d("JS alert", str2);
                return handlePopups(jsResult);
            }

            public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
                VastLog.d("JS confirm", str2);
                return handlePopups(jsResult);
            }

            public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
                VastLog.d("JS prompt", str2);
                return handlePopups(jsPromptResult);
            }

            private boolean handlePopups(JsResult jsResult) {
                jsResult.cancel();
                return true;
            }
        };
        this.bannerWebViewClient = new WebViewClient() {
            public void onPageFinished(WebView webView, String str) {
                webView.setBackgroundColor(0);
                webView.setLayerType(1, null);
            }

            @TargetApi(24)
            public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
                if (webResourceRequest.hasGesture()) {
                    VastView.this.touchedWebViews.add(webView);
                }
                return shouldOverrideUrlLoading(webView, webResourceRequest.getUrl().toString());
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (VastView.this.touchedWebViews.contains(webView)) {
                    VastLog.d(VastView.this.TAG, "banner clicked");
                    VastView.this.processClickThroughEvent(VastView.this.bannerTag, str);
                }
                return true;
            }
        };
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (VastView.this.isPlaybackStarted() || VastView.this.viewState.isCompanionShown) {
                    VastView.this.showOverlay(0);
                }
            }
        });
        this.topPanelHeight = Utils.dpToPx(context, 50.0f);
        this.textureView = new TextureView(context);
        this.textureView.setSurfaceTextureListener(this.textureListener);
        addView(this.textureView, new LayoutParams(-1, -1, 17));
        this.overlayView = new RelativeLayout(context);
        this.overlayView.setPadding(0, 0, 0, 0);
        this.overlayView.setBackgroundColor(0);
        this.overlayView.setVisibility(8);
        this.closeView = new CircleCountdownView(context, this.assetsColor, this.assetsBackgroundColor);
        this.closeView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                VastView.this.handleBackPress();
            }
        });
        this.overlayView.addView(this.closeView);
        addView(this.overlayView, new ViewGroup.LayoutParams(-1, -1));
        this.progressBar = new CircularProgressBar(context);
        this.progressBar.setColorSchemeColors(this.assetsColor);
        this.progressBar.setProgressBackgroundColor(this.assetsBackgroundColor);
        this.progressBar.setVisibility(8);
        addView(this.progressBar, new LayoutParams(-2, -2, 17));
    }

    public boolean display(@Nullable VastRequest vastRequest2) {
        return display(vastRequest2, false);
    }

    private boolean display(@Nullable VastRequest vastRequest2, boolean z) {
        stopPlayback();
        if (!z) {
            this.viewState = new VastViewState();
        }
        if (!Utils.isNetworkAvailable(getContext())) {
            this.vastRequest = null;
            handleClose();
            VastLog.e(this.TAG, "vastRequest.getVastAd() is null. Stop playing...");
            return false;
        }
        this.vastRequest = vastRequest2;
        if (vastRequest2 == null || vastRequest2.getVastAd() == null) {
            handleClose();
            VastLog.e(this.TAG, "vastRequest.getVastAd() is null. Stop playing...");
            return false;
        }
        VastAd vastAd = vastRequest2.getVastAd();
        AppodealExtensionTag appodealExtension = vastAd.getAppodealExtension();
        this.videoOrientation = vastRequest2.getPreferredVideoOrientation();
        if (appodealExtension != null) {
            if (appodealExtension.isShowCta()) {
                this.bannerTag = appodealExtension.getCompanionTag();
            }
            this.assetsColor = appodealExtension.getAssetsColor();
            this.assetsBackgroundColor = appodealExtension.getAssetsBackgroundColor();
        } else {
            this.bannerTag = null;
            this.assetsColor = Assets.mainAssetsColor;
            this.assetsBackgroundColor = Assets.backgroundColor;
        }
        if (this.bannerTag == null) {
            this.bannerTag = vastAd.getBanner(getContext());
        }
        configureBannerView(vastAd.getAppodealExtension());
        configureOverlayView(appodealExtension);
        configureCtaView(appodealExtension, this.bannerView != null);
        configureCloseView(vastRequest2, appodealExtension);
        configureMuteView(appodealExtension);
        configureRepeatView(appodealExtension);
        configureProgressView(appodealExtension);
        this.progressBar.setColorSchemeColors(this.assetsColor);
        this.progressBar.setProgressBackgroundColor(this.assetsBackgroundColor);
        if (this.viewState.isSkipEnabled) {
            this.closeView.changePercentage(100, 0);
        }
        if (this.listener != null) {
            this.listener.onOrientationRequested(this, vastRequest2, this.viewState.isCompanionShown ? this.companionOrientation : this.videoOrientation);
        }
        if (!z) {
            this.viewState.skipTime = vastAd.getSkipOffsetSec() > 0 ? vastAd.getSkipOffsetSec() : 5;
            if (this.listener != null) {
                this.listener.onShown(this, vastRequest2);
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("load (restoring: ");
        sb.append(z);
        sb.append(")");
        startPlayback(sb.toString());
        return true;
    }

    private void configureOverlayView(@Nullable VastExtension vastExtension) {
        cancelOverlayHiding();
        if (vastExtension == null || !vastExtension.isVideoClickable()) {
            this.overlayView.setVisibility(8);
            this.overlayView.setOnClickListener(null);
            this.overlayView.setClickable(false);
            return;
        }
        this.overlayView.setVisibility(0);
        this.overlayView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                VastView.this.handleInfoClicked();
            }
        });
    }

    private void configureCtaView(@Nullable VastExtension vastExtension, boolean z) {
        Pair pair;
        if (!z && (vastExtension == null || vastExtension.isShowCta())) {
            int dpToPx = Utils.dpToPx(getContext(), 3.0f);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(dpToPx, dpToPx, dpToPx, dpToPx);
            if (this.ctaView == null) {
                this.ctaView = new TextView(getContext());
                this.ctaView.setTextSize(15.0f);
                this.ctaView.setVisibility(0);
                this.ctaView.setGravity(16);
                this.ctaView.setShadowLayer(6.0f, 0.0f, 0.0f, Assets.shadowColor);
                this.ctaView.setBackgroundDrawable(createButtonBackground());
                this.ctaView.setPadding(30, 10, 30, 10);
                this.ctaView.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        VastView.this.handleInfoClicked();
                    }
                });
                this.overlayView.addView(this.ctaView);
            } else {
                this.ctaView.setVisibility(0);
            }
            String str = null;
            if (vastExtension != null) {
                str = vastExtension.getCtaText();
                pair = vastExtension.getCtaPosition();
            } else {
                pair = null;
            }
            applyAlignment(pair, ctaViewAlignment, layoutParams);
            this.ctaView.setTextColor(this.assetsColor);
            TextView textView = this.ctaView;
            if (str == null) {
                str = LEARN_MORE_TEXT;
            }
            textView.setText(str);
            this.ctaView.setLayoutParams(layoutParams);
        } else if (this.ctaView != null) {
            this.overlayView.removeView(this.ctaView);
        }
    }

    private void configureCloseView(@NonNull VastRequest vastRequest2, @Nullable VastExtension vastExtension) {
        this.closeView.setMainColor(this.assetsColor);
        this.closeView.setArcBackgroundColor(this.assetsBackgroundColor);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.topPanelHeight, this.topPanelHeight);
        applyAlignment(vastExtension != null ? vastExtension.getClosePosition() : null, closeViewAlignment, layoutParams);
        this.closeView.setLayoutParams(layoutParams);
        this.closeView.changePercentage(100, 0);
        if (vastRequest2.getVideoType() == VideoType.Rewarded) {
            this.closeView.setImage(Assets.getBitmapFromBase64(Assets.close));
            setCloseViewVisibility(false);
        } else if (this.viewState.isSkipEnabled || this.viewState.skipTime == 0) {
            this.closeView.setImage(Assets.getBitmapFromBase64(Assets.close));
            setCloseViewVisibility(true);
        } else {
            this.closeView.setImage(null);
            setCloseViewVisibility(true);
        }
    }

    private void configureMuteView(@Nullable VastExtension vastExtension) {
        if (vastExtension == null || vastExtension.isShowMute()) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.topPanelHeight, this.topPanelHeight);
            if (this.muteView == null) {
                this.muteView = new CircleCountdownView(getContext(), this.assetsColor, this.assetsBackgroundColor);
                this.muteView.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        VastView.this.toggleMute();
                    }
                });
                this.overlayView.addView(this.muteView);
            } else {
                this.muteView.setVisibility(0);
            }
            applyAlignment(vastExtension != null ? vastExtension.getMutePosition() : null, muteViewAlignment, layoutParams);
            this.muteView.setMainColor(this.assetsColor);
            this.muteView.setArcBackgroundColor(this.assetsBackgroundColor);
            this.muteView.setLayoutParams(layoutParams);
        } else if (this.muteView != null) {
            removeView(this.muteView);
        }
    }

    private void configureRepeatView(@Nullable VastExtension vastExtension) {
        if (this.repeatView != null) {
            this.overlayView.removeView(this.repeatView);
        }
    }

    private void configureProgressView(@Nullable VastExtension vastExtension) {
        if (vastExtension == null || vastExtension.isShowProgress()) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, Utils.dpToPx(getContext(), 3.0f));
            layoutParams.addRule(12);
            if (this.progressView == null) {
                this.progressView = new VastLinearCountdown(getContext(), this.assetsColor);
                this.overlayView.addView(this.progressView);
            }
            this.progressView.changePercentage(0.0f);
            this.progressView.setLineColor(this.assetsColor);
            this.progressView.setLayoutParams(layoutParams);
        } else if (this.progressView != null) {
            this.overlayView.removeView(this.progressView);
        }
    }

    private void configureBannerView(@Nullable VastExtension vastExtension) {
        if (this.bannerTag == null || this.viewState.isCompanionShown) {
            removeBannerView();
            return;
        }
        this.bannerView = createBannerView(getContext(), this.bannerTag, vastExtension);
        this.overlayView.addView(this.bannerView);
        trackBannerEvent(TrackingEvent.creativeView);
    }

    @SuppressLint({"SetJavaScriptEnabled", "ClickableViewAccessibility"})
    private WebView createBannerView(@NonNull Context context, @NonNull CompanionTag companionTag2, @Nullable VastExtension vastExtension) {
        int i;
        int i2;
        if (Utils.isTablet(context) && companionTag2.getWidth() == 728 && companionTag2.getHeight() == 90) {
            i2 = Utils.dpToPx(context, 728.0f);
            i = Utils.dpToPx(context, 90.0f);
        } else {
            i2 = Utils.dpToPx(context, 320.0f);
            i = Utils.dpToPx(context, 50.0f);
        }
        int dpToPx = Utils.dpToPx(getContext(), 3.0f);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i2, i);
        layoutParams.setMargins(dpToPx, dpToPx, dpToPx, dpToPx);
        applyAlignment(vastExtension != null ? vastExtension.getCtaPosition() : null, bannerAlignment, layoutParams);
        WebView webView = new WebView(context);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollContainer(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollBarStyle(33554432);
        webView.setFocusableInTouchMode(false);
        webView.setBackgroundColor(0);
        webView.setLayerType(1, null);
        webView.setOnTouchListener(this.bannerTouchListener);
        webView.setWebViewClient(this.bannerWebViewClient);
        webView.setWebChromeClient(this.bannerWebChromeClient);
        webView.setLayoutParams(layoutParams);
        String html = companionTag2.getHtml(i2, i, context.getResources().getDisplayMetrics().density);
        if (html != null) {
            webView.loadDataWithBaseURL("", html, WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
        }
        return webView;
    }

    private void removeBannerView() {
        if (this.bannerView != null) {
            this.overlayView.removeView(this.bannerView);
            this.bannerView = null;
        }
    }

    private void applyAlignment(@Nullable Pair<Integer, Integer> pair, @NonNull Pair<Integer, Integer> pair2, @NonNull RelativeLayout.LayoutParams layoutParams) {
        if (pair == null) {
            pair = pair2;
        }
        layoutParams.addRule(((Integer) pair.first).intValue());
        layoutParams.addRule(((Integer) pair.second).intValue());
    }

    private void createCompanion() {
        int i;
        int i2;
        if (isLoaded()) {
            int availableWidth = getAvailableWidth();
            int availableHeight = getAvailableHeight();
            this.companionTag = this.vastRequest.getVastAd().getCompanion(availableWidth, availableHeight);
            if (this.companionTag != null) {
                this.companionOrientation = this.companionTag.getWidth() >= this.companionTag.getHeight() ? 2 : 1;
            } else {
                this.companionOrientation = this.videoOrientation;
            }
            if (this.companionTag == null) {
                if (this.companionView == null) {
                    this.companionView = new ImageView(getContext());
                }
                this.companionView.setAdjustViewBounds(true);
                this.companionView.setScaleType(ScaleType.FIT_CENTER);
            } else {
                removeCompanionView();
                if (this.companionOrientation == 2) {
                    i = Math.max(availableWidth, availableHeight);
                    i2 = Math.min(availableWidth, availableHeight);
                } else {
                    i = Math.min(availableWidth, availableHeight);
                    i2 = Math.max(availableWidth, availableHeight);
                }
                Pair htmlForMraid = this.companionTag.getHtmlForMraid(i, i2, getResources().getDisplayMetrics().density);
                String str = (String) htmlForMraid.first;
                if (str != null) {
                    CompanionInterstitialListener companionInterstitialListener = new CompanionInterstitialListener();
                    this.companionInterstitial = MRAIDInterstitial.newBuilder(getContext(), str, ((Integer) ((Pair) htmlForMraid.second).first).intValue(), ((Integer) ((Pair) htmlForMraid.second).second).intValue()).setBaseUrl(null).setListener(companionInterstitialListener).setNativeFeatureListener(companionInterstitialListener).setPreload(true).setCloseTime(this.vastRequest.getCloseTime()).setIsTag(false).setUseLayout(this.vastRequest.isUseLayoutInCompanion()).build();
                    this.companionInterstitial.load();
                } else {
                    handleCompanionShowError();
                }
            }
        }
    }

    private void showCompanion() {
        if (isLoaded() && !this.isShowingCompanion) {
            createCompanion();
            this.isShowingCompanion = true;
            this.viewState.isCompanionShown = true;
            if (!(getResources().getConfiguration().orientation == this.companionOrientation || this.listener == null)) {
                this.listener.onOrientationRequested(this, this.vastRequest, this.companionOrientation);
            }
            setProgressBarVisibility(false);
            if (this.progressView != null) {
                this.progressView.setVisibility(8);
            }
            if (this.muteView != null) {
                this.muteView.setVisibility(8);
            }
            if (this.repeatView != null) {
                this.repeatView.setVisibility(8);
            }
            this.overlayView.setAlpha(1.0f);
            this.overlayView.setVisibility(0);
            this.overlayView.animate().cancel();
            if (this.companionTag == null) {
                this.closeView.changePercentage(100, 0);
                this.closeView.setImage(Assets.getBitmapFromBase64(Assets.close));
                this.closeView.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        VastView.this.handleClose();
                    }
                });
                setCloseViewVisibility(true);
                if (this.ctaView != null) {
                    this.ctaView.setBackgroundDrawable(createButtonBackground());
                    this.ctaView.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
                }
                if (this.companionView != null) {
                    final WeakReference weakReference = new WeakReference(this.companionView);
                    AnonymousClass8 r1 = new MediaFrameRetriever(getContext(), this.vastRequest.getFileUri(), this.vastRequest.getVastAd().getPickedMediaFileTag().getText()) {
                        /* access modifiers changed from: 0000 */
                        public void onObtained(@Nullable Bitmap bitmap) {
                            ImageView imageView = (ImageView) weakReference.get();
                            if (imageView == null) {
                                return;
                            }
                            if (bitmap == null) {
                                imageView.setOnClickListener(new OnClickListener() {
                                    public void onClick(View view) {
                                        VastView.this.handleInfoClicked();
                                        VastView.this.handleClose();
                                    }
                                });
                                return;
                            }
                            imageView.setImageBitmap(bitmap);
                            imageView.setAlpha(0.0f);
                            imageView.animate().alpha(1.0f).setDuration(100).setListener(new AnimatorListenerAdapter() {
                                public void onAnimationEnd(Animator animator) {
                                    super.onAnimationEnd(animator);
                                    VastView.this.textureView.setVisibility(8);
                                }
                            }).start();
                            imageView.setOnClickListener(new OnClickListener() {
                                public void onClick(View view) {
                                    VastView.this.handleInfoClicked();
                                }
                            });
                        }
                    };
                    this.currentMediaFrameRetriever = r1;
                }
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams.addRule(13);
                addView(this.companionView, layoutParams);
            } else {
                setCloseViewVisibility(false);
                this.textureView.setVisibility(8);
                removeBannerView();
                if (this.ctaView != null) {
                    this.ctaView.setVisibility(8);
                }
                if (this.companionInterstitial != null && this.companionInterstitial.isReady()) {
                    this.companionInterstitial.show(getId());
                    this.progressBar.bringToFront();
                }
            }
            stopPlayback();
            this.overlayView.bringToFront();
            trackCompanionEvent(TrackingEvent.creativeView);
        }
    }

    private void hideCompanion() {
        if (this.companionView != null) {
            removeCompanionView();
        } else if (this.companionInterstitial != null) {
            this.companionInterstitial.destroy();
            this.companionInterstitial = null;
        }
        this.isShowingCompanion = false;
    }

    private void removeCompanionView() {
        if (this.companionView != null) {
            cancelMediaFrameRetriever();
            removeView(this.companionView);
            this.companionView = null;
        }
    }

    private void cancelMediaFrameRetriever() {
        if (this.currentMediaFrameRetriever != null) {
            this.currentMediaFrameRetriever.cancel();
            this.currentMediaFrameRetriever = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        removeCallbacks(this.sizeChangeRunnable);
        post(this.sizeChangeRunnable);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onWindowFocusChanged: ");
        sb.append(z);
        VastLog.d(str, sb.toString());
        this.isWindowFocused = z;
        syncPlayback();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.isWindowFocused) {
            startPlayback("onAttachedToWindow");
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopPlayback();
    }

    /* access modifiers changed from: private */
    public void setCloseViewVisibility(boolean z) {
        if (z) {
            this.closeView.setVisibility(0);
            this.overlayView.bringToFront();
            return;
        }
        this.closeView.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void setProgressBarVisibility(boolean z) {
        if (z) {
            this.progressBar.setVisibility(0);
            this.progressBar.bringToFront();
            return;
        }
        this.progressBar.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void toggleMute() {
        this.viewState.isMuted = !this.viewState.isMuted;
        syncMuteView();
        trackEvent(this.viewState.isMuted ? TrackingEvent.mute : TrackingEvent.unmute);
    }

    /* access modifiers changed from: private */
    public void syncMuteView() {
        if (isPlaybackStarted() && this.muteView != null) {
            if (this.viewState.isMuted) {
                this.muteView.setImage(Assets.getBitmapFromBase64(Assets.unmute));
                this.mediaPlayer.setVolume(0.0f, 0.0f);
            } else {
                this.muteView.setImage(Assets.getBitmapFromBase64(Assets.mute));
                this.mediaPlayer.setVolume(1.0f, 1.0f);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showOverlay(final long j) {
        post(new Runnable() {
            public void run() {
                if (VastView.this.overlayView.getVisibility() != 0) {
                    VastView.this.overlayView.setVisibility(0);
                    VastView.this.overlayView.setAlpha(1.0f);
                    VastView.this.hideOverlay(j);
                } else if (!VastView.this.isOverlayHidePending) {
                    VastView.this.hideOverlay(j);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void hideOverlay(long j) {
        this.isOverlayHidePending = true;
        cancelOverlayHiding();
        postDelayed(this.overlayHideRunnable, (j * 1000) + 3000);
    }

    private void cancelOverlayHiding() {
        this.isOverlayHidePending = false;
        removeCallbacks(this.overlayHideRunnable);
    }

    public void handleBackPress() {
        if (!isSkipEnabled()) {
            return;
        }
        if (!isCompanionShown()) {
            performVideoCloseClick();
        } else if (this.vastRequest != null && this.vastRequest.getVideoType() == VideoType.NonRewarded) {
            if (this.companionTag == null) {
                handleClose();
            } else {
                handleCompanionClose();
            }
        }
    }

    private void performVideoCloseClick() {
        VastLog.e(this.TAG, "performVideoCloseClick");
        stopPlayback();
        if (!this.isReceivePlaybackError) {
            if (!this.viewState.isCompleted) {
                trackEvent(TrackingEvent.skip);
            }
            if (this.vastRequest != null && this.vastRequest.getMaxDurationMillis() > 0 && this.vastRequest.getVideoType() == VideoType.Rewarded && this.listener != null) {
                this.listener.onComplete(this, this.vastRequest);
            }
            handleVideoPlayingFinish();
            return;
        }
        handleClose();
    }

    /* access modifiers changed from: private */
    public void handleImpressions() {
        VastLog.d(this.TAG, "handleImpressions");
        if (this.vastRequest != null) {
            this.viewState.isImpressionProcessed = true;
            dispatchUrls(this.vastRequest.getVastAd().getImpressionUrlList());
        }
    }

    /* access modifiers changed from: private */
    public void handleComplete() {
        VastLog.d(this.TAG, "handleComplete");
        this.viewState.isSkipEnabled = true;
        if (!this.isReceivePlaybackError && !this.viewState.isCompleted) {
            this.viewState.isCompleted = true;
            if (this.listener != null) {
                this.listener.onComplete(this, this.vastRequest);
            }
            trackEvent(TrackingEvent.complete);
        }
        if (this.viewState.isCompleted) {
            handleVideoPlayingFinish();
        }
    }

    /* access modifiers changed from: private */
    public void handleInfoClicked() {
        VastLog.e(this.TAG, "handleInfoClicked");
        if (this.vastRequest != null) {
            processClickThroughEvent(this.vastRequest.getVastAd().getClickTrackingUrlList(), this.vastRequest.getVastAd().getClickThroughUrl());
        }
    }

    /* access modifiers changed from: private */
    public void handleClose() {
        VastLog.e(this.TAG, "handleClose");
        trackEvent(TrackingEvent.close);
        if (this.listener != null && this.vastRequest != null) {
            this.listener.onFinish(this, this.vastRequest, isFinished());
        }
    }

    /* access modifiers changed from: private */
    public void handleCompanionClose() {
        VastLog.e(this.TAG, "handleCompanionClose");
        trackCompanionEvent(TrackingEvent.close);
        if (this.listener != null && this.vastRequest != null) {
            this.listener.onFinish(this, this.vastRequest, isFinished());
        }
    }

    private void handleCompanionShowError() {
        VastLog.e(this.TAG, "handleCompanionShowError");
        trackErrorEvent(600);
        if (this.listener != null && this.vastRequest != null) {
            this.listener.onFinish(this, this.vastRequest, isFinished());
        }
    }

    /* access modifiers changed from: private */
    public void handlePlaybackError() {
        VastLog.e(this.TAG, "handlePlaybackError");
        this.isReceivePlaybackError = true;
        trackErrorEvent(VastError.ERROR_CODE_ERROR_SHOWING);
        handleVideoPlayingFinish();
    }

    private void handleVideoPlayingFinish() {
        VastLog.d(this.TAG, "finishVideoPlaying");
        stopPlayback();
        if (this.vastRequest == null || this.vastRequest.isAutoClose() || (this.vastRequest.getVastAd().getAppodealExtension() != null && !this.vastRequest.getVastAd().getAppodealExtension().isShowCompanion())) {
            handleClose();
            return;
        }
        if (isSkipEnabled()) {
            trackEvent(TrackingEvent.close);
        }
        setProgressBarVisibility(false);
        removeBannerView();
        showCompanion();
    }

    public void clickHandled() {
        if (isCompanionShown()) {
            setProgressBarVisibility(false);
        } else if (!this.isWindowFocused) {
            pausePlayback();
        } else {
            resumePlayback();
        }
    }

    public void clickHandleCanceled() {
        if (isCompanionShown()) {
            setProgressBarVisibility(false);
        } else {
            resumePlayback();
        }
    }

    public void clickHandleError() {
        if (isPlaybackStarted()) {
            resumePlayback();
        } else if (!isCompanionShown()) {
            showCompanion();
        } else {
            handleCompanionClose();
        }
    }

    /* access modifiers changed from: private */
    public void processClickThroughEvent(@Nullable CompanionTag companionTag2, @Nullable String str) {
        processClickThroughEvent(companionTag2 != null ? companionTag2.getCompanionClickTrackingList() : null, str);
    }

    private void processClickThroughEvent(@Nullable List<String> list, @Nullable String str) {
        String str2 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("processClickThroughEvent: ");
        sb.append(str);
        VastLog.d(str2, sb.toString());
        if (str != null) {
            dispatchUrls(list);
            if (this.listener != null && this.vastRequest != null) {
                pausePlayback();
                setProgressBarVisibility(true);
                this.listener.onClick(this, this.vastRequest, this, str);
            }
        }
    }

    private void trackErrorEvent(int i) {
        try {
            if (this.vastRequest != null) {
                this.vastRequest.sendError(i);
            }
        } catch (Exception e) {
            VastLog.e(this.TAG, e.getMessage());
        }
        if (this.listener != null && this.vastRequest != null) {
            this.listener.onError(this, this.vastRequest, i);
        }
    }

    /* access modifiers changed from: private */
    public void trackEvent(@NonNull TrackingEvent trackingEvent) {
        VastLog.d(this.TAG, String.format("Track Event: %s", new Object[]{trackingEvent}));
        VastAd vastAd = this.vastRequest != null ? this.vastRequest.getVastAd() : null;
        if (vastAd != null) {
            dispatchUrls(vastAd.getTrackingEventListMap(), trackingEvent);
        }
    }

    private void trackBannerEvent(@NonNull TrackingEvent trackingEvent) {
        VastLog.d(this.TAG, String.format("Track Banner Event: %s", new Object[]{trackingEvent}));
        if (this.bannerTag != null) {
            dispatchUrls(this.bannerTag.getTrackingEventListMap(), trackingEvent);
        }
    }

    private void trackCompanionEvent(@NonNull TrackingEvent trackingEvent) {
        VastLog.d(this.TAG, String.format("Track Companion Event: %s", new Object[]{trackingEvent}));
        if (this.companionTag != null) {
            dispatchUrls(this.companionTag.getTrackingEventListMap(), trackingEvent);
        }
    }

    private void dispatchUrls(@Nullable Map<TrackingEvent, List<String>> map, @NonNull TrackingEvent trackingEvent) {
        if (map == null || map.size() <= 0) {
            VastLog.d(this.TAG, String.format("Processing Event - fail: %s (tracking event map is null or empty)", new Object[]{trackingEvent}));
            return;
        }
        dispatchUrls((List) map.get(trackingEvent));
    }

    private void dispatchUrls(@Nullable List<String> list) {
        if (isLoaded()) {
            if (list == null || list.size() == 0) {
                VastLog.d(this.TAG, "\turl list is null");
            } else {
                this.vastRequest.fireUrls(list, null);
            }
        }
    }

    public boolean isLoaded() {
        return (this.vastRequest == null || this.vastRequest.getVastAd() == null) ? false : true;
    }

    public boolean isPlaybackStarted() {
        return this.mediaPlayer != null && this.isMediaPlayerPrepared;
    }

    public boolean isSkipEnabled() {
        return this.viewState.isSkipEnabled;
    }

    public boolean isCompanionShown() {
        return this.viewState.isCompanionShown;
    }

    public boolean isFinished() {
        return this.vastRequest != null && ((this.vastRequest.getCloseTime() == 0 && this.viewState.isCompleted) || (this.vastRequest.getCloseTime() > 0 && this.viewState.isCompanionShown));
    }

    private void prepareMediaPlayer() {
        try {
            if (isLoaded() && !this.viewState.isCompanionShown) {
                boolean z = false;
                if (this.mediaPlayer == null) {
                    this.mediaPlayer = new MediaPlayer();
                    this.mediaPlayer.setLooping(false);
                    this.mediaPlayer.setAudioStreamType(3);
                    this.mediaPlayer.setOnCompletionListener(this.playerCompletionListener);
                    this.mediaPlayer.setOnErrorListener(this.playerErrorListener);
                    this.mediaPlayer.setOnPreparedListener(this.playerPreparedListener);
                    this.mediaPlayer.setOnVideoSizeChangedListener(this.playerVideoSizeChangedListener);
                }
                if (this.vastRequest.getFileUri() == null) {
                    z = true;
                }
                setProgressBarVisibility(z);
                this.mediaPlayer.setSurface(this.currentSurface);
                if (this.vastRequest.getFileUri() == null) {
                    this.mediaPlayer.setDataSource(this.vastRequest.getVastAd().getPickedMediaFileTag().getText());
                } else {
                    this.mediaPlayer.setDataSource(getContext(), this.vastRequest.getFileUri());
                }
                this.mediaPlayer.prepareAsync();
            }
        } catch (Exception e) {
            VastLog.e(this.TAG, e.getMessage(), e);
            handlePlaybackError();
        }
    }

    /* access modifiers changed from: private */
    public void startPlayback(String str) {
        String str2 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("startPlayback: ");
        sb.append(str);
        VastLog.d(str2, sb.toString());
        if (isLoaded()) {
            if (this.viewState.isCompanionShown) {
                showCompanion();
            } else if (this.isWindowFocused) {
                if (this.isTextureCreated) {
                    stopPlayback();
                    hideCompanion();
                    configureVideoSurface();
                    prepareMediaPlayer();
                    VastHelper.addScreenStateChangeListener(this, this.screenStateChangeListener);
                } else {
                    this.isWaitingSurface = true;
                }
                if (this.textureView.getVisibility() != 0) {
                    this.textureView.setVisibility(0);
                }
            } else {
                this.isWaitingWindowFocus = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void pausePlayback() {
        if (isPlaybackStarted() && !this.viewState.isPaused) {
            VastLog.d(this.TAG, "pausePlayback");
            this.viewState.isPaused = true;
            this.viewState.currentVideoPosition = this.mediaPlayer.getCurrentPosition();
            this.mediaPlayer.pause();
            cancelPlaybackTracking();
            cancelOverlayHiding();
            trackEvent(TrackingEvent.pause);
        }
    }

    /* access modifiers changed from: private */
    public void resumePlayback() {
        if (this.viewState.isPaused && this.isWindowFocused) {
            VastLog.d(this.TAG, "resumePlayback");
            this.viewState.isPaused = false;
            if (isPlaybackStarted()) {
                this.mediaPlayer.start();
                syncOverlayView();
                startPlaybackTracking();
                setProgressBarVisibility(false);
                trackEvent(TrackingEvent.resume);
            } else if (!this.viewState.isCompanionShown) {
                startPlayback("resumePlayback");
            }
        }
    }

    private void stopPlayback() {
        this.viewState.isPaused = false;
        if (this.mediaPlayer != null) {
            VastLog.d(this.TAG, "stopPlayback");
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.stop();
            }
            this.mediaPlayer.release();
            this.mediaPlayer = null;
            this.isMediaPlayerPrepared = false;
            this.isReceivePlaybackError = false;
            cancelPlaybackTracking();
            VastHelper.removeScreenStateChangeListener(this);
        }
    }

    private void restartPlayback() {
        if (isLoaded()) {
            this.viewState.isCompanionShown = false;
            this.viewState.currentVideoPosition = 0;
            hideCompanion();
            configureBannerView(this.vastRequest.getVastAd().getAppodealExtension());
            startPlayback("restartPlayback");
        }
    }

    /* access modifiers changed from: private */
    public void syncPlayback() {
        if (!this.isWindowFocused || !VastHelper.isScreenOn(getContext())) {
            pausePlayback();
        } else if (this.isWaitingWindowFocus) {
            this.isWaitingWindowFocus = false;
            startPlayback("onWindowFocusChanged");
        } else if (!this.viewState.isCompanionShown) {
            resumePlayback();
        } else {
            setProgressBarVisibility(false);
        }
    }

    /* access modifiers changed from: private */
    public void configureVideoSurface() {
        int i;
        int i2;
        if (this.videoWidth == 0 || this.videoHeight == 0) {
            VastLog.d(this.TAG, "configureVideoSurface - skip: videoWidth or videoHeight is 0");
            return;
        }
        int availableWidth = getAvailableWidth();
        int availableHeight = getAvailableHeight();
        if (availableWidth > availableHeight) {
            i = Math.max(availableWidth, availableHeight);
            i2 = Math.min(availableWidth, availableHeight);
        } else {
            i = Math.min(availableWidth, availableHeight);
            i2 = Math.max(availableWidth, availableHeight);
        }
        if (i == 0) {
            i = this.videoWidth;
        }
        if (i2 == 0) {
            i2 = this.videoHeight;
        }
        double d = (double) i;
        double d2 = (double) this.videoWidth;
        Double.isNaN(d);
        Double.isNaN(d2);
        double d3 = d / d2;
        double d4 = (double) i2;
        double d5 = (double) this.videoHeight;
        Double.isNaN(d4);
        Double.isNaN(d5);
        double min = Math.min(d3, d4 / d5);
        double d6 = (double) this.videoWidth;
        Double.isNaN(d6);
        int round = (int) Math.round(d6 * min);
        double d7 = (double) this.videoHeight;
        Double.isNaN(d7);
        int round2 = (int) Math.round(d7 * min);
        LayoutParams layoutParams = (LayoutParams) this.textureView.getLayoutParams();
        if (!(layoutParams.width == round && layoutParams.height == round2)) {
            layoutParams.width = round;
            layoutParams.height = round2;
            this.textureView.setLayoutParams(layoutParams);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("configureVideoSurface: ratio=");
        sb.append(min);
        sb.append(", size=");
        sb.append(round);
        sb.append("/");
        sb.append(round2);
        VastLog.d(sb.toString());
    }

    /* access modifiers changed from: private */
    public void startPlaybackTracking() {
        startProgressTimer();
        cancelPlaybackTracking();
        this.playbackTrackingRunnable.run();
    }

    private void cancelPlaybackTracking() {
        removeCallbacks(this.playbackTrackingRunnable);
    }

    private void startProgressTimer() {
        this.videoProgressTracker.clear();
        this.progressTrackingErrorCount = 0;
        this.previousProgressPercent = 0.0f;
    }

    /* access modifiers changed from: private */
    public void syncOverlayView() {
        if (isLoaded()) {
            if (this.vastRequest.getVideoType() != VideoType.NonRewarded) {
                showOverlay(0);
            } else if (isPlaybackStarted() || this.viewState.isPaused) {
                showOverlay((long) Math.max(0, this.viewState.skipTime - (this.mediaPlayer.getCurrentPosition() / 1000)));
            } else {
                showOverlay((long) this.viewState.skipTime);
            }
        }
    }

    public void setListener(@Nullable VastViewListener vastViewListener) {
        this.listener = vastViewListener;
    }

    @Nullable
    public VastViewListener getListener() {
        return this.listener;
    }

    private int getAvailableWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private int getAvailableHeight() {
        return (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
    }

    private Drawable createButtonBackground() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(0);
        gradientDrawable.setColor(this.assetsBackgroundColor);
        gradientDrawable.setCornerRadius(100.0f);
        return gradientDrawable;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public Parcelable onSaveInstanceState() {
        if (isPlaybackStarted()) {
            this.viewState.currentVideoPosition = this.mediaPlayer.getCurrentPosition();
        }
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.vastViewState = this.viewState;
        savedState.vastRequest = this.vastRequest;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.vastViewState != null) {
            this.viewState = savedState.vastViewState;
        }
        if (savedState.vastRequest != null) {
            display(savedState.vastRequest, true);
        }
    }
}
