package com.explorestack.iab.vast;

import com.explorestack.iab.utils.Logger;
import com.explorestack.iab.utils.Logger.LogLevel;

public class VastLog {
    private static final Logger logger = new Logger("VastLog");

    public static void d(String str) {
        logger.d(str);
    }

    public static void d(String str, String str2) {
        logger.d(str, str2);
    }

    public static void e(String str) {
        logger.e(str);
    }

    public static void e(String str, String str2) {
        logger.e(str, str2);
    }

    public static void e(String str, Throwable th) {
        logger.e(str, th);
    }

    public static void e(String str, String str2, Throwable th) {
        logger.e(str, str2, th);
    }

    public static void setLoggingLevel(LogLevel logLevel) {
        logger.setLoggingLevel(logLevel);
    }
}
