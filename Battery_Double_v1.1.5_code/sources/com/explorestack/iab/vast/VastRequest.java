package com.explorestack.iab.vast;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.utils.Utils;
import com.explorestack.iab.vast.VastUrlProcessorRegistry.OnUrlReadyCallback;
import com.explorestack.iab.vast.activity.VastView;
import com.explorestack.iab.vast.processor.DefaultMediaPicker;
import com.explorestack.iab.vast.processor.VastAd;
import com.explorestack.iab.vast.processor.VastMediaPicker;
import com.explorestack.iab.vast.processor.VastProcessor;
import com.explorestack.iab.vast.processor.VastProcessorResult;
import com.explorestack.iab.vast.tags.AppodealExtensionTag;
import com.explorestack.iab.vast.tags.MediaFileTag;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class VastRequest implements Parcelable {
    private static final String CACHE_DIRECTORY = "/vast_rtb_cache/";
    private static final int CACHE_SIZE = 5;
    public static final Creator<VastRequest> CREATOR = new Creator<VastRequest>() {
        public VastRequest createFromParcel(Parcel parcel) {
            return new VastRequest(parcel);
        }

        public VastRequest[] newArray(int i) {
            return new VastRequest[i];
        }
    };
    public static final String PARAMS_ERROR_CODE = "params_error_code";
    private static final String TAG = "VastRequest";
    private static final OnUrlReadyCallback fireUrlProcessCallback = new OnUrlReadyCallback() {
        public void onUrlReady(String str) {
            VastLog.d(VastRequest.TAG, String.format("Fire url: %s", new Object[]{str}));
            Utils.httpGetURL(str);
        }
    };
    /* access modifiers changed from: private */
    public boolean autoClose;
    /* access modifiers changed from: private */
    public int closeTime;
    @Nullable
    private Bundle extras;
    @Nullable
    private Uri fileUri;
    @NonNull
    private final String hash;
    /* access modifiers changed from: private */
    public int maxDurationMillis;
    /* access modifiers changed from: private */
    @Nullable
    public VastMediaPicker<MediaFileTag> mediaFilePicker;
    /* access modifiers changed from: private */
    public boolean preCache;
    private int requestedOrientation;
    /* access modifiers changed from: private */
    public boolean useLayoutInCompanion;
    @Nullable
    private VastAd vastAd;
    @NonNull
    private VideoType videoType;
    /* access modifiers changed from: private */
    @Nullable
    public String xmlUrl;

    public class Builder {
        public Builder() {
        }

        public Builder setXmlUrl(@Nullable String str) {
            VastRequest.this.xmlUrl = str;
            return this;
        }

        public Builder setCloseTime(int i) {
            VastRequest.this.closeTime = i;
            return this;
        }

        public Builder setMaxDuration(int i) {
            VastRequest.this.maxDurationMillis = i;
            return this;
        }

        public Builder setPreCache(boolean z) {
            VastRequest.this.preCache = z;
            return this;
        }

        public Builder setAutoClose(boolean z) {
            VastRequest.this.autoClose = z;
            return this;
        }

        public Builder setUseLayoutInCompanion(boolean z) {
            VastRequest.this.useLayoutInCompanion = z;
            return this;
        }

        public Builder setMediaFilePicker(VastMediaPicker<MediaFileTag> vastMediaPicker) {
            VastRequest.this.mediaFilePicker = vastMediaPicker;
            return this;
        }

        public Builder addExtra(@NonNull String str, @Nullable String str2) {
            VastRequest.this.addExtra(str, str2);
            return this;
        }

        public VastRequest build() {
            return VastRequest.this;
        }
    }

    class Pair implements Comparable {
        public File mFile;
        public long mLastModified;

        public Pair(File file) {
            this.mFile = file;
            this.mLastModified = file.lastModified();
        }

        public int compareTo(@NonNull Object obj) {
            Pair pair = (Pair) obj;
            if (this.mLastModified > pair.mLastModified) {
                return -1;
            }
            return this.mLastModified == pair.mLastModified ? 0 : 1;
        }
    }

    public int describeContents() {
        return 0;
    }

    private VastRequest() {
        this.videoType = VideoType.NonRewarded;
        this.requestedOrientation = 0;
        this.useLayoutInCompanion = true;
        this.hash = Integer.toHexString(hashCode());
    }

    protected VastRequest(Parcel parcel) {
        this.videoType = VideoType.NonRewarded;
        boolean z = false;
        this.requestedOrientation = 0;
        this.useLayoutInCompanion = true;
        this.hash = parcel.readString();
        this.fileUri = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.vastAd = (VastAd) parcel.readParcelable(VastAd.class.getClassLoader());
        this.xmlUrl = parcel.readString();
        this.videoType = (VideoType) parcel.readSerializable();
        this.extras = parcel.readBundle(Bundle.class.getClassLoader());
        this.closeTime = parcel.readInt();
        this.maxDurationMillis = parcel.readInt();
        this.requestedOrientation = parcel.readInt();
        this.preCache = parcel.readByte() != 0;
        this.autoClose = parcel.readByte() != 0;
        if (parcel.readByte() != 0) {
            z = true;
        }
        this.useLayoutInCompanion = z;
        if (this.vastAd != null) {
            this.vastAd.setVastRequest(this);
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.hash);
        parcel.writeParcelable(this.fileUri, i);
        parcel.writeParcelable(this.vastAd, i);
        parcel.writeString(this.xmlUrl);
        parcel.writeSerializable(this.videoType);
        parcel.writeBundle(this.extras);
        parcel.writeInt(this.closeTime);
        parcel.writeInt(this.maxDurationMillis);
        parcel.writeInt(this.requestedOrientation);
        parcel.writeByte(this.preCache ? (byte) 1 : 0);
        parcel.writeByte(this.autoClose ? (byte) 1 : 0);
        parcel.writeByte(this.useLayoutInCompanion ? (byte) 1 : 0);
    }

    @NonNull
    public String getHash() {
        return this.hash;
    }

    @Nullable
    public Uri getFileUri() {
        return this.fileUri;
    }

    @Nullable
    public VastAd getVastAd() {
        return this.vastAd;
    }

    @NonNull
    public VideoType getVideoType() {
        return this.videoType;
    }

    public int getCloseTime() {
        return this.closeTime;
    }

    public int getMaxDurationMillis() {
        return this.maxDurationMillis;
    }

    public boolean isAutoClose() {
        return this.autoClose;
    }

    public boolean isUseLayoutInCompanion() {
        return this.useLayoutInCompanion;
    }

    public void loadVideoWithUrl(@NonNull final Context context, @NonNull final String str, @Nullable final VastRequestListener vastRequestListener) {
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("loadVideoWithUrl ");
        sb.append(str);
        VastLog.d(str2, sb.toString());
        this.vastAd = null;
        if (Utils.isNetworkAvailable(context)) {
            try {
                new Thread() {
                    /* JADX WARNING: Removed duplicated region for block: B:22:0x005c A[SYNTHETIC, Splitter:B:22:0x005c] */
                    /* JADX WARNING: Removed duplicated region for block: B:27:0x0062 A[SYNTHETIC, Splitter:B:27:0x0062] */
                    public void run() {
                        BufferedReader bufferedReader = null;
                        try {
                            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(new URL(str).openStream()));
                            try {
                                StringBuffer stringBuffer = new StringBuffer();
                                while (true) {
                                    String readLine = bufferedReader2.readLine();
                                    if (readLine != null) {
                                        stringBuffer.append(readLine);
                                        stringBuffer.append(System.getProperty("line.separator"));
                                    } else {
                                        try {
                                            break;
                                        } catch (IOException unused) {
                                        }
                                    }
                                }
                                bufferedReader2.close();
                                VastRequest.this.loadVideoWithData(context, stringBuffer.toString(), vastRequestListener);
                            } catch (Exception e) {
                                e = e;
                                bufferedReader = bufferedReader2;
                                try {
                                    VastRequest.this.sendError(context, 100, vastRequestListener);
                                    VastLog.e(VastRequest.TAG, (Throwable) e);
                                    if (bufferedReader != null) {
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                    if (bufferedReader != null) {
                                        try {
                                            bufferedReader.close();
                                        } catch (IOException unused2) {
                                        }
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                bufferedReader = bufferedReader2;
                                if (bufferedReader != null) {
                                }
                                throw th;
                            }
                        } catch (Exception e2) {
                            e = e2;
                            VastRequest.this.sendError(context, 100, vastRequestListener);
                            VastLog.e(VastRequest.TAG, (Throwable) e);
                            if (bufferedReader != null) {
                                try {
                                    bufferedReader.close();
                                } catch (IOException unused3) {
                                }
                            }
                        }
                    }
                }.start();
            } catch (Exception unused) {
                sendError(context, VastError.ERROR_CODE_BAD_URI, vastRequestListener);
            }
        } else {
            sendError(context, 1, vastRequestListener);
        }
    }

    public void loadVideoWithData(@NonNull final Context context, @NonNull final String str, @Nullable final VastRequestListener vastRequestListener) {
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("loadVideoWithData\n");
        sb.append(str);
        VastLog.d(str2, sb.toString());
        this.vastAd = null;
        if (Utils.isNetworkAvailable(context)) {
            try {
                new Thread() {
                    public void run() {
                        VastRequest.this.loadVideoWithDataSync(context, str, vastRequestListener);
                    }
                }.start();
            } catch (Exception unused) {
                sendError(context, VastError.ERROR_CODE_BAD_URI, vastRequestListener);
            }
        } else {
            sendError(context, 1, vastRequestListener);
        }
    }

    public void loadVideoWithDataSync(@NonNull Context context, @NonNull String str, @Nullable VastRequestListener vastRequestListener) {
        VastProcessorResult process = new VastProcessor(this, this.mediaFilePicker == null ? new DefaultMediaPicker(context) : this.mediaFilePicker).process(str);
        if (process.hasVastAd()) {
            this.vastAd = process.getVastAd();
            this.vastAd.setVastRequest(this);
            AppodealExtensionTag appodealExtension = this.vastAd.getAppodealExtension();
            if (appodealExtension != null && appodealExtension.getCompanionCloseTime() > 0) {
                this.closeTime = appodealExtension.getCompanionCloseTime();
            }
            if (this.preCache) {
                try {
                    cache(context, this.vastAd.getPickedMediaFileTag().getText());
                    if (this.fileUri != null && !TextUtils.isEmpty(this.fileUri.getPath())) {
                        if (new File(this.fileUri.getPath()).exists()) {
                            Bitmap createVideoThumbnail = ThumbnailUtils.createVideoThumbnail(this.fileUri.getPath(), 1);
                            if (createVideoThumbnail == null) {
                                VastLog.d(TAG, "video file not supported");
                                sendError(context, VastError.ERROR_CODE_BAD_FILE, vastRequestListener);
                            } else if (!createVideoThumbnail.equals(Bitmap.createBitmap(createVideoThumbnail.getWidth(), createVideoThumbnail.getHeight(), createVideoThumbnail.getConfig()))) {
                                try {
                                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                                    mediaMetadataRetriever.setDataSource(context, this.fileUri);
                                    long parseLong = Long.parseLong(mediaMetadataRetriever.extractMetadata(9));
                                    if (this.maxDurationMillis != 0) {
                                        if (parseLong > ((long) this.maxDurationMillis)) {
                                            sendError(context, VastError.ERROR_CODE_DURATION, vastRequestListener);
                                        }
                                    }
                                    sendReady(vastRequestListener);
                                } catch (Exception e) {
                                    VastLog.e(TAG, (Throwable) e);
                                    sendError(context, VastError.ERROR_CODE_BAD_FILE, vastRequestListener);
                                }
                            } else {
                                VastLog.d(TAG, "empty thumbnail");
                                sendError(context, VastError.ERROR_CODE_BAD_FILE, vastRequestListener);
                            }
                            clearCache(context);
                            return;
                        }
                    }
                    VastLog.d(TAG, "fileUri is null");
                    sendError(context, VastError.ERROR_CODE_BAD_URI, vastRequestListener);
                } catch (Exception unused) {
                    VastLog.d(TAG, "exception when to cache file");
                    sendError(context, VastError.ERROR_CODE_BAD_URI, vastRequestListener);
                }
            } else {
                sendReady(vastRequestListener);
            }
        } else {
            sendError(context, process.getResultCode(), vastRequestListener);
        }
    }

    private String getCacheDirName(Context context) {
        File externalFilesDir = context.getExternalFilesDir(null);
        if (externalFilesDir == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(externalFilesDir.getPath());
        sb.append(CACHE_DIRECTORY);
        return sb.toString();
    }

    private void cache(Context context, String str) throws Exception {
        String cacheDirName = getCacheDirName(context);
        if (cacheDirName != null) {
            File file = new File(cacheDirName);
            if (!file.exists()) {
                file.mkdirs();
            }
            int length = 230 - file.getPath().length();
            StringBuilder sb = new StringBuilder();
            sb.append("temp");
            sb.append(System.currentTimeMillis());
            String sb2 = sb.toString();
            String replace = str.substring(0, Math.min(length, str.length())).replace("/", "").replace(":", "");
            File file2 = new File(file, replace);
            if (file2.exists()) {
                this.fileUri = Uri.fromFile(file2);
                return;
            }
            File file3 = new File(file, sb2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(file3);
            long contentLength = (long) httpURLConnection.getContentLength();
            long j = 0;
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
                j += (long) read;
            }
            fileOutputStream.close();
            if (contentLength == j) {
                file3.renameTo(new File(file, replace));
            }
            this.fileUri = Uri.fromFile(new File(file, replace));
            return;
        }
        throw new FileNotFoundException("No dir for caching file");
    }

    private void clearCache(Context context) {
        try {
            String cacheDirName = getCacheDirName(context);
            if (cacheDirName != null) {
                File[] listFiles = new File(cacheDirName).listFiles();
                if (listFiles != null) {
                    if (listFiles.length > 5) {
                        Pair[] pairArr = new Pair[listFiles.length];
                        for (int i = 0; i < listFiles.length; i++) {
                            pairArr[i] = new Pair(listFiles[i]);
                        }
                        Arrays.sort(pairArr);
                        for (int i2 = 0; i2 < listFiles.length; i2++) {
                            listFiles[i2] = pairArr[i2].mFile;
                        }
                        for (int i3 = 5; i3 < listFiles.length; i3++) {
                            if (!Uri.fromFile(listFiles[i3]).equals(this.fileUri)) {
                                listFiles[i3].delete();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            VastLog.e(TAG, (Throwable) e);
        }
    }

    public void display(@NonNull Context context, @NonNull VideoType videoType2, @Nullable VastActivityListener vastActivityListener) {
        VastLog.d(TAG, "play");
        if (this.vastAd == null) {
            VastLog.d(TAG, "vastAd is null; nothing to play");
        } else if (Utils.isNetworkAvailable(context)) {
            this.videoType = videoType2;
            this.requestedOrientation = context.getResources().getConfiguration().orientation;
            new com.explorestack.iab.vast.activity.VastActivity.Builder().setRequest(this).setListener(vastActivityListener).display(context);
        } else {
            sendError(context, 1, vastActivityListener);
        }
    }

    public void display(@NonNull VastView vastView) {
        if (this.vastAd != null) {
            this.videoType = VideoType.NonRewarded;
            vastView.display(this);
            return;
        }
        VastLog.d(TAG, "vastAd is null; nothing to play");
    }

    private void sendReady(@Nullable final VastRequestListener vastRequestListener) {
        VastLog.d(TAG, "sendReady");
        if (vastRequestListener != null) {
            Utils.onUiThread(new Runnable() {
                public void run() {
                    vastRequestListener.onVastLoaded(VastRequest.this);
                }
            });
        }
    }

    private void sendVastModelError(int i) {
        try {
            sendError(i);
        } catch (Exception e) {
            VastLog.e(TAG, (Throwable) e);
        }
    }

    /* access modifiers changed from: private */
    public void sendError(final Context context, final int i, @Nullable final VastErrorListener vastErrorListener) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("sendError, code: ");
        sb.append(i);
        VastLog.d(str, sb.toString());
        if (VastError.isVastError(i)) {
            sendVastModelError(i);
        }
        if (vastErrorListener != null) {
            Utils.onUiThread(new Runnable() {
                public void run() {
                    vastErrorListener.onVastError(context, VastRequest.this, i);
                }
            });
        }
    }

    public int getRequestedOrientation() {
        return this.requestedOrientation;
    }

    public boolean isPreCache() {
        return this.preCache;
    }

    public boolean checkFile() {
        try {
            if (this.fileUri == null || TextUtils.isEmpty(this.fileUri.getPath()) || !new File(this.fileUri.getPath()).exists()) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public void addExtra(String str, String str2) {
        if (this.extras == null) {
            this.extras = new Bundle();
        }
        this.extras.putString(str, str2);
    }

    public void fireErrorUrls(@Nullable List<String> list, @Nullable Bundle bundle) {
        fireUrls(list, bundle);
    }

    public void fireUrls(@Nullable List<String> list, @Nullable Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (this.extras != null) {
            bundle2.putAll(this.extras);
        }
        if (bundle != null) {
            bundle2.putAll(bundle);
        }
        if (list != null) {
            VastUrlProcessorRegistry.processUrls(list, bundle2, fireUrlProcessCallback);
        } else {
            VastLog.d(TAG, "Url list is null");
        }
    }

    public int getPreferredVideoOrientation() {
        int i = 2;
        if (this.vastAd == null) {
            return 2;
        }
        MediaFileTag pickedMediaFileTag = this.vastAd.getPickedMediaFileTag();
        if (pickedMediaFileTag.getWidth() <= pickedMediaFileTag.getHeight()) {
            i = 1;
        }
        return i;
    }

    public void sendError(int i) {
        if (this.vastAd != null) {
            Bundle bundle = new Bundle();
            bundle.putInt(PARAMS_ERROR_CODE, i);
            fireErrorUrls(this.vastAd.getErrorUrlList(), bundle);
        }
    }

    public static Builder newBuilder() {
        VastRequest vastRequest = new VastRequest();
        vastRequest.getClass();
        return new Builder();
    }
}
