package com.explorestack.iab.vast.processor;

import android.support.annotation.Nullable;
import android.util.Pair;
import com.explorestack.iab.vast.tags.LinearCreativeTag;
import com.explorestack.iab.vast.tags.MediaFileTag;
import java.io.Serializable;
import java.util.List;

public abstract class VastMediaPicker<T extends MediaFileTag> implements Serializable {
    /* access modifiers changed from: 0000 */
    public abstract Pair<LinearCreativeTag, T> pickVideo(@Nullable List<Pair<LinearCreativeTag, T>> list);
}
