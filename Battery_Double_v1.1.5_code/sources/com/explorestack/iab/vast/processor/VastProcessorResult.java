package com.explorestack.iab.vast.processor;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.VastUrlProcessorRegistry;
import com.explorestack.iab.vast.tags.AdContentTag;
import java.util.ArrayList;
import java.util.List;

public class VastProcessorResult {
    private final List<String> errorUrls = new ArrayList();
    private boolean last = true;
    private int resultCode = -1;
    private VastAd vastAd;

    /* access modifiers changed from: 0000 */
    public List<String> getErrorUrls() {
        return this.errorUrls;
    }

    public int getResultCode() {
        return this.resultCode;
    }

    /* access modifiers changed from: 0000 */
    public void setResultCode(int i) {
        this.resultCode = i;
    }

    public VastAd getVastAd() {
        return this.vastAd;
    }

    /* access modifiers changed from: 0000 */
    public void setVastAd(VastAd vastAd2) {
        this.vastAd = vastAd2;
    }

    public boolean hasVastAd() {
        return this.vastAd != null;
    }

    /* access modifiers changed from: 0000 */
    public boolean isLast() {
        return this.last;
    }

    /* access modifiers changed from: 0000 */
    public void setLast(boolean z) {
        this.last = z;
    }

    /* access modifiers changed from: 0000 */
    public void recordError(@NonNull AdContentTag adContentTag, int i) {
        setResultCode(i);
        Bundle bundle = new Bundle();
        bundle.putInt(VastRequest.PARAMS_ERROR_CODE, i);
        List<String> errorUrlList = adContentTag.getErrorUrlList();
        if (errorUrlList != null && !errorUrlList.isEmpty()) {
            for (String processUrl : errorUrlList) {
                String processUrl2 = VastUrlProcessorRegistry.processUrl(processUrl, bundle);
                if (!TextUtils.isEmpty(processUrl2)) {
                    this.errorUrls.add(processUrl2);
                }
            }
        }
    }
}
