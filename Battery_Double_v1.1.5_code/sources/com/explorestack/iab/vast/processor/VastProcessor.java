package com.explorestack.iab.vast.processor;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;
import com.explorestack.iab.vast.TrackingEvent;
import com.explorestack.iab.vast.VastError;
import com.explorestack.iab.vast.VastLog;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.tags.AdContentTag;
import com.explorestack.iab.vast.tags.AdTag;
import com.explorestack.iab.vast.tags.AppodealExtensionTag;
import com.explorestack.iab.vast.tags.CompanionAdsCreativeTag;
import com.explorestack.iab.vast.tags.CompanionTag;
import com.explorestack.iab.vast.tags.CreativeContentTag;
import com.explorestack.iab.vast.tags.CreativeTag;
import com.explorestack.iab.vast.tags.ExtensionTag;
import com.explorestack.iab.vast.tags.InLineAdTag;
import com.explorestack.iab.vast.tags.LinearCreativeTag;
import com.explorestack.iab.vast.tags.MediaFileTag;
import com.explorestack.iab.vast.tags.VastParser;
import com.explorestack.iab.vast.tags.VastTag;
import com.explorestack.iab.vast.tags.VideoClicksTag;
import com.explorestack.iab.vast.tags.WrapperAdTag;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Stack;
import javax.net.ssl.SSLException;
import org.apache.http.conn.ConnectTimeoutException;

public class VastProcessor {
    private static final int MAX_PROCESSING_WRAPPER = 5;
    private static final String TAG = "VastProcessor";
    final Stack<AdContentTag> adContentTagStack;
    private int currentProcessingWrapper;
    @VisibleForTesting
    final int maxProcessingWrapper;
    private VastMediaPicker<MediaFileTag> mediaPicker;
    private final VastRequest vastRequest;

    public VastProcessor(@NonNull VastRequest vastRequest2, @NonNull VastMediaPicker<MediaFileTag> vastMediaPicker) {
        this(vastRequest2, vastMediaPicker, 5);
    }

    @VisibleForTesting
    VastProcessor(@NonNull VastRequest vastRequest2, @NonNull VastMediaPicker<MediaFileTag> vastMediaPicker, int i) {
        this.adContentTagStack = new Stack<>();
        this.currentProcessingWrapper = 0;
        this.vastRequest = vastRequest2;
        this.mediaPicker = vastMediaPicker;
        this.maxProcessingWrapper = i;
    }

    @NonNull
    public VastProcessorResult process(String str) {
        VastLog.d(TAG, "process");
        VastProcessorResult vastProcessorResult = new VastProcessorResult();
        try {
            VastTag parseVast = VastParser.parseVast(str);
            if (parseVast != null && parseVast.hasAd()) {
                return processAds(null, parseVast, new WrapperAttributes());
            }
            vastProcessorResult.setResultCode(101);
            return vastProcessorResult;
        } catch (Exception unused) {
            vastProcessorResult.setResultCode(100);
            return vastProcessorResult;
        }
    }

    @NonNull
    private VastProcessorResult processAds(AdContentTag adContentTag, VastTag vastTag, WrapperAttributes wrapperAttributes) {
        VastProcessorResult vastProcessorResult = new VastProcessorResult();
        for (int i = 0; i < vastTag.getAdTagList().size(); i++) {
            AdTag adTag = (AdTag) vastTag.getAdTagList().get(i);
            if (!(adTag == null || adTag.getAdContentTag() == null)) {
                AdContentTag adContentTag2 = adTag.getAdContentTag();
                if (adContentTag2 instanceof InLineAdTag) {
                    VastProcessorResult processInLine = processInLine((InLineAdTag) adContentTag2);
                    if (processInLine.hasVastAd()) {
                        return processInLine;
                    }
                    sendErrors(processInLine.getErrorUrls());
                    if (adContentTag == null) {
                        vastProcessorResult.setResultCode(processInLine.getResultCode());
                    } else if (processInLine.isLast()) {
                        vastProcessorResult.recordError(adContentTag, processInLine.getResultCode());
                    }
                } else if ((adContentTag2 instanceof WrapperAdTag) && wrapperAttributes.isFollowAdditionalWrappers()) {
                    VastProcessorResult processWrapper = processWrapper((WrapperAdTag) adContentTag2);
                    if (processWrapper.hasVastAd()) {
                        return processWrapper;
                    }
                    sendErrors(processWrapper.getErrorUrls());
                    if (adContentTag == null) {
                        vastProcessorResult.setResultCode(VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD);
                    } else if (processWrapper.isLast()) {
                        vastProcessorResult.recordError(adContentTag, processWrapper.getResultCode());
                    } else {
                        vastProcessorResult.recordError(adContentTag, VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD);
                    }
                    if (i == 0 && !wrapperAttributes.isFallbackOnNoAd()) {
                        return vastProcessorResult;
                    }
                }
                popToAdContent(adContentTag2);
            }
        }
        if (vastProcessorResult.getResultCode() == -1 && adContentTag != null) {
            vastProcessorResult.recordError(adContentTag, VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD);
        }
        return vastProcessorResult;
    }

    @NonNull
    private VastProcessorResult processInLine(InLineAdTag inLineAdTag) {
        this.adContentTagStack.push(inLineAdTag);
        VastProcessorResult vastProcessorResult = new VastProcessorResult();
        Pair findCreative = findCreative(inLineAdTag);
        if (findCreative == null) {
            vastProcessorResult.recordError(inLineAdTag, 101);
        } else if (findCreative.first == null && findCreative.second == null) {
            vastProcessorResult.recordError(inLineAdTag, VastError.ERROR_CODE_BAD_FILE);
        } else {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            EnumMap enumMap = new EnumMap(TrackingEvent.class);
            AppodealExtensionTag appodealExtensionTag = null;
            if (!this.adContentTagStack.empty()) {
                Iterator it = this.adContentTagStack.iterator();
                while (it.hasNext()) {
                    AdContentTag adContentTag = (AdContentTag) it.next();
                    if (adContentTag != null) {
                        if (adContentTag.getImpressionUrlList() != null) {
                            arrayList.addAll(adContentTag.getImpressionUrlList());
                        }
                        if (adContentTag.getCreativeTagList() != null) {
                            for (CreativeTag creativeTag : adContentTag.getCreativeTagList()) {
                                if (creativeTag != null) {
                                    CreativeContentTag creativeContentTag = creativeTag.getCreativeContentTag();
                                    if (creativeContentTag instanceof LinearCreativeTag) {
                                        LinearCreativeTag linearCreativeTag = (LinearCreativeTag) creativeContentTag;
                                        VideoClicksTag videoClicksTag = linearCreativeTag.getVideoClicksTag();
                                        if (!(videoClicksTag == null || videoClicksTag.getClickTrackingUrlList() == null)) {
                                            arrayList2.addAll(videoClicksTag.getClickTrackingUrlList());
                                        }
                                        if (linearCreativeTag.getTrackingEventListMap() != null) {
                                            enumMap.putAll(linearCreativeTag.getTrackingEventListMap());
                                        }
                                    }
                                }
                            }
                        }
                        if (appodealExtensionTag == null && adContentTag.getExtensionTagList() != null) {
                            Iterator it2 = adContentTag.getExtensionTagList().iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    break;
                                }
                                ExtensionTag extensionTag = (ExtensionTag) it2.next();
                                if (extensionTag instanceof AppodealExtensionTag) {
                                    appodealExtensionTag = (AppodealExtensionTag) extensionTag;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            VastAd vastAd = new VastAd((LinearCreativeTag) findCreative.first, (MediaFileTag) findCreative.second);
            vastAd.setImpressionUrlList(arrayList);
            vastAd.setErrorUrlList(getAllErrorUrls());
            vastAd.setClickTrackingUrlList(arrayList2);
            vastAd.setTrackingEventListMap(enumMap);
            vastAd.setCompanionTagList(getAllCompanion(inLineAdTag));
            vastAd.setAppodealExtension(appodealExtensionTag);
            vastProcessorResult.setResultCode(0);
            vastProcessorResult.setVastAd(vastAd);
        }
        return vastProcessorResult;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0155 A[SYNTHETIC, Splitter:B:105:0x0155] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0162 A[SYNTHETIC, Splitter:B:111:0x0162] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0113 A[SYNTHETIC, Splitter:B:75:0x0113] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0129 A[SYNTHETIC, Splitter:B:85:0x0129] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x013f A[SYNTHETIC, Splitter:B:95:0x013f] */
    @VisibleForTesting
    @NonNull
    public VastProcessorResult processWrapper(WrapperAdTag wrapperAdTag) {
        VastProcessorResult vastProcessorResult = new VastProcessorResult();
        if (isWrapperLimitExceeded()) {
            VastLog.e(TAG, String.format(Locale.ENGLISH, "VAST wrapping exceeded max limit of %d", new Object[]{Integer.valueOf(this.maxProcessingWrapper)}));
            vastProcessorResult.recordError(wrapperAdTag, 302);
            return vastProcessorResult;
        }
        this.currentProcessingWrapper++;
        this.adContentTagStack.push(wrapperAdTag);
        if (TextUtils.isEmpty(wrapperAdTag.getVastAdTagUri())) {
            VastLog.e(TAG, "VASTAdTagURI is null or empty");
            vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_BAD_URI);
            return vastProcessorResult;
        }
        WrapperAttributes wrapperAttributes = new WrapperAttributes(wrapperAdTag);
        InputStream inputStream = null;
        try {
            URLConnection openConnection = new URL(wrapperAdTag.getVastAdTagUri()).openConnection();
            int responseCode = ((HttpURLConnection) openConnection).getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream2 = openConnection.getInputStream();
                try {
                    VastTag parseVast = VastParser.parseVast(inputStream2);
                    if (parseVast == null) {
                        VastLog.e(TAG, "Invalid Vast");
                        vastProcessorResult.recordError(wrapperAdTag, 101);
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (Exception e) {
                                VastLog.e(TAG, (Throwable) e);
                            }
                        }
                        return vastProcessorResult;
                    } else if (!parseVast.hasAd()) {
                        VastLog.e(TAG, "Vast has no ad");
                        vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD);
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (Exception e2) {
                                VastLog.e(TAG, (Throwable) e2);
                            }
                        }
                        return vastProcessorResult;
                    } else if (parseVast.getAdTagList().size() <= 1 || wrapperAttributes.isAllowMultipleAds()) {
                        VastProcessorResult processAds = processAds(wrapperAdTag, parseVast, wrapperAttributes);
                        processAds.setLast(false);
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (Exception e3) {
                                VastLog.e(TAG, (Throwable) e3);
                            }
                        }
                        return processAds;
                    } else {
                        vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_GENERAL_WRAPPER);
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (Exception e4) {
                                VastLog.e(TAG, (Throwable) e4);
                            }
                        }
                        return vastProcessorResult;
                    }
                } catch (MalformedURLException e5) {
                    e = e5;
                    inputStream = inputStream2;
                    VastLog.e(TAG, (Throwable) e);
                    vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_BAD_URI);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e6) {
                            VastLog.e(TAG, (Throwable) e6);
                        }
                    }
                    return vastProcessorResult;
                } catch (SocketTimeoutException | UnknownHostException | SSLException | ConnectTimeoutException e7) {
                    e = e7;
                    inputStream = inputStream2;
                    VastLog.e(TAG, e);
                    vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_BAD_URI);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e8) {
                            VastLog.e(TAG, (Throwable) e8);
                        }
                    }
                    return vastProcessorResult;
                } catch (IOException e9) {
                    e = e9;
                    inputStream = inputStream2;
                    VastLog.e(TAG, (Throwable) e);
                    vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_BAD_URI);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e10) {
                            VastLog.e(TAG, (Throwable) e10);
                        }
                    }
                    return vastProcessorResult;
                } catch (Exception e11) {
                    e = e11;
                    inputStream = inputStream2;
                    try {
                        VastLog.e(TAG, (Throwable) e);
                        vastProcessorResult.recordError(wrapperAdTag, 100);
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (Exception e12) {
                                VastLog.e(TAG, (Throwable) e12);
                            }
                        }
                        return vastProcessorResult;
                    } catch (Throwable th) {
                        th = th;
                        if (inputStream != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    inputStream = inputStream2;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e13) {
                            VastLog.e(TAG, (Throwable) e13);
                        }
                    }
                    throw th;
                }
            } else if (responseCode != 204) {
                vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_BAD_URI);
                return vastProcessorResult;
            } else {
                VastLog.e(TAG, "Wrapper response code: 204");
                vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD);
                return vastProcessorResult;
            }
        } catch (MalformedURLException e14) {
            e = e14;
            VastLog.e(TAG, (Throwable) e);
            vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_BAD_URI);
            if (inputStream != null) {
            }
            return vastProcessorResult;
        } catch (SocketTimeoutException | UnknownHostException | SSLException | ConnectTimeoutException e15) {
            e = e15;
            VastLog.e(TAG, e);
            vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_BAD_URI);
            if (inputStream != null) {
            }
            return vastProcessorResult;
        } catch (IOException e16) {
            e = e16;
            VastLog.e(TAG, (Throwable) e);
            vastProcessorResult.recordError(wrapperAdTag, VastError.ERROR_CODE_BAD_URI);
            if (inputStream != null) {
            }
            return vastProcessorResult;
        } catch (Exception e17) {
            e = e17;
            VastLog.e(TAG, (Throwable) e);
            vastProcessorResult.recordError(wrapperAdTag, 100);
            if (inputStream != null) {
            }
            return vastProcessorResult;
        }
    }

    private boolean isWrapperLimitExceeded() {
        return this.currentProcessingWrapper >= this.maxProcessingWrapper;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void popToAdContent(AdContentTag adContentTag) {
        if (!this.adContentTagStack.empty()) {
            int search = this.adContentTagStack.search(adContentTag);
            for (int i = 0; i < search; i++) {
                this.adContentTagStack.pop();
            }
        }
    }

    private ArrayList<CompanionTag> getAllCompanion(AdContentTag adContentTag) {
        ArrayList<CompanionTag> arrayList = new ArrayList<>();
        for (CreativeTag creativeTag : adContentTag.getCreativeTagList()) {
            if (creativeTag != null) {
                CreativeContentTag creativeContentTag = creativeTag.getCreativeContentTag();
                if (creativeContentTag instanceof CompanionAdsCreativeTag) {
                    CompanionAdsCreativeTag companionAdsCreativeTag = (CompanionAdsCreativeTag) creativeContentTag;
                    if (companionAdsCreativeTag.getCompanionTagList() != null) {
                        arrayList.addAll(companionAdsCreativeTag.getCompanionTagList());
                    }
                }
            }
        }
        return arrayList;
    }

    private ArrayList<String> getAllErrorUrls() {
        ArrayList<String> arrayList = new ArrayList<>();
        if (this.adContentTagStack.empty()) {
            return arrayList;
        }
        Iterator it = this.adContentTagStack.iterator();
        while (it.hasNext()) {
            AdContentTag adContentTag = (AdContentTag) it.next();
            if (!(adContentTag == null || adContentTag.getErrorUrlList() == null)) {
                arrayList.addAll(adContentTag.getErrorUrlList());
            }
        }
        return arrayList;
    }

    private Pair<LinearCreativeTag, MediaFileTag> findCreative(InLineAdTag inLineAdTag) {
        ArrayList arrayList = new ArrayList();
        for (CreativeTag creativeTag : inLineAdTag.getCreativeTagList()) {
            if (creativeTag != null) {
                CreativeContentTag creativeContentTag = creativeTag.getCreativeContentTag();
                if (creativeContentTag instanceof LinearCreativeTag) {
                    LinearCreativeTag linearCreativeTag = (LinearCreativeTag) creativeContentTag;
                    List<MediaFileTag> mediaFileTagList = linearCreativeTag.getMediaFileTagList();
                    if (mediaFileTagList != null && !mediaFileTagList.isEmpty()) {
                        for (MediaFileTag pair : mediaFileTagList) {
                            arrayList.add(new Pair(linearCreativeTag, pair));
                        }
                    }
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        Pair<LinearCreativeTag, MediaFileTag> pickVideo = this.mediaPicker != null ? this.mediaPicker.pickVideo(arrayList) : null;
        if (pickVideo != null) {
            return pickVideo;
        }
        return new Pair<>(null, null);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void sendErrors(@NonNull List<String> list) {
        this.vastRequest.fireErrorUrls(list, null);
    }
}
