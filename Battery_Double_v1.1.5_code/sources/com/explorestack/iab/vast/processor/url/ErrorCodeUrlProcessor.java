package com.explorestack.iab.vast.processor.url;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.vast.VastLog;
import com.explorestack.iab.vast.VastRequest;

public class ErrorCodeUrlProcessor implements UrlProcessor {
    private static final String TAG = "VastAdUrlProcessor";

    @Nullable
    public String prepare(@Nullable String str, @Nullable Bundle bundle) {
        if (!TextUtils.isEmpty(str) && bundle != null && bundle.containsKey(VastRequest.PARAMS_ERROR_CODE)) {
            int i = bundle.getInt(VastRequest.PARAMS_ERROR_CODE, -1);
            if (i > -1) {
                VastLog.d(TAG, String.format("Before prepare url: %s", new Object[]{str}));
                if (str.contains("[ERRORCODE]")) {
                    str = str.replace("[ERRORCODE]", String.valueOf(i));
                }
                if (str.contains("%5BERRORCODE%5D")) {
                    str = str.replace("%5BERRORCODE%5D", String.valueOf(i));
                }
                VastLog.d(TAG, String.format("After prepare url: %s", new Object[]{str}));
            }
        }
        return str;
    }
}
