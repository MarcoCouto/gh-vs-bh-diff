package com.explorestack.iab.vast;

import android.content.Context;
import android.support.annotation.NonNull;

public interface VastErrorListener {
    void onVastError(@NonNull Context context, @NonNull VastRequest vastRequest, int i);
}
