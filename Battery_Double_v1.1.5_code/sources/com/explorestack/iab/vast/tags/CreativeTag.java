package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class CreativeTag extends VastXmlTag {
    private static final String[] supportedAttributes = {"id", VastAttributes.AD_ID};
    private CreativeContentTag creativeContentTag;

    CreativeTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.CREATIVE);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, VastTagName.LINEAR)) {
                    this.creativeContentTag = new LinearCreativeTag(xmlPullParser);
                } else if (isEquals(name, VastTagName.COMPANION_ADS)) {
                    this.creativeContentTag = new CompanionAdsCreativeTag(xmlPullParser);
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.CREATIVE);
    }

    public CreativeContentTag getCreativeContentTag() {
        return this.creativeContentTag;
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
