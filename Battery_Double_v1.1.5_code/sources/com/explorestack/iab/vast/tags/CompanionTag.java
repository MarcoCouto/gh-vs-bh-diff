package com.explorestack.iab.vast.tags;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import com.explorestack.iab.mraid.internal.MRAIDHtmlProcessor;
import com.explorestack.iab.vast.TrackingEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

public class CompanionTag extends VastXmlTag {
    private static final String[] supportedAttributes = {"width", "height", "id", VastAttributes.ASSET_WIDTH, VastAttributes.ASSET_HEIGHT, VastAttributes.EXPANDED_WIDTH, VastAttributes.EXPANDED_HEIGHT, VastAttributes.API_FRAMEWORK, VastAttributes.AD_SLOT_ID, VastAttributes.REQUIRED};
    private String adParameters;
    private String companionClickThrough;
    private List<String> companionClickTrackingList;
    private String htmlResource;
    private String iFrameResource;
    private StaticResourceTag staticResourceTag;
    private Map<TrackingEvent, List<String>> trackingEventListMap;

    CompanionTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.COMPANION);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, VastTagName.STATIC_RESOURCE)) {
                    StaticResourceTag staticResourceTag2 = new StaticResourceTag(xmlPullParser);
                    if (staticResourceTag2.isValidTag()) {
                        setStaticResourceTag(staticResourceTag2);
                    }
                } else if (isEquals(name, VastTagName.I_FRAME_RESOURCE)) {
                    setIFrameResource(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.HTML_RESOURCE)) {
                    setHtmlResource(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.COMPANION_CLICK_THROUGH)) {
                    setCompanionClickThrough(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.COMPANION_CLICK_TRACKING)) {
                    addClickTracking(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.TRACKING_EVENTS)) {
                    setTrackingEventListMap(new TrackingEventsTag(xmlPullParser).getTrackingEventListMap());
                } else if (isEquals(name, VastTagName.AD_PARAMETERS)) {
                    setAdParameters(parseText(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.COMPANION);
    }

    public StaticResourceTag getStaticResourceTag() {
        return this.staticResourceTag;
    }

    private void setStaticResourceTag(StaticResourceTag staticResourceTag2) {
        this.staticResourceTag = staticResourceTag2;
    }

    public String getIFrameResource() {
        return this.iFrameResource;
    }

    private void setIFrameResource(String str) {
        this.iFrameResource = str;
    }

    public String getHtmlResource() {
        return this.htmlResource;
    }

    public void setHtmlResource(String str) {
        this.htmlResource = str;
    }

    public String getCompanionClickThrough() {
        return this.companionClickThrough;
    }

    private void setCompanionClickThrough(String str) {
        this.companionClickThrough = str;
    }

    public List<String> getCompanionClickTrackingList() {
        return this.companionClickTrackingList;
    }

    private void addClickTracking(String str) {
        if (this.companionClickTrackingList == null) {
            this.companionClickTrackingList = new ArrayList();
        }
        this.companionClickTrackingList.add(str);
    }

    @Nullable
    public Map<TrackingEvent, List<String>> getTrackingEventListMap() {
        return this.trackingEventListMap;
    }

    private void setTrackingEventListMap(Map<TrackingEvent, List<String>> map) {
        this.trackingEventListMap = map;
    }

    public String getAdParameters() {
        return this.adParameters;
    }

    public void setAdParameters(String str) {
        this.adParameters = str;
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }

    public boolean isValidTag() {
        return !TextUtils.isEmpty(getAttributeValueByName("width")) && !TextUtils.isEmpty(getAttributeValueByName("height"));
    }

    @Nullable
    public Pair<String, Pair<Integer, Integer>> getHtmlForMraid(int i, int i2, float f) {
        if (this.htmlResource != null) {
            return new Pair<>(this.htmlResource, new Pair(Integer.valueOf(getWidth()), Integer.valueOf(getHeight())));
        }
        if (this.staticResourceTag != null) {
            float f2 = (float) i;
            float f3 = (float) i2;
            float f4 = f2 / f3;
            float width = ((float) getWidth()) / ((float) getHeight());
            if (!Float.isNaN(width)) {
                if (width <= f4) {
                    i = Math.round(f3 * width);
                } else {
                    i2 = Math.round(f2 / width);
                }
            }
            if (f != 0.0f) {
                i = Math.round(((float) i) / f);
                i2 = Math.round(((float) i2) / f);
            }
            return new Pair<>(String.format("<a href='%s'><img width='%s' height='%s' src='%s'/></a>", new Object[]{this.companionClickThrough, Integer.valueOf(i), Integer.valueOf(i2), this.staticResourceTag.getText()}), new Pair(Integer.valueOf(i), Integer.valueOf(i2)));
        } else if (this.iFrameResource == null) {
            return null;
        } else {
            return new Pair<>(String.format(Locale.ENGLISH, "<html style=\"overflow: hidden\"><body style=\"overflow: hidden\"><iframe style=\"overflow: hidden\" scrolling=\"no\" frameborder=\"no\" width=\"%d\" height=\"%d\" src=\"%s\"></iframe></body></html>", new Object[]{Integer.valueOf(i), Integer.valueOf(i2), this.iFrameResource}), new Pair(Integer.valueOf(0), Integer.valueOf(0)));
        }
    }

    public String getHtml(int i, int i2, float f) {
        int i3 = i;
        int i4 = i2;
        int width = getWidth();
        int height = getHeight();
        if (this.htmlResource != null) {
            String processRawHtml = MRAIDHtmlProcessor.processRawHtml(this.htmlResource);
            float f2 = (float) i3;
            float f3 = (float) i4;
            float f4 = f2 / f3;
            float f5 = ((float) width) / ((float) height);
            if (!Float.isNaN(f5)) {
                if (f5 <= f4) {
                    i3 = Math.round(f3 * f5);
                } else {
                    i4 = Math.round(f2 / f5);
                }
            }
            int round = Math.round(((float) i3) / f);
            int round2 = Math.round(((float) i4) / f);
            return String.format("<style type='text/css'>%s</style><div class='appodeal-outer'><div class='appodeal-middle'><div class='appodeal-inner'>%s</div></div></div>", new Object[]{String.format(Locale.ENGLISH, "body, p {margin:0; padding:0} img {max-width:%dpx; max-height:%dpx} #appnext-interstitial {min-width:%dpx; min-height:%dpx;}img[width='%d'][height='%d'] {width: %dpx; height: %dpx} .appodeal-outer {display: table; position: absolute; height: 100%%; width: 100%%;}.appodeal-middle {display: table-cell; vertical-align: middle;}.appodeal-inner {margin-left: auto; margin-right: auto; width: %dpx; height: %dpx;}.ad_slug_table {margin-left: auto !important; margin-right: auto !important;} #ad[align='center'] {height: %dpx;} #voxelPlayer {position: relative !important;} #lsm_mobile_ad #wrapper, #lsm_overlay {position: relative !important;}", new Object[]{Integer.valueOf(round), Integer.valueOf(round2), Integer.valueOf(round), Integer.valueOf(round2), Integer.valueOf(width), Integer.valueOf(height), Integer.valueOf(round), Integer.valueOf(round2), Integer.valueOf(round), Integer.valueOf(round2), Integer.valueOf(round2)}), processRawHtml});
        } else if (this.staticResourceTag != null) {
            String processRawHtml2 = MRAIDHtmlProcessor.processRawHtml(String.format("<a href='%s'><img width='%s' height='%s' src='%s'/></a>", new Object[]{this.companionClickThrough, Integer.valueOf(width), Integer.valueOf(height), this.staticResourceTag.getText()}));
            float f6 = (float) i3;
            float f7 = (float) i4;
            float f8 = f6 / f7;
            float f9 = ((float) width) / ((float) height);
            if (!Float.isNaN(f9)) {
                if (f9 <= f8) {
                    i3 = Math.round(f7 * f9);
                } else {
                    i4 = Math.round(f6 / f9);
                }
            }
            int round3 = Math.round(((float) i3) / f);
            int round4 = Math.round(((float) i4) / f);
            return String.format("<style type='text/css'>%s</style><div class='appodeal-outer'><div class='appodeal-middle'><div class='appodeal-inner'>%s</div></div></div>", new Object[]{String.format(Locale.ENGLISH, "body, p {margin:0; padding:0} img {max-width:%dpx; max-height:%dpx} #appnext-interstitial {min-width:%dpx; min-height:%dpx;}img[width='%d'][height='%d'] {width: %dpx; height: %dpx} .appodeal-outer {display: table; position: absolute; height: 100%%; width: 100%%;}.appodeal-middle {display: table-cell; vertical-align: middle;}.appodeal-inner {margin-left: auto; margin-right: auto; width: %dpx; height: %dpx;}.ad_slug_table {margin-left: auto !important; margin-right: auto !important;} #ad[align='center'] {height: %dpx;} #voxelPlayer {position: relative !important;} #lsm_mobile_ad #wrapper, #lsm_overlay {position: relative !important;}", new Object[]{Integer.valueOf(round3), Integer.valueOf(round4), Integer.valueOf(round3), Integer.valueOf(round4), Integer.valueOf(width), Integer.valueOf(height), Integer.valueOf(round3), Integer.valueOf(round4), Integer.valueOf(round3), Integer.valueOf(round4), Integer.valueOf(round4)}), processRawHtml2});
        } else if (this.iFrameResource == null) {
            return null;
        } else {
            return String.format("<html style=\"overflow: hidden\"><body style=\"overflow: hidden\"><iframe style=\"overflow: hidden\" scrolling=\"no\" frameborder=\"no\" width=\"%s\" height=\"%s\" src=\"%s\"></iframe></body></html>", new Object[]{Integer.valueOf(i), Integer.valueOf(i2), this.iFrameResource});
        }
    }

    public boolean isValid(int i, int i2) {
        Pair htmlForMraid = getHtmlForMraid(i, i2, 1.0f);
        return (htmlForMraid == null || htmlForMraid.first == null || htmlForMraid.second == null || getHtml(i, i2, 1.0f) == null) ? false : true;
    }

    public int getWidth() {
        return getIntegerAttributeValueByName("width");
    }

    public int getHeight() {
        return getIntegerAttributeValueByName("height");
    }
}
