package com.explorestack.iab.vast.tags;

import android.text.TextUtils;
import org.xmlpull.v1.XmlPullParser;

public class MediaFileTag extends VastXmlTag {
    private static final String[] supportedAttributes = {VastAttributes.DELIVERY, "type", "width", "height", VastAttributes.CODEC, "id", VastAttributes.BITRATE, VastAttributes.MIN_BITRATE, VastAttributes.MAX_BITRATE, VastAttributes.SCALABLE, VastAttributes.MAINTAIN_ASPECT_RATION, VastAttributes.API_FRAMEWORK};

    public boolean isTextSupported() {
        return true;
    }

    MediaFileTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }

    public boolean isValidTag() {
        return !TextUtils.isEmpty(getAttributeValueByName("type")) && !TextUtils.isEmpty(getAttributeValueByName("width")) && !TextUtils.isEmpty(getAttributeValueByName("height")) && !TextUtils.isEmpty(getText());
    }

    public int getWidth() {
        return getIntegerAttributeValueByName("width");
    }

    public int getHeight() {
        return getIntegerAttributeValueByName("height");
    }

    public String getType() {
        return getAttributeValueByName("type");
    }

    public String getApiFramework() {
        return getAttributeValueByName(VastAttributes.API_FRAMEWORK);
    }
}
