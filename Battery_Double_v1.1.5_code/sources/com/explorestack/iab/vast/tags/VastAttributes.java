package com.explorestack.iab.vast.tags;

public class VastAttributes {
    public static final String AD_ID = "adID";
    public static final String AD_SLOT_ID = "adSlotID";
    public static final String ALLOW_MULTIPLE_ADS = "allowMultipleAds";
    public static final String API_FRAMEWORK = "apiFramework";
    public static final String APPODEAL = "appodeal";
    public static final String ASSET_HEIGHT = "assetHeight";
    public static final String ASSET_WIDTH = "assetWidth";
    public static final String BITRATE = "bitrate";
    public static final String CODEC = "codec";
    public static final String CREATIVE_TYPE = "creativeType";
    public static final String DELIVERY = "delivery";
    public static final String EVENT = "event";
    public static final String EXPANDED_HEIGHT = "expandedHeight";
    public static final String EXPANDED_WIDTH = "expandedWidth";
    public static final String FALLBACK_ON_NO_AD = "fallbackOnNoAd";
    public static final String FOLLOW_ADDITIONAL_WRAPPERS = "followAdditionalWrappers";
    public static final String HEIGHT = "height";
    public static final String ID = "id";
    public static final String MAINTAIN_ASPECT_RATION = "maintainAspectRatio";
    public static final String MAX_BITRATE = "maxBitrate";
    public static final String MIN_BITRATE = "minBitrate";
    public static final String REQUIRED = "required";
    public static final String SCALABLE = "scalable";
    public static final String SKIP_OFFSET = "skipoffset";
    public static final String TYPE = "type";
    public static final String VERSION = "version";
    public static final String WIDTH = "width";
}
