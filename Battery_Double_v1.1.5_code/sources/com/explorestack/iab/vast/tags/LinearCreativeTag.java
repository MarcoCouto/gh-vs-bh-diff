package com.explorestack.iab.vast.tags;

import com.explorestack.iab.vast.TrackingEvent;
import com.explorestack.iab.vast.VastLog;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

public class LinearCreativeTag extends CreativeContentTag {
    private static final String[] supportedAttributes = {VastAttributes.SKIP_OFFSET};
    private String adParameters;
    private String duration;
    private List<MediaFileTag> mediaFileTagList;
    private int skipOffsetSec = -1;
    private EnumMap<TrackingEvent, List<String>> trackingEventListMap;
    private VideoClicksTag videoClicksTag;

    LinearCreativeTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.LINEAR);
        int transformHoursTimeToSec = transformHoursTimeToSec(getAttributeValueByName(VastAttributes.SKIP_OFFSET));
        if (transformHoursTimeToSec > -1) {
            setSkipOffsetSec(transformHoursTimeToSec);
        }
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, VastTagName.DURATION)) {
                    setDuration(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.MEDIA_FILES)) {
                    setMediaFileTagList(parseMediaFiles(xmlPullParser));
                } else if (isEquals(name, VastTagName.VIDEO_CLICKS)) {
                    setVideoClicksTag(new VideoClicksTag(xmlPullParser));
                } else if (isEquals(name, VastTagName.AD_PARAMETERS)) {
                    setAdParameters(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.TRACKING_EVENTS)) {
                    setTrackingEventListMap(new TrackingEventsTag(xmlPullParser).getTrackingEventListMap());
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.LINEAR);
    }

    private static List<MediaFileTag> parseMediaFiles(XmlPullParser xmlPullParser) throws Exception {
        xmlPullParser.require(2, null, VastTagName.MEDIA_FILES);
        ArrayList arrayList = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (isEquals(xmlPullParser.getName(), VastTagName.MEDIA_FILE)) {
                    MediaFileTag mediaFileTag = new MediaFileTag(xmlPullParser);
                    if (mediaFileTag.isValidTag()) {
                        arrayList.add(mediaFileTag);
                    } else {
                        VastLog.d("VastXmlTag", "MediaFile: is not valid. Skipping it.");
                        skip(xmlPullParser);
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.MEDIA_FILES);
        return arrayList;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String str) {
        this.duration = str;
    }

    public List<MediaFileTag> getMediaFileTagList() {
        return this.mediaFileTagList;
    }

    private void setMediaFileTagList(List<MediaFileTag> list) {
        this.mediaFileTagList = list;
    }

    public VideoClicksTag getVideoClicksTag() {
        return this.videoClicksTag;
    }

    private void setVideoClicksTag(VideoClicksTag videoClicksTag2) {
        this.videoClicksTag = videoClicksTag2;
    }

    public String getAdParameters() {
        return this.adParameters;
    }

    public void setAdParameters(String str) {
        this.adParameters = str;
    }

    public Map<TrackingEvent, List<String>> getTrackingEventListMap() {
        return this.trackingEventListMap;
    }

    private void setTrackingEventListMap(EnumMap<TrackingEvent, List<String>> enumMap) {
        this.trackingEventListMap = enumMap;
    }

    public int getSkipOffsetSec() {
        return this.skipOffsetSec;
    }

    private void setSkipOffsetSec(int i) {
        this.skipOffsetSec = i;
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
