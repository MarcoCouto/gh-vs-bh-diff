package com.explorestack.iab.vast.tags;

import android.text.TextUtils;
import org.xmlpull.v1.XmlPullParser;

public class StaticResourceTag extends VastXmlTag {
    private static final String SUPPORTED_STATIC_TYPE_REGEX = "image/.*(?i)(gif|jpeg|jpg|bmp|png)";
    private static final String[] supportedAttributes = {VastAttributes.CREATIVE_TYPE};

    public boolean isTextSupported() {
        return true;
    }

    StaticResourceTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }

    public boolean isValidTag() {
        String attributeValueByName = getAttributeValueByName(VastAttributes.CREATIVE_TYPE);
        if (!TextUtils.isEmpty(attributeValueByName)) {
            return attributeValueByName.matches(SUPPORTED_STATIC_TYPE_REGEX);
        }
        return false;
    }
}
