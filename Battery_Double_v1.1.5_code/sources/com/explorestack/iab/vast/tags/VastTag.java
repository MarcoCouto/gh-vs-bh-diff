package com.explorestack.iab.vast.tags;

import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class VastTag extends VastXmlTag {
    private static final String[] supportedAttributes = {"version"};
    private final List<AdTag> adTagList = new ArrayList();

    VastTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.VAST);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (isEquals(xmlPullParser.getName(), VastTagName.AD)) {
                    this.adTagList.add(new AdTag(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.VAST);
    }

    public List<AdTag> getAdTagList() {
        return this.adTagList;
    }

    public boolean hasAd() {
        return this.adTagList != null && this.adTagList.size() > 0;
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
