package com.explorestack.iab.vast.tags;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.vast.VastLog;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import org.xmlpull.v1.XmlPullParser;

public class VastXmlTag implements Serializable {
    static final String TAG = "VastXmlTag";
    private HashMap<String, String> attributes;
    private String text;

    public boolean isTextSupported() {
        return false;
    }

    public boolean isValidTag() {
        return true;
    }

    public VastXmlTag(XmlPullParser xmlPullParser) throws Exception {
        setAttributes(parseAttributes(this, xmlPullParser));
        if (isTextSupported()) {
            setText(parseText(xmlPullParser));
        }
    }

    public final String getText() {
        return this.text;
    }

    public final void setText(String str) {
        this.text = str;
    }

    private boolean hasAttributes() {
        return this.attributes != null && !this.attributes.isEmpty();
    }

    private void setAttributes(HashMap<String, String> hashMap) {
        this.attributes = hashMap;
    }

    /* access modifiers changed from: 0000 */
    public final String getAttributeValueByName(@NonNull String str) {
        if (hasAttributes()) {
            return (String) this.attributes.get(str);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public final int getIntegerAttributeValueByName(@NonNull String str) {
        int i;
        String attributeValueByName = getAttributeValueByName(str);
        if (TextUtils.isEmpty(attributeValueByName)) {
            VastLog.d(TAG, String.format(Locale.ENGLISH, "Error parse attribute value - %s, by name - %s", new Object[]{attributeValueByName, str}));
            return -1;
        }
        try {
            i = Integer.parseInt(attributeValueByName);
        } catch (Exception unused) {
            VastLog.e(TAG, String.format(Locale.ENGLISH, "Error parse attribute value - %s, by name - %s", new Object[]{attributeValueByName, str}));
            i = -1;
        }
        return i;
    }

    public final boolean getBooleanAttributeValueByName(@NonNull String str, boolean z) {
        String attributeValueByName = getAttributeValueByName(str);
        if (valueIsTrue(attributeValueByName)) {
            return true;
        }
        if (valueIsFalse(attributeValueByName)) {
            return false;
        }
        return z;
    }

    public String[] getSupportedAttributes() {
        return new String[0];
    }

    private boolean isSupportedAttribute(@Nullable String str) {
        String[] supportedAttributes = getSupportedAttributes();
        if (supportedAttributes != null && supportedAttributes.length > 0) {
            for (String equals : supportedAttributes) {
                if (equals.equals(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static HashMap<String, String> parseAttributes(@NonNull VastXmlTag vastXmlTag, @NonNull XmlPullParser xmlPullParser) {
        if (xmlPullParser.getAttributeCount() <= 0) {
            return null;
        }
        HashMap<String, String> hashMap = new HashMap<>();
        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
            if (vastXmlTag.isSupportedAttribute(xmlPullParser.getAttributeName(i))) {
                hashMap.put(xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
            }
        }
        return hashMap;
    }

    static String parseText(@NonNull XmlPullParser xmlPullParser) throws Exception {
        if (xmlPullParser.next() != 4) {
            return null;
        }
        String text2 = xmlPullParser.getText();
        if (!TextUtils.isEmpty(text2)) {
            text2 = text2.trim();
        }
        xmlPullParser.nextTag();
        return text2;
    }

    static boolean parseBoolean(@NonNull XmlPullParser xmlPullParser) throws Exception {
        return valueIsTrue(parseText(xmlPullParser));
    }

    static boolean valueIsTrue(@Nullable String str) {
        return str != null && (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("1"));
    }

    static boolean valueIsFalse(@Nullable String str) {
        return str != null && (str.equalsIgnoreCase("false") || str.equalsIgnoreCase("0"));
    }

    static int transformMinTimeToSec(@Nullable String str) {
        if (str != null) {
            String[] split = str.split(":");
            if (split.length > 1) {
                try {
                    return (Integer.parseInt(split[0]) * 60) + Integer.parseInt(split[1]);
                } catch (Exception e) {
                    VastLog.e(TAG, (Throwable) e);
                }
            }
        }
        return -1;
    }

    static int transformHoursTimeToSec(@Nullable String str) {
        if (str != null) {
            int indexOf = str.indexOf(".");
            if (indexOf > 0) {
                str = str.substring(0, indexOf);
            }
            String[] split = str.split(":");
            if (split.length > 2) {
                try {
                    return (Integer.parseInt(split[0]) * 60 * 60) + (Integer.parseInt(split[1]) * 60) + Integer.parseInt(split[2]);
                } catch (Exception e) {
                    VastLog.e(TAG, (Throwable) e);
                }
            }
        }
        return -1;
    }

    static int transformColorHexToInt(String str) {
        try {
            return Color.parseColor(str);
        } catch (Exception e) {
            VastLog.e(TAG, e.getMessage());
            return -1;
        }
    }

    static boolean isEquals(@Nullable String str, @Nullable String str2) {
        return str != null && str.equalsIgnoreCase(str2);
    }

    static void skip(@NonNull XmlPullParser xmlPullParser) throws Exception {
        if (xmlPullParser.getEventType() == 2) {
            int i = 1;
            while (i != 0) {
                switch (xmlPullParser.next()) {
                    case 2:
                        i++;
                        break;
                    case 3:
                        i--;
                        break;
                }
            }
            return;
        }
        throw new IllegalStateException();
    }
}
