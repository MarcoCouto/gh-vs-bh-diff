package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class AdTag extends VastXmlTag {
    private static final String[] supportedAttributes = {"id"};
    private AdContentTag adContentTag;

    AdTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.AD);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, VastTagName.IN_LINE)) {
                    this.adContentTag = new InLineAdTag(xmlPullParser);
                } else if (isEquals(name, VastTagName.WRAPPER)) {
                    this.adContentTag = new WrapperAdTag(xmlPullParser);
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.AD);
    }

    public AdContentTag getAdContentTag() {
        return this.adContentTag;
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
