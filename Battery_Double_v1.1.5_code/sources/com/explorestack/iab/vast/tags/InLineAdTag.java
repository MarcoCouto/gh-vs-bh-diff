package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class InLineAdTag extends AdContentTag {
    InLineAdTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.IN_LINE);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, VastTagName.CREATIVES)) {
                    setCreativeTagList(parseCreatives(xmlPullParser));
                } else if (isEquals(name, VastTagName.EXTENSIONS)) {
                    setExtensionTagList(parseExtensions(xmlPullParser));
                } else if (isEquals(name, VastTagName.IMPRESSION)) {
                    addImpressionUrl(parseText(xmlPullParser));
                } else if (isEquals(name, "Error")) {
                    addErrorUrl(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.AD_SYSTEM)) {
                    setAdSystemTag(new AdSystemTag(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.IN_LINE);
    }
}
