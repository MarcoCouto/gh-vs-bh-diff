package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class CreativeContentTag extends VastXmlTag {
    CreativeContentTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
    }
}
