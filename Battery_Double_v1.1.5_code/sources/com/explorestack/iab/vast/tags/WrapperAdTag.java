package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class WrapperAdTag extends AdContentTag {
    private static final String[] supportedAttributes = {VastAttributes.FOLLOW_ADDITIONAL_WRAPPERS, VastAttributes.ALLOW_MULTIPLE_ADS, VastAttributes.FALLBACK_ON_NO_AD};
    private String vastAdTagUri;

    public WrapperAdTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.WRAPPER);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, VastTagName.CREATIVES)) {
                    setCreativeTagList(parseCreatives(xmlPullParser));
                } else if (isEquals(name, VastTagName.EXTENSIONS)) {
                    setExtensionTagList(parseExtensions(xmlPullParser));
                } else if (isEquals(name, VastTagName.IMPRESSION)) {
                    addImpressionUrl(parseText(xmlPullParser));
                } else if (isEquals(name, "Error")) {
                    addErrorUrl(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.AD_SYSTEM)) {
                    setAdSystemTag(new AdSystemTag(xmlPullParser));
                } else if (isEquals(name, VastTagName.VAST_AD_TAG_URI)) {
                    setVastAdTagUri(parseText(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.WRAPPER);
    }

    public String getVastAdTagUri() {
        return this.vastAdTagUri;
    }

    private void setVastAdTagUri(String str) {
        this.vastAdTagUri = str;
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
