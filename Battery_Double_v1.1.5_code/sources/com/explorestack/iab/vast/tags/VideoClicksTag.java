package com.explorestack.iab.vast.tags;

import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class VideoClicksTag extends VastXmlTag {
    private String clickThroughUrl;
    private List<String> clickTrackingUrlList;
    private List<String> customClickUrlList;

    VideoClicksTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.VIDEO_CLICKS);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, VastTagName.CLICK_THROUGH)) {
                    setClickThroughUrl(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.CLICK_TRACKING)) {
                    addClickTracking(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.CUSTOM_CLICK)) {
                    addCustomClick(parseText(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.VIDEO_CLICKS);
    }

    public String getClickThroughUrl() {
        return this.clickThroughUrl;
    }

    private void setClickThroughUrl(String str) {
        this.clickThroughUrl = str;
    }

    public List<String> getClickTrackingUrlList() {
        return this.clickTrackingUrlList;
    }

    private void addClickTracking(String str) {
        if (this.clickTrackingUrlList == null) {
            this.clickTrackingUrlList = new ArrayList();
        }
        this.clickTrackingUrlList.add(str);
    }

    /* access modifiers changed from: 0000 */
    public List<String> getCustomClickUrlList() {
        return this.customClickUrlList;
    }

    private void addCustomClick(String str) {
        if (this.customClickUrlList == null) {
            this.customClickUrlList = new ArrayList();
        }
        this.customClickUrlList.add(str);
    }
}
