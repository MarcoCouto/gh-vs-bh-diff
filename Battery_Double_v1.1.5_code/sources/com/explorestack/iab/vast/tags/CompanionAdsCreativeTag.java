package com.explorestack.iab.vast.tags;

import com.explorestack.iab.vast.VastLog;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class CompanionAdsCreativeTag extends CreativeContentTag {
    private final List<CompanionTag> companionTagList = new ArrayList();

    CompanionAdsCreativeTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, VastTagName.COMPANION_ADS);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (isEquals(xmlPullParser.getName(), VastTagName.COMPANION)) {
                    CompanionTag companionTag = new CompanionTag(xmlPullParser);
                    if (companionTag.isValidTag()) {
                        this.companionTagList.add(companionTag);
                    } else {
                        VastLog.d("VastXmlTag", "Creative Companion: is not valid. Skipping it.");
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, VastTagName.COMPANION_ADS);
    }

    public List<CompanionTag> getCompanionTagList() {
        return this.companionTagList;
    }
}
