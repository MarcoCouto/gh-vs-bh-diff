package com.explorestack.iab.utils;

import android.text.TextUtils;
import android.util.Log;
import com.ironsource.sdk.constants.Constants.RequestParameters;

public class Logger {
    private static LogLevel level = LogLevel.error;
    private final String tag;

    public enum LogLevel {
        debug(1),
        info(2),
        warning(3),
        error(4),
        none(5);
        
        private int value;

        private LogLevel(int i) {
            this.value = i;
        }

        public int getValue() {
            return this.value;
        }
    }

    public Logger(String str) {
        this.tag = str;
    }

    public void d(String str) {
        if (mayLogMessage(LogLevel.debug, str)) {
            Log.d(this.tag, str);
        }
    }

    public void d(String str, String str2) {
        if (mayLogMessage(LogLevel.debug, str2)) {
            String str3 = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            Log.d(str3, sb.toString());
        }
    }

    public void e(String str) {
        if (mayLogMessage(LogLevel.error, str)) {
            Log.e(this.tag, str);
        }
    }

    public void e(String str, String str2) {
        if (mayLogMessage(LogLevel.error, str2)) {
            String str3 = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            Log.e(str3, sb.toString());
        }
    }

    public void e(String str, Throwable th) {
        if (mayLogMessage(LogLevel.error, str)) {
            Log.e(this.tag, str, th);
        }
    }

    public void e(String str, String str2, Throwable th) {
        if (mayLogMessage(LogLevel.error, str2)) {
            String str3 = this.tag;
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            Log.e(str3, sb.toString(), th);
        }
    }

    public void setLoggingLevel(LogLevel logLevel) {
        Log.d(this.tag, String.format("Changing logging level. From: %s, To: %s", new Object[]{level, logLevel}));
        level = logLevel;
    }

    public LogLevel getLoggingLevel() {
        return level;
    }

    private boolean mayLogMessage(LogLevel logLevel, String str) {
        return !TextUtils.isEmpty(str) && hasNecessaryLevel(logLevel);
    }

    private boolean hasNecessaryLevel(LogLevel logLevel) {
        return (level == null || logLevel == null || level.getValue() > logLevel.getValue()) ? false : true;
    }
}
