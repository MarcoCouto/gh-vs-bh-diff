package com.explorestack.iab.mraid.nativefeature;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import com.explorestack.iab.mraid.internal.MRAIDLog;
import com.explorestack.iab.mraid.internal.MRAIDNativeFeatureManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class MRAIDNativeFeatureProvider {
    private static final String TAG = "MRAIDNativeFeatureProvider";
    private final Context context;
    private final MRAIDNativeFeatureManager nativeFeatureManager;

    public MRAIDNativeFeatureProvider(Context context2, MRAIDNativeFeatureManager mRAIDNativeFeatureManager) {
        this.context = context2;
        this.nativeFeatureManager = mRAIDNativeFeatureManager;
    }

    public final void callTel(String str) {
        if (this.nativeFeatureManager.isTelSupported()) {
            this.context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
        }
    }

    @SuppressLint({"SimpleDateFormat"})
    @TargetApi(14)
    public void createCalendarEvent(String str) {
        long j;
        if (this.nativeFeatureManager.isCalendarSupported()) {
            try {
                JSONObject jSONObject = new JSONObject(str.replace("\\", "").replace("\"{", "{").replace("}\"", "}"));
                String optString = jSONObject.optString("description", "Untitled");
                String optString2 = jSONObject.optString("location", "unknown");
                String optString3 = jSONObject.optString("summary");
                String[] strArr = {"yyyy-MM-dd'T'HH:mmZ", "yyyy-MM-dd'T'HH:mm:ssZ"};
                String[] strArr2 = {jSONObject.getString("start"), jSONObject.optString(TtmlNode.END)};
                long j2 = 0;
                long j3 = 0;
                for (int i = 0; i < strArr2.length; i++) {
                    if (!TextUtils.isEmpty(strArr2[i])) {
                        strArr2[i] = strArr2[i].replaceAll("([+-]\\d\\d):(\\d\\d)$", "$1$2");
                        int i2 = 0;
                        while (true) {
                            if (i2 >= strArr.length) {
                                break;
                            }
                            try {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strArr[i2]);
                                if (i == 0) {
                                    j = simpleDateFormat.parse(strArr2[i]).getTime();
                                } else {
                                    j3 = simpleDateFormat.parse(strArr2[i]).getTime();
                                    j = j2;
                                }
                                j2 = j;
                            } catch (ParseException unused) {
                                i2++;
                            }
                        }
                    }
                }
                Intent type = new Intent("android.intent.action.INSERT").setType("vnd.android.cursor.item/event");
                type.putExtra("title", optString);
                type.putExtra("description", optString3);
                type.putExtra("eventLocation", optString2);
                if (j2 > 0) {
                    type.putExtra("beginTime", j2);
                }
                if (j3 > 0) {
                    type.putExtra("endTime", j3);
                }
                this.context.startActivity(type);
            } catch (JSONException e) {
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Error parsing JSON: ");
                sb.append(e.getLocalizedMessage());
                MRAIDLog.e(str2, sb.toString());
            }
        }
    }

    public void playVideo(String str) {
        this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    public void openBrowser(String str) {
        if (str.startsWith("market:")) {
            this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } else if (str.startsWith("http:") || str.startsWith("https:")) {
            this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        }
    }

    public void storePicture(final String str) {
        if (this.nativeFeatureManager.isStorePictureSupported()) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        MRAIDNativeFeatureProvider.this.storePictureInGallery(str);
                    } catch (Exception e) {
                        MRAIDLog.e(MRAIDNativeFeatureProvider.TAG, e.getLocalizedMessage());
                    }
                }
            }).start();
        }
    }

    public void sendSms(String str) {
        if (this.nativeFeatureManager.isSmsSupported()) {
            this.context.startActivity(new Intent("android.intent.action.SENDTO", Uri.parse(str)));
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"SimpleDateFormat"})
    public void storePictureInGallery(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
        StringBuilder sb = new StringBuilder();
        sb.append(getAlbumDir());
        sb.append("/img");
        sb.append(simpleDateFormat.format(new Date()));
        sb.append(".png");
        String sb2 = sb.toString();
        String str2 = TAG;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Saving image into: ");
        sb3.append(sb2);
        MRAIDLog.i(str2, sb3.toString());
        File file = new File(sb2);
        try {
            copyStream(new URL(str).openStream(), new FileOutputStream(file));
            MediaScannerConnection.scanFile(this.context, new String[]{file.getAbsolutePath()}, null, new OnScanCompletedListener() {
                public void onScanCompleted(String str, Uri uri) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("File successfully saved to ");
                    sb.append(str);
                    MRAIDLog.d(sb.toString());
                }
            });
            MRAIDLog.i(TAG, "Saved image successfully");
        } catch (MalformedURLException e) {
            String str3 = TAG;
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Not able to save image due to invalid URL: ");
            sb4.append(e.getLocalizedMessage());
            MRAIDLog.e(str3, sb4.toString());
        } catch (IOException e2) {
            String str4 = TAG;
            StringBuilder sb5 = new StringBuilder();
            sb5.append("Unable to save image: ");
            sb5.append(e2.getLocalizedMessage());
            MRAIDLog.e(str4, sb5.toString());
        }
    }

    private void copyStream(InputStream inputStream, OutputStream outputStream) {
        try {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr, 0, 1024);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Error saving picture: ");
            sb.append(e.getLocalizedMessage());
            MRAIDLog.i(str, sb.toString());
        }
    }

    private File getAlbumDir() {
        File file;
        if ("mounted".equals(Environment.getExternalStorageState())) {
            file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "NexageAd");
            if (!file.mkdirs() && !file.exists()) {
                MRAIDLog.i(TAG, "Failed to create camera directory");
                return null;
            }
        } else {
            MRAIDLog.i(TAG, "External storage is not mounted READ/WRITE.");
            file = null;
        }
        return file;
    }
}
