package com.explorestack.iab.mraid;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.explorestack.iab.mraid.MRAIDView.builder;
import com.explorestack.iab.mraid.internal.MRAIDLog;
import java.util.concurrent.atomic.AtomicInteger;

public class MRAIDInterstitial {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final AtomicInteger NEXT_ID = new AtomicInteger(0);
    private static final String TAG = "MRAID";
    /* access modifiers changed from: private */
    public int afd;
    /* access modifiers changed from: private */
    public boolean finishAttachedActivityDuringClose = false;
    public final int id = NEXT_ID.getAndIncrement();
    /* access modifiers changed from: private */
    public boolean isClosed;
    /* access modifiers changed from: private */
    public boolean isReady;
    /* access modifiers changed from: private */
    public MRAIDInterstitialListener listener;
    @Nullable
    @VisibleForTesting
    MRAIDView mraidView;
    @VisibleForTesting
    final MRAIDViewListener mraidViewListener = new MRAIDViewListener() {
        public boolean mraidViewResize(MRAIDView mRAIDView, int i, int i2, int i3, int i4) {
            return true;
        }

        public void mraidViewLoaded(MRAIDView mRAIDView) {
            MRAIDLog.d("ViewListener", "mraidViewLoaded");
            MRAIDInterstitial.this.isReady = true;
            if (MRAIDInterstitial.this.listener != null) {
                MRAIDInterstitial.this.listener.mraidInterstitialLoaded(MRAIDInterstitial.this);
            }
        }

        public void mraidViewExpand(MRAIDView mRAIDView) {
            MRAIDLog.d("ViewListener", "mraidViewExpand");
            if (MRAIDInterstitial.this.listener != null) {
                MRAIDInterstitial.this.listener.mraidInterstitialShow(MRAIDInterstitial.this);
            }
        }

        public void mraidViewClose(MRAIDView mRAIDView) {
            MRAIDLog.d("ViewListener", "mraidViewClose");
            MRAIDInterstitial.this.isReady = false;
            MRAIDInterstitial.this.isClosed = true;
            if (MRAIDInterstitial.this.afd > 0 && System.currentTimeMillis() - MRAIDInterstitial.this.showTime >= ((long) MRAIDInterstitial.this.afd)) {
                MRAIDInterstitial.this.trackAppodealXFinish();
            }
            if (MRAIDInterstitial.this.listener != null) {
                MRAIDInterstitial.this.listener.mraidInterstitialHide(MRAIDInterstitial.this);
            }
            if (MRAIDInterstitial.this.finishAttachedActivityDuringClose) {
                Activity interstitialActivity = mRAIDView.getInterstitialActivity();
                if (interstitialActivity != null) {
                    interstitialActivity.finish();
                    interstitialActivity.overridePendingTransition(0, 0);
                }
            }
            MRAIDInterstitial.this.destroy();
        }

        public void mraidViewNoFill(MRAIDView mRAIDView) {
            MRAIDLog.d("ViewListener", "mraidViewNoFill");
            MRAIDInterstitial.this.isReady = false;
            MRAIDInterstitial.this.destroy();
            if (MRAIDInterstitial.this.listener != null) {
                MRAIDInterstitial.this.listener.mraidInterstitialNoFill(MRAIDInterstitial.this);
            }
        }
    };
    /* access modifiers changed from: private */
    public long showTime;

    public class Builder {
        @NonNull
        private builder mraidBuilder;

        public Builder(Context context, String str, int i, int i2) {
            this.mraidBuilder = new builder(context, str, i, i2).setListener(MRAIDInterstitial.this.mraidViewListener).setIsInterstitial(true);
        }

        public Builder setBaseUrl(String str) {
            this.mraidBuilder.setBaseUrl(str);
            return this;
        }

        public Builder setSupportedNativeFeatures(String[] strArr) {
            this.mraidBuilder.setSupportedNativeFeatures(strArr);
            return this;
        }

        public Builder setListener(MRAIDInterstitialListener mRAIDInterstitialListener) {
            MRAIDInterstitial.this.listener = mRAIDInterstitialListener;
            return this;
        }

        public Builder setNativeFeatureListener(MRAIDNativeFeatureListener mRAIDNativeFeatureListener) {
            this.mraidBuilder.setNativeFeatureListener(mRAIDNativeFeatureListener);
            return this;
        }

        public Builder setPreload(boolean z) {
            this.mraidBuilder.setPreload(z);
            return this;
        }

        public Builder setCloseTime(int i) {
            this.mraidBuilder.setCloseTime(i);
            return this;
        }

        public Builder setIsTag(boolean z) {
            this.mraidBuilder.setIsTag(z);
            return this;
        }

        public Builder setUseLayout(boolean z) {
            this.mraidBuilder.setUseLayout(z);
            return this;
        }

        public MRAIDInterstitial build() {
            MRAIDInterstitial.this.mraidView = this.mraidBuilder.build();
            return MRAIDInterstitial.this;
        }
    }

    private MRAIDInterstitial() {
    }

    public void load() {
        if (this.mraidView != null) {
            this.mraidView.load();
            return;
        }
        throw new IllegalStateException("MRAIDView not loaded (mraidView == null)");
    }

    public void show() {
        show(null, -1, false);
    }

    public void show(Activity activity, boolean z) {
        show(activity, -1, z);
    }

    public void show(int i) {
        show(null, i, false);
    }

    public void show(Activity activity, int i, boolean z) {
        if (!isReady()) {
            MRAIDLog.w(TAG, "show failed: interstitial is not ready");
            return;
        }
        this.showTime = System.currentTimeMillis();
        this.mraidView.showAsInterstitial(activity, i);
        this.finishAttachedActivityDuringClose = z;
    }

    public void setAfd(int i) {
        this.afd = i;
    }

    public void setSegmentAndPlacement(String str, String str2) {
        if (this.mraidView != null) {
            this.mraidView.setSegmentAndPlacement(str, str2);
        }
    }

    public void trackAppodealXFinish() {
        if (this.mraidView != null) {
            this.mraidView.trackAppodealXFinish();
        }
    }

    public void dispatchClose() {
        if (this.mraidView != null) {
            this.mraidViewListener.mraidViewClose(this.mraidView);
        }
    }

    public void destroy() {
        this.isReady = false;
        if (this.mraidView != null) {
            this.mraidView.destroy();
            this.mraidView = null;
        }
    }

    public boolean isReady() {
        return this.isReady && this.mraidView != null;
    }

    public boolean isClosed() {
        return this.isClosed;
    }

    public static Builder newBuilder(Context context, String str, int i, int i2) {
        MRAIDInterstitial mRAIDInterstitial = new MRAIDInterstitial();
        mRAIDInterstitial.getClass();
        Builder builder = new Builder(context, str, i, i2);
        return builder;
    }
}
