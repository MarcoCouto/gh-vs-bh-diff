package com.explorestack.iab.mraid.internal;

import android.content.Context;
import android.os.Build.VERSION;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import java.util.ArrayList;

public class MRAIDNativeFeatureManager {
    private static final String TAG = "MRAIDNativeFeatureManager";
    private Context context;
    private ArrayList<String> supportedNativeFeatures;

    public MRAIDNativeFeatureManager(Context context2, ArrayList<String> arrayList) {
        this.context = context2;
        this.supportedNativeFeatures = arrayList;
    }

    public boolean isCalendarSupported() {
        boolean z = this.supportedNativeFeatures.contains(MRAIDNativeFeature.CALENDAR) && VERSION.SDK_INT >= 14 && this.context.checkCallingOrSelfPermission("android.permission.WRITE_CALENDAR") == 0;
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("isCalendarSupported ");
        sb.append(z);
        MRAIDLog.d(str, sb.toString());
        return z;
    }

    public boolean isInlineVideoSupported() {
        boolean contains = this.supportedNativeFeatures.contains(MRAIDNativeFeature.INLINE_VIDEO);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("isInlineVideoSupported ");
        sb.append(contains);
        MRAIDLog.d(str, sb.toString());
        return contains;
    }

    public boolean isSmsSupported() {
        boolean z = this.supportedNativeFeatures.contains("sms") && this.context.checkCallingOrSelfPermission("android.permission.SEND_SMS") == 0;
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("isSmsSupported ");
        sb.append(z);
        MRAIDLog.d(str, sb.toString());
        return z;
    }

    public boolean isStorePictureSupported() {
        boolean contains = this.supportedNativeFeatures.contains(MRAIDNativeFeature.STORE_PICTURE);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("isStorePictureSupported ");
        sb.append(contains);
        MRAIDLog.d(str, sb.toString());
        return contains;
    }

    public boolean isTelSupported() {
        boolean z = this.supportedNativeFeatures.contains("tel") && this.context.checkCallingOrSelfPermission("android.permission.CALL_PHONE") == 0;
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("isTelSupported ");
        sb.append(z);
        MRAIDLog.d(str, sb.toString());
        return z;
    }

    public ArrayList<String> getSupportedNativeFeatures() {
        return this.supportedNativeFeatures;
    }
}
