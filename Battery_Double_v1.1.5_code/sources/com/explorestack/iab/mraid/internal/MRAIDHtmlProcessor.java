package com.explorestack.iab.mraid.internal;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MRAIDHtmlProcessor {
    public static String processRawHtml(String str) {
        StringBuffer stringBuffer = new StringBuffer(str);
        Matcher matcher = Pattern.compile("<script\\s+[^>]*\\bsrc\\s*=\\s*([\\\"\\'])mraid\\.js\\1[^>]*>\\s*</script>\\n*", 2).matcher(stringBuffer);
        if (matcher.find()) {
            stringBuffer.delete(matcher.start(), matcher.end());
        }
        boolean contains = str.contains("<html");
        boolean contains2 = str.contains("<head");
        boolean contains3 = str.contains("<body");
        if ((!contains && (contains2 || contains3)) || (contains && !contains3)) {
            return str;
        }
        String property = System.getProperty("line.separator");
        if (!contains) {
            StringBuilder sb = new StringBuilder();
            sb.append("<html>");
            sb.append(property);
            sb.append("<head>");
            sb.append(property);
            sb.append("</head>");
            sb.append(property);
            sb.append("<body><div align='center'>");
            sb.append(property);
            stringBuffer.insert(0, sb.toString());
            stringBuffer.append("</div></body>");
            stringBuffer.append(property);
            stringBuffer.append("</html>");
        } else if (!contains2) {
            Matcher matcher2 = Pattern.compile("<html[^>]*>", 2).matcher(stringBuffer);
            for (int i = 0; matcher2.find(i); i = matcher2.end()) {
                int end = matcher2.end();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(property);
                sb2.append("<head>");
                sb2.append(property);
                sb2.append("</head>");
                stringBuffer.insert(end, sb2.toString());
            }
        }
        String str2 = "<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' />";
        StringBuilder sb3 = new StringBuilder();
        sb3.append("<style>");
        sb3.append(property);
        sb3.append("body { margin:0; padding:0;}");
        sb3.append(property);
        sb3.append("*:not(input) { -webkit-touch-callout:none; -webkit-user-select:none; -webkit-text-size-adjust:none; }");
        sb3.append(property);
        sb3.append("</style>");
        String sb4 = sb3.toString();
        Matcher matcher3 = Pattern.compile("<head[^>]*>", 2).matcher(stringBuffer);
        for (int i2 = 0; matcher3.find(i2); i2 = matcher3.end()) {
            int end2 = matcher3.end();
            StringBuilder sb5 = new StringBuilder();
            sb5.append(property);
            sb5.append(str2);
            sb5.append(property);
            sb5.append(sb4);
            stringBuffer.insert(end2, sb5.toString());
        }
        return stringBuffer.toString();
    }
}
