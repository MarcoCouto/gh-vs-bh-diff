package com.explorestack.iab.mraid.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.util.SparseArray;
import com.explorestack.iab.mraid.MRAIDInterstitial;
import com.explorestack.iab.mraid.internal.MRAIDLog;
import com.explorestack.iab.utils.Utils;
import com.google.android.exoplayer2.DefaultRenderersFactory;

public class MraidActivity extends Activity {
    private static final String PARAM_INTERSTITIAL_ID = "InterstitialId";
    private static final String PARAM_MRAID_TYPE = "InterstitialType";
    private static SparseArray<Pair<MRAIDInterstitial, MraidActivityListener>> displayCache = new SparseArray<>();
    @Nullable
    private MraidActivityListener activityListener;
    @Nullable
    private Integer interstitialId;
    /* access modifiers changed from: private */
    public boolean isSkippable = false;
    @Nullable
    private MRAIDInterstitial mraidInterstitial;
    @Nullable
    private MraidType mraidType;

    public interface MraidActivityListener {
        void onMraidActivityClose();

        void onMraidActivityShowFailed();
    }

    public enum MraidType {
        Static,
        Video,
        Rewarded
    }

    private static void removeFromDisplayCache(Integer num) {
        if (num != null) {
            displayCache.remove(num.intValue());
        }
    }

    public static void show(@Nullable Context context, @Nullable MRAIDInterstitial mRAIDInterstitial, @Nullable MraidType mraidType2, @Nullable MraidActivityListener mraidActivityListener) {
        if (context == null) {
            MRAIDLog.e("Context not provided for display mraid interstitial");
            if (mraidActivityListener != null) {
                mraidActivityListener.onMraidActivityShowFailed();
            }
        } else if (mRAIDInterstitial == null) {
            MRAIDLog.e("Mraid interstitial object not provided for display");
            if (mraidActivityListener != null) {
                mraidActivityListener.onMraidActivityShowFailed();
            }
        } else if (mraidType2 == null) {
            MRAIDLog.e("Mraid type not provided for display");
            if (mraidActivityListener != null) {
                mraidActivityListener.onMraidActivityShowFailed();
            }
        } else {
            try {
                displayCache.put(mRAIDInterstitial.id, Pair.create(mRAIDInterstitial, mraidActivityListener));
                Intent intent = new Intent(context, MraidActivity.class);
                intent.putExtra(PARAM_INTERSTITIAL_ID, mRAIDInterstitial.id);
                intent.putExtra(PARAM_MRAID_TYPE, mraidType2);
                intent.addFlags(268435456);
                intent.addFlags(8388608);
                context.startActivity(intent);
            } catch (Throwable th) {
                th.printStackTrace();
                if (mraidActivityListener != null) {
                    mraidActivityListener.onMraidActivityShowFailed();
                }
                removeFromDisplayCache(Integer.valueOf(mRAIDInterstitial.id));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        if (!getIntent().hasExtra(PARAM_INTERSTITIAL_ID)) {
            MRAIDLog.e("Mraid display cache id not provided");
            finish();
            return;
        }
        this.interstitialId = Integer.valueOf(getIntent().getIntExtra(PARAM_INTERSTITIAL_ID, 0));
        Pair pair = (Pair) displayCache.get(this.interstitialId.intValue());
        if (pair == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Mraid display cache data not found for id=");
            sb.append(this.interstitialId);
            MRAIDLog.e(sb.toString());
            finish();
            return;
        }
        this.mraidInterstitial = (MRAIDInterstitial) pair.first;
        this.activityListener = (MraidActivityListener) pair.second;
        if (this.mraidInterstitial == null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Mraid interstitial not found in display cache data with id=");
            sb2.append(this.interstitialId);
            MRAIDLog.e(sb2.toString());
            finish();
            overridePendingTransition(0, 0);
            if (this.activityListener != null) {
                this.activityListener.onMraidActivityShowFailed();
            }
            return;
        }
        this.mraidType = (MraidType) getIntent().getSerializableExtra(PARAM_MRAID_TYPE);
        if (this.mraidType == null) {
            MRAIDLog.e("Mraid type not provided");
            finish();
            overridePendingTransition(0, 0);
            if (this.activityListener != null) {
                this.activityListener.onMraidActivityShowFailed();
            }
            return;
        }
        Utils.applyFullscreenActivityFlags(this);
        switch (this.mraidType) {
            case Static:
                this.isSkippable = true;
                setRequestedOrientation(Utils.getScreenOrientation(this));
                break;
            case Video:
                getWindow().getDecorView().postDelayed(new Runnable() {
                    public void run() {
                        MraidActivity.this.isSkippable = true;
                    }
                }, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
                break;
        }
        this.mraidInterstitial.show(this, true);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.activityListener != null) {
            this.activityListener.onMraidActivityClose();
        }
        removeFromDisplayCache(this.interstitialId);
    }

    public void onBackPressed() {
        if (this.isSkippable) {
            finish();
            overridePendingTransition(0, 0);
        }
    }
}
