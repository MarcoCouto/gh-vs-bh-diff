package com.startapp.common.c;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private String f4193a;
    private String b;

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0014 A[SYNTHETIC, Splitter:B:13:0x0014] */
    public static <T> T a(String str, Class<T> cls) {
        a aVar = null;
        try {
            a aVar2 = new a(str);
            try {
                T a2 = aVar2.a(cls);
                try {
                    aVar2.close();
                } catch (IOException unused) {
                }
                return a2;
            } catch (Throwable th) {
                th = th;
                aVar = aVar2;
                if (aVar != null) {
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            if (aVar != null) {
                try {
                    aVar.close();
                } catch (IOException unused2) {
                }
            }
            throw th;
        }
    }

    public static String a(Object obj) {
        return b(obj).toString();
    }

    private static JSONObject b(Object obj) {
        Class cls = obj.getClass();
        ArrayList<Field> arrayList = new ArrayList<>();
        while (cls != null && !Object.class.equals(cls)) {
            arrayList.addAll(Arrays.asList(cls.getDeclaredFields()));
            cls = cls.getSuperclass();
        }
        JSONObject jSONObject = new JSONObject();
        for (Field field : arrayList) {
            int modifiers = field.getModifiers();
            if (!Modifier.isStatic(modifiers) && !Modifier.isTransient(modifiers)) {
                try {
                    field.setAccessible(true);
                    if (field.get(obj) != null) {
                        String a2 = com.b.a.a.a.b.a(field);
                        Annotation[] declaredAnnotations = field.getDeclaredAnnotations();
                        boolean z = false;
                        if (declaredAnnotations != null) {
                            if (declaredAnnotations.length != 0) {
                                Annotation annotation = field.getDeclaredAnnotations()[0];
                                if (annotation.annotationType().equals(e.class)) {
                                    z = ((e) annotation).a();
                                }
                            }
                        }
                        if (z) {
                            jSONObject.put(a2, b(field.get(obj)));
                        } else if (List.class.isAssignableFrom(field.getType())) {
                            JSONArray jSONArray = new JSONArray();
                            for (Object c : (List) field.get(obj)) {
                                jSONArray.put(c(c));
                            }
                            jSONObject.put(a2, jSONArray);
                        } else if (Set.class.isAssignableFrom(field.getType())) {
                            JSONArray jSONArray2 = new JSONArray();
                            for (Object c2 : (Set) field.get(obj)) {
                                jSONArray2.put(c(c2));
                            }
                            jSONObject.put(a2, jSONArray2);
                        } else if (Map.class.isAssignableFrom(field.getType())) {
                            JSONObject jSONObject2 = new JSONObject();
                            for (Entry entry : ((Map) field.get(obj)).entrySet()) {
                                jSONObject2.put(entry.getKey().toString(), c(entry.getValue()));
                            }
                            jSONObject.put(a2, jSONObject2);
                        } else {
                            jSONObject.put(a2, field.get(obj));
                        }
                    }
                } catch (IllegalAccessException | IllegalArgumentException | JSONException unused) {
                }
            }
        }
        return jSONObject;
    }

    private static Object c(Object obj) {
        if (com.b.a.a.a.b.a(obj)) {
            return obj;
        }
        return b(obj);
    }

    public final void a(String str) {
        this.f4193a = str;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final boolean a() {
        return (this.f4193a == null || this.b == null) ? false : true;
    }
}
