package com.startapp.common.c;

import android.os.Build.VERSION;
import com.b.a.a.a.b;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class a extends InputStream {
    private static Map<String, Class> c;

    /* renamed from: a reason: collision with root package name */
    private InputStream f4192a = null;
    private String b;

    @Deprecated
    public final int read() {
        return 0;
    }

    public a(String str) {
        this.b = str;
    }

    public final void close() {
        super.close();
    }

    public final <T> T a(Class<T> cls) {
        try {
            return a(cls, (JSONObject) null);
        } catch (c unused) {
            new StringBuilder("Error while trying to parse object ").append(cls.toString());
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:145:0x035e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0360, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0368, code lost:
        throw new com.startapp.common.c.c("Unknown error occurred ", r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x0360 A[ExcHandler: Throwable (r0v7 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:61:0x010e] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x015e A[Catch:{ Exception -> 0x0369, Throwable -> 0x0360 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x019f A[Catch:{ Exception -> 0x0369, Throwable -> 0x0360 }] */
    private <T> T a(Class<T> cls, JSONObject jSONObject) {
        JSONObject jSONObject2;
        T t;
        Field[] declaredFields;
        Field[] fieldArr;
        int length;
        int i;
        int i2;
        String str;
        Class cls2;
        boolean z;
        Class cls3;
        Class cls4;
        Class cls5;
        boolean z2;
        Object obj;
        T t2;
        Class<T> cls6 = cls;
        if (this.b != null) {
            if (this.b == null) {
                try {
                    this.b = a();
                } catch (Exception e) {
                    throw new c("Can't read input stream.", e);
                }
            }
            if (jSONObject == null) {
                try {
                    jSONObject2 = new JSONObject(this.b);
                } catch (JSONException e2) {
                    throw new c("Can't deserialize the object. Failed to create JSON object.", e2);
                }
            } else {
                jSONObject2 = jSONObject;
            }
            try {
                d dVar = (d) cls6.getAnnotation(d.class);
                boolean z3 = true;
                char c2 = 0;
                if (VERSION.SDK_INT >= 9 && cls6.equals(HttpCookie.class)) {
                    Constructor constructor = cls.getDeclaredConstructors()[0];
                    constructor.setAccessible(true);
                    t2 = constructor.newInstance(new Object[]{"name", "value"});
                } else if (cls.isPrimitive()) {
                    return cls.newInstance();
                } else {
                    if (cls6.getAnnotation(d.class) != null) {
                        if (!dVar.c()) {
                            if (!dVar.c()) {
                                String string = jSONObject2.getString(dVar.a());
                                String b2 = dVar.b();
                                StringBuilder sb = new StringBuilder();
                                sb.append(b2);
                                sb.append(".");
                                sb.append(string);
                                return a(Class.forName(sb.toString()), jSONObject2);
                            }
                            t = null;
                            declaredFields = cls.getDeclaredFields();
                            if (dVar != null || !dVar.c()) {
                                fieldArr = declaredFields;
                            } else {
                                int length2 = declaredFields.length;
                                Field[] declaredFields2 = cls.getSuperclass().getDeclaredFields();
                                int length3 = declaredFields2.length;
                                Field[] fieldArr2 = new Field[(length2 + length3)];
                                System.arraycopy(declaredFields, 0, fieldArr2, 0, length2);
                                System.arraycopy(declaredFields2, 0, fieldArr2, length2, length3);
                                fieldArr = fieldArr2;
                            }
                            length = fieldArr.length;
                            i = 0;
                            while (i < length) {
                                Field field = fieldArr[i];
                                int modifiers = field.getModifiers();
                                if (!Modifier.isStatic(modifiers) && !Modifier.isTransient(modifiers)) {
                                    String a2 = b.a(field);
                                    try {
                                        if (jSONObject2.has(a2)) {
                                            field.setAccessible(z3);
                                            if (field.getDeclaredAnnotations().length > 0) {
                                                Annotation annotation = field.getDeclaredAnnotations()[c2];
                                                if (annotation.annotationType().equals(e.class)) {
                                                    e eVar = (e) annotation;
                                                    cls5 = eVar.b();
                                                    cls4 = eVar.d();
                                                    cls3 = eVar.c();
                                                    z = eVar.a();
                                                    cls2 = eVar.e();
                                                    z2 = true;
                                                    if (field.getType().getAnnotation(d.class) == null) {
                                                        d dVar2 = (d) field.getType().getAnnotation(d.class);
                                                        String string2 = jSONObject2.getJSONObject(a2).getString(dVar2.a());
                                                        String b3 = dVar2.b();
                                                        StringBuilder sb2 = new StringBuilder();
                                                        sb2.append(b3);
                                                        sb2.append(".");
                                                        sb2.append(string2);
                                                        field.set(t, a(Class.forName(sb2.toString()), jSONObject2.getJSONObject(a2)));
                                                    } else if (z) {
                                                        field.set(t, a(field.getType(), jSONObject2.getJSONObject(a2)));
                                                    } else if (!z2 || (!Map.class.isAssignableFrom(cls5) && !Collection.class.isAssignableFrom(cls5))) {
                                                        String str2 = a2;
                                                        i2 = i;
                                                        if (field.getType().isEnum()) {
                                                            field.set(t, Enum.valueOf(cls5, (String) jSONObject2.get(str2)));
                                                        } else if (field.getType().isPrimitive()) {
                                                            Object obj2 = jSONObject2.get(str2);
                                                            Class type = field.getType();
                                                            if (!obj2.getClass().equals(type)) {
                                                                if (obj2.getClass().equals(String.class)) {
                                                                    if (type.equals(Integer.TYPE)) {
                                                                        obj2 = Integer.valueOf(jSONObject2.getInt(b.a(field)));
                                                                    }
                                                                } else if (type.equals(Integer.TYPE)) {
                                                                    obj2 = Integer.valueOf(((Number) obj2).intValue());
                                                                } else if (type.equals(Float.TYPE)) {
                                                                    obj2 = Float.valueOf(((Number) obj2).floatValue());
                                                                } else if (type.equals(Long.TYPE)) {
                                                                    obj2 = Long.valueOf(((Number) obj2).longValue());
                                                                } else if (type.equals(Double.TYPE)) {
                                                                    obj2 = Double.valueOf(((Number) obj2).doubleValue());
                                                                }
                                                            }
                                                            field.set(t, obj2);
                                                        } else if (field.getType().isArray()) {
                                                            if (cls5 != null) {
                                                                obj = a(jSONObject2, cls5, field);
                                                            } else {
                                                                obj = a(jSONObject2, field);
                                                            }
                                                            field.set(t, obj);
                                                        } else {
                                                            Object obj3 = jSONObject2.get(str2);
                                                            Class type2 = field.getType();
                                                            if (!obj3.getClass().equals(type2)) {
                                                                if (type2.equals(Integer.class)) {
                                                                    if (obj3.getClass().equals(Double.class)) {
                                                                        obj3 = Integer.valueOf(((Double) obj3).intValue());
                                                                    } else if (obj3.getClass().equals(Long.class)) {
                                                                        obj3 = Integer.valueOf(((Long) obj3).intValue());
                                                                    }
                                                                } else if (type2.equals(Long.class) && obj3.getClass().equals(Integer.class)) {
                                                                    obj3 = Long.valueOf(((Integer) obj3).longValue());
                                                                }
                                                            }
                                                            if (obj3.equals(null)) {
                                                                field.set(t, null);
                                                            } else {
                                                                field.set(t, obj3);
                                                            }
                                                        }
                                                        i = i2 + 1;
                                                        z3 = true;
                                                        c2 = 0;
                                                    } else {
                                                        if (cls5.equals(HashMap.class)) {
                                                            JSONObject jSONObject3 = jSONObject2.getJSONObject(a2);
                                                            Iterator keys = jSONObject3.keys();
                                                            str = a2;
                                                            JSONObject jSONObject4 = jSONObject3;
                                                            i2 = i;
                                                            field.set(t, a(cls4, cls3, cls2, jSONObject4, keys));
                                                        } else {
                                                            String str3 = a2;
                                                            i2 = i;
                                                            if (cls5.equals(ArrayList.class)) {
                                                                field.set(t, c(cls3, jSONObject2.getJSONArray(str3)));
                                                            } else if (cls5.equals(HashSet.class)) {
                                                                field.set(t, a(cls3, jSONObject2.getJSONArray(str3)));
                                                            } else if (cls5.equals(EnumSet.class)) {
                                                                field.set(t, b(cls3, jSONObject2.getJSONArray(str3)));
                                                            }
                                                        }
                                                        i = i2 + 1;
                                                        z3 = true;
                                                        c2 = 0;
                                                    }
                                                }
                                            }
                                            cls5 = null;
                                            cls4 = null;
                                            cls3 = null;
                                            cls2 = null;
                                            z2 = false;
                                            z = false;
                                            if (field.getType().getAnnotation(d.class) == null) {
                                            }
                                        }
                                    } catch (Exception e3) {
                                        e = e3;
                                        str = a2;
                                        i2 = i;
                                        String.format("Failed to get field [%s] %s", new Object[]{str, e.toString()});
                                        i = i2 + 1;
                                        z3 = true;
                                        c2 = 0;
                                    } catch (Throwable th) {
                                    }
                                }
                                i2 = i;
                                i = i2 + 1;
                                z3 = true;
                                c2 = 0;
                            }
                            return t;
                        }
                    }
                    Constructor declaredConstructor = cls6.getDeclaredConstructor(new Class[0]);
                    declaredConstructor.setAccessible(true);
                    t2 = declaredConstructor.newInstance(new Object[0]);
                }
                t = t2;
                declaredFields = cls.getDeclaredFields();
                if (dVar != null) {
                }
                fieldArr = declaredFields;
                length = fieldArr.length;
                i = 0;
                while (i < length) {
                }
                return t;
            } catch (ClassNotFoundException e4) {
                throw new c("Problem instantiating object class ", e4);
            } catch (JSONException e5) {
                throw new c("Problem instantiating object class ", e5);
            } catch (Exception e6) {
                throw new c("Can't deserialize the object. Failed to instantiate object.", e6);
            }
        } else {
            throw new c("Can't read object, the input is null.");
        }
    }

    private static Object a(JSONObject jSONObject, Field field) {
        Object obj;
        JSONArray jSONArray = jSONObject.getJSONArray(b.a(field));
        int length = jSONArray.length();
        Class cls = (Class) c.get(field.getType().getSimpleName());
        Object newInstance = Array.newInstance((Class) cls.getField("TYPE").get(null), length);
        for (int i = 0; i < length; i++) {
            String string = jSONArray.getString(i);
            Class<String> cls2 = String.class;
            if (cls.equals(Character.class)) {
                cls2 = Character.TYPE;
            }
            Constructor constructor = cls.getConstructor(new Class[]{cls2});
            if (cls.equals(Character.class)) {
                obj = constructor.newInstance(new Object[]{Character.valueOf(string.charAt(0))});
            } else {
                obj = constructor.newInstance(new Object[]{string});
            }
            Array.set(newInstance, i, obj);
        }
        return newInstance;
    }

    private <T> T[] a(JSONObject jSONObject, Class<T> cls, Field field) {
        JSONArray jSONArray = jSONObject.getJSONArray(b.a(field));
        int length = jSONArray.length();
        Object newInstance = Array.newInstance(cls, length);
        for (int i = 0; i < length; i++) {
            Array.set(newInstance, i, a(cls, jSONArray.getJSONObject(i)));
        }
        return (Object[]) newInstance;
    }

    private <K, V> Map<K, V> a(Class<K> cls, Class<V> cls2, Class cls3, JSONObject jSONObject, Iterator<K> it) {
        HashMap hashMap = new HashMap();
        while (it.hasNext()) {
            Object next = it.next();
            Object cast = cls.equals(Integer.class) ? cls.cast(Integer.valueOf(Integer.parseInt((String) next))) : next;
            if (cls.isEnum()) {
                cast = Enum.valueOf(cls, cast.toString());
            }
            String str = (String) next;
            JSONObject optJSONObject = jSONObject.optJSONObject(str);
            if (optJSONObject == null) {
                JSONArray optJSONArray = jSONObject.optJSONArray(str);
                if (optJSONArray != null) {
                    hashMap.put(cast, a(cls3, optJSONArray));
                } else if (cls2.isEnum()) {
                    hashMap.put(cast, Enum.valueOf(cls2, (String) jSONObject.get(str)));
                } else {
                    hashMap.put(cast, jSONObject.get(str));
                }
            } else {
                hashMap.put(cast, a(cls2, optJSONObject));
            }
        }
        return hashMap;
    }

    private <V> Set<V> a(Class<V> cls, JSONArray jSONArray) {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (optJSONObject == null) {
                hashSet.add(jSONArray.get(i));
            } else {
                hashSet.add(a(cls, optJSONObject));
            }
        }
        return hashSet;
    }

    private static <V> Set b(Class<V> cls, JSONArray jSONArray) {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < jSONArray.length(); i++) {
            hashSet.add(Enum.valueOf(cls, jSONArray.getString(i)));
        }
        return hashSet;
    }

    private <V> List<V> c(Class<V> cls, JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (optJSONObject == null) {
                arrayList.add(jSONArray.get(i));
            } else {
                arrayList.add(a(cls, optJSONObject));
            }
        }
        return arrayList;
    }

    private static String a() {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(null));
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        return sb.toString();
                    }
                    sb.append(readLine);
                } catch (IOException e) {
                    String.format("Can't Can't read input stream [%s]", new Object[]{e.toString()});
                    throw e;
                }
            }
        } catch (Exception e2) {
            String.format("Can't create BufferedReader [%s]", new Object[]{e2.toString()});
            throw e2;
        }
    }

    static {
        HashMap hashMap = new HashMap();
        c = hashMap;
        hashMap.put("int[]", Integer.class);
        c.put("long[]", Long.class);
        c.put("double[]", Double.class);
        c.put("float[]", Float.class);
        c.put("bool[]", Boolean.class);
        c.put("char[]", Character.class);
        c.put("byte[]", Byte.class);
        c.put("void[]", Void.class);
        c.put("short[]", Short.class);
    }
}
