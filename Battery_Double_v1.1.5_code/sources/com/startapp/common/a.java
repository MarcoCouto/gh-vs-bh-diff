package com.startapp.common;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.a.d;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    String f4161a;
    C0093a b;
    int c;

    /* renamed from: com.startapp.common.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public interface C0093a {
        void a(Bitmap bitmap, int i);
    }

    public a(String str, C0093a aVar, int i) {
        this.f4161a = str;
        this.b = aVar;
        this.c = i;
    }

    public final void a() {
        f.a(com.startapp.common.f.a.HIGH, (Runnable) new Runnable() {
            public final void run() {
                final Bitmap b = d.b(a.this.f4161a);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        if (a.this.b != null) {
                            a.this.b.a(b, a.this.c);
                        }
                    }
                });
            }
        });
    }
}
