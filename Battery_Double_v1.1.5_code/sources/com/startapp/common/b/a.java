package com.startapp.common.b;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo.Builder;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ServiceInfo;
import android.os.Build.VERSION;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import com.startapp.android.publish.common.metaData.InfoEventService;
import com.startapp.android.publish.common.metaData.PeriodicJobService;
import com.startapp.common.b.a.b;
import com.startapp.common.b.a.b.C0095b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: StartAppSDK */
public class a {
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a reason: collision with root package name */
    public static volatile a f4180a = null;
    private static volatile com.startapp.android.publish.ads.video.b.c.a b = null;
    private static volatile int c = 60000;
    private static final ExecutorService i = Executors.newSingleThreadExecutor();
    private static final ScheduledExecutorService j = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */
    public Context d;
    private List<com.startapp.common.b.a.a> e = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public Map<Integer, Integer> f = new ConcurrentHashMap();
    private AtomicInteger g = new AtomicInteger(0);
    private boolean h;

    private static int a(int i2, boolean z) {
        return z ? i2 | Integer.MIN_VALUE : i2;
    }

    public static void a() {
    }

    public static void b() {
    }

    private a(Context context) {
        this.d = context.getApplicationContext();
        this.h = d(context);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:4|5|(7:7|(1:9)|10|11|12|13|(2:15|(1:(6:17|18|19|20|(4:22|(1:24)|25|49)(1:48)|30)(3:46|32|(3:34|(1:36)|37)(0))))(0))(0)|38|39) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x009e */
    public static a a(Context context) {
        if (f4180a == null) {
            synchronized (a.class) {
                if (f4180a == null) {
                    if (context.getApplicationContext() != null) {
                        context = context.getApplicationContext();
                    }
                    f4180a = new a(context);
                    int i2 = 0;
                    SharedPreferences sharedPreferences = context.getSharedPreferences("RunnerManager", 0);
                    String str = null;
                    String string = sharedPreferences.getString("RegisteredClassesNames", null);
                    if (string != null) {
                        String[] split = string.split(",");
                        StringBuilder sb = new StringBuilder(string.length());
                        int length = split.length;
                        while (true) {
                            if (i2 < length) {
                                String str2 = split[i2];
                                try {
                                    "create CLS: ".concat(String.valueOf(str2));
                                    Class cls = Class.forName(str2);
                                    if (com.startapp.common.b.a.a.class.isAssignableFrom(cls)) {
                                        f4180a.e.add((com.startapp.common.b.a.a) cls.newInstance());
                                        if (sb.length() > 0) {
                                            sb.append(',');
                                        }
                                        sb.append(str2);
                                    }
                                } catch (ClassNotFoundException unused) {
                                } catch (Throwable unused2) {
                                    "create :".concat(String.valueOf(str2));
                                }
                                i2++;
                            } else if (!sb.toString().equals(string)) {
                                Editor edit = sharedPreferences.edit();
                                String str3 = "RegisteredClassesNames";
                                if (sb.length() > 0) {
                                    str = sb.toString();
                                }
                                edit.putString(str3, str).commit();
                            }
                        }
                    }
                }
            }
            break;
        }
        return f4180a;
    }

    public static void a(com.startapp.common.b.a.a aVar) {
        f4180a.e.add(aVar);
        String name = aVar.getClass().getName();
        SharedPreferences sharedPreferences = f4180a.d.getSharedPreferences("RunnerManager", 0);
        String string = sharedPreferences.getString("RegisteredClassesNames", null);
        if (string == null) {
            sharedPreferences.edit().putString("RegisteredClassesNames", name).commit();
            return;
        }
        if (!string.contains(name)) {
            StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(",");
            sb.append(name);
            sharedPreferences.edit().putString("RegisteredClassesNames", sb.toString()).commit();
        }
    }

    public static void a(com.startapp.android.publish.ads.video.b.c.a aVar) {
        b = aVar;
    }

    public static boolean a(final b bVar) {
        try {
            final int a2 = a(bVar.a(), bVar.e());
            StringBuilder sb = new StringBuilder("schedule ");
            sb.append(a2);
            sb.append(" ");
            sb.append(bVar);
            if (!f4180a.h) {
                final int incrementAndGet = f4180a.g.incrementAndGet();
                AnonymousClass1 r3 = new Runnable() {
                    public final void run() {
                        Integer num = (Integer) a.f4180a.f.get(Integer.valueOf(a2));
                        if (num != null && num.intValue() == incrementAndGet) {
                            if (!bVar.e()) {
                                a.f4180a.f.remove(Integer.valueOf(a2));
                            }
                            a.b(bVar, (C0095b) new C0095b() {
                                public final void a(com.startapp.common.b.a.b.a aVar) {
                                }
                            });
                        }
                    }
                };
                if (bVar.e()) {
                    j.scheduleAtFixedRate(r3, bVar.d(), bVar.d(), TimeUnit.MILLISECONDS);
                } else {
                    j.schedule(r3, bVar.c(), TimeUnit.MILLISECONDS);
                }
                f4180a.f.put(Integer.valueOf(a2), Integer.valueOf(incrementAndGet));
                return true;
            } else if (d()) {
                return a(a2, bVar);
            } else {
                return b(a2, bVar);
            }
        } catch (Exception unused) {
            return false;
        }
    }

    @TargetApi(21)
    private static boolean a(int i2, b bVar) {
        JobScheduler c2 = c(f4180a.d);
        if (c2 == null) {
            return false;
        }
        PersistableBundle persistableBundle = new PersistableBundle();
        Map b2 = bVar.b();
        for (String str : b2.keySet()) {
            persistableBundle.putString(str, (String) b2.get(str));
        }
        persistableBundle.putInt("__RUNNER_RECURRING_ID__", bVar.e() ? 1 : 0);
        persistableBundle.putLong("__RUNNER_TRIGGER_ID__", bVar.c());
        Builder builder = new Builder(i2, new ComponentName(f4180a.d, PeriodicJobService.class));
        builder.setExtras(persistableBundle);
        if (bVar.e()) {
            builder.setPeriodic(bVar.c());
        } else {
            builder.setMinimumLatency(bVar.c()).setOverrideDeadline(bVar.c() + ((long) c));
        }
        builder.setRequiredNetworkType(bVar.f() ? 1 : 0);
        int schedule = c2.schedule(builder.build());
        "jobScheduler.schedule ".concat(String.valueOf(schedule));
        if (schedule == 1) {
            return true;
        }
        return false;
    }

    private static boolean b(int i2, b bVar) {
        AlarmManager b2 = b(f4180a.d);
        if (b2 == null) {
            return false;
        }
        Intent intent = new Intent(f4180a.d, InfoEventService.class);
        Map b3 = bVar.b();
        for (String str : b3.keySet()) {
            intent.putExtra(str, (String) b3.get(str));
        }
        intent.putExtra("__RUNNER_TASK_ID__", i2);
        intent.putExtra("__RUNNER_RECURRING_ID__", bVar.e());
        intent.putExtra("__RUNNER_TRIGGER_ID__", bVar.c());
        PendingIntent service = PendingIntent.getService(f4180a.d, i2, intent, 134217728);
        b2.cancel(service);
        if (bVar.e()) {
            b2.setRepeating(0, System.currentTimeMillis() + bVar.d(), bVar.c(), service);
        } else {
            b2.set(3, SystemClock.elapsedRealtime() + bVar.c(), service);
        }
        return true;
    }

    @SuppressLint({"NewApi"})
    public static void a(int i2) {
        "cancelAlarm ".concat(String.valueOf(i2));
        try {
            int a2 = a(i2, false);
            try {
                if (!f4180a.h) {
                    f4180a.f.remove(Integer.valueOf(a2));
                } else if (d()) {
                    JobScheduler c2 = c(f4180a.d);
                    if (c2 != null) {
                        c2.cancel(a2);
                    }
                } else {
                    AlarmManager b2 = b(f4180a.d);
                    if (b2 != null) {
                        Intent intent = new Intent(f4180a.d, InfoEventService.class);
                        Context context = f4180a.d;
                        PendingIntent service = PendingIntent.getService(context, a2, intent, 134217728);
                        if (PendingIntent.getService(context, 0, intent, 268435456) != null) {
                            b2.cancel(service);
                            service.cancel();
                        }
                    }
                }
            } catch (Exception unused) {
                i2 = a2;
                "cancelAlarm ".concat(String.valueOf(i2));
            }
        } catch (Exception unused2) {
            "cancelAlarm ".concat(String.valueOf(i2));
        }
    }

    public static boolean a(Intent intent, @NonNull C0095b bVar) {
        new StringBuilder("runJob ").append(intent != 0 ? intent : "NULL");
        return intent != 0 && b(a(intent), bVar);
    }

    @TargetApi(21)
    public static boolean a(@NonNull JobParameters jobParameters, @NonNull C0095b bVar) {
        "runJob ".concat(String.valueOf(jobParameters));
        PersistableBundle extras = jobParameters.getExtras();
        boolean z = true;
        if (extras.getInt("__RUNNER_RECURRING_ID__") != 1) {
            z = false;
        }
        long j2 = extras.getLong("__RUNNER_TRIGGER_ID__", 0);
        HashMap hashMap = new HashMap(extras.keySet().size());
        for (String str : extras.keySet()) {
            Object obj = extras.get(str);
            if (obj instanceof String) {
                hashMap.put(str, (String) obj);
            }
        }
        return b(new com.startapp.common.b.b.a(jobParameters.getJobId()).a((Map<String, String>) hashMap).a(z).a(j2).b(), bVar);
    }

    /* access modifiers changed from: private */
    public static boolean b(final b bVar, @NonNull final C0095b bVar2) {
        StringBuilder sb = new StringBuilder("RunnerJob ");
        sb.append(bVar.a());
        sb.append(" ");
        sb.append(bVar.a() & Integer.MAX_VALUE);
        final int a2 = bVar.a() & Integer.MAX_VALUE;
        final b b2 = b(a2);
        if (b2 != null) {
            i.execute(new Runnable() {
                public final void run() {
                    b2.a(a.f4180a.d, a2, bVar.b(), new C0095b() {
                        public final void a(com.startapp.common.b.a.b.a aVar) {
                            StringBuilder sb = new StringBuilder("job.execute ");
                            sb.append(bVar.a());
                            sb.append(" ");
                            sb.append(aVar);
                            a.a();
                            if (aVar == com.startapp.common.b.a.b.a.RESCHEDULE && !bVar.e()) {
                                a.a(bVar);
                            }
                            bVar2.a(aVar);
                        }
                    });
                }
            });
            return true;
        }
        new StringBuilder("runJob: failed to get job for ID ").append(bVar.a());
        bVar2.a(com.startapp.common.b.a.b.a.FAILED);
        return false;
    }

    private static b b(int i2) {
        b bVar = null;
        for (com.startapp.common.b.a.a create : f4180a.e) {
            bVar = create.create(i2);
            if (bVar != null) {
                break;
            }
        }
        return bVar;
    }

    private static b a(@NonNull Intent intent) {
        HashMap hashMap;
        int intExtra = intent.getIntExtra("__RUNNER_TASK_ID__", -1);
        boolean booleanExtra = intent.getBooleanExtra("__RUNNER_RECURRING_ID__", false);
        long longExtra = intent.getLongExtra("__RUNNER_TRIGGER_ID__", 0);
        if (intent.getExtras() != null) {
            hashMap = new HashMap(intent.getExtras().keySet().size());
            for (String str : intent.getExtras().keySet()) {
                Object obj = intent.getExtras().get(str);
                if (obj instanceof String) {
                    hashMap.put(str, (String) obj);
                }
            }
        } else {
            hashMap = null;
        }
        return new com.startapp.common.b.b.a(intExtra).a((Map<String, String>) hashMap).a(booleanExtra).a(longExtra).b();
    }

    @Nullable
    private static AlarmManager b(Context context) {
        return (AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM);
    }

    @Nullable
    @TargetApi(21)
    private static JobScheduler c(Context context) {
        return (JobScheduler) context.getSystemService("jobscheduler");
    }

    private static boolean d() {
        return VERSION.SDK_INT >= 21;
    }

    private static boolean d(Context context) {
        try {
            for (ServiceInfo serviceInfo : context.getPackageManager().getPackageInfo(context.getPackageName(), 4).services) {
                if (serviceInfo.name.equals(InfoEventService.class.getName())) {
                    return true;
                }
            }
        } catch (Throwable unused) {
        }
        return false;
    }
}
