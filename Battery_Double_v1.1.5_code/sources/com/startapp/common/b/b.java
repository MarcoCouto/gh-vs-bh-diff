package com.startapp.common.b;

import java.util.HashMap;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private final a f4187a;

    /* compiled from: StartAppSDK */
    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public int f4188a;
        /* access modifiers changed from: private */
        public Map<String, String> b = new HashMap();
        /* access modifiers changed from: private */
        public long c;
        /* access modifiers changed from: private */
        public long d = 100;
        /* access modifiers changed from: private */
        public boolean e = false;
        /* access modifiers changed from: private */
        public boolean f = false;

        public a(int i) {
            this.f4188a = i;
        }

        public final a a(Map<String, String> map) {
            if (map != null) {
                this.b.putAll(map);
            }
            return this;
        }

        public final a a(String str, String str2) {
            this.b.put(str, str2);
            return this;
        }

        public final a a(long j) {
            this.c = j;
            return this;
        }

        public final a a(boolean z) {
            this.e = z;
            return this;
        }

        public final a a() {
            this.f = true;
            return this;
        }

        public final b b() {
            return new b(this, 0);
        }
    }

    /* synthetic */ b(a aVar, byte b) {
        this(aVar);
    }

    private b(a aVar) {
        this.f4187a = aVar;
    }

    public final int a() {
        return this.f4187a.f4188a;
    }

    public final Map<String, String> b() {
        return this.f4187a.b;
    }

    public final long c() {
        return this.f4187a.c;
    }

    public final long d() {
        return this.f4187a.d;
    }

    public final boolean e() {
        return this.f4187a.e;
    }

    public final boolean f() {
        return this.f4187a.f;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("RunnerRequest: ");
        sb.append(this.f4187a.f4188a);
        sb.append(" ");
        sb.append(this.f4187a.c);
        sb.append(" ");
        sb.append(this.f4187a.e);
        sb.append(" ");
        sb.append(this.f4187a.d);
        sb.append(" ");
        sb.append(this.f4187a.b);
        return sb.toString();
    }
}
