package com.startapp.common;

/* compiled from: StartAppSDK */
public final class e extends Exception {
    private boolean b;
    private int c;

    public e(String str, Throwable th, boolean z, int i) {
        super(str, th);
        this.b = false;
        this.c = 0;
        this.b = z;
        this.c = i;
    }

    public e(String str, Throwable th, int i) {
        this(str, th, false, i);
    }

    public final int a() {
        return this.c;
    }

    public e() {
        this.b = false;
        this.c = 0;
    }

    public e(String str, Throwable th) {
        this(str, th, 0);
    }

    private e(String str, Throwable th, byte b2) {
        this(str, th, false, 0);
    }

    public final boolean b() {
        return this.b;
    }
}
