package com.startapp.common.d;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private static CookieManager f4194a;
    private String b;
    private List<String> c;
    private List<String> d;

    @RequiresApi(api = 9)
    public static void a(Context context) {
        f4194a = new CookieManager(new b(context), CookiePolicy.ACCEPT_ALL);
    }

    public static void a(HttpURLConnection httpURLConnection, String str) {
        if (VERSION.SDK_INT >= 9) {
            CookieManager cookieManager = f4194a;
            if (cookieManager != null) {
                Map map = cookieManager.get(URI.create(str), httpURLConnection.getRequestProperties());
                if (map != null && map.size() > 0 && ((List) map.get("Cookie")).size() > 0) {
                    httpURLConnection.addRequestProperty("Cookie", TextUtils.join(RequestParameters.EQUAL, (Iterable) map.get("Cookie")));
                }
            }
        }
    }

    public static CookieManager a() {
        return f4194a;
    }

    public final String b() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final List<String> c() {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        return this.c;
    }

    public final List<String> d() {
        if (this.d == null) {
            this.d = new ArrayList();
        }
        return this.d;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("VASTVideoClicks [clickThrough=");
        sb.append(this.b);
        sb.append(", clickTracking=[");
        sb.append(this.c);
        sb.append("], customClick=[");
        sb.append(this.d);
        sb.append("] ]");
        return sb.toString();
    }
}
