package com.startapp.common.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RequiresApi(api = 9)
/* compiled from: StartAppSDK */
public final class b implements CookieStore {

    /* renamed from: a reason: collision with root package name */
    private final CookieStore f4195a = new CookieManager().getCookieStore();
    private final SharedPreferences b;

    public b(Context context) {
        this.b = context.getApplicationContext().getSharedPreferences("com.startapp.android.publish.CookiePrefsFile", 0);
        String string = this.b.getString("names", null);
        if (string != null) {
            for (String valueOf : TextUtils.split(string, ";")) {
                String string2 = this.b.getString("cookie_".concat(String.valueOf(valueOf)), null);
                if (string2 != null) {
                    HttpCookie httpCookie = (HttpCookie) com.startapp.common.c.b.a(string2, HttpCookie.class);
                    if (httpCookie != null) {
                        if (httpCookie.hasExpired()) {
                            a(httpCookie);
                            a();
                        } else {
                            this.f4195a.add(URI.create(httpCookie.getDomain()), httpCookie);
                        }
                    }
                }
            }
        }
    }

    public final void add(URI uri, HttpCookie httpCookie) {
        String b2 = b(httpCookie);
        this.f4195a.add(uri, httpCookie);
        Editor edit = this.b.edit();
        edit.putString("cookie_".concat(String.valueOf(b2)), com.startapp.common.c.b.a((Object) httpCookie));
        edit.commit();
        a();
    }

    public final List<HttpCookie> get(URI uri) {
        return this.f4195a.get(uri);
    }

    public final List<HttpCookie> getCookies() {
        return this.f4195a.getCookies();
    }

    public final List<URI> getURIs() {
        return this.f4195a.getURIs();
    }

    public final boolean remove(URI uri, HttpCookie httpCookie) {
        if (!this.f4195a.remove(uri, httpCookie)) {
            return false;
        }
        a(httpCookie);
        a();
        return true;
    }

    public final boolean removeAll() {
        if (!this.f4195a.removeAll()) {
            return false;
        }
        Editor edit = this.b.edit();
        edit.clear();
        edit.commit();
        a();
        return true;
    }

    private void a(HttpCookie httpCookie) {
        Editor edit = this.b.edit();
        StringBuilder sb = new StringBuilder("cookie_");
        sb.append(b(httpCookie));
        edit.remove(sb.toString());
        edit.commit();
    }

    private void a() {
        Editor edit = this.b.edit();
        edit.putString("names", TextUtils.join(";", b()));
        edit.commit();
    }

    private static String b(HttpCookie httpCookie) {
        StringBuilder sb = new StringBuilder();
        sb.append(httpCookie.getDomain());
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(httpCookie.getName());
        return sb.toString();
    }

    private Set<String> b() {
        HashSet hashSet = new HashSet();
        for (HttpCookie b2 : this.f4195a.getCookies()) {
            hashSet.add(b(b2));
        }
        return hashSet;
    }
}
