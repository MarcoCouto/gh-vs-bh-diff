package com.startapp.common.a;

import android.app.Activity;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.os.StatFs;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.GravityCompat;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.webkit.WebView;
import com.startapp.common.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class c {

    /* compiled from: StartAppSDK */
    public interface a {
        void T();
    }

    public static boolean a() {
        return VERSION.SDK_INT >= 12;
    }

    public static void a(View view, float f) {
        if (VERSION.SDK_INT >= 11) {
            view.setAlpha(f);
        }
    }

    public static void a(ViewTreeObserver viewTreeObserver, OnGlobalLayoutListener onGlobalLayoutListener) {
        if (VERSION.SDK_INT >= 16) {
            viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener);
        } else {
            viewTreeObserver.removeGlobalOnLayoutListener(onGlobalLayoutListener);
        }
    }

    public static boolean a(Context context) {
        try {
            if (VERSION.SDK_INT >= 17) {
                if (VERSION.SDK_INT < 21) {
                    if (Global.getInt(context.getContentResolver(), "install_non_market_apps") != 1) {
                        return false;
                    }
                    return true;
                }
            }
            if (Secure.getInt(context.getContentResolver(), "install_non_market_apps") != 1) {
                return false;
            }
            return true;
        } catch (SettingNotFoundException unused) {
            return false;
        }
    }

    public static int a(Activity activity, int i, boolean z) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int i2 = 0;
        switch (i) {
            case 1:
                if (VERSION.SDK_INT > 8 && !z && (rotation == 1 || rotation == 2)) {
                    i2 = 9;
                    break;
                }
            case 2:
                if (VERSION.SDK_INT > 8 && !z && rotation != 0 && rotation != 1) {
                    i2 = 8;
                    break;
                }
            default:
                i2 = 1;
                break;
        }
        try {
            activity.setRequestedOrientation(i2);
        } catch (Exception unused) {
        }
        return i2;
    }

    public static boolean b() {
        return VERSION.SDK_INT >= 14;
    }

    public static void a(WebView webView) {
        if (VERSION.SDK_INT >= 11) {
            webView.onPause();
            return;
        }
        try {
            Class.forName("android.webkit.WebView").getMethod("onPause", null).invoke(webView, null);
        } catch (Exception unused) {
        }
    }

    public static void b(WebView webView) {
        if (VERSION.SDK_INT >= 11) {
            webView.onResume();
            return;
        }
        try {
            Class.forName("android.webkit.WebView").getMethod("onResume", null).invoke(webView, null);
        } catch (Exception unused) {
        }
    }

    public static void a(View view, final a aVar) {
        if (VERSION.SDK_INT >= 11) {
            view.addOnLayoutChangeListener(new OnLayoutChangeListener() {
                public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    aVar.T();
                }
            });
        }
    }

    public static Long a(MemoryInfo memoryInfo) {
        if (VERSION.SDK_INT >= 16) {
            return Long.valueOf(memoryInfo.totalMem);
        }
        return null;
    }

    public static Set<String> b(Context context) {
        HashSet hashSet = new HashSet();
        if (VERSION.SDK_INT >= 11) {
            try {
                InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
                for (InputMethodInfo enabledInputMethodSubtypeList : inputMethodManager.getEnabledInputMethodList()) {
                    for (InputMethodSubtype inputMethodSubtype : inputMethodManager.getEnabledInputMethodSubtypeList(enabledInputMethodSubtypeList, true)) {
                        if (inputMethodSubtype.getMode().equals("keyboard")) {
                            hashSet.add(inputMethodSubtype.getLocale());
                        }
                    }
                }
            } catch (Exception e) {
                new StringBuilder("Failed to retreive keyboard input langs: ").append(e.getLocalizedMessage());
            }
        }
        return hashSet;
    }

    public static long a(File file) {
        long j;
        long j2;
        if (file != null) {
            try {
                if (file.isDirectory()) {
                    if (VERSION.SDK_INT >= 9) {
                        return file.getFreeSpace();
                    }
                    StatFs statFs = new StatFs(file.getPath());
                    if (VERSION.SDK_INT >= 18) {
                        j2 = statFs.getBlockSizeLong();
                        j = statFs.getFreeBlocksLong();
                    } else {
                        j2 = (long) statFs.getBlockSize();
                        j = (long) statFs.getFreeBlocks();
                    }
                    return j2 * j;
                }
            } catch (Exception e) {
                new StringBuilder("Failed calculating free space with error: ").append(e.getMessage());
                return -1;
            }
        }
        return -1;
    }

    public static boolean a(Context context, String str) {
        try {
            return VERSION.SDK_INT >= 23 ? context.checkSelfPermission(str) == 0 : context.checkCallingOrSelfPermission(str) == 0;
        } catch (Throwable th) {
            if ("Exception while checking permission: ".concat(String.valueOf(th)) != null) {
                th.getMessage();
            }
            return false;
        }
    }

    public static List<ScanResult> a(Context context, WifiManager wifiManager) {
        if (context == null || wifiManager == null) {
            return null;
        }
        boolean z = true;
        if (VERSION.SDK_INT >= 23 && !a(context, "android.permission.ACCESS_FINE_LOCATION") && !a(context, "android.permission.ACCESS_COARSE_LOCATION")) {
            z = false;
        }
        if (!z) {
            return null;
        }
        try {
            return wifiManager.getScanResults();
        } catch (Exception e) {
            new StringBuilder("Failed to retreive WIFI scan results: ").append(e.getLocalizedMessage());
            return null;
        }
    }

    public static int a(int i) {
        if (VERSION.SDK_INT >= 17) {
            return i;
        }
        switch (i) {
            case 16:
                return 0;
            case 17:
                return 1;
            case 20:
                return 9;
            case 21:
                return 11;
            case GravityCompat.START /*8388611*/:
                if (VERSION.SDK_INT < 14) {
                    return 3;
                }
                return i;
            case GravityCompat.END /*8388613*/:
                if (VERSION.SDK_INT < 14) {
                    return 5;
                }
                return i;
            default:
                return i;
        }
    }

    public static String a(Context context, TelephonyManager telephonyManager) {
        if (VERSION.SDK_INT >= 17 && (a(context, "android.permission.ACCESS_FINE_LOCATION") || a(context, "android.permission.ACCESS_COARSE_LOCATION"))) {
            List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
            if (allCellInfo != null) {
                for (CellInfo cellInfo : allCellInfo) {
                    if (cellInfo.isRegistered()) {
                        try {
                            Object invoke = Class.forName(cellInfo.getClass().getName()).getMethod("getCellSignalStrength", null).invoke(cellInfo, null);
                            return Integer.toString(Integer.parseInt(Class.forName(invoke.getClass().getName()).getMethod("getTimingAdvance", null).invoke(invoke, null).toString()));
                        } catch (Exception unused) {
                            return null;
                        }
                    }
                }
            }
        }
        return null;
    }

    public static int c(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException | Exception unused) {
            return 0;
        }
    }

    public static String d(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException | Exception unused) {
            return null;
        }
    }

    public static String a(String str, Context context) {
        String str2;
        try {
            str2 = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).sourceDir;
        } catch (NameNotFoundException unused) {
            str2 = null;
        }
        if (str2 != null) {
            return a((InputStream) new FileInputStream(str2), str);
        }
        return null;
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0039 */
    @VisibleForTesting
    private static String a(InputStream inputStream, String str) {
        int i;
        StringBuilder sb = new StringBuilder();
        try {
            byte[] bArr = new byte[8192];
            MessageDigest instance = MessageDigest.getInstance(str);
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                } else if (read > 0) {
                    instance.update(bArr, 0, read);
                }
            }
            byte[] digest = instance.digest();
            for (byte b : digest) {
                sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
            }
        } catch (Exception ) {
            inputStream.close();
            return sb.toString().toUpperCase();
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
            throw th;
        }
        try {
            inputStream.close();
        } catch (IOException unused2) {
        }
        return sb.toString().toUpperCase();
    }

    public static int e(Context context) {
        int i = 0;
        try {
            for (PackageInfo packageInfo : a(context.getPackageManager())) {
                if (!a(packageInfo) || packageInfo.packageName.equals(Constants.f4160a)) {
                    i++;
                }
            }
        } catch (Exception unused) {
        }
        return i;
    }

    public static boolean a(PackageInfo packageInfo) {
        return ((packageInfo.applicationInfo.flags & 1) == 0 && (packageInfo.applicationInfo.flags & 128) == 0) ? false : true;
    }

    public static boolean a(Context context, String str, int i) {
        try {
            if (context.getPackageManager().getPackageInfo(str, 128).versionCode >= i) {
                return true;
            }
            return false;
        } catch (NameNotFoundException unused) {
            return false;
        } catch (Exception unused2) {
            return false;
        }
    }

    @VisibleForTesting
    public static List<PackageInfo> a(PackageManager packageManager) {
        String str = new String("getInstalledPackages");
        return (List) packageManager.getClass().getMethod(str, new Class[]{Integer.TYPE}).invoke(packageManager, new Object[]{Integer.valueOf(8192)});
    }

    public static boolean f(Context context) {
        int i;
        try {
            if (VERSION.SDK_INT < 17) {
                i = Secure.getInt(context.getContentResolver(), "adb_enabled", 0);
            } else {
                i = Global.getInt(context.getContentResolver(), "adb_enabled", 0);
            }
            if (i != 0) {
                return true;
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean g(Context context) {
        try {
            return com.b.a.a.a.e.a.a(context);
        } catch (Throwable unused) {
            return false;
        }
    }

    public static boolean h(Context context) {
        try {
            return com.startapp.android.a.a.a(context);
        } catch (Throwable unused) {
            return false;
        }
    }

    public static String i(Context context) {
        try {
            Signature[] signatureArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures;
            if (signatureArr == null || signatureArr.length <= 0) {
                return null;
            }
            if (signatureArr.length == 1) {
                return signatureArr[0].toCharsString();
            }
            Arrays.sort(signatureArr, new Comparator<Signature>() {
                public final /* synthetic */ int compare(Object obj, Object obj2) {
                    return ((Signature) obj).toCharsString().compareTo(((Signature) obj2).toCharsString());
                }
            });
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < signatureArr.length; i++) {
                sb.append(signatureArr[i].toCharsString());
                if (i < signatureArr.length - 1) {
                    sb.append(';');
                }
            }
            return sb.toString();
        } catch (Exception unused) {
            return null;
        }
    }
}
