package com.startapp.common.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.startapp.common.e;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.nio.ByteOrder;

/* compiled from: StartAppSDK */
public final class g {

    /* compiled from: StartAppSDK */
    public static class a {

        /* renamed from: a reason: collision with root package name */
        private String f4175a;
        private String b;

        public final String a() {
            return this.f4175a;
        }

        public final void a(String str) {
            this.f4175a = str;
        }

        public final String b() {
            return this.b;
        }

        public final void b(String str) {
            this.b = str;
        }

        public final String toString() {
            StringBuilder sb = new StringBuilder("HttpResult: ");
            sb.append(this.b);
            sb.append(" ");
            sb.append(this.f4175a);
            return sb.toString();
        }
    }

    /* compiled from: StartAppSDK */
    public static class b {

        /* renamed from: a reason: collision with root package name */
        private String f4176a;
        private String b;
        private String c;

        public final String a() {
            return this.f4176a;
        }

        public final String b() {
            return this.b;
        }

        public final String c() {
            return this.c;
        }

        /* access modifiers changed from: 0000 */
        public final void a(String str) {
            this.f4176a = str;
        }

        /* access modifiers changed from: 0000 */
        public final void b(String str) {
            this.b = str;
        }

        /* access modifiers changed from: 0000 */
        public final void c(String str) {
            this.c = str;
        }
    }

    static {
        if (VERSION.SDK_INT < 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    public static String a(String str, byte[] bArr, String str2, boolean z) {
        return a(str, bArr, str2, z, "application/json");
    }

    /* JADX WARNING: type inference failed for: r6v1, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r1v1 */
    /* JADX WARNING: type inference failed for: r6v2 */
    /* JADX WARNING: type inference failed for: r6v5 */
    /* JADX WARNING: type inference failed for: r1v3 */
    /* JADX WARNING: type inference failed for: r1v7 */
    /* JADX WARNING: type inference failed for: r1v8 */
    /* JADX WARNING: type inference failed for: r6v7 */
    /* JADX WARNING: type inference failed for: r6v8 */
    /* JADX WARNING: type inference failed for: r6v9 */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:14|(2:18|19)|20|21) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0028, code lost:
        r7 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0029, code lost:
        r2 = null;
        r6 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002c, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x002d, code lost:
        r8 = r7;
        r2 = null;
        r7 = 0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0027 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0021 A[SYNTHETIC, Splitter:B:18:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0028 A[Catch:{ Exception -> 0x002c, all -> 0x0028 }, ExcHandler: all (th java.lang.Throwable), Splitter:B:4:0x0008] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00ef A[SYNTHETIC, Splitter:B:78:0x00ef] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00f4  */
    /* JADX WARNING: Unknown variable types count: 2 */
    private static String a(String str, byte[] bArr, String str2, boolean z, String str3) {
        ? r6;
        InputStream inputStream;
        int i;
        ? r1;
        OutputStream outputStream;
        String str4 = 0;
        try {
            HttpURLConnection b2 = b(str, bArr, str2, z, str3);
            if (bArr != null) {
                try {
                    if (bArr.length > 0) {
                        try {
                            outputStream = b2.getOutputStream();
                            try {
                                outputStream.write(bArr);
                                if (outputStream != null) {
                                    try {
                                        outputStream.flush();
                                        outputStream.close();
                                    } catch (IOException unused) {
                                    }
                                }
                            } catch (Throwable th) {
                                th = th;
                                if (outputStream != null) {
                                    outputStream.flush();
                                    outputStream.close();
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            outputStream = null;
                            if (outputStream != null) {
                            }
                            throw th;
                        }
                    }
                } catch (Exception e) {
                    e = e;
                    inputStream = null;
                    r1 = b2;
                    try {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Error execute Exception ");
                        sb.append(e.getMessage());
                        throw new e(sb.toString(), (Throwable) e, i);
                    } catch (Throwable th3) {
                        th = th3;
                        r6 = r1;
                        if (inputStream != null) {
                        }
                        if (r6 != 0) {
                        }
                        throw th;
                    }
                } catch (Throwable th4) {
                }
            }
            i = b2.getResponseCode();
            if (i != 200) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Error code = [");
                sb2.append(i);
                sb2.append(']');
                inputStream = b2.getErrorStream();
                if (inputStream != null) {
                    try {
                        StringWriter stringWriter = new StringWriter();
                        char[] cArr = new char[1024];
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                        while (true) {
                            int read = bufferedReader.read(cArr);
                            if (read == -1) {
                                break;
                            }
                            stringWriter.write(cArr, 0, read);
                        }
                        sb2.append(stringWriter.toString());
                    } catch (Exception e2) {
                        e = e2;
                        r1 = b2;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Error execute Exception ");
                        sb3.append(e.getMessage());
                        throw new e(sb3.toString(), (Throwable) e, i);
                    } catch (Throwable th5) {
                        th = th5;
                        r6 = b2;
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException unused2) {
                            }
                        }
                        if (r6 != 0) {
                            r6.disconnect();
                        }
                        throw th;
                    }
                }
                throw new Exception(sb2.toString());
            }
            InputStream inputStream2 = b2.getInputStream();
            if (inputStream2 != null) {
                try {
                    StringWriter stringWriter2 = new StringWriter();
                    char[] cArr2 = new char[1024];
                    BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(inputStream2, "UTF-8"));
                    while (true) {
                        int read2 = bufferedReader2.read(cArr2);
                        if (read2 == -1) {
                            break;
                        }
                        stringWriter2.write(cArr2, 0, read2);
                    }
                    str4 = stringWriter2.toString();
                } catch (Exception e3) {
                    r1 = b2;
                    inputStream = inputStream2;
                    e = e3;
                    StringBuilder sb32 = new StringBuilder();
                    sb32.append("Error execute Exception ");
                    sb32.append(e.getMessage());
                    throw new e(sb32.toString(), (Throwable) e, i);
                } catch (Throwable th6) {
                    th = th6;
                    inputStream = inputStream2;
                    r6 = b2;
                    if (inputStream != null) {
                    }
                    if (r6 != 0) {
                    }
                    throw th;
                }
            }
            if (inputStream2 != null) {
                try {
                    inputStream2.close();
                } catch (IOException unused3) {
                }
            }
            if (b2 != 0) {
                b2.disconnect();
            }
            return str4;
        } catch (Exception e4) {
            e = e4;
            inputStream = null;
            i = 0;
            r1 = str4;
            StringBuilder sb322 = new StringBuilder();
            sb322.append("Error execute Exception ");
            sb322.append(e.getMessage());
            throw new e(sb322.toString(), (Throwable) e, i);
        } catch (Throwable th7) {
            th = th7;
            r6 = 0;
            inputStream = null;
            if (inputStream != null) {
            }
            if (r6 != 0) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c5, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c6, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c8, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00c9, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00cb, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00cc, code lost:
        r3 = null;
        r12 = 0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00c8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00f6 A[SYNTHETIC, Splitter:B:67:0x00f6] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00fb  */
    public static a a(String str, String str2, boolean z) {
        HttpURLConnection httpURLConnection;
        InputStream inputStream;
        int i;
        boolean z2 = true;
        HttpURLConnection httpURLConnection2 = null;
        try {
            httpURLConnection = b(str, null, str2, z, null);
            try {
                i = httpURLConnection.getResponseCode();
                if (i == 200) {
                    if (VERSION.SDK_INT >= 9) {
                        CookieManager a2 = com.startapp.common.d.a.a();
                        if (a2 != null) {
                            a2.put(URI.create(str), httpURLConnection.getHeaderFields());
                        }
                    }
                    InputStream inputStream2 = httpURLConnection.getInputStream();
                    try {
                        a aVar = new a();
                        aVar.b(httpURLConnection.getContentType());
                        if (inputStream2 != null) {
                            StringWriter stringWriter = new StringWriter();
                            char[] cArr = new char[1024];
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream2, "UTF-8"));
                            while (true) {
                                int read = bufferedReader.read(cArr);
                                if (read == -1) {
                                    break;
                                }
                                stringWriter.write(cArr, 0, read);
                            }
                            aVar.a(stringWriter.toString());
                        }
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (IOException unused) {
                            }
                        }
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        return aVar;
                    } catch (Exception e) {
                        inputStream = inputStream2;
                        httpURLConnection2 = httpURLConnection;
                        e = e;
                        z2 = false;
                        try {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Error execute Exception ");
                            sb.append(e.getMessage());
                            throw new e(sb.toString(), e, z2, i);
                        } catch (Throwable th) {
                            th = th;
                            httpURLConnection = httpURLConnection2;
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException unused2) {
                                }
                            }
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        inputStream = inputStream2;
                        th = th2;
                        if (inputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Error sendGetWithResponse code = [");
                    sb2.append(i);
                    sb2.append(']');
                    inputStream = httpURLConnection.getErrorStream();
                    if (inputStream != null) {
                        try {
                            StringWriter stringWriter2 = new StringWriter();
                            char[] cArr2 = new char[1024];
                            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                            while (true) {
                                int read2 = bufferedReader2.read(cArr2);
                                if (read2 == -1) {
                                    break;
                                }
                                stringWriter2.write(cArr2, 0, read2);
                            }
                            sb2.append(stringWriter2.toString());
                        } catch (Exception e2) {
                            e = e2;
                            httpURLConnection2 = httpURLConnection;
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Error execute Exception ");
                            sb3.append(e.getMessage());
                            throw new e(sb3.toString(), e, z2, i);
                        } catch (Throwable th3) {
                            th = th3;
                            if (inputStream != null) {
                            }
                            if (httpURLConnection != null) {
                            }
                            throw th;
                        }
                    }
                    throw new e(sb2.toString(), null, true, i);
                }
            } catch (Exception e3) {
                e = e3;
                inputStream = null;
                z2 = false;
                httpURLConnection2 = httpURLConnection;
                StringBuilder sb32 = new StringBuilder();
                sb32.append("Error execute Exception ");
                sb32.append(e.getMessage());
                throw new e(sb32.toString(), e, z2, i);
            } catch (Throwable th4) {
            }
        } catch (Exception e4) {
            e = e4;
            inputStream = null;
            i = 0;
            StringBuilder sb322 = new StringBuilder();
            sb322.append("Error execute Exception ");
            sb322.append(e.getMessage());
            throw new e(sb322.toString(), e, z2, i);
        } catch (Throwable th5) {
            th = th5;
            httpURLConnection = null;
            inputStream = null;
            if (inputStream != null) {
            }
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    private static HttpURLConnection b(String str, byte[] bArr, String str2, boolean z, String str3) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.addRequestProperty(HttpRequest.HEADER_CACHE_CONTROL, "no-cache");
        com.startapp.common.d.a.a(httpURLConnection, str);
        if (!(str2 == null || str2.compareTo("-1") == 0)) {
            httpURLConnection.addRequestProperty("User-Agent", str2);
        }
        httpURLConnection.setRequestProperty("Accept", "application/json;text/html;text/plain");
        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.setConnectTimeout(10000);
        if (bArr != null) {
            httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setFixedLengthStreamingMode(bArr.length);
            httpURLConnection.setRequestProperty("Content-Type", str3);
            if (z) {
                httpURLConnection.setRequestProperty(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
            }
        } else {
            httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
        }
        return httpURLConnection;
    }

    @SuppressLint({"MissingPermission"})
    public static String a(Context context) {
        String str = "e100";
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                if (c.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
                    str = "e102";
                    if (VERSION.SDK_INT >= 23) {
                        Network activeNetwork = connectivityManager.getActiveNetwork();
                        if (activeNetwork != null) {
                            NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
                            if (networkCapabilities != null) {
                                if (networkCapabilities.hasTransport(1)) {
                                    return "WIFI";
                                }
                                if (networkCapabilities.hasTransport(0)) {
                                    str = c(context);
                                }
                            }
                        }
                    } else {
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                            String typeName = activeNetworkInfo.getTypeName();
                            if (typeName.toLowerCase().compareTo("WIFI".toLowerCase()) == 0) {
                                return "WIFI";
                            }
                            if (typeName.toLowerCase().compareTo("MOBILE".toLowerCase()) == 0) {
                                str = c(context);
                            }
                        }
                    }
                } else {
                    str = "e105";
                }
            }
            return str;
        } catch (Exception unused) {
            return "e105";
        }
    }

    private static String c(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        return telephonyManager != null ? Integer.toString(telephonyManager.getNetworkType()) : "e101";
    }

    public static b a(Context context, String str) {
        if (str.toLowerCase().compareTo("WIFI".toLowerCase()) == 0) {
            return d(context);
        }
        return null;
    }

    @SuppressLint({"MissingPermission"})
    private static b d(Context context) {
        String str = "e103";
        b bVar = new b();
        try {
            if (c.a(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiInfo connectionInfo = ((WifiManager) context.getApplicationContext().getSystemService("wifi")).getConnectionInfo();
                if (connectionInfo != null) {
                    int rssi = connectionInfo.getRssi();
                    bVar.c(Integer.toString(WifiManager.calculateSignalLevel(rssi, 5)));
                    bVar.b(Integer.toString(rssi));
                    str = null;
                }
            } else {
                str = "e105";
            }
        } catch (Exception unused) {
            str = "e100";
        }
        bVar.a(str);
        return bVar;
    }

    public static Boolean b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null && c.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                return Boolean.valueOf(activeNetworkInfo.isRoaming());
            }
        }
        return null;
    }

    public static String a(WifiInfo wifiInfo) {
        int ipAddress = wifiInfo.getIpAddress();
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }
        try {
            return InetAddress.getByAddress(BigInteger.valueOf((long) ipAddress).toByteArray()).getHostAddress();
        } catch (Exception unused) {
            return null;
        }
    }
}
