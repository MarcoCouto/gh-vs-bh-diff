package com.startapp.common.a;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* compiled from: StartAppSDK */
public final class f {

    /* renamed from: a reason: collision with root package name */
    private String f4174a;
    private int b;

    public static List<Location> a(Context context, boolean z, boolean z2) {
        ArrayList arrayList = new ArrayList();
        LinkedList<String> linkedList = new LinkedList<>();
        if (z && c.a(context, "android.permission.ACCESS_FINE_LOCATION")) {
            linkedList.add("gps");
            linkedList.add("passive");
            linkedList.add("network");
        } else if (z2 && c.a(context, "android.permission.ACCESS_COARSE_LOCATION")) {
            linkedList.add("network");
        }
        for (String lastKnownLocation : linkedList) {
            try {
                LocationManager locationManager = (LocationManager) context.getSystemService("location");
                if (locationManager == null) {
                    return null;
                }
                Location lastKnownLocation2 = locationManager.getLastKnownLocation(lastKnownLocation);
                if (lastKnownLocation2 != null) {
                    arrayList.add(lastKnownLocation2);
                }
            } catch (Exception | IllegalArgumentException | SecurityException unused) {
            }
        }
        return arrayList;
    }

    public static String a(List<Location> list) {
        StringBuilder sb = new StringBuilder();
        if (list == null || list.size() <= 0) {
            return sb.toString();
        }
        for (Location location : list) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(location.getLongitude());
            sb2.append(",");
            sb.append(sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append(location.getLatitude());
            sb3.append(",");
            sb.append(sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append(location.getAccuracy());
            sb4.append(",");
            sb.append(sb4.toString());
            StringBuilder sb5 = new StringBuilder();
            sb5.append(location.getProvider());
            sb5.append(",");
            sb.append(sb5.toString());
            StringBuilder sb6 = new StringBuilder();
            sb6.append(location.getTime());
            sb6.append(";");
            sb.append(sb6.toString());
        }
        return sb.toString().substring(0, sb.toString().length() - 1);
    }

    public f(String str, int i) {
        this.f4174a = str;
        this.b = i;
    }

    public final String a() {
        return this.f4174a;
    }

    public final int b() {
        return this.b;
    }
}
