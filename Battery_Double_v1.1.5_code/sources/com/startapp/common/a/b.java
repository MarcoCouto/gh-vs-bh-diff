package com.startapp.common.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.f;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class b {

    /* renamed from: a reason: collision with root package name */
    private static b f4165a = new b();
    private volatile C0094b b;

    /* compiled from: StartAppSDK */
    public static final class a implements ServiceConnection {

        /* renamed from: a reason: collision with root package name */
        private boolean f4167a = false;
        private final LinkedBlockingQueue<IBinder> b = new LinkedBlockingQueue<>(1);

        public final void onServiceDisconnected(ComponentName componentName) {
        }

        protected a() {
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.b.put(iBinder);
            } catch (InterruptedException unused) {
            }
        }

        public final IBinder a() {
            if (!this.f4167a) {
                this.f4167a = true;
                return (IBinder) this.b.take();
            }
            throw new IllegalStateException();
        }
    }

    /* renamed from: com.startapp.common.a.b$b reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public static class C0094b {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public String f4168a;
        private String b;
        private boolean c;
        private String d;

        /* synthetic */ C0094b(String str, byte b2) {
            this(str);
        }

        /* synthetic */ C0094b(String str, String str2, boolean z, byte b2) {
            this(str, str2, z);
        }

        private C0094b(String str, String str2, boolean z) {
            this.b = "";
            this.c = false;
            this.d = null;
            this.f4168a = str;
            this.b = str2;
            this.c = z;
        }

        private C0094b(String str) {
            this.b = "";
            this.c = false;
            this.d = null;
            this.f4168a = "0";
            this.d = str;
        }

        public final String a() {
            return this.f4168a;
        }

        public final boolean b() {
            return this.c;
        }

        public final String c() {
            return this.b;
        }

        public final String d() {
            return this.d;
        }
    }

    /* compiled from: StartAppSDK */
    static final class c implements IInterface {

        /* renamed from: a reason: collision with root package name */
        private IBinder f4169a;

        public c(IBinder iBinder) {
            this.f4169a = iBinder;
        }

        public final IBinder asBinder() {
            return this.f4169a;
        }

        public final String a() {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                this.f4169a.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        public final boolean b() {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                boolean z = true;
                obtain.writeInt(1);
                this.f4169a.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z = false;
                }
                return z;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
    }

    private b() {
    }

    public static b a() {
        return f4165a;
    }

    private static boolean a(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    public final C0094b a(final Context context) {
        if (this.b == null || this.b.f4168a.equals("0")) {
            synchronized (b.class) {
                if (this.b == null || this.b.f4168a.equals("0")) {
                    final SynchronousQueue synchronousQueue = new SynchronousQueue();
                    f.a(com.startapp.common.f.a.HIGH, (Runnable) new Runnable() {
                        public final void run() {
                            try {
                                synchronousQueue.offer(b.c(context));
                            } catch (Exception e) {
                                synchronousQueue.offer(new C0094b(e.getMessage(), 0));
                            } catch (Throwable th) {
                                synchronousQueue.offer(null);
                                throw th;
                            }
                        }
                    });
                    try {
                        this.b = (C0094b) synchronousQueue.poll(1, TimeUnit.SECONDS);
                        if (this.b == null) {
                            this.b = new C0094b("Getting advertisingId took too much time.", 0);
                        }
                    } catch (InterruptedException e) {
                        this.b = new C0094b(e.getMessage(), 0);
                    }
                }
            }
        }
        return this.b;
    }

    private static C0094b d(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            a aVar = new a();
            Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
            intent.setPackage("com.google.android.gms");
            try {
                if (context.getApplicationContext().bindService(intent, aVar, 1)) {
                    c cVar = new c(aVar.a());
                    return new C0094b(cVar.a(), "DEVICE", cVar.b(), 0);
                }
                context.getApplicationContext().unbindService(aVar);
                return new C0094b("Google Play connection failed", 0);
            } catch (Exception e) {
                return new C0094b(e.getMessage(), 0);
            } finally {
                context.getApplicationContext().unbindService(aVar);
            }
        } catch (Exception e2) {
            return new C0094b(e2.getMessage(), 0);
        }
    }

    /* access modifiers changed from: private */
    public static C0094b c(Context context) {
        if (!a("com.google.android.gms.ads.identifier.AdvertisingIdClient")) {
            return d(context);
        }
        try {
            Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{context.getApplicationContext()});
            return new C0094b((String) invoke.getClass().getMethod("getId", new Class[0]).invoke(invoke, new Object[0]), "APP", ((Boolean) invoke.getClass().getMethod(RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0])).booleanValue(), 0);
        } catch (Exception unused) {
            return d(context);
        }
    }
}
