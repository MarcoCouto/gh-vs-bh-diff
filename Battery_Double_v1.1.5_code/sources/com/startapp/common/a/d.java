package com.startapp.common.a;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import com.startapp.common.c.b;
import java.io.BufferedInputStream;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public final class d {

    /* renamed from: a reason: collision with root package name */
    private String f4171a;
    private Integer b;
    private Integer c;
    private Integer d;
    private Integer e;
    private Integer f;
    private Integer g;
    private String h;
    private Integer i;
    private List<b> j;
    private String k;
    private List<String> l;
    private List<String> m;

    /* compiled from: StartAppSDK */
    static class a extends FilterInputStream {
        public a(InputStream inputStream) {
            super(inputStream);
        }

        public final long skip(long j) {
            long j2 = 0;
            while (j2 < j) {
                long skip = this.in.skip(j - j2);
                if (skip == 0) {
                    if (read() < 0) {
                        break;
                    }
                    skip = 1;
                }
                j2 += skip;
            }
            return j2;
        }
    }

    public static Bitmap a(String str) {
        byte[] decode = Base64.decode(str, 0);
        return BitmapFactory.decodeByteArray(decode, 0, decode.length);
    }

    public static Drawable a(Resources resources, String str) {
        return new BitmapDrawable(resources, a(str));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002f, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003d, code lost:
        if (r5 == null) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0040, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0027, code lost:
        if (r5 != null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0029, code lost:
        r5.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002d, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002d A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x000c] */
    public static Bitmap b(String str) {
        Bitmap bitmap;
        HttpURLConnection httpURLConnection;
        try {
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                bitmap = BitmapFactory.decodeStream(new a(inputStream));
                bufferedInputStream.close();
                inputStream.close();
            } catch (Exception unused) {
            } catch (Throwable th) {
            }
        } catch (Exception unused2) {
            httpURLConnection = null;
            bitmap = null;
        } catch (Throwable th2) {
            Throwable th3 = th2;
            httpURLConnection = null;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th3;
        }
    }

    public final void c(String str) {
        this.f4171a = str;
    }

    public final void a(Integer num) {
        this.b = num;
    }

    public final void b(Integer num) {
        this.c = num;
    }

    public final void c(Integer num) {
        this.d = num;
    }

    public final void d(Integer num) {
        this.e = num;
    }

    public final void e(Integer num) {
        this.f = num;
    }

    public final void f(Integer num) {
        this.g = num;
    }

    public final void d(String str) {
        this.h = str;
    }

    public final void g(Integer num) {
        this.i = num;
    }

    public final List<b> a() {
        if (this.j == null) {
            this.j = new ArrayList();
        }
        return this.j;
    }

    public final void e(String str) {
        this.k = str;
    }

    public final List<String> b() {
        if (this.l == null) {
            this.l = new ArrayList();
        }
        return this.l;
    }

    public final List<String> c() {
        if (this.m == null) {
            this.m = new ArrayList();
        }
        return this.m;
    }

    public final boolean d() {
        Integer num = this.c;
        Integer num2 = this.b;
        if (!(num == null || num2 == null)) {
            if (num.intValue() > 0) {
                if (num2.intValue() > 0) {
                    Integer num3 = this.d;
                    Integer num4 = this.e;
                    if (!(num3 == null || num4 == null)) {
                        if (num3.intValue() > 0) {
                            if ((num4.intValue() > 0) && a().size() != 0) {
                                return true;
                            }
                            return false;
                        }
                    }
                    return false;
                }
            }
        }
        return false;
    }
}
