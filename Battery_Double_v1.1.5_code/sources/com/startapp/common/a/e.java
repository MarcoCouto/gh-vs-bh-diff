package com.startapp.common.a;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.f;
import com.startapp.common.f.a;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public final class e {

    /* renamed from: a reason: collision with root package name */
    private String f4172a;
    private String b;
    private String c;
    private String d;
    private Integer e;
    private Integer f;
    private Integer g;
    private Boolean h;
    private Boolean i;
    private String j;

    private static boolean a(int i2) {
        return i2 > 0 && i2 < 5000;
    }

    public static void a(Context context, String str, Serializable serializable) {
        c(context, str, serializable);
    }

    public static void b(final Context context, final String str, final Serializable serializable) {
        f.a(a.DEFAULT, (Runnable) new Runnable() {
            public final void run() {
                e.c(context, str, serializable);
            }
        });
    }

    public static void c(Context context, String str, Serializable serializable) {
        if (str != null) {
            try {
                a(d(context, null), str, serializable);
            } catch (Exception e2) {
                new StringBuilder("Failed writing to disk: ").append(e2.getLocalizedMessage());
            }
        }
    }

    public static void a(Context context, String str, String str2, Serializable serializable) {
        if (str2 != null) {
            try {
                a(e(context, str), str2, serializable);
            } catch (Exception e2) {
                new StringBuilder("Failed writing to disk: ").append(e2.getLocalizedMessage());
            }
        }
    }

    public static <T> T a(Context context, String str) {
        try {
            return a(d(context, null), str);
        } catch (Exception e2) {
            new StringBuilder("Failed to read from disk: ").append(e2.getLocalizedMessage());
            return null;
        } catch (Error e3) {
            new StringBuilder("Failed to read from disk: ").append(e3.getLocalizedMessage());
            return null;
        }
    }

    public static <T> T a(Context context, String str, String str2) {
        T t;
        if (str2 == null) {
            return null;
        }
        try {
            t = a(e(context, str), str2);
        } catch (Exception e2) {
            new StringBuilder("Failed to read from disk: ").append(e2.getLocalizedMessage());
            t = null;
            return t;
        } catch (Error e3) {
            new StringBuilder("Failed to read from disk: ").append(e3.getLocalizedMessage());
            t = null;
            return t;
        }
        return t;
    }

    public static <T> List<T> b(Context context, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            File file = new File(e(context, str));
            if (file.exists()) {
                if (file.isDirectory()) {
                    String[] list = file.list();
                    if (list == null) {
                        return null;
                    }
                    for (String file2 : list) {
                        File file3 = new File(file, file2);
                        new StringBuilder("Reading file: ").append(file3.getPath());
                        arrayList.add(b(file3));
                    }
                    return arrayList;
                }
            }
            return null;
        } catch (Exception e2) {
            new StringBuilder("Failed to read from disk: ").append(e2.getLocalizedMessage());
        }
    }

    public static void c(Context context, String str) {
        if (str != null) {
            a(new File(d(context, str)));
            a(new File(e(context, str)));
        }
    }

    private static String d(Context context, String str) {
        String str2;
        StringBuilder sb = new StringBuilder();
        sb.append(context.getFilesDir().toString());
        if (str != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(File.separator);
            sb2.append(str);
            str2 = sb2.toString();
        } else {
            str2 = "";
        }
        sb.append(str2);
        return sb.toString();
    }

    private static String e(Context context, String str) {
        String str2;
        StringBuilder sb = new StringBuilder();
        sb.append(context.getCacheDir().toString());
        if (str != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(File.separator);
            sb2.append(str);
            str2 = sb2.toString();
        } else {
            str2 = "";
        }
        sb.append(str2);
        return sb.toString();
    }

    private static void a(String str, String str2, Serializable serializable) {
        File file = new File(str);
        if (file.exists() || file.mkdirs()) {
            File file2 = new File(file, str2);
            new StringBuilder("Writing file: ").append(file2.getPath());
            a(serializable, file2);
        }
    }

    private static <T> T a(String str, String str2) {
        File file = new File(str);
        T t = null;
        if (!file.exists() || !file.isDirectory()) {
            return null;
        }
        File file2 = new File(file, str2);
        if (file2.exists()) {
            new StringBuilder("Reading file: ").append(file2.getPath());
            t = b(file2);
        }
        return t;
    }

    private static void a(File file) {
        if (file.isDirectory()) {
            for (File a2 : file.listFiles()) {
                a(a2);
            }
        }
        file.delete();
    }

    private static void a(Serializable serializable, File file) {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(serializable);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    private static <T> T b(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        T readObject = objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        return readObject;
    }

    public final String a() {
        return this.f4172a;
    }

    public final void a(String str) {
        this.f4172a = str;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final String b() {
        return this.d;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final Integer c() {
        return this.e;
    }

    public final void a(Integer num) {
        this.e = num;
    }

    public final Integer d() {
        return this.f;
    }

    public final void b(Integer num) {
        this.f = num;
    }

    public final Integer e() {
        return this.g;
    }

    public final void c(Integer num) {
        this.g = num;
    }

    public final void a(Boolean bool) {
        this.h = bool;
    }

    public final void b(Boolean bool) {
        this.i = bool;
    }

    public final void e(String str) {
        this.j = str;
    }

    public final boolean f() {
        if (TextUtils.isEmpty(this.d)) {
            return false;
        }
        Integer num = this.g;
        Integer num2 = this.f;
        if (num == null || num2 == null || !a(num.intValue()) || !a(num2.intValue()) || TextUtils.isEmpty(this.f4172a)) {
            return false;
        }
        return true;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("MediaFile [url=");
        sb.append(this.f4172a);
        sb.append(", id=");
        sb.append(this.b);
        sb.append(", delivery=");
        sb.append(this.c);
        sb.append(", type=");
        sb.append(this.d);
        sb.append(", bitrate=");
        sb.append(this.e);
        sb.append(", width=");
        sb.append(this.f);
        sb.append(", height=");
        sb.append(this.g);
        sb.append(", scalable=");
        sb.append(this.h);
        sb.append(", maintainAspectRatio=");
        sb.append(this.i);
        sb.append(", apiFramework=");
        sb.append(this.j);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
