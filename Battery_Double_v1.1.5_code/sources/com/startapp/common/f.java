package com.startapp.common;

import android.os.Build.VERSION;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: StartAppSDK */
public final class f {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f4196a = "f";
    private static final int b = (VERSION.SDK_INT < 22 ? 10 : 20);
    private static final int c = (VERSION.SDK_INT < 22 ? 4 : 8);
    private static final ThreadFactory d = new ThreadFactory() {

        /* renamed from: a reason: collision with root package name */
        private final AtomicInteger f4197a = new AtomicInteger(1);

        public final Thread newThread(Runnable runnable) {
            StringBuilder sb = new StringBuilder("highPriorityThreadFactory #");
            sb.append(this.f4197a.getAndIncrement());
            return new Thread(runnable, sb.toString());
        }
    };
    private static final ThreadFactory e = new ThreadFactory() {

        /* renamed from: a reason: collision with root package name */
        private final AtomicInteger f4198a = new AtomicInteger(1);

        public final Thread newThread(Runnable runnable) {
            StringBuilder sb = new StringBuilder("defaultPriorityThreadFactory #");
            sb.append(this.f4198a.getAndIncrement());
            return new Thread(runnable, sb.toString());
        }
    };
    private static final RejectedExecutionHandler f = new RejectedExecutionHandler() {
        public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            f.f4196a;
            new StringBuilder("ThreadPoolExecutor rejected execution! ").append(threadPoolExecutor);
        }
    };
    private static final Executor g;
    private static final Executor h;
    private static final ScheduledExecutorService i = new ScheduledThreadPoolExecutor(1);

    /* compiled from: StartAppSDK */
    public enum a {
        DEFAULT,
        HIGH
    }

    static {
        int i2 = b;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(i2, i2, 20, TimeUnit.SECONDS, new LinkedBlockingQueue(), d, f);
        g = threadPoolExecutor;
        int i3 = c;
        ThreadPoolExecutor threadPoolExecutor2 = new ThreadPoolExecutor(i3, i3, 20, TimeUnit.SECONDS, new LinkedBlockingQueue(), e, f);
        h = threadPoolExecutor2;
    }

    public static ScheduledFuture<?> a(Runnable runnable, long j) {
        return i.schedule(runnable, j, TimeUnit.MILLISECONDS);
    }

    public static void a(a aVar, Runnable runnable) {
        Executor executor;
        try {
            if (aVar.equals(a.HIGH)) {
                executor = g;
            } else {
                executor = h;
            }
            try {
                executor.execute(runnable);
            } catch (Exception unused) {
                new StringBuilder("executeWithPriority failed to execute! ").append(executor);
            }
        } catch (Exception unused2) {
            executor = null;
            new StringBuilder("executeWithPriority failed to execute! ").append(executor);
        }
    }
}
