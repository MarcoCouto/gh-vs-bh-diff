package com.startapp.a.a.c;

import com.google.android.exoplayer2.C;
import java.nio.charset.Charset;

/* compiled from: StartAppSDK */
public final class c {

    /* renamed from: a reason: collision with root package name */
    public static final Charset f3849a = Charset.forName("UTF-8");

    static {
        Charset.forName("ISO-8859-1");
        Charset.forName(C.ASCII_NAME);
        Charset.forName("UTF-16");
        Charset.forName("UTF-16BE");
        Charset.forName("UTF-16LE");
    }
}
