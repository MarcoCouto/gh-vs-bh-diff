package com.startapp.a.a.c;

import java.io.Serializable;
import java.io.Writer;

/* compiled from: StartAppSDK */
public final class e extends Writer implements Serializable {
    private final StringBuilder b;

    public final void close() {
    }

    public final void flush() {
    }

    public e() {
        this.b = new StringBuilder();
    }

    public e(byte b2) {
        this.b = new StringBuilder(4);
    }

    public final Writer append(char c) {
        this.b.append(c);
        return this;
    }

    public final Writer append(CharSequence charSequence) {
        this.b.append(charSequence);
        return this;
    }

    public final Writer append(CharSequence charSequence, int i, int i2) {
        this.b.append(charSequence, i, i2);
        return this;
    }

    public final void write(String str) {
        if (str != null) {
            this.b.append(str);
        }
    }

    public final void write(char[] cArr, int i, int i2) {
        if (cArr != null) {
            this.b.append(cArr, i, i2);
        }
    }

    public final String toString() {
        return this.b.toString();
    }
}
