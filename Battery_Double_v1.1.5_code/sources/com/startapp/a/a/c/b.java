package com.startapp.a.a.c;

import java.util.Arrays;

/* compiled from: StartAppSDK */
public abstract class b {

    /* renamed from: a reason: collision with root package name */
    protected final int f3847a = 0;
    private byte b = 61;
    private final int c = 3;
    private final int d = 4;
    private final int e;

    /* compiled from: StartAppSDK */
    static class a {

        /* renamed from: a reason: collision with root package name */
        int f3848a;
        byte[] b;
        int c;
        int d;
        boolean e;
        int f;
        int g;

        a() {
        }

        public final String toString() {
            return String.format("%s[buffer=%s, currentLinePos=%s, eof=%s, ibitWorkArea=%s, lbitWorkArea=%s, modulus=%s, pos=%s, readPos=%s]", new Object[]{getClass().getSimpleName(), Arrays.toString(this.b), Integer.valueOf(this.f), Boolean.valueOf(this.e), Integer.valueOf(this.f3848a), Long.valueOf(0), Integer.valueOf(this.g), Integer.valueOf(this.c), Integer.valueOf(this.d)});
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(byte[] bArr, int i, int i2, a aVar);

    /* access modifiers changed from: protected */
    public abstract boolean a(byte b2);

    protected b(int i) {
        this.e = i;
    }

    protected static byte[] a(int i, a aVar) {
        if (aVar.b != null && aVar.b.length >= aVar.c + 4) {
            return aVar.b;
        }
        if (aVar.b == null) {
            aVar.b = new byte[8192];
            aVar.c = 0;
            aVar.d = 0;
        } else {
            byte[] bArr = new byte[(aVar.b.length << 1)];
            System.arraycopy(aVar.b, 0, bArr, 0, aVar.b.length);
            aVar.b = bArr;
        }
        return aVar.b;
    }

    /* access modifiers changed from: protected */
    public final boolean b(byte[] bArr) {
        if (bArr == null) {
            return false;
        }
        for (byte b2 : bArr) {
            if (61 == b2 || a(b2)) {
                return true;
            }
        }
        return false;
    }
}
