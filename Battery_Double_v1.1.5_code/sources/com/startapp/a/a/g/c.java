package com.startapp.a.a.g;

import com.startapp.a.a.a.a;
import java.util.HashMap;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class c {

    /* renamed from: a reason: collision with root package name */
    private final Map<a, b> f3856a = new HashMap();

    public c() {
        this.f3856a.put(a.ZERO, new g());
        this.f3856a.put(a.THREE, new f());
        this.f3856a.put(a.FOUR, new e());
        this.f3856a.put(a.FIVE, new d());
    }

    public final a a(a aVar) {
        return ((b) this.f3856a.get(aVar)).b();
    }

    public final com.startapp.a.a.d.a b(a aVar) {
        return ((b) this.f3856a.get(aVar)).a();
    }
}
