package com.startapp.a.a.g;

import com.startapp.a.a.d.a;
import com.startapp.a.a.e.d;

/* compiled from: StartAppSDK */
abstract class b {

    /* renamed from: a reason: collision with root package name */
    private final a f3855a;
    private final a b;
    private final d c;
    private final com.startapp.a.a.a.a d;

    protected b(a aVar, a aVar2, d dVar, com.startapp.a.a.a.a aVar3) {
        this.f3855a = aVar;
        this.b = aVar2;
        this.c = dVar;
        this.d = aVar3;
    }

    /* access modifiers changed from: 0000 */
    public final a a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final com.startapp.a.a.a.a b() {
        return this.d;
    }
}
