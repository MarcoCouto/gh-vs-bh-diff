package com.startapp.a.a.g;

import com.mansoon.BatteryDouble.Config;

/* compiled from: StartAppSDK */
public enum a {
    ZERO("0", 1, 720),
    THREE {
    },
    FOUR(Config.DATA_HISTORY_DEFAULT, 3, 3500),
    FIVE("5", 3, 1000000);
    
    private final String e;
    private final int f;
    private final int g;

    private a(String str, int i, int i2) {
        this.e = str;
        this.f = i;
        this.g = i2;
    }

    public final String a() {
        return this.e;
    }

    public final int b() {
        return this.f;
    }

    public final int c() {
        return this.g;
    }
}
