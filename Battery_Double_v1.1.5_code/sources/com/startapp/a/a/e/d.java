package com.startapp.a.a.e;

import com.startapp.a.a.a.b;
import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.PrintStream;

/* compiled from: StartAppSDK */
public abstract class d {

    /* renamed from: a reason: collision with root package name */
    private final c f3853a = new c();

    /* access modifiers changed from: protected */
    public abstract b a(DataInput dataInput);

    public final b a(String str) {
        try {
            byte[] a2 = c.a(str);
            if (a2 == null) {
                return null;
            }
            return a(a(a2));
        } catch (Exception e) {
            if (e.getMessage() != null && e.getMessage().contains("HighPageCountException")) {
                PrintStream printStream = System.err;
                StringBuilder sb = new StringBuilder("HighPageCountException (PLM-2573) ");
                sb.append(e.getMessage());
                sb.append(", bad bloom token: ");
                sb.append(str);
                printStream.println(sb.toString());
            }
            return null;
        }
    }

    protected static void a(DataInput dataInput, b bVar, long j) {
        int c = bVar.c();
        long j2 = j;
        for (int i = 0; i < c; i++) {
            long[] a2 = bVar.a(i);
            long j3 = j2;
            int i2 = 0;
            while (true) {
                if (i2 >= 4096) {
                    j2 = j3;
                    break;
                }
                long j4 = j3 - 1;
                if (j3 <= 0) {
                    j2 = j4;
                    break;
                }
                a2[i2] = dataInput.readLong();
                i2++;
                j3 = j4;
            }
        }
    }

    /* access modifiers changed from: protected */
    public DataInput a(byte[] bArr) {
        return new DataInputStream(new ByteArrayInputStream(bArr));
    }
}
