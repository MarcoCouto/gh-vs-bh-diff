package com.startapp.a.a.e;

import com.startapp.a.a.a.b;
import java.io.DataInput;

/* compiled from: StartAppSDK */
public final class a extends d {

    /* renamed from: a reason: collision with root package name */
    private final int f3850a;
    private final int b;

    public a(int i, int i2) {
        this.f3850a = i;
        this.b = i2;
    }

    /* access modifiers changed from: protected */
    public final b a(DataInput dataInput) {
        b bVar = new b((long) (this.f3850a * this.b));
        a(dataInput, bVar, (long) bVar.b());
        return bVar;
    }
}
