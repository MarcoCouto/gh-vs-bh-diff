package com.startapp.a.a.e;

import com.startapp.a.a.a.b;
import java.io.DataInput;
import java.io.IOException;

/* compiled from: StartAppSDK */
public final class e extends d {
    /* access modifiers changed from: protected */
    public final DataInput a(byte[] bArr) {
        DataInput a2 = super.a(bArr);
        try {
            a2.readInt();
            return a2;
        } catch (IOException e) {
            throw new RuntimeException("problem incrementInputStreamForBackwordCompatability", e);
        }
    }

    /* access modifiers changed from: protected */
    public final b a(DataInput dataInput) {
        long readInt = (long) dataInput.readInt();
        b bVar = new b(readInt << 6);
        a(dataInput, bVar, readInt);
        return bVar;
    }
}
