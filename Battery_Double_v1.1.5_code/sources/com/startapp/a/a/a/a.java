package com.startapp.a.a.a;

import a.a.d.b.b;
import a.a.d.b.g;
import a.a.e;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.webkit.WebView;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.startapp.a.a.c.c;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayOutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private final int f3844a;
    private final int b;

    private static int b(int i, int i2) {
        return 0;
    }

    public a(int i, int i2) {
        this.f3844a = i;
        this.b = i2;
    }

    public final b a(List<String> list) {
        b bVar = new b((long) (this.f3844a * this.b));
        for (String bytes : list) {
            ByteBuffer wrap = ByteBuffer.wrap(bytes.getBytes());
            long[] jArr = new long[this.f3844a];
            long a2 = bVar.a() / ((long) this.f3844a);
            long a3 = a(wrap, wrap.position(), wrap.remaining(), 0);
            long a4 = a(wrap, wrap.position(), wrap.remaining(), a3);
            for (int i = 0; i < this.f3844a; i++) {
                long j = (long) i;
                jArr[i] = (j * a2) + Math.abs(((j * a4) + a3) % a2);
            }
            for (long a5 : jArr) {
                bVar.a(a5);
            }
        }
        return bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x00d1, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 3)) << 24);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x00dd, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 2)) << 16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00e9, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 1)) << 8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00f5, code lost:
        r5 = -4132994306676758123L;
        r3 = (((long) r0.get((r20 + r2) - r5)) ^ r3) * -4132994306676758123L;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0105, code lost:
        r1 = ((r3 >>> 47) ^ r3) * r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x010f, code lost:
        return r1 ^ (r1 >>> 47);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x00b5, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 5)) << 40);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00c3, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 4)) << 32);
     */
    private static long a(ByteBuffer byteBuffer, int i, int i2, long j) {
        ByteBuffer byteBuffer2 = byteBuffer;
        int i3 = i2;
        long j2 = (j & 4294967295L) ^ (((long) i3) * -4132994306676758123L);
        for (int i4 = 0; i4 < (i3 >> 3); i4++) {
            int i5 = i + (i4 << 3);
            long j3 = ((((long) byteBuffer2.get(i5)) & 255) + ((((long) byteBuffer2.get(i5 + 1)) & 255) << 8) + ((((long) byteBuffer2.get(i5 + 2)) & 255) << 16) + ((((long) byteBuffer2.get(i5 + 3)) & 255) << 24) + ((((long) byteBuffer2.get(i5 + 4)) & 255) << 32) + ((((long) byteBuffer2.get(i5 + 5)) & 255) << 40) + ((((long) byteBuffer2.get(i5 + 6)) & 255) << 48) + ((((long) byteBuffer2.get(i5 + 7)) & 255) << 56)) * -4132994306676758123L;
            j2 = (j2 ^ ((j3 ^ (j3 >>> 47)) * -4132994306676758123L)) * -4132994306676758123L;
        }
        int i6 = i3 & 7;
        switch (i6) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                j2 ^= ((long) byteBuffer2.get(((i + i3) - i6) + 6)) << 48;
                break;
            default:
                long j4 = -4132994306676758123L;
                break;
        }
    }

    public static String a(byte[] bArr) {
        Charset charset = c.f3849a;
        if (bArr == null) {
            return null;
        }
        return new String(bArr, charset);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x005f  */
    public static boolean a(ByteArrayOutputStream byteArrayOutputStream, String str) {
        HttpURLConnection httpURLConnection;
        g.b(byteArrayOutputStream, "$this$uploadTo");
        g.b(str, "url");
        boolean z = false;
        try {
            URLConnection openConnection = new URL(str).openConnection();
            if (openConnection != null) {
                httpURLConnection = (HttpURLConnection) openConnection;
                try {
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_PUT);
                    httpURLConnection.setRequestProperty("Content-Type", "image/jpeg");
                    byteArrayOutputStream.writeTo(httpURLConnection.getOutputStream());
                    httpURLConnection.getOutputStream().flush();
                    int responseCode = httpURLConnection.getResponseCode();
                    if (200 <= responseCode && 299 >= responseCode) {
                        z = true;
                    }
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    return z;
                } catch (Exception unused) {
                    if (httpURLConnection != null) {
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    if (httpURLConnection != null) {
                    }
                    throw th;
                }
            } else {
                throw new a.a.g("null cannot be cast to non-null type java.net.HttpURLConnection");
            }
        } catch (Exception unused2) {
            httpURLConnection = null;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return false;
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    public static boolean a(Context context, String str) {
        g.b(context, "$this$isPermissionGranted");
        g.b(str, ParametersKeys.PERMISSION);
        return com.startapp.common.a.c.a(context, str);
    }

    @NotNull
    public static Bitmap a(WebView webView) {
        g.b(webView, "$this$takeScreenshot");
        if (VERSION.SDK_INT >= 21) {
            WebView.enableSlowWholeDocumentDraw();
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Context context = webView.getContext();
        g.a((Object) context, "context");
        g.b(context, "$this$windowManager");
        Object systemService = context.getSystemService("window");
        if (systemService != null) {
            ((WindowManager) systemService).getDefaultDisplay().getMetrics(displayMetrics);
            int i = displayMetrics.widthPixels;
            int i2 = displayMetrics.heightPixels;
            webView.measure(i, i2);
            webView.layout(0, 0, i, i2);
            webView.setDrawingCacheEnabled(true);
            webView.buildDrawingCache(true);
            Thread.sleep(500);
            Bitmap createBitmap = Bitmap.createBitmap(webView.getDrawingCache());
            webView.setDrawingCacheEnabled(false);
            g.a((Object) createBitmap, "bitmap");
            return createBitmap;
        }
        throw new a.a.g("null cannot be cast to non-null type android.view.WindowManager");
    }

    @NotNull
    public static <A, B> e<A, B> a(A a2, B b2) {
        return new e<>(a2, b2);
    }

    public static int a(int i, int i2) {
        return i >= i2 ? i2 : i2 - b(b(i2, 1) - b(i, 1), 1);
    }

    @NotNull
    public static String a(Reader reader) {
        g.b(reader, "$this$readText");
        StringWriter stringWriter = new StringWriter();
        a(reader, (Writer) stringWriter);
        String stringWriter2 = stringWriter.toString();
        g.a((Object) stringWriter2, "buffer.toString()");
        return stringWriter2;
    }

    private static long a(Reader reader, Writer writer) {
        g.b(reader, "$this$copyTo");
        g.b(writer, "out");
        char[] cArr = new char[8192];
        int read = reader.read(cArr);
        long j = 0;
        while (read >= 0) {
            writer.write(cArr, 0, read);
            j += (long) read;
            read = reader.read(cArr);
        }
        return j;
    }

    @NotNull
    public static <T> Class<T> a(a.a.d.b.e<T> eVar) {
        g.b(eVar, "$this$javaObjectType");
        Class a2 = ((b) eVar).a();
        if (a2.isPrimitive()) {
            String name = a2.getName();
            if (name != null) {
                switch (name.hashCode()) {
                    case -1325958191:
                        if (name.equals("double")) {
                            a2 = Double.class;
                            break;
                        }
                        break;
                    case 104431:
                        if (name.equals("int")) {
                            a2 = Integer.class;
                            break;
                        }
                        break;
                    case 3039496:
                        if (name.equals("byte")) {
                            a2 = Byte.class;
                            break;
                        }
                        break;
                    case 3052374:
                        if (name.equals("char")) {
                            a2 = Character.class;
                            break;
                        }
                        break;
                    case 3327612:
                        if (name.equals("long")) {
                            a2 = Long.class;
                            break;
                        }
                        break;
                    case 3625364:
                        if (name.equals("void")) {
                            a2 = Void.class;
                            break;
                        }
                        break;
                    case 64711720:
                        if (name.equals("boolean")) {
                            a2 = Boolean.class;
                            break;
                        }
                        break;
                    case 97526364:
                        if (name.equals("float")) {
                            a2 = Float.class;
                            break;
                        }
                        break;
                    case 109413500:
                        if (name.equals("short")) {
                            a2 = Short.class;
                            break;
                        }
                        break;
                }
            }
            if (a2 != null) {
                return a2;
            }
            throw new a.a.g("null cannot be cast to non-null type java.lang.Class<T>");
        } else if (a2 != null) {
            return a2;
        } else {
            throw new a.a.g("null cannot be cast to non-null type java.lang.Class<T>");
        }
    }
}
