package com.startapp.a.a.a;

import java.io.Serializable;

/* compiled from: StartAppSDK */
public class b implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private static /* synthetic */ boolean f3845a = (!b.class.desiredAssertionStatus());
    private final long[][] d;
    private int e;
    private final int f;

    private static int b(long j) {
        return (int) (((j - 1) >>> 6) + 1);
    }

    public b(long j) {
        this.e = b(j);
        int i = this.e % 4096;
        int i2 = this.e / 4096;
        this.f = (i == 0 ? 0 : 1) + i2;
        if (this.f <= 100) {
            this.d = new long[this.f][];
            for (int i3 = 0; i3 < i2; i3++) {
                this.d[i3] = new long[4096];
            }
            if (i != 0) {
                this.d[this.d.length - 1] = new long[i];
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder("HighPageCountException pageCount = ");
        sb.append(this.f);
        throw new RuntimeException(sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public final long a() {
        return ((long) this.e) << 6;
    }

    public final int b() {
        return this.e;
    }

    public final int c() {
        return this.f;
    }

    public final long[] a(int i) {
        return this.d[i];
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j) {
        int i = (int) (j >> 6);
        if (i >= this.e) {
            int b = b(j + 1);
            if (f3845a || b <= this.e) {
                this.e = i + 1;
            } else {
                throw new AssertionError("Growing of paged bitset is not supported");
            }
        }
        long j2 = 1 << (((int) j) & 63);
        long[] jArr = this.d[i / 4096];
        int i2 = i % 4096;
        jArr[i2] = j2 | jArr[i2];
    }
}
