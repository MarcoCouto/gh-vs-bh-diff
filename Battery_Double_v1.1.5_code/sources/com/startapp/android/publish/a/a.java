package com.startapp.android.publish.a;

import android.content.Context;
import android.content.Intent;
import com.b.a.a.a.b;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.AdsConstants.AdApiType;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.e;
import com.startapp.android.publish.adsCommon.h;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.common.model.GetAdRequest;
import com.startapp.android.publish.common.model.GetAdResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public abstract class a extends e {
    private int g = 0;
    private Set<String> h = new HashSet();

    /* access modifiers changed from: protected */
    public abstract void a(Ad ad);

    public a(Context context, Ad ad, AdPreferences adPreferences, AdEventListener adEventListener, Placement placement) {
        super(context, ad, adPreferences, adEventListener, placement);
    }

    /* access modifiers changed from: protected */
    public final Object e() {
        GetAdRequest a2 = a();
        if (a2 == null) {
            return null;
        }
        if (this.h.size() == 0) {
            this.h.add(this.f4063a.getPackageName());
        }
        boolean z = false;
        if (this.g > 0) {
            a2.setEngInclude(false);
        }
        a2.setPackageExclude(this.h);
        if (this.g == 0) {
            z = true;
        }
        a2.setEngInclude(z);
        try {
            return (GetAdResponse) com.startapp.android.publish.adsCommon.l.a.a(this.f4063a, AdsConstants.a(AdApiType.JSON, f()), a2, GetAdResponse.class);
        } catch (com.startapp.common.e e) {
            this.f = e.getMessage();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Object obj) {
        GetAdResponse getAdResponse = (GetAdResponse) obj;
        boolean z = false;
        if (obj == null) {
            this.f = "Empty Response";
            return false;
        } else if (!getAdResponse.isValidResponse()) {
            this.f = getAdResponse.getErrorMessage();
            StringBuilder sb = new StringBuilder("Error msg = [");
            sb.append(this.f);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            return false;
        } else {
            h hVar = (h) this.b;
            List a2 = b.a(this.f4063a, getAdResponse.getAdsDetails(), this.g, this.h);
            hVar.a(a2);
            hVar.setAdInfoOverride(getAdResponse.getAdInfoOverride());
            if (getAdResponse.getAdsDetails() != null && getAdResponse.getAdsDetails().size() > 0) {
                z = true;
            }
            if (!z) {
                this.f = "Empty Response";
            } else if (a2.size() == 0 && this.g == 0) {
                this.g++;
                return d().booleanValue();
            }
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        Intent intent = new Intent("com.startapp.android.OnReceiveResponseBroadcastListener");
        intent.putExtra("adHashcode", this.b.hashCode());
        intent.putExtra("adResult", bool);
        com.startapp.common.b.a(this.f4063a).a(intent);
        if (bool.booleanValue()) {
            a((Ad) (h) this.b);
            this.d.onReceiveAd(this.b);
        }
    }
}
