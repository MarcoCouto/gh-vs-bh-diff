package com.startapp.android.publish.cache;

import com.startapp.android.publish.adsCommon.m;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class b extends e {
    private final FailuresHandler b = d.a().b().getFailuresHandler();
    private int c = 0;
    private boolean d = false;

    /* access modifiers changed from: protected */
    public final String e() {
        return "CacheErrorReloadTimer";
    }

    public b(g gVar) {
        super(gVar);
    }

    public final void a() {
        super.a();
        this.c = 0;
        this.d = false;
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        if (!m.a().j()) {
            return false;
        }
        if (!((this.b == null || this.b.getIntervals() == null) ? false : true)) {
            return false;
        }
        if (this.d) {
            return this.b.isInfiniteLastRetry();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final long d() {
        if (this.c >= this.b.getIntervals().size()) {
            return -1;
        }
        Long i = i();
        if (i == null) {
            return -1;
        }
        long millis = TimeUnit.SECONDS.toMillis((long) ((Integer) this.b.getIntervals().get(this.c)).intValue()) - (System.currentTimeMillis() - i.longValue());
        if (millis >= 0) {
            return millis;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.c == this.b.getIntervals().size() - 1) {
            this.d = true;
            new StringBuilder("Reached end index: ").append(this.c);
        } else {
            this.c++;
            new StringBuilder("Advanced to index: ").append(this.c);
        }
        super.b();
    }
}
