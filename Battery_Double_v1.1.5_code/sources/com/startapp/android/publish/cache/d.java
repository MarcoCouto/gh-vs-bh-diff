package com.startapp.android.publish.cache;

import android.content.Context;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.common.c.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class d implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private static volatile d f4125a = new d();
    private static final long serialVersionUID = 1;
    @e(a = true)
    private ACMConfig ACM = new ACMConfig();
    private String cacheMetaDataUpdateVersion = AdsConstants.c;
    private float sendCacheSizeProb = 20.0f;

    public static d a() {
        return f4125a;
    }

    public final ACMConfig b() {
        return this.ACM;
    }

    public static void a(Context context, d dVar) {
        dVar.cacheMetaDataUpdateVersion = AdsConstants.c;
        f4125a = dVar;
        com.startapp.common.a.e.a(context, "StartappCacheMetadata", (Serializable) dVar);
    }

    public final float c() {
        return this.sendCacheSizeProb;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        d dVar = (d) obj;
        return Float.compare(dVar.sendCacheSizeProb, this.sendCacheSizeProb) == 0 && j.b(this.ACM, dVar.ACM) && j.b(this.cacheMetaDataUpdateVersion, dVar.cacheMetaDataUpdateVersion);
    }

    public int hashCode() {
        return j.a(this.ACM, Float.valueOf(this.sendCacheSizeProb), this.cacheMetaDataUpdateVersion);
    }

    public static void a(Context context) {
        d dVar = (d) com.startapp.common.a.e.a(context, "StartappCacheMetadata");
        d dVar2 = new d();
        if (dVar != null) {
            boolean a2 = j.a(dVar, dVar2);
            if (!(!AdsConstants.c.equals(dVar.cacheMetaDataUpdateVersion)) && a2) {
                f.a(context, com.startapp.android.publish.adsCommon.g.d.METADATA_NULL, "CacheMetaData", "", "");
            }
            f4125a = dVar;
            return;
        }
        f4125a = dVar2;
    }
}
