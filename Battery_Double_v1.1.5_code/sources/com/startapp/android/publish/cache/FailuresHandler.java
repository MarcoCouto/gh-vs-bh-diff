package com.startapp.android.publish.cache;

import com.explorestack.iab.vast.VastError;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.common.c.e;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: StartAppSDK */
public class FailuresHandler implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean infiniteLastRetry = true;
    @e(b = ArrayList.class, c = Integer.class)
    private List<Integer> intervals = Arrays.asList(new Integer[]{Integer.valueOf(10), Integer.valueOf(30), Integer.valueOf(60), Integer.valueOf(VastError.ERROR_CODE_GENERAL_WRAPPER)});

    public List<Integer> getIntervals() {
        return this.intervals;
    }

    public boolean isInfiniteLastRetry() {
        return this.infiniteLastRetry;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        FailuresHandler failuresHandler = (FailuresHandler) obj;
        return this.infiniteLastRetry == failuresHandler.infiniteLastRetry && j.b(this.intervals, failuresHandler.intervals);
    }

    public int hashCode() {
        return j.a(this.intervals, Boolean.valueOf(this.infiniteLastRetry));
    }
}
