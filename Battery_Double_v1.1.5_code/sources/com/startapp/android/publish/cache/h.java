package com.startapp.android.publish.cache;

import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class h implements Serializable {
    private static final long serialVersionUID = 1;
    private String filename;
    private long lastUseTimestamp;
    private String videoLocation;

    public h(String str) {
        this.filename = str;
    }

    public final String a() {
        return this.filename;
    }

    public final String b() {
        return this.videoLocation;
    }

    public final void a(String str) {
        this.videoLocation = str;
    }

    public final void a(long j) {
        this.lastUseTimestamp = j;
    }

    public final int hashCode() {
        int i;
        if (this.filename == null) {
            i = 0;
        } else {
            i = this.filename.hashCode();
        }
        return i + 31;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        h hVar = (h) obj;
        if (this.filename == null) {
            if (hVar.filename != null) {
                return false;
            }
        } else if (!this.filename.equals(hVar.filename)) {
            return false;
        }
        return true;
    }
}
