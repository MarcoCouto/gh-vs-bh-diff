package com.startapp.android.publish.cache;

import com.startapp.android.publish.adsCommon.g;
import com.startapp.android.publish.adsCommon.m;

/* compiled from: StartAppSDK */
public final class f extends e {
    /* access modifiers changed from: protected */
    public final String e() {
        return "CacheTTLReloadTimer";
    }

    public f(g gVar) {
        super(gVar);
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return m.a().a(this.f4126a.c());
    }

    /* access modifiers changed from: protected */
    public final long d() {
        g b = this.f4126a.b();
        if (b == null) {
            return -1;
        }
        Long adCacheTtl = b.getAdCacheTtl();
        Long lastLoadTime = b.getLastLoadTime();
        if (adCacheTtl == null || lastLoadTime == null) {
            return -1;
        }
        long longValue = adCacheTtl.longValue() - (System.currentTimeMillis() - lastLoadTime.longValue());
        if (longValue >= 0) {
            return longValue;
        }
        return 0;
    }
}
