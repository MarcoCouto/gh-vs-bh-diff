package com.startapp.android.publish.cache;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.android.publish.adsCommon.Ad.AdType;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppAd.AdMode;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.adsCommon.g;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.adsCommon.m;
import com.startapp.android.publish.cache.g.b;
import com.startapp.android.publish.cache.i.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: StartAppSDK */
public final class a {
    private static a c = new a();

    /* renamed from: a reason: collision with root package name */
    final Map<c, g> f4117a = new ConcurrentHashMap();
    protected boolean b = false;
    private Map<String, String> d = new WeakHashMap();
    private boolean e = false;
    private Queue<C0091a> f = new ConcurrentLinkedQueue();
    private b g;
    /* access modifiers changed from: private */
    public Context h;

    /* renamed from: com.startapp.android.publish.cache.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    class C0091a {

        /* renamed from: a reason: collision with root package name */
        StartAppAd f4124a;
        Placement b;
        AdPreferences c;
        AdEventListener d;

        C0091a(StartAppAd startAppAd, Placement placement, AdPreferences adPreferences, AdEventListener adEventListener) {
            this.f4124a = startAppAd;
            this.b = placement;
            this.c = adPreferences;
            this.d = adEventListener;
        }
    }

    private a() {
    }

    public static a a() {
        return c;
    }

    public final c a(Context context, StartAppAd startAppAd, AdPreferences adPreferences, AdEventListener adEventListener) {
        if (!c(Placement.INAPP_SPLASH)) {
            return null;
        }
        return a(context, startAppAd, Placement.INAPP_SPLASH, adPreferences, adEventListener);
    }

    public final c a(Context context, AdPreferences adPreferences) {
        if (!c(Placement.INAPP_RETURN)) {
            return null;
        }
        return a(context, (StartAppAd) null, Placement.INAPP_RETURN, adPreferences, (AdEventListener) null);
    }

    public final c a(Context context, StartAppAd startAppAd, AdMode adMode, AdPreferences adPreferences, AdEventListener adEventListener) {
        Placement placement;
        if (adPreferences == null) {
            adPreferences = new AdPreferences();
        }
        AdPreferences adPreferences2 = adPreferences;
        boolean z = false;
        switch (adMode) {
            case OFFERWALL:
                if (j.a(128) || j.a(64)) {
                    z = true;
                }
                if (!z) {
                    placement = Placement.INAPP_FULL_SCREEN;
                    break;
                } else {
                    placement = Placement.INAPP_OFFER_WALL;
                    break;
                }
            case OVERLAY:
            case FULLPAGE:
            case VIDEO:
            case REWARDED_VIDEO:
                placement = Placement.INAPP_OVERLAY;
                break;
            case AUTOMATIC:
                if (j.a(128) || j.a(64)) {
                    z = true;
                }
                boolean a2 = j.a(4);
                boolean a3 = j.a(2);
                if (!a2 || !a3 || !z) {
                    if (!a2 && !a3) {
                        if (z) {
                            placement = Placement.INAPP_OFFER_WALL;
                            break;
                        }
                    } else {
                        placement = Placement.INAPP_OVERLAY;
                        break;
                    }
                } else {
                    if (new Random().nextInt(100) >= com.startapp.android.publish.adsCommon.b.a().b()) {
                        placement = Placement.INAPP_FULL_SCREEN;
                        break;
                    } else {
                        if ((new Random().nextInt(100) >= com.startapp.android.publish.adsCommon.b.a().c() && !j.a(adPreferences2, "forceFullpage")) || j.a(adPreferences2, "forceOverlay")) {
                            placement = Placement.INAPP_OVERLAY;
                            break;
                        }
                    }
                }
            default:
                placement = Placement.INAPP_FULL_SCREEN;
                break;
        }
        Placement placement2 = placement;
        if (adMode.equals(AdMode.REWARDED_VIDEO)) {
            c.a(adPreferences2, "type", AdType.REWARDED_VIDEO);
        }
        if (adMode.equals(AdMode.VIDEO)) {
            c.a(adPreferences2, "type", AdType.VIDEO);
        }
        return a(context, startAppAd, placement2, adPreferences2, adEventListener, false, 0);
    }

    public final void a(final Context context) {
        this.h = context.getApplicationContext();
        if (e()) {
            this.e = true;
            f.a(com.startapp.common.f.a.HIGH, (Runnable) new Runnable(context, new i.c() {
                public final void a(List<b> list) {
                    if (list != null) {
                        try {
                            for (b bVar : list) {
                                if (a.c(bVar.placement)) {
                                    new StringBuilder("Loading from disk: ").append(bVar.placement);
                                    a.this.a(context, null, bVar.a(), bVar.b(), null, true, bVar.c());
                                }
                            }
                        } catch (Exception unused) {
                        }
                    }
                    a.this.d(context);
                }
            }) {

                /* renamed from: a reason: collision with root package name */
                final /* synthetic */ c f4135a;
                private /* synthetic */ Context b;

                {
                    this.b = r1;
                    this.f4135a = r2;
                }

                public final void run() {
                    try {
                        Class<b> cls = b.class;
                        final List b2 = com.startapp.common.a.e.b(this.b, i.a());
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public final void run() {
                                AnonymousClass2.this.f4135a.a(b2);
                            }
                        });
                    } catch (Exception e) {
                        com.startapp.android.publish.adsCommon.g.f.a(this.b, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, " DiskAdCacheManager.loadCacheKeysAsync - Unexpected Thread Exception", e.getMessage(), "");
                    }
                }
            });
        }
    }

    public final void b() {
        if (!this.e) {
            synchronized (this.f4117a) {
                for (g e2 : this.f4117a.values()) {
                    e2.e();
                }
            }
        }
    }

    public final void b(Context context) {
        this.b = true;
        f.a(com.startapp.common.f.a.DEFAULT, (Runnable) new Runnable(context, new e() {
            public final void a() {
                a.this.b = false;
            }
        }) {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ e f4133a;
            private /* synthetic */ Context b;

            {
                this.b = r1;
                this.f4133a = r2;
            }

            public final void run() {
                try {
                    com.startapp.common.a.e.c(this.b, "startapp_ads");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            AnonymousClass1.this.f4133a.a();
                        }
                    });
                } catch (Exception e) {
                    com.startapp.android.publish.adsCommon.g.f.a(this.b, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, " DiskAdCacheManager.deleteDiskCacheAsync - Unexpected Thread Exception", e.getMessage(), "");
                }
            }
        });
    }

    public final void c(final Context context) {
        AnonymousClass3 r0 = new com.startapp.android.publish.common.metaData.f() {
            public final void a() {
            }

            public final void a(boolean z) {
                if (z) {
                    Set autoLoad = d.a().b().getAutoLoad();
                    if (autoLoad != null) {
                        for (AdMode adMode : a.this.a(autoLoad)) {
                            new StringBuilder("preCacheAds load ").append(adMode.name());
                            int b2 = com.startapp.android.publish.adsCommon.b.a().b();
                            if (adMode == AdMode.FULLPAGE) {
                                if (b2 > 0) {
                                    a.this.a(context, (StartAppAd) null, AdMode.FULLPAGE, new AdPreferences(), (AdEventListener) null);
                                }
                            } else if (adMode != AdMode.OFFERWALL) {
                                a.this.a(context, (StartAppAd) null, adMode, new AdPreferences(), (AdEventListener) null);
                            } else if (b2 < 100) {
                                a.this.a(context, (StartAppAd) null, AdMode.OFFERWALL, new AdPreferences(), (AdEventListener) null);
                            }
                            String a2 = a.a(adMode);
                            if (a2 != null) {
                                k.b(context, a2, Integer.valueOf(k.a(context, a2, Integer.valueOf(0)).intValue() + 1));
                            }
                        }
                    }
                }
            }
        };
        synchronized (com.startapp.android.publish.common.metaData.e.getLock()) {
            com.startapp.android.publish.common.metaData.e.getInstance().addMetaDataListener(r0);
        }
    }

    /* access modifiers changed from: protected */
    public final Set<AdMode> a(Set<AdMode> set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AdMode adMode = (AdMode) it.next();
            boolean z = false;
            if (k.a(this.h, a(adMode), Integer.valueOf(0)).intValue() >= com.startapp.android.publish.common.metaData.e.getInstance().getStopAutoLoadPreCacheAmount()) {
                z = true;
            }
            if (z) {
                new StringBuilder("preCacheAds.remove ").append(adMode.name());
                it.remove();
            }
        }
        if (!j.a(128) && !j.a(64)) {
            set.remove(AdMode.OFFERWALL);
        }
        if (!j.a(2) && !j.a(4)) {
            set.remove(AdMode.FULLPAGE);
        }
        if (!j.a(4)) {
            set.remove(AdMode.REWARDED_VIDEO);
            set.remove(AdMode.VIDEO);
        }
        return set;
    }

    private int d() {
        return this.f4117a.size();
    }

    public final g a(c cVar) {
        if (cVar == null) {
            return null;
        }
        new StringBuilder("Retrieving ad with ").append(cVar);
        g gVar = (g) this.f4117a.get(cVar);
        if (gVar != null) {
            return gVar.h();
        }
        return null;
    }

    public final g b(c cVar) {
        g gVar = cVar != null ? (g) this.f4117a.get(cVar) : null;
        if (gVar != null) {
            return gVar.b();
        }
        return null;
    }

    public final synchronized List<g> c() {
        return new ArrayList(this.f4117a.values());
    }

    public final String a(String str, String str2) {
        this.d.put(str2, str);
        return str2;
    }

    public final String a(String str) {
        return (String) this.d.get(str);
    }

    public final String b(String str) {
        StringBuilder sb = new StringBuilder("cache size: ");
        sb.append(this.d.size());
        sb.append(" - removing ");
        sb.append(str);
        return (String) this.d.remove(str);
    }

    private boolean e() {
        return !this.b && d.a().b().isLocalCache();
    }

    /* access modifiers changed from: protected */
    public final void d(Context context) {
        this.e = false;
        for (C0091a aVar : this.f) {
            if (c(aVar.b)) {
                new StringBuilder("Loading pending request for: ").append(aVar.b);
                a(context, aVar.f4124a, aVar.b, aVar.c, aVar.d);
            }
        }
        this.f.clear();
    }

    protected static String c(c cVar) {
        return String.valueOf(cVar.hashCode()).replace('-', '_');
    }

    public final c a(Context context, StartAppAd startAppAd, Placement placement, AdPreferences adPreferences, AdEventListener adEventListener) {
        return a(context, startAppAd, placement, adPreferences, adEventListener, false, 0);
    }

    /* access modifiers changed from: protected */
    public final c a(Context context, StartAppAd startAppAd, Placement placement, AdPreferences adPreferences, AdEventListener adEventListener, boolean z, int i) {
        g gVar;
        this.h = context.getApplicationContext();
        if (adPreferences == null) {
            adPreferences = new AdPreferences();
        }
        AdPreferences adPreferences2 = adPreferences;
        c cVar = new c(placement, adPreferences2);
        if (!this.e || z) {
            AdPreferences adPreferences3 = new AdPreferences(adPreferences2);
            synchronized (this.f4117a) {
                gVar = (g) this.f4117a.get(cVar);
                if (gVar == null) {
                    StringBuilder sb = new StringBuilder("CachedAd for ");
                    sb.append(placement);
                    sb.append(" not found. Adding new CachedAd with ");
                    sb.append(cVar);
                    if (AnonymousClass6.f4123a[placement.ordinal()] != 1) {
                        gVar = new g(context, placement, adPreferences3);
                    } else {
                        gVar = new g(context, placement, adPreferences3, 0);
                    }
                    if (this.g == null) {
                        this.g = new b() {
                            public final void a(g gVar) {
                                synchronized (a.this.f4117a) {
                                    c cVar = null;
                                    for (c cVar2 : a.this.f4117a.keySet()) {
                                        if (((g) a.this.f4117a.get(cVar2)) == gVar) {
                                            cVar = cVar2;
                                        }
                                    }
                                    if (cVar != null) {
                                        a.this.f4117a.remove(cVar);
                                        if (gVar.c() != Placement.INAPP_SPLASH) {
                                            com.startapp.android.publish.adsCommon.g.f.a(a.this.h, d.STOP_RELOAD_IN_CACHE, "", cVar.toString(), "");
                                        }
                                    }
                                }
                            }
                        };
                    }
                    gVar.a(this.g);
                    if (z) {
                        gVar.a(c(cVar));
                        gVar.d();
                        gVar.a(i);
                    }
                    a(cVar, gVar, context);
                } else {
                    StringBuilder sb2 = new StringBuilder("CachedAd for ");
                    sb2.append(placement);
                    sb2.append(" already exists.");
                    gVar.a(adPreferences3);
                }
            }
            gVar.a(startAppAd, adEventListener);
            return cVar;
        }
        new StringBuilder("Adding to pending queue: ").append(placement);
        Queue<C0091a> queue = this.f;
        C0091a aVar = new C0091a(startAppAd, placement, adPreferences2, adEventListener);
        queue.add(aVar);
        return cVar;
    }

    public final void a(Placement placement) {
        synchronized (this.f4117a) {
            Iterator it = this.f4117a.entrySet().iterator();
            while (it.hasNext()) {
                if (((c) ((Entry) it.next()).getKey()).a() == placement) {
                    it.remove();
                }
            }
        }
    }

    private void a(c cVar, g gVar, Context context) {
        synchronized (this.f4117a) {
            int maxCacheSize = d.a().b().getMaxCacheSize();
            if (maxCacheSize != 0 && d() >= maxCacheSize) {
                long j = Long.MAX_VALUE;
                Object obj = null;
                for (c cVar2 : this.f4117a.keySet()) {
                    g gVar2 = (g) this.f4117a.get(cVar2);
                    if (gVar2.c() == gVar.c() && gVar2.c < j) {
                        j = gVar2.c;
                        obj = cVar2;
                    }
                }
                if (obj != null) {
                    this.f4117a.remove(obj);
                }
            }
            this.f4117a.put(cVar, gVar);
            if (Math.random() * 100.0d < ((double) d.a().c())) {
                com.startapp.android.publish.adsCommon.g.f.a(context, new com.startapp.android.publish.adsCommon.g.e(d.GENERAL, "Cache Size", String.valueOf(d())), "");
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean c(Placement placement) {
        switch (placement) {
            case INAPP_SPLASH:
                return m.a().i() && !com.startapp.android.publish.adsCommon.b.a().z();
            case INAPP_RETURN:
                return m.a().h() && !com.startapp.android.publish.adsCommon.b.a().y();
            default:
                return true;
        }
    }

    public static String a(AdMode adMode) {
        if (adMode == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder("autoLoadNotShownAdPrefix");
        sb.append(adMode.name());
        return sb.toString();
    }

    public final void a(final Context context, boolean z) {
        if (e()) {
            f.a(com.startapp.common.f.a.DEFAULT, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        com.startapp.common.a.e.c(context, i.a());
                        com.startapp.common.a.e.c(context, i.b());
                        c cVar = null;
                        for (Entry entry : a.this.f4117a.entrySet()) {
                            c cVar2 = (c) entry.getKey();
                            if (cVar2.a() == Placement.INAPP_SPLASH) {
                                cVar = cVar2;
                            } else {
                                g gVar = (g) entry.getValue();
                                new StringBuilder("Saving to disk: ").append(cVar2.toString());
                                i.a(context, cVar2.a(), gVar.a(), a.c(cVar2), gVar.j());
                                i.a(context, gVar, a.c(cVar2));
                            }
                        }
                        if (cVar != null) {
                            a.this.f4117a.remove(cVar);
                        }
                    } catch (Exception e) {
                        com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "AdCacheManager.saveToDisk - Unexpected Thread Exception", e.getMessage(), "");
                    }
                }
            });
        }
        for (g gVar : this.f4117a.values()) {
            if (gVar.b() == null || !j.a(2) || !(gVar.b() instanceof com.startapp.android.publish.ads.b.e) || z || !d.a().b().shouldReturnAdLoadInBg()) {
                gVar.g();
            }
            gVar.f();
        }
    }
}
