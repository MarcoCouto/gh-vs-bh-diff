package com.startapp.android.publish.cache;

import android.os.Handler;
import android.os.Looper;

/* compiled from: StartAppSDK */
public abstract class e {

    /* renamed from: a reason: collision with root package name */
    protected g f4126a;
    private Handler b = null;
    private Long c = null;
    private boolean d = false;

    /* access modifiers changed from: protected */
    public abstract boolean c();

    /* access modifiers changed from: protected */
    public abstract long d();

    /* access modifiers changed from: protected */
    public String e() {
        return "CacheScheduledTask";
    }

    public e(g gVar) {
        this.f4126a = gVar;
    }

    public final void f() {
        if (!this.d) {
            if (this.c == null) {
                this.c = Long.valueOf(System.currentTimeMillis());
            }
            if (c()) {
                if (this.b == null) {
                    Looper myLooper = Looper.myLooper();
                    if (myLooper == null) {
                        myLooper = Looper.getMainLooper();
                    }
                    this.b = new Handler(myLooper);
                }
                long d2 = d();
                if (d2 >= 0) {
                    this.d = true;
                    e();
                    StringBuilder sb = new StringBuilder("Started for ");
                    sb.append(this.f4126a.c());
                    sb.append(" - scheduled to: ");
                    sb.append(d2);
                    this.b.postDelayed(new Runnable() {
                        public final void run() {
                            e.this.b();
                        }
                    }, d2);
                    return;
                }
                e();
                return;
            }
            e();
        }
    }

    public final void g() {
        j();
        k();
    }

    public final void h() {
        j();
        this.d = false;
    }

    public void a() {
        e();
        new StringBuilder("Resetting for ").append(this.f4126a.c());
        g();
    }

    /* access modifiers changed from: protected */
    public void b() {
        e();
        new StringBuilder("Time reached, reloading ").append(this.f4126a.c());
        k();
        this.f4126a.i();
    }

    /* access modifiers changed from: protected */
    public final Long i() {
        return this.c;
    }

    private void j() {
        if (this.b != null) {
            this.b.removeCallbacksAndMessages(null);
        }
    }

    private void k() {
        this.c = null;
        this.d = false;
    }
}
