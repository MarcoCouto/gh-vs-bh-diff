package com.startapp.android.publish.cache;

import com.facebook.appevents.UserDataStore;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.Ad.AdType;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import java.io.Serializable;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class c implements Serializable {
    private static final long serialVersionUID = 1;
    private String advertiserId;
    private Set<String> categories;
    private Set<String> categoriesExclude;
    private String country;
    private boolean forceFullpage = false;
    private boolean forceOfferWall2D = false;
    private boolean forceOfferWall3D = false;
    private boolean forceOverlay = false;
    private Double minCpm;
    private Placement placement;
    private String template;
    private boolean testMode = false;
    private AdType type;
    private boolean videoMuted = false;

    public c(Placement placement2, AdPreferences adPreferences) {
        AdType adType = null;
        this.country = null;
        this.advertiserId = null;
        this.template = null;
        this.type = null;
        this.placement = placement2;
        this.categories = adPreferences.getCategories();
        this.categoriesExclude = adPreferences.getCategoriesExclude();
        this.videoMuted = adPreferences.isVideoMuted();
        this.minCpm = adPreferences.getMinCpm();
        this.forceOfferWall3D = j.a(adPreferences, "forceOfferWall3D");
        this.forceOfferWall2D = j.a(adPreferences, "forceOfferWall2D");
        this.forceFullpage = j.a(adPreferences, "forceFullpage");
        this.forceOverlay = j.a(adPreferences, "forceOverlay");
        this.testMode = j.a(adPreferences, "testMode");
        this.country = j.b(adPreferences, UserDataStore.COUNTRY);
        this.advertiserId = j.b(adPreferences, "advertiserId");
        this.template = j.b(adPreferences, "template");
        Object a2 = j.a(adPreferences.getClass(), "type", (Object) adPreferences);
        if (a2 != null && (a2 instanceof AdType)) {
            adType = (AdType) a2;
        }
        this.type = adType;
    }

    public final Placement a() {
        return this.placement;
    }

    public final int hashCode() {
        int i = 0;
        int i2 = 1237;
        int hashCode = ((((((((((((((((((((((this.advertiserId == null ? 0 : this.advertiserId.hashCode()) + 31) * 31) + (this.categories == null ? 0 : this.categories.hashCode())) * 31) + (this.categoriesExclude == null ? 0 : this.categoriesExclude.hashCode())) * 31) + (this.country == null ? 0 : this.country.hashCode())) * 31) + (this.minCpm == null ? 0 : this.minCpm.hashCode())) * 31) + (this.forceFullpage ? 1231 : 1237)) * 31) + (this.forceOfferWall2D ? 1231 : 1237)) * 31) + (this.forceOfferWall3D ? 1231 : 1237)) * 31) + (this.forceOverlay ? 1231 : 1237)) * 31) + (this.placement == null ? 0 : this.placement.hashCode())) * 31) + (this.template == null ? 0 : this.template.hashCode())) * 31;
        if (this.testMode) {
            i2 = 1231;
        }
        int i3 = (hashCode + i2) * 31;
        if (this.type != null) {
            i = this.type.hashCode();
        }
        return i3 + i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (this.advertiserId == null) {
            if (cVar.advertiserId != null) {
                return false;
            }
        } else if (!this.advertiserId.equals(cVar.advertiserId)) {
            return false;
        }
        if (this.categories == null) {
            if (cVar.categories != null) {
                return false;
            }
        } else if (!this.categories.equals(cVar.categories)) {
            return false;
        }
        if (this.categoriesExclude == null) {
            if (cVar.categoriesExclude != null) {
                return false;
            }
        } else if (!this.categoriesExclude.equals(cVar.categoriesExclude)) {
            return false;
        }
        if (this.country == null) {
            if (cVar.country != null) {
                return false;
            }
        } else if (!this.country.equals(cVar.country)) {
            return false;
        }
        if (this.forceFullpage != cVar.forceFullpage || this.forceOfferWall2D != cVar.forceOfferWall2D || this.forceOfferWall3D != cVar.forceOfferWall3D || this.forceOverlay != cVar.forceOverlay || this.placement != cVar.placement) {
            return false;
        }
        if (this.template == null) {
            if (cVar.template != null) {
                return false;
            }
        } else if (!this.template.equals(cVar.template)) {
            return false;
        }
        if (this.testMode != cVar.testMode || this.videoMuted != cVar.videoMuted) {
            return false;
        }
        if (this.type == null) {
            if (cVar.type != null) {
                return false;
            }
        } else if (!this.type.equals(cVar.type)) {
            return false;
        }
        if (this.minCpm == null) {
            if (cVar.minCpm != null) {
                return false;
            }
        } else if (this.minCpm != cVar.minCpm) {
            return false;
        }
        return true;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("CacheKey [placement=");
        sb.append(this.placement);
        sb.append(", categories=");
        sb.append(this.categories);
        sb.append(", categoriesExclude=");
        sb.append(this.categoriesExclude);
        sb.append(", forceOfferWall3D=");
        sb.append(this.forceOfferWall3D);
        sb.append(", forceOfferWall2D=");
        sb.append(this.forceOfferWall2D);
        sb.append(", forceFullpage=");
        sb.append(this.forceFullpage);
        sb.append(", forceOverlay=");
        sb.append(this.forceOverlay);
        sb.append(", testMode=");
        sb.append(this.testMode);
        sb.append(", minCpm=");
        sb.append(this.minCpm);
        sb.append(", country=");
        sb.append(this.country);
        sb.append(", advertiserId=");
        sb.append(this.advertiserId);
        sb.append(", template=");
        sb.append(this.template);
        sb.append(", type=");
        sb.append(this.type);
        sb.append(", videoMuted=");
        sb.append(this.videoMuted);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
