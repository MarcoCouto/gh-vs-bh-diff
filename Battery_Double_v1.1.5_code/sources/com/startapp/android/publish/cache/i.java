package com.startapp.android.publish.cache;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.HtmlAd;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.g;
import com.startapp.android.publish.common.model.AdDetails;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.f;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class i {

    /* renamed from: a reason: collision with root package name */
    private List<String> f4132a;
    private String b;

    /* compiled from: StartAppSDK */
    public interface a {
        void a(g gVar);
    }

    /* compiled from: StartAppSDK */
    protected static class b implements Serializable {
        private static final long serialVersionUID = 1;
        protected AdPreferences adPreferences;
        private int numberOfLoadedAd;
        protected Placement placement;

        protected b(Placement placement2, AdPreferences adPreferences2) {
            this.placement = placement2;
            this.adPreferences = adPreferences2;
        }

        /* access modifiers changed from: protected */
        public final Placement a() {
            return this.placement;
        }

        /* access modifiers changed from: protected */
        public final AdPreferences b() {
            return this.adPreferences;
        }

        /* access modifiers changed from: protected */
        public final int c() {
            return this.numberOfLoadedAd;
        }

        /* access modifiers changed from: protected */
        public final void a(int i) {
            this.numberOfLoadedAd = i;
        }
    }

    /* compiled from: StartAppSDK */
    public interface c {
        void a(List<b> list);
    }

    /* compiled from: StartAppSDK */
    protected static class d implements Serializable {
        private static final long serialVersionUID = 1;
        private g ad;
        private String html;

        protected d(g gVar) {
            this.ad = gVar;
            if (this.ad != null && (this.ad instanceof HtmlAd)) {
                this.html = ((HtmlAd) this.ad).getHtml();
            }
        }

        /* access modifiers changed from: protected */
        public final g a() {
            return this.ad;
        }

        /* access modifiers changed from: protected */
        public final String b() {
            return this.html;
        }
    }

    /* compiled from: StartAppSDK */
    public interface e {
        void a();
    }

    protected static void a(Context context, Placement placement, AdPreferences adPreferences, String str, int i) {
        b bVar = new b(placement, adPreferences);
        bVar.a(i);
        com.startapp.common.a.e.a(context, a(), str, bVar);
    }

    protected static void a(Context context, g gVar, String str) {
        com.startapp.common.a.e.a(context, b(), str, new d(gVar.b()));
    }

    protected static void a(final Context context, final String str, final a aVar, final AdEventListener adEventListener) {
        f.a(com.startapp.common.f.a.HIGH, (Runnable) new Runnable() {
            public final void run() {
                try {
                    Class<d> cls = d.class;
                    final d dVar = (d) com.startapp.common.a.e.a(context, i.b(), str);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            try {
                                if (dVar == null) {
                                    new StringBuilder("File not found or error: ").append(str);
                                    adEventListener.onFailedToReceiveAd(null);
                                    return;
                                }
                                if (dVar.a() != null) {
                                    if (dVar.a().isReady()) {
                                        if (dVar.a().hasAdCacheTtlPassed()) {
                                            adEventListener.onFailedToReceiveAd(null);
                                            return;
                                        } else {
                                            i.a(context, dVar, aVar, adEventListener);
                                            return;
                                        }
                                    }
                                }
                                adEventListener.onFailedToReceiveAd(null);
                            } catch (Exception e) {
                                com.startapp.android.publish.adsCommon.g.f.a(context, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "DiskAdCacheManager.loadCachedAdAsync - Unexpected Thread Exception", e.getMessage(), "");
                                adEventListener.onFailedToReceiveAd(null);
                            }
                        }
                    });
                } catch (Exception e) {
                    com.startapp.android.publish.adsCommon.g.f.a(context, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "DiskAdCacheManager.loadCachedAdAsync - Unexpected Thread Exception", e.getMessage(), "");
                    adEventListener.onFailedToReceiveAd(null);
                }
            }
        });
    }

    protected static String a() {
        return "startapp_ads".concat(File.separator).concat("keys");
    }

    protected static String b() {
        return "startapp_ads".concat(File.separator).concat("interstitials");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    protected static void a(Context context, d dVar, a aVar, final AdEventListener adEventListener) {
        g a2 = dVar.a();
        a2.setContext(context);
        boolean z = false;
        if (j.a(2) && (a2 instanceof com.startapp.android.publish.ads.b.c)) {
            final com.startapp.android.publish.ads.b.c cVar = (com.startapp.android.publish.ads.b.c) a2;
            String b2 = dVar.b();
            if (b2 != null && !b2.equals("")) {
                if (com.startapp.android.publish.adsCommon.b.a().E()) {
                    List a3 = com.b.a.a.a.b.a(b2, 0);
                    ArrayList arrayList = new ArrayList();
                    if (com.b.a.a.a.b.a(context, a3, 0, (Set<String>) new HashSet<String>(), (List<com.startapp.android.publish.adsCommon.c.a>) arrayList).booleanValue()) {
                        new com.startapp.android.publish.adsCommon.c.b(context, arrayList).a();
                        if (z) {
                            a.a().a(b2, cVar.getHtmlUuid());
                            aVar.a(cVar);
                            j.a(context, b2, (com.startapp.android.publish.adsCommon.a.j.a) new com.startapp.android.publish.adsCommon.a.j.a() {
                                public final void a() {
                                    adEventListener.onReceiveAd(cVar);
                                }

                                public final void a(String str) {
                                    adEventListener.onFailedToReceiveAd(cVar);
                                }
                            });
                            return;
                        }
                    }
                }
                z = true;
                if (z) {
                }
            }
            adEventListener.onFailedToReceiveAd(null);
        } else if (!j.a(64) || !(a2 instanceof com.startapp.android.publish.ads.c.b.b)) {
            adEventListener.onFailedToReceiveAd(null);
        } else {
            com.startapp.android.publish.ads.c.b.b bVar = (com.startapp.android.publish.ads.c.b.b) a2;
            List d2 = bVar.d();
            if (d2 != null) {
                if (com.startapp.android.publish.adsCommon.b.a().E()) {
                    d2 = com.b.a.a.a.b.a(context, d2, 0, (Set<String>) new HashSet<String>());
                }
                if (d2 != null && d2.size() > 0) {
                    aVar.a(bVar);
                    a(bVar, adEventListener, d2);
                    return;
                }
            }
            adEventListener.onFailedToReceiveAd(null);
        }
    }

    private static void a(com.startapp.android.publish.ads.c.b.b bVar, AdEventListener adEventListener, List<AdDetails> list) {
        com.startapp.android.publish.adsCommon.m.a a2 = com.startapp.android.publish.ads.list3d.e.a().a(bVar.a());
        a2.a();
        for (AdDetails a3 : list) {
            a2.a(a3);
        }
        adEventListener.onReceiveAd(bVar);
    }

    public i(List<String> list, String str) {
        this.f4132a = list;
        this.b = str;
    }

    public final List<String> c() {
        return this.f4132a;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("[VideoEvent: tag=");
        sb.append(this.b);
        sb.append(", fullUrls=");
        sb.append(this.f4132a.toString());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
