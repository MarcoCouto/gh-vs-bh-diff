package com.startapp.android.publish.cache;

import android.app.Activity;
import android.content.Context;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.b.d;
import com.startapp.android.publish.ads.b.e;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.HtmlAd;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.h;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: StartAppSDK */
public final class g implements com.startapp.android.publish.cache.i.a {

    /* renamed from: a reason: collision with root package name */
    protected com.startapp.android.publish.adsCommon.g f4128a;
    protected AtomicBoolean b;
    protected long c;
    protected f d;
    protected b e;
    protected Map<AdEventListener, List<StartAppAd>> f;
    /* access modifiers changed from: private */
    public final Placement g;
    private Context h;
    private com.startapp.android.publish.adsCommon.a i;
    private AdPreferences j;
    private String k;
    private boolean l;
    private int m;
    private boolean n;
    private b o;

    /* compiled from: StartAppSDK */
    class a implements AdEventListener {

        /* renamed from: a reason: collision with root package name */
        private boolean f4131a = false;
        private boolean b = false;

        a() {
        }

        public final void onReceiveAd(Ad ad) {
            boolean z = g.this.f4128a != null && g.this.f4128a.getVideoCancelCallBack();
            if (!this.f4131a && !z) {
                this.f4131a = true;
                synchronized (g.this.f) {
                    for (AdEventListener adEventListener : g.this.f.keySet()) {
                        if (adEventListener != null) {
                            List<StartAppAd> list = (List) g.this.f.get(adEventListener);
                            if (list != null) {
                                for (StartAppAd startAppAd : list) {
                                    startAppAd.setErrorMessage(ad.getErrorMessage());
                                    new com.startapp.android.publish.adsCommon.adListeners.b(adEventListener).onReceiveAd(startAppAd);
                                }
                            }
                        }
                    }
                    g.this.f.clear();
                }
            }
            g.this.d.f();
            g.this.e.a();
            g.this.b.set(false);
        }

        public final void onFailedToReceiveAd(Ad ad) {
            ConcurrentHashMap concurrentHashMap;
            Map map = null;
            if (!this.b) {
                synchronized (g.this.f) {
                    concurrentHashMap = new ConcurrentHashMap(g.this.f);
                    g.this.f4128a = null;
                    g.this.f.clear();
                }
                map = concurrentHashMap;
            }
            if (map != null) {
                for (AdEventListener adEventListener : map.keySet()) {
                    if (adEventListener != null) {
                        List<StartAppAd> list = (List) map.get(adEventListener);
                        if (list != null) {
                            for (StartAppAd startAppAd : list) {
                                startAppAd.setErrorMessage(ad.getErrorMessage());
                                new com.startapp.android.publish.adsCommon.adListeners.b(adEventListener).onFailedToReceiveAd(startAppAd);
                            }
                        }
                    }
                }
            }
            this.b = true;
            g.this.e.f();
            g.this.d.a();
            g.this.b.set(false);
        }
    }

    /* compiled from: StartAppSDK */
    public interface b {
        void a(g gVar);
    }

    public g(Context context, Placement placement, AdPreferences adPreferences) {
        this.f4128a = null;
        this.b = new AtomicBoolean(false);
        this.k = null;
        this.l = false;
        this.d = null;
        this.e = null;
        this.f = new ConcurrentHashMap();
        this.n = true;
        this.g = placement;
        this.j = adPreferences;
        if (context instanceof Activity) {
            this.h = context.getApplicationContext();
            this.i = new com.startapp.android.publish.adsCommon.a((Activity) context);
        } else {
            this.h = context;
            this.i = null;
        }
        this.d = new f(this);
        this.e = new b(this);
    }

    public g(Context context, Placement placement, AdPreferences adPreferences, byte b2) {
        this(context, placement, adPreferences);
        this.n = false;
    }

    public final AdPreferences a() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences) {
        this.j = adPreferences;
    }

    public final com.startapp.android.publish.adsCommon.g b() {
        return this.f4128a;
    }

    /* access modifiers changed from: protected */
    public final Placement c() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.k = str;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.l = true;
    }

    public final void a(StartAppAd startAppAd, AdEventListener adEventListener) {
        a(startAppAd, adEventListener, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0077, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0019 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005b  */
    private void a(StartAppAd startAppAd, AdEventListener adEventListener, boolean z) {
        boolean z2;
        synchronized (this.f) {
            if (l() && !q()) {
                if (!z) {
                    z2 = false;
                    if (!z2) {
                        if (!(startAppAd == null || adEventListener == null)) {
                            List list = (List) this.f.get(adEventListener);
                            if (list == null) {
                                list = new ArrayList();
                                this.f.put(adEventListener, list);
                            }
                            list.add(startAppAd);
                        }
                        if (this.b.compareAndSet(false, true)) {
                            this.d.g();
                            this.e.g();
                            n();
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append(this.g);
                            sb.append(" ad is currently loading");
                            return;
                        }
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(this.g);
                        sb2.append(" ad already loaded");
                        if (!(startAppAd == null || adEventListener == null)) {
                            new com.startapp.android.publish.adsCommon.adListeners.b(adEventListener).onReceiveAd(startAppAd);
                        }
                    }
                }
            }
            z2 = true;
            if (!z2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        new StringBuilder("Invalidating: ").append(this.g);
        if (l()) {
            Context context = this.h;
            Ad ad = (Ad) this.f4128a;
            boolean z = false;
            if (ad != null) {
                HashSet hashSet = new HashSet();
                if (ad instanceof HtmlAd) {
                    z = com.b.a.a.a.b.a(context, com.b.a.a.a.b.a(((HtmlAd) ad).getHtml(), 0), 0, (Set<String>) hashSet, (List<com.startapp.android.publish.adsCommon.c.a>) new ArrayList<com.startapp.android.publish.adsCommon.c.a>()).booleanValue();
                } else if ((ad instanceof h) && com.b.a.a.a.b.a(context, ((h) ad).d(), 0, (Set<String>) hashSet, false).size() == 0) {
                    z = true;
                }
            }
            if (z || q()) {
                a(null, null, true);
            } else if (!this.b.get()) {
                this.d.f();
            }
        } else if (!this.b.get()) {
            this.e.f();
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.e.h();
    }

    /* access modifiers changed from: protected */
    public final void g() {
        this.d.h();
    }

    private boolean l() {
        return this.f4128a != null && this.f4128a.isReady();
    }

    public final com.startapp.android.publish.adsCommon.g h() {
        if (!l()) {
            return null;
        }
        com.startapp.android.publish.adsCommon.g gVar = this.f4128a;
        this.m = 0;
        if (!AdsConstants.OVERRIDE_NETWORK.booleanValue() && this.n) {
            new StringBuilder("Ad shown, reloading ").append(this.g);
            a(null, null, true);
            return gVar;
        } else if (this.n) {
            return gVar;
        } else {
            if (this.o != null) {
                this.o.a(this);
            }
            if (this.d == null) {
                return gVar;
            }
            this.d.a();
            return gVar;
        }
    }

    private com.startapp.android.publish.adsCommon.g m() {
        com.startapp.android.publish.adsCommon.g gVar;
        j.a(this.h, this.j);
        switch (this.g) {
            case INAPP_FULL_SCREEN:
                gVar = new d(this.h);
                break;
            case INAPP_OVERLAY:
                if (!j.a(4)) {
                    gVar = new d(this.h);
                    break;
                } else {
                    gVar = new com.startapp.android.publish.ads.video.g(this.h);
                    break;
                }
            case INAPP_OFFER_WALL:
                boolean z = false;
                boolean z2 = new Random().nextInt(100) < com.startapp.android.publish.adsCommon.b.a().d();
                boolean a2 = j.a(this.j, "forceOfferWall3D");
                boolean z3 = !j.a(this.j, "forceOfferWall2D");
                boolean a3 = j.a(64);
                if (j.a(64) && !j.a(128)) {
                    z = true;
                }
                if (!z && (!a3 || ((!z2 && !a2) || !z3))) {
                    gVar = new com.startapp.android.publish.ads.c.a.b(this.h);
                    break;
                } else {
                    gVar = new com.startapp.android.publish.ads.c.b.b(this.h);
                    break;
                }
            case INAPP_RETURN:
                gVar = new e(this.h);
                break;
            case INAPP_SPLASH:
                gVar = new com.startapp.android.publish.ads.splash.b(this.h);
                break;
            default:
                gVar = new d(this.h);
                break;
        }
        StringBuilder sb = new StringBuilder("ad Type: [");
        sb.append(gVar.getClass().toString());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return gVar;
    }

    private void n() {
        if (this.f4128a != null) {
            this.f4128a.setVideoCancelCallBack(false);
        }
        if (o()) {
            this.l = false;
            p();
            return;
        }
        k();
    }

    private boolean o() {
        return this.l && this.k != null;
    }

    private void p() {
        StringBuilder sb = new StringBuilder("Loading ");
        sb.append(this.g);
        sb.append(" from disk file name: ");
        sb.append(this.k);
        final a aVar = new a();
        i.a(this.h, this.k, (com.startapp.android.publish.cache.i.a) this, (AdEventListener) new AdEventListener() {
            public final void onReceiveAd(Ad ad) {
                aVar.onReceiveAd(ad);
            }

            public final void onFailedToReceiveAd(Ad ad) {
                StringBuilder sb = new StringBuilder("Failed to load ");
                sb.append(g.this.g);
                sb.append(" from disk");
                g.this.f4128a = null;
                g.this.k();
            }
        });
    }

    public final void a(b bVar) {
        this.o = bVar;
    }

    public final int j() {
        return this.m;
    }

    public final void a(int i2) {
        this.m = i2;
    }

    /* access modifiers changed from: protected */
    public final void k() {
        StringBuilder sb = new StringBuilder("Loading ");
        sb.append(this.g);
        sb.append(" from server");
        this.f4128a = m();
        this.f4128a.setActivityExtra(this.i);
        this.f4128a.load(this.j, new a());
        this.c = System.currentTimeMillis();
    }

    private boolean q() {
        if (this.f4128a == null) {
            return false;
        }
        return this.f4128a.hasAdCacheTtlPassed();
    }

    public final boolean i() {
        if (this.m < com.startapp.android.publish.common.metaData.e.getInstance().getStopAutoLoadAmount()) {
            a(null, null, true);
            this.m++;
            return true;
        }
        if (this.o != null) {
            this.o.a(this);
        }
        return false;
    }

    public final void a(com.startapp.android.publish.adsCommon.g gVar) {
        new StringBuilder("Success loading from disk: ").append(this.g);
        this.f4128a = gVar;
    }
}
