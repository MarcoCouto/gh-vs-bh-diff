package com.startapp.android.publish.adsCommon;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import com.b.a.a.a.b;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.metaData.f;
import com.startapp.common.Constants;
import com.startapp.common.a.c;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: StartAppSDK */
public final class l {

    /* renamed from: a reason: collision with root package name */
    private static List<PackageInfo> f4101a = null;
    private static List<PackageInfo> b = null;
    private static long c = 0;
    private static volatile Pair<a, String> d = null;
    private static volatile Pair<a, String> e = null;
    private static boolean f = true;
    private static boolean g = false;
    private static a h = a.UNDEFINED;

    /* compiled from: StartAppSDK */
    enum a {
        T1("token"),
        T2("token2"),
        UNDEFINED("");
        
        private final String text;

        private a(String str) {
            this.text = str;
        }

        public final String toString() {
            return this.text;
        }
    }

    public static long a() {
        return c;
    }

    public static void a(final Context context) {
        c(context);
        f = true;
        g = false;
        h = a.UNDEFINED;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                l.b();
                l.c(context);
            }
        }, intentFilter);
        e.getInstance().addMetaDataListener(new f() {
            public final void a(boolean z) {
                if (z) {
                    l.b();
                    l.c(context);
                }
                e.getInstance().addMetaDataListener(this);
            }

            public final void a() {
                e.getInstance().addMetaDataListener(this);
            }
        });
    }

    public static void b(Context context) {
        a(context, e.getInstance().getSimpleTokenConfig().a(context));
    }

    public static void c(final Context context) {
        try {
            if ((d == null || e == null) && e.getInstance().getSimpleTokenConfig().a(context)) {
                com.startapp.common.f.a(com.startapp.common.f.a.HIGH, (Runnable) new Runnable() {
                    public final void run() {
                        l.b(context);
                    }
                });
            }
        } catch (Exception e2) {
            com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "initSimpleTokenAsync", e2.getMessage(), "");
        }
    }

    @VisibleForTesting
    private static synchronized void a(Context context, boolean z) {
        synchronized (l.class) {
            if ((d == null || e == null) && z) {
                try {
                    g(context);
                    d = new Pair<>(a.T1, b.a(a(f4101a)));
                    e = new Pair<>(a.T2, b.a(a(b)));
                    StringBuilder sb = new StringBuilder("simpleToken : [");
                    sb.append(d);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    StringBuilder sb2 = new StringBuilder("simpleToken2 : [");
                    sb2.append(e);
                    sb2.append(RequestParameters.RIGHT_BRACKETS);
                } catch (Exception e2) {
                    com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "initSimpleToken", e2.getMessage(), "");
                }
            }
        }
    }

    public static void b() {
        d = null;
        e = null;
    }

    static Pair<String, String> d(Context context) {
        return a(context, e.getInstance().getSimpleTokenConfig().a(context), e.getInstance().isAlwaysSendToken(), e.getInstance().isToken1Mandatory());
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:2|3|(3:5|6|(7:8|(2:10|(1:12)(5:13|(1:16)(2:17|(1:19)(1:20))|21|(2:24|(1:26))|23))|14|(0)(0)|21|(0)|23)(3:27|(1:29)(1:30)|31))|32|33|34) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x0059 */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[Catch:{ Exception -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002b A[Catch:{ Exception -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[Catch:{ Exception -> 0x0059 }] */
    @VisibleForTesting
    private static synchronized Pair<String, String> a(Context context, boolean z, boolean z2, boolean z3) {
        Pair<String, String> pair;
        Pair pair2;
        synchronized (l.class) {
            Pair pair3 = new Pair(a.T1, "");
            if (z) {
                if (h == a.UNDEFINED) {
                    boolean z4 = f;
                    if (g) {
                        if (!f) {
                            pair2 = f(context);
                            if (z3) {
                                z4 = !g;
                            }
                            g = z4;
                            if (!z2) {
                                if (!k.a(context, "shared_prefs_simple_token", "").equals(pair2.second)) {
                                }
                            }
                            pair3 = pair2;
                        }
                    }
                    pair2 = e(context);
                    if (z3) {
                    }
                    g = z4;
                    if (!z2) {
                    }
                    pair3 = pair2;
                } else {
                    pair3 = h == a.T1 ? e(context) : f(context);
                }
            }
            pair = new Pair<>(((a) pair3.first).toString(), pair3.second);
        }
        return pair;
    }

    static void a(Pair<String, String> pair) {
        h = a.valueOf((String) pair.first);
    }

    public static Pair<String, String> c() {
        if (d != null) {
            return new Pair<>(((a) d.first).toString(), d.second);
        }
        return new Pair<>(a.T1.toString(), "");
    }

    public static Pair<String, String> d() {
        if (e != null) {
            return new Pair<>(((a) e.first).toString(), e.second);
        }
        return new Pair<>(a.T2.toString(), "");
    }

    private static Pair<a, String> e(Context context) {
        if (d == null) {
            b(context);
        }
        k.b(context, "shared_prefs_simple_token", (String) d.second);
        f = false;
        h = a.UNDEFINED;
        return new Pair<>(a.T1, d.second);
    }

    private static Pair<a, String> f(Context context) {
        if (e == null) {
            b(context);
        }
        k.b(context, "shared_prefs_simple_token2", (String) e.second);
        f = false;
        h = a.UNDEFINED;
        return new Pair<>(a.T2, e.second);
    }

    private static synchronized void g(Context context) {
        synchronized (l.class) {
            PackageManager packageManager = context.getPackageManager();
            Set installersList = e.getInstance().getInstallersList();
            Set preInstalledPackages = e.getInstance().getPreInstalledPackages();
            f4101a = new CopyOnWriteArrayList();
            b = new CopyOnWriteArrayList();
            try {
                List<PackageInfo> a2 = c.a(packageManager);
                c = VERSION.SDK_INT >= 9 ? Long.MAX_VALUE : 0;
                PackageInfo packageInfo = null;
                for (PackageInfo packageInfo2 : a2) {
                    if (VERSION.SDK_INT >= 9 && c > packageInfo2.firstInstallTime) {
                        c = packageInfo2.firstInstallTime;
                    }
                    if (!c.a(packageInfo2)) {
                        f4101a.add(packageInfo2);
                        try {
                            String installerPackageName = packageManager.getInstallerPackageName(packageInfo2.packageName);
                            if (installersList != null && installersList.contains(installerPackageName)) {
                                b.add(packageInfo2);
                            }
                        } catch (Exception e2) {
                            new StringBuilder("addToPackagesFromInstallers - can't add app to list ").append(e2.getMessage());
                        }
                    } else if (preInstalledPackages.contains(packageInfo2.packageName)) {
                        f4101a.add(packageInfo2);
                    } else if (packageInfo2.packageName.equals(Constants.f4160a)) {
                        packageInfo = packageInfo2;
                    }
                }
                f4101a = b(f4101a);
                b = b(b);
                if (packageInfo != null) {
                    f4101a.add(0, packageInfo);
                }
            } catch (Exception unused) {
            }
        }
    }

    private static List<String> a(List<PackageInfo> list) {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo packageInfo : list) {
            arrayList.add(packageInfo.packageName);
        }
        return arrayList;
    }

    private static List<PackageInfo> b(List<PackageInfo> list) {
        if (list.size() <= 100) {
            return list;
        }
        ArrayList arrayList = new ArrayList(list);
        c((List<PackageInfo>) arrayList);
        return arrayList.subList(0, 100);
    }

    private static void c(List<PackageInfo> list) {
        if (VERSION.SDK_INT >= 9) {
            Collections.sort(list, new Comparator<PackageInfo>() {
                @SuppressLint({"InlinedApi"})
                public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                    PackageInfo packageInfo = (PackageInfo) obj2;
                    long j = ((PackageInfo) obj).firstInstallTime;
                    long j2 = packageInfo.firstInstallTime;
                    if (j > j2) {
                        return -1;
                    }
                    return j == j2 ? 0 : 1;
                }
            });
        }
    }
}
