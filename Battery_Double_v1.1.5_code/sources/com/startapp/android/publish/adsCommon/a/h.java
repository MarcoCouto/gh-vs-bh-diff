package com.startapp.android.publish.adsCommon.a;

import android.content.Context;
import com.startapp.android.publish.adsCommon.b.b;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.metaData.g.a;
import com.startapp.android.publish.common.model.AdPreferences;
import java.util.UUID;

/* compiled from: StartAppSDK */
public final class h {

    /* renamed from: a reason: collision with root package name */
    private static h f4012a = new h();
    private String b = "";
    private long c = 0;
    private a d = a.LAUNCH;

    public final String a() {
        return this.b;
    }

    public final long b() {
        return this.c;
    }

    public final a c() {
        return this.d;
    }

    public final synchronized void a(Context context, a aVar) {
        this.b = UUID.randomUUID().toString();
        this.c = System.currentTimeMillis();
        this.d = aVar;
        StringBuilder sb = new StringBuilder("Starting new session: reason=");
        sb.append(aVar);
        sb.append(" sessionId=");
        sb.append(this.b);
        if (!j.a()) {
            b.a().b();
        }
        AdPreferences adPreferences = new AdPreferences();
        j.a(context, adPreferences);
        e.getInstance().loadFromServer(context, adPreferences, aVar, false, null, true);
    }

    public static h d() {
        return f4012a;
    }
}
