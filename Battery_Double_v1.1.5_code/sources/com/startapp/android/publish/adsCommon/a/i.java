package com.startapp.android.publish.adsCommon.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.b.a.a.a.b.b;
import com.b.a.a.a.c.e;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Set;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class i {

    /* renamed from: a reason: collision with root package name */
    private final b f4013a;

    public static int a(Context context, int i) {
        return Math.round(TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics()));
    }

    public static int b(Context context, int i) {
        return Math.round(((float) i) / context.getResources().getDisplayMetrics().density);
    }

    public static void a(TextView textView, Set<String> set) {
        if (set.contains("UNDERLINE")) {
            textView.setPaintFlags(textView.getPaintFlags() | 8);
        }
        int i = 0;
        if (set.contains("BOLD") && set.contains("ITALIC")) {
            i = 3;
        } else if (set.contains("BOLD")) {
            i = 1;
        } else if (set.contains("ITALIC")) {
            i = 2;
        }
        textView.setTypeface(null, i);
    }

    public static TextView a(Context context, Typeface typeface, float f, int i, int i2) {
        TextView textView = new TextView(context);
        textView.setTypeface(typeface, 1);
        textView.setTextSize(1, f);
        textView.setSingleLine(true);
        textView.setEllipsize(TruncateAt.END);
        textView.setTextColor(i);
        textView.setId(i2);
        return textView;
    }

    public static LayoutParams a(Context context, int[] iArr, int[] iArr2) {
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        for (int addRule : iArr2) {
            layoutParams.addRule(addRule);
        }
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = iArr[i] == 0 ? 0 : a(context, iArr[i]);
        }
        layoutParams.setMargins(iArr[0], iArr[1], iArr[2], iArr[3]);
        return layoutParams;
    }

    public static LayoutParams a(Context context, int[] iArr, int[] iArr2, int i, int i2) {
        LayoutParams a2 = a(context, iArr, iArr2);
        a2.addRule(i, i2);
        return a2;
    }

    public static ImageView a(Context context, Bitmap bitmap, int i) {
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(bitmap);
        imageView.setId(i);
        return imageView;
    }

    private i(b bVar) {
        this.f4013a = bVar;
    }

    public static i a(b bVar) {
        b bVar2 = bVar;
        com.b.a.a.a.b.a((Object) bVar, "AdSession is null");
        if (!bVar2.l()) {
            throw new IllegalStateException("Cannot create VideoEvents for JavaScript AdSession");
        } else if (!bVar2.i()) {
            com.b.a.a.a.b.a(bVar2);
            if (bVar2.e().e() == null) {
                i iVar = new i(bVar2);
                bVar2.e().a(iVar);
                return iVar;
            }
            throw new IllegalStateException("VideoEvents already exists for AdSession");
        } else {
            throw new IllegalStateException("AdSession is started");
        }
    }

    public final void a(float f, float f2) {
        if (f > 0.0f) {
            b(f2);
            com.b.a.a.a.b.b(this.f4013a);
            JSONObject jSONObject = new JSONObject();
            com.b.a.a.a.e.b.a(jSONObject, IronSourceConstants.EVENTS_DURATION, Float.valueOf(f));
            com.b.a.a.a.e.b.a(jSONObject, "videoPlayerVolume", Float.valueOf(f2));
            com.b.a.a.a.e.b.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(e.a().d()));
            this.f4013a.e().a("start", jSONObject);
            return;
        }
        throw new IllegalArgumentException("Invalid Video duration");
    }

    public final void a() {
        com.b.a.a.a.b.b(this.f4013a);
        this.f4013a.e().a("firstQuartile");
    }

    public final void b() {
        com.b.a.a.a.b.b(this.f4013a);
        this.f4013a.e().a("midpoint");
    }

    public final void c() {
        com.b.a.a.a.b.b(this.f4013a);
        this.f4013a.e().a("thirdQuartile");
    }

    public final void d() {
        com.b.a.a.a.b.b(this.f4013a);
        this.f4013a.e().a("complete");
    }

    public final void e() {
        com.b.a.a.a.b.b(this.f4013a);
        this.f4013a.e().a(CampaignEx.JSON_NATIVE_VIDEO_PAUSE);
    }

    public final void f() {
        com.b.a.a.a.b.b(this.f4013a);
        this.f4013a.e().a(String.VIDEO_BUFFER_START);
    }

    public final void g() {
        com.b.a.a.a.b.b(this.f4013a);
        this.f4013a.e().a("bufferFinish");
    }

    public final void h() {
        com.b.a.a.a.b.b(this.f4013a);
        this.f4013a.e().a(String.VIDEO_SKIPPED);
    }

    public final void a(float f) {
        b(f);
        com.b.a.a.a.b.b(this.f4013a);
        JSONObject jSONObject = new JSONObject();
        com.b.a.a.a.e.b.a(jSONObject, "videoPlayerVolume", Float.valueOf(f));
        com.b.a.a.a.e.b.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(e.a().d()));
        this.f4013a.e().a("volumeChange", jSONObject);
    }

    private static void b(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Invalid Video volume");
        }
    }
}
