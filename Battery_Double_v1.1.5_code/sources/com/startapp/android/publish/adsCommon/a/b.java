package com.startapp.android.publish.adsCommon.a;

import android.content.Context;
import android.os.SystemClock;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.metaData.i;
import com.startapp.android.publish.common.metaData.j;

/* compiled from: StartAppSDK */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private static volatile boolean f4006a = false;

    /* compiled from: StartAppSDK */
    public static final class a implements com.startapp.common.b.a.a {
        public final com.startapp.common.b.a.b create(int i) {
            if (i == 586482792) {
                return new j();
            }
            if (i != 786564404) {
                return null;
            }
            return new i();
        }
    }

    public static void a(Context context) {
        if (!f4006a) {
            f4006a = true;
            com.startapp.common.b.a.a((com.startapp.android.publish.ads.video.b.c.a) new com.startapp.android.publish.ads.video.b.c.a() {
            });
            com.startapp.common.b.a.a(context);
            com.startapp.common.b.a.a((com.startapp.common.b.a.a) new a());
        }
    }

    public static long a() {
        return SystemClock.elapsedRealtime() + (((long) e.getInstance().getPeriodicMetaDataInterval()) * ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
    }

    public static long b(Context context) {
        return SystemClock.elapsedRealtime() + (((long) e.getInstance().getPeriodicInfoEventIntervalInMinutes(context)) * ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
    }

    public static void c(Context context) {
        a(context, Long.valueOf(b(context)));
    }

    public static void a(Context context, Long l) {
        new StringBuilder("setMetaDataPeriodicAlarm executes ").append(l);
        if (!k.a(context, "periodicMetadataPaused", Boolean.FALSE).booleanValue() && e.getInstance().isPeriodicMetaDataEnabled()) {
            a(context, 586482792, l.longValue() - SystemClock.elapsedRealtime(), "periodicMetadataTriggerTime");
        }
    }

    public static void d(Context context) {
        a(context, b(context));
    }

    public static void a(Context context, long j) {
        if (!k.a(context, "periodicInfoEventPaused", Boolean.FALSE).booleanValue() && e.getInstance().isPeriodicInfoEventEnabled()) {
            a(context, 786564404, j - SystemClock.elapsedRealtime(), "periodicInfoEventTriggerTime");
        }
    }

    public static void a(int i) {
        com.startapp.common.b.a.a(i);
    }

    private static void a(Context context, int i, long j, String str) {
        if (com.startapp.common.b.a.a(new com.startapp.common.b.b.a(i).a(j).b())) {
            k.b(context, str, Long.valueOf(j + SystemClock.elapsedRealtime()));
        } else {
            f.a(context, d.EXCEPTION, "Util.setPeriodicAlarm - failed setting alarm ".concat(String.valueOf(i)), "", "");
        }
    }
}
