package com.startapp.android.publish.adsCommon.a;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.e;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class c extends f {

    /* renamed from: a reason: collision with root package name */
    private JSONObject f4007a;

    public c() {
        this.f4007a = null;
        this.f4007a = new JSONObject();
    }

    public final void a(String str, Object obj, boolean z, boolean z2) {
        if (!z || obj != null) {
            if (obj != null && !obj.toString().equals("")) {
                try {
                    this.f4007a.put(str, obj);
                    return;
                } catch (JSONException e) {
                    if (z) {
                        StringBuilder sb = new StringBuilder("failed converting to json object value: [");
                        sb.append(obj);
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        throw new e(sb.toString(), e);
                    }
                }
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder("Required key: [");
        sb2.append(str);
        sb2.append("] is missing");
        throw new e(sb2.toString(), null);
    }

    public final void a(String str, Set<String> set) {
        if (set != null && set.size() > 0) {
            try {
                this.f4007a.put(str, new JSONArray(set));
            } catch (JSONException unused) {
            }
        }
    }

    public final String toString() {
        return this.f4007a.toString();
    }
}
