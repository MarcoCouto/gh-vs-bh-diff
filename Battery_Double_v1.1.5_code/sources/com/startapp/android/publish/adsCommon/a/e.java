package com.startapp.android.publish.adsCommon.a;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class e {

    /* renamed from: a reason: collision with root package name */
    private String f4009a;
    private String b;
    private Set<String> c;

    public final String a() {
        return this.f4009a;
    }

    public final void a(String str) {
        this.f4009a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final Set<String> c() {
        return this.c;
    }

    public final void a(Set<String> set) {
        this.c = set;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("NameValueObject [name=");
        sb.append(this.f4009a);
        sb.append(", value=");
        sb.append(this.b);
        sb.append(", valueSet=");
        sb.append(this.c);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
