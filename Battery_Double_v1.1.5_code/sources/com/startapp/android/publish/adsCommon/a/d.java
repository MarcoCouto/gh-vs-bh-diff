package com.startapp.android.publish.adsCommon.a;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.e;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class d extends f {

    /* renamed from: a reason: collision with root package name */
    private List<e> f4008a;

    public d() {
        this.f4008a = null;
        this.f4008a = new ArrayList();
    }

    public final void a(String str, Object obj, boolean z, boolean z2) {
        if (!z || obj != null) {
            if (obj != null && !obj.toString().equals("")) {
                try {
                    e eVar = new e();
                    eVar.a(str);
                    String obj2 = obj.toString();
                    if (z2) {
                        obj2 = URLEncoder.encode(obj2, "UTF-8");
                    }
                    eVar.b(obj2);
                    this.f4008a.add(eVar);
                    return;
                } catch (UnsupportedEncodingException e) {
                    if (z) {
                        StringBuilder sb = new StringBuilder("failed encoding value: [");
                        sb.append(obj);
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        throw new e(sb.toString(), e);
                    }
                }
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder("Required key: [");
        sb2.append(str);
        sb2.append("] is missing");
        throw new e(sb2.toString(), null);
    }

    public final void a(String str, Set<String> set) {
        if (set != null) {
            e eVar = new e();
            eVar.a(str);
            HashSet hashSet = new HashSet();
            for (String encode : set) {
                try {
                    hashSet.add(URLEncoder.encode(encode, "UTF-8"));
                } catch (UnsupportedEncodingException unused) {
                }
            }
            eVar.a((Set<String>) hashSet);
            this.f4008a.add(eVar);
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.f4008a == null) {
            return sb.toString();
        }
        sb.append('?');
        for (e eVar : this.f4008a) {
            if (eVar.b() != null) {
                sb.append(eVar.a());
                sb.append('=');
                sb.append(eVar.b());
                sb.append('&');
            } else if (eVar.c() != null) {
                Set<String> c = eVar.c();
                if (c != null) {
                    for (String str : c) {
                        sb.append(eVar.a());
                        sb.append('=');
                        sb.append(str);
                        sb.append('&');
                    }
                }
            }
        }
        if (sb.length() != 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString().replace("+", "%20");
    }
}
