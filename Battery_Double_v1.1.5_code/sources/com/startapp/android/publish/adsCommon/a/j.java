package com.startapp.android.publish.adsCommon.a;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.GeneratedConstants;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.common.a.c;
import com.startapp.common.c.b;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: StartAppSDK */
public final class j {

    /* renamed from: a reason: collision with root package name */
    protected static int f4014a;
    private static Map<Activity, Integer> b = new WeakHashMap();
    private static boolean c = false;

    /* compiled from: StartAppSDK */
    public interface a {
        void a();

        void a(String str);
    }

    public static boolean a() {
        return new BigInteger(AdsConstants.d, 10).intValue() == 0;
    }

    public static String b() {
        String str = GeneratedConstants.INAPP_VERSION;
        if (str.equals("${version}")) {
            str = "0";
        }
        StringBuilder sb = new StringBuilder("SDK version: [");
        sb.append(str);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return str;
    }

    public static String c() {
        String str = GeneratedConstants.INAPP_FLAVOR;
        if (str.equals("${flavor}")) {
            str = GeneratedConstants.INAPP_FLAVOR;
        }
        StringBuilder sb = new StringBuilder("SDK Flavor: [");
        sb.append(str);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return str;
    }

    public static String d() {
        String str = GeneratedConstants.INAPP_PACKAGING;
        return str.equals("${packaging}") ? "" : str;
    }

    public static boolean a(long j) {
        String str = AdsConstants.d;
        if (str.equals("${flavor}") || (j & new BigInteger(str, 2).longValue()) != 0) {
            return true;
        }
        return false;
    }

    public static boolean e() {
        return a(2) || a(16) || a(32) || a(4);
    }

    public static String a(Double d) {
        if (d == null) {
            return null;
        }
        return String.format(Locale.US, "%.2f", new Object[]{d});
    }

    public static boolean a(Context context) {
        if (AdsConstants.OVERRIDE_HOST != null || AdsConstants.OVERRIDE_NETWORK.booleanValue()) {
            return true;
        }
        if (c.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            try {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                    return false;
                }
                return true;
            } catch (Exception e) {
                f.a(context, d.EXCEPTION, "Util.isNetworkAvailable - system service failed", e.getMessage(), "");
                return false;
            }
        }
        return false;
    }

    public static String a(String str, String str2, String str3) {
        if (str == null || str2 == null || str3 == null) {
            return null;
        }
        int indexOf = str.indexOf(str2);
        if (indexOf != -1) {
            int indexOf2 = str.indexOf(str3, str2.length() + indexOf);
            if (indexOf2 != -1) {
                return str.substring(indexOf + str2.length(), indexOf2);
            }
        }
        return null;
    }

    public static String b(Context context) {
        if (context.getResources().getConfiguration().orientation == 2) {
            return "landscape";
        }
        return context.getResources().getConfiguration().orientation == 1 ? "portrait" : "undefined";
    }

    public static int a(Activity activity, int i, boolean z) {
        if (z) {
            if (!b.containsKey(activity)) {
                b.put(activity, Integer.valueOf(activity.getRequestedOrientation()));
            }
            if (i == activity.getResources().getConfiguration().orientation) {
                return c.a(activity, i, false);
            }
            return c.a(activity, i, true);
        }
        int i2 = -1;
        if (b.containsKey(activity)) {
            i2 = ((Integer) b.get(activity)).intValue();
            try {
                activity.setRequestedOrientation(i2);
            } catch (Exception unused) {
            }
            b.remove(activity);
        }
        return i2;
    }

    public static void a(Activity activity, boolean z) {
        a(activity, activity.getResources().getConfiguration().orientation, z);
    }

    private static List<Field> a(List<Field> list, Class<?> cls) {
        list.addAll(Arrays.asList(cls.getDeclaredFields()));
        if (cls.getSuperclass() != null) {
            a(list, cls.getSuperclass());
        }
        return list;
    }

    public static <T> boolean a(T t, T t2) {
        boolean z = false;
        try {
            for (Field field : a((List<Field>) new LinkedList<Field>(), t2.getClass())) {
                int modifiers = field.getModifiers();
                if (!Modifier.isTransient(modifiers) && !Modifier.isStatic(modifiers)) {
                    field.setAccessible(true);
                    if (field.get(t) == null) {
                        Object obj = field.get(t2);
                        if (obj != null) {
                            field.set(t, obj);
                            z = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            new StringBuilder("Util.mergeDefaultValues failed: ").append(e.getMessage());
        }
        return z;
    }

    public static void a(Context context, AdPreferences adPreferences) {
        String a2 = k.a(context, "shared_prefs_devId", (String) null);
        String a3 = k.a(context, "shared_prefs_appId", (String) null);
        if (adPreferences.getPublisherId() == null) {
            adPreferences.setPublisherId(a2);
        }
        if (adPreferences.getProductId() == null) {
            adPreferences.setProductId(a3);
        }
        if (adPreferences.getProductId() == null && !c) {
            c = true;
            Log.e("StartApp", "Integration Error - App ID is missing");
        }
    }

    public static void a(Context context, String str, String str2) {
        if (str != null) {
            k.b(context, "shared_prefs_devId", str.trim());
        } else {
            k.b(context, "shared_prefs_devId", (String) null);
        }
        k.b(context, "shared_prefs_appId", str2.trim());
    }

    public static void a(Context context, String str, final a aVar) {
        if ("true".equals(a(str, "@doNotRender@", "@doNotRender@"))) {
            aVar.a();
            return;
        }
        try {
            final WebView webView = new WebView(context);
            final Handler handler = new Handler();
            if (AdsConstants.OVERRIDE_NETWORK.booleanValue()) {
                f4014a = 25000;
                webView.getSettings().setBlockNetworkImage(false);
                webView.getSettings().setLoadsImagesAutomatically(true);
                webView.getSettings().setJavaScriptEnabled(true);
            } else {
                f4014a = 0;
            }
            webView.setWebChromeClient(new WebChromeClient());
            webView.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    super.onPageFinished(webView, str);
                    StringBuilder sb = new StringBuilder("onPageFinished url=[");
                    sb.append(str);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    handler.removeCallbacksAndMessages(null);
                    handler.postDelayed(new Runnable() {
                        public final void run() {
                            webView.destroy();
                            aVar.a();
                        }
                    }, (long) j.f4014a);
                }

                public final void onReceivedError(final WebView webView, int i, final String str, String str2) {
                    super.onReceivedError(webView, i, str, str2);
                    StringBuilder sb = new StringBuilder("onReceivedError failingUrl=[");
                    sb.append(str2);
                    sb.append("], description=[");
                    sb.append(str);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    handler.removeCallbacksAndMessages(null);
                    handler.post(new Runnable() {
                        public final void run() {
                            webView.destroy();
                            aVar.a(str);
                        }
                    });
                }

                public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    StringBuilder sb = new StringBuilder("shouldOverrideUrlLoading url=[");
                    sb.append(str);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    return super.shouldOverrideUrlLoading(webView, str);
                }
            });
            a(context, webView, str);
            handler.postDelayed(new Runnable() {
                public final void run() {
                    webView.destroy();
                    aVar.a();
                }
            }, 25000);
        } catch (Exception e) {
            f.a(context, d.EXCEPTION, "Util.loadHtmlToCacheWebView - webview failed", e.getMessage(), "");
            aVar.a("WebView instantiation Error");
        }
    }

    public static void a(Context context, WebView webView, String str) {
        try {
            webView.loadDataWithBaseURL(e.getInstance().getHostForWebview(), str, WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
        } catch (Exception e) {
            if (context != null) {
                f.a(context, d.EXCEPTION, "Util.loadDataToWebview failed", e.getMessage(), "");
            }
        }
    }

    public static String c(Context context) {
        String str = "";
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
            if (resolveActivity == null || resolveActivity.activityInfo == null) {
                return str;
            }
            String str2 = resolveActivity.activityInfo.packageName;
            if (str2 != null) {
                try {
                    return str2.toLowerCase();
                } catch (Exception unused) {
                }
            }
            return str2;
        } catch (Exception unused2) {
            return str;
        }
    }

    public static boolean a(AdPreferences adPreferences, String str) {
        Object a2 = a(adPreferences.getClass(), str, (Object) adPreferences);
        if (a2 == null || !(a2 instanceof Boolean)) {
            return false;
        }
        return ((Boolean) a2).booleanValue();
    }

    public static String b(AdPreferences adPreferences, String str) {
        Object a2 = a(adPreferences.getClass(), str, (Object) adPreferences);
        if (a2 == null || !(a2 instanceof String)) {
            return null;
        }
        return (String) a2;
    }

    public static void a(AdPreferences adPreferences, String str, boolean z) {
        a(adPreferences.getClass(), str, (Object) adPreferences, (Object) Boolean.valueOf(z));
    }

    public static Object a(Class cls, String str, Object obj) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField.get(obj);
        } catch (NoSuchFieldException e) {
            e.getLocalizedMessage();
            return null;
        } catch (IllegalArgumentException e2) {
            e2.getLocalizedMessage();
            return null;
        } catch (IllegalAccessException e3) {
            e3.getLocalizedMessage();
            return null;
        }
    }

    public static void a(Class cls, String str, Object obj, Object obj2) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            declaredField.set(obj, obj2);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        }
    }

    public static String d(Context context) {
        return context.getPackageManager().getInstallerPackageName(context.getPackageName());
    }

    public static void a(WebView webView, String str, Object... objArr) {
        a(webView, true, str, objArr);
    }

    public static void a(WebView webView, boolean z, String str, Object... objArr) {
        if (webView != null) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("(");
                if (objArr != null) {
                    for (int i = 0; i < objArr.length; i++) {
                        if (!z || !(objArr[i] instanceof String)) {
                            sb.append(objArr[i]);
                        } else {
                            sb.append("\"");
                            sb.append(objArr[i]);
                            sb.append("\"");
                        }
                        if (i < objArr.length - 1) {
                            sb.append(",");
                        }
                    }
                }
                sb.append(")");
                new StringBuilder("runJavascript: ").append(sb.toString());
                StringBuilder sb2 = new StringBuilder("javascript:");
                sb2.append(sb.toString());
                webView.loadUrl(sb2.toString());
            } catch (Exception e) {
                new StringBuilder("runJavascript Exception: ").append(e.getMessage());
            }
        }
    }

    public static Class<?> a(Context context, Class<? extends Activity> cls, Class<? extends Activity> cls2) {
        if (a(context, cls) || !a(context, cls2)) {
            return cls;
        }
        StringBuilder sb = new StringBuilder("Expected activity ");
        sb.append(cls.getName());
        sb.append(" is missing from AndroidManifest.xml");
        Log.w("StartAppWall.Util", sb.toString());
        return cls2;
    }

    public static boolean a(Context context, Class<? extends Activity> cls) {
        try {
            for (ActivityInfo activityInfo : context.getPackageManager().getPackageInfo(context.getPackageName(), 1).activities) {
                if (activityInfo.name.equals(cls.getName())) {
                    return true;
                }
            }
        } catch (Exception unused) {
        }
        return false;
    }

    public static boolean e(Context context) {
        for (RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (runningAppProcessInfo.importance == 100 && runningAppProcessInfo.processName.equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean f(Context context) {
        try {
            ActivityInfo[] activityInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 1).activities;
            boolean z = false;
            int i = 0;
            while (!z) {
                try {
                    if (i >= activityInfoArr.length) {
                        return z;
                    }
                    int i2 = i + 1;
                    ActivityInfo activityInfo = activityInfoArr[i];
                    if (activityInfo.name.equals("com.startapp.android.publish.AppWallActivity") || activityInfo.name.equals("com.startapp.android.publish.adsCommon.OverlayActivity") || activityInfo.name.equals("com.startapp.android.publish.FullScreenActivity")) {
                        z = (activityInfo.flags & 512) == 0;
                    }
                    i = i2;
                } catch (NameNotFoundException | Exception unused) {
                    return z;
                }
            }
            return z;
        } catch (NameNotFoundException | Exception unused2) {
            return false;
        }
    }

    public static String a(String str, Context context) {
        try {
            new c();
            return c.a(str, context);
        } catch (Exception e) {
            f.a(context, d.EXCEPTION, "Util.getApkHash - system service failed", e.getMessage(), "");
            return null;
        }
    }

    public static long a(File file) {
        return c.a(file);
    }

    public static String a(Context context, int i) {
        try {
            Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), i);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            decodeResource.compress(CompressFormat.PNG, 100, byteArrayOutputStream);
            return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 2);
        } catch (Exception unused) {
            return "";
        }
    }

    public static <T> T a(String str, Class<T> cls) {
        T a2 = b.a(str, cls);
        if (a2 != null) {
            return a2;
        }
        throw new com.startapp.common.e();
    }

    public static void a(Object obj) {
        new Handler(Looper.getMainLooper()).postAtTime(null, obj, SystemClock.uptimeMillis() + 1000);
    }

    public static int a(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    public static <T> boolean b(T t, T t2) {
        if (t == null) {
            return t2 == null;
        }
        return t.equals(t2);
    }

    public static void a(Editor editor) {
        if (VERSION.SDK_INT < 9) {
            editor.commit();
        } else {
            editor.apply();
        }
    }
}
