package com.startapp.android.publish.adsCommon;

import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public class AdsConstants {
    public static final int AD_INFORMATION_EXTENDED_ID = 1475346434;
    public static final int AD_INFORMATION_ID = 1475346433;
    public static final Boolean FORCE_NATIVE_VIDEO_PLAYER = Boolean.FALSE;
    public static final int LIST_3D_CLOSE_BUTTON_ID = 1475346435;
    public static final String OVERRIDE_HOST = null;
    public static final Boolean OVERRIDE_NETWORK = Boolean.FALSE;
    public static final int SPLASH_NATIVE_MAIN_LAYOUT_ID = 1475346437;
    public static final int STARTAPP_AD_MAIN_LAYOUT_ID = 1475346432;
    public static final Boolean VIDEO_DEBUG = Boolean.FALSE;

    /* renamed from: a reason: collision with root package name */
    public static final String f3998a = new String("https://imp.startappservice.com/tracking/adImpression");
    public static final Boolean b = Boolean.FALSE;
    public static final String c = j.b();
    public static final String d = j.c();
    public static final String e = j.d();
    public static final String[] f = {"back_", "back_dark", "browser_icon_dark", "forward_", "forward_dark", "x_dark"};
    public static final String[] g = {"empty_star", "filled_star", "half_star"};
    private static String h = new String("get");
    private static String i;
    private static String j;
    private static String k = new String("trackdownload");
    private static String l;

    /* compiled from: StartAppSDK */
    public enum AdApiType {
        HTML,
        JSON
    }

    /* compiled from: StartAppSDK */
    public enum ServiceApiType {
        METADATA,
        DOWNLOAD
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(h);
        sb.append(new String("ads"));
        i = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(h);
        sb2.append(new String("htmlad"));
        j = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(h);
        sb3.append(new String("adsmetadata"));
        l = sb3.toString();
    }

    public static String a(ServiceApiType serviceApiType) {
        String str;
        String str2 = null;
        switch (serviceApiType) {
            case METADATA:
                str2 = l;
                str = e.getInstance().getMetaDataHost();
                break;
            case DOWNLOAD:
                str2 = k;
                str = e.getInstance().getAdPlatformHost();
                break;
            default:
                str = null;
                break;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        return sb.toString();
    }

    public static String a(AdApiType adApiType, Placement placement) {
        String str;
        String str2 = null;
        switch (adApiType) {
            case HTML:
                str2 = j;
                str = e.getInstance().getAdPlatformHost(placement);
                break;
            case JSON:
                str2 = i;
                str = e.getInstance().getAdPlatformHost(placement);
                break;
            default:
                str = null;
                break;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        return sb.toString();
    }

    public static Boolean a() {
        return VIDEO_DEBUG;
    }
}
