package com.startapp.android.publish.adsCommon;

import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.common.c.e;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class n implements Serializable {
    private static final long serialVersionUID = 1;
    @e(b = a.class)
    private a backMode = a.DISABLED;
    private int maxCachedVideos = 3;
    private int maxTimeForCachingIndicator = 10;
    private int maxVastCampaignExclude = 10;
    private int maxVastLevels = 7;
    private long minAvailableStorageRequired = 20;
    private int minTimeForCachingIndicator = 1;
    private int nativePlayerProbability = 100;
    private boolean progressive = false;
    private int progressiveBufferInPercentage = 20;
    private int progressiveInitialBufferInPercentage = 5;
    private int progressiveMinBufferToPlayFromCache = 30;
    private int rewardGrantPercentage = 100;
    private String soundMode = "default";
    private int vastMediaPicker = 0;
    private int vastPreferredBitrate = 0;
    private int vastRetryTimeout = 60000;
    private int vastTimeout = 30000;
    private int videoErrorsThreshold = 2;
    private boolean videoFallback = false;

    /* compiled from: StartAppSDK */
    public enum a {
        DISABLED,
        SKIP,
        CLOSE,
        BOTH
    }

    public final a a() {
        return this.backMode;
    }

    public final int b() {
        return this.maxCachedVideos;
    }

    public final long c() {
        return this.minAvailableStorageRequired;
    }

    public final int d() {
        return this.rewardGrantPercentage;
    }

    public final int e() {
        return this.videoErrorsThreshold;
    }

    public final long f() {
        return TimeUnit.SECONDS.toMillis((long) this.minTimeForCachingIndicator);
    }

    public final long g() {
        return TimeUnit.SECONDS.toMillis((long) this.maxTimeForCachingIndicator);
    }

    public final boolean h() {
        return this.videoFallback;
    }

    public final boolean i() {
        return this.progressive;
    }

    public final int j() {
        return this.progressiveBufferInPercentage;
    }

    public final int k() {
        return this.progressiveInitialBufferInPercentage;
    }

    public final int l() {
        return this.progressiveMinBufferToPlayFromCache;
    }

    public final String m() {
        return this.soundMode;
    }

    public final int n() {
        return this.maxVastLevels;
    }

    public final int o() {
        return this.vastTimeout;
    }

    public final int p() {
        return this.vastRetryTimeout;
    }

    public final int q() {
        return this.maxVastCampaignExclude;
    }

    public final int r() {
        return this.vastMediaPicker;
    }

    public final int s() {
        return this.vastPreferredBitrate;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        n nVar = (n) obj;
        return this.maxCachedVideos == nVar.maxCachedVideos && this.minAvailableStorageRequired == nVar.minAvailableStorageRequired && this.rewardGrantPercentage == nVar.rewardGrantPercentage && this.videoErrorsThreshold == nVar.videoErrorsThreshold && this.nativePlayerProbability == nVar.nativePlayerProbability && this.minTimeForCachingIndicator == nVar.minTimeForCachingIndicator && this.maxTimeForCachingIndicator == nVar.maxTimeForCachingIndicator && this.videoFallback == nVar.videoFallback && this.progressive == nVar.progressive && this.progressiveBufferInPercentage == nVar.progressiveBufferInPercentage && this.progressiveInitialBufferInPercentage == nVar.progressiveInitialBufferInPercentage && this.progressiveMinBufferToPlayFromCache == nVar.progressiveMinBufferToPlayFromCache && this.maxVastLevels == nVar.maxVastLevels && this.vastTimeout == nVar.vastTimeout && this.vastRetryTimeout == nVar.vastRetryTimeout && this.maxVastCampaignExclude == nVar.maxVastCampaignExclude && this.vastMediaPicker == nVar.vastMediaPicker && this.vastPreferredBitrate == nVar.vastPreferredBitrate && this.backMode == nVar.backMode && j.b(this.soundMode, nVar.soundMode);
    }

    public final int hashCode() {
        return j.a(Integer.valueOf(this.maxCachedVideos), Long.valueOf(this.minAvailableStorageRequired), Integer.valueOf(this.rewardGrantPercentage), Integer.valueOf(this.videoErrorsThreshold), this.backMode, Integer.valueOf(this.nativePlayerProbability), Integer.valueOf(this.minTimeForCachingIndicator), Integer.valueOf(this.maxTimeForCachingIndicator), Boolean.valueOf(this.videoFallback), Boolean.valueOf(this.progressive), Integer.valueOf(this.progressiveBufferInPercentage), Integer.valueOf(this.progressiveInitialBufferInPercentage), Integer.valueOf(this.progressiveMinBufferToPlayFromCache), this.soundMode, Integer.valueOf(this.maxVastLevels), Integer.valueOf(this.vastTimeout), Integer.valueOf(this.vastRetryTimeout), Integer.valueOf(this.maxVastCampaignExclude), Integer.valueOf(this.vastMediaPicker), Integer.valueOf(this.vastPreferredBitrate));
    }
}
