package com.startapp.android.publish.adsCommon.g;

import android.content.Context;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.util.Pair;
import com.facebook.places.model.PlaceFields;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.a.c;
import com.startapp.android.publish.adsCommon.a.f;
import com.startapp.android.publish.adsCommon.d;
import com.startapp.android.publish.adsCommon.l;
import com.startapp.common.a.a;
import java.util.List;
import org.json.JSONArray;

/* compiled from: StartAppSDK */
public class e extends d {

    /* renamed from: a reason: collision with root package name */
    private d f4077a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private JSONArray i;

    public e(d dVar) {
        this(dVar, "", "");
    }

    public e(d dVar, String str, String str2) {
        this.f4077a = dVar;
        this.b = str;
        this.c = str2;
    }

    public f getNameValueJson() {
        f nameValueJson = super.getNameValueJson();
        if (nameValueJson == null) {
            nameValueJson = new c();
        }
        String d2 = a.d();
        nameValueJson.a(a.a(), d2, true);
        nameValueJson.a(a.b(), a.b(d2), true);
        nameValueJson.a("category", this.f4077a.a(), true);
        nameValueJson.a("value", this.b, false);
        nameValueJson.a(com.mintegral.msdk.base.b.d.b, this.d, false);
        nameValueJson.a("orientation", this.e, false);
        nameValueJson.a("usedRam", this.f, false);
        nameValueJson.a("freeRam", this.g, false);
        nameValueJson.a("sessionTime", null, false);
        nameValueJson.a("appActivity", null, false);
        nameValueJson.a("details", this.c, false);
        nameValueJson.a("details_json", this.i, false);
        nameValueJson.a("cellScanRes", this.h, false);
        Pair c2 = l.c();
        Pair d3 = l.d();
        nameValueJson.a((String) c2.first, c2.second, false);
        nameValueJson.a((String) d3.first, d3.second, false);
        return nameValueJson;
    }

    public final d a() {
        return this.f4077a;
    }

    public final void d(String str) {
        this.b = str;
    }

    public final void e(String str) {
        this.d = str;
    }

    public final void f(String str) {
        this.e = str;
    }

    public final void g(String str) {
        this.f = str;
    }

    public final void h(String str) {
        this.g = str;
    }

    public final void a(JSONArray jSONArray) {
        this.i = jSONArray;
    }

    /* access modifiers changed from: 0000 */
    public final void a(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                List list = null;
                if (context != null) {
                    if (telephonyManager != null) {
                        if ((com.startapp.common.a.c.a(context, "android.permission.ACCESS_FINE_LOCATION") || com.startapp.common.a.c.a(context, "android.permission.ACCESS_COARSE_LOCATION")) && VERSION.SDK_INT >= 17) {
                            list = telephonyManager.getAllCellInfo();
                        }
                    }
                }
                if (list != null && list.size() > 0) {
                    this.h = a.c(list.toString());
                }
            }
        } catch (Exception e2) {
            new StringBuilder("Cannot fillCellDetails ").append(e2.getMessage());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("InfoEventRequest [category=");
        sb.append(this.f4077a.a());
        sb.append(", value=");
        sb.append(this.b);
        sb.append(", details=");
        sb.append(this.c);
        sb.append(", d=");
        sb.append(this.d);
        sb.append(", orientation=");
        sb.append(this.e);
        sb.append(", usedRam=");
        sb.append(this.f);
        sb.append(", freeRam=");
        sb.append(this.g);
        sb.append(", sessionTime=");
        sb.append(null);
        sb.append(", appActivity=");
        sb.append(null);
        sb.append(", details_json=");
        sb.append(this.i);
        sb.append(", cellScanRes=");
        sb.append(this.h);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
