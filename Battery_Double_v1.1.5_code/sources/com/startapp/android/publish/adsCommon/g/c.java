package com.startapp.android.publish.adsCommon.g;

import android.content.Context;
import com.startapp.android.publish.adsCommon.i.a;
import com.startapp.android.publish.adsCommon.i.b;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.common.d;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: StartAppSDK */
public final class c {
    static AtomicBoolean d = new AtomicBoolean(false);

    /* renamed from: a reason: collision with root package name */
    Context f4074a;
    int b;
    b c;
    private d e;
    private ArrayList<a> f;
    private Runnable g;

    public c(Context context) {
        this(context, false, null);
    }

    public c(Context context, boolean z, d dVar) {
        this.g = new Runnable() {
            public final void run() {
                synchronized (this) {
                    c cVar = c.this;
                    int i = cVar.b - 1;
                    cVar.b = i;
                    if (i == 0) {
                        f.a(c.this.f4074a, c.this.c, "", null);
                        c.d.set(false);
                        c.this.b();
                    }
                }
            }
        };
        this.f4074a = context;
        this.e = dVar;
        this.f = new ArrayList<>();
        this.c = new b(d.PERIODIC);
        this.c.a(z);
        if (e.getInstance().getSensorsConfig().b()) {
            this.f.add(new com.startapp.android.publish.adsCommon.i.c(context, this.g, this.c));
        }
        if (e.getInstance().getBluetoothConfig().b()) {
            this.f.add(new b(context, this.g, this.c));
        }
        this.b = this.f.size();
    }

    public final void a() {
        if (this.b > 0) {
            if (d.compareAndSet(false, true)) {
                for (int i = 0; i < this.b; i++) {
                    ((a) this.f.get(i)).a();
                }
                return;
            }
        }
        b();
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        if (this.e != null) {
            this.e.a(null);
        }
    }

    public final b c() {
        return this.c;
    }
}
