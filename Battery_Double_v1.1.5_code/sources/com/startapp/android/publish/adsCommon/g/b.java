package com.startapp.android.publish.adsCommon.g;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.a.c;
import com.startapp.android.publish.adsCommon.a.f;
import com.startapp.common.a.a;

/* compiled from: StartAppSDK */
public final class b extends e {

    /* renamed from: a reason: collision with root package name */
    private String f4073a;
    private String b;
    private boolean c;
    private String d;

    public b(d dVar) {
        super(dVar);
    }

    public final f getNameValueJson() {
        f nameValueJson = super.getNameValueJson();
        if (nameValueJson == null) {
            nameValueJson = new c();
        }
        nameValueJson.a("sens", this.f4073a, false);
        nameValueJson.a("bt", this.b, false);
        nameValueJson.a("isService", Boolean.valueOf(this.c), false);
        nameValueJson.a("packagingType", this.d, false);
        return nameValueJson;
    }

    public final void a(String str) {
        if (str != null) {
            this.f4073a = a.c(str);
        }
    }

    public final void b(String str) {
        if (str != null) {
            this.b = a.c(str);
        }
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(" DataEventRequest [sensors=");
        sb.append(this.f4073a);
        sb.append(", bluetooth=");
        sb.append(this.b);
        sb.append(", isService=");
        sb.append(this.c);
        sb.append(", packagingType=");
        sb.append(this.d);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
