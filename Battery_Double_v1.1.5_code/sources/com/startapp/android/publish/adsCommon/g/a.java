package com.startapp.android.publish.adsCommon.g;

import android.support.annotation.VisibleForTesting;
import com.startapp.android.publish.adsCommon.a.j;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class a implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private static final String f4072a = new String("https://infoevent.startappservice.com/tracking/infoEvent");
    private static final long serialVersionUID = 1;
    @VisibleForTesting
    public boolean dns = false;
    @VisibleForTesting
    public String hostPeriodic = f4072a;
    @VisibleForTesting
    public String hostSecured = f4072a;
    private int retryNum = 3;
    private int retryTime = 10;
    private boolean sendHopsOnFirstSucceededSmartRedirect = false;
    private float succeededSmartRedirectInfoProbability = 0.01f;

    public final String a() {
        return this.hostPeriodic != null ? this.hostPeriodic : f4072a;
    }

    public final int b() {
        return this.retryNum;
    }

    public final long c() {
        return TimeUnit.SECONDS.toMillis((long) this.retryTime);
    }

    public final float d() {
        return this.succeededSmartRedirectInfoProbability;
    }

    public final boolean e() {
        return this.sendHopsOnFirstSucceededSmartRedirect;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return this.dns == aVar.dns && this.retryNum == aVar.retryNum && this.retryTime == aVar.retryTime && Float.compare(aVar.succeededSmartRedirectInfoProbability, this.succeededSmartRedirectInfoProbability) == 0 && this.sendHopsOnFirstSucceededSmartRedirect == aVar.sendHopsOnFirstSucceededSmartRedirect && j.b(this.hostSecured, aVar.hostSecured) && j.b(this.hostPeriodic, aVar.hostPeriodic);
    }

    public final int hashCode() {
        return j.a(this.hostSecured, this.hostPeriodic, Boolean.valueOf(this.dns), Integer.valueOf(this.retryNum), Integer.valueOf(this.retryTime), Float.valueOf(this.succeededSmartRedirectInfoProbability), Boolean.valueOf(this.sendHopsOnFirstSucceededSmartRedirect));
    }
}
