package com.startapp.android.publish.adsCommon.g;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.support.v4.media.session.PlaybackStateCompat;
import com.b.a.a.a.d.b;
import com.b.a.a.a.d.c;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.g.a;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;

/* compiled from: StartAppSDK */
public final class f {

    /* renamed from: a reason: collision with root package name */
    private final b f4078a = new b(new c());

    public static void a(Context context, d dVar, String str, String str2, String str3) {
        a(context, new e(dVar, str, str2), str3, null);
    }

    public static void a(Context context, e eVar, String str) {
        a(context, eVar, str, null);
    }

    public static void a(Context context, e eVar, String str, a aVar) {
        if (!e.getInstance().getAnalyticsConfig().dns) {
            eVar.e(str);
            eVar.a(context);
            try {
                eVar.f(j.b(context));
                MemoryInfo memoryInfo = new MemoryInfo();
                ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
                eVar.h(Long.toString(memoryInfo.availMem / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED));
                Long a2 = com.startapp.common.a.c.a(memoryInfo);
                if (a2 != null) {
                    eVar.g(Long.toString((a2.longValue() - memoryInfo.availMem) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED));
                }
            } catch (Throwable unused) {
            }
            new StringBuilder("Sending ").append(eVar);
            new g(context, new AdPreferences(), eVar, aVar).a();
        }
    }

    public final com.b.a.a.a.d.a a() {
        return this.f4078a;
    }
}
