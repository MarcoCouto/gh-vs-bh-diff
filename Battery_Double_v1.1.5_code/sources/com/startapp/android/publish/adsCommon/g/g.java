package com.startapp.android.publish.adsCommon.g;

import android.content.Context;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.l;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.common.f;

/* compiled from: StartAppSDK */
public final class g {

    /* renamed from: a reason: collision with root package name */
    private final Context f4079a;
    private final AdPreferences b;
    private final e c;
    /* access modifiers changed from: private */
    public final a d;

    /* compiled from: StartAppSDK */
    public interface a {
        void a();
    }

    public g(Context context, AdPreferences adPreferences, e eVar, a aVar) {
        this.f4079a = context;
        this.b = adPreferences;
        this.c = eVar;
        this.d = aVar;
    }

    public final void a() {
        f.a(com.startapp.common.f.a.DEFAULT, (Runnable) new Runnable() {
            public final void run() {
                g.this.b();
                if (g.this.d != null) {
                    g.this.d.a();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(10:1|2|3|4|5|6|(1:8)|9|10|11) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0071, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x002b */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0043 A[Catch:{ e -> 0x0071 }] */
    public final boolean b() {
        new StringBuilder("Sending InfoEvent ").append(this.c);
        try {
            j.a(this.f4079a, this.b);
            l.b(this.f4079a);
            this.c.fillLocationDetails(this.b, this.f4079a);
            this.c.fillApplicationDetails(this.f4079a, this.b);
            String str = e.getInstance().getAnalyticsConfig().hostSecured;
            if (d.PERIODIC.equals(this.c.a())) {
                str = e.getInstance().getAnalyticsConfig().a();
            }
            com.startapp.android.publish.adsCommon.l.a.a(this.f4079a, str, this.c, e.getInstance().getAnalyticsConfig().b(), e.getInstance().getAnalyticsConfig().c());
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
