package com.startapp.android.publish.adsCommon.f;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.at;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.e;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.common.b.a.b;
import com.startapp.common.b.a.b.C0095b;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/* compiled from: StartAppSDK */
public final class a implements UncaughtExceptionHandler {

    /* renamed from: a reason: collision with root package name */
    private final UncaughtExceptionHandler f4069a = Thread.getDefaultUncaughtExceptionHandler();

    /* renamed from: com.startapp.android.publish.adsCommon.f.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public static final class C0088a implements com.startapp.common.b.a.a {
        public final b create(int i) {
            if (i != 868418937) {
                return null;
            }
            return new b() {
                public final void a(Context context, int i, Map<String, String> map, final C0095b bVar) {
                    String str = (String) map.get("KEY_STACK_TRACE");
                    if (!TextUtils.isEmpty(str)) {
                        if (str.length() > 1000) {
                            str = str.substring(0, 1000);
                        }
                        AnonymousClass1 r2 = new com.startapp.android.publish.adsCommon.g.g.a() {
                            public final void a() {
                                bVar.a(com.startapp.common.b.a.b.a.SUCCESS);
                            }
                        };
                        f.a(context, new e(d.EXCEPTION, "uncaughtException", str), "", r2);
                    }
                }
            };
        }
    }

    public static synchronized void a(Context context) {
        synchronized (a.class) {
            new a(context);
        }
    }

    private a(Context context) {
        Thread.setDefaultUncaughtExceptionHandler(this);
        com.startapp.common.b.a.a(context);
        com.startapp.common.b.a.a((com.startapp.common.b.a.a) new C0088a());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005f, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001a, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r1 = b(r6);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x001c */
    public final void uncaughtException(@Nullable Thread thread, @NonNull Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        a(th, new PrintStream(byteArrayOutputStream));
        String b = new String(byteArrayOutputStream.toByteArray());
        try {
            byteArrayOutputStream.close();
        } catch (IOException unused) {
        }
        if (!TextUtils.isEmpty(b) && (b.contains("startapp") || b.contains("truenet"))) {
            com.startapp.common.b.a.a(new com.startapp.common.b.b.a(868418937).a(1000).a("KEY_STACK_TRACE", a(b)).b());
        }
        this.f4069a.uncaughtException(thread, th);
    }

    private static String a(String str) {
        char[] charArray;
        StringBuilder sb = new StringBuilder(str.length());
        for (char c : str.toCharArray()) {
            if (c >= ' ' && c <= '~') {
                sb.append(c);
            } else if (sb.length() == 0 || sb.charAt(sb.length() - 1) != '#') {
                sb.append('#');
            }
        }
        return sb.toString();
    }

    private static List<String> a(Throwable th) {
        String str;
        String b = b(th);
        try {
            str = System.getProperty("line.separator");
        } catch (Exception unused) {
            str = "\r\n";
        }
        StringTokenizer stringTokenizer = new StringTokenizer(b, str);
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            int indexOf = nextToken.indexOf(at.f1547a);
            if (indexOf != -1 && nextToken.substring(0, indexOf).trim().length() == 0) {
                z = true;
                arrayList.add(nextToken);
            } else if (z) {
                break;
            }
        }
        return arrayList;
    }

    private static String b(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter, true));
        return stringWriter.getBuffer().toString();
    }

    private static void a(List<String> list, List<String> list2) {
        if (list == null || list2 == null) {
            throw new IllegalArgumentException("The List must not be null");
        }
        int size = list.size() - 1;
        int size2 = list2.size() - 1;
        while (size >= 0 && size2 >= 0) {
            if (((String) list.get(size)).equals((String) list2.get(size2))) {
                list.remove(size);
            }
            size--;
            size2--;
        }
    }

    private static void a(Throwable th, PrintStream printStream) {
        List list;
        if (th != null) {
            ArrayList arrayList = new ArrayList();
            while (th != null && !arrayList.contains(th)) {
                arrayList.add(th);
                th = th.getCause();
            }
            Throwable[] thArr = (Throwable[]) arrayList.toArray(new Throwable[0]);
            int length = thArr.length;
            ArrayList arrayList2 = new ArrayList();
            int i = length - 1;
            List a2 = a(thArr[i]);
            while (true) {
                length--;
                if (length < 0) {
                    break;
                }
                if (length != 0) {
                    list = a(thArr[length - 1]);
                    a(a2, list);
                } else {
                    list = a2;
                }
                if (length == i) {
                    arrayList2.add(thArr[length].toString());
                } else {
                    StringBuilder sb = new StringBuilder(" [wrapped] ");
                    sb.append(thArr[length].toString());
                    arrayList2.add(sb.toString());
                }
                arrayList2.addAll(a2);
                a2 = list;
            }
            for (String println : (String[]) arrayList2.toArray(new String[0])) {
                printStream.println(println);
            }
            printStream.flush();
        }
    }
}
