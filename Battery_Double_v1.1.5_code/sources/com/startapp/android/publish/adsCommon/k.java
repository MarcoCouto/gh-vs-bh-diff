package com.startapp.android.publish.adsCommon;

import android.content.Context;
import android.content.SharedPreferences;
import com.startapp.android.publish.adsCommon.a.j;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class k {

    /* renamed from: a reason: collision with root package name */
    private static SharedPreferences f4096a;

    public static SharedPreferences a(Context context) {
        if (f4096a == null) {
            f4096a = context.getApplicationContext().getSharedPreferences("com.startapp.android.publish", 0);
        }
        return f4096a;
    }

    public static Boolean a(Context context, String str, Boolean bool) {
        return Boolean.valueOf(a(context).getBoolean(str, bool.booleanValue()));
    }

    public static void b(Context context, String str, Boolean bool) {
        j.a(a(context).edit().putBoolean(str, bool.booleanValue()));
    }

    public static String a(Context context, String str, String str2) {
        return a(context).getString(str, str2);
    }

    public static void b(Context context, String str, String str2) {
        j.a(a(context).edit().putString(str, str2));
    }

    public static Integer a(Context context, String str, Integer num) {
        return Integer.valueOf(a(context).getInt(str, num.intValue()));
    }

    public static void b(Context context, String str, Integer num) {
        j.a(a(context).edit().putInt(str, num.intValue()));
    }

    public static Float a(Context context, String str, Float f) {
        return Float.valueOf(a(context).getFloat(str, f.floatValue()));
    }

    public static void b(Context context, String str, Float f) {
        j.a(a(context).edit().putFloat(str, f.floatValue()));
    }

    public static Long a(Context context, String str, Long l) {
        return Long.valueOf(a(context).getLong(str, l.longValue()));
    }

    public static void b(Context context, String str, Long l) {
        j.a(a(context).edit().putLong(str, l.longValue()));
    }

    public static void a(Context context, String str, Map<String, String> map) {
        b(context, str, new JSONObject(map).toString());
    }

    public static Map<String, String> b(Context context, String str, Map<String, String> map) {
        try {
            JSONObject jSONObject = new JSONObject(a(context).getString(str, null));
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str2 = (String) keys.next();
                map.put(str2, (String) jSONObject.get(str2));
            }
            return map;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Throwable unused) {
        }
        return map;
    }
}
