package com.startapp.android.publish.adsCommon;

import android.content.Context;
import com.startapp.android.publish.adsCommon.a.d;
import com.startapp.android.publish.adsCommon.a.f;
import com.startapp.android.publish.adsCommon.j.a;
import com.startapp.android.publish.adsCommon.j.b;
import com.startapp.common.a.c;

/* compiled from: StartAppSDK */
public final class j extends d {

    /* renamed from: a reason: collision with root package name */
    private b f4092a;
    private String b;

    public j(Context context) {
        this.f4092a = a.a(context);
        this.b = c.i(context);
    }

    public final f getNameValueMap() {
        f nameValueMap = super.getNameValueMap();
        if (nameValueMap == null) {
            nameValueMap = new d();
        }
        nameValueMap.a("placement", "INAPP_DOWNLOAD", true);
        if (this.f4092a != null) {
            nameValueMap.a("install_referrer", this.f4092a.a(), true);
            nameValueMap.a("referrer_click_timestamp_seconds", Long.valueOf(this.f4092a.b()), true);
            nameValueMap.a("install_begin_timestamp_seconds", Long.valueOf(this.f4092a.c()), true);
        }
        nameValueMap.a("apkSig", this.b, true);
        return nameValueMap;
    }
}
