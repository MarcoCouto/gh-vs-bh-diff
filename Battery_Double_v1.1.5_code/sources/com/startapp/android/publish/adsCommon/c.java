package com.startapp.android.publish.adsCommon;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.customtabs.CustomTabsIntent;
import android.text.TextUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.constants.LocationConst;
import com.smaato.sdk.core.api.VideoType;
import com.startapp.android.publish.adsCommon.Ad.AdType;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.activities.OverlayActivity;
import com.startapp.android.publish.adsCommon.e.b;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.e;
import com.startapp.android.publish.common.model.AdDetails;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.Constants;
import com.startapp.common.f;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class c {

    /* renamed from: a reason: collision with root package name */
    private static ProgressDialog f4050a;
    private final String b;
    private final URL c;
    private final String d;

    /* compiled from: StartAppSDK */
    static class a extends WebViewClient {

        /* renamed from: a reason: collision with root package name */
        protected String f4054a = "";
        protected String b;
        protected boolean c = false;
        protected boolean d = true;
        protected Runnable e;
        protected boolean f = false;
        protected boolean g = false;
        private boolean h = false;
        private long i;
        private long j;
        private Boolean k = null;
        private String l;
        private ProgressDialog m;
        private LinkedHashMap<String, Float> n = new LinkedHashMap<>();
        private long o;
        private Timer p;

        public a(long j2, long j3, boolean z, Boolean bool, ProgressDialog progressDialog, String str, String str2, String str3, Runnable runnable) {
            this.i = j2;
            this.j = j3;
            this.d = z;
            this.k = bool;
            this.m = progressDialog;
            this.f4054a = str;
            this.l = str2;
            this.b = str3;
            this.e = runnable;
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            StringBuilder sb = new StringBuilder("MyWebViewClientSmartRedirect::onPageStarted - [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            super.onPageStarted(webView, str, bitmap);
            if (!this.h) {
                this.o = System.currentTimeMillis();
                this.n.put(str, Float.valueOf(-1.0f));
                final Context context = webView.getContext();
                f.a((Runnable) new Runnable() {
                    /* JADX WARNING: Failed to process nested try/catch */
                    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0030 */
                    /* JADX WARNING: Removed duplicated region for block: B:17:0x0070 A[Catch:{ Exception -> 0x0078 }] */
                    public final void run() {
                        if (!a.this.c) {
                            e eVar = new e(d.FAILED_SMART_REDIRECT_HOP_INFO);
                            eVar.a(a.this.a());
                            if (a.this.g) {
                                eVar.d("Page Finished");
                            } else {
                                eVar.d("Timeout");
                            }
                            com.startapp.android.publish.adsCommon.g.f.a(context, eVar, a.this.b);
                            try {
                                a.this.f = true;
                                c.a(context);
                                a.this.b();
                                if (a.this.d || !com.startapp.android.publish.common.metaData.e.getInstance().isInAppBrowser()) {
                                    c.a(context, a.this.f4054a, a.this.b);
                                } else {
                                    c.b(context, a.this.f4054a, a.this.b);
                                }
                                if (a.this.e != null) {
                                    a.this.e.run();
                                }
                            } catch (Exception e) {
                                com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "AdsCommonUtils.startTimeout - error after time elapsed", e.getMessage(), a.this.b);
                            }
                        }
                    }
                }, this.i);
                this.h = true;
            }
            this.g = false;
            b();
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            StringBuilder sb = new StringBuilder("MyWebViewClientSmartRedirect::shouldOverrideUrlLoading - [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            try {
                long currentTimeMillis = System.currentTimeMillis();
                Float valueOf = Float.valueOf(((float) (currentTimeMillis - this.o)) / 1000.0f);
                this.o = currentTimeMillis;
                this.n.put(this.f4054a, valueOf);
                this.n.put(str, Float.valueOf(-1.0f));
                this.f4054a = str;
                String lowerCase = str.toLowerCase();
                boolean z = false;
                if (!c.b(lowerCase) && !lowerCase.startsWith("intent://")) {
                    return false;
                }
                if (!this.f) {
                    this.c = true;
                    c.a(webView.getContext());
                    b();
                    Context context = webView.getContext();
                    if (lowerCase.startsWith("intent://")) {
                        str = webView.getUrl();
                    }
                    c.c(context, str);
                    if (this.l == null || this.l.equals("") || this.f4054a.toLowerCase().contains(this.l.toLowerCase())) {
                        if (com.startapp.android.publish.common.metaData.e.getInstance().getAnalyticsConfig().e() && k.a(webView.getContext(), "firstSucceededSmartRedirect", Boolean.TRUE).booleanValue()) {
                            z = true;
                        }
                        float f2 = this.k == null ? com.startapp.android.publish.common.metaData.e.getInstance().getAnalyticsConfig().d() : this.k.booleanValue() ? 100.0f : 0.0f;
                        if (z || Math.random() * 100.0d < ((double) f2)) {
                            e eVar = new e(d.SUCCESS_SMART_REDIRECT_HOP_INFO);
                            eVar.a(a());
                            com.startapp.android.publish.adsCommon.g.f.a(webView.getContext(), eVar, this.b);
                            k.b(webView.getContext(), "firstSucceededSmartRedirect", Boolean.FALSE);
                        }
                    } else {
                        StringBuilder sb2 = new StringBuilder("Expected: ");
                        sb2.append(this.l);
                        sb2.append(" Link: ");
                        sb2.append(this.f4054a);
                        com.startapp.android.publish.adsCommon.g.f.a(webView.getContext(), d.WRONG_PACKAGE_REACHED, "Wrong package name reached", sb2.toString(), this.b);
                    }
                    if (this.e != null) {
                        this.e.run();
                    }
                }
                return true;
            } catch (Exception unused) {
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            StringBuilder sb = new StringBuilder("MyWebViewClientSmartRedirect::onPageFinished - [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            if (!this.c && !this.f && this.f4054a.equals(str) && str != null && !c.b(str) && (str.startsWith("http://") || str.startsWith("https://"))) {
                this.g = true;
                try {
                    a(str);
                } catch (Exception unused) {
                }
                final Context context = webView.getContext();
                b();
                try {
                    this.p = new Timer();
                    this.p.schedule(new TimerTask() {
                        public final void run() {
                            if (!a.this.f && !a.this.c) {
                                try {
                                    a.this.c = true;
                                    c.a(context);
                                    if (!a.this.d || !com.startapp.android.publish.common.metaData.e.getInstance().isInAppBrowser()) {
                                        c.a(context, a.this.f4054a, a.this.b);
                                    } else {
                                        c.b(context, a.this.f4054a, a.this.b);
                                    }
                                    if (a.this.e != null) {
                                        a.this.e.run();
                                    }
                                } catch (Exception e) {
                                    com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "AdsCommonUtils.startLoadedTimer - error after time elapsed", e.getMessage(), a.this.b);
                                }
                            }
                        }
                    }, this.j);
                } catch (Exception unused2) {
                    this.p = null;
                }
            }
            super.onPageFinished(webView, str);
        }

        public final void onReceivedError(WebView webView, int i2, String str, String str2) {
            StringBuilder sb = new StringBuilder("MyWebViewClientSmartRedirect::onReceivedError - [");
            sb.append(str);
            sb.append("], [");
            sb.append(str2);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            b();
            if (str2 != null && !c.b(str2) && c.c(str2)) {
                com.startapp.android.publish.adsCommon.g.f.a(webView.getContext(), d.FAILED_SMART_REDIRECT, Integer.toString(i2), str2, this.b);
            }
            super.onReceivedError(webView, i2, str, str2);
        }

        /* access modifiers changed from: private */
        public void b() {
            if (this.p != null) {
                this.p.cancel();
                this.p = null;
            }
        }

        private void a(String str) {
            if (((Float) this.n.get(str)).floatValue() < 0.0f) {
                this.n.put(str, Float.valueOf(((float) (System.currentTimeMillis() - this.o)) / 1000.0f));
            }
        }

        /* access modifiers changed from: protected */
        public final JSONArray a() {
            JSONArray jSONArray = new JSONArray();
            for (String str : this.n.keySet()) {
                JSONObject jSONObject = new JSONObject();
                try {
                    a(str);
                    jSONObject.put(LocationConst.TIME, ((Float) this.n.get(str)).toString());
                    jSONObject.put("url", str);
                    jSONArray.put(jSONObject);
                } catch (JSONException unused) {
                    StringBuilder sb = new StringBuilder("error puting url into json [");
                    sb.append(str);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                }
            }
            return jSONArray;
        }
    }

    public static void a(AdPreferences adPreferences, String str, AdType adType) {
        j.a(adPreferences.getClass(), str, (Object) adPreferences, (Object) adType);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.String, code=java.lang.Object, for r4v0, types: [java.lang.Object, java.lang.String] */
    public static String a(Context context, Object obj) {
        ApplicationInfo applicationInfo;
        try {
            return context.getResources().getString(context.getApplicationInfo().labelRes);
        } catch (NotFoundException unused) {
            PackageManager packageManager = context.getPackageManager();
            try {
                applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
            } catch (NameNotFoundException unused2) {
                applicationInfo = null;
            }
            if (applicationInfo != null) {
                obj = packageManager.getApplicationLabel(applicationInfo);
            }
            return (String) obj;
        }
    }

    public static boolean a(Activity activity) {
        boolean z = activity.getTheme().obtainStyledAttributes(new int[]{16843277}).getBoolean(0, false);
        if ((activity.getWindow().getAttributes().flags & 1024) != 0) {
            return true;
        }
        return z;
    }

    public static int a(String str) {
        String[] split = str.split(RequestParameters.AMPERSAND);
        return Integer.parseInt(split[split.length - 1].split(RequestParameters.EQUAL)[1]);
    }

    public static void a(Context context, String[] strArr, String str, String str2) {
        a(context, strArr, str, 0, str2);
    }

    public static void a(Context context, String[] strArr, String str, int i, String str2) {
        b nonImpressionReason = new b(str).setOffset(i).setNonImpressionReason(str2);
        if (strArr == null || strArr.length == 0) {
            com.startapp.android.publish.adsCommon.g.f.a(context, d.NON_IMPRESSION_NO_DPARAM, str2, nonImpressionReason.getProfileId(), "");
            return;
        }
        for (String str3 : strArr) {
            if (str3 != null && !str3.equalsIgnoreCase("")) {
                StringBuilder sb = new StringBuilder("Sending Impression: [");
                sb.append(str3);
                sb.append(RequestParameters.RIGHT_BRACKETS);
                a(context, str3, nonImpressionReason, false);
            }
        }
    }

    public static void a(Context context, String str, b bVar) {
        if (str != null && !str.equalsIgnoreCase("")) {
            StringBuilder sb = new StringBuilder("Sending Impression: [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            if (bVar != null) {
                bVar.setLocation(context);
            }
            a(context, str, bVar, true);
        }
    }

    public static void a(Context context, String[] strArr, b bVar) {
        if (strArr != null) {
            for (String a2 : strArr) {
                a(context, a2, bVar);
            }
        }
    }

    public static List<String> a(List<String> list, String str, String str2) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i < list.size()) {
            int i2 = i + 5;
            List subList = list.subList(i, Math.min(i2, list.size()));
            StringBuilder sb = new StringBuilder();
            sb.append(AdsConstants.f3998a);
            sb.append("?");
            sb.append(TextUtils.join(RequestParameters.AMPERSAND, subList));
            sb.append("&isShown=");
            sb.append(str);
            sb.append("&appPresence=".concat(String.valueOf(str2)));
            arrayList.add(sb.toString());
            i = i2;
        }
        new StringBuilder("newUrlList size = ").append(arrayList.size());
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0059 A[Catch:{ Exception -> 0x00c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005e A[Catch:{ Exception -> 0x00c6 }] */
    public static final void a(Context context, String str, String str2, b bVar, boolean z, boolean z2) {
        String str3;
        String str4;
        String str5;
        String str6;
        if (!TextUtils.isEmpty(str2)) {
            a(context, str2, bVar, true);
        }
        a.f4110a.b();
        if (!z2) {
            try {
                str3 = a(str, str2);
            } catch (Exception e) {
                d dVar = d.FAILED_EXTRACTING_DPARAMS;
                StringBuilder sb = new StringBuilder("Util.clickWithoutSmartRedirect(): Couldn't extract dparams with clickUrl ");
                sb.append(str);
                sb.append(" and tacking click url ");
                sb.append(str2);
                com.startapp.android.publish.adsCommon.g.f.a(context, dVar, sb.toString(), e.getMessage(), null);
                StringBuilder sb2 = new StringBuilder("Cannot start activity to handle url: [");
                sb2.append(str);
                sb2.append(RequestParameters.RIGHT_BRACKETS);
            }
            str4 = "InAppBrowser";
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(!d(str2) ? com.startapp.common.a.a.a(str3) : "");
            str6 = sb3.toString();
            try {
                if (com.startapp.android.publish.common.metaData.e.getInstance().isInAppBrowser() || !z) {
                    str5 = "externalBrowser";
                    if (TextUtils.isEmpty(str2) || !d(context)) {
                        a(context, str6, (String) null);
                    }
                    k.b(context, "shared_prefs_CookieFeatureTS", Long.valueOf(System.currentTimeMillis()));
                    new StringBuilder("forceExternal - write to sp - TS : ").append(SimpleDateFormat.getDateInstance().format(new Date()));
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str6);
                    sb4.append("&cki=1");
                    a(context, sb4.toString(), (String) null);
                    return;
                }
                b(context, str6, str3);
                return;
            } catch (Exception e2) {
                e = e2;
                str5 = str4;
                com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.clickWithoutSmartRedirect - Couldn't start activity for ".concat(String.valueOf(str5)), e.getMessage(), str3);
                StringBuilder sb5 = new StringBuilder("Cannot start activity to handle url: [");
                sb5.append(str6);
                sb5.append(RequestParameters.RIGHT_BRACKETS);
            }
        }
        str3 = null;
        str4 = "InAppBrowser";
        try {
            StringBuilder sb32 = new StringBuilder();
            sb32.append(str);
            sb32.append(!d(str2) ? com.startapp.common.a.a.a(str3) : "");
            str6 = sb32.toString();
            if (com.startapp.android.publish.common.metaData.e.getInstance().isInAppBrowser()) {
            }
            str5 = "externalBrowser";
            try {
                if (TextUtils.isEmpty(str2)) {
                }
                a(context, str6, (String) null);
            } catch (Exception e3) {
                e = e3;
                com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.clickWithoutSmartRedirect - Couldn't start activity for ".concat(String.valueOf(str5)), e.getMessage(), str3);
                StringBuilder sb52 = new StringBuilder("Cannot start activity to handle url: [");
                sb52.append(str6);
                sb52.append(RequestParameters.RIGHT_BRACKETS);
            }
        } catch (Exception e4) {
            e = e4;
            str6 = str;
            str5 = str4;
            com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.clickWithoutSmartRedirect - Couldn't start activity for ".concat(String.valueOf(str5)), e.getMessage(), str3);
            StringBuilder sb522 = new StringBuilder("Cannot start activity to handle url: [");
            sb522.append(str6);
            sb522.append(RequestParameters.RIGHT_BRACKETS);
        }
    }

    private static boolean d(String str) {
        return b.a().D() || TextUtils.isEmpty(str);
    }

    public static final void a(Context context, String str, String str2, String str3, b bVar, long j, long j2, boolean z, Boolean bool) {
        a(context, str, str2, str3, bVar, j, j2, z, bool, false, null);
    }

    public static final void a(Context context, String str, String str2, String str3, b bVar, long j, long j2, boolean z, Boolean bool, boolean z2, Runnable runnable) {
        Context context2 = context;
        String str4 = str;
        String str5 = str2;
        if (b.a().C()) {
            a.f4110a.b();
            String str6 = null;
            if (!z2) {
                try {
                    str6 = a(str, str2);
                } catch (Exception e) {
                    Exception exc = e;
                    d dVar = d.FAILED_EXTRACTING_DPARAMS;
                    StringBuilder sb = new StringBuilder("Util.clickWithSmartRedirect(): Couldn't extract dparams with clickUrl ");
                    sb.append(str);
                    sb.append(" and tacking click url ");
                    sb.append(str2);
                    com.startapp.android.publish.adsCommon.g.f.a(context, dVar, sb.toString(), exc.getMessage(), null);
                    StringBuilder sb2 = new StringBuilder("Cannot start activity to handle url: [");
                    sb2.append(str);
                    sb2.append(RequestParameters.RIGHT_BRACKETS);
                }
            }
            if (str5 != null && !str2.equals("")) {
                a(context, str2, bVar, true);
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(d(str2) ? com.startapp.common.a.a.a(str6) : "");
            a(context, sb3.toString(), str3, str6, j, j2, z, bool, runnable);
            return;
        }
        b bVar2 = bVar;
        a(context, str, str2, bVar, z, z2);
    }

    public static void b(Context context, String str, b bVar) {
        a(context, str, bVar, true);
    }

    private static void a(final Context context, final String str, final b bVar, final boolean z) {
        if (!str.equals("")) {
            f.a(com.startapp.common.f.a.HIGH, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        if (z) {
                            Context context = context;
                            StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append(com.startapp.common.a.a.a(c.a(str, (String) null)));
                            sb.append(bVar.getQueryString());
                            com.startapp.android.publish.adsCommon.l.a.a(context, sb.toString());
                            return;
                        }
                        Context context2 = context;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append(bVar.getQueryString());
                        com.startapp.android.publish.adsCommon.l.a.a(context2, sb2.toString());
                    } catch (com.startapp.common.e e) {
                        com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.sendTrackingMessage - Error sending tracking message", e.getMessage(), c.a(str, (String) null));
                    }
                }
            });
        }
    }

    public static void b(final Context context, final String str) {
        f.a(com.startapp.common.f.a.HIGH, (Runnable) new Runnable() {
            public final void run() {
                try {
                    com.startapp.android.publish.adsCommon.l.a.a(context, str);
                } catch (com.startapp.common.e e) {
                    com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.sendTrackingMessage - Error sending tracking message", e.getMessage(), "");
                }
            }
        });
    }

    private static final void a(Context context, String str, String str2, String str3, long j, long j2, boolean z, Boolean bool, Runnable runnable) {
        Context context2 = context;
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        com.startapp.common.b.a(context).a(new Intent("com.startapp.android.OnClickCallback"));
        if (b(str)) {
            if (str5 != null && !str5.equals("") && !str.toLowerCase().contains(str2.toLowerCase())) {
                StringBuilder sb = new StringBuilder("Expected: ");
                sb.append(str5);
                sb.append(" Link: ");
                sb.append(str4);
                com.startapp.android.publish.adsCommon.g.f.a(context2, d.WRONG_PACKAGE_REACHED, "Wrong package name reached", sb.toString(), str6);
            }
            a(context2, str4, str6);
            if (runnable != null) {
                runnable.run();
            }
            return;
        }
        if (context2 instanceof Activity) {
            j.a((Activity) context2, true);
        }
        try {
            final WebView webView = new WebView(context2);
            if (f4050a == null) {
                if (VERSION.SDK_INT >= 22) {
                    f4050a = new ProgressDialog(context2, 16974545);
                } else {
                    f4050a = new ProgressDialog(context2);
                }
                f4050a.setTitle(null);
                f4050a.setMessage("Loading....");
                f4050a.setIndeterminate(false);
                f4050a.setCancelable(false);
                f4050a.setOnCancelListener(new OnCancelListener() {
                    public final void onCancel(DialogInterface dialogInterface) {
                        webView.stopLoading();
                    }
                });
                if (!(context2 instanceof Activity) || ((Activity) context2).isFinishing()) {
                    if (!(context2 instanceof Activity) && b(context) && f4050a.getWindow() != null) {
                        if (VERSION.SDK_INT >= 26) {
                            f4050a.getWindow().setType(2038);
                        } else {
                            f4050a.getWindow().setType(2003);
                        }
                    }
                }
                f4050a.show();
            }
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebChromeClient(new WebChromeClient());
            a aVar = r2;
            WebView webView2 = webView;
            a aVar2 = new a(j, j2, z, bool, f4050a, str, str2, str3, runnable);
            webView2.setWebViewClient(aVar);
            webView2.loadUrl(str4);
        } catch (Exception e) {
            Context context3 = context;
            com.startapp.android.publish.adsCommon.g.f.a(context3, d.EXCEPTION, "Util.smartRedirect - Webview failed", e.getMessage(), str6);
            a(context3, str4, str6);
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    private static boolean b(Context context) {
        if (VERSION.SDK_INT >= 23) {
            return Settings.canDrawOverlays(context);
        }
        return com.startapp.common.a.c.a(context, "android.permission.SYSTEM_ALERT_WINDOW");
    }

    public static boolean b(String str) {
        return str.startsWith("market") || str.startsWith("http://play.google.com") || str.startsWith("https://play.google.com");
    }

    public static boolean c(String str) {
        return str != null && (str.startsWith("http://") || str.startsWith("https://"));
    }

    public static final void a(Context context) {
        if (context != null && (context instanceof Activity)) {
            j.a((Activity) context, false);
        }
        if (f4050a != null) {
            synchronized (f4050a) {
                if (f4050a != null && f4050a.isShowing()) {
                    try {
                        f4050a.cancel();
                    } catch (Exception e) {
                        com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "AdsCommonUtils.cancelProgress - progress.cancel() failed", e.getMessage(), "");
                    }
                    f4050a = null;
                }
            }
        }
    }

    public static void c(Context context, String str) {
        a(context, str, (String) null);
    }

    public static void a(Context context, String str, String str2) {
        a(context, str, str2, c(str));
    }

    private static void a(Context context, String str, String str2, boolean z) {
        if (context != null) {
            int i = 76021760;
            if (b.a().G() || !(context instanceof Activity)) {
                i = 344457216;
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(i);
            boolean a2 = a(context, intent);
            if (!a2) {
                try {
                    if (VERSION.SDK_INT >= 18 && com.startapp.android.publish.common.metaData.e.getInstance().getChromeCustomeTabsExternal() && c(context)) {
                        a(context, str, z);
                        return;
                    }
                } catch (Exception unused) {
                    a(context, str, str2, i);
                    return;
                }
            }
            if (z && !a2) {
                a(context, intent, i);
            }
            context.startActivity(intent);
        }
    }

    private static void a(Context context, Intent intent, int i) {
        String[] strArr = {"com.android.chrome", "com.android.browser", "com.opera.mini.native", "org.mozilla.firefox", "com.opera.browser"};
        try {
            List queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, i);
            if (queryIntentActivities != null && queryIntentActivities.size() > 1) {
                for (int i2 = 0; i2 < 5; i2++) {
                    String str = strArr[i2];
                    if (com.startapp.common.a.c.a(context, str, 0)) {
                        intent.setPackage(str);
                        return;
                    }
                }
            }
        } catch (Exception e) {
            com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "AdsCommonUtils.chooseDefaultBrowser", e.getMessage(), "");
        }
    }

    private static void a(Context context, String str, String str2, int i) {
        try {
            Intent parseUri = Intent.parseUri(str, i);
            a(context, parseUri);
            if (!(context instanceof Activity)) {
                parseUri.addFlags(268435456);
            }
            context.startActivity(parseUri);
        } catch (Exception e) {
            com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.openUrlExternally - Couldn't start activity", e.getMessage(), str2);
            StringBuilder sb = new StringBuilder("Cannot find activity to handle url: [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
        }
    }

    public static void b(Context context, String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.OpenAsInAppBrowser - Couldn't start activity", "Parameter clickUrl is null", str2);
        } else if (b(str) || !j.a(256)) {
            a(context, str, str2);
        } else {
            try {
                if (VERSION.SDK_INT >= 18 && com.startapp.android.publish.common.metaData.e.getInstance().getChromeCustomeTabsInternal() && c(context)) {
                    a(context, str, true);
                    return;
                }
            } catch (Exception e) {
                com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.OpenAsInAppBrowser - Couldn't openUrlChromeTabs", e.getMessage(), str2);
            }
            Intent intent = new Intent(context, OverlayActivity.class);
            if (VERSION.SDK_INT >= 21) {
                intent.addFlags(524288);
            }
            if (VERSION.SDK_INT >= 11) {
                intent.addFlags(32768);
            }
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            intent.setData(Uri.parse(str));
            intent.putExtra("placement", Placement.INAPP_BROWSER.getIndex());
            intent.putExtra("activityShouldLockOrientation", false);
            try {
                context.startActivity(intent);
            } catch (Exception e2) {
                com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "Util.OpenAsInAppBrowser - Couldn't start activity", e2.getMessage(), str2);
                StringBuilder sb = new StringBuilder("Cannot find activity to handle url: [");
                sb.append(str);
                sb.append(RequestParameters.RIGHT_BRACKETS);
            }
        }
    }

    @RequiresApi(api = 18)
    private static void a(Context context, String str, boolean z) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        Bundle bundle = new Bundle();
        bundle.putBinder(CustomTabsIntent.EXTRA_SESSION, null);
        intent.putExtras(bundle);
        if (z) {
            try {
                List queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
                if (queryIntentActivities != null && queryIntentActivities.size() > 1) {
                    intent.setPackage(((ResolveInfo) queryIntentActivities.get(0)).activityInfo.packageName);
                }
            } catch (Exception e) {
                com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "AdsCommonUtils.openUrlChromeTabs", e.getMessage(), "");
            }
        }
        context.startActivity(intent);
    }

    private static boolean c(Context context) {
        return k.a(context, "chromeTabs", Boolean.FALSE).booleanValue();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:0|(3:2|3|(2:6|4))|7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        com.startapp.android.publish.adsCommon.g.f.a(r5, com.startapp.android.publish.adsCommon.g.d.b, "Util.handleCPEClick - Couldn't start activity", r2.getMessage(), a(r4, (java.lang.String) null));
        r2 = new java.lang.StringBuilder("Cannot find activity to handle url: [");
        r2.append(r4);
        r2.append(com.ironsource.sdk.constants.Constants.RequestParameters.RIGHT_BRACKETS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0055, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0031 */
    public static void a(String str, String str2, String str3, Context context, b bVar) {
        a(context, str3, bVar, true);
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
        if (str2 != null) {
            JSONObject jSONObject = new JSONObject(str2);
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String valueOf = String.valueOf(keys.next());
                launchIntentForPackage.putExtra(valueOf, String.valueOf(jSONObject.get(valueOf)));
            }
        }
        context.startActivity(launchIntentForPackage);
    }

    public static String a(String str, String str2) {
        String str3 = "";
        if (str2 != null) {
            try {
                if (!str2.equals("")) {
                    str = str2;
                }
            } catch (Exception unused) {
                return str3;
            }
        }
        String[] split = str.split("[?&]d=");
        return split.length >= 2 ? split[1].split("[?&]")[0] : str3;
    }

    private static boolean a(Context context, Intent intent) {
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 0)) {
            if (resolveInfo.activityInfo.packageName.equalsIgnoreCase(Constants.f4160a)) {
                intent.setComponent(new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name));
                return true;
            }
        }
        return false;
    }

    public static String a() {
        StringBuilder sb = new StringBuilder("&position=");
        sb.append(b());
        return sb.toString();
    }

    public static String b() {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        int i = 0;
        while (i < 8) {
            if (stackTrace[i].getMethodName().compareTo("doHome") == 0) {
                return "home";
            }
            if (stackTrace[i].getMethodName().compareTo("onBackPressed") != 0) {
                i++;
            } else if (!a.f4110a.f() && !m.m()) {
                return VideoType.INTERSTITIAL;
            } else {
                a.f4110a.k();
                return "back";
            }
        }
        return VideoType.INTERSTITIAL;
    }

    public static String[] a(g gVar) {
        if (gVar instanceof HtmlAd) {
            return ((HtmlAd) gVar).getTrackingUrls();
        }
        if (gVar instanceof h) {
            return a(((h) gVar).d());
        }
        return new String[0];
    }

    public static String[] a(List<AdDetails> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (AdDetails trackingUrl : list) {
                arrayList.add(trackingUrl.getTrackingUrl());
            }
        }
        return (String[]) arrayList.toArray(new String[0]);
    }

    public static boolean a(Context context, Placement placement) {
        new StringBuilder("forceExternal - check -placement is : ").append(placement);
        if (placement.equals(Placement.INAPP_SPLASH) || !b.a().M()) {
            return false;
        }
        return d(context);
    }

    private static boolean d(Context context) {
        return !com.startapp.common.a.b.a().a(context).b() && a(k.a(context, "shared_prefs_CookieFeatureTS", Long.valueOf(0)).longValue(), System.currentTimeMillis());
    }

    private static boolean a(long j, long j2) {
        return j == 0 || j + (((long) b.a().L()) * 86400000) <= j2;
    }

    private c(String str, URL url, String str2) {
        this.b = str;
        this.c = url;
        this.d = str2;
    }

    public static c a(String str, URL url, String str2) {
        com.b.a.a.a.b.b(str, "VendorKey is null or empty");
        com.b.a.a.a.b.a((Object) url, "ResourceURL is null");
        com.b.a.a.a.b.b(str2, "VerificationParameters is null or empty");
        return new c(str, url, str2);
    }

    public final String c() {
        return this.b;
    }

    public final URL d() {
        return this.c;
    }

    public final String e() {
        return this.d;
    }
}
