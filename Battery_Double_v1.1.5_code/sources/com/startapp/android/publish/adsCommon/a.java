package com.startapp.android.publish.adsCommon;

import android.app.Activity;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class a implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean isActivityFullScreen;

    public a(Activity activity) {
        this.isActivityFullScreen = c.a(activity);
    }

    public final boolean a() {
        return this.isActivityFullScreen;
    }
}
