package com.startapp.android.publish.adsCommon.h.a;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import java.net.URLDecoder;
import java.util.Map;

/* compiled from: StartAppSDK */
public abstract class a implements b {
    private static final String TAG = "BaseMraidController";
    protected C0089a openListener;

    /* renamed from: com.startapp.android.publish.adsCommon.h.a.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public interface C0089a {
        boolean onClickEvent(String str);
    }

    public abstract void close();

    public void expand(String str) {
    }

    public abstract boolean isFeatureSupported(String str);

    public void resize() {
    }

    public abstract void setOrientationProperties(Map<String, String> map);

    public abstract void useCustomClose(String str);

    public a(@NonNull C0089a aVar) {
        this.openListener = aVar;
    }

    public boolean open(String str) {
        Exception e;
        String str2;
        try {
            str2 = URLDecoder.decode(str, "UTF-8").trim();
            try {
                if (str2.startsWith("sms")) {
                    return openSMS(str2);
                }
                if (str2.startsWith("tel")) {
                    return openTel(str2);
                }
                return this.openListener.onClickEvent(str2);
            } catch (Exception e2) {
                e = e2;
                e.getMessage();
                return this.openListener.onClickEvent(str2);
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str2 = str;
            e = exc;
            e.getMessage();
            return this.openListener.onClickEvent(str2);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        if (r0 != false) goto L_0x0015;
     */
    public void applyOrientationProperties(Activity activity, com.startapp.android.publish.adsCommon.h.c.a aVar) {
        try {
            int i = 0;
            boolean z = activity.getResources().getConfiguration().orientation == 1;
            if (aVar.b != 0) {
                if (aVar.b != 1) {
                    if (aVar.f4083a) {
                        i = -1;
                    }
                }
                activity.setRequestedOrientation(i);
            }
            i = 1;
            try {
                activity.setRequestedOrientation(i);
            } catch (Exception unused) {
            }
        } catch (Exception e) {
            f.a(activity, d.EXCEPTION, "BaseMraidController.applyOrientationProperties", e.getMessage(), "");
        }
    }

    public void setResizeProperties(@NonNull Map<String, String> map) {
        new StringBuilder("setResizeProperties ").append(map);
    }

    public void setExpandProperties(Map<String, String> map) {
        new StringBuilder("setExpandProperties ").append(map);
    }

    public void createCalendarEvent(String str) {
        isFeatureSupported(MRAIDNativeFeature.CALENDAR);
    }

    public void playVideo(String str) {
        isFeatureSupported(MRAIDNativeFeature.INLINE_VIDEO);
    }

    public void storePicture(String str) {
        isFeatureSupported(MRAIDNativeFeature.STORE_PICTURE);
    }

    public boolean openSMS(String str) {
        isFeatureSupported("sms");
        return true;
    }

    public boolean openTel(String str) {
        isFeatureSupported("tel");
        return true;
    }
}
