package com.startapp.android.publish.adsCommon.h.a;

import android.annotation.TargetApi;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.h.d.a;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: StartAppSDK */
public class d extends WebViewClient {
    private static final String MRAID_JS = "mraid.js";
    private static final String MRAID_PREFIX = "mraid://";
    private static final String TAG = "MraidWebViewClient";
    private b controller;
    private boolean isMraidInjected = false;

    public d(@NonNull b bVar) {
        this.controller = bVar;
    }

    /* access modifiers changed from: protected */
    public boolean isMraidUrl(String str) {
        return str != null && str.startsWith(MRAID_PREFIX);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (isMraidUrl(str)) {
            return invokeMraidMethod(str);
        }
        return this.controller.open(str);
    }

    @Nullable
    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        if (this.isMraidInjected || !matchesInjectionUrl(str)) {
            return super.shouldInterceptRequest(webView, str);
        }
        this.isMraidInjected = true;
        return createMraidInjectionResponse();
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
    }

    @VisibleForTesting
    public boolean matchesInjectionUrl(@NonNull String str) {
        try {
            return MRAID_JS.equals(Uri.parse(str.toLowerCase(Locale.US)).getLastPathSegment());
        } catch (Exception e) {
            new StringBuilder("matchesInjectionUrl Exception: ").append(e.getMessage());
            return false;
        }
    }

    @TargetApi(11)
    private WebResourceResponse createMraidInjectionResponse() {
        StringBuilder sb = new StringBuilder("javascript:");
        sb.append(a.a());
        return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(sb.toString().getBytes()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x010c, code lost:
        if (r14.containsKey("forceOrientation") != false) goto L_0x0158;
     */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01e2 A[Catch:{ Exception -> 0x021a }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01e5 A[Catch:{ Exception -> 0x021a }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01e8 A[Catch:{ Exception -> 0x021a }] */
    @VisibleForTesting
    public boolean invokeMraidMethod(String str) {
        char c;
        String str2;
        boolean z;
        String[] split;
        String str3 = str;
        String[] strArr = {"close", "resize"};
        String[] strArr2 = {"createCalendarEvent", Events.CREATIVE_EXPAND, "open", "playVideo", MRAIDNativeFeature.STORE_PICTURE, "useCustomClose"};
        String[] strArr3 = {"setOrientationProperties", "setResizeProperties"};
        try {
            String substring = str3.substring(8);
            HashMap hashMap = new HashMap();
            int indexOf = substring.indexOf(63);
            if (indexOf != -1) {
                String substring2 = substring.substring(0, indexOf);
                for (String str4 : substring.substring(indexOf + 1).split(RequestParameters.AMPERSAND)) {
                    int indexOf2 = str4.indexOf(61);
                    hashMap.put(str4.substring(0, indexOf2), str4.substring(indexOf2 + 1));
                }
                substring = substring2;
            }
            HashMap hashMap2 = null;
            if (!Arrays.asList(new String[]{"close", "createCalendarEvent", Events.CREATIVE_EXPAND, "open", "playVideo", "resize", "setOrientationProperties", "setResizeProperties", MRAIDNativeFeature.STORE_PICTURE, "useCustomClose"}).contains(substring)) {
                StringBuilder sb = new StringBuilder("command ");
                sb.append(substring);
                sb.append(" is unknown");
            } else {
                if (substring.equals("createCalendarEvent")) {
                    z = hashMap.containsKey("eventJSON");
                } else {
                    if (!substring.equals("open") && !substring.equals("playVideo")) {
                        if (!substring.equals(MRAIDNativeFeature.STORE_PICTURE)) {
                            if (!substring.equals("setOrientationProperties")) {
                                if (substring.equals("setResizeProperties")) {
                                    if (hashMap.containsKey("width") && hashMap.containsKey("height") && hashMap.containsKey("offsetX") && hashMap.containsKey("offsetY") && hashMap.containsKey("customClosePosition")) {
                                        if (!hashMap.containsKey("allowOffscreen")) {
                                        }
                                    }
                                } else if (substring.equals("useCustomClose")) {
                                    z = hashMap.containsKey("useCustomClose");
                                }
                                z = true;
                            } else if (hashMap.containsKey("allowOrientationChange")) {
                            }
                            z = false;
                        }
                    }
                    z = hashMap.containsKey("url");
                }
                if (!z) {
                    StringBuilder sb2 = new StringBuilder("command URL ");
                    sb2.append(str3);
                    sb2.append(" is missing parameters");
                } else {
                    hashMap2 = new HashMap();
                    hashMap2.put("command", substring);
                    hashMap2.putAll(hashMap);
                }
            }
            String str5 = (String) hashMap2.get("command");
            if (Arrays.asList(strArr).contains(str5)) {
                b.class.getDeclaredMethod(str5, new Class[0]).invoke(this.controller, new Object[0]);
            } else if (Arrays.asList(strArr2).contains(str5)) {
                Method declaredMethod = b.class.getDeclaredMethod(str5, new Class[]{String.class});
                int hashCode = str5.hashCode();
                if (hashCode != -733616544) {
                    if (hashCode == 1614272768) {
                        if (str5.equals("useCustomClose")) {
                            c = 1;
                            switch (c) {
                                case 0:
                                    str2 = "eventJSON";
                                    break;
                                case 1:
                                    str2 = "useCustomClose";
                                    break;
                                default:
                                    str2 = "url";
                                    break;
                            }
                            declaredMethod.invoke(this.controller, new Object[]{(String) hashMap2.get(str2)});
                        }
                    }
                } else if (str5.equals("createCalendarEvent")) {
                    c = 0;
                    switch (c) {
                        case 0:
                            break;
                        case 1:
                            break;
                    }
                    declaredMethod.invoke(this.controller, new Object[]{(String) hashMap2.get(str2)});
                }
                c = 65535;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
                declaredMethod.invoke(this.controller, new Object[]{(String) hashMap2.get(str2)});
            } else if (Arrays.asList(strArr3).contains(str5)) {
                b.class.getDeclaredMethod(str5, new Class[]{Map.class}).invoke(this.controller, new Object[]{hashMap2});
            }
            return true;
        } catch (Exception e) {
            StringBuilder sb3 = new StringBuilder("failed to invoke ");
            sb3.append(str3);
            sb3.append(". ");
            sb3.append(e.getMessage());
            return false;
        }
    }
}
