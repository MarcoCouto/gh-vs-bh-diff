package com.startapp.android.publish.adsCommon.h.a;

/* compiled from: StartAppSDK */
public enum c {
    LOADING,
    DEFAULT,
    EXPANDED,
    RESIZED,
    HIDDEN;

    public final String toString() {
        return name().toLowerCase();
    }
}
