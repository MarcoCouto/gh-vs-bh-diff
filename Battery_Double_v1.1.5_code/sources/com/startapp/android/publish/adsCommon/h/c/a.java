package com.startapp.android.publish.adsCommon.h.c;

import java.util.Arrays;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a {
    private static final List<String> c = Arrays.asList(new String[]{"portrait", "landscape", "none"});

    /* renamed from: a reason: collision with root package name */
    public boolean f4083a;
    public int b;

    public a() {
        this(0);
    }

    private a(byte b2) {
        this.f4083a = true;
        this.b = 2;
    }

    public static int a(String str) {
        int indexOf = c.indexOf(str);
        if (indexOf != -1) {
            return indexOf;
        }
        return 2;
    }
}
