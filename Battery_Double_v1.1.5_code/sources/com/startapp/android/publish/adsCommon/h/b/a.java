package com.startapp.android.publish.adsCommon.h.b;

import android.content.Context;
import android.os.Build.VERSION;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.startapp.common.a.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private Context f4082a;
    private List<String> b = new ArrayList();

    public a(Context context) {
        this.f4082a = context.getApplicationContext();
    }

    public final boolean a() {
        return this.b.contains(MRAIDNativeFeature.CALENDAR) && VERSION.SDK_INT >= 14 && c.a(this.f4082a, "android.permission.WRITE_CALENDAR");
    }

    public final boolean b() {
        return this.b.contains(MRAIDNativeFeature.INLINE_VIDEO);
    }

    public final boolean c() {
        return this.b.contains("sms") && c.a(this.f4082a, "android.permission.SEND_SMS");
    }

    public final boolean d() {
        return this.b.contains(MRAIDNativeFeature.STORE_PICTURE);
    }

    public final boolean e() {
        return this.b.contains("tel") && c.a(this.f4082a, "android.permission.CALL_PHONE");
    }

    public final boolean a(String str) {
        return this.b.contains(str);
    }
}
