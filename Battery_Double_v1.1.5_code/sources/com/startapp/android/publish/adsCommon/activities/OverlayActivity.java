package com.startapp.android.publish.adsCommon.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.view.KeyEvent;
import com.startapp.android.publish.ads.a.b;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;

/* compiled from: StartAppSDK */
public class OverlayActivity extends Activity {
    @VisibleForTesting

    /* renamed from: a reason: collision with root package name */
    private b f4019a;
    private boolean b;
    private int c;
    private boolean d;
    private Bundle e;
    private boolean f = false;
    private int g = -1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = false;
        overridePendingTransition(0, 0);
        super.onCreate(bundle);
        boolean booleanExtra = getIntent().getBooleanExtra("videoAd", false);
        requestWindowFeature(1);
        if (getIntent().getBooleanExtra(Events.CREATIVE_FULLSCREEN, false) || booleanExtra) {
            getWindow().setFlags(1024, 1024);
        }
        this.d = getIntent().getBooleanExtra("activityShouldLockOrientation", true);
        if (bundle == null && !booleanExtra) {
            com.startapp.common.b.a((Context) this).a(new Intent("com.startapp.android.ShowDisplayBroadcastListener"));
        }
        if (bundle != null) {
            this.g = bundle.getInt("activityLockedOrientation", -1);
            this.d = bundle.getBoolean("activityShouldLockOrientation", true);
        }
        this.c = getIntent().getIntExtra("orientation", getResources().getConfiguration().orientation);
        if (getResources().getConfiguration().orientation != this.c) {
            z = true;
        }
        this.b = z;
        if (!this.b) {
            a();
            this.f4019a.a(bundle);
            return;
        }
        this.e = bundle;
    }

    private void a() {
        this.f4019a = b.a(this, getIntent(), Placement.getByIndex(getIntent().getIntExtra("placement", 0)));
        if (this.f4019a == null) {
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.b) {
            a();
            this.f4019a.a(this.e);
            this.f4019a.s();
            this.b = false;
        }
        this.f4019a.t();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f4019a == null || this.f4019a.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.b) {
            this.f4019a.q();
            c.a((Context) this);
        }
        overridePendingTransition(0, 0);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (!this.b) {
            this.f4019a.b(bundle);
            bundle.putInt("activityLockedOrientation", this.g);
            bundle.putBoolean("activityShouldLockOrientation", this.d);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.g == -1) {
            this.g = j.a((Activity) this, this.c, this.d);
        } else {
            try {
                setRequestedOrientation(this.g);
            } catch (Exception unused) {
            }
        }
        if (!this.b) {
            this.f4019a.s();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (!this.b) {
            this.f4019a.r();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (!this.b) {
            this.f4019a.u();
            this.f4019a = null;
            j.a((Activity) this, false);
        }
        super.onDestroy();
    }

    public void onBackPressed() {
        if (!this.f4019a.p()) {
            super.onBackPressed();
        }
    }

    public void finish() {
        if (this.f4019a != null) {
            this.f4019a.o();
        }
        super.finish();
    }
}
