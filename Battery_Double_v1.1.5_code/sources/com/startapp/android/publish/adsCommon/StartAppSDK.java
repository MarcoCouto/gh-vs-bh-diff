package com.startapp.android.publish.adsCommon;

import android.app.Activity;
import android.content.Context;
import com.github.mikephil.charting.utils.Utils;
import com.startapp.android.publish.adsCommon.a.b;
import com.startapp.android.publish.common.metaData.g.a;

public class StartAppSDK {
    public static void init(Activity activity, String str) {
        init(activity, str, new SDKAdPreferences());
    }

    public static void init(Activity activity, String str, String str2) {
        init(activity, str, str2, new SDKAdPreferences());
    }

    public static void init(Activity activity, String str, boolean z) {
        init(activity, str, new SDKAdPreferences(), z);
    }

    public static void init(Activity activity, String str, String str2, boolean z) {
        init(activity, str, str2, new SDKAdPreferences(), z);
    }

    @Deprecated
    public static void init(Context context, String str, String str2) {
        init(context, str, str2, new SDKAdPreferences());
    }

    @Deprecated
    public static void init(Context context, String str, boolean z) {
        init(context, (String) null, str, z);
    }

    @Deprecated
    public static void init(Context context, String str, String str2, boolean z) {
        init(context, str, str2, new SDKAdPreferences(), z);
    }

    public static void inAppPurchaseMade(Context context) {
        inAppPurchaseMade(context, Utils.DOUBLE_EPSILON);
    }

    public static void inAppPurchaseMade(Context context, double d) {
        k.b(context, "payingUser", Boolean.TRUE);
        double floatValue = (double) k.a(context, "inAppPurchaseAmount", Float.valueOf(0.0f)).floatValue();
        Double.isNaN(floatValue);
        k.b(context, "inAppPurchaseAmount", Float.valueOf((float) (floatValue + d)));
        a.f4110a;
        m.a(context, a.IN_APP_PURCHASE);
    }

    public static void init(Activity activity, String str, SDKAdPreferences sDKAdPreferences) {
        a.f4110a.a(activity, null, str, sDKAdPreferences, true);
    }

    public static void init(Activity activity, String str, String str2, SDKAdPreferences sDKAdPreferences) {
        a.f4110a.a(activity, str, str2, sDKAdPreferences, true);
    }

    public static void init(Activity activity, String str, SDKAdPreferences sDKAdPreferences, boolean z) {
        a.f4110a.a(activity, null, str, sDKAdPreferences, z);
    }

    public static void init(Activity activity, String str, String str2, SDKAdPreferences sDKAdPreferences, boolean z) {
        a.f4110a.a(activity, str, str2, sDKAdPreferences, z);
    }

    @Deprecated
    public static void init(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences) {
        a.f4110a.a(context, str, str2, sDKAdPreferences, true);
    }

    @Deprecated
    public static void init(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences, boolean z) {
        a.f4110a.a(context, str, str2, sDKAdPreferences, z);
    }

    public static void startNewSession(Context context) {
        a.f4110a;
        m.a(context, a.CUSTOM);
    }

    public static void addWrapper(Context context, String str, String str2) {
        a.f4110a.a(context, str, str2);
    }

    public static void enableReturnAds(boolean z) {
        a.f4110a.d(z);
    }

    private static void pauseServices(Context context) {
        a.f4110a;
        k.b(context, "periodicInfoEventPaused", Boolean.TRUE);
        b.a(786564404);
        a.f4110a;
        k.b(context, "periodicMetadataPaused", Boolean.TRUE);
        b.a(586482792);
    }

    private static void resumeServices(Context context) {
        a.f4110a;
        k.b(context, "periodicInfoEventPaused", Boolean.FALSE);
        b.a(context, k.a(context, "periodicInfoEventTriggerTime", Long.valueOf(b.b(context))).longValue());
        a.f4110a;
        k.b(context, "periodicMetadataPaused", Boolean.FALSE);
        b.a(context, Long.valueOf(k.a(context, "periodicMetadataTriggerTime", Long.valueOf(b.a())).longValue()));
    }

    public static void setUserConsent(Context context, String str, long j, boolean z) {
        a.f4110a;
        m.a(context, str, z, true);
    }
}
