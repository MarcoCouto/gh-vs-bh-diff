package com.startapp.android.publish.adsCommon;

import android.content.Context;
import android.os.Handler;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener.NotDisplayedReason;
import com.startapp.android.publish.adsCommon.e.b;
import com.startapp.android.publish.common.metaData.e;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: StartAppSDK */
public final class i {

    /* renamed from: a reason: collision with root package name */
    private static final boolean f4086a = e.getInstance().isSupportIABViewability();
    private Handler b = new Handler();
    private long c;
    private Context d;
    private long e = -1;
    private long f;
    private boolean g;
    private boolean h;
    private String[] i;
    private b j;
    private AtomicBoolean k = new AtomicBoolean(false);
    private a l;

    /* compiled from: StartAppSDK */
    public interface a {
        void onSent();
    }

    public i(Context context, String[] strArr, b bVar, long j2) {
        this.d = context.getApplicationContext();
        this.i = strArr;
        this.j = bVar;
        this.c = j2;
    }

    public final void a(a aVar) {
        this.l = aVar;
    }

    public final void b() {
        if (this.g && this.h) {
            this.b.removeCallbacksAndMessages(null);
            this.e = System.currentTimeMillis();
            this.c -= this.e - this.f;
            this.h = false;
        }
    }

    public final void a(boolean z) {
        StringBuilder sb = new StringBuilder("cancel(");
        sb.append(z);
        sb.append(")");
        b(z);
        this.g = false;
        this.b.removeCallbacksAndMessages(null);
        this.h = false;
        this.e = -1;
        this.f = 0;
    }

    public final boolean c() {
        return this.k.get();
    }

    /* access modifiers changed from: protected */
    public final void b(boolean z) {
        if (this.k.compareAndSet(false, true)) {
            if (z) {
                c.a(this.d, this.i, this.j);
                if (this.l != null) {
                    this.l.onSent();
                }
            } else {
                c.a(this.d, this.i, this.j.getAdTag(), NotDisplayedReason.AD_CLOSED_TOO_QUICKLY.toString());
            }
        }
    }

    public final void a() {
        if (!this.k.get()) {
            if (f4086a) {
                long j2 = this.c;
                if (!this.h) {
                    this.h = true;
                    if (!this.g) {
                        this.g = true;
                    }
                    StringBuilder sb = new StringBuilder("Scheduling timer to: ");
                    sb.append(j2);
                    sb.append(" millis, Num urls = ");
                    sb.append(this.i.length);
                    this.f = System.currentTimeMillis();
                    this.b.postDelayed(new Runnable() {
                        public final void run() {
                            i.this.b(true);
                        }
                    }, j2);
                }
                return;
            }
            b(true);
        }
    }
}
