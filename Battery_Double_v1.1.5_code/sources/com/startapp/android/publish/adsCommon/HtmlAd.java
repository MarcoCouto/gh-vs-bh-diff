package com.startapp.android.publish.adsCommon;

import android.content.Context;
import com.b.a.a.a.b;
import com.startapp.android.publish.ads.splash.SplashConfig.Orientation;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adinformation.AdInformationPositions.Position;
import com.startapp.android.publish.adsCommon.c.a;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public abstract class HtmlAd extends Ad {
    protected static String launcherName = null;
    private static final long serialVersionUID = 1;
    private String adId = null;
    private List<a> apps;
    private String[] closingUrl = {""};
    private Long delayImpressionInSeconds;
    private int height;
    private String htmlUuid = "";
    public boolean[] inAppBrowserEnabled = {true};
    private boolean isMraidAd = false;
    private int orientation = 0;
    private String[] packageNames = {""};
    private int rewardDuration = 0;
    private boolean rewardedHideTimer = false;
    private Boolean[] sendRedirectHops = null;
    public boolean[] smartRedirect = {false};
    private String[] trackingClickUrls = {""};
    public String[] trackingUrls = {""};
    private int width;

    public String getHtml() {
        return com.startapp.android.publish.cache.a.a().a(this.htmlUuid);
    }

    public String getHtmlUuid() {
        return this.htmlUuid;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public int getWidth() {
        return this.width;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public void setClosingUrl(String str) {
        this.closingUrl = str.split(",");
    }

    public String[] getClosingUrl() {
        return this.closingUrl;
    }

    public int getHeight() {
        return this.height;
    }

    public void setSize(int i, int i2) {
        setWidth(i);
        setHeight(i2);
    }

    private void setPlayableRewardDuration(String str) {
        try {
            this.rewardDuration = Integer.parseInt(str);
        } catch (Exception e) {
            e.getMessage();
            f.a(this.context, d.EXCEPTION, "setPlayableRewardDuration: wrong number format.", e.getMessage(), "");
        }
    }

    public int getPlayableRewardDuration() {
        return this.rewardDuration;
    }

    private void setRewardedHideTimer(String str) {
        try {
            this.rewardedHideTimer = Boolean.parseBoolean(str);
        } catch (Exception e) {
            e.getMessage();
            f.a(this.context, d.EXCEPTION, "setRewardedHideTimer: wrong boolean format.", e.getMessage(), "");
        }
    }

    public boolean isRewardedHideTimer() {
        return this.rewardedHideTimer;
    }

    public HtmlAd(Context context, Placement placement) {
        super(context, placement);
        if (launcherName == null) {
            initDefaultLauncherName();
        }
    }

    public String gtAdId() {
        if (this.adId == null) {
            this.adId = j.a(getHtml(), "@adId@", "@adId@");
        }
        return this.adId;
    }

    private void initDefaultLauncherName() {
        launcherName = j.c(getContext());
    }

    private String injectOmsdkJavascript(String str) {
        try {
            return b.a(com.startapp.android.publish.b.b.a(), str);
        } catch (Exception e) {
            e.getMessage();
            f.a(this.context, d.EXCEPTION, "OMSDK: Failed to inject js to html ad.", e.getMessage(), "");
            return str;
        }
    }

    public void setHtml(String str) {
        if (com.startapp.android.publish.adsCommon.h.d.a.b(str)) {
            str = com.startapp.android.publish.adsCommon.h.d.a.a(str);
            setMraidAd(true);
        }
        if (e.getInstance().isOmsdkEnabled()) {
            str = injectOmsdkJavascript(str);
        }
        if (j.a(512)) {
            this.htmlUuid = com.startapp.android.publish.cache.a.a().a(str, UUID.randomUUID().toString());
        }
        String extractMetadata = extractMetadata(str, "@smartRedirect@");
        if (extractMetadata != null) {
            setSmartRedirect(extractMetadata);
        }
        String extractMetadata2 = extractMetadata(str, "@trackingClickUrl@");
        if (extractMetadata2 != null) {
            setTrackingClickUrl(extractMetadata2);
        }
        String extractMetadata3 = extractMetadata(str, "@closeUrl@");
        if (extractMetadata3 != null) {
            setClosingUrl(extractMetadata3);
        }
        String extractMetadata4 = extractMetadata(str, "@tracking@");
        if (extractMetadata4 != null) {
            setTrackingUrls(extractMetadata4);
        }
        String extractMetadata5 = extractMetadata(str, "@packageName@");
        if (extractMetadata5 != null) {
            setPackageNames(extractMetadata5);
        }
        String extractMetadata6 = extractMetadata(str, "@startappBrowserEnabled@");
        if (extractMetadata6 != null) {
            setInAppBrowserFlag(extractMetadata6);
        }
        String extractMetadata7 = extractMetadata(str, "@orientation@");
        if (extractMetadata7 != null && j.a(8)) {
            setOrientation(Orientation.getByName(extractMetadata7));
        }
        String extractMetadata8 = extractMetadata(str, "@adInfoEnable@");
        if (extractMetadata8 != null) {
            setAdInfoEnableOverride(extractMetadata8);
        }
        String extractMetadata9 = extractMetadata(str, "@adInfoPosition@");
        if (extractMetadata9 != null) {
            setAdInfoPositionOverride(extractMetadata9);
        }
        String extractMetadata10 = extractMetadata(str, "@ttl@");
        if (extractMetadata10 != null) {
            setAdCacheTtl(extractMetadata10);
        }
        String extractMetadata11 = extractMetadata(str, "@belowMinCPM@");
        if (extractMetadata11 != null) {
            setBelowMinCPM(extractMetadata11);
        }
        String extractMetadata12 = extractMetadata(str, "@delayImpressionInSeconds@");
        if (extractMetadata12 != null) {
            setDelayImpressionInSeconds(extractMetadata12);
        }
        String extractMetadata13 = extractMetadata(str, "@rewardDuration@");
        if (extractMetadata13 != null) {
            setPlayableRewardDuration(extractMetadata13);
        }
        String extractMetadata14 = extractMetadata(str, "@rewardedHideTimer@");
        if (extractMetadata14 != null) {
            setRewardedHideTimer(extractMetadata14);
        }
        String extractMetadata15 = extractMetadata(str, "@sendRedirectHops@");
        if (extractMetadata15 != null) {
            setSendRedirectHops(extractMetadata15);
        }
        if (this.smartRedirect.length < this.trackingUrls.length) {
            boolean[] zArr = new boolean[this.trackingUrls.length];
            int i = 0;
            while (i < this.smartRedirect.length) {
                zArr[i] = this.smartRedirect[i];
                i++;
            }
            while (i < this.trackingUrls.length) {
                zArr[i] = false;
                i++;
            }
            this.smartRedirect = zArr;
        }
    }

    private void setBelowMinCPM(String str) {
        if (Arrays.asList(str.split(",")).contains("false")) {
            this.belowMinCPM = false;
        } else {
            this.belowMinCPM = true;
        }
    }

    private void setInAppBrowserFlag(String str) {
        String[] split = str.split(",");
        this.inAppBrowserEnabled = new boolean[split.length];
        for (int i = 0; i < split.length; i++) {
            if (split[i].compareTo("false") == 0) {
                this.inAppBrowserEnabled[i] = false;
            } else {
                this.inAppBrowserEnabled[i] = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public String extractMetadata(String str, String str2) {
        return j.a(str, str2, str2);
    }

    /* access modifiers changed from: protected */
    public void setOrientation(Orientation orientation2) {
        this.orientation = 0;
        boolean a2 = j.a(8);
        if (orientation2 != null) {
            if (a2 && orientation2.equals(Orientation.PORTRAIT)) {
                this.orientation = 1;
            } else if (a2 && orientation2.equals(Orientation.LANDSCAPE)) {
                this.orientation = 2;
            }
        }
    }

    private void setSmartRedirect(String str) {
        String[] split = str.split(",");
        this.smartRedirect = new boolean[split.length];
        for (int i = 0; i < split.length; i++) {
            if (split[i].compareTo("true") == 0) {
                this.smartRedirect[i] = true;
            } else {
                this.smartRedirect[i] = false;
            }
        }
    }

    public boolean getSmartRedirect(int i) {
        if (i < 0 || i >= this.smartRedirect.length) {
            return false;
        }
        return this.smartRedirect[i];
    }

    public boolean[] getSmartRedirect() {
        return this.smartRedirect;
    }

    public boolean[] getInAppBrowserEnabled() {
        return this.inAppBrowserEnabled;
    }

    public boolean isInAppBrowserEnabled(int i) {
        if (this.inAppBrowserEnabled == null || i < 0 || i >= this.inAppBrowserEnabled.length) {
            return true;
        }
        return this.inAppBrowserEnabled[i];
    }

    private void setTrackingUrls(String str) {
        this.trackingUrls = str.split(",");
    }

    public String[] getTrackingUrls() {
        return this.trackingUrls;
    }

    public String[] getTrackingClickUrls() {
        return this.trackingClickUrls;
    }

    private void setTrackingClickUrl(String str) {
        this.trackingClickUrls = str.split(",");
    }

    public int getOrientation() {
        return this.orientation;
    }

    public String getTrackingUrls(int i) {
        if (i < 0 || i >= this.trackingUrls.length) {
            return null;
        }
        return this.trackingUrls[i];
    }

    private void setPackageNames(String str) {
        this.packageNames = str.split(",");
    }

    public String[] getPackageNames() {
        return this.packageNames;
    }

    public List<a> getApps() {
        return this.apps;
    }

    public void setApps(List<a> list) {
        this.apps = list;
    }

    private void setAdInfoEnableOverride(String str) {
        getAdInfoOverride().a(Boolean.parseBoolean(str));
    }

    private void setAdInfoPositionOverride(String str) {
        getAdInfoOverride().a(Position.getByName(str));
    }

    /* access modifiers changed from: protected */
    public String getLauncherName() {
        return launcherName;
    }

    public void setAdCacheTtl(String str) {
        String[] split;
        Long l = null;
        for (String str2 : str.split(",")) {
            if (!str2.equals("")) {
                try {
                    long parseLong = Long.parseLong(str2);
                    if (parseLong > 0 && (l == null || parseLong < l.longValue())) {
                        l = Long.valueOf(parseLong);
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        if (l != null) {
            this.adCacheTtl = Long.valueOf(TimeUnit.SECONDS.toMillis(l.longValue()));
        }
    }

    public Long getDelayImpressionInSeconds() {
        return this.delayImpressionInSeconds;
    }

    private void setDelayImpressionInSeconds(String str) {
        if (str != null && !str.equals("")) {
            try {
                this.delayImpressionInSeconds = Long.valueOf(Long.parseLong(str));
            } catch (NumberFormatException unused) {
            }
        }
    }

    public Boolean[] shouldSendRedirectHops() {
        return this.sendRedirectHops;
    }

    public Boolean shouldSendRedirectHops(int i) {
        if (this.sendRedirectHops == null || i < 0 || i >= this.sendRedirectHops.length) {
            return null;
        }
        return this.sendRedirectHops[i];
    }

    public void setSendRedirectHops(String str) {
        if (str != null && !str.equals("")) {
            String[] split = str.split(",");
            this.sendRedirectHops = new Boolean[split.length];
            for (int i = 0; i < split.length; i++) {
                if (split[i].compareTo("true") == 0) {
                    this.sendRedirectHops[i] = Boolean.TRUE;
                } else if (split[i].compareTo("false") == 0) {
                    this.sendRedirectHops[i] = Boolean.FALSE;
                } else {
                    this.sendRedirectHops[i] = null;
                }
            }
        }
    }

    public boolean isMraidAd() {
        return this.isMraidAd;
    }

    public void setMraidAd(boolean z) {
        this.isMraidAd = z;
    }
}
