package com.startapp.android.publish.adsCommon.e;

/* compiled from: StartAppSDK */
public final class a extends b {
    private static final long serialVersionUID = 1;
    private final String DURATION_PARAM_NAME = "&displayDuration=";
    private String duration;

    public a(String str, String str2) {
        super(str2);
        this.duration = str;
    }

    public final String getQueryString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.getQueryString());
        sb.append("&displayDuration=");
        sb.append(encode(this.duration));
        return sb.toString();
    }
}
