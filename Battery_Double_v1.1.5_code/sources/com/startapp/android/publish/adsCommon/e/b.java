package com.startapp.android.publish.adsCommon.e;

import android.content.Context;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.startapp.android.publish.adsCommon.a.h;
import com.startapp.android.publish.adsCommon.m;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.common.a.a;
import com.startapp.common.a.f;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: StartAppSDK */
public class b implements Serializable {
    private static final long serialVersionUID = 1;
    private String adTag;
    private String clientSessionId;
    private String location;
    private String nonImpressionReason;
    private int offset;
    private String profileId;

    public b() {
        this(null);
    }

    public b(String str) {
        this.adTag = str;
        this.clientSessionId = h.d().a();
        this.profileId = e.getInstance().getProfileId();
        this.offset = 0;
    }

    public String getAdTag() {
        return this.adTag;
    }

    public String getClientSessionId() {
        return this.clientSessionId;
    }

    public String getProfileId() {
        return this.profileId;
    }

    public int getOffset() {
        return this.offset;
    }

    public b setOffset(int i) {
        this.offset = i;
        return this;
    }

    public String getNonImpressionReason() {
        return this.nonImpressionReason;
    }

    public b setNonImpressionReason(String str) {
        this.nonImpressionReason = str;
        return this;
    }

    public void setLocation(Context context) {
        try {
            m.a();
            m.c(context);
            this.location = f.a(f.a(context, e.getInstance().getLocationConfig().isFiEnabled(), e.getInstance().getLocationConfig().isCoEnabled()));
        } catch (Exception unused) {
            this.location = null;
        }
    }

    private String getLocationQuery() {
        if (this.location == null || this.location.equals("")) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&locations=");
        sb.append(encode(a.c(this.location)));
        return sb.toString();
    }

    private String getNonImpressionReasonQuery() {
        if (this.nonImpressionReason == null || this.nonImpressionReason.equals("")) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&isShown=false&reason=");
        sb.append(encode(this.nonImpressionReason));
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String getOffsetQuery() {
        if (this.offset <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&offset=");
        sb.append(this.offset);
        return sb.toString();
    }

    private String getAdTagQuery() {
        if (this.adTag == null || this.adTag.equals("")) {
            return "";
        }
        int length = this.adTag.length();
        int i = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        if (length < 200) {
            i = this.adTag.length();
        }
        StringBuilder sb = new StringBuilder("&adTag=");
        sb.append(encode(this.adTag.substring(0, i)));
        return sb.toString();
    }

    private String getClientSessionIdQuery() {
        if (this.clientSessionId == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&clientSessionId=");
        sb.append(encode(this.clientSessionId));
        return sb.toString();
    }

    private String getProfileIdQuery() {
        if (this.profileId == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&profileId=");
        sb.append(encode(this.profileId));
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String encode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException unused) {
            return "";
        }
    }

    public String getQueryString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getAdTagQuery());
        sb.append(getClientSessionIdQuery());
        sb.append(getProfileIdQuery());
        sb.append(getOffsetQuery());
        sb.append(getNonImpressionReasonQuery());
        sb.append(getLocationQuery());
        return sb.toString();
    }
}
