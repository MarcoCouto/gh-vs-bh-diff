package com.startapp.android.publish.adsCommon.k;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.metaData.k;
import com.startapp.common.d;
import java.util.HashMap;
import org.json.JSONArray;

/* compiled from: StartAppSDK */
public final class b {

    /* renamed from: a reason: collision with root package name */
    protected a f4098a = new a();
    protected d b;
    private HashMap<Integer, a> c = null;
    private SensorManager d;
    /* access modifiers changed from: private */
    public int e;
    private SensorEventListener f = new SensorEventListener() {
        public final void onAccuracyChanged(Sensor sensor, int i) {
        }

        public final void onSensorChanged(SensorEvent sensorEvent) {
            if (b.this.f4098a.a(sensorEvent) == b.this.e) {
                b.this.b();
                if (b.this.b != null) {
                    b.this.b.a(b.this.c());
                }
            }
        }
    };

    /* compiled from: StartAppSDK */
    class a {

        /* renamed from: a reason: collision with root package name */
        private int f4100a;
        private int b;

        public a(int i, int i2) {
            this.f4100a = i;
            this.b = i2;
        }

        public final int a() {
            return this.f4100a;
        }

        public final int b() {
            return this.b;
        }
    }

    public b(Context context, d dVar) {
        this.d = (SensorManager) context.getSystemService("sensor");
        this.b = dVar;
        this.e = 0;
        this.c = new HashMap<>();
        k sensorsConfig = e.getInstance().getSensorsConfig();
        a(13, sensorsConfig.c());
        a(9, sensorsConfig.d());
        a(5, sensorsConfig.e());
        a(10, sensorsConfig.f());
        a(2, sensorsConfig.g());
        a(6, sensorsConfig.h());
        a(12, sensorsConfig.i());
        a(11, sensorsConfig.j());
        a(16, sensorsConfig.k());
    }

    public final void a() {
        for (Integer intValue : this.c.keySet()) {
            int intValue2 = intValue.intValue();
            a aVar = (a) this.c.get(Integer.valueOf(intValue2));
            if (VERSION.SDK_INT >= aVar.a()) {
                Sensor defaultSensor = this.d.getDefaultSensor(intValue2);
                if (defaultSensor != null) {
                    this.d.registerListener(this.f, defaultSensor, aVar.b());
                    this.e++;
                }
            }
        }
    }

    public final void b() {
        this.d.unregisterListener(this.f);
    }

    public final JSONArray c() {
        try {
            return this.f4098a.a();
        } catch (Exception unused) {
            return null;
        }
    }

    private void a(int i, com.startapp.android.publish.common.metaData.a aVar) {
        if (aVar.c()) {
            this.c.put(Integer.valueOf(i), new a(aVar.b(), aVar.a()));
        }
    }
}
