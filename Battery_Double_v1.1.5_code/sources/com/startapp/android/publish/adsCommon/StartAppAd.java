package com.startapp.android.publish.adsCommon;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.VisibleForTesting;
import com.startapp.android.publish.ads.splash.SplashConfig;
import com.startapp.android.publish.ads.splash.SplashHideListener;
import com.startapp.android.publish.adsCommon.Ad.AdState;
import com.startapp.android.publish.adsCommon.Ad.AdType;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.activities.AppWallActivity;
import com.startapp.android.publish.adsCommon.activities.OverlayActivity;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener.NotDisplayedReason;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.b.f;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.cache.a;
import com.startapp.android.publish.cache.c;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.Constants;
import com.startapp.common.b;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class StartAppAd extends Ad {
    private static final String TAG = "StartAppAd";
    private static final long serialVersionUID = 1;
    private static boolean testMode = false;
    @VisibleForTesting
    public g ad = null;
    private c adKey = null;
    private AdMode adMode = AdMode.AUTOMATIC;
    private AdPreferences adPreferences = null;
    AdDisplayListener callback = null;
    private BroadcastReceiver callbackBroadcastReceiver = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.startapp.android.ShowFailedDisplayBroadcastListener")) {
                if (intent.getExtras().containsKey("showFailedReason")) {
                    StartAppAd.this.setNotDisplayedReason((NotDisplayedReason) intent.getExtras().getSerializable("showFailedReason"));
                }
                if (StartAppAd.this.callback != null) {
                    StartAppAd.this.callback.adNotDisplayed(StartAppAd.this);
                }
                a(context);
            } else if (intent.getAction().equals("com.startapp.android.ShowDisplayBroadcastListener")) {
                if (StartAppAd.this.callback != null) {
                    StartAppAd.this.callback.adDisplayed(StartAppAd.this);
                }
            } else if (intent.getAction().equals("com.startapp.android.OnClickCallback")) {
                if (StartAppAd.this.callback != null) {
                    StartAppAd.this.callback.adClicked(StartAppAd.this);
                }
            } else if (!intent.getAction().equals("com.startapp.android.OnVideoCompleted")) {
                if (StartAppAd.this.callback != null) {
                    StartAppAd.this.callback.adHidden(StartAppAd.this);
                }
                a(context);
            } else if (StartAppAd.this.videoListener != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        StartAppAd.this.videoListener.onVideoCompleted();
                    }
                });
            }
            StartAppAd.this.ad = null;
        }

        private void a(Context context) {
            b.a(context).a((BroadcastReceiver) this);
        }
    };
    VideoListener videoListener = null;

    /* compiled from: StartAppSDK */
    public enum AdMode {
        AUTOMATIC,
        FULLPAGE,
        OFFERWALL,
        REWARDED_VIDEO,
        VIDEO,
        OVERLAY
    }

    /* access modifiers changed from: protected */
    public void loadAds(AdPreferences adPreferences2, AdEventListener adEventListener) {
    }

    public void onPause() {
    }

    public StartAppAd(Context context) {
        super(context, null);
    }

    public static void init(Context context, String str, String str2) {
        StartAppSDK.init(context, str, str2);
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences2, AdEventListener adEventListener) {
        if (!canShowAd()) {
            if (adEventListener != null) {
                setErrorMessage("serving ads disabled");
                adEventListener.onFailedToReceiveAd(this);
            }
            return false;
        }
        this.adKey = a.a().a(this.context, this, this.adMode, adPreferences2, adEventListener);
        if (this.adKey != null) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x0140  */
    @Deprecated
    public boolean show(String str, AdDisplayListener adDisplayListener) {
        f fVar;
        boolean z;
        setNotDisplayedReason(null);
        this.callback = new com.startapp.android.publish.adsCommon.adListeners.a(adDisplayListener);
        boolean z2 = false;
        if (!canShowAd()) {
            setNotDisplayedReason(NotDisplayedReason.SERVING_ADS_DISABLED);
            this.callback.adNotDisplayed(this);
            return false;
        }
        if (this.adKey == null) {
            loadAd(this.adMode, this.adPreferences);
        }
        if (isAppOnForeground()) {
            if (isNetworkAvailable()) {
                z = true;
                if (isReady()) {
                    Placement placement = getPlacement();
                    fVar = shouldDisplayAd(str, placement);
                    if (fVar.a()) {
                        this.ad = a.a().a(this.adKey);
                        if (this.ad != null) {
                            if (this.placement != Placement.INAPP_SPLASH || !a.f4110a.l()) {
                                boolean a2 = this.ad.a(str);
                                if (a2) {
                                    com.startapp.android.publish.adsCommon.b.b.a().a(new com.startapp.android.publish.adsCommon.b.a(placement, str));
                                    if (this.adMode == null || this.placement == Placement.INAPP_SPLASH || (this.adPreferences != null && !this.adPreferences.equals(new AdPreferences()))) {
                                        z = false;
                                    }
                                    if (z) {
                                        a.a();
                                        k.b(this.context, a.a(this.adMode), Integer.valueOf(0));
                                        if (this.adMode == AdMode.AUTOMATIC) {
                                            k.b(this.context, a.a(AdMode.FULLPAGE), Integer.valueOf(0));
                                            k.b(this.context, a.a(AdMode.OFFERWALL), Integer.valueOf(0));
                                        }
                                    }
                                } else if (this.ad instanceof Ad) {
                                    setNotDisplayedReason(((Ad) this.ad).getNotDisplayedReason());
                                }
                                loadAd(this.adMode, this.adPreferences, null);
                                z2 = a2;
                            }
                        }
                    } else {
                        setNotDisplayedReason(NotDisplayedReason.AD_RULES);
                        Constants.a();
                    }
                } else {
                    if (this.adMode == AdMode.REWARDED_VIDEO || this.adMode == AdMode.VIDEO || !showPreparedVideoFallbackAd(str)) {
                        z = false;
                    }
                    if (!z) {
                        setNotDisplayedReason(NotDisplayedReason.AD_NOT_READY);
                    }
                    fVar = null;
                    if (z2 || z) {
                        registerBroadcastReceiver("com.startapp.android.HideDisplayBroadcastListener");
                        registerBroadcastReceiver("com.startapp.android.ShowDisplayBroadcastListener");
                        registerBroadcastReceiver("com.startapp.android.ShowFailedDisplayBroadcastListener");
                        registerBroadcastReceiver("com.startapp.android.OnClickCallback");
                        registerBroadcastReceiver("com.startapp.android.OnVideoCompleted");
                    }
                    if (!z2) {
                        if (getNotDisplayedReason() == null) {
                            setNotDisplayedReason(NotDisplayedReason.INTERNAL_ERROR);
                        }
                        if (getNotDisplayedReason() != NotDisplayedReason.NETWORK_PROBLEM) {
                            if (getNotDisplayedReason() != NotDisplayedReason.AD_RULES) {
                                if (z) {
                                    c.a(this.context, c.a(this.ad != null ? this.ad : a.a().b(this.adKey)), str, NotDisplayedReason.AD_NOT_READY_VIDEO_FALLBACK.toString());
                                } else {
                                    c.a(this.context, c.a(this.ad != null ? this.ad : a.a().b(this.adKey)), str, getNotDisplayedReason().toString());
                                }
                            } else if (fVar != null) {
                                c.a(this.context, c.a(a.a().b(this.adKey)), str, fVar.c());
                            }
                        }
                        this.ad = null;
                        if (!z && this.callback != null) {
                            this.callback.adNotDisplayed(this);
                        }
                    }
                    return z2;
                }
            } else {
                setNotDisplayedReason(NotDisplayedReason.NETWORK_PROBLEM);
                fVar = null;
            }
            z = false;
            registerBroadcastReceiver("com.startapp.android.HideDisplayBroadcastListener");
            registerBroadcastReceiver("com.startapp.android.ShowDisplayBroadcastListener");
            registerBroadcastReceiver("com.startapp.android.ShowFailedDisplayBroadcastListener");
            registerBroadcastReceiver("com.startapp.android.OnClickCallback");
            registerBroadcastReceiver("com.startapp.android.OnVideoCompleted");
            if (!z2) {
            }
            return z2;
        }
        fVar = null;
        setNotDisplayedReason(NotDisplayedReason.APP_IN_BACKGROUND);
        z = false;
        registerBroadcastReceiver("com.startapp.android.HideDisplayBroadcastListener");
        registerBroadcastReceiver("com.startapp.android.ShowDisplayBroadcastListener");
        registerBroadcastReceiver("com.startapp.android.ShowFailedDisplayBroadcastListener");
        registerBroadcastReceiver("com.startapp.android.OnClickCallback");
        registerBroadcastReceiver("com.startapp.android.OnVideoCompleted");
        if (!z2) {
        }
        return z2;
    }

    private boolean showPreparedVideoFallbackAd(String str) {
        AdPreferences adPreferences2;
        if (!canShowAd() || !b.a().H().h()) {
            return false;
        }
        if (this.adPreferences == null) {
            adPreferences2 = new AdPreferences();
        } else {
            adPreferences2 = this.adPreferences;
        }
        adPreferences2.setType(AdType.NON_VIDEO);
        Placement placement = getPlacement();
        g b = a.a().b(new c(placement, adPreferences2));
        if (b == null || !b.isReady() || !shouldDisplayAd(str, placement).a()) {
            return false;
        }
        b.setVideoCancelCallBack(true);
        Constants.a();
        return b.a(str);
    }

    /* access modifiers changed from: protected */
    public f shouldDisplayAd(String str, Placement placement) {
        return b.a().F().a(placement, str);
    }

    /* access modifiers changed from: protected */
    public Placement getPlacement() {
        Placement placement = super.getPlacement();
        return (placement != null || this.adKey == null || a.a().b(this.adKey) == null) ? placement : ((Ad) a.a().b(this.adKey)).getPlacement();
    }

    /* access modifiers changed from: protected */
    public String getAdHtml() {
        g b = a.a().b(this.adKey);
        if (b == null || !(b instanceof HtmlAd)) {
            return null;
        }
        return ((HtmlAd) b).getHtml();
    }

    private void registerBroadcastReceiver(String str) {
        b.a(this.context).a(this.callbackBroadcastReceiver, new IntentFilter(str));
    }

    @Deprecated
    public boolean show() {
        return show(null, null);
    }

    private void setAdMode(AdMode adMode2) {
        this.adMode = adMode2;
    }

    private void setAdPrefs(AdPreferences adPreferences2) {
        this.adPreferences = adPreferences2;
    }

    public void loadAd() {
        loadAd(AdMode.AUTOMATIC, new AdPreferences(), null);
    }

    public void loadAd(AdPreferences adPreferences2) {
        loadAd(AdMode.AUTOMATIC, adPreferences2, null);
    }

    public void loadAd(AdEventListener adEventListener) {
        loadAd(AdMode.AUTOMATIC, new AdPreferences(), adEventListener);
    }

    public void loadAd(AdPreferences adPreferences2, AdEventListener adEventListener) {
        loadAd(AdMode.AUTOMATIC, adPreferences2, adEventListener);
    }

    public void loadAd(AdMode adMode2) {
        loadAd(adMode2, new AdPreferences(), null);
    }

    public void loadAd(AdMode adMode2, AdPreferences adPreferences2) {
        loadAd(adMode2, adPreferences2, null);
    }

    public void loadAd(AdMode adMode2, AdEventListener adEventListener) {
        loadAd(adMode2, new AdPreferences(), adEventListener);
    }

    public void loadAd(AdMode adMode2, AdPreferences adPreferences2, AdEventListener adEventListener) {
        setAdMode(adMode2);
        setAdPrefs(adPreferences2);
        try {
            load(adPreferences2, adEventListener);
        } catch (Exception e) {
            com.startapp.android.publish.adsCommon.g.f.a(this.context, d.EXCEPTION, "StartAppAd.loadAd - unexpected Error occurd", e.getMessage(), "");
            if (adEventListener != null) {
                adEventListener.onFailedToReceiveAd(this);
            }
        }
    }

    public boolean showAd() {
        return showAd(null, null);
    }

    public boolean showAd(String str) {
        return showAd(str, null);
    }

    public boolean showAd(AdDisplayListener adDisplayListener) {
        return showAd(null, adDisplayListener);
    }

    public boolean showAd(String str, AdDisplayListener adDisplayListener) {
        try {
            return show(str, adDisplayListener);
        } catch (Exception e) {
            com.startapp.android.publish.adsCommon.g.f.a(this.context, d.EXCEPTION, "StartAppAd.showAd - unexpected Error occurd", e.getMessage(), "");
            setNotDisplayedReason(NotDisplayedReason.INTERNAL_ERROR);
            if (adDisplayListener != null) {
                adDisplayListener.adNotDisplayed(null);
            }
            return false;
        }
    }

    public void setVideoListener(VideoListener videoListener2) {
        this.videoListener = videoListener2;
    }

    public void onResume() {
        if (!isReady()) {
            loadAd();
        }
    }

    public void onBackPressed() {
        showAd("exit_ad");
        a.f4110a.k();
    }

    public void onSaveInstanceState(Bundle bundle) {
        int i;
        switch (this.adMode) {
            case FULLPAGE:
                i = 1;
                break;
            case OFFERWALL:
                i = 2;
                break;
            case OVERLAY:
                i = 3;
                break;
            case REWARDED_VIDEO:
                i = 4;
                break;
            default:
                i = 0;
                break;
        }
        if (this.adPreferences != null) {
            bundle.putSerializable("AdPrefs", this.adPreferences);
        }
        bundle.putInt("AdMode", i);
    }

    public void onRestoreInstanceState(Bundle bundle) {
        int i = bundle.getInt("AdMode");
        this.adMode = AdMode.AUTOMATIC;
        if (i == 1) {
            this.adMode = AdMode.FULLPAGE;
        } else if (i == 2) {
            this.adMode = AdMode.OFFERWALL;
        } else if (i == 3) {
            this.adMode = AdMode.OVERLAY;
        } else if (i == 4) {
            this.adMode = AdMode.REWARDED_VIDEO;
        } else if (i == 5) {
            this.adMode = AdMode.VIDEO;
        }
        Serializable serializable = bundle.getSerializable("AdPrefs");
        if (serializable != null) {
            this.adPreferences = (AdPreferences) serializable;
        }
    }

    public void close() {
        if (this.callbackBroadcastReceiver != null) {
            b.a(this.context).a(this.callbackBroadcastReceiver);
        }
        b.a(this.context).a(new Intent("com.startapp.android.CloseAdActivity"));
    }

    public boolean isReady() {
        g b = a.a().b(this.adKey);
        if (b != null) {
            return b.isReady();
        }
        return false;
    }

    public boolean isNetworkAvailable() {
        return j.a(this.context);
    }

    private boolean isAppOnForeground() {
        boolean z = true;
        try {
            if (b.a().N() && !j.e(this.context)) {
                z = false;
            }
            return z;
        } catch (Exception unused) {
            return true;
        }
    }

    public c loadSplash(AdPreferences adPreferences2, AdEventListener adEventListener) {
        this.adKey = a.a().a(this.context, this, adPreferences2, adEventListener);
        return this.adKey;
    }

    public static void showSplash(Activity activity, Bundle bundle) {
        showSplash(activity, bundle, new SplashConfig());
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig) {
        showSplash(activity, bundle, splashConfig, new AdPreferences());
    }

    public static void showSplash(Activity activity, Bundle bundle, AdPreferences adPreferences2) {
        showSplash(activity, bundle, new SplashConfig(), adPreferences2);
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2) {
        showSplash(activity, bundle, splashConfig, adPreferences2, null);
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2, SplashHideListener splashHideListener) {
        showSplash(activity, bundle, splashConfig, adPreferences2, splashHideListener, true);
    }

    static void showSplash(final Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2, final SplashHideListener splashHideListener, boolean z) {
        if (bundle == null && e.getInstance().canShowAd()) {
            try {
                m a2 = a.f4110a;
                if (!a2.i() && z) {
                    a2.c(true);
                }
                a2.b(z);
                if (!z) {
                    if (adPreferences2 == null) {
                        adPreferences2 = new AdPreferences();
                    }
                    adPreferences2.setAs(Boolean.TRUE);
                }
                splashConfig.setDefaults(activity);
                j.a(activity, true);
                Intent intent = new Intent(activity, j.a((Context) activity, OverlayActivity.class, AppWallActivity.class));
                intent.putExtra("SplashConfig", splashConfig);
                intent.putExtra("AdPreference", adPreferences2);
                intent.putExtra("testMode", testMode);
                intent.putExtra(Events.CREATIVE_FULLSCREEN, c.a(activity));
                intent.putExtra("placement", Placement.INAPP_SPLASH.getIndex());
                intent.addFlags(1140883456);
                activity.startActivity(intent);
                b.a((Context) activity).a(new BroadcastReceiver() {
                    public final void onReceive(Context context, Intent intent) {
                        j.a(activity, false);
                        if (splashHideListener != null) {
                            splashHideListener.splashHidden();
                        }
                        b.a((Context) activity).a((BroadcastReceiver) this);
                    }
                }, new IntentFilter("com.startapp.android.splashHidden"));
            } catch (Exception e) {
                if (splashHideListener != null) {
                    splashHideListener.splashHidden();
                    com.startapp.android.publish.adsCommon.g.f.a(activity, d.EXCEPTION, "StartAppAd.showSplash - unexpected Error occurd", e.getMessage(), "");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getLauncherName() {
        g b = a.a().b(this.adKey);
        if (b != null) {
            return b.getLauncherName();
        }
        return j.c(getContext());
    }

    public AdState getState() {
        g b = a.a().b(this.adKey);
        if (b != null) {
            return b.getState();
        }
        return AdState.UN_INITIALIZED;
    }

    public boolean isBelowMinCPM() {
        g b = a.a().b(this.adKey);
        if (b != null) {
            return b.isBelowMinCPM();
        }
        return false;
    }

    public static boolean showAd(Context context) {
        try {
            return new StartAppAd(context).showAd();
        } catch (Exception e) {
            com.startapp.android.publish.adsCommon.g.f.a(context, d.EXCEPTION, "StartAppAd.showAd(one line integration) - unexpected Error occurd", e.getMessage(), "");
            return false;
        }
    }

    public static void onBackPressed(Context context) {
        new StartAppAd(context).onBackPressed();
    }

    public static void disableSplash() {
        a.f4110a.c(false);
    }

    public static void enableAutoInterstitial() {
        a.f4068a.a();
    }

    public static void disableAutoInterstitial() {
        a.f4068a.b();
    }

    public static void setAutoInterstitialPreferences(AutoInterstitialPreferences autoInterstitialPreferences) {
        a.f4068a.a(autoInterstitialPreferences);
    }
}
