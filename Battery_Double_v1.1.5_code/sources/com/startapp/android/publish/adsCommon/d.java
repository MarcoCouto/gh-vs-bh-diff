package com.startapp.android.publish.adsCommon;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import com.facebook.places.model.PlaceFields;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.a.h;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.common.a.b.C0094b;
import com.startapp.common.a.c;
import com.startapp.common.a.g;
import com.startapp.common.a.g.b;
import com.tapjoy.TapjoyConstants;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/* compiled from: StartAppSDK */
public abstract class d {
    private static final String OS = "android";
    private String age;
    private String androidId;
    private int appCode;
    private boolean appDebug;
    private Boolean appOnForeground;
    private String appVersion;
    private String blat;
    private String blon;
    private String bssid;
    private String cellSignalLevel;
    private String cellTimingAdv;
    private String cid;
    private String clientSessionId;
    private float density;
    private String deviceIP;
    private String deviceVersion;
    private Map<String, String> frameworksMap = new TreeMap();
    private int height;
    private Set<String> inputLangs;
    private String installerPkg;
    private String isp;
    private String ispName;
    private String lac;
    private String locale;
    private List<Location> locations = null;
    private String manufacturer;
    private String model;
    private String netOper;
    private String networkOperName;
    private String networkType;
    private String os = "android";
    private String packageId;
    private Map<String, String> parameters = new HashMap();
    private String personalizedAdsServing;
    private String productId;
    private String publisherId;
    protected Integer retry;
    private Boolean roaming;
    private boolean root;
    private long sdkFlavor = new BigInteger(AdsConstants.d, 2).longValue();
    private int sdkId = 3;
    private String sdkVersion = AdsConstants.c;
    private String signalLevel;
    private boolean simulator;
    private String ssid;
    private String subProductId;
    private String subPublisherId;
    private Boolean unknownSourcesAllowed;
    private boolean usbDebug;
    private C0094b userAdvertisingId;
    private String wfScanRes;
    private int width;
    private String wifiRSSILevel;
    private String wifiSignalLevel;

    /* compiled from: StartAppSDK */
    static class a {

        /* renamed from: a reason: collision with root package name */
        private ScanResult f4059a;

        public a(ScanResult scanResult) {
            this.f4059a = scanResult;
        }

        public final String toString() {
            CharSequence charSequence;
            StringBuilder sb = new StringBuilder();
            if (this.f4059a != null) {
                sb.append(this.f4059a.SSID);
                sb.append(',');
                sb.append(this.f4059a.BSSID);
                sb.append(',');
                sb.append(WifiManager.calculateSignalLevel(this.f4059a.level, 5));
                sb.append(',');
                sb.append(this.f4059a.level);
                sb.append(',');
                long currentTimeMillis = VERSION.SDK_INT >= 17 ? (System.currentTimeMillis() - SystemClock.elapsedRealtime()) + (this.f4059a.timestamp / 1000) : 0;
                if (currentTimeMillis != 0) {
                    sb.append(currentTimeMillis);
                }
                sb.append(',');
                ScanResult scanResult = this.f4059a;
                if (VERSION.SDK_INT >= 23) {
                    charSequence = scanResult.venueName;
                } else {
                    charSequence = "";
                }
                if (charSequence != null) {
                    sb.append(charSequence);
                }
            }
            return sb.toString();
        }
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public void setPublisherId(String str) {
        this.publisherId = str;
    }

    public long getSdkFlavor() {
        return this.sdkFlavor;
    }

    public void setSdkFlavor(long j) {
        this.sdkFlavor = j;
    }

    public String getPackageId() {
        return this.packageId;
    }

    public void setPackageId(String str) {
        this.packageId = str;
    }

    public String getAndroidId() {
        return this.androidId;
    }

    private void setAndroidId(Context context) {
        if (c.a(context, "com.google.android.gms", 0)) {
            try {
                if (Integer.parseInt(Integer.toString(context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode).substring(0, 1)) >= 4) {
                    this.androidId = Secure.getString(context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
                }
            } catch (NameNotFoundException unused) {
            } catch (Exception unused2) {
            }
        }
    }

    public String getInstallerPkg() {
        return this.installerPkg;
    }

    public void setInstallerPkg(String str) {
        this.installerPkg = str;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String str) {
        this.productId = str;
    }

    public String getNetworkType() {
        return this.networkType;
    }

    public String getSignalLevel() {
        return this.signalLevel;
    }

    private void setNetworkType(Context context) {
        this.networkType = g.a(context);
    }

    private void setNetworkLevels(Context context) {
        try {
            this.signalLevel = "e106";
            this.cellSignalLevel = "e106";
            this.wifiSignalLevel = "e106";
            this.wifiRSSILevel = "e106";
            com.startapp.common.c a2 = com.startapp.common.c.a();
            if (a2 != null) {
                this.cellSignalLevel = a2.b();
            }
            b bVar = null;
            if (e.getInstance().isWfScanEnabled()) {
                bVar = g.a(context, this.networkType);
            }
            if (bVar == null) {
                this.signalLevel = this.cellSignalLevel;
            } else if (bVar.a() == null) {
                this.signalLevel = bVar.c();
                this.wifiRSSILevel = bVar.b();
                this.wifiSignalLevel = bVar.c();
            } else {
                this.signalLevel = bVar.a();
                this.wifiRSSILevel = bVar.a();
                this.wifiSignalLevel = bVar.a();
            }
        } catch (Exception unused) {
        }
    }

    public String getCellSignalLevel() {
        return this.cellSignalLevel;
    }

    public String getWifiSignalLevel() {
        return this.wifiSignalLevel;
    }

    public String getWifiRssiLevel() {
        return this.wifiRSSILevel;
    }

    public C0094b getUserAdvertisingId() {
        return this.userAdvertisingId;
    }

    public String getAge() {
        return this.age;
    }

    public void setAge(String str) {
        this.age = str;
    }

    public void setUserAdvertisingId(C0094b bVar) {
        this.userAdvertisingId = bVar;
    }

    public String getIsp() {
        return this.isp;
    }

    public void setIsp(String str) {
        this.isp = str;
    }

    public String getIspName() {
        return this.ispName;
    }

    public void setIspName(String str) {
        this.ispName = str;
    }

    public String getNetOper() {
        return this.netOper;
    }

    public void setNetOper(String str) {
        this.netOper = str;
    }

    public String getNetworkOperName() {
        return this.networkOperName;
    }

    public void setNetworkOperName(String str) {
        this.networkOperName = str;
    }

    public String getCid() {
        return this.cid;
    }

    public void setCid(String str) {
        this.cid = str;
    }

    public String getLac() {
        return this.lac;
    }

    public void setLac(String str) {
        this.lac = str;
    }

    public String getBlat() {
        return this.blat;
    }

    public void setBlat(String str) {
        this.blat = str;
    }

    public String getBlon() {
        return this.blon;
    }

    public void setBlon(String str) {
        this.blon = str;
    }

    public String getSsid() {
        return this.ssid;
    }

    public void setSsid(String str) {
        this.ssid = str;
    }

    public String getBssid() {
        return this.bssid;
    }

    public void setBssid(String str) {
        this.bssid = str;
    }

    public String getWfScanRes() {
        return this.wfScanRes;
    }

    public void setWfScanRes(String str) {
        this.wfScanRes = str;
    }

    public String getModel() {
        return this.model;
    }

    public void setRetry(int i) {
        this.retry = null;
    }

    public void setModel(String str) {
        this.model = str;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public String getDeviceVersion() {
        return this.deviceVersion;
    }

    public void setDeviceVersion(String str) {
        this.deviceVersion = str;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setLocale(String str) {
        this.locale = str;
    }

    public String getSubPublisherId() {
        return this.subPublisherId;
    }

    public void setSubPublisherId(String str) {
        this.subPublisherId = str;
    }

    public String getSubProductId() {
        return this.subProductId;
    }

    public void setSubProductId(String str) {
        this.subProductId = str;
    }

    public String getOs() {
        return this.os;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public String getSdkVersion() {
        return this.sdkVersion;
    }

    public void setSdkVersion(String str) {
        this.sdkVersion = str;
    }

    public String getSessionId() {
        if (this.clientSessionId == null) {
            return "";
        }
        return this.clientSessionId;
    }

    public void setSessionId(String str) {
        this.clientSessionId = str;
    }

    public Boolean isRoaming() {
        return this.roaming;
    }

    public void setRoaming(Context context) {
        this.roaming = g.b(context);
    }

    public String getDeviceIP() {
        return this.deviceIP;
    }

    public void setDeviceIP(WifiInfo wifiInfo) {
        this.deviceIP = g.a(wifiInfo);
    }

    public Boolean isUnknownSourcesAllowed() {
        return this.unknownSourcesAllowed;
    }

    public void setUnknownSourcesAllowed(Boolean bool) {
        this.unknownSourcesAllowed = bool;
    }

    public float getDensity() {
        return this.density;
    }

    public void setDensity(float f) {
        this.density = f;
    }

    public Boolean isAppOnForeground() {
        return this.appOnForeground;
    }

    public void setAppOnForeground(Context context) {
        try {
            this.appOnForeground = Boolean.valueOf(j.e(context));
        } catch (Exception unused) {
            this.appOnForeground = null;
        }
    }

    public Set<String> getInputLangs() {
        return this.inputLangs;
    }

    public void setInputLangs(Set<String> set) {
        this.inputLangs = set;
    }

    public String getAppVersion() {
        return this.appVersion;
    }

    public void setAppVersion(String str) {
        this.appVersion = str;
    }

    public int getAppCode() {
        return this.appCode;
    }

    public void setAppCode(int i) {
        this.appCode = i;
    }

    public List<Location> getLocations() {
        return this.locations;
    }

    public void setLocations(List<Location> list) {
        this.locations = list;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BaseRequest [parameters=");
        sb.append(this.parameters);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public void fillApplicationDetails(Context context, AdPreferences adPreferences) {
        fillApplicationDetails(context, adPreferences, true);
    }

    public void fillApplicationDetails(Context context, AdPreferences adPreferences, boolean z) {
        setPublisherId(adPreferences.getPublisherId());
        setProductId(adPreferences.getProductId());
        this.frameworksMap = k.b(context, "sharedPrefsWrappers", this.frameworksMap);
        setAge(adPreferences.getAge(context));
        if (!e.getInstance().getDisableSendAdvertisingId()) {
            C0094b a2 = com.startapp.common.a.b.a().a(context);
            String a3 = a2.a();
            if (a3.equals("0") || a3.equals("")) {
                if (!k.a(context, com.startapp.android.publish.adsCommon.g.d.NO_ADVERTISING_ID.a(), Boolean.FALSE).booleanValue()) {
                    k.b(context, com.startapp.android.publish.adsCommon.g.d.NO_ADVERTISING_ID.a(), Boolean.TRUE);
                    f.a(context, com.startapp.android.publish.adsCommon.g.d.NO_ADVERTISING_ID, "BaseRequest.fillApplicationDetails", a2.d(), "");
                }
                setAndroidId(context);
            }
            setUserAdvertisingId(a2);
        }
        setPackageId(context.getPackageName());
        setInstallerPkg(j.d(context));
        setManufacturer(Build.MANUFACTURER);
        setModel(Build.MODEL);
        setDeviceVersion(Integer.toString(VERSION.SDK_INT));
        setLocale(context.getResources().getConfiguration().locale.toString());
        setInputLangs(c.b(context));
        setWidth(context.getResources().getDisplayMetrics().widthPixels);
        setHeight(context.getResources().getDisplayMetrics().heightPixels);
        setDensity(context.getResources().getDisplayMetrics().density);
        setAppOnForeground(context);
        setSessionId(h.d().a());
        setUnknownSourcesAllowed(Boolean.valueOf(c.a(context)));
        setRoaming(context);
        setNetworkType(context);
        setNetworkLevels(context);
        setAppVersion(c.d(context));
        setAppCode(c.c(context));
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                fillSimDetails(telephonyManager);
                fillNetworkOperatorDetails(context, telephonyManager);
                fillCellLocationDetails(context, telephonyManager);
                setCellTimingAdv(context, telephonyManager);
            }
        } catch (Exception unused) {
        }
        fillWifiDetails(context, z);
        this.usbDebug = c.f(context);
        this.root = c.g(context);
        this.simulator = c.h(context);
        this.appDebug = (context.getApplicationInfo().flags & 2) != 0;
        this.personalizedAdsServing = k.a(context, "USER_CONSENT_PERSONALIZED_ADS_SERVING", (String) null);
    }

    public com.startapp.android.publish.adsCommon.a.f getNameValueJson() {
        com.startapp.android.publish.adsCommon.a.c cVar = new com.startapp.android.publish.adsCommon.a.c();
        addParams(cVar);
        return cVar;
    }

    public com.startapp.android.publish.adsCommon.a.f getNameValueMap() {
        com.startapp.android.publish.adsCommon.a.d dVar = new com.startapp.android.publish.adsCommon.a.d();
        addParams(dVar);
        return dVar;
    }

    private void addParams(com.startapp.android.publish.adsCommon.a.f fVar) {
        fVar.a("publisherId", this.publisherId, false);
        fVar.a("productId", this.productId, true);
        fVar.a("os", this.os, true);
        fVar.a(GeneralPropertiesWorker.SDK_VERSION, this.sdkVersion, false);
        fVar.a("flavor", Long.valueOf(this.sdkFlavor), false);
        if (this.frameworksMap != null && !this.frameworksMap.isEmpty()) {
            String str = "";
            for (String str2 : this.frameworksMap.keySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(str2);
                sb.append(":");
                sb.append((String) this.frameworksMap.get(str2));
                sb.append(";");
                str = sb.toString();
            }
            fVar.a("frameworksData", str.substring(0, str.length() - 1), false, false);
        }
        fVar.a("packageId", this.packageId, false);
        fVar.a("installerPkg", this.installerPkg, false);
        fVar.a(IronSourceSegment.AGE, this.age, false);
        if (this.userAdvertisingId != null) {
            fVar.a("userAdvertisingId", this.userAdvertisingId.a(), false);
            if (this.userAdvertisingId.b()) {
                fVar.a("limat", Boolean.valueOf(this.userAdvertisingId.b()), false);
            }
            fVar.a("advertisingIdSource", this.userAdvertisingId.c(), false);
        } else if (this.androidId != null) {
            fVar.a("userId", this.androidId, false);
        }
        fVar.a("model", this.model, false);
        fVar.a("manufacturer", this.manufacturer, false);
        fVar.a("deviceVersion", this.deviceVersion, false);
        fVar.a("locale", this.locale, false);
        fVar.a("inputLangs", this.inputLangs);
        fVar.a("isp", this.isp, false);
        fVar.a("ispName", this.ispName, false);
        fVar.a("netOper", getNetOper(), false);
        fVar.a("networkOperName", getNetworkOperName(), false);
        fVar.a("cid", getCid(), false);
        fVar.a("lac", getLac(), false);
        fVar.a("blat", getBlat(), false);
        fVar.a("blon", getBlon(), false);
        fVar.a("ssid", getSsid(), false);
        fVar.a("bssid", getBssid(), false);
        fVar.a("wfScanRes", getWfScanRes(), false);
        fVar.a("subPublisherId", this.subPublisherId, false);
        fVar.a("subProductId", this.subProductId, false);
        fVar.a("retryCount", this.retry, false);
        fVar.a("roaming", isRoaming(), false);
        fVar.a("deviceIP", getDeviceIP(), false);
        fVar.a("grid", getNetworkType(), false);
        fVar.a("silev", getSignalLevel(), false);
        fVar.a("cellSignalLevel", getCellSignalLevel(), false);
        if (getWifiSignalLevel() != null) {
            fVar.a("wifiSignalLevel", getWifiSignalLevel(), false);
        }
        if (getWifiRssiLevel() != null) {
            fVar.a("wifiRssiLevel", getWifiRssiLevel(), false);
        }
        if (getCellTimingAdv() != null) {
            fVar.a("cellTimingAdv", getCellTimingAdv(), false);
        }
        fVar.a("outsource", isUnknownSourcesAllowed(), false);
        fVar.a("width", String.valueOf(this.width), false);
        fVar.a("height", String.valueOf(this.height), false);
        fVar.a("density", String.valueOf(this.density), false);
        fVar.a("fgApp", isAppOnForeground(), false);
        fVar.a("sdkId", String.valueOf(this.sdkId), true);
        fVar.a("clientSessionId", this.clientSessionId, false);
        fVar.a(RequestParameters.APPLICATION_VERSION_NAME, this.appVersion, false);
        fVar.a("appCode", Integer.valueOf(this.appCode), false);
        fVar.a("timeSinceBoot", Long.valueOf(getTimeSinceBoot()), false);
        if (getLocations() != null && getLocations().size() > 0) {
            String a2 = com.startapp.common.a.f.a(getLocations());
            if (a2 != null && !a2.equals("")) {
                fVar.a("locations", com.startapp.common.a.a.c(a2), false);
            }
        }
        fVar.a("udbg", Boolean.valueOf(this.usbDebug), false);
        fVar.a("root", Boolean.valueOf(this.root), false);
        fVar.a("smltr", Boolean.valueOf(this.simulator), false);
        fVar.a("isddbg", Boolean.valueOf(this.appDebug), false);
        fVar.a("pas", this.personalizedAdsServing, false);
    }

    public String getRequestString() {
        return getNameValueMap().toString();
    }

    private void fillSimDetails(TelephonyManager telephonyManager) {
        if (telephonyManager.getSimState() == 5) {
            setIsp(telephonyManager.getSimOperator());
            setIspName(telephonyManager.getSimOperatorName());
        }
    }

    private void fillNetworkOperatorDetails(Context context, TelephonyManager telephonyManager) {
        int phoneType = telephonyManager.getPhoneType();
        if (phoneType != 0 && phoneType != 2) {
            String networkOperator = telephonyManager.getNetworkOperator();
            if (networkOperator != null) {
                setNetOper(com.startapp.common.a.a.c(networkOperator));
            }
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            if (networkOperatorName != null) {
                setNetworkOperName(com.startapp.common.a.a.c(networkOperatorName));
            }
        }
    }

    private void fillCellLocationDetails(Context context, TelephonyManager telephonyManager) {
        if (c.a(context, "android.permission.ACCESS_FINE_LOCATION") || c.a(context, "android.permission.ACCESS_COARSE_LOCATION")) {
            CellLocation cellLocation = telephonyManager.getCellLocation();
            if (cellLocation != null) {
                if (cellLocation instanceof GsmCellLocation) {
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                    setCid(com.startapp.common.a.a.c(String.valueOf(gsmCellLocation.getCid())));
                    setLac(com.startapp.common.a.a.c(String.valueOf(gsmCellLocation.getLac())));
                } else if (cellLocation instanceof CdmaCellLocation) {
                    CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                    setBlat(com.startapp.common.a.a.c(String.valueOf(cdmaCellLocation.getBaseStationLatitude())));
                    setBlon(com.startapp.common.a.a.c(String.valueOf(cdmaCellLocation.getBaseStationLongitude())));
                }
            }
        }
    }

    private String getCellTimingAdv() {
        return this.cellTimingAdv;
    }

    private void setCellTimingAdv(Context context, TelephonyManager telephonyManager) {
        this.cellTimingAdv = c.a(context, telephonyManager);
    }

    private static long getTimeSinceBoot() {
        return SystemClock.elapsedRealtime();
    }

    public void fillLocationDetails(AdPreferences adPreferences, Context context) {
        boolean z;
        this.locations = new ArrayList();
        if (adPreferences == null || adPreferences.getLatitude() == null || adPreferences.getLongitude() == null) {
            z = false;
        } else {
            Location location = new Location("loc");
            location.setLongitude(adPreferences.getLongitude().doubleValue());
            location.setLongitude(adPreferences.getLongitude().doubleValue());
            location.setProvider(e.DEFAULT_LOCATION_SOURCE);
            this.locations.add(location);
            z = true;
        }
        a.f4110a;
        m.c(context);
        List a2 = com.startapp.common.a.f.a(context, e.getInstance().getLocationConfig().isFiEnabled(), e.getInstance().getLocationConfig().isCoEnabled());
        if (a2 != null && a2.size() > 0) {
            this.locations.addAll(a2);
            z = true;
        }
        setUsingLocation(context, z);
    }

    public static void setUsingLocation(Context context, boolean z) {
        k.b(context, "shared_prefs_using_location", Boolean.valueOf(z));
    }

    private void fillWifiDetails(Context context, boolean z) {
        try {
            if (e.getInstance().isWfScanEnabled()) {
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi");
                if (wifiManager != null && c.a(context, "android.permission.ACCESS_WIFI_STATE")) {
                    if (getNetworkType().equals("WIFI")) {
                        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                        if (connectionInfo != null) {
                            setDeviceIP(connectionInfo);
                            String ssid2 = connectionInfo.getSSID();
                            String bssid2 = connectionInfo.getBSSID();
                            if (ssid2 != null) {
                                setSsid(com.startapp.common.a.a.c(ssid2));
                            }
                            if (bssid2 != null) {
                                setBssid(com.startapp.common.a.a.c(bssid2));
                            }
                        }
                    }
                    if (z) {
                        List a2 = c.a(context, wifiManager);
                        if (a2 != null && !a2.equals(Collections.EMPTY_LIST)) {
                            ArrayList arrayList = new ArrayList();
                            for (int i = 0; i < Math.min(5, a2.size()); i++) {
                                arrayList.add(new a((ScanResult) a2.get(i)));
                            }
                            setWfScanRes(com.startapp.common.a.a.c(TextUtils.join(";", arrayList)));
                        }
                    }
                }
            }
        } catch (Exception unused) {
        }
    }
}
