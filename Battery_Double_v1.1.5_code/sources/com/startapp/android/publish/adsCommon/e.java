package com.startapp.android.publish.adsCommon;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.util.Pair;
import com.startapp.android.publish.adsCommon.Ad.AdState;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.adListeners.b;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.common.model.GetAdRequest;
import com.startapp.common.f;
import com.startapp.common.f.a;

/* compiled from: StartAppSDK */
public abstract class e {

    /* renamed from: a reason: collision with root package name */
    protected final Context f4063a;
    /* access modifiers changed from: protected */
    public final Ad b;
    protected final AdPreferences c;
    /* access modifiers changed from: protected */
    public final AdEventListener d;
    protected Placement e;
    protected String f = null;

    /* access modifiers changed from: protected */
    public abstract boolean a(Object obj);

    /* access modifiers changed from: protected */
    public abstract Object e();

    public e(Context context, Ad ad, AdPreferences adPreferences, AdEventListener adEventListener, Placement placement) {
        this.f4063a = context;
        this.b = ad;
        this.c = adPreferences;
        this.d = new b(adEventListener);
        this.e = placement;
    }

    public final void c() {
        f.a(a.HIGH, (Runnable) new Runnable() {
            public final void run() {
                Process.setThreadPriority(10);
                final Boolean d = e.this.d();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        e.this.a(d);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public final Boolean d() {
        return Boolean.valueOf(a(e()));
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        b(bool);
        if (!bool.booleanValue()) {
            this.b.setErrorMessage(this.f);
            this.d.onFailedToReceiveAd(this.b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(Boolean bool) {
        this.b.setState(bool.booleanValue() ? AdState.READY : AdState.UN_INITIALIZED);
    }

    /* access modifiers changed from: protected */
    public GetAdRequest a() {
        return b(new GetAdRequest());
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest b(GetAdRequest getAdRequest) {
        Pair d2 = l.d(this.f4063a);
        try {
            getAdRequest.fillAdPreferences(this.f4063a, this.c, this.e, d2);
            if (!b.a().D() && c.a(this.f4063a, this.e)) {
                getAdRequest.setDisableTwoClicks(true);
            }
            try {
                getAdRequest.fillApplicationDetails(this.f4063a, this.c, false);
            } catch (Exception e2) {
                com.startapp.android.publish.adsCommon.g.f.a(this.f4063a, d.EXCEPTION, "BaseService.GetAdRequest - fillApplicationDetails failed", e2.getMessage(), "");
            }
            return getAdRequest;
        } catch (Exception unused) {
            l.a(d2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final Placement f() {
        return this.e;
    }
}
