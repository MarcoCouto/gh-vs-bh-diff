package com.startapp.android.publish.adsCommon.c;

import android.content.Context;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.common.f;
import com.startapp.common.f.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private List<a> f4057a;
    private Context b;
    private List<String> c = new ArrayList();

    public b(Context context, List<a> list) {
        this.f4057a = list;
        this.b = context;
    }

    public final void a() {
        f.a(a.DEFAULT, (Runnable) new Runnable() {
            public final void run() {
                b.this.b();
            }
        });
    }

    /* access modifiers changed from: protected */
    public final Boolean b() {
        boolean z = false;
        try {
            List<a> list = this.f4057a;
            List<String> list2 = this.c;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (a aVar : list) {
                if (!aVar.c()) {
                    String str = aVar.a().split("tracking/adImpression[?]d=")[1];
                    if (aVar.d()) {
                        arrayList.add("d=".concat(String.valueOf(str)));
                    } else {
                        arrayList2.add("d=".concat(String.valueOf(str)));
                    }
                }
            }
            StringBuilder sb = new StringBuilder("appPresence tracking size = ");
            sb.append(arrayList.size());
            sb.append(" normal size = ");
            sb.append(arrayList2.size());
            if (!arrayList.isEmpty()) {
                list2.addAll(c.a((List<String>) arrayList, "false", "true"));
            }
            if (!arrayList2.isEmpty()) {
                list2.addAll(c.a((List<String>) arrayList2, "false", "false"));
            }
            for (int i = 0; i < this.c.size(); i++) {
                String str2 = (String) this.c.get(i);
                if (str2.length() != 0) {
                    c.a(this.b, str2, new com.startapp.android.publish.adsCommon.e.b().setNonImpressionReason("APP_PRESENCE"));
                }
            }
            z = true;
        } catch (Exception e) {
            com.startapp.android.publish.adsCommon.g.f.a(this.b, d.EXCEPTION, "AppPresenceHandler.doInBackground - sendAdImpressions failed", e.getMessage(), "");
        }
        return Boolean.valueOf(z);
    }
}
