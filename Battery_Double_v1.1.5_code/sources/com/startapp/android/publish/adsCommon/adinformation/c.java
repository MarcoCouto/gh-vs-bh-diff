package com.startapp.android.publish.adsCommon.adinformation;

import com.startapp.android.publish.adsCommon.adinformation.AdInformationPositions.Position;
import com.startapp.common.c.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class c implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean enable = true;
    private boolean enableOverride = false;
    @e(b = Position.class)
    private Position position = Position.getByName(AdInformationPositions.f4029a);
    private boolean positionOverride = false;

    private c() {
    }

    public static c a() {
        return new c();
    }

    public final boolean b() {
        return this.enable;
    }

    public final void a(boolean z) {
        this.enable = z;
        this.enableOverride = true;
    }

    public final Position c() {
        return this.position;
    }

    public final void a(Position position2) {
        this.position = position2;
        if (position2 != null) {
            this.positionOverride = true;
        } else {
            this.positionOverride = false;
        }
    }

    public final boolean d() {
        return this.positionOverride;
    }

    public final boolean e() {
        return this.enableOverride;
    }
}
