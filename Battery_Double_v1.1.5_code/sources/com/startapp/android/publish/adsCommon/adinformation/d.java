package com.startapp.android.publish.adsCommon.adinformation;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.android.publish.adsCommon.adinformation.AdInformationPositions.Position;
import com.startapp.android.publish.adsCommon.adinformation.b.C0087b;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.tapjoy.TJAdUnitConstants.String;

/* compiled from: StartAppSDK */
public final class d extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    private ImageView f4040a;
    private RelativeLayout b;
    private OnClickListener c = null;
    private AdInformationConfig d;
    private e e;
    private Placement f;
    private Position g;

    public d(Context context, C0087b bVar, Placement placement, c cVar, final OnClickListener onClickListener) {
        super(context);
        this.f = placement;
        this.c = new OnClickListener() {
            public final void onClick(View view) {
                onClickListener.onClick(view);
            }
        };
        getContext();
        this.d = b.c();
        if (this.d == null) {
            this.d = AdInformationConfig.a();
        }
        this.e = this.d.a(bVar.a());
        if (cVar == null || !cVar.d()) {
            this.g = this.d.a(this.f);
        } else {
            this.g = cVar.c();
        }
        this.f4040a = new ImageView(getContext());
        this.f4040a.setContentDescription(String.VIDEO_INFO);
        this.f4040a.setId(AdsConstants.AD_INFORMATION_ID);
        this.f4040a.setImageBitmap(this.e.a(getContext()));
        this.b = new RelativeLayout(getContext());
        LayoutParams layoutParams = new LayoutParams(i.a(getContext(), (int) (((float) this.e.b()) * this.d.d())), i.a(getContext(), (int) (((float) this.e.c()) * this.d.d())));
        this.b.setBackgroundColor(0);
        LayoutParams layoutParams2 = new LayoutParams(i.a(getContext(), this.e.b()), i.a(getContext(), this.e.c()));
        layoutParams2.setMargins(0, 0, 0, 0);
        this.f4040a.setPadding(0, 0, 0, 0);
        this.g.addRules(layoutParams2);
        this.b.addView(this.f4040a, layoutParams2);
        this.b.setOnClickListener(this.c);
        addView(this.b, layoutParams);
    }
}
