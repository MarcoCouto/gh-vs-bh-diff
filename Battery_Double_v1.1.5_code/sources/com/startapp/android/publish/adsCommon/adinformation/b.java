package com.startapp.android.publish.adsCommon.adinformation;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adinformation.AdInformationConfig.ImageResourceType;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.e;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.common.metaData.l;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class b implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    Context f4031a;
    RelativeLayout b;
    RelativeLayout c;
    private d d;
    private WebView e;
    private Dialog f = null;
    private Placement g;
    private Handler h = new Handler();
    private int i = a.f4038a;
    private boolean j = false;
    private c k;
    private AdInformationConfig l;
    private l m;
    private Runnable n = new Runnable() {
        public final void run() {
            try {
                b.this.e();
                l.a(b.this.f4031a, true);
                AdInformationConfig.b(b.this.f4031a);
                b.this.a(false);
            } catch (Exception e) {
                f.a(b.this.f4031a, new e(d.EXCEPTION, "AdInformationObject.acceptRunnable failed", e.getMessage()), "");
            }
        }
    };
    private Runnable o = new Runnable() {
        public final void run() {
            try {
                b.this.e();
                com.startapp.android.publish.adsCommon.l.b();
                l.a(b.this.f4031a, false);
                AdInformationConfig.b(b.this.f4031a);
                b.this.a(false);
            } catch (Exception e) {
                f.a(b.this.f4031a, new e(d.EXCEPTION, "AdInformationObject.declineRunnable failed", e.getMessage()), "");
            }
        }
    };
    private Runnable p = new Runnable() {
        public final void run() {
            try {
                b.this.e();
                b.this.d();
                b.this.a(false);
            } catch (Exception e) {
                f.a(b.this.f4031a, new e(d.EXCEPTION, "AdInformationObject.fullPrivacyPolicy failed", e.getMessage()), "");
            }
        }
    };

    /* renamed from: com.startapp.android.publish.adsCommon.adinformation.b$6 reason: invalid class name */
    /* compiled from: StartAppSDK */
    static /* synthetic */ class AnonymousClass6 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f4037a = new int[a.a().length];

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0010 */
        static {
            int[] iArr = f4037a;
            int i = a.b;
            iArr[1] = 1;
            try {
                int[] iArr2 = f4037a;
                int i2 = a.f4038a;
                iArr2[0] = 2;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    /* compiled from: StartAppSDK */
    public enum a {
        ;
        

        /* renamed from: a reason: collision with root package name */
        public static final int f4038a = 1;
        public static final int b = 2;

        static {
            c = new int[]{1, 2};
        }

        public static int[] a() {
            return (int[]) c.clone();
        }
    }

    /* renamed from: com.startapp.android.publish.adsCommon.adinformation.b$b reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public enum C0087b {
        SMALL(ImageResourceType.INFO_S, ImageResourceType.INFO_EX_S),
        LARGE(ImageResourceType.INFO_L, ImageResourceType.INFO_EX_L);
        
        private ImageResourceType infoExtendedType;
        private ImageResourceType infoType;

        private C0087b(ImageResourceType imageResourceType, ImageResourceType imageResourceType2) {
            this.infoType = imageResourceType;
            this.infoExtendedType = imageResourceType2;
        }

        public final ImageResourceType a() {
            return this.infoType;
        }
    }

    public b(Context context, C0087b bVar, Placement placement, c cVar) {
        this.f4031a = context;
        this.g = placement;
        this.k = cVar;
        this.l = a.b().a();
        this.m = com.startapp.android.publish.common.metaData.e.getInstance().getSimpleTokenConfig();
        d dVar = new d(context, bVar, placement, cVar, this);
        this.d = dVar;
    }

    public final View a() {
        return this.d;
    }

    public final boolean b() {
        return this.j;
    }

    public static AdInformationConfig c() {
        return a.b().a();
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        if (!this.g.isInterstitial() && (this.f4031a instanceof Activity)) {
            j.a((Activity) this.f4031a, z);
        }
    }

    public final void onClick(View view) {
        if (!this.m.a(this.f4031a) || !(this.f4031a instanceof Activity) || ((Activity) this.f4031a).isFinishing()) {
            d();
            return;
        }
        a(true);
        this.b = new RelativeLayout(this.f4031a);
        try {
            this.e = new WebView(this.f4031a);
            this.e.setWebViewClient(new WebViewClient());
            this.e.setWebChromeClient(new WebChromeClient());
            this.e.getSettings().setJavaScriptEnabled(true);
            this.e.setHorizontalScrollBarEnabled(false);
            this.e.setVerticalScrollBarEnabled(false);
            WebView webView = this.e;
            StringBuilder sb = new StringBuilder(this.l.e());
            if (k.a(this.f4031a, "shared_prefs_using_location", Boolean.FALSE).booleanValue()) {
                sb.append("?le=true");
            }
            webView.loadUrl(sb.toString());
            this.e.addJavascriptInterface(new AdInformationJsInterface(this.f4031a, this.n, this.o, this.p), "startappwall");
            Point point = new Point(1, 1);
            try {
                WindowManager windowManager = (WindowManager) this.f4031a.getSystemService("window");
                if (VERSION.SDK_INT >= 13) {
                    windowManager.getDefaultDisplay().getSize(point);
                } else {
                    point.x = windowManager.getDefaultDisplay().getWidth();
                    point.y = windowManager.getDefaultDisplay().getHeight();
                }
                LayoutParams layoutParams = new LayoutParams(-1, -1);
                layoutParams.addRule(13);
                this.e.setPadding(0, 0, 0, 0);
                layoutParams.setMargins(0, 0, 0, 0);
                this.b.addView(this.e, layoutParams);
                String a2 = c.a(this.f4031a, (String) null);
                if (a2 != null) {
                    WebView webView2 = this.e;
                    StringBuilder sb2 = new StringBuilder("javascript:window.onload=function(){document.getElementById('titlePlacement').innerText='");
                    sb2.append(a2);
                    sb2.append("';}");
                    webView2.loadUrl(sb2.toString());
                }
                switch (AnonymousClass6.f4037a[this.i - 1]) {
                    case 1:
                        final RelativeLayout relativeLayout = this.b;
                        this.j = true;
                        final LayoutParams layoutParams2 = new LayoutParams((int) (((float) point.x) * 0.9f), (int) (((float) point.y) * 0.85f));
                        layoutParams2.addRule(13);
                        this.h.post(new Runnable() {
                            public final void run() {
                                b.this.c.addView(relativeLayout, layoutParams2);
                            }
                        });
                        return;
                    case 2:
                        RelativeLayout relativeLayout2 = this.b;
                        this.j = true;
                        this.f = new Dialog(this.f4031a);
                        this.f.requestWindowFeature(1);
                        this.f.setContentView(relativeLayout2);
                        WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams();
                        layoutParams3.copyFrom(this.f.getWindow().getAttributes());
                        layoutParams3.width = (int) (((float) point.x) * 0.9f);
                        layoutParams3.height = (int) (((float) point.y) * 0.85f);
                        this.f.show();
                        this.f.getWindow().setAttributes(layoutParams3);
                        break;
                }
            } catch (Exception e2) {
                f.a(this.f4031a, d.EXCEPTION, "AdInformationObject.onClick - system service failed", e2.getMessage(), "");
                a(false);
            }
        } catch (Exception e3) {
            f.a(this.f4031a, d.EXCEPTION, "AdInformationObject.onClick - webview instantiation failed", e3.getMessage(), "");
            a(false);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        if (!j.a(256) || !com.startapp.android.publish.common.metaData.e.getInstance().isInAppBrowser()) {
            c.c(this.f4031a, this.l.b());
        } else {
            c.b(this.f4031a, this.l.b(), "");
        }
    }

    public final void e() {
        this.j = false;
        switch (AnonymousClass6.f4037a[this.i - 1]) {
            case 1:
                this.h.post(new Runnable() {
                    public final void run() {
                        b.this.c.removeView(b.this.b);
                    }
                });
                return;
            case 2:
                this.f.dismiss();
                break;
        }
    }

    public final void a(RelativeLayout relativeLayout) {
        boolean z;
        if (this.k == null || !this.k.e()) {
            z = a.b().a().a(this.f4031a);
        } else {
            z = this.k.b();
        }
        if (z) {
            this.c = relativeLayout;
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            if (this.k == null || !this.k.d()) {
                a.b().a().a(this.g).addRules(layoutParams);
            } else {
                this.k.c().addRules(layoutParams);
            }
            this.c.addView(this.d, layoutParams);
        }
    }
}
