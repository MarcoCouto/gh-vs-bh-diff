package com.startapp.android.publish.adsCommon.adinformation;

import android.content.Context;
import android.graphics.Bitmap;
import com.startapp.android.publish.adsCommon.a.a;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.common.a.C0093a;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class e implements Serializable {
    private static final long serialVersionUID = 1;

    /* renamed from: a reason: collision with root package name */
    private transient Bitmap f4042a;
    private transient Bitmap b;
    private transient Bitmap c = null;
    private int height = 1;
    private String imageFallbackUrl = "";
    private String imageUrlSecured = "";
    private String name;
    private int width = 1;

    private e() {
    }

    public final String a() {
        return this.name;
    }

    public final Bitmap a(Context context) {
        if (this.c == null) {
            this.c = this.f4042a;
            if (this.c == null) {
                if (this.b == null) {
                    this.b = a.a(context, this.imageFallbackUrl);
                }
                this.c = this.b;
            }
        }
        return this.c;
    }

    public final int b() {
        return this.width;
    }

    public final int c() {
        return this.height;
    }

    public final void a(int i) {
        this.width = i;
    }

    public final void b(int i) {
        this.height = i;
    }

    public final String d() {
        return this.imageUrlSecured != null ? this.imageUrlSecured : "";
    }

    /* access modifiers changed from: protected */
    public final void e() {
        a((Bitmap) null);
        new com.startapp.common.a(d(), new C0093a() {
            public final void a(Bitmap bitmap, int i) {
                e.this.a(bitmap);
            }
        }, 0).a();
    }

    public final void a(String str) {
        this.imageFallbackUrl = str;
    }

    /* access modifiers changed from: protected */
    public final void a(Bitmap bitmap) {
        this.f4042a = bitmap;
        if (bitmap != null) {
            this.c = bitmap;
        }
    }

    public static e b(String str) {
        e eVar = new e();
        eVar.name = str;
        return eVar;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        return this.width == eVar.width && this.height == eVar.height && j.b(this.imageUrlSecured, eVar.imageUrlSecured) && j.b(this.imageFallbackUrl, eVar.imageFallbackUrl) && j.b(this.name, eVar.name);
    }

    public final int hashCode() {
        return j.a(this.imageUrlSecured, this.imageFallbackUrl, Integer.valueOf(this.width), Integer.valueOf(this.height), this.name);
    }
}
