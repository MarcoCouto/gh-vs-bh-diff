package com.startapp.android.publish.adsCommon.adinformation;

import android.content.Context;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adinformation.AdInformationPositions.Position;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.c.e;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: StartAppSDK */
public final class AdInformationConfig implements Serializable {
    private static final long serialVersionUID = 1;
    @e(b = ArrayList.class, c = e.class)
    private List<e> ImageResources = new ArrayList();
    @e(b = HashMap.class, c = Position.class, d = Placement.class)
    protected HashMap<Placement, Position> Positions = new HashMap<>();

    /* renamed from: a reason: collision with root package name */
    private transient EnumMap<ImageResourceType, e> f4028a = new EnumMap<>(ImageResourceType.class);
    private String dialogUrlSecured = "https://d1byvlfiet2h9q.cloudfront.net/InApp/resources/adInformationDialog3.html";
    private boolean enabled = true;
    private String eulaUrlSecured = "https://www.com.startapp.com/policy/sdk-policy/";
    private float fatFingersFactor = 200.0f;

    /* compiled from: StartAppSDK */
    public enum ImageResourceType {
        INFO_S(17, 14),
        INFO_EX_S(88, 14),
        INFO_L(25, 21),
        INFO_EX_L(TsExtractor.TS_STREAM_TYPE_HDMV_DTS, 21);
        
        private int height;
        private int width;

        private ImageResourceType(int i, int i2) {
            this.width = i;
            this.height = i2;
        }

        public final int getDefaultWidth() {
            return this.width;
        }

        public final int getDefaultHeight() {
            return this.height;
        }

        public static ImageResourceType getByName(String str) {
            ImageResourceType imageResourceType = INFO_S;
            ImageResourceType[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].name().toLowerCase().compareTo(str.toLowerCase()) == 0) {
                    imageResourceType = values[i];
                }
            }
            return imageResourceType;
        }
    }

    private AdInformationConfig() {
    }

    public static AdInformationConfig a() {
        AdInformationConfig adInformationConfig = new AdInformationConfig();
        a(adInformationConfig);
        return adInformationConfig;
    }

    public static void a(AdInformationConfig adInformationConfig) {
        adInformationConfig.i();
        adInformationConfig.h();
    }

    public final String b() {
        return (this.eulaUrlSecured == null || this.eulaUrlSecured.equals("")) ? "https://www.com.startapp.com/policy/sdk-policy/" : this.eulaUrlSecured;
    }

    public final String c() {
        return (!this.f4028a.containsKey(ImageResourceType.INFO_L) || ((e) this.f4028a.get(ImageResourceType.INFO_L)).d().equals("")) ? "https://info.startappservice.com/InApp/resources/info_l.png" : ((e) this.f4028a.get(ImageResourceType.INFO_L)).d();
    }

    public final boolean a(Context context) {
        return !k.a(context, "userDisabledAdInformation", Boolean.FALSE).booleanValue() && this.enabled;
    }

    public static void b(Context context) {
        k.b(context, "userDisabledAdInformation", Boolean.FALSE);
    }

    public final float d() {
        return this.fatFingersFactor / 100.0f;
    }

    public final String e() {
        return this.dialogUrlSecured != null ? this.dialogUrlSecured : "https://d1byvlfiet2h9q.cloudfront.net/InApp/resources/adInformationDialog3.html";
    }

    public final Position a(Placement placement) {
        Position position = (Position) this.Positions.get(placement);
        if (position != null) {
            return position;
        }
        Position position2 = Position.BOTTOM_LEFT;
        this.Positions.put(placement, position2);
        return position2;
    }

    public final void f() {
        for (e eVar : this.ImageResources) {
            this.f4028a.put(ImageResourceType.getByName(eVar.a()), eVar);
            eVar.e();
        }
    }

    private void h() {
        ImageResourceType[] values = ImageResourceType.values();
        int length = values.length;
        int i = 0;
        while (i < length) {
            ImageResourceType imageResourceType = values[i];
            if (this.f4028a.get(imageResourceType) != null) {
                i++;
            } else {
                StringBuilder sb = new StringBuilder("AdInformation error in ImageResource [");
                sb.append(imageResourceType);
                sb.append("] cannot be found in MetaData");
                throw new IllegalArgumentException(sb.toString());
            }
        }
    }

    private void i() {
        ImageResourceType[] values;
        for (ImageResourceType imageResourceType : ImageResourceType.values()) {
            e eVar = (e) this.f4028a.get(imageResourceType);
            Boolean bool = Boolean.TRUE;
            if (eVar == null) {
                eVar = e.b(imageResourceType.name());
                Iterator it = this.ImageResources.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (ImageResourceType.getByName(((e) it.next()).a()).equals(imageResourceType)) {
                            bool = Boolean.FALSE;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                this.f4028a.put(imageResourceType, eVar);
                if (bool.booleanValue()) {
                    this.ImageResources.add(eVar);
                }
            }
            eVar.a(imageResourceType.getDefaultWidth());
            eVar.b(imageResourceType.getDefaultHeight());
            StringBuilder sb = new StringBuilder();
            sb.append(imageResourceType.name().toLowerCase());
            sb.append(".png");
            eVar.a(sb.toString());
        }
    }

    public final void g() {
        this.f4028a = new EnumMap<>(ImageResourceType.class);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdInformationConfig adInformationConfig = (AdInformationConfig) obj;
        return this.enabled == adInformationConfig.enabled && Float.compare(adInformationConfig.fatFingersFactor, this.fatFingersFactor) == 0 && j.b(this.dialogUrlSecured, adInformationConfig.dialogUrlSecured) && j.b(this.eulaUrlSecured, adInformationConfig.eulaUrlSecured) && j.b(this.Positions, adInformationConfig.Positions) && j.b(this.ImageResources, adInformationConfig.ImageResources);
    }

    public final int hashCode() {
        return j.a(Boolean.valueOf(this.enabled), Float.valueOf(this.fatFingersFactor), this.dialogUrlSecured, this.eulaUrlSecured, this.Positions, this.ImageResources);
    }

    public final e a(ImageResourceType imageResourceType) {
        return (e) this.f4028a.get(imageResourceType);
    }
}
