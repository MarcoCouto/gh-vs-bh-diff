package com.startapp.android.publish.adsCommon.adinformation;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.common.c.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class a implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private static volatile a f4030a = new a();
    private static Object b = new Object();
    private static final long serialVersionUID = 1;
    @e(a = true)
    private AdInformationConfig AdInformation = AdInformationConfig.a();
    private String adInformationMetadataUpdateVersion = AdsConstants.c;

    @VisibleForTesting
    public a() {
        this.AdInformation.f();
    }

    public final AdInformationConfig a() {
        return this.AdInformation;
    }

    public static a b() {
        return f4030a;
    }

    public static void a(Context context, a aVar) {
        synchronized (b) {
            aVar.adInformationMetadataUpdateVersion = AdsConstants.c;
            f4030a = aVar;
            AdInformationConfig.a(aVar.AdInformation);
            f4030a.AdInformation.f();
            com.startapp.common.a.e.a(context, "StartappAdInfoMetadata", (Serializable) aVar);
        }
    }

    public final String c() {
        return this.AdInformation.b();
    }

    public final String d() {
        return this.AdInformation.c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return j.b(this.AdInformation, aVar.AdInformation) && j.b(this.adInformationMetadataUpdateVersion, aVar.adInformationMetadataUpdateVersion);
    }

    public int hashCode() {
        return j.a(this.AdInformation, this.adInformationMetadataUpdateVersion);
    }

    public static void a(Context context) {
        a aVar = (a) com.startapp.common.a.e.a(context, "StartappAdInfoMetadata");
        a aVar2 = new a();
        if (aVar != null) {
            boolean a2 = j.a(aVar, aVar2);
            if (!(!AdsConstants.c.equals(aVar.adInformationMetadataUpdateVersion)) && a2) {
                f.a(context, d.METADATA_NULL, "AdInformationMetaData", "", "");
            }
            aVar.AdInformation.g();
            f4030a = aVar;
        } else {
            f4030a = aVar2;
        }
        f4030a.AdInformation.f();
    }
}
