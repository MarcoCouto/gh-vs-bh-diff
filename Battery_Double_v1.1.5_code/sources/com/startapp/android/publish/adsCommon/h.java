package com.startapp.android.publish.adsCommon;

import android.content.Context;
import com.startapp.android.publish.common.model.AdDetails;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public abstract class h extends Ad {
    private static final long serialVersionUID = 1;
    private List<AdDetails> adsDetails = null;

    public h(Context context, Placement placement) {
        super(context, placement);
    }

    public final void a(List<AdDetails> list) {
        this.adsDetails = list;
        Long l = null;
        if (this.adsDetails != null) {
            for (AdDetails adDetails : this.adsDetails) {
                if (!(adDetails == null || adDetails.getTtl() == null)) {
                    if (l == null || adDetails.getTtl().longValue() < l.longValue()) {
                        l = adDetails.getTtl();
                    }
                }
            }
        }
        if (l != null) {
            this.adCacheTtl = Long.valueOf(TimeUnit.SECONDS.toMillis(l.longValue()));
        }
        boolean z = true;
        for (AdDetails isBelowMinCPM : this.adsDetails) {
            if (!isBelowMinCPM.getIsBelowMinCPM()) {
                z = false;
            }
        }
        this.belowMinCPM = z;
    }

    public final List<AdDetails> d() {
        return this.adsDetails;
    }
}
