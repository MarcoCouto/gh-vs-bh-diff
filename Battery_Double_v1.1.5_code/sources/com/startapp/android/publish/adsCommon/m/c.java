package com.startapp.android.publish.adsCommon.m;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import java.lang.ref.WeakReference;

/* compiled from: StartAppSDK */
public final class c {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Handler f4113a = new Handler();
    @NonNull
    private final WeakReference<View> b;
    private final int c;

    /* compiled from: StartAppSDK */
    public interface a {
        boolean onUpdate(boolean z);
    }

    public c(@NonNull View view, int i, @NonNull final a aVar) {
        this.b = new WeakReference<>(view);
        this.c = i;
        this.f4113a.postDelayed(new Runnable() {
            public final void run() {
                if (aVar.onUpdate(a.a((View) c.this.b.get(), c.this.c))) {
                    c.this.f4113a.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    public final void a() {
        this.f4113a.removeCallbacksAndMessages(null);
    }
}
