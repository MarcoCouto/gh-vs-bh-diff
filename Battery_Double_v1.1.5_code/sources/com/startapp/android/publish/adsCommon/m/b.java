package com.startapp.android.publish.adsCommon.m;

import android.os.Handler;
import android.view.View;
import com.startapp.android.publish.adsCommon.i;
import java.lang.ref.WeakReference;

/* compiled from: StartAppSDK */
public class b implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private a f4112a;
    private Handler b = new Handler();
    private WeakReference<View> c;
    private final i d;
    private final int e;
    private boolean f = true;

    /* compiled from: StartAppSDK */
    public interface a {
        void a();
    }

    static {
        b.class.getSimpleName();
    }

    public b(View view, i iVar, int i) {
        this.c = new WeakReference<>(view);
        this.d = iVar;
        this.e = i;
    }

    public b(WeakReference<View> weakReference, i iVar, int i) {
        this.c = weakReference;
        this.d = iVar;
        this.e = i;
    }

    public final void a(a aVar) {
        this.f4112a = aVar;
    }

    public final void a() {
        if (c()) {
            run();
        }
    }

    public final void b() {
        try {
            if (this.d != null) {
                this.d.a(false);
            }
            if (this.b != null) {
                this.b.removeCallbacksAndMessages(null);
            }
        } catch (Exception e2) {
            new StringBuilder("ViewabilityRunner - clearVisibilityHandler failed ").append(e2.getMessage());
        }
    }

    public void run() {
        try {
            if (c()) {
                boolean a2 = a.a((View) this.c.get(), this.e);
                if (a2 && this.f) {
                    this.f = false;
                    this.d.a();
                    a aVar = this.f4112a;
                } else if (!a2 && !this.f) {
                    this.f = true;
                    this.d.b();
                    if (this.f4112a != null) {
                        this.f4112a.a();
                    }
                }
                this.b.postDelayed(this, 100);
                return;
            }
            b();
        } catch (Exception e2) {
            new StringBuilder("ViewabilityRunner.run - runnable error ").append(e2.getMessage());
            b();
        }
    }

    private boolean c() {
        return (this.d == null || this.d.c() || this.c.get() == null) ? false : true;
    }
}
