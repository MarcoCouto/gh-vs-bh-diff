package com.startapp.android.publish.adsCommon.m;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Region.Op;
import android.graphics.RegionIterator;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import com.startapp.android.publish.ads.list3d.ListItem;
import com.startapp.android.publish.ads.list3d.f;
import com.startapp.android.publish.adsCommon.e.b;
import com.startapp.android.publish.common.model.AdDetails;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private com.startapp.android.publish.ads.list3d.a f4111a = new com.startapp.android.publish.ads.list3d.a();
    private List<ListItem> b;
    private String c = "";

    public static boolean a(@Nullable View view, int i) {
        if (view == null || view.getParent() == null || view.getRootView() == null || view.getRootView().getParent() == null || !view.hasWindowFocus() || !view.isShown() || view.getWidth() <= 0 || view.getHeight() <= 0) {
            return false;
        }
        Rect rect = new Rect();
        if (view.getGlobalVisibleRect(rect) && !rect.isEmpty() && a(rect, view) >= ((view.getWidth() * view.getHeight()) * Math.min(Math.max(1, i), 100)) / 100) {
            return true;
        }
        return false;
    }

    private static int a(@NonNull Rect rect, @NonNull View view) {
        Region region = new Region(rect);
        Rect rect2 = new Rect();
        r5 = view;
        while (true) {
            int i = 0;
            if (!(r5.getParent() instanceof ViewGroup)) {
                while (new RegionIterator(region).next(rect2)) {
                    i += rect2.width() * rect2.height();
                }
                return i;
            } else if (VERSION.SDK_INT >= 11 && r5.getAlpha() < 1.0f) {
                return 0;
            } else {
                ViewGroup viewGroup = (ViewGroup) r5.getParent();
                int childCount = viewGroup.getChildCount();
                for (int indexOfChild = viewGroup.indexOfChild(r5) + 1; indexOfChild < childCount; indexOfChild++) {
                    View childAt = viewGroup.getChildAt(indexOfChild);
                    if (childAt != null && childAt.getGlobalVisibleRect(rect2)) {
                        region.op(rect2, Op.DIFFERENCE);
                    }
                }
                r5 = viewGroup;
            }
        }
    }

    public final void a() {
        this.b = new ArrayList();
        this.c = "";
    }

    public final void a(AdDetails adDetails) {
        ListItem listItem = new ListItem(adDetails);
        this.b.add(listItem);
        this.f4111a.a(this.b.size() - 1, listItem.a(), listItem.i());
    }

    public final void b() {
        this.f4111a.a();
    }

    public final void c() {
        this.f4111a.b();
    }

    public final void d() {
        this.f4111a.c();
    }

    public final List<ListItem> e() {
        return this.b;
    }

    public final Bitmap a(int i, String str, String str2) {
        return this.f4111a.a(i, str, str2);
    }

    public final void a(Context context, String str, b bVar, long j) {
        com.startapp.android.publish.ads.list3d.a aVar = this.f4111a;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.c);
        aVar.a(context, sb.toString(), bVar, j);
    }

    public final void a(String str) {
        com.startapp.android.publish.ads.list3d.a aVar = this.f4111a;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.c);
        aVar.a(sb.toString());
    }

    public final void a(f fVar, boolean z) {
        this.f4111a.a(fVar, z);
    }

    public final void b(String str) {
        this.c = str;
    }
}
