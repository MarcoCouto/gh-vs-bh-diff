package com.startapp.android.publish.adsCommon.adListeners;

import android.os.Handler;
import android.os.Looper;
import com.startapp.android.publish.adsCommon.Ad;

/* compiled from: StartAppSDK */
public final class a implements AdDisplayListener {

    /* renamed from: a reason: collision with root package name */
    AdDisplayListener f4020a;

    public a(AdDisplayListener adDisplayListener) {
        this.f4020a = adDisplayListener;
    }

    public final void adHidden(final Ad ad) {
        if (this.f4020a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.f4020a.adHidden(ad);
                }
            });
        }
    }

    public final void adDisplayed(final Ad ad) {
        if (this.f4020a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.f4020a.adDisplayed(ad);
                }
            });
        }
    }

    public final void adClicked(final Ad ad) {
        if (this.f4020a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.f4020a.adClicked(ad);
                }
            });
        }
    }

    public final void adNotDisplayed(final Ad ad) {
        if (this.f4020a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.f4020a.adNotDisplayed(ad);
                }
            });
        }
    }
}
