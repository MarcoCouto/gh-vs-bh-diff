package com.startapp.android.publish.adsCommon.adListeners;

import android.os.Handler;
import android.os.Looper;
import com.startapp.android.publish.adsCommon.Ad;

/* compiled from: StartAppSDK */
public final class b implements AdEventListener {

    /* renamed from: a reason: collision with root package name */
    AdEventListener f4025a;

    public b(AdEventListener adEventListener) {
        this.f4025a = adEventListener;
    }

    public final void onReceiveAd(final Ad ad) {
        if (this.f4025a != null) {
            a().post(new Runnable() {
                public final void run() {
                    b.this.f4025a.onReceiveAd(ad);
                }
            });
        }
    }

    public final void onFailedToReceiveAd(final Ad ad) {
        if (this.f4025a != null) {
            a().post(new Runnable() {
                public final void run() {
                    b.this.f4025a.onFailedToReceiveAd(ad);
                }
            });
        }
    }

    private static Handler a() {
        return new Handler(Looper.getMainLooper());
    }
}
