package com.startapp.android.publish.adsCommon;

import android.content.Context;
import com.startapp.android.publish.adsCommon.a.h;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener.NotDisplayedReason;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.adListeners.b;
import com.startapp.android.publish.adsCommon.adinformation.c;
import com.startapp.android.publish.cache.d;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.metaData.f;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/* compiled from: StartAppSDK */
public abstract class Ad implements Serializable {
    private static boolean init = false;
    private static final long serialVersionUID = 1;
    protected a activityExtra;
    protected Long adCacheTtl = null;
    private c adInfoOverride;
    protected boolean belowMinCPM = false;
    protected transient Context context;
    protected String errorMessage = null;
    protected Serializable extraData = null;
    private Long lastLoadTime = null;
    private NotDisplayedReason notDisplayedReason;
    protected Placement placement;
    private AdState state = AdState.UN_INITIALIZED;
    private AdType type;
    private boolean videoCancelCallBack;

    /* compiled from: StartAppSDK */
    public enum AdState {
        UN_INITIALIZED,
        PROCESSING,
        READY
    }

    /* compiled from: StartAppSDK */
    public enum AdType {
        INTERSTITIAL,
        RICH_TEXT,
        VIDEO,
        REWARDED_VIDEO,
        NON_VIDEO,
        VIDEO_NO_VAST
    }

    /* access modifiers changed from: protected */
    public abstract void loadAds(AdPreferences adPreferences, AdEventListener adEventListener);

    @Deprecated
    public boolean show() {
        return false;
    }

    public Ad(Context context2, Placement placement2) {
        this.context = context2;
        this.placement = placement2;
        if (j.e()) {
            this.adInfoOverride = c.a();
        }
    }

    public Serializable getExtraData() {
        return this.extraData;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public void setActivityExtra(a aVar) {
        this.activityExtra = aVar;
    }

    public void setExtraData(Serializable serializable) {
        this.extraData = serializable;
    }

    public AdState getState() {
        return this.state;
    }

    public boolean isBelowMinCPM() {
        return this.belowMinCPM;
    }

    public void setState(AdState adState) {
        this.state = adState;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public c getAdInfoOverride() {
        return this.adInfoOverride;
    }

    public void setAdInfoOverride(c cVar) {
        this.adInfoOverride = cVar;
    }

    /* access modifiers changed from: protected */
    public Placement getPlacement() {
        return this.placement;
    }

    /* access modifiers changed from: protected */
    public void setPlacement(Placement placement2) {
        this.placement = placement2;
    }

    @Deprecated
    public boolean load() {
        return load(new AdPreferences(), null);
    }

    @Deprecated
    public boolean load(AdEventListener adEventListener) {
        return load(new AdPreferences(), adEventListener);
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences) {
        return load(adPreferences, null);
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences, AdEventListener adEventListener) {
        return load(adPreferences, adEventListener, true);
    }

    /* access modifiers changed from: protected */
    public boolean load(final AdPreferences adPreferences, AdEventListener adEventListener, boolean z) {
        String str;
        boolean z2;
        final b bVar = new b(adEventListener);
        final AnonymousClass1 r10 = new AdEventListener() {
            public final void onReceiveAd(Ad ad) {
                Ad.this.setLastLoadTime(Long.valueOf(System.currentTimeMillis()));
                bVar.onReceiveAd(ad);
            }

            public final void onFailedToReceiveAd(Ad ad) {
                bVar.onFailedToReceiveAd(ad);
            }
        };
        if (!init) {
            l.c(this.context);
            init = true;
        }
        j.a(this.context, adPreferences);
        String str2 = "";
        if (adPreferences.getProductId() == null || "".equals(adPreferences.getProductId())) {
            str = "app ID was not set.";
            z2 = true;
        } else {
            str = str2;
            z2 = false;
        }
        if (this.state != AdState.UN_INITIALIZED) {
            str = "load() was already called.";
            z2 = true;
        }
        if (!j.a(this.context)) {
            str = "network not available.";
            z2 = true;
        }
        if (!canShowAd()) {
            str = "serving ads disabled";
            z2 = true;
        }
        if (z2) {
            setErrorMessage("Ad wasn't loaded: ".concat(String.valueOf(str)));
            r10.onFailedToReceiveAd(this);
            return false;
        }
        setState(AdState.PROCESSING);
        AnonymousClass2 r7 = new f() {
            public final void a(boolean z) {
                Ad.this.loadAds(adPreferences, r10);
            }

            public final void a() {
                Ad.this.loadAds(adPreferences, r10);
            }

            public static String a(Node node) {
                try {
                    Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
                    newTransformer.setOutputProperty("omit-xml-declaration", "yes");
                    newTransformer.setOutputProperty("method", "xml");
                    newTransformer.setOutputProperty("indent", "no");
                    newTransformer.setOutputProperty("encoding", "UTF-8");
                    StringWriter stringWriter = new StringWriter();
                    newTransformer.transform(new DOMSource(node), new StreamResult(stringWriter));
                    return stringWriter.toString();
                } catch (Exception unused) {
                    return null;
                }
            }

            public static Document a(String str) {
                try {
                    DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
                    newInstance.setIgnoringElementContentWhitespace(true);
                    newInstance.setIgnoringComments(true);
                    DocumentBuilder newDocumentBuilder = newInstance.newDocumentBuilder();
                    InputSource inputSource = new InputSource();
                    inputSource.setCharacterStream(new StringReader(str));
                    return newDocumentBuilder.parse(inputSource);
                } catch (Exception unused) {
                    return null;
                }
            }

            public static String b(Node node) {
                NodeList childNodes = node.getChildNodes();
                String str = null;
                for (int i = 0; i < childNodes.getLength(); i++) {
                    str = ((CharacterData) childNodes.item(i)).getData().trim();
                    if (str.length() != 0) {
                        return str;
                    }
                }
                return str;
            }
        };
        if (adPreferences.getType() != null) {
            setType(adPreferences.getType());
        }
        e.getInstance().loadFromServer(this.context, adPreferences, h.d().c(), z, r7);
        return true;
    }

    public boolean isReady() {
        return this.state == AdState.READY && !hasAdCacheTtlPassed();
    }

    public NotDisplayedReason getNotDisplayedReason() {
        return this.notDisplayedReason;
    }

    /* access modifiers changed from: protected */
    public void setNotDisplayedReason(NotDisplayedReason notDisplayedReason2) {
        this.notDisplayedReason = notDisplayedReason2;
    }

    /* access modifiers changed from: protected */
    public Long getAdCacheTtl() {
        long fallbackAdCacheTtl = getFallbackAdCacheTtl();
        if (this.adCacheTtl != null) {
            fallbackAdCacheTtl = Math.min(this.adCacheTtl.longValue(), fallbackAdCacheTtl);
        }
        return Long.valueOf(fallbackAdCacheTtl);
    }

    /* access modifiers changed from: protected */
    public long getFallbackAdCacheTtl() {
        return d.a().b().getAdCacheTtl();
    }

    /* access modifiers changed from: protected */
    public Long getLastLoadTime() {
        return this.lastLoadTime;
    }

    /* access modifiers changed from: private */
    public void setLastLoadTime(Long l) {
        this.lastLoadTime = l;
    }

    /* access modifiers changed from: protected */
    public boolean hasAdCacheTtlPassed() {
        if (this.lastLoadTime != null && System.currentTimeMillis() - this.lastLoadTime.longValue() > getAdCacheTtl().longValue()) {
            return true;
        }
        return false;
    }

    private void setType(AdType adType) {
        this.type = adType;
    }

    public AdType getType() {
        return this.type;
    }

    /* access modifiers changed from: protected */
    public boolean getVideoCancelCallBack() {
        return this.videoCancelCallBack;
    }

    /* access modifiers changed from: protected */
    public void setVideoCancelCallBack(boolean z) {
        this.videoCancelCallBack = z;
    }

    /* access modifiers changed from: protected */
    public boolean canShowAd() {
        return e.getInstance().canShowAd();
    }
}
