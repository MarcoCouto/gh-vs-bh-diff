package com.startapp.android.publish.adsCommon.j;

import android.os.Bundle;

/* compiled from: StartAppSDK */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private final Bundle f4095a;

    public b(Bundle bundle) {
        this.f4095a = bundle;
    }

    public final String a() {
        return this.f4095a.getString("install_referrer");
    }

    public final long b() {
        return this.f4095a.getLong("referrer_click_timestamp_seconds");
    }

    public final long c() {
        return this.f4095a.getLong("install_begin_timestamp_seconds");
    }
}
