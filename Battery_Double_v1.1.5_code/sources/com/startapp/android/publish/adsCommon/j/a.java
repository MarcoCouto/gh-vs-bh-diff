package com.startapp.android.publish.adsCommon.j;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewParent;
import com.a.a.a.a.a.C0002a;
import com.b.a.a.a.b.b;
import com.b.a.a.a.e.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static CountDownLatch f4093a;
    /* access modifiers changed from: private */
    public static volatile b b;
    private final HashMap<View, String> c = new HashMap<>();
    private final HashMap<View, ArrayList<String>> d = new HashMap<>();
    private final HashSet<View> e = new HashSet<>();
    private final HashSet<String> f = new HashSet<>();
    private final HashSet<String> g = new HashSet<>();
    private boolean h;

    /* renamed from: com.startapp.android.publish.adsCommon.j.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    static final class C0090a implements ServiceConnection {

        /* renamed from: a reason: collision with root package name */
        private String f4094a;

        /* synthetic */ C0090a(String str, byte b) {
            this(str);
        }

        private C0090a(String str) {
            this.f4094a = str;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            com.a.a.a.a.a a2 = C0002a.a(iBinder);
            Bundle bundle = new Bundle();
            bundle.putString(CampaignEx.JSON_KEY_PACKAGE_NAME, this.f4094a);
            try {
                a.b = new b(a2.a(bundle));
            } catch (RemoteException unused) {
            }
            a.f4093a.countDown();
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            a.f4093a.countDown();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:16|17|18|19) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x006f */
    public static b a(Context context) {
        if (b == null) {
            try {
                f4093a = new CountDownLatch(1);
                C0090a aVar = new C0090a(context.getPackageName(), 0);
                Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
                intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
                List queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
                if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
                    ResolveInfo resolveInfo = (ResolveInfo) queryIntentServices.get(0);
                    if (resolveInfo.serviceInfo != null) {
                        String str = resolveInfo.serviceInfo.packageName;
                        String str2 = resolveInfo.serviceInfo.name;
                        if ("com.android.vending".equals(str) && str2 != null && b(context)) {
                            if (context.bindService(new Intent(intent), aVar, 1)) {
                                f4093a.await(1, TimeUnit.SECONDS);
                                context.unbindService(aVar);
                            } else {
                                throw new Exception("failed to connect to referrer service");
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                f.a(context, d.EXCEPTION, "getReferrerDetails", th.getMessage(), "");
            }
        }
        return b;
    }

    private static boolean b(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            }
            return false;
        } catch (NameNotFoundException unused) {
            return false;
        }
    }

    public final HashSet<String> b() {
        return this.f;
    }

    public final HashSet<String> c() {
        return this.g;
    }

    public final void d() {
        com.b.a.a.a.c.a a2 = com.b.a.a.a.c.a.a();
        if (a2 != null) {
            for (b bVar : a2.c()) {
                View g2 = bVar.g();
                if (bVar.h()) {
                    if (g2 != null) {
                        boolean z = false;
                        if (g2.hasWindowFocus()) {
                            HashSet hashSet = new HashSet();
                            View view = g2;
                            while (true) {
                                if (view != null) {
                                    if (!c.c(view)) {
                                        break;
                                    }
                                    hashSet.add(view);
                                    ViewParent parent = view.getParent();
                                    view = parent instanceof View ? (View) parent : null;
                                } else {
                                    this.e.addAll(hashSet);
                                    z = true;
                                    break;
                                }
                            }
                        }
                        if (z) {
                            this.f.add(bVar.f());
                            this.c.put(g2, bVar.f());
                            a(bVar);
                        }
                    }
                    this.g.add(bVar.f());
                }
            }
        }
    }

    private void a(b bVar) {
        for (com.b.a.a.a.f.a aVar : bVar.c()) {
            View view = (View) aVar.get();
            if (view != null) {
                ArrayList arrayList = (ArrayList) this.d.get(view);
                if (arrayList == null) {
                    arrayList = new ArrayList();
                    this.d.put(view, arrayList);
                }
                arrayList.add(bVar.f());
            }
        }
    }

    public final void e() {
        this.c.clear();
        this.d.clear();
        this.e.clear();
        this.f.clear();
        this.g.clear();
        this.h = false;
    }

    public final void f() {
        this.h = true;
    }

    public final String a(View view) {
        if (this.c.size() == 0) {
            return null;
        }
        String str = (String) this.c.get(view);
        if (str != null) {
            this.c.remove(view);
        }
        return str;
    }

    public final ArrayList<String> b(View view) {
        if (this.d.size() == 0) {
            return null;
        }
        ArrayList<String> arrayList = (ArrayList) this.d.get(view);
        if (arrayList != null) {
            this.d.remove(view);
            Collections.sort(arrayList);
        }
        return arrayList;
    }

    public final int c(View view) {
        if (this.e.contains(view)) {
            return com.b.a.a.a.h.c.f1805a;
        }
        return this.h ? com.b.a.a.a.h.c.b : com.b.a.a.a.h.c.c;
    }
}
