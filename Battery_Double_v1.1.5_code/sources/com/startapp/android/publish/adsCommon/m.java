package com.startapp.android.publish.adsCommon;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.customtabs.CustomTabsService;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import com.adcolony.sdk.AdColonyAppOptions;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.splash.SplashConfig;
import com.startapp.android.publish.adsCommon.AdsConstants.ServiceApiType;
import com.startapp.android.publish.adsCommon.a.b;
import com.startapp.android.publish.adsCommon.a.h;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.activities.FullScreenActivity;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.cache.c;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.Constants;
import com.truenet.android.TrueNetSDK;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class m {

    /* renamed from: a reason: collision with root package name */
    private SDKAdPreferences f4106a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private boolean f;
    private long g;
    private Application h;
    private HashMap<Integer, Integer> i;
    private Object j;
    private Activity k;
    private boolean l;
    private String m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private Map<String, String> r;
    private Bundle s;
    private c t;
    private boolean u;
    private boolean v;
    private boolean w;
    private boolean x;
    private g y;
    /* access modifiers changed from: private */
    public boolean z;

    /* compiled from: StartAppSDK */
    static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static final m f4110a = new m(0);
    }

    /* synthetic */ m(byte b2) {
        this();
    }

    private m() {
        this.b = j.a(512);
        this.d = false;
        this.e = false;
        this.f = false;
        this.h = null;
        this.i = new HashMap<>();
        this.l = false;
        this.n = false;
        this.o = true;
        this.p = false;
        this.q = false;
        this.s = null;
        this.t = null;
        this.v = false;
        this.w = false;
        this.x = false;
        this.y = null;
        this.z = false;
    }

    public static m a() {
        return a.f4110a;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:100|(1:102)(2:103|(1:105)(1:114))|106|107|(1:111)|112|113) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:112:0x0273 */
    public final void a(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences, boolean z2) {
        final Context context2;
        try {
            if (j.a(2) && !j.a(context, FullScreenActivity.class)) {
                Log.w("StartAppSDKInternal", "FullScreenActivity is missing from AndroidManifest.xml");
            }
            if (context instanceof Activity) {
                this.m = context.getClass().getName();
            }
            context2 = context.getApplicationContext();
            try {
                com.startapp.android.publish.adsCommon.f.a.a(context2);
            } catch (Exception e2) {
                try {
                    f.a(context2, d.EXCEPTION, "init AdsExceptionHandler", e2.getMessage(), "");
                } catch (Exception e3) {
                    e = e3;
                }
            }
            boolean z3 = true;
            this.o = !j.f(context2);
            TreeMap treeMap = new TreeMap();
            if (c("org.apache.cordova.CordovaPlugin")) {
                treeMap.put("Cordova", j.b());
            }
            if (c("com.startapp.android.mediation.admob.StartAppCustomEvent")) {
                treeMap.put(AdColonyAppOptions.ADMOB, d("com.startapp.android.mediation.admob"));
            }
            if (c("com.mopub.mobileads.StartAppCustomEventInterstitial")) {
                treeMap.put(AdColonyAppOptions.MOPUB, d("com.mopub.mobileads"));
            }
            if (c("anywheresoftware.b4a.BA") && !a.f4110a.r.containsKey("B4A")) {
                treeMap.put(AdColonyAppOptions.MOPUB, "0");
            }
            if (!treeMap.isEmpty()) {
                k.a(context2, "sharedPrefsWrappers", (Map<String, String>) treeMap);
            }
            if (!this.l) {
                com.startapp.common.c.b(context2);
                b.a(context2);
                e.init(context2);
                if (!j.a()) {
                    b.a(context2);
                    if (j.a(16) || j.a(32)) {
                        com.startapp.android.publish.ads.banner.c.a(context2);
                    }
                    if (j.a(8)) {
                        com.startapp.android.publish.ads.splash.f.a(context2);
                    }
                    if (this.b) {
                        com.startapp.android.publish.cache.d.a(context2);
                    }
                    if (j.e()) {
                        com.startapp.android.publish.adsCommon.adinformation.a.a(context2);
                    }
                }
                l.a(context2);
                this.l = true;
                if (TextUtils.isEmpty(str2)) {
                    Log.e("StartAppSDK", "The appId wasn't set");
                }
                StringBuilder sb = new StringBuilder("Initialize StartAppSDK with DevID:[");
                sb.append(str);
                sb.append("], AppID:[");
                sb.append(str2);
                sb.append(RequestParameters.RIGHT_BRACKETS);
                j.a(context2, str, str2);
                this.f4106a = sDKAdPreferences;
                com.startapp.common.a.e.b(context2, "shared_prefs_sdk_ad_prefs", sDKAdPreferences);
                if (VERSION.SDK_INT >= 9) {
                    com.startapp.common.d.a.a(context2);
                }
                if (k.a(context2, "shared_prefs_first_init", Boolean.TRUE).booleanValue()) {
                    k.b(context2, "totalSessions", Integer.valueOf(0));
                    k.b(context2, "firstSessionTime", Long.valueOf(System.currentTimeMillis()));
                    com.startapp.common.f.a(com.startapp.common.f.a.DEFAULT, (Runnable) new Runnable() {
                        public final void run() {
                            try {
                                j jVar = new j(context2);
                                AdPreferences adPreferences = new AdPreferences();
                                j.a(context2, adPreferences);
                                jVar.fillApplicationDetails(context2, adPreferences);
                                com.startapp.android.publish.adsCommon.l.a.a(context2, AdsConstants.a(ServiceApiType.DOWNLOAD), jVar);
                                k.b(context2, "shared_prefs_first_init", Boolean.FALSE);
                            } catch (Exception e) {
                                f.a(context2, d.EXCEPTION, "StartAppSDKInternal.handleDownloadEvent", e.getMessage(), "");
                            }
                        }
                    });
                }
                Integer a2 = k.a(context2, "shared_prefs_app_version_id", Integer.valueOf(-1));
                int c2 = com.startapp.common.a.c.c(context2);
                if (a2.intValue() > 0 && c2 > a2.intValue()) {
                    this.q = true;
                }
                k.b(context2, "shared_prefs_app_version_id", Integer.valueOf(c2));
                this.u = false;
                this.c = false;
                if (com.startapp.common.a.c.b() && j.a(2)) {
                    this.u = z2;
                    this.c = true;
                    StringBuilder sb2 = new StringBuilder("Return Ads: [");
                    sb2.append(z2);
                    sb2.append(RequestParameters.RIGHT_BRACKETS);
                }
                if (this.b) {
                    if (!this.q) {
                        if (com.startapp.android.publish.cache.d.a().b().isLocalCache()) {
                            if (this.c) {
                                com.startapp.android.publish.cache.a.a().a(context2);
                            }
                            d(context2);
                            com.startapp.android.publish.cache.a.a().c(context2);
                        }
                    }
                    com.startapp.android.publish.cache.a.a().b(context2);
                    d(context2);
                    com.startapp.android.publish.cache.a.a().c(context2);
                }
                a(context2, com.startapp.android.publish.common.metaData.g.a.LAUNCH);
                String e4 = e(context2);
                if (VERSION.SDK_INT >= 18) {
                    if (e4 != null) {
                        Intent intent = new Intent(CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION);
                        intent.setPackage(e4);
                        List queryIntentServices = context2.getPackageManager().queryIntentServices(intent, 0);
                        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
                            z3 = false;
                        }
                        k.b(context2, "chromeTabs", Boolean.valueOf(z3));
                    }
                }
                k.b(context2, "chromeTabs", Boolean.FALSE);
            }
            k.b(context2, "periodicInfoEventPaused", Boolean.FALSE);
            k.b(context2, "periodicMetadataPaused", Boolean.FALSE);
            AnonymousClass1 r8 = new com.startapp.android.publish.common.metaData.f() {
                public final void a(boolean z) {
                    if (e.getInstance().isUserAgentEnabled()) {
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable(context2) {
                            public final void run() {
                                try {
                                    k.b(r3, "User-Agent", new WebView(r3).getSettings().getUserAgentString());
                                } catch (Exception e) {
                                    f.a(r3, d.EXCEPTION, "NetworkUtils.saveUserAgent - Webview failed", e.getMessage(), "");
                                }
                            }
                        }, TimeUnit.SECONDS.toMillis(e.getInstance().getUserAgentDelayInSeconds()));
                    }
                    b.c(context2);
                    b.d(context2);
                    m.this.a(context2);
                    if (VERSION.SDK_INT > 8) {
                        if (e.getInstance().getTrueNetEnabled()) {
                            if (!m.this.z) {
                                m.this.z = true;
                                TrueNetSDK.with(context2, k.a(context2, "shared_prefs_appId", (String) null));
                            }
                            TrueNetSDK.enable(context2, true);
                        } else if (m.this.z) {
                            TrueNetSDK.enable(context2, false);
                        }
                    }
                }

                public final void a() {
                    if (e.getInstance().isUserAgentEnabled()) {
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable(context2) {
                            public final void run() {
                                try {
                                    k.b(r3, "User-Agent", new WebView(r3).getSettings().getUserAgentString());
                                } catch (Exception e) {
                                    f.a(r3, d.EXCEPTION, "NetworkUtils.saveUserAgent - Webview failed", e.getMessage(), "");
                                }
                            }
                        }, TimeUnit.SECONDS.toMillis(10));
                    }
                }
            };
            if (e.getInstance().isReady()) {
                r8.a(false);
            } else {
                e.getInstance().addMetaDataListener(r8);
            }
            if (com.startapp.common.a.c.b()) {
                if (context2 instanceof Activity) {
                    this.k = (Activity) context2;
                    this.h = this.k.getApplication();
                } else if (context2.getApplicationContext() instanceof Application) {
                    this.h = (Application) context2.getApplicationContext();
                } else {
                    return;
                }
                if (!(this.j == null || this.h == null)) {
                    this.h.unregisterActivityLifecycleCallbacks((ActivityLifecycleCallbacks) this.j);
                }
                Application application = this.h;
                AnonymousClass3 r9 = new ActivityLifecycleCallbacks() {
                    public final void onActivityStopped(Activity activity) {
                        StringBuilder sb = new StringBuilder("onActivityStopped [");
                        sb.append(activity.getClass().getName());
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        a.f4110a.c(activity);
                    }

                    public final void onActivityStarted(Activity activity) {
                        StringBuilder sb = new StringBuilder("onActivityStarted [");
                        sb.append(activity.getClass().getName());
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        a.f4110a.a(activity);
                    }

                    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                        StringBuilder sb = new StringBuilder("onActivitySaveInstanceState [");
                        sb.append(activity.getClass().getName());
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        a.f4110a;
                    }

                    public final void onActivityResumed(Activity activity) {
                        StringBuilder sb = new StringBuilder("onActivityResumed [");
                        sb.append(activity.getClass().getName());
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        a.f4110a.b(activity);
                    }

                    public final void onActivityPaused(Activity activity) {
                        StringBuilder sb = new StringBuilder("onActivityPaused [");
                        sb.append(activity.getClass().getName());
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        a.f4110a.e();
                    }

                    public final void onActivityDestroyed(Activity activity) {
                        StringBuilder sb = new StringBuilder("onActivityDestroyed [");
                        sb.append(activity.getClass().getName());
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        a.f4110a.d(activity);
                    }

                    public final void onActivityCreated(Activity activity, Bundle bundle) {
                        StringBuilder sb = new StringBuilder("onActivityCreated [");
                        sb.append(activity.getClass().getName());
                        sb.append(", ");
                        sb.append(bundle);
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        a.f4110a.a(activity, bundle);
                        if (j.a(2)) {
                            a.f4068a.a(activity, bundle);
                        }
                    }
                };
                application.registerActivityLifecycleCallbacks(r9);
                this.j = r9;
            }
        } catch (Exception e5) {
            e = e5;
            context2 = context;
            f.a(context2, d.EXCEPTION, "StartAppSDKInternal.intialize - unexpected error occurd", e.getMessage(), "");
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(Context context) {
        com.startapp.android.publish.adsCommon.g.c cVar = new com.startapp.android.publish.adsCommon.g.c(context);
        cVar.c().c(AdsConstants.e);
        cVar.a();
        if (this.q) {
            f.a(context, d.GENERAL, "packagingType", AdsConstants.e, "");
        }
    }

    public final void a(Activity activity, Bundle bundle) {
        if (bundle == null && this.m != null && activity.getClass().getName().equals(this.m)) {
            this.l = false;
        }
        this.s = bundle;
    }

    public final void a(Activity activity) {
        StringBuilder sb = new StringBuilder("onActivityStarted [");
        sb.append(activity.getClass().getName());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        if (j.a(8) && !b.a().z() && !this.w && !b(AdColonyAppOptions.MOPUB) && !b(AdColonyAppOptions.ADMOB) && !this.x && activity.getClass().getName().equals(this.m) && !this.v && this.i.size() == 0) {
            StartAppAd.showSplash(activity, this.s, new SplashConfig(), new AdPreferences(), null, false);
        }
        this.x = true;
        if (this.d) {
            if (e.getInstance().canShowAd() && this.u && !b.a().y() && !j.a() && !this.p) {
                if (System.currentTimeMillis() - this.g > b.a().x()) {
                    this.y = com.startapp.android.publish.cache.a.a().a(this.t);
                    if (this.y != null && this.y.isReady()) {
                        com.startapp.android.publish.adsCommon.b.f a2 = b.a().F().a(Placement.INAPP_RETURN, (String) null);
                        if (!a2.a()) {
                            c.a((Context) activity, ((com.startapp.android.publish.ads.b.e) this.y).getTrackingUrls(), (String) null, a2.c());
                            Constants.a();
                        } else if (this.y.a(null)) {
                            com.startapp.android.publish.adsCommon.b.b.a().a(new com.startapp.android.publish.adsCommon.b.a(Placement.INAPP_RETURN, null));
                        }
                    }
                }
            }
            if (System.currentTimeMillis() - this.g > e.getInstance().getSessionMaxBackgroundTime()) {
                a((Context) activity, com.startapp.android.publish.common.metaData.g.a.APP_IDLE);
            }
        }
        this.f = false;
        this.d = false;
        if (((Integer) this.i.get(Integer.valueOf(activity.hashCode()))) == null) {
            this.i.put(Integer.valueOf(activity.hashCode()), Integer.valueOf(1));
            StringBuilder sb2 = new StringBuilder("Activity Added:[");
            sb2.append(activity.getClass().getName());
            sb2.append(RequestParameters.RIGHT_BRACKETS);
            return;
        }
        StringBuilder sb3 = new StringBuilder("Activity [");
        sb3.append(activity.getClass().getName());
        sb3.append("] already exists");
    }

    public final void b(Activity activity) {
        if (this.b && this.e) {
            this.e = false;
            com.startapp.android.publish.cache.a.a().b();
        }
        if (this.n) {
            this.n = false;
            l.c(activity.getApplicationContext());
        }
        this.k = activity;
    }

    public final void b() {
        this.n = true;
        this.e = true;
    }

    public final void a(boolean z2) {
        this.p = z2;
    }

    public final boolean c() {
        return this.o;
    }

    public final boolean d() {
        return this.q;
    }

    public final void e() {
        this.g = System.currentTimeMillis();
        this.k = null;
    }

    public final void c(Activity activity) {
        StringBuilder sb = new StringBuilder("onActivityStopped [");
        sb.append(activity.getClass().getName());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        Integer num = (Integer) this.i.get(Integer.valueOf(activity.hashCode()));
        if (num != null) {
            Integer valueOf = Integer.valueOf(num.intValue() - 1);
            if (valueOf.intValue() == 0) {
                this.i.remove(Integer.valueOf(activity.hashCode()));
            } else {
                this.i.put(Integer.valueOf(activity.hashCode()), valueOf);
            }
            StringBuilder sb2 = new StringBuilder("Activity removed:[");
            sb2.append(activity.getClass().getName());
            sb2.append(RequestParameters.RIGHT_BRACKETS);
            if (this.i.size() == 0) {
                if (!this.f) {
                    this.d = true;
                    d((Context) activity);
                    if (com.startapp.common.c.a() != null) {
                        com.startapp.common.c.a().a(activity);
                    }
                }
                if (this.b) {
                    com.startapp.android.publish.cache.a.a().a(activity.getApplicationContext(), this.f);
                    this.e = true;
                }
            }
        } else {
            StringBuilder sb3 = new StringBuilder("Activity hadn't been found:[");
            sb3.append(activity.getClass().getName());
            sb3.append(RequestParameters.RIGHT_BRACKETS);
        }
    }

    public final void d(Activity activity) {
        if (activity.getClass().getName().equals(this.m)) {
            this.x = false;
        }
        if (this.i.size() == 0) {
            this.d = false;
        }
    }

    public final boolean f() {
        if (this.k != null) {
            return this.k.isTaskRoot();
        }
        return true;
    }

    public final String g() {
        return this.m;
    }

    public final boolean h() {
        return this.u;
    }

    public final void b(boolean z2) {
        this.v = z2;
    }

    public final void c(boolean z2) {
        this.w = !z2;
        if (!z2) {
            com.startapp.android.publish.cache.a.a().a(Placement.INAPP_SPLASH);
        }
    }

    public final boolean i() {
        return !this.w;
    }

    public final boolean j() {
        return this.c && !this.d && !this.f;
    }

    public final boolean a(Placement placement) {
        if (!this.c || this.f) {
            return false;
        }
        if (!this.d) {
            return true;
        }
        if (placement != Placement.INAPP_RETURN || !com.startapp.android.publish.cache.d.a().b().shouldReturnAdLoadInBg()) {
            return false;
        }
        return true;
    }

    public final void k() {
        this.d = false;
        this.f = true;
    }

    public final boolean l() {
        return this.c && this.d;
    }

    public final void a(Context context, String str, String str2) {
        if (this.r == null) {
            this.r = new TreeMap();
        }
        this.r.put(str, str2);
        k.a(context, "sharedPrefsWrappers", this.r);
    }

    private String a(String str) {
        if (this.r == null) {
            return null;
        }
        return (String) this.r.get(str);
    }

    private boolean b(String str) {
        return a(str) != null;
    }

    public final SDKAdPreferences b(Context context) {
        if (this.f4106a == null) {
            Class<SDKAdPreferences> cls = SDKAdPreferences.class;
            SDKAdPreferences sDKAdPreferences = (SDKAdPreferences) com.startapp.common.a.e.a(context, "shared_prefs_sdk_ad_prefs");
            if (sDKAdPreferences == null) {
                this.f4106a = new SDKAdPreferences();
            } else {
                this.f4106a = sDKAdPreferences;
            }
        }
        return this.f4106a;
    }

    protected static void a(Context context, com.startapp.android.publish.common.metaData.g.a aVar) {
        h.d().a(context, aVar);
    }

    /* access modifiers changed from: protected */
    public final void d(boolean z2) {
        boolean z3 = z2 && com.startapp.common.a.c.b();
        this.u = z3;
        if (!z3) {
            com.startapp.android.publish.cache.a.a().a(Placement.INAPP_RETURN);
        }
    }

    protected static void a(Context context, String str, boolean z2, boolean z3) {
        if (!TextUtils.isEmpty(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(z2 ? "1" : "0");
            sb.append(z3 ? "M" : "A");
            f.a(context, d.USER_CONSENT, str, sb.toString(), "");
            if (str.toLowerCase().equals("pas")) {
                k.b(context, "USER_CONSENT_PERSONALIZED_ADS_SERVING", z2 ? "1" : "0");
                h.d().a(context, com.startapp.android.publish.common.metaData.g.a.PAS);
            }
        }
    }

    public static void c(Context context) {
        b(context, "android.permission.ACCESS_FINE_LOCATION", "USER_CONSENT_FINE_LOCATION");
        b(context, "android.permission.ACCESS_COARSE_LOCATION", "USER_CONSENT_COARSE_LOCATION");
    }

    private static void b(Context context, String str, String str2) {
        boolean booleanValue = k.a(context, str2, Boolean.FALSE).booleanValue();
        boolean a2 = com.startapp.common.a.c.a(context, str);
        if (booleanValue != a2) {
            k.b(context, str2, Boolean.valueOf(a2));
            System.currentTimeMillis();
            a(context, str, a2, false);
        }
    }

    private void d(Context context) {
        a(context, new AdPreferences());
    }

    private static boolean c(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        } catch (Exception unused2) {
            return false;
        }
    }

    private static String d(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".StartAppConstants");
            return (String) Class.forName(sb.toString()).getField("WRAPPER_VERSION").get(null);
        } catch (Exception unused) {
            return "0";
        }
    }

    private static String e(Context context) {
        String str;
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.example.com"));
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
            String str2 = resolveActivity != null ? resolveActivity.activityInfo.packageName : null;
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
            ArrayList arrayList = new ArrayList();
            for (ResolveInfo resolveInfo : queryIntentActivities) {
                Intent intent2 = new Intent();
                intent2.setAction(CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION);
                intent2.setPackage(resolveInfo.activityInfo.packageName);
                if (packageManager.resolveService(intent2, 0) != null) {
                    arrayList.add(resolveInfo.activityInfo.packageName);
                }
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            if (arrayList.size() == 1) {
                str = (String) arrayList.get(0);
            } else if (!TextUtils.isEmpty(str2) && !a(context, intent) && arrayList.contains(str2)) {
                return str2;
            } else {
                if (!arrayList.contains("com.android.chrome")) {
                    return null;
                }
                str = "com.android.chrome";
            }
            return str;
        } catch (Exception unused) {
            return null;
        }
    }

    private static boolean a(Context context, Intent intent) {
        try {
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 64);
            if (queryIntentActivities != null) {
                if (queryIntentActivities.size() != 0) {
                    for (ResolveInfo resolveInfo : queryIntentActivities) {
                        IntentFilter intentFilter = resolveInfo.filter;
                        if (intentFilter != null && intentFilter.countDataAuthorities() != 0 && intentFilter.countDataPaths() != 0 && resolveInfo.activityInfo != null) {
                            return true;
                        }
                    }
                    return false;
                }
            }
            return false;
        } catch (RuntimeException unused) {
        }
    }

    private void a(Context context, AdPreferences adPreferences) {
        if (this.u && !b.a().y()) {
            this.t = com.startapp.android.publish.cache.a.a().a(context, adPreferences);
        }
    }

    public static boolean m() {
        return a.f4110a.a(AdColonyAppOptions.UNITY) != null;
    }
}
