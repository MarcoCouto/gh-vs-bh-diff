package com.startapp.android.publish.adsCommon;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.common.metaData.h;
import com.startapp.common.c.e;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class b implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private static transient Object f4044a = new Object();
    private static Integer b = Integer.valueOf(18);
    private static Integer c = Integer.valueOf(-1);
    private static Set<String> d = new HashSet(Arrays.asList(new String[]{"BOLD"}));
    private static Integer e = Integer.valueOf(ViewCompat.MEASURED_STATE_MASK);
    private static Integer f = Integer.valueOf(-14803426);
    private static Integer g = Integer.valueOf(-1);
    private static b h = new b();
    private static final long serialVersionUID = 1;
    private String acMetadataUpdateVersion = AdsConstants.c;
    @e(a = true)
    private com.startapp.android.publish.adsCommon.b.e adRules = new com.startapp.android.publish.adsCommon.b.e();
    private boolean appPresence = true;
    private boolean autoInterstitialEnabled = true;
    private Integer backgroundGradientBottom = Integer.valueOf(-14606047);
    private Integer backgroundGradientTop = Integer.valueOf(-14606047);
    private int defaultActivitiesBetweenAds = 1;
    private int defaultSecondsBetweenAds = 0;
    private boolean disableInAppStore = false;
    private boolean disableReturnAd = false;
    private boolean disableSplashAd = false;
    private boolean disableTwoClicks = false;
    private boolean enableForceExternalBrowser = true;
    private boolean enableSmartRedirect = true;
    private boolean enforceForeground = false;
    private int forceExternalBrowserDaysInterval = 7;
    private Integer fullpageOfferWallProbability = Integer.valueOf(100);
    private Integer fullpageOverlayProbability = Integer.valueOf(0);
    private Integer homeProbability3D = Integer.valueOf(80);
    private Integer itemDescriptionTextColor = h.DEFAULT_ITEM_DESC_TEXT_COLOR;
    @e(b = HashSet.class)
    private Set<String> itemDescriptionTextDecoration = h.DEFAULT_ITEM_DESC_TEXT_DECORATION;
    private Integer itemDescriptionTextSize = h.DEFAULT_ITEM_DESC_TEXT_SIZE;
    private Integer itemGradientBottom = Integer.valueOf(h.DEFAULT_ITEM_BOTTOM);
    private Integer itemGradientTop = Integer.valueOf(h.DEFAULT_ITEM_TOP);
    private Integer itemTitleTextColor = h.DEFAULT_ITEM_TITLE_TEXT_COLOR;
    @e(b = HashSet.class)
    private Set<String> itemTitleTextDecoration = h.DEFAULT_ITEM_TITLE_TEXT_DECORATION;
    private Integer itemTitleTextSize = h.DEFAULT_ITEM_TITLE_TEXT_SIZE;
    private Integer maxAds = Integer.valueOf(10);
    private Integer poweredByBackgroundColor = f;
    private Integer poweredByTextColor = g;
    private Integer probability3D = Integer.valueOf(0);
    private long returnAdMinBackgroundTime = 300;
    private long smartRedirectLoadedTimeout = 1000;
    private int smartRedirectTimeout = 5;
    @e(b = HashMap.class, c = h.class)
    private HashMap<String, h> templates = new HashMap<>();
    private Integer titleBackgroundColor = Integer.valueOf(-14803426);
    private String titleContent = "Recommended for you";
    private Integer titleLineColor = e;
    private Integer titleTextColor = c;
    @e(b = HashSet.class)
    private Set<String> titleTextDecoration = d;
    private Integer titleTextSize = b;
    @e(a = true)
    private n video = new n();

    public static b a() {
        return h;
    }

    public final int b() {
        return this.fullpageOfferWallProbability.intValue();
    }

    public final int c() {
        return this.fullpageOverlayProbability.intValue();
    }

    public final int d() {
        return this.probability3D.intValue();
    }

    public final int e() {
        return this.backgroundGradientTop.intValue();
    }

    public final h a(String str) {
        return (h) this.templates.get(str);
    }

    public final int f() {
        return this.backgroundGradientBottom.intValue();
    }

    public final int g() {
        return this.maxAds.intValue();
    }

    public final Integer h() {
        return this.titleBackgroundColor;
    }

    public final String i() {
        return this.titleContent;
    }

    public final Integer j() {
        return this.titleTextSize;
    }

    public final Integer k() {
        return this.titleTextColor;
    }

    public final Set<String> l() {
        return this.titleTextDecoration;
    }

    public final Integer m() {
        return this.titleLineColor;
    }

    public final int n() {
        return this.itemGradientTop.intValue();
    }

    public final int o() {
        return this.itemGradientBottom.intValue();
    }

    public final Integer p() {
        return this.itemTitleTextSize;
    }

    public final Integer q() {
        return this.itemTitleTextColor;
    }

    public final Set<String> r() {
        return this.itemTitleTextDecoration;
    }

    public final Integer s() {
        return this.itemDescriptionTextSize;
    }

    public final Integer t() {
        return this.itemDescriptionTextColor;
    }

    public final Set<String> u() {
        return this.itemDescriptionTextDecoration;
    }

    public final Integer v() {
        return this.poweredByBackgroundColor;
    }

    public final Integer w() {
        return this.poweredByTextColor;
    }

    public final long x() {
        return TimeUnit.SECONDS.toMillis(this.returnAdMinBackgroundTime);
    }

    public final boolean y() {
        return this.disableReturnAd;
    }

    public final boolean z() {
        return this.disableSplashAd;
    }

    public final long A() {
        return TimeUnit.SECONDS.toMillis((long) this.smartRedirectTimeout);
    }

    public final long B() {
        return this.smartRedirectLoadedTimeout;
    }

    public final boolean C() {
        return this.enableSmartRedirect;
    }

    public final boolean D() {
        return this.disableTwoClicks;
    }

    public final boolean E() {
        return this.appPresence;
    }

    public final com.startapp.android.publish.adsCommon.b.e F() {
        return this.adRules;
    }

    public final boolean G() {
        return this.disableInAppStore;
    }

    public final n H() {
        return this.video;
    }

    public final boolean I() {
        return this.autoInterstitialEnabled;
    }

    public final int J() {
        return this.defaultActivitiesBetweenAds;
    }

    public final int K() {
        return this.defaultSecondsBetweenAds;
    }

    public final int L() {
        return this.forceExternalBrowserDaysInterval;
    }

    public final boolean M() {
        return this.enableForceExternalBrowser;
    }

    public final boolean N() {
        return this.enforceForeground;
    }

    public static void a(Context context, b bVar) {
        synchronized (f4044a) {
            bVar.acMetadataUpdateVersion = AdsConstants.c;
            h = bVar;
            com.startapp.common.a.e.a(context, "StartappAdsMetadata", (Serializable) bVar);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        return this.returnAdMinBackgroundTime == bVar.returnAdMinBackgroundTime && this.disableReturnAd == bVar.disableReturnAd && this.disableSplashAd == bVar.disableSplashAd && this.smartRedirectTimeout == bVar.smartRedirectTimeout && this.smartRedirectLoadedTimeout == bVar.smartRedirectLoadedTimeout && this.enableSmartRedirect == bVar.enableSmartRedirect && this.autoInterstitialEnabled == bVar.autoInterstitialEnabled && this.defaultActivitiesBetweenAds == bVar.defaultActivitiesBetweenAds && this.defaultSecondsBetweenAds == bVar.defaultSecondsBetweenAds && this.disableTwoClicks == bVar.disableTwoClicks && this.appPresence == bVar.appPresence && this.disableInAppStore == bVar.disableInAppStore && this.forceExternalBrowserDaysInterval == bVar.forceExternalBrowserDaysInterval && this.enableForceExternalBrowser == bVar.enableForceExternalBrowser && this.enforceForeground == bVar.enforceForeground && j.b(this.acMetadataUpdateVersion, bVar.acMetadataUpdateVersion) && j.b(this.probability3D, bVar.probability3D) && j.b(this.homeProbability3D, bVar.homeProbability3D) && j.b(this.fullpageOfferWallProbability, bVar.fullpageOfferWallProbability) && j.b(this.fullpageOverlayProbability, bVar.fullpageOverlayProbability) && j.b(this.backgroundGradientTop, bVar.backgroundGradientTop) && j.b(this.backgroundGradientBottom, bVar.backgroundGradientBottom) && j.b(this.maxAds, bVar.maxAds) && j.b(this.titleBackgroundColor, bVar.titleBackgroundColor) && j.b(this.titleContent, bVar.titleContent) && j.b(this.titleTextSize, bVar.titleTextSize) && j.b(this.titleTextColor, bVar.titleTextColor) && j.b(this.titleTextDecoration, bVar.titleTextDecoration) && j.b(this.titleLineColor, bVar.titleLineColor) && j.b(this.itemGradientTop, bVar.itemGradientTop) && j.b(this.itemGradientBottom, bVar.itemGradientBottom) && j.b(this.itemTitleTextSize, bVar.itemTitleTextSize) && j.b(this.itemTitleTextColor, bVar.itemTitleTextColor) && j.b(this.itemTitleTextDecoration, bVar.itemTitleTextDecoration) && j.b(this.itemDescriptionTextSize, bVar.itemDescriptionTextSize) && j.b(this.itemDescriptionTextColor, bVar.itemDescriptionTextColor) && j.b(this.itemDescriptionTextDecoration, bVar.itemDescriptionTextDecoration) && j.b(this.templates, bVar.templates) && j.b(this.adRules, bVar.adRules) && j.b(this.poweredByBackgroundColor, bVar.poweredByBackgroundColor) && j.b(this.poweredByTextColor, bVar.poweredByTextColor) && j.b(this.video, bVar.video);
    }

    public int hashCode() {
        return j.a(this.acMetadataUpdateVersion, this.probability3D, this.homeProbability3D, this.fullpageOfferWallProbability, this.fullpageOverlayProbability, this.backgroundGradientTop, this.backgroundGradientBottom, this.maxAds, this.titleBackgroundColor, this.titleContent, this.titleTextSize, this.titleTextColor, this.titleTextDecoration, this.titleLineColor, this.itemGradientTop, this.itemGradientBottom, this.itemTitleTextSize, this.itemTitleTextColor, this.itemTitleTextDecoration, this.itemDescriptionTextSize, this.itemDescriptionTextColor, this.itemDescriptionTextDecoration, this.templates, this.adRules, this.poweredByBackgroundColor, this.poweredByTextColor, Long.valueOf(this.returnAdMinBackgroundTime), Boolean.valueOf(this.disableReturnAd), Boolean.valueOf(this.disableSplashAd), Integer.valueOf(this.smartRedirectTimeout), Long.valueOf(this.smartRedirectLoadedTimeout), Boolean.valueOf(this.enableSmartRedirect), Boolean.valueOf(this.autoInterstitialEnabled), Integer.valueOf(this.defaultActivitiesBetweenAds), Integer.valueOf(this.defaultSecondsBetweenAds), Boolean.valueOf(this.disableTwoClicks), Boolean.valueOf(this.appPresence), Boolean.valueOf(this.disableInAppStore), this.video, Integer.valueOf(this.forceExternalBrowserDaysInterval), Boolean.valueOf(this.enableForceExternalBrowser), Boolean.valueOf(this.enforceForeground));
    }

    public static void a(Context context) {
        b bVar = (b) com.startapp.common.a.e.a(context, "StartappAdsMetadata");
        b bVar2 = new b();
        if (bVar != null) {
            boolean a2 = j.a(bVar, bVar2);
            if (!(!AdsConstants.c.equals(bVar.acMetadataUpdateVersion)) && a2) {
                f.a(context, d.METADATA_NULL, "AdsCommonMetaData", "", "");
            }
            bVar.adRules.b();
            h = bVar;
            return;
        }
        h = bVar2;
    }
}
