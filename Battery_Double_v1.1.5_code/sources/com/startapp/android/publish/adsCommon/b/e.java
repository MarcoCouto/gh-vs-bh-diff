package com.startapp.android.publish.adsCommon.b;

import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.Constants;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class e implements Serializable {
    private static final long serialVersionUID = 1;

    /* renamed from: a reason: collision with root package name */
    private transient Set<Class<? extends c>> f4048a = new HashSet();
    private boolean applyOnBannerRefresh = true;
    @com.startapp.common.c.e(b = HashMap.class, c = ArrayList.class, d = Placement.class, e = c.class)
    private Map<Placement, List<c>> placements = new HashMap();
    @com.startapp.common.c.e(b = ArrayList.class, c = c.class)
    private List<c> session = new ArrayList();
    @com.startapp.common.c.e(b = HashMap.class, c = ArrayList.class, e = c.class)
    private Map<String, List<c>> tags = new HashMap();

    public final boolean a() {
        return this.applyOnBannerRefresh;
    }

    public final synchronized f a(Placement placement, String str) {
        f a2;
        String str2;
        this.f4048a.clear();
        List list = (List) this.tags.get(str);
        b.a().a(str);
        a2 = a(list, d.TAG);
        if (a2.a()) {
            List list2 = (List) this.placements.get(placement);
            b.a().a(placement);
            a2 = a(list2, d.PLACEMENT);
            if (a2.a()) {
                List<c> list3 = this.session;
                b.a();
                a2 = a(list3, d.SESSION);
            }
        }
        StringBuilder sb = new StringBuilder("shouldDisplayAd result: ");
        sb.append(a2.a());
        if (a2.a()) {
            str2 = "";
        } else {
            StringBuilder sb2 = new StringBuilder(" because of rule ");
            sb2.append(a2.b());
            str2 = sb2.toString();
        }
        sb.append(str2);
        return a2;
    }

    private f a(List<c> list, d dVar) {
        if (list == null) {
            return new f();
        }
        for (c cVar : list) {
            if (!this.f4048a.contains(cVar.getClass())) {
                if (!cVar.a()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(cVar.getClass().getSimpleName());
                    sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(dVar);
                    Constants.a();
                    return new f(false, sb.toString());
                }
                this.f4048a.add(cVar.getClass());
            }
        }
        return new f();
    }

    public final void b() {
        this.f4048a = new HashSet();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        return this.applyOnBannerRefresh == eVar.applyOnBannerRefresh && j.b(this.session, eVar.session) && j.b(this.placements, eVar.placements) && j.b(this.tags, eVar.tags);
    }

    public final int hashCode() {
        return j.a(this.session, this.placements, this.tags, Boolean.valueOf(this.applyOnBannerRefresh));
    }
}
