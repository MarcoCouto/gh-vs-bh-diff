package com.startapp.android.publish.adsCommon.b;

import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.common.c.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class g implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private static transient g f4049a = new g();
    @e(a = true)
    private e adRules = new e();
    private String adaptMetaDataUpdateVersion = AdsConstants.c;

    private g() {
    }

    public static g a() {
        return f4049a;
    }

    public final e b() {
        return this.adRules;
    }
}
