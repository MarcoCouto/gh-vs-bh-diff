package com.startapp.android.publish.adsCommon.b;

import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class f implements Serializable {
    private static final long serialVersionUID = 1;
    private String reason;
    private boolean shouldDisplayAd;

    public f(boolean z, String str) {
        this.shouldDisplayAd = z;
        this.reason = str;
    }

    public f() {
        this(true, "");
    }

    public final boolean a() {
        return this.shouldDisplayAd;
    }

    public final String b() {
        return this.reason;
    }

    public final String c() {
        return this.reason != null ? this.reason.split(" ")[0] : "";
    }
}
