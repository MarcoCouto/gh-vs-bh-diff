package com.startapp.android.publish.adsCommon.b;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class a implements Comparable<a> {

    /* renamed from: a reason: collision with root package name */
    private long f4045a = System.currentTimeMillis();
    private Placement b;
    private String c;

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        long j = this.f4045a - ((a) obj).f4045a;
        if (j > 0) {
            return 1;
        }
        return j == 0 ? 0 : -1;
    }

    public a(Placement placement, String str) {
        this.b = placement;
        if (str == null) {
            str = "";
        }
        this.c = str;
    }

    public final Placement a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdDisplayEvent [displayTime=");
        sb.append(this.f4045a);
        sb.append(", placement=");
        sb.append(this.b);
        sb.append(", adTag=");
        sb.append(this.c);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
