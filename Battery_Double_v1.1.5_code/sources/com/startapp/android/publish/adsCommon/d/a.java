package com.startapp.android.publish.adsCommon.d;

import android.bluetooth.BluetoothDevice;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private Set<BluetoothDevice> f4060a;
    private Set<BluetoothDevice> b;

    public final void a(BluetoothDevice bluetoothDevice) {
        if (this.b == null) {
            this.b = new HashSet();
        }
        this.b.add(bluetoothDevice);
    }

    public final void a(Set<BluetoothDevice> set) {
        this.f4060a = set;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.f4060a != null && this.f4060a.size() > 0) {
                jSONObject.put("paired", b(this.f4060a));
            }
            if (this.b != null && this.b.size() > 0) {
                jSONObject.put(ParametersKeys.AVAILABLE, b(this.b));
            }
        } catch (Exception unused) {
        }
        if (jSONObject.length() > 0) {
            return jSONObject;
        }
        return null;
    }

    private static JSONArray b(Set<BluetoothDevice> set) {
        try {
            JSONArray jSONArray = new JSONArray();
            for (BluetoothDevice bluetoothDevice : set) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("bluetoothClass", bluetoothDevice.getBluetoothClass().getDeviceClass());
                jSONObject.put("name", bluetoothDevice.getName());
                jSONObject.put("mac", bluetoothDevice.getAddress());
                jSONObject.put("bondState", bluetoothDevice.getBondState());
                jSONArray.put(jSONObject);
            }
            return jSONArray;
        } catch (Exception unused) {
            return null;
        }
    }
}
