package com.startapp.android.publish.adsCommon.i;

import android.content.Context;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.common.metaData.e;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class b extends a {
    public b(Context context, Runnable runnable, com.startapp.android.publish.adsCommon.g.b bVar) {
        super(context, runnable, bVar);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            long millis = TimeUnit.SECONDS.toMillis((long) e.getInstance().getBluetoothConfig().a());
            final com.startapp.android.publish.adsCommon.d.b bVar = new com.startapp.android.publish.adsCommon.d.b(this.f4088a, this);
            a(new Runnable() {
                public final void run() {
                    bVar.a();
                    b.this.a(bVar.b());
                }
            }, millis);
            long currentTimeMillis = System.currentTimeMillis();
            boolean z = currentTimeMillis - k.a(this.f4088a, "lastBtDiscoveringTime", Long.valueOf(0)).longValue() >= ((long) e.getInstance().getBluetoothConfig().c()) * ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
            if (z) {
                k.b(this.f4088a, "lastBtDiscoveringTime", Long.valueOf(currentTimeMillis));
            }
            bVar.a(z);
        } catch (Exception unused) {
            a(null);
        }
    }

    public final void a(Object obj) {
        if (obj != null) {
            this.b.b(obj.toString());
        }
        super.a(obj);
    }
}
