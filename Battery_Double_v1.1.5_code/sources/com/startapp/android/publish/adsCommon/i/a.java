package com.startapp.android.publish.adsCommon.i;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.android.publish.adsCommon.g.b;
import com.startapp.common.d;
import com.startapp.common.f;

/* compiled from: StartAppSDK */
public abstract class a implements d {

    /* renamed from: a reason: collision with root package name */
    protected final Context f4088a;
    protected b b;
    private Runnable c;
    private Handler d = new Handler(Looper.getMainLooper());

    /* access modifiers changed from: protected */
    public abstract void b();

    public a(Context context, Runnable runnable, b bVar) {
        this.f4088a = context;
        this.c = runnable;
        this.b = bVar;
    }

    public final void a() {
        f.a(com.startapp.common.f.a.DEFAULT, (Runnable) new Runnable() {
            public final void run() {
                a.this.b();
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(Runnable runnable, long j) {
        this.d.postDelayed(runnable, j);
    }

    public void a(Object obj) {
        if (this.d != null) {
            this.d.removeCallbacksAndMessages(null);
        }
        this.c.run();
    }
}
