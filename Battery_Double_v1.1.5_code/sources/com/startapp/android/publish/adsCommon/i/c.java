package com.startapp.android.publish.adsCommon.i;

import android.content.Context;
import com.startapp.android.publish.adsCommon.g.b;
import com.startapp.android.publish.common.metaData.e;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class c extends a {
    public c(Context context, Runnable runnable, b bVar) {
        super(context, runnable, bVar);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            long millis = TimeUnit.SECONDS.toMillis((long) e.getInstance().getSensorsConfig().a());
            final com.startapp.android.publish.adsCommon.k.b bVar = new com.startapp.android.publish.adsCommon.k.b(this.f4088a, this);
            a(new Runnable() {
                public final void run() {
                    bVar.b();
                    c.this.a(bVar.c());
                }
            }, millis);
            bVar.a();
        } catch (Exception unused) {
            a(null);
        }
    }

    public final void a(Object obj) {
        if (obj != null) {
            this.b.a(obj.toString());
        }
        super.a(obj);
    }
}
