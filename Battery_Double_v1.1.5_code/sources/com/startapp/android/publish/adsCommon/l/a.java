package com.startapp.android.publish.adsCommon.l;

import android.content.Context;
import android.graphics.Point;
import com.startapp.android.publish.adsCommon.BaseResponse;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.common.a.b;
import com.startapp.common.a.b.C0094b;
import com.startapp.common.a.g;
import com.startapp.common.e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private Point f4105a = new Point();

    public static <T extends BaseResponse> T a(Context context, String str, d dVar, Class<T> cls) {
        return (BaseResponse) j.a(b(context, str, dVar).a(), cls);
    }

    public static com.startapp.common.a.g.a a(Context context, String str, d dVar) {
        return b(context, str, dVar);
    }

    public static boolean a(Context context, String str) {
        b(context, str, null);
        return true;
    }

    private static com.startapp.common.a.g.a b(Context context, String str, d dVar) {
        String str2;
        if (dVar != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(dVar.getRequestString());
            str2 = sb.toString();
        } else {
            str2 = str;
        }
        a(context);
        String str3 = str2;
        int i = 1;
        while (true) {
            if (dVar == null || i <= 1) {
                break;
            }
            try {
                dVar.setRetry(i - 1);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(dVar.getRequestString());
                str3 = sb2.toString();
                break;
            } catch (e e) {
                if (!e.b() || i >= 3) {
                    break;
                }
                if (!(e.a() == 0 || !com.startapp.android.publish.common.metaData.e.getInstance().getInvalidForRetry().contains(Integer.valueOf(e.a())))) {
                    break;
                }
                i++;
                throw e;
            }
        }
        return g.a(str3, k.a(context, "User-Agent", "-1"), com.startapp.android.publish.common.metaData.e.getInstance().isCompressionEnabled());
    }

    private static byte[] a(byte[] bArr) {
        GZIPOutputStream gZIPOutputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(byteArrayOutputStream);
            try {
                gZIPOutputStream2.write(bArr);
                gZIPOutputStream2.flush();
                gZIPOutputStream2.close();
                return byteArrayOutputStream.toByteArray();
            } catch (IOException e) {
                e = e;
                gZIPOutputStream = gZIPOutputStream2;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                gZIPOutputStream = gZIPOutputStream2;
                if (gZIPOutputStream != null) {
                    try {
                        gZIPOutputStream.close();
                    } catch (Exception unused) {
                    }
                }
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            throw e;
        }
    }

    private static Map<String, String> a(Context context) {
        HashMap hashMap = new HashMap();
        if (!com.startapp.android.publish.common.metaData.e.getInstance().getDisableSendAdvertisingId()) {
            C0094b a2 = b.a().a(context);
            String a3 = a2.a();
            if ((a3.equals("0") || a3.equals("")) && !k.a(context, com.startapp.android.publish.adsCommon.g.d.NO_ADVERTISING_ID.a(), Boolean.FALSE).booleanValue()) {
                k.b(context, com.startapp.android.publish.adsCommon.g.d.NO_ADVERTISING_ID.a(), Boolean.TRUE);
                f.a(context, com.startapp.android.publish.adsCommon.g.d.NO_ADVERTISING_ID, "TransportHttpApache.addAdditionalHeaders", a2.d(), "");
            }
            String a4 = a2.a();
            try {
                a4 = URLEncoder.encode(a4, "UTF-8");
            } catch (UnsupportedEncodingException unused) {
            }
            hashMap.put("device-id", a4);
        }
        hashMap.put("Accept-Language", Locale.getDefault().toString());
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        r7 = r4;
        r4 = r3;
        r3 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0034, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0056, code lost:
        throw new com.startapp.common.e("failed compressing json to gzip", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005e, code lost:
        throw new com.startapp.common.e("failed encoding json to UTF-8", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0067, code lost:
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006d, code lost:
        if (r12 > 0) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        java.lang.Thread.sleep(r12);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0034 A[ExcHandler: IOException (r8v2 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:8:0x0014] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0036 A[ExcHandler: UnsupportedEncodingException (r8v1 'e' java.io.UnsupportedEncodingException A[CUSTOM_DECLARE]), Splitter:B:8:0x0014] */
    public static boolean a(Context context, String str, d dVar, int i, long j) {
        Object nameValueJson = dVar != null ? dVar.getNameValueJson() : null;
        a(context);
        boolean z = false;
        byte[] bArr = null;
        int i2 = 1;
        while (!z) {
            if (nameValueJson != null) {
                try {
                    byte[] bytes = nameValueJson.toString().getBytes("UTF-8");
                    bArr = com.startapp.android.publish.common.metaData.e.getInstance().isCompressionEnabled() ? a(bytes) : bytes;
                } catch (e e) {
                    e = e;
                    if (e.b()) {
                    }
                    throw e;
                } catch (UnsupportedEncodingException e2) {
                } catch (IOException e3) {
                }
            }
            g.a(str, bArr, k.a(context, "User-Agent", "-1"), com.startapp.android.publish.common.metaData.e.getInstance().isCompressionEnabled());
            z = true;
        }
        return true;
    }

    public a() {
    }

    public a(int i, int i2) {
        a(i, i2);
    }

    private void a(int i) {
        this.f4105a.x = i;
    }

    private void b(int i) {
        this.f4105a.y = i;
    }

    public final void a(int i, int i2) {
        a(i);
        b(i2);
    }

    public final int a() {
        return this.f4105a.x;
    }

    public final int b() {
        return this.f4105a.y;
    }
}
