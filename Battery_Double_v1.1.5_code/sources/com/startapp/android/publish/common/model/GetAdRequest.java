package com.startapp.android.publish.common.model;

import android.content.Context;
import android.os.Build.VERSION;
import android.provider.Settings.Global;
import android.provider.Settings.System;
import android.telephony.NeighboringCellInfo;
import android.util.Pair;
import com.facebook.appevents.UserDataStore;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.Ad.AdType;
import com.startapp.android.publish.adsCommon.SDKAdPreferences.Gender;
import com.startapp.android.publish.adsCommon.a.f;
import com.startapp.android.publish.adsCommon.a.h;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.b;
import com.startapp.android.publish.adsCommon.d;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.a.a;
import java.util.HashSet;
import java.util.Set;

/* compiled from: StartAppSDK */
public class GetAdRequest extends d {
    private int adsDisplayed;
    private int adsNumber = 1;
    private String advertiserId = null;
    private Boolean ai;
    private Boolean as;
    private Set<String> campaignExclude = null;
    private Set<String> categories = null;
    private Set<String> categoriesExclude = null;
    private boolean contentAd;
    private String country = null;
    private boolean engInclude = true;
    private Gender gender;
    private Boolean isAutoDateTimeEnabled;
    private boolean isDefaultMetaData = true;
    private boolean isDisableTwoClicks = b.a().D();
    private boolean isHardwareAccelerated = true;
    private String keywords;
    private Double minCpm;
    private String moreImg;
    private int offset = 0;
    private Set<String> packageExclude = null;
    private Set<String> packageInclude = null;
    private Placement placement;
    private String primaryImg;
    private String profileId;
    private Pair<String, String> simpleToken;
    private String template;
    private boolean testMode;
    private long timeSinceSessionStart = (System.currentTimeMillis() - h.d().b());
    private AdType type = null;

    /* compiled from: StartAppSDK */
    static class CellScanResult {
        private static final char DELIMITER = ',';
        private NeighboringCellInfo cellInfo;

        public CellScanResult(NeighboringCellInfo neighboringCellInfo) {
            this.cellInfo = neighboringCellInfo;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            if (this.cellInfo != null) {
                sb.append(this.cellInfo.getCid());
                sb.append(DELIMITER);
                sb.append(this.cellInfo.getRssi());
                sb.append(DELIMITER);
                sb.append(this.cellInfo.getPsc());
                sb.append(DELIMITER);
                sb.append(this.cellInfo.getNetworkType());
                sb.append(DELIMITER);
                sb.append(this.cellInfo.getLac());
            }
            return sb.toString();
        }
    }

    /* compiled from: StartAppSDK */
    protected enum VideoRequestMode {
        INTERSTITIAL,
        REWARDED
    }

    /* compiled from: StartAppSDK */
    protected enum VideoRequestType {
        ENABLED,
        DISABLED,
        FORCED,
        FORCED_NONVAST
    }

    public GetAdRequest() {
        if (!j.a()) {
            this.adsDisplayed = com.startapp.android.publish.adsCommon.b.b.a().c();
        }
        this.profileId = e.getInstance().getProfileId();
    }

    public Placement getPlacement() {
        return this.placement;
    }

    public void setPlacement(Placement placement2) {
        this.placement = placement2;
    }

    public boolean isTestMode() {
        return this.testMode;
    }

    /* access modifiers changed from: protected */
    public boolean isDisableTwoClicks() {
        return this.isDisableTwoClicks;
    }

    public void setDisableTwoClicks(boolean z) {
        this.isDisableTwoClicks = z;
    }

    public void setTestMode(boolean z) {
        this.testMode = z;
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender2) {
        this.gender = gender2;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    public String getTemplate() {
        return this.template;
    }

    public void setTemplate(String str) {
        this.template = str;
    }

    public int getAdsNumber() {
        return this.adsNumber;
    }

    public void setAdsNumber(int i) {
        this.adsNumber = i;
    }

    public String getPrimaryImg() {
        return this.primaryImg;
    }

    public void setPrimaryImg(String str) {
        this.primaryImg = str;
    }

    public String getMoreImg() {
        return this.moreImg;
    }

    public void setMoreImg(String str) {
        this.moreImg = str;
    }

    public boolean isContentAd() {
        return this.contentAd;
    }

    public void setContentAd(boolean z) {
        this.contentAd = z;
    }

    public Set<String> getCategories() {
        return this.categories;
    }

    public void setCategories(Set<String> set) {
        this.categories = set;
    }

    public double getMinCpm() {
        return this.minCpm.doubleValue();
    }

    public void setMinCpm(double d) {
        this.minCpm = Double.valueOf(d);
    }

    public void addCategory(String str) {
        if (this.categories == null) {
            this.categories = new HashSet();
        }
        this.categories.add(str);
    }

    public Set<String> getCategoriesExclude() {
        return this.categoriesExclude;
    }

    public void setCategoriesExclude(Set<String> set) {
        this.categoriesExclude = set;
    }

    public void addCategoryExclude(String str) {
        if (this.categoriesExclude == null) {
            this.categoriesExclude = new HashSet();
        }
        this.categoriesExclude.add(str);
    }

    public Set<String> getPackageExclude() {
        return this.packageExclude;
    }

    public void setPackageExclude(Set<String> set) {
        this.packageExclude = set;
    }

    public Set<String> getPackageInclude() {
        return this.packageInclude;
    }

    public void setPackageInclude(Set<String> set) {
        this.packageInclude = set;
    }

    public Set<String> getCampaignExclude() {
        return this.campaignExclude;
    }

    public boolean hasCampaignExclude() {
        return this.campaignExclude != null && this.campaignExclude.size() > 0;
    }

    public void setCampaignExclude(Set<String> set) {
        this.campaignExclude = set;
    }

    public int getOffset() {
        return this.offset;
    }

    public void setOffset(int i) {
        this.offset = i;
    }

    public Pair<String, String> getSimpleToken() {
        return this.simpleToken;
    }

    public void setSimpleToken(Pair<String, String> pair) {
        this.simpleToken = pair;
    }

    public boolean isEngInclude() {
        return this.engInclude;
    }

    public void setEngInclude(boolean z) {
        this.engInclude = z;
    }

    public Boolean getAi() {
        return this.ai;
    }

    public void setAi(Boolean bool) {
        this.ai = bool;
    }

    public Boolean getAs() {
        return this.as;
    }

    public void setAs(Boolean bool) {
        this.as = bool;
    }

    public void setRetry(int i) {
        this.retry = Integer.valueOf(i);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GetAdRequest [");
        StringBuilder sb2 = new StringBuilder("placement=");
        sb2.append(this.placement);
        sb.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder(", testMode=");
        sb3.append(this.testMode);
        sb.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder(", gender=");
        sb4.append(this.gender);
        sb.append(sb4.toString());
        StringBuilder sb5 = new StringBuilder(", ai=");
        sb5.append(this.ai);
        sb.append(sb5.toString());
        StringBuilder sb6 = new StringBuilder(", as=");
        sb6.append(this.as);
        sb.append(sb6.toString());
        StringBuilder sb7 = new StringBuilder(", keywords=");
        sb7.append(this.keywords);
        sb.append(sb7.toString());
        StringBuilder sb8 = new StringBuilder(", template=");
        sb8.append(this.template);
        sb.append(sb8.toString());
        StringBuilder sb9 = new StringBuilder(", adsNumber=");
        sb9.append(this.adsNumber);
        sb.append(sb9.toString());
        StringBuilder sb10 = new StringBuilder(", offset=");
        sb10.append(this.offset);
        sb.append(sb10.toString());
        StringBuilder sb11 = new StringBuilder(", categories=");
        sb11.append(this.categories);
        sb.append(sb11.toString());
        StringBuilder sb12 = new StringBuilder(", categoriesExclude=");
        sb12.append(this.categoriesExclude);
        sb.append(sb12.toString());
        StringBuilder sb13 = new StringBuilder(", packageExclude=");
        sb13.append(this.packageExclude);
        sb.append(sb13.toString());
        StringBuilder sb14 = new StringBuilder(", packageInclude=");
        sb14.append(this.packageInclude);
        sb.append(sb14.toString());
        StringBuilder sb15 = new StringBuilder(", simpleToken=");
        sb15.append(this.simpleToken);
        sb.append(sb15.toString());
        StringBuilder sb16 = new StringBuilder(", engInclude=");
        sb16.append(this.engInclude);
        sb.append(sb16.toString());
        StringBuilder sb17 = new StringBuilder(", country=");
        sb17.append(this.country);
        sb.append(sb17.toString());
        StringBuilder sb18 = new StringBuilder(", advertiserId=");
        sb18.append(this.advertiserId);
        sb.append(sb18.toString());
        StringBuilder sb19 = new StringBuilder(", type=");
        sb19.append(this.type);
        sb.append(sb19.toString());
        StringBuilder sb20 = new StringBuilder(", minCpm=");
        sb20.append(this.minCpm);
        sb.append(sb20.toString());
        StringBuilder sb21 = new StringBuilder(", sessionStartTime=");
        sb21.append(this.timeSinceSessionStart);
        sb.append(sb21.toString());
        StringBuilder sb22 = new StringBuilder(", adsDisplayed=");
        sb22.append(this.adsDisplayed);
        sb.append(sb22.toString());
        StringBuilder sb23 = new StringBuilder(", profileId=");
        sb23.append(this.profileId);
        sb.append(sb23.toString());
        StringBuilder sb24 = new StringBuilder(", hardwareAccelerated=");
        sb24.append(this.isHardwareAccelerated);
        sb.append(sb24.toString());
        StringBuilder sb25 = new StringBuilder(", primaryImg=");
        sb25.append(this.primaryImg);
        sb.append(sb25.toString());
        StringBuilder sb26 = new StringBuilder(", moreImg=");
        sb26.append(this.moreImg);
        sb.append(sb26.toString());
        StringBuilder sb27 = new StringBuilder(", contentAd=");
        sb27.append(this.contentAd);
        sb.append(sb27.toString());
        StringBuilder sb28 = new StringBuilder(", defaultMetaData=");
        sb28.append(this.isDefaultMetaData);
        sb.append(sb28.toString());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public void fillAdPreferences(Context context, AdPreferences adPreferences, Placement placement2, Pair<String, String> pair) {
        this.placement = placement2;
        this.simpleToken = pair;
        this.ai = adPreferences.getAi();
        this.as = adPreferences.getAs();
        this.gender = adPreferences.getGender(context);
        this.keywords = adPreferences.getKeywords();
        this.testMode = adPreferences.isTestMode();
        this.categories = adPreferences.getCategories();
        this.categoriesExclude = adPreferences.getCategoriesExclude();
        this.isHardwareAccelerated = adPreferences.isHardwareAccelerated();
        boolean z = false;
        if (VERSION.SDK_INT < 17 ? System.getInt(context.getContentResolver(), "auto_time", 0) > 0 : Global.getInt(context.getContentResolver(), "auto_time", 0) > 0) {
            z = true;
        }
        this.isAutoDateTimeEnabled = Boolean.valueOf(z);
        this.minCpm = adPreferences.getMinCpm();
        this.isDefaultMetaData = !e.isLoadedFromServer(context);
        fillLocationDetails(adPreferences, context);
        setCountry(adPreferences.country);
        setAdvertiser(adPreferences.advertiserId);
        setTemplate(adPreferences.template);
        setType(adPreferences.type);
        setPackageInclude(adPreferences.packageInclude);
    }

    public boolean isAdTypeVideo() {
        return getType() == AdType.VIDEO || getType() == AdType.REWARDED_VIDEO;
    }

    public f getNameValueMap() {
        f nameValueMap = super.getNameValueMap();
        if (nameValueMap == null) {
            nameValueMap = new com.startapp.android.publish.adsCommon.a.d();
        }
        addParams(nameValueMap);
        return nameValueMap;
    }

    private void addParams(f fVar) {
        fVar.a("placement", this.placement.name(), true);
        fVar.a("testMode", Boolean.toString(this.testMode), false);
        fVar.a("gender", this.gender, false);
        fVar.a("keywords", this.keywords, false);
        fVar.a("template", this.template, false);
        fVar.a("adsNumber", Integer.toString(this.adsNumber), false);
        fVar.a("category", this.categories);
        fVar.a("categoryExclude", this.categoriesExclude);
        fVar.a("packageExclude", this.packageExclude);
        fVar.a("campaignExclude", this.campaignExclude);
        fVar.a("offset", Integer.toString(this.offset), false);
        fVar.a("ai", this.ai, false);
        fVar.a("as", this.as, false);
        fVar.a("minCPM", j.a(this.minCpm), false);
        fVar.a("twoClicks", Boolean.valueOf(!this.isDisableTwoClicks), false);
        fVar.a("engInclude", Boolean.toString(this.engInclude), false);
        if (getType() == AdType.INTERSTITIAL || getType() == AdType.RICH_TEXT) {
            fVar.a("type", this.type, false);
        }
        fVar.a("timeSinceSessionStart", Long.valueOf(this.timeSinceSessionStart), true);
        fVar.a("adsDisplayed", Integer.valueOf(this.adsDisplayed), true);
        fVar.a("profileId", this.profileId, false);
        fVar.a("hardwareAccelerated", Boolean.valueOf(this.isHardwareAccelerated), false);
        fVar.a("dts", this.isAutoDateTimeEnabled, false);
        fVar.a("downloadingMode", "CACHE", false);
        fVar.a("primaryImg", this.primaryImg, false);
        fVar.a("moreImg", this.moreImg, false);
        fVar.a("contentAd", Boolean.toString(this.contentAd), false);
        String d = a.d();
        fVar.a(a.a(), d, true);
        String c = a.c();
        StringBuilder sb = new StringBuilder();
        sb.append(getProductId());
        sb.append(this.placement.name());
        sb.append(getSessionId());
        sb.append(getSdkVersion());
        sb.append(d);
        fVar.a(c, a.b(sb.toString()), true, false);
        if (getCountry() != null) {
            fVar.a(UserDataStore.COUNTRY, getCountry(), false);
        }
        if (getAdvertiserId() != null) {
            fVar.a("advertiserId", getAdvertiserId(), false);
        }
        if (getPackageInclude() != null) {
            fVar.a("packageInclude", getPackageInclude());
        }
        fVar.a("defaultMetaData", Boolean.valueOf(this.isDefaultMetaData), true);
        fVar.a((String) this.simpleToken.first, this.simpleToken.second, false);
    }

    public void setCountry(String str) {
        this.country = str;
    }

    public void setAdvertiser(String str) {
        this.advertiserId = str;
    }

    public String getCountry() {
        return this.country;
    }

    public String getAdvertiserId() {
        return this.advertiserId;
    }

    public AdType getType() {
        return this.type;
    }

    public void setType(AdType adType) {
        this.type = adType;
    }
}
