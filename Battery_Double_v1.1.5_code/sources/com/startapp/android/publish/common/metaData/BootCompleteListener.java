package com.startapp.android.publish.common.metaData;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.startapp.android.publish.adsCommon.a.b;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;

/* compiled from: StartAppSDK */
public class BootCompleteListener extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime() + ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
            b.a(context);
            b.a(context, Long.valueOf(elapsedRealtime));
            b.a(context, elapsedRealtime);
        } catch (Exception e) {
            f.a(context, d.EXCEPTION, "BootCompleteListener.onReceive - failed to start services", e.getMessage(), "");
        }
    }
}
