package com.startapp.android.publish.common.metaData;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.Constants;
import com.startapp.common.a.C0093a;
import com.startapp.common.a.c;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class e implements Serializable {
    public static final String DEFAULT_AD_PLATFORM_HOST = new String("https://req.startappservice.com/1.5/");
    public static final boolean DEFAULT_ALWAYS_SEND_TOKEN = true;
    public static final String DEFAULT_ASSETS_BASE_URL_SECURED = "";
    public static final boolean DEFAULT_BT_ENABLED = false;
    public static final boolean DEFAULT_COMPRESSION_ENABLED = false;
    public static final boolean DEFAULT_INAPPBROWSER = true;
    public static final Set<String> DEFAULT_INSTALLERS_LIST = new HashSet(Arrays.asList(new String[]{Constants.f4160a}));
    public static final Set<Integer> DEFAULT_INVALID_NETWORK_CODES_INFO_EVENTS = new HashSet(Arrays.asList(new Integer[]{Integer.valueOf(204)}));
    public static final long DEFAULT_LAST_KNOWN_LOCATION_THRESHOLD = 30;
    public static final String DEFAULT_LOCATION_SOURCE = "API";
    public static final String DEFAULT_METADATA_HOST = new String("https://init.startappservice.com/1.5/");
    public static final int DEFAULT_NOT_VISIBLE_BANNER_RELOAD_INTERVAL = 3600;
    public static final boolean DEFAULT_OM_SDK_STATE = false;
    public static final boolean DEFAULT_PERIODIC_INFOEVENT_ENABLED = false;
    public static final int DEFAULT_PERIODIC_INFOEVENT_INTERVAL = 360;
    public static final int[] DEFAULT_PERIODIC_INFOEVENT_INTERVALS = {60, 60, PsExtractor.VIDEO_STREAM_MASK};
    public static final boolean DEFAULT_PERIODIC_INFOEVENT_ON_RUN_TIME = false;
    public static final boolean DEFAULT_PERIODIC_METADATA_ENABLED = false;
    public static final int DEFAULT_PERIODIC_METADATA_INTERVAL = 360;
    public static final Set<String> DEFAULT_PRE_INSTALLED_PACKAGES = new HashSet(Arrays.asList(new String[]{new String("com.facebook.katana"), new String("com.yandex.browser")}));
    public static final String DEFAULT_PROFILE_ID = null;
    public static final int DEFAULT_SESSION_MAX_BACKGROUND_TIME = 1800;
    public static final boolean DEFAULT_SIMPLE_TOKEN_ENABLED = true;
    public static final int DEFAULT_STOP_AUTO_LOAD_AMOUNT = 3;
    public static final int DEFAULT_STOP_AUTO_LOAD_PRE_CAHE_AMOUNT = 3;
    public static final boolean DEFAULT_WF_SCAN_ENABLED = false;
    public static final String KEY_METADATA = "metaData";
    private static volatile e instance = new e();
    private static final Object lock = new Object();
    private static final long serialVersionUID = 1;
    private static c task;
    private long IABDisplayImpressionDelayInSeconds = 1;
    private long IABVideoImpressionDelayInSeconds = 2;
    @com.startapp.common.c.e(a = true)
    private l SimpleToken = new l();
    private boolean SupportIABViewability = true;
    private String adPlatformBannerHostSecured;
    @VisibleForTesting
    public String adPlatformHostSecured = DEFAULT_AD_PLATFORM_HOST;
    private String adPlatformNativeHostSecured;
    private String adPlatformOverlayHostSecured;
    private String adPlatformReturnHostSecured;
    private String adPlatformSplashHostSecured;
    private boolean alwaysSendToken = true;
    @VisibleForTesting
    @com.startapp.common.c.e(a = true)
    public com.startapp.android.publish.adsCommon.g.a analytics = new com.startapp.android.publish.adsCommon.g.a();
    private String assetsBaseUrlSecured = "";
    @com.startapp.common.c.e(a = true)
    private b btConfig = new b();
    private boolean btEnabled = false;
    private boolean chromeCustomeTabsExternal = true;
    private boolean chromeCustomeTabsInternal = true;
    private boolean compressionEnabled = false;
    private boolean disableSendAdvertisingId = AdsConstants.b.booleanValue();
    private boolean dns = false;
    private boolean inAppBrowser = true;
    @com.startapp.common.c.e(b = b.class)
    private b inAppBrowserPreLoad;
    @com.startapp.common.c.e(b = HashSet.class)
    private Set<String> installersList = DEFAULT_INSTALLERS_LIST;
    @com.startapp.common.c.e(b = HashSet.class)
    private Set<Integer> invalidForRetry = new HashSet();
    @com.startapp.common.c.e(b = HashSet.class)
    private Set<Integer> invalidNetworkCodesInfoEvents = DEFAULT_INVALID_NETWORK_CODES_INFO_EVENTS;
    private boolean isToken1Mandatory = true;
    private transient boolean loading = false;
    @com.startapp.common.c.e(a = true)
    private d location = new d();
    @VisibleForTesting
    public String metaDataHostSecured = DEFAULT_METADATA_HOST;
    private transient List<f> metaDataListeners = new ArrayList();
    private String metadataUpdateVersion = AdsConstants.c;
    private int notVisibleBannerReloadInterval = 3600;
    private boolean omSdkEnabled = false;
    private int[] periodicEventIntMin = DEFAULT_PERIODIC_INFOEVENT_INTERVALS;
    private boolean periodicInfoEventEnabled = false;
    private int periodicInfoEventIntervalInMinutes = 360;
    private boolean periodicInfoEventOnRunTimeEnabled = false;
    private boolean periodicMetaDataEnabled = false;
    private int periodicMetaDataIntervalInMinutes = 360;
    @com.startapp.common.c.e(b = HashSet.class)
    private Set<String> preInstalledPackages = DEFAULT_PRE_INSTALLED_PACKAGES;
    private String profileId = DEFAULT_PROFILE_ID;
    private transient boolean ready = false;
    @com.startapp.common.c.e(a = true)
    private k sensorsConfig = new k();
    private int sessionMaxBackgroundTime = 1800;
    private boolean simpleToken2 = true;
    private int stopAutoLoadAmount = 3;
    private int stopAutoLoadPreCacheAmount = 3;
    private boolean trueNetEnabled = false;
    private long userAgentDelayInSeconds = 5;
    private boolean userAgentEnabled = true;
    private boolean webViewSecured = true;
    private boolean wfScanEnabled = false;

    /* compiled from: StartAppSDK */
    public static class a implements C0093a {

        /* renamed from: a reason: collision with root package name */
        private Context f4148a;
        private String b;

        public a(Context context, String str) {
            this.f4148a = context;
            this.b = str;
        }

        public final void a(Bitmap bitmap, int i) {
            if (bitmap != null) {
                com.startapp.android.publish.adsCommon.a.a.a(this.f4148a, bitmap, this.b, ".png");
            }
        }
    }

    /* compiled from: StartAppSDK */
    public enum b {
        DISABLED,
        CONTENT,
        FULL
    }

    public l getSimpleTokenConfig() {
        return this.SimpleToken;
    }

    /* access modifiers changed from: protected */
    public void setSimpleTokenConfig(l lVar) {
        this.SimpleToken = lVar;
    }

    public static void update(Context context, e eVar, boolean z) {
        List<f> list;
        synchronized (lock) {
            Constants.a();
            if (getInstance().metaDataListeners != null) {
                list = new ArrayList<>(getInstance().metaDataListeners);
                getInstance().metaDataListeners.clear();
            } else {
                list = null;
            }
            eVar.metaDataListeners = getInstance().metaDataListeners;
            eVar.applyAdPlatformProtocolToHosts();
            eVar.metadataUpdateVersion = AdsConstants.c;
            com.startapp.common.a.e.b(context, "StartappMetadata", eVar);
            eVar.loading = false;
            eVar.ready = true;
            if (!j.b(getInstance(), eVar)) {
                z = true;
            }
            instance = eVar;
            k.b(context, "totalSessions", Integer.valueOf(k.a(context, "totalSessions", Integer.valueOf(0)).intValue() + 1));
            task = null;
        }
        if (list != null) {
            for (f a2 : list) {
                a2.a(z);
            }
        }
    }

    public static void failedLoading() {
        ArrayList<f> arrayList;
        synchronized (lock) {
            if (getInstance().metaDataListeners != null) {
                arrayList = new ArrayList<>(getInstance().metaDataListeners);
                getInstance().metaDataListeners.clear();
            } else {
                arrayList = null;
            }
            getInstance().loading = false;
        }
        if (arrayList != null) {
            for (f a2 : arrayList) {
                a2.a();
            }
        }
    }

    public static boolean isLoadedFromServer(Context context) {
        return context.getFileStreamPath("StartappMetadata").exists();
    }

    public void loadFromServer(Context context, AdPreferences adPreferences, com.startapp.android.publish.common.metaData.g.a aVar, boolean z, f fVar) {
        loadFromServer(context, adPreferences, aVar, z, fVar, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        if (r8 == false) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        if (r9 == null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0032, code lost:
        r9.a(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0066, code lost:
        return;
     */
    public void loadFromServer(Context context, AdPreferences adPreferences, com.startapp.android.publish.common.metaData.g.a aVar, boolean z, f fVar, boolean z2) {
        if (TextUtils.isEmpty(adPreferences.getProductId())) {
            f.a(context, d.NO_APPID, "MetaData.loadFromServer", "The productId wasn't set", "");
        }
        if (!z && fVar != null) {
            fVar.a(false);
        }
        synchronized (lock) {
            if (getInstance().isReady()) {
                if (z2) {
                }
            }
            if (!getInstance().isLoading() || z2) {
                this.loading = true;
                this.ready = false;
                if (task != null) {
                    task.b();
                }
                c cVar = new c(context, adPreferences, aVar);
                task = cVar;
                cVar.a();
            }
            if (z && fVar != null) {
                getInstance().addMetaDataListener(fVar);
            }
        }
    }

    public void addMetaDataListener(f fVar) {
        synchronized (lock) {
            this.metaDataListeners.add(fVar);
        }
    }

    public static Object getLock() {
        return lock;
    }

    public boolean isLoading() {
        return this.loading;
    }

    public boolean isReady() {
        return this.ready;
    }

    public void setReady(boolean z) {
        this.ready = z;
    }

    private boolean isMetaDataVersionChanged() {
        return !AdsConstants.c.equals(this.metadataUpdateVersion);
    }

    public String getAssetsBaseUrl() {
        return this.assetsBaseUrlSecured != null ? this.assetsBaseUrlSecured : "";
    }

    public boolean isPeriodicMetaDataEnabled() {
        return this.periodicMetaDataEnabled;
    }

    public void setPeriodicMetaDataEnabled(boolean z) {
        this.periodicMetaDataEnabled = z;
    }

    public int getPeriodicMetaDataInterval() {
        return this.periodicMetaDataIntervalInMinutes;
    }

    public void setPeriodicMetaDataInterval(int i) {
        this.periodicMetaDataIntervalInMinutes = i;
    }

    public boolean isPeriodicInfoEventEnabled() {
        return this.periodicInfoEventEnabled;
    }

    public boolean isPeriodicInfoEventOnRunTimeEnabled() {
        return this.periodicInfoEventOnRunTimeEnabled;
    }

    public void setPeriodicInfoEventEnabled(boolean z) {
        this.periodicInfoEventEnabled = z;
    }

    public int getPeriodicInfoEventIntervalInMinutes(Context context) {
        if (this.periodicEventIntMin == null || this.periodicEventIntMin.length < 3) {
            this.periodicEventIntMin = DEFAULT_PERIODIC_INFOEVENT_INTERVALS;
        }
        if (c.a(context, "android.permission.ACCESS_FINE_LOCATION")) {
            int i = this.periodicEventIntMin[0];
            if (i <= 0) {
                return DEFAULT_PERIODIC_INFOEVENT_INTERVALS[0];
            }
            return i;
        } else if (!c.a(context, "android.permission.ACCESS_COARSE_LOCATION")) {
            return this.periodicEventIntMin[2];
        } else {
            int i2 = this.periodicEventIntMin[1];
            if (i2 <= 0) {
                return DEFAULT_PERIODIC_INFOEVENT_INTERVALS[1];
            }
            return i2;
        }
    }

    public void setPeriodicInfoEventIntervalInMinutes(int i) {
        this.periodicInfoEventIntervalInMinutes = i;
    }

    public Set<Integer> getInvalidForRetry() {
        if (this.invalidForRetry != null) {
            return this.invalidForRetry;
        }
        return new HashSet();
    }

    public Set<Integer> getInvalidNetworkCodesInfoEvents() {
        if (this.invalidNetworkCodesInfoEvents != null) {
            return this.invalidNetworkCodesInfoEvents;
        }
        return DEFAULT_INVALID_NETWORK_CODES_INFO_EVENTS;
    }

    public String getMetaDataHost() {
        if (AdsConstants.OVERRIDE_HOST != null) {
            return AdsConstants.OVERRIDE_HOST;
        }
        return this.metaDataHostSecured;
    }

    public String getAdPlatformHost() {
        if (AdsConstants.OVERRIDE_HOST != null) {
            return AdsConstants.OVERRIDE_HOST;
        }
        return this.adPlatformHostSecured != null ? this.adPlatformHostSecured : DEFAULT_AD_PLATFORM_HOST;
    }

    public String getAdPlatformHost(Placement placement) {
        switch (placement) {
            case INAPP_BANNER:
                return this.adPlatformBannerHostSecured != null ? this.adPlatformBannerHostSecured : getAdPlatformHost();
            case INAPP_OVERLAY:
                return this.adPlatformOverlayHostSecured != null ? this.adPlatformOverlayHostSecured : getAdPlatformHost();
            case INAPP_NATIVE:
                return this.adPlatformNativeHostSecured != null ? this.adPlatformNativeHostSecured : getAdPlatformHost();
            case INAPP_RETURN:
                return this.adPlatformReturnHostSecured != null ? this.adPlatformReturnHostSecured : getAdPlatformHost();
            case INAPP_SPLASH:
                return this.adPlatformSplashHostSecured != null ? this.adPlatformSplashHostSecured : getAdPlatformHost();
            default:
                return getAdPlatformHost();
        }
    }

    public String getHostForWebview() {
        return getHostForWebview(getInstance().getAdPlatformHost(), VERSION.SDK_INT, this.webViewSecured);
    }

    @VisibleForTesting
    public static String getHostForWebview(String str, int i, boolean z) {
        String str2 = (i > 26 || z) ? "https" : "http";
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append("://");
        if (str.startsWith(sb.toString())) {
            return str;
        }
        int indexOf = str.indexOf(58);
        if (indexOf == -1) {
            return str;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str2);
        sb2.append(str.substring(indexOf));
        return sb2.toString();
    }

    public long getSessionMaxBackgroundTime() {
        return TimeUnit.SECONDS.toMillis((long) this.sessionMaxBackgroundTime);
    }

    public Set<String> getInstallersList() {
        return this.installersList;
    }

    public void setInstallersList(Set<String> set) {
        this.installersList = set;
    }

    public Set<String> getPreInstalledPackages() {
        Set<String> set = this.preInstalledPackages;
        if (set == null) {
            set = DEFAULT_PRE_INSTALLED_PACKAGES;
        }
        return Collections.unmodifiableSet(set);
    }

    public void setPreInstalledPackages(Set<String> set) {
        this.preInstalledPackages = set;
    }

    public boolean isSimpleToken2() {
        return this.simpleToken2;
    }

    public void setSimpleToken2(boolean z) {
        this.simpleToken2 = z;
    }

    public boolean isAlwaysSendToken() {
        return this.alwaysSendToken;
    }

    public void setAlwaysSendToken(boolean z) {
        this.alwaysSendToken = z;
    }

    public boolean isToken1Mandatory() {
        return this.isToken1Mandatory;
    }

    public boolean isCompressionEnabled() {
        return this.compressionEnabled;
    }

    public void setCompressionEnabled(boolean z) {
        this.compressionEnabled = z;
    }

    public boolean isInAppBrowser() {
        return j.a(256) && this.inAppBrowser;
    }

    public void setInAppBrowser(boolean z) {
        this.inAppBrowser = z;
    }

    public b getInAppBrowserPreLoad() {
        return this.inAppBrowserPreLoad;
    }

    public void setInAppBrowserPreLoad(b bVar) {
        this.inAppBrowserPreLoad = bVar;
    }

    public String getProfileId() {
        return this.profileId;
    }

    public com.startapp.android.publish.adsCommon.g.a getAnalyticsConfig() {
        return this.analytics;
    }

    public k getSensorsConfig() {
        return this.sensorsConfig;
    }

    public b getBluetoothConfig() {
        return this.btConfig;
    }

    public d getLocationConfig() {
        return this.location;
    }

    public boolean isWfScanEnabled() {
        return this.wfScanEnabled;
    }

    public int getNotVisibleBannerReloadInterval() {
        return this.notVisibleBannerReloadInterval;
    }

    public static e getInstance() {
        return instance;
    }

    public long getIABDisplayImpressionDelayInSeconds() {
        return this.IABDisplayImpressionDelayInSeconds;
    }

    public long getIABVideoImpressionDelayInSeconds() {
        return this.IABVideoImpressionDelayInSeconds;
    }

    public long getUserAgentDelayInSeconds() {
        return this.userAgentDelayInSeconds;
    }

    public boolean isUserAgentEnabled() {
        return this.userAgentEnabled;
    }

    public boolean isSupportIABViewability() {
        return this.SupportIABViewability;
    }

    public void applyAdPlatformProtocolToHosts() {
        this.adPlatformHostSecured = replaceAdProtocol(this.adPlatformHostSecured, DEFAULT_AD_PLATFORM_HOST);
        this.metaDataHostSecured = replaceAdProtocol(this.metaDataHostSecured, DEFAULT_METADATA_HOST);
        this.adPlatformBannerHostSecured = replaceAdProtocol(this.adPlatformBannerHostSecured, null);
        this.adPlatformSplashHostSecured = replaceAdProtocol(this.adPlatformSplashHostSecured, null);
        this.adPlatformReturnHostSecured = replaceAdProtocol(this.adPlatformReturnHostSecured, null);
        this.adPlatformOverlayHostSecured = replaceAdProtocol(this.adPlatformOverlayHostSecured, null);
        this.adPlatformNativeHostSecured = replaceAdProtocol(this.adPlatformNativeHostSecured, null);
    }

    public boolean canShowAd() {
        return !this.dns;
    }

    public int getStopAutoLoadAmount() {
        return this.stopAutoLoadAmount;
    }

    public int getStopAutoLoadPreCacheAmount() {
        return this.stopAutoLoadPreCacheAmount;
    }

    public boolean getTrueNetEnabled() {
        return this.trueNetEnabled;
    }

    public boolean getChromeCustomeTabsInternal() {
        return this.chromeCustomeTabsInternal;
    }

    public boolean getChromeCustomeTabsExternal() {
        return this.chromeCustomeTabsExternal;
    }

    public boolean getDisableSendAdvertisingId() {
        return this.disableSendAdvertisingId;
    }

    private String replaceAdProtocol(String str, String str2) {
        return str != null ? str.replace("%AdPlatformProtocol%", "1.5") : str2;
    }

    private void initTransientFields() {
        this.loading = false;
        this.ready = false;
        this.metaDataListeners = new ArrayList();
    }

    public static void preCacheResources(Context context, String str) {
        if (str != null && !str.equals("")) {
            if (!com.startapp.android.publish.adsCommon.a.a.a(context, "close_button", ".png") && !j.a()) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("close_button.png");
                new com.startapp.common.a(sb.toString(), new a(context, "close_button"), 0).a();
            }
            if (j.a(256)) {
                String[] strArr = AdsConstants.f;
                for (int i = 0; i < 6; i++) {
                    String str2 = strArr[i];
                    if (!com.startapp.android.publish.adsCommon.a.a.a(context, str2, ".png")) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append(str2);
                        sb2.append(".png");
                        new com.startapp.common.a(sb2.toString(), new a(context, str2), 0).a();
                    }
                }
            }
            if (j.a(64)) {
                String[] strArr2 = AdsConstants.g;
                for (int i2 = 0; i2 < 3; i2++) {
                    String str3 = strArr2[i2];
                    if (!com.startapp.android.publish.adsCommon.a.a.a(context, str3, ".png")) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(str);
                        sb3.append(str3);
                        sb3.append(".png");
                        new com.startapp.common.a(sb3.toString(), new a(context, str3), 0).a();
                    }
                }
                if (!com.startapp.android.publish.adsCommon.a.a.a(context, "logo", ".png")) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str);
                    sb4.append("logo.png");
                    new com.startapp.common.a(sb4.toString(), new a(context, "logo"), 0).a();
                }
            } else if (j.a(32)) {
                String[] strArr3 = AdsConstants.g;
                for (int i3 = 0; i3 < 3; i3++) {
                    String str4 = strArr3[i3];
                    if (!com.startapp.android.publish.adsCommon.a.a.a(context, str4, ".png")) {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append(str);
                        sb5.append(str4);
                        sb5.append(".png");
                        new com.startapp.common.a(sb5.toString(), new a(context, str4), 0).a();
                    }
                }
            }
        }
    }

    public boolean isOmsdkEnabled() {
        return this.omSdkEnabled;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        return this.sessionMaxBackgroundTime == eVar.sessionMaxBackgroundTime && this.simpleToken2 == eVar.simpleToken2 && this.alwaysSendToken == eVar.alwaysSendToken && this.isToken1Mandatory == eVar.isToken1Mandatory && this.compressionEnabled == eVar.compressionEnabled && this.btEnabled == eVar.btEnabled && this.periodicMetaDataEnabled == eVar.periodicMetaDataEnabled && this.periodicMetaDataIntervalInMinutes == eVar.periodicMetaDataIntervalInMinutes && this.periodicInfoEventEnabled == eVar.periodicInfoEventEnabled && this.periodicInfoEventOnRunTimeEnabled == eVar.periodicInfoEventOnRunTimeEnabled && this.periodicInfoEventIntervalInMinutes == eVar.periodicInfoEventIntervalInMinutes && this.inAppBrowser == eVar.inAppBrowser && this.SupportIABViewability == eVar.SupportIABViewability && this.IABDisplayImpressionDelayInSeconds == eVar.IABDisplayImpressionDelayInSeconds && this.IABVideoImpressionDelayInSeconds == eVar.IABVideoImpressionDelayInSeconds && this.userAgentDelayInSeconds == eVar.userAgentDelayInSeconds && this.userAgentEnabled == eVar.userAgentEnabled && this.wfScanEnabled == eVar.wfScanEnabled && this.dns == eVar.dns && this.stopAutoLoadAmount == eVar.stopAutoLoadAmount && this.stopAutoLoadPreCacheAmount == eVar.stopAutoLoadPreCacheAmount && this.trueNetEnabled == eVar.trueNetEnabled && this.webViewSecured == eVar.webViewSecured && this.omSdkEnabled == eVar.omSdkEnabled && this.chromeCustomeTabsInternal == eVar.chromeCustomeTabsInternal && this.chromeCustomeTabsExternal == eVar.chromeCustomeTabsExternal && this.disableSendAdvertisingId == eVar.disableSendAdvertisingId && this.notVisibleBannerReloadInterval == eVar.notVisibleBannerReloadInterval && j.b(this.SimpleToken, eVar.SimpleToken) && j.b(this.metaDataHostSecured, eVar.metaDataHostSecured) && j.b(this.adPlatformHostSecured, eVar.adPlatformHostSecured) && j.b(this.adPlatformBannerHostSecured, eVar.adPlatformBannerHostSecured) && j.b(this.adPlatformSplashHostSecured, eVar.adPlatformSplashHostSecured) && j.b(this.adPlatformReturnHostSecured, eVar.adPlatformReturnHostSecured) && j.b(this.adPlatformOverlayHostSecured, eVar.adPlatformOverlayHostSecured) && j.b(this.adPlatformNativeHostSecured, eVar.adPlatformNativeHostSecured) && j.b(this.profileId, eVar.profileId) && j.b(this.installersList, eVar.installersList) && j.b(this.preInstalledPackages, eVar.preInstalledPackages) && Arrays.equals(this.periodicEventIntMin, eVar.periodicEventIntMin) && j.b(this.sensorsConfig, eVar.sensorsConfig) && j.b(this.btConfig, eVar.btConfig) && j.b(this.assetsBaseUrlSecured, eVar.assetsBaseUrlSecured) && this.inAppBrowserPreLoad == eVar.inAppBrowserPreLoad && j.b(this.invalidForRetry, eVar.invalidForRetry) && j.b(this.invalidNetworkCodesInfoEvents, eVar.invalidNetworkCodesInfoEvents) && j.b(this.analytics, eVar.analytics) && j.b(this.location, eVar.location) && j.b(this.metadataUpdateVersion, eVar.metadataUpdateVersion);
    }

    public int hashCode() {
        return (j.a(this.SimpleToken, Integer.valueOf(this.notVisibleBannerReloadInterval), this.metaDataHostSecured, this.adPlatformHostSecured, this.adPlatformBannerHostSecured, this.adPlatformSplashHostSecured, this.adPlatformReturnHostSecured, this.adPlatformOverlayHostSecured, this.adPlatformNativeHostSecured, Integer.valueOf(this.sessionMaxBackgroundTime), this.profileId, this.installersList, this.preInstalledPackages, Boolean.valueOf(this.simpleToken2), Boolean.valueOf(this.alwaysSendToken), Boolean.valueOf(this.isToken1Mandatory), Boolean.valueOf(this.compressionEnabled), Boolean.valueOf(this.btEnabled), Boolean.valueOf(this.periodicMetaDataEnabled), Integer.valueOf(this.periodicMetaDataIntervalInMinutes), Boolean.valueOf(this.periodicInfoEventEnabled), Boolean.valueOf(this.periodicInfoEventOnRunTimeEnabled), Integer.valueOf(this.periodicInfoEventIntervalInMinutes), Boolean.valueOf(this.inAppBrowser), Boolean.valueOf(this.SupportIABViewability), Long.valueOf(this.IABDisplayImpressionDelayInSeconds), Long.valueOf(this.IABVideoImpressionDelayInSeconds), Long.valueOf(this.userAgentDelayInSeconds), Boolean.valueOf(this.userAgentEnabled), this.sensorsConfig, this.btConfig, this.assetsBaseUrlSecured, this.inAppBrowserPreLoad, this.invalidForRetry, this.invalidNetworkCodesInfoEvents, this.analytics, this.location, Boolean.valueOf(this.wfScanEnabled), this.metadataUpdateVersion, Boolean.valueOf(this.dns), Integer.valueOf(this.stopAutoLoadAmount), Integer.valueOf(this.stopAutoLoadPreCacheAmount), Boolean.valueOf(this.trueNetEnabled), Boolean.valueOf(this.webViewSecured), Boolean.valueOf(this.omSdkEnabled), Boolean.valueOf(this.chromeCustomeTabsInternal), Boolean.valueOf(this.chromeCustomeTabsExternal), Boolean.valueOf(this.disableSendAdvertisingId)) * 31) + Arrays.hashCode(this.periodicEventIntMin);
    }

    public static void init(Context context) {
        e eVar = (e) com.startapp.common.a.e.a(context, "StartappMetadata");
        e eVar2 = new e();
        if (eVar != null) {
            boolean a2 = j.a(eVar, eVar2);
            if (!eVar.isMetaDataVersionChanged() && a2) {
                f.a(context, d.METADATA_NULL, "MetaData", "", "");
            }
            eVar.initTransientFields();
            instance = eVar;
        } else {
            instance = eVar2;
        }
        getInstance().applyAdPlatformProtocolToHosts();
    }
}
