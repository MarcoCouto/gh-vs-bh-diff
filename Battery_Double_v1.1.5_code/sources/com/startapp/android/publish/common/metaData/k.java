package com.startapp.android.publish.common.metaData;

import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.common.c.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class k implements Serializable {
    private static final long serialVersionUID = 1;
    @e(a = true)
    private a ambientTemperatureSensor = new a(14);
    private boolean enabled = false;
    @e(a = true)
    private a gravitySensor = new a(9);
    @e(a = true)
    private a gyroscopeUncalibratedSensor = new a(18);
    @e(a = true)
    private a lightSensor = new a(3);
    @e(a = true)
    private a linearAccelerationSensor = new a(9);
    @e(a = true)
    private a magneticFieldSensor = new a(3);
    @e(a = true)
    private a pressureSensor = new a(9);
    @e(a = true)
    private a relativeHumiditySensor = new a(14);
    @e(a = true)
    private a rotationVectorSensor = new a(9);
    private int timeoutInSec = 10;

    public final int a() {
        return this.timeoutInSec;
    }

    public final boolean b() {
        return this.enabled;
    }

    public final a c() {
        return this.ambientTemperatureSensor;
    }

    public final a d() {
        return this.gravitySensor;
    }

    public final a e() {
        return this.lightSensor;
    }

    public final a f() {
        return this.linearAccelerationSensor;
    }

    public final a g() {
        return this.magneticFieldSensor;
    }

    public final a h() {
        return this.pressureSensor;
    }

    public final a i() {
        return this.relativeHumiditySensor;
    }

    public final a j() {
        return this.rotationVectorSensor;
    }

    public final a k() {
        return this.gyroscopeUncalibratedSensor;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        k kVar = (k) obj;
        return this.timeoutInSec == kVar.timeoutInSec && this.enabled == kVar.enabled && j.b(this.ambientTemperatureSensor, kVar.ambientTemperatureSensor) && j.b(this.gravitySensor, kVar.gravitySensor) && j.b(this.lightSensor, kVar.lightSensor) && j.b(this.linearAccelerationSensor, kVar.linearAccelerationSensor) && j.b(this.magneticFieldSensor, kVar.magneticFieldSensor) && j.b(this.pressureSensor, kVar.pressureSensor) && j.b(this.relativeHumiditySensor, kVar.relativeHumiditySensor) && j.b(this.rotationVectorSensor, kVar.rotationVectorSensor) && j.b(this.gyroscopeUncalibratedSensor, kVar.gyroscopeUncalibratedSensor);
    }

    public final int hashCode() {
        return j.a(Integer.valueOf(this.timeoutInSec), Boolean.valueOf(this.enabled), this.ambientTemperatureSensor, this.gravitySensor, this.lightSensor, this.linearAccelerationSensor, this.magneticFieldSensor, this.pressureSensor, this.relativeHumiditySensor, this.rotationVectorSensor, this.gyroscopeUncalibratedSensor);
    }
}
