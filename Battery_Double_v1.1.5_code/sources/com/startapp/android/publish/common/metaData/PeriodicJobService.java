package com.startapp.android.publish.common.metaData;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import com.startapp.common.b.a;
import com.startapp.common.b.a.b;
import com.startapp.common.b.a.b.C0095b;

@TargetApi(21)
/* compiled from: StartAppSDK */
public class PeriodicJobService extends JobService {

    /* renamed from: a reason: collision with root package name */
    private static final String f4142a = "PeriodicJobService";

    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }

    public boolean onStartJob(final JobParameters jobParameters) {
        a.a((Context) this);
        boolean a2 = a.a(jobParameters, (C0095b) new C0095b() {
            public final void a(b.a aVar) {
                PeriodicJobService.this.jobFinished(jobParameters, false);
            }
        });
        "onStartJob: RunnerManager.runJob".concat(String.valueOf(a2));
        a.b();
        return a2;
    }
}
