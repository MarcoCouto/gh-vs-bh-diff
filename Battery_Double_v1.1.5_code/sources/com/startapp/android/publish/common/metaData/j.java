package com.startapp.android.publish.common.metaData;

import android.content.Context;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.AdsConstants.ServiceApiType;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.adsCommon.l;
import com.startapp.android.publish.common.metaData.g.a;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.common.b.a.b;
import com.startapp.common.b.a.b.C0095b;
import com.startapp.common.c;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class j implements b {
    public final void a(Context context, int i, Map<String, String> map, C0095b bVar) {
        try {
            c.b(context);
            e.init(context);
            if (e.getInstance().isPeriodicMetaDataEnabled()) {
                final AdPreferences adPreferences = new AdPreferences(k.a(context, "shared_prefs_devId", (String) null), k.a(context, "shared_prefs_appId", ""));
                final Context context2 = context;
                final C0095b bVar2 = bVar;
                AnonymousClass1 r0 = new c(context, adPreferences, a.PERIODIC) {
                    private e b = null;

                    /* access modifiers changed from: protected */
                    public final Boolean c() {
                        try {
                            l.b(context2);
                            g gVar = new g(context2, a.PERIODIC);
                            gVar.fillApplicationDetails(context2, adPreferences, false);
                            gVar.fillLocationDetails(adPreferences, context2);
                            this.b = (e) com.startapp.android.publish.adsCommon.a.j.a(com.startapp.android.publish.adsCommon.l.a.a(context2, AdsConstants.a(ServiceApiType.METADATA), gVar).a(), e.class);
                            return Boolean.TRUE;
                        } catch (Exception unused) {
                            return Boolean.FALSE;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public final void a(Boolean bool) {
                        try {
                            if (!(!bool.booleanValue() || this.b == null || context2 == null)) {
                                e.update(context2, this.b, this.f4144a);
                            }
                            com.startapp.android.publish.adsCommon.a.b.c(context2);
                            if (bVar2 != null) {
                                bVar2.a(b.a.SUCCESS);
                            }
                        } catch (Exception e2) {
                            f.a(context2, d.EXCEPTION, "PeriodicMetaData.onPostExecute", e2.getMessage(), "");
                        }
                    }
                };
                r0.a();
            }
        } catch (Exception e) {
            f.a(context, d.EXCEPTION, "PeriodicMetaData.execute", e.getMessage(), "");
        }
    }
}
