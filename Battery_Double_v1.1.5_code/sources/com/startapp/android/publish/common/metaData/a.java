package com.startapp.android.publish.common.metaData;

import com.startapp.android.publish.adsCommon.a.j;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class a implements Serializable {
    private static final long serialVersionUID = 1;
    private int delay = 3;
    private boolean enabled = true;
    private int minApiLevel = 18;

    public a() {
    }

    public a(int i) {
        this.minApiLevel = i;
    }

    public final int a() {
        return this.delay;
    }

    public final int b() {
        return this.minApiLevel;
    }

    public final boolean c() {
        return this.enabled;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return this.delay == aVar.delay && this.minApiLevel == aVar.minApiLevel && this.enabled == aVar.enabled;
    }

    public final int hashCode() {
        return j.a(Integer.valueOf(this.delay), Integer.valueOf(this.minApiLevel), Boolean.valueOf(this.enabled));
    }
}
