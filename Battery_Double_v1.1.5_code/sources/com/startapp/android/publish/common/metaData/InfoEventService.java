package com.startapp.android.publish.common.metaData;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.startapp.common.b.a;
import com.startapp.common.b.a.b;
import com.startapp.common.b.a.b.C0095b;

/* compiled from: StartAppSDK */
public class InfoEventService extends Service {

    /* renamed from: a reason: collision with root package name */
    private static final String f4140a = "InfoEventService";

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        a.a((Context) this);
        "onHandleIntent: RunnerManager.runJob".concat(String.valueOf(a.a(intent, (C0095b) new C0095b() {
            public final void a(b.a aVar) {
                InfoEventService.this.stopSelf();
            }
        })));
        a.b();
        return super.onStartCommand(intent, i, i2);
    }
}
