package com.startapp.android.publish.common.metaData;

import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.common.c.e;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class h implements Serializable {
    public static final int DEFAULT_ITEM_BOTTOM = -8750199;
    public static final Integer DEFAULT_ITEM_DESC_TEXT_COLOR = Integer.valueOf(-1);
    public static final Set<String> DEFAULT_ITEM_DESC_TEXT_DECORATION = new HashSet();
    public static final Integer DEFAULT_ITEM_DESC_TEXT_SIZE = Integer.valueOf(14);
    public static final Integer DEFAULT_ITEM_TITLE_TEXT_COLOR = Integer.valueOf(-1);
    public static final Set<String> DEFAULT_ITEM_TITLE_TEXT_DECORATION = new HashSet(Arrays.asList(new String[]{"BOLD"}));
    public static final Integer DEFAULT_ITEM_TITLE_TEXT_SIZE = Integer.valueOf(18);
    public static final int DEFAULT_ITEM_TOP = -14014151;
    private static final long serialVersionUID = 1;
    private Integer itemDescriptionTextColor = DEFAULT_ITEM_DESC_TEXT_COLOR;
    @e(b = HashSet.class)
    private Set<String> itemDescriptionTextDecoration = DEFAULT_ITEM_DESC_TEXT_DECORATION;
    private Integer itemDescriptionTextSize = DEFAULT_ITEM_DESC_TEXT_SIZE;
    private Integer itemGradientBottom = Integer.valueOf(DEFAULT_ITEM_BOTTOM);
    private Integer itemGradientTop = Integer.valueOf(DEFAULT_ITEM_TOP);
    private Integer itemTitleTextColor = DEFAULT_ITEM_TITLE_TEXT_COLOR;
    @e(b = HashSet.class)
    private Set<String> itemTitleTextDecoration = DEFAULT_ITEM_TITLE_TEXT_DECORATION;
    private Integer itemTitleTextSize = DEFAULT_ITEM_TITLE_TEXT_SIZE;
    private String name = "";

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final Integer getItemGradientTop() {
        return this.itemGradientTop;
    }

    public final Integer getItemGradientBottom() {
        return this.itemGradientBottom;
    }

    public final Integer getItemTitleTextSize() {
        return this.itemTitleTextSize;
    }

    public final Integer getItemTitleTextColor() {
        return this.itemTitleTextColor;
    }

    public final Set<String> getItemTitleTextDecoration() {
        return this.itemTitleTextDecoration;
    }

    public final Integer getItemDescriptionTextSize() {
        return this.itemDescriptionTextSize;
    }

    public final Integer getItemDescriptionTextColor() {
        return this.itemDescriptionTextColor;
    }

    public final Set<String> getItemDescriptionTextDecoration() {
        return this.itemDescriptionTextDecoration;
    }

    public final void setItemGradientTop(Integer num) {
        this.itemGradientTop = num;
    }

    public final void setItemGradientBottom(Integer num) {
        this.itemGradientBottom = num;
    }

    public final void setItemTitleTextSize(Integer num) {
        this.itemTitleTextSize = num;
    }

    public final void setItemTitleTextColor(Integer num) {
        this.itemTitleTextColor = num;
    }

    public final void setItemTitleTextDecoration(Set<String> set) {
        this.itemTitleTextDecoration = set;
    }

    public final void setItemDescriptionTextSize(Integer num) {
        this.itemDescriptionTextSize = num;
    }

    public final void setItemDescriptionTextColor(Integer num) {
        this.itemDescriptionTextColor = num;
    }

    public final void setItemDescriptionTextDecoration(Set<String> set) {
        this.itemDescriptionTextDecoration = set;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        h hVar = (h) obj;
        return j.b(this.name, hVar.name) && j.b(this.itemGradientTop, hVar.itemGradientTop) && j.b(this.itemGradientBottom, hVar.itemGradientBottom) && j.b(this.itemTitleTextSize, hVar.itemTitleTextSize) && j.b(this.itemTitleTextColor, hVar.itemTitleTextColor) && j.b(this.itemTitleTextDecoration, hVar.itemTitleTextDecoration) && j.b(this.itemDescriptionTextSize, hVar.itemDescriptionTextSize) && j.b(this.itemDescriptionTextColor, hVar.itemDescriptionTextColor) && j.b(this.itemDescriptionTextDecoration, hVar.itemDescriptionTextDecoration);
    }

    public final int hashCode() {
        return j.a(this.name, this.itemGradientTop, this.itemGradientBottom, this.itemTitleTextSize, this.itemTitleTextColor, this.itemTitleTextDecoration, this.itemDescriptionTextSize, this.itemDescriptionTextColor, this.itemDescriptionTextDecoration);
    }
}
