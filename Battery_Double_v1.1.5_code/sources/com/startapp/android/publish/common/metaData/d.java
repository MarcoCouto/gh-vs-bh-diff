package com.startapp.android.publish.common.metaData;

import android.support.annotation.VisibleForTesting;
import com.startapp.android.publish.adsCommon.a.j;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class d implements Serializable {
    private static final boolean DEFAULT_CO_ENABLED = false;
    private static final boolean DEFAULT_FI_ENABLED = false;
    private static final long serialVersionUID = 1;
    private boolean coEnabled = false;
    private boolean fiEnabled = false;

    public final boolean isFiEnabled() {
        return this.fiEnabled;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final void setFiEnabled(boolean z) {
        this.fiEnabled = z;
    }

    public final boolean isCoEnabled() {
        return this.coEnabled;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        d dVar = (d) obj;
        return this.fiEnabled == dVar.fiEnabled && this.coEnabled == dVar.coEnabled;
    }

    public final int hashCode() {
        return j.a(Boolean.valueOf(this.fiEnabled), Boolean.valueOf(this.coEnabled));
    }
}
