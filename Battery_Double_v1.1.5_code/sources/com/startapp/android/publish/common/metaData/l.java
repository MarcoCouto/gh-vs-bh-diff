package com.startapp.android.publish.common.metaData;

import android.content.Context;
import com.startapp.android.publish.adsCommon.k;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class l implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean enabled = false;

    public static void a(Context context, boolean z) {
        k.b(context, "userDisabledSimpleToken", Boolean.valueOf(!z));
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.enabled == ((l) obj).enabled;
    }

    public final int hashCode() {
        return Boolean.valueOf(this.enabled).hashCode();
    }

    public final boolean a(Context context) {
        return !k.a(context, "userDisabledSimpleToken", Boolean.FALSE).booleanValue() && this.enabled;
    }
}
