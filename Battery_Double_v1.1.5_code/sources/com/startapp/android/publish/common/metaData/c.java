package com.startapp.android.publish.common.metaData;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.android.publish.ads.splash.f;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.AdsConstants.ServiceApiType;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.b;
import com.startapp.android.publish.adsCommon.g.e;
import com.startapp.android.publish.cache.d;
import com.startapp.android.publish.common.metaData.g.a;
import com.startapp.android.publish.common.model.AdPreferences;
import java.net.UnknownHostException;

/* compiled from: StartAppSDK */
public class c {

    /* renamed from: a reason: collision with root package name */
    protected boolean f4144a = false;
    private final Context b;
    private final AdPreferences c;
    private a d;
    private e e = null;
    private com.startapp.android.publish.ads.banner.c f = null;
    private f g = null;
    private d h = null;
    private com.startapp.android.publish.adsCommon.adinformation.a i = null;
    private b j = null;
    private boolean k = false;

    public c(Context context, AdPreferences adPreferences, a aVar) {
        this.b = context;
        this.c = adPreferences;
        this.d = aVar;
    }

    public final void a() {
        com.startapp.common.f.a(com.startapp.common.f.a.HIGH, (Runnable) new Runnable() {
            public final void run() {
                final Boolean c = c.this.c();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        c.this.a(c);
                    }
                });
            }
        });
    }

    public final void b() {
        this.k = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:20|21|(4:27|(12:29|30|31|(1:33)|38|(3:42|43|(1:45))|50|(4:52|53|54|(1:56))|61|(3:63|64|(1:66))|71|(3:73|74|(1:76)))|81|82)|83|84) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:83:0x017c */
    public Boolean c() {
        g gVar = new g(this.b, this.d);
        try {
            gVar.fillApplicationDetails(this.b, this.c, false);
            gVar.fillLocationDetails(this.c, this.b);
            String a2 = com.startapp.android.publish.adsCommon.l.a.a(this.b, AdsConstants.a(ServiceApiType.METADATA), gVar).a();
            this.e = (e) j.a(a2, e.class);
            if (!j.a()) {
                this.j = (b) j.a(a2, b.class);
                if (j.a(16) || j.a(32)) {
                    this.f = (com.startapp.android.publish.ads.banner.c) j.a(a2, com.startapp.android.publish.ads.banner.c.class);
                }
                if (j.a(8)) {
                    this.g = (f) j.a(a2, f.class);
                }
                if (j.a(512)) {
                    this.h = (d) j.a(a2, d.class);
                }
                if (j.e()) {
                    this.i = (com.startapp.android.publish.adsCommon.adinformation.a) j.a(a2, com.startapp.android.publish.adsCommon.adinformation.a.class);
                }
            }
            synchronized (e.getLock()) {
                if (!(this.k || this.e == null || this.b == null)) {
                    if (!j.a()) {
                        try {
                            if (!j.b(b.a(), this.j)) {
                                this.f4144a = true;
                                b.a(this.b, this.j);
                            }
                        } catch (Exception e2) {
                            a("GetMetaDataAsyncTask-adscommon update fail", e2.getMessage());
                        }
                        if (j.a(16) || j.a(32)) {
                            try {
                                if (!j.b(com.startapp.android.publish.ads.banner.c.a(), this.f)) {
                                    this.f4144a = true;
                                    com.startapp.android.publish.ads.banner.c.a(this.b, this.f);
                                }
                            } catch (Exception e3) {
                                a("GetMetaDataAsyncTask-banner update fail", e3.getMessage());
                            }
                        }
                        if (j.a(8)) {
                            this.g.a().setDefaults(this.b);
                            try {
                                if (!j.b(f.b(), this.g)) {
                                    this.f4144a = true;
                                    f.a(this.b, this.g);
                                }
                            } catch (Exception e4) {
                                a("GetMetaDataAsyncTask-splash update fail", e4.getMessage());
                            }
                        }
                        if (j.a(512)) {
                            try {
                                if (!j.b(d.a(), this.h)) {
                                    this.f4144a = true;
                                    d.a(this.b, this.h);
                                }
                            } catch (Exception e5) {
                                a("GetMetaDataAsyncTask-cache update fail", e5.getMessage());
                            }
                        }
                        if (j.e()) {
                            try {
                                if (!j.b(com.startapp.android.publish.adsCommon.adinformation.a.b(), this.i)) {
                                    this.f4144a = true;
                                    com.startapp.android.publish.adsCommon.adinformation.a.a(this.b, this.i);
                                }
                            } catch (Exception e6) {
                                a("GetMetaDataAsyncTask-adInfo update fail", e6.getMessage());
                            }
                        }
                    }
                    e.preCacheResources(this.b, this.e.getAssetsBaseUrl());
                }
            }
            return Boolean.TRUE;
        } catch (Exception e7) {
            if (!(e7 instanceof UnknownHostException) || !e7.getMessage().contains("init.startappservice.com")) {
                com.startapp.android.publish.adsCommon.g.f.a(this.b, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "GetMetaDataAsync.doInBackground - MetaData request failed", e7.getMessage(), "");
            }
            return Boolean.FALSE;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        synchronized (e.getLock()) {
            if (!this.k) {
                if (!bool.booleanValue() || this.e == null || this.b == null) {
                    e.failedLoading();
                } else {
                    try {
                        e.update(this.b, this.e, this.f4144a);
                    } catch (Exception e2) {
                        a("GetMetaDataAsyncTask.onPostExecute-metadata update fail", e2.getMessage());
                    }
                }
            }
        }
    }

    private void a(String str, String str2) {
        com.startapp.android.publish.adsCommon.g.f.a(this.b, new e(com.startapp.android.publish.adsCommon.g.d.EXCEPTION, str, str2), "");
    }
}
