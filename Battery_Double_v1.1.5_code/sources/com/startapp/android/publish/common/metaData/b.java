package com.startapp.android.publish.common.metaData;

import com.startapp.android.publish.adsCommon.a.j;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class b implements Serializable {
    private static final long serialVersionUID = 1;
    private int discoveryIntervalInMinutes = 1440;
    private boolean enabled = false;
    private int timeoutInSec = 20;

    public final int a() {
        return this.timeoutInSec;
    }

    public final boolean b() {
        return this.enabled;
    }

    public final int c() {
        return this.discoveryIntervalInMinutes;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        return this.timeoutInSec == bVar.timeoutInSec && this.enabled == bVar.enabled && this.discoveryIntervalInMinutes == bVar.discoveryIntervalInMinutes;
    }

    public final int hashCode() {
        return j.a(Integer.valueOf(this.timeoutInSec), Boolean.valueOf(this.enabled), Integer.valueOf(this.discoveryIntervalInMinutes));
    }
}
