package com.startapp.android.publish.common.metaData;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Pair;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.a.f;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.d;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.adsCommon.l;
import com.startapp.android.publish.adsCommon.m;
import com.startapp.common.a.c;
import io.fabric.sdk.android.services.common.CommonUtils;

/* compiled from: StartAppSDK */
public final class g extends d {

    /* renamed from: a reason: collision with root package name */
    private int f4150a;
    private int b;
    private boolean c;
    private float d;
    private a e;
    private String f = e.getInstance().getProfileId();
    private String g;
    private Integer h;
    private Pair<String, String> i;
    private long j;

    /* compiled from: StartAppSDK */
    public enum a {
        LAUNCH(1),
        APP_IDLE(2),
        IN_APP_PURCHASE(3),
        CUSTOM(4),
        PERIODIC(5),
        PAS(6);
        
        private int index;

        private a(int i) {
            this.index = i;
        }
    }

    public g(Context context, a aVar) {
        this.f4150a = k.a(context, "totalSessions", Integer.valueOf(0)).intValue();
        this.b = (int) ((System.currentTimeMillis() - k.a(context, "firstSessionTime", Long.valueOf(System.currentTimeMillis())).longValue()) / 86400000);
        this.d = k.a(context, "inAppPurchaseAmount", Float.valueOf(0.0f)).floatValue();
        this.c = k.a(context, "payingUser", Boolean.FALSE).booleanValue();
        this.e = aVar;
        SharedPreferences a2 = k.a(context);
        boolean d2 = m.a().d();
        new j();
        String string = a2.getString("shared_prefs_app_apk_hash", null);
        if (TextUtils.isEmpty(string) || d2) {
            string = j.a(CommonUtils.SHA256_INSTANCE, context);
            a2.edit().putString("shared_prefs_app_apk_hash", string).commit();
        }
        this.g = string;
        int e2 = c.e(context);
        if (e2 > 0) {
            this.h = Integer.valueOf(e2);
        }
        this.i = l.c();
        this.j = l.a();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("MetaDataRequest [totalSessions=");
        sb.append(this.f4150a);
        sb.append(", daysSinceFirstSession=");
        sb.append(this.b);
        sb.append(", payingUser=");
        sb.append(this.c);
        sb.append(", paidAmount=");
        sb.append(this.d);
        sb.append(", reason=");
        sb.append(this.e);
        sb.append(", profileId=");
        sb.append(this.f);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public final f getNameValueMap() {
        f nameValueMap = super.getNameValueMap();
        if (nameValueMap == null) {
            nameValueMap = new com.startapp.android.publish.adsCommon.a.d();
        }
        nameValueMap.a(com.startapp.common.a.a.a(), com.startapp.common.a.a.d(), true);
        nameValueMap.a("totalSessions", Integer.valueOf(this.f4150a), true);
        nameValueMap.a("daysSinceFirstSession", Integer.valueOf(this.b), true);
        nameValueMap.a("payingUser", Boolean.valueOf(this.c), true);
        nameValueMap.a("profileId", this.f, false);
        nameValueMap.a("paidAmount", Float.valueOf(this.d), true);
        nameValueMap.a(IronSourceConstants.EVENTS_ERROR_REASON, this.e, true);
        if (this.g != null) {
            nameValueMap.a("apkHash", this.g, false);
        }
        nameValueMap.a("ian", this.h, false);
        nameValueMap.a((String) this.i.first, this.i.second, false);
        if (this.j > 0 && this.j < Long.MAX_VALUE) {
            nameValueMap.a("firstInstalledAppTS", Long.valueOf(this.j), false);
        }
        return nameValueMap;
    }
}
