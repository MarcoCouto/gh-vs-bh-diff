package com.startapp.android.publish.common.metaData;

import android.content.Context;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.common.b.a.b;
import com.startapp.common.b.a.b.C0095b;
import com.startapp.common.b.a.b.a;
import com.startapp.common.c;
import com.startapp.common.d;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class i implements b {
    public final void a(final Context context, int i, Map<String, String> map, final C0095b bVar) {
        try {
            c.b(context);
            e.init(context);
            e.getInstance().setReady(true);
            if (e.getInstance().isPeriodicInfoEventEnabled()) {
                new com.startapp.android.publish.adsCommon.g.c(context, true, new d() {
                    public final void a(Object obj) {
                        if (bVar != null) {
                            com.startapp.android.publish.adsCommon.a.b.d(context);
                            bVar.a(a.SUCCESS);
                        }
                    }
                }).a();
                return;
            }
            com.startapp.android.publish.adsCommon.a.b.d(context);
            bVar.a(a.SUCCESS);
        } catch (Exception e) {
            f.a(context, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "PeriodicInfoEvent.execute", e.getMessage(), "");
        }
    }
}
