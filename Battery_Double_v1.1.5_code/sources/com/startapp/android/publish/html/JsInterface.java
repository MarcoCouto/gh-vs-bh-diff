package com.startapp.android.publish.html;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.adsCommon.e.b;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import java.util.Iterator;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public class JsInterface {
    private Runnable clickCallback;
    private Runnable closeCallback;
    private Runnable enableScrollCallback;
    protected boolean inAppBrowserEnabled;
    protected Context mContext;
    private b params;
    private boolean processed;

    public JsInterface(Context context, Runnable runnable, b bVar, boolean z) {
        this(context, runnable, bVar);
        this.inAppBrowserEnabled = z;
    }

    public JsInterface(Context context, Runnable runnable, b bVar) {
        this.processed = false;
        this.inAppBrowserEnabled = true;
        this.closeCallback = null;
        this.clickCallback = null;
        this.enableScrollCallback = null;
        this.closeCallback = runnable;
        this.mContext = context;
        this.params = bVar;
    }

    public JsInterface(Context context, Runnable runnable, Runnable runnable2, Runnable runnable3, b bVar, boolean z) {
        this(context, runnable, bVar, z);
        this.clickCallback = runnable2;
        this.enableScrollCallback = runnable3;
    }

    public JsInterface(Context context, Runnable runnable, Runnable runnable2, b bVar) {
        this(context, runnable, bVar);
        this.clickCallback = runnable2;
    }

    @JavascriptInterface
    public void closeAd() {
        if (!this.processed) {
            this.processed = true;
            this.closeCallback.run();
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @JavascriptInterface
    public void openApp(String str, String str2, String str3) {
        if (str != null && !TextUtils.isEmpty(str)) {
            c.b(this.mContext, str, this.params);
        }
        Intent launchIntentForPackage = this.mContext.getPackageManager().getLaunchIntentForPackage(str2);
        if (str3 != null) {
            JSONObject jSONObject = new JSONObject(str3);
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String valueOf = String.valueOf(keys.next());
                launchIntentForPackage.putExtra(valueOf, String.valueOf(jSONObject.get(valueOf)));
            }
        }
        try {
            this.mContext.startActivity(launchIntentForPackage);
        } catch (Exception e) {
            f.a(this.mContext, d.EXCEPTION, "JsInterface.openApp - Couldn't start activity", e.getMessage(), c.a(str, (String) null));
            StringBuilder sb = new StringBuilder("Cannot find activity to handle url: [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
        }
        if (this.clickCallback == null) {
            this.clickCallback.run();
        }
    }

    @JavascriptInterface
    public void externalLinks(String str) {
        if (!this.inAppBrowserEnabled || !j.a(256)) {
            c.c(this.mContext, str);
        } else {
            c.b(this.mContext, str, (String) null);
        }
    }

    @JavascriptInterface
    public void enableScroll(String str) {
        if (this.enableScrollCallback != null) {
            this.enableScrollCallback.run();
        }
    }
}
