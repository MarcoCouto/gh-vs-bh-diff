package com.startapp.android.publish.html;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.b.a.a.a.b;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.AdsConstants.AdApiType;
import com.startapp.android.publish.adsCommon.HtmlAd;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.e;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.l;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.common.model.GetAdRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public abstract class a extends e {
    protected Set<String> g = new HashSet();
    protected GetAdRequest h;
    private Set<String> i = new HashSet();
    private List<com.startapp.android.publish.adsCommon.c.a> j = new ArrayList();
    private int k = 0;
    private boolean l;

    /* access modifiers changed from: protected */
    public boolean a(GetAdRequest getAdRequest) {
        return getAdRequest != null;
    }

    public a(Context context, Ad ad, AdPreferences adPreferences, AdEventListener adEventListener, Placement placement, boolean z) {
        super(context, ad, adPreferences, adEventListener, placement);
        this.l = z;
    }

    /* access modifiers changed from: protected */
    public final Object e() {
        this.h = a();
        if (!a(this.h)) {
            return null;
        }
        if (this.i.size() == 0) {
            this.i.add(this.f4063a.getPackageName());
        }
        this.h.setPackageExclude(this.i);
        this.h.setCampaignExclude(this.g);
        if (this.k > 0) {
            this.h.setEngInclude(false);
            if (com.startapp.android.publish.common.metaData.e.getInstance().getSimpleTokenConfig().a(this.f4063a)) {
                l.b(this.f4063a);
            }
        }
        try {
            return com.startapp.android.publish.adsCommon.l.a.a(this.f4063a, AdsConstants.a(AdApiType.HTML, f()), this.h);
        } catch (com.startapp.common.e e) {
            if (!com.startapp.android.publish.common.metaData.e.getInstance().getInvalidNetworkCodesInfoEvents().contains(Integer.valueOf(e.a()))) {
                f.a(this.f4063a, d.EXCEPTION, "BaseHtmlService.sendCommand - network failure", e.getMessage(), "");
            }
            this.f = e.getMessage();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(Object obj) {
        try {
            this.j = new ArrayList();
            String a2 = ((com.startapp.common.a.g.a) obj).a();
            if (TextUtils.isEmpty(a2)) {
                if (this.f == null) {
                    if (this.h == null || !this.h.isAdTypeVideo()) {
                        this.f = "Empty Ad";
                    } else {
                        this.f = "Video isn't available";
                    }
                }
                return false;
            }
            List a3 = b.a(a2, this.k);
            if (!(com.startapp.android.publish.adsCommon.b.a().E() ? b.a(this.f4063a, a3, this.k, this.i, this.j).booleanValue() : false)) {
                ((HtmlAd) this.b).setApps(a3);
                ((HtmlAd) this.b).setHtml(a2);
                return true;
            }
            this.k++;
            new com.startapp.android.publish.adsCommon.c.b(this.f4063a, this.j).a();
            return d().booleanValue();
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        super.a(bool);
        StringBuilder sb = new StringBuilder("Html onPostExecute, result=[");
        sb.append(bool);
        sb.append(RequestParameters.RIGHT_BRACKETS);
    }

    /* access modifiers changed from: protected */
    public void b(Boolean bool) {
        super.b(bool);
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        Intent intent = new Intent("com.startapp.android.OnReceiveResponseBroadcastListener");
        intent.putExtra("adHashcode", this.b.hashCode());
        intent.putExtra("adResult", z);
        com.startapp.common.b.a(this.f4063a).a(intent);
        if (!z || this.b == null) {
            StringBuilder sb = new StringBuilder("Html onPostExecute failed error=[");
            sb.append(this.f);
            sb.append(RequestParameters.RIGHT_BRACKETS);
        } else if (this.l) {
            j.a(this.f4063a, ((HtmlAd) this.b).getHtml(), (com.startapp.android.publish.adsCommon.a.j.a) new com.startapp.android.publish.adsCommon.a.j.a() {
                public final void a() {
                    a.this.d.onReceiveAd(a.this.b);
                }

                public final void a(String str) {
                    a.this.b.setErrorMessage(str);
                    a.this.d.onFailedToReceiveAd(a.this.b);
                }
            });
        } else if (z) {
            this.d.onReceiveAd(this.b);
        } else {
            this.d.onFailedToReceiveAd(this.b);
        }
    }
}
