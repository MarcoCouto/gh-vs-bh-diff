package com.startapp.android.publish.b;

import com.startapp.common.c.e;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a implements Serializable {
    private static final long serialVersionUID = 1;
    @e(b = c.class, f = "adVerifications")
    private c[] adVerification;

    public a() {
    }

    public a(c[] cVarArr) {
        this.adVerification = cVarArr;
    }

    public final List<c> getAdVerification() {
        if (this.adVerification == null) {
            return null;
        }
        return Arrays.asList(this.adVerification);
    }
}
