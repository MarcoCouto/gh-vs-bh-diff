package com.startapp.android.publish.b;

import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class c implements Serializable {
    private static final long serialVersionUID = 1;
    private String javascriptResourceUrl;
    private String vendorKey;
    private String verificationParameters;

    public c() {
    }

    public c(String str, String str2, String str3) {
        this.vendorKey = str;
        this.javascriptResourceUrl = str2;
        this.verificationParameters = str3;
    }

    public final String getVendorKey() {
        return this.vendorKey;
    }

    public final String getVerificationScriptUrl() {
        return this.javascriptResourceUrl;
    }

    public final String getVerificationParameters() {
        return this.verificationParameters;
    }
}
