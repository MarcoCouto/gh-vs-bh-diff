package com.startapp.android.publish.ads.nativead;

import android.content.Context;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.h;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class b extends h {
    private static final long serialVersionUID = 1;
    private NativeAdPreferences config;

    public b(Context context, NativeAdPreferences nativeAdPreferences) {
        super(context, Placement.INAPP_NATIVE);
        this.config = nativeAdPreferences;
    }

    /* access modifiers changed from: protected */
    public final void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
        a aVar = new a(this.context, this, adPreferences, adEventListener, this.config);
        aVar.c();
    }
}
