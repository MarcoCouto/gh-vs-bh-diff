package com.startapp.android.publish.ads.nativead;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.startapp.android.publish.ads.nativead.StartAppNativeAd.CampaignAction;
import java.util.List;

/* compiled from: StartAppSDK */
public interface NativeAdInterface {
    CampaignAction getCampaignAction();

    String getCategory();

    String getDescription();

    Bitmap getImageBitmap();

    String getImageUrl();

    String getInstalls();

    String getPackacgeName();

    float getRating();

    Bitmap getSecondaryImageBitmap();

    String getSecondaryImageUrl();

    String getTitle();

    boolean isApp();

    boolean isBelowMinCPM();

    void registerViewForInteraction(@NonNull View view);

    void registerViewForInteraction(@NonNull View view, @Nullable List<View> list);

    void registerViewForInteraction(@NonNull View view, @Nullable List<View> list, @Nullable NativeAdDisplayListener nativeAdDisplayListener);

    void unregisterView();
}
