package com.startapp.android.publish.ads.nativead;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnClickListener;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.banner.c;
import com.startapp.android.publish.ads.nativead.StartAppNativeAd.CampaignAction;
import com.startapp.android.publish.adsCommon.i;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdDetails;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.a.C0093a;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class NativeAdDetails implements NativeAdInterface {

    /* renamed from: a reason: collision with root package name */
    private AdDetails f3914a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public Bitmap c;
    /* access modifiers changed from: private */
    public Bitmap d;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public a g;
    private String h;
    private com.startapp.android.publish.adsCommon.m.b i;
    private WeakReference<View> j = new WeakReference<>(null);
    /* access modifiers changed from: private */
    public OnAttachStateChangeListener k;
    /* access modifiers changed from: private */
    public c l;

    /* compiled from: StartAppSDK */
    public interface a {
        void onNativeAdDetailsLoaded(int i);
    }

    /* compiled from: StartAppSDK */
    class b implements OnClickListener {
        private b() {
        }

        /* synthetic */ b(NativeAdDetails nativeAdDetails, byte b) {
            this();
        }

        public final void onClick(View view) {
            NativeAdDetails.a(NativeAdDetails.this, view);
        }
    }

    public NativeAdDetails(AdDetails adDetails, NativeAdPreferences nativeAdPreferences, int i2, a aVar) {
        StringBuilder sb = new StringBuilder("Initializiang SingleAd [");
        sb.append(i2);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        this.f3914a = adDetails;
        this.b = i2;
        this.g = aVar;
        if (nativeAdPreferences.isAutoBitmapDownload()) {
            new com.startapp.common.a(getImageUrl(), new C0093a() {
                public final void a(Bitmap bitmap, int i) {
                    NativeAdDetails.this.c = bitmap;
                    new com.startapp.common.a(NativeAdDetails.this.getSecondaryImageUrl(), new C0093a() {
                        public final void a(Bitmap bitmap, int i) {
                            NativeAdDetails.this.d = bitmap;
                            NativeAdDetails.this.b();
                        }
                    }, i).a();
                }
            }, i2).a();
        } else {
            b();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("         Title: [");
        sb.append(getTitle());
        sb.append("]\n         Description: [");
        sb.append(getDescription().substring(0, 30));
        sb.append("]...\n         Rating: [");
        sb.append(getRating());
        sb.append("]\n         Installs: [");
        sb.append(getInstalls());
        sb.append("]\n         Category: [");
        sb.append(getCategory());
        sb.append("]\n         PackageName: [");
        sb.append(getPackacgeName());
        sb.append("]\n         CampaginAction: [");
        sb.append(getCampaignAction());
        sb.append("]\n");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        unregisterView();
    }

    public boolean isBelowMinCPM() {
        return this.f3914a != null && this.f3914a.getIsBelowMinCPM();
    }

    /* access modifiers changed from: private */
    public void b() {
        new Handler().post(new Runnable() {
            public final void run() {
                StringBuilder sb = new StringBuilder("SingleAd [");
                sb.append(NativeAdDetails.this.b);
                sb.append("] Loaded");
                if (NativeAdDetails.this.g != null) {
                    NativeAdDetails.this.g.onNativeAdDetailsLoaded(NativeAdDetails.this.b);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.h = str;
    }

    public String getTitle() {
        return this.f3914a != null ? this.f3914a.getTitle() : "";
    }

    public String getDescription() {
        return this.f3914a != null ? this.f3914a.getDescription() : "";
    }

    public float getRating() {
        if (this.f3914a != null) {
            return this.f3914a.getRating();
        }
        return 5.0f;
    }

    public String getImageUrl() {
        return this.f3914a != null ? this.f3914a.getImageUrl() : "";
    }

    public String getSecondaryImageUrl() {
        return this.f3914a != null ? this.f3914a.getSecondaryImageUrl() : "";
    }

    public Bitmap getImageBitmap() {
        return this.c;
    }

    public Bitmap getSecondaryImageBitmap() {
        return this.d;
    }

    public String getInstalls() {
        return this.f3914a != null ? this.f3914a.getInstalls() : "";
    }

    public String getCategory() {
        return this.f3914a != null ? this.f3914a.getCategory() : "";
    }

    public String getPackacgeName() {
        return this.f3914a != null ? this.f3914a.getPackageName() : "";
    }

    public CampaignAction getCampaignAction() {
        CampaignAction campaignAction = CampaignAction.OPEN_MARKET;
        return (this.f3914a == null || !this.f3914a.isCPE()) ? campaignAction : CampaignAction.LAUNCH_APP;
    }

    public boolean isApp() {
        if (this.f3914a != null) {
            return this.f3914a.isApp();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final AdDetails a() {
        return this.f3914a;
    }

    public void registerViewForInteraction(@NonNull View view, @Nullable List<View> list) {
        registerViewForInteraction(view, list, null);
    }

    public void registerViewForInteraction(@NonNull View view, @Nullable List<View> list, @Nullable NativeAdDisplayListener nativeAdDisplayListener) {
        if (list == null || list.isEmpty() || this.j.get() != null) {
            registerViewForInteraction(view);
        } else {
            b bVar = new b(this, 0);
            for (View onClickListener : list) {
                onClickListener.setOnClickListener(bVar);
            }
        }
        if (nativeAdDisplayListener != null) {
            this.l = new c(nativeAdDisplayListener);
        }
    }

    public void unregisterView() {
        e();
        View view = (View) this.j.get();
        this.j.clear();
        if (!(view == null || VERSION.SDK_INT < 12 || this.k == null)) {
            view.removeOnAttachStateChangeListener(this.k);
        }
        if (this.c != null) {
            this.c.recycle();
            this.c = null;
        }
        if (this.d != null) {
            this.d.recycle();
            this.d = null;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.i == null && !this.e) {
            View view = (View) this.j.get();
            if (view == null) {
                if (this.l != null) {
                    this.l.adNotDisplayed(this);
                }
                return;
            }
            i iVar = new i(view.getContext(), new String[]{this.f3914a.getTrackingUrl()}, new com.startapp.android.publish.adsCommon.e.b(this.h), f());
            iVar.a((com.startapp.android.publish.adsCommon.i.a) new com.startapp.android.publish.adsCommon.i.a() {
                public final void onSent() {
                    NativeAdDetails.this.e = true;
                    if (NativeAdDetails.this.l != null) {
                        NativeAdDetails.this.l.adDisplayed(NativeAdDetails.this);
                    }
                }
            });
            this.i = new com.startapp.android.publish.adsCommon.m.b(this.j, iVar, d());
            this.i.a(new com.startapp.android.publish.adsCommon.m.b.a() {
                public final void a() {
                    if (NativeAdDetails.this.l != null && !NativeAdDetails.this.f) {
                        NativeAdDetails.this.l.adHidden(NativeAdDetails.this);
                        NativeAdDetails.this.f = true;
                    }
                }
            });
            this.i.a();
        }
    }

    private static int d() {
        return c.a().b().q();
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.i != null) {
            this.i.b();
            this.i = null;
        }
    }

    private long f() {
        if (this.f3914a.getDelayImpressionInSeconds() != null) {
            return TimeUnit.SECONDS.toMillis(this.f3914a.getDelayImpressionInSeconds().longValue());
        }
        return TimeUnit.SECONDS.toMillis(e.getInstance().getIABDisplayImpressionDelayInSeconds());
    }

    public void registerViewForInteraction(@NonNull View view) {
        this.j = new WeakReference<>(view);
        if (view.hasWindowFocus() || VERSION.SDK_INT < 12) {
            c();
        } else {
            if (this.k == null) {
                this.k = new OnAttachStateChangeListener() {
                    public final void onViewAttachedToWindow(View view) {
                        NativeAdDetails.this.c();
                    }

                    public final void onViewDetachedFromWindow(View view) {
                        NativeAdDetails.this.e();
                        view.removeOnAttachStateChangeListener(NativeAdDetails.this.k);
                    }
                };
            }
            view.addOnAttachStateChangeListener(this.k);
        }
        ((View) this.j.get()).setOnClickListener(new b(this, 0));
    }

    static /* synthetic */ void a(NativeAdDetails nativeAdDetails, View view) {
        Context context = view.getContext();
        switch (nativeAdDetails.getCampaignAction()) {
            case OPEN_MARKET:
                boolean a2 = com.startapp.android.publish.adsCommon.c.a(context, Placement.INAPP_NATIVE);
                if (nativeAdDetails.f3914a.isSmartRedirect() && !a2) {
                    com.startapp.android.publish.adsCommon.c.a(context, nativeAdDetails.f3914a.getClickUrl(), nativeAdDetails.f3914a.getTrackingClickUrl(), nativeAdDetails.f3914a.getPackageName(), new com.startapp.android.publish.adsCommon.e.b(nativeAdDetails.h), com.startapp.android.publish.adsCommon.b.a().A(), com.startapp.android.publish.adsCommon.b.a().B(), nativeAdDetails.f3914a.isStartappBrowserEnabled(), nativeAdDetails.f3914a.shouldSendRedirectHops());
                    break;
                } else {
                    com.startapp.android.publish.adsCommon.c.a(context, nativeAdDetails.f3914a.getClickUrl(), nativeAdDetails.f3914a.getTrackingClickUrl(), new com.startapp.android.publish.adsCommon.e.b(nativeAdDetails.h), nativeAdDetails.f3914a.isStartappBrowserEnabled() && !a2, false);
                    break;
                }
            case LAUNCH_APP:
                com.startapp.android.publish.adsCommon.c.a(nativeAdDetails.getPackacgeName(), nativeAdDetails.f3914a.getIntentDetails(), nativeAdDetails.f3914a.getClickUrl(), context, new com.startapp.android.publish.adsCommon.e.b(nativeAdDetails.h));
                break;
        }
        if (nativeAdDetails.l != null) {
            nativeAdDetails.l.adClicked(nativeAdDetails);
        }
    }
}
