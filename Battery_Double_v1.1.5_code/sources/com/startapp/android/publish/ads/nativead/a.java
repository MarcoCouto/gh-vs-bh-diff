package com.startapp.android.publish.ads.nativead;

import android.content.Context;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.common.model.GetAdRequest;

/* compiled from: StartAppSDK */
public final class a extends com.startapp.android.publish.a.a {
    private NativeAdPreferences g;

    /* access modifiers changed from: protected */
    public final void a(Ad ad) {
    }

    public a(Context context, Ad ad, AdPreferences adPreferences, AdEventListener adEventListener, NativeAdPreferences nativeAdPreferences) {
        super(context, ad, adPreferences, adEventListener, Placement.INAPP_NATIVE);
        this.g = nativeAdPreferences;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        GetAdRequest a2 = super.a();
        if (a2 == null) {
            return null;
        }
        a2.setAdsNumber(this.g.getAdsNumber());
        if (this.g.getImageSize() != null) {
            a2.setWidth(this.g.getImageSize().getWidth());
            a2.setHeight(this.g.getImageSize().getHeight());
        } else {
            int primaryImageSize = this.g.getPrimaryImageSize();
            if (primaryImageSize == -1) {
                primaryImageSize = 2;
            }
            a2.setPrimaryImg(Integer.toString(primaryImageSize));
            int secondaryImageSize = this.g.getSecondaryImageSize();
            if (secondaryImageSize == -1) {
                secondaryImageSize = 2;
            }
            a2.setMoreImg(Integer.toString(secondaryImageSize));
        }
        if (this.g.isContentAd()) {
            a2.setContentAd(this.g.isContentAd());
        }
        return a2;
    }
}
