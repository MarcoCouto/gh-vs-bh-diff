package com.startapp.android.publish.ads.nativead;

import android.content.Context;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.adListeners.b;
import com.startapp.android.publish.adsCommon.b.f;
import com.startapp.android.publish.adsCommon.b.g;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.common.model.AdDetails;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public class StartAppNativeAd extends Ad implements com.startapp.android.publish.ads.nativead.NativeAdDetails.a {
    private static final String TAG = "StartAppNativeAd";
    private static final long serialVersionUID = 1;
    private a adEventDelegate;
    boolean isLoading = false;
    private List<NativeAdDetails> listNativeAds = new ArrayList();
    private b nativeAd;
    private NativeAdPreferences preferences;
    private int totalObjectsLoaded = 0;

    /* compiled from: StartAppSDK */
    public enum CampaignAction {
        LAUNCH_APP,
        OPEN_MARKET
    }

    /* compiled from: StartAppSDK */
    class a implements AdEventListener {

        /* renamed from: a reason: collision with root package name */
        private AdEventListener f3923a = null;

        public a(AdEventListener adEventListener) {
            this.f3923a = new b(adEventListener);
        }

        public final void onReceiveAd(Ad ad) {
            StartAppNativeAd.this.initNativeAdList();
        }

        public final void onFailedToReceiveAd(Ad ad) {
            StartAppNativeAd.this.setErrorMessage(ad.getErrorMessage());
            if (this.f3923a != null) {
                this.f3923a.onFailedToReceiveAd(StartAppNativeAd.this);
                this.f3923a = null;
            }
            StartAppNativeAd.this.isLoading = false;
            StartAppNativeAd.this.initNativeAdList();
        }

        public final AdEventListener a() {
            return this.f3923a;
        }
    }

    /* access modifiers changed from: protected */
    public void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
    }

    public StartAppNativeAd(Context context) {
        super(context, Placement.INAPP_NATIVE);
    }

    private NativeAdPreferences getPreferences() {
        return this.preferences;
    }

    private void setPreferences(NativeAdPreferences nativeAdPreferences) {
        this.preferences = nativeAdPreferences;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n===== StartAppNativeAd =====\n");
        for (int i = 0; i < getNumberOfAds(); i++) {
            stringBuffer.append(this.listNativeAds.get(i));
        }
        stringBuffer.append("===== End StartAppNativeAd =====");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: 0000 */
    public void initNativeAdList() {
        this.totalObjectsLoaded = 0;
        if (this.listNativeAds == null) {
            this.listNativeAds = new ArrayList();
        }
        this.listNativeAds.clear();
        if (this.nativeAd != null && this.nativeAd.d() != null) {
            for (int i = 0; i < this.nativeAd.d().size(); i++) {
                this.listNativeAds.add(new NativeAdDetails((AdDetails) this.nativeAd.d().get(i), getPreferences(), i, this));
            }
        }
    }

    public void onNativeAdDetailsLoaded(int i) {
        this.totalObjectsLoaded++;
        if (this.nativeAd.d() != null && this.totalObjectsLoaded == this.nativeAd.d().size()) {
            onNativeAdLoaded();
        }
    }

    private void onNativeAdLoaded() {
        this.isLoading = false;
        setErrorMessage(null);
        if (this.adEventDelegate != null) {
            AdEventListener a2 = this.adEventDelegate.a();
            if (a2 != null) {
                a2.onReceiveAd(this);
            }
        }
    }

    public int getNumberOfAds() {
        if (this.listNativeAds != null) {
            return this.listNativeAds.size();
        }
        return 0;
    }

    public boolean isBelowMinCPM() {
        return this.nativeAd.isBelowMinCPM();
    }

    public boolean loadAd() {
        return loadAd(new NativeAdPreferences(), null);
    }

    public boolean loadAd(AdEventListener adEventListener) {
        return loadAd(new NativeAdPreferences(), adEventListener);
    }

    public boolean loadAd(NativeAdPreferences nativeAdPreferences) {
        return loadAd(nativeAdPreferences, null);
    }

    public boolean loadAd(NativeAdPreferences nativeAdPreferences, AdEventListener adEventListener) {
        this.adEventDelegate = new a(adEventListener);
        setPreferences(nativeAdPreferences);
        if (this.isLoading) {
            setErrorMessage("Ad is currently being loaded");
            return false;
        }
        this.isLoading = true;
        this.nativeAd = new b(this.context, getPreferences());
        return this.nativeAd.load(nativeAdPreferences, this.adEventDelegate);
    }

    public ArrayList<NativeAdDetails> getNativeAds() {
        return getNativeAds(null);
    }

    public ArrayList<NativeAdDetails> getNativeAds(String str) {
        ArrayList<NativeAdDetails> arrayList = new ArrayList<>();
        f a2 = g.a().b().a(Placement.INAPP_NATIVE, str);
        if (!a2.a()) {
            c.a(this.context, c.a(getAdDetailsList()), str, a2.c());
            Constants.a();
        } else if (this.listNativeAds != null) {
            for (NativeAdDetails nativeAdDetails : this.listNativeAds) {
                nativeAdDetails.a(str);
                arrayList.add(nativeAdDetails);
            }
            com.startapp.android.publish.adsCommon.b.b.a().a(new com.startapp.android.publish.adsCommon.b.a(Placement.INAPP_NATIVE, str));
        }
        return arrayList;
    }

    private List<AdDetails> getAdDetailsList() {
        ArrayList arrayList = new ArrayList();
        if (this.listNativeAds != null) {
            for (NativeAdDetails a2 : this.listNativeAds) {
                arrayList.add(a2.a());
            }
        }
        return arrayList;
    }

    public static String getPrivacyURL() {
        if (com.startapp.android.publish.adsCommon.adinformation.a.b().c() == null) {
            return "";
        }
        String c = com.startapp.android.publish.adsCommon.adinformation.a.b().c();
        if (c.contains("http://") || c.contains("https://")) {
            return com.startapp.android.publish.adsCommon.adinformation.a.b().c();
        }
        StringBuilder sb = new StringBuilder("https://");
        sb.append(com.startapp.android.publish.adsCommon.adinformation.a.b().c());
        return sb.toString();
    }

    public static String getPrivacyImageUrl() {
        return com.startapp.android.publish.adsCommon.adinformation.a.b().d();
    }
}
