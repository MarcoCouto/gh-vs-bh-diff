package com.startapp.android.publish.ads.nativead;

import android.os.Handler;
import android.os.Looper;

/* compiled from: StartAppSDK */
final class c implements NativeAdDisplayListener {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public NativeAdDisplayListener f3924a;

    c(NativeAdDisplayListener nativeAdDisplayListener) {
        this.f3924a = nativeAdDisplayListener;
    }

    public final void adHidden(final NativeAdInterface nativeAdInterface) {
        if (this.f3924a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    c.this.f3924a.adHidden(nativeAdInterface);
                }
            });
        }
    }

    public final void adDisplayed(final NativeAdInterface nativeAdInterface) {
        if (this.f3924a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    c.this.f3924a.adDisplayed(nativeAdInterface);
                }
            });
        }
    }

    public final void adClicked(final NativeAdInterface nativeAdInterface) {
        if (this.f3924a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    c.this.f3924a.adClicked(nativeAdInterface);
                }
            });
        }
    }

    public final void adNotDisplayed(final NativeAdInterface nativeAdInterface) {
        if (this.f3924a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    c.this.f3924a.adNotDisplayed(nativeAdInterface);
                }
            });
        }
    }
}
