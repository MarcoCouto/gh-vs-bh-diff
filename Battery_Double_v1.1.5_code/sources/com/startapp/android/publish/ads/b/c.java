package com.startapp.android.publish.ads.b;

import android.content.Context;
import android.content.Intent;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.startapp.android.publish.adsCommon.Ad.AdState;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.HtmlAd;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.activities.AppWallActivity;
import com.startapp.android.publish.adsCommon.activities.FullScreenActivity;
import com.startapp.android.publish.adsCommon.activities.OverlayActivity;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener.NotDisplayedReason;
import com.startapp.android.publish.adsCommon.b;
import com.startapp.android.publish.adsCommon.g;
import com.startapp.android.publish.adsCommon.n.a;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;

/* compiled from: StartAppSDK */
public abstract class c extends HtmlAd implements g {
    private static final long serialVersionUID = 1;

    /* access modifiers changed from: protected */
    public boolean a() {
        return false;
    }

    public c(Context context, Placement placement) {
        super(context, placement);
    }

    /* JADX WARNING: type inference failed for: r1v14, types: [java.lang.Boolean[], java.io.Serializable] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v14, types: [java.lang.Boolean[], java.io.Serializable]
  assigns: [java.lang.Boolean[]]
  uses: [java.io.Serializable]
  mth insns count: 173
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final boolean a(String str) {
        Class<FullScreenActivity> cls;
        String b = com.startapp.android.publish.adsCommon.c.b();
        if (!a() || !b.a().H().a().equals(a.DISABLED) || !b.equals("back")) {
            if (!AdsConstants.OVERRIDE_NETWORK.booleanValue()) {
                setState(AdState.UN_INITIALIZED);
            }
            if (getHtml() == null) {
                setNotDisplayedReason(NotDisplayedReason.INTERNAL_ERROR);
                return false;
            } else if (hasAdCacheTtlPassed()) {
                setNotDisplayedReason(NotDisplayedReason.AD_EXPIRED);
                return false;
            } else {
                boolean a2 = this.activityExtra != null ? this.activityExtra.a() : false;
                Context context = this.context;
                if (((getOrientation() != 0 && getOrientation() != this.context.getResources().getConfiguration().orientation) || a() || isMraidAd() || b.equals("back")) && j.a(getContext(), FullScreenActivity.class)) {
                    cls = FullScreenActivity.class;
                } else {
                    cls = j.a(getContext(), OverlayActivity.class, AppWallActivity.class);
                }
                Intent intent = new Intent(context, cls);
                intent.putExtra("fileUrl", "exit.html");
                String[] trackingUrls = getTrackingUrls();
                String a3 = com.startapp.android.publish.adsCommon.c.a();
                for (int i = 0; i < trackingUrls.length; i++) {
                    if (trackingUrls[i] != null && !"".equals(trackingUrls[i])) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(trackingUrls[i]);
                        sb.append(a3);
                        trackingUrls[i] = sb.toString();
                    }
                }
                intent.putExtra("tracking", trackingUrls);
                intent.putExtra("trackingClickUrl", getTrackingClickUrls());
                intent.putExtra("packageNames", getPackageNames());
                intent.putExtra("htmlUuid", getHtmlUuid());
                intent.putExtra("smartRedirect", this.smartRedirect);
                intent.putExtra("browserEnabled", getInAppBrowserEnabled());
                intent.putExtra("placement", this.placement.getIndex());
                intent.putExtra("adInfoOverride", getAdInfoOverride());
                intent.putExtra("ad", this);
                intent.putExtra("videoAd", a());
                intent.putExtra(Events.CREATIVE_FULLSCREEN, a2);
                intent.putExtra("orientation", getOrientation() == 0 ? this.context.getResources().getConfiguration().orientation : getOrientation());
                intent.putExtra("adTag", str);
                intent.putExtra("lastLoadTime", getLastLoadTime());
                intent.putExtra("adCacheTtl", getAdCacheTtl());
                intent.putExtra("closingUrl", getClosingUrl());
                intent.putExtra("rewardDuration", getPlayableRewardDuration());
                intent.putExtra("rewardedHideTimer", isRewardedHideTimer());
                if (getDelayImpressionInSeconds() != null) {
                    intent.putExtra("delayImpressionSeconds", getDelayImpressionInSeconds());
                }
                intent.putExtra("sendRedirectHops", shouldSendRedirectHops());
                intent.putExtra("mraidAd", isMraidAd());
                if (isMraidAd()) {
                    intent.putExtra("activityShouldLockOrientation", false);
                }
                if (j.a(8) && (this instanceof com.startapp.android.publish.ads.splash.b)) {
                    intent.putExtra("isSplash", true);
                }
                intent.putExtra(ParametersKeys.POSITION, b);
                intent.addFlags(343932928);
                this.context.startActivity(intent);
                return true;
            }
        } else {
            setNotDisplayedReason(NotDisplayedReason.VIDEO_BACK);
            return false;
        }
    }

    public String getLauncherName() {
        return super.getLauncherName();
    }

    public Long getLastLoadTime() {
        return super.getLastLoadTime();
    }

    public Long getAdCacheTtl() {
        return super.getAdCacheTtl();
    }

    public boolean hasAdCacheTtlPassed() {
        return super.hasAdCacheTtlPassed();
    }

    public boolean getVideoCancelCallBack() {
        return super.getVideoCancelCallBack();
    }

    public void setVideoCancelCallBack(boolean z) {
        super.setVideoCancelCallBack(z);
    }
}
