package com.startapp.android.publish.ads.b;

import android.content.Context;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.cache.d;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class e extends c {
    private static final long serialVersionUID = 1;

    public e(Context context) {
        super(context, Placement.INAPP_RETURN);
    }

    /* access modifiers changed from: protected */
    public final void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
        new b(this.context, this, adPreferences, adEventListener).c();
    }

    /* access modifiers changed from: protected */
    public final long getFallbackAdCacheTtl() {
        return d.a().b().getReturnAdCacheTTL();
    }
}
