package com.startapp.android.publish.ads.c.b;

import android.content.Context;
import android.content.Intent;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.startapp.android.publish.ads.list3d.List3DActivity;
import com.startapp.android.publish.ads.list3d.e;
import com.startapp.android.publish.adsCommon.Ad.AdState;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener.NotDisplayedReason;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.adsCommon.g;
import com.startapp.android.publish.adsCommon.h;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.UUID;

/* compiled from: StartAppSDK */
public final class b extends h implements g {

    /* renamed from: a reason: collision with root package name */
    private static String f3893a = null;
    private static final long serialVersionUID = 1;
    private final String uuid = UUID.randomUUID().toString();

    public b(Context context) {
        super(context, Placement.INAPP_OFFER_WALL);
        if (f3893a == null) {
            f3893a = j.c(context);
        }
    }

    /* access modifiers changed from: protected */
    public final void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
        new a(this.context, this, adPreferences, adEventListener).c();
    }

    public final boolean a(String str) {
        e.a().a(this.uuid).b(c.a());
        boolean a2 = this.activityExtra != null ? this.activityExtra.a() : false;
        if (hasAdCacheTtlPassed()) {
            setNotDisplayedReason(NotDisplayedReason.AD_EXPIRED);
            return false;
        }
        Intent intent = new Intent(this.context, List3DActivity.class);
        intent.putExtra("adInfoOverride", getAdInfoOverride());
        intent.putExtra(Events.CREATIVE_FULLSCREEN, a2);
        intent.putExtra("adTag", str);
        intent.putExtra("lastLoadTime", getLastLoadTime());
        intent.putExtra("adCacheTtl", getAdCacheTtl());
        intent.putExtra(ParametersKeys.POSITION, c.b());
        intent.putExtra("listModelUuid", this.uuid);
        intent.addFlags(343932928);
        this.context.startActivity(intent);
        if (!AdsConstants.OVERRIDE_NETWORK.booleanValue()) {
            setState(AdState.UN_INITIALIZED);
        }
        return true;
    }

    public final String getLauncherName() {
        return f3893a;
    }

    public final String a() {
        return this.uuid;
    }

    public final Long getLastLoadTime() {
        return super.getLastLoadTime();
    }

    public final Long getAdCacheTtl() {
        return super.getAdCacheTtl();
    }

    public final boolean hasAdCacheTtlPassed() {
        return super.hasAdCacheTtlPassed();
    }

    public final boolean getVideoCancelCallBack() {
        return super.getVideoCancelCallBack();
    }

    public final void setVideoCancelCallBack(boolean z) {
        super.setVideoCancelCallBack(z);
    }
}
