package com.startapp.android.publish.ads.c.a;

import android.content.Context;
import com.startapp.android.publish.ads.b.c;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class b extends c {
    private static final long serialVersionUID = 1;

    public b(Context context) {
        super(context, Placement.INAPP_OFFER_WALL);
    }

    /* access modifiers changed from: protected */
    public final void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
        new a(this.context, this, adPreferences, adEventListener).c();
    }
}
