package com.startapp.android.publish.ads.a;

import android.os.Handler;
import android.webkit.WebView;
import com.smaato.sdk.core.api.VideoType;

/* compiled from: StartAppSDK */
public final class f extends c {
    public final void a(WebView webView) {
        super.a(webView);
        if (g().equals(VideoType.INTERSTITIAL)) {
            webView.setBackgroundColor(0);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(final WebView webView) {
        new Handler().postDelayed(new Runnable() {
            public final void run() {
                try {
                    webView.setBackgroundColor(0);
                } catch (Exception unused) {
                }
            }
        }, 1000);
    }
}
