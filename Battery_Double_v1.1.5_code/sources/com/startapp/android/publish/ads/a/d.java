package com.startapp.android.publish.ads.a;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.ConsoleMessage;
import android.webkit.ConsoleMessage.MessageLevel;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.smaato.sdk.core.api.VideoType;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.h.a.a.C0089a;
import com.startapp.android.publish.adsCommon.h.a.c;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class d extends c implements C0089a {
    /* access modifiers changed from: private */
    public c j = c.LOADING;
    /* access modifiers changed from: private */
    public b k;
    /* access modifiers changed from: private */
    public com.startapp.android.publish.adsCommon.h.b.a l;
    /* access modifiers changed from: private */
    public com.startapp.android.publish.adsCommon.h.c.a m;
    /* access modifiers changed from: private */
    public ImageButton n;
    /* access modifiers changed from: private */
    public TextView o;
    /* access modifiers changed from: private */
    public ImageView p;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    /* access modifiers changed from: private */
    public Handler s = null;

    /* compiled from: StartAppSDK */
    class a extends com.startapp.android.publish.adsCommon.h.a.d {
        public a(com.startapp.android.publish.adsCommon.h.a.b bVar) {
            super(bVar);
        }

        public final void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (d.this.j == c.LOADING) {
                com.b.a.a.a.b.a(VideoType.INTERSTITIAL, webView);
                com.b.a.a.a.b.a((Context) d.this.b(), webView, d.this.l);
                d.this.G();
                d.m(d.this);
                d.this.j = c.DEFAULT;
                com.b.a.a.a.b.a(d.this.j, webView);
                com.b.a.a.a.b.a(webView);
                if (d.this.r) {
                    d.this.k.fireViewableChangeEvent();
                }
                d.n(d.this);
                d.this.a((View) d.this.n);
            }
        }
    }

    /* compiled from: StartAppSDK */
    class b extends com.startapp.android.publish.adsCommon.h.a.a {
        public b(C0089a aVar) {
            super(aVar);
        }

        public final void close() {
            d.this.j = c.HIDDEN;
            com.b.a.a.a.b.a(d.this.j, d.this.c);
            d.this.i.run();
        }

        public final void useCustomClose(String str) {
            boolean parseBoolean = Boolean.parseBoolean(str);
            if (d.this.q != parseBoolean) {
                d.this.q = parseBoolean;
                if (parseBoolean) {
                    d.h(d.this);
                    return;
                }
                d.this.H();
            }
        }

        public final void setOrientationProperties(@NonNull Map<String, String> map) {
            new StringBuilder("setOrientationProperties: ").append(map);
            boolean parseBoolean = Boolean.parseBoolean((String) map.get("allowOrientationChange"));
            String str = (String) map.get("forceOrientation");
            if (d.this.m.f4083a != parseBoolean || d.this.m.b != com.startapp.android.publish.adsCommon.h.c.a.a(str)) {
                d.this.m.f4083a = parseBoolean;
                d.this.m.b = com.startapp.android.publish.adsCommon.h.c.a.a(str);
                applyOrientationProperties(d.this.b(), d.this.m);
            }
        }

        public final boolean isFeatureSupported(String str) {
            return d.this.l.a(str);
        }

        public final void fireViewableChangeEvent() {
            com.b.a.a.a.b.a(d.this.c, d.this.r);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean b(String str) {
        return false;
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        if (this.l == null) {
            this.l = new com.startapp.android.publish.adsCommon.h.b.a(b());
        }
        if (this.m == null) {
            this.m = new com.startapp.android.publish.adsCommon.h.c.a();
        }
        if (this.k == null) {
            this.k = new b(this);
        }
    }

    public final void s() {
        super.s();
        if (this.s == null && F()) {
            this.s = new Handler();
        }
        this.r = true;
        if (this.j == c.DEFAULT) {
            this.k.fireViewableChangeEvent();
        }
    }

    public final void t() {
        G();
    }

    public final void q() {
        this.r = false;
        if (this.j == c.DEFAULT) {
            this.k.fireViewableChangeEvent();
        }
        super.q();
    }

    /* access modifiers changed from: protected */
    public final void w() {
        this.c.setWebViewClient(new a(this.k));
        this.c.setWebChromeClient(new WebChromeClient() {
            public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                try {
                    if (consoleMessage.messageLevel() == MessageLevel.ERROR) {
                        new StringBuilder("WebChromeClient console error: ").append(consoleMessage.message());
                        if (consoleMessage.message().contains(CampaignEx.JSON_KEY_MRAID)) {
                            f.a(d.this.b(), com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "MraidMode.ConsoleError", consoleMessage.message(), "");
                        }
                    } else {
                        new StringBuilder("WebChromeClient console log: ").append(consoleMessage.message());
                    }
                } catch (Exception e) {
                    new StringBuilder("WebChromeClient onConsoleMessage Exception: ").append(e.getMessage());
                }
                return super.onConsoleMessage(consoleMessage);
            }
        });
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str, boolean z) {
        this.j = c.HIDDEN;
        com.b.a.a.a.b.a(this.j, this.c);
        try {
            return super.a(str, z);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("url = [");
            sb.append(str);
            sb.append("], ");
            sb.append(e.getMessage());
            f.a(b(), com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "MraidMode.adClicked", sb.toString(), "");
            return false;
        }
    }

    @VisibleForTesting
    public final void G() {
        Activity b2 = b();
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            b2.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int i = displayMetrics.widthPixels;
            int i2 = displayMetrics.heightPixels;
            com.b.a.a.a.b.a((Context) b2, i, i2, this.c);
            com.b.a.a.a.b.b(b2, i, i2, this.c);
            com.b.a.a.a.b.a(b2, 0, 0, i, i2, this.c);
            com.b.a.a.a.b.b(b2, 0, 0, i, i2, this.c);
        } catch (Exception e) {
            f.a(b2, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "MraidMode.updateDisplayMetrics", e.getMessage(), "");
        }
    }

    /* access modifiers changed from: private */
    public void H() {
        try {
            if (this.n != null) {
                this.n.setImageDrawable(com.startapp.common.a.d.a(b().getResources(), "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA39pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDozODRkZTAxYi00OWRkLWM4NDYtYThkNC0wZWRiMDMwYTZlODAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QkE0Q0U2MUY2QzA0MTFFNUE3MkJGQjQ1MTkzOEYxQUUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QkE0Q0U2MUU2QzA0MTFFNUE3MkJGQjQ1MTkzOEYxQUUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjlkZjAyMGU0LTNlYmUtZTY0ZC04YjRiLWM5ZWY4MTU4ZjFhYyIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOmU1MzEzNDdlLTZjMDEtMTFlNS1hZGZlLThmMTBjZWYxMGRiZSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PngNsEEAAANeSURBVHjatFfNS1tBEH+pUZOQ0B4i3sTSxHMRFNQoFBEP7dHgvyDiKWgguQra9F+oxqNiwOTQ+oFI1ZM3jSf1YK5FL41ooaKZzu+x+4gv2bx9Rgd+JNn5zO7s7IzH0CQiCvLHZ8YnxkfGe8ZbwS4zSowTxi/GT4/Hc2u8BLHjCOM745b06VboRJpx7GN8ZfyDxUqlQgcHB5RMJmloaIg6Ozupra3NBL5jDTzIQFYQdDOw5db5B8YxLDw+PtLKygr19PQQWDqIRqOUzWZNXUHH2rvBgr2M39C6uLig/v5+bcd2QLdUKskgYLNX57yvIL2zs0OhUOjZziU6Ojro8PBQBnGl3Alm+BknkMI54mybdS4BW3t7ezKIInzVCwDJYm4Zon4p5xLYzfPzcxlEpl7S3SNpmjlznZwQiXn/5CjEnTUzt5GBsbExamlpUfLBg0wjG8vLy3IXlqTzEAoH7m4kElEqTk1Nmfd7bW2tbhBYAw8ykFXZgQ9RJ1CsQghgEr/29/eVStPT09XFhdbX18nr9Vr81tZWyuVyFh+yMzMzSnvwJWjyDS+MYic2NzeV17O7u9vg2m79jsfjBv9bg7PbxOrqqjExMWHxIdvV1aW0V+VrFDtwhFCGh4cbnl0mk6kp+BsbGybsBNlGtkZGRqToEQK4xjfUc6csXlhYcHyFFhcXHe3Al6BrQz427e3tWldpfn5e6Rw83cIkHyvXAUAZb4SdsKZbPe0BaB+Bz+cjTiDlDmxtbZkybo9AKwn9fj9tb2875gBkINvIFnzJJMQ1PMV9GBgYUF6bQCBgFAoFY3x8/Ml6KpUy0un0kzXIQBY6KqrydapViPL5fM0/Rfcj+fhuJw5CqxBpleJYLEY3NzeW8dnZ2RoZrEmCLHQcSvGdWYrFe7CEFTwUqqjR85XLZUokEkoZ8CADWe3HqKoTcnyOdW5KI5m+vj56eHiQz3G0bkNyeXn5ag3J2dmZ/PffVC1Z8bVast3d3eqWLKDVlAaDwaadh8Nhvaa0XluOHg7n9lzn0MWRarfltp0oysEErRqGDTeDCbK9ajApuh7TxGiWERlrjWZzc3M0ODhYM5phDTzbaHb/rNHMFkhUNK13LobTv6K2RJ3se1yO519s4/k7wf5jG89/6I7n/wUYAGo3YtcprD4sAAAAAElFTkSuQmCC"));
                this.n.setScaleType(ScaleType.FIT_CENTER);
            }
        } catch (Exception e) {
            f.a(b(), com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "MraidMode.showDefaultCloseButton", e.getMessage(), "");
        }
    }

    /* access modifiers changed from: protected */
    public final String B() {
        double currentTimeMillis = (double) (System.currentTimeMillis() - this.f);
        Double.isNaN(currentTimeMillis);
        return String.valueOf(currentTimeMillis / 1000.0d);
    }

    /* access modifiers changed from: protected */
    public final boolean F() {
        return d() > 0;
    }

    public final boolean p() {
        if (I()) {
            return super.p();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean I() {
        return (System.currentTimeMillis() - this.f) / 1000 >= ((long) d());
    }

    static /* synthetic */ void h(d dVar) {
        try {
            if (dVar.n != null) {
                dVar.n.setImageResource(17170445);
            }
        } catch (Exception e) {
            f.a(dVar.b(), com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "MraidMode.removeDefaultCloseButton", e.getMessage(), "");
        }
    }

    static /* synthetic */ void m(d dVar) {
        try {
            RelativeLayout relativeLayout = new RelativeLayout(dVar.b());
            dVar.n = new ImageButton(dVar.b());
            dVar.n.setBackgroundColor(0);
            dVar.n.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    if (d.this.I()) {
                        d.this.k.close();
                    }
                }
            });
            int a2 = i.a((Context) dVar.b(), 50);
            LayoutParams layoutParams = new LayoutParams(a2, a2);
            layoutParams.addRule(13);
            relativeLayout.addView(dVar.n, layoutParams);
            if (dVar.F() && !dVar.e()) {
                int a3 = i.a((Context) dVar.b(), 32);
                LayoutParams layoutParams2 = new LayoutParams(a3, a3);
                layoutParams2.addRule(13);
                dVar.p = new ImageView(dVar.b());
                ImageView imageView = dVar.p;
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setShape(1);
                gradientDrawable.setColor(ViewCompat.MEASURED_STATE_MASK);
                gradientDrawable.setStroke(2, -1);
                int a4 = i.a((Context) dVar.b(), 32);
                gradientDrawable.setSize(a4, a4);
                imageView.setImageDrawable(gradientDrawable);
                dVar.p.setScaleType(ScaleType.FIT_CENTER);
                relativeLayout.addView(dVar.p, layoutParams2);
                dVar.o = new TextView(dVar.b());
                dVar.o.setTextColor(-1);
                dVar.o.setGravity(17);
                relativeLayout.addView(dVar.o, layoutParams2);
            }
            if (!dVar.q) {
                dVar.H();
            }
            LayoutParams layoutParams3 = new LayoutParams(a2, a2);
            layoutParams3.addRule(10);
            layoutParams3.addRule(11);
            dVar.e.addView(relativeLayout, layoutParams3);
        } catch (Exception e) {
            f.a(dVar.b(), com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "MraidMode.addCloseRegion", e.getMessage(), "");
        }
    }

    static /* synthetic */ void n(d dVar) {
        if (dVar.s != null) {
            dVar.s.post(new Runnable() {
                public final void run() {
                    long d = (((long) (d.this.d() * 1000)) - System.currentTimeMillis()) + d.this.f;
                    if (d.this.o != null) {
                        long j = d / 1000;
                        if (j > 0 && d % 1000 < 100) {
                            j--;
                        }
                        d.this.o.setText(String.valueOf(j));
                    }
                    if (d >= 1000) {
                        d.this.s.postDelayed(this, c.a(d));
                        return;
                    }
                    if (d.this.o != null) {
                        d.this.p.setVisibility(8);
                        d.this.o.setVisibility(8);
                    }
                    d.this.D();
                }
            });
        }
    }

    public final boolean onClickEvent(String str) {
        return a(str, true);
    }
}
