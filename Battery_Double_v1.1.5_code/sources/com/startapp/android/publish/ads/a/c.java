package com.startapp.android.publish.ads.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.b.a.a.a.b.b;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.i;
import com.startapp.android.publish.adsCommon.m;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.html.JsInterface;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class c extends b {
    protected WebView c;
    protected b d;
    protected RelativeLayout e;
    protected long f = 0;
    protected boolean g = false;
    /* access modifiers changed from: protected */
    public int h = 0;
    protected Runnable i = new Runnable() {
        public final void run() {
            c.this.z();
            c.this.n();
        }
    };
    private Long j;
    private Long k;
    private i l;
    /* access modifiers changed from: private */
    public boolean m = true;
    /* access modifiers changed from: private */
    public boolean n = false;
    private Runnable o = new Runnable() {
        public final void run() {
            c.this.m = true;
            WebView webView = c.this.c;
            if (webView != null) {
                webView.setOnTouchListener(null);
            }
        }
    };

    /* compiled from: StartAppSDK */
    class a extends WebViewClient {
        a() {
        }

        public final void onPageFinished(WebView webView, String str) {
            c.this.b(webView);
            c.this.a("gClientInterface.setMode", c.this.g());
            c.this.a("enableScheme", "externalLinks");
            c.this.a((View) null);
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            StringBuilder sb = new StringBuilder("MyWebViewClient::shouldOverrideUrlLoading - [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            if (!c.this.n || c.this.m) {
                return c.this.a(str, false);
            }
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void E() {
    }

    /* access modifiers changed from: protected */
    public boolean F() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void b(WebView webView) {
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle == null) {
            if (a().hasExtra("lastLoadTime")) {
                this.j = (Long) a().getSerializableExtra("lastLoadTime");
            }
            if (a().hasExtra("adCacheTtl")) {
                this.k = (Long) a().getSerializableExtra("adCacheTtl");
            }
        } else {
            if (bundle.containsKey("postrollHtml")) {
                a(bundle.getString("postrollHtml"));
            }
            if (bundle.containsKey("lastLoadTime")) {
                this.j = (Long) bundle.getSerializable("lastLoadTime");
            }
            if (bundle.containsKey("adCacheTtl")) {
                this.k = (Long) bundle.getSerializable("adCacheTtl");
            }
            this.g = bundle.getBoolean("videoCompletedBroadcastSent", false);
            this.h = bundle.getInt("replayNum");
        }
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        if (f() != null) {
            bundle.putString("postrollHtml", f());
        }
        if (this.j != null) {
            bundle.putLong("lastLoadTime", this.j.longValue());
        }
        if (this.k != null) {
            bundle.putLong("adCacheTtl", this.k.longValue());
        }
        bundle.putBoolean("videoCompletedBroadcastSent", this.g);
        bundle.putInt("replayNum", this.h);
    }

    public final void u() {
        super.u();
        if (this.d != null) {
            this.d.b();
            this.d = null;
        }
        j.a((Object) this.c);
    }

    /* access modifiers changed from: protected */
    public void w() {
        this.c.setWebViewClient(new a());
        this.c.setWebChromeClient(new WebChromeClient());
    }

    /* access modifiers changed from: protected */
    public JsInterface x() {
        JsInterface jsInterface = new JsInterface(b(), this.i, this.i, this.o, G(), a(0));
        return jsInterface;
    }

    /* access modifiers changed from: protected */
    public void y() {
        this.l.a();
    }

    public void a(WebView webView) {
        this.m = false;
        webView.setOnTouchListener(new OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                c.this.m = true;
                return motionEvent.getAction() == 2;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        if (e.getInstance().isOmsdkEnabled() && this.d == null) {
            this.d = com.b.a.a.a.b.b(this.c);
            if (this.d != null && this.c != null) {
                if (this.f3862a != null) {
                    View a2 = this.f3862a.a();
                    if (a2 != null) {
                        this.d.b(a2);
                    }
                }
                if (view != null) {
                    this.d.b(view);
                }
                this.d.a(this.c);
                this.d.a();
                com.b.a.a.a.b.a.a(this.d).a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, Object... objArr) {
        j.a(this.c, str, objArr);
    }

    /* access modifiers changed from: protected */
    public static long a(long j2) {
        long j3 = j2 % 1000;
        if (j3 == 0) {
            return 1000;
        }
        return j3;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0035 A[SYNTHETIC, Splitter:B:13:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004c  */
    public boolean a(String str, boolean z) {
        boolean z2;
        this.l.a(true);
        Ad v = v();
        if (com.startapp.android.publish.adsCommon.c.a(b().getApplicationContext(), this.b)) {
            if (!(j.a(8) && (v instanceof com.startapp.android.publish.ads.splash.b))) {
                z2 = true;
                if (!b(str)) {
                    try {
                        int a2 = com.startapp.android.publish.adsCommon.c.a(str);
                        if (!c()[a2] || z2) {
                            b(str, a2, z);
                        } else {
                            a(str, a2, z);
                        }
                    } catch (Exception unused) {
                        return false;
                    }
                } else if (!c()[0] || z2) {
                    b(str, 0, z);
                } else {
                    a(str, 0, z);
                }
                return true;
            }
        }
        z2 = false;
        if (!b(str)) {
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        return !this.n && str.contains("index=");
    }

    /* access modifiers changed from: protected */
    public void z() {
        String[] k2 = k();
        if (k2 != null && k2.length > 0 && k()[0] != null) {
            com.startapp.android.publish.adsCommon.c.b((Context) b(), k()[0], G());
        }
    }

    private void a(String str, int i2, boolean z) {
        int i3 = i2;
        Activity b = b();
        String str2 = null;
        String str3 = i3 < i().length ? i()[i3] : null;
        if (i3 < j().length) {
            str2 = j()[i3];
        }
        com.startapp.android.publish.adsCommon.c.a(b, str, str3, str2, G(), com.startapp.android.publish.adsCommon.b.a().A(), com.startapp.android.publish.adsCommon.b.a().B(), a(i3), b(i3), z, new Runnable() {
            public final void run() {
                c.this.n();
            }
        });
    }

    private void b(String str, int i2, boolean z) {
        com.startapp.common.b.a((Context) b()).a(new Intent("com.startapp.android.OnClickCallback"));
        com.startapp.android.publish.adsCommon.c.a(b(), str, i2 < i().length ? i()[i2] : null, G(), a(i2) && !com.startapp.android.publish.adsCommon.c.a(b().getApplicationContext(), this.b), z);
        n();
    }

    public void n() {
        super.n();
        m.a().a(false);
        if (this.l != null) {
            this.l.a(false);
        }
        b().runOnUiThread(new Runnable() {
            public final void run() {
                if (c.this.c != null) {
                    com.startapp.common.a.c.a(c.this.c);
                }
            }
        });
    }

    public void q() {
        if (this.l != null) {
            this.l.b();
        }
        if (this.f3862a != null && this.f3862a.b()) {
            this.f3862a.e();
        }
        if (this.c != null) {
            com.startapp.common.a.c.a(this.c);
        }
        if (g().equals("back")) {
            n();
        }
    }

    private com.startapp.android.publish.adsCommon.e.b G() {
        return new com.startapp.android.publish.adsCommon.e.a(B(), l());
    }

    /* access modifiers changed from: protected */
    public com.startapp.android.publish.adsCommon.e.b A() {
        return new com.startapp.android.publish.adsCommon.e.b(l());
    }

    /* access modifiers changed from: protected */
    public String B() {
        double currentTimeMillis = (double) (System.currentTimeMillis() - this.f);
        Double.isNaN(currentTimeMillis);
        return String.valueOf(currentTimeMillis / 1000.0d);
    }

    public boolean p() {
        z();
        m.a().a(false);
        this.l.a(false);
        return false;
    }

    /* access modifiers changed from: protected */
    public long C() {
        if (m() != null) {
            return TimeUnit.SECONDS.toMillis(m().longValue());
        }
        return TimeUnit.SECONDS.toMillis(e.getInstance().getIABDisplayImpressionDelayInSeconds());
    }

    /* access modifiers changed from: protected */
    public final void D() {
        if (F() && !this.g && this.h == 0) {
            this.g = true;
            com.startapp.common.b.a((Context) b()).a(new Intent("com.startapp.android.OnVideoCompleted"));
            E();
        }
    }

    public void s() {
        if (v() instanceof com.startapp.android.publish.ads.b.c ? ((com.startapp.android.publish.ads.b.c) v()).hasAdCacheTtlPassed() : false) {
            n();
            return;
        }
        m.a().a(true);
        if (this.l == null) {
            i iVar = new i(b(), h(), A(), C());
            this.l = iVar;
        }
        if (this.c == null) {
            this.e = new RelativeLayout(b());
            this.e.setContentDescription("StartApp Ad");
            this.e.setId(AdsConstants.STARTAPP_AD_MAIN_LAYOUT_ID);
            b().setContentView(this.e);
            try {
                this.c = new WebView(b().getApplicationContext());
                this.c.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                b().getWindow().getDecorView().findViewById(16908290).setBackgroundColor(7829367);
                this.c.setVerticalScrollBarEnabled(false);
                this.c.setHorizontalScrollBarEnabled(false);
                this.c.getSettings().setJavaScriptEnabled(true);
                WebView webView = this.c;
                if (VERSION.SDK_INT >= 17) {
                    webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
                }
                this.c.setOnLongClickListener(new OnLongClickListener() {
                    public final boolean onLongClick(View view) {
                        return true;
                    }
                });
                this.c.setLongClickable(false);
                this.c.addJavascriptInterface(x(), "startappwall");
                y();
                a(this.c);
                j.a((Context) b(), this.c, f());
                this.n = "true".equals(j.a(f(), "@jsTag@", "@jsTag@"));
                w();
                this.e.addView(this.c, new LayoutParams(-1, -1));
                a(this.e);
            } catch (Exception e2) {
                f.a(b(), d.EXCEPTION, "InterstitialMode.onResume - WebView failed", e2.getMessage(), "");
                n();
            }
        } else {
            com.startapp.common.a.c.b(this.c);
            this.l.a();
        }
        this.f = System.currentTimeMillis();
    }
}
