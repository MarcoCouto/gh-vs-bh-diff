package com.startapp.android.publish.ads.a;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.RelativeLayout;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.splash.g;
import com.startapp.android.publish.ads.video.h;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adinformation.b.C0087b;
import com.startapp.android.publish.adsCommon.adinformation.c;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.inappbrowser.a;

/* compiled from: StartAppSDK */
public abstract class b {

    /* renamed from: a reason: collision with root package name */
    protected com.startapp.android.publish.adsCommon.adinformation.b f3862a = null;
    protected Placement b;
    private Intent c;
    private Activity d;
    private BroadcastReceiver e = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            b.this.n();
        }
    };
    private String[] f;
    private boolean[] g;
    private boolean[] h = {true};
    private String i;
    private String[] j;
    private String[] k;
    private String[] l;
    private Ad m;
    private String n;
    private boolean o;
    private c p;
    private String q;
    private Long r;
    private Boolean[] s = null;
    private int t = 0;
    private boolean u = false;
    private boolean v = false;

    public boolean a(int i2, KeyEvent keyEvent) {
        return false;
    }

    public void b(Bundle bundle) {
    }

    public boolean p() {
        return false;
    }

    public void r() {
    }

    public abstract void s();

    public void t() {
    }

    public static b a(Activity activity, Intent intent, Placement placement) {
        b bVar = null;
        switch (placement) {
            case INAPP_OFFER_WALL:
                if (j.a(128) || j.a(64)) {
                    bVar = new e();
                    break;
                }
            case INAPP_RETURN:
            case INAPP_OVERLAY:
                if (!j.a(4) || !intent.getBooleanExtra("videoAd", false)) {
                    if (!intent.getBooleanExtra("mraidAd", false)) {
                        bVar = new f();
                        break;
                    } else {
                        bVar = new d();
                        break;
                    }
                } else {
                    bVar = new h();
                    break;
                }
                break;
            case INAPP_SPLASH:
                if (j.a(8)) {
                    bVar = new g();
                    break;
                }
                break;
            case INAPP_FULL_SCREEN:
            case INAPP_BROWSER:
                if (j.a(256)) {
                    Uri data = intent.getData();
                    if (data != null) {
                        bVar = new a(data.toString());
                        break;
                    } else {
                        return null;
                    }
                }
                break;
            default:
                bVar = new a();
                break;
        }
        bVar.c = intent;
        bVar.d = activity;
        bVar.i = intent.getStringExtra(ParametersKeys.POSITION);
        bVar.j = intent.getStringArrayExtra("tracking");
        bVar.k = intent.getStringArrayExtra("trackingClickUrl");
        bVar.l = intent.getStringArrayExtra("packageNames");
        bVar.f = intent.getStringArrayExtra("closingUrl");
        bVar.g = intent.getBooleanArrayExtra("smartRedirect");
        bVar.h = intent.getBooleanArrayExtra("browserEnabled");
        String stringExtra = intent.getStringExtra("htmlUuid");
        if (stringExtra != null) {
            if (AdsConstants.OVERRIDE_NETWORK.booleanValue()) {
                bVar.n = com.startapp.android.publish.cache.a.a().a(stringExtra);
            } else {
                bVar.n = com.startapp.android.publish.cache.a.a().b(stringExtra);
            }
        }
        bVar.o = intent.getBooleanExtra("isSplash", false);
        bVar.p = (c) intent.getSerializableExtra("adInfoOverride");
        bVar.q = intent.getStringExtra("adTag");
        bVar.b = placement;
        bVar.f = intent.getStringArrayExtra("closingUrl");
        bVar.t = intent.getIntExtra("rewardDuration", 0);
        bVar.u = intent.getBooleanExtra("rewardedHideTimer", false);
        if (bVar.g == null) {
            bVar.g = new boolean[]{true};
        }
        if (bVar.h == null) {
            bVar.h = new boolean[]{true};
        }
        bVar.m = (Ad) intent.getSerializableExtra("ad");
        long longExtra = intent.getLongExtra("delayImpressionSeconds", -1);
        if (longExtra != -1) {
            bVar.r = Long.valueOf(longExtra);
        }
        bVar.s = (Boolean[]) intent.getSerializableExtra("sendRedirectHops");
        StringBuilder sb = new StringBuilder("Placement=[");
        sb.append(bVar.b);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return bVar;
    }

    public final Intent a() {
        return this.c;
    }

    public final Activity b() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.n = str;
    }

    /* access modifiers changed from: protected */
    public final boolean[] c() {
        return this.g;
    }

    public final int d() {
        return this.t;
    }

    public final boolean e() {
        return this.u;
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i2) {
        if (this.h == null || i2 < 0 || i2 >= this.h.length) {
            return true;
        }
        return this.h[i2];
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final String[] h() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final String[] i() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public final String[] j() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final String[] k() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final String l() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public final void a(RelativeLayout relativeLayout) {
        this.f3862a = new com.startapp.android.publish.adsCommon.adinformation.b(this.d, C0087b.LARGE, this.b, this.p);
        this.f3862a.a(relativeLayout);
    }

    public final Long m() {
        return this.r;
    }

    public final Boolean b(int i2) {
        if (this.s == null || i2 < 0 || i2 >= this.s.length) {
            return null;
        }
        return this.s[i2];
    }

    public void o() {
        com.startapp.common.b.a((Context) this.d).a(new Intent("com.startapp.android.HideDisplayBroadcastListener"));
    }

    public void q() {
        n();
    }

    public void u() {
        if (this.e != null) {
            com.startapp.common.b.a((Context) this.d).a(this.e);
        }
        this.e = null;
    }

    public final Ad v() {
        return this.m;
    }

    public void n() {
        this.d.runOnUiThread(new Runnable() {
            public final void run() {
                b.this.b().finish();
            }
        });
    }

    public void a(Bundle bundle) {
        com.startapp.common.b.a((Context) this.d).a(this.e, new IntentFilter("com.startapp.android.CloseAdActivity"));
    }
}
