package com.startapp.android.publish.ads.list3d;

import com.startapp.android.publish.adsCommon.m.a;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: StartAppSDK */
public final class e {

    /* renamed from: a reason: collision with root package name */
    private static e f3913a = new e();
    private Map<String, a> b = new ConcurrentHashMap();

    private e() {
    }

    public static e a() {
        return f3913a;
    }

    public final a a(String str) {
        if (this.b.containsKey(str)) {
            return (a) this.b.get(str);
        }
        a aVar = new a();
        this.b.put(str, aVar);
        StringBuilder sb = new StringBuilder("Created new model for uuid ");
        sb.append(str);
        sb.append(", Size = ");
        sb.append(this.b.size());
        return aVar;
    }

    public final void b(String str) {
        this.b.remove(str);
        StringBuilder sb = new StringBuilder("Model for ");
        sb.append(str);
        sb.append(" was removed, Size = ");
        sb.append(this.b.size());
    }
}
