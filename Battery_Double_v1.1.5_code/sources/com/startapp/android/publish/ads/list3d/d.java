package com.startapp.android.publish.ads.list3d;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.text.TextUtils.TruncateAt;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.android.publish.a.b;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.android.publish.common.metaData.h;
import com.startapp.common.a.c;

/* compiled from: StartAppSDK */
public final class d {

    /* renamed from: a reason: collision with root package name */
    private RelativeLayout f3911a;
    private ImageView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private b f;
    private h g = null;

    public d(Context context) {
        Context context2 = context;
        context2.setTheme(16973829);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        this.f3911a = new RelativeLayout(context2);
        this.f3911a.setBackgroundDrawable(new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{com.startapp.android.publish.adsCommon.b.a().n(), com.startapp.android.publish.adsCommon.b.a().o()}));
        this.f3911a.setLayoutParams(layoutParams);
        int a2 = i.a(context2, 3);
        int a3 = i.a(context2, 4);
        int a4 = i.a(context2, 5);
        int a5 = i.a(context2, 6);
        int a6 = i.a(context2, 10);
        int a7 = i.a(context2, 84);
        this.f3911a.setPadding(a6, a2, a6, a2);
        this.f3911a.setTag(this);
        this.b = new ImageView(context2);
        this.b.setId(1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(a7, a7);
        layoutParams2.addRule(15);
        this.b.setLayoutParams(layoutParams2);
        this.b.setPadding(0, 0, a5, 0);
        this.c = new TextView(context2);
        this.c.setId(2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(c.a(17), 1);
        layoutParams3.addRule(6, 1);
        this.c.setLayoutParams(layoutParams3);
        this.c.setPadding(0, 0, 0, a4);
        this.c.setTextColor(com.startapp.android.publish.adsCommon.b.a().q().intValue());
        this.c.setTextSize((float) com.startapp.android.publish.adsCommon.b.a().p().intValue());
        this.c.setSingleLine(true);
        this.c.setEllipsize(TruncateAt.END);
        i.a(this.c, com.startapp.android.publish.adsCommon.b.a().r());
        this.d = new TextView(context2);
        this.d.setId(3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.addRule(c.a(17), 1);
        layoutParams4.addRule(3, 2);
        layoutParams4.setMargins(0, 0, 0, a4);
        this.d.setLayoutParams(layoutParams4);
        this.d.setTextColor(com.startapp.android.publish.adsCommon.b.a().t().intValue());
        this.d.setTextSize((float) com.startapp.android.publish.adsCommon.b.a().s().intValue());
        this.d.setSingleLine(true);
        this.d.setEllipsize(TruncateAt.END);
        i.a(this.d, com.startapp.android.publish.adsCommon.b.a().u());
        this.f = new b(context2);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.addRule(c.a(17), 1);
        layoutParams5.addRule(8, 1);
        layoutParams5.setMargins(0, 0, 0, -a4);
        this.f.setLayoutParams(layoutParams5);
        this.f.setPadding(0, 0, 0, a3);
        this.f.setId(5);
        this.e = new TextView(context2);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams6.addRule(c.a(21));
        layoutParams6.addRule(8, 1);
        this.e.setLayoutParams(layoutParams6);
        a(false);
        this.e.setTextColor(-1);
        this.e.setTextSize(12.0f);
        this.e.setTypeface(null, 1);
        this.e.setPadding(a6, a5, a6, a5);
        this.e.setId(4);
        this.e.setShadowLayer(2.5f, -3.0f, 3.0f, -9013642);
        this.e.setBackgroundDrawable(new ShapeDrawable(new RoundRectShape(new float[]{10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f}, null, null)) {
            /* access modifiers changed from: protected */
            public final void onDraw(Shape shape, Canvas canvas, Paint paint) {
                paint.setColor(-11363070);
                paint.setMaskFilter(new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 5.0f, 3.0f));
                super.onDraw(shape, canvas, paint);
            }
        });
        this.f3911a.addView(this.b);
        this.f3911a.addView(this.c);
        this.f3911a.addView(this.d);
        this.f3911a.addView(this.f);
        this.f3911a.addView(this.e);
    }

    public final RelativeLayout a() {
        return this.f3911a;
    }

    public final ImageView b() {
        return this.b;
    }

    public final TextView c() {
        return this.c;
    }

    public final TextView d() {
        return this.d;
    }

    public final b e() {
        return this.f;
    }

    public final void a(boolean z) {
        if (z) {
            this.e.setText("Open");
        } else {
            this.e.setText("Download");
        }
    }

    public final void a(h hVar) {
        if (this.g != hVar) {
            this.g = hVar;
            this.f3911a.setBackgroundDrawable(new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{hVar.getItemGradientTop().intValue(), hVar.getItemGradientBottom().intValue()}));
            this.c.setTextSize((float) hVar.getItemTitleTextSize().intValue());
            this.c.setTextColor(hVar.getItemTitleTextColor().intValue());
            i.a(this.c, hVar.getItemTitleTextDecoration());
            this.d.setTextSize((float) hVar.getItemDescriptionTextSize().intValue());
            this.d.setTextColor(hVar.getItemDescriptionTextColor().intValue());
            i.a(this.d, hVar.getItemDescriptionTextDecoration());
        }
    }
}
