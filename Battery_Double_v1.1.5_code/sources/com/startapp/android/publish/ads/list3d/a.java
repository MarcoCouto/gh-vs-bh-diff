package com.startapp.android.publish.ads.list3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import com.startapp.android.publish.adsCommon.i;
import com.startapp.common.a.d;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    Hashtable<String, Bitmap> f3902a = new Hashtable<>();
    f b;
    int c = 0;
    ConcurrentLinkedQueue<b> d = new ConcurrentLinkedQueue<>();
    private HashMap<String, i> e = new HashMap<>();
    private Set<String> f = new HashSet();

    /* renamed from: com.startapp.android.publish.ads.list3d.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    class C0084a extends AsyncTask<Void, Void, Bitmap> {

        /* renamed from: a reason: collision with root package name */
        private int f3903a = -1;
        private String b;
        private String c;

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            Bitmap bitmap = (Bitmap) obj;
            a.this.c--;
            if (bitmap != null) {
                a.this.f3902a.put(this.b, bitmap);
                if (a.this.b != null) {
                    a.this.b.a(this.f3903a);
                }
                a aVar = a.this;
                if (!aVar.d.isEmpty()) {
                    b bVar = (b) aVar.d.poll();
                    new C0084a(bVar.f3904a, bVar.b, bVar.c).execute(new Void[0]);
                }
            }
        }

        public C0084a(int i, String str, String str2) {
            this.f3903a = i;
            this.b = str;
            this.c = str2;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return d.b(this.c);
        }
    }

    /* compiled from: StartAppSDK */
    class b {

        /* renamed from: a reason: collision with root package name */
        int f3904a;
        String b;
        String c;

        public b(int i, String str, String str2) {
            this.f3904a = i;
            this.b = str;
            this.c = str2;
        }
    }

    public final void a(f fVar, boolean z) {
        this.b = fVar;
        if (z) {
            this.f.clear();
            this.c = 0;
            this.d.clear();
            if (this.e != null) {
                for (String str : this.e.keySet()) {
                    ((i) this.e.get(str)).a(false);
                }
                this.e.clear();
            }
        }
    }

    public final void a(Context context, String str, com.startapp.android.publish.adsCommon.e.b bVar, long j) {
        if (!this.e.containsKey(str)) {
            i iVar = new i(context, new String[]{str}, bVar, j);
            this.e.put(str, iVar);
            iVar.a();
        }
    }

    public final void a(String str) {
        if (this.e != null && this.e.containsKey(str) && this.e.get(str) != null) {
            ((i) this.e.get(str)).a(true);
        }
    }

    public final void a() {
        for (String str : this.e.keySet()) {
            if (this.e.get(str) != null) {
                ((i) this.e.get(str)).b();
            }
        }
    }

    public final void b() {
        for (String str : this.e.keySet()) {
            if (this.e.get(str) != null) {
                ((i) this.e.get(str)).a();
            }
        }
    }

    public final void c() {
        for (String str : this.e.keySet()) {
            if (this.e.get(str) != null) {
                ((i) this.e.get(str)).a(false);
            }
        }
    }

    public final Bitmap a(int i, String str, String str2) {
        Bitmap bitmap = (Bitmap) this.f3902a.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        if (!this.f.contains(str)) {
            this.f.add(str);
            if (this.c >= 15) {
                this.d.add(new b(i, str, str2));
            } else {
                this.c++;
                new C0084a(i, str, str2).execute(new Void[0]);
            }
        }
        return null;
    }
}
