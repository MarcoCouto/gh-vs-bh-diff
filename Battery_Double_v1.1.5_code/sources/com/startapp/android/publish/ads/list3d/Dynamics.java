package com.startapp.android.publish.ads.list3d;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.animation.AnimationUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;

/* compiled from: StartAppSDK */
public abstract class Dynamics implements Parcelable {

    /* renamed from: a reason: collision with root package name */
    protected float f3894a;
    protected float b;
    private float c = Float.MAX_VALUE;
    private float d = -3.4028235E38f;
    private long e = 0;

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    public int describeContents() {
        return 0;
    }

    public Dynamics() {
    }

    public Dynamics(Parcel parcel) {
        this.f3894a = parcel.readFloat();
        this.b = parcel.readFloat();
        this.c = parcel.readFloat();
        this.d = parcel.readFloat();
        this.e = AnimationUtils.currentAnimationTimeMillis();
    }

    public final void a(float f, float f2, long j) {
        this.b = f2;
        this.f3894a = f;
        this.e = j;
    }

    public final float a() {
        return this.f3894a;
    }

    public final float b() {
        return this.b;
    }

    public final boolean c() {
        return ((Math.abs(this.b) > 0.5f ? 1 : (Math.abs(this.b) == 0.5f ? 0 : -1)) < 0) && (((this.f3894a - 0.4f) > this.c ? 1 : ((this.f3894a - 0.4f) == this.c ? 0 : -1)) < 0 && ((this.f3894a + 0.4f) > this.d ? 1 : ((this.f3894a + 0.4f) == this.d ? 0 : -1)) > 0);
    }

    public final void a(float f) {
        this.c = f;
    }

    public final void b(float f) {
        this.d = f;
    }

    public final void a(long j) {
        if (this.e != 0) {
            int i = (int) (j - this.e);
            if (i > 50) {
                i = 50;
            }
            a(i);
        }
        this.e = j;
    }

    /* access modifiers changed from: protected */
    public final float d() {
        if (this.f3894a > this.c) {
            return this.c - this.f3894a;
        }
        if (this.f3894a < this.d) {
            return this.d - this.f3894a;
        }
        return 0.0f;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.f3894a);
        parcel.writeFloat(this.b);
        parcel.writeFloat(this.c);
        parcel.writeFloat(this.d);
    }

    public void a(double d2) {
        double d3 = (double) this.f3894a;
        Double.isNaN(d3);
        this.f3894a = (float) (d3 * d2);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Position: [");
        sb.append(this.f3894a);
        sb.append("], Velocity:[");
        sb.append(this.b);
        sb.append("], MaxPos: [");
        sb.append(this.c);
        sb.append("], mMinPos: [");
        sb.append(this.d);
        sb.append("] LastTime:[");
        sb.append(this.e);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
