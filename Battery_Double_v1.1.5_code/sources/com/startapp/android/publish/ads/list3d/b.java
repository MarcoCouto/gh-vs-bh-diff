package com.startapp.android.publish.ads.list3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.startapp.android.publish.adsCommon.m.a;
import com.startapp.android.publish.common.metaData.e;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class b extends ArrayAdapter<ListItem> {

    /* renamed from: a reason: collision with root package name */
    private String f3905a;
    private String b;

    public b(Context context, List<ListItem> list, String str, String str2) {
        super(context, 0, list);
        this.f3905a = str;
        this.b = str2;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        d dVar;
        long millis;
        if (view == null) {
            dVar = new d(getContext());
            view2 = dVar.a();
        } else {
            view2 = view;
            dVar = (d) view.getTag();
        }
        ListItem listItem = (ListItem) getItem(i);
        dVar.a(com.startapp.android.publish.adsCommon.b.a().a(listItem.m()));
        dVar.c().setText(listItem.g());
        dVar.d().setText(listItem.h());
        Bitmap a2 = e.a().a(this.b).a(i, listItem.a(), listItem.i());
        if (a2 == null) {
            dVar.b().setImageResource(17301651);
            dVar.b().setTag("tag_error");
        } else {
            dVar.b().setImageBitmap(a2);
            dVar.b().setTag("tag_ok");
        }
        dVar.e().setRating(listItem.j());
        dVar.a(listItem.p());
        a a3 = e.a().a(this.b);
        Context context = getContext();
        String c = listItem.c();
        com.startapp.android.publish.adsCommon.e.b bVar = new com.startapp.android.publish.adsCommon.e.b(this.f3905a);
        if (listItem.q() != null) {
            millis = TimeUnit.SECONDS.toMillis(listItem.q().longValue());
        } else {
            millis = TimeUnit.SECONDS.toMillis(e.getInstance().getIABDisplayImpressionDelayInSeconds());
        }
        a3.a(context, c, bVar, millis);
        return view2;
    }
}
