package com.startapp.android.publish.ads.list3d;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.a;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adinformation.b;
import com.startapp.android.publish.adsCommon.adinformation.b.C0087b;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.m;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.List;

/* compiled from: StartAppSDK */
public class List3DActivity extends Activity implements f {

    /* renamed from: a reason: collision with root package name */
    String f3895a;
    String b;
    List<ListItem> c;
    private c d;
    private ProgressDialog e = null;
    private WebView f = null;
    private int g;
    private b h;
    private Long i;
    private Long j;
    private String k;
    private long l = 0;
    private long m = 0;
    private BroadcastReceiver n = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            List3DActivity.this.finish();
        }
    };

    public void onCreate(Bundle bundle) {
        View view;
        try {
            overridePendingTransition(0, 0);
            super.onCreate(bundle);
            if (getIntent().getBooleanExtra(Events.CREATIVE_FULLSCREEN, false)) {
                requestWindowFeature(1);
                getWindow().setFlags(1024, 1024);
            }
            if (bundle == null) {
                com.startapp.common.b.a((Context) this).a(new Intent("com.startapp.android.ShowDisplayBroadcastListener"));
                this.i = (Long) getIntent().getSerializableExtra("lastLoadTime");
                this.j = (Long) getIntent().getSerializableExtra("adCacheTtl");
            } else {
                if (bundle.containsKey("lastLoadTime")) {
                    this.i = (Long) bundle.getSerializable("lastLoadTime");
                }
                if (bundle.containsKey("adCacheTtl")) {
                    this.j = (Long) bundle.getSerializable("adCacheTtl");
                }
            }
            this.k = getIntent().getStringExtra(ParametersKeys.POSITION);
            this.f3895a = getIntent().getStringExtra("listModelUuid");
            com.startapp.common.b.a((Context) this).a(this.n, new IntentFilter("com.startapp.android.CloseAdActivity"));
            this.g = getResources().getConfiguration().orientation;
            j.a((Activity) this, true);
            boolean booleanExtra = getIntent().getBooleanExtra("overlay", false);
            requestWindowFeature(1);
            this.b = getIntent().getStringExtra("adTag");
            int e2 = com.startapp.android.publish.adsCommon.b.a().e();
            int f2 = com.startapp.android.publish.adsCommon.b.a().f();
            this.d = new c(this, this.b, this.f3895a);
            this.d.setBackgroundDrawable(new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{e2, f2}));
            this.c = e.a().a(this.f3895a).e();
            if (this.c == null) {
                finish();
                return;
            }
            if (booleanExtra) {
                com.startapp.common.b.a((Context) this).a(this.d.p, new IntentFilter("com.startapp.android.Activity3DGetValues"));
            } else {
                this.d.a();
                this.d.setHint(true);
                this.d.setFade(true);
            }
            b bVar = new b(this, this.c, this.b, this.f3895a);
            e.a().a(this.f3895a).a((f) this, !booleanExtra);
            this.d.setAdapter(bVar);
            this.d.setDynamics(new SimpleDynamics());
            this.d.setOnItemClickListener(new OnItemClickListener() {
                public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    int i2 = i;
                    String b = ((ListItem) List3DActivity.this.c.get(i2)).b();
                    String e = ((ListItem) List3DActivity.this.c.get(i2)).e();
                    String f = ((ListItem) List3DActivity.this.c.get(i2)).f();
                    boolean k = ((ListItem) List3DActivity.this.c.get(i2)).k();
                    boolean l = ((ListItem) List3DActivity.this.c.get(i2)).l();
                    String o = ((ListItem) List3DActivity.this.c.get(i2)).o();
                    String n = ((ListItem) List3DActivity.this.c.get(i2)).n();
                    Boolean r = ((ListItem) List3DActivity.this.c.get(i2)).r();
                    e.a().a(List3DActivity.this.f3895a).a(((ListItem) List3DActivity.this.c.get(i2)).c());
                    if (o == null || TextUtils.isEmpty(o)) {
                        if (!TextUtils.isEmpty(b)) {
                            boolean a2 = c.a(List3DActivity.this.getApplicationContext(), Placement.INAPP_OFFER_WALL);
                            if (!k || a2) {
                                c.a(List3DActivity.this, b, e, List3DActivity.this.a(), l && !a2, false);
                                List3DActivity.this.finish();
                                return;
                            }
                            c.a(List3DActivity.this, b, e, f, List3DActivity.this.a(), com.startapp.android.publish.adsCommon.b.a().A(), com.startapp.android.publish.adsCommon.b.a().B(), l, r, false, new Runnable() {
                                public final void run() {
                                    List3DActivity.this.finish();
                                }
                            });
                        }
                        return;
                    }
                    c.a(o, n, b, (Context) List3DActivity.this, new com.startapp.android.publish.adsCommon.e.b(List3DActivity.this.b));
                    List3DActivity.this.finish();
                }
            });
            RelativeLayout relativeLayout = new RelativeLayout(this);
            relativeLayout.setContentDescription("StartApp Ad");
            relativeLayout.setId(AdsConstants.STARTAPP_AD_MAIN_LAYOUT_ID);
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(1);
            relativeLayout.addView(linearLayout, layoutParams2);
            RelativeLayout relativeLayout2 = new RelativeLayout(this);
            relativeLayout2.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
            relativeLayout2.setBackgroundColor(com.startapp.android.publish.adsCommon.b.a().h().intValue());
            linearLayout.addView(relativeLayout2);
            TextView textView = new TextView(this);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams3.addRule(13);
            textView.setLayoutParams(layoutParams3);
            textView.setPadding(0, i.a((Context) this, 2), 0, i.a((Context) this, 5));
            textView.setTextColor(com.startapp.android.publish.adsCommon.b.a().k().intValue());
            textView.setTextSize((float) com.startapp.android.publish.adsCommon.b.a().j().intValue());
            textView.setSingleLine(true);
            textView.setEllipsize(TruncateAt.END);
            textView.setText(com.startapp.android.publish.adsCommon.b.a().i());
            textView.setShadowLayer(2.5f, -2.0f, 2.0f, -11513776);
            i.a(textView, com.startapp.android.publish.adsCommon.b.a().l());
            relativeLayout2.addView(textView);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams4.addRule(11);
            layoutParams4.addRule(15);
            Bitmap a2 = a.a(this, "close_button.png");
            if (a2 != null) {
                view = new ImageButton(this, null, 16973839);
                ((ImageButton) view).setImageBitmap(Bitmap.createScaledBitmap(a2, i.a((Context) this, 36), i.a((Context) this, 36), true));
            } else {
                view = new TextView(this);
                ((TextView) view).setText("   x   ");
                ((TextView) view).setTextSize(20.0f);
            }
            view.setLayoutParams(layoutParams4);
            view.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    c.b((Context) List3DActivity.this, List3DActivity.this.b(), List3DActivity.this.a());
                    List3DActivity.this.finish();
                }
            });
            view.setContentDescription(AvidJSONUtil.KEY_X);
            view.setId(AdsConstants.LIST_3D_CLOSE_BUTTON_ID);
            relativeLayout2.addView(view);
            View view2 = new View(this);
            view2.setLayoutParams(new LinearLayout.LayoutParams(-1, i.a((Context) this, 2)));
            view2.setBackgroundColor(com.startapp.android.publish.adsCommon.b.a().m().intValue());
            linearLayout.addView(view2);
            LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, 0);
            layoutParams5.weight = 1.0f;
            this.d.setLayoutParams(layoutParams5);
            linearLayout.addView(this.d);
            LinearLayout linearLayout2 = new LinearLayout(this);
            LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams6.gravity = 80;
            linearLayout2.setLayoutParams(layoutParams6);
            linearLayout2.setBackgroundColor(com.startapp.android.publish.adsCommon.b.a().v().intValue());
            linearLayout2.setGravity(17);
            linearLayout.addView(linearLayout2);
            TextView textView2 = new TextView(this);
            textView2.setTextColor(com.startapp.android.publish.adsCommon.b.a().w().intValue());
            textView2.setPadding(0, i.a((Context) this, 2), 0, i.a((Context) this, 3));
            textView2.setText("Powered By ");
            textView2.setTextSize(16.0f);
            linearLayout2.addView(textView2);
            ImageView imageView = new ImageView(this);
            imageView.setImageBitmap(Bitmap.createScaledBitmap(a.a(this, "logo.png"), i.a((Context) this, 56), i.a((Context) this, 12), true));
            linearLayout2.addView(imageView);
            this.h = new b(this, C0087b.LARGE, Placement.INAPP_OFFER_WALL, (com.startapp.android.publish.adsCommon.adinformation.c) getIntent().getSerializableExtra("adInfoOverride"));
            this.h.a(relativeLayout);
            setContentView(relativeLayout, layoutParams);
            new Handler().postDelayed(new Runnable() {
                public final void run() {
                    List3DActivity.this.sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
                }
            }, 500);
        } catch (Throwable th) {
            f.a(this, d.EXCEPTION, "List3DActivity.onCreate", th.getMessage(), "");
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final com.startapp.android.publish.adsCommon.e.b a() {
        this.l = System.currentTimeMillis();
        double d2 = (double) (this.l - this.m);
        Double.isNaN(d2);
        return new com.startapp.android.publish.adsCommon.e.a(String.valueOf(d2 / 1000.0d), this.b);
    }

    /* access modifiers changed from: protected */
    public final String b() {
        if (this.c == null || this.c.isEmpty()) {
            return "";
        }
        return ((ListItem) this.c.get(0)).d() != null ? ((ListItem) this.c.get(0)).d() : "";
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        boolean z = false;
        if (!(this.i == null || this.j == null || System.currentTimeMillis() - this.i.longValue() <= this.j.longValue())) {
            z = true;
        }
        if (z) {
            finish();
            return;
        }
        m.a().a(true);
        this.m = System.currentTimeMillis();
        e.a().a(this.f3895a).c();
    }

    public void onBackPressed() {
        e.a().a(this.f3895a).d();
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        j.a((Activity) this, false);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        e.a().a(this.f3895a).b();
        if (this.h != null && this.h.b()) {
            this.h.e();
        }
        overridePendingTransition(0, 0);
        if (this.k != null && this.k.equals("back")) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.i != null) {
            bundle.putSerializable("lastLoadTime", this.i);
        }
        if (this.j != null) {
            bundle.putSerializable("adCacheTtl", this.j);
        }
    }

    public final void a(int i2) {
        View childAt = this.d.getChildAt(i2 - this.d.getFirstItemPosition());
        if (childAt != null) {
            d dVar = (d) childAt.getTag();
            com.startapp.android.publish.adsCommon.m.a a2 = e.a().a(this.f3895a);
            if (a2 != null && a2.e() != null && i2 < a2.e().size()) {
                ListItem listItem = (ListItem) a2.e().get(i2);
                dVar.b().setImageBitmap(a2.a(i2, listItem.a(), listItem.i()));
                dVar.b().requestLayout();
                dVar.a(listItem.p());
            }
        }
    }

    public void finish() {
        try {
            this.l = System.currentTimeMillis();
            c.b((Context) this, b(), a());
            m.a().a(false);
            if (this.g == getResources().getConfiguration().orientation) {
                com.startapp.common.b.a((Context) this).a(new Intent("com.startapp.android.HideDisplayBroadcastListener"));
            }
            synchronized (this) {
                if (this.n != null) {
                    com.startapp.common.b.a((Context) this).a(this.n);
                    this.n = null;
                }
            }
            if (this.f3895a != null) {
                e.a().a(this.f3895a).d();
                if (!AdsConstants.OVERRIDE_NETWORK.booleanValue()) {
                    e.a().b(this.f3895a);
                }
            }
        } catch (Exception e2) {
            f.a(this, d.EXCEPTION, "List3DActivity.finish", e2.getMessage(), "");
        }
        super.finish();
    }
}
