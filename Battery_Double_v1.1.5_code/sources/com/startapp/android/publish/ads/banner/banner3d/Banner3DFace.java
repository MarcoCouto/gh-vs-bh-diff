package com.startapp.android.publish.ads.banner.banner3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.banner.BannerOptions;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.adsCommon.e.b;
import com.startapp.android.publish.adsCommon.i;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdDetails;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.a;
import com.startapp.common.a.C0093a;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: StartAppSDK */
public class Banner3DFace implements Parcelable, C0093a {
    public static final Creator<Banner3DFace> CREATOR = new Creator<Banner3DFace>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new Banner3DFace[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new Banner3DFace(parcel);
        }
    };

    /* renamed from: a reason: collision with root package name */
    private AdDetails f3883a;
    private Point b;
    private Bitmap c = null;
    private Bitmap d = null;
    private AtomicBoolean e = new AtomicBoolean(false);
    private b f;
    private i g = null;
    private b h = null;

    public int describeContents() {
        return 0;
    }

    public Banner3DFace(Context context, ViewGroup viewGroup, AdDetails adDetails, BannerOptions bannerOptions, b bVar) {
        this.f3883a = adDetails;
        this.f = bVar;
        a(context, bannerOptions, viewGroup);
    }

    public final Bitmap a() {
        return this.d;
    }

    public final void a(Context context, BannerOptions bannerOptions, ViewGroup viewGroup) {
        int a2 = com.startapp.android.publish.adsCommon.a.i.a(context, bannerOptions.e() - 5);
        this.b = new Point((int) (((float) com.startapp.android.publish.adsCommon.a.i.a(context, bannerOptions.d())) * bannerOptions.j()), (int) (((float) com.startapp.android.publish.adsCommon.a.i.a(context, bannerOptions.e())) * bannerOptions.k()));
        this.h = new b(context, new Point(bannerOptions.d(), bannerOptions.e()));
        this.h.setText(this.f3883a.getTitle());
        this.h.setRating(this.f3883a.getRating());
        this.h.setDescription(this.f3883a.getDescription());
        this.h.setButtonText(this.f3883a.isCPE());
        if (this.c != null) {
            this.h.a(this.c, a2, a2);
        } else {
            this.h.a(a2, a2);
            new a(this.f3883a.getImageUrl(), this, 0).a();
            StringBuilder sb = new StringBuilder(" Banner Face Image Async Request: [");
            sb.append(this.f3883a.getTitle());
            sb.append(RequestParameters.RIGHT_BRACKETS);
        }
        LayoutParams layoutParams = new LayoutParams(this.b.x, this.b.y);
        layoutParams.addRule(13);
        viewGroup.addView(this.h, layoutParams);
        this.h.setVisibility(8);
        d();
    }

    private void d() {
        this.d = a((View) this.h);
        if (this.b.x > 0 && this.b.y > 0) {
            this.d = Bitmap.createScaledBitmap(this.d, this.b.x, this.b.y, false);
        }
    }

    private static Bitmap a(View view) {
        view.measure(view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap createBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.draw(canvas);
        return createBitmap;
    }

    public final void a(Bitmap bitmap, int i) {
        if (bitmap != null && this.h != null) {
            this.c = bitmap;
            this.h.setImage(bitmap);
            d();
        }
    }

    public final void b() {
        if (this.g != null) {
            this.g.a(false);
        }
    }

    public Banner3DFace(Parcel parcel) {
        this.f3883a = (AdDetails) parcel.readParcelable(AdDetails.class.getClassLoader());
        this.b = new Point(1, 1);
        this.b.x = parcel.readInt();
        this.b.y = parcel.readInt();
        this.c = (Bitmap) parcel.readParcelable(Bitmap.class.getClassLoader());
        boolean[] zArr = new boolean[1];
        parcel.readBooleanArray(zArr);
        this.e.set(zArr[0]);
        this.f = (b) parcel.readSerializable();
    }

    private static void a(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
        }
    }

    public final i a(Context context) {
        long millis;
        if (this.f3883a.getTrackingUrl() == null || !this.e.compareAndSet(false, true)) {
            return null;
        }
        String[] strArr = {this.f3883a.getTrackingUrl()};
        b bVar = this.f;
        if (this.f3883a.getDelayImpressionInSeconds() != null) {
            millis = TimeUnit.SECONDS.toMillis(this.f3883a.getDelayImpressionInSeconds().longValue());
        } else {
            millis = TimeUnit.SECONDS.toMillis(e.getInstance().getIABDisplayImpressionDelayInSeconds());
        }
        i iVar = new i(context, strArr, bVar, millis);
        this.g = iVar;
        return this.g;
    }

    public final void b(Context context) {
        String intentPackageName = this.f3883a.getIntentPackageName();
        boolean a2 = c.a(context, Placement.INAPP_BANNER);
        if (this.g != null) {
            this.g.a(true);
        }
        if (intentPackageName != null && !"null".equals(intentPackageName) && !TextUtils.isEmpty(intentPackageName)) {
            c.a(intentPackageName, this.f3883a.getIntentDetails(), this.f3883a.getClickUrl(), context, this.f);
        } else if (!this.f3883a.isSmartRedirect() || a2) {
            c.a(context, this.f3883a.getClickUrl(), this.f3883a.getTrackingClickUrl(), this.f, this.f3883a.isStartappBrowserEnabled() && !a2, false);
        } else {
            c.a(context, this.f3883a.getClickUrl(), this.f3883a.getTrackingClickUrl(), this.f3883a.getPackageName(), this.f, com.startapp.android.publish.adsCommon.b.a().A(), com.startapp.android.publish.adsCommon.b.a().B(), this.f3883a.isStartappBrowserEnabled(), this.f3883a.shouldSendRedirectHops());
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f3883a, i);
        parcel.writeInt(this.b.x);
        parcel.writeInt(this.b.y);
        parcel.writeParcelable(this.c, i);
        parcel.writeBooleanArray(new boolean[]{this.e.get()});
        parcel.writeSerializable(this.f);
    }

    public final void c() {
        a(this.c);
        a(this.d);
        this.c = null;
        this.d = null;
        if (this.g != null) {
            this.g.a(false);
        }
        if (this.h != null) {
            this.h.removeAllViews();
            this.h = null;
        }
    }
}
