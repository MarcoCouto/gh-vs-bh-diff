package com.startapp.android.publish.ads.banner;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import com.explorestack.iab.vast.VastError;
import com.startapp.android.publish.ads.banner.bannerstandard.BannerStandard;
import com.startapp.android.publish.common.model.AdPreferences;

/* compiled from: StartAppSDK */
public class Mrec extends BannerStandard {
    /* access modifiers changed from: protected */
    public String getBannerName() {
        return "StartApp Mrec";
    }

    /* access modifiers changed from: protected */
    public int getBannerType() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public int getHeightInDp() {
        return 250;
    }

    /* access modifiers changed from: protected */
    public int getWidthInDp() {
        return VastError.ERROR_CODE_GENERAL_WRAPPER;
    }

    public Mrec(Activity activity) {
        super(activity);
    }

    public Mrec(Activity activity, AdPreferences adPreferences) {
        super(activity, adPreferences);
    }

    public Mrec(Activity activity, BannerListener bannerListener) {
        super(activity, bannerListener);
    }

    public Mrec(Activity activity, AdPreferences adPreferences, BannerListener bannerListener) {
        super(activity, adPreferences, bannerListener);
    }

    public Mrec(Activity activity, AttributeSet attributeSet) {
        super(activity, attributeSet);
    }

    public Mrec(Activity activity, AttributeSet attributeSet, int i) {
        super(activity, attributeSet, i);
    }

    @Deprecated
    public Mrec(Context context) {
        super(context);
    }

    @Deprecated
    public Mrec(Context context, AdPreferences adPreferences) {
        super(context, adPreferences);
    }

    @Deprecated
    public Mrec(Context context, BannerListener bannerListener) {
        super(context, bannerListener);
    }

    @Deprecated
    public Mrec(Context context, AdPreferences adPreferences, BannerListener bannerListener) {
        super(context, adPreferences, bannerListener);
    }

    @Deprecated
    public Mrec(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Deprecated
    public Mrec(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void loadAd(int i, int i2) {
        super.loadAd(getWidthInDp(), getHeightInDp());
    }
}
