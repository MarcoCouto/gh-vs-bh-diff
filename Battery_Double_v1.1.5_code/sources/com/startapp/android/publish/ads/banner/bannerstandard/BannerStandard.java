package com.startapp.android.publish.ads.banner.bannerstandard;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.explorestack.iab.vast.VastError;
import com.startapp.android.publish.ads.banner.BannerBase;
import com.startapp.android.publish.ads.banner.BannerInterface;
import com.startapp.android.publish.ads.banner.BannerListener;
import com.startapp.android.publish.ads.banner.BannerOptions;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.Ad.AdState;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.adinformation.b;
import com.startapp.android.publish.adsCommon.adinformation.b.C0087b;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.h.a.a.C0089a;
import com.startapp.android.publish.adsCommon.h.a.d;
import com.startapp.android.publish.adsCommon.i;
import com.startapp.android.publish.adsCommon.l.a;
import com.startapp.android.publish.adsCommon.m.c;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.html.JsInterface;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class BannerStandard extends BannerBase implements BannerInterface, AdEventListener {
    private static final int ID_WEBVIEW = 159868225;
    private static final String TAG = "BannerHtml";
    protected a adHtml;
    private RelativeLayout adInformationContatiner;
    private b adInformationLayout;
    protected AdPreferences adPreferences;
    private com.b.a.a.a.b.b adSession;
    private boolean callbackSent;
    private boolean clickCallbackSent;
    private CloseableLayout closeableAdContainer;
    private boolean defaultLoad;
    /* access modifiers changed from: private */
    public long htmlRenderTime;
    private final Handler htmlRenderWaitingTimer;
    private boolean initBannerCalled;
    /* access modifiers changed from: private */
    public boolean jsTag;
    protected BannerListener listener;
    private boolean loaded;
    /* access modifiers changed from: private */
    public MraidBannerController mraidController;
    /* access modifiers changed from: private */
    public MraidBannerController mraidTwoPartController;
    private BannerOptions options;
    private ViewGroup rootView;
    private i scheduledImpression;
    /* access modifiers changed from: private */
    public a size$747797a0;
    private c twoPartViewabilityTracker;
    @VisibleForTesting
    public WebView twoPartWebView;
    private c viewabilityTracker;
    private boolean visible;
    @VisibleForTesting
    public WebView webView;
    private RelativeLayout webViewContainer;
    /* access modifiers changed from: private */
    public boolean webViewTouched;

    /* compiled from: StartAppSDK */
    private class MraidBannerController extends com.startapp.android.publish.adsCommon.h.a.a {
        @NonNull
        private WebView activeWebView;
        /* access modifiers changed from: private */
        public com.startapp.android.publish.adsCommon.h.a.c mraidState = com.startapp.android.publish.adsCommon.h.a.c.LOADING;
        private boolean mraidVisibility = false;
        /* access modifiers changed from: private */
        public com.startapp.android.publish.adsCommon.h.b.a nativeFeatureManager;
        private com.startapp.android.publish.adsCommon.h.c.a orientationProperties;
        private com.startapp.android.publish.adsCommon.h.c.b resizeProperties;

        /* compiled from: StartAppSDK */
        class BannerWebViewClient extends d {
            BannerWebViewClient(com.startapp.android.publish.adsCommon.h.a.b bVar) {
                super(bVar);
            }

            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
                if (MraidBannerController.this.mraidState == com.startapp.android.publish.adsCommon.h.a.c.LOADING) {
                    com.b.a.a.a.b.a(String.INLINE, webView);
                    com.b.a.a.a.b.a(BannerStandard.this.getContext(), webView, MraidBannerController.this.nativeFeatureManager);
                    MraidBannerController.this.updateDisplayMetrics(webView);
                    MraidBannerController.this.mraidState = com.startapp.android.publish.adsCommon.h.a.c.DEFAULT;
                    com.b.a.a.a.b.a(MraidBannerController.this.mraidState, webView);
                    com.b.a.a.a.b.a(webView);
                }
                BannerStandard.this.onWebviewPageFinished(webView);
            }
        }

        MraidBannerController(WebView webView, @NonNull C0089a aVar) {
            super(aVar);
            this.activeWebView = webView;
            this.activeWebView.setWebViewClient(new BannerWebViewClient(this));
            this.nativeFeatureManager = new com.startapp.android.publish.adsCommon.h.b.a(BannerStandard.this.getContext());
            this.orientationProperties = new com.startapp.android.publish.adsCommon.h.c.a();
        }

        /* access modifiers changed from: 0000 */
        public com.startapp.android.publish.adsCommon.h.c.b getResizeProperties() {
            return this.resizeProperties;
        }

        public void setResizeProperties(@NonNull Map<String, String> map) {
            boolean z;
            try {
                int parseInt = Integer.parseInt((String) map.get("width"));
                int parseInt2 = Integer.parseInt((String) map.get("height"));
                int parseInt3 = Integer.parseInt((String) map.get("offsetX"));
                int parseInt4 = Integer.parseInt((String) map.get("offsetY"));
                String str = (String) map.get("allowOffscreen");
                String str2 = (String) map.get("customClosePosition");
                if (str != null) {
                    if (!Boolean.parseBoolean(str)) {
                        z = false;
                        com.startapp.android.publish.adsCommon.h.c.b bVar = new com.startapp.android.publish.adsCommon.h.c.b(parseInt, parseInt2, parseInt3, parseInt4, str2, z);
                        this.resizeProperties = bVar;
                    }
                }
                z = true;
                com.startapp.android.publish.adsCommon.h.c.b bVar2 = new com.startapp.android.publish.adsCommon.h.c.b(parseInt, parseInt2, parseInt3, parseInt4, str2, z);
                this.resizeProperties = bVar2;
            } catch (Exception unused) {
                com.b.a.a.a.b.a(this.activeWebView, "wrong format", "setResizeProperties");
            }
        }

        /* access modifiers changed from: 0000 */
        public com.startapp.android.publish.adsCommon.h.a.c getState() {
            return this.mraidState;
        }

        /* access modifiers changed from: 0000 */
        public void setState(com.startapp.android.publish.adsCommon.h.a.c cVar) {
            this.mraidState = cVar;
            com.b.a.a.a.b.a(this.mraidState, this.activeWebView);
        }

        public void close() {
            BannerStandard.this.handleCollapse();
        }

        public void expand(String str) {
            BannerStandard.this.handleExpand(str);
        }

        public void resize() {
            BannerStandard.this.handleResize();
        }

        public void useCustomClose(String str) {
            BannerStandard.this.handleCustomClose(Boolean.parseBoolean(str));
        }

        public void setOrientationProperties(Map<String, String> map) {
            boolean parseBoolean = Boolean.parseBoolean((String) map.get("allowOrientationChange"));
            String str = (String) map.get("forceOrientation");
            if (this.orientationProperties.f4083a != parseBoolean || this.orientationProperties.b != com.startapp.android.publish.adsCommon.h.c.a.a(str)) {
                this.orientationProperties.f4083a = parseBoolean;
                this.orientationProperties.b = com.startapp.android.publish.adsCommon.h.c.a.a(str);
                applyOrientationProperties((Activity) BannerStandard.this.getContext(), this.orientationProperties);
            }
        }

        public void setExpandProperties(Map<String, String> map) {
            String str = (String) map.get("useCustomClose");
            if (str != null) {
                BannerStandard.this.handleCustomClose(Boolean.parseBoolean(str));
            }
        }

        public boolean isFeatureSupported(String str) {
            return this.nativeFeatureManager.a(str);
        }

        /* access modifiers changed from: private */
        public void fireViewableChangeEvent(boolean z) {
            if (this.mraidVisibility != z) {
                this.mraidVisibility = z;
                com.b.a.a.a.b.a(this.activeWebView, this.mraidVisibility);
            }
        }

        /* access modifiers changed from: private */
        public void updateDisplayMetrics(@NonNull WebView webView) {
            Context context = BannerStandard.this.getContext();
            try {
                DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                int i = displayMetrics.widthPixels;
                int i2 = displayMetrics.heightPixels;
                int[] iArr = new int[2];
                BannerStandard.this.getLocationOnScreen(iArr);
                int i3 = iArr[0];
                int i4 = iArr[1];
                com.b.a.a.a.b.a(context, i, i2, webView);
                com.b.a.a.a.b.b(context, i3, i4, BannerStandard.this.size$747797a0.a(), BannerStandard.this.size$747797a0.b(), webView);
                com.b.a.a.a.b.b(context, i, i2, webView);
                com.b.a.a.a.b.a(context, i3, i4, BannerStandard.this.size$747797a0.a(), BannerStandard.this.size$747797a0.b(), webView);
            } catch (Exception e) {
                f.a(context, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "BannerStandard.updateDisplayMetrics", e.getMessage(), "");
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getBannerName() {
        return "StartApp Banner";
    }

    /* access modifiers changed from: protected */
    public int getBannerType() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getHeightInDp() {
        return 50;
    }

    /* access modifiers changed from: protected */
    public int getWidthInDp() {
        return VastError.ERROR_CODE_GENERAL_WRAPPER;
    }

    public BannerStandard(Activity activity) {
        this((Context) activity);
    }

    public BannerStandard(Activity activity, AdPreferences adPreferences2) {
        this((Context) activity, adPreferences2);
    }

    public BannerStandard(Activity activity, BannerListener bannerListener) {
        this((Context) activity, bannerListener);
    }

    public BannerStandard(Activity activity, AdPreferences adPreferences2, BannerListener bannerListener) {
        this((Context) activity, adPreferences2, bannerListener);
    }

    public BannerStandard(Activity activity, boolean z) {
        this((Context) activity, z);
    }

    public BannerStandard(Activity activity, boolean z, AdPreferences adPreferences2) {
        this((Context) activity, z, adPreferences2);
    }

    public BannerStandard(Activity activity, AttributeSet attributeSet) {
        this((Context) activity, attributeSet);
    }

    public BannerStandard(Activity activity, AttributeSet attributeSet, int i) {
        this((Context) activity, attributeSet, i);
    }

    @Deprecated
    public BannerStandard(Context context) {
        this(context, true, (AdPreferences) null);
    }

    @Deprecated
    public BannerStandard(Context context, AdPreferences adPreferences2) {
        this(context, true, adPreferences2);
    }

    @Deprecated
    public BannerStandard(Context context, BannerListener bannerListener) {
        this(context, true, (AdPreferences) null);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public BannerStandard(Context context, AdPreferences adPreferences2, BannerListener bannerListener) {
        this(context, true, adPreferences2);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public BannerStandard(Context context, boolean z) {
        this(context, z, (AdPreferences) null);
    }

    @Deprecated
    public BannerStandard(Context context, boolean z, AdPreferences adPreferences2) {
        super(context);
        this.loaded = false;
        this.webViewTouched = true;
        this.jsTag = false;
        this.defaultLoad = true;
        this.visible = true;
        this.initBannerCalled = false;
        this.htmlRenderWaitingTimer = new Handler(Looper.getMainLooper());
        this.callbackSent = false;
        this.clickCallbackSent = false;
        this.adInformationLayout = null;
        this.adInformationContatiner = null;
        try {
            this.defaultLoad = z;
            this.adPreferences = adPreferences2;
            init();
        } catch (Exception e) {
            f.a(context, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "BannerStandard.constructor - unexpected error occurd", e.getMessage(), "");
        }
    }

    @Deprecated
    public BannerStandard(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Deprecated
    public BannerStandard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.loaded = false;
        this.webViewTouched = true;
        this.jsTag = false;
        this.defaultLoad = true;
        this.visible = true;
        this.initBannerCalled = false;
        this.htmlRenderWaitingTimer = new Handler(Looper.getMainLooper());
        this.callbackSent = false;
        this.clickCallbackSent = false;
        this.adInformationLayout = null;
        this.adInformationContatiner = null;
        try {
            init();
        } catch (Exception e) {
            f.a(context, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "BannerStandard.constructor - unexpected error occurd", e.getMessage(), "");
        }
    }

    private void addAdInformationLayout() {
        if (this.adInformationLayout == null && this.adInformationContatiner == null) {
            this.adInformationContatiner = new RelativeLayout(getContext());
            this.adInformationLayout = new b(getContext(), C0087b.SMALL, Placement.INAPP_BANNER, this.adHtml.getAdInfoOverride());
            this.adInformationLayout.a(this.adInformationContatiner);
        }
        try {
            ViewGroup viewGroup = (ViewGroup) this.adInformationContatiner.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(this.adInformationContatiner);
            }
        } catch (Exception unused) {
        }
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.webView.addView(this.adInformationContatiner, layoutParams);
    }

    public void hideBanner() {
        this.visible = false;
        setVisibility(8);
    }

    public void showBanner() {
        this.visible = true;
        setVisibility(0);
    }

    private void prepareWebView(WebView webView2) {
        webView2.setBackgroundColor(0);
        webView2.setHorizontalScrollBarEnabled(false);
        webView2.getSettings().setJavaScriptEnabled(true);
        webView2.setVerticalScrollBarEnabled(false);
        webView2.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                BannerStandard.this.webViewTouched = true;
                if (motionEvent.getAction() == 2) {
                    return true;
                }
                return false;
            }
        });
        webView2.setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View view) {
                return true;
            }
        });
        webView2.setLongClickable(false);
    }

    /* access modifiers changed from: protected */
    public void initRuntime() {
        try {
            Context context = getContext();
            this.closeableAdContainer = new CloseableLayout(context);
            this.closeableAdContainer.setOnCloseListener(new CloseableLayout.b() {
                public void onClose() {
                    BannerStandard.this.handleCollapse();
                }
            });
            this.webView = new WebView(context);
            this.mraidController = new MraidBannerController(this.webView, new C0089a() {
                public boolean onClickEvent(String str) {
                    if (!BannerStandard.this.jsTag || BannerStandard.this.webViewTouched) {
                        return BannerStandard.this.handleClick(str);
                    }
                    return false;
                }
            });
            this.options = new BannerOptions();
            this.adHtml = new a(context, getOffset());
            if (this.adPreferences == null) {
                this.adPreferences = new AdPreferences();
            }
            this.size$747797a0 = new a(getWidthInDp(), getHeightInDp());
            if (getId() == -1) {
                setId(getBannerId());
            }
            this.webView.setId(ID_WEBVIEW);
            setVisibility(8);
            prepareWebView(this.webView);
            this.options = com.startapp.android.publish.ads.banner.c.a().c();
            getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    com.startapp.common.a.c.a(BannerStandard.this.getViewTreeObserver(), (OnGlobalLayoutListener) this);
                    BannerStandard.this.setHardwareAcceleration(BannerStandard.this.adPreferences);
                    BannerStandard.this.initBanner();
                }
            });
        } catch (Exception e) {
            f.a(getContext(), com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "BannerStandard.init - webview failed", e.getMessage(), "");
            hideBanner();
            onFailedToReceiveBanner("BannerStandard.init - webview failed");
        }
    }

    public void setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        super.setLayoutParams(layoutParams);
        if (layoutParams.width > 0 && layoutParams.height > 0) {
            new Handler().post(new Runnable() {
                public void run() {
                    BannerStandard.this.initBanner();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public View getViewableBanner() {
        if (this.webViewContainer != null) {
            return this.webViewContainer;
        }
        return super.getViewableBanner();
    }

    /* access modifiers changed from: protected */
    public void initBanner() {
        if (!this.initBannerCalled && this.webView != null) {
            this.initBannerCalled = true;
            int a2 = com.startapp.android.publish.adsCommon.a.i.a(getContext(), this.size$747797a0.a());
            int a3 = com.startapp.android.publish.adsCommon.a.i.a(getContext(), this.size$747797a0.b());
            setMinimumWidth(a2);
            setMinimumHeight(a3);
            this.webView.addJavascriptInterface(new JsInterface(getContext(), (Runnable) new Runnable() {
                public void run() {
                }
            }, new com.startapp.android.publish.adsCommon.e.b(getAdTag()), this.adHtml.isInAppBrowserEnabled(0)), "startappwall");
            this.webViewContainer = new RelativeLayout(getContext());
            attachWebViewToContainer(this.webView);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(13);
            addView(this.webViewContainer, layoutParams);
            if (this.loaded || this.isManualLoading) {
                onReceiveAd(this.adHtml);
            } else if (this.defaultLoad) {
                loadBanner();
            }
        }
    }

    private void attachWebViewToContainer(View view) {
        LayoutParams layoutParams = new LayoutParams(com.startapp.android.publish.adsCommon.a.i.a(getContext(), this.size$747797a0.a()), com.startapp.android.publish.adsCommon.a.i.a(getContext(), this.size$747797a0.b()));
        layoutParams.addRule(13);
        this.webViewContainer.addView(view, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void scheduleReloadTask() {
        if (this.scheduledImpression != null && this.scheduledImpression.c()) {
            super.scheduleReloadTask();
        }
    }

    /* access modifiers changed from: protected */
    public void reload() {
        if (this.adSession != null) {
            this.adSession.b();
            this.adSession = null;
        }
        if (this.adPreferences == null) {
            this.adPreferences = new AdPreferences();
        }
        Point availableSize = this.isManualLoading ? this.desirableSizeForManualLoading : getAvailableSize();
        this.adHtml.setSize(availableSize.x, availableSize.y);
        this.adHtml.setState(AdState.UN_INITIALIZED);
        this.adHtml.a(getBannerType());
        this.adHtml.load(this.adPreferences, this);
    }

    private Point getAvailableSize() {
        Point point = new Point();
        if (getLayoutParams() != null && getLayoutParams().width > 0) {
            point.x = com.startapp.android.publish.adsCommon.a.i.b(getContext(), getLayoutParams().width + 1);
        }
        if (getLayoutParams() != null && getLayoutParams().height > 0) {
            point.y = com.startapp.android.publish.adsCommon.a.i.b(getContext(), getLayoutParams().height + 1);
        }
        if (getLayoutParams() != null && getLayoutParams().width > 0 && getLayoutParams().height > 0) {
            this.adHtml.b();
        }
        if (getLayoutParams() == null || getLayoutParams().width <= 0 || getLayoutParams().height <= 0) {
            DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
            try {
                View view = (View) getParent();
                while (view != null && (view.getMeasuredWidth() <= 0 || view.getMeasuredHeight() <= 0)) {
                    if (view.getMeasuredWidth() > 0) {
                        setPointWidthIfNotSet(point, com.startapp.android.publish.adsCommon.a.i.b(getContext(), (view.getMeasuredWidth() - view.getPaddingLeft()) - view.getPaddingRight()));
                    }
                    if (view.getMeasuredHeight() > 0) {
                        setPointHeightIfNotSet(point, com.startapp.android.publish.adsCommon.a.i.b(getContext(), (view.getMeasuredHeight() - view.getPaddingBottom()) - view.getPaddingTop()));
                    }
                    view = (View) view.getParent();
                }
                if (view == null) {
                    determineSizeByScreen(point, displayMetrics);
                } else {
                    setPointWidthIfNotSet(point, com.startapp.android.publish.adsCommon.a.i.b(getContext(), (view.getMeasuredWidth() - view.getPaddingLeft()) - view.getPaddingRight()));
                    setPointHeightIfNotSet(point, com.startapp.android.publish.adsCommon.a.i.b(getContext(), (view.getMeasuredHeight() - view.getPaddingBottom()) - view.getPaddingTop()));
                }
            } catch (Exception e) {
                f.a(getContext(), com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "BannerStandard.getAvailableSize - system service failed", e.getMessage(), "");
                determineSizeByScreen(point, displayMetrics);
            }
        }
        StringBuilder sb = new StringBuilder("============ exit Application Size [");
        sb.append(point.x);
        sb.append(",");
        sb.append(point.y);
        sb.append("] =========");
        return point;
    }

    private void determineSizeByScreen(Point point, DisplayMetrics displayMetrics) {
        setPointWidthIfNotSet(point, com.startapp.android.publish.adsCommon.a.i.b(getContext(), displayMetrics.widthPixels));
        setPointHeightIfNotSet(point, com.startapp.android.publish.adsCommon.a.i.b(getContext(), displayMetrics.heightPixels));
    }

    private void setPointWidthIfNotSet(Point point, int i) {
        if (point.x <= 0) {
            point.x = i;
        }
    }

    private void setPointHeightIfNotSet(Point point, int i) {
        if (point.y <= 0) {
            point.y = i;
        }
    }

    public void onReceiveAd(Ad ad) {
        if (this.isManualLoading) {
            this.isManualLoading = false;
            this.loaded = true;
            if (this.listener != null && !this.callbackSent) {
                this.callbackSent = true;
                this.listener.onReceiveAd(this);
            }
            return;
        }
        this.webViewTouched = false;
        removeView(this.adInformationContatiner);
        if (this.adHtml == null || this.adHtml.getHtml() == null || this.adHtml.getHtml().compareTo("") == 0) {
            onFailedToReceiveBanner("No Banner received");
            return;
        }
        this.jsTag = "true".equals(j.a(this.adHtml.getHtml(), "@jsTag@", "@jsTag@"));
        loadHtml();
        try {
            if (setSize(Integer.parseInt(j.a(this.adHtml.getHtml(), "@width@", "@width@")), Integer.parseInt(j.a(this.adHtml.getHtml(), "@height@", "@height@")))) {
                this.loaded = true;
                addAdInformationLayout();
                makeImpression();
                addDisplayEventOnLoad();
                addVisibilityTracker();
                if (this.listener != null && !this.callbackSent) {
                    this.callbackSent = true;
                    this.listener.onReceiveAd(this);
                }
                if (this.visible) {
                    setVisibility(0);
                }
                return;
            }
            onFailedToReceiveBanner("Banner cannot be displayed (not enough room)");
        } catch (NumberFormatException unused) {
            onFailedToReceiveBanner("Error Casting width & height from HTML");
        } catch (Exception e) {
            new StringBuilder("Unknown error occurred ").append(e.getMessage());
            onFailedToReceiveBanner(e.getMessage());
        }
    }

    private void onFailedToReceiveBanner(String str) {
        setErrorMessage(str);
        this.isManualLoading = false;
        if (this.listener != null && !this.callbackSent) {
            this.callbackSent = true;
            this.listener.onFailedToReceiveAd(this);
        }
    }

    @VisibleForTesting
    public void loadHtml() {
        this.htmlRenderWaitingTimer.postDelayed(new Runnable() {
            public void run() {
                BannerStandard.this.load();
            }
        }, (long) getRefreshRate());
        this.htmlRenderTime = System.currentTimeMillis();
        j.a(getContext(), this.webView, this.adHtml.getHtml());
    }

    private void addVisibilityTracker() {
        if (this.adHtml != null && this.adHtml.isMraidAd()) {
            this.viewabilityTracker = new c(this.webView, getMinViewabilityPercentage(), new c.a() {
                public boolean onUpdate(boolean z) {
                    BannerStandard.this.mraidController.fireViewableChangeEvent(z);
                    return BannerStandard.this.adHtml.isMraidAd();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void makeImpression() {
        i iVar = new i(getContext(), this.adHtml.getTrackingUrls(), new com.startapp.android.publish.adsCommon.e.b(getAdTag()), getImpressionDelayMillis());
        this.scheduledImpression = iVar;
        this.scheduledImpression.a((i.a) new i.a() {
            public void onSent() {
                BannerStandard.this.htmlRenderTime = System.currentTimeMillis() - BannerStandard.this.htmlRenderTime;
                BannerStandard.this.scheduleReloadTask();
            }
        });
        startVisibilityRunnable(this.scheduledImpression);
    }

    private long getImpressionDelayMillis() {
        if (this.adHtml.getDelayImpressionInSeconds() != null) {
            return TimeUnit.SECONDS.toMillis(this.adHtml.getDelayImpressionInSeconds().longValue());
        }
        return TimeUnit.SECONDS.toMillis(e.getInstance().getIABDisplayImpressionDelayInSeconds());
    }

    private boolean setSize(int i, int i2) {
        Point availableSize = getAvailableSize();
        if (availableSize.x < i || availableSize.y < i2) {
            Point point = new Point(0, 0);
            ViewGroup.LayoutParams layoutParams = this.webView.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new LayoutParams(point.x, point.y);
            } else {
                layoutParams.width = point.x;
                layoutParams.height = point.y;
            }
            this.webView.setLayoutParams(layoutParams);
            return false;
        }
        this.size$747797a0.a(i, i2);
        int a2 = com.startapp.android.publish.adsCommon.a.i.a(getContext(), this.size$747797a0.a());
        int a3 = com.startapp.android.publish.adsCommon.a.i.a(getContext(), this.size$747797a0.b());
        setMinimumWidth(a2);
        setMinimumHeight(a3);
        ViewGroup.LayoutParams layoutParams2 = this.webView.getLayoutParams();
        if (layoutParams2 == null) {
            layoutParams2 = new LayoutParams(a2, a3);
        } else {
            layoutParams2.width = a2;
            layoutParams2.height = a3;
        }
        this.webView.setLayoutParams(layoutParams2);
        return true;
    }

    public void onFailedToReceiveAd(Ad ad) {
        onFailedToReceiveBanner(ad.getErrorMessage());
    }

    /* access modifiers changed from: protected */
    public void onWebviewPageFinished(@NonNull WebView webView2) {
        cancelHtmlWaitingTimer();
        if (e.getInstance().isOmsdkEnabled()) {
            this.adSession = com.b.a.a.a.b.b(webView2);
            if (this.adSession != null) {
                if (this.adInformationContatiner != null) {
                    this.adSession.b(this.adInformationContatiner);
                }
                if (this.closeableAdContainer != null) {
                    this.adSession.b(this.closeableAdContainer);
                }
                this.adSession.a(webView2);
                this.adSession.a();
                com.b.a.a.a.b.a.a(this.adSession).a();
            }
        }
    }

    private void onResume() {
        if (this.webView != null) {
            com.startapp.common.a.c.b(this.webView);
        }
        if (this.twoPartWebView != null) {
            com.startapp.common.a.c.b(this.twoPartWebView);
        }
    }

    private void onPause() {
        if (this.webView != null) {
            com.startapp.common.a.c.a(this.webView);
        }
        if (this.twoPartWebView != null) {
            com.startapp.common.a.c.a(this.twoPartWebView);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        onResume();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        onPause();
        cancelScheduledImpression(false);
        cancelViewabilityTracking();
        cancelHtmlWaitingTimer();
        if (this.adSession != null) {
            this.adSession.b();
            this.adSession = null;
            j.a((Object) this.webView);
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            onResume();
        } else {
            onPause();
        }
    }

    public void setBannerListener(BannerListener bannerListener) {
        this.listener = bannerListener;
    }

    /* access modifiers changed from: protected */
    public int getRefreshRate() {
        return this.options.i();
    }

    /* access modifiers changed from: protected */
    public int getAdjustedRefreshRate() {
        return Math.max(getRefreshRate() - ((int) this.htmlRenderTime), 0);
    }

    /* access modifiers changed from: protected */
    public int getOffset() {
        if (this.adHtml == null) {
            return 0;
        }
        return this.adHtml.a();
    }

    /* access modifiers changed from: protected */
    public int getBannerId() {
        return this.innerBannerStandardId;
    }

    /* access modifiers changed from: protected */
    public void setBannerId(int i) {
        this.innerBannerStandardId = i;
    }

    public void setAdTag(String str) {
        this.adTag = str;
    }

    /* access modifiers changed from: protected */
    public void cancelScheduledImpression(boolean z) {
        if (this.scheduledImpression != null) {
            this.scheduledImpression.a(z);
        }
    }

    private void cancelViewabilityTracking() {
        if (this.viewabilityTracker != null) {
            this.viewabilityTracker.a();
        }
        if (this.twoPartViewabilityTracker != null) {
            this.twoPartViewabilityTracker.a();
        }
    }

    private void cancelHtmlWaitingTimer() {
        this.htmlRenderWaitingTimer.removeCallbacksAndMessages(null);
    }

    /* access modifiers changed from: private */
    public boolean handleClick(String str) {
        if (!this.clickCallbackSent) {
            this.clickCallbackSent = true;
            if (this.listener != null) {
                this.listener.onClick(this);
            }
        }
        cancelScheduledImpression(true);
        cancelViewabilityTracking();
        cancelHtmlWaitingTimer();
        boolean a2 = com.startapp.android.publish.adsCommon.c.a(getContext(), Placement.INAPP_BANNER);
        if (!this.jsTag) {
            if (str.contains("index=")) {
                try {
                    int a3 = com.startapp.android.publish.adsCommon.c.a(str);
                    String str2 = null;
                    if (!this.adHtml.getSmartRedirect(a3) || a2) {
                        Context context = getContext();
                        if (a3 < this.adHtml.getTrackingClickUrls().length) {
                            str2 = this.adHtml.getTrackingClickUrls()[a3];
                        }
                        com.startapp.android.publish.adsCommon.c.a(context, str, str2, new com.startapp.android.publish.adsCommon.e.b(getAdTag()), this.adHtml.isInAppBrowserEnabled(a3) && !a2, false);
                        this.webView.stopLoading();
                        setClicked(true);
                        return true;
                    }
                    Context context2 = getContext();
                    String str3 = a3 < this.adHtml.getTrackingClickUrls().length ? this.adHtml.getTrackingClickUrls()[a3] : null;
                    if (a3 < this.adHtml.getPackageNames().length) {
                        str2 = this.adHtml.getPackageNames()[a3];
                    }
                    com.startapp.android.publish.adsCommon.c.a(context2, str, str3, str2, new com.startapp.android.publish.adsCommon.e.b(getAdTag()), com.startapp.android.publish.adsCommon.b.a().A(), com.startapp.android.publish.adsCommon.b.a().B(), this.adHtml.isInAppBrowserEnabled(a3), this.adHtml.shouldSendRedirectHops(a3));
                    this.webView.stopLoading();
                    setClicked(true);
                    return true;
                } catch (Exception unused) {
                    return false;
                }
            }
        } else {
            String str4 = str;
        }
        if (!this.adHtml.getSmartRedirect(0) || a2) {
            com.startapp.android.publish.adsCommon.c.a(getContext(), str, this.adHtml.getTrackingClickUrls()[0], new com.startapp.android.publish.adsCommon.e.b(getAdTag()), this.adHtml.isInAppBrowserEnabled(0) && !a2, false);
            this.webView.stopLoading();
            setClicked(true);
            return true;
        }
        com.startapp.android.publish.adsCommon.c.a(getContext(), str, this.adHtml.getTrackingClickUrls()[0], this.adHtml.getPackageNames()[0], new com.startapp.android.publish.adsCommon.e.b(getAdTag()), com.startapp.android.publish.adsCommon.b.a().A(), com.startapp.android.publish.adsCommon.b.a().B(), this.adHtml.isInAppBrowserEnabled(0), this.adHtml.shouldSendRedirectHops(0));
        this.webView.stopLoading();
        setClicked(true);
        return true;
    }

    /* access modifiers changed from: private */
    public void handleCustomClose(boolean z) {
        if (z != (!this.closeableAdContainer.a())) {
            this.closeableAdContainer.setCloseVisible(!z);
        }
    }

    /* access modifiers changed from: private */
    public void handleCollapse() {
        if (this.mraidController.getState() != com.startapp.android.publish.adsCommon.h.a.c.LOADING && this.mraidController.getState() != com.startapp.android.publish.adsCommon.h.a.c.HIDDEN) {
            if (this.mraidController.getState() == com.startapp.android.publish.adsCommon.h.a.c.RESIZED || this.mraidController.getState() == com.startapp.android.publish.adsCommon.h.a.c.EXPANDED) {
                if (this.mraidTwoPartController != null) {
                    detachTwoPartMraidController();
                } else {
                    this.closeableAdContainer.removeView(this.webView);
                    attachWebViewToContainer(this.webView);
                    this.webViewContainer.setVisibility(0);
                }
                CloseableLayout closeableLayout = this.closeableAdContainer;
                if (!(closeableLayout == null || closeableLayout.getParent() == null || !(closeableLayout.getParent() instanceof ViewGroup))) {
                    ((ViewGroup) closeableLayout.getParent()).removeView(closeableLayout);
                }
                this.mraidController.setState(com.startapp.android.publish.adsCommon.h.a.c.DEFAULT);
            } else if (this.mraidController.getState() == com.startapp.android.publish.adsCommon.h.a.c.DEFAULT) {
                this.webViewContainer.setVisibility(4);
                this.mraidController.setState(com.startapp.android.publish.adsCommon.h.a.c.HIDDEN);
            }
            scheduleReloadTask();
        }
    }

    private void setupTwoPartMraidController(String str) {
        this.webViewTouched = false;
        if (this.twoPartWebView == null) {
            this.twoPartWebView = new WebView(getContext());
        }
        this.mraidTwoPartController = new MraidBannerController(this.twoPartWebView, new C0089a() {
            public boolean onClickEvent(String str) {
                if (!BannerStandard.this.jsTag || BannerStandard.this.webViewTouched) {
                    return BannerStandard.this.handleClick(str);
                }
                return false;
            }
        });
        this.twoPartViewabilityTracker = new c(this.twoPartWebView, getMinViewabilityPercentage(), new c.a() {
            public boolean onUpdate(boolean z) {
                BannerStandard.this.mraidController.fireViewableChangeEvent(z);
                BannerStandard.this.mraidTwoPartController.fireViewableChangeEvent(z);
                return BannerStandard.this.adHtml.isMraidAd();
            }
        });
        this.twoPartWebView.setId(159868226);
        prepareWebView(this.twoPartWebView);
        this.twoPartWebView.loadUrl(str);
    }

    private void detachTwoPartMraidController() {
        this.closeableAdContainer.removeView(this.twoPartWebView);
        this.twoPartViewabilityTracker.a();
        this.twoPartViewabilityTracker = null;
        this.mraidTwoPartController = null;
        this.twoPartWebView.stopLoading();
        this.twoPartWebView = null;
    }

    /* access modifiers changed from: private */
    public void handleExpand(@Nullable String str) {
        cancelReloadTask();
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        boolean z = str != null && !TextUtils.isEmpty(str);
        if (z) {
            setupTwoPartMraidController(str);
        }
        if (this.mraidController.getState() == com.startapp.android.publish.adsCommon.h.a.c.DEFAULT) {
            if (z) {
                this.closeableAdContainer.addView(this.twoPartWebView, layoutParams);
            } else {
                this.webViewContainer.removeView(this.webView);
                this.webViewContainer.setVisibility(4);
                this.closeableAdContainer.addView(this.webView, layoutParams);
            }
            getAndMemoizeRootView().addView(this.closeableAdContainer, new FrameLayout.LayoutParams(-1, -1));
        } else if (this.mraidController.getState() == com.startapp.android.publish.adsCommon.h.a.c.RESIZED && z) {
            this.closeableAdContainer.removeView(this.webView);
            this.webViewContainer.addView(this.webView, layoutParams);
            this.webViewContainer.setVisibility(4);
            this.closeableAdContainer.addView(this.twoPartWebView, layoutParams);
        }
        this.closeableAdContainer.setLayoutParams(layoutParams);
        this.mraidController.setState(com.startapp.android.publish.adsCommon.h.a.c.EXPANDED);
    }

    /* access modifiers changed from: 0000 */
    public int clampInt(int i, int i2, int i3) {
        return Math.max(i, Math.min(i2, i3));
    }

    /* access modifiers changed from: private */
    public void handleResize() {
        com.startapp.android.publish.adsCommon.h.c.b resizeProperties = this.mraidController.getResizeProperties();
        if (resizeProperties == null) {
            com.b.a.a.a.b.a(this.webView, "requires: setResizeProperties first", "resize");
            return;
        }
        cancelReloadTask();
        if (this.mraidController.getState() != com.startapp.android.publish.adsCommon.h.a.c.LOADING && this.mraidController.getState() != com.startapp.android.publish.adsCommon.h.a.c.HIDDEN) {
            if (this.mraidController.getState() == com.startapp.android.publish.adsCommon.h.a.c.EXPANDED) {
                com.b.a.a.a.b.a(this.webView, "Not allowed to resize from an already expanded ad", "resize");
                return;
            }
            int i = resizeProperties.f4084a;
            int i2 = resizeProperties.b;
            int i3 = resizeProperties.c;
            int i4 = resizeProperties.d;
            int[] iArr = new int[2];
            this.webView.getLocationOnScreen(iArr);
            Context context = getContext();
            int b = com.startapp.android.publish.adsCommon.a.i.b(context, iArr[0]) + i3;
            int b2 = com.startapp.android.publish.adsCommon.a.i.b(context, iArr[1]) + i4;
            Rect rect = new Rect(b, b2, i + b, i2 + b2);
            ViewGroup topmostView = getTopmostView();
            int b3 = com.startapp.android.publish.adsCommon.a.i.b(context, topmostView.getWidth());
            int b4 = com.startapp.android.publish.adsCommon.a.i.b(context, topmostView.getHeight());
            int[] iArr2 = new int[2];
            topmostView.getLocationOnScreen(iArr2);
            int b5 = com.startapp.android.publish.adsCommon.a.i.b(context, iArr2[0]);
            int b6 = com.startapp.android.publish.adsCommon.a.i.b(context, iArr2[1]);
            if (!resizeProperties.f) {
                if (rect.width() > b3 || rect.height() > b4) {
                    com.b.a.a.a.b.a(this.webView, "Not enough room for the ad", "resize");
                    return;
                }
                rect.offsetTo(clampInt(b5, rect.left, (b5 + b3) - rect.width()), clampInt(b6, rect.top, (b6 + b4) - rect.height()));
            }
            Rect rect2 = new Rect();
            try {
                CloseableLayout.a a2 = CloseableLayout.a.a(resizeProperties.e, CloseableLayout.a.TOP_RIGHT);
                this.closeableAdContainer.a(a2, rect, rect2);
                if (!new Rect(b5, b6, b3 + b5, b4 + b6).contains(rect2)) {
                    com.b.a.a.a.b.a(this.webView, "The close region to appear within the max allowed size", "resize");
                } else if (!rect.contains(rect2)) {
                    com.b.a.a.a.b.a(this.webView, "The close region to appear within the max allowed size", "resize");
                } else {
                    this.closeableAdContainer.setCloseVisible(false);
                    this.closeableAdContainer.setClosePosition(a2);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(rect.width(), rect.height());
                    layoutParams.leftMargin = rect.left - b5;
                    layoutParams.topMargin = rect.top - b6;
                    if (this.mraidController.getState() == com.startapp.android.publish.adsCommon.h.a.c.DEFAULT) {
                        this.webViewContainer.removeView(this.webView);
                        this.webViewContainer.setVisibility(4);
                        this.closeableAdContainer.addView(this.webView, new FrameLayout.LayoutParams(-1, -1));
                        getAndMemoizeRootView().addView(this.closeableAdContainer, layoutParams);
                    } else if (this.mraidController.getState() == com.startapp.android.publish.adsCommon.h.a.c.RESIZED) {
                        this.closeableAdContainer.setLayoutParams(layoutParams);
                    }
                    this.closeableAdContainer.setClosePosition(a2);
                    this.mraidController.setState(com.startapp.android.publish.adsCommon.h.a.c.RESIZED);
                }
            } catch (Exception e) {
                com.b.a.a.a.b.a(this.webView, e.getMessage(), "resize");
            }
        }
    }

    private ViewGroup getTopmostView() {
        View view;
        if (this.rootView != null) {
            return this.rootView;
        }
        Context context = getContext();
        RelativeLayout relativeLayout = this.webViewContainer;
        View view2 = null;
        if (!(context instanceof Activity)) {
            view = null;
        } else {
            view = ((Activity) context).getWindow().getDecorView().findViewById(16908290);
        }
        if (relativeLayout != null) {
            View rootView2 = relativeLayout.getRootView();
            if (rootView2 != null) {
                view2 = rootView2.findViewById(16908290);
                if (view2 == null) {
                    view2 = rootView2;
                }
            }
        }
        if (view == null) {
            view = view2;
        }
        return view instanceof ViewGroup ? (ViewGroup) view : this.webViewContainer;
    }

    private ViewGroup getAndMemoizeRootView() {
        if (this.rootView == null) {
            this.rootView = getTopmostView();
        }
        return this.rootView;
    }
}
