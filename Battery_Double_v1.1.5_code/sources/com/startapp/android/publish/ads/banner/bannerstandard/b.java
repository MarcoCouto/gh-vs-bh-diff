package com.startapp.android.publish.ads.banner.bannerstandard;

import android.content.Context;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.banner.c;
import com.startapp.android.publish.adsCommon.HtmlAd;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.common.model.GetAdRequest;
import com.startapp.android.publish.html.a;

/* compiled from: StartAppSDK */
public final class b extends a {
    private int i = 0;

    public b(Context context, HtmlAd htmlAd, int i2, AdPreferences adPreferences, AdEventListener adEventListener) {
        super(context, htmlAd, adPreferences, adEventListener, Placement.INAPP_BANNER, false);
        this.i = i2;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        a aVar = (a) this.b;
        com.startapp.android.publish.ads.banner.a aVar2 = new com.startapp.android.publish.ads.banner.a();
        b((GetAdRequest) aVar2);
        aVar2.setWidth(aVar.getWidth());
        aVar2.setHeight(aVar.getHeight());
        aVar2.setOffset(this.i);
        aVar2.setAdsNumber(c.a().b().g());
        aVar2.a(aVar.c());
        aVar2.a(aVar.d());
        return aVar2;
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        a(bool.booleanValue());
        StringBuilder sb = new StringBuilder("Html onPostExecute, result=[");
        sb.append(bool);
        sb.append(RequestParameters.RIGHT_BRACKETS);
    }
}
