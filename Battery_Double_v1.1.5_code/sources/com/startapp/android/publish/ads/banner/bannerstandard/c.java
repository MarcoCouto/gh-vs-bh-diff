package com.startapp.android.publish.ads.banner.bannerstandard;

import com.b.a.a.a.b;
import com.b.a.a.a.b.d;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class c {

    /* renamed from: a reason: collision with root package name */
    private final d f3891a;
    private final d b;
    private final boolean c;

    private c(d dVar, d dVar2) {
        this.f3891a = dVar;
        if (dVar2 == null) {
            this.b = d.NONE;
        } else {
            this.b = dVar2;
        }
        this.c = false;
    }

    public static c a(d dVar, d dVar2) {
        b.a((Object) dVar, "Impression owner is null");
        if (!dVar.equals(d.NONE)) {
            return new c(dVar, dVar2);
        }
        throw new IllegalArgumentException("Impression owner is none");
    }

    public final boolean a() {
        return d.NATIVE == this.f3891a;
    }

    public final boolean b() {
        return d.NATIVE == this.b;
    }

    public final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        com.b.a.a.a.e.b.a(jSONObject, "impressionOwner", this.f3891a);
        com.b.a.a.a.e.b.a(jSONObject, "videoEventsOwner", this.b);
        com.b.a.a.a.e.b.a(jSONObject, "isolateVerificationScripts", Boolean.FALSE);
        return jSONObject;
    }
}
