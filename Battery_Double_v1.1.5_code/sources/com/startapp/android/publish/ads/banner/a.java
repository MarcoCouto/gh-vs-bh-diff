package com.startapp.android.publish.ads.banner;

import com.startapp.android.publish.adsCommon.a.d;
import com.startapp.android.publish.adsCommon.a.f;
import com.startapp.android.publish.common.model.GetAdRequest;

/* compiled from: StartAppSDK */
public final class a extends GetAdRequest {

    /* renamed from: a reason: collision with root package name */
    private boolean f3881a;
    private int b;

    public final void a(boolean z) {
        this.f3881a = z;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final f getNameValueMap() {
        f nameValueMap = super.getNameValueMap();
        if (nameValueMap == null) {
            nameValueMap = new d();
        }
        nameValueMap.a("fixedSize", Boolean.valueOf(this.f3881a), false);
        nameValueMap.a("bnrt", Integer.valueOf(this.b), false);
        return nameValueMap;
    }
}
