package com.startapp.android.publish.ads.banner;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.b.f;
import com.startapp.android.publish.adsCommon.b.g;
import com.startapp.android.publish.adsCommon.m.b;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.Constants;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledFuture;

/* compiled from: StartAppSDK */
public abstract class BannerBase extends RelativeLayout {
    private static final String TAG = "BannerLayout";
    protected AdPreferences adPreferences;
    protected f adRulesResult;
    protected String adTag;
    private boolean attachedToWindow;
    private boolean clicked;
    protected Point desirableSizeForManualLoading;
    protected boolean drawn;
    private String error;
    private boolean firstLoad;
    protected int innerBanner3dId;
    protected int innerBannerStandardId;
    protected boolean isManualLoading;
    private ScheduledFuture<?> notVisibleReloadFuture;
    protected int offset;
    private boolean shouldReloadBanner;
    private a task;
    private Timer timer;
    protected b viewabilityRunner;

    /* compiled from: StartAppSDK */
    class a extends TimerTask {
        a() {
        }

        public final void run() {
            BannerBase.this.post(new Runnable() {
                public final void run() {
                    if (BannerBase.this.isShown() || (BannerBase.this.adRulesResult != null && !BannerBase.this.adRulesResult.a())) {
                        BannerBase.this.load();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public abstract int getBannerId();

    /* access modifiers changed from: protected */
    public abstract String getBannerName();

    /* access modifiers changed from: protected */
    public abstract int getHeightInDp();

    /* access modifiers changed from: protected */
    public abstract int getOffset();

    /* access modifiers changed from: protected */
    public abstract int getRefreshRate();

    /* access modifiers changed from: protected */
    public View getViewableBanner() {
        return this;
    }

    /* access modifiers changed from: protected */
    public abstract int getWidthInDp();

    /* access modifiers changed from: protected */
    public abstract void initRuntime();

    /* access modifiers changed from: protected */
    public abstract void reload();

    public abstract void setAdTag(String str);

    /* access modifiers changed from: protected */
    public abstract void setBannerId(int i);

    public BannerBase(Context context) {
        super(context);
        this.attachedToWindow = false;
        this.offset = 0;
        this.firstLoad = true;
        this.drawn = false;
        this.innerBanner3dId = new Random().nextInt(DefaultOggSeeker.MATCH_BYTE_RANGE) + 159868227;
        this.innerBannerStandardId = this.innerBanner3dId + 1;
        this.adTag = null;
        this.clicked = false;
        this.shouldReloadBanner = false;
        this.notVisibleReloadFuture = null;
        this.timer = new Timer();
        this.task = new a();
    }

    public BannerBase(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BannerBase(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.attachedToWindow = false;
        this.offset = 0;
        this.firstLoad = true;
        this.drawn = false;
        this.innerBanner3dId = new Random().nextInt(DefaultOggSeeker.MATCH_BYTE_RANGE) + 159868227;
        this.innerBannerStandardId = this.innerBanner3dId + 1;
        this.adTag = null;
        this.clicked = false;
        this.shouldReloadBanner = false;
        this.notVisibleReloadFuture = null;
        this.timer = new Timer();
        this.task = new a();
        setBannerAttrs(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void init() {
        if (!isInEditMode()) {
            initRuntime();
        } else {
            initDebug();
        }
    }

    private void initDebug() {
        setMinimumWidth(i.a(getContext(), getWidthInDp()));
        setMinimumHeight(i.a(getContext(), getHeightInDp()));
        setBackgroundColor(Color.rgb(169, 169, 169));
        TextView textView = new TextView(getContext());
        textView.setText(getBannerName());
        textView.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        addView(textView, layoutParams);
    }

    /* access modifiers changed from: protected */
    public int getAdjustedRefreshRate() {
        return getRefreshRate();
    }

    /* access modifiers changed from: protected */
    public String getAdTag() {
        return this.adTag;
    }

    public void loadAd(int i, int i2) {
        if (getParent() == null) {
            this.isManualLoading = true;
            this.desirableSizeForManualLoading = new Point(i, i2);
            loadBanner();
        }
    }

    public void loadAd() {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        loadAd(i.b(getContext(), displayMetrics.widthPixels), i.b(getContext(), displayMetrics.heightPixels));
    }

    /* access modifiers changed from: protected */
    public void loadBanner() {
        scheduleReloadTask();
        load();
    }

    private void rescheduleNotVisibleReload() {
        if (this.notVisibleReloadFuture != null) {
            this.notVisibleReloadFuture.cancel(false);
        }
        this.notVisibleReloadFuture = com.startapp.common.f.a((Runnable) new Runnable() {
            public final void run() {
                BannerBase.this.loadBanner();
            }
        }, (long) (e.getInstance().getNotVisibleBannerReloadInterval() * 1000));
    }

    /* access modifiers changed from: protected */
    public void load() {
        clearVisibilityHandler();
        if (this.adRulesResult == null || g.a().b().a()) {
            this.adRulesResult = g.a().b().a(Placement.INAPP_BANNER, getAdTag());
            if (this.adRulesResult.a()) {
                reload();
                return;
            }
            setVisibility(4);
            Constants.a();
            return;
        }
        if (this.adRulesResult.a()) {
            reload();
        }
    }

    private void clearVisibilityHandler() {
        if (this.viewabilityRunner != null) {
            this.viewabilityRunner.b();
            this.viewabilityRunner = null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldSendImpression(com.startapp.android.publish.adsCommon.i iVar) {
        return iVar != null && !iVar.c();
    }

    /* access modifiers changed from: protected */
    public int getMinViewabilityPercentage() {
        return c.a().b().q();
    }

    /* access modifiers changed from: protected */
    public void startVisibilityRunnable(com.startapp.android.publish.adsCommon.i iVar) {
        if (this.viewabilityRunner == null) {
            this.viewabilityRunner = new b(getViewableBanner(), iVar, getMinViewabilityPercentage());
            this.viewabilityRunner.a();
        }
    }

    private void setBannerAttrs(Context context, AttributeSet attributeSet) {
        setAdTag(new b(context, attributeSet).a());
    }

    /* access modifiers changed from: protected */
    public void scheduleReloadTask() {
        if (this.attachedToWindow && !isInEditMode()) {
            if (this.task != null) {
                this.task.cancel();
            }
            if (this.timer != null) {
                this.timer.cancel();
            }
            this.task = new a();
            this.timer = new Timer();
            this.timer.schedule(this.task, (long) getAdjustedRefreshRate());
            rescheduleNotVisibleReload();
        }
    }

    /* access modifiers changed from: protected */
    public void cancelReloadTask() {
        if (!isInEditMode()) {
            if (this.task != null) {
                this.task.cancel();
            }
            if (this.timer != null) {
                this.timer.cancel();
            }
            if (this.notVisibleReloadFuture != null) {
                this.notVisibleReloadFuture.cancel(false);
            }
            this.task = null;
            this.timer = null;
            this.notVisibleReloadFuture = null;
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        if (isClicked()) {
            setClicked(false);
            this.shouldReloadBanner = true;
        }
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putInt(RequestParameters.BANNER_ID, getBannerId());
        bundle.putParcelable("upperState", onSaveInstanceState);
        bundle.putSerializable("adRulesResult", this.adRulesResult);
        bundle.putSerializable("adPreferences", this.adPreferences);
        bundle.putInt("offset", this.offset);
        bundle.putBoolean("firstLoad", this.firstLoad);
        bundle.putBoolean("shouldReloadBanner", this.shouldReloadBanner);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof Bundle)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        Bundle bundle = (Bundle) parcelable;
        setBannerId(bundle.getInt(RequestParameters.BANNER_ID));
        this.adRulesResult = (f) bundle.getSerializable("adRulesResult");
        this.adPreferences = (AdPreferences) bundle.getSerializable("adPreferences");
        this.offset = bundle.getInt("offset");
        this.firstLoad = bundle.getBoolean("firstLoad");
        this.shouldReloadBanner = bundle.getBoolean("shouldReloadBanner");
        super.onRestoreInstanceState(bundle.getParcelable("upperState"));
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.attachedToWindow = true;
        scheduleReloadTask();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.attachedToWindow = false;
        cancelReloadTask();
        clearVisibilityHandler();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            if (this.shouldReloadBanner) {
                this.shouldReloadBanner = false;
                load();
            }
            this.attachedToWindow = true;
            scheduleReloadTask();
            return;
        }
        this.attachedToWindow = false;
        cancelReloadTask();
    }

    public boolean isFirstLoad() {
        return this.firstLoad;
    }

    public void setFirstLoad(boolean z) {
        this.firstLoad = z;
    }

    /* access modifiers changed from: protected */
    public void addDisplayEventOnLoad() {
        if (isFirstLoad() || g.a().b().a()) {
            setFirstLoad(false);
            com.startapp.android.publish.adsCommon.b.b.a().a(new com.startapp.android.publish.adsCommon.b.a(Placement.INAPP_BANNER, getAdTag()));
        }
    }

    /* access modifiers changed from: protected */
    public void setHardwareAcceleration(AdPreferences adPreferences2) {
        boolean z;
        String str = "hardwareAccelerated";
        boolean z2 = this.attachedToWindow;
        if (VERSION.SDK_INT < 11 || 1 == getLayerType() || !z2) {
            z = false;
        } else {
            z = isHardwareAccelerated();
        }
        j.a(adPreferences2, str, z);
    }

    public boolean isClicked() {
        return this.clicked;
    }

    public void setClicked(boolean z) {
        this.clicked = z;
    }

    public void setErrorMessage(String str) {
        this.error = str;
    }

    public String getErrorMessage() {
        return this.error;
    }
}
