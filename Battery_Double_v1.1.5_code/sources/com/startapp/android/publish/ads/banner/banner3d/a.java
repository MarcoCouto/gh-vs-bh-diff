package com.startapp.android.publish.ads.banner.banner3d;

import android.content.Context;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.h;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class a extends h {
    private static final long serialVersionUID = 1;
    private boolean fixedSize;
    private int offset;

    public a(Context context, int i) {
        super(context, Placement.INAPP_BANNER);
        this.offset = i;
    }

    /* access modifiers changed from: protected */
    public final void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
        c cVar = new c(this.context, this, this.offset, adPreferences, adEventListener);
        cVar.c();
        this.offset++;
    }

    public final int a() {
        return this.offset;
    }

    public final void b() {
        this.fixedSize = true;
    }

    public final boolean c() {
        return this.fixedSize;
    }
}
