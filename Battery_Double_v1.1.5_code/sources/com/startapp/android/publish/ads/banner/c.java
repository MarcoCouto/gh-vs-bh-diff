package com.startapp.android.publish.ads.banner;

import android.content.Context;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.common.c.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class c implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private static Object f3892a = new Object();
    private static volatile c b = new c();
    private static final long serialVersionUID = 1;
    @e(a = true)
    private BannerOptions BannerOptions = new BannerOptions();
    private String bannerMetadataUpdateVersion = AdsConstants.c;

    public static c a() {
        return b;
    }

    public final BannerOptions b() {
        return this.BannerOptions;
    }

    public final BannerOptions c() {
        return new BannerOptions(this.BannerOptions);
    }

    public static void a(Context context, c cVar) {
        synchronized (f3892a) {
            cVar.bannerMetadataUpdateVersion = AdsConstants.c;
            b = cVar;
            com.startapp.common.a.e.a(context, "StartappBannerMetadata", (Serializable) cVar);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        return j.b(this.BannerOptions, cVar.BannerOptions) && j.b(this.bannerMetadataUpdateVersion, cVar.bannerMetadataUpdateVersion);
    }

    public int hashCode() {
        return j.a(this.BannerOptions, this.bannerMetadataUpdateVersion);
    }

    public static void a(Context context) {
        c cVar = (c) com.startapp.common.a.e.a(context, "StartappBannerMetadata");
        c cVar2 = new c();
        if (cVar != null) {
            boolean a2 = j.a(cVar, cVar2);
            if (!(!AdsConstants.c.equals(cVar.bannerMetadataUpdateVersion)) && a2) {
                f.a(context, d.METADATA_NULL, "BannerMetaData", "", "");
            }
            b = cVar;
            return;
        }
        b = cVar2;
    }
}
