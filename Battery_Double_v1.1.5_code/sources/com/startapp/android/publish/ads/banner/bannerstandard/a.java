package com.startapp.android.publish.ads.banner.bannerstandard;

import android.content.Context;
import com.startapp.android.publish.adsCommon.HtmlAd;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class a extends HtmlAd {
    private static final long serialVersionUID = 1;
    private int bannerType;
    private boolean fixedSize;
    private int offset = 0;

    public a(Context context, int i) {
        super(context, Placement.INAPP_BANNER);
        this.offset = i;
    }

    /* access modifiers changed from: protected */
    public final void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
        b bVar = new b(this.context, this, this.offset, adPreferences, adEventListener);
        bVar.c();
        this.offset++;
    }

    public final int a() {
        return this.offset;
    }

    public final void b() {
        this.fixedSize = true;
    }

    public final boolean c() {
        return this.fixedSize;
    }

    public final int d() {
        return this.bannerType;
    }

    public final void a(int i) {
        this.bannerType = i;
    }
}
