package com.startapp.android.publish.ads.banner.banner3d;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowManager;
import com.explorestack.iab.vast.VastError;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.ads.banner.BannerOptions;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.l.a;

/* compiled from: StartAppSDK */
public final class Banner3DSize {

    /* compiled from: StartAppSDK */
    public enum Size {
        XXSMALL(new a(280, 50)),
        XSMALL(new a(VastError.ERROR_CODE_GENERAL_WRAPPER, 50)),
        SMALL(new a(ModuleDescriptor.MODULE_VERSION, 50)),
        MEDIUM(new a(468, 60)),
        LARGE(new a(728, 90)),
        XLARGE(new a(1024, 90));
        
        private a size$747797a0;

        private Size(a aVar) {
            this.size$747797a0 = aVar;
        }

        public final a getSize$2729ce21() {
            return this.size$747797a0;
        }
    }

    public static boolean a(Context context, ViewParent viewParent, BannerOptions bannerOptions, Banner3D banner3D, a aVar) {
        Size[] values;
        a a2 = a(context, viewParent, bannerOptions, banner3D);
        aVar.a(a2.a(), a2.b());
        boolean z = false;
        for (Size size : Size.values()) {
            if (size.getSize$2729ce21().a() <= a2.a() && size.getSize$2729ce21().b() <= a2.b()) {
                StringBuilder sb = new StringBuilder("BannerSize [");
                sb.append(size.getSize$2729ce21().a());
                sb.append(",");
                sb.append(size.getSize$2729ce21().b());
                sb.append(RequestParameters.RIGHT_BRACKETS);
                bannerOptions.a(size.getSize$2729ce21().a(), size.getSize$2729ce21().b());
                z = true;
            }
        }
        if (!z) {
            bannerOptions.a(0, 0);
        }
        StringBuilder sb2 = new StringBuilder("============== Optimize Size [");
        sb2.append(z);
        sb2.append("] ==========");
        return z;
    }

    private static a a(Context context, ViewParent viewParent, BannerOptions bannerOptions, Banner3D banner3D) {
        Point point = new Point();
        point.x = bannerOptions.d();
        point.y = bannerOptions.e();
        if (banner3D.getLayoutParams() != null && banner3D.getLayoutParams().width > 0) {
            point.x = i.b(context, banner3D.getLayoutParams().width + 1);
        }
        if (banner3D.getLayoutParams() != null && banner3D.getLayoutParams().height > 0) {
            point.y = i.b(context, banner3D.getLayoutParams().height + 1);
        }
        if (banner3D.getLayoutParams() == null || banner3D.getLayoutParams().width <= 0 || banner3D.getLayoutParams().height <= 0) {
            if (context instanceof Activity) {
                View decorView = ((Activity) context).getWindow().getDecorView();
                try {
                    View view = (View) viewParent;
                    if (view instanceof Banner) {
                        view = (View) view.getParent();
                    }
                    boolean z = false;
                    boolean z2 = false;
                    while (view != null && (view.getMeasuredWidth() <= 0 || view.getMeasuredHeight() <= 0)) {
                        if (view.getMeasuredWidth() > 0 && !z) {
                            b(context, point, view);
                            z = true;
                        }
                        if (view.getMeasuredHeight() > 0 && !z2) {
                            a(context, point, view);
                            z2 = true;
                        }
                        view = (View) view.getParent();
                    }
                    if (view == null) {
                        c(context, point, decorView);
                    } else {
                        if (!z) {
                            b(context, point, view);
                        }
                        if (!z2) {
                            a(context, point, view);
                        }
                    }
                } catch (Exception unused) {
                    c(context, point, decorView);
                }
            } else {
                try {
                    WindowManager windowManager = (WindowManager) context.getSystemService("window");
                    if (windowManager != null) {
                        if (VERSION.SDK_INT >= 13) {
                            windowManager.getDefaultDisplay().getSize(point);
                        } else {
                            point.x = windowManager.getDefaultDisplay().getWidth();
                            point.y = windowManager.getDefaultDisplay().getHeight();
                        }
                        point.x = i.b(context, point.x);
                        point.y = i.b(context, point.y);
                    }
                } catch (Exception e) {
                    f.a(context, d.EXCEPTION, "Banner3DSize.getApplicationSize - system service failed", e.getMessage(), "");
                }
            }
        }
        StringBuilder sb = new StringBuilder("============ exit Application Size [");
        sb.append(point.x);
        sb.append(",");
        sb.append(point.y);
        sb.append("] =========");
        return new a(point.x, point.y);
    }

    private static void a(Context context, Point point, View view) {
        point.y = i.b(context, (view.getMeasuredHeight() - view.getPaddingBottom()) - view.getPaddingTop());
    }

    private static void b(Context context, Point point, View view) {
        point.x = i.b(context, (view.getMeasuredWidth() - view.getPaddingLeft()) - view.getPaddingRight());
    }

    private static void c(Context context, Point point, View view) {
        point.x = i.b(context, view.getMeasuredWidth());
        point.y = i.b(context, view.getMeasuredHeight());
    }
}
