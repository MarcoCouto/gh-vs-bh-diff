package com.startapp.android.publish.ads.banner.banner3d;

import android.content.Context;
import com.startapp.android.publish.a.a;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.common.model.GetAdRequest;

/* compiled from: StartAppSDK */
public final class c extends a {
    private int g = 0;

    /* access modifiers changed from: protected */
    public final void a(Ad ad) {
    }

    public c(Context context, a aVar, int i, AdPreferences adPreferences, AdEventListener adEventListener) {
        super(context, aVar, adPreferences, adEventListener, Placement.INAPP_BANNER);
        this.g = i;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        a aVar = (a) this.b;
        com.startapp.android.publish.ads.banner.a aVar2 = new com.startapp.android.publish.ads.banner.a();
        b((GetAdRequest) aVar2);
        aVar2.setAdsNumber(com.startapp.android.publish.ads.banner.c.a().b().f());
        aVar2.setOffset(this.g);
        aVar2.a(aVar.c());
        return aVar2;
    }
}
