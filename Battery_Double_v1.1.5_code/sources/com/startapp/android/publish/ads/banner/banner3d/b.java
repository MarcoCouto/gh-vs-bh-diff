package com.startapp.android.publish.ads.banner.banner3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils.TruncateAt;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.android.publish.ads.banner.banner3d.Banner3DSize.Size;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.common.a.c;

/* compiled from: StartAppSDK */
public final class b extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    private TextView f3884a;
    private TextView b;
    private ImageView c;
    private com.startapp.android.publish.a.b d;
    private TextView e;
    private Point f;

    /* compiled from: StartAppSDK */
    enum a {
        XS,
        S,
        M,
        L,
        XL
    }

    public b(Context context, Point point) {
        int i;
        int i2;
        super(context);
        this.f = point;
        Context context2 = getContext();
        a templateBySize = getTemplateBySize();
        setBackgroundDrawable(new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{com.startapp.android.publish.adsCommon.b.a().n(), com.startapp.android.publish.adsCommon.b.a().o()}));
        setLayoutParams(new LayoutParams(-2, -2));
        int a2 = i.a(context2, 2);
        int a3 = i.a(context2, 3);
        i.a(context2, 4);
        int a4 = i.a(context2, 5);
        int a5 = i.a(context2, 6);
        int a6 = i.a(context2, 8);
        i.a(context2, 10);
        int a7 = i.a(context2, 20);
        i.a(context2, 84);
        int a8 = i.a(context2, 90);
        setPadding(a4, 0, a4, 0);
        setTag(this);
        this.c = new ImageView(context2);
        this.c.setId(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a8, a8);
        layoutParams.addRule(15);
        this.c.setLayoutParams(layoutParams);
        this.f3884a = new TextView(context2);
        this.f3884a.setId(2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(c.a(17), 1);
        layoutParams2.addRule(14);
        this.f3884a.setLayoutParams(layoutParams2);
        this.f3884a.setTextColor(com.startapp.android.publish.adsCommon.b.a().q().intValue());
        this.f3884a.setGravity(c.a((int) GravityCompat.START));
        this.f3884a.setBackgroundColor(0);
        switch (templateBySize) {
            case XS:
            case S:
                this.f3884a.setTextSize(17.0f);
                this.f3884a.setPadding(a3, 0, 0, a2);
                Context context3 = getContext();
                double d2 = (double) this.f.x;
                Double.isNaN(d2);
                layoutParams2.width = i.a(context3, (int) (d2 * 0.55d));
                break;
            case M:
                this.f3884a.setTextSize(17.0f);
                this.f3884a.setPadding(a3, 0, 0, a2);
                Context context4 = getContext();
                double d3 = (double) this.f.x;
                Double.isNaN(d3);
                layoutParams2.width = i.a(context4, (int) (d3 * 0.65d));
                break;
            case L:
            case XL:
                this.f3884a.setTextSize(22.0f);
                this.f3884a.setPadding(a3, 0, 0, a4);
                break;
        }
        this.f3884a.setSingleLine(true);
        this.f3884a.setEllipsize(TruncateAt.END);
        i.a(this.f3884a, com.startapp.android.publish.adsCommon.b.a().r());
        this.b = new TextView(context2);
        this.b.setId(3);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(c.a(17), 1);
        layoutParams3.addRule(3, 2);
        layoutParams3.setMargins(0, 0, 0, a4);
        this.b.setLayoutParams(layoutParams3);
        this.b.setTextColor(com.startapp.android.publish.adsCommon.b.a().t().intValue());
        this.b.setTextSize(18.0f);
        this.b.setMaxLines(2);
        this.b.setLines(2);
        this.b.setSingleLine(false);
        this.b.setEllipsize(TruncateAt.MARQUEE);
        this.b.setHorizontallyScrolling(true);
        this.b.setPadding(a3, 0, 0, 0);
        this.d = new com.startapp.android.publish.a.b(getContext());
        this.d.setId(5);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        switch (templateBySize) {
            case XS:
            case S:
            case M:
                i2 = a3;
                layoutParams4.addRule(c.a(17), 1);
                layoutParams4.addRule(8, 1);
                break;
            case L:
            case XL:
                layoutParams4.addRule(c.a(17), 2);
                Context context5 = getContext();
                i2 = a3;
                double d4 = (double) this.f.x;
                Double.isNaN(d4);
                layoutParams3.width = i.a(context5, (int) (d4 * 0.6d));
                break;
            default:
                i = a3;
                break;
        }
        i = i2;
        layoutParams4.setMargins(i, a6, i, 0);
        this.d.setLayoutParams(layoutParams4);
        this.e = new TextView(context2);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        switch (templateBySize) {
            case XS:
            case S:
            case M:
                this.e.setTextSize(13.0f);
                layoutParams5.addRule(c.a(17), 2);
                layoutParams5.addRule(15);
                break;
            case L:
                layoutParams5.addRule(c.a(17), 3);
                layoutParams5.addRule(15);
                layoutParams5.setMargins(a7, 0, 0, 0);
                this.e.setTextSize(26.0f);
                break;
            case XL:
                layoutParams5.addRule(c.a(17), 3);
                layoutParams5.addRule(15);
                layoutParams5.setMargins(a7 * 7, 0, 0, 0);
                this.e.setTextSize(26.0f);
                break;
        }
        this.e.setPadding(a5, a5, a5, a5);
        this.e.setLayoutParams(layoutParams5);
        setButtonText(false);
        this.e.setTextColor(-1);
        this.e.setTypeface(null, 1);
        this.e.setId(4);
        this.e.setShadowLayer(2.5f, -3.0f, 3.0f, -9013642);
        this.e.setBackgroundDrawable(new ShapeDrawable(new RoundRectShape(new float[]{10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f}, null, null)) {
            /* access modifiers changed from: protected */
            public final void onDraw(Shape shape, Canvas canvas, Paint paint) {
                paint.setColor(-11363070);
                paint.setMaskFilter(new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 5.0f, 3.0f));
                super.onDraw(shape, canvas, paint);
            }
        });
        addView(this.c);
        addView(this.f3884a);
        switch (templateBySize) {
            case XS:
            case S:
            case M:
                addView(this.e);
                break;
            case L:
            case XL:
                addView(this.e);
                addView(this.b);
                break;
        }
        addView(this.d);
    }

    public final void setText(String str) {
        this.f3884a.setText(str);
    }

    public final void setImage(Bitmap bitmap) {
        this.c.setImageBitmap(bitmap);
    }

    public final void a(int i, int i2) {
        this.c.setImageResource(17301651);
        LayoutParams layoutParams = this.c.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        this.c.setLayoutParams(layoutParams);
    }

    public final void setRating(float f2) {
        try {
            this.d.setRating(f2);
        } catch (NullPointerException unused) {
        }
    }

    public final void a(Bitmap bitmap, int i, int i2) {
        this.c.setImageBitmap(bitmap);
        LayoutParams layoutParams = this.c.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        this.c.setLayoutParams(layoutParams);
    }

    public final void setDescription(String str) {
        if (str != null && str.compareTo("") != 0) {
            String[] a2 = a(str);
            String str2 = a2[0];
            String str3 = "";
            if (a2[1] != null) {
                str3 = a(a2[1])[0];
            }
            if (str.length() >= 110) {
                StringBuilder sb = new StringBuilder();
                sb.append(str3);
                sb.append("...");
                str3 = sb.toString();
            }
            TextView textView = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append("\n");
            sb2.append(str3);
            textView.setText(sb2.toString());
        }
    }

    private static String[] a(String str) {
        boolean z;
        String[] strArr = new String[2];
        int i = 55;
        if (str.length() > 55) {
            char[] charArray = str.substring(0, 55).toCharArray();
            int length = charArray.length - 1;
            int i2 = length - 1;
            while (true) {
                if (i2 <= 0) {
                    z = false;
                    break;
                } else if (charArray[i2] == ' ') {
                    length = i2;
                    z = true;
                    break;
                } else {
                    i2--;
                }
            }
            if (z) {
                i = length;
            }
            strArr[0] = str.substring(0, i);
            strArr[1] = str.substring(i + 1, str.length());
        } else {
            strArr[0] = str;
            strArr[1] = null;
        }
        return strArr;
    }

    private a getTemplateBySize() {
        a aVar = a.S;
        if (this.f.x > Size.SMALL.getSize$2729ce21().a() || this.f.y > Size.SMALL.getSize$2729ce21().b()) {
            aVar = a.M;
        }
        if (this.f.x > Size.MEDIUM.getSize$2729ce21().a() || this.f.y > Size.MEDIUM.getSize$2729ce21().b()) {
            aVar = a.L;
        }
        return (this.f.x > Size.LARGE.getSize$2729ce21().a() || this.f.y > Size.LARGE.getSize$2729ce21().b()) ? a.XL : aVar;
    }

    public final void setButtonText(boolean z) {
        if (z) {
            this.e.setText("OPEN");
        } else {
            this.e.setText("DOWNLOAD");
        }
    }
}
