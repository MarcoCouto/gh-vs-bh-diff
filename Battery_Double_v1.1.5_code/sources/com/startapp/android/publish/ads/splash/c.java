package com.startapp.android.publish.ads.splash;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.b.g;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.metaData.f;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.Constants;
import com.startapp.common.b;

/* compiled from: StartAppSDK */
public final class c implements e {

    /* renamed from: a reason: collision with root package name */
    Activity f3929a;
    boolean b;
    int c;
    private boolean d;
    private boolean e;
    private boolean f;
    private boolean g;
    private boolean h;
    private d i;
    private BroadcastReceiver j;

    /* compiled from: StartAppSDK */
    enum a {
        ;
        

        /* renamed from: a reason: collision with root package name */
        public static final int f3933a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;

        static {
            int[] iArr = {1, 2, 3, 4, 5};
        }
    }

    public c(Activity activity) {
        this.d = false;
        this.e = true;
        this.f = false;
        this.g = false;
        this.h = false;
        this.b = false;
        this.c = a.f3933a;
        this.i = null;
        this.j = new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                c.this.f();
            }
        };
        this.f3929a = activity;
    }

    public c(Activity activity, d dVar) {
        this(activity);
        this.i = dVar;
    }

    public final void a(final Runnable runnable, final com.startapp.android.publish.cache.c cVar) {
        this.d = true;
        AnonymousClass1 r0 = new f() {
            private Runnable d = new Runnable() {
                public final void run() {
                    c.this.b = true;
                    if (c.this.c != a.e) {
                        c.this.c(runnable, cVar);
                    }
                }
            };

            public final void a(boolean z) {
                c.this.f3929a.runOnUiThread(this.d);
            }

            public final void a() {
                c.this.f3929a.runOnUiThread(this.d);
            }
        };
        if (this.c != a.e) {
            synchronized (e.getLock()) {
                if (e.getInstance().isReady()) {
                    r0.a(false);
                } else {
                    e.getInstance().addMetaDataListener(r0);
                }
            }
            return;
        }
        i();
    }

    public final void a() {
        this.d = true;
    }

    public final void a(Runnable runnable) {
        if (this.c == a.f3933a) {
            this.c = a.b;
        }
        b(runnable);
    }

    public final void b() {
        this.c = a.e;
        b(null);
    }

    public final boolean b(Runnable runnable, com.startapp.android.publish.cache.c cVar) {
        if (!this.h) {
            if (this.c == a.f3933a) {
                this.e = false;
                this.c = a.e;
                i();
                return true;
            } else if (this.c == a.b) {
                this.b = true;
                c(runnable, cVar);
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final void c(Runnable runnable, com.startapp.android.publish.cache.c cVar) {
        com.startapp.android.publish.adsCommon.b.f a2 = g.a().b().a(Placement.INAPP_SPLASH, (String) null);
        new StringBuilder("checkAdRulesAndShowAd: shouldDisplayAd ").append(a2.a());
        if (a2.a()) {
            b(runnable);
            return;
        }
        this.c = a.e;
        if (cVar != null) {
            com.startapp.android.publish.adsCommon.c.a((Context) this.f3929a, com.startapp.android.publish.adsCommon.c.a(com.startapp.android.publish.cache.a.a().b(cVar)), (String) null, a2.c());
        }
        Constants.a();
        i();
    }

    public final void c() {
        this.c = a.d;
        j();
        if (!this.f3929a.isFinishing()) {
            this.f3929a.finish();
        }
    }

    private void b(Runnable runnable) {
        if (this.d && (this.b || runnable == null)) {
            if (this.c == a.b && runnable != null) {
                this.e = false;
                runnable.run();
            } else if (this.c != a.f3933a) {
                i();
            }
        }
    }

    public final void a(StartAppAd startAppAd) {
        if (this.c == a.c && !this.g) {
            startAppAd.close();
            c();
        }
    }

    public final void d() {
        if (this.c != a.c && this.c != a.e) {
            this.c = a.e;
            if (this.e) {
                j();
            }
        }
    }

    private void i() {
        a(this.i, (e) this);
    }

    private void j() {
        if (!this.f) {
            this.f = true;
            b.a((Context) this.f3929a).a(new Intent("com.startapp.android.splashHidden"));
        }
        if (this.j != null) {
            try {
                Log.v("startapp", "unregistering receiver");
                b.a((Context) this.f3929a).a(this.j);
            } catch (IllegalArgumentException unused) {
            }
        }
    }

    public final void e() {
        this.h = true;
    }

    public final void f() {
        this.g = true;
    }

    public final void g() {
        b.a((Context) this.f3929a).a(this.j, new IntentFilter("com.startapp.android.adInfoWasClickedBroadcastListener"));
    }

    protected static void a(d dVar, e eVar) {
        if (dVar == null) {
            eVar.h();
            return;
        }
        dVar.callback = eVar;
        dVar.b();
    }

    public final void h() {
        j();
        if (!this.f3929a.isFinishing()) {
            this.f3929a.finish();
        }
    }
}
