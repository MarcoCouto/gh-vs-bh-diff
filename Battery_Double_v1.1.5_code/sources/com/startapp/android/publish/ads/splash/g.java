package com.startapp.android.publish.ads.splash;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Toast;
import com.startapp.android.publish.ads.a.b;
import com.startapp.android.publish.common.model.AdPreferences;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class g extends b {
    private SplashConfig c = null;
    private h d;
    private boolean e = false;
    private boolean f = false;

    public final void o() {
    }

    public final void q() {
    }

    public final void u() {
    }

    public final void a(Bundle bundle) {
        this.c = (SplashConfig) a().getSerializableExtra("SplashConfig");
    }

    public final boolean a(int i, KeyEvent keyEvent) {
        if (this.e) {
            if (i == 25) {
                if (!this.f) {
                    this.f = true;
                    h hVar = this.d;
                    hVar.e = true;
                    hVar.b.e();
                    Toast.makeText(b(), "Test Mode", 0).show();
                    return true;
                }
            } else if (i == 24 && this.f) {
                b().finish();
                return true;
            }
        }
        return i == 4;
    }

    public final void r() {
        if (this.d != null) {
            this.d.b();
        }
    }

    public final void s() {
        AdPreferences adPreferences;
        if (this.c != null) {
            Serializable serializableExtra = a().getSerializableExtra("AdPreference");
            if (serializableExtra != null) {
                adPreferences = (AdPreferences) serializableExtra;
            } else {
                adPreferences = new AdPreferences();
            }
            this.e = a().getBooleanExtra("testMode", false);
            this.d = new h(b(), this.c, adPreferences);
            this.d.a();
        }
    }
}
