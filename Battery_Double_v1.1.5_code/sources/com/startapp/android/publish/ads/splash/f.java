package com.startapp.android.publish.ads.splash;

import android.content.Context;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.common.c.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class f implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private static volatile f f3936a = new f();
    private static Object b = new Object();
    private static final long serialVersionUID = 1;
    @e(a = true)
    private SplashConfig SplashConfig = new SplashConfig();
    private String splashMetadataUpdateVersion = AdsConstants.c;

    public final SplashConfig a() {
        return this.SplashConfig;
    }

    public static f b() {
        return f3936a;
    }

    public static void a(Context context, f fVar) {
        synchronized (b) {
            fVar.splashMetadataUpdateVersion = AdsConstants.c;
            f3936a = fVar;
            com.startapp.common.a.e.a(context, "StartappSplashMetadata", (Serializable) fVar);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        return j.b(this.SplashConfig, fVar.SplashConfig) && j.b(this.splashMetadataUpdateVersion, fVar.splashMetadataUpdateVersion);
    }

    public int hashCode() {
        return j.a(this.SplashConfig, this.splashMetadataUpdateVersion);
    }

    public static void a(Context context) {
        f fVar = (f) com.startapp.common.a.e.a(context, "StartappSplashMetadata");
        f fVar2 = new f();
        if (fVar != null) {
            boolean a2 = j.a(fVar, fVar2);
            if (!(!AdsConstants.c.equals(fVar.splashMetadataUpdateVersion)) && a2) {
                com.startapp.android.publish.adsCommon.g.f.a(context, d.METADATA_NULL, "SplashMetaData", "", "");
            }
            f3936a = fVar;
            return;
        }
        f3936a = fVar2;
    }
}
