package com.startapp.android.publish.ads.splash;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.splash.SplashConfig.MaxAdDisplayTime;
import com.startapp.android.publish.ads.splash.SplashConfig.Orientation;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.adsCommon.b.f;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.cache.c;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class h {

    /* renamed from: a reason: collision with root package name */
    Activity f3937a;
    c b;
    c c;
    d d = null;
    boolean e = false;
    a f;
    Runnable g = new Runnable() {
        public final void run() {
            c.a(h.this.d, (e) new e() {
                public final void h() {
                    if (!h.this.e && h.this.f != null) {
                        h.this.f.showAd((AdDisplayListener) new AdDisplayListener() {
                            public final void adNotDisplayed(Ad ad) {
                            }

                            public final void adHidden(Ad ad) {
                                h.this.b.c();
                            }

                            public final void adDisplayed(Ad ad) {
                                h.this.b.c = a.c;
                            }

                            public final void adClicked(Ad ad) {
                                h.this.b.f();
                            }
                        });
                        h.this.f();
                        h.this.f3937a.finish();
                    }
                }
            });
        }
    };
    private SplashConfig h;
    private Handler i = new Handler();
    private AdPreferences j;
    private Runnable k = new Runnable() {
        public final void run() {
            if (h.this.c()) {
                h.this.d();
                h.this.e();
                return;
            }
            h.this.f3937a.finish();
        }
    };
    private AdEventListener l = new AdEventListener() {
        public final void onReceiveAd(Ad ad) {
            h.this.b.a(h.this.g);
        }

        public final void onFailedToReceiveAd(Ad ad) {
            if (h.this.f != null) {
                h.this.b.b();
            }
        }
    };

    /* compiled from: StartAppSDK */
    static class a extends StartAppAd {
        private static final long serialVersionUID = 1;

        public a(Context context) {
            super(context);
            this.placement = Placement.INAPP_SPLASH;
        }

        /* access modifiers changed from: protected */
        public final f shouldDisplayAd(String str, Placement placement) {
            return new f();
        }
    }

    public h(Activity activity, SplashConfig splashConfig, AdPreferences adPreferences) {
        this.f3937a = activity;
        this.h = splashConfig;
        this.j = adPreferences;
        try {
            this.h.initSplashLogo(this.f3937a);
            if (!g()) {
                this.d = this.h.initSplashHtml(this.f3937a);
            }
            this.b = new c(activity, this.d);
        } catch (Exception e2) {
            this.b = new c(activity);
            this.b.a();
            this.b.b();
            com.startapp.android.publish.adsCommon.g.f.a(activity, d.EXCEPTION, "SplashScreen.constructor - WebView failed", e2.getMessage(), "");
        }
    }

    public final void a() {
        this.b.g();
        int i2 = this.f3937a.getResources().getConfiguration().orientation;
        if (this.h.getOrientation() == Orientation.AUTO) {
            if (i2 == 2) {
                this.h.setOrientation(Orientation.LANDSCAPE);
            } else {
                this.h.setOrientation(Orientation.PORTRAIT);
            }
        }
        boolean z = false;
        boolean z2 = true;
        switch (this.h.getOrientation()) {
            case PORTRAIT:
                if (i2 == 2) {
                    z = true;
                }
                Activity activity = this.f3937a;
                if (VERSION.SDK_INT < 9) {
                    activity.setRequestedOrientation(1);
                    break;
                } else {
                    try {
                        activity.setRequestedOrientation(7);
                        break;
                    } catch (Exception unused) {
                        break;
                    }
                }
            case LANDSCAPE:
                if (i2 != 1) {
                    z2 = false;
                }
                Activity activity2 = this.f3937a;
                if (VERSION.SDK_INT >= 9) {
                    try {
                        activity2.setRequestedOrientation(6);
                    } catch (Exception unused2) {
                    }
                } else {
                    activity2.setRequestedOrientation(0);
                }
                z = z2;
                break;
        }
        StringBuilder sb = new StringBuilder("Set Orientation: [");
        sb.append(this.h.getOrientation().toString());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        if (!z) {
            this.i.post(this.k);
        } else {
            this.i.postDelayed(this.k, 100);
        }
    }

    public final void b() {
        this.i.removeCallbacks(this.k);
        this.b.d();
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        if (this.h.validate(this.f3937a)) {
            View view = null;
            if (g()) {
                view = this.h.getLayout(this.f3937a);
            } else if (this.d != null) {
                view = this.d.c();
            }
            if (view == null) {
                return false;
            }
            this.f3937a.setContentView(view, new LayoutParams(-1, -1));
            return true;
        }
        throw new IllegalArgumentException(this.h.getErrorMessage());
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        this.f = new a(this.f3937a.getApplicationContext());
        this.c = this.f.loadSplash(this.j, this.l);
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        this.i.postDelayed(new Runnable() {
            public final void run() {
                if (h.this.b.b(h.this.g, h.this.c)) {
                    h.this.f = null;
                    h.this.c = null;
                }
            }
        }, this.h.getMaxLoadAdTimeout().longValue());
        this.i.postDelayed(new Runnable() {
            public final void run() {
                h.this.b.a(h.this.g, h.this.c);
            }
        }, this.h.getMinSplashTime().getIndex());
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        if (this.h.getMaxAdDisplayTime() != MaxAdDisplayTime.FOR_EVER) {
            this.i.postDelayed(new Runnable() {
                public final void run() {
                    h.this.b.a((StartAppAd) h.this.f);
                }
            }, this.h.getMaxAdDisplayTime().getIndex());
        }
    }

    private boolean g() {
        return !this.h.isHtmlSplash() || this.h.isUserDefinedSplash();
    }
}
