package com.startapp.android.publish.ads.video.b;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.widget.VideoView;
import com.startapp.android.publish.ads.video.b.c.C0086c;
import com.startapp.android.publish.ads.video.b.c.g;
import com.startapp.android.publish.ads.video.b.c.h;
import com.startapp.android.publish.ads.video.c;

/* compiled from: StartAppSDK */
public final class b extends a implements OnCompletionListener, OnErrorListener, OnPreparedListener {
    private MediaPlayer f;
    private VideoView g;

    /* compiled from: StartAppSDK */
    public enum a {
        MEDIA_ERROR_IO,
        MEDIA_ERROR_MALFORMED,
        MEDIA_ERROR_UNSUPPORTED,
        MEDIA_ERROR_TIMED_OUT;

        public static a a(int i) {
            if (i == -1010) {
                return MEDIA_ERROR_UNSUPPORTED;
            }
            if (i == -1007) {
                return MEDIA_ERROR_MALFORMED;
            }
            if (i == -1004) {
                return MEDIA_ERROR_IO;
            }
            if (i != -110) {
                return MEDIA_ERROR_IO;
            }
            return MEDIA_ERROR_TIMED_OUT;
        }
    }

    /* renamed from: com.startapp.android.publish.ads.video.b.b$b reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public enum C0085b {
        ;
        

        /* renamed from: a reason: collision with root package name */
        public static final int f3955a = 2;

        public static int a(int i) {
            return i == 100 ? 2 : 1;
        }

        static {
            int[] iArr = {1, 2};
        }
    }

    public b(VideoView videoView) {
        this.g = videoView;
        this.g.setOnPreparedListener(this);
        this.g.setOnCompletionListener(this);
        this.g.setOnErrorListener(this);
    }

    public final void a() {
        this.g.start();
    }

    public final void a(int i) {
        StringBuilder sb = new StringBuilder("seekTo(");
        sb.append(i);
        sb.append(")");
        this.g.seekTo(i);
    }

    public final void b() {
        this.g.pause();
    }

    public final void c() {
        this.g.stopPlayback();
    }

    public final void a(boolean z) {
        StringBuilder sb = new StringBuilder("setMute(");
        sb.append(z);
        sb.append(")");
        if (this.f != null) {
            if (z) {
                this.f.setVolume(0.0f, 0.0f);
                return;
            }
            this.f.setVolume(1.0f, 1.0f);
        }
    }

    public final int d() {
        return this.g.getCurrentPosition();
    }

    public final int e() {
        return this.g.getDuration();
    }

    public final boolean f() {
        return this.f != null;
    }

    public final void a(String str) {
        StringBuilder sb = new StringBuilder("setVideoLocation(");
        sb.append(str);
        sb.append(")");
        super.a(str);
        this.g.setVideoPath(this.f3952a);
    }

    public final void g() {
        if (this.f != null) {
            this.f = null;
        }
        c.a().a((C0086c) null);
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        this.f = mediaPlayer;
        if (this.b != null) {
            this.b.R();
        }
        if (!com.startapp.android.publish.adsCommon.c.c(this.f3952a) || this.f == null) {
            if (!com.startapp.android.publish.adsCommon.c.c(this.f3952a)) {
                c.a().a(this.e);
            }
            return;
        }
        this.f.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
            public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
                if (b.this.e != null) {
                    b.this.e.g(i);
                }
            }
        });
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (this.d != null) {
            this.d.S();
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        StringBuilder sb = new StringBuilder("onError(");
        sb.append(i);
        sb.append(", ");
        sb.append(i2);
        sb.append(")");
        if (this.c == null) {
            return false;
        }
        return this.c.b(new g(C0085b.a(i) == C0085b.f3955a ? h.SERVER_DIED : h.UNKNOWN, a.a(i2).toString(), mediaPlayer != null ? mediaPlayer.getCurrentPosition() : -1));
    }
}
