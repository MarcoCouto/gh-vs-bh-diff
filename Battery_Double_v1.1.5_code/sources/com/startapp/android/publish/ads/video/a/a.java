package com.startapp.android.publish.ads.video.a;

import android.text.TextUtils;
import com.startapp.android.publish.ads.video.tracking.VideoTrackingLink;
import com.startapp.android.publish.ads.video.tracking.VideoTrackingLink.TrackingSource;
import com.startapp.android.publish.ads.video.tracking.VideoTrackingParams;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.cache.i;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private VideoTrackingLink[] f3950a;
    private VideoTrackingParams b;
    private String c;
    private int d;
    private String e = "";
    private com.startapp.android.publish.ads.video.c.a.a f;

    public a(VideoTrackingLink[] videoTrackingLinkArr, VideoTrackingParams videoTrackingParams, String str, int i) {
        this.f3950a = videoTrackingLinkArr;
        this.b = videoTrackingParams;
        this.c = str;
        this.d = i;
    }

    public final a a(com.startapp.android.publish.ads.video.c.a.a aVar) {
        this.f = aVar;
        return this;
    }

    public final a a(String str) {
        this.e = str;
        return this;
    }

    public final i a() {
        VideoTrackingLink[] videoTrackingLinkArr;
        Object obj;
        String str;
        Object obj2 = null;
        if (!((this.f3950a == null || this.b == null) ? false : true)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        VideoTrackingLink[] videoTrackingLinkArr2 = this.f3950a;
        int length = videoTrackingLinkArr2.length;
        int i = 0;
        while (i < length) {
            VideoTrackingLink videoTrackingLink = videoTrackingLinkArr2[i];
            if (videoTrackingLink.getTrackingUrl() == null) {
                new StringBuilder("Ignoring tracking link - tracking url is null: tracking link = ").append(videoTrackingLink);
            } else if (this.b.getOffset() <= 0 || videoTrackingLink.shouldAppendReplay()) {
                StringBuilder sb = new StringBuilder();
                TrackingSource trackingSource = videoTrackingLink.getTrackingSource();
                VideoTrackingParams replayParameter = this.b.setInternalTrackingParamsIndicator(trackingSource != null && trackingSource == TrackingSource.STARTAPP).setShouldAppendOffset(videoTrackingLink.shouldAppendReplay()).setReplayParameter(videoTrackingLink.getReplayParameter());
                String trackingUrl = videoTrackingLink.getTrackingUrl();
                String str2 = "[ASSETURI]";
                if (this.c != null) {
                    str = TextUtils.htmlEncode(this.c);
                } else {
                    str = "";
                }
                int i2 = this.d;
                videoTrackingLinkArr = videoTrackingLinkArr2;
                long convert = TimeUnit.SECONDS.convert((long) i2, TimeUnit.MILLISECONDS);
                Object[] objArr = {Long.valueOf(convert / 3600), Long.valueOf((convert % 3600) / 60), Long.valueOf(convert % 60), Long.valueOf((long) (i2 % 1000))};
                String format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format(new Date());
                int length2 = format.length() - 2;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(format.substring(0, length2));
                sb2.append(":");
                sb2.append(format.substring(length2));
                String replace = trackingUrl.replace(str2, str).replace("[CONTENTPLAYHEAD]", TextUtils.htmlEncode(String.format(Locale.US, "%02d:%02d:%02d.%03d", objArr))).replace("[CACHEBUSTING]", TextUtils.htmlEncode(String.valueOf(new SecureRandom().nextInt(90000000) + 10000000))).replace("[TIMESTAMP]", TextUtils.htmlEncode(sb2.toString()));
                if (this.f != null) {
                    replace = replace.replace("[ERRORCODE]", String.valueOf(this.f.a()));
                }
                sb.append(replace);
                sb.append(replayParameter.getQueryString());
                if (replayParameter.getInternalTrackingParamsIndicator()) {
                    obj = null;
                    sb.append(com.startapp.common.a.a.a(c.a(trackingUrl, (String) null)));
                } else {
                    obj = null;
                }
                arrayList.add(sb.toString());
                i++;
                obj2 = obj;
                videoTrackingLinkArr2 = videoTrackingLinkArr;
            } else {
                new StringBuilder("Ignoring tracking link - external replay event: tracking link = ").append(videoTrackingLink);
            }
            obj = obj2;
            videoTrackingLinkArr = videoTrackingLinkArr2;
            i++;
            obj2 = obj;
            videoTrackingLinkArr2 = videoTrackingLinkArr;
        }
        return new i(arrayList, this.e);
    }
}
