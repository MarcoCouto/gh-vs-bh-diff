package com.startapp.android.publish.ads.video;

import android.content.Context;
import com.startapp.android.publish.ads.video.c.a.d;
import com.startapp.android.publish.ads.video.tracking.VideoTrackingLink;
import com.startapp.android.publish.ads.video.tracking.VideoTrackingParams;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.Ad.AdType;
import com.startapp.android.publish.adsCommon.HtmlAd;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.cache.c;
import com.startapp.android.publish.common.metaData.e;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.common.model.GetAdRequest;
import com.startapp.android.publish.html.a;
import com.startapp.common.a.g;
import com.startapp.common.f;
import java.net.URL;
import java.util.ArrayList;

/* compiled from: StartAppSDK */
public final class b extends a implements c.a {
    private long i = System.currentTimeMillis();
    private volatile c j;
    private com.startapp.android.publish.ads.video.c.a.c k;
    private int l = 0;

    public b(Context context, Ad ad, AdPreferences adPreferences, AdEventListener adEventListener) {
        com.startapp.android.publish.ads.video.c.a.c cVar;
        super(context, ad, adPreferences, adEventListener, Placement.INAPP_OVERLAY, true);
        if (com.startapp.android.publish.adsCommon.b.a().H().r() == 0) {
            cVar = new com.startapp.android.publish.ads.video.c.a.c(context);
        } else {
            cVar = new d(context, com.startapp.android.publish.adsCommon.b.a().H().s());
        }
        this.k = cVar;
    }

    /* access modifiers changed from: protected */
    public final boolean a(Object obj) {
        boolean z;
        g.a aVar = (g.a) obj;
        String str = null;
        boolean z2 = true;
        if (aVar == null || !aVar.b().toLowerCase().contains("json")) {
            if (aVar != null) {
                str = aVar.a();
            }
            if (com.startapp.android.publish.adsCommon.b.a().H().h()) {
                if (j.a(str, "@videoJson@", "@videoJson@") == null) {
                    z2 = false;
                }
                if (z2) {
                    b(false);
                }
            }
            return super.a(obj);
        }
        if (com.startapp.android.publish.adsCommon.b.a().H().h() && !this.h.hasCampaignExclude()) {
            b(true);
        }
        try {
            d dVar = (d) com.startapp.common.c.b.a(aVar.a(), d.class);
            if (dVar == null || dVar.getVastTag() == null) {
                z = a("no VAST wrapper in json", null, true);
            } else {
                com.startapp.android.publish.ads.video.c.b.a aVar2 = new com.startapp.android.publish.ads.video.c.b.a(com.startapp.android.publish.adsCommon.b.a().H().n(), com.startapp.android.publish.adsCommon.b.a().H().o());
                com.startapp.android.publish.ads.video.c.a.a a2 = aVar2.a(this.f4063a, dVar.getVastTag(), this.k);
                ((g) this.b).a(aVar2.a(), this.b.getType() != AdType.REWARDED_VIDEO);
                if (dVar.getTtlSec() != null) {
                    ((g) this.b).setAdCacheTtl(dVar.getTtlSec());
                }
                if (a2 == com.startapp.android.publish.ads.video.c.a.a.ErrorNone) {
                    a(com.startapp.android.publish.ads.video.c.a.a.SAProcessSuccess);
                    aVar.a(dVar.getAdmTag());
                    aVar.b(WebRequest.CONTENT_TYPE_HTML);
                    z = super.a((Object) aVar);
                } else {
                    a(a2);
                    if (dVar.getCampaignId() != null) {
                        this.g.add(dVar.getCampaignId());
                    }
                    this.l++;
                    ((g) this.b).c();
                    if (System.currentTimeMillis() - this.i >= ((long) com.startapp.android.publish.adsCommon.b.a().H().p())) {
                        z = a("VAST retry timeout", null, false);
                    } else if (this.l > com.startapp.android.publish.adsCommon.b.a().H().q()) {
                        z = a("VAST too many excludes", null, false);
                    } else {
                        z = d().booleanValue();
                    }
                }
            }
            return z;
        } catch (Exception e) {
            return a("VAST json parsing", e, true);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(final Boolean bool) {
        super.a(bool);
        if (!bool.booleanValue() || !g()) {
            a(bool.booleanValue());
            return;
        }
        if (com.startapp.android.publish.adsCommon.b.a().H().i()) {
            super.b(bool);
        }
        b().setVideoMuted(this.c.isVideoMuted());
        AnonymousClass1 r6 = new i.a() {
            public final void a(String str) {
                if (str != null) {
                    if (!str.equals("downloadInterrupted")) {
                        b.super.b(bool);
                        b.this.b().setLocalVideoPath(str);
                    }
                    b.this.a(bool.booleanValue());
                    return;
                }
                b.this.a(false);
                b.this.d.onFailedToReceiveAd(b.this.b);
                b.this.a(com.startapp.android.publish.ads.video.c.a.a.FileNotFound);
            }
        };
        e a2 = e.a();
        Context applicationContext = this.f4063a.getApplicationContext();
        String videoUrl = b().getVideoUrl();
        f.a aVar = f.a.HIGH;
        AnonymousClass1 r2 = new Runnable(applicationContext, videoUrl, r6, this) {

            /* renamed from: a reason: collision with root package name */
            private /* synthetic */ Context f3970a;
            private /* synthetic */ String b;
            private /* synthetic */ i.a c;
            private /* synthetic */ c.a d;

            {
                this.f3970a = r2;
                this.b = r3;
                this.c = r4;
                this.d = r5;
            }

            public final void run() {
                e.a(e.this, this.f3970a, this.b, this.c, this.d);
            }
        };
        f.a(aVar, (Runnable) r2);
    }

    /* access modifiers changed from: protected */
    public final boolean a(GetAdRequest getAdRequest) {
        if (!super.a(getAdRequest)) {
            return false;
        }
        if (getAdRequest.isAdTypeVideo()) {
            j.a a2 = j.a(this.f4063a);
            if (a2 != j.a.ELIGIBLE) {
                this.f = a2.a();
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        return b((GetAdRequest) new a());
    }

    private boolean g() {
        return b() != null;
    }

    /* access modifiers changed from: protected */
    public final void b(Boolean bool) {
        if (!g()) {
            super.b(bool);
        }
    }

    /* access modifiers changed from: 0000 */
    public final f b() {
        return ((g) this.b).b();
    }

    private void b(boolean z) {
        AdPreferences adPreferences;
        if ((this.b.getType() != AdType.REWARDED_VIDEO && this.b.getType() != AdType.VIDEO) || z) {
            if (this.c == null) {
                adPreferences = new AdPreferences();
            } else {
                adPreferences = new AdPreferences(this.c);
            }
            AdPreferences adPreferences2 = adPreferences;
            adPreferences2.setType((this.b.getType() == AdType.REWARDED_VIDEO || this.b.getType() == AdType.VIDEO) ? AdType.VIDEO_NO_VAST : AdType.NON_VIDEO);
            c a2 = com.startapp.android.publish.cache.a.a().a(this.f4063a, (StartAppAd) null, this.e, adPreferences2, (AdEventListener) null);
            if (z) {
                this.j = a2;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(com.startapp.android.publish.ads.video.c.a.a aVar) {
        VideoTrackingLink[] videoTrackingLinkArr = null;
        try {
            if (!(b() == null || b().getVideoTrackingDetails() == null)) {
                videoTrackingLinkArr = b().getVideoTrackingDetails().getInlineErrorTrackingUrls();
            }
            if (videoTrackingLinkArr != null && videoTrackingLinkArr.length > 0) {
                if (aVar == com.startapp.android.publish.ads.video.c.a.a.SAShowBeforeVast || aVar == com.startapp.android.publish.ads.video.c.a.a.SAProcessSuccess) {
                    try {
                        ArrayList arrayList = new ArrayList();
                        String lowerCase = new URL(e.getInstance().getAdPlatformHost()).getHost().split("\\.")[1].toLowerCase();
                        for (VideoTrackingLink videoTrackingLink : videoTrackingLinkArr) {
                            if (videoTrackingLink.getTrackingUrl() != null && videoTrackingLink.getTrackingUrl().toLowerCase().contains(lowerCase)) {
                                arrayList.add(videoTrackingLink);
                            }
                        }
                        if (arrayList.size() > 0) {
                            videoTrackingLinkArr = (VideoTrackingLink[]) arrayList.toArray(new VideoTrackingLink[arrayList.size()]);
                        } else {
                            return;
                        }
                    } catch (Exception e) {
                        com.startapp.android.publish.adsCommon.g.f.a(this.f4063a, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "GetVideoEnabledService.sendVideoErrorEvent filter sa links", e.getMessage(), "");
                    }
                }
                j.a(this.f4063a, new com.startapp.android.publish.ads.video.a.a(videoTrackingLinkArr, new VideoTrackingParams("", 0, 0, "1"), b().getVideoUrl(), 0).a("error").a(aVar).a());
            }
        } catch (Exception e2) {
            com.startapp.android.publish.adsCommon.g.f.a(this.f4063a, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "GetVideoEnabledService.sendVideoErrorEvent", e2.getMessage(), "");
        }
    }

    private boolean a(String str, Throwable th, boolean z) {
        if (z) {
            com.startapp.android.publish.adsCommon.g.f.a(this.f4063a, com.startapp.android.publish.adsCommon.g.d.EXCEPTION, str, th != null ? th.getMessage() : "", "");
        }
        com.startapp.android.publish.adsCommon.g a2 = com.startapp.android.publish.cache.a.a().a(this.j);
        if (a2 instanceof HtmlAd) {
            g.a aVar = new g.a();
            aVar.b(WebRequest.CONTENT_TYPE_HTML);
            aVar.a(((HtmlAd) a2).getHtml());
            return super.a((Object) aVar);
        }
        this.b.setErrorMessage(this.f);
        return false;
    }

    public final void a(String str) {
        b().setLocalVideoPath(str);
    }
}
