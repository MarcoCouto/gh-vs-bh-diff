package com.startapp.android.publish.ads.video.tracking;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.video.c.a.b;
import com.startapp.common.a.f;
import com.startapp.common.c.e;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/* compiled from: StartAppSDK */
public class VideoTrackingDetails implements Serializable {
    private static final long serialVersionUID = 1;
    @e(b = AbsoluteTrackingLink.class)
    private AbsoluteTrackingLink[] absoluteTrackingUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] creativeViewUrls;
    @e(b = FractionTrackingLink.class)
    private FractionTrackingLink[] fractionTrackingUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] impressionUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] inlineErrorTrackingUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] soundMuteUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] soundUnmuteUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoClickTrackingUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoClosedUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPausedUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPostRollClosedUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPostRollImpressionUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoResumedUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoRewardedUrls;
    @e(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoSkippedUrls;

    public VideoTrackingDetails() {
    }

    public VideoTrackingDetails(com.startapp.android.publish.ads.video.c.a.e eVar) {
        if (eVar != null) {
            HashMap a2 = eVar.a();
            ArrayList arrayList = new ArrayList();
            addFractionUrls((List) a2.get(b.start), 0, arrayList);
            addFractionUrls((List) a2.get(b.firstQuartile), 25, arrayList);
            addFractionUrls((List) a2.get(b.midpoint), 50, arrayList);
            addFractionUrls((List) a2.get(b.thirdQuartile), 75, arrayList);
            addFractionUrls((List) a2.get(b.complete), 100, arrayList);
            this.fractionTrackingUrls = (FractionTrackingLink[]) arrayList.toArray(new FractionTrackingLink[arrayList.size()]);
            this.impressionUrls = urlToTrackingList(eVar.c());
            this.soundMuteUrls = trackingToTrackingList((List) a2.get(b.mute));
            this.soundUnmuteUrls = trackingToTrackingList((List) a2.get(b.unmute));
            this.videoPausedUrls = trackingToTrackingList((List) a2.get(b.pause));
            this.videoResumedUrls = trackingToTrackingList((List) a2.get(b.resume));
            this.videoSkippedUrls = trackingToTrackingList((List) a2.get(b.skip));
            this.videoPausedUrls = trackingToTrackingList((List) a2.get(b.pause));
            this.videoClosedUrls = trackingToTrackingList((List) a2.get(b.close));
            this.inlineErrorTrackingUrls = urlToTrackingList(eVar.d());
            this.videoClickTrackingUrls = urlToTrackingList(eVar.b().c());
            this.absoluteTrackingUrls = toAbsoluteTrackingList((List) a2.get(b.progress));
        }
    }

    public FractionTrackingLink[] getFractionTrackingUrls() {
        return this.fractionTrackingUrls;
    }

    public AbsoluteTrackingLink[] getAbsoluteTrackingUrls() {
        return this.absoluteTrackingUrls;
    }

    public ActionTrackingLink[] getImpressionUrls() {
        return this.impressionUrls;
    }

    public ActionTrackingLink[] getSoundUnmuteUrls() {
        return this.soundUnmuteUrls;
    }

    public ActionTrackingLink[] getCreativeViewUrls() {
        return this.creativeViewUrls;
    }

    public ActionTrackingLink[] getSoundMuteUrls() {
        return this.soundMuteUrls;
    }

    public ActionTrackingLink[] getVideoPausedUrls() {
        return this.videoPausedUrls;
    }

    public ActionTrackingLink[] getVideoResumedUrls() {
        return this.videoResumedUrls;
    }

    public ActionTrackingLink[] getVideoSkippedUrls() {
        return this.videoSkippedUrls;
    }

    public ActionTrackingLink[] getVideoClosedUrls() {
        return this.videoClosedUrls;
    }

    public ActionTrackingLink[] getVideoPostRollImpressionUrls() {
        return this.videoPostRollImpressionUrls;
    }

    public ActionTrackingLink[] getVideoPostRollClosedUrls() {
        return this.videoPostRollClosedUrls;
    }

    public ActionTrackingLink[] getVideoRewardedUrls() {
        return this.videoRewardedUrls;
    }

    public ActionTrackingLink[] getVideoClickTrackingUrls() {
        return this.videoClickTrackingUrls;
    }

    public ActionTrackingLink[] getInlineErrorTrackingUrls() {
        return this.inlineErrorTrackingUrls;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VideoTrackingDetails [fractionTrackingUrls=");
        sb.append(printTrackingLinks(this.fractionTrackingUrls));
        sb.append(", absoluteTrackingUrls=");
        sb.append(printTrackingLinks(this.absoluteTrackingUrls));
        sb.append(", impressionUrls=");
        sb.append(printTrackingLinks(this.impressionUrls));
        sb.append(", creativeViewUrls=");
        sb.append(printTrackingLinks(this.creativeViewUrls));
        sb.append(", soundMuteUrls=");
        sb.append(printTrackingLinks(this.soundMuteUrls));
        sb.append(", soundUnmuteUrls=");
        sb.append(printTrackingLinks(this.soundUnmuteUrls));
        sb.append(", videoPausedUrls=");
        sb.append(printTrackingLinks(this.videoPausedUrls));
        sb.append(", videoResumedUrls=");
        sb.append(printTrackingLinks(this.videoResumedUrls));
        sb.append(", videoSkippedUrls=");
        sb.append(printTrackingLinks(this.videoSkippedUrls));
        sb.append(", videoClosedUrls=");
        sb.append(printTrackingLinks(this.videoClosedUrls));
        sb.append(", videoPostRollImpressionUrls=");
        sb.append(printTrackingLinks(this.videoPostRollImpressionUrls));
        sb.append(", videoPostRollClosedUrls=");
        sb.append(printTrackingLinks(this.videoPostRollClosedUrls));
        sb.append(", videoRewardedUrls=");
        sb.append(printTrackingLinks(this.videoRewardedUrls));
        sb.append(", videoClickTrackingUrls=");
        sb.append(printTrackingLinks(this.videoClickTrackingUrls));
        sb.append(", inlineErrorTrackingUrls=");
        sb.append(printTrackingLinks(this.inlineErrorTrackingUrls));
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    private String printTrackingLinks(VideoTrackingLink[] videoTrackingLinkArr) {
        return videoTrackingLinkArr != null ? Arrays.toString(videoTrackingLinkArr) : "";
    }

    private static void addFractionUrls(List<f> list, int i, List<FractionTrackingLink> list2) {
        if (list != null) {
            for (f fVar : list) {
                FractionTrackingLink fractionTrackingLink = new FractionTrackingLink();
                fractionTrackingLink.setTrackingUrl(fVar.a());
                fractionTrackingLink.setFraction(i);
                fractionTrackingLink.setAppendReplayParameter(true);
                fractionTrackingLink.setReplayParameter("");
                list2.add(fractionTrackingLink);
            }
        }
    }

    private static ActionTrackingLink[] trackingToTrackingList(List<f> list) {
        if (list == null) {
            return new ActionTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (f fVar : list) {
            ActionTrackingLink actionTrackingLink = new ActionTrackingLink();
            actionTrackingLink.setTrackingUrl(fVar.a());
            actionTrackingLink.setAppendReplayParameter(true);
            actionTrackingLink.setReplayParameter("");
            arrayList.add(actionTrackingLink);
        }
        return (ActionTrackingLink[]) arrayList.toArray(new ActionTrackingLink[arrayList.size()]);
    }

    private static ActionTrackingLink[] urlToTrackingList(List<String> list) {
        if (list == null) {
            return new ActionTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (String str : list) {
            ActionTrackingLink actionTrackingLink = new ActionTrackingLink();
            actionTrackingLink.setTrackingUrl(str);
            actionTrackingLink.setAppendReplayParameter(true);
            actionTrackingLink.setReplayParameter("");
            arrayList.add(actionTrackingLink);
        }
        return (ActionTrackingLink[]) arrayList.toArray(new ActionTrackingLink[arrayList.size()]);
    }

    private AbsoluteTrackingLink[] toAbsoluteTrackingList(List<f> list) {
        if (list == null) {
            return new AbsoluteTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (f fVar : list) {
            AbsoluteTrackingLink absoluteTrackingLink = new AbsoluteTrackingLink();
            absoluteTrackingLink.setTrackingUrl(fVar.a());
            if (fVar.b() != -1) {
                absoluteTrackingLink.setVideoOffsetMillis(fVar.b());
            }
            absoluteTrackingLink.setAppendReplayParameter(true);
            absoluteTrackingLink.setReplayParameter("");
            arrayList.add(absoluteTrackingLink);
        }
        return (AbsoluteTrackingLink[]) arrayList.toArray(new AbsoluteTrackingLink[arrayList.size()]);
    }
}
