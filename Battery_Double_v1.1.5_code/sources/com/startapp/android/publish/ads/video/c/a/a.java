package com.startapp.android.publish.ads.video.c.a;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.explorestack.iab.vast.VastError;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.tapjoy.TJAdUnitConstants;

/* compiled from: StartAppSDK */
public enum a {
    ErrorNone(0),
    XMLParsingError(100),
    SchemaValidationError(101),
    VersionOfResponseNotSupported(102),
    TraffickingError(Callback.DEFAULT_DRAG_ANIMATION_DURATION),
    VideoPlayerExpectingDifferentLinearity(201),
    VideoPlayerExpectingDifferentDuration(VastError.ERROR_CODE_DURATION),
    VideoPlayerExpectingDifferentSize(203),
    AdCategoryRequired(204),
    GeneralWrapperError(VastError.ERROR_CODE_GENERAL_WRAPPER),
    WrapperTimeout(VastError.ERROR_CODE_BAD_URI),
    WrapperLimitReached(302),
    WrapperNoReponse(VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD),
    InlineResponseTimeout(304),
    GeneralLinearError(400),
    FileNotFound(VastError.ERROR_CODE_NO_FILE),
    TimeoutMediaFileURI(402),
    MediaNotSupported(VastError.ERROR_CODE_BAD_FILE),
    MediaFileDisplayError(VastError.ERROR_CODE_ERROR_SHOWING),
    MezzanineNotPovided(406),
    MezzanineDownloadInProgrees(407),
    ConditionalAdRejected(408),
    InteractiveCreativeFileNotExecuted(409),
    VerificationNotExecuted(410),
    MezzanineNotAsExpected(411),
    GeneralNonLinearAdsError(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL),
    CreativeTooLarge(IronSourceError.ERROR_CODE_NO_CONFIGURATION_AVAILABLE),
    ResourceDownloadFailed(IronSourceError.ERROR_CODE_USING_CACHED_CONFIGURATION),
    NonLinearResourceNotSupported(503),
    GeneralCompanionAdsError(600),
    CompanionTooLarge(IronSourceError.ERROR_BN_LOAD_AFTER_LONG_INITIATION),
    CompanionNotDisplay(IronSourceError.ERROR_BN_INIT_FAILED_AFTER_LOAD),
    CompanionFetchFailed(IronSourceError.ERROR_BN_LOAD_WHILE_LONG_INITIATION),
    CompanionNotSupported(IronSourceError.ERROR_BN_LOAD_PLACEMENT_CAPPED),
    UndefinedError(VastError.ERROR_CODE_UNKNOWN),
    GeneralVPAIDerror(901),
    SAShowBeforeVast(10000),
    SAProcessSuccess(20000);
    
    private int value;

    private a(int i) {
        this.value = i;
    }

    public final int a() {
        return this.value;
    }
}
