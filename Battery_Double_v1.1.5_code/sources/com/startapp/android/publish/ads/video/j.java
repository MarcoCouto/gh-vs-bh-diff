package com.startapp.android.publish.ads.video;

import android.content.Context;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.startapp.android.publish.adsCommon.activities.FullScreenActivity;
import com.startapp.android.publish.adsCommon.b;
import com.startapp.android.publish.adsCommon.c;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.android.publish.cache.i;
import java.io.DataInputStream;
import java.io.File;
import java.net.URL;

/* compiled from: StartAppSDK */
public final class j {

    /* renamed from: a reason: collision with root package name */
    private final String f3994a;
    private final String b;

    /* compiled from: StartAppSDK */
    public enum a {
        ELIGIBLE(""),
        INELIGIBLE_NO_STORAGE("Not enough storage for video"),
        INELIGIBLE_MISSING_ACTIVITY("FullScreenActivity not declared in AndroidManifest.xml"),
        INELIGIBLE_ERRORS_THRESHOLD_REACHED("Video errors threshold reached.");
        
        private String desctiption;

        private a(String str) {
            this.desctiption = str;
        }

        public final String a() {
            return this.desctiption;
        }
    }

    /* JADX WARNING: type inference failed for: r0v1, types: [java.io.DataInputStream, java.io.FileOutputStream, java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r6v0, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r4v0, types: [java.io.DataInputStream] */
    /* JADX WARNING: type inference failed for: r3v0, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r0v2, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r6v1, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r4v1, types: [java.io.DataInputStream] */
    /* JADX WARNING: type inference failed for: r3v1, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r4v2 */
    /* JADX WARNING: type inference failed for: r3v2 */
    /* JADX WARNING: type inference failed for: r6v2 */
    /* JADX WARNING: type inference failed for: r3v3 */
    /* JADX WARNING: type inference failed for: r4v3 */
    /* JADX WARNING: type inference failed for: r4v4 */
    /* JADX WARNING: type inference failed for: r3v4 */
    /* JADX WARNING: type inference failed for: r6v3 */
    /* JADX WARNING: type inference failed for: r3v5 */
    /* JADX WARNING: type inference failed for: r4v5 */
    /* JADX WARNING: type inference failed for: r1v5, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r3v7, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r4v6 */
    /* JADX WARNING: type inference failed for: r4v7 */
    /* JADX WARNING: type inference failed for: r4v8, types: [java.io.DataInputStream] */
    /* JADX WARNING: type inference failed for: r6v4 */
    /* JADX WARNING: type inference failed for: r6v5 */
    /* JADX WARNING: type inference failed for: r6v8, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r0v3 */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r6v9 */
    /* JADX WARNING: type inference failed for: r4v9 */
    /* JADX WARNING: type inference failed for: r3v8 */
    /* JADX WARNING: type inference failed for: r4v10 */
    /* JADX WARNING: type inference failed for: r3v9 */
    /* JADX WARNING: type inference failed for: r3v10 */
    /* JADX WARNING: type inference failed for: r4v11 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: type inference failed for: r3v12 */
    /* JADX WARNING: type inference failed for: r3v13 */
    /* JADX WARNING: type inference failed for: r3v14 */
    /* JADX WARNING: type inference failed for: r3v15 */
    /* JADX WARNING: type inference failed for: r3v16 */
    /* JADX WARNING: type inference failed for: r3v17 */
    /* JADX WARNING: type inference failed for: r4v12 */
    /* JADX WARNING: type inference failed for: r4v13 */
    /* JADX WARNING: type inference failed for: r4v14 */
    /* JADX WARNING: type inference failed for: r6v10 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v1, types: [java.io.DataInputStream, java.io.FileOutputStream, java.io.InputStream]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [?[OBJECT, ARRAY], java.io.InputStream, java.io.DataInputStream, java.io.FileOutputStream]
  mth insns count: 99
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 20 */
    public static String a(Context context, URL url, String str) {
        ? r6;
        ? r4;
        ? r3;
        ? r0;
        ? r62;
        ? r42;
        ? r32;
        ? r43;
        ? r33;
        ? r44;
        ? r45;
        ? r34;
        ? r46;
        new StringBuilder("Downloading video from ").append(url);
        ? r02 = 0;
        try {
            ? a2 = a(context, str);
            File file = new File(a2);
            if (file.exists()) {
                try {
                    r02.close();
                    r02.close();
                    r02.close();
                } catch (Exception unused) {
                }
                return a2;
            }
            ? openStream = url.openStream();
            try {
                ? dataInputStream = new DataInputStream(openStream);
                try {
                    byte[] bArr = new byte[4096];
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(".temp");
                    ? openFileOutput = context.openFileOutput(sb.toString(), 0);
                    while (true) {
                        try {
                            int read = dataInputStream.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            openFileOutput.write(bArr, 0, read);
                        } catch (Exception e) {
                            e = e;
                            r32 = openStream;
                            r42 = dataInputStream;
                            r62 = openFileOutput;
                            try {
                                Log.e("StartAppWall.VideoUtil", "Error downloading video from ".concat(String.valueOf(url)), e);
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(str);
                                sb2.append(".temp");
                                new File(a(context, sb2.toString())).delete();
                                try {
                                    r32.close();
                                    r42.close();
                                    r62.close();
                                    r0 = r02;
                                } catch (Exception unused2) {
                                    r0 = r02;
                                }
                                return r0;
                            } catch (Throwable th) {
                                th = th;
                                r6 = r62;
                                r4 = r42;
                                r3 = r32;
                                try {
                                    r3.close();
                                    r4.close();
                                    r6.close();
                                } catch (Exception unused3) {
                                }
                                throw th;
                            }
                        }
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append(".temp");
                    new File(a(context, sb3.toString())).renameTo(file);
                    try {
                        openStream.close();
                        dataInputStream.close();
                        openFileOutput.close();
                    } catch (Exception unused4) {
                    }
                    r0 = a2;
                } catch (Exception e2) {
                    e = e2;
                    r62 = r02;
                    r32 = openStream;
                    r42 = dataInputStream;
                    Log.e("StartAppWall.VideoUtil", "Error downloading video from ".concat(String.valueOf(url)), e);
                    StringBuilder sb22 = new StringBuilder();
                    sb22.append(str);
                    sb22.append(".temp");
                    new File(a(context, sb22.toString())).delete();
                    r32.close();
                    r42.close();
                    r62.close();
                    r0 = r02;
                    return r0;
                } catch (Throwable th2) {
                    th = th2;
                    r6 = r02;
                    r3 = openStream;
                    r4 = dataInputStream;
                    r3.close();
                    r4.close();
                    r6.close();
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                r44 = r02;
                r33 = openStream;
                r62 = r43;
                r42 = r43;
                r32 = r33;
                Log.e("StartAppWall.VideoUtil", "Error downloading video from ".concat(String.valueOf(url)), e);
                StringBuilder sb222 = new StringBuilder();
                sb222.append(str);
                sb222.append(".temp");
                new File(a(context, sb222.toString())).delete();
                r32.close();
                r42.close();
                r62.close();
                r0 = r02;
                return r0;
            } catch (Throwable th3) {
                th = th3;
                r46 = r02;
                r34 = openStream;
                r6 = r45;
                r4 = r45;
                r3 = r34;
                r3.close();
                r4.close();
                r6.close();
                throw th;
            }
            return r0;
        } catch (Exception e4) {
            e = e4;
            ? r35 = r02;
            r44 = r35;
            r33 = r35;
            r62 = r43;
            r42 = r43;
            r32 = r33;
            Log.e("StartAppWall.VideoUtil", "Error downloading video from ".concat(String.valueOf(url)), e);
            StringBuilder sb2222 = new StringBuilder();
            sb2222.append(str);
            sb2222.append(".temp");
            new File(a(context, sb2222.toString())).delete();
            r32.close();
            r42.close();
            r62.close();
            r0 = r02;
            return r0;
        } catch (Throwable th4) {
            th = th4;
            ? r36 = r02;
            r46 = r36;
            r34 = r36;
            r6 = r45;
            r4 = r45;
            r3 = r34;
            r3.close();
            r4.close();
            r6.close();
            throw th;
        }
    }

    public static a a(Context context) {
        a aVar = a.ELIGIBLE;
        boolean z = true;
        if (b.a().H().e() >= 0 && k.a(context, "videoErrorsCount", Integer.valueOf(0)).intValue() >= b.a().H().e()) {
            aVar = a.INELIGIBLE_ERRORS_THRESHOLD_REACHED;
        }
        if (!com.startapp.android.publish.adsCommon.a.j.a(context, FullScreenActivity.class)) {
            aVar = a.INELIGIBLE_MISSING_ACTIVITY;
        }
        long a2 = com.startapp.android.publish.adsCommon.a.j.a(context.getFilesDir());
        if (a2 < 0 || a2 / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID <= (b.a().H().c() << 10)) {
            z = false;
        }
        return !z ? a.INELIGIBLE_NO_STORAGE : aVar;
    }

    public static void b(Context context) {
        k.b(context, "videoErrorsCount", Integer.valueOf(k.a(context, "videoErrorsCount", Integer.valueOf(0)).intValue() + 1));
    }

    public static void a(Context context, i iVar) {
        if (iVar != null) {
            for (String b2 : iVar.c()) {
                c.b(context, b2);
            }
        }
    }

    static String a(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getFilesDir());
        sb.append("/");
        sb.append(str);
        return sb.toString();
    }

    private j(String str, String str2) {
        this.f3994a = str;
        this.b = str2;
    }

    public static j a(String str, String str2) {
        com.b.a.a.a.b.b(str, "Name is null or empty");
        com.b.a.a.a.b.b(str2, "Version is null or empty");
        return new j(str, str2);
    }

    public final String a() {
        return this.f3994a;
    }

    public final String b() {
        return this.b;
    }
}
