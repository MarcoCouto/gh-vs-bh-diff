package com.startapp.android.publish.ads.video.c.a;

import android.content.Context;
import com.startapp.common.a.e;
import java.util.Comparator;

/* compiled from: StartAppSDK */
public final class d extends c {
    /* access modifiers changed from: private */
    public final double c;
    /* access modifiers changed from: private */
    public final int d = (this.f3963a * this.b);
    /* access modifiers changed from: private */
    public final int e;

    public d(Context context, int i) {
        super(context);
        this.e = i;
        double d2 = (double) this.f3963a;
        double d3 = (double) this.b;
        Double.isNaN(d2);
        Double.isNaN(d3);
        this.c = d2 / d3;
    }

    /* access modifiers changed from: protected */
    public final Comparator<e> a() {
        return new Comparator<e>() {
            public final /* synthetic */ int compare(Object obj, Object obj2) {
                e eVar = (e) obj;
                e eVar2 = (e) obj2;
                double doubleValue = d.a(eVar.d().intValue(), eVar.e().intValue(), d.this.c, d.this.d).doubleValue();
                double doubleValue2 = d.a(eVar2.d().intValue(), eVar2.e().intValue(), d.this.c, d.this.d).doubleValue();
                if (doubleValue < doubleValue2) {
                    return -1;
                }
                if (doubleValue > doubleValue2) {
                    return 1;
                }
                Integer c = eVar.c();
                Integer c2 = eVar2.c();
                if (c == null && c2 == null) {
                    return 0;
                }
                if (c == null) {
                    return 1;
                }
                if (c2 == null) {
                    return -1;
                }
                Integer valueOf = Integer.valueOf(Math.abs(d.this.e - c.intValue()));
                Integer valueOf2 = Integer.valueOf(Math.abs(d.this.e - c2.intValue()));
                int intValue = valueOf.intValue();
                int intValue2 = valueOf2.intValue();
                if (intValue < intValue2) {
                    return -1;
                }
                if (intValue == intValue2) {
                    return 0;
                }
                return 1;
            }
        };
    }

    static /* synthetic */ Double a(int i, int i2, double d2, int i3) {
        double d3 = (double) i;
        double d4 = (double) i2;
        Double.isNaN(d3);
        Double.isNaN(d4);
        double d5 = (d3 / d4) / d2;
        double d6 = (double) (i * i2);
        double d7 = (double) i3;
        Double.isNaN(d6);
        Double.isNaN(d7);
        return Double.valueOf((Math.abs(Math.log(d5)) * 40.0d) + (Math.abs(Math.log(d6 / d7)) * 60.0d));
    }
}
