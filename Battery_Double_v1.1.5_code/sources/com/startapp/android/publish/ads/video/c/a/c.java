package com.startapp.android.publish.ads.video.c.a;

import android.content.Context;
import android.util.DisplayMetrics;
import com.startapp.common.a.e;
import com.startapp.common.a.g;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/* compiled from: StartAppSDK */
public class c {

    /* renamed from: a reason: collision with root package name */
    protected int f3963a;
    protected int b;
    /* access modifiers changed from: private */
    public int c = (this.f3963a * this.b);

    /* compiled from: StartAppSDK */
    class a implements Comparator<e> {
        private a() {
        }

        /* synthetic */ a(c cVar, byte b) {
            this();
        }

        public final /* synthetic */ int compare(Object obj, Object obj2) {
            e eVar = (e) obj;
            e eVar2 = (e) obj2;
            int intValue = eVar.d().intValue() * eVar.e().intValue();
            int intValue2 = eVar2.d().intValue() * eVar2.e().intValue();
            int abs = Math.abs(intValue - c.this.c);
            int abs2 = Math.abs(intValue2 - c.this.c);
            if (abs < abs2) {
                return -1;
            }
            return abs > abs2 ? 1 : 0;
        }
    }

    public c(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.f3963a = displayMetrics.widthPixels;
        this.b = displayMetrics.heightPixels;
        if (!g.a(context).equals("WIFI")) {
            this.c = (int) (((float) this.c) * 0.75f);
        }
    }

    /* access modifiers changed from: protected */
    public Comparator<e> a() {
        return new a(this, 0);
    }

    public final e a(List<e> list) {
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                if (!eVar.f() || !eVar.b().matches("video/.*(?i)(mp4|3gpp|mp2t|webm|matroska)")) {
                    it.remove();
                }
            }
            if (list.size() != 0) {
                Collections.sort(list, a());
                if (list == null || list.size() <= 0) {
                    return null;
                }
                return (e) list.get(0);
            }
        }
        return null;
    }
}
