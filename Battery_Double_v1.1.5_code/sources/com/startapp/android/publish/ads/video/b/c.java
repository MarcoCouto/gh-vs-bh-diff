package com.startapp.android.publish.ads.video.b;

/* compiled from: StartAppSDK */
public interface c {

    /* compiled from: StartAppSDK */
    public interface a {
    }

    /* compiled from: StartAppSDK */
    public interface b {
    }

    /* renamed from: com.startapp.android.publish.ads.video.b.c$c reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public interface C0086c {
        void g(int i);
    }

    /* compiled from: StartAppSDK */
    public interface d {
        void S();
    }

    /* compiled from: StartAppSDK */
    public interface e {
        boolean b(g gVar);
    }

    /* compiled from: StartAppSDK */
    public interface f {
        void R();
    }

    /* compiled from: StartAppSDK */
    public static class g {

        /* renamed from: a reason: collision with root package name */
        private h f3956a;
        private String b;
        private int c;

        public g(h hVar, String str, int i) {
            this.f3956a = hVar;
            this.b = str;
            this.c = i;
        }

        public final h a() {
            return this.f3956a;
        }

        public final String b() {
            return this.b;
        }

        public final int c() {
            return this.c;
        }
    }

    /* compiled from: StartAppSDK */
    public enum h {
        UNKNOWN,
        SERVER_DIED,
        BUFFERING_TIMEOUT,
        PLAYER_CREATION
    }

    void a();

    void a(int i);

    void a(a aVar);

    void a(b bVar);

    void a(C0086c cVar);

    void a(d dVar);

    void a(e eVar);

    void a(f fVar);

    void a(String str);

    void a(boolean z);

    void b();

    void c();

    int d();

    int e();

    boolean f();

    void g();
}
