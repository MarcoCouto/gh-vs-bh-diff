package com.startapp.android.publish.ads.video.c.b;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import com.explorestack.iab.vast.tags.VastTagName;
import com.startapp.android.publish.ads.video.c.a.c;
import com.startapp.android.publish.ads.video.c.a.e;
import com.startapp.android.publish.adsCommon.Ad.AnonymousClass2;
import com.startapp.android.publish.adsCommon.k;
import com.startapp.common.a.g;
import com.tapjoy.TJAdUnitConstants;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/* compiled from: StartAppSDK */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private final int f3968a;
    private final int b;
    private e c;
    private StringBuilder d = new StringBuilder(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
    private long e = -1;

    public a(int i, int i2) {
        this.f3968a = i;
        this.b = i2;
    }

    public final e a() {
        return this.c;
    }

    public final com.startapp.android.publish.ads.video.c.a.a a(Context context, String str, c cVar) {
        Document document = null;
        this.c = null;
        this.e = System.currentTimeMillis();
        com.startapp.android.publish.ads.video.c.a.a a2 = a(context, str, 0);
        if (a2 == com.startapp.android.publish.ads.video.c.a.a.XMLParsingError) {
            new StringBuilder("processXml error ").append(a2);
            return com.startapp.android.publish.ads.video.c.a.a.XMLParsingError;
        }
        String sb = this.d.toString();
        if (sb != null && sb.length() > 0) {
            StringBuilder sb2 = new StringBuilder("<VASTS>");
            sb2.append(sb);
            sb2.append("</VASTS>");
            document = AnonymousClass2.a(sb2.toString());
        }
        if (document == null) {
            new StringBuilder("wrapMergedVastDocWithVasts error ").append(a2);
            return com.startapp.android.publish.ads.video.c.a.a.XMLParsingError;
        }
        this.c = new e(document);
        if (!this.c.a(cVar)) {
            new StringBuilder("validate error ").append(a2);
            if (a2 == com.startapp.android.publish.ads.video.c.a.a.ErrorNone) {
                a2 = com.startapp.android.publish.ads.video.c.a.a.MediaNotSupported;
            }
        }
        return a2;
    }

    @VisibleForTesting
    private com.startapp.android.publish.ads.video.c.a.a a(Context context, String str, int i) {
        String str2;
        if (i >= this.f3968a) {
            new StringBuilder("VAST wrapping exceeded max limit of ").append(this.f3968a);
            return com.startapp.android.publish.ads.video.c.a.a.WrapperLimitReached;
        } else if (System.currentTimeMillis() - this.e > ((long) this.b) && this.e > 0) {
            return com.startapp.android.publish.ads.video.c.a.a.WrapperTimeout;
        } else {
            Document a2 = AnonymousClass2.a(str);
            if (a2 != null) {
                a2.getDocumentElement().normalize();
            }
            if (a2 == null) {
                return com.startapp.android.publish.ads.video.c.a.a.XMLParsingError;
            }
            if (a2 != null) {
                NodeList elementsByTagName = a2.getElementsByTagName(VastTagName.VAST);
                if (elementsByTagName != null && elementsByTagName.getLength() > 0) {
                    str2 = AnonymousClass2.a(elementsByTagName.item(0));
                    if (a2.getChildNodes().getLength() != 0 || a2.getChildNodes().item(0).getChildNodes().getLength() == 0 || str2 == null) {
                        return com.startapp.android.publish.ads.video.c.a.a.WrapperNoReponse;
                    }
                    this.d.append(str2);
                    NodeList elementsByTagName2 = a2.getElementsByTagName(VastTagName.VAST_AD_TAG_URI);
                    if (elementsByTagName2 == null || elementsByTagName2.getLength() == 0) {
                        return com.startapp.android.publish.ads.video.c.a.a.ErrorNone;
                    }
                    try {
                        com.startapp.common.a.g.a a3 = g.a(AnonymousClass2.b(elementsByTagName2.item(0)).replace(" ", "%20"), k.a(context, "User-Agent", "-1"), false);
                        if (a3.a() != null) {
                            return a(context, a3.a(), i + 1);
                        }
                        return com.startapp.android.publish.ads.video.c.a.a.WrapperNoReponse;
                    } catch (Exception unused) {
                        return com.startapp.android.publish.ads.video.c.a.a.GeneralWrapperError;
                    }
                }
            }
            str2 = null;
            if (a2.getChildNodes().getLength() != 0) {
            }
            return com.startapp.android.publish.ads.video.c.a.a.WrapperNoReponse;
        }
    }
}
