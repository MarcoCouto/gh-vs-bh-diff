package com.startapp.android.publish.ads.video;

import android.content.Context;
import com.startapp.android.publish.ads.b.c;
import com.startapp.android.publish.ads.splash.SplashConfig.Orientation;
import com.startapp.android.publish.ads.video.c.a.e;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.common.c.b;

/* compiled from: StartAppSDK */
public final class g extends c {
    private static final long serialVersionUID = 1;
    private f videoAdDetails = null;

    public g(Context context) {
        super(context, Placement.INAPP_OVERLAY);
    }

    /* access modifiers changed from: protected */
    public final void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
        new b(this.context, this, adPreferences, adEventListener).c();
    }

    public final void setHtml(String str) {
        super.setHtml(str);
        String extractMetadata = extractMetadata(str, "@videoJson@");
        if (extractMetadata != null) {
            this.videoAdDetails = (f) b.a(extractMetadata, f.class);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return this.videoAdDetails != null;
    }

    public final f b() {
        return this.videoAdDetails;
    }

    public final void a(e eVar, boolean z) {
        if (eVar != null) {
            this.videoAdDetails = new f(eVar, z);
            com.startapp.common.a.e f = eVar.f();
            if (f != null) {
                if (f.d().intValue() > f.e().intValue()) {
                    setOrientation(Orientation.LANDSCAPE);
                    return;
                }
                setOrientation(Orientation.PORTRAIT);
            }
        }
    }

    public final void c() {
        this.videoAdDetails = null;
    }
}
