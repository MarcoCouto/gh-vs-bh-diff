package com.startapp.android.publish.ads.video;

import android.content.Context;
import android.util.Base64;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.startapp.android.publish.adsCommon.b;
import com.startapp.android.publish.cache.a;
import com.startapp.android.publish.cache.g;
import com.startapp.android.publish.cache.h;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: StartAppSDK */
public final class e {

    /* renamed from: a reason: collision with root package name */
    private static e f3969a = new e();
    private LinkedList<h> b = new LinkedList<>();

    private e() {
    }

    private boolean a(int i) {
        boolean z;
        Iterator it = this.b.iterator();
        boolean z2 = false;
        while (it.hasNext() && this.b.size() > i) {
            h hVar = (h) it.next();
            String b2 = hVar.b();
            Iterator it2 = a.a().c().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                g gVar = (g) it2.next();
                if (gVar.b() instanceof g) {
                    g gVar2 = (g) gVar.b();
                    if (!(gVar2.b() == null || gVar2.b().getLocalVideoPath() == null || !gVar2.b().getLocalVideoPath().equals(b2))) {
                        z = true;
                        break;
                    }
                }
            }
            if (!z) {
                it.remove();
                if (hVar.b() != null) {
                    new File(hVar.b()).delete();
                    StringBuilder sb = new StringBuilder("cachedVideoAds reached the maximum of ");
                    sb.append(i);
                    sb.append(" videos - removed ");
                    sb.append(hVar.a());
                    sb.append(" Size = ");
                    sb.append(this.b.size());
                }
                z2 = true;
            }
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, h hVar) {
        if (this.b.contains(hVar)) {
            this.b.remove(hVar);
            StringBuilder sb = new StringBuilder("cachedVideoAds already contained ");
            sb.append(hVar.a());
            sb.append(" - removed. Size = ");
            sb.append(this.b.size());
        }
        a(b.a().H().b() - 1);
        this.b.add(hVar);
        a(context);
        StringBuilder sb2 = new StringBuilder("Added ");
        sb2.append(hVar.a());
        sb2.append(" to cachedVideoAds. Size = ");
        sb2.append(this.b.size());
    }

    private void a(Context context) {
        com.startapp.common.a.e.b(context, "CachedAds", this.b);
    }

    public static e a() {
        return f3969a;
    }

    static /* synthetic */ void a(e eVar, final Context context, String str, final i.a aVar, final c.a aVar2) {
        String str2;
        if (eVar.b == null) {
            Class<LinkedList> cls = LinkedList.class;
            eVar.b = (LinkedList) com.startapp.common.a.e.a(context, "CachedAds");
            if (eVar.b == null) {
                eVar.b = new LinkedList<>();
            }
            if (eVar.a(b.a().H().b())) {
                eVar.a(context);
            }
        }
        try {
            URL url = new URL(str);
            StringBuilder sb = new StringBuilder();
            sb.append(url.getHost());
            sb.append(url.getPath().replace("/", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR));
            String sb2 = sb.toString();
            try {
                String substring = sb2.substring(0, sb2.lastIndexOf(46));
                String substring2 = sb2.substring(sb2.lastIndexOf(46));
                StringBuilder sb3 = new StringBuilder();
                sb3.append(new String(Base64.encodeToString(MessageDigest.getInstance(CommonMD5.TAG).digest(substring.getBytes()), 0)).replaceAll("[^a-zA-Z0-9]+", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR));
                sb3.append(substring2);
                str2 = sb3.toString();
            } catch (NoSuchAlgorithmException unused) {
                str2 = sb2;
            }
            final h hVar = new h(str2);
            i iVar = new i(context, url, str2, new i.a() {
                public final void a(String str) {
                    if (aVar != null) {
                        aVar.a(str);
                    }
                    if (str != null) {
                        hVar.a(System.currentTimeMillis());
                        hVar.a(str);
                        e.this.a(context, hVar);
                    }
                }
            }, new c.a() {
                public final void a(String str) {
                    if (aVar2 != null) {
                        aVar2.a(str);
                    }
                }
            });
            iVar.a();
        } catch (MalformedURLException unused2) {
            if (aVar != null) {
                aVar.a(null);
            }
        }
    }
}
