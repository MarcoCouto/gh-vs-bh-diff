package com.startapp.android.publish.ads.video;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.android.publish.adsCommon.b;
import java.net.URL;

/* compiled from: StartAppSDK */
public final class i {

    /* renamed from: a reason: collision with root package name */
    protected a f3992a;
    private Context b;
    private URL c;
    private String d;
    private com.startapp.android.publish.ads.video.c.a e;

    /* compiled from: StartAppSDK */
    public interface a {
        void a(String str);
    }

    public i(Context context, URL url, String str, a aVar, com.startapp.android.publish.ads.video.c.a aVar2) {
        this.b = context;
        this.c = url;
        this.d = str;
        this.f3992a = aVar;
        this.e = aVar2;
    }

    public final void a() {
        final String str;
        try {
            str = b.a().H().i() ? b.f3967a.a(this.b, this.c, this.d, this.e) : j.a(this.b, this.c, this.d);
        } catch (Exception unused) {
            str = null;
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                if (i.this.f3992a != null) {
                    i.this.f3992a.a(str);
                }
            }
        });
    }
}
