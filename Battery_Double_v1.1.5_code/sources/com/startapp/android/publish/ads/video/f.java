package com.startapp.android.publish.ads.video;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.video.tracking.VideoTrackingDetails;
import com.startapp.android.publish.b.c;
import com.startapp.common.c.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class f implements Serializable {
    private static final long serialVersionUID = 1;
    @e(b = c.class, f = "adVerifications")
    private c[] adVerifications;
    private String clickUrl;
    private boolean clickable;
    private boolean closeable;
    private boolean isVideoMuted;
    private String localVideoPath;
    @e(b = a.class)
    private a postRoll;
    private boolean skippable;
    private int skippableAfter;
    @e(a = true)
    private VideoTrackingDetails videoTrackingDetails;
    private String videoUrl;

    /* compiled from: StartAppSDK */
    public enum a {
        IMAGE,
        LAST_FRAME,
        NONE
    }

    public f() {
    }

    public f(com.startapp.android.publish.ads.video.c.a.e eVar, boolean z) {
        if (eVar != null) {
            this.videoTrackingDetails = new VideoTrackingDetails(eVar);
            if (eVar.f() != null) {
                this.videoUrl = eVar.f().a();
            }
            boolean z2 = true;
            if (z) {
                this.skippableAfter = eVar.e();
                this.skippable = this.skippableAfter != Integer.MAX_VALUE;
            } else {
                this.skippable = false;
            }
            this.clickUrl = eVar.b().b();
            if (this.clickUrl == null) {
                z2 = false;
            }
            this.clickable = z2;
            this.postRoll = a.NONE;
            setAdVerifications(eVar.g());
        }
    }

    public String getVideoUrl() {
        return this.videoUrl;
    }

    public String getLocalVideoPath() {
        return this.localVideoPath;
    }

    public void setLocalVideoPath(String str) {
        this.localVideoPath = str;
    }

    public a getPostRollType() {
        return this.postRoll;
    }

    public boolean isCloseable() {
        return this.closeable;
    }

    public boolean isSkippable() {
        return this.skippable;
    }

    public int getSkippableAfter() {
        return this.skippableAfter;
    }

    public boolean isClickable() {
        return this.clickable;
    }

    public VideoTrackingDetails getVideoTrackingDetails() {
        return this.videoTrackingDetails;
    }

    public boolean isVideoMuted() {
        return this.isVideoMuted;
    }

    public void setVideoMuted(boolean z) {
        this.isVideoMuted = z;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public com.startapp.android.publish.b.a getAdVerifications() {
        return new com.startapp.android.publish.b.a(this.adVerifications);
    }

    public void setAdVerifications(com.startapp.android.publish.b.a aVar) {
        if (aVar != null && aVar.getAdVerification() != null) {
            this.adVerifications = (c[]) aVar.getAdVerification().toArray(new c[aVar.getAdVerification().size()]);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VideoAdDetails [videoUrl=");
        sb.append(this.videoUrl);
        sb.append(", localVideoPath=");
        sb.append(this.localVideoPath);
        sb.append(", postRoll=");
        sb.append(this.postRoll);
        sb.append(", closeable=");
        sb.append(this.closeable);
        sb.append(", skippable=");
        sb.append(this.skippable);
        sb.append(", skippableAfter=");
        sb.append(this.skippableAfter);
        sb.append(", videoTrackingDetails= ");
        sb.append(this.videoTrackingDetails);
        sb.append(", isVideoMuted= ");
        sb.append(this.isVideoMuted);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
