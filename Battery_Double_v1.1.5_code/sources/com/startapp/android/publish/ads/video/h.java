package com.startapp.android.publish.ads.video;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
import com.facebook.appevents.codeless.internal.Constants;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.ads.video.b.c.C0086c;
import com.startapp.android.publish.ads.video.b.c.d;
import com.startapp.android.publish.ads.video.b.c.e;
import com.startapp.android.publish.ads.video.b.c.f;
import com.startapp.android.publish.ads.video.b.c.g;
import com.startapp.android.publish.ads.video.tracking.AbsoluteTrackingLink;
import com.startapp.android.publish.ads.video.tracking.ActionTrackingLink;
import com.startapp.android.publish.ads.video.tracking.FractionTrackingLink;
import com.startapp.android.publish.ads.video.tracking.VideoClickedTrackingParams;
import com.startapp.android.publish.ads.video.tracking.VideoClickedTrackingParams.ClickOrigin;
import com.startapp.android.publish.ads.video.tracking.VideoPausedTrackingParams;
import com.startapp.android.publish.ads.video.tracking.VideoPausedTrackingParams.PauseOrigin;
import com.startapp.android.publish.ads.video.tracking.VideoProgressTrackingParams;
import com.startapp.android.publish.ads.video.tracking.VideoTrackingLink;
import com.startapp.android.publish.ads.video.tracking.VideoTrackingParams;
import com.startapp.android.publish.adsCommon.Ad.AdType;
import com.startapp.android.publish.adsCommon.AdsConstants;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener.NotDisplayedReason;
import com.startapp.android.publish.html.JsInterface;
import com.tapjoy.TJAdUnitConstants.String;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class h extends com.startapp.android.publish.ads.a.c implements com.startapp.android.publish.ads.video.b.c.a, com.startapp.android.publish.ads.video.b.c.b, C0086c, d, e, f, com.startapp.common.a.c.a {
    private boolean A = false;
    private boolean B = false;
    private HashMap<Integer, Boolean> C = new HashMap<>();
    private HashMap<Integer, Boolean> D = new HashMap<>();
    private int E = 1;
    private boolean F = false;
    private boolean G = false;
    private int H = 0;
    private boolean I = false;
    private boolean J = false;
    private boolean K = false;
    private int L = 0;
    private int M;
    private String N = null;
    private Handler O = new Handler();
    private Map<Integer, List<FractionTrackingLink>> P = new HashMap();
    private Map<Integer, List<AbsoluteTrackingLink>> Q = new HashMap();
    private long R;
    private ClickOrigin S;
    private long T;
    /* access modifiers changed from: private */
    public i U;
    private boolean V = false;
    /* access modifiers changed from: private */
    public BroadcastReceiver W = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            if (!h.this.W.isInitialStickyBroadcast() && h.this.m != h.this.X()) {
                h.this.m = !h.this.m;
                h.this.P();
                h.this.a(h.this.m);
            }
        }
    };
    protected com.startapp.android.publish.ads.video.b.c j;
    protected VideoView k;
    protected ProgressBar l;
    protected boolean m = false;
    protected int n = 0;
    protected boolean o;
    protected boolean p = false;
    protected boolean q = false;
    protected boolean r = false;
    protected boolean s = false;
    protected Handler t = new Handler();
    protected Handler u = new Handler();
    protected Handler v = new Handler();
    private RelativeLayout w;
    private RelativeLayout x;
    private int y = 0;
    private int z = 0;

    /* compiled from: StartAppSDK */
    enum a {
        PLAYER,
        POST_ROLL
    }

    /* compiled from: StartAppSDK */
    enum b {
        ON,
        OFF
    }

    /* compiled from: StartAppSDK */
    enum c {
        ;
        

        /* renamed from: a reason: collision with root package name */
        public static final int f3991a = 1;
        public static final int b = 2;
        public static final int c = 3;

        static {
            int[] iArr = {1, 2, 3};
        }
    }

    /* access modifiers changed from: protected */
    public final void y() {
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        try {
            this.R = System.currentTimeMillis();
            this.M = 100 / com.startapp.android.publish.adsCommon.b.a().H().j();
            boolean z2 = true;
            if (g().equals("back")) {
                if (com.startapp.android.publish.adsCommon.b.a().H().a().equals(com.startapp.android.publish.adsCommon.n.a.BOTH)) {
                    this.F = true;
                    this.G = true;
                } else if (com.startapp.android.publish.adsCommon.b.a().H().a().equals(com.startapp.android.publish.adsCommon.n.a.SKIP)) {
                    this.F = true;
                    this.G = false;
                } else if (com.startapp.android.publish.adsCommon.b.a().H().a().equals(com.startapp.android.publish.adsCommon.n.a.CLOSE)) {
                    this.F = false;
                    this.G = true;
                } else {
                    com.startapp.android.publish.adsCommon.b.a().H().a().equals(com.startapp.android.publish.adsCommon.n.a.DISABLED);
                    this.F = false;
                    this.G = false;
                }
            }
            FractionTrackingLink[] fractionTrackingUrls = N().getVideoTrackingDetails().getFractionTrackingUrls();
            if (fractionTrackingUrls != null) {
                for (FractionTrackingLink fractionTrackingLink : fractionTrackingUrls) {
                    List list = (List) this.P.get(Integer.valueOf(fractionTrackingLink.getFraction()));
                    if (list == null) {
                        list = new ArrayList();
                        this.P.put(Integer.valueOf(fractionTrackingLink.getFraction()), list);
                    }
                    list.add(fractionTrackingLink);
                }
            }
            AbsoluteTrackingLink[] absoluteTrackingUrls = N().getVideoTrackingDetails().getAbsoluteTrackingUrls();
            if (absoluteTrackingUrls != null) {
                for (AbsoluteTrackingLink absoluteTrackingLink : absoluteTrackingUrls) {
                    List list2 = (List) this.Q.get(Integer.valueOf(absoluteTrackingLink.getVideoOffsetMillis()));
                    if (list2 == null) {
                        list2 = new ArrayList();
                        this.Q.put(Integer.valueOf(absoluteTrackingLink.getVideoOffsetMillis()), list2);
                    }
                    list2.add(absoluteTrackingLink);
                }
            }
            if (!X() && !N().isVideoMuted()) {
                if (!com.startapp.android.publish.adsCommon.b.a().H().m().equals("muted")) {
                    z2 = false;
                }
            }
            this.m = z2;
            if (bundle != null && bundle.containsKey("currentPosition")) {
                this.n = bundle.getInt("currentPosition");
                this.y = bundle.getInt("latestPosition");
                this.C = (HashMap) bundle.getSerializable("fractionProgressImpressionsSent");
                this.D = (HashMap) bundle.getSerializable("absoluteProgressImpressionsSent");
                this.m = bundle.getBoolean(String.IS_MUTED);
                this.o = bundle.getBoolean("shouldSetBg");
                this.E = bundle.getInt("pauseNum");
            }
        } catch (Exception unused) {
            al();
            StringBuilder sb = new StringBuilder("packages : ");
            sb.append(j());
            com.startapp.android.publish.adsCommon.g.f.a(b().getApplicationContext(), com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "VideoMode.onCreate", sb.toString(), "");
            n();
        }
    }

    public final void a(WebView webView) {
        super.a(webView);
        webView.setBackgroundColor(33554431);
        if (VERSION.SDK_INT >= 11) {
            webView.setLayerType(1, null);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        this.A = true;
        if (this.B && am()) {
            G();
        } else if (ag()) {
            b((View) this.c);
        }
        if (an()) {
            U();
        }
        if (ag()) {
            Z();
        }
        f N2 = N();
        if (com.startapp.android.publish.common.metaData.e.getInstance().isOmsdkEnabled() && this.d == null && N2 != null && N2.getAdVerifications() != null && N2.getAdVerifications().getAdVerification() != null) {
            this.d = com.b.a.a.a.b.a(this.c.getContext(), N().getAdVerifications());
            if (this.d != null) {
                this.U = i.a(this.d);
                View a2 = this.f3862a.a();
                if (a2 != null) {
                    this.d.b(a2);
                }
                this.d.b(this.c);
                this.d.b(this.x);
                this.d.a(this.k);
                this.d.a();
                com.b.a.a.a.b.a.a(this.d).a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void G() {
        if (this.p) {
            b((View) this.k);
            if (!ag()) {
                aa();
            }
        }
    }

    public final void s() {
        super.s();
        b().registerReceiver(this.W, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
        this.V = true;
        if (!b().isFinishing()) {
            if (this.k == null) {
                Context applicationContext = b().getApplicationContext();
                this.T = System.currentTimeMillis();
                this.x = (RelativeLayout) b().findViewById(AdsConstants.STARTAPP_AD_MAIN_LAYOUT_ID);
                LayoutParams layoutParams = new LayoutParams(-1, -1);
                this.k = new VideoView(applicationContext);
                this.k.setId(100);
                LayoutParams layoutParams2 = new LayoutParams(-1, -1);
                layoutParams2.addRule(13);
                this.l = new ProgressBar(applicationContext, null, 16843399);
                this.l.setVisibility(4);
                LayoutParams layoutParams3 = new LayoutParams(-2, -2);
                layoutParams3.addRule(14);
                layoutParams3.addRule(15);
                this.w = new RelativeLayout(applicationContext);
                this.w.setId(1475346436);
                b().setContentView(this.w);
                this.w.addView(this.k, layoutParams2);
                this.w.addView(this.x, layoutParams);
                this.w.addView(this.l, layoutParams3);
                if (AdsConstants.a().booleanValue()) {
                    LayoutParams layoutParams4 = new LayoutParams(-2, -2);
                    layoutParams4.addRule(12);
                    layoutParams4.addRule(14);
                    RelativeLayout relativeLayout = this.w;
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sb2 = new StringBuilder("url=");
                    sb2.append(N().getVideoUrl());
                    sb.append(sb2.toString());
                    TextView textView = new TextView(applicationContext);
                    textView.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                    com.startapp.common.a.c.a((View) textView, 0.5f);
                    textView.setTextColor(-7829368);
                    textView.setSingleLine(false);
                    textView.setText(sb.toString());
                    relativeLayout.addView(textView, layoutParams4);
                }
                this.f3862a.a().setVisibility(4);
            }
            if (this.j == null) {
                this.j = new com.startapp.android.publish.ads.video.b.b(this.k);
            }
            this.B = false;
            this.w.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            H();
            if (ag()) {
                this.f3862a.a().setVisibility(0);
                this.k.setVisibility(4);
            } else if (this.n != 0) {
                this.j.a(this.n);
                PauseOrigin pauseOrigin = PauseOrigin.EXTERNAL;
                new StringBuilder("Sending resume event with pause origin: ").append(pauseOrigin);
                ActionTrackingLink[] videoResumedUrls = N().getVideoTrackingDetails().getVideoResumedUrls();
                VideoPausedTrackingParams videoPausedTrackingParams = new VideoPausedTrackingParams(l(), i(this.y), this.h, this.E, pauseOrigin, this.N);
                a(videoResumedUrls, videoPausedTrackingParams, this.y, "resumed");
                this.E++;
            }
            this.j.a((f) this);
            this.j.a((d) this);
            this.j.a((e) this);
            this.j.a((com.startapp.android.publish.ads.video.b.c.b) this);
            this.j.a((C0086c) this);
            this.j.a((com.startapp.android.publish.ads.video.b.c.a) this);
            com.startapp.common.a.c.a((View) this.k, (com.startapp.common.a.c.a) this);
        }
    }

    /* access modifiers changed from: protected */
    public final void H() {
        boolean i = com.startapp.android.publish.adsCommon.b.a().H().i();
        String localVideoPath = N().getLocalVideoPath();
        if (localVideoPath != null) {
            this.j.a(localVideoPath);
            if (i) {
                b.f3967a;
                if (c.b(localVideoPath)) {
                    this.s = true;
                    this.K = true;
                    this.H = com.startapp.android.publish.adsCommon.b.a().H().k();
                }
            }
        } else if (i) {
            String videoUrl = N().getVideoUrl();
            b.f3967a.a(videoUrl);
            this.j.a(videoUrl);
            this.s = true;
            V();
        } else {
            h(c.c);
        }
        if (this.N == null) {
            this.N = this.s ? "2" : "1";
        }
    }

    private void U() {
        this.J = true;
        ab();
        if (ag()) {
            this.j.b();
            return;
        }
        new Handler().postDelayed(new Runnable() {
            public final void run() {
                if (h.this.j != null) {
                    h.this.j.a();
                    if (h.this.U != null) {
                        h.this.U.a((float) h.this.j.e(), h.this.m ? 0.0f : 1.0f);
                    }
                    h.this.p = true;
                    h.this.I();
                    new Handler().post(new Runnable() {
                        public final void run() {
                            h.this.G();
                        }
                    });
                }
            }
        }, Y());
        if (this.n == 0) {
            this.t.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (h.this.j != null) {
                            if (h.this.j.d() > 0) {
                                h.this.e(0);
                                h.this.f(0);
                                if (h.this.h == 0) {
                                    h.this.Q();
                                    com.startapp.common.b.a((Context) h.this.b()).a(new Intent("com.startapp.android.ShowDisplayBroadcastListener"));
                                }
                            } else if (!h.this.q) {
                                h.this.t.postDelayed(this, 100);
                            }
                        }
                    } catch (Exception e) {
                        com.startapp.android.publish.adsCommon.g.f.a(h.this.b().getApplicationContext(), new com.startapp.android.publish.adsCommon.g.e(com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "VideoMode.startVideoPlayback", e.getMessage()), h.this.ap());
                        h.this.n();
                    }
                }
            }, 100);
        }
        ah();
        ak();
        ac();
        ad();
        this.f3862a.a().setVisibility(4);
        P();
    }

    private void V() {
        if (!W()) {
            this.r = false;
            this.v.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        h.this.l.setVisibility(0);
                        if (h.this.U != null) {
                            h.this.U.f();
                        }
                        h.this.v.postDelayed(new Runnable() {
                            public final void run() {
                                try {
                                    h.this.I();
                                    h.this.r = true;
                                    h.this.a(new g(com.startapp.android.publish.ads.video.b.c.h.BUFFERING_TIMEOUT, "Buffering timeout reached", h.this.n));
                                } catch (Exception e) {
                                    com.startapp.android.publish.adsCommon.g.f.a(h.this.b().getApplicationContext(), new com.startapp.android.publish.adsCommon.g.e(com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "VideoMode.startBufferingIndicator", e.getMessage()), "");
                                }
                            }
                        }, com.startapp.android.publish.adsCommon.b.a().H().g());
                    } catch (Exception e) {
                        h.this.I();
                        com.startapp.android.publish.adsCommon.g.f.a(h.this.b().getApplicationContext(), new com.startapp.android.publish.adsCommon.g.e(com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "VideoMode.startBufferingIndicator", e.getMessage()), h.this.ap());
                    }
                }
            }, com.startapp.android.publish.adsCommon.b.a().H().f());
        }
    }

    /* access modifiers changed from: protected */
    public final void I() {
        this.v.removeCallbacksAndMessages(null);
        if (W()) {
            this.l.setVisibility(8);
            if (this.U != null) {
                this.U.g();
            }
        }
    }

    private boolean W() {
        return this.l != null && this.l.isShown();
    }

    /* access modifiers changed from: private */
    public boolean X() {
        AudioManager audioManager = (AudioManager) b().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager == null || (audioManager.getRingerMode() != 0 && audioManager.getRingerMode() != 1)) {
            return false;
        }
        return true;
    }

    private long Y() {
        long currentTimeMillis = System.currentTimeMillis() - this.R;
        if (this.n == 0 && this.h == 0 && currentTimeMillis < 500) {
            return Math.max(200, 500 - currentTimeMillis);
        }
        return 0;
    }

    private void Z() {
        String str = "videoApi.setReplayEnabled";
        Object[] objArr = new Object[1];
        objArr[0] = Boolean.valueOf(this.j != null);
        a(str, objArr);
        StringBuilder sb = new StringBuilder();
        sb.append(a.POST_ROLL);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(N().getPostRollType());
        a("videoApi.setMode", sb.toString());
        a("videoApi.setCloseable", Boolean.TRUE);
    }

    private void aa() {
        a("videoApi.setClickableVideo", Boolean.valueOf(N().isClickable()));
        a("videoApi.setMode", a.PLAYER.toString());
        String str = "videoApi.setCloseable";
        Object[] objArr = new Object[1];
        objArr[0] = Boolean.valueOf(N().isCloseable() || this.G);
        a(str, objArr);
        a("videoApi.setSkippable", Boolean.valueOf(ao()));
    }

    private void ab() {
        a("videoApi.setVideoDuration", Integer.valueOf(this.j.e() / 1000));
        K();
        ae();
        a("videoApi.setVideoCurrentPosition", Integer.valueOf(this.n / 1000));
    }

    /* access modifiers changed from: protected */
    public final void J() {
        a("videoApi.setVideoCurrentPosition", Integer.valueOf(0));
        a("videoApi.setSkipTimer", Integer.valueOf(0));
    }

    private void b(View view) {
        a("videoApi.setVideoFrame", Integer.valueOf(i.b(b(), view.getLeft())), Integer.valueOf(i.b(b(), view.getTop())), Integer.valueOf(i.b(b(), view.getWidth())), Integer.valueOf(i.b(b(), view.getHeight())));
    }

    private void ac() {
        this.u.post(new Runnable() {
            public final void run() {
                int K = h.this.K();
                if (K >= 1000) {
                    h.this.u.postDelayed(this, h.a((long) K) + 50);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final int K() {
        int af = af();
        int i = af / 1000;
        if (i > 0 && af % 1000 < 100) {
            i--;
        }
        a("videoApi.setVideoRemainingTimer", Integer.valueOf(i));
        return af;
    }

    private void ad() {
        ae();
        this.u.post(new Runnable() {

            /* renamed from: a reason: collision with root package name */
            private boolean f3984a;
            private final int b = h.this.d(com.startapp.android.publish.adsCommon.b.a().H().d());

            public final void run() {
                try {
                    int c2 = h.this.c(h.this.j.d() + 50);
                    if (c2 >= 0 && !this.f3984a) {
                        if (c2 != 0) {
                            if (h.this.n < h.this.N().getSkippableAfter() * 1000) {
                                h.this.a("videoApi.setSkipTimer", Integer.valueOf(c2));
                            }
                        }
                        this.f3984a = true;
                        h.this.a("videoApi.setSkipTimer", Integer.valueOf(0));
                    }
                    if (h.this.s && h.this.j.d() >= this.b) {
                        h.this.D();
                    }
                    int d = (h.this.j.d() + 50) / 1000;
                    h.this.a("videoApi.setVideoCurrentPosition", Integer.valueOf(d));
                    if (d < h.this.j.e() / 1000) {
                        h.this.u.postDelayed(this, h.this.L());
                    }
                } catch (Exception unused) {
                }
            }
        });
    }

    private void ae() {
        a("videoApi.setSkipTimer", Integer.valueOf(c(this.n + 50)));
    }

    private int af() {
        if (this.j.d() != this.j.e() || ag()) {
            return this.j.e() - this.j.d();
        }
        return this.j.e();
    }

    /* access modifiers changed from: protected */
    public final long L() {
        return (long) (1000 - (this.j.d() % 1000));
    }

    /* access modifiers changed from: protected */
    public final int c(int i) {
        if (this.F || this.h > 0) {
            return 0;
        }
        int skippableAfter = (N().getSkippableAfter() * 1000) - i;
        if (skippableAfter <= 0) {
            return 0;
        }
        return (skippableAfter / 1000) + 1;
    }

    private void h(int i) {
        if (i == c.f3991a && this.U != null) {
            this.U.d();
        }
        if (i == c.c && this.U != null) {
            this.U.h();
        }
        if (i == c.c || i == c.b) {
            this.t.removeCallbacksAndMessages(null);
            this.O.removeCallbacksAndMessages(null);
            if (this.j != null) {
                this.y = this.j.d();
                this.j.b();
            }
        } else {
            this.y = this.z;
            D();
        }
        this.u.removeCallbacksAndMessages(null);
        this.C.clear();
        this.D.clear();
        if (i == c.b) {
            this.n = -1;
            return;
        }
        if (N().getPostRollType() != com.startapp.android.publish.ads.video.f.a.NONE) {
            Z();
            this.f3862a.a().setVisibility(0);
        }
        if (N().getPostRollType() == com.startapp.android.publish.ads.video.f.a.IMAGE) {
            new Handler().postDelayed(new Runnable() {
                public final void run() {
                    if (!h.this.r) {
                        h.this.k.setVisibility(4);
                    }
                }
            }, 1000);
        } else if (N().getPostRollType() == com.startapp.android.publish.ads.video.f.a.NONE) {
            n();
        }
        this.n = -1;
        if (N().getPostRollType() != com.startapp.android.publish.ads.video.f.a.NONE) {
            aq();
        }
    }

    /* access modifiers changed from: protected */
    public final void M() {
        this.n = 0;
    }

    private boolean ag() {
        return this.n == -1;
    }

    private void ah() {
        this.z = this.j.e();
        ai();
        aj();
    }

    private void ai() {
        for (Integer intValue : this.P.keySet()) {
            final int intValue2 = intValue.intValue();
            a(d(intValue2), this.t, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        h.this.e(intValue2);
                    } catch (Exception e) {
                        com.startapp.android.publish.adsCommon.g.f.a(h.this.b().getApplicationContext(), new com.startapp.android.publish.adsCommon.g.e(com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "VideoMode.scheduleFractionProgressEvents", e.getMessage()), h.this.ap());
                    }
                }
            });
        }
    }

    private void aj() {
        for (Integer intValue : this.Q.keySet()) {
            final int intValue2 = intValue.intValue();
            a(intValue2, this.t, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        h.this.f(intValue2);
                    } catch (Exception e) {
                        com.startapp.android.publish.adsCommon.g.f.a(h.this.b().getApplicationContext(), new com.startapp.android.publish.adsCommon.g.e(com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "VideoMode.scheduleAbsoluteProgressEvents", e.getMessage()), h.this.ap());
                    }
                }
            });
        }
    }

    private void ak() {
        if (!this.s) {
            a(d(com.startapp.android.publish.adsCommon.b.a().H().d()), this.O, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        h.this.D();
                    } catch (Exception e) {
                        com.startapp.android.publish.adsCommon.g.f.a(h.this.b().getApplicationContext(), new com.startapp.android.publish.adsCommon.g.e(com.startapp.android.publish.adsCommon.g.d.EXCEPTION, "VideoMode.scheduleVideoListenerEvents", e.getMessage()), h.this.ap());
                    }
                }
            });
        }
    }

    private void a(int i, Handler handler, Runnable runnable) {
        if (this.n < i) {
            handler.postDelayed(runnable, (long) (i - this.n));
        }
    }

    /* access modifiers changed from: protected */
    public final int d(int i) {
        return (this.z * i) / 100;
    }

    /* access modifiers changed from: protected */
    public final boolean F() {
        return v().getType() == AdType.REWARDED_VIDEO;
    }

    public final void b(Bundle bundle) {
        super.b(bundle);
        bundle.putInt("currentPosition", this.n);
        bundle.putInt("latestPosition", this.y);
        bundle.putSerializable("fractionProgressImpressionsSent", this.C);
        bundle.putSerializable("absoluteProgressImpressionsSent", this.D);
        bundle.putBoolean(String.IS_MUTED, this.m);
        bundle.putBoolean("shouldSetBg", this.o);
        bundle.putInt("pauseNum", this.E);
    }

    /* access modifiers changed from: protected */
    public final f N() {
        return ((g) v()).b();
    }

    public final void q() {
        if (!ag() && !b().isFinishing() && !this.G && !this.F) {
            PauseOrigin pauseOrigin = PauseOrigin.EXTERNAL;
            if (this.j != null) {
                int d = this.j.d();
                this.n = d;
                this.y = d;
                this.j.b();
                if (this.U != null) {
                    this.U.e();
                }
            }
            new StringBuilder("Sending pause event with origin: ").append(pauseOrigin);
            ActionTrackingLink[] videoPausedUrls = N().getVideoTrackingDetails().getVideoPausedUrls();
            VideoPausedTrackingParams videoPausedTrackingParams = new VideoPausedTrackingParams(l(), i(this.y), this.h, this.E, pauseOrigin, this.N);
            a(videoPausedUrls, videoPausedTrackingParams, this.y, "paused");
        }
        if (this.j != null) {
            this.j.g();
            this.j = null;
        }
        this.t.removeCallbacksAndMessages(null);
        this.u.removeCallbacksAndMessages(null);
        this.O.removeCallbacksAndMessages(null);
        I();
        this.o = true;
        if (this.V) {
            b().unregisterReceiver(this.W);
            this.V = false;
        }
        super.q();
    }

    public final void n() {
        super.n();
        if (this.K) {
            b.f3967a;
            c.c(N().getLocalVideoPath());
        }
    }

    /* access modifiers changed from: protected */
    public final JsInterface x() {
        VideoJsInterface videoJsInterface = new VideoJsInterface(b(), this.i, this.i, new Runnable() {
            public final void run() {
                h.this.h = h.this.h + 1;
                h.this.k.setVisibility(0);
                h.this.o = false;
                h.this.M();
                h.this.J();
                h.this.H();
            }
        }, new Runnable() {
            public final void run() {
                h.this.O();
            }
        }, new Runnable() {
            public final void run() {
                h.this.m = !h.this.m;
                h.this.P();
                h.this.a(h.this.m);
            }
        }, new com.startapp.android.publish.adsCommon.e.b(l()), a(0));
        return videoJsInterface;
    }

    /* access modifiers changed from: protected */
    public final void O() {
        if (W()) {
            I();
        }
        h(c.c);
        a(N().getVideoTrackingDetails().getVideoSkippedUrls(), new VideoTrackingParams(l(), i(this.y), this.h, this.N), this.y, String.VIDEO_SKIPPED);
    }

    /* access modifiers changed from: protected */
    public final void P() {
        String str;
        if (this.j != null) {
            try {
                if (this.m) {
                    this.j.a(true);
                } else {
                    this.j.a(false);
                }
            } catch (IllegalStateException unused) {
            }
        }
        String str2 = "videoApi.setSound";
        Object[] objArr = new Object[1];
        if (this.m) {
            str = b.OFF.toString();
        } else {
            str = b.ON.toString();
        }
        objArr[0] = str;
        a(str2, objArr);
    }

    /* access modifiers changed from: protected */
    public final void a(g gVar) {
        com.startapp.android.publish.ads.video.c.a.a aVar;
        com.startapp.android.publish.adsCommon.g.f.a(b(), com.startapp.android.publish.adsCommon.g.d.VIDEO_MEDIA_PLAYER_ERROR, gVar.a().toString(), gVar.b(), ap());
        switch (gVar.a()) {
            case SERVER_DIED:
                aVar = com.startapp.android.publish.ads.video.c.a.a.GeneralLinearError;
                break;
            case BUFFERING_TIMEOUT:
                aVar = com.startapp.android.publish.ads.video.c.a.a.TimeoutMediaFileURI;
                break;
            case PLAYER_CREATION:
                aVar = com.startapp.android.publish.ads.video.c.a.a.MediaFileDisplayError;
                break;
            default:
                aVar = com.startapp.android.publish.ads.video.c.a.a.UndefinedError;
                break;
        }
        j.a((Context) b(), new com.startapp.android.publish.ads.video.a.a(N().getVideoTrackingDetails().getInlineErrorTrackingUrls(), new VideoTrackingParams(l(), i(this.y), this.h, this.N), N().getVideoUrl(), this.y).a(aVar).a("error").a());
        if ((this.s ? this.j.d() : this.n) == 0) {
            com.startapp.android.publish.adsCommon.c.a((Context) b(), h(), l(), this.h, NotDisplayedReason.VIDEO_ERROR.toString());
            if (!this.s) {
                j.b(b());
            } else if (!gVar.a().equals(com.startapp.android.publish.ads.video.b.c.h.BUFFERING_TIMEOUT)) {
                j.b(b());
            }
        }
        if ((!F() || this.g) && N().getPostRollType() != com.startapp.android.publish.ads.video.f.a.NONE) {
            h(c.c);
            return;
        }
        al();
        n();
    }

    private void al() {
        Intent intent = new Intent("com.startapp.android.ShowFailedDisplayBroadcastListener");
        intent.putExtra("showFailedReason", NotDisplayedReason.VIDEO_ERROR);
        com.startapp.common.b.a((Context) b()).a(intent);
        this.q = true;
    }

    private boolean am() {
        return this.j != null && this.j.f();
    }

    private boolean an() {
        return !this.s ? am() && this.A : this.H >= com.startapp.android.publish.adsCommon.b.a().H().k() && am() && this.A;
    }

    public final boolean p() {
        if (ag()) {
            z();
            return false;
        }
        int c2 = c(this.j.d() + 50);
        if (ao() && c2 == 0) {
            O();
            return true;
        } else if (!N().isCloseable() && !this.G) {
            return true;
        } else {
            z();
            return false;
        }
    }

    private boolean ao() {
        return this.h > 0 || N().isSkippable() || this.F;
    }

    private int i(int i) {
        if (this.z > 0) {
            return (i * 100) / this.z;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public String ap() {
        try {
            String[] h = h();
            if (h != null && h.length > 0) {
                return com.startapp.android.publish.adsCommon.c.a(h[0], (String) null);
            }
        } catch (Exception unused) {
        }
        return "";
    }

    public final void o() {
        if (!this.q) {
            super.o();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str, boolean z2) {
        if (!TextUtils.isEmpty(N().getClickUrl())) {
            str = N().getClickUrl();
            z2 = true;
        }
        this.S = ag() ? ClickOrigin.POSTROLL : ClickOrigin.VIDEO;
        new StringBuilder("Video clicked from: ").append(this.S);
        if (this.S == ClickOrigin.VIDEO) {
            h(c.b);
        }
        ClickOrigin clickOrigin = this.S;
        new StringBuilder("Sending video clicked event with origin: ").append(clickOrigin.toString());
        ActionTrackingLink[] videoClickTrackingUrls = N().getVideoTrackingDetails().getVideoClickTrackingUrls();
        VideoClickedTrackingParams videoClickedTrackingParams = new VideoClickedTrackingParams(l(), i(this.y), this.h, clickOrigin, this.N);
        a(videoClickTrackingUrls, videoClickedTrackingParams, this.y, "clicked");
        return super.a(str, z2);
    }

    /* access modifiers changed from: protected */
    public final void z() {
        if (!this.q) {
            if (ag() || this.k == null) {
                ar();
                super.z();
                return;
            }
            as();
        }
    }

    /* access modifiers changed from: protected */
    public final String B() {
        double currentTimeMillis = (double) (System.currentTimeMillis() - this.T);
        Double.isNaN(currentTimeMillis);
        return String.valueOf(currentTimeMillis / 1000.0d);
    }

    /* access modifiers changed from: protected */
    public final com.startapp.android.publish.adsCommon.e.b A() {
        return new VideoTrackingParams(l(), 0, this.h, this.N);
    }

    /* access modifiers changed from: protected */
    public final long C() {
        if (m() != null) {
            return TimeUnit.SECONDS.toMillis(m().longValue());
        }
        return TimeUnit.SECONDS.toMillis(com.startapp.android.publish.common.metaData.e.getInstance().getIABVideoImpressionDelayInSeconds());
    }

    /* access modifiers changed from: protected */
    public final void Q() {
        super.y();
        a(N().getVideoTrackingDetails().getImpressionUrls(), new VideoTrackingParams(l(), 0, this.h, this.N), 0, "impression");
        a(N().getVideoTrackingDetails().getCreativeViewUrls(), new VideoTrackingParams(l(), 0, this.h, this.N), 0, "creativeView");
    }

    /* access modifiers changed from: protected */
    public final void e(int i) {
        if (this.C.get(Integer.valueOf(i)) == null) {
            if (this.P.containsKey(Integer.valueOf(i))) {
                List list = (List) this.P.get(Integer.valueOf(i));
                StringBuilder sb = new StringBuilder("Sending fraction progress event with fraction: ");
                sb.append(i);
                sb.append(", total: ");
                sb.append(list.size());
                a((VideoTrackingLink[]) list.toArray(new FractionTrackingLink[list.size()]), new VideoProgressTrackingParams(l(), i, this.h, this.N), d(i), "fraction");
                if (this.U != null) {
                    if (i == 25) {
                        this.U.a();
                    } else if (i == 50) {
                        this.U.b();
                    } else if (i == 75) {
                        this.U.c();
                    }
                }
            }
            this.C.put(Integer.valueOf(i), Boolean.TRUE);
        }
    }

    /* access modifiers changed from: protected */
    public final void f(int i) {
        if (this.D.get(Integer.valueOf(i)) == null) {
            if (this.Q.containsKey(Integer.valueOf(i))) {
                List list = (List) this.Q.get(Integer.valueOf(i));
                StringBuilder sb = new StringBuilder("Sending absolute progress event with video progress: ");
                sb.append(i);
                sb.append(", total: ");
                sb.append(list.size());
                a((VideoTrackingLink[]) list.toArray(new AbsoluteTrackingLink[list.size()]), new VideoProgressTrackingParams(l(), i, this.h, this.N), i, Constants.PATH_TYPE_ABSOLUTE);
            }
            this.D.put(Integer.valueOf(i), Boolean.TRUE);
        }
    }

    private void aq() {
        a(N().getVideoTrackingDetails().getVideoPostRollImpressionUrls(), new VideoTrackingParams(l(), i(this.y), this.h, this.N), this.y, "postrollImression");
    }

    /* access modifiers changed from: protected */
    public final void E() {
        a(N().getVideoTrackingDetails().getVideoRewardedUrls(), new VideoTrackingParams(l(), com.startapp.android.publish.adsCommon.b.a().H().d(), this.h, this.N), d(com.startapp.android.publish.adsCommon.b.a().H().d()), "rewarded");
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z2) {
        ActionTrackingLink[] actionTrackingLinkArr;
        StringBuilder sb = new StringBuilder("Sending sound ");
        sb.append(z2 ? "muted " : "unmuted ");
        sb.append("event");
        if (z2) {
            actionTrackingLinkArr = N().getVideoTrackingDetails().getSoundMuteUrls();
        } else {
            actionTrackingLinkArr = N().getVideoTrackingDetails().getSoundUnmuteUrls();
        }
        a(actionTrackingLinkArr, new VideoTrackingParams(l(), i(this.j.d()), this.h, this.N), this.j.d(), "sound");
        if (this.U != null) {
            this.U.a(z2 ? 0.0f : 1.0f);
        }
    }

    private void ar() {
        a(N().getVideoTrackingDetails().getVideoPostRollClosedUrls(), new VideoTrackingParams(l(), i(this.y), this.h, this.N), this.y, "postrollClosed");
    }

    private void as() {
        a(N().getVideoTrackingDetails().getVideoClosedUrls(), new VideoTrackingParams(l(), i(this.j.d()), this.h, this.N), this.j.d(), "closed");
    }

    private void a(VideoTrackingLink[] videoTrackingLinkArr, VideoTrackingParams videoTrackingParams, int i, String str) {
        j.a((Context) b(), new com.startapp.android.publish.ads.video.a.a(videoTrackingLinkArr, videoTrackingParams, N().getVideoUrl(), i).a(str).a());
    }

    public final void R() {
        this.I = true;
        if (this.A && this.B) {
            G();
        }
        if (an()) {
            U();
        }
    }

    public final void S() {
        if (!ag()) {
            h(c.f3991a);
        }
        if (this.j != null) {
            this.j.c();
        }
    }

    public final void g(int i) {
        if (this.s && this.I && this.j != null && this.j.e() != 0) {
            StringBuilder sb = new StringBuilder("buffered percent = [");
            sb.append(i);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            this.H = i;
            int d = (this.j.d() * 100) / this.j.e();
            if (W()) {
                if (!this.J && an()) {
                    U();
                } else if (this.H == 100 || this.H - d > com.startapp.android.publish.adsCommon.b.a().H().j()) {
                    StringBuilder sb2 = new StringBuilder("progressive video resumed, buffered percent: [");
                    sb2.append(Integer.toString(this.H));
                    sb2.append(RequestParameters.RIGHT_BRACKETS);
                    this.j.a();
                    I();
                }
            } else if (this.H < 100 && this.H - d <= com.startapp.android.publish.adsCommon.b.a().H().k()) {
                StringBuilder sb3 = new StringBuilder("progressive video paused, buffered percent: [");
                sb3.append(Integer.toString(this.H));
                sb3.append(RequestParameters.RIGHT_BRACKETS);
                this.j.b();
                V();
            }
        }
    }

    public final boolean b(g gVar) {
        this.I = false;
        if (!this.s || this.L > this.M || gVar.c() <= 0 || !gVar.b().equals(com.startapp.android.publish.ads.video.b.b.a.MEDIA_ERROR_IO.toString())) {
            a(gVar);
        } else {
            this.L++;
            V();
            this.j.a(N().getLocalVideoPath());
            this.j.a(gVar.c());
        }
        return true;
    }

    public final void T() {
        this.B = true;
        if (this.A && am()) {
            G();
        }
    }
}
