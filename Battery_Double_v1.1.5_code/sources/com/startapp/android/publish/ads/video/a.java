package com.startapp.android.publish.ads.video;

import android.content.Context;
import android.util.Pair;
import com.startapp.android.publish.adsCommon.Ad.AdType;
import com.startapp.android.publish.adsCommon.a.d;
import com.startapp.android.publish.adsCommon.a.f;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.common.model.AdPreferences;
import com.startapp.android.publish.common.model.AdPreferences.Placement;
import com.startapp.android.publish.common.model.GetAdRequest;

/* compiled from: StartAppSDK */
public final class a extends GetAdRequest {

    /* renamed from: a reason: collision with root package name */
    private VideoRequestType f3949a;
    private VideoRequestMode b = VideoRequestMode.INTERSTITIAL;

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    public final void fillAdPreferences(Context context, AdPreferences adPreferences, Placement placement, Pair<String, String> pair) {
        super.fillAdPreferences(context, adPreferences, placement, pair);
        if (getType() != null) {
            if (getType() != AdType.NON_VIDEO) {
                if (getType() == AdType.VIDEO_NO_VAST) {
                    this.f3949a = VideoRequestType.FORCED_NONVAST;
                } else if (isAdTypeVideo()) {
                    this.f3949a = VideoRequestType.FORCED;
                }
                if (getType() == AdType.REWARDED_VIDEO) {
                    this.b = VideoRequestMode.REWARDED;
                }
                if (getType() == AdType.VIDEO) {
                    this.b = VideoRequestMode.INTERSTITIAL;
                    return;
                }
                return;
            }
        } else if (j.a(context) == com.startapp.android.publish.ads.video.j.a.ELIGIBLE) {
            if (!j.a(2)) {
                this.f3949a = VideoRequestType.FORCED;
            } else {
                this.f3949a = VideoRequestType.ENABLED;
            }
            if (getType() == AdType.REWARDED_VIDEO) {
            }
            if (getType() == AdType.VIDEO) {
            }
        }
        this.f3949a = VideoRequestType.DISABLED;
        if (getType() == AdType.REWARDED_VIDEO) {
        }
        if (getType() == AdType.VIDEO) {
        }
    }

    public final f getNameValueMap() {
        f nameValueMap = super.getNameValueMap();
        if (nameValueMap == null) {
            nameValueMap = new d();
        }
        nameValueMap.a("video", this.f3949a, false);
        nameValueMap.a("videoMode", this.b, false);
        return nameValueMap;
    }
}
