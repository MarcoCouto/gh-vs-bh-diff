package com.startapp.android.publish.ads.video;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.android.publish.ads.video.b.c.C0086c;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/* compiled from: StartAppSDK */
public final class c {

    /* renamed from: a reason: collision with root package name */
    private boolean f3958a;
    /* access modifiers changed from: private */
    public C0086c b;
    private String c;

    /* compiled from: StartAppSDK */
    public interface a {
        void a(String str);
    }

    /* compiled from: StartAppSDK */
    static class b {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static final c f3967a = new c(0);
    }

    /* synthetic */ c(byte b2) {
        this();
    }

    private c() {
        this.f3958a = true;
        this.b = null;
        this.c = null;
    }

    public static c a() {
        return b.f3967a;
    }

    public final void a(C0086c cVar) {
        this.b = cVar;
    }

    /* JADX WARNING: type inference failed for: r7v0, types: [java.io.DataInputStream, java.io.FileOutputStream, java.lang.String, java.io.InputStream] */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0140, code lost:
        r24 = r12;
        r7 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0144, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0145, code lost:
        r24 = r12;
        r5 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r7v0, types: [java.io.DataInputStream, java.io.FileOutputStream, java.lang.String, java.io.InputStream]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [java.lang.String, java.io.InputStream, java.io.DataInputStream, java.io.FileOutputStream]
  mth insns count: 160
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0144 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:13:0x005b] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final String a(Context context, URL url, String str, a aVar) {
        DataInputStream dataInputStream;
        InputStream inputStream;
        FileOutputStream fileOutputStream;
        String str2;
        InputStream inputStream2;
        String str3;
        FileOutputStream fileOutputStream2;
        InputStream inputStream3;
        int read;
        Context context2 = context;
        URL url2 = url;
        String str4 = str;
        final a aVar2 = aVar;
        new StringBuilder("Downloading video from ").append(url2);
        this.c = url.toString();
        this.f3958a = true;
        int l = com.startapp.android.publish.adsCommon.b.a().H().l();
        ? r7 = 0;
        try {
            String a2 = j.a(context2, str4);
            File file = new File(a2);
            if (file.exists()) {
                try {
                    this.c = r7;
                    r7.close();
                    r7.close();
                    r7.close();
                } catch (Exception unused) {
                }
                return a2;
            }
            URLConnection openConnection = url.openConnection();
            openConnection.connect();
            int contentLength = openConnection.getContentLength();
            inputStream = openConnection.getInputStream();
            try {
                DataInputStream dataInputStream2 = new DataInputStream(inputStream);
                try {
                    byte[] bArr = new byte[4096];
                    StringBuilder sb = new StringBuilder();
                    sb.append(str4);
                    sb.append(".temp");
                    str3 = sb.toString();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(a2);
                    sb2.append(".temp");
                    final String sb3 = sb2.toString();
                    fileOutputStream = context2.openFileOutput(str3, 0);
                    int i = 0;
                    boolean z = false;
                    int i2 = 0;
                    while (true) {
                        try {
                            read = dataInputStream2.read(bArr);
                            if (read <= 0 || !this.f3958a) {
                                String str5 = a2;
                                File file2 = file;
                                dataInputStream = dataInputStream2;
                            } else {
                                fileOutputStream.write(bArr, 0, read);
                                int i3 = i + read;
                                String str6 = a2;
                                File file3 = file;
                                double d = (double) i3;
                                Double.isNaN(d);
                                double d2 = d * 100.0d;
                                dataInputStream = dataInputStream2;
                                byte[] bArr2 = bArr;
                                double d3 = (double) contentLength;
                                Double.isNaN(d3);
                                final int i4 = (int) (d2 / d3);
                                if (i4 >= l) {
                                    if (!z && aVar2 != null) {
                                        try {
                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                public final void run() {
                                                    aVar2.a(sb3);
                                                }
                                            });
                                            z = true;
                                        } catch (Exception unused2) {
                                            inputStream2 = inputStream;
                                            try {
                                                new StringBuilder("Error downloading video from ").append(url2);
                                                new File(j.a(context2, str3)).delete();
                                                try {
                                                    this.c = null;
                                                    inputStream2.close();
                                                    dataInputStream.close();
                                                    fileOutputStream.close();
                                                } catch (Exception unused3) {
                                                }
                                                str2 = null;
                                                return str2;
                                            } catch (Throwable th) {
                                                th = th;
                                                inputStream = inputStream2;
                                                try {
                                                    this.c = null;
                                                    inputStream.close();
                                                    dataInputStream.close();
                                                    fileOutputStream.close();
                                                } catch (Exception unused4) {
                                                }
                                                throw th;
                                            }
                                        } catch (Throwable th2) {
                                            th = th2;
                                            this.c = null;
                                            inputStream.close();
                                            dataInputStream.close();
                                            fileOutputStream.close();
                                            throw th;
                                        }
                                    }
                                    if (i4 >= i2 + 1) {
                                        if (this.b != null) {
                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                public final void run() {
                                                    if (c.this.b != null) {
                                                        c.this.b.g(i4);
                                                    }
                                                }
                                            });
                                        }
                                        i = i3;
                                        i2 = i4;
                                        a2 = str6;
                                        file = file3;
                                        dataInputStream2 = dataInputStream;
                                        bArr = bArr2;
                                    }
                                }
                                i = i3;
                                a2 = str6;
                                file = file3;
                                dataInputStream2 = dataInputStream;
                                bArr = bArr2;
                            }
                        } catch (Exception unused5) {
                            dataInputStream = dataInputStream2;
                            inputStream2 = inputStream;
                            new StringBuilder("Error downloading video from ").append(url2);
                            new File(j.a(context2, str3)).delete();
                            this.c = null;
                            inputStream2.close();
                            dataInputStream.close();
                            fileOutputStream.close();
                            str2 = null;
                            return str2;
                        } catch (Throwable th3) {
                            th = th3;
                            dataInputStream = dataInputStream2;
                            this.c = null;
                            inputStream.close();
                            dataInputStream.close();
                            fileOutputStream.close();
                            throw th;
                        }
                    }
                    String str52 = a2;
                    File file22 = file;
                    dataInputStream = dataInputStream2;
                    if (this.f3958a || read <= 0) {
                        a(new File(j.a(context2, str3)), file22);
                        try {
                            this.c = null;
                            inputStream.close();
                            dataInputStream.close();
                            fileOutputStream.close();
                        } catch (Exception unused6) {
                        }
                        str2 = str52;
                        return str2;
                    }
                    new File(j.a(context2, str3)).delete();
                    String str7 = "downloadInterrupted";
                    try {
                        this.c = null;
                        inputStream.close();
                        dataInputStream.close();
                        fileOutputStream.close();
                    } catch (Exception unused7) {
                    }
                    return str7;
                } catch (Exception unused8) {
                    DataInputStream dataInputStream3 = dataInputStream2;
                    inputStream2 = inputStream;
                    str3 = null;
                    fileOutputStream = null;
                    new StringBuilder("Error downloading video from ").append(url2);
                    new File(j.a(context2, str3)).delete();
                    this.c = null;
                    inputStream2.close();
                    dataInputStream.close();
                    fileOutputStream.close();
                    str2 = null;
                    return str2;
                } catch (Throwable th4) {
                }
            } catch (Exception unused9) {
                inputStream3 = inputStream;
                str3 = null;
                fileOutputStream2 = null;
                dataInputStream = null;
                new StringBuilder("Error downloading video from ").append(url2);
                new File(j.a(context2, str3)).delete();
                this.c = null;
                inputStream2.close();
                dataInputStream.close();
                fileOutputStream.close();
                str2 = null;
                return str2;
            } catch (Throwable th5) {
                th = th5;
                fileOutputStream = null;
                dataInputStream = null;
                this.c = null;
                inputStream.close();
                dataInputStream.close();
                fileOutputStream.close();
                throw th;
            }
        } catch (Exception unused10) {
            str3 = null;
            fileOutputStream2 = null;
            inputStream3 = null;
            dataInputStream = null;
            new StringBuilder("Error downloading video from ").append(url2);
            new File(j.a(context2, str3)).delete();
            this.c = null;
            inputStream2.close();
            dataInputStream.close();
            fileOutputStream.close();
            str2 = null;
            return str2;
        } catch (Throwable th6) {
            th = th6;
            fileOutputStream = null;
            inputStream = null;
            dataInputStream = null;
            this.c = null;
            inputStream.close();
            dataInputStream.close();
            fileOutputStream.close();
            throw th;
        }
    }

    public final void a(String str) {
        if (str != null && str.equals(this.c)) {
            this.f3958a = false;
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0033 */
    private static void a(File file, File file2) {
        OutputStream outputStream;
        InputStream inputStream;
        FileOutputStream fileOutputStream;
        outputStream = null;
        try {
            inputStream = new FileInputStream(file);
            try {
                fileOutputStream = new FileOutputStream(file2);
            } catch (Exception ) {
                try {
                    inputStream.close();
                    outputStream.close();
                } catch (Exception unused) {
                    return;
                }
            } catch (Throwable th) {
                th = th;
                try {
                    inputStream.close();
                    outputStream.close();
                } catch (Exception unused2) {
                }
                throw th;
            }
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read > 0) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        try {
                            inputStream.close();
                            fileOutputStream.close();
                            return;
                        } catch (Exception unused3) {
                            return;
                        }
                    }
                }
            } catch (Exception unused4) {
                outputStream = fileOutputStream;
                inputStream.close();
                outputStream.close();
            } catch (Throwable th2) {
                th = th2;
                outputStream = fileOutputStream;
                inputStream.close();
                outputStream.close();
                throw th;
            }
        } catch (Exception unused5) {
            inputStream = null;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            inputStream.close();
            outputStream.close();
            throw th;
        }
    }

    public static boolean b(String str) {
        return str != null && str.endsWith(".temp");
    }

    public static void c(String str) {
        if (b(str)) {
            new File(str).delete();
        }
    }
}
