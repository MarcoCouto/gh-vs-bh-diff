package com.startapp.android.publish.inappbrowser;

import android.graphics.Bitmap;

/* compiled from: StartAppSDK */
public final class c {

    /* renamed from: a reason: collision with root package name */
    private Bitmap f4159a = null;
    private int b;
    private int c;
    private String d;

    public c(int i, int i2, String str) {
        this.b = i;
        this.c = i2;
        this.d = str;
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final Bitmap d() {
        return this.f4159a;
    }

    public final void a(Bitmap bitmap) {
        this.f4159a = bitmap;
    }
}
