package org.altbeacon.beacon;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.support.v4.view.InputDeviceCompat;
import com.applovin.sdk.AppLovinTargetingData;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.altbeacon.beacon.logging.LogManager;

public class BeaconParser {
    private static final Pattern D_PATTERN = Pattern.compile("d\\:(\\d+)\\-(\\d+)([bl]?)");
    private static final char[] HEX_ARRAY = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', AppLovinTargetingData.GENDER_FEMALE};
    private static final Pattern I_PATTERN = Pattern.compile("i\\:(\\d+)\\-(\\d+)(l?)");
    private static final Pattern M_PATTERN = Pattern.compile("m\\:(\\d+)-(\\d+)\\=([0-9A-Fa-f]+)");
    private static final Pattern P_PATTERN = Pattern.compile("p\\:(\\d+)\\-(\\d+)");
    private static final Pattern S_PATTERN = Pattern.compile("s\\:(\\d+)-(\\d+)\\=([0-9A-Fa-f]+)");
    private static final String TAG = "BeaconParser";
    protected List<Integer> mDataEndOffsets = new ArrayList();
    protected List<Boolean> mDataLittleEndianFlags = new ArrayList();
    protected List<Integer> mDataStartOffsets = new ArrayList();
    protected int[] mHardwareAssistManufacturers = {76};
    protected List<Integer> mIdentifierEndOffsets = new ArrayList();
    protected List<Boolean> mIdentifierLittleEndianFlags = new ArrayList();
    protected List<Integer> mIdentifierStartOffsets = new ArrayList();
    private Long mMatchingBeaconTypeCode;
    protected Integer mMatchingBeaconTypeCodeEndOffset;
    protected Integer mMatchingBeaconTypeCodeStartOffset;
    protected Integer mPowerEndOffset;
    protected Integer mPowerStartOffset;
    protected Long mServiceUuid;
    protected Integer mServiceUuidEndOffset;
    protected Integer mServiceUuidStartOffset;

    public static class BeaconLayoutException extends RuntimeException {
        public BeaconLayoutException(String str) {
            super(str);
        }
    }

    public BeaconParser setBeaconLayout(String str) {
        String[] split;
        for (String str2 : str.split(",")) {
            Matcher matcher = I_PATTERN.matcher(str2);
            boolean z = false;
            while (matcher.find()) {
                try {
                    int parseInt = Integer.parseInt(matcher.group(1));
                    int parseInt2 = Integer.parseInt(matcher.group(2));
                    this.mIdentifierLittleEndianFlags.add(Boolean.valueOf(matcher.group(3).equals("l")));
                    this.mIdentifierStartOffsets.add(Integer.valueOf(parseInt));
                    this.mIdentifierEndOffsets.add(Integer.valueOf(parseInt2));
                    z = true;
                } catch (NumberFormatException unused) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Cannot parse integer byte offset in term: ");
                    sb.append(str2);
                    throw new BeaconLayoutException(sb.toString());
                }
            }
            Matcher matcher2 = D_PATTERN.matcher(str2);
            while (matcher2.find()) {
                try {
                    int parseInt3 = Integer.parseInt(matcher2.group(1));
                    int parseInt4 = Integer.parseInt(matcher2.group(2));
                    this.mDataLittleEndianFlags.add(Boolean.valueOf(matcher2.group(3).equals("l")));
                    this.mDataStartOffsets.add(Integer.valueOf(parseInt3));
                    this.mDataEndOffsets.add(Integer.valueOf(parseInt4));
                    z = true;
                } catch (NumberFormatException unused2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Cannot parse integer byte offset in term: ");
                    sb2.append(str2);
                    throw new BeaconLayoutException(sb2.toString());
                }
            }
            Matcher matcher3 = P_PATTERN.matcher(str2);
            while (matcher3.find()) {
                try {
                    int parseInt5 = Integer.parseInt(matcher3.group(1));
                    int parseInt6 = Integer.parseInt(matcher3.group(2));
                    this.mPowerStartOffset = Integer.valueOf(parseInt5);
                    this.mPowerEndOffset = Integer.valueOf(parseInt6);
                    z = true;
                } catch (NumberFormatException unused3) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Cannot parse integer power byte offset in term: ");
                    sb3.append(str2);
                    throw new BeaconLayoutException(sb3.toString());
                }
            }
            Matcher matcher4 = M_PATTERN.matcher(str2);
            while (matcher4.find()) {
                try {
                    int parseInt7 = Integer.parseInt(matcher4.group(1));
                    int parseInt8 = Integer.parseInt(matcher4.group(2));
                    this.mMatchingBeaconTypeCodeStartOffset = Integer.valueOf(parseInt7);
                    this.mMatchingBeaconTypeCodeEndOffset = Integer.valueOf(parseInt8);
                    String group = matcher4.group(3);
                    try {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("0x");
                        sb4.append(group);
                        this.mMatchingBeaconTypeCode = Long.decode(sb4.toString());
                        z = true;
                    } catch (NumberFormatException unused4) {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("Cannot parse beacon type code: ");
                        sb5.append(group);
                        sb5.append(" in term: ");
                        sb5.append(str2);
                        throw new BeaconLayoutException(sb5.toString());
                    }
                } catch (NumberFormatException unused5) {
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("Cannot parse integer byte offset in term: ");
                    sb6.append(str2);
                    throw new BeaconLayoutException(sb6.toString());
                }
            }
            Matcher matcher5 = S_PATTERN.matcher(str2);
            while (matcher5.find()) {
                try {
                    int parseInt9 = Integer.parseInt(matcher5.group(1));
                    int parseInt10 = Integer.parseInt(matcher5.group(2));
                    this.mServiceUuidStartOffset = Integer.valueOf(parseInt9);
                    this.mServiceUuidEndOffset = Integer.valueOf(parseInt10);
                    String group2 = matcher5.group(3);
                    try {
                        StringBuilder sb7 = new StringBuilder();
                        sb7.append("0x");
                        sb7.append(group2);
                        this.mServiceUuid = Long.decode(sb7.toString());
                        z = true;
                    } catch (NumberFormatException unused6) {
                        StringBuilder sb8 = new StringBuilder();
                        sb8.append("Cannot parse serviceUuid: ");
                        sb8.append(group2);
                        sb8.append(" in term: ");
                        sb8.append(str2);
                        throw new BeaconLayoutException(sb8.toString());
                    }
                } catch (NumberFormatException unused7) {
                    StringBuilder sb9 = new StringBuilder();
                    sb9.append("Cannot parse integer byte offset in term: ");
                    sb9.append(str2);
                    throw new BeaconLayoutException(sb9.toString());
                }
            }
            if (!z) {
                LogManager.d(TAG, "cannot parse term %s", str2);
                StringBuilder sb10 = new StringBuilder();
                sb10.append("Cannot parse beacon layout term: ");
                sb10.append(str2);
                throw new BeaconLayoutException(sb10.toString());
            }
        }
        if (this.mPowerStartOffset == null || this.mPowerEndOffset == null) {
            throw new BeaconLayoutException("You must supply a power byte offset with a prefix of 'p'");
        } else if (this.mMatchingBeaconTypeCodeStartOffset == null || this.mMatchingBeaconTypeCodeEndOffset == null) {
            throw new BeaconLayoutException("You must supply a matching beacon type expression with a prefix of 'm'");
        } else if (this.mIdentifierStartOffsets.size() != 0 && this.mIdentifierEndOffsets.size() != 0) {
            return this;
        } else {
            throw new BeaconLayoutException("You must supply at least one identifier offset withh a prefix of 'i'");
        }
    }

    public int[] getHardwareAssistManufacturers() {
        return this.mHardwareAssistManufacturers;
    }

    public void setHardwareAssistManufacturerCodes(int[] iArr) {
        this.mHardwareAssistManufacturers = iArr;
    }

    public Long getMatchingBeaconTypeCode() {
        return this.mMatchingBeaconTypeCode;
    }

    public int getMatchingBeaconTypeCodeStartOffset() {
        return this.mMatchingBeaconTypeCodeStartOffset.intValue();
    }

    public int getMatchingBeaconTypeCodeEndOffset() {
        return this.mMatchingBeaconTypeCodeEndOffset.intValue();
    }

    public Long getServiceUuid() {
        return this.mServiceUuid;
    }

    public int getMServiceUuidStartOffset() {
        return this.mServiceUuidStartOffset.intValue();
    }

    public int getServiceUuidEndOffset() {
        return this.mServiceUuidEndOffset.intValue();
    }

    @TargetApi(5)
    public Beacon fromScanData(byte[] bArr, int i, BluetoothDevice bluetoothDevice) {
        return fromScanData(bArr, i, bluetoothDevice, new Beacon());
    }

    /* access modifiers changed from: protected */
    @TargetApi(5)
    public Beacon fromScanData(byte[] bArr, int i, BluetoothDevice bluetoothDevice, Beacon beacon) {
        byte[] bArr2;
        int i2;
        boolean z;
        String str;
        byte[] longToByteArray = longToByteArray(getMatchingBeaconTypeCode().longValue(), (this.mMatchingBeaconTypeCodeEndOffset.intValue() - this.mMatchingBeaconTypeCodeStartOffset.intValue()) + 1);
        String str2 = null;
        if (getServiceUuid() != null) {
            i2 = 11;
            bArr2 = longToByteArray(getServiceUuid().longValue(), (this.mServiceUuidEndOffset.intValue() - this.mServiceUuidStartOffset.intValue()) + 1);
        } else {
            i2 = 5;
            bArr2 = null;
        }
        int i3 = 2;
        while (true) {
            if (i3 > i2) {
                z = false;
                break;
            } else if (getServiceUuid() != null) {
                if (byteArraysMatch(bArr, this.mServiceUuidStartOffset.intValue() + i3, bArr2, 0) && byteArraysMatch(bArr, this.mMatchingBeaconTypeCodeStartOffset.intValue() + i3, longToByteArray, 0)) {
                    break;
                }
                i3++;
            } else if (byteArraysMatch(bArr, this.mMatchingBeaconTypeCodeStartOffset.intValue() + i3, longToByteArray, 0)) {
                break;
            } else {
                i3++;
            }
        }
        z = true;
        if (!z) {
            if (getServiceUuid() == null) {
                if (LogManager.isVerboseLoggingEnabled()) {
                    LogManager.d(TAG, "This is not a matching Beacon advertisement. (Was expecting %s.  The bytes I see are: %s", byteArrayToString(longToByteArray), bytesToHex(bArr));
                }
            } else if (LogManager.isVerboseLoggingEnabled()) {
                LogManager.d(TAG, "This is not a matching Beacon advertisement. (Was expecting %s and %s.  The bytes I see are: %s", byteArrayToString(bArr2), byteArrayToString(longToByteArray), bytesToHex(bArr));
            }
            return null;
        }
        if (LogManager.isVerboseLoggingEnabled()) {
            LogManager.d(TAG, "This is a recognized beacon advertisement -- %s seen", byteArrayToString(longToByteArray));
        }
        ArrayList arrayList = new ArrayList();
        for (int i4 = 0; i4 < this.mIdentifierEndOffsets.size(); i4++) {
            arrayList.add(Identifier.fromBytes(bArr, ((Integer) this.mIdentifierStartOffsets.get(i4)).intValue() + i3, ((Integer) this.mIdentifierEndOffsets.get(i4)).intValue() + i3 + 1, ((Boolean) this.mIdentifierLittleEndianFlags.get(i4)).booleanValue()));
        }
        ArrayList arrayList2 = new ArrayList();
        for (int i5 = 0; i5 < this.mDataEndOffsets.size(); i5++) {
            arrayList2.add(Long.valueOf(Long.parseLong(byteArrayToFormattedString(bArr, ((Integer) this.mDataStartOffsets.get(i5)).intValue() + i3, ((Integer) this.mDataEndOffsets.get(i5)).intValue() + i3, ((Boolean) this.mDataLittleEndianFlags.get(i5)).booleanValue()))));
        }
        int parseInt = Integer.parseInt(byteArrayToFormattedString(bArr, this.mPowerStartOffset.intValue() + i3, this.mPowerEndOffset.intValue() + i3, false));
        if (parseInt > 127) {
            parseInt += InputDeviceCompat.SOURCE_ANY;
        }
        int parseInt2 = Integer.parseInt(byteArrayToFormattedString(bArr, this.mMatchingBeaconTypeCodeStartOffset.intValue() + i3, this.mMatchingBeaconTypeCodeEndOffset.intValue() + i3, false));
        int parseInt3 = Integer.parseInt(byteArrayToFormattedString(bArr, i3, i3 + 1, true));
        if (bluetoothDevice != null) {
            str2 = bluetoothDevice.getAddress();
            str = bluetoothDevice.getName();
        } else {
            str = null;
        }
        beacon.mIdentifiers = arrayList;
        beacon.mDataFields = arrayList2;
        beacon.mTxPower = parseInt;
        beacon.mRssi = i;
        beacon.mBeaconTypeCode = parseInt2;
        if (this.mServiceUuid != null) {
            beacon.mServiceUuid = (int) this.mServiceUuid.longValue();
        } else {
            beacon.mServiceUuid = -1;
        }
        beacon.mBluetoothAddress = str2;
        beacon.mBluetoothName = str;
        beacon.mManufacturer = parseInt3;
        return beacon;
    }

    public byte[] getBeaconAdvertisementData(Beacon beacon) {
        int i = -1;
        if (this.mMatchingBeaconTypeCodeEndOffset != null && this.mMatchingBeaconTypeCodeEndOffset.intValue() > -1) {
            i = this.mMatchingBeaconTypeCodeEndOffset.intValue();
        }
        if (this.mPowerEndOffset != null && this.mPowerEndOffset.intValue() > i) {
            i = this.mPowerEndOffset.intValue();
        }
        int i2 = i;
        for (int i3 = 0; i3 < this.mIdentifierStartOffsets.size(); i3++) {
            if (this.mIdentifierEndOffsets.get(i3) != null && ((Integer) this.mIdentifierEndOffsets.get(i3)).intValue() > i2) {
                i2 = ((Integer) this.mIdentifierEndOffsets.get(i3)).intValue();
            }
        }
        for (int i4 = 0; i4 < this.mDataEndOffsets.size(); i4++) {
            if (this.mDataEndOffsets.get(i4) != null && ((Integer) this.mDataEndOffsets.get(i4)).intValue() > i2) {
                i2 = ((Integer) this.mDataEndOffsets.get(i4)).intValue();
            }
        }
        byte[] bArr = new byte[((i2 + 1) - 2)];
        getMatchingBeaconTypeCode().longValue();
        for (int intValue = this.mMatchingBeaconTypeCodeStartOffset.intValue(); intValue <= this.mMatchingBeaconTypeCodeEndOffset.intValue(); intValue++) {
            bArr[intValue - 2] = (byte) ((int) ((getMatchingBeaconTypeCode().longValue() >> (8 * (this.mMatchingBeaconTypeCodeEndOffset.intValue() - intValue))) & 255));
        }
        for (int i5 = 0; i5 < this.mIdentifierStartOffsets.size(); i5++) {
            byte[] byteArrayOfSpecifiedEndianness = beacon.getIdentifier(i5).toByteArrayOfSpecifiedEndianness(((Boolean) this.mIdentifierLittleEndianFlags.get(i5)).booleanValue());
            for (int intValue2 = ((Integer) this.mIdentifierStartOffsets.get(i5)).intValue(); intValue2 <= ((Integer) this.mIdentifierEndOffsets.get(i5)).intValue(); intValue2++) {
                if (((Integer) this.mIdentifierEndOffsets.get(i5)).intValue() - intValue2 < byteArrayOfSpecifiedEndianness.length) {
                    bArr[intValue2 - 2] = byteArrayOfSpecifiedEndianness[((Integer) this.mIdentifierEndOffsets.get(i5)).intValue() - intValue2];
                } else {
                    bArr[intValue2 - 2] = 0;
                }
            }
        }
        for (int intValue3 = this.mPowerStartOffset.intValue(); intValue3 <= this.mPowerEndOffset.intValue(); intValue3++) {
            bArr[intValue3 - 2] = (byte) ((beacon.getTxPower() >> ((intValue3 - this.mPowerStartOffset.intValue()) * 8)) & 255);
        }
        for (int i6 = 0; i6 < this.mDataStartOffsets.size(); i6++) {
            long longValue = ((Long) beacon.getDataFields().get(i6)).longValue();
            int intValue4 = ((Integer) this.mDataStartOffsets.get(i6)).intValue();
            while (intValue4 <= ((Integer) this.mDataEndOffsets.get(i6)).intValue()) {
                bArr[(((Boolean) this.mDataLittleEndianFlags.get(i6)).booleanValue() ? ((Integer) this.mDataEndOffsets.get(i6)).intValue() - intValue4 : intValue4) - 2] = (byte) ((int) ((longValue >> ((intValue4 - ((Integer) this.mDataStartOffsets.get(i6)).intValue()) * 8)) & 255));
                intValue4++;
            }
        }
        return bArr;
    }

    public BeaconParser setMatchingBeaconTypeCode(Long l) {
        this.mMatchingBeaconTypeCode = l;
        return this;
    }

    public int getIdentifierByteCount(int i) {
        return (((Integer) this.mIdentifierEndOffsets.get(i)).intValue() - ((Integer) this.mIdentifierStartOffsets.get(i)).intValue()) + 1;
    }

    public int getIdentifierCount() {
        return this.mIdentifierStartOffsets.size();
    }

    public int getDataFieldCount() {
        return this.mDataStartOffsets.size();
    }

    protected static String bytesToHex(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i] & 255;
            int i2 = i * 2;
            cArr[i2] = HEX_ARRAY[b >>> 4];
            cArr[i2 + 1] = HEX_ARRAY[b & 15];
        }
        return new String(cArr);
    }

    public static byte[] longToByteArray(long j, int i) {
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = ((i - i2) - 1) * 8;
            bArr[i2] = (byte) ((int) (((255 << i3) & j) >> ((int) ((long) i3))));
        }
        return bArr;
    }

    private boolean byteArraysMatch(byte[] bArr, int i, byte[] bArr2, int i2) {
        int length = bArr.length > bArr2.length ? bArr2.length : bArr.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (bArr[i3 + i] != bArr2[i3 + i2]) {
                return false;
            }
        }
        return true;
    }

    private String byteArrayToString(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte valueOf : bArr) {
            sb.append(String.format("%02x", new Object[]{Byte.valueOf(valueOf)}));
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    private String byteArrayToFormattedString(byte[] bArr, int i, int i2, boolean z) {
        int i3 = i2 - i;
        int i4 = i3 + 1;
        byte[] bArr2 = new byte[i4];
        if (z) {
            for (int i5 = 0; i5 <= i3; i5++) {
                bArr2[i5] = bArr[((bArr2.length + i) - 1) - i5];
            }
        } else {
            for (int i6 = 0; i6 <= i3; i6++) {
                bArr2[i6] = bArr[i + i6];
            }
        }
        if (i4 < 5) {
            long j = 0;
            for (int i7 = 0; i7 < bArr2.length; i7++) {
                j += ((long) (bArr2[(bArr2.length - i7) - 1] & 255)) * ((long) Math.pow(256.0d, ((double) i7) * 1.0d));
            }
            return Long.toString(j);
        }
        String bytesToHex = bytesToHex(bArr2);
        if (bArr2.length == 16) {
            StringBuilder sb = new StringBuilder();
            sb.append(bytesToHex.substring(0, 8));
            sb.append("-");
            sb.append(bytesToHex.substring(8, 12));
            sb.append("-");
            sb.append(bytesToHex.substring(12, 16));
            sb.append("-");
            sb.append(bytesToHex.substring(16, 20));
            sb.append("-");
            sb.append(bytesToHex.substring(20, 32));
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("0x");
        sb2.append(bytesToHex);
        return sb2.toString();
    }
}
