package org.altbeacon.beacon.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.altbeacon.beacon.Region;

public class StartRMData implements Parcelable {
    public static final Creator<StartRMData> CREATOR = new Creator<StartRMData>() {
        public StartRMData createFromParcel(Parcel parcel) {
            return new StartRMData(parcel);
        }

        public StartRMData[] newArray(int i) {
            return new StartRMData[i];
        }
    };
    private boolean backgroundFlag;
    private long betweenScanPeriod;
    private String callbackPackageName;
    private Region region;
    private long scanPeriod;

    public int describeContents() {
        return 0;
    }

    public StartRMData(Region region2, String str) {
        this.region = region2;
        this.callbackPackageName = str;
    }

    public StartRMData(long j, long j2, boolean z) {
        this.scanPeriod = j;
        this.betweenScanPeriod = j2;
        this.backgroundFlag = z;
    }

    public StartRMData(Region region2, String str, long j, long j2, boolean z) {
        this.scanPeriod = j;
        this.betweenScanPeriod = j2;
        this.region = region2;
        this.callbackPackageName = str;
        this.backgroundFlag = z;
    }

    public long getScanPeriod() {
        return this.scanPeriod;
    }

    public long getBetweenScanPeriod() {
        return this.betweenScanPeriod;
    }

    public Region getRegionData() {
        return this.region;
    }

    public String getCallbackPackageName() {
        return this.callbackPackageName;
    }

    public boolean getBackgroundFlag() {
        return this.backgroundFlag;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.region, i);
        parcel.writeString(this.callbackPackageName);
        parcel.writeLong(this.scanPeriod);
        parcel.writeLong(this.betweenScanPeriod);
        parcel.writeByte(this.backgroundFlag ? (byte) 1 : 0);
    }

    private StartRMData(Parcel parcel) {
        this.region = (Region) parcel.readParcelable(StartRMData.class.getClassLoader());
        this.callbackPackageName = parcel.readString();
        this.scanPeriod = parcel.readLong();
        this.betweenScanPeriod = parcel.readLong();
        this.backgroundFlag = parcel.readByte() != 0;
    }
}
