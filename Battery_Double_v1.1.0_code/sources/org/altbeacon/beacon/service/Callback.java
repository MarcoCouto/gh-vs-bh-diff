package org.altbeacon.beacon.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import org.altbeacon.beacon.logging.LogManager;

public class Callback {
    private static final String TAG = "Callback";
    private Intent intent;

    public Callback(String str) {
        if (str != null) {
            this.intent = new Intent();
            this.intent.setComponent(new ComponentName(str, "org.altbeacon.beacon.BeaconIntentProcessor"));
        }
    }

    public Intent getIntent() {
        return this.intent;
    }

    public void setIntent(Intent intent2) {
        this.intent = intent2;
    }

    public boolean call(Context context, String str, Parcelable parcelable) {
        if (this.intent == null) {
            return false;
        }
        LogManager.d(TAG, "attempting callback via intent: %s", this.intent.getComponent());
        this.intent.putExtra(str, parcelable);
        context.startService(this.intent);
        return true;
    }
}
