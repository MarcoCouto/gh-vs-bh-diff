package org.altbeacon.beacon.service.scanner;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import java.util.Date;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.bluetooth.BluetoothCrashResolver;

@TargetApi(18)
public class CycledLeScannerForJellyBeanMr2 extends CycledLeScanner {
    private static final String TAG = "CycledLeScannerForJellyBeanMr2";
    private LeScanCallback leScanCallback;

    public CycledLeScannerForJellyBeanMr2(Context context, long j, long j2, boolean z, CycledLeScanCallback cycledLeScanCallback, BluetoothCrashResolver bluetoothCrashResolver) {
        super(context, j, j2, z, cycledLeScanCallback, bluetoothCrashResolver);
    }

    /* access modifiers changed from: protected */
    public void stopScan() {
        try {
            BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();
            if (bluetoothAdapter != null) {
                bluetoothAdapter.stopLeScan(getLeScanCallback());
            }
        } catch (Exception e) {
            LogManager.e(e, TAG, "Internal Android exception scanning for beacons", new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public boolean deferScanIfNeeded() {
        long time = this.mNextScanCycleStartTime - new Date().getTime();
        if (time <= 0) {
            return false;
        }
        LogManager.d(TAG, "Waiting to start next bluetooth scan for another %s milliseconds", Long.valueOf(time));
        if (this.mBackgroundFlag) {
            setWakeUpAlarm();
        }
        Handler handler = this.mHandler;
        AnonymousClass1 r3 = new Runnable() {
            public void run() {
                CycledLeScannerForJellyBeanMr2.this.scanLeDevice(Boolean.valueOf(true));
            }
        };
        if (time > 1000) {
            time = 1000;
        }
        handler.postDelayed(r3, time);
        return true;
    }

    /* access modifiers changed from: protected */
    public void startScan() {
        getBluetoothAdapter().startLeScan(getLeScanCallback());
    }

    /* access modifiers changed from: protected */
    public void finishScan() {
        getBluetoothAdapter().stopLeScan(getLeScanCallback());
        this.mScanningPaused = true;
    }

    /* access modifiers changed from: private */
    public LeScanCallback getLeScanCallback() {
        if (this.leScanCallback == null) {
            this.leScanCallback = new LeScanCallback() {
                public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
                    LogManager.d(CycledLeScannerForJellyBeanMr2.TAG, "got record", new Object[0]);
                    CycledLeScannerForJellyBeanMr2.this.mCycledLeScanCallback.onLeScan(bluetoothDevice, i, bArr);
                    CycledLeScannerForJellyBeanMr2.this.mBluetoothCrashResolver.notifyScannedDevice(bluetoothDevice, CycledLeScannerForJellyBeanMr2.this.getLeScanCallback());
                }
            };
        }
        return this.leScanCallback;
    }
}
