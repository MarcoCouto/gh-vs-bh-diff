package org.altbeacon.beacon.service;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.BuildConfig;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.distance.DistanceCalculator;
import org.altbeacon.beacon.distance.ModelSpecificDistanceCalculator;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.beacon.service.scanner.CycledLeScanCallback;
import org.altbeacon.beacon.service.scanner.CycledLeScanner;
import org.altbeacon.bluetooth.BluetoothCrashResolver;

@TargetApi(5)
public class BeaconService extends Service {
    public static final int MSG_SET_SCAN_PERIODS = 6;
    public static final int MSG_START_MONITORING = 4;
    public static final int MSG_START_RANGING = 2;
    public static final int MSG_STOP_MONITORING = 5;
    public static final int MSG_STOP_RANGING = 3;
    public static final String TAG = "BeaconService";
    /* access modifiers changed from: private */
    public List<BeaconParser> beaconParsers;
    private int bindCount = 0;
    private BluetoothCrashResolver bluetoothCrashResolver;
    private DistanceCalculator defaultDistanceCalculator = null;
    private Handler handler = new Handler();
    private boolean mBackgroundFlag = false;
    private CycledLeScanCallback mCycledLeScanCallback = new CycledLeScanCallback() {
        @TargetApi(11)
        public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
            new ScanProcessor().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ScanData[]{new ScanData(bluetoothDevice, i, bArr)});
        }

        public void onCycleEnd() {
            BeaconService.this.processExpiredMonitors();
            BeaconService.this.processRangeData();
            if (BeaconService.this.simulatedScanData != null) {
                LogManager.w(BeaconService.TAG, "Simulated scan data is deprecated and will be removed in a future release. Please use the new BeaconSimulator interface instead.", new Object[0]);
                ApplicationInfo applicationInfo = BeaconService.this.getApplicationInfo();
                int i = applicationInfo.flags & 2;
                applicationInfo.flags = i;
                if (i != 0) {
                    for (Beacon access$400 : BeaconService.this.simulatedScanData) {
                        BeaconService.this.processBeaconFromScan(access$400);
                    }
                } else {
                    LogManager.w(BeaconService.TAG, "Simulated scan data provided, but ignored because we are not running in debug mode.  Please remove simulated scan data for production.", new Object[0]);
                }
            }
            if (BeaconManager.getBeaconSimulator() == null) {
                return;
            }
            if (BeaconManager.getBeaconSimulator().getBeacons() != null) {
                ApplicationInfo applicationInfo2 = BeaconService.this.getApplicationInfo();
                int i2 = applicationInfo2.flags & 2;
                applicationInfo2.flags = i2;
                if (i2 != 0) {
                    for (Beacon access$4002 : BeaconManager.getBeaconSimulator().getBeacons()) {
                        BeaconService.this.processBeaconFromScan(access$4002);
                    }
                    return;
                }
                LogManager.w(BeaconService.TAG, "Beacon simulations provided, but ignored because we are not running in debug mode.  Please remove beacon simulations for production.", new Object[0]);
                return;
            }
            LogManager.w(BeaconService.TAG, "getBeacons is returning null. No simulated beacons to report.", new Object[0]);
        }
    };
    private CycledLeScanner mCycledScanner;
    final Messenger mMessenger = new Messenger(new IncomingHandler(this));
    private Map<Region, MonitorState> monitoredRegionState = new HashMap();
    private Map<Region, RangeState> rangedRegionState = new HashMap();
    private boolean scanningEnabled = false;
    /* access modifiers changed from: private */
    public List<Beacon> simulatedScanData = null;
    private HashSet<Beacon> trackedBeacons;
    int trackedBeaconsPacketCount;

    public class BeaconBinder extends Binder {
        public BeaconBinder() {
        }

        public BeaconService getService() {
            LogManager.i(BeaconService.TAG, "getService of BeaconBinder called", new Object[0]);
            return BeaconService.this;
        }
    }

    static class IncomingHandler extends Handler {
        private final WeakReference<BeaconService> mService;

        IncomingHandler(BeaconService beaconService) {
            this.mService = new WeakReference<>(beaconService);
        }

        public void handleMessage(Message message) {
            BeaconService beaconService = (BeaconService) this.mService.get();
            StartRMData startRMData = (StartRMData) message.obj;
            if (beaconService != null) {
                switch (message.what) {
                    case 2:
                        LogManager.i(BeaconService.TAG, "start ranging received", new Object[0]);
                        beaconService.startRangingBeaconsInRegion(startRMData.getRegionData(), new Callback(startRMData.getCallbackPackageName()));
                        beaconService.setScanPeriods(startRMData.getScanPeriod(), startRMData.getBetweenScanPeriod(), startRMData.getBackgroundFlag());
                        return;
                    case 3:
                        LogManager.i(BeaconService.TAG, "stop ranging received", new Object[0]);
                        beaconService.stopRangingBeaconsInRegion(startRMData.getRegionData());
                        beaconService.setScanPeriods(startRMData.getScanPeriod(), startRMData.getBetweenScanPeriod(), startRMData.getBackgroundFlag());
                        return;
                    case 4:
                        LogManager.i(BeaconService.TAG, "start monitoring received", new Object[0]);
                        beaconService.startMonitoringBeaconsInRegion(startRMData.getRegionData(), new Callback(startRMData.getCallbackPackageName()));
                        beaconService.setScanPeriods(startRMData.getScanPeriod(), startRMData.getBetweenScanPeriod(), startRMData.getBackgroundFlag());
                        return;
                    case 5:
                        LogManager.i(BeaconService.TAG, "stop monitoring received", new Object[0]);
                        beaconService.stopMonitoringBeaconsInRegion(startRMData.getRegionData());
                        beaconService.setScanPeriods(startRMData.getScanPeriod(), startRMData.getBetweenScanPeriod(), startRMData.getBackgroundFlag());
                        return;
                    case 6:
                        LogManager.i(BeaconService.TAG, "set scan intervals received", new Object[0]);
                        beaconService.setScanPeriods(startRMData.getScanPeriod(), startRMData.getBetweenScanPeriod(), startRMData.getBackgroundFlag());
                        return;
                    default:
                        super.handleMessage(message);
                        return;
                }
            }
        }
    }

    private class ScanData {
        BluetoothDevice device;
        int rssi;
        byte[] scanRecord;

        public ScanData(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
            this.device = bluetoothDevice;
            this.rssi = i;
            this.scanRecord = bArr;
        }
    }

    private class ScanProcessor extends AsyncTask<ScanData, Void, Void> {
        DetectionTracker mDetectionTracker;

        /* access modifiers changed from: protected */
        public void onPostExecute(Void voidR) {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Void... voidArr) {
        }

        private ScanProcessor() {
            this.mDetectionTracker = DetectionTracker.getInstance();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(ScanData... scanDataArr) {
            ScanData scanData = scanDataArr[0];
            Beacon beacon = null;
            for (BeaconParser fromScanData : BeaconService.this.beaconParsers) {
                beacon = fromScanData.fromScanData(scanData.scanRecord, scanData.rssi, scanData.device);
                if (beacon != null) {
                    break;
                }
            }
            if (beacon != null) {
                this.mDetectionTracker.recordDetection();
                BeaconService.this.processBeaconFromScan(beacon);
            }
            return null;
        }
    }

    public IBinder onBind(Intent intent) {
        LogManager.i(TAG, "binding", new Object[0]);
        this.bindCount++;
        return this.mMessenger.getBinder();
    }

    public boolean onUnbind(Intent intent) {
        LogManager.i(TAG, "unbinding", new Object[0]);
        this.bindCount--;
        return false;
    }

    public void onCreate() {
        LogManager.i(TAG, "beaconService version %s is starting up", BuildConfig.VERSION_NAME);
        this.bluetoothCrashResolver = new BluetoothCrashResolver(this);
        this.bluetoothCrashResolver.start();
        this.mCycledScanner = CycledLeScanner.createScanner(this, BeaconManager.DEFAULT_FOREGROUND_SCAN_PERIOD, 0, this.mBackgroundFlag, this.mCycledLeScanCallback, this.bluetoothCrashResolver);
        this.beaconParsers = BeaconManager.getInstanceForApplication(getApplicationContext()).getBeaconParsers();
        this.defaultDistanceCalculator = new ModelSpecificDistanceCalculator(this, BeaconManager.getDistanceModelUpdateUrl());
        Beacon.setDistanceCalculator(this.defaultDistanceCalculator);
        try {
            this.simulatedScanData = (List) Class.forName("org.altbeacon.beacon.SimulatedScanData").getField("beacons").get(null);
        } catch (ClassNotFoundException unused) {
            LogManager.d(TAG, "No org.altbeacon.beacon.SimulatedScanData class exists.", new Object[0]);
        } catch (Exception e) {
            LogManager.e(e, TAG, "Cannot get simulated Scan data.  Make sure your org.altbeacon.beacon.SimulatedScanData class defines a field with the signature 'public static List<Beacon> beacons'", new Object[0]);
        }
    }

    @TargetApi(18)
    public void onDestroy() {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to API 18.", new Object[0]);
            return;
        }
        this.bluetoothCrashResolver.stop();
        LogManager.i(TAG, "onDestroy called.  stopping scanning", new Object[0]);
        this.handler.removeCallbacksAndMessages(null);
        this.mCycledScanner.stop();
    }

    public void startRangingBeaconsInRegion(Region region, Callback callback) {
        synchronized (this.rangedRegionState) {
            if (this.rangedRegionState.containsKey(region)) {
                LogManager.i(TAG, "Already ranging that region -- will replace existing region.", new Object[0]);
                this.rangedRegionState.remove(region);
            }
            this.rangedRegionState.put(region, new RangeState(callback));
            LogManager.d(TAG, "Currently ranging %s regions.", Integer.valueOf(this.rangedRegionState.size()));
        }
        if (!this.scanningEnabled) {
            this.mCycledScanner.start();
        }
    }

    public void stopRangingBeaconsInRegion(Region region) {
        int size;
        synchronized (this.rangedRegionState) {
            this.rangedRegionState.remove(region);
            size = this.rangedRegionState.size();
            LogManager.d(TAG, "Currently ranging %s regions.", Integer.valueOf(this.rangedRegionState.size()));
        }
        if (this.scanningEnabled && size == 0 && this.monitoredRegionState.size() == 0) {
            this.mCycledScanner.stop();
        }
    }

    public void startMonitoringBeaconsInRegion(Region region, Callback callback) {
        LogManager.d(TAG, "startMonitoring called", new Object[0]);
        synchronized (this.monitoredRegionState) {
            if (this.monitoredRegionState.containsKey(region)) {
                LogManager.i(TAG, "Already monitoring that region -- will replace existing region monitor.", new Object[0]);
                this.monitoredRegionState.remove(region);
            }
            this.monitoredRegionState.put(region, new MonitorState(callback));
        }
        LogManager.d(TAG, "Currently monitoring %s regions.", Integer.valueOf(this.monitoredRegionState.size()));
        if (!this.scanningEnabled) {
            this.mCycledScanner.start();
        }
    }

    public void stopMonitoringBeaconsInRegion(Region region) {
        int size;
        LogManager.d(TAG, "stopMonitoring called", new Object[0]);
        synchronized (this.monitoredRegionState) {
            this.monitoredRegionState.remove(region);
            size = this.monitoredRegionState.size();
        }
        LogManager.d(TAG, "Currently monitoring %s regions.", Integer.valueOf(this.monitoredRegionState.size()));
        if (this.scanningEnabled && size == 0 && this.monitoredRegionState.size() == 0) {
            this.mCycledScanner.stop();
        }
    }

    public void setScanPeriods(long j, long j2, boolean z) {
        this.mCycledScanner.setScanPeriods(j, j2, z);
    }

    /* access modifiers changed from: private */
    public void processRangeData() {
        synchronized (this.rangedRegionState) {
            for (Region region : this.rangedRegionState.keySet()) {
                RangeState rangeState = (RangeState) this.rangedRegionState.get(region);
                LogManager.d(TAG, "Calling ranging callback", new Object[0]);
                rangeState.getCallback().call(this, "rangingData", new RangingData(rangeState.finalizeBeacons(), region));
            }
        }
    }

    /* access modifiers changed from: private */
    public void processExpiredMonitors() {
        synchronized (this.monitoredRegionState) {
            for (Region region : this.monitoredRegionState.keySet()) {
                MonitorState monitorState = (MonitorState) this.monitoredRegionState.get(region);
                if (monitorState.isNewlyOutside()) {
                    LogManager.d(TAG, "found a monitor that expired: %s", region);
                    monitorState.getCallback().call(this, "monitoringData", new MonitoringData(monitorState.isInside(), region));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void processBeaconFromScan(Beacon beacon) {
        List<Region> matchingRegions;
        if (this.trackedBeacons == null) {
            this.trackedBeacons = new HashSet<>();
        }
        if (Stats.getInstance().isEnabled()) {
            Stats.getInstance().log(beacon);
        }
        this.trackedBeaconsPacketCount++;
        if (this.trackedBeacons.contains(beacon)) {
            LogManager.d(TAG, "beacon detected multiple times in scan cycle : %s", beacon.toString());
        }
        this.trackedBeacons.add(beacon);
        LogManager.d(TAG, "beacon detected : %s", beacon.toString());
        synchronized (this.monitoredRegionState) {
            matchingRegions = matchingRegions(beacon, this.monitoredRegionState.keySet());
        }
        for (Region region : matchingRegions) {
            MonitorState monitorState = (MonitorState) this.monitoredRegionState.get(region);
            if (monitorState.markInside()) {
                monitorState.getCallback().call(this, "monitoringData", new MonitoringData(monitorState.isInside(), region));
            }
        }
        LogManager.d(TAG, "looking for ranging region matches for this beacon", new Object[0]);
        synchronized (this.rangedRegionState) {
            for (Region region2 : matchingRegions(beacon, this.rangedRegionState.keySet())) {
                LogManager.d(TAG, "matches ranging region: %s", region2);
                ((RangeState) this.rangedRegionState.get(region2)).addBeacon(beacon);
            }
        }
    }

    private List<Region> matchingRegions(Beacon beacon, Collection<Region> collection) {
        ArrayList arrayList = new ArrayList();
        for (Region region : collection) {
            if (region.matchesBeacon(beacon)) {
                arrayList.add(region);
            } else {
                LogManager.d(TAG, "This region (%s) does not match beacon: %s", region, beacon);
            }
        }
        return arrayList;
    }
}
