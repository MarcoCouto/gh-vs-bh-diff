package org.altbeacon.beacon;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.beacon.logging.Loggers;
import org.altbeacon.beacon.service.BeaconService;
import org.altbeacon.beacon.service.StartRMData;
import org.altbeacon.beacon.simulator.BeaconSimulator;

@TargetApi(4)
public class BeaconManager {
    public static final long DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD = 300000;
    public static final long DEFAULT_BACKGROUND_SCAN_PERIOD = 10000;
    public static final long DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD = 0;
    public static final long DEFAULT_FOREGROUND_SCAN_PERIOD = 1100;
    private static final String TAG = "BeaconManager";
    protected static BeaconSimulator beaconSimulator = null;
    protected static BeaconManager client = null;
    protected static String distanceModelUpdateUrl = "http://data.altbeacon.org/android-distance.json";
    private static boolean sAndroidLScanningDisabled = false;
    private static boolean sManifestCheckingDisabled = false;
    private long backgroundBetweenScanPeriod = DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD;
    private long backgroundScanPeriod = DEFAULT_BACKGROUND_SCAN_PERIOD;
    private final ArrayList<BeaconParser> beaconParsers = new ArrayList<>();
    private ServiceConnection beaconServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            LogManager.d(BeaconManager.TAG, "we have a connection to the service now", new Object[0]);
            BeaconManager.this.serviceMessenger = new Messenger(iBinder);
            synchronized (BeaconManager.this.consumers) {
                for (Entry entry : BeaconManager.this.consumers.entrySet()) {
                    if (!((ConsumerInfo) entry.getValue()).isConnected) {
                        ((BeaconConsumer) entry.getKey()).onBeaconServiceConnect();
                        ((ConsumerInfo) entry.getValue()).isConnected = true;
                    }
                }
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            LogManager.e(BeaconManager.TAG, "onServiceDisconnected", new Object[0]);
            BeaconManager.this.serviceMessenger = null;
        }
    };
    /* access modifiers changed from: private */
    public final ConcurrentMap<BeaconConsumer, ConsumerInfo> consumers = new ConcurrentHashMap();
    protected RangeNotifier dataRequestNotifier = null;
    private long foregroundBetweenScanPeriod = 0;
    private long foregroundScanPeriod = DEFAULT_FOREGROUND_SCAN_PERIOD;
    private boolean mBackgroundMode = false;
    private boolean mBackgroundModeUninitialized = true;
    private Context mContext;
    protected MonitorNotifier monitorNotifier = null;
    private final ArrayList<Region> monitoredRegions = new ArrayList<>();
    protected RangeNotifier rangeNotifier = null;
    private final ArrayList<Region> rangedRegions = new ArrayList<>();
    /* access modifiers changed from: private */
    public Messenger serviceMessenger = null;

    private class ConsumerInfo {
        public boolean isConnected;

        private ConsumerInfo() {
            this.isConnected = false;
        }
    }

    public class ServiceNotDeclaredException extends RuntimeException {
        public ServiceNotDeclaredException() {
            super("The BeaconService is not properly declared in AndroidManifest.xml.  If using Eclipse, please verify that your project.properties has manifestmerger.enabled=true");
        }
    }

    @Deprecated
    public static void setDebug(boolean z) {
        if (z) {
            LogManager.setLogger(Loggers.verboseLogger());
            LogManager.setVerboseLoggingEnabled(true);
            return;
        }
        LogManager.setLogger(Loggers.empty());
        LogManager.setVerboseLoggingEnabled(false);
    }

    public void setForegroundScanPeriod(long j) {
        this.foregroundScanPeriod = j;
    }

    public void setForegroundBetweenScanPeriod(long j) {
        this.foregroundBetweenScanPeriod = j;
    }

    public void setBackgroundScanPeriod(long j) {
        this.backgroundScanPeriod = j;
    }

    public void setBackgroundBetweenScanPeriod(long j) {
        this.backgroundBetweenScanPeriod = j;
    }

    public static BeaconManager getInstanceForApplication(Context context) {
        if (client == null) {
            LogManager.d(TAG, "BeaconManager instance creation", new Object[0]);
            client = new BeaconManager(context);
        }
        return client;
    }

    public List<BeaconParser> getBeaconParsers() {
        if (isAnyConsumerBound()) {
            return Collections.unmodifiableList(this.beaconParsers);
        }
        return this.beaconParsers;
    }

    protected BeaconManager(Context context) {
        this.mContext = context;
        if (!sManifestCheckingDisabled) {
            verifyServiceDeclaration();
        }
        this.beaconParsers.add(new AltBeaconParser());
    }

    @TargetApi(18)
    public boolean checkAvailability() throws BleNotAvailableException {
        if (VERSION.SDK_INT < 18) {
            throw new BleNotAvailableException("Bluetooth LE not supported by this device");
        } else if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth_le")) {
            return ((BluetoothManager) this.mContext.getSystemService("bluetooth")).getAdapter().isEnabled();
        } else {
            throw new BleNotAvailableException("Bluetooth LE not supported by this device");
        }
    }

    public void bind(BeaconConsumer beaconConsumer) {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
            return;
        }
        synchronized (this.consumers) {
            if (((ConsumerInfo) this.consumers.putIfAbsent(beaconConsumer, new ConsumerInfo())) != null) {
                LogManager.d(TAG, "This consumer is already bound", new Object[0]);
            } else {
                LogManager.d(TAG, "This consumer is not bound.  binding: %s", beaconConsumer);
                beaconConsumer.bindService(new Intent(beaconConsumer.getApplicationContext(), BeaconService.class), this.beaconServiceConnection, 1);
                LogManager.d(TAG, "consumer count is now: %s", Integer.valueOf(this.consumers.size()));
            }
        }
    }

    public void unbind(BeaconConsumer beaconConsumer) {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
            return;
        }
        synchronized (this.consumers) {
            if (this.consumers.containsKey(beaconConsumer)) {
                LogManager.d(TAG, "Unbinding", new Object[0]);
                beaconConsumer.unbindService(this.beaconServiceConnection);
                this.consumers.remove(beaconConsumer);
                if (this.consumers.size() == 0) {
                    this.serviceMessenger = null;
                }
            } else {
                LogManager.d(TAG, "This consumer is not bound to: %s", beaconConsumer);
                LogManager.d(TAG, "Bound consumers: ", new Object[0]);
                for (Entry value : this.consumers.entrySet()) {
                    LogManager.d(TAG, String.valueOf(value.getValue()), new Object[0]);
                }
            }
        }
    }

    public boolean isBound(BeaconConsumer beaconConsumer) {
        boolean z;
        synchronized (this.consumers) {
            if (beaconConsumer != null) {
                try {
                    if (!(this.consumers.get(beaconConsumer) == null || this.serviceMessenger == null)) {
                        z = true;
                    }
                } finally {
                }
            }
            z = false;
        }
        return z;
    }

    public boolean isAnyConsumerBound() {
        boolean z;
        synchronized (this.consumers) {
            z = this.consumers.size() > 0 && this.serviceMessenger != null;
        }
        return z;
    }

    public void setBackgroundMode(boolean z) {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
        }
        this.mBackgroundModeUninitialized = false;
        if (z != this.mBackgroundMode) {
            this.mBackgroundMode = z;
            try {
                updateScanPeriods();
            } catch (RemoteException unused) {
                LogManager.e(TAG, "Cannot contact service to set scan periods", new Object[0]);
            }
        }
    }

    public boolean isBackgroundModeUninitialized() {
        return this.mBackgroundModeUninitialized;
    }

    public void setRangeNotifier(RangeNotifier rangeNotifier2) {
        this.rangeNotifier = rangeNotifier2;
    }

    public void setMonitorNotifier(MonitorNotifier monitorNotifier2) {
        this.monitorNotifier = monitorNotifier2;
    }

    @TargetApi(18)
    public void startRangingBeaconsInRegion(Region region) throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Message obtain = Message.obtain(null, 2, 0, 0);
            StartRMData startRMData = new StartRMData(region, callbackPackageName(), getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            obtain.obj = startRMData;
            this.serviceMessenger.send(obtain);
            synchronized (this.rangedRegions) {
                this.rangedRegions.add(region);
            }
        }
    }

    @TargetApi(18)
    public void stopRangingBeaconsInRegion(Region region) throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Region region2 = null;
            Message obtain = Message.obtain(null, 3, 0, 0);
            StartRMData startRMData = new StartRMData(region, callbackPackageName(), getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            obtain.obj = startRMData;
            this.serviceMessenger.send(obtain);
            synchronized (this.rangedRegions) {
                Iterator it = this.rangedRegions.iterator();
                while (it.hasNext()) {
                    Region region3 = (Region) it.next();
                    if (region.getUniqueId().equals(region3.getUniqueId())) {
                        region2 = region3;
                    }
                }
                this.rangedRegions.remove(region2);
            }
        }
    }

    @TargetApi(18)
    public void startMonitoringBeaconsInRegion(Region region) throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to API 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Message obtain = Message.obtain(null, 4, 0, 0);
            StartRMData startRMData = new StartRMData(region, callbackPackageName(), getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            obtain.obj = startRMData;
            this.serviceMessenger.send(obtain);
            synchronized (this.monitoredRegions) {
                this.monitoredRegions.add(region);
            }
        }
    }

    @TargetApi(18)
    public void stopMonitoringBeaconsInRegion(Region region) throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to API 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Region region2 = null;
            Message obtain = Message.obtain(null, 5, 0, 0);
            StartRMData startRMData = new StartRMData(region, callbackPackageName(), getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            obtain.obj = startRMData;
            this.serviceMessenger.send(obtain);
            synchronized (this.monitoredRegions) {
                Iterator it = this.monitoredRegions.iterator();
                while (it.hasNext()) {
                    Region region3 = (Region) it.next();
                    if (region.getUniqueId().equals(region3.getUniqueId())) {
                        region2 = region3;
                    }
                }
                this.monitoredRegions.remove(region2);
            }
        }
    }

    @TargetApi(18)
    public void updateScanPeriods() throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to API 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Message obtain = Message.obtain(null, 6, 0, 0);
            LogManager.d(TAG, "updating background flag to %s", Boolean.valueOf(this.mBackgroundMode));
            LogManager.d(TAG, "updating scan period to %s, %s", Long.valueOf(getScanPeriod()), Long.valueOf(getBetweenScanPeriod()));
            StartRMData startRMData = new StartRMData(getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            obtain.obj = startRMData;
            this.serviceMessenger.send(obtain);
        }
    }

    private String callbackPackageName() {
        String packageName = this.mContext.getPackageName();
        LogManager.d(TAG, "callback packageName: %s", packageName);
        return packageName;
    }

    public MonitorNotifier getMonitoringNotifier() {
        return this.monitorNotifier;
    }

    public RangeNotifier getRangingNotifier() {
        return this.rangeNotifier;
    }

    public Collection<Region> getMonitoredRegions() {
        ArrayList arrayList;
        synchronized (this.monitoredRegions) {
            arrayList = new ArrayList(this.monitoredRegions);
        }
        return arrayList;
    }

    public Collection<Region> getRangedRegions() {
        ArrayList arrayList;
        synchronized (this.rangedRegions) {
            arrayList = new ArrayList(this.rangedRegions);
        }
        return arrayList;
    }

    @Deprecated
    public static void logDebug(String str, String str2) {
        LogManager.d(str, str2, new Object[0]);
    }

    @Deprecated
    public static void logDebug(String str, String str2, Throwable th) {
        LogManager.d(th, str, str2, new Object[0]);
    }

    public static String getDistanceModelUpdateUrl() {
        return distanceModelUpdateUrl;
    }

    public static void setDistanceModelUpdateUrl(String str) {
        distanceModelUpdateUrl = str;
    }

    public static void setBeaconSimulator(BeaconSimulator beaconSimulator2) {
        beaconSimulator = beaconSimulator2;
    }

    public static BeaconSimulator getBeaconSimulator() {
        return beaconSimulator;
    }

    /* access modifiers changed from: protected */
    public void setDataRequestNotifier(RangeNotifier rangeNotifier2) {
        this.dataRequestNotifier = rangeNotifier2;
    }

    /* access modifiers changed from: protected */
    public RangeNotifier getDataRequestNotifier() {
        return this.dataRequestNotifier;
    }

    private long getScanPeriod() {
        if (this.mBackgroundMode) {
            return this.backgroundScanPeriod;
        }
        return this.foregroundScanPeriod;
    }

    private long getBetweenScanPeriod() {
        if (this.mBackgroundMode) {
            return this.backgroundBetweenScanPeriod;
        }
        return this.foregroundBetweenScanPeriod;
    }

    private void verifyServiceDeclaration() {
        if (this.mContext.getPackageManager().queryIntentServices(new Intent(this.mContext, BeaconService.class), 65536).size() == 0) {
            throw new ServiceNotDeclaredException();
        }
    }

    public static boolean isAndroidLScanningDisabled() {
        return sAndroidLScanningDisabled;
    }

    public static void setAndroidLScanningDisabled(boolean z) {
        sAndroidLScanningDisabled = z;
    }

    public static void setsManifestCheckingDisabled(boolean z) {
        sManifestCheckingDisabled = z;
    }
}
