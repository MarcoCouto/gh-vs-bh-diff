package android.support.coreutils;

public final class R {

    public static final class attr {
        public static final int font = 2130968751;
        public static final int fontProviderAuthority = 2130968753;
        public static final int fontProviderCerts = 2130968754;
        public static final int fontProviderFetchStrategy = 2130968755;
        public static final int fontProviderFetchTimeout = 2130968756;
        public static final int fontProviderPackage = 2130968757;
        public static final int fontProviderQuery = 2130968758;
        public static final int fontStyle = 2130968759;
        public static final int fontWeight = 2130968760;
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2131034112;
    }

    public static final class color {
        public static final int notification_action_color_filter = 2131099770;
        public static final int notification_icon_bg_color = 2131099771;
        public static final int ripple_material_light = 2131099782;
        public static final int secondary_text_default_material_light = 2131099784;
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131165283;
        public static final int compat_button_inset_vertical_material = 2131165284;
        public static final int compat_button_padding_horizontal_material = 2131165285;
        public static final int compat_button_padding_vertical_material = 2131165286;
        public static final int compat_control_corner_material = 2131165287;
        public static final int notification_action_icon_size = 2131165349;
        public static final int notification_action_text_size = 2131165350;
        public static final int notification_big_circle_margin = 2131165351;
        public static final int notification_content_margin_start = 2131165352;
        public static final int notification_large_icon_height = 2131165353;
        public static final int notification_large_icon_width = 2131165354;
        public static final int notification_main_column_padding_top = 2131165355;
        public static final int notification_media_narrow_margin = 2131165356;
        public static final int notification_right_icon_size = 2131165357;
        public static final int notification_right_side_padding_top = 2131165358;
        public static final int notification_small_icon_background_padding = 2131165359;
        public static final int notification_small_icon_size_as_large = 2131165360;
        public static final int notification_subtext_size = 2131165361;
        public static final int notification_top_pad = 2131165362;
        public static final int notification_top_pad_large_text = 2131165363;
    }

    public static final class drawable {
        public static final int notification_action_background = 2131231057;
        public static final int notification_bg = 2131231058;
        public static final int notification_bg_low = 2131231059;
        public static final int notification_bg_low_normal = 2131231060;
        public static final int notification_bg_low_pressed = 2131231061;
        public static final int notification_bg_normal = 2131231062;
        public static final int notification_bg_normal_pressed = 2131231063;
        public static final int notification_icon_background = 2131231064;
        public static final int notification_template_icon_bg = 2131231065;
        public static final int notification_template_icon_low_bg = 2131231066;
        public static final int notification_tile_bg = 2131231067;
        public static final int notify_panel_notification_icon_bg = 2131231068;
    }

    public static final class id {
        public static final int action_container = 2131296273;
        public static final int action_divider = 2131296275;
        public static final int action_image = 2131296276;
        public static final int action_text = 2131296287;
        public static final int actions = 2131296288;
        public static final int async = 2131296312;
        public static final int blocking = 2131296329;
        public static final int chronometer = 2131296352;
        public static final int forever = 2131296402;
        public static final int icon = 2131296408;
        public static final int icon_group = 2131296409;
        public static final int info = 2131296418;
        public static final int italic = 2131296423;
        public static final int line1 = 2131296433;
        public static final int line3 = 2131296434;
        public static final int normal = 2131296469;
        public static final int notification_background = 2131296470;
        public static final int notification_main_column = 2131296471;
        public static final int notification_main_column_container = 2131296472;
        public static final int right_icon = 2131296488;
        public static final int right_side = 2131296489;
        public static final int tag_transition_group = 2131296543;
        public static final int text = 2131296553;
        public static final int text2 = 2131296554;
        public static final int time = 2131296560;
        public static final int title = 2131296561;
    }

    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 2131361802;
    }

    public static final class layout {
        public static final int notification_action = 2131427403;
        public static final int notification_action_tombstone = 2131427404;
        public static final int notification_template_custom_big = 2131427411;
        public static final int notification_template_icon_group = 2131427412;
        public static final int notification_template_part_chronometer = 2131427416;
        public static final int notification_template_part_time = 2131427417;
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131624190;
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131689735;
        public static final int TextAppearance_Compat_Notification_Info = 2131689736;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131689738;
        public static final int TextAppearance_Compat_Notification_Time = 2131689741;
        public static final int TextAppearance_Compat_Notification_Title = 2131689743;
        public static final int Widget_Compat_NotificationActionContainer = 2131689869;
        public static final int Widget_Compat_NotificationActionText = 2131689870;
    }

    public static final class styleable {
        public static final int[] FontFamily = {com.mansoon.BatteryDouble.R.attr.fontProviderAuthority, com.mansoon.BatteryDouble.R.attr.fontProviderCerts, com.mansoon.BatteryDouble.R.attr.fontProviderFetchStrategy, com.mansoon.BatteryDouble.R.attr.fontProviderFetchTimeout, com.mansoon.BatteryDouble.R.attr.fontProviderPackage, com.mansoon.BatteryDouble.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, com.mansoon.BatteryDouble.R.attr.font, com.mansoon.BatteryDouble.R.attr.fontStyle, com.mansoon.BatteryDouble.R.attr.fontWeight};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_font = 3;
        public static final int FontFamilyFont_fontStyle = 4;
        public static final int FontFamilyFont_fontWeight = 5;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
    }
}
