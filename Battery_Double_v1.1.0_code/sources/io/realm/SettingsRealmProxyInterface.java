package io.realm;

public interface SettingsRealmProxyInterface {
    boolean realmGet$bluetoothEnabled();

    int realmGet$developerMode();

    boolean realmGet$flashlightEnabled();

    boolean realmGet$locationEnabled();

    boolean realmGet$nfcEnabled();

    boolean realmGet$powersaverEnabled();

    int realmGet$unknownSources();

    void realmSet$bluetoothEnabled(boolean z);

    void realmSet$developerMode(int i);

    void realmSet$flashlightEnabled(boolean z);

    void realmSet$locationEnabled(boolean z);

    void realmSet$nfcEnabled(boolean z);

    void realmSet$powersaverEnabled(boolean z);

    void realmSet$unknownSources(int i);
}
