package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.mansoon.BatteryDouble.models.data.Settings;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SettingsRealmProxy extends Settings implements RealmObjectProxy, SettingsRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private SettingsColumnInfo columnInfo;
    private ProxyState<Settings> proxyState;

    static final class SettingsColumnInfo extends ColumnInfo {
        long bluetoothEnabledIndex;
        long developerModeIndex;
        long flashlightEnabledIndex;
        long locationEnabledIndex;
        long nfcEnabledIndex;
        long powersaverEnabledIndex;
        long unknownSourcesIndex;

        SettingsColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(7);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("Settings");
            this.bluetoothEnabledIndex = addColumnDetails("bluetoothEnabled", objectSchemaInfo);
            this.locationEnabledIndex = addColumnDetails("locationEnabled", objectSchemaInfo);
            this.powersaverEnabledIndex = addColumnDetails("powersaverEnabled", objectSchemaInfo);
            this.flashlightEnabledIndex = addColumnDetails("flashlightEnabled", objectSchemaInfo);
            this.nfcEnabledIndex = addColumnDetails("nfcEnabled", objectSchemaInfo);
            this.unknownSourcesIndex = addColumnDetails("unknownSources", objectSchemaInfo);
            this.developerModeIndex = addColumnDetails("developerMode", objectSchemaInfo);
        }

        SettingsColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new SettingsColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            SettingsColumnInfo settingsColumnInfo = (SettingsColumnInfo) columnInfo;
            SettingsColumnInfo settingsColumnInfo2 = (SettingsColumnInfo) columnInfo2;
            settingsColumnInfo2.bluetoothEnabledIndex = settingsColumnInfo.bluetoothEnabledIndex;
            settingsColumnInfo2.locationEnabledIndex = settingsColumnInfo.locationEnabledIndex;
            settingsColumnInfo2.powersaverEnabledIndex = settingsColumnInfo.powersaverEnabledIndex;
            settingsColumnInfo2.flashlightEnabledIndex = settingsColumnInfo.flashlightEnabledIndex;
            settingsColumnInfo2.nfcEnabledIndex = settingsColumnInfo.nfcEnabledIndex;
            settingsColumnInfo2.unknownSourcesIndex = settingsColumnInfo.unknownSourcesIndex;
            settingsColumnInfo2.developerModeIndex = settingsColumnInfo.developerModeIndex;
        }
    }

    public static String getTableName() {
        return "class_Settings";
    }

    static {
        ArrayList arrayList = new ArrayList(7);
        arrayList.add("bluetoothEnabled");
        arrayList.add("locationEnabled");
        arrayList.add("powersaverEnabled");
        arrayList.add("flashlightEnabled");
        arrayList.add("nfcEnabled");
        arrayList.add("unknownSources");
        arrayList.add("developerMode");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    SettingsRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (SettingsColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public boolean realmGet$bluetoothEnabled() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getBoolean(this.columnInfo.bluetoothEnabledIndex);
    }

    public void realmSet$bluetoothEnabled(boolean z) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setBoolean(this.columnInfo.bluetoothEnabledIndex, z);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setBoolean(this.columnInfo.bluetoothEnabledIndex, row$realm.getIndex(), z, true);
        }
    }

    public boolean realmGet$locationEnabled() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getBoolean(this.columnInfo.locationEnabledIndex);
    }

    public void realmSet$locationEnabled(boolean z) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setBoolean(this.columnInfo.locationEnabledIndex, z);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setBoolean(this.columnInfo.locationEnabledIndex, row$realm.getIndex(), z, true);
        }
    }

    public boolean realmGet$powersaverEnabled() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getBoolean(this.columnInfo.powersaverEnabledIndex);
    }

    public void realmSet$powersaverEnabled(boolean z) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setBoolean(this.columnInfo.powersaverEnabledIndex, z);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setBoolean(this.columnInfo.powersaverEnabledIndex, row$realm.getIndex(), z, true);
        }
    }

    public boolean realmGet$flashlightEnabled() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getBoolean(this.columnInfo.flashlightEnabledIndex);
    }

    public void realmSet$flashlightEnabled(boolean z) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setBoolean(this.columnInfo.flashlightEnabledIndex, z);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setBoolean(this.columnInfo.flashlightEnabledIndex, row$realm.getIndex(), z, true);
        }
    }

    public boolean realmGet$nfcEnabled() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getBoolean(this.columnInfo.nfcEnabledIndex);
    }

    public void realmSet$nfcEnabled(boolean z) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setBoolean(this.columnInfo.nfcEnabledIndex, z);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setBoolean(this.columnInfo.nfcEnabledIndex, row$realm.getIndex(), z, true);
        }
    }

    public int realmGet$unknownSources() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.unknownSourcesIndex);
    }

    public void realmSet$unknownSources(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.unknownSourcesIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.unknownSourcesIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$developerMode() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.developerModeIndex);
    }

    public void realmSet$developerMode(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.developerModeIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.developerModeIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("Settings", 7, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("bluetoothEnabled", RealmFieldType.BOOLEAN, false, false, true);
        builder2.addPersistedProperty("locationEnabled", RealmFieldType.BOOLEAN, false, false, true);
        builder2.addPersistedProperty("powersaverEnabled", RealmFieldType.BOOLEAN, false, false, true);
        builder2.addPersistedProperty("flashlightEnabled", RealmFieldType.BOOLEAN, false, false, true);
        builder2.addPersistedProperty("nfcEnabled", RealmFieldType.BOOLEAN, false, false, true);
        builder2.addPersistedProperty("unknownSources", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("developerMode", RealmFieldType.INTEGER, false, false, true);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static SettingsColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new SettingsColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Settings createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        Settings settings = (Settings) realm.createObjectInternal(Settings.class, true, Collections.emptyList());
        SettingsRealmProxyInterface settingsRealmProxyInterface = settings;
        if (jSONObject.has("bluetoothEnabled")) {
            if (jSONObject.isNull("bluetoothEnabled")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'bluetoothEnabled' to null.");
            }
            settingsRealmProxyInterface.realmSet$bluetoothEnabled(jSONObject.getBoolean("bluetoothEnabled"));
        }
        if (jSONObject.has("locationEnabled")) {
            if (jSONObject.isNull("locationEnabled")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'locationEnabled' to null.");
            }
            settingsRealmProxyInterface.realmSet$locationEnabled(jSONObject.getBoolean("locationEnabled"));
        }
        if (jSONObject.has("powersaverEnabled")) {
            if (jSONObject.isNull("powersaverEnabled")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'powersaverEnabled' to null.");
            }
            settingsRealmProxyInterface.realmSet$powersaverEnabled(jSONObject.getBoolean("powersaverEnabled"));
        }
        if (jSONObject.has("flashlightEnabled")) {
            if (jSONObject.isNull("flashlightEnabled")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'flashlightEnabled' to null.");
            }
            settingsRealmProxyInterface.realmSet$flashlightEnabled(jSONObject.getBoolean("flashlightEnabled"));
        }
        if (jSONObject.has("nfcEnabled")) {
            if (jSONObject.isNull("nfcEnabled")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'nfcEnabled' to null.");
            }
            settingsRealmProxyInterface.realmSet$nfcEnabled(jSONObject.getBoolean("nfcEnabled"));
        }
        if (jSONObject.has("unknownSources")) {
            if (jSONObject.isNull("unknownSources")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'unknownSources' to null.");
            }
            settingsRealmProxyInterface.realmSet$unknownSources(jSONObject.getInt("unknownSources"));
        }
        if (jSONObject.has("developerMode")) {
            if (jSONObject.isNull("developerMode")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'developerMode' to null.");
            }
            settingsRealmProxyInterface.realmSet$developerMode(jSONObject.getInt("developerMode"));
        }
        return settings;
    }

    @TargetApi(11)
    public static Settings createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        Settings settings = new Settings();
        SettingsRealmProxyInterface settingsRealmProxyInterface = settings;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("bluetoothEnabled")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    settingsRealmProxyInterface.realmSet$bluetoothEnabled(jsonReader.nextBoolean());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'bluetoothEnabled' to null.");
                }
            } else if (nextName.equals("locationEnabled")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    settingsRealmProxyInterface.realmSet$locationEnabled(jsonReader.nextBoolean());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'locationEnabled' to null.");
                }
            } else if (nextName.equals("powersaverEnabled")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    settingsRealmProxyInterface.realmSet$powersaverEnabled(jsonReader.nextBoolean());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'powersaverEnabled' to null.");
                }
            } else if (nextName.equals("flashlightEnabled")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    settingsRealmProxyInterface.realmSet$flashlightEnabled(jsonReader.nextBoolean());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'flashlightEnabled' to null.");
                }
            } else if (nextName.equals("nfcEnabled")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    settingsRealmProxyInterface.realmSet$nfcEnabled(jsonReader.nextBoolean());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'nfcEnabled' to null.");
                }
            } else if (nextName.equals("unknownSources")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    settingsRealmProxyInterface.realmSet$unknownSources(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'unknownSources' to null.");
                }
            } else if (!nextName.equals("developerMode")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                settingsRealmProxyInterface.realmSet$developerMode(jsonReader.nextInt());
            } else {
                jsonReader.skipValue();
                throw new IllegalArgumentException("Trying to set non-nullable field 'developerMode' to null.");
            }
        }
        jsonReader.endObject();
        return (Settings) realm.copyToRealm(settings);
    }

    public static Settings copyOrUpdate(Realm realm, Settings settings, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (settings instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) settings;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return settings;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(settings);
        if (realmObjectProxy2 != null) {
            return (Settings) realmObjectProxy2;
        }
        return copy(realm, settings, z, map);
    }

    public static Settings copy(Realm realm, Settings settings, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(settings);
        if (realmObjectProxy != null) {
            return (Settings) realmObjectProxy;
        }
        Settings settings2 = (Settings) realm.createObjectInternal(Settings.class, false, Collections.emptyList());
        map.put(settings, (RealmObjectProxy) settings2);
        SettingsRealmProxyInterface settingsRealmProxyInterface = settings;
        SettingsRealmProxyInterface settingsRealmProxyInterface2 = settings2;
        settingsRealmProxyInterface2.realmSet$bluetoothEnabled(settingsRealmProxyInterface.realmGet$bluetoothEnabled());
        settingsRealmProxyInterface2.realmSet$locationEnabled(settingsRealmProxyInterface.realmGet$locationEnabled());
        settingsRealmProxyInterface2.realmSet$powersaverEnabled(settingsRealmProxyInterface.realmGet$powersaverEnabled());
        settingsRealmProxyInterface2.realmSet$flashlightEnabled(settingsRealmProxyInterface.realmGet$flashlightEnabled());
        settingsRealmProxyInterface2.realmSet$nfcEnabled(settingsRealmProxyInterface.realmGet$nfcEnabled());
        settingsRealmProxyInterface2.realmSet$unknownSources(settingsRealmProxyInterface.realmGet$unknownSources());
        settingsRealmProxyInterface2.realmSet$developerMode(settingsRealmProxyInterface.realmGet$developerMode());
        return settings2;
    }

    public static long insert(Realm realm, Settings settings, Map<RealmModel, Long> map) {
        Settings settings2 = settings;
        if (settings2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) settings2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(Settings.class);
        long nativePtr = table.getNativePtr();
        SettingsColumnInfo settingsColumnInfo = (SettingsColumnInfo) realm.getSchema().getColumnInfo(Settings.class);
        long createRow = OsObject.createRow(table);
        map.put(settings2, Long.valueOf(createRow));
        SettingsRealmProxyInterface settingsRealmProxyInterface = settings2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetBoolean(j, settingsColumnInfo.bluetoothEnabledIndex, j2, settingsRealmProxyInterface.realmGet$bluetoothEnabled(), false);
        Table.nativeSetBoolean(j, settingsColumnInfo.locationEnabledIndex, j2, settingsRealmProxyInterface.realmGet$locationEnabled(), false);
        Table.nativeSetBoolean(j, settingsColumnInfo.powersaverEnabledIndex, j2, settingsRealmProxyInterface.realmGet$powersaverEnabled(), false);
        Table.nativeSetBoolean(j, settingsColumnInfo.flashlightEnabledIndex, j2, settingsRealmProxyInterface.realmGet$flashlightEnabled(), false);
        Table.nativeSetBoolean(j, settingsColumnInfo.nfcEnabledIndex, j2, settingsRealmProxyInterface.realmGet$nfcEnabled(), false);
        Table.nativeSetLong(j, settingsColumnInfo.unknownSourcesIndex, j2, (long) settingsRealmProxyInterface.realmGet$unknownSources(), false);
        Table.nativeSetLong(j, settingsColumnInfo.developerModeIndex, j2, (long) settingsRealmProxyInterface.realmGet$developerMode(), false);
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(Settings.class);
        long nativePtr = table.getNativePtr();
        SettingsColumnInfo settingsColumnInfo = (SettingsColumnInfo) realm.getSchema().getColumnInfo(Settings.class);
        while (it.hasNext()) {
            Settings settings = (Settings) it.next();
            if (!map2.containsKey(settings)) {
                if (settings instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) settings;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(settings, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(settings, Long.valueOf(createRow));
                SettingsRealmProxyInterface settingsRealmProxyInterface = settings;
                long j = nativePtr;
                long j2 = createRow;
                Table.nativeSetBoolean(j, settingsColumnInfo.bluetoothEnabledIndex, createRow, settingsRealmProxyInterface.realmGet$bluetoothEnabled(), false);
                long j3 = j2;
                Table.nativeSetBoolean(j, settingsColumnInfo.locationEnabledIndex, j3, settingsRealmProxyInterface.realmGet$locationEnabled(), false);
                Table.nativeSetBoolean(j, settingsColumnInfo.powersaverEnabledIndex, j3, settingsRealmProxyInterface.realmGet$powersaverEnabled(), false);
                Table.nativeSetBoolean(j, settingsColumnInfo.flashlightEnabledIndex, j3, settingsRealmProxyInterface.realmGet$flashlightEnabled(), false);
                Table.nativeSetBoolean(j, settingsColumnInfo.nfcEnabledIndex, j3, settingsRealmProxyInterface.realmGet$nfcEnabled(), false);
                Table.nativeSetLong(nativePtr, settingsColumnInfo.unknownSourcesIndex, j3, (long) settingsRealmProxyInterface.realmGet$unknownSources(), false);
                Table.nativeSetLong(nativePtr, settingsColumnInfo.developerModeIndex, j3, (long) settingsRealmProxyInterface.realmGet$developerMode(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, Settings settings, Map<RealmModel, Long> map) {
        Settings settings2 = settings;
        if (settings2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) settings2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(Settings.class);
        long nativePtr = table.getNativePtr();
        SettingsColumnInfo settingsColumnInfo = (SettingsColumnInfo) realm.getSchema().getColumnInfo(Settings.class);
        long createRow = OsObject.createRow(table);
        map.put(settings2, Long.valueOf(createRow));
        SettingsRealmProxyInterface settingsRealmProxyInterface = settings2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetBoolean(j, settingsColumnInfo.bluetoothEnabledIndex, j2, settingsRealmProxyInterface.realmGet$bluetoothEnabled(), false);
        Table.nativeSetBoolean(j, settingsColumnInfo.locationEnabledIndex, j2, settingsRealmProxyInterface.realmGet$locationEnabled(), false);
        Table.nativeSetBoolean(j, settingsColumnInfo.powersaverEnabledIndex, j2, settingsRealmProxyInterface.realmGet$powersaverEnabled(), false);
        Table.nativeSetBoolean(j, settingsColumnInfo.flashlightEnabledIndex, j2, settingsRealmProxyInterface.realmGet$flashlightEnabled(), false);
        Table.nativeSetBoolean(j, settingsColumnInfo.nfcEnabledIndex, j2, settingsRealmProxyInterface.realmGet$nfcEnabled(), false);
        Table.nativeSetLong(j, settingsColumnInfo.unknownSourcesIndex, j2, (long) settingsRealmProxyInterface.realmGet$unknownSources(), false);
        Table.nativeSetLong(j, settingsColumnInfo.developerModeIndex, j2, (long) settingsRealmProxyInterface.realmGet$developerMode(), false);
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(Settings.class);
        long nativePtr = table.getNativePtr();
        SettingsColumnInfo settingsColumnInfo = (SettingsColumnInfo) realm.getSchema().getColumnInfo(Settings.class);
        while (it.hasNext()) {
            Settings settings = (Settings) it.next();
            if (!map2.containsKey(settings)) {
                if (settings instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) settings;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(settings, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(settings, Long.valueOf(createRow));
                SettingsRealmProxyInterface settingsRealmProxyInterface = settings;
                long j = nativePtr;
                long j2 = createRow;
                Table.nativeSetBoolean(j, settingsColumnInfo.bluetoothEnabledIndex, createRow, settingsRealmProxyInterface.realmGet$bluetoothEnabled(), false);
                long j3 = j2;
                Table.nativeSetBoolean(j, settingsColumnInfo.locationEnabledIndex, j3, settingsRealmProxyInterface.realmGet$locationEnabled(), false);
                Table.nativeSetBoolean(j, settingsColumnInfo.powersaverEnabledIndex, j3, settingsRealmProxyInterface.realmGet$powersaverEnabled(), false);
                Table.nativeSetBoolean(j, settingsColumnInfo.flashlightEnabledIndex, j3, settingsRealmProxyInterface.realmGet$flashlightEnabled(), false);
                Table.nativeSetBoolean(j, settingsColumnInfo.nfcEnabledIndex, j3, settingsRealmProxyInterface.realmGet$nfcEnabled(), false);
                Table.nativeSetLong(nativePtr, settingsColumnInfo.unknownSourcesIndex, j3, (long) settingsRealmProxyInterface.realmGet$unknownSources(), false);
                Table.nativeSetLong(nativePtr, settingsColumnInfo.developerModeIndex, j3, (long) settingsRealmProxyInterface.realmGet$developerMode(), false);
            }
        }
    }

    public static Settings createDetachedCopy(Settings settings, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        Settings settings2;
        if (i > i2 || settings == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(settings);
        if (cacheData == null) {
            settings2 = new Settings();
            map.put(settings, new CacheData(i, settings2));
        } else if (i >= cacheData.minDepth) {
            return (Settings) cacheData.object;
        } else {
            Settings settings3 = (Settings) cacheData.object;
            cacheData.minDepth = i;
            settings2 = settings3;
        }
        SettingsRealmProxyInterface settingsRealmProxyInterface = settings2;
        SettingsRealmProxyInterface settingsRealmProxyInterface2 = settings;
        settingsRealmProxyInterface.realmSet$bluetoothEnabled(settingsRealmProxyInterface2.realmGet$bluetoothEnabled());
        settingsRealmProxyInterface.realmSet$locationEnabled(settingsRealmProxyInterface2.realmGet$locationEnabled());
        settingsRealmProxyInterface.realmSet$powersaverEnabled(settingsRealmProxyInterface2.realmGet$powersaverEnabled());
        settingsRealmProxyInterface.realmSet$flashlightEnabled(settingsRealmProxyInterface2.realmGet$flashlightEnabled());
        settingsRealmProxyInterface.realmSet$nfcEnabled(settingsRealmProxyInterface2.realmGet$nfcEnabled());
        settingsRealmProxyInterface.realmSet$unknownSources(settingsRealmProxyInterface2.realmGet$unknownSources());
        settingsRealmProxyInterface.realmSet$developerMode(settingsRealmProxyInterface2.realmGet$developerMode());
        return settings2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Settings = proxy[");
        sb.append("{bluetoothEnabled:");
        sb.append(realmGet$bluetoothEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{locationEnabled:");
        sb.append(realmGet$locationEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{powersaverEnabled:");
        sb.append(realmGet$powersaverEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{flashlightEnabled:");
        sb.append(realmGet$flashlightEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{nfcEnabled:");
        sb.append(realmGet$nfcEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{unknownSources:");
        sb.append(realmGet$unknownSources());
        sb.append("}");
        sb.append(",");
        sb.append("{developerMode:");
        sb.append(realmGet$developerMode());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SettingsRealmProxy settingsRealmProxy = (SettingsRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = settingsRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = settingsRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == settingsRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
