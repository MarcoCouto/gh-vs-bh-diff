package io.realm;

import android.util.JsonReader;
import com.mansoon.BatteryDouble.models.data.AppPermission;
import com.mansoon.BatteryDouble.models.data.AppSignature;
import com.mansoon.BatteryDouble.models.data.BatteryDetails;
import com.mansoon.BatteryDouble.models.data.BatterySession;
import com.mansoon.BatteryDouble.models.data.BatteryUsage;
import com.mansoon.BatteryDouble.models.data.CallInfo;
import com.mansoon.BatteryDouble.models.data.CallMonth;
import com.mansoon.BatteryDouble.models.data.CellInfo;
import com.mansoon.BatteryDouble.models.data.CpuStatus;
import com.mansoon.BatteryDouble.models.data.Device;
import com.mansoon.BatteryDouble.models.data.Feature;
import com.mansoon.BatteryDouble.models.data.LocationProvider;
import com.mansoon.BatteryDouble.models.data.Message;
import com.mansoon.BatteryDouble.models.data.NetworkDetails;
import com.mansoon.BatteryDouble.models.data.NetworkStatistics;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import com.mansoon.BatteryDouble.models.data.Sample;
import com.mansoon.BatteryDouble.models.data.Settings;
import com.mansoon.BatteryDouble.models.data.StorageDetails;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.annotations.RealmModule;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Row;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {
    private static final Set<Class<? extends RealmModel>> MODEL_CLASSES;

    public boolean transformerApplied() {
        return true;
    }

    DefaultRealmModuleMediator() {
    }

    static {
        HashSet hashSet = new HashSet(19);
        hashSet.add(AppPermission.class);
        hashSet.add(LocationProvider.class);
        hashSet.add(ProcessInfo.class);
        hashSet.add(Device.class);
        hashSet.add(NetworkDetails.class);
        hashSet.add(Sample.class);
        hashSet.add(CpuStatus.class);
        hashSet.add(Feature.class);
        hashSet.add(BatteryDetails.class);
        hashSet.add(CellInfo.class);
        hashSet.add(NetworkStatistics.class);
        hashSet.add(AppSignature.class);
        hashSet.add(BatteryUsage.class);
        hashSet.add(StorageDetails.class);
        hashSet.add(CallMonth.class);
        hashSet.add(Settings.class);
        hashSet.add(CallInfo.class);
        hashSet.add(Message.class);
        hashSet.add(BatterySession.class);
        MODEL_CLASSES = Collections.unmodifiableSet(hashSet);
    }

    public Map<Class<? extends RealmModel>, OsObjectSchemaInfo> getExpectedObjectSchemaInfoMap() {
        HashMap hashMap = new HashMap(19);
        hashMap.put(AppPermission.class, AppPermissionRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(LocationProvider.class, LocationProviderRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(ProcessInfo.class, ProcessInfoRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(Device.class, DeviceRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(NetworkDetails.class, NetworkDetailsRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(Sample.class, SampleRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(CpuStatus.class, CpuStatusRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(Feature.class, FeatureRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(BatteryDetails.class, BatteryDetailsRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(CellInfo.class, CellInfoRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(NetworkStatistics.class, NetworkStatisticsRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(AppSignature.class, AppSignatureRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(BatteryUsage.class, BatteryUsageRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(StorageDetails.class, StorageDetailsRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(CallMonth.class, CallMonthRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(Settings.class, SettingsRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(CallInfo.class, CallInfoRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(Message.class, MessageRealmProxy.getExpectedObjectSchemaInfo());
        hashMap.put(BatterySession.class, BatterySessionRealmProxy.getExpectedObjectSchemaInfo());
        return hashMap;
    }

    public ColumnInfo createColumnInfo(Class<? extends RealmModel> cls, OsSchemaInfo osSchemaInfo) {
        checkClass(cls);
        if (cls.equals(AppPermission.class)) {
            return AppPermissionRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(LocationProvider.class)) {
            return LocationProviderRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(ProcessInfo.class)) {
            return ProcessInfoRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(Device.class)) {
            return DeviceRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(NetworkDetails.class)) {
            return NetworkDetailsRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(Sample.class)) {
            return SampleRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(CpuStatus.class)) {
            return CpuStatusRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(Feature.class)) {
            return FeatureRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(BatteryDetails.class)) {
            return BatteryDetailsRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(CellInfo.class)) {
            return CellInfoRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(NetworkStatistics.class)) {
            return NetworkStatisticsRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(AppSignature.class)) {
            return AppSignatureRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(BatteryUsage.class)) {
            return BatteryUsageRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(StorageDetails.class)) {
            return StorageDetailsRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(CallMonth.class)) {
            return CallMonthRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(Settings.class)) {
            return SettingsRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(CallInfo.class)) {
            return CallInfoRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(Message.class)) {
            return MessageRealmProxy.createColumnInfo(osSchemaInfo);
        }
        if (cls.equals(BatterySession.class)) {
            return BatterySessionRealmProxy.createColumnInfo(osSchemaInfo);
        }
        throw getMissingProxyClassException(cls);
    }

    public List<String> getFieldNames(Class<? extends RealmModel> cls) {
        checkClass(cls);
        if (cls.equals(AppPermission.class)) {
            return AppPermissionRealmProxy.getFieldNames();
        }
        if (cls.equals(LocationProvider.class)) {
            return LocationProviderRealmProxy.getFieldNames();
        }
        if (cls.equals(ProcessInfo.class)) {
            return ProcessInfoRealmProxy.getFieldNames();
        }
        if (cls.equals(Device.class)) {
            return DeviceRealmProxy.getFieldNames();
        }
        if (cls.equals(NetworkDetails.class)) {
            return NetworkDetailsRealmProxy.getFieldNames();
        }
        if (cls.equals(Sample.class)) {
            return SampleRealmProxy.getFieldNames();
        }
        if (cls.equals(CpuStatus.class)) {
            return CpuStatusRealmProxy.getFieldNames();
        }
        if (cls.equals(Feature.class)) {
            return FeatureRealmProxy.getFieldNames();
        }
        if (cls.equals(BatteryDetails.class)) {
            return BatteryDetailsRealmProxy.getFieldNames();
        }
        if (cls.equals(CellInfo.class)) {
            return CellInfoRealmProxy.getFieldNames();
        }
        if (cls.equals(NetworkStatistics.class)) {
            return NetworkStatisticsRealmProxy.getFieldNames();
        }
        if (cls.equals(AppSignature.class)) {
            return AppSignatureRealmProxy.getFieldNames();
        }
        if (cls.equals(BatteryUsage.class)) {
            return BatteryUsageRealmProxy.getFieldNames();
        }
        if (cls.equals(StorageDetails.class)) {
            return StorageDetailsRealmProxy.getFieldNames();
        }
        if (cls.equals(CallMonth.class)) {
            return CallMonthRealmProxy.getFieldNames();
        }
        if (cls.equals(Settings.class)) {
            return SettingsRealmProxy.getFieldNames();
        }
        if (cls.equals(CallInfo.class)) {
            return CallInfoRealmProxy.getFieldNames();
        }
        if (cls.equals(Message.class)) {
            return MessageRealmProxy.getFieldNames();
        }
        if (cls.equals(BatterySession.class)) {
            return BatterySessionRealmProxy.getFieldNames();
        }
        throw getMissingProxyClassException(cls);
    }

    public String getTableName(Class<? extends RealmModel> cls) {
        checkClass(cls);
        if (cls.equals(AppPermission.class)) {
            return AppPermissionRealmProxy.getTableName();
        }
        if (cls.equals(LocationProvider.class)) {
            return LocationProviderRealmProxy.getTableName();
        }
        if (cls.equals(ProcessInfo.class)) {
            return ProcessInfoRealmProxy.getTableName();
        }
        if (cls.equals(Device.class)) {
            return DeviceRealmProxy.getTableName();
        }
        if (cls.equals(NetworkDetails.class)) {
            return NetworkDetailsRealmProxy.getTableName();
        }
        if (cls.equals(Sample.class)) {
            return SampleRealmProxy.getTableName();
        }
        if (cls.equals(CpuStatus.class)) {
            return CpuStatusRealmProxy.getTableName();
        }
        if (cls.equals(Feature.class)) {
            return FeatureRealmProxy.getTableName();
        }
        if (cls.equals(BatteryDetails.class)) {
            return BatteryDetailsRealmProxy.getTableName();
        }
        if (cls.equals(CellInfo.class)) {
            return CellInfoRealmProxy.getTableName();
        }
        if (cls.equals(NetworkStatistics.class)) {
            return NetworkStatisticsRealmProxy.getTableName();
        }
        if (cls.equals(AppSignature.class)) {
            return AppSignatureRealmProxy.getTableName();
        }
        if (cls.equals(BatteryUsage.class)) {
            return BatteryUsageRealmProxy.getTableName();
        }
        if (cls.equals(StorageDetails.class)) {
            return StorageDetailsRealmProxy.getTableName();
        }
        if (cls.equals(CallMonth.class)) {
            return CallMonthRealmProxy.getTableName();
        }
        if (cls.equals(Settings.class)) {
            return SettingsRealmProxy.getTableName();
        }
        if (cls.equals(CallInfo.class)) {
            return CallInfoRealmProxy.getTableName();
        }
        if (cls.equals(Message.class)) {
            return MessageRealmProxy.getTableName();
        }
        if (cls.equals(BatterySession.class)) {
            return BatterySessionRealmProxy.getTableName();
        }
        throw getMissingProxyClassException(cls);
    }

    public <E extends RealmModel> E newInstance(Class<E> cls, Object obj, Row row, ColumnInfo columnInfo, boolean z, List<String> list) {
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        try {
            realmObjectContext.set((BaseRealm) obj, row, columnInfo, z, list);
            checkClass(cls);
            if (cls.equals(AppPermission.class)) {
                return (RealmModel) cls.cast(new AppPermissionRealmProxy());
            }
            if (cls.equals(LocationProvider.class)) {
                E e = (RealmModel) cls.cast(new LocationProviderRealmProxy());
                realmObjectContext.clear();
                return e;
            } else if (cls.equals(ProcessInfo.class)) {
                E e2 = (RealmModel) cls.cast(new ProcessInfoRealmProxy());
                realmObjectContext.clear();
                return e2;
            } else if (cls.equals(Device.class)) {
                E e3 = (RealmModel) cls.cast(new DeviceRealmProxy());
                realmObjectContext.clear();
                return e3;
            } else if (cls.equals(NetworkDetails.class)) {
                E e4 = (RealmModel) cls.cast(new NetworkDetailsRealmProxy());
                realmObjectContext.clear();
                return e4;
            } else if (cls.equals(Sample.class)) {
                E e5 = (RealmModel) cls.cast(new SampleRealmProxy());
                realmObjectContext.clear();
                return e5;
            } else if (cls.equals(CpuStatus.class)) {
                E e6 = (RealmModel) cls.cast(new CpuStatusRealmProxy());
                realmObjectContext.clear();
                return e6;
            } else if (cls.equals(Feature.class)) {
                E e7 = (RealmModel) cls.cast(new FeatureRealmProxy());
                realmObjectContext.clear();
                return e7;
            } else if (cls.equals(BatteryDetails.class)) {
                E e8 = (RealmModel) cls.cast(new BatteryDetailsRealmProxy());
                realmObjectContext.clear();
                return e8;
            } else if (cls.equals(CellInfo.class)) {
                E e9 = (RealmModel) cls.cast(new CellInfoRealmProxy());
                realmObjectContext.clear();
                return e9;
            } else if (cls.equals(NetworkStatistics.class)) {
                E e10 = (RealmModel) cls.cast(new NetworkStatisticsRealmProxy());
                realmObjectContext.clear();
                return e10;
            } else if (cls.equals(AppSignature.class)) {
                E e11 = (RealmModel) cls.cast(new AppSignatureRealmProxy());
                realmObjectContext.clear();
                return e11;
            } else if (cls.equals(BatteryUsage.class)) {
                E e12 = (RealmModel) cls.cast(new BatteryUsageRealmProxy());
                realmObjectContext.clear();
                return e12;
            } else if (cls.equals(StorageDetails.class)) {
                E e13 = (RealmModel) cls.cast(new StorageDetailsRealmProxy());
                realmObjectContext.clear();
                return e13;
            } else if (cls.equals(CallMonth.class)) {
                E e14 = (RealmModel) cls.cast(new CallMonthRealmProxy());
                realmObjectContext.clear();
                return e14;
            } else if (cls.equals(Settings.class)) {
                E e15 = (RealmModel) cls.cast(new SettingsRealmProxy());
                realmObjectContext.clear();
                return e15;
            } else if (cls.equals(CallInfo.class)) {
                E e16 = (RealmModel) cls.cast(new CallInfoRealmProxy());
                realmObjectContext.clear();
                return e16;
            } else if (cls.equals(Message.class)) {
                E e17 = (RealmModel) cls.cast(new MessageRealmProxy());
                realmObjectContext.clear();
                return e17;
            } else if (cls.equals(BatterySession.class)) {
                E e18 = (RealmModel) cls.cast(new BatterySessionRealmProxy());
                realmObjectContext.clear();
                return e18;
            } else {
                throw getMissingProxyClassException(cls);
            }
        } finally {
            realmObjectContext.clear();
        }
    }

    public Set<Class<? extends RealmModel>> getModelClasses() {
        return MODEL_CLASSES;
    }

    public <E extends RealmModel> E copyOrUpdate(Realm realm, E e, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        Class superclass = e instanceof RealmObjectProxy ? e.getClass().getSuperclass() : e.getClass();
        if (superclass.equals(AppPermission.class)) {
            return (RealmModel) superclass.cast(AppPermissionRealmProxy.copyOrUpdate(realm, (AppPermission) e, z, map));
        }
        if (superclass.equals(LocationProvider.class)) {
            return (RealmModel) superclass.cast(LocationProviderRealmProxy.copyOrUpdate(realm, (LocationProvider) e, z, map));
        }
        if (superclass.equals(ProcessInfo.class)) {
            return (RealmModel) superclass.cast(ProcessInfoRealmProxy.copyOrUpdate(realm, (ProcessInfo) e, z, map));
        }
        if (superclass.equals(Device.class)) {
            return (RealmModel) superclass.cast(DeviceRealmProxy.copyOrUpdate(realm, (Device) e, z, map));
        }
        if (superclass.equals(NetworkDetails.class)) {
            return (RealmModel) superclass.cast(NetworkDetailsRealmProxy.copyOrUpdate(realm, (NetworkDetails) e, z, map));
        }
        if (superclass.equals(Sample.class)) {
            return (RealmModel) superclass.cast(SampleRealmProxy.copyOrUpdate(realm, (Sample) e, z, map));
        }
        if (superclass.equals(CpuStatus.class)) {
            return (RealmModel) superclass.cast(CpuStatusRealmProxy.copyOrUpdate(realm, (CpuStatus) e, z, map));
        }
        if (superclass.equals(Feature.class)) {
            return (RealmModel) superclass.cast(FeatureRealmProxy.copyOrUpdate(realm, (Feature) e, z, map));
        }
        if (superclass.equals(BatteryDetails.class)) {
            return (RealmModel) superclass.cast(BatteryDetailsRealmProxy.copyOrUpdate(realm, (BatteryDetails) e, z, map));
        }
        if (superclass.equals(CellInfo.class)) {
            return (RealmModel) superclass.cast(CellInfoRealmProxy.copyOrUpdate(realm, (CellInfo) e, z, map));
        }
        if (superclass.equals(NetworkStatistics.class)) {
            return (RealmModel) superclass.cast(NetworkStatisticsRealmProxy.copyOrUpdate(realm, (NetworkStatistics) e, z, map));
        }
        if (superclass.equals(AppSignature.class)) {
            return (RealmModel) superclass.cast(AppSignatureRealmProxy.copyOrUpdate(realm, (AppSignature) e, z, map));
        }
        if (superclass.equals(BatteryUsage.class)) {
            return (RealmModel) superclass.cast(BatteryUsageRealmProxy.copyOrUpdate(realm, (BatteryUsage) e, z, map));
        }
        if (superclass.equals(StorageDetails.class)) {
            return (RealmModel) superclass.cast(StorageDetailsRealmProxy.copyOrUpdate(realm, (StorageDetails) e, z, map));
        }
        if (superclass.equals(CallMonth.class)) {
            return (RealmModel) superclass.cast(CallMonthRealmProxy.copyOrUpdate(realm, (CallMonth) e, z, map));
        }
        if (superclass.equals(Settings.class)) {
            return (RealmModel) superclass.cast(SettingsRealmProxy.copyOrUpdate(realm, (Settings) e, z, map));
        }
        if (superclass.equals(CallInfo.class)) {
            return (RealmModel) superclass.cast(CallInfoRealmProxy.copyOrUpdate(realm, (CallInfo) e, z, map));
        }
        if (superclass.equals(Message.class)) {
            return (RealmModel) superclass.cast(MessageRealmProxy.copyOrUpdate(realm, (Message) e, z, map));
        }
        if (superclass.equals(BatterySession.class)) {
            return (RealmModel) superclass.cast(BatterySessionRealmProxy.copyOrUpdate(realm, (BatterySession) e, z, map));
        }
        throw getMissingProxyClassException(superclass);
    }

    public void insert(Realm realm, RealmModel realmModel, Map<RealmModel, Long> map) {
        Class superclass = realmModel instanceof RealmObjectProxy ? realmModel.getClass().getSuperclass() : realmModel.getClass();
        if (superclass.equals(AppPermission.class)) {
            AppPermissionRealmProxy.insert(realm, (AppPermission) realmModel, map);
        } else if (superclass.equals(LocationProvider.class)) {
            LocationProviderRealmProxy.insert(realm, (LocationProvider) realmModel, map);
        } else if (superclass.equals(ProcessInfo.class)) {
            ProcessInfoRealmProxy.insert(realm, (ProcessInfo) realmModel, map);
        } else if (superclass.equals(Device.class)) {
            DeviceRealmProxy.insert(realm, (Device) realmModel, map);
        } else if (superclass.equals(NetworkDetails.class)) {
            NetworkDetailsRealmProxy.insert(realm, (NetworkDetails) realmModel, map);
        } else if (superclass.equals(Sample.class)) {
            SampleRealmProxy.insert(realm, (Sample) realmModel, map);
        } else if (superclass.equals(CpuStatus.class)) {
            CpuStatusRealmProxy.insert(realm, (CpuStatus) realmModel, map);
        } else if (superclass.equals(Feature.class)) {
            FeatureRealmProxy.insert(realm, (Feature) realmModel, map);
        } else if (superclass.equals(BatteryDetails.class)) {
            BatteryDetailsRealmProxy.insert(realm, (BatteryDetails) realmModel, map);
        } else if (superclass.equals(CellInfo.class)) {
            CellInfoRealmProxy.insert(realm, (CellInfo) realmModel, map);
        } else if (superclass.equals(NetworkStatistics.class)) {
            NetworkStatisticsRealmProxy.insert(realm, (NetworkStatistics) realmModel, map);
        } else if (superclass.equals(AppSignature.class)) {
            AppSignatureRealmProxy.insert(realm, (AppSignature) realmModel, map);
        } else if (superclass.equals(BatteryUsage.class)) {
            BatteryUsageRealmProxy.insert(realm, (BatteryUsage) realmModel, map);
        } else if (superclass.equals(StorageDetails.class)) {
            StorageDetailsRealmProxy.insert(realm, (StorageDetails) realmModel, map);
        } else if (superclass.equals(CallMonth.class)) {
            CallMonthRealmProxy.insert(realm, (CallMonth) realmModel, map);
        } else if (superclass.equals(Settings.class)) {
            SettingsRealmProxy.insert(realm, (Settings) realmModel, map);
        } else if (superclass.equals(CallInfo.class)) {
            CallInfoRealmProxy.insert(realm, (CallInfo) realmModel, map);
        } else if (superclass.equals(Message.class)) {
            MessageRealmProxy.insert(realm, (Message) realmModel, map);
        } else if (superclass.equals(BatterySession.class)) {
            BatterySessionRealmProxy.insert(realm, (BatterySession) realmModel, map);
        } else {
            throw getMissingProxyClassException(superclass);
        }
    }

    public void insert(Realm realm, Collection<? extends RealmModel> collection) {
        Iterator it = collection.iterator();
        HashMap hashMap = new HashMap(collection.size());
        if (it.hasNext()) {
            RealmModel realmModel = (RealmModel) it.next();
            Class superclass = realmModel instanceof RealmObjectProxy ? realmModel.getClass().getSuperclass() : realmModel.getClass();
            if (superclass.equals(AppPermission.class)) {
                AppPermissionRealmProxy.insert(realm, (AppPermission) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(LocationProvider.class)) {
                LocationProviderRealmProxy.insert(realm, (LocationProvider) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(ProcessInfo.class)) {
                ProcessInfoRealmProxy.insert(realm, (ProcessInfo) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Device.class)) {
                DeviceRealmProxy.insert(realm, (Device) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(NetworkDetails.class)) {
                NetworkDetailsRealmProxy.insert(realm, (NetworkDetails) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Sample.class)) {
                SampleRealmProxy.insert(realm, (Sample) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CpuStatus.class)) {
                CpuStatusRealmProxy.insert(realm, (CpuStatus) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Feature.class)) {
                FeatureRealmProxy.insert(realm, (Feature) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatteryDetails.class)) {
                BatteryDetailsRealmProxy.insert(realm, (BatteryDetails) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CellInfo.class)) {
                CellInfoRealmProxy.insert(realm, (CellInfo) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(NetworkStatistics.class)) {
                NetworkStatisticsRealmProxy.insert(realm, (NetworkStatistics) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(AppSignature.class)) {
                AppSignatureRealmProxy.insert(realm, (AppSignature) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatteryUsage.class)) {
                BatteryUsageRealmProxy.insert(realm, (BatteryUsage) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(StorageDetails.class)) {
                StorageDetailsRealmProxy.insert(realm, (StorageDetails) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CallMonth.class)) {
                CallMonthRealmProxy.insert(realm, (CallMonth) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Settings.class)) {
                SettingsRealmProxy.insert(realm, (Settings) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CallInfo.class)) {
                CallInfoRealmProxy.insert(realm, (CallInfo) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Message.class)) {
                MessageRealmProxy.insert(realm, (Message) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatterySession.class)) {
                BatterySessionRealmProxy.insert(realm, (BatterySession) realmModel, (Map<RealmModel, Long>) hashMap);
            } else {
                throw getMissingProxyClassException(superclass);
            }
            if (!it.hasNext()) {
                return;
            }
            if (superclass.equals(AppPermission.class)) {
                AppPermissionRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(LocationProvider.class)) {
                LocationProviderRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(ProcessInfo.class)) {
                ProcessInfoRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Device.class)) {
                DeviceRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(NetworkDetails.class)) {
                NetworkDetailsRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Sample.class)) {
                SampleRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CpuStatus.class)) {
                CpuStatusRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Feature.class)) {
                FeatureRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatteryDetails.class)) {
                BatteryDetailsRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CellInfo.class)) {
                CellInfoRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(NetworkStatistics.class)) {
                NetworkStatisticsRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(AppSignature.class)) {
                AppSignatureRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatteryUsage.class)) {
                BatteryUsageRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(StorageDetails.class)) {
                StorageDetailsRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CallMonth.class)) {
                CallMonthRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Settings.class)) {
                SettingsRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CallInfo.class)) {
                CallInfoRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Message.class)) {
                MessageRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatterySession.class)) {
                BatterySessionRealmProxy.insert(realm, it, (Map<RealmModel, Long>) hashMap);
            } else {
                throw getMissingProxyClassException(superclass);
            }
        }
    }

    public void insertOrUpdate(Realm realm, RealmModel realmModel, Map<RealmModel, Long> map) {
        Class superclass = realmModel instanceof RealmObjectProxy ? realmModel.getClass().getSuperclass() : realmModel.getClass();
        if (superclass.equals(AppPermission.class)) {
            AppPermissionRealmProxy.insertOrUpdate(realm, (AppPermission) realmModel, map);
        } else if (superclass.equals(LocationProvider.class)) {
            LocationProviderRealmProxy.insertOrUpdate(realm, (LocationProvider) realmModel, map);
        } else if (superclass.equals(ProcessInfo.class)) {
            ProcessInfoRealmProxy.insertOrUpdate(realm, (ProcessInfo) realmModel, map);
        } else if (superclass.equals(Device.class)) {
            DeviceRealmProxy.insertOrUpdate(realm, (Device) realmModel, map);
        } else if (superclass.equals(NetworkDetails.class)) {
            NetworkDetailsRealmProxy.insertOrUpdate(realm, (NetworkDetails) realmModel, map);
        } else if (superclass.equals(Sample.class)) {
            SampleRealmProxy.insertOrUpdate(realm, (Sample) realmModel, map);
        } else if (superclass.equals(CpuStatus.class)) {
            CpuStatusRealmProxy.insertOrUpdate(realm, (CpuStatus) realmModel, map);
        } else if (superclass.equals(Feature.class)) {
            FeatureRealmProxy.insertOrUpdate(realm, (Feature) realmModel, map);
        } else if (superclass.equals(BatteryDetails.class)) {
            BatteryDetailsRealmProxy.insertOrUpdate(realm, (BatteryDetails) realmModel, map);
        } else if (superclass.equals(CellInfo.class)) {
            CellInfoRealmProxy.insertOrUpdate(realm, (CellInfo) realmModel, map);
        } else if (superclass.equals(NetworkStatistics.class)) {
            NetworkStatisticsRealmProxy.insertOrUpdate(realm, (NetworkStatistics) realmModel, map);
        } else if (superclass.equals(AppSignature.class)) {
            AppSignatureRealmProxy.insertOrUpdate(realm, (AppSignature) realmModel, map);
        } else if (superclass.equals(BatteryUsage.class)) {
            BatteryUsageRealmProxy.insertOrUpdate(realm, (BatteryUsage) realmModel, map);
        } else if (superclass.equals(StorageDetails.class)) {
            StorageDetailsRealmProxy.insertOrUpdate(realm, (StorageDetails) realmModel, map);
        } else if (superclass.equals(CallMonth.class)) {
            CallMonthRealmProxy.insertOrUpdate(realm, (CallMonth) realmModel, map);
        } else if (superclass.equals(Settings.class)) {
            SettingsRealmProxy.insertOrUpdate(realm, (Settings) realmModel, map);
        } else if (superclass.equals(CallInfo.class)) {
            CallInfoRealmProxy.insertOrUpdate(realm, (CallInfo) realmModel, map);
        } else if (superclass.equals(Message.class)) {
            MessageRealmProxy.insertOrUpdate(realm, (Message) realmModel, map);
        } else if (superclass.equals(BatterySession.class)) {
            BatterySessionRealmProxy.insertOrUpdate(realm, (BatterySession) realmModel, map);
        } else {
            throw getMissingProxyClassException(superclass);
        }
    }

    public void insertOrUpdate(Realm realm, Collection<? extends RealmModel> collection) {
        Iterator it = collection.iterator();
        HashMap hashMap = new HashMap(collection.size());
        if (it.hasNext()) {
            RealmModel realmModel = (RealmModel) it.next();
            Class superclass = realmModel instanceof RealmObjectProxy ? realmModel.getClass().getSuperclass() : realmModel.getClass();
            if (superclass.equals(AppPermission.class)) {
                AppPermissionRealmProxy.insertOrUpdate(realm, (AppPermission) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(LocationProvider.class)) {
                LocationProviderRealmProxy.insertOrUpdate(realm, (LocationProvider) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(ProcessInfo.class)) {
                ProcessInfoRealmProxy.insertOrUpdate(realm, (ProcessInfo) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Device.class)) {
                DeviceRealmProxy.insertOrUpdate(realm, (Device) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(NetworkDetails.class)) {
                NetworkDetailsRealmProxy.insertOrUpdate(realm, (NetworkDetails) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Sample.class)) {
                SampleRealmProxy.insertOrUpdate(realm, (Sample) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CpuStatus.class)) {
                CpuStatusRealmProxy.insertOrUpdate(realm, (CpuStatus) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Feature.class)) {
                FeatureRealmProxy.insertOrUpdate(realm, (Feature) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatteryDetails.class)) {
                BatteryDetailsRealmProxy.insertOrUpdate(realm, (BatteryDetails) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CellInfo.class)) {
                CellInfoRealmProxy.insertOrUpdate(realm, (CellInfo) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(NetworkStatistics.class)) {
                NetworkStatisticsRealmProxy.insertOrUpdate(realm, (NetworkStatistics) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(AppSignature.class)) {
                AppSignatureRealmProxy.insertOrUpdate(realm, (AppSignature) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatteryUsage.class)) {
                BatteryUsageRealmProxy.insertOrUpdate(realm, (BatteryUsage) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(StorageDetails.class)) {
                StorageDetailsRealmProxy.insertOrUpdate(realm, (StorageDetails) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CallMonth.class)) {
                CallMonthRealmProxy.insertOrUpdate(realm, (CallMonth) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Settings.class)) {
                SettingsRealmProxy.insertOrUpdate(realm, (Settings) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CallInfo.class)) {
                CallInfoRealmProxy.insertOrUpdate(realm, (CallInfo) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Message.class)) {
                MessageRealmProxy.insertOrUpdate(realm, (Message) realmModel, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatterySession.class)) {
                BatterySessionRealmProxy.insertOrUpdate(realm, (BatterySession) realmModel, (Map<RealmModel, Long>) hashMap);
            } else {
                throw getMissingProxyClassException(superclass);
            }
            if (!it.hasNext()) {
                return;
            }
            if (superclass.equals(AppPermission.class)) {
                AppPermissionRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(LocationProvider.class)) {
                LocationProviderRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(ProcessInfo.class)) {
                ProcessInfoRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Device.class)) {
                DeviceRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(NetworkDetails.class)) {
                NetworkDetailsRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Sample.class)) {
                SampleRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CpuStatus.class)) {
                CpuStatusRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Feature.class)) {
                FeatureRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatteryDetails.class)) {
                BatteryDetailsRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CellInfo.class)) {
                CellInfoRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(NetworkStatistics.class)) {
                NetworkStatisticsRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(AppSignature.class)) {
                AppSignatureRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatteryUsage.class)) {
                BatteryUsageRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(StorageDetails.class)) {
                StorageDetailsRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CallMonth.class)) {
                CallMonthRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Settings.class)) {
                SettingsRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(CallInfo.class)) {
                CallInfoRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(Message.class)) {
                MessageRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else if (superclass.equals(BatterySession.class)) {
                BatterySessionRealmProxy.insertOrUpdate(realm, it, (Map<RealmModel, Long>) hashMap);
            } else {
                throw getMissingProxyClassException(superclass);
            }
        }
    }

    public <E extends RealmModel> E createOrUpdateUsingJsonObject(Class<E> cls, Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        checkClass(cls);
        if (cls.equals(AppPermission.class)) {
            return (RealmModel) cls.cast(AppPermissionRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(LocationProvider.class)) {
            return (RealmModel) cls.cast(LocationProviderRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(ProcessInfo.class)) {
            return (RealmModel) cls.cast(ProcessInfoRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(Device.class)) {
            return (RealmModel) cls.cast(DeviceRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(NetworkDetails.class)) {
            return (RealmModel) cls.cast(NetworkDetailsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(Sample.class)) {
            return (RealmModel) cls.cast(SampleRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(CpuStatus.class)) {
            return (RealmModel) cls.cast(CpuStatusRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(Feature.class)) {
            return (RealmModel) cls.cast(FeatureRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(BatteryDetails.class)) {
            return (RealmModel) cls.cast(BatteryDetailsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(CellInfo.class)) {
            return (RealmModel) cls.cast(CellInfoRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(NetworkStatistics.class)) {
            return (RealmModel) cls.cast(NetworkStatisticsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(AppSignature.class)) {
            return (RealmModel) cls.cast(AppSignatureRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(BatteryUsage.class)) {
            return (RealmModel) cls.cast(BatteryUsageRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(StorageDetails.class)) {
            return (RealmModel) cls.cast(StorageDetailsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(CallMonth.class)) {
            return (RealmModel) cls.cast(CallMonthRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(Settings.class)) {
            return (RealmModel) cls.cast(SettingsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(CallInfo.class)) {
            return (RealmModel) cls.cast(CallInfoRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(Message.class)) {
            return (RealmModel) cls.cast(MessageRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        if (cls.equals(BatterySession.class)) {
            return (RealmModel) cls.cast(BatterySessionRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject, z));
        }
        throw getMissingProxyClassException(cls);
    }

    public <E extends RealmModel> E createUsingJsonStream(Class<E> cls, Realm realm, JsonReader jsonReader) throws IOException {
        checkClass(cls);
        if (cls.equals(AppPermission.class)) {
            return (RealmModel) cls.cast(AppPermissionRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(LocationProvider.class)) {
            return (RealmModel) cls.cast(LocationProviderRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(ProcessInfo.class)) {
            return (RealmModel) cls.cast(ProcessInfoRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(Device.class)) {
            return (RealmModel) cls.cast(DeviceRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(NetworkDetails.class)) {
            return (RealmModel) cls.cast(NetworkDetailsRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(Sample.class)) {
            return (RealmModel) cls.cast(SampleRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(CpuStatus.class)) {
            return (RealmModel) cls.cast(CpuStatusRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(Feature.class)) {
            return (RealmModel) cls.cast(FeatureRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(BatteryDetails.class)) {
            return (RealmModel) cls.cast(BatteryDetailsRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(CellInfo.class)) {
            return (RealmModel) cls.cast(CellInfoRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(NetworkStatistics.class)) {
            return (RealmModel) cls.cast(NetworkStatisticsRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(AppSignature.class)) {
            return (RealmModel) cls.cast(AppSignatureRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(BatteryUsage.class)) {
            return (RealmModel) cls.cast(BatteryUsageRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(StorageDetails.class)) {
            return (RealmModel) cls.cast(StorageDetailsRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(CallMonth.class)) {
            return (RealmModel) cls.cast(CallMonthRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(Settings.class)) {
            return (RealmModel) cls.cast(SettingsRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(CallInfo.class)) {
            return (RealmModel) cls.cast(CallInfoRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(Message.class)) {
            return (RealmModel) cls.cast(MessageRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        if (cls.equals(BatterySession.class)) {
            return (RealmModel) cls.cast(BatterySessionRealmProxy.createUsingJsonStream(realm, jsonReader));
        }
        throw getMissingProxyClassException(cls);
    }

    public <E extends RealmModel> E createDetachedCopy(E e, int i, Map<RealmModel, CacheData<RealmModel>> map) {
        Class superclass = e.getClass().getSuperclass();
        if (superclass.equals(AppPermission.class)) {
            return (RealmModel) superclass.cast(AppPermissionRealmProxy.createDetachedCopy((AppPermission) e, 0, i, map));
        }
        if (superclass.equals(LocationProvider.class)) {
            return (RealmModel) superclass.cast(LocationProviderRealmProxy.createDetachedCopy((LocationProvider) e, 0, i, map));
        }
        if (superclass.equals(ProcessInfo.class)) {
            return (RealmModel) superclass.cast(ProcessInfoRealmProxy.createDetachedCopy((ProcessInfo) e, 0, i, map));
        }
        if (superclass.equals(Device.class)) {
            return (RealmModel) superclass.cast(DeviceRealmProxy.createDetachedCopy((Device) e, 0, i, map));
        }
        if (superclass.equals(NetworkDetails.class)) {
            return (RealmModel) superclass.cast(NetworkDetailsRealmProxy.createDetachedCopy((NetworkDetails) e, 0, i, map));
        }
        if (superclass.equals(Sample.class)) {
            return (RealmModel) superclass.cast(SampleRealmProxy.createDetachedCopy((Sample) e, 0, i, map));
        }
        if (superclass.equals(CpuStatus.class)) {
            return (RealmModel) superclass.cast(CpuStatusRealmProxy.createDetachedCopy((CpuStatus) e, 0, i, map));
        }
        if (superclass.equals(Feature.class)) {
            return (RealmModel) superclass.cast(FeatureRealmProxy.createDetachedCopy((Feature) e, 0, i, map));
        }
        if (superclass.equals(BatteryDetails.class)) {
            return (RealmModel) superclass.cast(BatteryDetailsRealmProxy.createDetachedCopy((BatteryDetails) e, 0, i, map));
        }
        if (superclass.equals(CellInfo.class)) {
            return (RealmModel) superclass.cast(CellInfoRealmProxy.createDetachedCopy((CellInfo) e, 0, i, map));
        }
        if (superclass.equals(NetworkStatistics.class)) {
            return (RealmModel) superclass.cast(NetworkStatisticsRealmProxy.createDetachedCopy((NetworkStatistics) e, 0, i, map));
        }
        if (superclass.equals(AppSignature.class)) {
            return (RealmModel) superclass.cast(AppSignatureRealmProxy.createDetachedCopy((AppSignature) e, 0, i, map));
        }
        if (superclass.equals(BatteryUsage.class)) {
            return (RealmModel) superclass.cast(BatteryUsageRealmProxy.createDetachedCopy((BatteryUsage) e, 0, i, map));
        }
        if (superclass.equals(StorageDetails.class)) {
            return (RealmModel) superclass.cast(StorageDetailsRealmProxy.createDetachedCopy((StorageDetails) e, 0, i, map));
        }
        if (superclass.equals(CallMonth.class)) {
            return (RealmModel) superclass.cast(CallMonthRealmProxy.createDetachedCopy((CallMonth) e, 0, i, map));
        }
        if (superclass.equals(Settings.class)) {
            return (RealmModel) superclass.cast(SettingsRealmProxy.createDetachedCopy((Settings) e, 0, i, map));
        }
        if (superclass.equals(CallInfo.class)) {
            return (RealmModel) superclass.cast(CallInfoRealmProxy.createDetachedCopy((CallInfo) e, 0, i, map));
        }
        if (superclass.equals(Message.class)) {
            return (RealmModel) superclass.cast(MessageRealmProxy.createDetachedCopy((Message) e, 0, i, map));
        }
        if (superclass.equals(BatterySession.class)) {
            return (RealmModel) superclass.cast(BatterySessionRealmProxy.createDetachedCopy((BatterySession) e, 0, i, map));
        }
        throw getMissingProxyClassException(superclass);
    }
}
