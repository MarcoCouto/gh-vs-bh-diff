package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.mansoon.BatteryDouble.models.data.CallMonth;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class CallMonthRealmProxy extends CallMonth implements RealmObjectProxy, CallMonthRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private CallMonthColumnInfo columnInfo;
    private ProxyState<CallMonth> proxyState;

    static final class CallMonthColumnInfo extends ColumnInfo {
        long totalCallInDurIndex;
        long totalCallInNumIndex;
        long totalCallOutDurIndex;
        long totalCallOutNumIndex;
        long totalMissedCallNumIndex;

        CallMonthColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(5);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("CallMonth");
            this.totalCallInNumIndex = addColumnDetails("totalCallInNum", objectSchemaInfo);
            this.totalCallOutNumIndex = addColumnDetails("totalCallOutNum", objectSchemaInfo);
            this.totalMissedCallNumIndex = addColumnDetails("totalMissedCallNum", objectSchemaInfo);
            this.totalCallInDurIndex = addColumnDetails("totalCallInDur", objectSchemaInfo);
            this.totalCallOutDurIndex = addColumnDetails("totalCallOutDur", objectSchemaInfo);
        }

        CallMonthColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new CallMonthColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            CallMonthColumnInfo callMonthColumnInfo = (CallMonthColumnInfo) columnInfo;
            CallMonthColumnInfo callMonthColumnInfo2 = (CallMonthColumnInfo) columnInfo2;
            callMonthColumnInfo2.totalCallInNumIndex = callMonthColumnInfo.totalCallInNumIndex;
            callMonthColumnInfo2.totalCallOutNumIndex = callMonthColumnInfo.totalCallOutNumIndex;
            callMonthColumnInfo2.totalMissedCallNumIndex = callMonthColumnInfo.totalMissedCallNumIndex;
            callMonthColumnInfo2.totalCallInDurIndex = callMonthColumnInfo.totalCallInDurIndex;
            callMonthColumnInfo2.totalCallOutDurIndex = callMonthColumnInfo.totalCallOutDurIndex;
        }
    }

    public static String getTableName() {
        return "class_CallMonth";
    }

    static {
        ArrayList arrayList = new ArrayList(5);
        arrayList.add("totalCallInNum");
        arrayList.add("totalCallOutNum");
        arrayList.add("totalMissedCallNum");
        arrayList.add("totalCallInDur");
        arrayList.add("totalCallOutDur");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    CallMonthRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (CallMonthColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public int realmGet$totalCallInNum() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.totalCallInNumIndex);
    }

    public void realmSet$totalCallInNum(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalCallInNumIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalCallInNumIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$totalCallOutNum() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.totalCallOutNumIndex);
    }

    public void realmSet$totalCallOutNum(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalCallOutNumIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalCallOutNumIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$totalMissedCallNum() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.totalMissedCallNumIndex);
    }

    public void realmSet$totalMissedCallNum(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalMissedCallNumIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalMissedCallNumIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public long realmGet$totalCallInDur() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getLong(this.columnInfo.totalCallInDurIndex);
    }

    public void realmSet$totalCallInDur(long j) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalCallInDurIndex, j);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalCallInDurIndex, row$realm.getIndex(), j, true);
        }
    }

    public long realmGet$totalCallOutDur() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getLong(this.columnInfo.totalCallOutDurIndex);
    }

    public void realmSet$totalCallOutDur(long j) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalCallOutDurIndex, j);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalCallOutDurIndex, row$realm.getIndex(), j, true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("CallMonth", 5, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("totalCallInNum", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("totalCallOutNum", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("totalMissedCallNum", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("totalCallInDur", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("totalCallOutDur", RealmFieldType.INTEGER, false, false, true);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static CallMonthColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new CallMonthColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static CallMonth createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        CallMonth callMonth = (CallMonth) realm.createObjectInternal(CallMonth.class, true, Collections.emptyList());
        CallMonthRealmProxyInterface callMonthRealmProxyInterface = callMonth;
        if (jSONObject.has("totalCallInNum")) {
            if (jSONObject.isNull("totalCallInNum")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalCallInNum' to null.");
            }
            callMonthRealmProxyInterface.realmSet$totalCallInNum(jSONObject.getInt("totalCallInNum"));
        }
        if (jSONObject.has("totalCallOutNum")) {
            if (jSONObject.isNull("totalCallOutNum")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalCallOutNum' to null.");
            }
            callMonthRealmProxyInterface.realmSet$totalCallOutNum(jSONObject.getInt("totalCallOutNum"));
        }
        if (jSONObject.has("totalMissedCallNum")) {
            if (jSONObject.isNull("totalMissedCallNum")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalMissedCallNum' to null.");
            }
            callMonthRealmProxyInterface.realmSet$totalMissedCallNum(jSONObject.getInt("totalMissedCallNum"));
        }
        if (jSONObject.has("totalCallInDur")) {
            if (jSONObject.isNull("totalCallInDur")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalCallInDur' to null.");
            }
            callMonthRealmProxyInterface.realmSet$totalCallInDur(jSONObject.getLong("totalCallInDur"));
        }
        if (jSONObject.has("totalCallOutDur")) {
            if (jSONObject.isNull("totalCallOutDur")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalCallOutDur' to null.");
            }
            callMonthRealmProxyInterface.realmSet$totalCallOutDur(jSONObject.getLong("totalCallOutDur"));
        }
        return callMonth;
    }

    @TargetApi(11)
    public static CallMonth createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        CallMonth callMonth = new CallMonth();
        CallMonthRealmProxyInterface callMonthRealmProxyInterface = callMonth;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("totalCallInNum")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    callMonthRealmProxyInterface.realmSet$totalCallInNum(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'totalCallInNum' to null.");
                }
            } else if (nextName.equals("totalCallOutNum")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    callMonthRealmProxyInterface.realmSet$totalCallOutNum(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'totalCallOutNum' to null.");
                }
            } else if (nextName.equals("totalMissedCallNum")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    callMonthRealmProxyInterface.realmSet$totalMissedCallNum(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'totalMissedCallNum' to null.");
                }
            } else if (nextName.equals("totalCallInDur")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    callMonthRealmProxyInterface.realmSet$totalCallInDur(jsonReader.nextLong());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'totalCallInDur' to null.");
                }
            } else if (!nextName.equals("totalCallOutDur")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                callMonthRealmProxyInterface.realmSet$totalCallOutDur(jsonReader.nextLong());
            } else {
                jsonReader.skipValue();
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalCallOutDur' to null.");
            }
        }
        jsonReader.endObject();
        return (CallMonth) realm.copyToRealm(callMonth);
    }

    public static CallMonth copyOrUpdate(Realm realm, CallMonth callMonth, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (callMonth instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callMonth;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return callMonth;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(callMonth);
        if (realmObjectProxy2 != null) {
            return (CallMonth) realmObjectProxy2;
        }
        return copy(realm, callMonth, z, map);
    }

    public static CallMonth copy(Realm realm, CallMonth callMonth, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(callMonth);
        if (realmObjectProxy != null) {
            return (CallMonth) realmObjectProxy;
        }
        CallMonth callMonth2 = (CallMonth) realm.createObjectInternal(CallMonth.class, false, Collections.emptyList());
        map.put(callMonth, (RealmObjectProxy) callMonth2);
        CallMonthRealmProxyInterface callMonthRealmProxyInterface = callMonth;
        CallMonthRealmProxyInterface callMonthRealmProxyInterface2 = callMonth2;
        callMonthRealmProxyInterface2.realmSet$totalCallInNum(callMonthRealmProxyInterface.realmGet$totalCallInNum());
        callMonthRealmProxyInterface2.realmSet$totalCallOutNum(callMonthRealmProxyInterface.realmGet$totalCallOutNum());
        callMonthRealmProxyInterface2.realmSet$totalMissedCallNum(callMonthRealmProxyInterface.realmGet$totalMissedCallNum());
        callMonthRealmProxyInterface2.realmSet$totalCallInDur(callMonthRealmProxyInterface.realmGet$totalCallInDur());
        callMonthRealmProxyInterface2.realmSet$totalCallOutDur(callMonthRealmProxyInterface.realmGet$totalCallOutDur());
        return callMonth2;
    }

    public static long insert(Realm realm, CallMonth callMonth, Map<RealmModel, Long> map) {
        CallMonth callMonth2 = callMonth;
        if (callMonth2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callMonth2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(CallMonth.class);
        long nativePtr = table.getNativePtr();
        CallMonthColumnInfo callMonthColumnInfo = (CallMonthColumnInfo) realm.getSchema().getColumnInfo(CallMonth.class);
        long createRow = OsObject.createRow(table);
        map.put(callMonth2, Long.valueOf(createRow));
        CallMonthRealmProxyInterface callMonthRealmProxyInterface = callMonth2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetLong(j, callMonthColumnInfo.totalCallInNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalCallInNum(), false);
        Table.nativeSetLong(j, callMonthColumnInfo.totalCallOutNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalCallOutNum(), false);
        Table.nativeSetLong(j, callMonthColumnInfo.totalMissedCallNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalMissedCallNum(), false);
        Table.nativeSetLong(j, callMonthColumnInfo.totalCallInDurIndex, j2, callMonthRealmProxyInterface.realmGet$totalCallInDur(), false);
        Table.nativeSetLong(j, callMonthColumnInfo.totalCallOutDurIndex, j2, callMonthRealmProxyInterface.realmGet$totalCallOutDur(), false);
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(CallMonth.class);
        long nativePtr = table.getNativePtr();
        CallMonthColumnInfo callMonthColumnInfo = (CallMonthColumnInfo) realm.getSchema().getColumnInfo(CallMonth.class);
        while (it.hasNext()) {
            CallMonth callMonth = (CallMonth) it.next();
            if (!map2.containsKey(callMonth)) {
                if (callMonth instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callMonth;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(callMonth, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(callMonth, Long.valueOf(createRow));
                CallMonthRealmProxyInterface callMonthRealmProxyInterface = callMonth;
                long j = createRow;
                Table.nativeSetLong(nativePtr, callMonthColumnInfo.totalCallInNumIndex, createRow, (long) callMonthRealmProxyInterface.realmGet$totalCallInNum(), false);
                long j2 = j;
                Table.nativeSetLong(nativePtr, callMonthColumnInfo.totalCallOutNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalCallOutNum(), false);
                long j3 = nativePtr;
                Table.nativeSetLong(j3, callMonthColumnInfo.totalMissedCallNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalMissedCallNum(), false);
                Table.nativeSetLong(j3, callMonthColumnInfo.totalCallInDurIndex, j2, callMonthRealmProxyInterface.realmGet$totalCallInDur(), false);
                Table.nativeSetLong(j3, callMonthColumnInfo.totalCallOutDurIndex, j2, callMonthRealmProxyInterface.realmGet$totalCallOutDur(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, CallMonth callMonth, Map<RealmModel, Long> map) {
        CallMonth callMonth2 = callMonth;
        if (callMonth2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callMonth2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(CallMonth.class);
        long nativePtr = table.getNativePtr();
        CallMonthColumnInfo callMonthColumnInfo = (CallMonthColumnInfo) realm.getSchema().getColumnInfo(CallMonth.class);
        long createRow = OsObject.createRow(table);
        map.put(callMonth2, Long.valueOf(createRow));
        CallMonthRealmProxyInterface callMonthRealmProxyInterface = callMonth2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetLong(j, callMonthColumnInfo.totalCallInNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalCallInNum(), false);
        Table.nativeSetLong(j, callMonthColumnInfo.totalCallOutNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalCallOutNum(), false);
        Table.nativeSetLong(j, callMonthColumnInfo.totalMissedCallNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalMissedCallNum(), false);
        Table.nativeSetLong(j, callMonthColumnInfo.totalCallInDurIndex, j2, callMonthRealmProxyInterface.realmGet$totalCallInDur(), false);
        Table.nativeSetLong(j, callMonthColumnInfo.totalCallOutDurIndex, j2, callMonthRealmProxyInterface.realmGet$totalCallOutDur(), false);
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(CallMonth.class);
        long nativePtr = table.getNativePtr();
        CallMonthColumnInfo callMonthColumnInfo = (CallMonthColumnInfo) realm.getSchema().getColumnInfo(CallMonth.class);
        while (it.hasNext()) {
            CallMonth callMonth = (CallMonth) it.next();
            if (!map2.containsKey(callMonth)) {
                if (callMonth instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callMonth;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(callMonth, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(callMonth, Long.valueOf(createRow));
                CallMonthRealmProxyInterface callMonthRealmProxyInterface = callMonth;
                long j = createRow;
                Table.nativeSetLong(nativePtr, callMonthColumnInfo.totalCallInNumIndex, createRow, (long) callMonthRealmProxyInterface.realmGet$totalCallInNum(), false);
                long j2 = j;
                Table.nativeSetLong(nativePtr, callMonthColumnInfo.totalCallOutNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalCallOutNum(), false);
                long j3 = nativePtr;
                Table.nativeSetLong(j3, callMonthColumnInfo.totalMissedCallNumIndex, j2, (long) callMonthRealmProxyInterface.realmGet$totalMissedCallNum(), false);
                Table.nativeSetLong(j3, callMonthColumnInfo.totalCallInDurIndex, j2, callMonthRealmProxyInterface.realmGet$totalCallInDur(), false);
                Table.nativeSetLong(j3, callMonthColumnInfo.totalCallOutDurIndex, j2, callMonthRealmProxyInterface.realmGet$totalCallOutDur(), false);
            }
        }
    }

    public static CallMonth createDetachedCopy(CallMonth callMonth, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        CallMonth callMonth2;
        if (i > i2 || callMonth == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(callMonth);
        if (cacheData == null) {
            callMonth2 = new CallMonth();
            map.put(callMonth, new CacheData(i, callMonth2));
        } else if (i >= cacheData.minDepth) {
            return (CallMonth) cacheData.object;
        } else {
            CallMonth callMonth3 = (CallMonth) cacheData.object;
            cacheData.minDepth = i;
            callMonth2 = callMonth3;
        }
        CallMonthRealmProxyInterface callMonthRealmProxyInterface = callMonth2;
        CallMonthRealmProxyInterface callMonthRealmProxyInterface2 = callMonth;
        callMonthRealmProxyInterface.realmSet$totalCallInNum(callMonthRealmProxyInterface2.realmGet$totalCallInNum());
        callMonthRealmProxyInterface.realmSet$totalCallOutNum(callMonthRealmProxyInterface2.realmGet$totalCallOutNum());
        callMonthRealmProxyInterface.realmSet$totalMissedCallNum(callMonthRealmProxyInterface2.realmGet$totalMissedCallNum());
        callMonthRealmProxyInterface.realmSet$totalCallInDur(callMonthRealmProxyInterface2.realmGet$totalCallInDur());
        callMonthRealmProxyInterface.realmSet$totalCallOutDur(callMonthRealmProxyInterface2.realmGet$totalCallOutDur());
        return callMonth2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("CallMonth = proxy[");
        sb.append("{totalCallInNum:");
        sb.append(realmGet$totalCallInNum());
        sb.append("}");
        sb.append(",");
        sb.append("{totalCallOutNum:");
        sb.append(realmGet$totalCallOutNum());
        sb.append("}");
        sb.append(",");
        sb.append("{totalMissedCallNum:");
        sb.append(realmGet$totalMissedCallNum());
        sb.append("}");
        sb.append(",");
        sb.append("{totalCallInDur:");
        sb.append(realmGet$totalCallInDur());
        sb.append("}");
        sb.append(",");
        sb.append("{totalCallOutDur:");
        sb.append(realmGet$totalCallOutDur());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CallMonthRealmProxy callMonthRealmProxy = (CallMonthRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = callMonthRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = callMonthRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == callMonthRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
