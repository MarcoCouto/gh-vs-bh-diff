package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;

public abstract class zzdo extends zzr implements zzdn {
    public zzdo() {
        super("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    public static zzdn asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
        if (queryLocalInterface instanceof zzdn) {
            return (zzdn) queryLocalInterface;
        }
        return new zzdp(iBinder);
    }

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r1v6, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v7, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v8 */
    /* JADX WARNING: type inference failed for: r5v2 */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v11, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v14, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v15, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v16, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v19, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v20, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v26, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v29, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v30, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v34, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v37, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v38, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v39, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v42, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v43, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v44, types: [com.google.android.gms.internal.measurement.zzdw] */
    /* JADX WARNING: type inference failed for: r1v47, types: [com.google.android.gms.internal.measurement.zzdx] */
    /* JADX WARNING: type inference failed for: r1v48, types: [com.google.android.gms.internal.measurement.zzdw] */
    /* JADX WARNING: type inference failed for: r1v49, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v52, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v53, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v54, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v57, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v58, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v59, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v62, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v63, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v64, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v67, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v68, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v83, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v86, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v87, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v88, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v91, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v92, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v94, types: [com.google.android.gms.internal.measurement.zzdt] */
    /* JADX WARNING: type inference failed for: r1v97, types: [com.google.android.gms.internal.measurement.zzdv] */
    /* JADX WARNING: type inference failed for: r1v98, types: [com.google.android.gms.internal.measurement.zzdt] */
    /* JADX WARNING: type inference failed for: r1v99, types: [com.google.android.gms.internal.measurement.zzdt] */
    /* JADX WARNING: type inference failed for: r1v102, types: [com.google.android.gms.internal.measurement.zzdv] */
    /* JADX WARNING: type inference failed for: r1v103, types: [com.google.android.gms.internal.measurement.zzdt] */
    /* JADX WARNING: type inference failed for: r1v104, types: [com.google.android.gms.internal.measurement.zzdt] */
    /* JADX WARNING: type inference failed for: r1v107, types: [com.google.android.gms.internal.measurement.zzdv] */
    /* JADX WARNING: type inference failed for: r1v108, types: [com.google.android.gms.internal.measurement.zzdt] */
    /* JADX WARNING: type inference failed for: r1v109, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v112, types: [com.google.android.gms.internal.measurement.zzds] */
    /* JADX WARNING: type inference failed for: r1v113, types: [com.google.android.gms.internal.measurement.zzdq] */
    /* JADX WARNING: type inference failed for: r1v114 */
    /* JADX WARNING: type inference failed for: r1v115 */
    /* JADX WARNING: type inference failed for: r1v116 */
    /* JADX WARNING: type inference failed for: r1v117 */
    /* JADX WARNING: type inference failed for: r1v118 */
    /* JADX WARNING: type inference failed for: r1v119 */
    /* JADX WARNING: type inference failed for: r1v120 */
    /* JADX WARNING: type inference failed for: r1v121 */
    /* JADX WARNING: type inference failed for: r1v122 */
    /* JADX WARNING: type inference failed for: r1v123 */
    /* JADX WARNING: type inference failed for: r1v124 */
    /* JADX WARNING: type inference failed for: r1v125 */
    /* JADX WARNING: type inference failed for: r1v126 */
    /* JADX WARNING: type inference failed for: r1v127 */
    /* JADX WARNING: type inference failed for: r1v128 */
    /* JADX WARNING: type inference failed for: r1v129 */
    /* JADX WARNING: type inference failed for: r1v130 */
    /* JADX WARNING: type inference failed for: r1v131 */
    /* JADX WARNING: type inference failed for: r1v132 */
    /* JADX WARNING: type inference failed for: r1v133 */
    /* JADX WARNING: type inference failed for: r1v134 */
    /* JADX WARNING: type inference failed for: r1v135 */
    /* JADX WARNING: type inference failed for: r1v136 */
    /* JADX WARNING: type inference failed for: r1v137 */
    /* JADX WARNING: type inference failed for: r1v138 */
    /* JADX WARNING: type inference failed for: r1v139 */
    /* JADX WARNING: type inference failed for: r1v140 */
    /* JADX WARNING: type inference failed for: r1v141 */
    /* JADX WARNING: type inference failed for: r1v142 */
    /* JADX WARNING: type inference failed for: r1v143 */
    /* JADX WARNING: type inference failed for: r1v144 */
    /* JADX WARNING: type inference failed for: r1v145 */
    /* JADX WARNING: type inference failed for: r1v146 */
    /* JADX WARNING: type inference failed for: r1v147 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.measurement.zzds, com.google.android.gms.internal.measurement.zzdq, com.google.android.gms.internal.measurement.zzdx, com.google.android.gms.internal.measurement.zzdw, com.google.android.gms.internal.measurement.zzdv, com.google.android.gms.internal.measurement.zzdt]
  uses: [?[OBJECT, ARRAY], com.google.android.gms.internal.measurement.zzdq, com.google.android.gms.internal.measurement.zzdw, com.google.android.gms.internal.measurement.zzdt]
  mth insns count: 319
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 37 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ? r1 = 0;
        switch (i) {
            case 1:
                initialize(Stub.asInterface(parcel.readStrongBinder()), (zzdy) zzs.zza(parcel, zzdy.CREATOR), parcel.readLong());
                break;
            case 2:
                logEvent(parcel.readString(), parcel.readString(), (Bundle) zzs.zza(parcel, Bundle.CREATOR), zzs.zza(parcel), zzs.zza(parcel), parcel.readLong());
                break;
            case 3:
                String readString = parcel.readString();
                String readString2 = parcel.readString();
                Bundle bundle = (Bundle) zzs.zza(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface;
                    } else {
                        r1 = new zzds(readStrongBinder);
                    }
                }
                logEventAndBundle(readString, readString2, bundle, r1, parcel.readLong());
                break;
            case 4:
                setUserProperty(parcel.readString(), parcel.readString(), Stub.asInterface(parcel.readStrongBinder()), zzs.zza(parcel), parcel.readLong());
                break;
            case 5:
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                boolean zza = zzs.zza(parcel);
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface2 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface2;
                    } else {
                        r1 = new zzds(readStrongBinder2);
                    }
                }
                getUserProperties(readString3, readString4, zza, r1);
                break;
            case 6:
                String readString5 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface3 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface3;
                    } else {
                        r1 = new zzds(readStrongBinder3);
                    }
                }
                getMaxUserProperties(readString5, r1);
                break;
            case 7:
                setUserId(parcel.readString(), parcel.readLong());
                break;
            case 8:
                setConditionalUserProperty((Bundle) zzs.zza(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 9:
                clearConditionalUserProperty(parcel.readString(), parcel.readString(), (Bundle) zzs.zza(parcel, Bundle.CREATOR));
                break;
            case 10:
                String readString6 = parcel.readString();
                String readString7 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface4 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface4;
                    } else {
                        r1 = new zzds(readStrongBinder4);
                    }
                }
                getConditionalUserProperties(readString6, readString7, r1);
                break;
            case 11:
                setMeasurementEnabled(zzs.zza(parcel), parcel.readLong());
                break;
            case 12:
                resetAnalyticsData(parcel.readLong());
                break;
            case 13:
                setMinimumSessionDuration(parcel.readLong());
                break;
            case 14:
                setSessionTimeoutDuration(parcel.readLong());
                break;
            case 15:
                setCurrentScreen(Stub.asInterface(parcel.readStrongBinder()), parcel.readString(), parcel.readString(), parcel.readLong());
                break;
            case 16:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface5 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface5;
                    } else {
                        r1 = new zzds(readStrongBinder5);
                    }
                }
                getCurrentScreenName(r1);
                break;
            case 17:
                IBinder readStrongBinder6 = parcel.readStrongBinder();
                if (readStrongBinder6 != null) {
                    IInterface queryLocalInterface6 = readStrongBinder6.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface6 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface6;
                    } else {
                        r1 = new zzds(readStrongBinder6);
                    }
                }
                getCurrentScreenClass(r1);
                break;
            case 18:
                IBinder readStrongBinder7 = parcel.readStrongBinder();
                if (readStrongBinder7 != null) {
                    IInterface queryLocalInterface7 = readStrongBinder7.queryLocalInterface("com.google.android.gms.measurement.api.internal.IStringProvider");
                    if (queryLocalInterface7 instanceof zzdw) {
                        r1 = (zzdw) queryLocalInterface7;
                    } else {
                        r1 = new zzdx(readStrongBinder7);
                    }
                }
                setInstanceIdProvider(r1);
                break;
            case 19:
                IBinder readStrongBinder8 = parcel.readStrongBinder();
                if (readStrongBinder8 != null) {
                    IInterface queryLocalInterface8 = readStrongBinder8.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface8 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface8;
                    } else {
                        r1 = new zzds(readStrongBinder8);
                    }
                }
                getCachedAppInstanceId(r1);
                break;
            case 20:
                IBinder readStrongBinder9 = parcel.readStrongBinder();
                if (readStrongBinder9 != null) {
                    IInterface queryLocalInterface9 = readStrongBinder9.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface9 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface9;
                    } else {
                        r1 = new zzds(readStrongBinder9);
                    }
                }
                getAppInstanceId(r1);
                break;
            case 21:
                IBinder readStrongBinder10 = parcel.readStrongBinder();
                if (readStrongBinder10 != null) {
                    IInterface queryLocalInterface10 = readStrongBinder10.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface10 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface10;
                    } else {
                        r1 = new zzds(readStrongBinder10);
                    }
                }
                getGmpAppId(r1);
                break;
            case 22:
                IBinder readStrongBinder11 = parcel.readStrongBinder();
                if (readStrongBinder11 != null) {
                    IInterface queryLocalInterface11 = readStrongBinder11.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface11 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface11;
                    } else {
                        r1 = new zzds(readStrongBinder11);
                    }
                }
                generateEventId(r1);
                break;
            case 23:
                beginAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 24:
                endAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 25:
                onActivityStarted(Stub.asInterface(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 26:
                onActivityStopped(Stub.asInterface(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 27:
                onActivityCreated(Stub.asInterface(parcel.readStrongBinder()), (Bundle) zzs.zza(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 28:
                onActivityDestroyed(Stub.asInterface(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 29:
                onActivityPaused(Stub.asInterface(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 30:
                onActivityResumed(Stub.asInterface(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 31:
                IObjectWrapper asInterface = Stub.asInterface(parcel.readStrongBinder());
                IBinder readStrongBinder12 = parcel.readStrongBinder();
                if (readStrongBinder12 != null) {
                    IInterface queryLocalInterface12 = readStrongBinder12.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface12 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface12;
                    } else {
                        r1 = new zzds(readStrongBinder12);
                    }
                }
                onActivitySaveInstanceState(asInterface, r1, parcel.readLong());
                break;
            case 32:
                Bundle bundle2 = (Bundle) zzs.zza(parcel, Bundle.CREATOR);
                IBinder readStrongBinder13 = parcel.readStrongBinder();
                if (readStrongBinder13 != null) {
                    IInterface queryLocalInterface13 = readStrongBinder13.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface13 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface13;
                    } else {
                        r1 = new zzds(readStrongBinder13);
                    }
                }
                performAction(bundle2, r1, parcel.readLong());
                break;
            case 33:
                logHealthData(parcel.readInt(), parcel.readString(), Stub.asInterface(parcel.readStrongBinder()), Stub.asInterface(parcel.readStrongBinder()), Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 34:
                IBinder readStrongBinder14 = parcel.readStrongBinder();
                if (readStrongBinder14 != null) {
                    IInterface queryLocalInterface14 = readStrongBinder14.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface14 instanceof zzdt) {
                        r1 = (zzdt) queryLocalInterface14;
                    } else {
                        r1 = new zzdv(readStrongBinder14);
                    }
                }
                setEventInterceptor(r1);
                break;
            case 35:
                IBinder readStrongBinder15 = parcel.readStrongBinder();
                if (readStrongBinder15 != null) {
                    IInterface queryLocalInterface15 = readStrongBinder15.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface15 instanceof zzdt) {
                        r1 = (zzdt) queryLocalInterface15;
                    } else {
                        r1 = new zzdv(readStrongBinder15);
                    }
                }
                registerOnMeasurementEventListener(r1);
                break;
            case 36:
                IBinder readStrongBinder16 = parcel.readStrongBinder();
                if (readStrongBinder16 != null) {
                    IInterface queryLocalInterface16 = readStrongBinder16.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface16 instanceof zzdt) {
                        r1 = (zzdt) queryLocalInterface16;
                    } else {
                        r1 = new zzdv(readStrongBinder16);
                    }
                }
                unregisterOnMeasurementEventListener(r1);
                break;
            case 37:
                initForTests(zzs.zzb(parcel));
                break;
            case 38:
                IBinder readStrongBinder17 = parcel.readStrongBinder();
                if (readStrongBinder17 != null) {
                    IInterface queryLocalInterface17 = readStrongBinder17.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface17 instanceof zzdq) {
                        r1 = (zzdq) queryLocalInterface17;
                    } else {
                        r1 = new zzds(readStrongBinder17);
                    }
                }
                getTestFlag(r1, parcel.readInt());
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
