package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.WindowManager;

@TargetApi(16)
public final class zzqs {
    private final zzqt zzbjy;
    private final boolean zzbjz;
    private final long zzbka;
    private final long zzbkb;
    private long zzbkc;
    private long zzbkd;
    private long zzbke;
    private boolean zzbkf;
    private long zzbkg;
    private long zzbkh;
    private long zzbki;

    public zzqs() {
        this(-1.0d);
    }

    public zzqs(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        this(windowManager.getDefaultDisplay() != null ? (double) windowManager.getDefaultDisplay().getRefreshRate() : -1.0d);
    }

    private zzqs(double d) {
        this.zzbjz = d != -1.0d;
        if (this.zzbjz) {
            this.zzbjy = zzqt.zzhv();
            this.zzbka = (long) (1.0E9d / d);
            this.zzbkb = (this.zzbka * 80) / 100;
            return;
        }
        this.zzbjy = null;
        this.zzbka = -1;
        this.zzbkb = -1;
    }

    public final void enable() {
        this.zzbkf = false;
        if (this.zzbjz) {
            this.zzbjy.zzhw();
        }
    }

    public final void disable() {
        if (this.zzbjz) {
            this.zzbjy.zzhx();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004b  */
    public final long zzh(long j, long j2) {
        long j3;
        long j4;
        long j5;
        long j6 = 1000 * j;
        if (this.zzbkf) {
            if (j != this.zzbkc) {
                this.zzbki++;
                this.zzbkd = this.zzbke;
            }
            if (this.zzbki >= 6) {
                j3 = ((j6 - this.zzbkh) / this.zzbki) + this.zzbkd;
                if (zzi(j3, j2)) {
                    this.zzbkf = false;
                } else {
                    j4 = (this.zzbkg + j3) - this.zzbkh;
                    if (!this.zzbkf) {
                        this.zzbkh = j6;
                        this.zzbkg = j2;
                        this.zzbki = 0;
                        this.zzbkf = true;
                    }
                    this.zzbkc = j;
                    this.zzbke = j3;
                    if (this.zzbjy != null || this.zzbjy.zzbkj == 0) {
                        return j4;
                    }
                    long j7 = this.zzbjy.zzbkj;
                    long j8 = this.zzbka;
                    long j9 = j7 + (((j4 - j7) / j8) * j8);
                    if (j4 <= j9) {
                        j5 = j9 - j8;
                    } else {
                        long j10 = j9;
                        j9 = j8 + j9;
                        j5 = j10;
                    }
                    if (j9 - j4 >= j4 - j5) {
                        j9 = j5;
                    }
                    return j9 - this.zzbkb;
                }
            } else if (zzi(j6, j2)) {
                this.zzbkf = false;
            }
        }
        j4 = j2;
        j3 = j6;
        if (!this.zzbkf) {
        }
        this.zzbkc = j;
        this.zzbke = j3;
        if (this.zzbjy != null) {
        }
        return j4;
    }

    private final boolean zzi(long j, long j2) {
        return Math.abs((j2 - this.zzbkg) - (j - this.zzbkh)) > 20000000;
    }
}
