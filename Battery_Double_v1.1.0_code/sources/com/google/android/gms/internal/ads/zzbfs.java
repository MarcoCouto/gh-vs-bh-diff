package com.google.android.gms.internal.ads;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzark
public final class zzbfs extends zzbfk {
    private static final Set<String> zzewz = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat zzexa = new DecimalFormat("#,###");
    private File zzexb;
    private boolean zzexc;

    public zzbfs(zzbdz zzbdz) {
        super(zzbdz);
        File cacheDir = this.mContext.getCacheDir();
        if (cacheDir == null) {
            zzaxz.zzeo("Context.getCacheDir() returned null");
            return;
        }
        this.zzexb = new File(cacheDir, "admobVideoStreams");
        if (!this.zzexb.isDirectory() && !this.zzexb.mkdirs()) {
            String str = "Could not create preload cache directory at ";
            String valueOf = String.valueOf(this.zzexb.getAbsolutePath());
            zzaxz.zzeo(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            this.zzexb = null;
        } else if (!this.zzexb.setReadable(true, false) || !this.zzexb.setExecutable(true, false)) {
            String str2 = "Could not set cache file permissions at ";
            String valueOf2 = String.valueOf(this.zzexb.getAbsolutePath());
            zzaxz.zzeo(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
            this.zzexb = null;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:223|224|225|(4:226|227|228|229)) */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01ec, code lost:
        r3 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01f2, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzdn(r3);
        r5.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01f8, code lost:
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01fd, code lost:
        if ((r5 instanceof java.net.HttpURLConnection) == false) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:?, code lost:
        r1 = r5.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0208, code lost:
        if (r1 < 400) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x020a, code lost:
        r2 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x020c, code lost:
        r3 = "HTTP request failed. Code: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:?, code lost:
        r4 = java.lang.String.valueOf(java.lang.Integer.toString(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x021a, code lost:
        if (r4.length() == 0) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x021c, code lost:
        r3 = r3.concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0226, code lost:
        r3 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:?, code lost:
        r6 = new java.lang.StringBuilder(32 + java.lang.String.valueOf(r33).length());
        r6.append("HTTP status code ");
        r6.append(r1);
        r6.append(" at ");
        r6.append(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0250, code lost:
        throw new java.io.IOException(r6.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0251, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0252, code lost:
        r1 = r0;
        r4 = r3;
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0256, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0257, code lost:
        r1 = r0;
        r3 = r2;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x025a, code lost:
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
        r7 = r5.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0261, code lost:
        if (r7 >= 0) goto L_0x028c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0263, code lost:
        r1 = "Stream cache aborted, missing content-length header at ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:?, code lost:
        r2 = java.lang.String.valueOf(r33);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x026d, code lost:
        if (r2.length() == 0) goto L_0x0274;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x026f, code lost:
        r1 = r1.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0274, code lost:
        r1 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x027a, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzeo(r1);
        zza(r9, r12.getAbsolutePath(), "contentLengthMissing", null);
        zzewz.remove(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x028b, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:?, code lost:
        r1 = zzexa.format((long) r7);
        r3 = ((java.lang.Integer) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcou)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x02a3, code lost:
        if (r7 <= r3) goto L_0x02fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:?, code lost:
        r3 = new java.lang.StringBuilder((33 + java.lang.String.valueOf(r1).length()) + java.lang.String.valueOf(r33).length());
        r3.append("Content length ");
        r3.append(r1);
        r3.append(" exceeds limit at ");
        r3.append(r9);
        com.google.android.gms.internal.ads.zzaxz.zzeo(r3.toString());
        r2 = "File too big for full file cache. Size: ";
        r1 = java.lang.String.valueOf(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x02df, code lost:
        if (r1.length() == 0) goto L_0x02e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x02e1, code lost:
        r1 = r2.concat(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x02e6, code lost:
        r1 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x02eb, code lost:
        zza(r9, r12.getAbsolutePath(), "sizeExceeded", r1);
        zzewz.remove(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x02f9, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:?, code lost:
        r2 = new java.lang.StringBuilder((20 + java.lang.String.valueOf(r1).length()) + java.lang.String.valueOf(r33).length());
        r2.append("Caching ");
        r2.append(r1);
        r2.append(" bytes from ");
        r2.append(r9);
        com.google.android.gms.internal.ads.zzaxz.zzdn(r2.toString());
        r5 = java.nio.channels.Channels.newChannel(r5.getInputStream());
        r4 = new java.io.FileOutputStream(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:?, code lost:
        r2 = r4.getChannel();
        r1 = java.nio.ByteBuffer.allocate(1048576);
        r10 = com.google.android.gms.ads.internal.zzbv.zzlm();
        r17 = r10.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0353, code lost:
        r20 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:?, code lost:
        r11 = new com.google.android.gms.internal.ads.zzbai(((java.lang.Long) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcox)).longValue());
        r14 = ((java.lang.Long) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcow)).longValue();
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x036f, code lost:
        r21 = r5.read(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0373, code lost:
        if (r21 < 0) goto L_0x047f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0375, code lost:
        r6 = r6 + r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0377, code lost:
        if (r6 <= r3) goto L_0x03bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0379, code lost:
        r1 = "sizeExceeded";
        r2 = "File too big for full file cache. Size: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0389, code lost:
        if (r3.length() == 0) goto L_0x0391;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x038b, code lost:
        r10 = r2.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0396, code lost:
        r10 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x039e, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x039f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03a0, code lost:
        r3 = r1;
        r2 = r20;
        r1 = r0;
        r31 = r10;
        r10 = r4;
        r4 = r31;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03ab, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03ac, code lost:
        r3 = r1;
        r10 = r4;
        r2 = r20;
        r4 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03b7, code lost:
        r3 = r16;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:?, code lost:
        r1.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03c4, code lost:
        if (r2.write(r1) > 0) goto L_0x0475;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x03c6, code lost:
        r1.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x03d5, code lost:
        if ((r10.currentTimeMillis() - r17) <= (1000 * r14)) goto L_0x0406;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x03d7, code lost:
        r1 = "downloadTimeout";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:?, code lost:
        r2 = java.lang.Long.toString(r14);
        r5 = new java.lang.StringBuilder(29 + java.lang.String.valueOf(r2).length());
        r5.append("Timeout exceeded. Limit: ");
        r5.append(r2);
        r5.append(" sec");
        r10 = r5.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x0405, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0406, code lost:
        r25 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x040a, code lost:
        if (r8.zzexc == false) goto L_0x0416;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x040c, code lost:
        r1 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0415, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x041a, code lost:
        if (r11.tryAcquire() == false) goto L_0x044c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x041c, code lost:
        r26 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x0426, code lost:
        r27 = r11;
        r23 = r25;
        r11 = com.google.android.gms.internal.ads.zzbat.zztu;
        r1 = r1;
        r24 = r2;
        r25 = r3;
        r28 = r14;
        r14 = r4;
        r19 = r6;
        r30 = r5;
        r21 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:?, code lost:
        r1 = new com.google.android.gms.internal.ads.zzbfl(r8, r9, r12.getAbsolutePath(), r6, r7, false);
        r11.post(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x044c, code lost:
        r24 = r2;
        r30 = r5;
        r19 = r6;
        r21 = r7;
        r26 = r10;
        r27 = r11;
        r28 = r14;
        r23 = r25;
        r25 = r3;
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x0460, code lost:
        r4 = r14;
        r6 = r19;
        r7 = r21;
        r1 = r23;
        r2 = r24;
        r3 = r25;
        r10 = r26;
        r11 = r27;
        r14 = r28;
        r5 = r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x0475, code lost:
        r28 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x0479, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x047a, code lost:
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x047b, code lost:
        r1 = r0;
        r10 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x047f, code lost:
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:?, code lost:
        r14.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0488, code lost:
        if (com.google.android.gms.internal.ads.zzaxz.isLoggable(3) == false) goto L_0x04c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:?, code lost:
        r1 = zzexa.format((long) r6);
        r3 = new java.lang.StringBuilder((22 + java.lang.String.valueOf(r1).length()) + java.lang.String.valueOf(r33).length());
        r3.append("Preloaded ");
        r3.append(r1);
        r3.append(" bytes from ");
        r3.append(r9);
        com.google.android.gms.internal.ads.zzaxz.zzdn(r3.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x04c2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:?, code lost:
        r12.setReadable(true, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x04cd, code lost:
        if (r13.isFile() == false) goto L_0x04d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:?, code lost:
        r13.setLastModified(java.lang.System.currentTimeMillis());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:?, code lost:
        r13.createNewFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:?, code lost:
        zza(r9, r12.getAbsolutePath(), r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x04e3, code lost:
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:?, code lost:
        zzewz.remove(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x04e9, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x04ea, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:231:0x04ec, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x04ee, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x04ef, code lost:
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x04f0, code lost:
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x04f3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x04f4, code lost:
        r14 = r4;
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x04f6, code lost:
        r1 = r0;
        r10 = r14;
        r3 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x04fa, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x04fc, code lost:
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x0504, code lost:
        throw new java.io.IOException("Too many redirects (20)");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x0505, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x0507, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x0508, code lost:
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x0509, code lost:
        r1 = r0;
        r3 = r16;
        r4 = null;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x0510, code lost:
        if ((r1 instanceof java.lang.RuntimeException) != false) goto L_0x0512;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x0512, code lost:
        com.google.android.gms.ads.internal.zzbv.zzlj().zza(r1, "VideoStreamFullFileCache.preload");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x0520, code lost:
        if (r8.zzexc == false) goto L_0x0547;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0522, code lost:
        r5 = new java.lang.StringBuilder(26 + java.lang.String.valueOf(r33).length());
        r5.append("Preload aborted for URL \"");
        r5.append(r9);
        r5.append("\"");
        com.google.android.gms.internal.ads.zzaxz.zzen(r5.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x0547, code lost:
        r6 = new java.lang.StringBuilder(25 + java.lang.String.valueOf(r33).length());
        r6.append("Preload failed for URL \"");
        r6.append(r9);
        r6.append("\"");
        com.google.android.gms.internal.ads.zzaxz.zzc(r6.toString(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x0577, code lost:
        r1 = "Could not delete partial cache file at ";
        r5 = java.lang.String.valueOf(r12.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x0585, code lost:
        if (r5.length() == 0) goto L_0x058c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x0587, code lost:
        r1 = r1.concat(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x058c, code lost:
        r1 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x0592, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzeo(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x0595, code lost:
        zza(r9, r12.getAbsolutePath(), r3, r4);
        zzewz.remove(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x05a2, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0133, code lost:
        r16 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        com.google.android.gms.ads.internal.zzbv.zzls();
        r1 = ((java.lang.Integer) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcoy)).intValue();
        r3 = new java.net.URL(r9);
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x014f, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0152, code lost:
        if (r2 > 20) goto L_0x04fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0154, code lost:
        r5 = r3.openConnection();
        r5.setConnectTimeout(r1);
        r5.setReadTimeout(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0160, code lost:
        if ((r5 instanceof java.net.HttpURLConnection) != false) goto L_0x0172;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0169, code lost:
        throw new java.io.IOException("Invalid protocol.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x016a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x016b, code lost:
        r1 = r0;
        r4 = null;
        r2 = r15;
        r3 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        r5 = (java.net.HttpURLConnection) r5;
        r6 = new com.google.android.gms.internal.ads.zzbax();
        r6.zza(r5, (byte[]) null);
        r5.setInstanceFollowRedirects(false);
        r7 = r5.getResponseCode();
        r6.zza(r5, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0189, code lost:
        if ((r7 / 100) != 3) goto L_0x01fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        r4 = r5.getHeaderField(io.fabric.sdk.android.services.network.HttpRequest.HEADER_LOCATION);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0191, code lost:
        if (r4 != null) goto L_0x019b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x019a, code lost:
        throw new java.io.IOException("Missing Location header in redirect");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x019b, code lost:
        r6 = new java.net.URL(r3, r4);
        r3 = r6.getProtocol();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01a4, code lost:
        if (r3 != null) goto L_0x01ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01ad, code lost:
        throw new java.io.IOException("Protocol is null");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01b4, code lost:
        if (r3.equals("http") != false) goto L_0x01db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01bc, code lost:
        if (r3.equals("https") != false) goto L_0x01db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01be, code lost:
        r2 = "Unsupported scheme: ";
        r3 = java.lang.String.valueOf(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01ca, code lost:
        if (r3.length() == 0) goto L_0x01d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01cc, code lost:
        r2 = r2.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01d1, code lost:
        r2 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01da, code lost:
        throw new java.io.IOException(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01db, code lost:
        r3 = "Redirecting to ";
        r4 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01e5, code lost:
        if (r4.length() == 0) goto L_0x01ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01e7, code lost:
        r3 = r3.concat(r4);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:223:0x04da */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x0512  */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x0522  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0547  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0587  */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x058c  */
    public final boolean zzex(String str) {
        int i;
        String str2;
        String str3;
        boolean z;
        File[] listFiles;
        String str4 = str;
        FileOutputStream fileOutputStream = null;
        if (this.zzexb == null) {
            zza(str4, null, "noCacheDir", null);
            return false;
        }
        do {
            if (this.zzexb == null) {
                i = 0;
            } else {
                i = 0;
                for (File name : this.zzexb.listFiles()) {
                    if (!name.getName().endsWith(".done")) {
                        i++;
                    }
                }
            }
            if (i > ((Integer) zzwu.zzpz().zzd(zzaan.zzcot)).intValue()) {
                if (this.zzexb != null) {
                    long j = Long.MAX_VALUE;
                    File file = null;
                    for (File file2 : this.zzexb.listFiles()) {
                        if (!file2.getName().endsWith(".done")) {
                            long lastModified = file2.lastModified();
                            if (lastModified < j) {
                                file = file2;
                                j = lastModified;
                            }
                        }
                    }
                    if (file != null) {
                        z = file.delete();
                        File zzc = zzc(file);
                        if (zzc.isFile()) {
                            z &= zzc.delete();
                            continue;
                        } else {
                            continue;
                        }
                    }
                }
                z = false;
                continue;
            } else {
                File file3 = new File(this.zzexb, zzey(str));
                File zzc2 = zzc(file3);
                if (!file3.isFile() || !zzc2.isFile()) {
                    String valueOf = String.valueOf(this.zzexb.getAbsolutePath());
                    String valueOf2 = String.valueOf(str);
                    Object concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                    synchronized (zzewz) {
                        try {
                            if (zzewz.contains(concat)) {
                                String str5 = "Stream cache already in progress at ";
                                String valueOf3 = String.valueOf(str);
                                if (valueOf3.length() != 0) {
                                    str3 = str5.concat(valueOf3);
                                } else {
                                    th = new String(str5);
                                }
                                zzaxz.zzeo(str3);
                                zza(str4, file3.getAbsolutePath(), "inProgress", null);
                                return false;
                            }
                            zzewz.add(concat);
                        } finally {
                            while (true) {
                                str2 = th;
                            }
                        }
                    }
                } else {
                    int length = (int) file3.length();
                    String str6 = "Stream cache hit at ";
                    String valueOf4 = String.valueOf(str);
                    zzaxz.zzdn(valueOf4.length() != 0 ? str6.concat(valueOf4) : new String(str6));
                    zza(str4, file3.getAbsolutePath(), length);
                    return true;
                }
            }
        } while (z);
        zzaxz.zzeo("Unable to expire stream cache");
        zza(str4, null, "expireFailed", null);
        return false;
    }

    public final void abort() {
        this.zzexc = true;
    }

    private final File zzc(File file) {
        return new File(this.zzexb, String.valueOf(file.getName()).concat(".done"));
    }
}
