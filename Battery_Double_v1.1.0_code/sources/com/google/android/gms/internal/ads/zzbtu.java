package com.google.android.gms.internal.ads;

import java.io.IOException;

abstract class zzbtu<T, B> {
    zzbtu() {
    }

    /* access modifiers changed from: 0000 */
    public abstract void zza(B b, int i, long j);

    /* access modifiers changed from: 0000 */
    public abstract void zza(B b, int i, zzbpu zzbpu);

    /* access modifiers changed from: 0000 */
    public abstract void zza(B b, int i, T t);

    /* access modifiers changed from: 0000 */
    public abstract void zza(T t, zzbup zzbup) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract boolean zza(zzbtb zzbtb);

    /* access modifiers changed from: 0000 */
    public abstract int zzac(T t);

    /* access modifiers changed from: 0000 */
    public abstract T zzag(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract B zzah(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract int zzai(T t);

    /* access modifiers changed from: 0000 */
    public abstract B zzaoy();

    /* access modifiers changed from: 0000 */
    public abstract void zzb(B b, int i, long j);

    /* access modifiers changed from: 0000 */
    public abstract void zzc(B b, int i, int i2);

    /* access modifiers changed from: 0000 */
    public abstract void zzc(T t, zzbup zzbup) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void zzf(Object obj, T t);

    /* access modifiers changed from: 0000 */
    public abstract void zzg(Object obj, B b);

    /* access modifiers changed from: 0000 */
    public abstract T zzh(T t, T t2);

    /* access modifiers changed from: 0000 */
    public abstract void zzs(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract T zzz(B b);

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
    public final boolean zza(B b, zzbtb zzbtb) throws IOException {
        int tag = zzbtb.getTag();
        int i = tag >>> 3;
        switch (tag & 7) {
            case 0:
                zza(b, i, zzbtb.zzakw());
                return true;
            case 1:
                zzb(b, i, zzbtb.zzaky());
                return true;
            case 2:
                zza(b, i, zzbtb.zzalc());
                return true;
            case 3:
                Object zzaoy = zzaoy();
                int i2 = (i << 3) | 4;
                while (zzbtb.zzals() != Integer.MAX_VALUE) {
                    if (!zza((B) zzaoy, zzbtb)) {
                        if (i2 == zzbtb.getTag()) {
                            throw zzbrl.zzang();
                        }
                        zza(b, i, (T) zzz(zzaoy));
                        return true;
                    }
                }
                if (i2 == zzbtb.getTag()) {
                }
            case 4:
                return false;
            case 5:
                zzc(b, i, zzbtb.zzakz());
                return true;
            default:
                throw zzbrl.zzanh();
        }
    }
}
