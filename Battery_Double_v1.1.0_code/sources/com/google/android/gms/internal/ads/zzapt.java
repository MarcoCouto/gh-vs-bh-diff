package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.google.android.gms.ads.internal.zzbb;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONObject;

@zzark
public final class zzapt extends zzaxv {
    private final Object mLock;
    /* access modifiers changed from: private */
    public final zzapm zzdsj;
    private final zzaxg zzdsk;
    private final zzasm zzdsl;
    private final zzapw zzdta;
    private Future<zzaxf> zzdtb;

    public zzapt(Context context, zzbb zzbb, zzaxg zzaxg, zzcu zzcu, zzapm zzapm, zzaba zzaba) {
        zzapw zzapw = new zzapw(context, zzbb, new zzazs(context), zzcu, zzaxg, zzaba);
        this(zzaxg, zzapm, zzapw);
    }

    private zzapt(zzaxg zzaxg, zzapm zzapm, zzapw zzapw) {
        this.mLock = new Object();
        this.zzdsk = zzaxg;
        this.zzdsl = zzaxg.zzehy;
        this.zzdsj = zzapm;
        this.zzdta = zzapw;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0037  */
    public final void zzki() {
        int i;
        zzaxf zzaxf;
        int i2 = 0;
        zzaxf zzaxf2 = null;
        try {
            synchronized (this.mLock) {
                this.zzdtb = zzayf.zza(this.zzdta);
            }
            i = -2;
            zzaxf2 = (zzaxf) this.zzdtb.get(ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS, TimeUnit.MILLISECONDS);
        } catch (TimeoutException unused) {
            zzaxz.zzeo("Timed out waiting for native ad.");
            i2 = 2;
            this.zzdtb.cancel(true);
        } catch (InterruptedException | CancellationException | ExecutionException unused2) {
        } catch (Throwable th) {
            while (true) {
            }
            throw th;
        }
        if (zzaxf2 == null) {
            zzaxf = zzaxf2;
        } else {
            zzwb zzwb = this.zzdsk.zzeag.zzdwg;
            int i3 = this.zzdsl.orientation;
            long j = this.zzdsl.zzdlx;
            String str = this.zzdsk.zzeag.zzdwj;
            long j2 = this.zzdsl.zzdye;
            zzwf zzwf = this.zzdsk.zzbst;
            int i4 = i3;
            long j3 = this.zzdsl.zzdyc;
            long j4 = this.zzdsk.zzehn;
            long j5 = this.zzdsl.zzdyh;
            String str2 = this.zzdsl.zzdyi;
            JSONObject jSONObject = this.zzdsk.zzehh;
            boolean z = this.zzdsk.zzehy.zzdyu;
            long j6 = j2;
            zzaso zzaso = this.zzdsk.zzehy.zzdyv;
            String str3 = this.zzdsl.zzdyy;
            zzum zzum = this.zzdsk.zzehw;
            boolean z2 = this.zzdsk.zzehy.zzbph;
            boolean z3 = this.zzdsk.zzehy.zzdzc;
            boolean z4 = this.zzdsk.zzehy.zzbpi;
            boolean z5 = z;
            JSONObject jSONObject2 = jSONObject;
            String str4 = str2;
            long j7 = j5;
            long j8 = j3;
            long j9 = j4;
            int i5 = i4;
            zzwf zzwf2 = zzwf;
            zzaso zzaso2 = zzaso;
            zzaxf = new zzaxf(zzwb, null, null, i, null, null, i5, j, str, false, null, null, null, null, null, j6, zzwf2, j8, j9, j7, str4, jSONObject2, null, null, null, null, z5, zzaso2, null, null, str3, zzum, z2, false, z3, null, z4, this.zzdsk.zzehy.zzdzd, this.zzdsk.zzehy.zzdzf);
        }
        zzayh.zzelc.post(new zzapu(this, zzaxf));
        i = i2;
        if (zzaxf2 == null) {
        }
        zzayh.zzelc.post(new zzapu(this, zzaxf));
    }

    public final void onStop() {
        synchronized (this.mLock) {
            if (this.zzdtb != null) {
                this.zzdtb.cancel(true);
            }
        }
    }
}
