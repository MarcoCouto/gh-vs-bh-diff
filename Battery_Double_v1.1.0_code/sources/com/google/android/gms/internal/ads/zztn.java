package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

@zzark
public final class zztn {
    private final int zzbyz;
    private final zztd zzbzb;
    private String zzbzj;
    private String zzbzk;
    private final boolean zzbzl = false;
    private final int zzbzm;
    private final int zzbzn;

    public zztn(int i, int i2, int i3) {
        this.zzbyz = i;
        if (i2 > 64 || i2 < 0) {
            this.zzbzm = 64;
        } else {
            this.zzbzm = i2;
        }
        if (i3 <= 0) {
            this.zzbzn = 1;
        } else {
            this.zzbzn = i3;
        }
        this.zzbzb = new zztm(this.zzbzm);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0108, code lost:
        if (r3.size() < r1.zzbyz) goto L_0x010c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0113 A[LOOP:0: B:1:0x0012->B:65:0x0113, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0117 A[EDGE_INSN: B:76:0x0117->B:66:0x0117 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x010c A[SYNTHETIC] */
    public final String zza(ArrayList<String> arrayList, ArrayList<zztc> arrayList2) {
        boolean z;
        String str;
        String[] zze;
        boolean z2;
        int i;
        ArrayList<zztc> arrayList3 = arrayList2;
        Collections.sort(arrayList3, new zzto(this));
        HashSet hashSet = new HashSet();
        int i2 = 0;
        while (i2 < arrayList2.size()) {
            String[] split = Normalizer.normalize((CharSequence) arrayList.get(((zztc) arrayList3.get(i2)).zzob()), Form.NFKC).toLowerCase(Locale.US).split("\n");
            if (split.length != 0) {
                int i3 = 0;
                while (true) {
                    if (i3 >= split.length) {
                        break;
                    }
                    String str2 = split[i3];
                    if (str2.indexOf("'") != -1) {
                        StringBuilder sb = new StringBuilder(str2);
                        int i4 = 1;
                        boolean z3 = false;
                        while (true) {
                            int i5 = i4 + 2;
                            if (i5 > sb.length()) {
                                break;
                            }
                            if (sb.charAt(i4) == '\'') {
                                if (sb.charAt(i4 - 1) != ' ') {
                                    int i6 = i4 + 1;
                                    if ((sb.charAt(i6) == 's' || sb.charAt(i6) == 'S') && (i5 == sb.length() || sb.charAt(i5) == ' ')) {
                                        sb.insert(i4, ' ');
                                        i4 = i5;
                                        i = 1;
                                        z3 = true;
                                    }
                                }
                                sb.setCharAt(i4, ' ');
                                i = 1;
                                z3 = true;
                            } else {
                                i = 1;
                            }
                            i4 += i;
                        }
                        str = z3 ? sb.toString() : null;
                        if (str != null) {
                            this.zzbzk = str;
                            zze = zzth.zze(str, true);
                            if (zze.length < this.zzbzn) {
                                int i7 = 0;
                                while (true) {
                                    if (i7 >= zze.length) {
                                        break;
                                    }
                                    String str3 = "";
                                    int i8 = 0;
                                    while (true) {
                                        if (i8 >= this.zzbzn) {
                                            z2 = true;
                                            break;
                                        }
                                        int i9 = i7 + i8;
                                        if (i9 >= zze.length) {
                                            z2 = false;
                                            break;
                                        }
                                        if (i8 > 0) {
                                            str3 = String.valueOf(str3).concat(" ");
                                        }
                                        String valueOf = String.valueOf(str3);
                                        String valueOf2 = String.valueOf(zze[i9]);
                                        str3 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                                        i8++;
                                    }
                                    if (!z2) {
                                        break;
                                    }
                                    hashSet.add(str3);
                                    if (hashSet.size() >= this.zzbyz) {
                                        break;
                                    }
                                    i7++;
                                }
                            }
                            i3++;
                        }
                    }
                    str = str2;
                    zze = zzth.zze(str, true);
                    if (zze.length < this.zzbzn) {
                    }
                    i3++;
                }
                z = false;
                if (z) {
                    break;
                }
                i2++;
            }
            z = true;
            if (z) {
            }
        }
        zztg zztg = new zztg();
        this.zzbzj = "";
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            try {
                zztg.write(this.zzbzb.zzay((String) it.next()));
            } catch (IOException e) {
                zzaxz.zzb("Error while writing hash to byteStream", e);
            }
        }
        return zztg.toString();
    }
}
