package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Arrays;

final class zzbqh extends zzbqf {
    private final byte[] buffer;
    private int limit;
    private int pos;
    private final boolean zzflv;
    private int zzflw;
    private int zzflx;
    private int zzfly;
    private int zzflz;

    private zzbqh(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.zzflz = Integer.MAX_VALUE;
        this.buffer = bArr;
        this.limit = i2 + i;
        this.pos = i;
        this.zzflx = this.pos;
        this.zzflv = z;
    }

    public final int zzaku() throws IOException {
        if (zzalk()) {
            this.zzfly = 0;
            return 0;
        }
        this.zzfly = zzalm();
        if ((this.zzfly >>> 3) != 0) {
            return this.zzfly;
        }
        throw zzbrl.zzanf();
    }

    public final void zzeo(int i) throws zzbrl {
        if (this.zzfly != i) {
            throw zzbrl.zzang();
        }
    }

    public final boolean zzep(int i) throws IOException {
        int zzaku;
        int i2 = 0;
        switch (i & 7) {
            case 0:
                if (this.limit - this.pos >= 10) {
                    while (i2 < 10) {
                        byte[] bArr = this.buffer;
                        int i3 = this.pos;
                        this.pos = i3 + 1;
                        if (bArr[i3] < 0) {
                            i2++;
                        }
                    }
                    throw zzbrl.zzane();
                }
                while (i2 < 10) {
                    if (zzalr() < 0) {
                        i2++;
                    }
                }
                throw zzbrl.zzane();
                return true;
            case 1:
                zzet(8);
                return true;
            case 2:
                zzet(zzalm());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                zzet(4);
                return true;
            default:
                throw zzbrl.zzanh();
        }
        do {
            zzaku = zzaku();
            if (zzaku != 0) {
            }
            zzeo(((i >>> 3) << 3) | 4);
            return true;
        } while (zzep(zzaku));
        zzeo(((i >>> 3) << 3) | 4);
        return true;
    }

    public final double readDouble() throws IOException {
        return Double.longBitsToDouble(zzalp());
    }

    public final float readFloat() throws IOException {
        return Float.intBitsToFloat(zzalo());
    }

    public final long zzakv() throws IOException {
        return zzaln();
    }

    public final long zzakw() throws IOException {
        return zzaln();
    }

    public final int zzakx() throws IOException {
        return zzalm();
    }

    public final long zzaky() throws IOException {
        return zzalp();
    }

    public final int zzakz() throws IOException {
        return zzalo();
    }

    public final boolean zzala() throws IOException {
        return zzaln() != 0;
    }

    public final String readString() throws IOException {
        int zzalm = zzalm();
        if (zzalm > 0 && zzalm <= this.limit - this.pos) {
            String str = new String(this.buffer, this.pos, zzalm, zzbrf.UTF_8);
            this.pos += zzalm;
            return str;
        } else if (zzalm == 0) {
            return "";
        } else {
            if (zzalm < 0) {
                throw zzbrl.zzand();
            }
            throw zzbrl.zzanc();
        }
    }

    public final String zzalb() throws IOException {
        int zzalm = zzalm();
        if (zzalm > 0 && zzalm <= this.limit - this.pos) {
            String zzo = zzbuc.zzo(this.buffer, this.pos, zzalm);
            this.pos += zzalm;
            return zzo;
        } else if (zzalm == 0) {
            return "";
        } else {
            if (zzalm <= 0) {
                throw zzbrl.zzand();
            }
            throw zzbrl.zzanc();
        }
    }

    public final <T extends zzbsl> T zza(zzbsw<T> zzbsw, zzbqq zzbqq) throws IOException {
        int zzalm = zzalm();
        if (this.zzflq >= this.zzflr) {
            throw zzbrl.zzani();
        }
        int zzer = zzer(zzalm);
        this.zzflq++;
        T t = (zzbsl) zzbsw.zza(this, zzbqq);
        zzeo(0);
        this.zzflq--;
        zzes(zzer);
        return t;
    }

    public final zzbpu zzalc() throws IOException {
        byte[] bArr;
        int zzalm = zzalm();
        if (zzalm > 0 && zzalm <= this.limit - this.pos) {
            zzbpu zzi = zzbpu.zzi(this.buffer, this.pos, zzalm);
            this.pos += zzalm;
            return zzi;
        } else if (zzalm == 0) {
            return zzbpu.zzfli;
        } else {
            if (zzalm > 0 && zzalm <= this.limit - this.pos) {
                int i = this.pos;
                this.pos += zzalm;
                bArr = Arrays.copyOfRange(this.buffer, i, this.pos);
            } else if (zzalm > 0) {
                throw zzbrl.zzanc();
            } else if (zzalm == 0) {
                bArr = zzbrf.zzfqr;
            } else {
                throw zzbrl.zzand();
            }
            return zzbpu.zzs(bArr);
        }
    }

    public final int zzald() throws IOException {
        return zzalm();
    }

    public final int zzale() throws IOException {
        return zzalm();
    }

    public final int zzalf() throws IOException {
        return zzalo();
    }

    public final long zzalg() throws IOException {
        return zzalp();
    }

    public final int zzalh() throws IOException {
        return zzeu(zzalm());
    }

    public final long zzali() throws IOException {
        return zzax(zzaln());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        if (r1[r2] >= 0) goto L_0x006a;
     */
    private final int zzalm() throws IOException {
        byte b;
        int i = this.pos;
        if (this.limit != i) {
            byte[] bArr = this.buffer;
            int i2 = i + 1;
            byte b2 = bArr[i];
            if (b2 >= 0) {
                this.pos = i2;
                return b2;
            } else if (this.limit - i2 >= 9) {
                int i3 = i2 + 1;
                byte b3 = b2 ^ (bArr[i2] << 7);
                if (b3 < 0) {
                    b = b3 ^ Byte.MIN_VALUE;
                } else {
                    int i4 = i3 + 1;
                    byte b4 = b3 ^ (bArr[i3] << 14);
                    if (b4 >= 0) {
                        b = b4 ^ 16256;
                    } else {
                        i3 = i4 + 1;
                        byte b5 = b4 ^ (bArr[i4] << 21);
                        if (b5 < 0) {
                            b = b5 ^ -2080896;
                        } else {
                            i4 = i3 + 1;
                            byte b6 = bArr[i3];
                            b = (b5 ^ (b6 << 28)) ^ 266354560;
                            if (b6 < 0) {
                                i3 = i4 + 1;
                                if (bArr[i4] < 0) {
                                    i4 = i3 + 1;
                                    if (bArr[i3] < 0) {
                                        i3 = i4 + 1;
                                        if (bArr[i4] < 0) {
                                            i4 = i3 + 1;
                                            if (bArr[i3] < 0) {
                                                i3 = i4 + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i3 = i4;
                }
                this.pos = i3;
                return b;
            }
        }
        return (int) zzalj();
    }

    private final long zzaln() throws IOException {
        long j;
        int i;
        long j2;
        long j3;
        int i2 = this.pos;
        if (this.limit != i2) {
            byte[] bArr = this.buffer;
            int i3 = i2 + 1;
            byte b = bArr[i2];
            if (b >= 0) {
                this.pos = i3;
                return (long) b;
            } else if (this.limit - i3 >= 9) {
                int i4 = i3 + 1;
                byte b2 = b ^ (bArr[i3] << 7);
                if (b2 < 0) {
                    j3 = (long) (b2 ^ Byte.MIN_VALUE);
                } else {
                    int i5 = i4 + 1;
                    byte b3 = b2 ^ (bArr[i4] << 14);
                    if (b3 >= 0) {
                        long j4 = (long) (b3 ^ 16256);
                        i = i5;
                        j = j4;
                    } else {
                        i4 = i5 + 1;
                        byte b4 = b3 ^ (bArr[i5] << 21);
                        if (b4 < 0) {
                            j3 = (long) (b4 ^ -2080896);
                        } else {
                            long j5 = (long) b4;
                            i = i4 + 1;
                            long j6 = (((long) bArr[i4]) << 28) ^ j5;
                            if (j6 >= 0) {
                                j = j6 ^ 266354560;
                            } else {
                                int i6 = i + 1;
                                long j7 = j6 ^ (((long) bArr[i]) << 35);
                                if (j7 < 0) {
                                    j2 = -34093383808L ^ j7;
                                } else {
                                    i = i6 + 1;
                                    long j8 = j7 ^ (((long) bArr[i6]) << 42);
                                    if (j8 >= 0) {
                                        j = j8 ^ 4363953127296L;
                                    } else {
                                        i6 = i + 1;
                                        long j9 = j8 ^ (((long) bArr[i]) << 49);
                                        if (j9 < 0) {
                                            j2 = -558586000294016L ^ j9;
                                        } else {
                                            i = i6 + 1;
                                            long j10 = (j9 ^ (((long) bArr[i6]) << 56)) ^ 71499008037633920L;
                                            if (j10 < 0) {
                                                i6 = i + 1;
                                                if (((long) bArr[i]) >= 0) {
                                                    j = j10;
                                                    i = i6;
                                                }
                                            } else {
                                                j = j10;
                                            }
                                        }
                                    }
                                }
                                j = j2;
                                i = i6;
                            }
                        }
                    }
                    this.pos = i;
                    return j;
                }
                j = j3;
                i = i4;
                this.pos = i;
                return j;
            }
        }
        return zzalj();
    }

    /* access modifiers changed from: 0000 */
    public final long zzalj() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte zzalr = zzalr();
            j |= ((long) (zzalr & Byte.MAX_VALUE)) << i;
            if ((zzalr & 128) == 0) {
                return j;
            }
        }
        throw zzbrl.zzane();
    }

    private final int zzalo() throws IOException {
        int i = this.pos;
        if (this.limit - i < 4) {
            throw zzbrl.zzanc();
        }
        byte[] bArr = this.buffer;
        this.pos = i + 4;
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    private final long zzalp() throws IOException {
        int i = this.pos;
        if (this.limit - i < 8) {
            throw zzbrl.zzanc();
        }
        byte[] bArr = this.buffer;
        this.pos = i + 8;
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    public final int zzer(int i) throws zzbrl {
        if (i < 0) {
            throw zzbrl.zzand();
        }
        int zzall = i + zzall();
        int i2 = this.zzflz;
        if (zzall > i2) {
            throw zzbrl.zzanc();
        }
        this.zzflz = zzall;
        zzalq();
        return i2;
    }

    private final void zzalq() {
        this.limit += this.zzflw;
        int i = this.limit - this.zzflx;
        if (i > this.zzflz) {
            this.zzflw = i - this.zzflz;
            this.limit -= this.zzflw;
            return;
        }
        this.zzflw = 0;
    }

    public final void zzes(int i) {
        this.zzflz = i;
        zzalq();
    }

    public final boolean zzalk() throws IOException {
        return this.pos == this.limit;
    }

    public final int zzall() {
        return this.pos - this.zzflx;
    }

    private final byte zzalr() throws IOException {
        if (this.pos == this.limit) {
            throw zzbrl.zzanc();
        }
        byte[] bArr = this.buffer;
        int i = this.pos;
        this.pos = i + 1;
        return bArr[i];
    }

    public final void zzet(int i) throws IOException {
        if (i >= 0 && i <= this.limit - this.pos) {
            this.pos += i;
        } else if (i < 0) {
            throw zzbrl.zzand();
        } else {
            throw zzbrl.zzanc();
        }
    }
}
