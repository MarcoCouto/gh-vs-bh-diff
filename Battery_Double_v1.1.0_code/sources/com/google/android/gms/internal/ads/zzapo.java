package com.google.android.gms.internal.ads;

import org.json.JSONObject;

@zzark
public final class zzapo extends zzaxv {
    /* access modifiers changed from: private */
    public final zzapm zzdsj;
    private final zzaxg zzdsk;
    private final zzasm zzdsl = this.zzdsk.zzehy;

    public zzapo(zzaxg zzaxg, zzapm zzapm) {
        this.zzdsk = zzaxg;
        this.zzdsj = zzapm;
    }

    public final void onStop() {
    }

    public final void zzki() {
        zzwb zzwb = this.zzdsk.zzeag.zzdwg;
        int i = this.zzdsl.orientation;
        long j = this.zzdsl.zzdlx;
        String str = this.zzdsk.zzeag.zzdwj;
        long j2 = this.zzdsl.zzdye;
        zzwf zzwf = this.zzdsk.zzbst;
        long j3 = this.zzdsl.zzdyc;
        long j4 = this.zzdsk.zzehn;
        long j5 = j2;
        long j6 = this.zzdsl.zzdyh;
        String str2 = this.zzdsl.zzdyi;
        JSONObject jSONObject = this.zzdsk.zzehh;
        boolean z = this.zzdsk.zzehy.zzdyu;
        zzaso zzaso = this.zzdsk.zzehy.zzdyv;
        zzum zzum = this.zzdsk.zzehw;
        boolean z2 = this.zzdsk.zzehy.zzbph;
        boolean z3 = this.zzdsk.zzehx;
        boolean z4 = this.zzdsk.zzehy.zzdzc;
        boolean z5 = this.zzdsk.zzehy.zzbpi;
        String str3 = str2;
        zzwf zzwf2 = zzwf;
        long j7 = j5;
        zzaxf zzaxf = r1;
        long j8 = j3;
        long j9 = j4;
        long j10 = j6;
        zzaxf zzaxf2 = new zzaxf(zzwb, null, null, 0, null, null, i, j, str, false, null, null, null, null, null, j7, zzwf2, j8, j9, j10, str3, jSONObject, null, null, null, null, z, zzaso, null, null, null, zzum, z2, z3, z4, null, z5, this.zzdsk.zzehy.zzdzd, this.zzdsk.zzehy.zzdzf);
        zzayh.zzelc.post(new zzapp(this, zzaxf));
    }
}
