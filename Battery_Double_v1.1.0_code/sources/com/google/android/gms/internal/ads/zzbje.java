package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks;
import com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

@VisibleForTesting
final class zzbje implements BaseConnectionCallbacks, BaseOnConnectionFailedListener {
    private final String packageName;
    @VisibleForTesting
    private zzbjf zzfcr;
    private final String zzfcs;
    private final LinkedBlockingQueue<zzbl> zzfct;
    private final HandlerThread zzfcu = new HandlerThread("GassClient");

    public zzbje(Context context, String str, String str2) {
        this.packageName = str;
        this.zzfcs = str2;
        this.zzfcu.start();
        this.zzfcr = new zzbjf(context, this.zzfcu.getLooper(), this, this);
        this.zzfct = new LinkedBlockingQueue<>();
        this.zzfcr.checkAvailabilityAndConnect();
    }

    public final zzbl zzdj(int i) {
        zzbl zzbl;
        try {
            zzbl = (zzbl) this.zzfct.poll(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException unused) {
            zzbl = null;
        }
        return zzbl == null ? zzafl() : zzbl;
    }

    private final zzbjk zzafk() {
        try {
            return this.zzfcr.zzafm();
        } catch (DeadObjectException | IllegalStateException unused) {
            return null;
        }
    }

    public final void onConnectionSuspended(int i) {
        try {
            this.zzfct.put(zzafl());
        } catch (InterruptedException unused) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0039, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003a, code lost:
        zzwi();
        r3.zzfcu.quit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0042, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0025, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        r3.zzfct.put(zzafl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0031, code lost:
        zzwi();
        r3.zzfcu.quit();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0027 */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0025 A[ExcHandler: all (r4v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:2:0x0006] */
    public final void onConnected(Bundle bundle) {
        zzbjk zzafk = zzafk();
        if (zzafk != null) {
            try {
                this.zzfct.put(zzafk.zza(new zzbjg(this.packageName, this.zzfcs)).zzafn());
                zzwi();
                this.zzfcu.quit();
            } catch (InterruptedException unused) {
            } catch (Throwable th) {
            }
        }
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            this.zzfct.put(zzafl());
        } catch (InterruptedException unused) {
        }
    }

    private final void zzwi() {
        if (this.zzfcr == null) {
            return;
        }
        if (this.zzfcr.isConnected() || this.zzfcr.isConnecting()) {
            this.zzfcr.disconnect();
        }
    }

    @VisibleForTesting
    private static zzbl zzafl() {
        zzbl zzbl = new zzbl();
        zzbl.zzeo = Long.valueOf(PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID);
        return zzbl;
    }
}
