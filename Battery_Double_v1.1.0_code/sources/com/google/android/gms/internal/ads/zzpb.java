package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.NoRouteToHostException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class zzpb implements zzov {
    private static final Pattern zzbgd = Pattern.compile("^bytes (\\d+)-(\\d+)/(\\d+)$");
    private static final AtomicReference<byte[]> zzbge = new AtomicReference<>();
    private zzoz zzazo;
    private boolean zzbfr;
    private final boolean zzbgf;
    private final int zzbgg;
    private final int zzbgh;
    private final String zzbgi;
    private final zzpz<String> zzbgj;
    private final zzpe zzbgk;
    private final zzpe zzbgl;
    private final zzpn<? super zzpb> zzbgm;
    private HttpURLConnection zzbgn;
    private InputStream zzbgo;
    private long zzbgp;
    private long zzbgq;
    private long zzbgr;
    private long zzcd;

    public zzpb(String str, zzpz<String> zzpz, zzpn<? super zzpb> zzpn, int i, int i2, boolean z, zzpe zzpe) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException();
        }
        this.zzbgi = str;
        this.zzbgj = null;
        this.zzbgm = zzpn;
        this.zzbgl = new zzpe();
        this.zzbgg = i;
        this.zzbgh = i2;
        this.zzbgf = true;
        this.zzbgk = null;
    }

    public final Uri getUri() {
        if (this.zzbgn == null) {
            return null;
        }
        return Uri.parse(this.zzbgn.getURL().toString());
    }

    public final Map<String, List<String>> getResponseHeaders() {
        if (this.zzbgn == null) {
            return null;
        }
        return this.zzbgn.getHeaderFields();
    }

    public final long zza(zzoz zzoz) throws zzpc {
        HttpURLConnection httpURLConnection;
        zzoz zzoz2 = zzoz;
        this.zzazo = zzoz2;
        long j = 0;
        this.zzcd = 0;
        this.zzbgr = 0;
        try {
            URL url = new URL(zzoz2.uri.toString());
            byte[] bArr = zzoz2.zzbft;
            long j2 = zzoz2.zzaha;
            long j3 = zzoz2.zzcc;
            boolean zzbg = zzoz2.zzbg(1);
            if (!this.zzbgf) {
                httpURLConnection = zza(url, bArr, j2, j3, zzbg, true);
            } else {
                URL url2 = url;
                byte[] bArr2 = bArr;
                int i = 0;
                while (true) {
                    int i2 = i + 1;
                    if (i <= 20) {
                        URL url3 = url2;
                        int i3 = i2;
                        long j4 = j3;
                        long j5 = j2;
                        httpURLConnection = zza(url2, bArr2, j2, j3, zzbg, false);
                        int responseCode = httpURLConnection.getResponseCode();
                        if (!(responseCode == 300 || responseCode == 301 || responseCode == 302 || responseCode == 303)) {
                            if (bArr2 == null) {
                                if (responseCode != 307) {
                                    if (responseCode != 308) {
                                        break;
                                    }
                                }
                            } else {
                                break;
                            }
                        }
                        bArr2 = null;
                        String headerField = httpURLConnection.getHeaderField(HttpRequest.HEADER_LOCATION);
                        httpURLConnection.disconnect();
                        if (headerField == null) {
                            throw new ProtocolException("Null location redirect");
                        }
                        url2 = new URL(url3, headerField);
                        String protocol = url2.getProtocol();
                        if ("https".equals(protocol) || "http".equals(protocol)) {
                            i = i3;
                            j3 = j4;
                            j2 = j5;
                        } else {
                            String str = "Unsupported protocol redirect: ";
                            String valueOf = String.valueOf(protocol);
                            throw new ProtocolException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                        }
                    } else {
                        int i4 = i2;
                        StringBuilder sb = new StringBuilder(31);
                        sb.append("Too many redirects: ");
                        sb.append(i4);
                        throw new NoRouteToHostException(sb.toString());
                    }
                }
            }
            this.zzbgn = httpURLConnection;
            try {
                int responseCode2 = this.zzbgn.getResponseCode();
                if (responseCode2 < 200 || responseCode2 > 299) {
                    Map headerFields = this.zzbgn.getHeaderFields();
                    zzgw();
                    zzpd zzpd = new zzpd(responseCode2, headerFields, zzoz2);
                    if (responseCode2 == 416) {
                        zzpd.initCause(new zzox(0));
                    }
                    throw zzpd;
                }
                this.zzbgn.getContentType();
                if (responseCode2 == 200 && zzoz2.zzaha != 0) {
                    j = zzoz2.zzaha;
                }
                this.zzbgp = j;
                if (!zzoz2.zzbg(1)) {
                    long j6 = -1;
                    if (zzoz2.zzcc != -1) {
                        this.zzbgq = zzoz2.zzcc;
                    } else {
                        long zzc = zzc(this.zzbgn);
                        if (zzc != -1) {
                            j6 = zzc - this.zzbgp;
                        }
                        this.zzbgq = j6;
                    }
                } else {
                    this.zzbgq = zzoz2.zzcc;
                }
                try {
                    this.zzbgo = this.zzbgn.getInputStream();
                    this.zzbfr = true;
                    if (this.zzbgm != null) {
                        this.zzbgm.zza(this, zzoz2);
                    }
                    return this.zzbgq;
                } catch (IOException e) {
                    IOException iOException = e;
                    zzgw();
                    throw new zzpc(iOException, zzoz2, 1);
                }
            } catch (IOException e2) {
                IOException iOException2 = e2;
                zzgw();
                String str2 = "Unable to connect to ";
                String valueOf2 = String.valueOf(zzoz2.uri.toString());
                throw new zzpc(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2), iOException2, zzoz2, 1);
            }
        } catch (IOException e3) {
            IOException iOException3 = e3;
            String str3 = "Unable to connect to ";
            String valueOf3 = String.valueOf(zzoz2.uri.toString());
            throw new zzpc(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3), iOException3, zzoz2, 1);
        }
    }

    public final int read(byte[] bArr, int i, int i2) throws zzpc {
        try {
            if (this.zzbgr != this.zzbgp) {
                byte[] bArr2 = (byte[]) zzbge.getAndSet(null);
                if (bArr2 == null) {
                    bArr2 = new byte[4096];
                }
                while (this.zzbgr != this.zzbgp) {
                    int read = this.zzbgo.read(bArr2, 0, (int) Math.min(this.zzbgp - this.zzbgr, (long) bArr2.length));
                    if (Thread.interrupted()) {
                        throw new InterruptedIOException();
                    } else if (read == -1) {
                        throw new EOFException();
                    } else {
                        this.zzbgr += (long) read;
                        if (this.zzbgm != null) {
                            this.zzbgm.zzc(this, read);
                        }
                    }
                }
                zzbge.set(bArr2);
            }
            if (i2 == 0) {
                return 0;
            }
            if (this.zzbgq != -1) {
                long j = this.zzbgq - this.zzcd;
                if (j == 0) {
                    return -1;
                }
                i2 = (int) Math.min((long) i2, j);
            }
            int read2 = this.zzbgo.read(bArr, i, i2);
            if (read2 != -1) {
                this.zzcd += (long) read2;
                if (this.zzbgm != null) {
                    this.zzbgm.zzc(this, read2);
                }
                return read2;
            } else if (this.zzbgq == -1) {
                return -1;
            } else {
                throw new EOFException();
            }
        } catch (IOException e) {
            throw new zzpc(e, this.zzazo, 2);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:4|(1:6)(1:7)|8|(5:13|14|(2:16|(1:18))(1:19)|21|(1:25))|26|27) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0038, code lost:
        if (r3 > android.support.v4.media.session.PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) goto L_0x003a;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x006b */
    public final void close() throws zzpc {
        try {
            if (this.zzbgo != null) {
                HttpURLConnection httpURLConnection = this.zzbgn;
                long j = this.zzbgq == -1 ? this.zzbgq : this.zzbgq - this.zzcd;
                if (zzqe.SDK_INT == 19 || zzqe.SDK_INT == 20) {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    if (j == -1) {
                        if (inputStream.read() == -1) {
                        }
                    }
                    String name = inputStream.getClass().getName();
                    if (name.equals("com.android.okhttp.internal.http.HttpTransport$ChunkedInputStream") || name.equals("com.android.okhttp.internal.http.HttpTransport$FixedLengthInputStream")) {
                        Method declaredMethod = inputStream.getClass().getSuperclass().getDeclaredMethod("unexpectedEndOfInput", new Class[0]);
                        declaredMethod.setAccessible(true);
                        declaredMethod.invoke(inputStream, new Object[0]);
                    }
                }
                this.zzbgo.close();
            }
            this.zzbgo = null;
            zzgw();
            if (this.zzbfr) {
                this.zzbfr = false;
                if (this.zzbgm != null) {
                    this.zzbgm.zze(this);
                }
            }
        } catch (IOException e) {
            throw new zzpc(e, this.zzazo, 3);
        } catch (Throwable th) {
            this.zzbgo = null;
            zzgw();
            if (this.zzbfr) {
                this.zzbfr = false;
                if (this.zzbgm != null) {
                    this.zzbgm.zze(this);
                }
            }
            throw th;
        }
    }

    private final HttpURLConnection zza(URL url, byte[] bArr, long j, long j2, boolean z, boolean z2) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setConnectTimeout(this.zzbgg);
        httpURLConnection.setReadTimeout(this.zzbgh);
        for (Entry entry : this.zzbgl.zzgx().entrySet()) {
            httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
        if (!(j == 0 && j2 == -1)) {
            StringBuilder sb = new StringBuilder(27);
            sb.append("bytes=");
            sb.append(j);
            sb.append("-");
            String sb2 = sb.toString();
            if (j2 != -1) {
                String valueOf = String.valueOf(sb2);
                long j3 = (j + j2) - 1;
                StringBuilder sb3 = new StringBuilder(20 + String.valueOf(valueOf).length());
                sb3.append(valueOf);
                sb3.append(j3);
                sb2 = sb3.toString();
            }
            httpURLConnection.setRequestProperty("Range", sb2);
        }
        httpURLConnection.setRequestProperty("User-Agent", this.zzbgi);
        if (!z) {
            httpURLConnection.setRequestProperty(HttpRequest.HEADER_ACCEPT_ENCODING, "identity");
        }
        httpURLConnection.setInstanceFollowRedirects(z2);
        httpURLConnection.setDoOutput(bArr != null);
        if (bArr != null) {
            httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
            if (bArr.length != 0) {
                httpURLConnection.setFixedLengthStreamingMode(bArr.length);
                httpURLConnection.connect();
                OutputStream outputStream = httpURLConnection.getOutputStream();
                outputStream.write(bArr);
                outputStream.close();
                return httpURLConnection;
            }
        }
        httpURLConnection.connect();
        return httpURLConnection;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0045  */
    private static long zzc(HttpURLConnection httpURLConnection) {
        long j;
        String headerField;
        String headerField2 = httpURLConnection.getHeaderField(HttpRequest.HEADER_CONTENT_LENGTH);
        if (!TextUtils.isEmpty(headerField2)) {
            try {
                j = Long.parseLong(headerField2);
            } catch (NumberFormatException unused) {
                StringBuilder sb = new StringBuilder(28 + String.valueOf(headerField2).length());
                sb.append("Unexpected Content-Length [");
                sb.append(headerField2);
                sb.append("]");
                Log.e("DefaultHttpDataSource", sb.toString());
            }
            headerField = httpURLConnection.getHeaderField("Content-Range");
            if (!TextUtils.isEmpty(headerField)) {
                return j;
            }
            Matcher matcher = zzbgd.matcher(headerField);
            if (!matcher.find()) {
                return j;
            }
            try {
                long parseLong = (Long.parseLong(matcher.group(2)) - Long.parseLong(matcher.group(1))) + 1;
                if (j >= 0) {
                    if (j == parseLong) {
                        return j;
                    }
                    StringBuilder sb2 = new StringBuilder(26 + String.valueOf(headerField2).length() + String.valueOf(headerField).length());
                    sb2.append("Inconsistent headers [");
                    sb2.append(headerField2);
                    sb2.append("] [");
                    sb2.append(headerField);
                    sb2.append("]");
                    Log.w("DefaultHttpDataSource", sb2.toString());
                    parseLong = Math.max(j, parseLong);
                }
                return parseLong;
            } catch (NumberFormatException unused2) {
                StringBuilder sb3 = new StringBuilder(27 + String.valueOf(headerField).length());
                sb3.append("Unexpected Content-Range [");
                sb3.append(headerField);
                sb3.append("]");
                Log.e("DefaultHttpDataSource", sb3.toString());
                return j;
            }
        }
        j = -1;
        headerField = httpURLConnection.getHeaderField("Content-Range");
        if (!TextUtils.isEmpty(headerField)) {
        }
    }

    private final void zzgw() {
        if (this.zzbgn != null) {
            try {
                this.zzbgn.disconnect();
            } catch (Exception e) {
                Log.e("DefaultHttpDataSource", "Unexpected error while disconnecting", e);
            }
            this.zzbgn = null;
        }
    }
}
