package com.google.android.gms.internal.ads;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@zzark
public final class zzavu extends zzaxv implements zzavt {
    private final Context mContext;
    private final Object mLock;
    private final zzaxg zzdsk;
    private final long zzeet;
    private final ArrayList<zzavk> zzefh;
    private final List<zzavn> zzefi;
    private final HashSet<String> zzefj;
    private final zzauk zzefk;

    public zzavu(Context context, zzaxg zzaxg, zzauk zzauk) {
        Context context2 = context;
        zzaxg zzaxg2 = zzaxg;
        zzauk zzauk2 = zzauk;
        this(context2, zzaxg2, zzauk2, ((Long) zzwu.zzpz().zzd(zzaan.zzcrj)).longValue());
    }

    public final void onStop() {
    }

    public final void zza(String str, int i) {
    }

    @VisibleForTesting
    private zzavu(Context context, zzaxg zzaxg, zzauk zzauk, long j) {
        this.zzefh = new ArrayList<>();
        this.zzefi = new ArrayList();
        this.zzefj = new HashSet<>();
        this.mLock = new Object();
        this.mContext = context;
        this.zzdsk = zzaxg;
        this.zzefk = zzauk;
        this.zzeet = j;
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:95:0x016f */
    public final void zzki() {
        zzavk zzavk;
        Object obj;
        Iterator it;
        Iterator it2 = this.zzdsk.zzehj.zzdlp.iterator();
        while (it2.hasNext()) {
            zzakq zzakq = (zzakq) it2.next();
            String str = zzakq.zzdle;
            for (String str2 : zzakq.zzdkw) {
                if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str2) || "com.google.ads.mediation.customevent.CustomEventAdapter".equals(str2)) {
                    try {
                        str2 = new JSONObject(str).getString("class_name");
                    } catch (JSONException e) {
                        it = it2;
                        zzaxz.zzb("Unable to determine custom event class name, skipping...", e);
                    }
                }
                String str3 = str2;
                Object obj2 = this.mLock;
                synchronized (obj2) {
                    try {
                        zzavy zzdd = this.zzefk.zzdd(str3);
                        if (!(zzdd == null || zzdd.zzxo() == null)) {
                            if (zzdd.zzxn() != null) {
                                r1 = r1;
                                obj = obj2;
                                it = it2;
                                zzavk zzavk2 = r1;
                                zzavk zzavk3 = new zzavk(this.mContext, str3, str, zzakq, this.zzdsk, zzdd, this, this.zzeet);
                                zzavk2.zza(this.zzefk.zzxb());
                                this.zzefh.add(zzavk2);
                                it2 = it;
                            }
                        }
                        obj = obj2;
                        it = it2;
                        this.zzefi.add(new zzavp().zzdg(zzakq.zzdkx).zzdf(str3).zzar(0).zzcu(7).zzxm());
                        it2 = it;
                    } catch (Throwable th) {
                        th = th;
                        Throwable th2 = th;
                        throw th2;
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        ArrayList arrayList = this.zzefh;
        int size = arrayList.size();
        int i = 0;
        int i2 = 0;
        while (i2 < size) {
            Object obj3 = arrayList.get(i2);
            i2++;
            zzavk zzavk4 = (zzavk) obj3;
            if (hashSet.add(zzavk4.zzdml)) {
                zzavk4.zzxi();
            }
        }
        ArrayList arrayList2 = this.zzefh;
        int size2 = arrayList2.size();
        while (true) {
            if (i >= size2) {
                break;
            }
            Object obj4 = arrayList2.get(i);
            i++;
            zzavk = (zzavk) obj4;
            try {
                zzavk.zzxi().get();
                synchronized (this.mLock) {
                    try {
                        if (!TextUtils.isEmpty(zzavk.zzdml)) {
                            this.zzefi.add(zzavk.zzxj());
                        }
                    } catch (Throwable th3) {
                        while (true) {
                            throw th3;
                        }
                    }
                }
                synchronized (this.mLock) {
                    try {
                        if (this.zzefj.contains(zzavk.zzdml)) {
                            zzbat.zztu.post(new zzavv(this, zza(-2, zzavk.zzdml, zzavk.zzxk())));
                            return;
                        }
                    } catch (Throwable th4) {
                        throw th4;
                    }
                }
            } catch (InterruptedException ) {
                Thread.currentThread().interrupt();
                synchronized (this.mLock) {
                    try {
                        if (!TextUtils.isEmpty(zzavk.zzdml)) {
                            this.zzefi.add(zzavk.zzxj());
                        }
                    } catch (Throwable th5) {
                        throw th5;
                    }
                }
            } catch (Exception e2) {
                try {
                    zzaxz.zzc("Unable to resolve rewarded adapter.", e2);
                    synchronized (this.mLock) {
                        if (!TextUtils.isEmpty(zzavk.zzdml)) {
                            this.zzefi.add(zzavk.zzxj());
                        }
                    }
                } catch (Throwable th6) {
                    while (true) {
                        throw th6;
                    }
                }
            } catch (Throwable th7) {
                throw th7;
            }
        }
        zzbat.zztu.post(new zzavw(this, zza(3, null, null)));
    }

    public final void zzde(String str) {
        synchronized (this.mLock) {
            this.zzefj.add(str);
        }
    }

    private final zzaxf zza(int i, @Nullable String str, @Nullable zzakq zzakq) {
        boolean z;
        long j;
        String str2;
        zzwf zzwf;
        String str3;
        long j2;
        int i2;
        zzwb zzwb = this.zzdsk.zzeag.zzdwg;
        List<String> list = this.zzdsk.zzehy.zzdlq;
        List<String> list2 = this.zzdsk.zzehy.zzdlr;
        List<String> list3 = this.zzdsk.zzehy.zzdyf;
        int i3 = this.zzdsk.zzehy.orientation;
        long j3 = this.zzdsk.zzehy.zzdlx;
        String str4 = this.zzdsk.zzeag.zzdwj;
        boolean z2 = this.zzdsk.zzehy.zzdyd;
        zzakr zzakr = this.zzdsk.zzehj;
        long j4 = this.zzdsk.zzehy.zzdye;
        zzwf zzwf2 = this.zzdsk.zzbst;
        long j5 = j4;
        zzakr zzakr2 = zzakr;
        long j6 = this.zzdsk.zzehy.zzdyc;
        long j7 = this.zzdsk.zzehn;
        long j8 = this.zzdsk.zzehy.zzdyh;
        String str5 = this.zzdsk.zzehy.zzdyi;
        JSONObject jSONObject = this.zzdsk.zzehh;
        zzawd zzawd = this.zzdsk.zzehy.zzdyr;
        List<String> list4 = this.zzdsk.zzehy.zzdys;
        List<String> list5 = this.zzdsk.zzehy.zzdyt;
        boolean z3 = this.zzdsk.zzehy.zzdyu;
        zzaso zzaso = this.zzdsk.zzehy.zzdyv;
        JSONObject jSONObject2 = jSONObject;
        StringBuilder sb = new StringBuilder("");
        if (this.zzefi == null) {
            str3 = sb.toString();
            zzwf = zzwf2;
            z = z2;
            str2 = str5;
            j = j8;
        } else {
            Iterator it = this.zzefi.iterator();
            while (true) {
                int i4 = 1;
                zzwf = zzwf2;
                if (it.hasNext()) {
                    zzavn zzavn = (zzavn) it.next();
                    if (zzavn != null) {
                        Iterator it2 = it;
                        if (!TextUtils.isEmpty(zzavn.zzdkx)) {
                            String str6 = zzavn.zzdkx;
                            String str7 = str5;
                            switch (zzavn.errorCode) {
                                case 3:
                                    break;
                                case 4:
                                    i4 = 2;
                                    break;
                                case 5:
                                    i4 = 4;
                                    break;
                                case 6:
                                    j2 = j8;
                                    i2 = 0;
                                    break;
                                case 7:
                                    i4 = 3;
                                    break;
                                default:
                                    i4 = 6;
                                    break;
                            }
                            j2 = j8;
                            i2 = i4;
                            long j9 = zzavn.zzdng;
                            boolean z4 = z2;
                            StringBuilder sb2 = new StringBuilder(33 + String.valueOf(str6).length());
                            sb2.append(str6);
                            sb2.append(".");
                            sb2.append(i2);
                            sb2.append(".");
                            sb2.append(j9);
                            sb.append(String.valueOf(sb2.toString()).concat(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR));
                            zzwf2 = zzwf;
                            it = it2;
                            str5 = str7;
                            j8 = j2;
                            z2 = z4;
                        } else {
                            zzwf2 = zzwf;
                            it = it2;
                        }
                    } else {
                        zzwf2 = zzwf;
                    }
                } else {
                    z = z2;
                    str2 = str5;
                    j = j8;
                    str3 = sb.substring(0, Math.max(0, sb.length() - 1));
                }
            }
        }
        List<String> list6 = this.zzdsk.zzehy.zzdlu;
        String str8 = this.zzdsk.zzehy.zzdyy;
        zzum zzum = this.zzdsk.zzehw;
        boolean z5 = this.zzdsk.zzehy.zzbph;
        boolean z6 = this.zzdsk.zzehx;
        boolean z7 = this.zzdsk.zzehy.zzdzc;
        List<String> list7 = this.zzdsk.zzehy.zzdls;
        boolean z8 = this.zzdsk.zzehy.zzbpi;
        boolean z9 = z7;
        JSONObject jSONObject3 = jSONObject2;
        boolean z10 = z6;
        zzwf zzwf3 = zzwf;
        int i5 = i;
        boolean z11 = z5;
        boolean z12 = z;
        zzum zzum2 = zzum;
        String str9 = str2;
        zzakq zzakq2 = zzakq;
        zzakr zzakr3 = zzakr2;
        List<String> list8 = list6;
        String str10 = str;
        long j10 = j5;
        long j11 = j6;
        long j12 = j7;
        long j13 = j;
        List<String> list9 = list8;
        String str11 = str8;
        zzaxf zzaxf = new zzaxf(zzwb, null, list, i5, list2, list3, i3, j3, str4, z12, zzakq2, null, str10, zzakr3, null, j10, zzwf3, j11, j12, j13, str9, jSONObject3, null, zzawd, list4, list5, z3, zzaso, str3, list9, str11, zzum2, z11, z10, z9, list7, z8, this.zzdsk.zzehy.zzdzd, this.zzdsk.zzehy.zzdzf);
        return zzaxf;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzl(zzaxf zzaxf) {
        this.zzefk.zzxc().zzb(zzaxf);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzm(zzaxf zzaxf) {
        this.zzefk.zzxc().zzb(zzaxf);
    }
}
