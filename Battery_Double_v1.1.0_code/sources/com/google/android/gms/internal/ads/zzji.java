package com.google.android.gms.internal.ads;

import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.internal.ads.zzhp.zza;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.UUID;

public final class zzji implements zzhz {
    private static final zzic zzahq = new zzjj();
    private static final int zzaps = zzqe.zzam("seig");
    private static final byte[] zzapt = {-94, 57, 79, 82, 90, -101, 79, 20, -94, 68, 108, 66, 124, 100, -115, -12};
    private final int flags;
    private long zzaan;
    private final zzpx zzahx;
    private int zzajm;
    private int zzajn;
    private zzib zzajq;
    private final zzjs zzapu;
    private final SparseArray<zzjl> zzapv;
    private final zzpx zzapw;
    private final zzpx zzapx;
    private final zzpx zzapy;
    private final zzqb zzapz;
    private final zzpx zzaqa;
    private final byte[] zzaqb;
    private final Stack<zziw> zzaqc;
    private final LinkedList<zzjk> zzaqd;
    private int zzaqe;
    private int zzaqf;
    private long zzaqg;
    private int zzaqh;
    private zzpx zzaqi;
    private long zzaqj;
    private int zzaqk;
    private long zzaql;
    private zzjl zzaqm;
    private int zzaqn;
    private boolean zzaqo;
    private zzii zzaqp;
    private zzii[] zzaqq;
    private boolean zzaqr;

    public zzji() {
        this(0);
    }

    public final void release() {
    }

    public zzji(int i) {
        this(i, null);
    }

    private zzji(int i, zzqb zzqb) {
        this(i, null, null);
    }

    private zzji(int i, zzqb zzqb, zzjs zzjs) {
        this.flags = i;
        this.zzapz = null;
        this.zzapu = null;
        this.zzaqa = new zzpx(16);
        this.zzahx = new zzpx(zzpu.zzbhi);
        this.zzapw = new zzpx(5);
        this.zzapx = new zzpx();
        this.zzapy = new zzpx(1);
        this.zzaqb = new byte[16];
        this.zzaqc = new Stack<>();
        this.zzaqd = new LinkedList<>();
        this.zzapv = new SparseArray<>();
        this.zzaan = C.TIME_UNSET;
        this.zzaql = C.TIME_UNSET;
        zzei();
    }

    public final boolean zza(zzia zzia) throws IOException, InterruptedException {
        return zzjr.zzd(zzia);
    }

    public final void zza(zzib zzib) {
        this.zzajq = zzib;
    }

    public final void zzc(long j, long j2) {
        int size = this.zzapv.size();
        for (int i = 0; i < size; i++) {
            ((zzjl) this.zzapv.valueAt(i)).reset();
        }
        this.zzaqd.clear();
        this.zzaqk = 0;
        this.zzaqc.clear();
        zzei();
    }

    /* JADX WARNING: Removed duplicated region for block: B:256:0x0621 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x038c A[SYNTHETIC] */
    public final int zza(zzia zzia, zzif zzif) throws IOException, InterruptedException {
        zzji zzji;
        zzia zzia2;
        boolean z;
        long zzhh;
        long zzhh2;
        boolean z2;
        int i;
        zzjt zzjt;
        zzij zzij;
        zzjt zzjt2;
        int i2;
        zzjt zzjt3;
        int i3;
        zzji zzji2 = this;
        zzia zzia3 = zzia;
        while (true) {
            int i4 = 4;
            zzjl zzjl = null;
            switch (zzji2.zzaqe) {
                case 0:
                    zzji = zzji2;
                    zzia2 = zzia3;
                    if (zzji.zzaqh == 0) {
                        z = false;
                        if (zzia2.zza(zzji.zzaqa.data, 0, 8, true)) {
                            zzji.zzaqh = 8;
                            zzji.zzaqa.setPosition(0);
                            zzji.zzaqg = zzji.zzaqa.zzhd();
                            zzji.zzaqf = zzji.zzaqa.readInt();
                        }
                        if (!z) {
                            return -1;
                        }
                    }
                    if (zzji.zzaqg == 1) {
                        zzia2.readFully(zzji.zzaqa.data, 8, 8);
                        zzji.zzaqh += 8;
                        zzji.zzaqg = zzji.zzaqa.zzhh();
                    }
                    if (zzji.zzaqg < ((long) zzji.zzaqh)) {
                        throw new zzfx("Atom size less than header length (unsupported).");
                    }
                    long position = zzia.getPosition() - ((long) zzji.zzaqh);
                    if (zzji.zzaqf == zziv.zzamk) {
                        int size = zzji.zzapv.size();
                        for (int i5 = 0; i5 < size; i5++) {
                            zzju zzju = ((zzjl) zzji.zzapv.valueAt(i5)).zzaqt;
                            zzju.zzasu = position;
                            zzju.zzasw = position;
                            zzju.zzasv = position;
                        }
                    }
                    if (zzji.zzaqf == zziv.zzalh) {
                        zzji.zzaqm = null;
                        zzji.zzaqj = position + zzji.zzaqg;
                        if (!zzji.zzaqr) {
                            zzji.zzajq.zza(new zzih(zzji.zzaan));
                            zzji.zzaqr = true;
                        }
                        zzji.zzaqe = 2;
                    } else {
                        int i6 = zzji.zzaqf;
                        if (i6 == zziv.zzamb || i6 == zziv.zzamd || i6 == zziv.zzame || i6 == zziv.zzamf || i6 == zziv.zzamg || i6 == zziv.zzamk || i6 == zziv.zzaml || i6 == zziv.zzamm || i6 == zziv.zzamp) {
                            long position2 = (zzia.getPosition() + zzji.zzaqg) - 8;
                            zzji.zzaqc.add(new zziw(zzji.zzaqf, position2));
                            if (zzji.zzaqg == ((long) zzji.zzaqh)) {
                                zzji.zzt(position2);
                            } else {
                                zzei();
                            }
                        } else {
                            int i7 = zzji.zzaqf;
                            if (i7 == zziv.zzams || i7 == zziv.zzamr || i7 == zziv.zzamc || i7 == zziv.zzama || i7 == zziv.zzamt || i7 == zziv.zzalw || i7 == zziv.zzalx || i7 == zziv.zzamo || i7 == zziv.zzaly || i7 == zziv.zzalz || i7 == zziv.zzamu || i7 == zziv.zzanc || i7 == zziv.zzand || i7 == zziv.zzanh || i7 == zziv.zzang || i7 == zziv.zzane || i7 == zziv.zzanf || i7 == zziv.zzamq || i7 == zziv.zzamn || i7 == zziv.zzaog) {
                                if (zzji.zzaqh != 8) {
                                    throw new zzfx("Leaf atom defines extended atom size (unsupported).");
                                } else if (zzji.zzaqg > 2147483647L) {
                                    throw new zzfx("Leaf atom with length > 2147483647 (unsupported).");
                                } else {
                                    zzji.zzaqi = new zzpx((int) zzji.zzaqg);
                                    System.arraycopy(zzji.zzaqa.data, 0, zzji.zzaqi.data, 0, 8);
                                    zzji.zzaqe = 1;
                                }
                            } else if (zzji.zzaqg > 2147483647L) {
                                throw new zzfx("Skipping atom with length > 2147483647 (unsupported).");
                            } else {
                                zzji.zzaqi = null;
                                zzji.zzaqe = 1;
                            }
                        }
                    }
                    z = true;
                    if (!z) {
                    }
                    break;
                case 1:
                    int i8 = ((int) zzji2.zzaqg) - zzji2.zzaqh;
                    if (zzji2.zzaqi != null) {
                        zzia3.readFully(zzji2.zzaqi.data, 8, i8);
                        zzix zzix = new zzix(zzji2.zzaqf, zzji2.zzaqi);
                        long position3 = zzia.getPosition();
                        if (!zzji2.zzaqc.isEmpty()) {
                            ((zziw) zzji2.zzaqc.peek()).zza(zzix);
                            zzji = zzji2;
                            zzia2 = zzia3;
                        } else {
                            if (zzix.type == zziv.zzama) {
                                zzpx zzpx = zzix.zzaos;
                                zzpx.setPosition(8);
                                int zzaf = zziv.zzaf(zzpx.readInt());
                                zzpx.zzbl(4);
                                long zzhd = zzpx.zzhd();
                                if (zzaf == 0) {
                                    zzhh = zzpx.zzhd();
                                    zzhh2 = position3 + zzpx.zzhd();
                                } else {
                                    zzhh = zzpx.zzhh();
                                    zzhh2 = position3 + zzpx.zzhh();
                                }
                                long j = zzhh2;
                                long j2 = zzhh;
                                long zza = zzqe.zza(j2, (long) C.MICROS_PER_SECOND, zzhd);
                                zzpx.zzbl(2);
                                int readUnsignedShort = zzpx.readUnsignedShort();
                                int[] iArr = new int[readUnsignedShort];
                                long[] jArr = new long[readUnsignedShort];
                                long[] jArr2 = new long[readUnsignedShort];
                                long[] jArr3 = new long[readUnsignedShort];
                                long j3 = j2;
                                long j4 = zza;
                                int i9 = 0;
                                while (i9 < readUnsignedShort) {
                                    int readInt = zzpx.readInt();
                                    if ((Integer.MIN_VALUE & readInt) != 0) {
                                        throw new zzfx("Unhandled indirect reference");
                                    }
                                    long zzhd2 = zzpx.zzhd();
                                    iArr[i9] = Integer.MAX_VALUE & readInt;
                                    jArr[i9] = j;
                                    jArr3[i9] = j4;
                                    j3 += zzhd2;
                                    long[] jArr4 = jArr;
                                    long[] jArr5 = jArr2;
                                    long j5 = zza;
                                    j4 = zzqe.zza(j3, (long) C.MICROS_PER_SECOND, zzhd);
                                    jArr5[i9] = j4 - jArr3[i9];
                                    zzpx.zzbl(4);
                                    j += (long) iArr[i9];
                                    i9++;
                                    zza = j5;
                                    jArr = jArr4;
                                    jArr2 = jArr5;
                                    zzia zzia4 = zzia;
                                }
                                Pair create = Pair.create(Long.valueOf(zza), new zzhw(iArr, jArr, jArr2, jArr3));
                                zzji = this;
                                zzji.zzaql = ((Long) create.first).longValue();
                                zzji.zzajq.zza((zzig) create.second);
                                zzji.zzaqr = true;
                            } else {
                                zzji = zzji2;
                                if (zzix.type == zziv.zzaog) {
                                    zzpx zzpx2 = zzix.zzaos;
                                    if (zzji.zzaqp != null) {
                                        zzpx2.setPosition(12);
                                        zzpx2.zzhi();
                                        zzpx2.zzhi();
                                        long zza2 = zzqe.zza(zzpx2.zzhd(), (long) C.MICROS_PER_SECOND, zzpx2.zzhd());
                                        zzpx2.setPosition(12);
                                        int zzhb = zzpx2.zzhb();
                                        zzji.zzaqp.zza(zzpx2, zzhb);
                                        if (zzji.zzaql != C.TIME_UNSET) {
                                            zzji.zzaqp.zza(zzji.zzaql + zza2, 1, zzhb, 0, null);
                                        } else {
                                            zzji.zzaqd.addLast(new zzjk(zza2, zzhb));
                                            zzji.zzaqk += zzhb;
                                        }
                                    }
                                }
                            }
                            zzia2 = zzia;
                        }
                    } else {
                        zzji = zzji2;
                        zzia2 = zzia3;
                        zzia2.zzw(i8);
                    }
                    zzji.zzt(zzia.getPosition());
                    break;
                case 2:
                    int size2 = zzji2.zzapv.size();
                    long j6 = Long.MAX_VALUE;
                    for (int i10 = 0; i10 < size2; i10++) {
                        zzju zzju2 = ((zzjl) zzji2.zzapv.valueAt(i10)).zzaqt;
                        if (zzju2.zzatj && zzju2.zzasw < j6) {
                            j6 = zzju2.zzasw;
                            zzjl = (zzjl) zzji2.zzapv.valueAt(i10);
                        }
                    }
                    if (zzjl != null) {
                        int position4 = (int) (j6 - zzia.getPosition());
                        if (position4 >= 0) {
                            zzia3.zzw(position4);
                            zzju zzju3 = zzjl.zzaqt;
                            zzia3.readFully(zzju3.zzati.data, 0, zzju3.zzath);
                            zzju3.zzati.setPosition(0);
                            zzju3.zzatj = false;
                            break;
                        } else {
                            throw new zzfx("Offset to encryption data was negative.");
                        }
                    } else {
                        zzji2.zzaqe = 3;
                        continue;
                    }
                default:
                    zzji = zzji2;
                    zzia2 = zzia3;
                    if (zzji.zzaqe == 3) {
                        if (zzji.zzaqm == null) {
                            SparseArray<zzjl> sparseArray = zzji.zzapv;
                            int size3 = sparseArray.size();
                            long j7 = Long.MAX_VALUE;
                            zzjl zzjl2 = null;
                            for (int i11 = 0; i11 < size3; i11++) {
                                zzjl zzjl3 = (zzjl) sparseArray.valueAt(i11);
                                if (zzjl3.zzaqy != zzjl3.zzaqt.zzasx) {
                                    long j8 = zzjl3.zzaqt.zzasy[zzjl3.zzaqy];
                                    if (j8 < j7) {
                                        zzjl2 = zzjl3;
                                        j7 = j8;
                                    }
                                }
                            }
                            if (zzjl2 == null) {
                                int position5 = (int) (zzji.zzaqj - zzia.getPosition());
                                if (position5 < 0) {
                                    throw new zzfx("Offset to end of mdat was negative.");
                                }
                                zzia2.zzw(position5);
                                zzei();
                                i = 0;
                                z2 = false;
                                if (z2) {
                                    return i;
                                }
                            } else {
                                int position6 = (int) (zzjl2.zzaqt.zzasy[zzjl2.zzaqy] - zzia.getPosition());
                                if (position6 < 0) {
                                    Log.w("FragmentedMp4Extractor", "Ignoring negative offset to sample data.");
                                    position6 = 0;
                                }
                                zzia2.zzw(position6);
                                zzji.zzaqm = zzjl2;
                            }
                        }
                        zzji.zzaqn = zzji.zzaqm.zzaqt.zzata[zzji.zzaqm.zzaqw];
                        if (zzji.zzaqm.zzaqt.zzate) {
                            zzjl zzjl4 = zzji.zzaqm;
                            zzju zzju4 = zzjl4.zzaqt;
                            zzpx zzpx3 = zzju4.zzati;
                            int i12 = zzju4.zzast.zzapo;
                            if (zzju4.zzatg != null) {
                                zzjt3 = zzju4.zzatg;
                            } else {
                                zzjt3 = zzjl4.zzaqu.zzasn[i12];
                            }
                            int i13 = zzjt3.zzasr;
                            boolean z3 = zzju4.zzatf[zzjl4.zzaqw];
                            zzji.zzapy.data[0] = (byte) ((z3 ? 128 : 0) | i13);
                            zzji.zzapy.setPosition(0);
                            zzii zzii = zzjl4.zzakw;
                            zzii.zza(zzji.zzapy, 1);
                            zzii.zza(zzpx3, i13);
                            if (!z3) {
                                i3 = i13 + 1;
                            } else {
                                int readUnsignedShort2 = zzpx3.readUnsignedShort();
                                zzpx3.zzbl(-2);
                                int i14 = (readUnsignedShort2 * 6) + 2;
                                zzii.zza(zzpx3, i14);
                                i3 = i13 + 1 + i14;
                            }
                            zzji.zzajn = i3;
                            zzji.zzaqn += zzji.zzajn;
                        } else {
                            zzji.zzajn = 0;
                        }
                        if (zzji.zzaqm.zzaqu.zzasm == 1) {
                            zzji.zzaqn -= 8;
                            zzia2.zzw(8);
                        }
                        zzji.zzaqe = 4;
                        zzji.zzajm = 0;
                    }
                    zzju zzju5 = zzji.zzaqm.zzaqt;
                    zzjs zzjs = zzji.zzaqm.zzaqu;
                    zzii zzii2 = zzji.zzaqm.zzakw;
                    int i15 = zzji.zzaqm.zzaqw;
                    if (zzjs.zzakx != 0) {
                        byte[] bArr = zzji.zzapw.data;
                        bArr[0] = 0;
                        bArr[1] = 0;
                        bArr[2] = 0;
                        int i16 = zzjs.zzakx + 1;
                        int i17 = 4 - zzjs.zzakx;
                        while (zzji.zzajn < zzji.zzaqn) {
                            if (zzji.zzajm == 0) {
                                zzia2.readFully(bArr, i17, i16);
                                zzji.zzapw.setPosition(0);
                                zzji.zzajm = zzji.zzapw.zzhg() - 1;
                                zzji.zzahx.setPosition(0);
                                zzii2.zza(zzji.zzahx, i4);
                                zzii2.zza(zzji.zzapw, 1);
                                zzji.zzaqo = zzji.zzaqq != null && zzpu.zza(zzjs.zzaad.zzzj, bArr[i4]);
                                zzji.zzajn += 5;
                                zzji.zzaqn += i17;
                            } else {
                                if (zzji.zzaqo) {
                                    zzji.zzapx.reset(zzji.zzajm);
                                    zzia2.readFully(zzji.zzapx.data, 0, zzji.zzajm);
                                    zzii2.zza(zzji.zzapx, zzji.zzajm);
                                    i2 = zzji.zzajm;
                                    int zzb = zzpu.zzb(zzji.zzapx.data, zzji.zzapx.limit());
                                    zzji.zzapx.setPosition(MimeTypes.VIDEO_H265.equals(zzjs.zzaad.zzzj) ? 1 : 0);
                                    zzji.zzapx.zzbk(zzb);
                                    zzoc.zza(zzju5.zzal(i15) * 1000, zzji.zzapx, zzji.zzaqq);
                                } else {
                                    i2 = zzii2.zza(zzia2, zzji.zzajm, false);
                                }
                                zzji.zzajn += i2;
                                zzji.zzajm -= i2;
                                i4 = 4;
                            }
                        }
                    } else {
                        while (zzji.zzajn < zzji.zzaqn) {
                            zzji.zzajn += zzii2.zza(zzia2, zzji.zzaqn - zzji.zzajn, false);
                        }
                    }
                    long zzal = zzju5.zzal(i15) * 1000;
                    if (zzji.zzapz != null) {
                        throw new NoSuchMethodError();
                    }
                    boolean z4 = (zzju5.zzate ? (char) 0 : 0) | zzju5.zzatd[i15];
                    if (zzju5.zzate) {
                        if (zzju5.zzatg != null) {
                            zzjt2 = zzju5.zzatg;
                        } else {
                            zzjt2 = zzjs.zzasn[zzju5.zzast.zzapo];
                        }
                        zzjt = zzjt2;
                        zzij = zzjt != zzji.zzaqm.zzara ? new zzij(1, zzjt.zzass) : zzji.zzaqm.zzaqz;
                    } else {
                        zzij = null;
                        zzjt = null;
                    }
                    zzji.zzaqm.zzaqz = zzij;
                    zzji.zzaqm.zzara = zzjt;
                    zzii2.zza(zzal, z4 ? 1 : 0, zzji.zzaqn, 0, zzij);
                    while (!zzji.zzaqd.isEmpty()) {
                        zzjk zzjk = (zzjk) zzji.zzaqd.removeFirst();
                        zzji.zzaqk -= zzjk.size;
                        zzji.zzaqp.zza(zzal + zzjk.zzaqs, 1, zzjk.size, zzji.zzaqk, null);
                    }
                    zzji.zzaqm.zzaqw++;
                    zzji.zzaqm.zzaqx++;
                    if (zzji.zzaqm.zzaqx == zzju5.zzasz[zzji.zzaqm.zzaqy]) {
                        zzji.zzaqm.zzaqy++;
                        i = 0;
                        zzji.zzaqm.zzaqx = 0;
                        zzji.zzaqm = null;
                    } else {
                        i = 0;
                    }
                    zzji.zzaqe = 3;
                    z2 = true;
                    if (z2) {
                    }
                    break;
            }
            zzia3 = zzia2;
            zzji2 = zzji;
        }
    }

    private final void zzei() {
        this.zzaqe = 0;
        this.zzaqh = 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:161:0x03f2  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x03f9  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0403  */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x0625  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x0653  */
    /* JADX WARNING: Removed duplicated region for block: B:293:0x061d A[SYNTHETIC] */
    private final void zzt(long j) throws zzfx {
        zzji zzji;
        int i;
        int i2;
        zziw zziw;
        SparseArray<zzjl> sparseArray;
        int i3;
        byte[] bArr;
        int i4;
        zzju zzju;
        int size;
        int i5;
        byte[] bArr2;
        int i6;
        int i7;
        int i8;
        zzjl zzjl;
        zziw zziw2;
        int i9;
        zziw zziw3;
        zzju zzju2;
        byte[] bArr3;
        int i10;
        List<zzix> list;
        int i11;
        long j2;
        int i12;
        int[] iArr;
        long j3;
        long j4;
        int i13;
        int i14;
        int i15;
        int i16;
        boolean z;
        boolean z2;
        boolean z3;
        SparseArray sparseArray2;
        zzji zzji2 = this;
        while (!zzji2.zzaqc.isEmpty() && ((zziw) zzji2.zzaqc.peek()).zzaop == j) {
            zziw zziw4 = (zziw) zzji2.zzaqc.pop();
            int i17 = 12;
            int i18 = 8;
            int i19 = 1;
            if (zziw4.type == zziv.zzamb) {
                zzpo.checkState(true, "Unexpected moov box.");
                zzhp zzb = zzb(zziw4.zzaoq);
                zziw zzaj = zziw4.zzaj(zziv.zzamm);
                SparseArray sparseArray3 = new SparseArray();
                int size2 = zzaj.zzaoq.size();
                long j5 = -9223372036854775807L;
                int i20 = 0;
                while (i20 < size2) {
                    zzix zzix = (zzix) zzaj.zzaoq.get(i20);
                    if (zzix.type == zziv.zzaly) {
                        zzpx zzpx = zzix.zzaos;
                        zzpx.setPosition(i17);
                        Pair create = Pair.create(Integer.valueOf(zzpx.readInt()), new zzjf(zzpx.zzhg() - i19, zzpx.zzhg(), zzpx.zzhg(), zzpx.readInt()));
                        sparseArray3.put(((Integer) create.first).intValue(), (zzjf) create.second);
                    } else if (zzix.type == zziv.zzamn) {
                        zzpx zzpx2 = zzix.zzaos;
                        zzpx2.setPosition(8);
                        j5 = zziv.zzaf(zzpx2.readInt()) == 0 ? zzpx2.zzhd() : zzpx2.zzhh();
                    }
                    i20++;
                    i17 = 12;
                    i19 = 1;
                }
                SparseArray sparseArray4 = new SparseArray();
                int size3 = zziw4.zzaor.size();
                int i21 = 0;
                while (i21 < size3) {
                    zziw zziw5 = (zziw) zziw4.zzaor.get(i21);
                    if (zziw5.type == zziv.zzamd) {
                        sparseArray2 = sparseArray3;
                        zzjs zza = zziy.zza(zziw5, zziw4.zzai(zziv.zzamc), j5, zzb, false);
                        if (zza != null) {
                            sparseArray4.put(zza.id, zza);
                        }
                    } else {
                        sparseArray2 = sparseArray3;
                    }
                    i21++;
                    sparseArray3 = sparseArray2;
                }
                SparseArray sparseArray5 = sparseArray3;
                int size4 = sparseArray4.size();
                if (zzji2.zzapv.size() == 0) {
                    for (int i22 = 0; i22 < size4; i22++) {
                        zzjs zzjs = (zzjs) sparseArray4.valueAt(i22);
                        zzjl zzjl2 = new zzjl(zzji2.zzajq.zzb(i22, zzjs.type));
                        zzjl2.zza(zzjs, (zzjf) sparseArray5.get(zzjs.id));
                        zzji2.zzapv.put(zzjs.id, zzjl2);
                        zzji2.zzaan = Math.max(zzji2.zzaan, zzjs.zzaan);
                    }
                    if ((zzji2.flags & 4) != 0 && zzji2.zzaqp == null) {
                        zzji2.zzaqp = zzji2.zzajq.zzb(zzji2.zzapv.size(), 4);
                        zzji2.zzaqp.zzf(zzfs.zza((String) null, MimeTypes.APPLICATION_EMSG, Long.MAX_VALUE));
                    }
                    if ((zzji2.flags & 8) != 0 && zzji2.zzaqq == null) {
                        zzii zzb2 = zzji2.zzajq.zzb(zzji2.zzapv.size() + 1, 3);
                        zzb2.zzf(zzfs.zza((String) null, MimeTypes.APPLICATION_CEA608, (String) null, -1, 0, (String) null, (zzhp) null));
                        zzji2.zzaqq = new zzii[]{zzb2};
                    }
                    zzji2.zzajq.zzdy();
                } else {
                    zzpo.checkState(zzji2.zzapv.size() == size4);
                    for (int i23 = 0; i23 < size4; i23++) {
                        zzjs zzjs2 = (zzjs) sparseArray4.valueAt(i23);
                        ((zzjl) zzji2.zzapv.get(zzjs2.id)).zza(zzjs2, (zzjf) sparseArray5.get(zzjs2.id));
                    }
                }
            } else {
                zzjl zzjl3 = null;
                if (zziw4.type == zziv.zzamk) {
                    SparseArray<zzjl> sparseArray6 = zzji2.zzapv;
                    int i24 = zzji2.flags;
                    byte[] bArr4 = zzji2.zzaqb;
                    int size5 = zziw4.zzaor.size();
                    int i25 = 0;
                    while (i25 < size5) {
                        zziw zziw6 = (zziw) zziw4.zzaor.get(i25);
                        if (zziw6.type == zziv.zzaml) {
                            zzpx zzpx3 = zziw6.zzai(zziv.zzalx).zzaos;
                            zzpx3.setPosition(i18);
                            int zzag = zziv.zzag(zzpx3.readInt());
                            int readInt = zzpx3.readInt();
                            if ((i24 & 16) != 0) {
                                readInt = 0;
                            }
                            zzjl zzjl4 = (zzjl) sparseArray6.get(readInt);
                            if (zzjl4 == null) {
                                zzjl4 = zzjl3;
                                i3 = size5;
                            } else {
                                if ((zzag & 1) != 0) {
                                    i3 = size5;
                                    long zzhh = zzpx3.zzhh();
                                    zzjl4.zzaqt.zzasv = zzhh;
                                    zzjl4.zzaqt.zzasw = zzhh;
                                } else {
                                    i3 = size5;
                                }
                                zzjf zzjf = zzjl4.zzaqv;
                                zzjl4.zzaqt.zzast = new zzjf((zzag & 2) != 0 ? zzpx3.zzhg() - 1 : zzjf.zzapo, (zzag & 8) != 0 ? zzpx3.zzhg() : zzjf.duration, (zzag & 16) != 0 ? zzpx3.zzhg() : zzjf.size, (zzag & 32) != 0 ? zzpx3.zzhg() : zzjf.flags);
                            }
                            if (zzjl4 != null) {
                                zzju zzju3 = zzjl4.zzaqt;
                                long j6 = zzju3.zzatk;
                                zzjl4.reset();
                                if (zziw6.zzai(zziv.zzalw) != null && (i24 & 2) == 0) {
                                    zzpx zzpx4 = zziw6.zzai(zziv.zzalw).zzaos;
                                    zzpx4.setPosition(8);
                                    j6 = zziv.zzaf(zzpx4.readInt()) == 1 ? zzpx4.zzhh() : zzpx4.zzhd();
                                }
                                List<zzix> list2 = zziw6.zzaoq;
                                int size6 = list2.size();
                                sparseArray = sparseArray6;
                                int i26 = 0;
                                int i27 = 0;
                                int i28 = 0;
                                while (i28 < size6) {
                                    zzix zzix2 = (zzix) list2.get(i28);
                                    long j7 = j6;
                                    if (zzix2.type == zziv.zzalz) {
                                        zzpx zzpx5 = zzix2.zzaos;
                                        zzpx5.setPosition(12);
                                        int zzhg = zzpx5.zzhg();
                                        if (zzhg > 0) {
                                            i27 += zzhg;
                                            i26++;
                                        }
                                    }
                                    i28++;
                                    j6 = j7;
                                }
                                long j8 = j6;
                                zzjl4.zzaqy = 0;
                                zzjl4.zzaqx = 0;
                                zzjl4.zzaqw = 0;
                                zzju zzju4 = zzjl4.zzaqt;
                                zzju4.zzasx = i26;
                                zzju4.zzapk = i27;
                                if (zzju4.zzasz == null || zzju4.zzasz.length < i26) {
                                    zzju4.zzasy = new long[i26];
                                    zzju4.zzasz = new int[i26];
                                }
                                if (zzju4.zzata == null || zzju4.zzata.length < i27) {
                                    int i29 = (i27 * 125) / 100;
                                    zzju4.zzata = new int[i29];
                                    zzju4.zzatb = new int[i29];
                                    zzju4.zzatc = new long[i29];
                                    zzju4.zzatd = new boolean[i29];
                                    zzju4.zzatf = new boolean[i29];
                                }
                                int i30 = 0;
                                int i31 = 0;
                                int i32 = 0;
                                while (i30 < size6) {
                                    zzix zzix3 = (zzix) list2.get(i30);
                                    if (zzix3.type == zziv.zzalz) {
                                        int i33 = i31 + 1;
                                        zzpx zzpx6 = zzix3.zzaos;
                                        zzpx6.setPosition(8);
                                        int zzag2 = zziv.zzag(zzpx6.readInt());
                                        int i34 = i33;
                                        zzjs zzjs3 = zzjl4.zzaqu;
                                        list = list2;
                                        zzju zzju5 = zzjl4.zzaqt;
                                        i10 = size6;
                                        zzjf zzjf2 = zzju5.zzast;
                                        zzju5.zzasz[i31] = zzpx6.zzhg();
                                        bArr3 = bArr4;
                                        zzju2 = zzju3;
                                        zzju5.zzasy[i31] = zzju5.zzasv;
                                        if ((zzag2 & 1) != 0) {
                                            long[] jArr = zzju5.zzasy;
                                            zziw3 = zziw4;
                                            i9 = i25;
                                            zziw2 = zziw6;
                                            jArr[i31] = jArr[i31] + ((long) zzpx6.readInt());
                                        } else {
                                            zziw3 = zziw4;
                                            i9 = i25;
                                            zziw2 = zziw6;
                                        }
                                        boolean z4 = (zzag2 & 4) != 0;
                                        int i35 = zzjf2.flags;
                                        if (z4) {
                                            i35 = zzpx6.zzhg();
                                        }
                                        boolean z5 = (zzag2 & 256) != 0;
                                        boolean z6 = (zzag2 & 512) != 0;
                                        boolean z7 = (zzag2 & 1024) != 0;
                                        boolean z8 = (zzag2 & 2048) != 0;
                                        if (zzjs3.zzaso != null) {
                                            i11 = i35;
                                            if (zzjs3.zzaso.length == 1 && zzjs3.zzaso[0] == 0) {
                                                zzjl = zzjl4;
                                                j2 = zzqe.zza(zzjs3.zzasp[0], 1000, zzjs3.zzcr);
                                                int[] iArr2 = zzju5.zzata;
                                                int[] iArr3 = zzju5.zzatb;
                                                i8 = i30;
                                                long[] jArr2 = zzju5.zzatc;
                                                int[] iArr4 = iArr2;
                                                boolean[] zArr = zzju5.zzatd;
                                                long[] jArr3 = jArr2;
                                                boolean z9 = (zzjs3.type == 2 || (i24 & 1) == 0) ? false : true;
                                                i12 = zzju5.zzasz[i31] + i32;
                                                int i36 = i32;
                                                i7 = i24;
                                                long j9 = zzjs3.zzcr;
                                                if (i31 <= 0) {
                                                    iArr = iArr3;
                                                    j3 = j2;
                                                    j4 = zzju5.zzatk;
                                                } else {
                                                    iArr = iArr3;
                                                    j3 = j2;
                                                    j4 = j8;
                                                }
                                                i13 = i36;
                                                while (i13 < i12) {
                                                    if (z5) {
                                                        i14 = zzpx6.zzhg();
                                                    } else {
                                                        i14 = zzjf2.duration;
                                                    }
                                                    if (z6) {
                                                        i16 = zzpx6.zzhg();
                                                        i15 = i12;
                                                    } else {
                                                        i15 = i12;
                                                        i16 = zzjf2.size;
                                                    }
                                                    int i37 = (i13 != 0 || !z4) ? z7 ? zzpx6.readInt() : zzjf2.flags : i11;
                                                    if (z8) {
                                                        z3 = z4;
                                                        z2 = z5;
                                                        z = z6;
                                                        iArr[i13] = (int) (((long) (zzpx6.readInt() * 1000)) / j9);
                                                    } else {
                                                        z3 = z4;
                                                        z2 = z5;
                                                        z = z6;
                                                        iArr[i13] = 0;
                                                    }
                                                    jArr3[i13] = zzqe.zza(j4, 1000, j9) - j3;
                                                    iArr4[i13] = i16;
                                                    zArr[i13] = ((i37 >> 16) & 1) == 0 && (!z9 || i13 == 0);
                                                    j4 += (long) i14;
                                                    i13++;
                                                    i12 = i15;
                                                    z4 = z3;
                                                    z5 = z2;
                                                    z6 = z;
                                                }
                                                int i38 = i12;
                                                zzju5.zzatk = j4;
                                                i31 = i34;
                                                i32 = i38;
                                            }
                                        } else {
                                            i11 = i35;
                                        }
                                        zzjl = zzjl4;
                                        j2 = 0;
                                        int[] iArr22 = zzju5.zzata;
                                        int[] iArr32 = zzju5.zzatb;
                                        i8 = i30;
                                        long[] jArr22 = zzju5.zzatc;
                                        int[] iArr42 = iArr22;
                                        boolean[] zArr2 = zzju5.zzatd;
                                        long[] jArr32 = jArr22;
                                        if (zzjs3.type == 2) {
                                        }
                                        i12 = zzju5.zzasz[i31] + i32;
                                        int i362 = i32;
                                        i7 = i24;
                                        long j92 = zzjs3.zzcr;
                                        if (i31 <= 0) {
                                        }
                                        i13 = i362;
                                        while (i13 < i12) {
                                        }
                                        int i382 = i12;
                                        zzju5.zzatk = j4;
                                        i31 = i34;
                                        i32 = i382;
                                    } else {
                                        zziw3 = zziw4;
                                        i8 = i30;
                                        int i39 = i32;
                                        i7 = i24;
                                        bArr3 = bArr4;
                                        zzju2 = zzju3;
                                        i9 = i25;
                                        zziw2 = zziw6;
                                        list = list2;
                                        i10 = size6;
                                        zzjl = zzjl4;
                                    }
                                    i30 = i8 + 1;
                                    list2 = list;
                                    size6 = i10;
                                    bArr4 = bArr3;
                                    zzju3 = zzju2;
                                    zziw4 = zziw3;
                                    i25 = i9;
                                    zziw6 = zziw2;
                                    zzjl4 = zzjl;
                                    i24 = i7;
                                }
                                zziw = zziw4;
                                i = i24;
                                byte[] bArr5 = bArr4;
                                zzju zzju6 = zzju3;
                                i2 = i25;
                                zziw zziw7 = zziw6;
                                zzjl zzjl5 = zzjl4;
                                zzix zzai = zziw6.zzai(zziv.zzanc);
                                if (zzai != null) {
                                    zzju = zzju6;
                                    zzjt zzjt = zzjl5.zzaqu.zzasn[zzju.zzast.zzapo];
                                    zzpx zzpx7 = zzai.zzaos;
                                    int i40 = zzjt.zzasr;
                                    zzpx7.setPosition(8);
                                    if ((zziv.zzag(zzpx7.readInt()) & 1) == 1) {
                                        zzpx7.zzbl(8);
                                    }
                                    int readUnsignedByte = zzpx7.readUnsignedByte();
                                    int zzhg2 = zzpx7.zzhg();
                                    if (zzhg2 != zzju.zzapk) {
                                        int i41 = zzju.zzapk;
                                        StringBuilder sb = new StringBuilder(41);
                                        sb.append("Length mismatch: ");
                                        sb.append(zzhg2);
                                        sb.append(", ");
                                        sb.append(i41);
                                        throw new zzfx(sb.toString());
                                    }
                                    if (readUnsignedByte == 0) {
                                        boolean[] zArr3 = zzju.zzatf;
                                        i6 = 0;
                                        for (int i42 = 0; i42 < zzhg2; i42++) {
                                            int readUnsignedByte2 = zzpx7.readUnsignedByte();
                                            i6 += readUnsignedByte2;
                                            zArr3[i42] = readUnsignedByte2 > i40;
                                        }
                                    } else {
                                        i6 = 0 + (readUnsignedByte * zzhg2);
                                        Arrays.fill(zzju.zzatf, 0, zzhg2, readUnsignedByte > i40);
                                    }
                                    zzju.zzak(i6);
                                } else {
                                    zzju = zzju6;
                                }
                                zzix zzai2 = zziw6.zzai(zziv.zzand);
                                if (zzai2 != null) {
                                    zzpx zzpx8 = zzai2.zzaos;
                                    zzpx8.setPosition(8);
                                    int readInt2 = zzpx8.readInt();
                                    if ((zziv.zzag(readInt2) & 1) == 1) {
                                        zzpx8.zzbl(8);
                                    }
                                    int zzhg3 = zzpx8.zzhg();
                                    if (zzhg3 != 1) {
                                        StringBuilder sb2 = new StringBuilder(40);
                                        sb2.append("Unexpected saio entry count: ");
                                        sb2.append(zzhg3);
                                        throw new zzfx(sb2.toString());
                                    }
                                    zzju.zzasw += zziv.zzaf(readInt2) == 0 ? zzpx8.zzhd() : zzpx8.zzhh();
                                }
                                zzix zzai3 = zziw6.zzai(zziv.zzanh);
                                if (zzai3 != null) {
                                    zza(zzai3.zzaos, 0, zzju);
                                }
                                zzix zzai4 = zziw6.zzai(zziv.zzane);
                                zzix zzai5 = zziw6.zzai(zziv.zzanf);
                                if (!(zzai4 == null || zzai5 == null)) {
                                    zzpx zzpx9 = zzai4.zzaos;
                                    zzpx zzpx10 = zzai5.zzaos;
                                    zzpx9.setPosition(8);
                                    int readInt3 = zzpx9.readInt();
                                    if (zzpx9.readInt() == zzaps) {
                                        if (zziv.zzaf(readInt3) == 1) {
                                            zzpx9.zzbl(4);
                                        }
                                        if (zzpx9.readInt() != 1) {
                                            throw new zzfx("Entry count in sbgp != 1 (unsupported).");
                                        }
                                        zzpx10.setPosition(8);
                                        int readInt4 = zzpx10.readInt();
                                        if (zzpx10.readInt() == zzaps) {
                                            int zzaf = zziv.zzaf(readInt4);
                                            if (zzaf == 1) {
                                                if (zzpx10.zzhd() == 0) {
                                                    throw new zzfx("Variable length decription in sgpd found (unsupported)");
                                                }
                                            } else if (zzaf >= 2) {
                                                zzpx10.zzbl(4);
                                                if (zzpx10.zzhd() == 1) {
                                                    throw new zzfx("Entry count in sgpd != 1 (unsupported).");
                                                }
                                                zzpx10.zzbl(2);
                                                if (zzpx10.readUnsignedByte() == 1) {
                                                    int readUnsignedByte3 = zzpx10.readUnsignedByte();
                                                    byte[] bArr6 = new byte[16];
                                                    zzpx10.zze(bArr6, 0, 16);
                                                    zzju.zzate = true;
                                                    zzju.zzatg = new zzjt(true, readUnsignedByte3, bArr6);
                                                }
                                            }
                                            if (zzpx10.zzhd() == 1) {
                                            }
                                        }
                                        size = zziw6.zzaoq.size();
                                        i5 = 0;
                                        while (i5 < size) {
                                            zzix zzix4 = (zzix) zziw6.zzaoq.get(i5);
                                            if (zzix4.type == zziv.zzang) {
                                                zzpx zzpx11 = zzix4.zzaos;
                                                zzpx11.setPosition(8);
                                                bArr2 = bArr5;
                                                zzpx11.zze(bArr2, 0, 16);
                                                if (Arrays.equals(bArr2, zzapt)) {
                                                    zza(zzpx11, 16, zzju);
                                                }
                                            } else {
                                                bArr2 = bArr5;
                                            }
                                            i5++;
                                            bArr5 = bArr2;
                                        }
                                        bArr = bArr5;
                                    }
                                }
                                size = zziw6.zzaoq.size();
                                i5 = 0;
                                while (i5 < size) {
                                }
                                bArr = bArr5;
                            } else {
                                zziw = zziw4;
                                sparseArray = sparseArray6;
                                i = i24;
                                bArr = bArr4;
                                i2 = i25;
                            }
                            i4 = 8;
                        } else {
                            zziw = zziw4;
                            sparseArray = sparseArray6;
                            i = i24;
                            bArr = bArr4;
                            i3 = size5;
                            i4 = i18;
                            i2 = i25;
                        }
                        i25 = i2 + 1;
                        i18 = i4;
                        bArr4 = bArr;
                        size5 = i3;
                        sparseArray6 = sparseArray;
                        zziw4 = zziw;
                        i24 = i;
                        zzjl3 = null;
                    }
                    zzhp zzb3 = zzb(zziw4.zzaoq);
                    if (zzb3 != null) {
                        zzji = this;
                        int size7 = zzji.zzapv.size();
                        for (int i43 = 0; i43 < size7; i43++) {
                            zzjl zzjl6 = (zzjl) zzji.zzapv.valueAt(i43);
                            zzjl6.zzakw.zzf(zzjl6.zzaqu.zzaad.zza(zzb3));
                        }
                    } else {
                        zzji = this;
                    }
                } else {
                    zzji = zzji2;
                    if (!zzji.zzaqc.isEmpty()) {
                        ((zziw) zzji.zzaqc.peek()).zza(zziw4);
                    }
                }
                zzji2 = zzji;
            }
        }
        zzji zzji3 = zzji2;
        zzei();
    }

    private static void zza(zzpx zzpx, int i, zzju zzju) throws zzfx {
        zzpx.setPosition(i + 8);
        int zzag = zziv.zzag(zzpx.readInt());
        if ((zzag & 1) != 0) {
            throw new zzfx("Overriding TrackEncryptionBox parameters is unsupported.");
        }
        boolean z = (zzag & 2) != 0;
        int zzhg = zzpx.zzhg();
        if (zzhg != zzju.zzapk) {
            int i2 = zzju.zzapk;
            StringBuilder sb = new StringBuilder(41);
            sb.append("Length mismatch: ");
            sb.append(zzhg);
            sb.append(", ");
            sb.append(i2);
            throw new zzfx(sb.toString());
        }
        Arrays.fill(zzju.zzatf, 0, zzhg, z);
        zzju.zzak(zzpx.zzhb());
        zzpx.zze(zzju.zzati.data, 0, zzju.zzath);
        zzju.zzati.setPosition(0);
        zzju.zzatj = false;
    }

    private static zzhp zzb(List<zzix> list) {
        int size = list.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            zzix zzix = (zzix) list.get(i);
            if (zzix.type == zziv.zzamu) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                byte[] bArr = zzix.zzaos.data;
                UUID zze = zzjq.zze(bArr);
                if (zze == null) {
                    Log.w("FragmentedMp4Extractor", "Skipped pssh atom (failed to extract uuid)");
                } else {
                    arrayList.add(new zza(zze, MimeTypes.VIDEO_MP4, bArr));
                }
            }
        }
        if (arrayList == null) {
            return null;
        }
        return new zzhp((List<zza>) arrayList);
    }
}
