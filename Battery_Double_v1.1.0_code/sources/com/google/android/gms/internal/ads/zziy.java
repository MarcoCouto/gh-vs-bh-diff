package com.google.android.gms.internal.ads;

import android.util.Log;
import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.internal.ads.zzki.zza;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class zziy {
    private static final int zzaob = zzqe.zzam("meta");
    private static final int zzaot = zzqe.zzam("vide");
    private static final int zzaou = zzqe.zzam("soun");
    private static final int zzaov = zzqe.zzam(MimeTypes.BASE_TYPE_TEXT);
    private static final int zzaow = zzqe.zzam("sbtl");
    private static final int zzaox = zzqe.zzam("subt");
    private static final int zzaoy = zzqe.zzam("clcp");
    private static final int zzaoz = zzqe.zzam("cenc");

    /* JADX WARNING: Removed duplicated region for block: B:193:0x03b6  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x03c0  */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x03c6  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x03c9  */
    /* JADX WARNING: Removed duplicated region for block: B:233:0x041e  */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x0583  */
    /* JADX WARNING: Removed duplicated region for block: B:377:0x0774  */
    /* JADX WARNING: Removed duplicated region for block: B:378:0x07ae  */
    /* JADX WARNING: Removed duplicated region for block: B:382:0x07f0  */
    /* JADX WARNING: Removed duplicated region for block: B:403:0x0855 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:404:0x0856  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0147  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01b0  */
    public static zzjs zza(zziw zziw, zzix zzix, long j, zzhp zzhp, boolean z) throws zzfx {
        boolean z2;
        long j2;
        long j3;
        zzix zzix2;
        long j4;
        int readInt;
        int i;
        zzjb zzjb;
        zziw zzaj;
        Pair pair;
        zzjs zzjs;
        zzje zzje;
        zzpx zzpx;
        Pair pair2;
        int i2;
        int i3;
        int i4;
        int i5;
        String str;
        int i6;
        zzjb zzjb2;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z3;
        int i11;
        boolean z4;
        String str2;
        List<byte[]> list;
        Pair pair3;
        String str3;
        int i12;
        int i13;
        boolean z5;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        zzjb zzjb3;
        zzpx zzpx2;
        List list2;
        int i19;
        int i20;
        String str4;
        int i21;
        int i22;
        String str5;
        zzjb zzjb4;
        int i23;
        int i24;
        int i25;
        int position;
        String str6;
        byte[] bArr;
        String str7;
        byte[] bArr2;
        int i26;
        List list3;
        boolean z6;
        boolean z7;
        String str8;
        zziw zziw2 = zziw;
        zzhp zzhp2 = zzhp;
        zziw zzaj2 = zziw2.zzaj(zziv.zzame);
        zzpx zzpx3 = zzaj2.zzai(zziv.zzams).zzaos;
        zzpx3.setPosition(16);
        int readInt2 = zzpx3.readInt();
        int i27 = readInt2 == zzaou ? 1 : readInt2 == zzaot ? 2 : (readInt2 == zzaov || readInt2 == zzaow || readInt2 == zzaox || readInt2 == zzaoy) ? 3 : readInt2 == zzaob ? 4 : -1;
        if (i27 == -1) {
            return null;
        }
        zzpx zzpx4 = zziw2.zzai(zziv.zzamo).zzaos;
        zzpx4.setPosition(8);
        int zzaf = zziv.zzaf(zzpx4.readInt());
        zzpx4.zzbl(zzaf == 0 ? 8 : 16);
        int readInt3 = zzpx4.readInt();
        zzpx4.zzbl(4);
        int position2 = zzpx4.getPosition();
        int i28 = zzaf == 0 ? 4 : 8;
        int i29 = 0;
        while (true) {
            if (i29 >= i28) {
                z2 = true;
                break;
            } else if (zzpx4.data[position2 + i29] != -1) {
                z2 = false;
                break;
            } else {
                i29++;
            }
        }
        if (z2) {
            zzpx4.zzbl(i28);
        } else {
            long zzhd = zzaf == 0 ? zzpx4.zzhd() : zzpx4.zzhh();
            if (zzhd != 0) {
                j2 = zzhd;
                zzpx4.zzbl(16);
                int readInt4 = zzpx4.readInt();
                int readInt5 = zzpx4.readInt();
                zzpx4.zzbl(4);
                int readInt6 = zzpx4.readInt();
                int readInt7 = zzpx4.readInt();
                int i30 = (readInt4 != 0 && readInt5 == 65536 && readInt6 == -65536 && readInt7 == 0) ? 90 : (readInt4 != 0 && readInt5 == -65536 && readInt6 == 65536 && readInt7 == 0) ? 270 : (readInt4 != -65536 && readInt5 == 0 && readInt6 == 0 && readInt7 == -65536) ? 180 : 0;
                zzje zzje2 = new zzje(readInt3, j2, i30);
                if (j != C.TIME_UNSET) {
                    j3 = zzje2.zzcs;
                    zzix2 = zzix;
                } else {
                    zzix2 = zzix;
                    j3 = j;
                }
                zzpx zzpx5 = zzix2.zzaos;
                zzpx5.setPosition(8);
                zzpx5.zzbl(zziv.zzaf(zzpx5.readInt()) != 0 ? 8 : 16);
                long zzhd2 = zzpx5.zzhd();
                if (j3 != C.TIME_UNSET) {
                    j4 = -9223372036854775807L;
                } else {
                    j4 = zzqe.zza(j3, (long) C.MICROS_PER_SECOND, zzhd2);
                }
                zziw zzaj3 = zzaj2.zzaj(zziv.zzamf).zzaj(zziv.zzamg);
                zzpx zzpx6 = zzaj2.zzai(zziv.zzamr).zzaos;
                zzpx6.setPosition(8);
                int zzaf2 = zziv.zzaf(zzpx6.readInt());
                zzpx6.zzbl(zzaf2 != 0 ? 8 : 16);
                long zzhd3 = zzpx6.zzhd();
                zzpx6.zzbl(zzaf2 != 0 ? 4 : 8);
                int readUnsignedShort = zzpx6.readUnsignedShort();
                char c = (char) (((readUnsignedShort >> 10) & 31) + 96);
                char c2 = (char) (((readUnsignedShort >> 5) & 31) + 96);
                char c3 = (char) ((readUnsignedShort & 31) + 96);
                StringBuilder sb = new StringBuilder(3);
                sb.append(c);
                sb.append(c2);
                sb.append(c3);
                Pair create = Pair.create(Long.valueOf(zzhd3), sb.toString());
                zzpx zzpx7 = zzaj3.zzai(zziv.zzamt).zzaos;
                int zzb = zzje2.id;
                int zzc = zzje2.zzzo;
                String str9 = (String) create.second;
                zzpx7.setPosition(12);
                readInt = zzpx7.readInt();
                zzjb zzjb5 = new zzjb(readInt);
                i = 0;
                while (i < readInt) {
                    int position3 = zzpx7.getPosition();
                    int readInt8 = zzpx7.readInt();
                    zzpo.checkArgument(readInt8 > 0, "childAtomSize should be positive");
                    int readInt9 = zzpx7.readInt();
                    if (readInt9 == zziv.zzalb || readInt9 == zziv.zzalc || readInt9 == zziv.zzamz || readInt9 == zziv.zzanl || readInt9 == zziv.zzald || readInt9 == zziv.zzale || readInt9 == zziv.zzalf || readInt9 == zziv.zzaok || readInt9 == zziv.zzaol) {
                        int i31 = readInt8;
                        int i32 = i;
                        i4 = readInt;
                        str = str9;
                        i6 = i27;
                        i7 = zzb;
                        Pair pair4 = create;
                        zzje zzje3 = zzje2;
                        zzjb zzjb6 = zzjb5;
                        zzpx zzpx8 = zzpx7;
                        int i33 = position3;
                        zzpx8.setPosition(i33 + 8 + 8);
                        zzpx8.zzbl(16);
                        int readUnsignedShort2 = zzpx8.readUnsignedShort();
                        int readUnsignedShort3 = zzpx8.readUnsignedShort();
                        zzpx8.zzbl(50);
                        int position4 = zzpx8.getPosition();
                        if (readInt9 == zziv.zzamz) {
                            i8 = i32;
                            i9 = i31;
                            readInt9 = zza(zzpx8, i33, i9, zzjb6, i8);
                            zzpx8.setPosition(position4);
                        } else {
                            i8 = i32;
                            i9 = i31;
                        }
                        float f = 1.0f;
                        int i34 = -1;
                        boolean z8 = false;
                        String str10 = null;
                        List list4 = null;
                        byte[] bArr3 = null;
                        while (position4 - i33 < i9) {
                            zzpx8.setPosition(position4);
                            int position5 = zzpx8.getPosition();
                            int readInt10 = zzpx8.readInt();
                            if (readInt10 == 0 && zzpx8.getPosition() - i33 == i9) {
                                int i35 = i9;
                                if (str10 == null) {
                                    i2 = i8;
                                    float f2 = f;
                                    i3 = i33;
                                    zzpx = zzpx8;
                                    pair2 = pair4;
                                    zzjb zzjb7 = zzjb6;
                                    i5 = i35;
                                    zzje = zzje3;
                                    zzfs zza = zzfs.zza(Integer.toString(i7), str10, null, -1, -1, readUnsignedShort2, readUnsignedShort3, -1.0f, list4, zzc, f2, bArr3, i34, null, zzhp);
                                    zzjb2 = zzjb7;
                                    zzjb2.zzaad = zza;
                                    zzpx zzpx9 = zzpx;
                                    zzpx9.setPosition(i3 + i5);
                                    i = i2 + 1;
                                    zzhp2 = zzhp;
                                    zzb = i7;
                                    zzpx7 = zzpx9;
                                    zzjb5 = zzjb2;
                                    i27 = i6;
                                    str9 = str;
                                    readInt = i4;
                                    create = pair2;
                                    zzje2 = zzje;
                                    zziw zziw3 = zziw;
                                } else {
                                    i2 = i8;
                                    i3 = i33;
                                    zzpx = zzpx8;
                                    zzjb2 = zzjb6;
                                    zzje = zzje3;
                                    pair2 = pair4;
                                    i5 = i35;
                                }
                            } else {
                                if (readInt10 > 0) {
                                    i10 = i9;
                                    z3 = true;
                                } else {
                                    i10 = i9;
                                    z3 = false;
                                }
                                zzpo.checkArgument(z3, "childAtomSize should be positive");
                                int readInt11 = zzpx8.readInt();
                                if (readInt11 == zziv.zzamh) {
                                    zzpo.checkState(str10 == null);
                                    str10 = MimeTypes.VIDEO_H264;
                                    zzpx8.setPosition(position5 + 8);
                                    zzqh zzg = zzqh.zzg(zzpx8);
                                    list = zzg.zzzl;
                                    zzjb6.zzakx = zzg.zzakx;
                                    if (!z8) {
                                        f = zzg.zzbhq;
                                    }
                                } else if (readInt11 == zziv.zzami) {
                                    zzpo.checkState(str10 == null);
                                    str10 = MimeTypes.VIDEO_H265;
                                    zzpx8.setPosition(position5 + 8);
                                    zzqn zzi = zzqn.zzi(zzpx8);
                                    list = zzi.zzzl;
                                    zzjb6.zzakx = zzi.zzakx;
                                } else {
                                    if (readInt11 == zziv.zzaom) {
                                        zzpo.checkState(str10 == null);
                                        str2 = readInt9 == zziv.zzaok ? MimeTypes.VIDEO_VP8 : MimeTypes.VIDEO_VP9;
                                    } else if (readInt11 == zziv.zzalg) {
                                        zzpo.checkState(str10 == null);
                                        str2 = MimeTypes.VIDEO_H263;
                                    } else {
                                        if (readInt11 == zziv.zzamj) {
                                            zzpo.checkState(str10 == null);
                                            Pair zzb2 = zzb(zzpx8, position5);
                                            String str11 = (String) zzb2.first;
                                            list4 = Collections.singletonList((byte[]) zzb2.second);
                                            i11 = readInt9;
                                            str10 = str11;
                                        } else if (readInt11 == zziv.zzani) {
                                            zzpx8.setPosition(position5 + 8);
                                            f = ((float) zzpx8.zzhg()) / ((float) zzpx8.zzhg());
                                            i11 = readInt9;
                                            z8 = true;
                                        } else if (readInt11 == zziv.zzaoi) {
                                            int i36 = position5 + 8;
                                            while (true) {
                                                if (i36 - position5 < readInt10) {
                                                    zzpx8.setPosition(i36);
                                                    int readInt12 = zzpx8.readInt();
                                                    i11 = readInt9;
                                                    z4 = z8;
                                                    if (zzpx8.readInt() == zziv.zzaoj) {
                                                        bArr3 = Arrays.copyOfRange(zzpx8.data, i36, readInt12 + i36);
                                                    } else {
                                                        i36 += readInt12;
                                                        readInt9 = i11;
                                                        z8 = z4;
                                                    }
                                                } else {
                                                    i11 = readInt9;
                                                    z4 = z8;
                                                    bArr3 = null;
                                                }
                                            }
                                            z8 = z4;
                                        } else {
                                            i11 = readInt9;
                                            boolean z9 = z8;
                                            if (readInt11 == zziv.zzaoh) {
                                                int readUnsignedByte = zzpx8.readUnsignedByte();
                                                zzpx8.zzbl(3);
                                                if (readUnsignedByte == 0) {
                                                    switch (zzpx8.readUnsignedByte()) {
                                                        case 0:
                                                            i34 = 0;
                                                            break;
                                                        case 1:
                                                            z8 = z9;
                                                            i34 = 1;
                                                            break;
                                                        case 2:
                                                            z8 = z9;
                                                            i34 = 2;
                                                            break;
                                                        case 3:
                                                            i34 = 3;
                                                            break;
                                                    }
                                                }
                                            }
                                            z8 = z9;
                                            position4 += readInt10;
                                            i9 = i10;
                                            readInt9 = i11;
                                        }
                                        position4 += readInt10;
                                        i9 = i10;
                                        readInt9 = i11;
                                    }
                                    i11 = readInt9;
                                    position4 += readInt10;
                                    i9 = i10;
                                    readInt9 = i11;
                                }
                                i11 = readInt9;
                                list4 = list;
                                position4 += readInt10;
                                i9 = i10;
                                readInt9 = i11;
                            }
                        }
                        int i352 = i9;
                        if (str10 == null) {
                        }
                    } else {
                        if (readInt9 == zziv.zzali || readInt9 == zziv.zzana || readInt9 == zziv.zzaln || readInt9 == zziv.zzalp || readInt9 == zziv.zzalr || readInt9 == zziv.zzalu || readInt9 == zziv.zzals || readInt9 == zziv.zzalt || readInt9 == zziv.zzany || readInt9 == zziv.zzanz || readInt9 == zziv.zzall || readInt9 == zziv.zzalm || readInt9 == zziv.zzalj || readInt9 == zziv.zzaoo) {
                            i12 = i;
                            i4 = readInt;
                            str3 = str9;
                            i6 = i27;
                            i7 = zzb;
                            pair3 = create;
                            zzje zzje4 = zzje2;
                            int i37 = -1;
                            int i38 = readInt8;
                            zzjb zzjb8 = zzjb5;
                            int i39 = position3;
                            zzpx7.setPosition(i39 + 8 + 8);
                            if (z) {
                                i13 = zzpx7.readUnsignedShort();
                                zzpx7.zzbl(6);
                            } else {
                                zzpx7.zzbl(8);
                                i13 = 0;
                            }
                            if (i13 != 0) {
                                z5 = true;
                                if (i13 == 1) {
                                    i14 = 2;
                                } else {
                                    i14 = 2;
                                    if (i13 == 2) {
                                        zzpx7.zzbl(16);
                                        i16 = (int) Math.round(Double.longBitsToDouble(zzpx7.readLong()));
                                        i15 = zzpx7.zzhg();
                                        zzpx7.zzbl(20);
                                        int position6 = zzpx7.getPosition();
                                        if (readInt9 != zziv.zzana) {
                                            i17 = i12;
                                            readInt9 = zza(zzpx7, i39, i38, zzjb8, i17);
                                            zzpx7.setPosition(position6);
                                        } else {
                                            i17 = i12;
                                        }
                                        String str12 = readInt9 != zziv.zzaln ? MimeTypes.AUDIO_AC3 : readInt9 == zziv.zzalp ? MimeTypes.AUDIO_E_AC3 : readInt9 == zziv.zzalr ? MimeTypes.AUDIO_DTS : (readInt9 == zziv.zzals || readInt9 == zziv.zzalt) ? MimeTypes.AUDIO_DTS_HD : readInt9 == zziv.zzalu ? MimeTypes.AUDIO_DTS_EXPRESS : readInt9 == zziv.zzany ? MimeTypes.AUDIO_AMR_NB : readInt9 == zziv.zzanz ? MimeTypes.AUDIO_AMR_WB : (readInt9 == zziv.zzall || readInt9 == zziv.zzalm) ? MimeTypes.AUDIO_RAW : readInt9 == zziv.zzalj ? MimeTypes.AUDIO_MPEG : readInt9 == zziv.zzaoo ? MimeTypes.AUDIO_ALAC : null;
                                        int i40 = i16;
                                        int i41 = i15;
                                        i18 = position6;
                                        byte[] bArr4 = null;
                                        String str13 = str12;
                                        while (i18 - i39 < i38) {
                                            zzpx7.setPosition(i18);
                                            int readInt13 = zzpx7.readInt();
                                            zzpo.checkArgument(readInt13 > 0 ? z5 : false, "childAtomSize should be positive");
                                            int readInt14 = zzpx7.readInt();
                                            if (readInt14 == zziv.zzamj || (z && readInt14 == zziv.zzalk)) {
                                                i23 = readInt13;
                                                byte[] bArr5 = bArr4;
                                                String str14 = str13;
                                                i25 = i18;
                                                i21 = i17;
                                                i20 = i39;
                                                i19 = i38;
                                                str5 = str3;
                                                i22 = 2;
                                                zzjb4 = zzjb8;
                                                if (readInt14 == zziv.zzamj) {
                                                    position = i25;
                                                } else {
                                                    position = zzpx7.getPosition();
                                                    while (true) {
                                                        if (position - i25 < i23) {
                                                            zzpx7.setPosition(position);
                                                            int readInt15 = zzpx7.readInt();
                                                            zzpo.checkArgument(readInt15 > 0, "childAtomSize should be positive");
                                                            if (zzpx7.readInt() != zziv.zzamj) {
                                                                position += readInt15;
                                                            }
                                                        } else {
                                                            position = -1;
                                                        }
                                                    }
                                                }
                                                i24 = -1;
                                                if (position != -1) {
                                                    Pair zzb3 = zzb(zzpx7, position);
                                                    str6 = (String) zzb3.first;
                                                    bArr = (byte[]) zzb3.second;
                                                    if (MimeTypes.AUDIO_AAC.equals(str6)) {
                                                        Pair zzf = zzpp.zzf(bArr);
                                                        int intValue = ((Integer) zzf.first).intValue();
                                                        i41 = ((Integer) zzf.second).intValue();
                                                        i40 = intValue;
                                                    }
                                                } else {
                                                    bArr = bArr5;
                                                    str6 = str14;
                                                }
                                                str4 = str6;
                                            } else {
                                                if (readInt14 == zziv.zzalo) {
                                                    zzpx7.setPosition(i18 + 8);
                                                    str7 = str3;
                                                    zzjb8.zzaad = zzgg.zza(zzpx7, Integer.toString(i7), str7, zzhp2);
                                                } else {
                                                    str7 = str3;
                                                    if (readInt14 == zziv.zzalq) {
                                                        zzpx7.setPosition(i18 + 8);
                                                        zzjb8.zzaad = zzgg.zzb(zzpx7, Integer.toString(i7), str7, zzhp2);
                                                    } else {
                                                        if (readInt14 == zziv.zzalv) {
                                                            i23 = readInt13;
                                                            bArr2 = bArr4;
                                                            i26 = i18;
                                                            i21 = i17;
                                                            str5 = str7;
                                                            i22 = 2;
                                                            str4 = str13;
                                                            i20 = i39;
                                                            i19 = i38;
                                                            zzjb4 = zzjb8;
                                                            zzjb4.zzaad = zzfs.zza(Integer.toString(i7), str13, null, -1, -1, i41, i40, null, zzhp2, 0, str5);
                                                        } else {
                                                            i23 = readInt13;
                                                            bArr2 = bArr4;
                                                            str4 = str13;
                                                            i26 = i18;
                                                            i21 = i17;
                                                            str5 = str7;
                                                            i20 = i39;
                                                            i19 = i38;
                                                            i22 = 2;
                                                            zzjb4 = zzjb8;
                                                            if (readInt14 == zziv.zzaoo) {
                                                                byte[] bArr6 = new byte[i23];
                                                                i25 = i26;
                                                                zzpx7.setPosition(i25);
                                                                zzpx7.zze(bArr6, 0, i23);
                                                                bArr4 = bArr6;
                                                                i24 = -1;
                                                            }
                                                        }
                                                        i25 = i26;
                                                        bArr4 = bArr2;
                                                        i24 = -1;
                                                    }
                                                }
                                                i23 = readInt13;
                                                bArr2 = bArr4;
                                                str4 = str13;
                                                i25 = i18;
                                                i21 = i17;
                                                str5 = str7;
                                                i20 = i39;
                                                i19 = i38;
                                                i22 = 2;
                                                zzjb4 = zzjb8;
                                                bArr4 = bArr2;
                                                i24 = -1;
                                            }
                                            i18 = i25 + i23;
                                            i37 = i24;
                                            zzjb8 = zzjb4;
                                            str3 = str5;
                                            i14 = i22;
                                            i17 = i21;
                                            str13 = str4;
                                            i39 = i20;
                                            i38 = i19;
                                            z5 = true;
                                        }
                                        byte[] bArr7 = bArr4;
                                        String str15 = str13;
                                        int i42 = i17;
                                        int i43 = i14;
                                        int i44 = i39;
                                        int i45 = i37;
                                        int i46 = i38;
                                        str = str3;
                                        zzjb3 = zzjb8;
                                        if (zzjb3.zzaad == null) {
                                            String str16 = str15;
                                            if (str16 != null) {
                                                int i47 = MimeTypes.AUDIO_RAW.equals(str16) ? i43 : i45;
                                                String num = Integer.toString(i7);
                                                byte[] bArr8 = bArr7;
                                                if (bArr8 == null) {
                                                    list2 = null;
                                                } else {
                                                    list2 = Collections.singletonList(bArr8);
                                                }
                                                int i48 = i45;
                                                zzpx2 = zzpx7;
                                                zzjb3.zzaad = zzfs.zza(num, str16, null, -1, -1, i41, i40, i47, list2, zzhp2, 0, str);
                                                zzpx = zzpx2;
                                                zzjb2 = zzjb3;
                                                zzje = zzje4;
                                                pair2 = pair3;
                                                i2 = i42;
                                                i3 = i44;
                                                i5 = i46;
                                            }
                                        }
                                        int i49 = i45;
                                        zzpx2 = zzpx7;
                                        zzpx = zzpx2;
                                        zzjb2 = zzjb3;
                                        zzje = zzje4;
                                        pair2 = pair3;
                                        i2 = i42;
                                        i3 = i44;
                                        i5 = i46;
                                    } else {
                                        i3 = i39;
                                        zzjb2 = zzjb8;
                                        zzpx = zzpx7;
                                        i5 = i38;
                                        zzje = zzje4;
                                    }
                                }
                            } else {
                                i14 = 2;
                                z5 = true;
                            }
                            i15 = zzpx7.readUnsignedShort();
                            zzpx7.zzbl(6);
                            int zzhf = zzpx7.zzhf();
                            if (i13 == z5) {
                                zzpx7.zzbl(16);
                            }
                            i16 = zzhf;
                            int position62 = zzpx7.getPosition();
                            if (readInt9 != zziv.zzana) {
                            }
                            if (readInt9 != zziv.zzaln) {
                            }
                            int i402 = i16;
                            int i412 = i15;
                            i18 = position62;
                            byte[] bArr42 = null;
                            String str132 = str12;
                            while (i18 - i39 < i38) {
                            }
                            byte[] bArr72 = bArr42;
                            String str152 = str132;
                            int i422 = i17;
                            int i432 = i14;
                            int i442 = i39;
                            int i452 = i37;
                            int i462 = i38;
                            str = str3;
                            zzjb3 = zzjb8;
                            if (zzjb3.zzaad == null) {
                            }
                            int i492 = i452;
                            zzpx2 = zzpx7;
                            zzpx = zzpx2;
                            zzjb2 = zzjb3;
                            zzje = zzje4;
                            pair2 = pair3;
                            i2 = i422;
                            i3 = i442;
                            i5 = i462;
                        } else if (readInt9 == zziv.zzanj || readInt9 == zziv.zzanu || readInt9 == zziv.zzanv || readInt9 == zziv.zzanw || readInt9 == zziv.zzanx) {
                            int i50 = i;
                            zzpx7.setPosition(position3 + 8 + 8);
                            long j5 = Long.MAX_VALUE;
                            if (readInt9 == zziv.zzanj) {
                                str8 = MimeTypes.APPLICATION_TTML;
                                list3 = null;
                                z7 = true;
                                z6 = false;
                            } else if (readInt9 == zziv.zzanu) {
                                String str17 = MimeTypes.APPLICATION_TX3G;
                                int i51 = (readInt8 - 8) - 8;
                                byte[] bArr9 = new byte[i51];
                                z6 = false;
                                zzpx7.zze(bArr9, 0, i51);
                                list3 = Collections.singletonList(bArr9);
                                z7 = true;
                                str8 = str17;
                            } else {
                                z6 = false;
                                if (readInt9 == zziv.zzanv) {
                                    str8 = MimeTypes.APPLICATION_MP4VTT;
                                } else if (readInt9 == zziv.zzanw) {
                                    str8 = MimeTypes.APPLICATION_TTML;
                                    j5 = 0;
                                } else if (readInt9 == zziv.zzanx) {
                                    String str18 = MimeTypes.APPLICATION_MP4CEA608;
                                    z7 = true;
                                    zzjb5.zzapi = 1;
                                    str8 = str18;
                                    list3 = null;
                                } else {
                                    throw new IllegalStateException();
                                }
                                z7 = true;
                                list3 = null;
                            }
                            zzje zzje5 = zzje2;
                            int i52 = readInt8;
                            int i53 = position3;
                            boolean z10 = z7;
                            i12 = i50;
                            zzjb zzjb9 = zzjb5;
                            boolean z11 = z6;
                            i4 = readInt;
                            str3 = str9;
                            i6 = i27;
                            i7 = zzb;
                            pair3 = create;
                            zzfs zza2 = zzfs.zza(Integer.toString(zzb), str8, (String) null, -1, 0, str9, -1, zzhp2, j5, list3);
                            zzjb zzjb10 = zzjb9;
                            zzjb10.zzaad = zza2;
                            zzjb2 = zzjb10;
                            zzpx = zzpx7;
                            i5 = i52;
                            zzje = zzje5;
                            i3 = i53;
                        } else if (readInt9 == zziv.zzaon) {
                            int i54 = i;
                            zzjb5.zzaad = zzfs.zza(Integer.toString(zzb), MimeTypes.APPLICATION_CAMERA_MOTION, null, -1, zzhp2);
                            i5 = readInt8;
                            i3 = position3;
                            zzjb2 = zzjb5;
                            i4 = readInt;
                            str = str9;
                            i6 = i27;
                            i7 = zzb;
                            zzpx = zzpx7;
                            pair2 = create;
                            zzje = zzje2;
                            i2 = i54;
                        } else {
                            i5 = readInt8;
                            i3 = position3;
                            i2 = i;
                            zzjb2 = zzjb5;
                            i4 = readInt;
                            str = str9;
                            i6 = i27;
                            i7 = zzb;
                            zzpx = zzpx7;
                            pair2 = create;
                            zzje = zzje2;
                        }
                        i2 = i12;
                        str = str3;
                        pair2 = pair3;
                    }
                    zzpx zzpx92 = zzpx;
                    zzpx92.setPosition(i3 + i5);
                    i = i2 + 1;
                    zzhp2 = zzhp;
                    zzb = i7;
                    zzpx7 = zzpx92;
                    zzjb5 = zzjb2;
                    i27 = i6;
                    str9 = str;
                    readInt = i4;
                    create = pair2;
                    zzje2 = zzje;
                    zziw zziw32 = zziw;
                }
                zzjb = zzjb5;
                int i55 = i27;
                Pair pair5 = create;
                zzje zzje6 = zzje2;
                zzaj = zziw.zzaj(zziv.zzamp);
                if (zzaj != null) {
                    zzix zzai = zzaj.zzai(zziv.zzamq);
                    if (zzai != null) {
                        zzpx zzpx10 = zzai.zzaos;
                        zzpx10.setPosition(8);
                        int zzaf3 = zziv.zzaf(zzpx10.readInt());
                        int zzhg = zzpx10.zzhg();
                        long[] jArr = new long[zzhg];
                        long[] jArr2 = new long[zzhg];
                        for (int i56 = 0; i56 < zzhg; i56++) {
                            jArr[i56] = zzaf3 == 1 ? zzpx10.zzhh() : zzpx10.zzhd();
                            jArr2[i56] = zzaf3 == 1 ? zzpx10.readLong() : (long) zzpx10.readInt();
                            if (zzpx10.readShort() != 1) {
                                throw new IllegalArgumentException("Unsupported media rate.");
                            }
                            zzpx10.zzbl(2);
                        }
                        pair = Pair.create(jArr, jArr2);
                        zzjs = null;
                        if (zzjb.zzaad == null) {
                            return zzjs;
                        }
                        zzjs zzjs2 = new zzjs(zzje6.id, i55, ((Long) pair5.first).longValue(), zzhd2, j4, zzjb.zzaad, zzjb.zzapi, zzjb.zzaph, zzjb.zzakx, (long[]) pair.first, (long[]) pair.second);
                        return zzjs2;
                    }
                }
                zzjs = null;
                pair = Pair.create(null, null);
                if (zzjb.zzaad == null) {
                }
            }
        }
        j2 = -9223372036854775807L;
        zzpx4.zzbl(16);
        int readInt42 = zzpx4.readInt();
        int readInt52 = zzpx4.readInt();
        zzpx4.zzbl(4);
        int readInt62 = zzpx4.readInt();
        int readInt72 = zzpx4.readInt();
        if (readInt42 != 0) {
        }
        zzje zzje22 = new zzje(readInt3, j2, i30);
        if (j != C.TIME_UNSET) {
        }
        zzpx zzpx52 = zzix2.zzaos;
        zzpx52.setPosition(8);
        zzpx52.zzbl(zziv.zzaf(zzpx52.readInt()) != 0 ? 8 : 16);
        long zzhd22 = zzpx52.zzhd();
        if (j3 != C.TIME_UNSET) {
        }
        zziw zzaj32 = zzaj2.zzaj(zziv.zzamf).zzaj(zziv.zzamg);
        zzpx zzpx62 = zzaj2.zzai(zziv.zzamr).zzaos;
        zzpx62.setPosition(8);
        int zzaf22 = zziv.zzaf(zzpx62.readInt());
        zzpx62.zzbl(zzaf22 != 0 ? 8 : 16);
        long zzhd32 = zzpx62.zzhd();
        zzpx62.zzbl(zzaf22 != 0 ? 4 : 8);
        int readUnsignedShort4 = zzpx62.readUnsignedShort();
        char c4 = (char) (((readUnsignedShort4 >> 10) & 31) + 96);
        char c22 = (char) (((readUnsignedShort4 >> 5) & 31) + 96);
        char c32 = (char) ((readUnsignedShort4 & 31) + 96);
        StringBuilder sb2 = new StringBuilder(3);
        sb2.append(c4);
        sb2.append(c22);
        sb2.append(c32);
        Pair create2 = Pair.create(Long.valueOf(zzhd32), sb2.toString());
        zzpx zzpx72 = zzaj32.zzai(zziv.zzamt).zzaos;
        int zzb4 = zzje22.id;
        int zzc2 = zzje22.zzzo;
        String str92 = (String) create2.second;
        zzpx72.setPosition(12);
        readInt = zzpx72.readInt();
        zzjb zzjb52 = new zzjb(readInt);
        i = 0;
        while (i < readInt) {
        }
        zzjb = zzjb52;
        int i552 = i27;
        Pair pair52 = create2;
        zzje zzje62 = zzje22;
        zzaj = zziw.zzaj(zziv.zzamp);
        if (zzaj != null) {
        }
        zzjs = null;
        pair = Pair.create(null, null);
        if (zzjb.zzaad == null) {
        }
    }

    public static zzjv zza(zzjs zzjs, zziw zziw, zzid zzid) throws zzfx {
        zzja zzja;
        boolean z;
        int i;
        int i2;
        int i3;
        int i4;
        int[] iArr;
        long[] jArr;
        int[] iArr2;
        long[] jArr2;
        long j;
        int[] iArr3;
        long[] jArr3;
        boolean z2;
        long[] jArr4;
        int i5;
        long[] jArr5;
        int[] iArr4;
        int[] iArr5;
        int[] iArr6;
        zzpx zzpx;
        zzjs zzjs2 = zzjs;
        zziw zziw2 = zziw;
        zzid zzid2 = zzid;
        zzix zzai = zziw2.zzai(zziv.zzanq);
        if (zzai != null) {
            zzja = new zzjc(zzai);
        } else {
            zzix zzai2 = zziw2.zzai(zziv.zzanr);
            if (zzai2 == null) {
                throw new zzfx("Track has no sample table size information");
            }
            zzja = new zzjd(zzai2);
        }
        int zzef = zzja.zzef();
        if (zzef == 0) {
            zzjv zzjv = new zzjv(new long[0], new int[0], 0, new long[0], new int[0]);
            return zzjv;
        }
        zzix zzai3 = zziw2.zzai(zziv.zzans);
        if (zzai3 == null) {
            zzai3 = zziw2.zzai(zziv.zzant);
            z = true;
        } else {
            z = false;
        }
        zzpx zzpx2 = zzai3.zzaos;
        zzpx zzpx3 = zziw2.zzai(zziv.zzanp).zzaos;
        zzpx zzpx4 = zziw2.zzai(zziv.zzanm).zzaos;
        zzix zzai4 = zziw2.zzai(zziv.zzann);
        zzpx zzpx5 = zzai4 != null ? zzai4.zzaos : null;
        zzix zzai5 = zziw2.zzai(zziv.zzano);
        zzpx zzpx6 = zzai5 != null ? zzai5.zzaos : null;
        zziz zziz = new zziz(zzpx3, zzpx2, z);
        zzpx4.setPosition(12);
        int zzhg = zzpx4.zzhg() - 1;
        int zzhg2 = zzpx4.zzhg();
        int zzhg3 = zzpx4.zzhg();
        if (zzpx6 != null) {
            zzpx6.setPosition(12);
            i = zzpx6.zzhg();
        } else {
            i = 0;
        }
        int i6 = -1;
        if (zzpx5 != null) {
            zzpx5.setPosition(12);
            i2 = zzpx5.zzhg();
            if (i2 > 0) {
                i6 = zzpx5.zzhg() - 1;
            } else {
                zzpx5 = null;
            }
        } else {
            i2 = 0;
        }
        long j2 = 0;
        if (!(zzja.zzeh() && MimeTypes.AUDIO_RAW.equals(zzjs2.zzaad.zzzj) && zzhg == 0 && i == 0 && i2 == 0)) {
            jArr2 = new long[zzef];
            iArr = new int[zzef];
            jArr = new long[zzef];
            int i7 = i2;
            iArr2 = new int[zzef];
            zzpx zzpx7 = zzpx4;
            int i8 = i;
            int i9 = i6;
            long j3 = 0;
            int i10 = i7;
            int i11 = 0;
            int i12 = 0;
            int i13 = 0;
            int i14 = 0;
            int i15 = zzhg;
            long j4 = 0;
            int i16 = 0;
            int i17 = zzhg3;
            int i18 = zzhg2;
            int i19 = i17;
            while (i16 < zzef) {
                while (i13 == 0) {
                    int i20 = zzef;
                    zzpo.checkState(zziz.zzee());
                    int i21 = i19;
                    int i22 = i15;
                    long j5 = zziz.zzapb;
                    i13 = zziz.zzapa;
                    j4 = j5;
                    zzef = i20;
                    i19 = i21;
                    i15 = i22;
                }
                int i23 = zzef;
                int i24 = i19;
                int i25 = i15;
                if (zzpx6 != null) {
                    while (i14 == 0 && i8 > 0) {
                        i14 = zzpx6.zzhg();
                        i12 = zzpx6.readInt();
                        i8--;
                    }
                    i14--;
                }
                int i26 = i12;
                jArr2[i16] = j4;
                iArr[i16] = zzja.zzeg();
                if (iArr[i16] > i11) {
                    i11 = iArr[i16];
                }
                jArr[i16] = j3 + ((long) i26);
                iArr2[i16] = zzpx5 == null ? 1 : 0;
                if (i16 == i9) {
                    iArr2[i16] = 1;
                    i10--;
                    if (i10 > 0) {
                        i9 = zzpx5.zzhg() - 1;
                    }
                }
                int i27 = i10;
                int i28 = i9;
                int i29 = i26;
                int i30 = i24;
                j3 += (long) i30;
                i18--;
                if (i18 != 0 || i25 <= 0) {
                    zzpx = zzpx7;
                    i15 = i25;
                } else {
                    zzpx = zzpx7;
                    i15 = i25 - 1;
                    i18 = zzpx.zzhg();
                    i30 = zzpx.zzhg();
                }
                zzpx zzpx8 = zzpx;
                j4 += (long) iArr[i16];
                i13--;
                i16++;
                zzef = i23;
                i12 = i29;
                i9 = i28;
                zzpx7 = zzpx8;
                int i31 = i27;
                i19 = i30;
                i10 = i31;
            }
            i3 = zzef;
            int i32 = i15;
            zzpo.checkArgument(i14 == 0);
            while (i8 > 0) {
                zzpo.checkArgument(zzpx6.zzhg() == 0);
                zzpx6.readInt();
                i8--;
            }
            if (i10 == 0 && i18 == 0 && i13 == 0 && i32 == 0) {
                zzjs2 = zzjs;
            } else {
                int i33 = i10;
                zzjs2 = zzjs;
                int i34 = zzjs2.id;
                StringBuilder sb = new StringBuilder(215);
                sb.append("Inconsistent stbl box for track ");
                sb.append(i34);
                sb.append(": remainingSynchronizationSamples ");
                sb.append(i33);
                sb.append(", remainingSamplesAtTimestampDelta ");
                sb.append(i18);
                sb.append(", remainingSamplesInChunk ");
                sb.append(i13);
                sb.append(", remainingTimestampDeltaChanges ");
                sb.append(i32);
                Log.w("AtomParsers", sb.toString());
            }
            j = j3;
            i4 = i11;
        } else {
            i3 = zzef;
            long[] jArr6 = new long[zziz.length];
            int[] iArr7 = new int[zziz.length];
            while (zziz.zzee()) {
                jArr6[zziz.index] = zziz.zzapb;
                iArr7[zziz.index] = zziz.zzapa;
            }
            int zzeg = zzja.zzeg();
            long j6 = (long) zzhg3;
            int i35 = 8192 / zzeg;
            int i36 = 0;
            for (int zzf : iArr7) {
                i36 += zzqe.zzf(zzf, i35);
            }
            long[] jArr7 = new long[i36];
            int[] iArr8 = new int[i36];
            long[] jArr8 = new long[i36];
            int[] iArr9 = new int[i36];
            int i37 = 0;
            int i38 = 0;
            int i39 = 0;
            for (int i40 = 0; i40 < iArr7.length; i40++) {
                int i41 = iArr7[i40];
                long j7 = jArr6[i40];
                while (i41 > 0) {
                    int min = Math.min(i35, i41);
                    jArr7[i38] = j7;
                    iArr8[i38] = zzeg * min;
                    long[] jArr9 = jArr6;
                    i39 = Math.max(i39, iArr8[i38]);
                    int[] iArr10 = iArr7;
                    jArr8[i38] = ((long) i37) * j6;
                    iArr9[i38] = 1;
                    j7 += (long) iArr8[i38];
                    i37 += min;
                    i41 -= min;
                    i38++;
                    jArr6 = jArr9;
                    iArr7 = iArr10;
                }
                long[] jArr10 = jArr6;
                int[] iArr11 = iArr7;
            }
            zzjh zzjh = new zzjh(jArr7, iArr8, i39, jArr8, iArr9);
            jArr2 = zzjh.zzagu;
            iArr = zzjh.zzagt;
            int i42 = zzjh.zzapp;
            jArr = zzjh.zzapq;
            iArr2 = zzjh.zzapr;
            i4 = i42;
            j = 0;
        }
        if (zzjs2.zzaso != null) {
            zzid zzid3 = zzid;
            if (!zzid.zzea()) {
                if (zzjs2.zzaso.length == 1 && zzjs2.type == 1 && jArr.length >= 2) {
                    long j8 = zzjs2.zzasp[0];
                    long zza = zzqe.zza(zzjs2.zzaso[0], zzjs2.zzcr, zzjs2.zzasl) + j8;
                    if (jArr[0] <= j8 && j8 < jArr[1] && jArr[jArr.length - 1] < zza && zza <= j) {
                        long j9 = j - zza;
                        long zza2 = zzqe.zza(j8 - jArr[0], (long) zzjs2.zzaad.zzzu, zzjs2.zzcr);
                        long zza3 = zzqe.zza(j9, (long) zzjs2.zzaad.zzzu, zzjs2.zzcr);
                        if (!(zza2 == 0 && zza3 == 0) && zza2 <= 2147483647L && zza3 <= 2147483647L) {
                            zzid3.zzzw = (int) zza2;
                            zzid3.zzzx = (int) zza3;
                            zzqe.zza(jArr, (long) C.MICROS_PER_SECOND, zzjs2.zzcr);
                            zzjv zzjv2 = new zzjv(jArr2, iArr, i4, jArr, iArr2);
                            return zzjv2;
                        }
                    }
                }
                if (zzjs2.zzaso.length == 1) {
                    char c = 0;
                    if (zzjs2.zzaso[0] == 0) {
                        int i43 = 0;
                        while (i43 < jArr.length) {
                            jArr[i43] = zzqe.zza(jArr[i43] - zzjs2.zzasp[c], (long) C.MICROS_PER_SECOND, zzjs2.zzcr);
                            i43++;
                            c = 0;
                        }
                        zzjv zzjv3 = new zzjv(jArr2, iArr, i4, jArr, iArr2);
                        return zzjv3;
                    }
                }
                boolean z3 = zzjs2.type == 1;
                int i44 = 0;
                boolean z4 = false;
                int i45 = 0;
                int i46 = 0;
                while (i44 < zzjs2.zzaso.length) {
                    long j10 = zzjs2.zzasp[i44];
                    if (j10 != -1) {
                        iArr6 = iArr;
                        long zza4 = zzqe.zza(zzjs2.zzaso[i44], zzjs2.zzcr, zzjs2.zzasl);
                        int zzb = zzqe.zzb(jArr, j10, true, true);
                        int zzb2 = zzqe.zzb(jArr, j10 + zza4, z3, false);
                        i45 += zzb2 - zzb;
                        z4 |= i46 != zzb;
                        i46 = zzb2;
                    } else {
                        iArr6 = iArr;
                    }
                    i44++;
                    iArr = iArr6;
                }
                int[] iArr12 = iArr;
                boolean z5 = (i45 != i3) | z4;
                long[] jArr11 = z5 ? new long[i45] : jArr2;
                int[] iArr13 = z5 ? new int[i45] : iArr12;
                if (z5) {
                    i4 = 0;
                }
                int[] iArr14 = z5 ? new int[i45] : iArr2;
                long[] jArr12 = new long[i45];
                int i47 = i4;
                int i48 = 0;
                int i49 = 0;
                while (i48 < zzjs2.zzaso.length) {
                    long j11 = zzjs2.zzasp[i48];
                    long j12 = zzjs2.zzaso[i48];
                    if (j11 != -1) {
                        int[] iArr15 = iArr14;
                        i5 = i48;
                        long[] jArr13 = jArr11;
                        jArr4 = jArr12;
                        long zza5 = zzqe.zza(j12, zzjs2.zzcr, zzjs2.zzasl) + j11;
                        int zzb3 = zzqe.zzb(jArr, j11, true, true);
                        int zzb4 = zzqe.zzb(jArr, zza5, z3, false);
                        if (z5) {
                            int i50 = zzb4 - zzb3;
                            jArr5 = jArr13;
                            System.arraycopy(jArr2, zzb3, jArr5, i49, i50);
                            iArr4 = iArr12;
                            System.arraycopy(iArr4, zzb3, iArr13, i49, i50);
                            z2 = z3;
                            iArr5 = iArr15;
                            System.arraycopy(iArr2, zzb3, iArr5, i49, i50);
                        } else {
                            z2 = z3;
                            iArr4 = iArr12;
                            iArr5 = iArr15;
                            jArr5 = jArr13;
                        }
                        int i51 = i47;
                        while (zzb3 < zzb4) {
                            long[] jArr14 = jArr2;
                            int[] iArr16 = iArr2;
                            long j13 = j11;
                            jArr4[i49] = zzqe.zza(j2, (long) C.MICROS_PER_SECOND, zzjs2.zzasl) + zzqe.zza(jArr[zzb3] - j11, (long) C.MICROS_PER_SECOND, zzjs2.zzcr);
                            if (z5 && iArr13[i49] > i51) {
                                i51 = iArr4[zzb3];
                            }
                            i49++;
                            zzb3++;
                            jArr2 = jArr14;
                            iArr2 = iArr16;
                            j11 = j13;
                        }
                        jArr3 = jArr2;
                        iArr3 = iArr2;
                        i47 = i51;
                    } else {
                        z2 = z3;
                        jArr5 = jArr11;
                        jArr4 = jArr12;
                        jArr3 = jArr2;
                        iArr3 = iArr2;
                        iArr5 = iArr14;
                        i5 = i48;
                        iArr4 = iArr12;
                    }
                    j2 += j12;
                    i48 = i5 + 1;
                    iArr12 = iArr4;
                    jArr11 = jArr5;
                    jArr12 = jArr4;
                    jArr2 = jArr3;
                    iArr2 = iArr3;
                    iArr14 = iArr5;
                    z3 = z2;
                }
                long[] jArr15 = jArr11;
                long[] jArr16 = jArr12;
                int[] iArr17 = iArr14;
                boolean z6 = false;
                for (int i52 = 0; i52 < iArr17.length && !z6; i52++) {
                    z6 |= (iArr17[i52] & 1) != 0;
                }
                if (!z6) {
                    throw new zzfx("The edited sample sequence does not contain a sync sample.");
                }
                zzjv zzjv4 = new zzjv(jArr15, iArr13, i47, jArr16, iArr17);
                return zzjv4;
            }
        }
        long[] jArr17 = jArr2;
        int[] iArr18 = iArr2;
        int[] iArr19 = iArr;
        zzqe.zza(jArr, (long) C.MICROS_PER_SECOND, zzjs2.zzcr);
        zzjv zzjv5 = new zzjv(jArr17, iArr19, i4, jArr, iArr18);
        return zzjv5;
    }

    public static zzki zza(zzix zzix, boolean z) {
        if (z) {
            return null;
        }
        zzpx zzpx = zzix.zzaos;
        zzpx.setPosition(8);
        while (zzpx.zzhb() >= 8) {
            int position = zzpx.getPosition();
            int readInt = zzpx.readInt();
            if (zzpx.readInt() == zziv.zzaob) {
                zzpx.setPosition(position);
                int i = position + readInt;
                zzpx.zzbl(12);
                while (true) {
                    if (zzpx.getPosition() >= i) {
                        break;
                    }
                    int position2 = zzpx.getPosition();
                    int readInt2 = zzpx.readInt();
                    if (zzpx.readInt() == zziv.zzaoc) {
                        zzpx.setPosition(position2);
                        int i2 = position2 + readInt2;
                        zzpx.zzbl(8);
                        ArrayList arrayList = new ArrayList();
                        while (zzpx.getPosition() < i2) {
                            zza zzd = zzjm.zzd(zzpx);
                            if (zzd != null) {
                                arrayList.add(zzd);
                            }
                        }
                        if (!arrayList.isEmpty()) {
                            return new zzki((List<? extends zza>) arrayList);
                        }
                    } else {
                        zzpx.zzbl(readInt2 - 8);
                    }
                }
                return null;
            }
            zzpx.zzbl(readInt - 8);
        }
        return null;
    }

    private static Pair<String, byte[]> zzb(zzpx zzpx, int i) {
        zzpx.setPosition(i + 8 + 4);
        zzpx.zzbl(1);
        zzc(zzpx);
        zzpx.zzbl(2);
        int readUnsignedByte = zzpx.readUnsignedByte();
        if ((readUnsignedByte & 128) != 0) {
            zzpx.zzbl(2);
        }
        if ((readUnsignedByte & 64) != 0) {
            zzpx.zzbl(zzpx.readUnsignedShort());
        }
        if ((readUnsignedByte & 32) != 0) {
            zzpx.zzbl(2);
        }
        zzpx.zzbl(1);
        zzc(zzpx);
        String str = null;
        switch (zzpx.readUnsignedByte()) {
            case 32:
                str = MimeTypes.VIDEO_MP4V;
                break;
            case 33:
                str = MimeTypes.VIDEO_H264;
                break;
            case 35:
                str = MimeTypes.VIDEO_H265;
                break;
            case 64:
            case 102:
            case 103:
            case 104:
                str = MimeTypes.AUDIO_AAC;
                break;
            case 107:
                return Pair.create(MimeTypes.AUDIO_MPEG, null);
            case 165:
                str = MimeTypes.AUDIO_AC3;
                break;
            case 166:
                str = MimeTypes.AUDIO_E_AC3;
                break;
            case 169:
            case 172:
                return Pair.create(MimeTypes.AUDIO_DTS, null);
            case 170:
            case 171:
                return Pair.create(MimeTypes.AUDIO_DTS_HD, null);
        }
        zzpx.zzbl(12);
        zzpx.zzbl(1);
        int zzc = zzc(zzpx);
        byte[] bArr = new byte[zzc];
        zzpx.zze(bArr, 0, zzc);
        return Pair.create(str, bArr);
    }

    private static int zza(zzpx zzpx, int i, int i2, zzjb zzjb, int i3) {
        Object obj;
        zzpx zzpx2 = zzpx;
        int position = zzpx.getPosition();
        while (true) {
            boolean z = false;
            if (position - i >= i2) {
                return 0;
            }
            zzpx2.setPosition(position);
            int readInt = zzpx.readInt();
            zzpo.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (zzpx.readInt() == zziv.zzamv) {
                int i4 = position + 8;
                Pair pair = null;
                boolean z2 = false;
                Object obj2 = null;
                Object obj3 = null;
                while (i4 - position < readInt) {
                    zzpx2.setPosition(i4);
                    int readInt2 = zzpx.readInt();
                    int readInt3 = zzpx.readInt();
                    if (readInt3 == zziv.zzanb) {
                        obj2 = Integer.valueOf(zzpx.readInt());
                    } else if (readInt3 == zziv.zzamw) {
                        zzpx2.zzbl(4);
                        z2 = zzpx.readInt() == zzaoz;
                    } else if (readInt3 == zziv.zzamx) {
                        int i5 = i4 + 8;
                        while (true) {
                            if (i5 - i4 >= readInt2) {
                                obj = null;
                                break;
                            }
                            zzpx2.setPosition(i5);
                            int readInt4 = zzpx.readInt();
                            if (zzpx.readInt() == zziv.zzamy) {
                                zzpx2.zzbl(6);
                                boolean z3 = zzpx.readUnsignedByte() == 1;
                                int readUnsignedByte = zzpx.readUnsignedByte();
                                byte[] bArr = new byte[16];
                                zzpx2.zze(bArr, 0, 16);
                                obj = new zzjt(z3, readUnsignedByte, bArr);
                            } else {
                                i5 += readInt4;
                            }
                        }
                        obj3 = obj;
                    }
                    i4 += readInt2;
                }
                if (z2) {
                    zzpo.checkArgument(obj2 != null, "frma atom is mandatory");
                    if (obj3 != null) {
                        z = true;
                    }
                    zzpo.checkArgument(z, "schi->tenc atom is mandatory");
                    pair = Pair.create(obj2, obj3);
                }
                if (pair != null) {
                    zzjb.zzaph[i3] = (zzjt) pair.second;
                    return ((Integer) pair.first).intValue();
                }
            }
            zzjb zzjb2 = zzjb;
            position += readInt;
        }
    }

    private static int zzc(zzpx zzpx) {
        int readUnsignedByte = zzpx.readUnsignedByte();
        int i = readUnsignedByte & 127;
        while ((readUnsignedByte & 128) == 128) {
            readUnsignedByte = zzpx.readUnsignedByte();
            i = (i << 7) | (readUnsignedByte & 127);
        }
        return i;
    }
}
