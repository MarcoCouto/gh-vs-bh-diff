package com.google.android.gms.internal.ads;

final /* synthetic */ class zzboh {
    static final /* synthetic */ int[] zzfjd = new int[zzboj.values().length];
    static final /* synthetic */ int[] zzfje = new int[zzboi.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(14:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
    static {
        try {
            zzfje[zzboi.NIST_P256.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zzfje[zzboi.NIST_P384.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            zzfje[zzboi.NIST_P521.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        zzfjd[zzboj.UNCOMPRESSED.ordinal()] = 1;
        zzfjd[zzboj.DO_NOT_USE_CRUNCHY_UNCOMPRESSED.ordinal()] = 2;
        zzfjd[zzboj.COMPRESSED.ordinal()] = 3;
    }
}
