package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import com.google.android.exoplayer2.C;
import java.io.IOException;

final class zzfn implements Callback, zzln, zzlp, zzoq {
    private final Handler handler;
    private int repeatMode = 0;
    private int state;
    private final zzfz[] zzwu;
    private final zzop zzwv;
    private final Handler zzwx;
    private final zzgf zzxa;
    private final zzge zzxb;
    private boolean zzxd;
    private boolean zzxh;
    private zzgc zzxi;
    private zzfy zzxm;
    private zzfp zzxn;
    private final zzga[] zzxs;
    private final zzfw zzxt;
    private final zzqa zzxu;
    private final HandlerThread zzxv;
    private final zzfg zzxw;
    private zzfz zzxx;
    private zzps zzxy;
    private zzlo zzxz;
    private zzfz[] zzya;
    private boolean zzyb;
    private boolean zzyc;
    private int zzyd;
    private int zzye;
    private long zzyf;
    private int zzyg;
    private zzfq zzyh;
    private long zzyi;
    private zzfo zzyj;
    private zzfo zzyk;
    private zzfo zzyl;

    public zzfn(zzfz[] zzfzArr, zzop zzop, zzfw zzfw, boolean z, int i, Handler handler2, zzfp zzfp, zzfg zzfg) {
        this.zzwu = zzfzArr;
        this.zzwv = zzop;
        this.zzxt = zzfw;
        this.zzxd = z;
        this.zzwx = handler2;
        this.state = 1;
        this.zzxn = zzfp;
        this.zzxw = zzfg;
        this.zzxs = new zzga[zzfzArr.length];
        for (int i2 = 0; i2 < zzfzArr.length; i2++) {
            zzfzArr[i2].setIndex(i2);
            this.zzxs[i2] = zzfzArr[i2].zzbe();
        }
        this.zzxu = new zzqa();
        this.zzya = new zzfz[0];
        this.zzxa = new zzgf();
        this.zzxb = new zzge();
        zzop.zza(this);
        this.zzxm = zzfy.zzaaf;
        this.zzxv = new HandlerThread("ExoPlayerImplInternal:Handler", -16);
        this.zzxv.start();
        this.handler = new Handler(this.zzxv.getLooper(), this);
    }

    public final void zza(zzlo zzlo, boolean z) {
        this.handler.obtainMessage(0, 1, 0, zzlo).sendToTarget();
    }

    public final void zzc(boolean z) {
        this.handler.obtainMessage(1, z ? 1 : 0, 0).sendToTarget();
    }

    public final void zza(zzgc zzgc, int i, long j) {
        this.handler.obtainMessage(3, new zzfq(zzgc, i, j)).sendToTarget();
    }

    public final void stop() {
        this.handler.sendEmptyMessage(5);
    }

    public final void zza(zzfj... zzfjArr) {
        if (this.zzyb) {
            Log.w("ExoPlayerImplInternal", "Ignoring messages sent after release.");
            return;
        }
        this.zzyd++;
        this.handler.obtainMessage(11, zzfjArr).sendToTarget();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:11|12|13|14|23|20|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x001f, code lost:
        continue;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0027 */
    public final synchronized void zzb(zzfj... zzfjArr) {
        if (this.zzyb) {
            Log.w("ExoPlayerImplInternal", "Ignoring messages sent after release.");
            return;
        }
        int i = this.zzyd;
        this.zzyd = i + 1;
        this.handler.obtainMessage(11, zzfjArr).sendToTarget();
        while (this.zzye <= i) {
            wait();
            Thread.currentThread().interrupt();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:10|11|12|13|23|20|8) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x000d, code lost:
        continue;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0015 */
    public final synchronized void release() {
        if (!this.zzyb) {
            this.handler.sendEmptyMessage(6);
            while (!this.zzyb) {
                wait();
                Thread.currentThread().interrupt();
            }
            this.zzxv.quit();
        }
    }

    public final void zzb(zzgc zzgc, Object obj) {
        this.handler.obtainMessage(7, Pair.create(zzgc, obj)).sendToTarget();
    }

    public final void zza(zzlm zzlm) {
        this.handler.obtainMessage(8, zzlm).sendToTarget();
    }

    public final void zzbu() {
        this.handler.sendEmptyMessage(10);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:499:0x08bb, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:500:0x08bc, code lost:
        r1 = r0;
        android.util.Log.e("ExoPlayerImplInternal", "Internal runtime error.", r1);
        r8.zzwx.obtainMessage(8, com.google.android.gms.internal.ads.zzff.zza(r1)).sendToTarget();
        zzby();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:501:0x08d6, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:502:0x08d7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:503:0x08d8, code lost:
        r3 = 8;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:506:0x08f3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:507:0x08f4, code lost:
        r3 = 8;
        r1 = r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x028f A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x0292 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0296 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x0358 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x036c A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x0537 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:301:0x053e A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:304:0x0558 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:305:0x055b A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x0596 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x05aa A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x05c6 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }, LOOP:8: B:323:0x05c6->B:327:0x05d8, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:397:0x0728 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:436:0x07e1 A[Catch:{ all -> 0x044f, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzff -> 0x08b6, IOException -> 0x08b1, RuntimeException -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:499:0x08bb A[ExcHandler: RuntimeException (r0v2 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    public final boolean handleMessage(Message message) {
        int i;
        Throwable th;
        long j;
        zzfz[] zzfzArr;
        int i2;
        boolean z;
        long j2;
        boolean z2;
        boolean z3;
        int i3;
        long zzcb;
        int i4;
        int intValue;
        long longValue;
        zzfy zzfy;
        int i5;
        zzfo zzfo;
        boolean z4;
        zzfo zzfo2;
        boolean z5;
        Message message2 = message;
        try {
            long j3 = 0;
            int i6 = 0;
            switch (message2.what) {
                case 0:
                    zzlo zzlo = (zzlo) message2.obj;
                    boolean z6 = message2.arg1 != 0;
                    this.zzwx.sendEmptyMessage(0);
                    zzf(true);
                    this.zzxt.zzcg();
                    if (z6) {
                        this.zzxn = new zzfp(0, C.TIME_UNSET);
                    }
                    this.zzxz = zzlo;
                    zzlo.zza(this.zzxw, true, this);
                    setState(2);
                    this.handler.sendEmptyMessage(2);
                    return true;
                case 1:
                    boolean z7 = message2.arg1 != 0;
                    this.zzyc = false;
                    this.zzxd = z7;
                    if (!z7) {
                        zzbw();
                        zzbx();
                    } else if (this.state == 3) {
                        zzbv();
                        this.handler.sendEmptyMessage(2);
                    } else if (this.state == 2) {
                        this.handler.sendEmptyMessage(2);
                    }
                    return true;
                case 2:
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    if (this.zzxi == null) {
                        this.zzxz.zzfg();
                        j = elapsedRealtime;
                    } else {
                        if (this.zzyj == null) {
                            i3 = this.zzxn.zzyr;
                        } else {
                            int i7 = this.zzyj.zzyr;
                            if (!this.zzyj.zzyt && this.zzyj.zzcc()) {
                                if (this.zzxi.zza(i7, this.zzxb, false).zzaan != C.TIME_UNSET) {
                                    if (this.zzyl == null || this.zzyj.index - this.zzyl.index != 100) {
                                        i3 = this.zzxi.zza(i7, this.zzxb, this.zzxa, this.repeatMode);
                                    }
                                }
                            }
                            j = elapsedRealtime;
                            if (this.zzyj != null) {
                                if (!this.zzyj.zzcc()) {
                                    if (this.zzyj != null && !this.zzxh) {
                                        zzca();
                                    }
                                    if (this.zzyl != null) {
                                        while (this.zzyl != this.zzyk && this.zzyi >= this.zzyl.zzyw.zzyq) {
                                            this.zzyl.release();
                                            zzb(this.zzyl.zzyw);
                                            this.zzxn = new zzfp(this.zzyl.zzyr, this.zzyl.zzys);
                                            zzbx();
                                            this.zzwx.obtainMessage(5, this.zzxn).sendToTarget();
                                        }
                                        if (this.zzyk.zzyt) {
                                            for (int i8 = 0; i8 < this.zzwu.length; i8++) {
                                                zzfz zzfz = this.zzwu[i8];
                                                zzlv zzlv = this.zzyk.zzyo[i8];
                                                if (zzlv != null && zzfz.zzbg() == zzlv && zzfz.zzbh()) {
                                                    zzfz.zzbi();
                                                }
                                            }
                                        } else {
                                            int i9 = 0;
                                            while (true) {
                                                if (i9 < this.zzwu.length) {
                                                    zzfz zzfz2 = this.zzwu[i9];
                                                    zzlv zzlv2 = this.zzyk.zzyo[i9];
                                                    if (zzfz2.zzbg() == zzlv2) {
                                                        if (zzlv2 == null || zzfz2.zzbh()) {
                                                            i9++;
                                                        }
                                                    }
                                                } else if (this.zzyk.zzyw != null && this.zzyk.zzyw.zzyu) {
                                                    zzor zzor = this.zzyk.zzyx;
                                                    this.zzyk = this.zzyk.zzyw;
                                                    zzor zzor2 = this.zzyk.zzyx;
                                                    boolean z8 = this.zzyk.zzym.zzey() != C.TIME_UNSET;
                                                    for (int i10 = 0; i10 < this.zzwu.length; i10++) {
                                                        zzfz zzfz3 = this.zzwu[i10];
                                                        if (zzor.zzbfl.zzbe(i10) != null) {
                                                            if (!z8) {
                                                                if (!zzfz3.zzbj()) {
                                                                    zzom zzbe = zzor2.zzbfl.zzbe(i10);
                                                                    zzgb zzgb = zzor.zzbfn[i10];
                                                                    zzgb zzgb2 = zzor2.zzbfn[i10];
                                                                    if (zzbe != null && zzgb2.equals(zzgb)) {
                                                                        zzfs[] zzfsArr = new zzfs[zzbe.length()];
                                                                        for (int i11 = 0; i11 < zzfsArr.length; i11++) {
                                                                            zzfsArr[i11] = zzbe.zzat(i11);
                                                                        }
                                                                        zzfz3.zza(zzfsArr, this.zzyk.zzyo[i10], this.zzyk.zzcb());
                                                                    }
                                                                }
                                                            }
                                                            zzfz3.zzbi();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            zze(false);
                            if (this.zzyl != null) {
                            }
                        }
                        if (i3 >= this.zzxi.zzcl()) {
                            this.zzxz.zzfg();
                            j = elapsedRealtime;
                            if (this.zzyj != null) {
                            }
                            zze(false);
                            if (this.zzyl != null) {
                            }
                        } else {
                            if (this.zzyj == null) {
                                j3 = this.zzxn.zzyz;
                            } else {
                                this.zzxi.zza(i3, this.zzxb, false);
                                this.zzxi.zza(0, this.zzxa, false, 0);
                                if (i3 == 0) {
                                    j = elapsedRealtime;
                                    Pair zza = zza(this.zzxi, 0, C.TIME_UNSET, Math.max(0, (this.zzyj.zzcb() + this.zzxi.zza(this.zzyj.zzyr, this.zzxb, false).zzaan) - this.zzyi));
                                    if (zza != null) {
                                        int intValue2 = ((Integer) zza.first).intValue();
                                        j3 = ((Long) zza.second).longValue();
                                        i3 = intValue2;
                                        if (this.zzyj != null) {
                                            zzcb = 60000000 + j3;
                                        } else {
                                            zzcb = this.zzyj.zzcb() + this.zzxi.zza(this.zzyj.zzyr, this.zzxb, false).zzaan;
                                        }
                                        long j4 = zzcb;
                                        int i12 = this.zzyj != null ? 0 : this.zzyj.index + 1;
                                        boolean zzi = zzi(i3);
                                        this.zzxi.zza(i3, this.zzxb, true);
                                        zzfo zzfo3 = new zzfo(this.zzwu, this.zzxs, j4, this.zzwv, this.zzxt, this.zzxz, this.zzxb.zzyn, i12, i3, zzi, j3);
                                        if (this.zzyj != null) {
                                            this.zzyj.zzyw = zzfo3;
                                        }
                                        this.zzyj = zzfo3;
                                        this.zzyj.zzym.zza(this, j3);
                                        zze(true);
                                    }
                                    if (this.zzyj != null) {
                                    }
                                    zze(false);
                                    if (this.zzyl != null) {
                                    }
                                }
                            }
                            j = elapsedRealtime;
                            if (this.zzyj != null) {
                            }
                            long j42 = zzcb;
                            if (this.zzyj != null) {
                            }
                            boolean zzi2 = zzi(i3);
                            this.zzxi.zza(i3, this.zzxb, true);
                            zzfo zzfo32 = new zzfo(this.zzwu, this.zzxs, j42, this.zzwv, this.zzxt, this.zzxz, this.zzxb.zzyn, i12, i3, zzi2, j3);
                            if (this.zzyj != null) {
                            }
                            this.zzyj = zzfo32;
                            this.zzyj.zzym.zza(this, j3);
                            zze(true);
                            if (this.zzyj != null) {
                            }
                            zze(false);
                            if (this.zzyl != null) {
                            }
                        }
                    }
                    if (this.zzyl == null) {
                        zzbz();
                        zza(j, 10);
                    } else {
                        zzqc.beginSection("doSomeWork");
                        zzbx();
                        this.zzyl.zzym.zzaa(this.zzxn.zzyz);
                        boolean z9 = true;
                        boolean z10 = true;
                        for (zzfz zzfz4 : this.zzya) {
                            zzfz4.zzb(this.zzyi, this.zzyf);
                            z10 = z10 && zzfz4.zzcj();
                            if (!zzfz4.isReady()) {
                                if (!zzfz4.zzcj()) {
                                    z3 = false;
                                    if (!z3) {
                                        zzfz4.zzbk();
                                    }
                                    z9 = !z9 && z3;
                                }
                            }
                            z3 = true;
                            if (!z3) {
                            }
                            if (!z9) {
                            }
                        }
                        if (!z9) {
                            zzbz();
                        }
                        if (this.zzxy != null) {
                            zzfy zzcx = this.zzxy.zzcx();
                            if (!zzcx.equals(this.zzxm)) {
                                this.zzxm = zzcx;
                                this.zzxu.zza(this.zzxy);
                                this.zzwx.obtainMessage(7, zzcx).sendToTarget();
                            }
                        }
                        long j5 = this.zzxi.zza(this.zzyl.zzyr, this.zzxb, false).zzaan;
                        if (!z10 || ((j5 != C.TIME_UNSET && j5 > this.zzxn.zzyz) || !this.zzyl.zzyt)) {
                            i2 = 2;
                            if (this.state == 2) {
                                if (this.zzya.length > 0) {
                                    if (z9) {
                                        boolean z11 = this.zzyc;
                                        if (!this.zzyj.zzyu) {
                                            j2 = this.zzyj.zzys;
                                        } else {
                                            j2 = this.zzyj.zzym.zzez();
                                        }
                                        if (j2 == Long.MIN_VALUE) {
                                            if (this.zzyj.zzyt) {
                                                z2 = true;
                                                if (z2) {
                                                    z = true;
                                                }
                                            } else {
                                                j2 = this.zzxi.zza(this.zzyj.zzyr, this.zzxb, false).zzaan;
                                            }
                                        }
                                        z2 = this.zzxt.zzc(j2 - (this.zzyi - this.zzyj.zzcb()), z11);
                                        if (z2) {
                                        }
                                    }
                                    z = false;
                                } else {
                                    z = zzi(j5);
                                }
                                if (z) {
                                    setState(3);
                                    if (this.zzxd) {
                                        zzbv();
                                    }
                                }
                            } else if (this.state == 3) {
                                if (this.zzya.length <= 0) {
                                    z9 = zzi(j5);
                                }
                                if (!z9) {
                                    this.zzyc = this.zzxd;
                                    setState(2);
                                    zzbw();
                                }
                            }
                        } else {
                            setState(4);
                            zzbw();
                            i2 = 2;
                        }
                        if (this.state == i2) {
                            zzfz[] zzfzArr2 = this.zzya;
                            int length = zzfzArr2.length;
                            while (i6 < length) {
                                zzfzArr2[i6].zzbk();
                                i6++;
                            }
                        }
                        if (this.zzxd) {
                            if (this.state != 3) {
                            }
                            zza(j, 10);
                            zzqc.endSection();
                        }
                        if (this.state == i2) {
                            zza(j, 10);
                            zzqc.endSection();
                        } else {
                            if (this.zzya.length != 0) {
                                zza(j, 1000);
                            } else {
                                this.handler.removeMessages(i2);
                            }
                            zzqc.endSection();
                        }
                    }
                    return true;
                case 3:
                    zzfq zzfq = (zzfq) message2.obj;
                    if (this.zzxi == null) {
                        this.zzyg++;
                        this.zzyh = zzfq;
                    } else {
                        Pair zza2 = zza(zzfq);
                        if (zza2 == null) {
                            this.zzxn = new zzfp(0, 0);
                            this.zzwx.obtainMessage(4, 1, 0, this.zzxn).sendToTarget();
                            this.zzxn = new zzfp(0, C.TIME_UNSET);
                            setState(4);
                            zzf(false);
                        } else {
                            i4 = zzfq.zzzc == C.TIME_UNSET ? 1 : 0;
                            intValue = ((Integer) zza2.first).intValue();
                            longValue = ((Long) zza2.second).longValue();
                            if (intValue == this.zzxn.zzyr && longValue / 1000 == this.zzxn.zzyz / 1000) {
                                this.zzxn = new zzfp(intValue, longValue);
                                this.zzwx.obtainMessage(4, i4, 0, this.zzxn).sendToTarget();
                            } else {
                                long zza3 = zza(intValue, longValue);
                                int i13 = i4 | (longValue != zza3 ? 1 : 0);
                                this.zzxn = new zzfp(intValue, zza3);
                                this.zzwx.obtainMessage(4, i13 != 0 ? 1 : 0, 0, this.zzxn).sendToTarget();
                            }
                        }
                    }
                    return true;
                case 4:
                    zzfy zzfy2 = (zzfy) message2.obj;
                    if (this.zzxy != null) {
                        zzfy = this.zzxy.zzb(zzfy2);
                    } else {
                        zzfy = this.zzxu.zzb(zzfy2);
                    }
                    this.zzxm = zzfy;
                    this.zzwx.obtainMessage(7, zzfy).sendToTarget();
                    return true;
                case 5:
                    zzby();
                    return true;
                case 6:
                    zzf(true);
                    this.zzxt.zzch();
                    setState(1);
                    synchronized (this) {
                        this.zzyb = true;
                        notifyAll();
                    }
                    return true;
                case 7:
                    Pair pair = (Pair) message2.obj;
                    zzgc zzgc = this.zzxi;
                    this.zzxi = (zzgc) pair.first;
                    Object obj = pair.second;
                    if (zzgc == null) {
                        if (this.zzyg > 0) {
                            Pair zza4 = zza(this.zzyh);
                            i5 = this.zzyg;
                            this.zzyg = 0;
                            this.zzyh = null;
                            if (zza4 == null) {
                                zza(obj, i5);
                            } else {
                                this.zzxn = new zzfp(((Integer) zza4.first).intValue(), ((Long) zza4.second).longValue());
                                if (this.zzyl == null) {
                                    zzfo = this.zzyl;
                                } else {
                                    zzfo = this.zzyj;
                                }
                                if (zzfo != null) {
                                    int zzc = this.zzxi.zzc(zzfo.zzyn);
                                    if (zzc == -1) {
                                        int zza5 = zza(zzfo.zzyr, zzgc, this.zzxi);
                                        if (zza5 == -1) {
                                            zza(obj, i5);
                                        } else {
                                            this.zzxi.zza(zza5, this.zzxb, false);
                                            Pair zzb = zzb(0, (long) C.TIME_UNSET);
                                            int intValue3 = ((Integer) zzb.first).intValue();
                                            long longValue2 = ((Long) zzb.second).longValue();
                                            this.zzxi.zza(intValue3, this.zzxb, true);
                                            Object obj2 = this.zzxb.zzyn;
                                            zzfo.zzyr = -1;
                                            while (zzfo.zzyw != null) {
                                                zzfo = zzfo.zzyw;
                                                zzfo.zzyr = zzfo.zzyn.equals(obj2) ? intValue3 : -1;
                                            }
                                            this.zzxn = new zzfp(intValue3, zza(intValue3, longValue2));
                                        }
                                    } else {
                                        zzfo.zzc(zzc, zzi(zzc));
                                        boolean z12 = zzfo == this.zzyk;
                                        if (zzc != this.zzxn.zzyr) {
                                            zzfp zzfp = this.zzxn;
                                            zzfp zzfp2 = new zzfp(zzc, zzfp.zzys);
                                            zzfp2.zzyz = zzfp.zzyz;
                                            zzfp2.zzza = zzfp.zzza;
                                            this.zzxn = zzfp2;
                                        }
                                        while (true) {
                                            if (zzfo.zzyw != null) {
                                                zzfo zzfo4 = zzfo.zzyw;
                                                zzc = this.zzxi.zza(zzc, this.zzxb, this.zzxa, this.repeatMode);
                                                if (zzc != -1 && zzfo4.zzyn.equals(this.zzxi.zza(zzc, this.zzxb, true).zzyn)) {
                                                    zzfo4.zzc(zzc, zzi(zzc));
                                                    z12 |= zzfo4 == this.zzyk;
                                                    zzfo = zzfo4;
                                                } else if (z12) {
                                                    int i14 = this.zzyl.zzyr;
                                                    this.zzxn = new zzfp(i14, zza(i14, this.zzxn.zzyz));
                                                } else {
                                                    this.zzyj = zzfo;
                                                    this.zzyj.zzyw = null;
                                                    zza(zzfo4);
                                                }
                                            }
                                        }
                                        if (z12) {
                                        }
                                    }
                                }
                                zzb(obj, i5);
                            }
                        } else if (this.zzxn.zzys == C.TIME_UNSET) {
                            if (this.zzxi.isEmpty()) {
                                zza(obj, 0);
                            } else {
                                Pair zzb2 = zzb(0, (long) C.TIME_UNSET);
                                this.zzxn = new zzfp(((Integer) zzb2.first).intValue(), ((Long) zzb2.second).longValue());
                            }
                        }
                        return true;
                    }
                    i5 = 0;
                    if (this.zzyl == null) {
                    }
                    if (zzfo != null) {
                    }
                    zzb(obj, i5);
                    return true;
                case 8:
                    zzlm zzlm = (zzlm) message2.obj;
                    if (this.zzyj != null) {
                        if (this.zzyj.zzym == zzlm) {
                            zzfo zzfo5 = this.zzyj;
                            zzfo5.zzyu = true;
                            zzfo5.zzcd();
                            zzfo5.zzys = zzfo5.zzb(zzfo5.zzys, false);
                            if (this.zzyl == null) {
                                this.zzyk = this.zzyj;
                                zzh(this.zzyk.zzys);
                                zzb(this.zzyk);
                            }
                            zzca();
                        }
                    }
                    return true;
                case 9:
                    zzlm zzlm2 = (zzlm) message2.obj;
                    if (this.zzyj != null) {
                        if (this.zzyj.zzym == zzlm2) {
                            zzca();
                        }
                    }
                    return true;
                case 10:
                    if (this.zzyl != null) {
                        zzfo zzfo6 = this.zzyl;
                        boolean z13 = true;
                        while (true) {
                            if (zzfo6 != null) {
                                if (zzfo6.zzyu) {
                                    if (!zzfo6.zzcd()) {
                                        if (zzfo6 == this.zzyk) {
                                            z13 = false;
                                        }
                                        zzfo6 = zzfo6.zzyw;
                                    } else {
                                        if (z13) {
                                            boolean z14 = this.zzyk != this.zzyl;
                                            zza(this.zzyl.zzyw);
                                            this.zzyl.zzyw = null;
                                            this.zzyj = this.zzyl;
                                            this.zzyk = this.zzyl;
                                            boolean[] zArr = new boolean[this.zzwu.length];
                                            long zza6 = this.zzyl.zza(this.zzxn.zzyz, z14, zArr);
                                            if (zza6 != this.zzxn.zzyz) {
                                                this.zzxn.zzyz = zza6;
                                                zzh(zza6);
                                            }
                                            boolean[] zArr2 = new boolean[this.zzwu.length];
                                            int i15 = 0;
                                            for (int i16 = 0; i16 < this.zzwu.length; i16++) {
                                                zzfz zzfz5 = this.zzwu[i16];
                                                zArr2[i16] = zzfz5.getState() != 0;
                                                zzlv zzlv3 = this.zzyl.zzyo[i16];
                                                if (zzlv3 != null) {
                                                    i15++;
                                                }
                                                if (zArr2[i16]) {
                                                    if (zzlv3 != zzfz5.zzbg()) {
                                                        if (zzfz5 == this.zzxx) {
                                                            if (zzlv3 == null) {
                                                                this.zzxu.zza(this.zzxy);
                                                            }
                                                            this.zzxy = null;
                                                            this.zzxx = null;
                                                        }
                                                        zza(zzfz5);
                                                        zzfz5.disable();
                                                    } else if (zArr[i16]) {
                                                        zzfz5.zzd(this.zzyi);
                                                    }
                                                }
                                            }
                                            this.zzwx.obtainMessage(3, zzfo6.zzyx).sendToTarget();
                                            zza(zArr2, i15);
                                        } else {
                                            this.zzyj = zzfo6;
                                            for (zzfo zzfo7 = this.zzyj.zzyw; zzfo7 != null; zzfo7 = zzfo7.zzyw) {
                                                zzfo7.release();
                                            }
                                            this.zzyj.zzyw = null;
                                            if (this.zzyj.zzyu) {
                                                this.zzyj.zzb(Math.max(this.zzyj.zzys, this.zzyi - this.zzyj.zzcb()), false);
                                            }
                                        }
                                        zzca();
                                        zzbx();
                                        this.handler.sendEmptyMessage(2);
                                    }
                                }
                            }
                        }
                    }
                    return true;
                case 11:
                    zzfj[] zzfjArr = (zzfj[]) message2.obj;
                    int length2 = zzfjArr.length;
                    while (i6 < length2) {
                        zzfj zzfj = zzfjArr[i6];
                        zzfj.zzwr.zza(zzfj.zzws, zzfj.zzwt);
                        i6++;
                    }
                    if (this.zzxz != null) {
                        this.handler.sendEmptyMessage(2);
                    }
                    synchronized (this) {
                        this.zzye++;
                        notifyAll();
                    }
                    return true;
                case 12:
                    int i17 = message2.arg1;
                    this.repeatMode = i17;
                    zzfo zzfo8 = this.zzyl != null ? this.zzyl : this.zzyj;
                    if (zzfo8 != null) {
                        boolean z15 = zzfo8 == this.zzyk;
                        if (zzfo8 == this.zzyj) {
                            z4 = z15;
                            zzfo2 = zzfo8;
                            z5 = true;
                        } else {
                            z4 = z15;
                            zzfo2 = zzfo8;
                            z5 = false;
                        }
                        while (true) {
                            int zza7 = this.zzxi.zza(zzfo2.zzyr, this.zzxb, this.zzxa, i17);
                            if (zzfo2.zzyw != null && zza7 != -1 && zzfo2.zzyw.zzyr == zza7) {
                                zzfo2 = zzfo2.zzyw;
                                z4 |= zzfo2 == this.zzyk;
                                z5 |= zzfo2 == this.zzyj;
                            }
                        }
                        if (zzfo2.zzyw != null) {
                            zza(zzfo2.zzyw);
                            zzfo2.zzyw = null;
                        }
                        zzfo2.zzyt = zzi(zzfo2.zzyr);
                        if (!z5) {
                            this.zzyj = zzfo2;
                        }
                        if (!z4 && this.zzyl != null) {
                            int i18 = this.zzyl.zzyr;
                            this.zzxn = new zzfp(i18, zza(i18, this.zzxn.zzyz));
                        }
                        if (this.state == 4 && i17 != 0) {
                            setState(2);
                        }
                    }
                    return true;
                default:
                    return false;
            }
        } catch (zzff e) {
            th = e;
            i = 8;
        } catch (IOException e2) {
            IOException iOException = e2;
            int i19 = 8;
            Log.e("ExoPlayerImplInternal", "Source error.", iOException);
            this.zzwx.obtainMessage(i19, zzff.zza(iOException)).sendToTarget();
            zzby();
            return true;
        } catch (RuntimeException e3) {
        } catch (Throwable th2) {
            throw th2;
        }
        Log.e("ExoPlayerImplInternal", "Renderer error.", th);
        this.zzwx.obtainMessage(i, th).sendToTarget();
        zzby();
        return true;
    }

    private final void setState(int i) {
        if (this.state != i) {
            this.state = i;
            this.zzwx.obtainMessage(1, i, 0).sendToTarget();
        }
    }

    private final void zze(boolean z) {
        if (this.zzxh != z) {
            this.zzxh = z;
            this.zzwx.obtainMessage(2, z ? 1 : 0, 0).sendToTarget();
        }
    }

    private final void zzbv() throws zzff {
        this.zzyc = false;
        this.zzxu.start();
        for (zzfz start : this.zzya) {
            start.start();
        }
    }

    private final void zzbw() throws zzff {
        this.zzxu.stop();
        for (zzfz zza : this.zzya) {
            zza(zza);
        }
    }

    private final void zzbx() throws zzff {
        long j;
        if (this.zzyl != null) {
            long zzey = this.zzyl.zzym.zzey();
            if (zzey != C.TIME_UNSET) {
                zzh(zzey);
            } else {
                if (this.zzxx == null || this.zzxx.zzcj()) {
                    this.zzyi = this.zzxu.zzde();
                } else {
                    this.zzyi = this.zzxy.zzde();
                    this.zzxu.zzam(this.zzyi);
                }
                zzey = this.zzyi - this.zzyl.zzcb();
            }
            this.zzxn.zzyz = zzey;
            this.zzyf = SystemClock.elapsedRealtime() * 1000;
            if (this.zzya.length == 0) {
                j = Long.MIN_VALUE;
            } else {
                j = this.zzyl.zzym.zzez();
            }
            zzfp zzfp = this.zzxn;
            if (j == Long.MIN_VALUE) {
                j = this.zzxi.zza(this.zzyl.zzyr, this.zzxb, false).zzaan;
            }
            zzfp.zzza = j;
        }
    }

    private final void zza(long j, long j2) {
        this.handler.removeMessages(2);
        long elapsedRealtime = (j + j2) - SystemClock.elapsedRealtime();
        if (elapsedRealtime <= 0) {
            this.handler.sendEmptyMessage(2);
        } else {
            this.handler.sendEmptyMessageDelayed(2, elapsedRealtime);
        }
    }

    private final long zza(int i, long j) throws zzff {
        zzfo zzfo;
        zzbw();
        this.zzyc = false;
        setState(2);
        if (this.zzyl == null) {
            if (this.zzyj != null) {
                this.zzyj.release();
            }
            zzfo = null;
        } else {
            zzfo = null;
            for (zzfo zzfo2 = this.zzyl; zzfo2 != null; zzfo2 = zzfo2.zzyw) {
                if (zzfo2.zzyr != i || !zzfo2.zzyu) {
                    zzfo2.release();
                } else {
                    zzfo = zzfo2;
                }
            }
        }
        if (!(this.zzyl == zzfo && this.zzyl == this.zzyk)) {
            for (zzfz disable : this.zzya) {
                disable.disable();
            }
            this.zzya = new zzfz[0];
            this.zzxy = null;
            this.zzxx = null;
            this.zzyl = null;
        }
        if (zzfo != null) {
            zzfo.zzyw = null;
            this.zzyj = zzfo;
            this.zzyk = zzfo;
            zzb(zzfo);
            if (this.zzyl.zzyv) {
                j = this.zzyl.zzym.zzab(j);
            }
            zzh(j);
            zzca();
        } else {
            this.zzyj = null;
            this.zzyk = null;
            this.zzyl = null;
            zzh(j);
        }
        this.handler.sendEmptyMessage(2);
        return j;
    }

    private final void zzh(long j) throws zzff {
        long j2;
        if (this.zzyl == null) {
            j2 = j + 60000000;
        } else {
            j2 = j + this.zzyl.zzcb();
        }
        this.zzyi = j2;
        this.zzxu.zzam(this.zzyi);
        for (zzfz zzd : this.zzya) {
            zzd.zzd(this.zzyi);
        }
    }

    private final void zzby() {
        zzf(true);
        this.zzxt.onStopped();
        setState(1);
    }

    private final void zzf(boolean z) {
        zzfz[] zzfzArr;
        zzfo zzfo;
        this.handler.removeMessages(2);
        this.zzyc = false;
        this.zzxu.stop();
        this.zzxy = null;
        this.zzxx = null;
        this.zzyi = 60000000;
        for (zzfz zzfz : this.zzya) {
            try {
                zza(zzfz);
                zzfz.disable();
            } catch (zzff | RuntimeException e) {
                Log.e("ExoPlayerImplInternal", "Stop failed.", e);
            }
        }
        this.zzya = new zzfz[0];
        if (this.zzyl != null) {
            zzfo = this.zzyl;
        } else {
            zzfo = this.zzyj;
        }
        zza(zzfo);
        this.zzyj = null;
        this.zzyk = null;
        this.zzyl = null;
        zze(false);
        if (z) {
            if (this.zzxz != null) {
                this.zzxz.zzfh();
                this.zzxz = null;
            }
            this.zzxi = null;
        }
    }

    private static void zza(zzfz zzfz) throws zzff {
        if (zzfz.getState() == 2) {
            zzfz.stop();
        }
    }

    private final boolean zzi(long j) {
        return j == C.TIME_UNSET || this.zzxn.zzyz < j || (this.zzyl.zzyw != null && this.zzyl.zzyw.zzyu);
    }

    private final void zzbz() throws IOException {
        if (this.zzyj != null && !this.zzyj.zzyu && (this.zzyk == null || this.zzyk.zzyw == this.zzyj)) {
            zzfz[] zzfzArr = this.zzya;
            int length = zzfzArr.length;
            int i = 0;
            while (i < length) {
                if (zzfzArr[i].zzbh()) {
                    i++;
                } else {
                    return;
                }
            }
            this.zzyj.zzym.zzew();
        }
    }

    private final void zza(Object obj, int i) {
        this.zzxn = new zzfp(0, 0);
        zzb(obj, i);
        this.zzxn = new zzfp(0, C.TIME_UNSET);
        setState(4);
        zzf(false);
    }

    private final void zzb(Object obj, int i) {
        this.zzwx.obtainMessage(6, new zzfr(this.zzxi, obj, this.zzxn, i)).sendToTarget();
    }

    private final int zza(int i, zzgc zzgc, zzgc zzgc2) {
        int zzcl = zzgc.zzcl();
        int i2 = i;
        int i3 = -1;
        for (int i4 = 0; i4 < zzcl && i3 == -1; i4++) {
            i2 = zzgc.zza(i2, this.zzxb, this.zzxa, this.repeatMode);
            i3 = zzgc2.zzc(zzgc.zza(i2, this.zzxb, true).zzyn);
        }
        return i3;
    }

    private final boolean zzi(int i) {
        this.zzxi.zza(i, this.zzxb, false);
        if (this.zzxi.zza(0, this.zzxa, false, 0).zzaat || this.zzxi.zza(i, this.zzxb, this.zzxa, this.repeatMode) != -1) {
            return false;
        }
        return true;
    }

    private final Pair<Integer, Long> zza(zzfq zzfq) {
        zzgc zzgc = zzfq.zzxi;
        if (zzgc.isEmpty()) {
            zzgc = this.zzxi;
        }
        try {
            Pair<Integer, Long> zzb = zzb(zzgc, zzfq.zzzb, zzfq.zzzc);
            if (this.zzxi == zzgc) {
                return zzb;
            }
            int zzc = this.zzxi.zzc(zzgc.zza(((Integer) zzb.first).intValue(), this.zzxb, true).zzyn);
            if (zzc != -1) {
                return Pair.create(Integer.valueOf(zzc), (Long) zzb.second);
            }
            int zza = zza(((Integer) zzb.first).intValue(), zzgc, this.zzxi);
            if (zza == -1) {
                return null;
            }
            this.zzxi.zza(zza, this.zzxb, false);
            return zzb(0, (long) C.TIME_UNSET);
        } catch (IndexOutOfBoundsException unused) {
            throw new zzfv(this.zzxi, zzfq.zzzb, zzfq.zzzc);
        }
    }

    private final Pair<Integer, Long> zzb(int i, long j) {
        return zzb(this.zzxi, i, C.TIME_UNSET);
    }

    private final Pair<Integer, Long> zzb(zzgc zzgc, int i, long j) {
        return zza(zzgc, i, j, 0);
    }

    private final Pair<Integer, Long> zza(zzgc zzgc, int i, long j, long j2) {
        zzpo.zzc(i, 0, zzgc.zzck());
        zzgc.zza(i, this.zzxa, false, j2);
        if (j == C.TIME_UNSET) {
            j = this.zzxa.zzaaw;
            if (j == C.TIME_UNSET) {
                return null;
            }
        }
        long j3 = this.zzxa.zzaax + j;
        long j4 = zzgc.zza(0, this.zzxb, false).zzaan;
        int i2 = 0;
        while (j4 != C.TIME_UNSET && j3 >= j4 && i2 < this.zzxa.zzaav) {
            j3 -= j4;
            i2++;
            j4 = zzgc.zza(i2, this.zzxb, false).zzaan;
        }
        return Pair.create(Integer.valueOf(i2), Long.valueOf(j3));
    }

    private final void zzca() {
        long j;
        if (!this.zzyj.zzyu) {
            j = 0;
        } else {
            j = this.zzyj.zzym.zzeu();
        }
        if (j == Long.MIN_VALUE) {
            zze(false);
            return;
        }
        long zzcb = this.zzyi - this.zzyj.zzcb();
        boolean zzk = this.zzxt.zzk(j - zzcb);
        zze(zzk);
        if (zzk) {
            this.zzyj.zzym.zzy(zzcb);
        }
    }

    private static void zza(zzfo zzfo) {
        while (zzfo != null) {
            zzfo.release();
            zzfo = zzfo.zzyw;
        }
    }

    private final void zzb(zzfo zzfo) throws zzff {
        if (this.zzyl != zzfo) {
            boolean[] zArr = new boolean[this.zzwu.length];
            int i = 0;
            for (int i2 = 0; i2 < this.zzwu.length; i2++) {
                zzfz zzfz = this.zzwu[i2];
                zArr[i2] = zzfz.getState() != 0;
                zzom zzbe = zzfo.zzyx.zzbfl.zzbe(i2);
                if (zzbe != null) {
                    i++;
                }
                if (zArr[i2] && (zzbe == null || (zzfz.zzbj() && zzfz.zzbg() == this.zzyl.zzyo[i2]))) {
                    if (zzfz == this.zzxx) {
                        this.zzxu.zza(this.zzxy);
                        this.zzxy = null;
                        this.zzxx = null;
                    }
                    zza(zzfz);
                    zzfz.disable();
                }
            }
            this.zzyl = zzfo;
            this.zzwx.obtainMessage(3, zzfo.zzyx).sendToTarget();
            zza(zArr, i);
        }
    }

    private final void zza(boolean[] zArr, int i) throws zzff {
        this.zzya = new zzfz[i];
        int i2 = 0;
        for (int i3 = 0; i3 < this.zzwu.length; i3++) {
            zzfz zzfz = this.zzwu[i3];
            zzom zzbe = this.zzyl.zzyx.zzbfl.zzbe(i3);
            if (zzbe != null) {
                int i4 = i2 + 1;
                this.zzya[i2] = zzfz;
                if (zzfz.getState() == 0) {
                    zzgb zzgb = this.zzyl.zzyx.zzbfn[i3];
                    boolean z = this.zzxd && this.state == 3;
                    boolean z2 = !zArr[i3] && z;
                    zzfs[] zzfsArr = new zzfs[zzbe.length()];
                    for (int i5 = 0; i5 < zzfsArr.length; i5++) {
                        zzfsArr[i5] = zzbe.zzat(i5);
                    }
                    zzfz.zza(zzgb, zzfsArr, this.zzyl.zzyo[i3], this.zzyi, z2, this.zzyl.zzcb());
                    zzps zzbf = zzfz.zzbf();
                    if (zzbf != null) {
                        if (this.zzxy != null) {
                            throw zzff.zza((RuntimeException) new IllegalStateException("Multiple renderer media clocks enabled."));
                        }
                        this.zzxy = zzbf;
                        this.zzxx = zzfz;
                        this.zzxy.zzb(this.zzxm);
                    }
                    if (z) {
                        zzfz.start();
                    }
                }
                i2 = i4;
            }
        }
    }

    public final /* synthetic */ void zza(zzlw zzlw) {
        this.handler.obtainMessage(9, (zzlm) zzlw).sendToTarget();
    }
}
