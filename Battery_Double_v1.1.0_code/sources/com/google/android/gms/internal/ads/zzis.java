package com.google.android.gms.internal.ads;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.Log;
import com.google.android.exoplayer2.util.MimeTypes;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class zzis {
    public int height;
    public int number;
    public int type;
    public int width;
    /* access modifiers changed from: private */
    public String zzaaa;
    public String zzajs;
    public int zzajt;
    public boolean zzaju;
    public byte[] zzajv;
    public zzij zzajw;
    public byte[] zzajx;
    public int zzajy;
    public int zzajz;
    public int zzaka;
    public boolean zzakb;
    public int zzakc;
    public int zzakd;
    public int zzake;
    public int zzakf;
    public int zzakg;
    public float zzakh;
    public float zzaki;
    public float zzakj;
    public float zzakk;
    public float zzakl;
    public float zzakm;
    public float zzakn;
    public float zzako;
    public float zzakp;
    public float zzakq;
    public int zzakr;
    public long zzaks;
    public long zzakt;
    public boolean zzaku;
    public boolean zzakv;
    public zzii zzakw;
    public int zzakx;
    public zzhp zzzm;
    public int zzzq;
    public byte[] zzzr;
    public int zzzt;
    public int zzzu;

    private zzis() {
        this.width = -1;
        this.height = -1;
        this.zzajy = -1;
        this.zzajz = -1;
        this.zzaka = 0;
        this.zzzr = null;
        this.zzzq = -1;
        this.zzakb = false;
        this.zzakc = -1;
        this.zzakd = -1;
        this.zzake = -1;
        this.zzakf = 1000;
        this.zzakg = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        this.zzakh = -1.0f;
        this.zzaki = -1.0f;
        this.zzakj = -1.0f;
        this.zzakk = -1.0f;
        this.zzakl = -1.0f;
        this.zzakm = -1.0f;
        this.zzakn = -1.0f;
        this.zzako = -1.0f;
        this.zzakp = -1.0f;
        this.zzakq = -1.0f;
        this.zzzt = 1;
        this.zzakr = -1;
        this.zzzu = 8000;
        this.zzaks = 0;
        this.zzakt = 0;
        this.zzakv = true;
        this.zzaaa = "eng";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:117:0x025d, code lost:
        r14 = r1;
        r17 = 4096;
        r20 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x026c, code lost:
        r14 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x02ba, code lost:
        r14 = r1;
        r17 = r2;
        r2 = r3;
        r20 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x030d, code lost:
        r14 = r1;
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x030f, code lost:
        r17 = -1;
        r20 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x032d, code lost:
        r14 = r1;
        r17 = -1;
        r20 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0332, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0333, code lost:
        r1 = r0.zzakv | false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0338, code lost:
        if (r0.zzaku == false) goto L_0x033c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x033a, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x033c, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x033d, code lost:
        r1 = r1 | r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0342, code lost:
        if (com.google.android.gms.internal.ads.zzpt.zzab(r14) == false) goto L_0x0366;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0344, code lost:
        r1 = com.google.android.gms.internal.ads.zzfs.zza(java.lang.Integer.toString(r30), r14, null, -1, r17, r0.zzzt, r0.zzzu, r20, r2, r0.zzzm, r1 ? 1 : 0, r0.zzaaa);
        r8 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x036a, code lost:
        if (com.google.android.gms.internal.ads.zzpt.zzac(r14) == false) goto L_0x048c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x036e, code lost:
        if (r0.zzaka != 0) goto L_0x0386;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0372, code lost:
        if (r0.zzajy != -1) goto L_0x0377;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0374, code lost:
        r1 = r0.width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0377, code lost:
        r1 = r0.zzajy;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0379, code lost:
        r0.zzajy = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x037d, code lost:
        if (r0.zzajz != -1) goto L_0x0382;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x037f, code lost:
        r1 = r0.height;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x0382, code lost:
        r1 = r0.zzajz;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0384, code lost:
        r0.zzajz = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x038a, code lost:
        if (r0.zzajy == -1) goto L_0x03a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x038e, code lost:
        if (r0.zzajz == -1) goto L_0x03a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0390, code lost:
        r23 = ((float) (r0.height * r0.zzajy)) / ((float) (r0.width * r0.zzajz));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x03a0, code lost:
        r23 = -1.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03a4, code lost:
        if (r0.zzakb == false) goto L_0x0463;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03aa, code lost:
        if (r0.zzakh == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03b0, code lost:
        if (r0.zzaki == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x03b6, code lost:
        if (r0.zzakj == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03bc, code lost:
        if (r0.zzakk == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x03c2, code lost:
        if (r0.zzakl == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x03c8, code lost:
        if (r0.zzakm == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x03ce, code lost:
        if (r0.zzakn == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x03d4, code lost:
        if (r0.zzako == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x03da, code lost:
        if (r0.zzakp == -1.0f) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x03e0, code lost:
        if (r0.zzakq != -1.0f) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x03e3, code lost:
        r12 = new byte[25];
        r1 = java.nio.ByteBuffer.wrap(r12);
        r1.put(0);
        r1.putShort((short) ((int) ((r0.zzakh * 50000.0f) + 0.5f)));
        r1.putShort((short) ((int) ((r0.zzaki * 50000.0f) + 0.5f)));
        r1.putShort((short) ((int) ((r0.zzakj * 50000.0f) + 0.5f)));
        r1.putShort((short) ((int) ((r0.zzakk * 50000.0f) + 0.5f)));
        r1.putShort((short) ((int) ((r0.zzakl * 50000.0f) + 0.5f)));
        r1.putShort((short) ((int) ((r0.zzakm * 50000.0f) + 0.5f)));
        r1.putShort((short) ((int) ((r0.zzakn * 50000.0f) + 0.5f)));
        r1.putShort((short) ((int) ((r0.zzako * 50000.0f) + 0.5f)));
        r1.putShort((short) ((int) (r0.zzakp + 0.5f)));
        r1.putShort((short) ((int) (r0.zzakq + 0.5f)));
        r1.putShort((short) r0.zzakf);
        r1.putShort((short) r0.zzakg);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0455, code lost:
        r26 = new com.google.android.gms.internal.ads.zzqi(r0.zzakc, r0.zzake, r0.zzakd, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x0463, code lost:
        r26 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x0465, code lost:
        r1 = com.google.android.gms.internal.ads.zzfs.zza(java.lang.Integer.toString(r30), r14, null, -1, r17, r0.width, r0.height, -1.0f, r2, -1, r23, r0.zzzr, r0.zzzq, r26, r0.zzzm);
        r8 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0492, code lost:
        if (com.google.android.exoplayer2.util.MimeTypes.APPLICATION_SUBRIP.equals(r14) == false) goto L_0x04aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x0494, code lost:
        r1 = com.google.android.gms.internal.ads.zzfs.zza(java.lang.Integer.toString(r30), r14, (java.lang.String) null, -1, r1 ? 1 : 0, r0.zzaaa, r0.zzzm);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x04b0, code lost:
        if (com.google.android.exoplayer2.util.MimeTypes.APPLICATION_VOBSUB.equals(r14) != false) goto L_0x04cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x04b8, code lost:
        if (com.google.android.exoplayer2.util.MimeTypes.APPLICATION_PGS.equals(r14) != false) goto L_0x04cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x04c0, code lost:
        if (com.google.android.exoplayer2.util.MimeTypes.APPLICATION_DVBSUBS.equals(r14) == false) goto L_0x04c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x04ca, code lost:
        throw new com.google.android.gms.internal.ads.zzfx("Unexpected MIME type.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x04cb, code lost:
        r1 = com.google.android.gms.internal.ads.zzfs.zza(java.lang.Integer.toString(r30), r14, (java.lang.String) null, -1, r2, r0.zzaaa, r0.zzzm);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x04e0, code lost:
        r0.zzakw = r29.zzb(r0.number, r8);
        r0.zzakw.zzf(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x04ef, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01d0, code lost:
        r14 = r1;
        r20 = r2;
        r17 = -1;
     */
    public final void zza(zzib zzib, int i) throws zzfx {
        char c;
        List list;
        String str;
        String str2;
        List<byte[]> list2;
        String str3;
        String str4;
        List list3;
        int i2;
        String str5;
        String str6;
        String str7;
        int zzbp;
        String str8 = this.zzajs;
        int i3 = 3;
        switch (str8.hashCode()) {
            case -2095576542:
                if (str8.equals("V_MPEG4/ISO/AP")) {
                    c = 5;
                    break;
                }
            case -2095575984:
                if (str8.equals("V_MPEG4/ISO/SP")) {
                    c = 3;
                    break;
                }
            case -1985379776:
                if (str8.equals("A_MS/ACM")) {
                    c = 22;
                    break;
                }
            case -1784763192:
                if (str8.equals("A_TRUEHD")) {
                    c = 17;
                    break;
                }
            case -1730367663:
                if (str8.equals("A_VORBIS")) {
                    c = 10;
                    break;
                }
            case -1482641358:
                if (str8.equals("A_MPEG/L2")) {
                    c = 13;
                    break;
                }
            case -1482641357:
                if (str8.equals("A_MPEG/L3")) {
                    c = 14;
                    break;
                }
            case -1373388978:
                if (str8.equals("V_MS/VFW/FOURCC")) {
                    c = 8;
                    break;
                }
            case -933872740:
                if (str8.equals("S_DVBSUB")) {
                    c = 27;
                    break;
                }
            case -538363189:
                if (str8.equals("V_MPEG4/ISO/ASP")) {
                    c = 4;
                    break;
                }
            case -538363109:
                if (str8.equals("V_MPEG4/ISO/AVC")) {
                    c = 6;
                    break;
                }
            case -425012669:
                if (str8.equals("S_VOBSUB")) {
                    c = 25;
                    break;
                }
            case -356037306:
                if (str8.equals("A_DTS/LOSSLESS")) {
                    c = 20;
                    break;
                }
            case 62923557:
                if (str8.equals("A_AAC")) {
                    c = 12;
                    break;
                }
            case 62923603:
                if (str8.equals("A_AC3")) {
                    c = 15;
                    break;
                }
            case 62927045:
                if (str8.equals("A_DTS")) {
                    c = 18;
                    break;
                }
            case 82338133:
                if (str8.equals("V_VP8")) {
                    c = 0;
                    break;
                }
            case 82338134:
                if (str8.equals("V_VP9")) {
                    c = 1;
                    break;
                }
            case 99146302:
                if (str8.equals("S_HDMV/PGS")) {
                    c = 26;
                    break;
                }
            case 444813526:
                if (str8.equals("V_THEORA")) {
                    c = 9;
                    break;
                }
            case 542569478:
                if (str8.equals("A_DTS/EXPRESS")) {
                    c = 19;
                    break;
                }
            case 725957860:
                if (str8.equals("A_PCM/INT/LIT")) {
                    c = 23;
                    break;
                }
            case 855502857:
                if (str8.equals("V_MPEGH/ISO/HEVC")) {
                    c = 7;
                    break;
                }
            case 1422270023:
                if (str8.equals("S_TEXT/UTF8")) {
                    c = 24;
                    break;
                }
            case 1809237540:
                if (str8.equals("V_MPEG2")) {
                    c = 2;
                    break;
                }
            case 1950749482:
                if (str8.equals("A_EAC3")) {
                    c = 16;
                    break;
                }
            case 1950789798:
                if (str8.equals("A_FLAC")) {
                    c = 21;
                    break;
                }
            case 1951062397:
                if (str8.equals("A_OPUS")) {
                    c = 11;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        byte[] bArr = null;
        switch (c) {
            case 0:
                str = MimeTypes.VIDEO_VP8;
                break;
            case 1:
                str = MimeTypes.VIDEO_VP9;
                break;
            case 2:
                str = MimeTypes.VIDEO_MPEG2;
                break;
            case 3:
            case 4:
            case 5:
                str2 = MimeTypes.VIDEO_MP4V;
                if (this.zzajx != null) {
                    list = Collections.singletonList(this.zzajx);
                    break;
                } else {
                    list = null;
                    break;
                }
            case 6:
                str3 = MimeTypes.VIDEO_H264;
                zzqh zzg = zzqh.zzg(new zzpx(this.zzajx));
                list2 = zzg.zzzl;
                this.zzakx = zzg.zzakx;
                break;
            case 7:
                str3 = MimeTypes.VIDEO_H265;
                zzqn zzi = zzqn.zzi(new zzpx(this.zzajx));
                list2 = zzi.zzzl;
                this.zzakx = zzi.zzakx;
                break;
            case 8:
                List zza = zza(new zzpx(this.zzajx));
                if (zza != null) {
                    str4 = MimeTypes.VIDEO_VC1;
                } else {
                    Log.w("MatroskaExtractor", "Unsupported FourCC. Setting mimeType to video/x-unknown");
                    str4 = MimeTypes.VIDEO_UNKNOWN;
                }
                String str9 = str4;
                int i4 = -1;
                int i5 = -1;
                list = zza;
                break;
            case 9:
                str = MimeTypes.VIDEO_UNKNOWN;
                break;
            case 10:
                str5 = MimeTypes.AUDIO_VORBIS;
                i2 = 8192;
                list3 = zzd(this.zzajx);
                break;
            case 11:
                str5 = MimeTypes.AUDIO_OPUS;
                i2 = 5760;
                list3 = new ArrayList(3);
                list3.add(this.zzajx);
                list3.add(ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong(this.zzaks).array());
                list3.add(ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong(this.zzakt).array());
                break;
            case 12:
                str2 = MimeTypes.AUDIO_AAC;
                list = Collections.singletonList(this.zzajx);
                break;
            case 13:
                str6 = MimeTypes.AUDIO_MPEG_L2;
                break;
            case 14:
                str6 = MimeTypes.AUDIO_MPEG;
                break;
            case 15:
                str = MimeTypes.AUDIO_AC3;
                break;
            case 16:
                str = MimeTypes.AUDIO_E_AC3;
                break;
            case 17:
                str = MimeTypes.AUDIO_TRUEHD;
                break;
            case 18:
            case 19:
                str = MimeTypes.AUDIO_DTS;
                break;
            case 20:
                str = MimeTypes.AUDIO_DTS_HD;
                break;
            case 21:
                str2 = MimeTypes.AUDIO_FLAC;
                list = Collections.singletonList(this.zzajx);
                break;
            case 22:
                str7 = MimeTypes.AUDIO_RAW;
                if (!zzb(new zzpx(this.zzajx))) {
                    str = MimeTypes.AUDIO_UNKNOWN;
                    String str10 = "MatroskaExtractor";
                    String str11 = "Non-PCM MS/ACM is unsupported. Setting mimeType to ";
                    String valueOf = String.valueOf(str);
                    Log.w(str10, valueOf.length() != 0 ? str11.concat(valueOf) : new String(str11));
                    break;
                } else {
                    zzbp = zzqe.zzbp(this.zzakr);
                    if (zzbp == 0) {
                        str = MimeTypes.AUDIO_UNKNOWN;
                        int i6 = this.zzakr;
                        StringBuilder sb = new StringBuilder(60 + String.valueOf(str).length());
                        sb.append("Unsupported PCM bit depth: ");
                        sb.append(i6);
                        sb.append(". Setting mimeType to ");
                        sb.append(str);
                        Log.w("MatroskaExtractor", sb.toString());
                        break;
                    }
                }
                break;
            case 23:
                str7 = MimeTypes.AUDIO_RAW;
                zzbp = zzqe.zzbp(this.zzakr);
                if (zzbp == 0) {
                    str = MimeTypes.AUDIO_UNKNOWN;
                    int i7 = this.zzakr;
                    StringBuilder sb2 = new StringBuilder(60 + String.valueOf(str).length());
                    sb2.append("Unsupported PCM bit depth: ");
                    sb2.append(i7);
                    sb2.append(". Setting mimeType to ");
                    sb2.append(str);
                    Log.w("MatroskaExtractor", sb2.toString());
                    break;
                }
                break;
            case 24:
                str = MimeTypes.APPLICATION_SUBRIP;
                break;
            case 25:
                str2 = MimeTypes.APPLICATION_VOBSUB;
                list = Collections.singletonList(this.zzajx);
                break;
            case 26:
                str = MimeTypes.APPLICATION_PGS;
                break;
            case 27:
                str2 = MimeTypes.APPLICATION_DVBSUBS;
                list = Collections.singletonList(new byte[]{this.zzajx[0], this.zzajx[1], this.zzajx[2], this.zzajx[3]});
                break;
            default:
                throw new zzfx("Unrecognized codec identifier.");
        }
    }

    private static List<byte[]> zza(zzpx zzpx) throws zzfx {
        try {
            zzpx.zzbl(16);
            if (zzpx.zzhe() != 826496599) {
                return null;
            }
            byte[] bArr = zzpx.data;
            for (int position = zzpx.getPosition() + 20; position < bArr.length - 4; position++) {
                if (bArr[position] == 0 && bArr[position + 1] == 0 && bArr[position + 2] == 1 && bArr[position + 3] == 15) {
                    return Collections.singletonList(Arrays.copyOfRange(bArr, position, bArr.length));
                }
            }
            throw new zzfx("Failed to find FourCC VC1 initialization data");
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzfx("Error parsing FourCC VC1 codec private");
        }
    }

    private static List<byte[]> zzd(byte[] bArr) throws zzfx {
        try {
            if (bArr[0] != 2) {
                throw new zzfx("Error parsing vorbis codec private");
            }
            int i = 0;
            int i2 = 1;
            while (bArr[i2] == -1) {
                i += 255;
                i2++;
            }
            int i3 = i2 + 1;
            int i4 = i + bArr[i2];
            int i5 = 0;
            while (bArr[i3] == -1) {
                i5 += 255;
                i3++;
            }
            int i6 = i3 + 1;
            int i7 = i5 + bArr[i3];
            if (bArr[i6] != 1) {
                throw new zzfx("Error parsing vorbis codec private");
            }
            byte[] bArr2 = new byte[i4];
            System.arraycopy(bArr, i6, bArr2, 0, i4);
            int i8 = i6 + i4;
            if (bArr[i8] != 3) {
                throw new zzfx("Error parsing vorbis codec private");
            }
            int i9 = i8 + i7;
            if (bArr[i9] != 5) {
                throw new zzfx("Error parsing vorbis codec private");
            }
            byte[] bArr3 = new byte[(bArr.length - i9)];
            System.arraycopy(bArr, i9, bArr3, 0, bArr.length - i9);
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(bArr2);
            arrayList.add(bArr3);
            return arrayList;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzfx("Error parsing vorbis codec private");
        }
    }

    private static boolean zzb(zzpx zzpx) throws zzfx {
        try {
            int zzhc = zzpx.zzhc();
            if (zzhc == 1) {
                return true;
            }
            if (zzhc != 65534) {
                return false;
            }
            zzpx.setPosition(24);
            return zzpx.readLong() == zzip.zzaht.getMostSignificantBits() && zzpx.readLong() == zzip.zzaht.getLeastSignificantBits();
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzfx("Error parsing MS/ACM codec private");
        }
    }

    /* synthetic */ zzis(zziq zziq) {
        this();
    }
}
