package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

@zzark
final class zzbcc {
    private final Object zzepk = new Object();
    private final List<Runnable> zzepl = new ArrayList();
    private boolean zzepm = false;

    public final void zza(Runnable runnable, Executor executor) {
        synchronized (this.zzepk) {
            if (this.zzepm) {
                executor.execute(runnable);
            } else {
                this.zzepl.add(new zzbcd(executor, runnable));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r2 >= r1) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        r3 = r0.get(r2);
        r2 = r2 + 1;
        ((java.lang.Runnable) r3).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        r0 = r0;
        r1 = r0.size();
        r2 = 0;
     */
    public final void zzaaw() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.zzepk) {
            if (!this.zzepm) {
                arrayList.addAll(this.zzepl);
                this.zzepl.clear();
                this.zzepm = true;
            }
        }
    }
}
