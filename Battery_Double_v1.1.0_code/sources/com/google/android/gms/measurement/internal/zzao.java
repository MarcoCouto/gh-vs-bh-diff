package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

public final class zzao extends zzf {
    private final zzap zzalm = new zzap(this, getContext(), "google_app_measurement_local.db");
    private boolean zzaln;

    zzao(zzbw zzbw) {
        super(zzbw);
    }

    /* access modifiers changed from: protected */
    public final boolean zzgy() {
        return false;
    }

    @WorkerThread
    public final void resetAnalyticsData() {
        zzgg();
        zzaf();
        try {
            int delete = 0 + getWritableDatabase().delete("messages", null, null);
            if (delete > 0) {
                zzgt().zzjo().zzg("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzgt().zzjg().zzg("Error resetting local analytics data. error", e);
        }
    }

    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v1, types: [boolean, int] */
    /* JADX WARNING: type inference failed for: r7v0 */
    /* JADX WARNING: type inference failed for: r12v0, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v0, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r9v1 */
    /* JADX WARNING: type inference failed for: r7v1 */
    /* JADX WARNING: type inference failed for: r12v1 */
    /* JADX WARNING: type inference failed for: r3v3 */
    /* JADX WARNING: type inference failed for: r9v2, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r7v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v3 */
    /* JADX WARNING: type inference failed for: r9v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v5 */
    /* JADX WARNING: type inference failed for: r12v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r7v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r9v6 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r9v7 */
    /* JADX WARNING: type inference failed for: r12v4 */
    /* JADX WARNING: type inference failed for: r9v8, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r12v6, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r7v5 */
    /* JADX WARNING: type inference failed for: r7v6 */
    /* JADX WARNING: type inference failed for: r12v7 */
    /* JADX WARNING: type inference failed for: r7v7 */
    /* JADX WARNING: type inference failed for: r12v8 */
    /* JADX WARNING: type inference failed for: r3v18 */
    /* JADX WARNING: type inference failed for: r7v8 */
    /* JADX WARNING: type inference failed for: r7v9 */
    /* JADX WARNING: type inference failed for: r7v10 */
    /* JADX WARNING: type inference failed for: r7v11 */
    /* JADX WARNING: type inference failed for: r7v12 */
    /* JADX WARNING: type inference failed for: r9v9 */
    /* JADX WARNING: type inference failed for: r3v19 */
    /* JADX WARNING: type inference failed for: r9v10 */
    /* JADX WARNING: type inference failed for: r9v11 */
    /* JADX WARNING: type inference failed for: r7v13 */
    /* JADX WARNING: type inference failed for: r7v14 */
    /* JADX WARNING: type inference failed for: r9v12 */
    /* JADX WARNING: type inference failed for: r9v13 */
    /* JADX WARNING: type inference failed for: r7v15 */
    /* JADX WARNING: type inference failed for: r7v16 */
    /* JADX WARNING: type inference failed for: r12v9 */
    /* JADX WARNING: type inference failed for: r9v14 */
    /* JADX WARNING: type inference failed for: r9v15 */
    /* JADX WARNING: type inference failed for: r9v16 */
    /* JADX WARNING: type inference failed for: r9v17 */
    /* JADX WARNING: type inference failed for: r9v18 */
    /* JADX WARNING: type inference failed for: r12v10 */
    /* JADX WARNING: type inference failed for: r12v11 */
    /* JADX WARNING: type inference failed for: r12v12 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v1, types: [boolean, int]
  assigns: []
  uses: [?[int, short, byte, char], int, boolean]
  mth insns count: 173
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00d1 A[SYNTHETIC, Splitter:B:51:0x00d1] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0129 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0129 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0129 A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 19 */
    @WorkerThread
    private final boolean zza(int i, byte[] bArr) {
        ? r12;
        ? r9;
        Throwable th;
        ? r92;
        ? r7;
        ? r93;
        ? r72;
        Object obj;
        ? r94;
        ? r73;
        ? r122;
        ? r74;
        Object obj2;
        ? r75;
        ? r76;
        ? r123;
        zzgg();
        zzaf();
        ? r3 = 0;
        if (this.zzaln) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(i));
        contentValues.put("entry", bArr);
        int i2 = 5;
        int i3 = 0;
        int i4 = 5;
        ? r32 = r3;
        while (i3 < i2) {
            ? r77 = 0;
            try {
                ? writableDatabase = getWritableDatabase();
                if (writableDatabase == 0) {
                    try {
                        this.zzaln = true;
                        if (writableDatabase != 0) {
                            writableDatabase.close();
                        }
                        return r32;
                    } catch (SQLiteFullException e) {
                        obj = e;
                        r76 = r77;
                        r93 = writableDatabase;
                    } catch (SQLiteDatabaseLockedException unused) {
                        r75 = r77;
                        r94 = writableDatabase;
                        try {
                            r92 = r94;
                            r7 = r73;
                            SystemClock.sleep((long) i4);
                            r92 = r94;
                            r7 = r73;
                            i4 += 20;
                            if (r73 != 0) {
                            }
                            if (r94 != 0) {
                            }
                            i3++;
                            i2 = 5;
                            r32 = 0;
                        } catch (Throwable th2) {
                            th = th2;
                            r12 = r7;
                            r9 = r92;
                            if (r12 != 0) {
                            }
                            if (r9 != 0) {
                            }
                            throw th;
                        }
                    } catch (SQLiteException e2) {
                        obj2 = e2;
                        r123 = 0;
                        r74 = writableDatabase;
                        r122 = r123;
                        if (r74 != 0) {
                        }
                        zzgt().zzjg().zzg("Error writing entry to local database", obj2);
                        this.zzaln = true;
                        if (r122 != 0) {
                        }
                        if (r74 != 0) {
                        }
                        i3++;
                        i2 = 5;
                        r32 = 0;
                    }
                } else {
                    writableDatabase.beginTransaction();
                    long j = 0;
                    ? rawQuery = writableDatabase.rawQuery("select count(1) from messages", null);
                    if (rawQuery != 0) {
                        try {
                            if (rawQuery.moveToFirst()) {
                                j = rawQuery.getLong(r32);
                            }
                        } catch (SQLiteFullException e3) {
                            obj = e3;
                            r76 = rawQuery;
                            r93 = writableDatabase;
                            r92 = r93;
                            r7 = r72;
                            zzgt().zzjg().zzg("Error writing entry to local database", obj);
                            this.zzaln = true;
                            r92 = r93;
                            r7 = r72;
                            if (r72 != 0) {
                                r72.close();
                            }
                            if (r93 == 0) {
                                r93.close();
                            }
                            i3++;
                            i2 = 5;
                            r32 = 0;
                        } catch (SQLiteDatabaseLockedException unused2) {
                            r75 = rawQuery;
                            r94 = writableDatabase;
                            r92 = r94;
                            r7 = r73;
                            SystemClock.sleep((long) i4);
                            r92 = r94;
                            r7 = r73;
                            i4 += 20;
                            if (r73 != 0) {
                                r73.close();
                            }
                            if (r94 != 0) {
                                r94.close();
                            }
                            i3++;
                            i2 = 5;
                            r32 = 0;
                        } catch (SQLiteException e4) {
                            obj2 = e4;
                            r123 = rawQuery;
                            r74 = writableDatabase;
                            r122 = r123;
                            if (r74 != 0) {
                                try {
                                    if (r74.inTransaction()) {
                                        r74.endTransaction();
                                    }
                                } catch (Throwable th3) {
                                    th = th3;
                                    r9 = r74;
                                    r12 = r122;
                                    if (r12 != 0) {
                                    }
                                    if (r9 != 0) {
                                    }
                                    throw th;
                                }
                            }
                            zzgt().zzjg().zzg("Error writing entry to local database", obj2);
                            this.zzaln = true;
                            if (r122 != 0) {
                                r122.close();
                            }
                            if (r74 != 0) {
                                r74.close();
                            }
                            i3++;
                            i2 = 5;
                            r32 = 0;
                        } catch (Throwable th4) {
                            th = th4;
                            r9 = writableDatabase;
                            r12 = rawQuery;
                            if (r12 != 0) {
                                r12.close();
                            }
                            if (r9 != 0) {
                                r9.close();
                            }
                            throw th;
                        }
                    }
                    if (j >= 100000) {
                        zzgt().zzjg().zzby("Data loss, local db full");
                        long j2 = (100000 - j) + 1;
                        String[] strArr = new String[1];
                        strArr[r32] = Long.toString(j2);
                        long delete = (long) writableDatabase.delete("messages", "rowid in (select rowid from messages order by rowid asc limit ?)", strArr);
                        if (delete != j2) {
                            zzgt().zzjg().zzd("Different delete count than expected in local db. expected, received, difference", Long.valueOf(j2), Long.valueOf(delete), Long.valueOf(j2 - delete));
                        }
                    }
                    writableDatabase.insertOrThrow("messages", null, contentValues);
                    writableDatabase.setTransactionSuccessful();
                    writableDatabase.endTransaction();
                    if (rawQuery != 0) {
                        rawQuery.close();
                    }
                    if (writableDatabase != 0) {
                        writableDatabase.close();
                    }
                    return true;
                }
            } catch (SQLiteFullException e5) {
                obj = e5;
                r93 = 0;
                r76 = r77;
                r92 = r93;
                r7 = r72;
                zzgt().zzjg().zzg("Error writing entry to local database", obj);
                this.zzaln = true;
                r92 = r93;
                r7 = r72;
                if (r72 != 0) {
                }
                if (r93 == 0) {
                }
                i3++;
                i2 = 5;
                r32 = 0;
            } catch (SQLiteDatabaseLockedException unused3) {
                r94 = 0;
                r75 = r77;
                r92 = r94;
                r7 = r73;
                SystemClock.sleep((long) i4);
                r92 = r94;
                r7 = r73;
                i4 += 20;
                if (r73 != 0) {
                }
                if (r94 != 0) {
                }
                i3++;
                i2 = 5;
                r32 = 0;
            } catch (SQLiteException e6) {
                obj2 = e6;
                r122 = 0;
                r74 = r77;
                if (r74 != 0) {
                }
                zzgt().zzjg().zzg("Error writing entry to local database", obj2);
                this.zzaln = true;
                if (r122 != 0) {
                }
                if (r74 != 0) {
                }
                i3++;
                i2 = 5;
                r32 = 0;
            } catch (Throwable th5) {
                th = th5;
                r9 = 0;
                r12 = 0;
                if (r12 != 0) {
                }
                if (r9 != 0) {
                }
                throw th;
            }
        }
        zzgt().zzjj().zzby("Failed to write entry to local database");
        return false;
    }

    public final boolean zza(zzag zzag) {
        Parcel obtain = Parcel.obtain();
        zzag.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(0, marshall);
        }
        zzgt().zzjj().zzby("Event is too long for local database. Sending event directly to service");
        return false;
    }

    public final boolean zza(zzfv zzfv) {
        Parcel obtain = Parcel.obtain();
        zzfv.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(1, marshall);
        }
        zzgt().zzjj().zzby("User property too long for local database. Sending directly to service");
        return false;
    }

    public final boolean zzc(zzo zzo) {
        zzgr();
        byte[] zza = zzfy.zza((Parcelable) zzo);
        if (zza.length <= 131072) {
            return zza(2, zza);
        }
        zzgt().zzjj().zzby("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:59|60|61|62) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:75|76|77|78) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:45|46|47|48|171) */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x017e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x017f, code lost:
        r4 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0184, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0185, code lost:
        r4 = r15;
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x018b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x018c, code lost:
        r4 = r15;
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x01a0, code lost:
        if (r15.inTransaction() != false) goto L_0x01a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x01a2, code lost:
        r15.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x01b6, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x01bb, code lost:
        r15.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x01c9, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x01ce, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x01ea, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x01ef, code lost:
        r15.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x01f7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x01f8, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x01fc, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0201, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0043, code lost:
        r4 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        zzgt().zzjg().zzby("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r12.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        zzgt().zzjg().zzby("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        r12.recycle();
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        zzgt().zzjg().zzby("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        r12.recycle();
        r13 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00a9 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x00da */
    /* JADX WARNING: Missing exception handler attribute for start block: B:75:0x0111 */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x019c A[SYNTHETIC, Splitter:B:118:0x019c] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x01c9  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x01ea  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x01ef  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x01fc  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0201  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x01f2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x01f2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x01f2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:12:0x0031] */
    public final List<AbstractSafeParcelable> zzr(int i) {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        SQLiteDatabase sQLiteDatabase2;
        Object obj;
        Object obj2;
        Parcel obtain;
        Parcel obtain2;
        Parcel obtain3;
        zzaf();
        zzgg();
        if (this.zzaln) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!getContext().getDatabasePath("google_app_measurement_local.db").exists()) {
            return arrayList;
        }
        int i2 = 5;
        int i3 = 5;
        int i4 = 0;
        while (i4 < i2) {
            try {
                sQLiteDatabase2 = getWritableDatabase();
                if (sQLiteDatabase2 == null) {
                    try {
                        this.zzaln = true;
                        if (sQLiteDatabase2 != null) {
                            sQLiteDatabase2.close();
                        }
                        return null;
                    } catch (SQLiteFullException e) {
                        obj = e;
                        cursor = null;
                    } catch (SQLiteDatabaseLockedException unused) {
                    } catch (SQLiteException e2) {
                        obj2 = e2;
                        cursor = null;
                        if (sQLiteDatabase2 != null) {
                        }
                        zzgt().zzjg().zzg("Error reading entries from local database", obj2);
                        this.zzaln = true;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase2 != null) {
                        }
                        i4++;
                        i2 = 5;
                    } catch (Throwable th2) {
                        th = th2;
                        cursor = null;
                        sQLiteDatabase = sQLiteDatabase2;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                } else {
                    sQLiteDatabase2.beginTransaction();
                    sQLiteDatabase = sQLiteDatabase2;
                    try {
                        cursor = sQLiteDatabase2.query("messages", new String[]{"rowid", "type", "entry"}, null, null, null, null, "rowid asc", Integer.toString(100));
                        long j = -1;
                        while (cursor.moveToNext()) {
                            try {
                                j = cursor.getLong(0);
                                int i5 = cursor.getInt(1);
                                byte[] blob = cursor.getBlob(2);
                                if (i5 == 0) {
                                    obtain3 = Parcel.obtain();
                                    obtain3.unmarshall(blob, 0, blob.length);
                                    obtain3.setDataPosition(0);
                                    zzag zzag = (zzag) zzag.CREATOR.createFromParcel(obtain3);
                                    obtain3.recycle();
                                    if (zzag != null) {
                                        arrayList.add(zzag);
                                    }
                                } else if (i5 == 1) {
                                    obtain2 = Parcel.obtain();
                                    obtain2.unmarshall(blob, 0, blob.length);
                                    obtain2.setDataPosition(0);
                                    Object obj3 = (zzfv) zzfv.CREATOR.createFromParcel(obtain2);
                                    obtain2.recycle();
                                    if (obj3 != null) {
                                        arrayList.add(obj3);
                                    }
                                } else if (i5 == 2) {
                                    obtain = Parcel.obtain();
                                    obtain.unmarshall(blob, 0, blob.length);
                                    obtain.setDataPosition(0);
                                    Object obj4 = (zzo) zzo.CREATOR.createFromParcel(obtain);
                                    obtain.recycle();
                                    if (obj4 != null) {
                                        arrayList.add(obj4);
                                    }
                                } else {
                                    zzgt().zzjg().zzby("Unknown record type in local database");
                                }
                            } catch (SQLiteFullException e3) {
                                e = e3;
                            } catch (SQLiteDatabaseLockedException unused2) {
                                try {
                                    SystemClock.sleep((long) i3);
                                    i3 += 20;
                                    if (cursor != null) {
                                    }
                                    if (sQLiteDatabase == null) {
                                    }
                                    i4++;
                                    i2 = 5;
                                } catch (Throwable th3) {
                                    th = th3;
                                    if (cursor != null) {
                                    }
                                    if (sQLiteDatabase != null) {
                                    }
                                    throw th;
                                }
                            } catch (SQLiteException e4) {
                                e = e4;
                                sQLiteDatabase2 = sQLiteDatabase;
                                obj2 = e;
                                if (sQLiteDatabase2 != null) {
                                }
                                zzgt().zzjg().zzg("Error reading entries from local database", obj2);
                                this.zzaln = true;
                                if (cursor != null) {
                                }
                                if (sQLiteDatabase2 != null) {
                                }
                                i4++;
                                i2 = 5;
                            } catch (Throwable th4) {
                                Throwable th5 = th4;
                                obtain3.recycle();
                                throw th5;
                            }
                        }
                        if (sQLiteDatabase.delete("messages", "rowid <= ?", new String[]{Long.toString(j)}) < arrayList.size()) {
                            zzgt().zzjg().zzby("Fewer entries removed from local database than expected");
                        }
                        sQLiteDatabase.setTransactionSuccessful();
                        sQLiteDatabase.endTransaction();
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        return arrayList;
                    } catch (SQLiteFullException e5) {
                        e = e5;
                        cursor = null;
                        sQLiteDatabase2 = sQLiteDatabase;
                        obj = e;
                        zzgt().zzjg().zzg("Error reading entries from local database", obj);
                        this.zzaln = true;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase2 != null) {
                        }
                        i4++;
                        i2 = 5;
                    } catch (SQLiteDatabaseLockedException unused3) {
                        cursor = null;
                        SystemClock.sleep((long) i3);
                        i3 += 20;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase == null) {
                        }
                        i4++;
                        i2 = 5;
                    } catch (SQLiteException e6) {
                        e = e6;
                        cursor = null;
                        sQLiteDatabase2 = sQLiteDatabase;
                        obj2 = e;
                        if (sQLiteDatabase2 != null) {
                        }
                        zzgt().zzjg().zzg("Error reading entries from local database", obj2);
                        this.zzaln = true;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase2 != null) {
                        }
                        i4++;
                        i2 = 5;
                    } catch (Throwable th6) {
                        th = th6;
                        th = th;
                        cursor = null;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteFullException e7) {
                obj = e7;
                cursor = null;
                sQLiteDatabase2 = null;
                zzgt().zzjg().zzg("Error reading entries from local database", obj);
                this.zzaln = true;
                if (cursor != null) {
                }
                if (sQLiteDatabase2 != null) {
                }
                i4++;
                i2 = 5;
            } catch (SQLiteDatabaseLockedException unused4) {
                sQLiteDatabase = null;
                cursor = null;
                SystemClock.sleep((long) i3);
                i3 += 20;
                if (cursor != null) {
                }
                if (sQLiteDatabase == null) {
                }
                i4++;
                i2 = 5;
            } catch (SQLiteException e8) {
                obj2 = e8;
                cursor = null;
                sQLiteDatabase2 = null;
                if (sQLiteDatabase2 != null) {
                }
                zzgt().zzjg().zzg("Error reading entries from local database", obj2);
                this.zzaln = true;
                if (cursor != null) {
                }
                if (sQLiteDatabase2 != null) {
                }
                i4++;
                i2 = 5;
            } catch (Throwable th7) {
                th = th7;
                sQLiteDatabase = null;
                cursor = null;
                if (cursor != null) {
                }
                if (sQLiteDatabase != null) {
                }
                throw th;
            }
        }
        zzgt().zzjj().zzby("Failed to read events from database in reasonable time");
        return null;
    }

    @WorkerThread
    @VisibleForTesting
    private final SQLiteDatabase getWritableDatabase() throws SQLiteException {
        if (this.zzaln) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.zzalm.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.zzaln = true;
        return null;
    }

    public final /* bridge */ /* synthetic */ void zzgf() {
        super.zzgf();
    }

    public final /* bridge */ /* synthetic */ void zzgg() {
        super.zzgg();
    }

    public final /* bridge */ /* synthetic */ void zzgh() {
        super.zzgh();
    }

    public final /* bridge */ /* synthetic */ void zzaf() {
        super.zzaf();
    }

    public final /* bridge */ /* synthetic */ zza zzgi() {
        return super.zzgi();
    }

    public final /* bridge */ /* synthetic */ zzda zzgj() {
        return super.zzgj();
    }

    public final /* bridge */ /* synthetic */ zzam zzgk() {
        return super.zzgk();
    }

    public final /* bridge */ /* synthetic */ zzeb zzgl() {
        return super.zzgl();
    }

    public final /* bridge */ /* synthetic */ zzdy zzgm() {
        return super.zzgm();
    }

    public final /* bridge */ /* synthetic */ zzao zzgn() {
        return super.zzgn();
    }

    public final /* bridge */ /* synthetic */ zzfd zzgo() {
        return super.zzgo();
    }

    public final /* bridge */ /* synthetic */ zzaa zzgp() {
        return super.zzgp();
    }

    public final /* bridge */ /* synthetic */ Clock zzbx() {
        return super.zzbx();
    }

    public final /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public final /* bridge */ /* synthetic */ zzaq zzgq() {
        return super.zzgq();
    }

    public final /* bridge */ /* synthetic */ zzfy zzgr() {
        return super.zzgr();
    }

    public final /* bridge */ /* synthetic */ zzbr zzgs() {
        return super.zzgs();
    }

    public final /* bridge */ /* synthetic */ zzas zzgt() {
        return super.zzgt();
    }

    public final /* bridge */ /* synthetic */ zzbd zzgu() {
        return super.zzgu();
    }

    public final /* bridge */ /* synthetic */ zzq zzgv() {
        return super.zzgv();
    }

    public final /* bridge */ /* synthetic */ zzn zzgw() {
        return super.zzgw();
    }
}
