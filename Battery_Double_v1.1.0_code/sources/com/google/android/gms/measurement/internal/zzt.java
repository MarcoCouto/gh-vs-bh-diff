package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcelable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import bolts.MeasurementEvent;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzfi;
import com.google.android.gms.internal.measurement.zzfj;
import com.google.android.gms.internal.measurement.zzfm;
import com.google.android.gms.internal.measurement.zzft;
import com.google.android.gms.internal.measurement.zzfu;
import com.google.android.gms.internal.measurement.zzfw;
import com.google.android.gms.internal.measurement.zzfx;
import com.google.android.gms.internal.measurement.zzxz;
import com.google.android.gms.internal.measurement.zzya;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.google.android.gms.measurement.api.AppMeasurementSdk.ConditionalUserProperty;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class zzt extends zzfn {
    /* access modifiers changed from: private */
    public static final String[] zzagz = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;"};
    /* access modifiers changed from: private */
    public static final String[] zzaha = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    /* access modifiers changed from: private */
    public static final String[] zzahb = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;"};
    /* access modifiers changed from: private */
    public static final String[] zzahc = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    /* access modifiers changed from: private */
    public static final String[] zzahd = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    /* access modifiers changed from: private */
    public static final String[] zzahe = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    private final zzw zzahf = new zzw(this, getContext(), "google_app_measurement.db");
    /* access modifiers changed from: private */
    public final zzfj zzahg = new zzfj(zzbx());

    zzt(zzfo zzfo) {
        super(zzfo);
    }

    /* access modifiers changed from: protected */
    public final boolean zzgy() {
        return false;
    }

    @WorkerThread
    public final void beginTransaction() {
        zzcl();
        getWritableDatabase().beginTransaction();
    }

    @WorkerThread
    public final void setTransactionSuccessful() {
        zzcl();
        getWritableDatabase().setTransactionSuccessful();
    }

    @WorkerThread
    public final void endTransaction() {
        zzcl();
        getWritableDatabase().endTransaction();
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x003b  */
    @WorkerThread
    private final long zza(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            Cursor cursor2 = getWritableDatabase().rawQuery(str, strArr);
            try {
                if (cursor2.moveToFirst()) {
                    long j = cursor2.getLong(0);
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    return j;
                }
                throw new SQLiteException("Database returned empty set");
            } catch (SQLiteException e) {
                e = e;
                cursor = cursor2;
                try {
                    zzgt().zzjg().zze("Database error", str, e);
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            e = e2;
            zzgt().zzjg().zze("Database error", str, e);
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0039  */
    @WorkerThread
    private final long zza(String str, String[] strArr, long j) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = getWritableDatabase().rawQuery(str, strArr);
            try {
                if (rawQuery.moveToFirst()) {
                    long j2 = rawQuery.getLong(0);
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    return j2;
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j;
            } catch (SQLiteException e) {
                e = e;
                cursor = rawQuery;
                try {
                    zzgt().zzjg().zze("Database error", str, e);
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                cursor = rawQuery;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            e = e2;
            zzgt().zzjg().zze("Database error", str, e);
            throw e;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    @VisibleForTesting
    public final SQLiteDatabase getWritableDatabase() {
        zzaf();
        try {
            return this.zzahf.getWritableDatabase();
        } catch (SQLiteException e) {
            zzgt().zzjj().zzg("Error opening database", e);
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:71:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0135  */
    @WorkerThread
    public final zzac zzg(String str, String str2) {
        Cursor cursor;
        Throwable th;
        Object obj;
        Boolean bool;
        Cursor cursor2;
        String str3 = str2;
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        zzaf();
        zzcl();
        try {
            boolean z = true;
            cursor = getWritableDatabase().query("events", new String[]{"lifetime_count", "current_bundle_count", "last_fire_timestamp", "last_bundled_timestamp", "last_bundled_day", "last_sampled_complex_event_id", "last_sampling_rate", "last_exempt_from_sampling"}, "app_id=? and name=?", new String[]{str, str3}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                long j = cursor.getLong(0);
                long j2 = cursor.getLong(1);
                long j3 = cursor.getLong(2);
                long j4 = cursor.isNull(3) ? 0 : cursor.getLong(3);
                Long valueOf = cursor.isNull(4) ? null : Long.valueOf(cursor.getLong(4));
                Long valueOf2 = cursor.isNull(5) ? null : Long.valueOf(cursor.getLong(5));
                Long valueOf3 = cursor.isNull(6) ? null : Long.valueOf(cursor.getLong(6));
                if (!cursor.isNull(7)) {
                    try {
                        if (cursor.getLong(7) != 1) {
                            z = false;
                        }
                        bool = Boolean.valueOf(z);
                    } catch (SQLiteException e) {
                        e = e;
                        obj = e;
                        try {
                            zzgt().zzjg().zzd("Error querying events. appId", zzas.zzbw(str), zzgq().zzbt(str2), obj);
                            if (cursor != null) {
                                cursor.close();
                            }
                            return null;
                        } catch (Throwable th2) {
                            th = th2;
                            th = th;
                            if (cursor != null) {
                                cursor.close();
                            }
                            throw th;
                        }
                    }
                } else {
                    bool = null;
                }
                r1 = r1;
                Cursor cursor3 = cursor;
                try {
                    zzac zzac = new zzac(str, str3, j, j2, j3, j4, valueOf, valueOf2, valueOf3, bool);
                    cursor2 = cursor3;
                    try {
                        if (cursor2.moveToNext()) {
                            zzgt().zzjg().zzg("Got multiple records for event aggregates, expected one. appId", zzas.zzbw(str));
                        }
                        if (cursor2 != null) {
                            cursor2.close();
                        }
                        return zzac;
                    } catch (SQLiteException e2) {
                        e = e2;
                        cursor = cursor2;
                        obj = e;
                        zzgt().zzjg().zzd("Error querying events. appId", zzas.zzbw(str), zzgq().zzbt(str2), obj);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th3) {
                        th = th3;
                        cursor = cursor2;
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (SQLiteException e3) {
                    e = e3;
                    cursor2 = cursor3;
                    cursor = cursor2;
                    obj = e;
                    zzgt().zzjg().zzd("Error querying events. appId", zzas.zzbw(str), zzgq().zzbt(str2), obj);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th4) {
                    th = th4;
                    cursor2 = cursor3;
                    cursor = cursor2;
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e4) {
                e = e4;
                Cursor cursor4 = cursor;
                obj = e;
                zzgt().zzjg().zzd("Error querying events. appId", zzas.zzbw(str), zzgq().zzbt(str2), obj);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th5) {
                th = th5;
                Cursor cursor5 = cursor;
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e5) {
            obj = e5;
            cursor = null;
            zzgt().zzjg().zzd("Error querying events. appId", zzas.zzbw(str), zzgq().zzbt(str2), obj);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th6) {
            th = th6;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @WorkerThread
    public final void zza(zzac zzac) {
        Preconditions.checkNotNull(zzac);
        zzaf();
        zzcl();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzac.zztt);
        contentValues.put("name", zzac.name);
        contentValues.put("lifetime_count", Long.valueOf(zzac.zzahv));
        contentValues.put("current_bundle_count", Long.valueOf(zzac.zzahw));
        contentValues.put("last_fire_timestamp", Long.valueOf(zzac.zzahx));
        contentValues.put("last_bundled_timestamp", Long.valueOf(zzac.zzahy));
        contentValues.put("last_bundled_day", zzac.zzahz);
        contentValues.put("last_sampled_complex_event_id", zzac.zzaia);
        contentValues.put("last_sampling_rate", zzac.zzaib);
        contentValues.put("last_exempt_from_sampling", (zzac.zzaic == null || !zzac.zzaic.booleanValue()) ? null : Long.valueOf(1));
        try {
            if (getWritableDatabase().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                zzgt().zzjg().zzg("Failed to insert/update event aggregates (got -1). appId", zzas.zzbw(zzac.zztt));
            }
        } catch (SQLiteException e) {
            zzgt().zzjg().zze("Error storing event aggregates. appId", zzas.zzbw(zzac.zztt), e);
        }
    }

    @WorkerThread
    public final void zzh(String str, String str2) {
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        zzaf();
        zzcl();
        try {
            zzgt().zzjo().zzg("Deleted user attribute rows", Integer.valueOf(getWritableDatabase().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e) {
            zzgt().zzjg().zzd("Error deleting user attribute. appId", zzas.zzbw(str), zzgq().zzbv(str2), e);
        }
    }

    @WorkerThread
    public final boolean zza(zzfx zzfx) {
        Preconditions.checkNotNull(zzfx);
        zzaf();
        zzcl();
        if (zzi(zzfx.zztt, zzfx.name) == null) {
            if (zzfy.zzct(zzfx.name)) {
                if (zza("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{zzfx.zztt}) >= 25) {
                    return false;
                }
            } else {
                if (zza("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{zzfx.zztt, zzfx.origin}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzfx.zztt);
        contentValues.put("origin", zzfx.origin);
        contentValues.put("name", zzfx.name);
        contentValues.put("set_timestamp", Long.valueOf(zzfx.zzauk));
        zza(contentValues, "value", zzfx.value);
        try {
            if (getWritableDatabase().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                zzgt().zzjg().zzg("Failed to insert/update user property (got -1). appId", zzas.zzbw(zzfx.zztt));
            }
        } catch (SQLiteException e) {
            zzgt().zzjg().zze("Error storing user property. appId", zzas.zzbw(zzfx.zztt), e);
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ac  */
    @WorkerThread
    public final zzfx zzi(String str, String str2) {
        Cursor cursor;
        Throwable th;
        Object obj;
        String str3 = str2;
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        zzaf();
        zzcl();
        try {
            cursor = getWritableDatabase().query("user_attributes", new String[]{"set_timestamp", "value", "origin"}, "app_id=? and name=?", new String[]{str, str3}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                long j = cursor.getLong(0);
                try {
                    String str4 = str;
                    zzfx zzfx = new zzfx(str4, cursor.getString(2), str3, j, zza(cursor, 1));
                    if (cursor.moveToNext()) {
                        zzgt().zzjg().zzg("Got multiple records for user property, expected one. appId", zzas.zzbw(str));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return zzfx;
                } catch (SQLiteException e) {
                    e = e;
                    obj = e;
                    try {
                        zzgt().zzjg().zzd("Error querying user property. appId", zzas.zzbw(str), zzgq().zzbv(str3), obj);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e2) {
                e = e2;
                obj = e;
                zzgt().zzjg().zzd("Error querying user property. appId", zzas.zzbw(str), zzgq().zzbv(str3), obj);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th3) {
                th = th3;
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            obj = e3;
            cursor = null;
            zzgt().zzjg().zzd("Error querying user property. appId", zzas.zzbw(str), zzgq().zzbv(str3), obj);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a1  */
    @WorkerThread
    public final List<zzfx> zzbl(String str) {
        Cursor cursor;
        Preconditions.checkNotEmpty(str);
        zzaf();
        zzcl();
        ArrayList arrayList = new ArrayList();
        try {
            cursor = getWritableDatabase().query("user_attributes", new String[]{"name", "origin", "set_timestamp", "value"}, "app_id=?", new String[]{str}, null, null, "rowid", "1000");
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                do {
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    if (string2 == null) {
                        string2 = "";
                    }
                    String str2 = string2;
                    long j = cursor.getLong(2);
                    Object zza = zza(cursor, 3);
                    if (zza == null) {
                        zzgt().zzjg().zzg("Read invalid user property value, ignoring it. appId", zzas.zzbw(str));
                    } else {
                        zzfx zzfx = new zzfx(str, str2, string, j, zza);
                        arrayList.add(zzfx);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e) {
                e = e;
                try {
                    zzgt().zzjg().zze("Error querying user properties. appId", zzas.zzbw(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
            zzgt().zzjg().zze("Error querying user properties. appId", zzas.zzbw(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0032, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0033, code lost:
        r13 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0104, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0105, code lost:
        r13 = r23;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0109, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x010a, code lost:
        r13 = r23;
        r12 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0125, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x012e, code lost:
        r2.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0104 A[ExcHandler: all (r0v5 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x012e  */
    @WorkerThread
    public final List<zzfx> zzb(String str, String str2, String str3) {
        Cursor cursor;
        String str4;
        Object obj;
        Preconditions.checkNotEmpty(str);
        zzaf();
        zzcl();
        ArrayList arrayList = new ArrayList();
        Cursor cursor2 = null;
        try {
            int i = 3;
            ArrayList arrayList2 = new ArrayList(3);
            String str5 = str;
            arrayList2.add(str5);
            StringBuilder sb = new StringBuilder("app_id=?");
            if (!TextUtils.isEmpty(str2)) {
                str4 = str2;
                arrayList2.add(str4);
                sb.append(" and origin=?");
            } else {
                str4 = str2;
            }
            if (!TextUtils.isEmpty(str3)) {
                arrayList2.add(String.valueOf(str3).concat("*"));
                sb.append(" and name glob ?");
            }
            cursor = getWritableDatabase().query("user_attributes", new String[]{"name", "set_timestamp", "value", "origin"}, sb.toString(), (String[]) arrayList2.toArray(new String[arrayList2.size()]), null, null, "rowid", NativeContentAd.ASSET_HEADLINE);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                while (true) {
                    if (arrayList.size() >= 1000) {
                        zzgt().zzjg().zzg("Read more than the max allowed user properties, ignoring excess", Integer.valueOf(1000));
                        break;
                    }
                    String string = cursor.getString(0);
                    long j = cursor.getLong(1);
                    try {
                        Object zza = zza(cursor, 2);
                        String string2 = cursor.getString(i);
                        if (zza == null) {
                            try {
                                zzgt().zzjg().zzd("(2)Read invalid user property value, ignoring it", zzas.zzbw(str), string2, str3);
                            } catch (SQLiteException e) {
                                obj = e;
                                str4 = string2;
                                try {
                                    zzgt().zzjg().zzd("(2)Error querying user properties", zzas.zzbw(str), str4, obj);
                                    if (cursor != null) {
                                    }
                                    return null;
                                } catch (Throwable th) {
                                    th = th;
                                    Throwable th2 = th;
                                    cursor2 = cursor;
                                    if (cursor2 != null) {
                                    }
                                    throw th2;
                                }
                            }
                        } else {
                            String str6 = str3;
                            zzfx zzfx = r5;
                            zzfx zzfx2 = new zzfx(str5, string2, string, j, zza);
                            arrayList.add(zzfx);
                        }
                        if (!cursor.moveToNext()) {
                            break;
                        }
                        str4 = string2;
                        i = 3;
                    } catch (SQLiteException e2) {
                        e = e2;
                        obj = e;
                        zzgt().zzjg().zzd("(2)Error querying user properties", zzas.zzbw(str), str4, obj);
                        if (cursor != null) {
                        }
                        return null;
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e3) {
                e = e3;
                obj = e;
                zzgt().zzjg().zzd("(2)Error querying user properties", zzas.zzbw(str), str4, obj);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th3) {
                th = th3;
                Throwable th22 = th;
                cursor2 = cursor;
                if (cursor2 != null) {
                }
                throw th22;
            }
        } catch (SQLiteException e4) {
            e = e4;
            str4 = str2;
            obj = e;
            cursor = null;
            zzgt().zzjg().zzd("(2)Error querying user properties", zzas.zzbw(str), str4, obj);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th4) {
        }
    }

    @WorkerThread
    public final boolean zza(zzo zzo) {
        Preconditions.checkNotNull(zzo);
        zzaf();
        zzcl();
        if (zzi(zzo.packageName, zzo.zzags.name) == null) {
            if (zza("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{zzo.packageName}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzo.packageName);
        contentValues.put("origin", zzo.origin);
        contentValues.put("name", zzo.zzags.name);
        zza(contentValues, "value", zzo.zzags.getValue());
        contentValues.put(ConditionalUserProperty.ACTIVE, Boolean.valueOf(zzo.active));
        contentValues.put(ConditionalUserProperty.TRIGGER_EVENT_NAME, zzo.triggerEventName);
        contentValues.put(ConditionalUserProperty.TRIGGER_TIMEOUT, Long.valueOf(zzo.triggerTimeout));
        zzgr();
        contentValues.put("timed_out_event", zzfy.zza((Parcelable) zzo.zzagt));
        contentValues.put(ConditionalUserProperty.CREATION_TIMESTAMP, Long.valueOf(zzo.creationTimestamp));
        zzgr();
        contentValues.put("triggered_event", zzfy.zza((Parcelable) zzo.zzagu));
        contentValues.put(ConditionalUserProperty.TRIGGERED_TIMESTAMP, Long.valueOf(zzo.zzags.zzauk));
        contentValues.put(ConditionalUserProperty.TIME_TO_LIVE, Long.valueOf(zzo.timeToLive));
        zzgr();
        contentValues.put("expired_event", zzfy.zza((Parcelable) zzo.zzagv));
        try {
            if (getWritableDatabase().insertWithOnConflict("conditional_properties", null, contentValues, 5) == -1) {
                zzgt().zzjg().zzg("Failed to insert/update conditional user property (got -1)", zzas.zzbw(zzo.packageName));
            }
        } catch (SQLiteException e) {
            zzgt().zzjg().zze("Error storing conditional user property", zzas.zzbw(zzo.packageName), e);
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0128  */
    @WorkerThread
    public final zzo zzj(String str, String str2) {
        Cursor cursor;
        Throwable th;
        Object obj;
        String str3 = str2;
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        zzaf();
        zzcl();
        try {
            cursor = getWritableDatabase().query("conditional_properties", new String[]{"origin", "value", ConditionalUserProperty.ACTIVE, ConditionalUserProperty.TRIGGER_EVENT_NAME, ConditionalUserProperty.TRIGGER_TIMEOUT, "timed_out_event", ConditionalUserProperty.CREATION_TIMESTAMP, "triggered_event", ConditionalUserProperty.TRIGGERED_TIMESTAMP, ConditionalUserProperty.TIME_TO_LIVE, "expired_event"}, "app_id=? and name=?", new String[]{str, str3}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                try {
                    Object zza = zza(cursor, 1);
                    boolean z = cursor.getInt(2) != 0;
                    String string2 = cursor.getString(3);
                    long j = cursor.getLong(4);
                    zzag zzag = (zzag) zzjr().zza(cursor.getBlob(5), zzag.CREATOR);
                    long j2 = cursor.getLong(6);
                    zzag zzag2 = (zzag) zzjr().zza(cursor.getBlob(7), zzag.CREATOR);
                    long j3 = cursor.getLong(8);
                    long j4 = cursor.getLong(9);
                    zzag zzag3 = (zzag) zzjr().zza(cursor.getBlob(10), zzag.CREATOR);
                    zzfv zzfv = new zzfv(str3, j3, zza, string);
                    zzo zzo = new zzo(str, string, zzfv, j2, z, string2, zzag, j, zzag2, j4, zzag3);
                    if (cursor.moveToNext()) {
                        zzgt().zzjg().zze("Got multiple records for conditional property, expected one", zzas.zzbw(str), zzgq().zzbv(str3));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return zzo;
                } catch (SQLiteException e) {
                    e = e;
                    obj = e;
                    try {
                        zzgt().zzjg().zzd("Error querying conditional property", zzas.zzbw(str), zzgq().zzbv(str3), obj);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e2) {
                e = e2;
                obj = e;
                zzgt().zzjg().zzd("Error querying conditional property", zzas.zzbw(str), zzgq().zzbv(str3), obj);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th3) {
                th = th3;
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            obj = e3;
            cursor = null;
            zzgt().zzjg().zzd("Error querying conditional property", zzas.zzbw(str), zzgq().zzbv(str3), obj);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @WorkerThread
    public final int zzk(String str, String str2) {
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        zzaf();
        zzcl();
        try {
            return getWritableDatabase().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e) {
            zzgt().zzjg().zzd("Error deleting conditional property", zzas.zzbw(str), zzgq().zzbv(str2), e);
            return 0;
        }
    }

    @WorkerThread
    public final List<zzo> zzc(String str, String str2, String str3) {
        Preconditions.checkNotEmpty(str);
        zzaf();
        zzcl();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat("*"));
            sb.append(" and name glob ?");
        }
        return zzb(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x012d  */
    public final List<zzo> zzb(String str, String[] strArr) {
        Throwable th;
        Object obj;
        zzaf();
        zzcl();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            Cursor cursor2 = getWritableDatabase().query("conditional_properties", new String[]{"app_id", "origin", "name", "value", ConditionalUserProperty.ACTIVE, ConditionalUserProperty.TRIGGER_EVENT_NAME, ConditionalUserProperty.TRIGGER_TIMEOUT, "timed_out_event", ConditionalUserProperty.CREATION_TIMESTAMP, "triggered_event", ConditionalUserProperty.TRIGGERED_TIMESTAMP, ConditionalUserProperty.TIME_TO_LIVE, "expired_event"}, str, strArr, null, null, "rowid", NativeContentAd.ASSET_HEADLINE);
            try {
                if (!cursor2.moveToFirst()) {
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    return arrayList;
                }
                while (true) {
                    if (arrayList.size() < 1000) {
                        boolean z = false;
                        String string = cursor2.getString(0);
                        String string2 = cursor2.getString(1);
                        String string3 = cursor2.getString(2);
                        Object zza = zza(cursor2, 3);
                        if (cursor2.getInt(4) != 0) {
                            z = true;
                        }
                        String string4 = cursor2.getString(5);
                        long j = cursor2.getLong(6);
                        zzag zzag = (zzag) zzjr().zza(cursor2.getBlob(7), zzag.CREATOR);
                        long j2 = cursor2.getLong(8);
                        zzag zzag2 = (zzag) zzjr().zza(cursor2.getBlob(9), zzag.CREATOR);
                        long j3 = cursor2.getLong(10);
                        long j4 = cursor2.getLong(11);
                        zzag zzag3 = (zzag) zzjr().zza(cursor2.getBlob(12), zzag.CREATOR);
                        zzfv zzfv = new zzfv(string3, j3, zza, string2);
                        boolean z2 = z;
                        zzo zzo = r4;
                        zzo zzo2 = new zzo(string, string2, zzfv, j2, z2, string4, zzag, j, zzag2, j4, zzag3);
                        arrayList.add(zzo);
                        if (!cursor2.moveToNext()) {
                            break;
                        }
                    } else {
                        zzgt().zzjg().zzg("Read more than the max allowed conditional properties, ignoring extra", Integer.valueOf(1000));
                        break;
                    }
                }
                if (cursor2 != null) {
                    cursor2.close();
                }
                return arrayList;
            } catch (SQLiteException e) {
                obj = e;
                cursor = cursor2;
                try {
                    zzgt().zzjg().zzg("Error querying conditional user property value", obj);
                    List<zzo> emptyList = Collections.emptyList();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyList;
                } catch (Throwable th2) {
                    th = th2;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            obj = e2;
            zzgt().zzjg().zzg("Error querying conditional user property value", obj);
            List<zzo> emptyList2 = Collections.emptyList();
            if (cursor != null) {
            }
            return emptyList2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0115 A[Catch:{ SQLiteException -> 0x01a9, all -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0119 A[Catch:{ SQLiteException -> 0x01a9, all -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x014d A[Catch:{ SQLiteException -> 0x01a9, all -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0150 A[Catch:{ SQLiteException -> 0x01a9, all -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x015f A[Catch:{ SQLiteException -> 0x01a9, all -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0190 A[Catch:{ SQLiteException -> 0x01a9, all -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01dd  */
    @WorkerThread
    public final zzg zzbm(String str) {
        Cursor cursor;
        Throwable th;
        Cursor cursor2;
        Object obj;
        boolean z;
        boolean z2;
        String str2 = str;
        Preconditions.checkNotEmpty(str);
        zzaf();
        zzcl();
        try {
            boolean z3 = false;
            cursor = getWritableDatabase().query("apps", new String[]{"app_instance_id", "gmp_app_id", "resettable_device_id_hash", "last_bundle_index", "last_bundle_start_timestamp", "last_bundle_end_timestamp", "app_version", "app_store", "gmp_version", "dev_cert_hash", "measurement_enabled", "day", "daily_public_events_count", "daily_events_count", "daily_conversions_count", "config_fetched_time", "failed_config_fetch_time", "app_version_int", "firebase_instance_id", "daily_error_events_count", "daily_realtime_events_count", "health_monitor_sample", "android_id", "adid_reporting_enabled", "ssaid_reporting_enabled", "admob_app_id"}, "app_id=?", new String[]{str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                try {
                    zzg zzg = new zzg(this.zzamv.zzmh(), str2);
                    zzg.zzaj(cursor.getString(0));
                    zzg.zzak(cursor.getString(1));
                    zzg.zzam(cursor.getString(2));
                    zzg.zzt(cursor.getLong(3));
                    zzg.zzo(cursor.getLong(4));
                    zzg.zzp(cursor.getLong(5));
                    zzg.setAppVersion(cursor.getString(6));
                    zzg.zzao(cursor.getString(7));
                    zzg.zzr(cursor.getLong(8));
                    zzg.zzs(cursor.getLong(9));
                    if (!cursor.isNull(10)) {
                        if (cursor.getInt(10) == 0) {
                            z = false;
                            zzg.setMeasurementEnabled(z);
                            zzg.zzw(cursor.getLong(11));
                            zzg.zzx(cursor.getLong(12));
                            zzg.zzy(cursor.getLong(13));
                            zzg.zzz(cursor.getLong(14));
                            zzg.zzu(cursor.getLong(15));
                            zzg.zzv(cursor.getLong(16));
                            zzg.zzq(!cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                            zzg.zzan(cursor.getString(18));
                            zzg.zzab(cursor.getLong(19));
                            zzg.zzaa(cursor.getLong(20));
                            zzg.zzap(cursor.getString(21));
                            zzg.zzac(!cursor.isNull(22) ? 0 : cursor.getLong(22));
                            if (!cursor.isNull(23)) {
                                if (cursor.getInt(23) == 0) {
                                    z2 = false;
                                    zzg.zze(z2);
                                    if (cursor.isNull(24) || cursor.getInt(24) != 0) {
                                        z3 = true;
                                    }
                                    zzg.zzf(z3);
                                    zzg.zzal(cursor.getString(25));
                                    zzg.zzha();
                                    if (cursor.moveToNext()) {
                                        zzgt().zzjg().zzg("Got multiple records for app, expected one. appId", zzas.zzbw(str));
                                    }
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return zzg;
                                }
                            }
                            z2 = true;
                            zzg.zze(z2);
                            z3 = true;
                            zzg.zzf(z3);
                            zzg.zzal(cursor.getString(25));
                            zzg.zzha();
                            if (cursor.moveToNext()) {
                            }
                            if (cursor != null) {
                            }
                            return zzg;
                        }
                    }
                    z = true;
                    zzg.setMeasurementEnabled(z);
                    zzg.zzw(cursor.getLong(11));
                    zzg.zzx(cursor.getLong(12));
                    zzg.zzy(cursor.getLong(13));
                    zzg.zzz(cursor.getLong(14));
                    zzg.zzu(cursor.getLong(15));
                    zzg.zzv(cursor.getLong(16));
                    zzg.zzq(!cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                    zzg.zzan(cursor.getString(18));
                    zzg.zzab(cursor.getLong(19));
                    zzg.zzaa(cursor.getLong(20));
                    zzg.zzap(cursor.getString(21));
                    zzg.zzac(!cursor.isNull(22) ? 0 : cursor.getLong(22));
                    if (!cursor.isNull(23)) {
                    }
                    z2 = true;
                    zzg.zze(z2);
                    z3 = true;
                    zzg.zzf(z3);
                    zzg.zzal(cursor.getString(25));
                    zzg.zzha();
                    if (cursor.moveToNext()) {
                    }
                    if (cursor != null) {
                    }
                    return zzg;
                } catch (SQLiteException e) {
                    e = e;
                    cursor2 = cursor;
                    obj = e;
                    try {
                        zzgt().zzjg().zze("Error querying app. appId", zzas.zzbw(str), obj);
                        if (cursor2 != null) {
                            cursor2.close();
                        }
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        cursor = cursor2;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                e = e2;
                cursor2 = cursor;
                obj = e;
                zzgt().zzjg().zze("Error querying app. appId", zzas.zzbw(str), obj);
                if (cursor2 != null) {
                }
                return null;
            } catch (Throwable th4) {
                th = th4;
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            obj = e3;
            cursor2 = null;
            zzgt().zzjg().zze("Error querying app. appId", zzas.zzbw(str), obj);
            if (cursor2 != null) {
            }
            return null;
        } catch (Throwable th5) {
            th = th5;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @WorkerThread
    public final void zza(zzg zzg) {
        Preconditions.checkNotNull(zzg);
        zzaf();
        zzcl();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzg.zzal());
        contentValues.put("app_instance_id", zzg.getAppInstanceId());
        contentValues.put("gmp_app_id", zzg.getGmpAppId());
        contentValues.put("resettable_device_id_hash", zzg.zzhc());
        contentValues.put("last_bundle_index", Long.valueOf(zzg.zzhj()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(zzg.zzhd()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(zzg.zzhe()));
        contentValues.put("app_version", zzg.zzak());
        contentValues.put("app_store", zzg.zzhg());
        contentValues.put("gmp_version", Long.valueOf(zzg.zzhh()));
        contentValues.put("dev_cert_hash", Long.valueOf(zzg.zzhi()));
        contentValues.put("measurement_enabled", Boolean.valueOf(zzg.isMeasurementEnabled()));
        contentValues.put("day", Long.valueOf(zzg.zzhn()));
        contentValues.put("daily_public_events_count", Long.valueOf(zzg.zzho()));
        contentValues.put("daily_events_count", Long.valueOf(zzg.zzhp()));
        contentValues.put("daily_conversions_count", Long.valueOf(zzg.zzhq()));
        contentValues.put("config_fetched_time", Long.valueOf(zzg.zzhk()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(zzg.zzhl()));
        contentValues.put("app_version_int", Long.valueOf(zzg.zzhf()));
        contentValues.put("firebase_instance_id", zzg.getFirebaseInstanceId());
        contentValues.put("daily_error_events_count", Long.valueOf(zzg.zzhs()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(zzg.zzhr()));
        contentValues.put("health_monitor_sample", zzg.zzht());
        contentValues.put("android_id", Long.valueOf(zzg.zzhv()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(zzg.zzhw()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(zzg.zzhx()));
        contentValues.put("admob_app_id", zzg.zzhb());
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (((long) writableDatabase.update("apps", contentValues, "app_id = ?", new String[]{zzg.zzal()})) == 0 && writableDatabase.insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                zzgt().zzjg().zzg("Failed to insert/update app (got -1). appId", zzas.zzbw(zzg.zzal()));
            }
        } catch (SQLiteException e) {
            zzgt().zzjg().zze("Error storing app. appId", zzas.zzbw(zzg.zzal()), e);
        }
    }

    public final long zzbn(String str) {
        Preconditions.checkNotEmpty(str);
        zzaf();
        zzcl();
        try {
            return (long) getWritableDatabase().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, zzgv().zzb(str, zzai.zzajj))))});
        } catch (SQLiteException e) {
            zzgt().zzjg().zze("Error deleting over the limit events. appId", zzas.zzbw(str), e);
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x011d  */
    @WorkerThread
    public final zzu zza(long j, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        Throwable th;
        Object obj;
        Preconditions.checkNotEmpty(str);
        zzaf();
        zzcl();
        String[] strArr = {str};
        zzu zzu = new zzu();
        Cursor cursor = null;
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            SQLiteDatabase sQLiteDatabase = writableDatabase;
            Cursor cursor2 = sQLiteDatabase.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor2.moveToFirst()) {
                    zzgt().zzjj().zzg("Not updating daily counts, app is not known. appId", zzas.zzbw(str));
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    return zzu;
                }
                if (cursor2.getLong(0) == j) {
                    zzu.zzahi = cursor2.getLong(1);
                    zzu.zzahh = cursor2.getLong(2);
                    zzu.zzahj = cursor2.getLong(3);
                    zzu.zzahk = cursor2.getLong(4);
                    zzu.zzahl = cursor2.getLong(5);
                }
                if (z) {
                    zzu.zzahi++;
                }
                if (z2) {
                    zzu.zzahh++;
                }
                if (z3) {
                    zzu.zzahj++;
                }
                if (z4) {
                    zzu.zzahk++;
                }
                if (z5) {
                    zzu.zzahl++;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("day", Long.valueOf(j));
                contentValues.put("daily_public_events_count", Long.valueOf(zzu.zzahh));
                contentValues.put("daily_events_count", Long.valueOf(zzu.zzahi));
                contentValues.put("daily_conversions_count", Long.valueOf(zzu.zzahj));
                contentValues.put("daily_error_events_count", Long.valueOf(zzu.zzahk));
                contentValues.put("daily_realtime_events_count", Long.valueOf(zzu.zzahl));
                writableDatabase.update("apps", contentValues, "app_id=?", strArr);
                if (cursor2 != null) {
                    cursor2.close();
                }
                return zzu;
            } catch (SQLiteException e) {
                obj = e;
                cursor = cursor2;
                try {
                    zzgt().zzjg().zze("Error updating daily counts. appId", zzas.zzbw(str), obj);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return zzu;
                } catch (Throwable th2) {
                    th = th2;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            obj = e2;
            zzgt().zzjg().zze("Error updating daily counts. appId", zzas.zzbw(str), obj);
            if (cursor != null) {
            }
            return zzu;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0073  */
    @WorkerThread
    public final byte[] zzbo(String str) {
        Cursor cursor;
        Preconditions.checkNotEmpty(str);
        zzaf();
        zzcl();
        try {
            cursor = getWritableDatabase().query("apps", new String[]{"remote_config"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                if (cursor.moveToNext()) {
                    zzgt().zzjg().zzg("Got multiple records for app config, expected one. appId", zzas.zzbw(str));
                }
                if (cursor != null) {
                    cursor.close();
                }
                return blob;
            } catch (SQLiteException e) {
                e = e;
                try {
                    zzgt().zzjg().zze("Error querying remote config. appId", zzas.zzbw(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
            zzgt().zzjg().zze("Error querying remote config. appId", zzas.zzbw(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @WorkerThread
    public final boolean zza(zzfw zzfw, boolean z) {
        zzaf();
        zzcl();
        Preconditions.checkNotNull(zzfw);
        Preconditions.checkNotEmpty(zzfw.zztt);
        Preconditions.checkNotNull(zzfw.zzaxm);
        zzij();
        long currentTimeMillis = zzbx().currentTimeMillis();
        if (zzfw.zzaxm.longValue() < currentTimeMillis - zzq.zzib() || zzfw.zzaxm.longValue() > zzq.zzib() + currentTimeMillis) {
            zzgt().zzjj().zzd("Storing bundle outside of the max uploading time span. appId, now, timestamp", zzas.zzbw(zzfw.zztt), Long.valueOf(currentTimeMillis), zzfw.zzaxm);
        }
        try {
            byte[] bArr = new byte[zzfw.zzvx()];
            zzya zzk = zzya.zzk(bArr, 0, bArr.length);
            zzfw.zza(zzk);
            zzk.zzza();
            byte[] zzb = zzjr().zzb(bArr);
            zzgt().zzjo().zzg("Saving bundle, size", Integer.valueOf(zzb.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zzfw.zztt);
            contentValues.put("bundle_end_timestamp", zzfw.zzaxm);
            contentValues.put(ShareConstants.WEB_DIALOG_PARAM_DATA, zzb);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            if (zzfw.zzayj != null) {
                contentValues.put("retry_count", zzfw.zzayj);
            }
            try {
                if (getWritableDatabase().insert("queue", null, contentValues) != -1) {
                    return true;
                }
                zzgt().zzjg().zzg("Failed to insert bundle (got -1). appId", zzas.zzbw(zzfw.zztt));
                return false;
            } catch (SQLiteException e) {
                zzgt().zzjg().zze("Error storing bundle. appId", zzas.zzbw(zzfw.zztt), e);
                return false;
            }
        } catch (IOException e2) {
            zzgt().zzjg().zze("Data loss. Failed to serialize bundle. appId", zzas.zzbw(zzfw.zztt), e2);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    @WorkerThread
    public final String zzih() {
        Cursor cursor;
        try {
            cursor = getWritableDatabase().rawQuery("select app_id from queue order by has_realtime desc, rowid asc limit 1;", null);
            try {
                if (cursor.moveToFirst()) {
                    String string = cursor.getString(0);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return string;
                }
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            } catch (SQLiteException e) {
                e = e;
                try {
                    zzgt().zzjg().zzg("Database error getting next bundle app id", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
            zzgt().zzjg().zzg("Database error getting next bundle app id", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final boolean zzii() {
        return zza("select count(1) > 0 from queue where has_realtime = 1", (String[]) null) != 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00f3  */
    @WorkerThread
    public final List<Pair<zzfw, Long>> zzb(String str, int i, int i2) {
        zzaf();
        zzcl();
        Preconditions.checkArgument(i > 0);
        Preconditions.checkArgument(i2 > 0);
        Preconditions.checkNotEmpty(str);
        Cursor cursor = null;
        try {
            Cursor cursor2 = getWritableDatabase().query("queue", new String[]{"rowid", ShareConstants.WEB_DIALOG_PARAM_DATA, "retry_count"}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(i));
            try {
                if (!cursor2.moveToFirst()) {
                    List<Pair<zzfw, Long>> emptyList = Collections.emptyList();
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    return emptyList;
                }
                ArrayList arrayList = new ArrayList();
                int i3 = 0;
                do {
                    long j = cursor2.getLong(0);
                    try {
                        byte[] zza = zzjr().zza(cursor2.getBlob(1));
                        if (!arrayList.isEmpty() && zza.length + i3 > i2) {
                            break;
                        }
                        zzxz zzj = zzxz.zzj(zza, 0, zza.length);
                        zzfw zzfw = new zzfw();
                        try {
                            zzfw.zza(zzj);
                            if (!cursor2.isNull(2)) {
                                zzfw.zzayj = Integer.valueOf(cursor2.getInt(2));
                            }
                            i3 += zza.length;
                            arrayList.add(Pair.create(zzfw, Long.valueOf(j)));
                        } catch (IOException e) {
                            zzgt().zzjg().zze("Failed to merge queued bundle. appId", zzas.zzbw(str), e);
                        }
                        if (!cursor2.moveToNext()) {
                            break;
                        }
                    } catch (IOException e2) {
                        zzgt().zzjg().zze("Failed to unzip queued bundle. appId", zzas.zzbw(str), e2);
                    }
                } while (i3 <= i2);
                if (cursor2 != null) {
                    cursor2.close();
                }
                return arrayList;
            } catch (SQLiteException e3) {
                e = e3;
                cursor = cursor2;
                try {
                    zzgt().zzjg().zze("Error querying bundles. appId", zzas.zzbw(str), e);
                    List<Pair<zzfw, Long>> emptyList2 = Collections.emptyList();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyList2;
                } catch (Throwable th) {
                    th = th;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            zzgt().zzjg().zze("Error querying bundles. appId", zzas.zzbw(str), e);
            List<Pair<zzfw, Long>> emptyList22 = Collections.emptyList();
            if (cursor != null) {
            }
            return emptyList22;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzij() {
        zzaf();
        zzcl();
        if (zzip()) {
            long j = zzgu().zzand.get();
            long elapsedRealtime = zzbx().elapsedRealtime();
            if (Math.abs(elapsedRealtime - j) > ((Long) zzai.zzajs.get()).longValue()) {
                zzgu().zzand.set(elapsedRealtime);
                zzaf();
                zzcl();
                if (zzip()) {
                    int delete = getWritableDatabase().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(zzbx().currentTimeMillis()), String.valueOf(zzq.zzib())});
                    if (delete > 0) {
                        zzgt().zzjo().zzg("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    @VisibleForTesting
    public final void zzc(List<Long> list) {
        zzaf();
        zzcl();
        Preconditions.checkNotNull(list);
        Preconditions.checkNotZero(list.size());
        if (zzip()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(2 + String.valueOf(join).length());
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(80 + String.valueOf(sb2).length());
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (zza(sb3.toString(), (String[]) null) > 0) {
                zzgt().zzjj().zzby("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                StringBuilder sb4 = new StringBuilder(127 + String.valueOf(sb2).length());
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                writableDatabase.execSQL(sb4.toString());
            } catch (SQLiteException e) {
                zzgt().zzjg().zzg("Error incrementing retry count. error", e);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zza(String str, zzfi[] zzfiArr) {
        boolean z;
        zzcl();
        zzaf();
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotNull(zzfiArr);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            zzcl();
            zzaf();
            Preconditions.checkNotEmpty(str);
            SQLiteDatabase writableDatabase2 = getWritableDatabase();
            writableDatabase2.delete("property_filters", "app_id=?", new String[]{str});
            writableDatabase2.delete("event_filters", "app_id=?", new String[]{str});
            for (zzfi zzfi : zzfiArr) {
                zzcl();
                zzaf();
                Preconditions.checkNotEmpty(str);
                Preconditions.checkNotNull(zzfi);
                Preconditions.checkNotNull(zzfi.zzavg);
                Preconditions.checkNotNull(zzfi.zzavf);
                if (zzfi.zzave == null) {
                    zzgt().zzjj().zzg("Audience with no ID. appId", zzas.zzbw(str));
                } else {
                    int intValue = zzfi.zzave.intValue();
                    zzfj[] zzfjArr = zzfi.zzavg;
                    int length = zzfjArr.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            zzfm[] zzfmArr = zzfi.zzavf;
                            int length2 = zzfmArr.length;
                            int i2 = 0;
                            while (true) {
                                if (i2 >= length2) {
                                    zzfj[] zzfjArr2 = zzfi.zzavg;
                                    int length3 = zzfjArr2.length;
                                    int i3 = 0;
                                    while (true) {
                                        if (i3 >= length3) {
                                            z = true;
                                            break;
                                        } else if (!zza(str, intValue, zzfjArr2[i3])) {
                                            z = false;
                                            break;
                                        } else {
                                            i3++;
                                        }
                                    }
                                    if (z) {
                                        zzfm[] zzfmArr2 = zzfi.zzavf;
                                        int length4 = zzfmArr2.length;
                                        int i4 = 0;
                                        while (true) {
                                            if (i4 >= length4) {
                                                break;
                                            } else if (!zza(str, intValue, zzfmArr2[i4])) {
                                                z = false;
                                                break;
                                            } else {
                                                i4++;
                                            }
                                        }
                                    }
                                    if (!z) {
                                        zzcl();
                                        zzaf();
                                        Preconditions.checkNotEmpty(str);
                                        SQLiteDatabase writableDatabase3 = getWritableDatabase();
                                        writableDatabase3.delete("property_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(intValue)});
                                        writableDatabase3.delete("event_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(intValue)});
                                    }
                                } else if (zzfmArr[i2].zzavk == null) {
                                    zzgt().zzjj().zze("Property filter with no ID. Audience definition ignored. appId, audienceId", zzas.zzbw(str), zzfi.zzave);
                                    break;
                                } else {
                                    i2++;
                                }
                            }
                        } else if (zzfjArr[i].zzavk == null) {
                            zzgt().zzjj().zze("Event filter with no ID. Audience definition ignored. appId, audienceId", zzas.zzbw(str), zzfi.zzave);
                            break;
                        } else {
                            i++;
                        }
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (zzfi zzfi2 : zzfiArr) {
                arrayList.add(zzfi2.zzave);
            }
            zza(str, (List<Integer>) arrayList);
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    @WorkerThread
    private final boolean zza(String str, int i, zzfj zzfj) {
        zzcl();
        zzaf();
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotNull(zzfj);
        if (TextUtils.isEmpty(zzfj.zzavl)) {
            zzgt().zzjj().zzd("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", zzas.zzbw(str), Integer.valueOf(i), String.valueOf(zzfj.zzavk));
            return false;
        }
        try {
            byte[] bArr = new byte[zzfj.zzvx()];
            zzya zzk = zzya.zzk(bArr, 0, bArr.length);
            zzfj.zza(zzk);
            zzk.zzza();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i));
            contentValues.put("filter_id", zzfj.zzavk);
            contentValues.put(MeasurementEvent.MEASUREMENT_EVENT_NAME_KEY, zzfj.zzavl);
            contentValues.put(ShareConstants.WEB_DIALOG_PARAM_DATA, bArr);
            try {
                if (getWritableDatabase().insertWithOnConflict("event_filters", null, contentValues, 5) == -1) {
                    zzgt().zzjg().zzg("Failed to insert event filter (got -1). appId", zzas.zzbw(str));
                }
                return true;
            } catch (SQLiteException e) {
                zzgt().zzjg().zze("Error storing event filter. appId", zzas.zzbw(str), e);
                return false;
            }
        } catch (IOException e2) {
            zzgt().zzjg().zze("Configuration loss. Failed to serialize event filter. appId", zzas.zzbw(str), e2);
            return false;
        }
    }

    @WorkerThread
    private final boolean zza(String str, int i, zzfm zzfm) {
        zzcl();
        zzaf();
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotNull(zzfm);
        if (TextUtils.isEmpty(zzfm.zzawa)) {
            zzgt().zzjj().zzd("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", zzas.zzbw(str), Integer.valueOf(i), String.valueOf(zzfm.zzavk));
            return false;
        }
        try {
            byte[] bArr = new byte[zzfm.zzvx()];
            zzya zzk = zzya.zzk(bArr, 0, bArr.length);
            zzfm.zza(zzk);
            zzk.zzza();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i));
            contentValues.put("filter_id", zzfm.zzavk);
            contentValues.put("property_name", zzfm.zzawa);
            contentValues.put(ShareConstants.WEB_DIALOG_PARAM_DATA, bArr);
            try {
                if (getWritableDatabase().insertWithOnConflict("property_filters", null, contentValues, 5) != -1) {
                    return true;
                }
                zzgt().zzjg().zzg("Failed to insert property filter (got -1). appId", zzas.zzbw(str));
                return false;
            } catch (SQLiteException e) {
                zzgt().zzjg().zze("Error storing property filter. appId", zzas.zzbw(str), e);
                return false;
            }
        } catch (IOException e2) {
            zzgt().zzjg().zze("Configuration loss. Failed to serialize property filter. appId", zzas.zzbw(str), e2);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b2  */
    public final Map<Integer, List<zzfj>> zzl(String str, String str2) {
        Cursor cursor;
        zzcl();
        zzaf();
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        ArrayMap arrayMap = new ArrayMap();
        try {
            cursor = getWritableDatabase().query("event_filters", new String[]{"audience_id", ShareConstants.WEB_DIALOG_PARAM_DATA}, "app_id=? AND event_name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<zzfj>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    zzxz zzj = zzxz.zzj(blob, 0, blob.length);
                    zzfj zzfj = new zzfj();
                    try {
                        zzfj.zza(zzj);
                        int i = cursor.getInt(0);
                        List list = (List) arrayMap.get(Integer.valueOf(i));
                        if (list == null) {
                            list = new ArrayList();
                            arrayMap.put(Integer.valueOf(i), list);
                        }
                        list.add(zzfj);
                    } catch (IOException e) {
                        zzgt().zzjg().zze("Failed to merge filter. appId", zzas.zzbw(str), e);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayMap;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    zzgt().zzjg().zze("Database error querying filters. appId", zzas.zzbw(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            zzgt().zzjg().zze("Database error querying filters. appId", zzas.zzbw(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b2  */
    public final Map<Integer, List<zzfm>> zzm(String str, String str2) {
        Cursor cursor;
        zzcl();
        zzaf();
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        ArrayMap arrayMap = new ArrayMap();
        try {
            cursor = getWritableDatabase().query("property_filters", new String[]{"audience_id", ShareConstants.WEB_DIALOG_PARAM_DATA}, "app_id=? AND property_name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<zzfm>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    zzxz zzj = zzxz.zzj(blob, 0, blob.length);
                    zzfm zzfm = new zzfm();
                    try {
                        zzfm.zza(zzj);
                        int i = cursor.getInt(0);
                        List list = (List) arrayMap.get(Integer.valueOf(i));
                        if (list == null) {
                            list = new ArrayList();
                            arrayMap.put(Integer.valueOf(i), list);
                        }
                        list.add(zzfm);
                    } catch (IOException e) {
                        zzgt().zzjg().zze("Failed to merge filter", zzas.zzbw(str), e);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayMap;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    zzgt().zzjg().zze("Database error querying filters. appId", zzas.zzbw(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            zzgt().zzjg().zze("Database error querying filters. appId", zzas.zzbw(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private final boolean zza(String str, List<Integer> list) {
        Preconditions.checkNotEmpty(str);
        zzcl();
        zzaf();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        try {
            long zza = zza("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(2000, zzgv().zzb(str, zzai.zzajz)));
            if (zza <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                Integer num = (Integer) list.get(i);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(140 + String.valueOf(sb2).length());
            sb3.append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ");
            sb3.append(sb2);
            sb3.append(" order by rowid desc limit -1 offset ?)");
            return writableDatabase.delete("audience_filter_values", sb3.toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e) {
            zzgt().zzjg().zze("Database error querying filters. appId", zzas.zzbw(str), e);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0098  */
    public final Map<Integer, zzfx> zzbp(String str) {
        Cursor cursor;
        zzcl();
        zzaf();
        Preconditions.checkNotEmpty(str);
        try {
            cursor = getWritableDatabase().query("audience_filter_values", new String[]{"audience_id", "current_results"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                ArrayMap arrayMap = new ArrayMap();
                do {
                    int i = cursor.getInt(0);
                    byte[] blob = cursor.getBlob(1);
                    zzxz zzj = zzxz.zzj(blob, 0, blob.length);
                    zzfx zzfx = new zzfx();
                    try {
                        zzfx.zza(zzj);
                        arrayMap.put(Integer.valueOf(i), zzfx);
                    } catch (IOException e) {
                        zzgt().zzjg().zzd("Failed to merge filter results. appId, audienceId, error", zzas.zzbw(str), Integer.valueOf(i), e);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayMap;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    zzgt().zzjg().zze("Database error querying filter results. appId", zzas.zzbw(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            zzgt().zzjg().zze("Database error querying filter results. appId", zzas.zzbw(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @WorkerThread
    private static void zza(ContentValues contentValues, String str, Object obj) {
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotNull(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final Object zza(Cursor cursor, int i) {
        int type = cursor.getType(i);
        switch (type) {
            case 0:
                zzgt().zzjg().zzby("Loaded invalid null value from database");
                return null;
            case 1:
                return Long.valueOf(cursor.getLong(i));
            case 2:
                return Double.valueOf(cursor.getDouble(i));
            case 3:
                return cursor.getString(i);
            case 4:
                zzgt().zzjg().zzby("Loaded invalid blob type value, ignoring it");
                return null;
            default:
                zzgt().zzjg().zzg("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
        }
    }

    @WorkerThread
    public final long zzik() {
        return zza("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    @VisibleForTesting
    public final long zzn(String str, String str2) {
        long j;
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        zzaf();
        zzcl();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            StringBuilder sb = new StringBuilder(32 + String.valueOf(str2).length());
            sb.append("select ");
            sb.append(str2);
            sb.append(" from app2 where app_id=?");
            j = zza(sb.toString(), new String[]{str}, -1);
            if (j == -1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("app_id", str);
                contentValues.put("first_open_count", Integer.valueOf(0));
                contentValues.put("previous_install_count", Integer.valueOf(0));
                if (writableDatabase.insertWithOnConflict("app2", null, contentValues, 5) == -1) {
                    zzgt().zzjg().zze("Failed to insert column (got -1). appId", zzas.zzbw(str), str2);
                    writableDatabase.endTransaction();
                    return -1;
                }
                j = 0;
            }
            try {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str);
                contentValues2.put(str2, Long.valueOf(1 + j));
                if (((long) writableDatabase.update("app2", contentValues2, "app_id = ?", new String[]{str})) == 0) {
                    zzgt().zzjg().zze("Failed to update column (got 0). appId", zzas.zzbw(str), str2);
                    writableDatabase.endTransaction();
                    return -1;
                }
                writableDatabase.setTransactionSuccessful();
                writableDatabase.endTransaction();
                return j;
            } catch (SQLiteException e) {
                e = e;
                try {
                    zzgt().zzjg().zzd("Error inserting column. appId", zzas.zzbw(str), str2, e);
                    return j;
                } finally {
                    writableDatabase.endTransaction();
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            j = 0;
            zzgt().zzjg().zzd("Error inserting column. appId", zzas.zzbw(str), str2, e);
            return j;
        }
    }

    @WorkerThread
    public final long zzil() {
        return zza("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    public final long zza(zzfw zzfw) throws IOException {
        long j;
        zzaf();
        zzcl();
        Preconditions.checkNotNull(zzfw);
        Preconditions.checkNotEmpty(zzfw.zztt);
        try {
            byte[] bArr = new byte[zzfw.zzvx()];
            zzya zzk = zzya.zzk(bArr, 0, bArr.length);
            zzfw.zza(zzk);
            zzk.zzza();
            zzfu zzjr = zzjr();
            Preconditions.checkNotNull(bArr);
            zzjr.zzgr().zzaf();
            MessageDigest messageDigest = zzfy.getMessageDigest();
            if (messageDigest == null) {
                zzjr.zzgt().zzjg().zzby("Failed to get MD5");
                j = 0;
            } else {
                j = zzfy.zzc(messageDigest.digest(bArr));
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zzfw.zztt);
            contentValues.put("metadata_fingerprint", Long.valueOf(j));
            contentValues.put(TtmlNode.TAG_METADATA, bArr);
            try {
                getWritableDatabase().insertWithOnConflict("raw_events_metadata", null, contentValues, 4);
                return j;
            } catch (SQLiteException e) {
                zzgt().zzjg().zze("Error storing raw event metadata. appId", zzas.zzbw(zzfw.zztt), e);
                throw e;
            }
        } catch (IOException e2) {
            zzgt().zzjg().zze("Data loss. Failed to serialize event metadata. appId", zzas.zzbw(zzfw.zztt), e2);
            throw e2;
        }
    }

    public final boolean zzim() {
        return zza("select count(1) > 0 from raw_events", (String[]) null) != 0;
    }

    public final boolean zzin() {
        return zza("select count(1) > 0 from raw_events where realtime = 1", (String[]) null) != 0;
    }

    public final long zzbq(String str) {
        Preconditions.checkNotEmpty(str);
        return zza("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b  */
    public final String zzad(long j) {
        Cursor cursor;
        zzaf();
        zzcl();
        try {
            cursor = getWritableDatabase().rawQuery("select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;", new String[]{String.valueOf(j)});
            try {
                if (!cursor.moveToFirst()) {
                    zzgt().zzjo().zzby("No expired configs for apps with pending events");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                if (cursor != null) {
                    cursor.close();
                }
                return string;
            } catch (SQLiteException e) {
                e = e;
                try {
                    zzgt().zzjg().zzg("Error selecting expired configs", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
            zzgt().zzjg().zzg("Error selecting expired configs", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0044  */
    public final long zzio() {
        Cursor cursor = null;
        try {
            Cursor rawQuery = getWritableDatabase().rawQuery("select rowid from raw_events order by rowid desc limit 1;", null);
            try {
                if (!rawQuery.moveToFirst()) {
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    return -1;
                }
                long j = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j;
            } catch (SQLiteException e) {
                Cursor cursor2 = rawQuery;
                e = e;
                cursor = cursor2;
                try {
                    zzgt().zzjg().zzg("Error querying raw events", e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return -1;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                cursor = rawQuery;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            e = e2;
            zzgt().zzjg().zzg("Error querying raw events", e);
            if (cursor != null) {
            }
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008f  */
    public final Pair<zzft, Long> zza(String str, Long l) {
        Cursor cursor;
        zzaf();
        zzcl();
        try {
            cursor = getWritableDatabase().rawQuery("select main_event, children_to_process from main_event_params where app_id=? and event_id=?", new String[]{str, String.valueOf(l)});
            try {
                if (!cursor.moveToFirst()) {
                    zzgt().zzjo().zzby("Main event not found");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                Long valueOf = Long.valueOf(cursor.getLong(1));
                zzxz zzj = zzxz.zzj(blob, 0, blob.length);
                zzft zzft = new zzft();
                try {
                    zzft.zza(zzj);
                    Pair<zzft, Long> create = Pair.create(zzft, valueOf);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return create;
                } catch (IOException e) {
                    zzgt().zzjg().zzd("Failed to merge main event. appId, eventId", zzas.zzbw(str), l, e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    zzgt().zzjg().zzg("Error selecting main event", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            zzgt().zzjg().zzg("Error selecting main event", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final boolean zza(String str, Long l, long j, zzft zzft) {
        zzaf();
        zzcl();
        Preconditions.checkNotNull(zzft);
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotNull(l);
        try {
            byte[] bArr = new byte[zzft.zzvx()];
            zzya zzk = zzya.zzk(bArr, 0, bArr.length);
            zzft.zza(zzk);
            zzk.zzza();
            zzgt().zzjo().zze("Saving complex main event, appId, data size", zzgq().zzbt(str), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("event_id", l);
            contentValues.put("children_to_process", Long.valueOf(j));
            contentValues.put("main_event", bArr);
            try {
                if (getWritableDatabase().insertWithOnConflict("main_event_params", null, contentValues, 5) != -1) {
                    return true;
                }
                zzgt().zzjg().zzg("Failed to insert complex main event (got -1). appId", zzas.zzbw(str));
                return false;
            } catch (SQLiteException e) {
                zzgt().zzjg().zze("Error storing complex main event. appId", zzas.zzbw(str), e);
                return false;
            }
        } catch (IOException e2) {
            zzgt().zzjg().zzd("Data loss. Failed to serialize event params/data. appId, eventId", zzas.zzbw(str), l, e2);
            return false;
        }
    }

    public final boolean zza(zzab zzab, long j, boolean z) {
        zzaf();
        zzcl();
        Preconditions.checkNotNull(zzab);
        Preconditions.checkNotEmpty(zzab.zztt);
        zzft zzft = new zzft();
        zzft.zzaxc = Long.valueOf(zzab.zzaht);
        zzft.zzaxa = new zzfu[zzab.zzahu.size()];
        Iterator it = zzab.zzahu.iterator();
        int i = 0;
        while (it.hasNext()) {
            String str = (String) it.next();
            zzfu zzfu = new zzfu();
            int i2 = i + 1;
            zzft.zzaxa[i] = zzfu;
            zzfu.name = str;
            zzjr().zza(zzfu, zzab.zzahu.get(str));
            i = i2;
        }
        try {
            byte[] bArr = new byte[zzft.zzvx()];
            zzya zzk = zzya.zzk(bArr, 0, bArr.length);
            zzft.zza(zzk);
            zzk.zzza();
            zzgt().zzjo().zze("Saving event, name, data size", zzgq().zzbt(zzab.name), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zzab.zztt);
            contentValues.put("name", zzab.name);
            contentValues.put(Param.TIMESTAMP, Long.valueOf(zzab.timestamp));
            contentValues.put("metadata_fingerprint", Long.valueOf(j));
            contentValues.put(ShareConstants.WEB_DIALOG_PARAM_DATA, bArr);
            contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
            try {
                if (getWritableDatabase().insert("raw_events", null, contentValues) != -1) {
                    return true;
                }
                zzgt().zzjg().zzg("Failed to insert raw event (got -1). appId", zzas.zzbw(zzab.zztt));
                return false;
            } catch (SQLiteException e) {
                zzgt().zzjg().zze("Error storing raw event. appId", zzas.zzbw(zzab.zztt), e);
                return false;
            }
        } catch (IOException e2) {
            zzgt().zzjg().zze("Data loss. Failed to serialize event params/data. appId", zzas.zzbw(zzab.zztt), e2);
            return false;
        }
    }

    private final boolean zzip() {
        return getContext().getDatabasePath("google_app_measurement.db").exists();
    }
}
