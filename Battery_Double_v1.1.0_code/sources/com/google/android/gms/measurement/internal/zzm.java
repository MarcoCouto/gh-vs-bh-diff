package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzfj;
import com.google.android.gms.internal.measurement.zzfk;
import com.google.android.gms.internal.measurement.zzfl;
import com.google.android.gms.internal.measurement.zzfm;
import com.google.android.gms.internal.measurement.zzfn;
import com.google.android.gms.internal.measurement.zzfr;
import com.google.android.gms.internal.measurement.zzfs;
import com.google.android.gms.internal.measurement.zzft;
import com.google.android.gms.internal.measurement.zzfu;
import com.google.android.gms.internal.measurement.zzfx;
import com.google.android.gms.internal.measurement.zzfy;
import com.google.android.gms.internal.measurement.zzfz;
import com.google.android.gms.internal.measurement.zzya;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class zzm extends zzfn {
    zzm(zzfo zzfo) {
        super(zzfo);
    }

    /* access modifiers changed from: protected */
    public final boolean zzgy() {
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x02e6  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0328  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0395  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x03f0  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0445  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x045a  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x046b  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01d3  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x02b0  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x02c8  */
    @WorkerThread
    public final zzfr[] zza(String str, zzft[] zzftArr, zzfz[] zzfzArr) {
        int i;
        ArrayMap arrayMap;
        Iterator it;
        ArrayMap arrayMap2;
        ArrayMap arrayMap3;
        ArrayMap arrayMap4;
        zzfy[] zzfyArr;
        int i2;
        Iterator it2;
        Map map;
        int i3;
        BitSet bitSet;
        ArrayMap arrayMap5;
        ArrayMap arrayMap6;
        Map map2;
        ArrayMap arrayMap7;
        ArrayMap arrayMap8;
        BitSet bitSet2;
        Map map3;
        Map map4;
        int i4;
        ArrayMap arrayMap9;
        ArrayMap arrayMap10;
        BitSet bitSet3;
        ArrayMap arrayMap11;
        ArrayMap arrayMap12;
        ArrayMap arrayMap13;
        ArrayMap arrayMap14;
        ArrayMap arrayMap15;
        ArrayMap arrayMap16;
        int i5;
        int i6;
        HashSet hashSet;
        String str2;
        ArrayMap arrayMap17;
        zzfu[] zzfuArr;
        long j;
        Long l;
        zzft zzft;
        long j2;
        zzft zzft2;
        String str3;
        zzac zzg;
        zzft zzft3;
        ArrayMap arrayMap18;
        ArrayMap arrayMap19;
        ArrayMap arrayMap20;
        ArrayMap arrayMap21;
        ArrayMap arrayMap22;
        HashSet hashSet2;
        zzac zzac;
        Map map5;
        String str4;
        Iterator it3;
        Iterator it4;
        Map map6;
        BitSet bitSet4;
        ArrayMap arrayMap23;
        Map map7;
        ArrayMap arrayMap24;
        ArrayMap arrayMap25;
        Map map8;
        Map map9;
        BitSet bitSet5;
        BitSet bitSet6;
        ArrayMap arrayMap26;
        ArrayMap arrayMap27;
        Iterator it5;
        BitSet bitSet7;
        String str5;
        ArrayMap arrayMap28;
        ArrayMap arrayMap29;
        ArrayMap arrayMap30;
        String str6;
        Iterator it6;
        zzft zzft4;
        BitSet bitSet8;
        HashSet hashSet3;
        BitSet bitSet9;
        Map map10;
        Map map11;
        BitSet bitSet10;
        BitSet bitSet11;
        ArrayMap arrayMap31;
        int i7;
        int i8;
        zzft zzft5;
        zzft zzft6;
        String str7;
        int i9;
        boolean z;
        ArrayMap arrayMap32;
        int i10;
        int i11;
        zzfu[] zzfuArr2;
        int i12;
        zzft zzft7;
        HashSet hashSet4;
        Iterator it7;
        BitSet bitSet12;
        ArrayMap arrayMap33;
        BitSet bitSet13;
        ArrayMap arrayMap34;
        ArrayMap arrayMap35;
        ArrayMap arrayMap36;
        boolean z2;
        String str8 = str;
        zzft[] zzftArr2 = zzftArr;
        zzfz[] zzfzArr2 = zzfzArr;
        Preconditions.checkNotEmpty(str);
        HashSet hashSet5 = new HashSet();
        ArrayMap arrayMap37 = new ArrayMap();
        ArrayMap arrayMap38 = new ArrayMap();
        ArrayMap arrayMap39 = new ArrayMap();
        ArrayMap arrayMap40 = new ArrayMap();
        ArrayMap arrayMap41 = new ArrayMap();
        boolean zzbb = zzgv().zzbb(str8);
        Map zzbp = zzjt().zzbp(str8);
        if (zzbp != null) {
            Iterator it8 = zzbp.keySet().iterator();
            while (it8.hasNext()) {
                int intValue = ((Integer) it8.next()).intValue();
                zzfx zzfx = (zzfx) zzbp.get(Integer.valueOf(intValue));
                BitSet bitSet14 = (BitSet) arrayMap38.get(Integer.valueOf(intValue));
                Map map12 = zzbp;
                BitSet bitSet15 = (BitSet) arrayMap39.get(Integer.valueOf(intValue));
                if (zzbb) {
                    bitSet12 = bitSet15;
                    arrayMap33 = new ArrayMap();
                    if (zzfx != null) {
                        it7 = it8;
                        if (zzfx.zzayp != null) {
                            zzfs[] zzfsArr = zzfx.zzayp;
                            hashSet4 = hashSet5;
                            int length = zzfsArr.length;
                            int i13 = 0;
                            while (i13 < length) {
                                int i14 = length;
                                zzfs zzfs = zzfsArr[i13];
                                zzfs[] zzfsArr2 = zzfsArr;
                                if (zzfs.zzawx != null) {
                                    arrayMap33.put(zzfs.zzawx, zzfs.zzawy);
                                }
                                i13++;
                                length = i14;
                                zzfsArr = zzfsArr2;
                            }
                            arrayMap40.put(Integer.valueOf(intValue), arrayMap33);
                        }
                    } else {
                        it7 = it8;
                    }
                    hashSet4 = hashSet5;
                    arrayMap40.put(Integer.valueOf(intValue), arrayMap33);
                } else {
                    bitSet12 = bitSet15;
                    it7 = it8;
                    hashSet4 = hashSet5;
                    arrayMap33 = null;
                }
                if (bitSet14 == null) {
                    bitSet14 = new BitSet();
                    arrayMap38.put(Integer.valueOf(intValue), bitSet14);
                    bitSet13 = new BitSet();
                    arrayMap39.put(Integer.valueOf(intValue), bitSet13);
                } else {
                    bitSet13 = bitSet12;
                }
                int i15 = 0;
                while (i15 < (zzfx.zzayn.length << 6)) {
                    if (zzfu.zza(zzfx.zzayn, i15)) {
                        arrayMap36 = arrayMap40;
                        arrayMap35 = arrayMap39;
                        arrayMap34 = arrayMap38;
                        zzgt().zzjo().zze("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue), Integer.valueOf(i15));
                        bitSet13.set(i15);
                        if (zzfu.zza(zzfx.zzayo, i15)) {
                            bitSet14.set(i15);
                            z2 = true;
                            if (arrayMap33 != null && !z2) {
                                arrayMap33.remove(Integer.valueOf(i15));
                            }
                            i15++;
                            arrayMap40 = arrayMap36;
                            arrayMap39 = arrayMap35;
                            arrayMap38 = arrayMap34;
                        }
                    } else {
                        arrayMap36 = arrayMap40;
                        arrayMap35 = arrayMap39;
                        arrayMap34 = arrayMap38;
                    }
                    z2 = false;
                    arrayMap33.remove(Integer.valueOf(i15));
                    i15++;
                    arrayMap40 = arrayMap36;
                    arrayMap39 = arrayMap35;
                    arrayMap38 = arrayMap34;
                }
                ArrayMap arrayMap42 = arrayMap40;
                ArrayMap arrayMap43 = arrayMap39;
                ArrayMap arrayMap44 = arrayMap38;
                zzfr zzfr = new zzfr();
                arrayMap37.put(Integer.valueOf(intValue), zzfr);
                zzfr.zzawv = Boolean.valueOf(false);
                zzfr.zzawu = zzfx;
                zzfr.zzawt = new zzfx();
                zzfr.zzawt.zzayo = zzfu.zza(bitSet14);
                zzfr.zzawt.zzayn = zzfu.zza(bitSet13);
                if (zzbb) {
                    zzfr.zzawt.zzayp = zzb(arrayMap33);
                    arrayMap41.put(Integer.valueOf(intValue), new ArrayMap());
                }
                zzbp = map12;
                it8 = it7;
                hashSet5 = hashSet4;
                arrayMap40 = arrayMap42;
                arrayMap39 = arrayMap43;
                arrayMap38 = arrayMap44;
                zzfz[] zzfzArr3 = zzfzArr;
            }
        }
        ArrayMap arrayMap45 = arrayMap40;
        ArrayMap arrayMap46 = arrayMap39;
        ArrayMap arrayMap47 = arrayMap38;
        HashSet hashSet6 = hashSet5;
        if (zzftArr2 != null) {
            ArrayMap arrayMap48 = new ArrayMap();
            int length2 = zzftArr2.length;
            zzft zzft8 = null;
            Long l2 = null;
            int i16 = 0;
            long j3 = 0;
            while (i16 < length2) {
                zzft zzft9 = zzftArr2[i16];
                String str9 = zzft9.name;
                zzfu[] zzfuArr3 = zzft9.zzaxa;
                if (zzgv().zzd(str8, zzai.zzaki)) {
                    zzjr();
                    Long l3 = (Long) zzfu.zzb(zzft9, "_eid");
                    boolean z3 = l3 != null;
                    if (z3) {
                        i9 = length2;
                        if (str9.equals("_ep")) {
                            z = true;
                            if (!z) {
                                zzjr();
                                String str10 = (String) zzfu.zzb(zzft9, "_en");
                                if (TextUtils.isEmpty(str10)) {
                                    zzgt().zzjg().zzg("Extra parameter without an event name. eventId", l3);
                                    i10 = i16;
                                    arrayMap32 = arrayMap41;
                                    i11 = i9;
                                } else {
                                    if (zzft8 == null || l2 == null || l3.longValue() != l2.longValue()) {
                                        Pair zza = zzjt().zza(str8, l3);
                                        if (zza == null || zza.first == null) {
                                            i10 = i16;
                                            arrayMap32 = arrayMap41;
                                            i11 = i9;
                                            zzgt().zzjg().zze("Extra parameter without existing main event. eventName, eventId", str10, l3);
                                        } else {
                                            zzft8 = (zzft) zza.first;
                                            j3 = ((Long) zza.second).longValue();
                                            zzjr();
                                            l2 = (Long) zzfu.zzb(zzft8, "_eid");
                                        }
                                    }
                                    zzft zzft10 = zzft8;
                                    Long l4 = l2;
                                    long j4 = j3 - 1;
                                    if (j4 <= 0) {
                                        zzt zzjt = zzjt();
                                        zzjt.zzaf();
                                        zzjt.zzgt().zzjo().zzg("Clearing complex main event info. appId", str8);
                                        try {
                                            SQLiteDatabase writableDatabase = zzjt.getWritableDatabase();
                                            String str11 = "delete from main_event_params where app_id=?";
                                            zzft7 = zzft9;
                                            try {
                                                String[] strArr = new String[1];
                                                try {
                                                    strArr[0] = str8;
                                                    writableDatabase.execSQL(str11, strArr);
                                                } catch (SQLiteException e) {
                                                    e = e;
                                                }
                                            } catch (SQLiteException e2) {
                                                e = e2;
                                                zzjt.zzgt().zzjg().zzg("Error clearing complex main event", e);
                                                i5 = i16;
                                                arrayMap17 = arrayMap41;
                                                i6 = i9;
                                                zzft2 = zzft7;
                                                zzfu[] zzfuArr4 = new zzfu[(zzft10.zzaxa.length + zzfuArr3.length)];
                                                i12 = 0;
                                                while (r4 < r3) {
                                                }
                                                if (i12 > 0) {
                                                }
                                                j = j4;
                                                l = l4;
                                                j2 = 0;
                                                zzg = zzjt().zzg(str8, zzft2.name);
                                                if (zzg == null) {
                                                }
                                                zzjt().zza(zzac);
                                                long j5 = zzac.zzahv;
                                                ArrayMap arrayMap49 = arrayMap20;
                                                map5 = (Map) arrayMap49.get(str3);
                                                if (map5 == null) {
                                                }
                                                Map map13 = map5;
                                                it3 = map13.keySet().iterator();
                                                while (it3.hasNext()) {
                                                }
                                                arrayMap12 = arrayMap22;
                                                arrayMap16 = arrayMap49;
                                                str2 = str4;
                                                arrayMap15 = arrayMap17;
                                                arrayMap11 = arrayMap21;
                                                arrayMap13 = arrayMap19;
                                                arrayMap14 = arrayMap18;
                                                hashSet = hashSet2;
                                                zzft8 = zzft;
                                                l2 = l;
                                                j3 = j;
                                                i16 = i5 + 1;
                                                zzftArr2 = zzftArr;
                                                str8 = str2;
                                                hashSet6 = hashSet;
                                                length2 = i6;
                                                arrayMap48 = arrayMap16;
                                                arrayMap41 = arrayMap15;
                                                arrayMap46 = arrayMap14;
                                                arrayMap37 = arrayMap13;
                                                arrayMap45 = arrayMap12;
                                                arrayMap47 = arrayMap11;
                                            }
                                        } catch (SQLiteException e3) {
                                            e = e3;
                                            zzft7 = zzft9;
                                            zzjt.zzgt().zzjg().zzg("Error clearing complex main event", e);
                                            i5 = i16;
                                            arrayMap17 = arrayMap41;
                                            i6 = i9;
                                            zzft2 = zzft7;
                                            zzfu[] zzfuArr42 = new zzfu[(zzft10.zzaxa.length + zzfuArr3.length)];
                                            i12 = 0;
                                            while (r4 < r3) {
                                            }
                                            if (i12 > 0) {
                                            }
                                            j = j4;
                                            l = l4;
                                            j2 = 0;
                                            zzg = zzjt().zzg(str8, zzft2.name);
                                            if (zzg == null) {
                                            }
                                            zzjt().zza(zzac);
                                            long j52 = zzac.zzahv;
                                            ArrayMap arrayMap492 = arrayMap20;
                                            map5 = (Map) arrayMap492.get(str3);
                                            if (map5 == null) {
                                            }
                                            Map map132 = map5;
                                            it3 = map132.keySet().iterator();
                                            while (it3.hasNext()) {
                                            }
                                            arrayMap12 = arrayMap22;
                                            arrayMap16 = arrayMap492;
                                            str2 = str4;
                                            arrayMap15 = arrayMap17;
                                            arrayMap11 = arrayMap21;
                                            arrayMap13 = arrayMap19;
                                            arrayMap14 = arrayMap18;
                                            hashSet = hashSet2;
                                            zzft8 = zzft;
                                            l2 = l;
                                            j3 = j;
                                            i16 = i5 + 1;
                                            zzftArr2 = zzftArr;
                                            str8 = str2;
                                            hashSet6 = hashSet;
                                            length2 = i6;
                                            arrayMap48 = arrayMap16;
                                            arrayMap41 = arrayMap15;
                                            arrayMap46 = arrayMap14;
                                            arrayMap37 = arrayMap13;
                                            arrayMap45 = arrayMap12;
                                            arrayMap47 = arrayMap11;
                                        }
                                        i5 = i16;
                                        arrayMap17 = arrayMap41;
                                        i6 = i9;
                                        zzft2 = zzft7;
                                    } else {
                                        zzft2 = zzft9;
                                        i5 = i16;
                                        i6 = i9;
                                        arrayMap17 = arrayMap41;
                                        zzjt().zza(str8, l3, j4, zzft10);
                                    }
                                    zzfu[] zzfuArr422 = new zzfu[(zzft10.zzaxa.length + zzfuArr3.length)];
                                    i12 = 0;
                                    for (zzfu zzfu : zzft10.zzaxa) {
                                        zzjr();
                                        if (zzfu.zza(zzft2, zzfu.name) == null) {
                                            int i17 = i12 + 1;
                                            zzfuArr422[i12] = zzfu;
                                            i12 = i17;
                                        }
                                    }
                                    if (i12 > 0) {
                                        int length3 = zzfuArr3.length;
                                        int i18 = 0;
                                        while (i18 < length3) {
                                            int i19 = i12 + 1;
                                            zzfuArr422[i12] = zzfuArr3[i18];
                                            i18++;
                                            i12 = i19;
                                        }
                                        if (i12 != zzfuArr422.length) {
                                            zzfuArr422 = (zzfu[]) Arrays.copyOf(zzfuArr422, i12);
                                        }
                                        zzfuArr = zzfuArr422;
                                        str3 = str10;
                                        zzft = zzft10;
                                    } else {
                                        zzgt().zzjj().zzg("No unique parameters in main event. eventName", str10);
                                        str3 = str10;
                                        zzft = zzft10;
                                        zzfuArr = zzfuArr3;
                                    }
                                    j = j4;
                                    l = l4;
                                    j2 = 0;
                                    zzg = zzjt().zzg(str8, zzft2.name);
                                    if (zzg == null) {
                                        zzgt().zzjj().zze("Event aggregate wasn't created during raw event logging. appId, event", zzas.zzbw(str), zzgq().zzbt(str3));
                                        long j6 = j2;
                                        arrayMap20 = arrayMap48;
                                        arrayMap21 = arrayMap47;
                                        arrayMap18 = arrayMap46;
                                        arrayMap19 = arrayMap37;
                                        hashSet2 = hashSet6;
                                        zzft3 = zzft2;
                                        arrayMap22 = arrayMap45;
                                        zzfz[] zzfzArr4 = zzfzArr;
                                        zzac = new zzac(str8, zzft2.name, 1, 1, zzft2.zzaxb.longValue(), 0, null, null, null, null);
                                    } else {
                                        arrayMap20 = arrayMap48;
                                        arrayMap19 = arrayMap37;
                                        zzft3 = zzft2;
                                        long j7 = j2;
                                        hashSet2 = hashSet6;
                                        arrayMap22 = arrayMap45;
                                        arrayMap18 = arrayMap46;
                                        arrayMap21 = arrayMap47;
                                        zzfz[] zzfzArr5 = zzfzArr;
                                        zzac zzac2 = new zzac(zzg.zztt, zzg.name, zzg.zzahv + 1, zzg.zzahw + 1, zzg.zzahx, zzg.zzahy, zzg.zzahz, zzg.zzaia, zzg.zzaib, zzg.zzaic);
                                        zzac = zzac2;
                                    }
                                    zzjt().zza(zzac);
                                    long j522 = zzac.zzahv;
                                    ArrayMap arrayMap4922 = arrayMap20;
                                    map5 = (Map) arrayMap4922.get(str3);
                                    if (map5 == null) {
                                        str4 = str;
                                        map5 = zzjt().zzl(str4, str3);
                                        if (map5 == null) {
                                            map5 = new ArrayMap();
                                        }
                                        arrayMap4922.put(str3, map5);
                                    } else {
                                        str4 = str;
                                    }
                                    Map map1322 = map5;
                                    it3 = map1322.keySet().iterator();
                                    while (it3.hasNext()) {
                                        int intValue2 = ((Integer) it3.next()).intValue();
                                        if (hashSet2.contains(Integer.valueOf(intValue2))) {
                                            zzgt().zzjo().zzg("Skipping failed audience ID", Integer.valueOf(intValue2));
                                        } else {
                                            ArrayMap arrayMap50 = arrayMap19;
                                            zzfr zzfr2 = (zzfr) arrayMap50.get(Integer.valueOf(intValue2));
                                            ArrayMap arrayMap51 = arrayMap21;
                                            BitSet bitSet16 = (BitSet) arrayMap51.get(Integer.valueOf(intValue2));
                                            HashSet hashSet7 = hashSet2;
                                            ArrayMap arrayMap52 = arrayMap4922;
                                            ArrayMap arrayMap53 = arrayMap18;
                                            BitSet bitSet17 = (BitSet) arrayMap53.get(Integer.valueOf(intValue2));
                                            if (zzbb) {
                                                bitSet4 = bitSet17;
                                                map6 = (Map) arrayMap22.get(Integer.valueOf(intValue2));
                                                it4 = it3;
                                                arrayMap23 = arrayMap17;
                                                map7 = (Map) arrayMap23.get(Integer.valueOf(intValue2));
                                            } else {
                                                bitSet4 = bitSet17;
                                                it4 = it3;
                                                arrayMap23 = arrayMap17;
                                                map7 = null;
                                                map6 = null;
                                            }
                                            if (zzfr2 == null) {
                                                zzfr zzfr3 = new zzfr();
                                                arrayMap50.put(Integer.valueOf(intValue2), zzfr3);
                                                Map map14 = map7;
                                                zzfr3.zzawv = Boolean.valueOf(true);
                                                BitSet bitSet18 = new BitSet();
                                                arrayMap51.put(Integer.valueOf(intValue2), bitSet18);
                                                BitSet bitSet19 = new BitSet();
                                                arrayMap53.put(Integer.valueOf(intValue2), bitSet19);
                                                if (zzbb) {
                                                    ArrayMap arrayMap54 = new ArrayMap();
                                                    bitSet11 = bitSet18;
                                                    arrayMap22.put(Integer.valueOf(intValue2), arrayMap54);
                                                    ArrayMap arrayMap55 = new ArrayMap();
                                                    bitSet10 = bitSet19;
                                                    arrayMap23.put(Integer.valueOf(intValue2), arrayMap55);
                                                    arrayMap24 = arrayMap53;
                                                    arrayMap25 = arrayMap23;
                                                    map9 = arrayMap55;
                                                    map8 = arrayMap54;
                                                } else {
                                                    bitSet11 = bitSet18;
                                                    bitSet10 = bitSet19;
                                                    arrayMap24 = arrayMap53;
                                                    arrayMap25 = arrayMap23;
                                                    map8 = map6;
                                                    map9 = map14;
                                                }
                                                bitSet16 = bitSet11;
                                                bitSet6 = bitSet10;
                                            } else {
                                                arrayMap24 = arrayMap53;
                                                arrayMap25 = arrayMap23;
                                                bitSet6 = bitSet4;
                                                map8 = map6;
                                                map9 = map7;
                                            }
                                            Iterator it9 = ((List) map1322.get(Integer.valueOf(intValue2))).iterator();
                                            while (it9.hasNext()) {
                                                Map map15 = map1322;
                                                zzfj zzfj = (zzfj) it9.next();
                                                BitSet bitSet20 = bitSet6;
                                                ArrayMap arrayMap56 = arrayMap50;
                                                if (zzgt().isLoggable(2)) {
                                                    it5 = it9;
                                                    arrayMap27 = arrayMap22;
                                                    arrayMap26 = arrayMap51;
                                                    zzgt().zzjo().zzd("Evaluating filter. audience, filter, event", Integer.valueOf(intValue2), zzfj.zzavk, zzgq().zzbt(zzfj.zzavl));
                                                    zzgt().zzjo().zzg("Filter definition", zzjr().zza(zzfj));
                                                } else {
                                                    it5 = it9;
                                                    arrayMap27 = arrayMap22;
                                                    arrayMap26 = arrayMap51;
                                                }
                                                if (zzfj.zzavk == null || zzfj.zzavk.intValue() > 256) {
                                                    String str12 = str3;
                                                    Map map16 = map9;
                                                    Map map17 = map8;
                                                    zzft zzft11 = zzft3;
                                                    HashSet hashSet8 = hashSet7;
                                                    BitSet bitSet21 = bitSet20;
                                                    Iterator it10 = it5;
                                                    ArrayMap arrayMap57 = arrayMap27;
                                                    ArrayMap arrayMap58 = arrayMap26;
                                                    BitSet bitSet22 = bitSet5;
                                                    String str13 = str;
                                                    zzgt().zzjj().zze("Invalid event filter ID. appId, id", zzas.zzbw(str), String.valueOf(zzfj.zzavk));
                                                    bitSet7 = bitSet21;
                                                    hashSet7 = hashSet8;
                                                    bitSet5 = bitSet22;
                                                    zzft3 = zzft11;
                                                    it9 = it10;
                                                    str3 = str12;
                                                    map8 = map17;
                                                    map9 = map16;
                                                    map1322 = map15;
                                                    arrayMap50 = arrayMap56;
                                                    arrayMap51 = arrayMap58;
                                                    str5 = str13;
                                                    arrayMap28 = arrayMap57;
                                                } else {
                                                    if (zzbb) {
                                                        boolean z4 = (zzfj == null || zzfj.zzavh == null || !zzfj.zzavh.booleanValue()) ? false : true;
                                                        boolean z5 = (zzfj == null || zzfj.zzavi == null || !zzfj.zzavi.booleanValue()) ? false : true;
                                                        if (!bitSet5.get(zzfj.zzavk.intValue()) || z4 || z5) {
                                                            it6 = it5;
                                                            hashSet3 = hashSet7;
                                                            arrayMap30 = arrayMap27;
                                                            Map map18 = map8;
                                                            bitSet8 = bitSet5;
                                                            str6 = str3;
                                                            Map map19 = map9;
                                                            arrayMap29 = arrayMap26;
                                                            bitSet9 = bitSet20;
                                                            Boolean zza2 = zza(zzfj, str3, zzfuArr, j522);
                                                            zzgt().zzjo().zzg("Event filter result", zza2 == 0 ? "null" : zza2);
                                                            if (zza2 == 0) {
                                                                hashSet3.add(Integer.valueOf(intValue2));
                                                            } else {
                                                                bitSet9.set(zzfj.zzavk.intValue());
                                                                if (zza2.booleanValue()) {
                                                                    bitSet8.set(zzfj.zzavk.intValue());
                                                                    if (z4 || z5) {
                                                                        zzft4 = zzft3;
                                                                        if (zzft4.zzaxb == null) {
                                                                            bitSet7 = bitSet9;
                                                                            hashSet7 = hashSet3;
                                                                            bitSet5 = bitSet8;
                                                                            zzft3 = zzft4;
                                                                            it9 = it6;
                                                                            str3 = str6;
                                                                            map1322 = map15;
                                                                            arrayMap50 = arrayMap56;
                                                                            arrayMap28 = arrayMap30;
                                                                            map8 = map18;
                                                                            arrayMap51 = arrayMap29;
                                                                            map9 = map19;
                                                                            str5 = str;
                                                                        } else if (z5) {
                                                                            Map map20 = map19;
                                                                            zzb(map20, zzfj.zzavk.intValue(), zzft4.zzaxb.longValue());
                                                                            bitSet7 = bitSet9;
                                                                            hashSet7 = hashSet3;
                                                                            bitSet5 = bitSet8;
                                                                            zzft3 = zzft4;
                                                                            it9 = it6;
                                                                            map1322 = map15;
                                                                            arrayMap50 = arrayMap56;
                                                                            arrayMap28 = arrayMap30;
                                                                            map8 = map18;
                                                                            arrayMap51 = arrayMap29;
                                                                            str5 = str;
                                                                            map9 = map20;
                                                                            str3 = str6;
                                                                        } else {
                                                                            map11 = map19;
                                                                            map10 = map18;
                                                                            zza(map10, zzfj.zzavk.intValue(), zzft4.zzaxb.longValue());
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            bitSet7 = bitSet9;
                                                            hashSet7 = hashSet3;
                                                            bitSet5 = bitSet8;
                                                            it9 = it6;
                                                            str3 = str6;
                                                            map1322 = map15;
                                                            arrayMap50 = arrayMap56;
                                                            arrayMap28 = arrayMap30;
                                                            map8 = map18;
                                                            arrayMap51 = arrayMap29;
                                                            map9 = map19;
                                                            str5 = str;
                                                        } else {
                                                            zzgt().zzjo().zze("Event filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID", Integer.valueOf(intValue2), zzfj.zzavk);
                                                            map1322 = map15;
                                                            bitSet7 = bitSet20;
                                                            arrayMap50 = arrayMap56;
                                                            it9 = it5;
                                                            arrayMap28 = arrayMap27;
                                                            arrayMap51 = arrayMap26;
                                                            str5 = str;
                                                        }
                                                    } else {
                                                        str6 = str3;
                                                        map11 = map9;
                                                        map10 = map8;
                                                        zzft4 = zzft3;
                                                        hashSet3 = hashSet7;
                                                        bitSet9 = bitSet20;
                                                        it6 = it5;
                                                        arrayMap30 = arrayMap27;
                                                        arrayMap29 = arrayMap26;
                                                        bitSet8 = bitSet5;
                                                        if (bitSet8.get(zzfj.zzavk.intValue())) {
                                                            zzgt().zzjo().zze("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue2), zzfj.zzavk);
                                                        } else {
                                                            Map map21 = map11;
                                                            Map map22 = map10;
                                                            Boolean zza3 = zza(zzfj, str6, zzfuArr, j522);
                                                            zzgt().zzjo().zzg("Event filter result", zza3 == 0 ? "null" : zza3);
                                                            if (zza3 == 0) {
                                                                hashSet3.add(Integer.valueOf(intValue2));
                                                            } else {
                                                                bitSet9.set(zzfj.zzavk.intValue());
                                                                if (zza3.booleanValue()) {
                                                                    bitSet8.set(zzfj.zzavk.intValue());
                                                                }
                                                            }
                                                            bitSet7 = bitSet9;
                                                            hashSet7 = hashSet3;
                                                            bitSet5 = bitSet8;
                                                            zzft3 = zzft4;
                                                            it9 = it6;
                                                            str3 = str6;
                                                            map8 = map22;
                                                            map9 = map21;
                                                            map1322 = map15;
                                                            arrayMap50 = arrayMap56;
                                                            arrayMap28 = arrayMap30;
                                                            arrayMap51 = arrayMap29;
                                                            str5 = str;
                                                        }
                                                    }
                                                    bitSet7 = bitSet9;
                                                    hashSet7 = hashSet3;
                                                    bitSet5 = bitSet8;
                                                    zzft3 = zzft4;
                                                    it9 = it6;
                                                    map1322 = map15;
                                                    arrayMap50 = arrayMap56;
                                                    arrayMap28 = arrayMap30;
                                                    str5 = str;
                                                    map9 = map11;
                                                    map8 = map10;
                                                    str3 = str6;
                                                    arrayMap51 = arrayMap29;
                                                }
                                            }
                                            ArrayMap arrayMap59 = arrayMap22;
                                            String str14 = str4;
                                            arrayMap21 = arrayMap51;
                                            hashSet2 = hashSet7;
                                            arrayMap4922 = arrayMap52;
                                            it3 = it4;
                                            arrayMap17 = arrayMap25;
                                            arrayMap18 = arrayMap24;
                                            arrayMap19 = arrayMap50;
                                            arrayMap22 = arrayMap59;
                                        }
                                    }
                                    arrayMap12 = arrayMap22;
                                    arrayMap16 = arrayMap4922;
                                    str2 = str4;
                                    arrayMap15 = arrayMap17;
                                    arrayMap11 = arrayMap21;
                                    arrayMap13 = arrayMap19;
                                    arrayMap14 = arrayMap18;
                                    hashSet = hashSet2;
                                    zzft8 = zzft;
                                    l2 = l;
                                    j3 = j;
                                    i16 = i5 + 1;
                                    zzftArr2 = zzftArr;
                                    str8 = str2;
                                    hashSet6 = hashSet;
                                    length2 = i6;
                                    arrayMap48 = arrayMap16;
                                    arrayMap41 = arrayMap15;
                                    arrayMap46 = arrayMap14;
                                    arrayMap37 = arrayMap13;
                                    arrayMap45 = arrayMap12;
                                    arrayMap47 = arrayMap11;
                                }
                                arrayMap16 = arrayMap48;
                                arrayMap13 = arrayMap37;
                                str2 = str8;
                                hashSet = hashSet6;
                                arrayMap12 = arrayMap45;
                                arrayMap14 = arrayMap46;
                                arrayMap11 = arrayMap47;
                                arrayMap15 = arrayMap32;
                                i16 = i5 + 1;
                                zzftArr2 = zzftArr;
                                str8 = str2;
                                hashSet6 = hashSet;
                                length2 = i6;
                                arrayMap48 = arrayMap16;
                                arrayMap41 = arrayMap15;
                                arrayMap46 = arrayMap14;
                                arrayMap37 = arrayMap13;
                                arrayMap45 = arrayMap12;
                                arrayMap47 = arrayMap11;
                            } else {
                                zzft5 = zzft9;
                                i7 = i16;
                                arrayMap31 = arrayMap41;
                                i8 = i9;
                                if (z3) {
                                    zzjr();
                                    Object valueOf = Long.valueOf(0);
                                    Object zzb = zzfu.zzb(zzft5, "_epc");
                                    if (zzb == null) {
                                        zzb = valueOf;
                                    }
                                    j3 = ((Long) zzb).longValue();
                                    if (j3 <= 0) {
                                        zzgt().zzjj().zzg("Complex event with zero extra param count. eventName", str9);
                                        j2 = 0;
                                    } else {
                                        j2 = 0;
                                        zzjt().zza(str8, l3, j3, zzft5);
                                    }
                                    l = l3;
                                    str7 = str9;
                                    zzft6 = zzft5;
                                    zzfuArr = zzfuArr3;
                                    j = j3;
                                    zzg = zzjt().zzg(str8, zzft2.name);
                                    if (zzg == null) {
                                    }
                                    zzjt().zza(zzac);
                                    long j5222 = zzac.zzahv;
                                    ArrayMap arrayMap49222 = arrayMap20;
                                    map5 = (Map) arrayMap49222.get(str3);
                                    if (map5 == null) {
                                    }
                                    Map map13222 = map5;
                                    it3 = map13222.keySet().iterator();
                                    while (it3.hasNext()) {
                                    }
                                    arrayMap12 = arrayMap22;
                                    arrayMap16 = arrayMap49222;
                                    str2 = str4;
                                    arrayMap15 = arrayMap17;
                                    arrayMap11 = arrayMap21;
                                    arrayMap13 = arrayMap19;
                                    arrayMap14 = arrayMap18;
                                    hashSet = hashSet2;
                                    zzft8 = zzft;
                                    l2 = l;
                                    j3 = j;
                                    i16 = i5 + 1;
                                    zzftArr2 = zzftArr;
                                    str8 = str2;
                                    hashSet6 = hashSet;
                                    length2 = i6;
                                    arrayMap48 = arrayMap16;
                                    arrayMap41 = arrayMap15;
                                    arrayMap46 = arrayMap14;
                                    arrayMap37 = arrayMap13;
                                    arrayMap45 = arrayMap12;
                                    arrayMap47 = arrayMap11;
                                }
                            }
                        }
                    } else {
                        i9 = length2;
                    }
                    z = false;
                    if (!z) {
                    }
                } else {
                    zzft5 = zzft9;
                    i8 = length2;
                    i7 = i16;
                    arrayMap31 = arrayMap41;
                }
                j2 = 0;
                zzft6 = zzft8;
                l = l2;
                str7 = str9;
                zzfuArr = zzfuArr3;
                j = j3;
                zzg = zzjt().zzg(str8, zzft2.name);
                if (zzg == null) {
                }
                zzjt().zza(zzac);
                long j52222 = zzac.zzahv;
                ArrayMap arrayMap492222 = arrayMap20;
                map5 = (Map) arrayMap492222.get(str3);
                if (map5 == null) {
                }
                Map map132222 = map5;
                it3 = map132222.keySet().iterator();
                while (it3.hasNext()) {
                }
                arrayMap12 = arrayMap22;
                arrayMap16 = arrayMap492222;
                str2 = str4;
                arrayMap15 = arrayMap17;
                arrayMap11 = arrayMap21;
                arrayMap13 = arrayMap19;
                arrayMap14 = arrayMap18;
                hashSet = hashSet2;
                zzft8 = zzft;
                l2 = l;
                j3 = j;
                i16 = i5 + 1;
                zzftArr2 = zzftArr;
                str8 = str2;
                hashSet6 = hashSet;
                length2 = i6;
                arrayMap48 = arrayMap16;
                arrayMap41 = arrayMap15;
                arrayMap46 = arrayMap14;
                arrayMap37 = arrayMap13;
                arrayMap45 = arrayMap12;
                arrayMap47 = arrayMap11;
            }
        }
        ArrayMap arrayMap60 = arrayMap41;
        ArrayMap arrayMap61 = arrayMap37;
        String str15 = str8;
        HashSet hashSet9 = hashSet6;
        ArrayMap arrayMap62 = arrayMap45;
        ArrayMap arrayMap63 = arrayMap46;
        ArrayMap arrayMap64 = arrayMap47;
        zzfz[] zzfzArr6 = zzfzArr;
        if (zzfzArr6 != null) {
            ArrayMap arrayMap65 = new ArrayMap();
            int length4 = zzfzArr6.length;
            int i20 = 0;
            while (i20 < length4) {
                zzfz zzfz = zzfzArr6[i20];
                Map map23 = (Map) arrayMap65.get(zzfz.name);
                if (map23 == null) {
                    map23 = zzjt().zzm(str15, zzfz.name);
                    if (map23 == null) {
                        map23 = new ArrayMap();
                    }
                    arrayMap65.put(zzfz.name, map23);
                }
                Iterator it11 = map23.keySet().iterator();
                while (it11.hasNext()) {
                    int intValue3 = ((Integer) it11.next()).intValue();
                    if (hashSet9.contains(Integer.valueOf(intValue3))) {
                        zzgt().zzjo().zzg("Skipping failed audience ID", Integer.valueOf(intValue3));
                    } else {
                        ArrayMap arrayMap66 = arrayMap61;
                        zzfr zzfr4 = (zzfr) arrayMap66.get(Integer.valueOf(intValue3));
                        ArrayMap arrayMap67 = arrayMap64;
                        BitSet bitSet23 = (BitSet) arrayMap67.get(Integer.valueOf(intValue3));
                        ArrayMap arrayMap68 = arrayMap65;
                        ArrayMap arrayMap69 = arrayMap63;
                        BitSet bitSet24 = (BitSet) arrayMap69.get(Integer.valueOf(intValue3));
                        if (zzbb) {
                            bitSet = bitSet24;
                            i3 = length4;
                            arrayMap6 = arrayMap62;
                            map = (Map) arrayMap6.get(Integer.valueOf(intValue3));
                            it2 = it11;
                            arrayMap5 = arrayMap60;
                            map2 = (Map) arrayMap5.get(Integer.valueOf(intValue3));
                        } else {
                            bitSet = bitSet24;
                            i3 = length4;
                            it2 = it11;
                            arrayMap5 = arrayMap60;
                            arrayMap6 = arrayMap62;
                            map2 = null;
                            map = null;
                        }
                        if (zzfr4 == null) {
                            zzfr zzfr5 = new zzfr();
                            arrayMap66.put(Integer.valueOf(intValue3), zzfr5);
                            Map map24 = map2;
                            zzfr5.zzawv = Boolean.valueOf(true);
                            BitSet bitSet25 = new BitSet();
                            arrayMap67.put(Integer.valueOf(intValue3), bitSet25);
                            bitSet2 = new BitSet();
                            arrayMap69.put(Integer.valueOf(intValue3), bitSet2);
                            if (zzbb) {
                                ArrayMap arrayMap70 = new ArrayMap();
                                bitSet3 = bitSet25;
                                arrayMap6.put(Integer.valueOf(intValue3), arrayMap70);
                                ArrayMap arrayMap71 = new ArrayMap();
                                BitSet bitSet26 = bitSet2;
                                arrayMap5.put(Integer.valueOf(intValue3), arrayMap71);
                                arrayMap7 = arrayMap6;
                                arrayMap8 = arrayMap5;
                                bitSet2 = bitSet26;
                                map3 = arrayMap71;
                                map4 = arrayMap70;
                            } else {
                                bitSet3 = bitSet25;
                                BitSet bitSet27 = bitSet2;
                                arrayMap7 = arrayMap6;
                                arrayMap8 = arrayMap5;
                                map4 = map;
                                map3 = map24;
                            }
                            bitSet23 = bitSet3;
                        } else {
                            Map map25 = map2;
                            arrayMap7 = arrayMap6;
                            arrayMap8 = arrayMap5;
                            bitSet2 = bitSet;
                            map4 = map;
                            map3 = map25;
                        }
                        Iterator it12 = ((List) map23.get(Integer.valueOf(intValue3))).iterator();
                        while (true) {
                            if (!it12.hasNext()) {
                                arrayMap63 = arrayMap69;
                                arrayMap61 = arrayMap66;
                                arrayMap64 = arrayMap67;
                                arrayMap65 = arrayMap68;
                                length4 = i3;
                                it11 = it2;
                                arrayMap60 = arrayMap8;
                                arrayMap62 = arrayMap7;
                                break;
                            }
                            Iterator it13 = it12;
                            zzfm zzfm = (zzfm) it12.next();
                            Map map26 = map23;
                            ArrayMap arrayMap72 = arrayMap69;
                            if (zzgt().isLoggable(2)) {
                                arrayMap10 = arrayMap66;
                                arrayMap9 = arrayMap67;
                                i4 = i20;
                                zzgt().zzjo().zzd("Evaluating filter. audience, filter, property", Integer.valueOf(intValue3), zzfm.zzavk, zzgq().zzbv(zzfm.zzawa));
                                zzgt().zzjo().zzg("Filter definition", zzjr().zza(zzfm));
                            } else {
                                i4 = i20;
                                arrayMap10 = arrayMap66;
                                arrayMap9 = arrayMap67;
                            }
                            if (zzfm.zzavk == null || zzfm.zzavk.intValue() > 256) {
                                str15 = str;
                                zzgt().zzjj().zze("Invalid property filter ID. appId, id", zzas.zzbw(str), String.valueOf(zzfm.zzavk));
                                hashSet9.add(Integer.valueOf(intValue3));
                                arrayMap65 = arrayMap68;
                                length4 = i3;
                                it11 = it2;
                                arrayMap60 = arrayMap8;
                                arrayMap62 = arrayMap7;
                                map23 = map26;
                                arrayMap63 = arrayMap72;
                                arrayMap61 = arrayMap10;
                                arrayMap64 = arrayMap9;
                                i20 = i4;
                            } else {
                                if (zzbb) {
                                    boolean z6 = (zzfm == null || zzfm.zzavh == null || !zzfm.zzavh.booleanValue()) ? false : true;
                                    boolean z7 = (zzfm == null || zzfm.zzavi == null || !zzfm.zzavi.booleanValue()) ? false : true;
                                    if (!bitSet23.get(zzfm.zzavk.intValue()) || z6 || z7) {
                                        Boolean zza4 = zza(zzfm, zzfz);
                                        zzgt().zzjo().zzg("Property filter result", zza4 == 0 ? "null" : zza4);
                                        if (zza4 == 0) {
                                            hashSet9.add(Integer.valueOf(intValue3));
                                        } else {
                                            bitSet2.set(zzfm.zzavk.intValue());
                                            bitSet23.set(zzfm.zzavk.intValue(), zza4.booleanValue());
                                            if (zza4.booleanValue() && ((z6 || z7) && zzfz.zzayu != null)) {
                                                if (z7) {
                                                    zzb(map3, zzfm.zzavk.intValue(), zzfz.zzayu.longValue());
                                                } else {
                                                    zza(map4, zzfm.zzavk.intValue(), zzfz.zzayu.longValue());
                                                }
                                            }
                                        }
                                    } else {
                                        zzgt().zzjo().zze("Property filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID", Integer.valueOf(intValue3), zzfm.zzavk);
                                    }
                                } else if (bitSet23.get(zzfm.zzavk.intValue())) {
                                    zzgt().zzjo().zze("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue3), zzfm.zzavk);
                                } else {
                                    Boolean zza5 = zza(zzfm, zzfz);
                                    zzgt().zzjo().zzg("Property filter result", zza5 == 0 ? "null" : zza5);
                                    if (zza5 == 0) {
                                        hashSet9.add(Integer.valueOf(intValue3));
                                    } else {
                                        bitSet2.set(zzfm.zzavk.intValue());
                                        if (zza5.booleanValue()) {
                                            bitSet23.set(zzfm.zzavk.intValue());
                                        }
                                    }
                                }
                                it12 = it13;
                                map23 = map26;
                                arrayMap69 = arrayMap72;
                                arrayMap66 = arrayMap10;
                                arrayMap67 = arrayMap9;
                                i20 = i4;
                                str15 = str;
                            }
                        }
                        zzfz[] zzfzArr7 = zzfzArr;
                    }
                }
                ArrayMap arrayMap73 = arrayMap65;
                int i21 = length4;
                ArrayMap arrayMap74 = arrayMap60;
                ArrayMap arrayMap75 = arrayMap63;
                ArrayMap arrayMap76 = arrayMap61;
                ArrayMap arrayMap77 = arrayMap62;
                ArrayMap arrayMap78 = arrayMap64;
                i20++;
                zzfzArr6 = zzfzArr;
            }
        }
        ArrayMap arrayMap79 = arrayMap60;
        ArrayMap arrayMap80 = arrayMap63;
        ArrayMap arrayMap81 = arrayMap61;
        ArrayMap arrayMap82 = arrayMap62;
        ArrayMap arrayMap83 = arrayMap64;
        zzfr[] zzfrArr = new zzfr[arrayMap83.size()];
        Iterator it14 = arrayMap83.keySet().iterator();
        int i22 = 0;
        while (it14.hasNext()) {
            int intValue4 = ((Integer) it14.next()).intValue();
            if (!hashSet9.contains(Integer.valueOf(intValue4))) {
                ArrayMap arrayMap84 = arrayMap81;
                zzfr zzfr6 = (zzfr) arrayMap84.get(Integer.valueOf(intValue4));
                if (zzfr6 == null) {
                    zzfr6 = new zzfr();
                }
                int i23 = i22 + 1;
                zzfrArr[i22] = zzfr6;
                zzfr6.zzave = Integer.valueOf(intValue4);
                zzfr6.zzawt = new zzfx();
                zzfr6.zzawt.zzayo = zzfu.zza((BitSet) arrayMap83.get(Integer.valueOf(intValue4)));
                ArrayMap arrayMap85 = arrayMap80;
                zzfr6.zzawt.zzayn = zzfu.zza((BitSet) arrayMap85.get(Integer.valueOf(intValue4)));
                if (zzbb) {
                    arrayMap4 = arrayMap82;
                    zzfr6.zzawt.zzayp = zzb((Map) arrayMap4.get(Integer.valueOf(intValue4)));
                    zzfx zzfx2 = zzfr6.zzawt;
                    arrayMap3 = arrayMap79;
                    Map map27 = (Map) arrayMap3.get(Integer.valueOf(intValue4));
                    if (map27 == null) {
                        arrayMap2 = arrayMap83;
                        zzfyArr = new zzfy[0];
                        it = it14;
                        arrayMap = arrayMap84;
                        i = i23;
                    } else {
                        arrayMap2 = arrayMap83;
                        zzfy[] zzfyArr2 = new zzfy[map27.size()];
                        it = it14;
                        Iterator it15 = map27.keySet().iterator();
                        int i24 = 0;
                        while (it15.hasNext()) {
                            Iterator it16 = it15;
                            Integer num = (Integer) it15.next();
                            ArrayMap arrayMap86 = arrayMap84;
                            zzfy zzfy = new zzfy();
                            zzfy.zzawx = num;
                            List list = (List) map27.get(num);
                            if (list != null) {
                                Collections.sort(list);
                                i2 = i23;
                                long[] jArr = new long[list.size()];
                                Iterator it17 = list.iterator();
                                int i25 = 0;
                                while (it17.hasNext()) {
                                    Iterator it18 = it17;
                                    int i26 = i25 + 1;
                                    jArr[i25] = ((Long) it17.next()).longValue();
                                    i25 = i26;
                                    it17 = it18;
                                }
                                zzfy.zzays = jArr;
                            } else {
                                i2 = i23;
                            }
                            int i27 = i24 + 1;
                            zzfyArr2[i24] = zzfy;
                            i24 = i27;
                            it15 = it16;
                            arrayMap84 = arrayMap86;
                            i23 = i2;
                        }
                        arrayMap = arrayMap84;
                        i = i23;
                        zzfyArr = zzfyArr2;
                    }
                    zzfx2.zzayq = zzfyArr;
                } else {
                    arrayMap2 = arrayMap83;
                    it = it14;
                    arrayMap = arrayMap84;
                    i = i23;
                    arrayMap3 = arrayMap79;
                    arrayMap4 = arrayMap82;
                }
                zzt zzjt2 = zzjt();
                zzfx zzfx3 = zzfr6.zzawt;
                zzjt2.zzcl();
                zzjt2.zzaf();
                Preconditions.checkNotEmpty(str);
                Preconditions.checkNotNull(zzfx3);
                try {
                    byte[] bArr = new byte[zzfx3.zzvx()];
                    try {
                        zzya zzk = zzya.zzk(bArr, 0, bArr.length);
                        zzfx3.zza(zzk);
                        zzk.zzza();
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("app_id", str15);
                        contentValues.put("audience_id", Integer.valueOf(intValue4));
                        contentValues.put("current_results", bArr);
                        try {
                            try {
                                if (zzjt2.getWritableDatabase().insertWithOnConflict("audience_filter_values", null, contentValues, 5) == -1) {
                                    zzjt2.zzgt().zzjg().zzg("Failed to insert filter results (got -1). appId", zzas.zzbw(str));
                                }
                            } catch (SQLiteException e4) {
                                e = e4;
                                zzjt2.zzgt().zzjg().zze("Error storing filter results. appId", zzas.zzbw(str), e);
                                arrayMap80 = arrayMap85;
                                arrayMap82 = arrayMap4;
                                arrayMap79 = arrayMap3;
                                arrayMap83 = arrayMap2;
                                it14 = it;
                                arrayMap81 = arrayMap;
                                i22 = i;
                            }
                        } catch (SQLiteException e5) {
                            e = e5;
                            zzjt2.zzgt().zzjg().zze("Error storing filter results. appId", zzas.zzbw(str), e);
                            arrayMap80 = arrayMap85;
                            arrayMap82 = arrayMap4;
                            arrayMap79 = arrayMap3;
                            arrayMap83 = arrayMap2;
                            it14 = it;
                            arrayMap81 = arrayMap;
                            i22 = i;
                        }
                    } catch (IOException e6) {
                        e = e6;
                        zzjt2.zzgt().zzjg().zze("Configuration loss. Failed to serialize filter results. appId", zzas.zzbw(str), e);
                        arrayMap80 = arrayMap85;
                        arrayMap82 = arrayMap4;
                        arrayMap79 = arrayMap3;
                        arrayMap83 = arrayMap2;
                        it14 = it;
                        arrayMap81 = arrayMap;
                        i22 = i;
                    }
                } catch (IOException e7) {
                    e = e7;
                    zzjt2.zzgt().zzjg().zze("Configuration loss. Failed to serialize filter results. appId", zzas.zzbw(str), e);
                    arrayMap80 = arrayMap85;
                    arrayMap82 = arrayMap4;
                    arrayMap79 = arrayMap3;
                    arrayMap83 = arrayMap2;
                    it14 = it;
                    arrayMap81 = arrayMap;
                    i22 = i;
                }
                arrayMap80 = arrayMap85;
                arrayMap82 = arrayMap4;
                arrayMap79 = arrayMap3;
                arrayMap83 = arrayMap2;
                it14 = it;
                arrayMap81 = arrayMap;
                i22 = i;
            }
        }
        return (zzfr[]) Arrays.copyOf(zzfrArr, i22);
    }

    private final Boolean zza(zzfj zzfj, String str, zzfu[] zzfuArr, long j) {
        zzfk[] zzfkArr;
        zzfk[] zzfkArr2;
        Boolean bool;
        if (zzfj.zzavo != null) {
            Boolean zza = zza(j, zzfj.zzavo);
            if (zza == null) {
                return null;
            }
            if (!zza.booleanValue()) {
                return Boolean.valueOf(false);
            }
        }
        HashSet hashSet = new HashSet();
        for (zzfk zzfk : zzfj.zzavm) {
            if (TextUtils.isEmpty(zzfk.zzavt)) {
                zzgt().zzjj().zzg("null or empty param name in filter. event", zzgq().zzbt(str));
                return null;
            }
            hashSet.add(zzfk.zzavt);
        }
        ArrayMap arrayMap = new ArrayMap();
        for (zzfu zzfu : zzfuArr) {
            if (hashSet.contains(zzfu.name)) {
                if (zzfu.zzaxe != null) {
                    arrayMap.put(zzfu.name, zzfu.zzaxe);
                } else if (zzfu.zzaun != null) {
                    arrayMap.put(zzfu.name, zzfu.zzaun);
                } else if (zzfu.zzaml != null) {
                    arrayMap.put(zzfu.name, zzfu.zzaml);
                } else {
                    zzgt().zzjj().zze("Unknown value for param. event, param", zzgq().zzbt(str), zzgq().zzbu(zzfu.name));
                    return null;
                }
            }
        }
        for (zzfk zzfk2 : zzfj.zzavm) {
            boolean equals = Boolean.TRUE.equals(zzfk2.zzavs);
            String str2 = zzfk2.zzavt;
            if (TextUtils.isEmpty(str2)) {
                zzgt().zzjj().zzg("Event has empty param name. event", zzgq().zzbt(str));
                return null;
            }
            Object obj = arrayMap.get(str2);
            if (obj instanceof Long) {
                if (zzfk2.zzavr == null) {
                    zzgt().zzjj().zze("No number filter for long param. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                    return null;
                }
                Boolean zza2 = zza(((Long) obj).longValue(), zzfk2.zzavr);
                if (zza2 == null) {
                    return null;
                }
                if ((true ^ zza2.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj instanceof Double) {
                if (zzfk2.zzavr == null) {
                    zzgt().zzjj().zze("No number filter for double param. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                    return null;
                }
                Boolean zza3 = zza(((Double) obj).doubleValue(), zzfk2.zzavr);
                if (zza3 == null) {
                    return null;
                }
                if ((true ^ zza3.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj instanceof String) {
                if (zzfk2.zzavq != null) {
                    bool = zza((String) obj, zzfk2.zzavq);
                } else if (zzfk2.zzavr != null) {
                    String str3 = (String) obj;
                    if (zzfu.zzcs(str3)) {
                        bool = zza(str3, zzfk2.zzavr);
                    } else {
                        zzgt().zzjj().zze("Invalid param value for number filter. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                        return null;
                    }
                } else {
                    zzgt().zzjj().zze("No filter for String param. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                    return null;
                }
                if (bool == null) {
                    return null;
                }
                if ((true ^ bool.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj == null) {
                zzgt().zzjo().zze("Missing param for filter. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                return Boolean.valueOf(false);
            } else {
                zzgt().zzjj().zze("Unknown param type. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                return null;
            }
        }
        return Boolean.valueOf(true);
    }

    private final Boolean zza(zzfm zzfm, zzfz zzfz) {
        zzfk zzfk = zzfm.zzawb;
        if (zzfk == null) {
            zzgt().zzjj().zzg("Missing property filter. property", zzgq().zzbv(zzfz.name));
            return null;
        }
        boolean equals = Boolean.TRUE.equals(zzfk.zzavs);
        if (zzfz.zzaxe != null) {
            if (zzfk.zzavr != null) {
                return zza(zza(zzfz.zzaxe.longValue(), zzfk.zzavr), equals);
            }
            zzgt().zzjj().zzg("No number filter for long property. property", zzgq().zzbv(zzfz.name));
            return null;
        } else if (zzfz.zzaun != null) {
            if (zzfk.zzavr != null) {
                return zza(zza(zzfz.zzaun.doubleValue(), zzfk.zzavr), equals);
            }
            zzgt().zzjj().zzg("No number filter for double property. property", zzgq().zzbv(zzfz.name));
            return null;
        } else if (zzfz.zzaml == null) {
            zzgt().zzjj().zzg("User property has no value, property", zzgq().zzbv(zzfz.name));
            return null;
        } else if (zzfk.zzavq != null) {
            return zza(zza(zzfz.zzaml, zzfk.zzavq), equals);
        } else {
            if (zzfk.zzavr == null) {
                zzgt().zzjj().zzg("No string or number filter defined. property", zzgq().zzbv(zzfz.name));
            } else if (zzfu.zzcs(zzfz.zzaml)) {
                return zza(zza(zzfz.zzaml, zzfk.zzavr), equals);
            } else {
                zzgt().zzjj().zze("Invalid user property value for Numeric number filter. property, value", zzgq().zzbv(zzfz.name), zzfz.zzaml);
            }
            return null;
        }
    }

    @VisibleForTesting
    private static Boolean zza(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    @VisibleForTesting
    private final Boolean zza(String str, zzfn zzfn) {
        String str2;
        List list;
        Preconditions.checkNotNull(zzfn);
        if (str == null || zzfn.zzawc == null || zzfn.zzawc.intValue() == 0) {
            return null;
        }
        if (zzfn.zzawc.intValue() == 6) {
            if (zzfn.zzawf == null || zzfn.zzawf.length == 0) {
                return null;
            }
        } else if (zzfn.zzawd == null) {
            return null;
        }
        int intValue = zzfn.zzawc.intValue();
        boolean z = zzfn.zzawe != null && zzfn.zzawe.booleanValue();
        if (z || intValue == 1 || intValue == 6) {
            str2 = zzfn.zzawd;
        } else {
            str2 = zzfn.zzawd.toUpperCase(Locale.ENGLISH);
        }
        String str3 = str2;
        if (zzfn.zzawf == null) {
            list = null;
        } else {
            String[] strArr = zzfn.zzawf;
            if (z) {
                list = Arrays.asList(strArr);
            } else {
                ArrayList arrayList = new ArrayList();
                for (String upperCase : strArr) {
                    arrayList.add(upperCase.toUpperCase(Locale.ENGLISH));
                }
                list = arrayList;
            }
        }
        return zza(str, intValue, z, str3, list, intValue == 1 ? str3 : null);
    }

    private final Boolean zza(String str, int i, boolean z, String str2, List<String> list, String str3) {
        if (str == null) {
            return null;
        }
        if (i == 6) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && i != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (i) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    zzgt().zzjj().zzg("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    private final Boolean zza(long j, zzfl zzfl) {
        try {
            return zza(new BigDecimal(j), zzfl, (double) Utils.DOUBLE_EPSILON);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private final Boolean zza(double d, zzfl zzfl) {
        try {
            return zza(new BigDecimal(d), zzfl, Math.ulp(d));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private final Boolean zza(String str, zzfl zzfl) {
        if (!zzfu.zzcs(str)) {
            return null;
        }
        try {
            return zza(new BigDecimal(str), zzfl, (double) Utils.DOUBLE_EPSILON);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0072, code lost:
        if (r3 != null) goto L_0x0074;
     */
    @VisibleForTesting
    private static Boolean zza(BigDecimal bigDecimal, zzfl zzfl, double d) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        Preconditions.checkNotNull(zzfl);
        if (zzfl.zzavu == null || zzfl.zzavu.intValue() == 0) {
            return null;
        }
        if (zzfl.zzavu.intValue() == 4) {
            if (zzfl.zzavx == null || zzfl.zzavy == null) {
                return null;
            }
        } else if (zzfl.zzavw == null) {
            return null;
        }
        int intValue = zzfl.zzavu.intValue();
        if (zzfl.zzavu.intValue() == 4) {
            if (!zzfu.zzcs(zzfl.zzavx) || !zzfu.zzcs(zzfl.zzavy)) {
                return null;
            }
            try {
                BigDecimal bigDecimal5 = new BigDecimal(zzfl.zzavx);
                bigDecimal3 = new BigDecimal(zzfl.zzavy);
                bigDecimal2 = bigDecimal5;
                bigDecimal4 = null;
            } catch (NumberFormatException unused) {
                return null;
            }
        } else if (!zzfu.zzcs(zzfl.zzavw)) {
            return null;
        } else {
            try {
                bigDecimal4 = new BigDecimal(zzfl.zzavw);
                bigDecimal2 = null;
                bigDecimal3 = null;
            } catch (NumberFormatException unused2) {
                return null;
            }
        }
        if (intValue == 4) {
            if (bigDecimal2 == null) {
                return null;
            }
        }
        boolean z = false;
        switch (intValue) {
            case 1:
                if (bigDecimal.compareTo(bigDecimal4) == -1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 2:
                if (bigDecimal.compareTo(bigDecimal4) == 1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 3:
                if (d != Utils.DOUBLE_EPSILON) {
                    if (bigDecimal.compareTo(bigDecimal4.subtract(new BigDecimal(d).multiply(new BigDecimal(2)))) == 1 && bigDecimal.compareTo(bigDecimal4.add(new BigDecimal(d).multiply(new BigDecimal(2)))) == -1) {
                        z = true;
                    }
                    return Boolean.valueOf(z);
                }
                if (bigDecimal.compareTo(bigDecimal4) == 0) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 4:
                if (!(bigDecimal.compareTo(bigDecimal2) == -1 || bigDecimal.compareTo(bigDecimal3) == 1)) {
                    z = true;
                }
                return Boolean.valueOf(z);
        }
        return null;
    }

    private static zzfs[] zzb(Map<Integer, Long> map) {
        if (map == null) {
            return null;
        }
        int i = 0;
        zzfs[] zzfsArr = new zzfs[map.size()];
        for (Integer num : map.keySet()) {
            zzfs zzfs = new zzfs();
            zzfs.zzawx = num;
            zzfs.zzawy = (Long) map.get(num);
            int i2 = i + 1;
            zzfsArr[i] = zzfs;
            i = i2;
        }
        return zzfsArr;
    }

    private static void zza(Map<Integer, Long> map, int i, long j) {
        Long l = (Long) map.get(Integer.valueOf(i));
        long j2 = j / 1000;
        if (l == null || j2 > l.longValue()) {
            map.put(Integer.valueOf(i), Long.valueOf(j2));
        }
    }

    private static void zzb(Map<Integer, List<Long>> map, int i, long j) {
        List list = (List) map.get(Integer.valueOf(i));
        if (list == null) {
            list = new ArrayList();
            map.put(Integer.valueOf(i), list);
        }
        list.add(Long.valueOf(j / 1000));
    }
}
