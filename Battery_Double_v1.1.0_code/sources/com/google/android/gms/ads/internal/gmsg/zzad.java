package com.google.android.gms.ads.internal.gmsg;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzaan;
import com.google.android.gms.internal.ads.zzayh;
import com.google.android.gms.internal.ads.zzcu;
import com.google.android.gms.internal.ads.zzwu;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@VisibleForTesting
public final class zzad {
    private final Context mContext;
    private final View mView;
    private final zzcu zzdgf;

    public zzad(Context context, zzcu zzcu, View view) {
        this.mContext = context;
        this.zzdgf = zzcu;
        this.mView = view;
    }

    private static Intent zze(Uri uri) {
        if (uri == null) {
            return null;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setData(uri);
        intent.setAction("android.intent.action.VIEW");
        return intent;
    }

    @VisibleForTesting
    private final ResolveInfo zzb(Intent intent) {
        return zza(intent, new ArrayList<>());
    }

    @VisibleForTesting
    private final ResolveInfo zza(Intent intent, ArrayList<ResolveInfo> arrayList) {
        ResolveInfo resolveInfo = null;
        try {
            PackageManager packageManager = this.mContext.getPackageManager();
            if (packageManager == null) {
                return null;
            }
            List queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 65536);
            if (queryIntentActivities != null && resolveActivity != null) {
                int i = 0;
                while (true) {
                    if (i >= queryIntentActivities.size()) {
                        break;
                    }
                    ResolveInfo resolveInfo2 = (ResolveInfo) queryIntentActivities.get(i);
                    if (resolveActivity != null && resolveActivity.activityInfo.name.equals(resolveInfo2.activityInfo.name)) {
                        resolveInfo = resolveActivity;
                        break;
                    }
                    i++;
                }
            }
            arrayList.addAll(queryIntentActivities);
            return resolveInfo;
        } catch (Throwable th) {
            zzbv.zzlj().zza(th, "OpenSystemBrowserHandler.getDefaultBrowserResolverForIntent");
        }
    }

    private static Intent zza(Intent intent, ResolveInfo resolveInfo) {
        Intent intent2 = new Intent(intent);
        intent2.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        return intent2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c4  */
    @VisibleForTesting
    public final Intent zzi(Map<String, String> map) {
        boolean z;
        ResolveInfo zza;
        ActivityManager activityManager = (ActivityManager) this.mContext.getSystemService("activity");
        String str = (String) map.get("u");
        Uri uri = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Uri parse = Uri.parse(zzac.zza(this.mContext, this.zzdgf, str, this.mView, null));
        boolean parseBoolean = Boolean.parseBoolean((String) map.get("use_first_package"));
        boolean parseBoolean2 = Boolean.parseBoolean((String) map.get("use_running_process"));
        if (!Boolean.parseBoolean((String) map.get("use_custom_tabs"))) {
            if (!((Boolean) zzwu.zzpz().zzd(zzaan.zzcwb)).booleanValue()) {
                z = false;
                if (!"http".equalsIgnoreCase(parse.getScheme())) {
                    uri = parse.buildUpon().scheme("https").build();
                } else if ("https".equalsIgnoreCase(parse.getScheme())) {
                    uri = parse.buildUpon().scheme("http").build();
                }
                ArrayList arrayList = new ArrayList();
                Intent zze = zze(parse);
                Intent zze2 = zze(uri);
                if (z) {
                    zzbv.zzlf();
                    zzayh.zzb(this.mContext, zze);
                    zzbv.zzlf();
                    zzayh.zzb(this.mContext, zze2);
                }
                zza = zza(zze, arrayList);
                if (zza == null) {
                    return zza(zze, zza);
                }
                if (zze2 != null) {
                    ResolveInfo zzb = zzb(zze2);
                    if (zzb != null) {
                        Intent zza2 = zza(zze, zzb);
                        if (zzb(zza2) != null) {
                            return zza2;
                        }
                    }
                }
                if (arrayList.size() == 0) {
                    return zze;
                }
                if (parseBoolean2 && activityManager != null) {
                    List runningAppProcesses = activityManager.getRunningAppProcesses();
                    if (runningAppProcesses != null) {
                        ArrayList arrayList2 = arrayList;
                        int size = arrayList2.size();
                        int i = 0;
                        while (i < size) {
                            Object obj = arrayList2.get(i);
                            i++;
                            ResolveInfo resolveInfo = (ResolveInfo) obj;
                            Iterator it = runningAppProcesses.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (((RunningAppProcessInfo) it.next()).processName.equals(resolveInfo.activityInfo.packageName)) {
                                        return zza(zze, resolveInfo);
                                    }
                                }
                            }
                        }
                    }
                }
                return parseBoolean ? zza(zze, (ResolveInfo) arrayList.get(0)) : zze;
            }
        }
        z = true;
        if (!"http".equalsIgnoreCase(parse.getScheme())) {
        }
        ArrayList arrayList3 = new ArrayList();
        Intent zze3 = zze(parse);
        Intent zze22 = zze(uri);
        if (z) {
        }
        zza = zza(zze3, arrayList3);
        if (zza == null) {
        }
    }
}
