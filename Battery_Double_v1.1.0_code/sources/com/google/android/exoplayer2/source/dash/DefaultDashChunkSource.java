package com.google.android.exoplayer2.source.dash;

import android.net.Uri;
import android.os.SystemClock;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.ChunkIndex;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor;
import com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor;
import com.google.android.exoplayer2.extractor.rawcc.RawCcExtractor;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.chunk.Chunk;
import com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper;
import com.google.android.exoplayer2.source.chunk.ChunkHolder;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.google.android.exoplayer2.source.chunk.ContainerMediaChunk;
import com.google.android.exoplayer2.source.chunk.InitializationChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.SingleSampleMediaChunk;
import com.google.android.exoplayer2.source.dash.manifest.AdaptationSet;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.source.dash.manifest.RangedUri;
import com.google.android.exoplayer2.source.dash.manifest.Representation;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException;
import com.google.android.exoplayer2.upstream.LoaderErrorThrower;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.List;

public class DefaultDashChunkSource implements DashChunkSource {
    private final int adaptationSetIndex;
    private final DataSource dataSource;
    private final long elapsedRealtimeOffsetMs;
    private IOException fatalError;
    private DashManifest manifest;
    private final LoaderErrorThrower manifestLoaderErrorThrower;
    private final int maxSegmentsPerLoad;
    private boolean missingLastSegment;
    private int periodIndex;
    private final RepresentationHolder[] representationHolders;
    private final TrackSelection trackSelection;

    public static final class Factory implements com.google.android.exoplayer2.source.dash.DashChunkSource.Factory {
        private final com.google.android.exoplayer2.upstream.DataSource.Factory dataSourceFactory;
        private final int maxSegmentsPerLoad;

        public Factory(com.google.android.exoplayer2.upstream.DataSource.Factory factory) {
            this(factory, 1);
        }

        public Factory(com.google.android.exoplayer2.upstream.DataSource.Factory factory, int i) {
            this.dataSourceFactory = factory;
            this.maxSegmentsPerLoad = i;
        }

        public DashChunkSource createDashChunkSource(LoaderErrorThrower loaderErrorThrower, DashManifest dashManifest, int i, int i2, TrackSelection trackSelection, long j, boolean z, boolean z2) {
            DefaultDashChunkSource defaultDashChunkSource = new DefaultDashChunkSource(loaderErrorThrower, dashManifest, i, i2, trackSelection, this.dataSourceFactory.createDataSource(), j, this.maxSegmentsPerLoad, z, z2);
            return defaultDashChunkSource;
        }
    }

    protected static final class RepresentationHolder {
        public final ChunkExtractorWrapper extractorWrapper;
        private long periodDurationUs;
        public Representation representation;
        public DashSegmentIndex segmentIndex;
        private int segmentNumShift;
        public final int trackType;

        public RepresentationHolder(long j, Representation representation2, boolean z, boolean z2, int i) {
            Extractor extractor;
            this.periodDurationUs = j;
            this.representation = representation2;
            this.trackType = i;
            String str = representation2.format.containerMimeType;
            if (mimeTypeIsRawText(str)) {
                this.extractorWrapper = null;
            } else {
                if (MimeTypes.APPLICATION_RAWCC.equals(str)) {
                    extractor = new RawCcExtractor(representation2.format);
                } else if (mimeTypeIsWebm(str)) {
                    extractor = new MatroskaExtractor(1);
                } else {
                    int i2 = 0;
                    if (z) {
                        i2 = 4;
                    }
                    if (z2) {
                        i2 |= 8;
                    }
                    extractor = new FragmentedMp4Extractor(i2);
                }
                this.extractorWrapper = new ChunkExtractorWrapper(extractor, representation2.format);
            }
            this.segmentIndex = representation2.getIndex();
        }

        public void updateRepresentation(long j, Representation representation2) throws BehindLiveWindowException {
            DashSegmentIndex index = this.representation.getIndex();
            DashSegmentIndex index2 = representation2.getIndex();
            this.periodDurationUs = j;
            this.representation = representation2;
            if (index != null) {
                this.segmentIndex = index2;
                if (index.isExplicit()) {
                    int segmentCount = index.getSegmentCount(this.periodDurationUs);
                    if (segmentCount != 0) {
                        int firstSegmentNum = (index.getFirstSegmentNum() + segmentCount) - 1;
                        long timeUs = index.getTimeUs(firstSegmentNum) + index.getDurationUs(firstSegmentNum, this.periodDurationUs);
                        int firstSegmentNum2 = index2.getFirstSegmentNum();
                        long timeUs2 = index2.getTimeUs(firstSegmentNum2);
                        int i = (timeUs > timeUs2 ? 1 : (timeUs == timeUs2 ? 0 : -1));
                        if (i == 0) {
                            this.segmentNumShift += (firstSegmentNum + 1) - firstSegmentNum2;
                        } else if (i < 0) {
                            throw new BehindLiveWindowException();
                        } else {
                            this.segmentNumShift += index.getSegmentNum(timeUs2, this.periodDurationUs) - firstSegmentNum2;
                        }
                    }
                }
            }
        }

        public int getFirstSegmentNum() {
            return this.segmentIndex.getFirstSegmentNum() + this.segmentNumShift;
        }

        public int getSegmentCount() {
            return this.segmentIndex.getSegmentCount(this.periodDurationUs);
        }

        public long getSegmentStartTimeUs(int i) {
            return this.segmentIndex.getTimeUs(i - this.segmentNumShift);
        }

        public long getSegmentEndTimeUs(int i) {
            return getSegmentStartTimeUs(i) + this.segmentIndex.getDurationUs(i - this.segmentNumShift, this.periodDurationUs);
        }

        public int getSegmentNum(long j) {
            return this.segmentIndex.getSegmentNum(j, this.periodDurationUs) + this.segmentNumShift;
        }

        public RangedUri getSegmentUrl(int i) {
            return this.segmentIndex.getSegmentUrl(i - this.segmentNumShift);
        }

        private static boolean mimeTypeIsWebm(String str) {
            return str.startsWith(MimeTypes.VIDEO_WEBM) || str.startsWith(MimeTypes.AUDIO_WEBM) || str.startsWith(MimeTypes.APPLICATION_WEBM);
        }

        private static boolean mimeTypeIsRawText(String str) {
            return MimeTypes.isText(str) || MimeTypes.APPLICATION_TTML.equals(str);
        }
    }

    public DefaultDashChunkSource(LoaderErrorThrower loaderErrorThrower, DashManifest dashManifest, int i, int i2, TrackSelection trackSelection2, DataSource dataSource2, long j, int i3, boolean z, boolean z2) {
        TrackSelection trackSelection3 = trackSelection2;
        this.manifestLoaderErrorThrower = loaderErrorThrower;
        this.manifest = dashManifest;
        this.adaptationSetIndex = i2;
        this.trackSelection = trackSelection3;
        this.dataSource = dataSource2;
        this.periodIndex = i;
        this.elapsedRealtimeOffsetMs = j;
        this.maxSegmentsPerLoad = i3;
        long periodDurationUs = dashManifest.getPeriodDurationUs(i);
        AdaptationSet adaptationSet = getAdaptationSet();
        List<Representation> list = adaptationSet.representations;
        this.representationHolders = new RepresentationHolder[trackSelection2.length()];
        for (int i4 = 0; i4 < this.representationHolders.length; i4++) {
            Representation representation = (Representation) list.get(trackSelection3.getIndexInTrackGroup(i4));
            RepresentationHolder[] representationHolderArr = this.representationHolders;
            RepresentationHolder representationHolder = new RepresentationHolder(periodDurationUs, representation, z, z2, adaptationSet.type);
            representationHolderArr[i4] = representationHolder;
        }
    }

    public void updateManifest(DashManifest dashManifest, int i) {
        try {
            this.manifest = dashManifest;
            this.periodIndex = i;
            long periodDurationUs = this.manifest.getPeriodDurationUs(this.periodIndex);
            List<Representation> list = getAdaptationSet().representations;
            for (int i2 = 0; i2 < this.representationHolders.length; i2++) {
                this.representationHolders[i2].updateRepresentation(periodDurationUs, (Representation) list.get(this.trackSelection.getIndexInTrackGroup(i2)));
            }
        } catch (BehindLiveWindowException e) {
            this.fatalError = e;
        }
    }

    public void maybeThrowError() throws IOException {
        if (this.fatalError != null) {
            throw this.fatalError;
        }
        this.manifestLoaderErrorThrower.maybeThrowError();
    }

    public int getPreferredQueueSize(long j, List<? extends MediaChunk> list) {
        if (this.fatalError != null || this.trackSelection.length() < 2) {
            return list.size();
        }
        return this.trackSelection.evaluateQueueSize(j, list);
    }

    public final void getNextChunk(MediaChunk mediaChunk, long j, ChunkHolder chunkHolder) {
        int i;
        int nextChunkIndex;
        MediaChunk mediaChunk2 = mediaChunk;
        long j2 = j;
        ChunkHolder chunkHolder2 = chunkHolder;
        if (this.fatalError == null) {
            this.trackSelection.updateSelectedTrack(mediaChunk2 != null ? mediaChunk2.endTimeUs - j2 : 0);
            RepresentationHolder representationHolder = this.representationHolders[this.trackSelection.getSelectedIndex()];
            if (representationHolder.extractorWrapper != null) {
                Representation representation = representationHolder.representation;
                RangedUri initializationUri = representationHolder.extractorWrapper.getSampleFormats() == null ? representation.getInitializationUri() : null;
                RangedUri indexUri = representationHolder.segmentIndex == null ? representation.getIndexUri() : null;
                if (!(initializationUri == null && indexUri == null)) {
                    chunkHolder2.chunk = newInitializationChunk(representationHolder, this.dataSource, this.trackSelection.getSelectedFormat(), this.trackSelection.getSelectionReason(), this.trackSelection.getSelectionData(), initializationUri, indexUri);
                    return;
                }
            }
            long nowUnixTimeUs = getNowUnixTimeUs();
            int segmentCount = representationHolder.getSegmentCount();
            boolean z = false;
            if (segmentCount == 0) {
                if (!this.manifest.dynamic || this.periodIndex < this.manifest.getPeriodCount() - 1) {
                    z = true;
                }
                chunkHolder2.endOfStream = z;
                return;
            }
            int firstSegmentNum = representationHolder.getFirstSegmentNum();
            if (segmentCount == -1) {
                long j3 = (nowUnixTimeUs - (this.manifest.availabilityStartTime * 1000)) - (this.manifest.getPeriod(this.periodIndex).startMs * 1000);
                if (this.manifest.timeShiftBufferDepth != C.TIME_UNSET) {
                    firstSegmentNum = Math.max(firstSegmentNum, representationHolder.getSegmentNum(j3 - (this.manifest.timeShiftBufferDepth * 1000)));
                }
                i = representationHolder.getSegmentNum(j3) - 1;
            } else {
                i = (segmentCount + firstSegmentNum) - 1;
            }
            if (mediaChunk2 == null) {
                nextChunkIndex = Util.constrainValue(representationHolder.getSegmentNum(j2), firstSegmentNum, i);
            } else {
                nextChunkIndex = mediaChunk.getNextChunkIndex();
                if (nextChunkIndex < firstSegmentNum) {
                    this.fatalError = new BehindLiveWindowException();
                    return;
                }
            }
            int i2 = nextChunkIndex;
            if (i2 > i || (this.missingLastSegment && i2 >= i)) {
                if (!this.manifest.dynamic || this.periodIndex < this.manifest.getPeriodCount() - 1) {
                    z = true;
                }
                chunkHolder2.endOfStream = z;
                return;
            }
            chunkHolder2.chunk = newMediaChunk(representationHolder, this.dataSource, this.trackSelection.getSelectedFormat(), this.trackSelection.getSelectionReason(), this.trackSelection.getSelectionData(), i2, Math.min(this.maxSegmentsPerLoad, (i - i2) + 1));
        }
    }

    public void onChunkLoadCompleted(Chunk chunk) {
        if (chunk instanceof InitializationChunk) {
            RepresentationHolder representationHolder = this.representationHolders[this.trackSelection.indexOf(((InitializationChunk) chunk).trackFormat)];
            if (representationHolder.segmentIndex == null) {
                SeekMap seekMap = representationHolder.extractorWrapper.getSeekMap();
                if (seekMap != null) {
                    representationHolder.segmentIndex = new DashWrappingSegmentIndex((ChunkIndex) seekMap);
                }
            }
        }
    }

    public boolean onChunkLoadError(Chunk chunk, boolean z, Exception exc) {
        if (!z) {
            return false;
        }
        if (!this.manifest.dynamic && (chunk instanceof MediaChunk) && (exc instanceof InvalidResponseCodeException) && ((InvalidResponseCodeException) exc).responseCode == 404) {
            RepresentationHolder representationHolder = this.representationHolders[this.trackSelection.indexOf(chunk.trackFormat)];
            int segmentCount = representationHolder.getSegmentCount();
            if (!(segmentCount == -1 || segmentCount == 0 || ((MediaChunk) chunk).getNextChunkIndex() <= (representationHolder.getFirstSegmentNum() + segmentCount) - 1)) {
                this.missingLastSegment = true;
                return true;
            }
        }
        return ChunkedTrackBlacklistUtil.maybeBlacklistTrack(this.trackSelection, this.trackSelection.indexOf(chunk.trackFormat), exc);
    }

    private AdaptationSet getAdaptationSet() {
        return (AdaptationSet) this.manifest.getPeriod(this.periodIndex).adaptationSets.get(this.adaptationSetIndex);
    }

    private long getNowUnixTimeUs() {
        if (this.elapsedRealtimeOffsetMs != 0) {
            return (SystemClock.elapsedRealtime() + this.elapsedRealtimeOffsetMs) * 1000;
        }
        return System.currentTimeMillis() * 1000;
    }

    private static Chunk newInitializationChunk(RepresentationHolder representationHolder, DataSource dataSource2, Format format, int i, Object obj, RangedUri rangedUri, RangedUri rangedUri2) {
        String str = representationHolder.representation.baseUrl;
        if (rangedUri != null) {
            rangedUri2 = rangedUri.attemptMerge(rangedUri2, str);
            if (rangedUri2 == null) {
                rangedUri2 = rangedUri;
            }
        }
        DataSpec dataSpec = new DataSpec(rangedUri2.resolveUri(str), rangedUri2.start, rangedUri2.length, representationHolder.representation.getCacheKey());
        InitializationChunk initializationChunk = new InitializationChunk(dataSource2, dataSpec, format, i, obj, representationHolder.extractorWrapper);
        return initializationChunk;
    }

    private static Chunk newMediaChunk(RepresentationHolder representationHolder, DataSource dataSource2, Format format, int i, Object obj, int i2, int i3) {
        RepresentationHolder representationHolder2 = representationHolder;
        int i4 = i2;
        Representation representation = representationHolder2.representation;
        long segmentStartTimeUs = representationHolder2.getSegmentStartTimeUs(i4);
        RangedUri segmentUrl = representationHolder2.getSegmentUrl(i4);
        String str = representation.baseUrl;
        if (representationHolder2.extractorWrapper == null) {
            long segmentEndTimeUs = representationHolder2.getSegmentEndTimeUs(i4);
            DataSpec dataSpec = new DataSpec(segmentUrl.resolveUri(str), segmentUrl.start, segmentUrl.length, representation.getCacheKey());
            SingleSampleMediaChunk singleSampleMediaChunk = new SingleSampleMediaChunk(dataSource2, dataSpec, format, i, obj, segmentStartTimeUs, segmentEndTimeUs, i4, representationHolder2.trackType, format);
            return singleSampleMediaChunk;
        }
        RangedUri rangedUri = segmentUrl;
        int i5 = 1;
        int i6 = 1;
        int i7 = i3;
        while (i5 < i7) {
            RangedUri attemptMerge = rangedUri.attemptMerge(representationHolder2.getSegmentUrl(i4 + i5), str);
            if (attemptMerge == null) {
                break;
            }
            i6++;
            i5++;
            rangedUri = attemptMerge;
        }
        long segmentEndTimeUs2 = representationHolder2.getSegmentEndTimeUs((i4 + i6) - 1);
        Uri resolveUri = rangedUri.resolveUri(str);
        long j = rangedUri.start;
        DataSpec dataSpec2 = new DataSpec(resolveUri, j, rangedUri.length, representation.getCacheKey());
        ContainerMediaChunk containerMediaChunk = new ContainerMediaChunk(dataSource2, dataSpec2, format, i, obj, segmentStartTimeUs, segmentEndTimeUs2, i4, i6, -representation.presentationTimeOffsetUs, representationHolder2.extractorWrapper);
        return containerMediaChunk;
    }
}
