package com.google.android.exoplayer2.source.dash.manifest;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.Util;
import java.util.List;

public abstract class SegmentBase {
    final RangedUri initialization;
    final long presentationTimeOffset;
    final long timescale;

    public static abstract class MultiSegmentBase extends SegmentBase {
        final long duration;
        final List<SegmentTimelineElement> segmentTimeline;
        final int startNumber;

        public abstract int getSegmentCount(long j);

        public abstract RangedUri getSegmentUrl(Representation representation, int i);

        public MultiSegmentBase(RangedUri rangedUri, long j, long j2, int i, long j3, List<SegmentTimelineElement> list) {
            super(rangedUri, j, j2);
            this.startNumber = i;
            this.duration = j3;
            this.segmentTimeline = list;
        }

        public int getSegmentNum(long j, long j2) {
            int firstSegmentNum = getFirstSegmentNum();
            int segmentCount = getSegmentCount(j2);
            if (segmentCount == 0) {
                return firstSegmentNum;
            }
            if (this.segmentTimeline == null) {
                int i = ((int) (j / ((this.duration * C.MICROS_PER_SECOND) / this.timescale))) + this.startNumber;
                if (i >= firstSegmentNum) {
                    if (segmentCount == -1) {
                        firstSegmentNum = i;
                    } else {
                        firstSegmentNum = Math.min(i, (firstSegmentNum + segmentCount) - 1);
                    }
                }
                return firstSegmentNum;
            }
            int i2 = (segmentCount + firstSegmentNum) - 1;
            int i3 = firstSegmentNum;
            while (i3 <= i2) {
                int i4 = ((i2 - i3) / 2) + i3;
                int i5 = (getSegmentTimeUs(i4) > j ? 1 : (getSegmentTimeUs(i4) == j ? 0 : -1));
                if (i5 < 0) {
                    i3 = i4 + 1;
                } else if (i5 <= 0) {
                    return i4;
                } else {
                    i2 = i4 - 1;
                }
            }
            if (i3 != firstSegmentNum) {
                i3 = i2;
            }
            return i3;
        }

        public final long getSegmentDurationUs(int i, long j) {
            if (this.segmentTimeline != null) {
                return (((SegmentTimelineElement) this.segmentTimeline.get(i - this.startNumber)).duration * C.MICROS_PER_SECOND) / this.timescale;
            }
            int segmentCount = getSegmentCount(j);
            return (segmentCount == -1 || i != (getFirstSegmentNum() + segmentCount) + -1) ? (this.duration * C.MICROS_PER_SECOND) / this.timescale : j - getSegmentTimeUs(i);
        }

        public final long getSegmentTimeUs(int i) {
            long j;
            if (this.segmentTimeline != null) {
                j = ((SegmentTimelineElement) this.segmentTimeline.get(i - this.startNumber)).startTime - this.presentationTimeOffset;
            } else {
                j = ((long) (i - this.startNumber)) * this.duration;
            }
            return Util.scaleLargeTimestamp(j, C.MICROS_PER_SECOND, this.timescale);
        }

        public int getFirstSegmentNum() {
            return this.startNumber;
        }

        public boolean isExplicit() {
            return this.segmentTimeline != null;
        }
    }

    public static class SegmentList extends MultiSegmentBase {
        final List<RangedUri> mediaSegments;

        public boolean isExplicit() {
            return true;
        }

        public SegmentList(RangedUri rangedUri, long j, long j2, int i, long j3, List<SegmentTimelineElement> list, List<RangedUri> list2) {
            super(rangedUri, j, j2, i, j3, list);
            this.mediaSegments = list2;
        }

        public RangedUri getSegmentUrl(Representation representation, int i) {
            return (RangedUri) this.mediaSegments.get(i - this.startNumber);
        }

        public int getSegmentCount(long j) {
            return this.mediaSegments.size();
        }
    }

    public static class SegmentTemplate extends MultiSegmentBase {
        final UrlTemplate initializationTemplate;
        final UrlTemplate mediaTemplate;

        public SegmentTemplate(RangedUri rangedUri, long j, long j2, int i, long j3, List<SegmentTimelineElement> list, UrlTemplate urlTemplate, UrlTemplate urlTemplate2) {
            super(rangedUri, j, j2, i, j3, list);
            this.initializationTemplate = urlTemplate;
            this.mediaTemplate = urlTemplate2;
        }

        public RangedUri getInitialization(Representation representation) {
            if (this.initializationTemplate == null) {
                return super.getInitialization(representation);
            }
            RangedUri rangedUri = new RangedUri(this.initializationTemplate.buildUri(representation.format.id, 0, representation.format.bitrate, 0), 0, -1);
            return rangedUri;
        }

        public RangedUri getSegmentUrl(Representation representation, int i) {
            long j;
            Representation representation2 = representation;
            if (this.segmentTimeline != null) {
                j = ((SegmentTimelineElement) this.segmentTimeline.get(i - this.startNumber)).startTime;
            } else {
                j = ((long) (i - this.startNumber)) * this.duration;
            }
            long j2 = j;
            RangedUri rangedUri = new RangedUri(this.mediaTemplate.buildUri(representation2.format.id, i, representation2.format.bitrate, j2), 0, -1);
            return rangedUri;
        }

        public int getSegmentCount(long j) {
            if (this.segmentTimeline != null) {
                return this.segmentTimeline.size();
            }
            if (j != C.TIME_UNSET) {
                return (int) Util.ceilDivide(j, (this.duration * C.MICROS_PER_SECOND) / this.timescale);
            }
            return -1;
        }
    }

    public static class SegmentTimelineElement {
        final long duration;
        final long startTime;

        public SegmentTimelineElement(long j, long j2) {
            this.startTime = j;
            this.duration = j2;
        }
    }

    public static class SingleSegmentBase extends SegmentBase {
        final long indexLength;
        final long indexStart;

        public SingleSegmentBase(RangedUri rangedUri, long j, long j2, long j3, long j4) {
            super(rangedUri, j, j2);
            this.indexStart = j3;
            this.indexLength = j4;
        }

        public SingleSegmentBase() {
            this(null, 1, 0, 0, 0);
        }

        public RangedUri getIndex() {
            if (this.indexLength <= 0) {
                return null;
            }
            RangedUri rangedUri = new RangedUri(null, this.indexStart, this.indexLength);
            return rangedUri;
        }
    }

    public SegmentBase(RangedUri rangedUri, long j, long j2) {
        this.initialization = rangedUri;
        this.timescale = j;
        this.presentationTimeOffset = j2;
    }

    public RangedUri getInitialization(Representation representation) {
        return this.initialization;
    }

    public long getPresentationTimeOffsetUs() {
        return Util.scaleLargeTimestamp(this.presentationTimeOffset, C.MICROS_PER_SECOND, this.timescale);
    }
}
